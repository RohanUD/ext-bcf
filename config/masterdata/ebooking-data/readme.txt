Import below files(attached) strictly in same order:
Order of import:
1. Terminal (terminal) - Rename the file terminal_data to terminal_data-001.csv
3. Route (route) - route_data-001.csv
4. ShipFacilities (import directly from hac) - ShipFacilities.impex
5. ShipInfo (shipinfo) - shipinfo_test_data-001.csv
6. Schedule (schedule) - schedule_data-001.csv
7. Ameneties (amenities) - amenities_amenities_test_data-001.csv

Sync Product catalogs.
Run full indexing.


Routes for Testing:
a) Northern Routes

PPH-BEC-4 nov(transfer Route)
PPH-PBB-3 nov(direct Route)
PBB-BEC-4 nov(stopover Route)

b) Normal Routes

POB-PST (4th November)
POF-BEC (4th November)
SHW-POF (24th November)
POF-SHW (21st December)
