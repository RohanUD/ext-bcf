/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.RouteBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.TransportBundleTemplateModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;


public class DestinationLocationCodeValueResolver extends BCFAbstractValueResolver
{

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{
		final List<TransportBundleTemplateModel> transportTemplates = getTransportBundles(dealBundleTemplate);

		final List<RouteBundleTemplateModel> routeBundleTemplateModels = new ArrayList<>();
		transportTemplates.forEach(transportBundle ->
				routeBundleTemplateModels.add((RouteBundleTemplateModel) transportBundle.getChildTemplates().stream()
						.filter(bundle -> ((RouteBundleTemplateModel) bundle).getOriginDestinationRefNumber().equals(0))
						.findFirst().get()));

		if (CollectionUtils.isNotEmpty(routeBundleTemplateModels))
		{
			final TransportFacilityModel transportFacilityModel = routeBundleTemplateModels.get(0).getTravelRoute().getDestination();
				final String destinationCode = transportFacilityModel.getLocation().getCode();
				document.addField(indexedProperty, destinationCode, resolverContext.getFieldQualifier());
			return;
		}

		isValueResolved(indexedProperty);
	}

}
