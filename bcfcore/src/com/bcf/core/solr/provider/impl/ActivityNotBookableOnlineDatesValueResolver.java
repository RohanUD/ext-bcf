/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import java.util.Date;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.ActivitySaleStatusModel;
import com.bcf.core.model.product.ActivityProductModel;


public class ActivityNotBookableOnlineDatesValueResolver extends AbstractValueResolver<ActivityProductModel, Object, Object>
{

	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
	private static final String START_DATE = "startDate";
	private static final String END_DATE = "endDate";


	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final ActivityProductModel activityProductModel, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{
		ActivitySaleStatusModel notBookableOnlineStatus = activityProductModel.getSaleStatuses().stream()
				.filter(status -> SaleStatusType.NOT_BOOKABLE_ONLINE.equals(status.getSaleStatusType()))
				.findFirst().orElse(null);
		if (notBookableOnlineStatus == null)
		{
			return;
		}

		Date date = null;
		if (isIndexPropertyContainsProperty(indexedProperty, START_DATE))
		{
			date = notBookableOnlineStatus.getStartDate();
		}
		else if (isIndexPropertyContainsProperty(indexedProperty, END_DATE))
		{
			date = notBookableOnlineStatus.getEndDate();
		}

		if (date != null)
		{
			document.addField(indexedProperty, date, resolverContext.getFieldQualifier());
			return;
		}

		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	private boolean isIndexPropertyContainsProperty(final IndexedProperty indexedProperty, String property)
	{
		return indexedProperty.getName().toLowerCase().contains(property.toLowerCase());
	}

}
