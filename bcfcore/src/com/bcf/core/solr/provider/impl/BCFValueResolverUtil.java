/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import java.util.Collection;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;


public final class BCFValueResolverUtil
{
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	private BCFValueResolverUtil(){
		//Not called, created to resolve sonar issue
	}

	public static void checkForOptional(final IndexedProperty indexedProperty) throws FieldValueProviderException
	{
		final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
				OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	public static void checkForOptional(final IndexedProperty indexedProperty, final Object valueResolved)
			throws FieldValueProviderException
	{
		if (valueResolved instanceof Collection)
		{
			final Collection collection = (Collection) valueResolved;
			if (CollectionUtils.isNotEmpty(collection))
			{
				return;
			}
		}
		if (Objects.nonNull(valueResolved))
		{
			return;
		}
		final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
				OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}
}
