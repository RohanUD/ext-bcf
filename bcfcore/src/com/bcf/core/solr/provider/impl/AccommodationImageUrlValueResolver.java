/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingGalleryService;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


public class AccommodationImageUrlValueResolver extends BCFAbstractValueResolver
{
	private AccommodationOfferingGalleryService accommodationOfferingGalleryService;

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{
		final List<AccommodationBundleTemplateModel> accommodationTemplates = getAccommodationBundles(dealBundleTemplate);

		if (CollectionUtils.isNotEmpty(accommodationTemplates))
		{
			final List<AccommodationOfferingModel> offering =
					accommodationTemplates.stream().map(AccommodationBundleTemplateModel::getAccommodationOffering)
							.collect(Collectors.toList());
			final List<String> galleryCodes = offering.stream().map(AccommodationOfferingModel::getGalleryCode)
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(galleryCodes))
			{
				final AccommodationOfferingGalleryModel gallery = getAccommodationOfferingGalleryService()
							.getAccommodationOfferingGallery(galleryCodes.get(0), dealBundleTemplate.getCatalogVersion());
					if (gallery != null && gallery.getListImage() != null)
					{
						document.addField(indexedProperty, gallery.getListImage().getURL(), resolverContext.getFieldQualifier());
					}
			}
			return;
		}

		isValueResolved(indexedProperty);
	}

	protected AccommodationOfferingGalleryService getAccommodationOfferingGalleryService()
	{
		return accommodationOfferingGalleryService;
	}

	@Required
	public void setAccommodationOfferingGalleryService(
			final AccommodationOfferingGalleryService accommodationOfferingGalleryService)
	{
		this.accommodationOfferingGalleryService = accommodationOfferingGalleryService;
	}
}

