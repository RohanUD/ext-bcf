/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.util.StreamUtil;


public class PromotionBcfVacationLocationCodeResolver extends AbstractValueResolver<PromotionSourceRuleModel, Object, Object>
{
	private BcfTravelLocationService bcfTravelLocationService;
	private ValueResolverHelper valueResolverHelper;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final PromotionSourceRuleModel promotionSourceRuleModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final List<String> bcfVacationLocationCodes = StreamUtil.safeStream(bcfAccommodationOfferingService
				.getAccommodationOfferingByProducts(
						getValueResolverHelper().filterRoomRateProducts(promotionSourceRuleModel.getConditions())))
				.map(this::getBcfVacationLocationCode)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		for (final String bcfVacationLocationCode : bcfVacationLocationCodes)
		{
			document.addField(indexedProperty, bcfVacationLocationCode, resolverContext.getFieldQualifier());
		}
	}

	private String getBcfVacationLocationCode(final AccommodationOfferingModel accommodationOfferingModel)
	{
		return Optional.ofNullable(
				getBcfTravelLocationService().findBcfVacationLocationByOriginalLocation(accommodationOfferingModel.getLocation()))
				.map(BcfVacationLocationModel::getCode)
				.orElse(null);
	}

	public BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}

	public ValueResolverHelper getValueResolverHelper()
	{
		return valueResolverHelper;
	}

	@Required
	public void setValueResolverHelper(final ValueResolverHelper valueResolverHelper)
	{
		this.valueResolverHelper = valueResolverHelper;
	}

	public BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}
}
