/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import java.util.List;
import java.util.Optional;
import javax.annotation.Resource;
import com.bcf.core.enums.PropertyType;


public class PropertyTypeFacetValueDisplayNameProvider implements FacetValueDisplayNameProvider
{
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;


	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty indexedProperty, final String propertyTypeCode)
	{

		final List<PropertyType> propertyTypeEnum = enumerationService.getEnumerationValues(PropertyType.class);

		final String propertyName;
		final Optional<PropertyType> optionalProperty = propertyTypeEnum.stream()
				.filter(property -> propertyTypeCode.equals(property.getCode())).findFirst();
		if (optionalProperty.isPresent())
		{
			propertyName = enumerationService.getEnumerationName(optionalProperty.get());
			return propertyName;
		}
		return propertyTypeCode;
	}

}
