/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.ActivitySaleStatusModel;
import com.bcf.core.model.product.ActivityProductModel;


public class ActivityStopSaleValueResolver extends AbstractValueResolver<ActivityProductModel, Object, Object>
{
	private static final Logger LOG = Logger.getLogger(ActivityStopSaleValueResolver.class);

	private ModelService modelService;
	private Map<String, String> propertyMap;

	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final ActivityProductModel activityProductModel, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{
		ActivitySaleStatusModel stopSaleStatus = activityProductModel.getSaleStatuses().stream()
				.filter(status -> SaleStatusType.STOPSALE.equals(status.getSaleStatusType()))
				.findFirst().orElse(null);
		if (stopSaleStatus == null)
		{
			LOG.info("No Stop sale dates available to index for Activity product :" + activityProductModel.getPk()
					.getLongValueAsString());
			return;
		}
		final Date date = stopSaleStatus.getProperty(indexedProperty.getName());


		document.addField(indexedProperty, date, resolverContext.getFieldQualifier());

		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public Map<String, String> getPropertyMap()
	{
		return propertyMap;
	}

	@Required
	public void setPropertyMap(final Map<String, String> propertyMap)
	{
		this.propertyMap = propertyMap;
	}
}
