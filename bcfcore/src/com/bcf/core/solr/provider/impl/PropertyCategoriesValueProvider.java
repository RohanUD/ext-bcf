/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.enums.PropertyCategoryType;
import com.bcf.core.util.StreamUtil;


public class PropertyCategoriesValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
   @Resource
   private FieldNameProvider solrFieldNameProvider;

   @Resource
   private BcfAccommodationOfferingService bcfAccommodationOfferingService;

   @Resource
   private ValueResolverHelper valueResolverHelper;

   @Override
   public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
         final Object model)
	{
      if (model instanceof PromotionSourceRuleModel)
      {
         final Set<String> rootRateProducts = valueResolverHelper
               .filterRoomRateProducts(((PromotionSourceRuleModel) model).getConditions());

         return StreamUtil
               .safeStream(bcfAccommodationOfferingService.getAccommodationOfferingByProducts(rootRateProducts))
               .map(AccommodationOfferingModel::getPropertyCategoryTypes)
               .flatMap(Collection::stream)
               .map(propertyCategoryType -> createFieldValue(propertyCategoryType, indexedProperty))
               .flatMap(Collection::stream)
               .collect(Collectors.toList());
      }
      return Collections.emptyList();
   }

   protected List<FieldValue> createFieldValue(final PropertyCategoryType propertyCategoryType,
         final IndexedProperty indexedProperty)
   {
      return StreamUtil.safeStream(solrFieldNameProvider.getFieldNames(indexedProperty, null))
            .map(fieldName -> new FieldValue(fieldName, propertyCategoryType.getCode()))
            .collect(Collectors.toList());
   }
}
