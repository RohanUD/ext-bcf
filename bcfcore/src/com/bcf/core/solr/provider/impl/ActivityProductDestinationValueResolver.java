/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.Collections;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class ActivityProductDestinationValueResolver extends AbstractValueResolver<ActivityProductModel, Object, Object>
{
	private static final Logger LOG = Logger.getLogger(ActivityProductDestinationValueResolver.class);
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final ActivityProductModel activityProductModel, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{

		if (Objects.nonNull(activityProductModel.getDestination()))
		{
			final LocationModel destination = bcfTravelLocationService
					.getLocationWithLocationType(Collections.singletonList(activityProductModel.getDestination()), LocationType.CITY);
			if (Objects.nonNull(destination))
			{
				BcfVacationLocationModel bcfVacationLocation = bcfTravelLocationService
						.findBcfVacationLocationByOriginalLocation(destination);
				if (Objects.isNull(bcfVacationLocation))
				{
					LOG.warn(String.format("No BcfVacationLocation found for location code [%s]", destination.getCode()));
					return;
				}
				document.addField(indexedProperty, bcfVacationLocation.getName(), resolverContext.getFieldQualifier());
			}
			return;
		}

		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}

	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
