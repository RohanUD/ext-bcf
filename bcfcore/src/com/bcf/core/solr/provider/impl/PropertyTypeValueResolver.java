/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.enums.PropertyType;


public class PropertyTypeValueResolver extends BCFAbstractValueResolver
{
	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext)
			throws
			FieldValueProviderException
	{
		final List<AccommodationBundleTemplateModel> accommodationTemplates = getAccommodationBundles(dealBundleTemplate);

		if (CollectionUtils.isNotEmpty(accommodationTemplates))
		{
			final PropertyType propertyType = accommodationTemplates.get(0).getAccommodationOffering().getPropertyType();
			if (Objects.nonNull(propertyType))
			{
				document.addField(indexedProperty, propertyType.getCode(), resolverContext.getFieldQualifier());
				return;
			}
		}

		isValueResolved(indexedProperty);
	}

}
