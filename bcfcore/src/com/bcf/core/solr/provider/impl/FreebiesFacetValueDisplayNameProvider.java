/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;


public class FreebiesFacetValueDisplayNameProvider implements FacetValueDisplayNameProvider
{
	private ProductService productService;

	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty indexedProperty, final String freebiesCode)
	{
		final ProductModel productModel = getProductService().getProductForCode(freebiesCode);
		return productModel != null ? productModel.getName() : freebiesCode;
	}


	public ProductService getProductService()
	{
		return productService;
	}


	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}
}
