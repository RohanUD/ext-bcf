/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import org.springframework.beans.factory.annotation.Required;


public class RoomRateProductCodesResolver extends AbstractValueResolver<PromotionSourceRuleModel, Object, Object>
{
	private ValueResolverHelper valueResolverHelper;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final PromotionSourceRuleModel promotionSourceRuleModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		for (final String roomRateProduct : getValueResolverHelper()
				.filterRoomRateProducts(promotionSourceRuleModel.getConditions()))
		{
			document.addField(indexedProperty, roomRateProduct, resolverContext.getFieldQualifier());
		}
	}

	public ValueResolverHelper getValueResolverHelper()
	{
		return valueResolverHelper;
	}

	@Required
	public void setValueResolverHelper(final ValueResolverHelper valueResolverHelper)
	{
		this.valueResolverHelper = valueResolverHelper;
	}
}
