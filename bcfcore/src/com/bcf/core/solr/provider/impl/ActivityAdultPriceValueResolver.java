/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.Date;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.util.BcfPriceInfo;


public class ActivityAdultPriceValueResolver extends AbstractValueResolver<ActivityProductModel, Object, Object>
{
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
	private static final String ADULT_PASSENGER_TYPE_CODE = "adult";

	private PassengerTypeService passengerTypeService;
	private BCFTravelCommercePriceService bcfTravelCommercePriceService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext indexerBatchContext,
			final IndexedProperty indexedProperty, final ActivityProductModel activityProduct,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final Double activityAdultPriceValue = getActivityAdultPriceValue(activityProduct);
		if (Objects.nonNull(activityAdultPriceValue))
		{
			document.addField(indexedProperty, activityAdultPriceValue, resolverContext.getFieldQualifier());
			return;
		}

		final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
				OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	protected Double getActivityAdultPriceValue(final ActivityProductModel activityProduct)
	{
		final PassengerTypeModel passengerType = getPassengerTypeService().getPassengerType(ADULT_PASSENGER_TYPE_CODE);
		final Date commencementDate = new Date(); // considering indexed day
		final BcfPriceInfo bcfPriceInfo = getBcfTravelCommercePriceService()
				.getBcfPriceInfo(activityProduct, 1l, passengerType, commencementDate);
		if (Objects.isNull(bcfPriceInfo))
		{
			return null;
		}

		return bcfPriceInfo.getNetPriceVal();
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected BCFTravelCommercePriceService getBcfTravelCommercePriceService()
	{
		return bcfTravelCommercePriceService;
	}

	@Required
	public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
	{
		this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
	}
}
