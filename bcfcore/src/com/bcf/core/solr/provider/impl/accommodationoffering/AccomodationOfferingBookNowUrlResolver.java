/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl.accommodationoffering;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import org.apache.commons.lang3.StringUtils;


public class AccomodationOfferingBookNowUrlResolver extends AbstractValueResolver<AccommodationOfferingModel, Object, Object>
{
	private static final String PACKAGE_HOTEL_DETAILS_PAGE_PATH = "/vacations/hotels/";
	private static final String HYPHEN = "-";
	private static final String SLASH = "/";

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final AccommodationOfferingModel accommodationOfferingModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final StringBuilder bookNowUrlBuilder = new StringBuilder();
		bookNowUrlBuilder.append(PACKAGE_HOTEL_DETAILS_PAGE_PATH);
		bookNowUrlBuilder.append(accommodationOfferingModel.getLocation().getName());
		bookNowUrlBuilder.append(SLASH);
		bookNowUrlBuilder.append(accommodationOfferingModel.getName().replace(StringUtils.SPACE, HYPHEN));
		bookNowUrlBuilder.append(SLASH);
		bookNowUrlBuilder.append(accommodationOfferingModel.getCode());
		document.addField(indexedProperty, bookNowUrlBuilder.toString(),
				resolverContext.getFieldQualifier());
	}
}
