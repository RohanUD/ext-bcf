/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import javax.annotation.Resource;
import com.bcf.core.accommodation.service.BcfTravelLocationService;


public class HotelLocationNameValueDisplayNameProvider implements FacetValueDisplayNameProvider
{
   @Resource
   private BcfTravelLocationService travelLocationService;

   @Override
   public String getDisplayName(final SearchQuery query, final IndexedProperty indexedProperty, final String locationCode)
   {
      if (locationCode != null)
      {
         final LocationModel location = travelLocationService.getLocation(locationCode);
         return location.getName();
      }

      return locationCode;
   }

}
