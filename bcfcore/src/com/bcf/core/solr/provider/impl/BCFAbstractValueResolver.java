/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.TransportBundleTemplateModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public abstract class BCFAbstractValueResolver extends AbstractValueResolver<DealBundleTemplateModel, Object, Object>
{
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	protected List<TransportBundleTemplateModel> getTransportBundles(final DealBundleTemplateModel dealBundleTemplate)
	{
		final List<BundleTemplateModel> bundleTemplate = dealBundleTemplate.getChildTemplates();
		final List<TransportBundleTemplateModel> transportTemplates = new ArrayList<>();
		if (Objects.nonNull(dealBundleTemplate.getIsMultiCityDeal()) && dealBundleTemplate.getIsMultiCityDeal())
		{
			final List<DealBundleTemplateModel> childDealBundles = new ArrayList<>();
			bundleTemplate.forEach(dealBundle -> childDealBundles.add((DealBundleTemplateModel) dealBundle));
			childDealBundles.forEach(childDealBundle -> {
				final TransportBundleTemplateModel transportBundle = (TransportBundleTemplateModel) childDealBundle
						.getChildTemplates().stream()
						.filter(template -> TransportBundleTemplateModel._TYPECODE.equals(template.getItemtype())).findFirst()
						.get();
				transportTemplates.add(transportBundle);
			});
		}
		else
		{
			final TransportBundleTemplateModel transportBundle = (TransportBundleTemplateModel) bundleTemplate.stream()
					.filter(bundle -> TransportBundleTemplateModel._TYPECODE.equals(bundle.getItemtype()))
					.findFirst().get();
			transportTemplates.add(transportBundle);
		}
		return transportTemplates;
	}

	protected List<AccommodationBundleTemplateModel> getAccommodationBundles(final DealBundleTemplateModel dealBundleTemplate)
	{
		final List<BundleTemplateModel> bundleTemplate = dealBundleTemplate.getChildTemplates();
		final List<AccommodationBundleTemplateModel> accommodationTemplates = new ArrayList<>();
		if (Objects.nonNull(dealBundleTemplate.getIsMultiCityDeal()) && dealBundleTemplate.getIsMultiCityDeal())
		{
			final List<DealBundleTemplateModel> childDealBundles = new ArrayList<>();
			bundleTemplate.forEach(dealBundle -> childDealBundles.add((DealBundleTemplateModel) dealBundle));
			childDealBundles.forEach(childDealBundle -> {
				final AccommodationBundleTemplateModel accommodationBundle = (AccommodationBundleTemplateModel) childDealBundle
						.getChildTemplates().stream()
						.filter(template -> AccommodationBundleTemplateModel._TYPECODE.equals(template.getItemtype())).findFirst()
						.get();
				accommodationTemplates.add(accommodationBundle);
			});
		}
		else
		{
			final AccommodationBundleTemplateModel accommodationBundle = (AccommodationBundleTemplateModel) bundleTemplate.stream()
					.filter(bundle -> AccommodationBundleTemplateModel._TYPECODE.equals(bundle.getItemtype()))
					.findFirst().get();
			accommodationTemplates.add(accommodationBundle);
		}
		return accommodationTemplates;
	}

	protected void isValueResolved(final IndexedProperty indexedProperty) throws
			FieldValueProviderException
	{
		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)

		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}
}
