/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;


public class HotelPOSValueResolver extends BCFAbstractValueResolver
{
	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext)
			throws FieldValueProviderException
	{
		final List<AccommodationBundleTemplateModel> accommodationTemplates = getAccommodationBundles(dealBundleTemplate);

		if (CollectionUtils.isNotEmpty(accommodationTemplates))
		{
			for (final AccommodationBundleTemplateModel accommodationTemplateModel : accommodationTemplates)
			{
				final PointOfServiceModel pointOfService = accommodationTemplateModel.getAccommodationOffering().getLocation()
						.getPointOfService().stream().findFirst().get();
					if (pointOfService != null && pointOfService.getLatitude() != null && pointOfService.getLongitude() != null)
					{
						final String coordinates =
								pointOfService.getLatitude() + "_" + pointOfService.getLongitude();
						document.addField(indexedProperty, coordinates, resolverContext.getFieldQualifier());
					}
			}
			return;
		}
		isValueResolved(indexedProperty);
	}
}
