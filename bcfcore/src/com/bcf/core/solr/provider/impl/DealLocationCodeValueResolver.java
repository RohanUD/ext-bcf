/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;


public class DealLocationCodeValueResolver extends BCFAbstractValueResolver
{
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext)
			throws FieldValueProviderException
	{
		final List<AccommodationBundleTemplateModel> accommodationTemplates = getAccommodationBundles(dealBundleTemplate);
		if (CollectionUtils.isNotEmpty(accommodationTemplates))
		{
			final String cityLocationCode = getCityLocationCode(accommodationTemplates.get(0).getAccommodationOffering());
			if (StringUtils.isNotEmpty(cityLocationCode))
			{
				document.addField(indexedProperty, cityLocationCode, resolverContext.getFieldQualifier());
				return;
			}
		}
		isValueResolved(indexedProperty);

	}

	/**
	 * Gets the city location code.
	 *
	 * @param accommodationOffering the accommodation offering
	 * @return the city location code
	 */
	protected String getCityLocationCode(final AccommodationOfferingModel accommodationOffering)
	{
		final LocationModel location = getBcfTravelLocationService()
				.getLocationWithLocationType(Collections.singletonList(accommodationOffering.getLocation()), LocationType.CITY);
		if (Objects.isNull(location))
		{
			return StringUtils.EMPTY;
		}

		return location.getCode();
	}

	/**
	 * @return the bcfTravelLocationService
	 */
	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	/**
	 * @param bcfTravelLocationService the bcfTravelLocationService to set
	 */
	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
