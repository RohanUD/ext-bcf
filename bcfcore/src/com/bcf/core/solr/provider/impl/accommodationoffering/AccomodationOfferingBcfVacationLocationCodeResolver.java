/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl.accommodationoffering;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class AccomodationOfferingBcfVacationLocationCodeResolver
		extends AbstractValueResolver<AccommodationOfferingModel, Object, Object>
{
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final AccommodationOfferingModel accommodationOfferingModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final String bcfVacationLocationCode = Optional.ofNullable(
				getBcfTravelLocationService().findBcfVacationLocationByOriginalLocation(accommodationOfferingModel.getLocation()))
				.map(BcfVacationLocationModel::getCode)
				.orElse(null);
		document.addField(indexedProperty, bcfVacationLocationCode, resolverContext.getFieldQualifier());
	}

	public BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
