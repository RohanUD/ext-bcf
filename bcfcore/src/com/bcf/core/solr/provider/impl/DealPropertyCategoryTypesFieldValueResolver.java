/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


public class DealPropertyCategoryTypesFieldValueResolver extends BCFAbstractValueResolver
{
	private EnumerationService enumerationService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext indexerBatchContext,
			final IndexedProperty indexedProperty, final DealBundleTemplateModel dealBundleTemplate,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException

	{
		final List<AccommodationBundleTemplateModel> accommodationTemplates = getAccommodationBundles(dealBundleTemplate);
		if (CollectionUtils.isNotEmpty(accommodationTemplates))
		{
			final List<String> propertyCategoryTypeNames = accommodationTemplates.stream()
					.map(AccommodationBundleTemplateModel::getAccommodationOffering)
					.flatMap(accommodationOffering -> accommodationOffering.getPropertyCategoryTypes().stream())
					.map(propertyCategoryType -> getEnumerationService().getEnumerationName(propertyCategoryType))
					.collect(Collectors.toList());
			document.addField(indexedProperty, propertyCategoryTypeNames, resolverContext.getFieldQualifier());
			return;
		}

		isValueResolved(indexedProperty);
	}

	/**
	 * Gets enumeration service.
	 *
	 * @return the enumerationService
	 */
	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	/**
	 * Sets enumeration service.
	 *
	 * @param enumerationService
	 *           the enumerationService to set
	 */
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
