package com.bcf.core.solr.provider.impl.accommodationoffering;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import com.bcf.core.solr.provider.impl.BCFValueResolverUtil;


public class AccomodationOfferingPropertyCategoriesResolver
		extends AbstractValueResolver<AccommodationOfferingModel, Object, Object>
{
	@Resource
	private EnumerationService enumerationService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final AccommodationOfferingModel accommodationOfferingModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		if (Objects.nonNull(accommodationOfferingModel.getPropertyCategoryTypes()))
		{
			final List<String> propertyCategoryTypeNames = accommodationOfferingModel.getPropertyCategoryTypes().stream()
					.map(propertyCategoryType -> enumerationService.getEnumerationName(propertyCategoryType))
					.collect(Collectors.toList());
			document.addField(indexedProperty, propertyCategoryTypeNames, resolverContext.getFieldQualifier());
			return;
		}
		BCFValueResolverUtil.checkForOptional(indexedProperty);
	}
}
