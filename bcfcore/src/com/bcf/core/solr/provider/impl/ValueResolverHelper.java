/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.core.model.product.ActivityProductModel;
import com.jayway.jsonpath.JsonPath;


public class ValueResolverHelper
{

   private static final Logger LOG = Logger.getLogger(ValueResolverHelper.class);

   private static final String QUALIFYING_PRODUCTS_PATH = "$..[?(@.definitionId =='y_qualifying_products')].parameters.products.value";

   @Resource
   private ProductService productService;

   public Set<String> filterRoomRateProducts(final String conditions)
   {
      final Predicate predicate = PredicateUtils.instanceofPredicate(RoomRateProductModel.class);
      return filter(getQualifyingProductCodes(conditions), predicate);
   }

   public Set<String> filterActivityProducts(final String conditions)
   {
      final Predicate predicate = PredicateUtils.instanceofPredicate(ActivityProductModel.class);
      return filter(getQualifyingProductCodes(conditions), predicate);
   }

   public Set<String> filter(final Set<String> productCodes, final Predicate predicate)
   {

      Set<String> finalList = new HashSet<>();
      for (String productCode : productCodes)
      {
         try
         {
            final ProductModel product = productService.getProductForCode(productCode);
            if (predicate.evaluate(product))
            {
               finalList.add(productCode);
            }
         }
         catch (Exception ex)
         {
            LOG.error("Error during filter, continue as normal:" + ex);
         }
      }

      return finalList;
   }

   public Set<String> getQualifyingProductCodes(final String promotionSourceRuleConditions)
   {
      final List<String> finalList = new ArrayList();

      if (StringUtils.isNotBlank(promotionSourceRuleConditions))
      {
         final List products = JsonPath
               .read(promotionSourceRuleConditions, QUALIFYING_PRODUCTS_PATH);

         populateProducts(products, finalList);
      }

      return finalList.stream().collect(Collectors.toSet());
   }

   private static void populateProducts(final List products, final List<String> finalList)
   {
      if (products != null && !products.isEmpty())
      {
         for (final Object product : products)
         {
            if (product instanceof String)
            {
               finalList.add((String) product);
            }
            else if (product instanceof List)
            {
               populateProducts((List) product, finalList);
            }
         }
      }
   }





}
