package com.bcf.core.solr.provider.impl.accommodationoffering;

import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.solr.provider.impl.RatePlanConfigsValueResolver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;


public class BcfRatePlanConfigsValueResolver extends RatePlanConfigsValueResolver
{

	@Override
	protected List<String> getRatePlanConfigChain(final MarketingRatePlanInfoModel marketingRatePlanInfoModel) {
		if (CollectionUtils.isEmpty(marketingRatePlanInfoModel.getRatePlanConfig())) {
			return Collections.emptyList();
		} else {
			final List<String> ratePlanConfigChain = new ArrayList();
			marketingRatePlanInfoModel.getRatePlanConfig().stream().distinct().forEach((ratePlanConfig) -> {
				final String ratePlanCofigCode = ratePlanConfig.getRatePlan().getCode() + "|" + ratePlanConfig.getAccommodation().getCode() + "|" + ratePlanConfig.getQuantity();
				ratePlanConfigChain.add(ratePlanCofigCode);
			});
			return ratePlanConfigChain;
		}
	}
}
