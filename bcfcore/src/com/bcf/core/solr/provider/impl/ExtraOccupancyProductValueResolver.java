/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.solr.provider.impl.AbstractDateBasedValueResolver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class ExtraOccupancyProductValueResolver extends AbstractDateBasedValueResolver<MarketingRatePlanInfoModel, Object, Object>
{
	private String passengerType;
	private PriceRowService priceRowService;

	public ExtraOccupancyProductValueResolver() {
		//constructor
	}

	protected void addFieldValues(InputDocument document, IndexerBatchContext batchContext, IndexedProperty indexedProperty, MarketingRatePlanInfoModel marketingRatePlanInfo, AbstractDateBasedValueResolver.ValueResolverContext<Object, Object> resolverContext, Date documentDate) throws FieldValueProviderException {
		List<AccommodationModel> accommodations = this.getAccommodations(marketingRatePlanInfo.getRatePlanConfig());
		if (CollectionUtils.isNotEmpty(accommodations)) {

			List<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodations.stream().filter(accommodation->CollectionUtils.isNotEmpty(accommodation.getExtraGuestProducts())).flatMap(accommodation->accommodation.getExtraGuestProducts().stream()).collect(
					Collectors
					.toList());

			extraGuestProducts= Optional.ofNullable(extraGuestProducts)
					.orElseGet(Collections::emptyList).stream().distinct().filter(extraGuestProduct->priceRowService.getPriceRow(extraGuestProduct, passengerType,documentDate)!=null).collect(
					Collectors.toList());

        if(CollectionUtils.isNotEmpty(extraGuestProducts)){

			  final Map<ExtraGuestOccupancyProductModel, Double> extraGuestPriceValueMap =  extraGuestProducts.stream().collect(Collectors
					  .toMap(extraGuestProduct -> extraGuestProduct,
							  extraGuestProduct -> priceRowService.getPriceRow(extraGuestProduct, passengerType,documentDate)
									  .getPrice()));
			  final Map.Entry<ExtraGuestOccupancyProductModel, Double> minExtraGuestPriceValueMapEntry = extraGuestPriceValueMap
					  .entrySet().stream().min(Map.Entry.comparingByValue()).get();

			  final ExtraGuestOccupancyProductModel extraGuestOccupancyProductModel = minExtraGuestPriceValueMapEntry.getKey();

			  document.addField(indexedProperty, extraGuestOccupancyProductModel.getCode(), resolverContext.getFieldQualifier());
		  }

		} else {
			boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, "optional", true);
			if (!isOptional) {
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	protected List<AccommodationModel> getAccommodations(Collection<RatePlanConfigModel> ratePlanConfigs) {
		List<AccommodationModel> accommodationModels = new ArrayList();
		if (CollectionUtils.isNotEmpty(ratePlanConfigs)) {
			ratePlanConfigs.forEach(ratePlanConfigModel -> {
				for(int i = 0; i < ratePlanConfigModel.getQuantity(); ++i) {
					accommodationModels.add(ratePlanConfigModel.getAccommodation());
				}

			});
		}

		return accommodationModels;
	}

	public String getPassengerType()
	{
		return passengerType;
	}

	public void setPassengerType(final String passengerType)
	{
		this.passengerType = passengerType;
	}

	public PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}
}
