/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.DealType;


public class DealTypeFacetValueDisplayNameProvider implements FacetValueDisplayNameProvider
{
	private EnumerationService enumerationService;

	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty indexedProperty, final String dealTypeCode)
	{
		final List<DealType> dealTypeEnum = getEnumerationService().getEnumerationValues(DealType.class);
		final String dealTypeName;
		final Optional<DealType> optionalDeal = dealTypeEnum.stream().filter(deal -> dealTypeCode.equals(deal.getCode()))
				.findFirst();
		if (optionalDeal.isPresent())
		{
			dealTypeName = getEnumerationService().getEnumerationName(optionalDeal.get());
			return dealTypeName;
		}
		return dealTypeCode;
	}

	/**
	 * @return the enumerationService
	 */
	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	/**
	 * @param enumerationService
	 *           the enumerationService to set
	 */
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
