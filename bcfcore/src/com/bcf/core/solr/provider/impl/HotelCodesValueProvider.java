/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;


public class HotelCodesValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
   @Resource
   private FieldNameProvider solrFieldNameProvider;

   @Resource
   private BcfAccommodationOfferingService bcfAccommodationOfferingService;

   private static final String OPTIONAL_PARAM = "optional";

   private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

   @Resource
   private ValueResolverHelper valueResolverHelper;


   @Override
   public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
         final Object model) throws FieldValueProviderException
   {
      final Collection<FieldValue> fieldValues = new ArrayList<>();

      if (model instanceof PromotionSourceRuleModel)
      {

         final PromotionSourceRuleModel promotionSourceRule = (PromotionSourceRuleModel) model;

         final Set<String> roomRateProducts = valueResolverHelper.filterRoomRateProducts(promotionSourceRule.getConditions());

         final Set<AccommodationOfferingModel> hotels = bcfAccommodationOfferingService
               .getAccommodationOfferingByProducts(roomRateProducts);

         if (!hotels.isEmpty())
         {
            for (final AccommodationOfferingModel hotel : hotels)
            {
               fieldValues.addAll(createFieldValue(hotel.getCode(), indexedProperty));
            }
         }
         else
         {
            final boolean isOptional = ValueProviderParameterUtils
                  .getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);

            if (!isOptional)
            {
               throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
            }
         }
      }

      return fieldValues;
   }

   protected List<FieldValue> createFieldValue(final String hotel, final IndexedProperty indexedProperty)
   {
      final List<FieldValue> fieldValues = new ArrayList<>();
      final Collection<String> fieldNames = solrFieldNameProvider.getFieldNames(indexedProperty, null);

      for (final String fieldName : fieldNames)
      {
         fieldValues.add(new FieldValue(fieldName, hotel));
      }

      return fieldValues;
   }

}
