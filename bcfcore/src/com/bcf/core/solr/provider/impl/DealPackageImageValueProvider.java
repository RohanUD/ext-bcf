/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


public class DealPackageImageValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	@Resource
	private FieldNameProvider solrFieldNameProvider;

	@Resource
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	private static final String OPTIONAL_PARAM = "optional";

	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (!(model instanceof PromotionSourceRuleModel))
		{
			return Collections.emptyList();
		}
		final String packageImageUrl = bcfResponsiveMediaStrategy.getResponsiveJson(((PromotionSourceRuleModel) model).getImage());
		if (StringUtils.isNotEmpty(packageImageUrl))
		{
			return createFieldValue(packageImageUrl, indexedProperty);
		}
		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
		return Collections.emptyList();
	}

	protected List<FieldValue> createFieldValue(final String packageImageUrl, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();
		final Collection<String> fieldNames = solrFieldNameProvider.getFieldNames(indexedProperty, null);

		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, packageImageUrl));
		}

		return fieldValues;
	}

}
