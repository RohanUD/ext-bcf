/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingGalleryService;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

public class AccommodationOfferingImageUrlValueResolver extends AbstractValueResolver<MarketingRatePlanInfoModel, Object, Object> {

    private static final String OPTIONAL_PARAM = "optional";
    private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
    private AccommodationOfferingGalleryService accommodationOfferingGalleryService;

    @Override
    protected void addFieldValues(InputDocument document, IndexerBatchContext indexerBatchContext, IndexedProperty indexedProperty, MarketingRatePlanInfoModel marketingRatePlanInfoModel, ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
    {
        String galleryCode = marketingRatePlanInfoModel.getAccommodationOffering().getGalleryCode();
        if (StringUtils.isNotEmpty(galleryCode))
        {
            AccommodationOfferingGalleryModel gallery = getAccommodationOfferingGalleryService().getAccommodationOfferingGallery(galleryCode, marketingRatePlanInfoModel.getCatalogVersion());
            if (Objects.nonNull(gallery))
            {
                document.addField(indexedProperty, gallery.getCode(), resolverContext.getFieldQualifier());
                return;
            }
        }

        boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
        if (!isOptional)
        {
            throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
        }
    }

    protected AccommodationOfferingGalleryService getAccommodationOfferingGalleryService() {
        return this.accommodationOfferingGalleryService;
    }

    @Required
    public void setAccommodationOfferingGalleryService(AccommodationOfferingGalleryService accommodationOfferingGalleryService) {
        this.accommodationOfferingGalleryService = accommodationOfferingGalleryService;
    }


}
