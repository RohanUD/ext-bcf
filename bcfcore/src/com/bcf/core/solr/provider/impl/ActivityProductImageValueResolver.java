/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;


public class ActivityProductImageValueResolver extends AbstractValueResolver<ActivityProductModel, Object, Object>
{
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext indexerBatchContext,
			final IndexedProperty indexedProperty, final ActivityProductModel activityProductModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final List<String> responsiveUrls = StreamUtil.safeStream(activityProductModel.getNormal())
				.map(bcfResponsiveMediaStrategy::getResponsiveJson).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(responsiveUrls))
		{
			document.addField(indexedProperty, responsiveUrls, resolverContext.getFieldQualifier());
			return;
		}

		final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
				OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
