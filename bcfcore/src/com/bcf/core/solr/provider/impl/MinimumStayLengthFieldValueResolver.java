/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.solr.provider.impl.AbstractDateBasedValueResolver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.model.accommodation.MinimumNightsStayRulesModel;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.BcfRatePlanUtil;


public class MinimumStayLengthFieldValueResolver
		extends AbstractDateBasedValueResolver<MarketingRatePlanInfoModel, Object, Object>
{
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final MarketingRatePlanInfoModel marketingRatePlanInfoModel,
			final ValueResolverContext<Object, Object> resolverContext, final Date documentDate)
			throws FieldValueProviderException
	{
		 Integer minimumNightsStay =null;
		List<AccommodationModel> accommodations = this.getAccommodations(marketingRatePlanInfoModel.getRatePlanConfig());
		if (CollectionUtils.isNotEmpty(accommodations))
		{

			for (AccommodationModel accommodationModel : accommodations)
			{

				for (MinimumNightsStayRulesModel minimumNightsStayRulesModel : accommodationModel.getMinimumNightsStayRules())
				{

					if (BCFDateUtils.isBetweenDatesInclusive(documentDate, minimumNightsStayRulesModel.getStartDate(),
							minimumNightsStayRulesModel.getEndDate()) && BcfRatePlanUtil.isValidForDayOfWeek(documentDate,
							minimumNightsStayRulesModel.getDaysOfWeek()))
					{

						if(minimumNightsStay==null || minimumNightsStayRulesModel.getMinimumNightsStay()<minimumNightsStay){
							minimumNightsStay=minimumNightsStayRulesModel.getMinimumNightsStay();
						}





					}

				}
			}


		}
		if(minimumNightsStay==null){
			minimumNightsStay=1;
		}
		document.addField(indexedProperty, minimumNightsStay.toString(), resolverContext.getFieldQualifier());


	}

	protected List<AccommodationModel> getAccommodations(Collection<RatePlanConfigModel> ratePlanConfigs) {
		List<AccommodationModel> accommodationModels = new ArrayList();
		if (CollectionUtils.isNotEmpty(ratePlanConfigs)) {
			ratePlanConfigs.forEach((ratePlanConfigModel) -> {
				for(int i = 0; i < ratePlanConfigModel.getQuantity(); ++i) {
					accommodationModels.add(ratePlanConfigModel.getAccommodation());
				}

			});
		}

		return accommodationModels;
	}
}
