/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.BCFDateUtils;


public class DealSoldFromToValueResolver extends BCFAbstractValueResolver
{
	private Map<String, String> propertyMap;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final DealBundleTemplateModel dealBundleTemplate, final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{

		final Date date = dealBundleTemplate.getProperty(getPropertyMap().get(indexedProperty.getName()));

		final Date soldFromOrToDate = Objects.nonNull(date) ? date :
				BCFDateUtils.getDummyDate();
		document.addField(indexedProperty, soldFromOrToDate, resolverContext.getFieldQualifier());
		isValueResolved(indexedProperty);
	}

	public Map<String, String> getPropertyMap()
	{
		return propertyMap;
	}

	@Required
	public void setPropertyMap(final Map<String, String> propertyMap)
	{
		this.propertyMap = propertyMap;
	}
}
