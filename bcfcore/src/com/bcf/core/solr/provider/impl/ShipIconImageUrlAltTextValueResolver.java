/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Objects;


public class ShipIconImageUrlAltTextValueResolver extends AbstractValueResolver<TransportOfferingModel, Object, Object>
{

	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	@Override
	protected void addFieldValues(
			final InputDocument document, final IndexerBatchContext indexerBatchContext, final IndexedProperty indexedProperty,
			final TransportOfferingModel transportOffering, final ValueResolverContext<Object, Object> resolverContext)
			throws FieldValueProviderException
	{
		if (Objects.nonNull(transportOffering.getTransportVehicle()) && Objects
				.nonNull(transportOffering.getTransportVehicle().getTransportVehicleInfo()) && transportOffering.getTransportVehicle()
				.getTransportVehicleInfo() instanceof ShipInfoModel)
		{
			final MediaModel shipIcon = ((ShipInfoModel) transportOffering.getTransportVehicle().getTransportVehicleInfo())
					.getShipIcon();
			if (Objects.nonNull(shipIcon))
			{
				document.addField(indexedProperty, shipIcon.getAltText(), resolverContext.getFieldQualifier());
				return;
			}
		}

		final boolean isOptional = ValueProviderParameterUtils
				.getBoolean(indexedProperty, OPTIONAL_PARAM, OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}
}
