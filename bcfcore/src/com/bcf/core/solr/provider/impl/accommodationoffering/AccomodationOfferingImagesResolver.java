/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl.accommodationoffering;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingGalleryService;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.solr.provider.impl.BCFValueResolverUtil;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;


public class AccomodationOfferingImagesResolver extends AbstractValueResolver<AccommodationOfferingModel, Object, Object>
{
	private AccommodationOfferingGalleryService accommodationOfferingGalleryService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CatalogVersionService catalogVersionService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final AccommodationOfferingModel accommodationOfferingModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final List<String> responsiveImages = Optional.ofNullable(accommodationOfferingModel.getGalleryCode())
				.map(this::getResponsiveImages)
				.orElse(Collections.emptyList());
		for (final String image : responsiveImages)
		{
			document.addField(indexedProperty, image, resolverContext.getFieldQualifier());
		}
		BCFValueResolverUtil.checkForOptional(indexedProperty, responsiveImages);
	}

	private List<String> getResponsiveImages(final String galleryCode)
	{
		final AccommodationOfferingGalleryModel gallery = getAccommodationOfferingGalleryService()
				.getAccommodationOfferingGallery(galleryCode,
						getCatalogVersionService().getCatalogVersion("bcfProductCatalog", "Online"));
		return StreamUtil.safeStream(gallery.getGallery())
				.map(bcfResponsiveMediaStrategy::getResponsiveJson)
				.collect(Collectors.toList());
	}

	public AccommodationOfferingGalleryService getAccommodationOfferingGalleryService()
	{
		return accommodationOfferingGalleryService;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	@Required
	public void setAccommodationOfferingGalleryService(
			final AccommodationOfferingGalleryService accommodationOfferingGalleryService)
	{
		this.accommodationOfferingGalleryService = accommodationOfferingGalleryService;
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
