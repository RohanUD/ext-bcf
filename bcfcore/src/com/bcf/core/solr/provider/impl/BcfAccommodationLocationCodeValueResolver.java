/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solr.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collections;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class BcfAccommodationLocationCodeValueResolver extends AbstractValueResolver<MarketingRatePlanInfoModel, Object, Object>
{
	private static final Logger LOG = Logger.getLogger(BcfAccommodationLocationCodeValueResolver.class);
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext indexerBatchContext,
			final IndexedProperty indexedProperty, final MarketingRatePlanInfoModel marketingRatePlanInfoModel,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final String cityLocationCode = getCityLocationCode(marketingRatePlanInfoModel.getAccommodationOffering());

		if (StringUtils.isNotEmpty(cityLocationCode))
		{
			document.addField(indexedProperty, cityLocationCode, resolverContext.getFieldQualifier());
			return;
		}

		final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
				OPTIONAL_PARAM_DEFAULT_VALUE);
		if (!isOptional)
		{
			throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
		}
	}

	/**
	 * Gets the city location code of the BCFVacationLocation from the Location.
	 *
	 * @param accommodationOffering the accommodation offering
	 * @return the city location code
	 */
	protected String getCityLocationCode(final AccommodationOfferingModel accommodationOffering)
	{
		final LocationModel location = getBcfTravelLocationService()
				.getLocationWithLocationType(Collections.singletonList(accommodationOffering.getLocation()), LocationType.CITY);
		if (Objects.isNull(location))
		{
			return StringUtils.EMPTY;
		}
		BcfVacationLocationModel bcfVacationLocation = getBcfTravelLocationService()
				.findBcfVacationLocationByOriginalLocation(location);
		if (Objects.isNull(bcfVacationLocation))
		{
			LOG.warn(String.format("BCFVacationLocation is null for location code [%s]", location.getCode()));
			return StringUtils.EMPTY;
		}
		return bcfVacationLocation.getCode();
	}

	/**
	 * @return the bcfTravelLocationService
	 */
	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	/**
	 * @param bcfTravelLocationService the bcfTravelLocationService to set
	 */
	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
