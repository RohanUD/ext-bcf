package com.bcf.core.pagination.impl;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.core.pagination.GlobalPaginationResultsBuilder;


public class BCFGlobalPaginationResultsBuilder implements GlobalPaginationResultsBuilder
{
	private PaginatedFlexibleSearchService paginatedFlexibleSearchService;

	@Override
	public <T> SearchPageData<T> createPaginatedResults(final String query, final Map<String, Object> queryParams,
			final int pageSize, final int currentPage)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		queryParams.entrySet().forEach(entry -> flexibleSearchQuery.addQueryParameter(entry.getKey(), entry.getValue()));
		final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = new PaginatedFlexibleSearchParameter();
		paginatedFlexibleSearchParameter.setFlexibleSearchQuery(flexibleSearchQuery);
		paginatedFlexibleSearchParameter
				.setSearchPageData(PaginatedSearchUtils.createSearchPageDataWithPagination(pageSize, currentPage, true));
		return getPaginatedFlexibleSearchService().search(paginatedFlexibleSearchParameter);
	}

	public PaginatedFlexibleSearchService getPaginatedFlexibleSearchService()
	{
		return paginatedFlexibleSearchService;
	}

	@Resource
	public void setPaginatedFlexibleSearchService(final PaginatedFlexibleSearchService paginatedFlexibleSearchService)
	{
		this.paginatedFlexibleSearchService = paginatedFlexibleSearchService;
	}
}
