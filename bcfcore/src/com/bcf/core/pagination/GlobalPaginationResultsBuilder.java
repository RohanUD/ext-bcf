package com.bcf.core.pagination;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import java.util.Map;


public interface GlobalPaginationResultsBuilder
{
	<T> SearchPageData<T> createPaginatedResults(String query, Map<String, Object> queryParams, int pageSize, int currentPage);
}
