/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.core.jalo.cronjob;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.travelservices.cronjob.UpdateStockLevelsToTransportOfferingJob;
import de.hybris.platform.travelservices.model.cronjob.UpdateStockLevelsToTransportOfferingCronJobModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;


public class BcfUpdateStockLevelsToTransportOfferingJob extends UpdateStockLevelsToTransportOfferingJob
{

	private static final Logger LOG = Logger.getLogger(BcfUpdateStockLevelsToTransportOfferingJob.class);

	private static final String FAREPRODUCT = "FAREPRODUCT";
	static final String PRODUCTCATALOG = "bcfProductCatalog";
	static final String CATALOGVERSION = CatalogManager.OFFLINE_VERSION;

	private CategoryService categoryService;
	private CatalogVersionService catalogVersionService;

	@Override
	public PerformResult perform(
			final UpdateStockLevelsToTransportOfferingCronJobModel updateStockLevelsToTransportOfferingCronJobModel)
	{
		LOG.info("Performing Cronjob -> BcfUpdateStockLevelsToTransportOfferingJob....");

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);
		final CategoryModel category = getCategoryService().getCategoryForCode(catalogVersion, FAREPRODUCT);
		final List<String> fareProducts = category.getProducts().stream()
				.filter(product -> product instanceof FareProductModel).map(ProductModel::getCode)
				.collect(Collectors.toList());

		for (final TransportOfferingModel transportOffering : getTransportOfferingService().getTransportOfferings())
		{
			if (transportOffering.getActive())
			{
				if (transportOffering.getDefault() != null && transportOffering.getDefault())
				{
					setupStockLevelForFareProducts(transportOffering,
							fareProducts.stream().filter(fareProduct -> fareProduct.equals(BcfCoreConstants.DEFAULT_FARE_PRODUCT_CODE))
									.collect(
											Collectors.toList()));
					setupStockLevel(transportOffering, updateStockLevelsToTransportOfferingCronJobModel.getAncillaryStockLevel());
				}
				else
				{
					setupStockLevelForFareProducts(transportOffering,
							fareProducts.stream().filter(fareProduct -> !fareProduct.equals(BcfCoreConstants.DEFAULT_FARE_PRODUCT_CODE))
									.collect(
											Collectors.toList()));
					setupStockLevel(transportOffering, updateStockLevelsToTransportOfferingCronJobModel.getAncillaryStockLevel());
				}
				getModelService().save(transportOffering);
			}
		}

		LOG.info("Cronjob -> BcfUpdateStockLevelsToTransportOfferingJob Completed");

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * Setup stock level for fare products.
	 *
	 * @param transportOffering the transport offering
	 * @param fareProducts      the fare products
	 */
	protected void setupStockLevelForFareProducts(final TransportOfferingModel transportOffering, final List<String> fareProducts)
	{
		fareProducts.stream().distinct().forEach(code -> {
			StockLevelModel stockLevelModel = null;
			if (CollectionUtils.isNotEmpty(transportOffering.getStockLevels()))
			{
				stockLevelModel = transportOffering.getStockLevels().stream()
						.filter(stockLevel -> StringUtils.equals(code, stockLevel.getProductCode())).findFirst().orElse(null);
			}
			if (Objects.isNull(stockLevelModel))
			{
				stockLevelModel = getModelService().create(StockLevelModel.class);
				stockLevelModel.setWarehouse(transportOffering);
				stockLevelModel.setProductCode(code);
			}
			stockLevelModel.setAvailable(0);
			stockLevelModel.setInStockStatus(InStockStatus.FORCEINSTOCK);
			getModelService().save(stockLevelModel);
		});
	}

	/**
	 * @return the categoryService
	 */
	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	/**
	 * @param categoryService the categoryService to set
	 */
	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
