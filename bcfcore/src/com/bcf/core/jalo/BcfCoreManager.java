/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.jalo;

import de.hybris.platform.core.systemsetup.CoreSystemSetup;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.bcf.core.constants.BcfCoreConstants;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 */
@SuppressWarnings("PMD")
public class BcfCoreManager extends GeneratedBcfCoreManager
{
	public static final BcfCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfCoreManager) em.getExtension(BcfCoreConstants.EXTENSIONNAME);
	}
}
