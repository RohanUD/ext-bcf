/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.jalo;

import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.jalo.TravelEurope1PriceFactory;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class BCFTravelEurope1PriceFactory extends TravelEurope1PriceFactory
{
	private static final String JOURNEY_DATE = "journeyDepartureDate";

	@Override
	public List<PriceInformation> getProductPriceInformations(final Product product, final Map<String, String> searchCriteria)
			throws JaloPriceFactoryException
	{
		final SessionContext ctx = getSession().getSessionContext();

		Date journeyDate = null;
		if (searchCriteria != null && searchCriteria.containsKey(JOURNEY_DATE))
		{
			final String departureDateStr = searchCriteria.get(JOURNEY_DATE);
			//remove the departure search criteria, as this attribute does not contained in PriceRowModel
			searchCriteria.remove(JOURNEY_DATE);
			journeyDate = TravelDateUtils.convertStringDateToDate(departureDateStr, TravelservicesConstants.DATE_PATTERN);
		}

		ctx.setAttribute(TravelservicesConstants.PRICING_SEARCH_CRITERIA_MAP, searchCriteria);
		final User user = ctx.getUser();
		final Currency currency = ctx.getCurrency();
		final EnumerationValue ppg = getPPG(ctx, product);
		final EnumerationValue upg = getUPG(ctx, user);

		if (journeyDate == null)
		{
			return super.getProductPriceInformations(product, searchCriteria);
		}
		return getPriceInformations(ctx, product, ppg, ctx.getUser(), upg, currency, true, journeyDate, null);
	}
}
