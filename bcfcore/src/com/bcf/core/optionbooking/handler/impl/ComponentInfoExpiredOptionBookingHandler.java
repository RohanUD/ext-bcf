/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 17/7/19 7:03 PM
 */

package com.bcf.core.optionbooking.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import com.bcf.core.service.BcfBookingService;
import com.bcf.notification.request.optionbooking.ExpiredOptionBookingDetail;


public class ComponentInfoExpiredOptionBookingHandler implements ExpiredOptionBookingNotificationHandler
{
	@Resource(name = "bcfBookingService")
	BcfBookingService bcfBookingService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final ExpiredOptionBookingDetail expiredOptionBookingDetail)
	{
		List<String> onHoldComponentList = new ArrayList<>();

		if (bcfBookingService.checkIfOrderEntriesExistOfType(abstractOrderModel, OrderEntryType.ACCOMMODATION))
		{
			onHoldComponentList.add("HOTEL");
		}
		if (bcfBookingService.checkIfOrderEntriesExistOfType(abstractOrderModel, OrderEntryType.TRANSPORT))
		{
			onHoldComponentList.add("FERRY");
		}
		if (bcfBookingService.checkIfOrderEntriesExistOfType(abstractOrderModel, OrderEntryType.ACTIVITY))
		{
			onHoldComponentList.add("ACTIVITY");
		}

		expiredOptionBookingDetail.setOnHoldComponentList(onHoldComponentList);
	}
}
