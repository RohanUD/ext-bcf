/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 17/7/19 7:11 PM
 */

package com.bcf.core.optionbooking.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import com.bcf.notification.request.optionbooking.ExpiredOptionBookingDetail;


public interface ExpiredOptionBookingNotificationHandler
{
	void handle(final AbstractOrderModel abstractOrderModel, final ExpiredOptionBookingDetail expiredOptionBookingDetail);
}
