/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 17/7/19 6:50 PM
 */

package com.bcf.core.optionbooking.manager.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.ArrayList;
import java.util.List;
import com.bcf.core.optionbooking.handler.impl.ExpiredOptionBookingNotificationHandler;
import com.bcf.core.optionbooking.manager.ExpiredOptionBookingNotificationPipelineManager;
import com.bcf.notification.request.optionbooking.ExpiredOptionBookingDetail;
import com.bcf.notification.request.optionbooking.ExpiredOptionBookingNotificationData;


public class DefaultExpiredOptionBookingNotificationPipelineManager implements ExpiredOptionBookingNotificationPipelineManager
{

	private static final String EXPIRED_OPTION_BOOKINGS_EVENT = "expiredOptionBookings";
	private List<ExpiredOptionBookingNotificationHandler> handlers;

	@Override
	public ExpiredOptionBookingNotificationData executePipeline(final List<AbstractOrderModel> abstractOrderModels)
	{
		ExpiredOptionBookingNotificationData expiredOptionBookingNotificationData = new ExpiredOptionBookingNotificationData();
		List<ExpiredOptionBookingDetail> expiredOptionBookingDetailList = new ArrayList<>();
		expiredOptionBookingNotificationData.setExpiredOptionBookingDetailList(expiredOptionBookingDetailList);

		abstractOrderModels.forEach(abstractOrderModel -> {
			ExpiredOptionBookingDetail expiredOptionBookingDetail = new ExpiredOptionBookingDetail();
			getHandlers().forEach(handler -> {
				handler.handle(abstractOrderModel, expiredOptionBookingDetail);
			});
			expiredOptionBookingNotificationData.getExpiredOptionBookingDetailList().add(expiredOptionBookingDetail);
		});

		expiredOptionBookingNotificationData.setEventType(EXPIRED_OPTION_BOOKINGS_EVENT);
		return expiredOptionBookingNotificationData;
	}

	public List<ExpiredOptionBookingNotificationHandler> getHandlers()
	{
		return handlers;
	}

	public void setHandlers(final List<ExpiredOptionBookingNotificationHandler> handlers)
	{
		this.handlers = handlers;
	}
}
