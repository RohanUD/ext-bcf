/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.impex.translator;

import de.hybris.platform.impex.jalo.translators.SingleValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.util.Config;


public class ConfigurationPropertyValueTranslator extends SingleValueTranslator
{
	@Override
	protected Object convertToJalo(final String s, final Item item)
	{
		return Config.getString(s, "");
	}

	@Override
	protected String convertToString(final Object code)
	{
		return null;
	}
}
