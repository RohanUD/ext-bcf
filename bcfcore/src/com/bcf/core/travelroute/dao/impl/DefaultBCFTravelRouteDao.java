/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelroute.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelRouteDao;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.enums.RouteRegion;
import com.bcf.core.travelroute.dao.BCFTravelRouteDao;


public class DefaultBCFTravelRouteDao extends DefaultTravelRouteDao implements BCFTravelRouteDao
{
	private static final String DESTINATIONS = "destinations";
	private static final String ACTIVE = "active";
	private static final String SELECT_TRAVEL_ROUTE = "SELECT {" + TravelRouteModel.PK + "} FROM {" + TravelRouteModel._TYPECODE
			+ "} WHERE {";

	private static final String TRAVEL_ROUTE_FOR_CURRENT_CONDITIONS =
			SELECT_TRAVEL_ROUTE + TravelRouteModel.CURRENTCONDITIONENABLED + "}=1";

	private static final String TRAVEL_ROUTE_FOR_CURRENT_CONDITIONS_FERRY_TRACKING =
			SELECT_TRAVEL_ROUTE + TravelRouteModel.CURRENTCONDITIONFERRYTRACKINGENABLED + "}=1";
	private static final String SELECT_TRAVEL_ROUTES_FOR_TRANSPORT_FACILITIES = "SELECT {tr.pk} FROM {TravelRoute as tr JOIN "
					+ "TransportFacility as tfo ON {tr.origin}={tfo.pk}  JOIN "
					+ "TransportFacility AS tfd ON {tr.destination}={tfd.pk}}";

	private static final String DIRECT_TRAVEL_ROUTES = SELECT_TRAVEL_ROUTES_FOR_TRANSPORT_FACILITIES + "WHERE {tfo.code} = "
					+ "?originCode AND {tfd.code} = ?destinationCode AND {indirectRoute}=0";

	private static final String TRAVEL_ROUTES_FOR_ACTIVE_TERMINALS = SELECT_TRAVEL_ROUTES_FOR_TRANSPORT_FACILITIES + "where"
					+ " {tfo.active}=1 AND {tfd.active}=1 AND {indirectRoute}=0 AND {tr.mapkmlfile} IS NOT null";


	private static final String TRAVEL_ROUTE_FOR_TYPE = SELECT_TRAVEL_ROUTE + TravelRouteModel.ROUTEREGION + "}=?routeRegion";

	private static final String TRAVEL_ROUTE_DESTINATED_TO_TF = SELECT_TRAVEL_ROUTE + TravelRouteModel.DESTINATION + "} IN (?" + DESTINATIONS + ")";

	private static final String ACTIVE_TRAVEL_ROUTES = "Select {pk} from {TravelRoute} where {active}=?active";
	public DefaultBCFTravelRouteDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<TravelRouteModel> findAllTravelRoutes()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(ACTIVE, 1);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(ACTIVE_TRAVEL_ROUTES, params);
		final SearchResult<TravelRouteModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> findTravelRoutesDestinatedToTF(final Collection<TransportFacilityModel> transportFacilities)
	{
		ServicesUtil.validateParameterNotNull(transportFacilities, "transportFacilities must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(DESTINATIONS, transportFacilities);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(TRAVEL_ROUTE_DESTINATED_TO_TF, params);
		final SearchResult<TravelRouteModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> findTravelRoutesOriginFromTF(final TransportFacilityModel transportFacility)
	{
		ServicesUtil.validateParameterNotNull(transportFacility, "transportFacility must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(TravelRouteModel.ORIGIN, transportFacility);
		final List<TravelRouteModel> travelRouteModel = find(params);
		return CollectionUtils.isNotEmpty(travelRouteModel) ? travelRouteModel : Collections.emptyList();
	}

	@Override
	public List<TravelRouteModel> findTravelRoutesUsingRouteType(final RouteRegion routeRegion)
	{
		ServicesUtil.validateParameterNotNull(routeRegion, "routeType must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put("routeRegion", routeRegion);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(TRAVEL_ROUTE_FOR_TYPE, params);
		final SearchResult<TravelRouteModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> findAllTravelRoutesForCurrentConditions()
	{

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(TRAVEL_ROUTE_FOR_CURRENT_CONDITIONS);
		final SearchResult<TravelRouteModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> findAllTravelRoutesForCurrentConditionsFerryTracking()
	{

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(TRAVEL_ROUTE_FOR_CURRENT_CONDITIONS_FERRY_TRACKING);
		final SearchResult<TravelRouteModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> findDirectTravelRoutes(final String originCode, final String destinationCode)
	{
		ServicesUtil.validateParameterNotNull(originCode, "originCode must not be null!");
		ServicesUtil.validateParameterNotNull(destinationCode, "destinationCode must not be null!");

		final Map<String, String> params = new HashMap<>();
		params.put("originCode", originCode);
		params.put("destinationCode", destinationCode);
		final SearchResult<TravelRouteModel> searchResult = getFlexibleSearchService().search(DIRECT_TRAVEL_ROUTES, params);

		return searchResult.getResult();
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesForActiveTerminals() {
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(TRAVEL_ROUTES_FOR_ACTIVE_TERMINALS);
		final SearchResult<TravelRouteModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}
}
