/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelroute.dao;

import de.hybris.platform.travelservices.dao.TravelRouteDao;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.Collection;
import java.util.List;
import com.bcf.core.enums.RouteRegion;


public interface BCFTravelRouteDao extends TravelRouteDao
{
	/**
	 * Method returns a list of TravelRouteModel from the database
	 *
	 * @return List<TravelRouteModel> list
	 */
	List<TravelRouteModel> findAllTravelRoutes();

	List<TravelRouteModel> findTravelRoutesDestinatedToTF(Collection<TransportFacilityModel> transportFacilities);

	List<TravelRouteModel> findTravelRoutesOriginFromTF(TransportFacilityModel transportFacility);

	List<TravelRouteModel> findTravelRoutesUsingRouteType(RouteRegion routeRegion);

	List<TravelRouteModel> findAllTravelRoutesForCurrentConditions();

	List<TravelRouteModel> findAllTravelRoutesForCurrentConditionsFerryTracking();

	List<TravelRouteModel> findDirectTravelRoutes(String originCode, String destinationCode);

	List<TravelRouteModel> getTravelRoutesForActiveTerminals();
}
