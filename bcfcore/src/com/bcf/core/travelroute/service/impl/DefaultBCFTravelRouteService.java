/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelroute.service.impl;

import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.services.impl.DefaultTravelRouteService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.enums.RouteRegion;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.travelroute.dao.BCFTravelRouteDao;
import com.bcf.core.travelroute.service.BCFTravelRouteService;


public class DefaultBCFTravelRouteService extends DefaultTravelRouteService implements BCFTravelRouteService
{

	private BCFTravelRouteDao bcfTravelRouteDao;
	private BcfTravelLocationService travelLocationService;
	private BcfTransportFacilityService transportFacilityService;

	@Override
	public List<TravelRouteModel> getAllTravelRoutes()
	{
		return getBcfTravelRouteDao().findAllTravelRoutes();
	}

	@Override
	public List<TravelRouteModel> getTravelRouteForCurrentConditions()
	{
		return getBcfTravelRouteDao().findAllTravelRoutesForCurrentConditions();
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesForCurrentConditionsFerryTracking()
	{
		return getBcfTravelRouteDao().findAllTravelRoutesForCurrentConditionsFerryTracking();
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesByRegions(final RouteRegion routeRegion)
	{
		return getBcfTravelRouteDao().findTravelRoutesUsingRouteType(routeRegion);
	}

	@Override
	public Set<TravelRouteModel> getTravelRoutesForDestination(final String locationCode)
	{
		Set<TravelRouteModel> connectedTravelRoutes = new HashSet<>();
		final LocationModel location = getTravelLocationService().getLocation(locationCode);
		if (Objects.nonNull(location))
		{
			final LocationModel cityLocation = getTravelLocationService()
					.getLocationWithLocationType(Collections.singletonList(location), LocationType.CITY);
			final TransportFacilityModel linkedTransportFacility = getTransportFacilityService()
					.getTransportFacilityForLocation(cityLocation);
			if (Objects.nonNull(linkedTransportFacility))
			{
				final List<TravelRouteModel> linkedTravelRoutes = getBcfTravelRouteDao()
						.findTravelRoutesDestinatedToTF(Collections.singleton(linkedTransportFacility));
				connectedTravelRoutes.addAll(linkedTravelRoutes);
			}
		}
		return connectedTravelRoutes;
	}

	@Override
	public Set<TravelRouteModel> getBcfVacationTravelRoutesForDestination(final String locationCode)
	{
		final Set<TravelRouteModel> connectedTravelRoutes = new HashSet<>();

		if(StringUtils.isNotBlank(locationCode))
		{
			final BcfVacationLocationModel vacationLocation = getTravelLocationService().findBcfVacationLocationByCode(locationCode);
			connectedTravelRoutes.addAll(vacationLocation.getTravelRoutes().stream().collect(Collectors.toSet()));
		}
		return connectedTravelRoutes;
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesForOrigin(final TransportFacilityModel transportFacility)
	{
		return getBcfTravelRouteDao().findTravelRoutesOriginFromTF(transportFacility);
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesForDestination(final TransportFacilityModel transportFacility)
	{
		final Collection<TransportFacilityModel> transportFacilities = new ArrayList<>();
		transportFacilities.add(transportFacility);
		return getBcfTravelRouteDao().findTravelRoutesDestinatedToTF(transportFacilities);
	}

	@Override
	public boolean isOpenTicketAllowedForRoute(final String travelRouteCode)
	{
		final TravelRouteModel routeModel = getBcfTravelRouteDao().findTravelRoute(travelRouteCode);
		return routeModel != null && routeModel.isAllowOpenTicket();
	}

	@Override
	public List<TravelRouteModel> getDirectTravelRoutes(final String originCode, final String destinationCode)
	{
		return getBcfTravelRouteDao().findDirectTravelRoutes(originCode, destinationCode);
	}

	@Override
	public List<TravelRouteModel> getTravelRoutesForActiveTerminals()
	{
		return getBcfTravelRouteDao().getTravelRoutesForActiveTerminals();
	}

	protected BcfTravelLocationService getTravelLocationService()
	{
		return travelLocationService;
	}

	@Required
	public void setTravelLocationService(final BcfTravelLocationService travelLocationService)
	{
		this.travelLocationService = travelLocationService;
	}

	protected BcfTransportFacilityService getTransportFacilityService()
	{
		return transportFacilityService;
	}

	@Required
	public void setTransportFacilityService(final BcfTransportFacilityService transportFacilityService)
	{
		this.transportFacilityService = transportFacilityService;
	}

	protected BCFTravelRouteDao getBcfTravelRouteDao()
	{
		return bcfTravelRouteDao;
	}

	@Required
	public void setBcfTravelRouteDao(final BCFTravelRouteDao bcfTravelRouteDao)
	{
		this.bcfTravelRouteDao = bcfTravelRouteDao;
	}
}
