/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelroute.service;

import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.util.List;
import java.util.Set;
import com.bcf.core.enums.RouteRegion;


public interface BCFTravelRouteService extends TravelRouteService
{
	/**
	 * return all travel routes
	 */
	List<TravelRouteModel> getAllTravelRoutes();

	/**
	 * This will return the travel routes which are enabled for Current Conditions.
	 *
	 * @return
	 */
	List<TravelRouteModel> getTravelRouteForCurrentConditions();

	/**
	 * This will return the travel routes which are enabled for Current Conditions Ferry Tracking.
	 *
	 * @return
	 */

	List<TravelRouteModel> getTravelRoutesForCurrentConditionsFerryTracking();

	List<TravelRouteModel> getTravelRoutesByRegions(RouteRegion routeRegion);

	/**
	 * Returns a list of TravelRouteModel for locationCode
	 */
	Set<TravelRouteModel> getTravelRoutesForDestination(String locationCode);

	/**
	 * Gets the travel routes for origin.
	 *
	 * @param transportFacility
	 *           the transport facility
	 * @return the travel routes for origin
	 */
	List<TravelRouteModel> getTravelRoutesForOrigin(TransportFacilityModel transportFacility);

	List<TravelRouteModel> getTravelRoutesForDestination(TransportFacilityModel transportFacility);

	boolean isOpenTicketAllowedForRoute(final String travelRouteCode);

	List<TravelRouteModel> getDirectTravelRoutes(String originCode, String destinationCode);

	Set<TravelRouteModel> getBcfVacationTravelRoutesForDestination(String locationCode);

	List<TravelRouteModel> getTravelRoutesForActiveTerminals();
}
