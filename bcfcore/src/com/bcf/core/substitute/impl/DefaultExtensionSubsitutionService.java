/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.substitute.impl;

import java.util.Map;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.substitute.ExtensionSubstitutionService;


/**
 * Service needed for addons to search files in correct extensions
 */
public class DefaultExtensionSubsitutionService implements ExtensionSubstitutionService
{

	private Map<String, String> extensionSubstitutionMap;

	@Override
	public String getSubstitutedExtension(final String extension)
	{
		final String sub = getExtensionSubstitutionMap().get(extension);
		if (StringUtils.isEmpty(sub))
		{
			return extension;
		}

		return sub;
	}

	/**
	 * @return the extensionSubstitutionMap
	 */
	protected Map<String, String> getExtensionSubstitutionMap()
	{
		return extensionSubstitutionMap;
	}

	/**
	 * @param extensionSubstitutionMap the extensionSubstitutionMap to set
	 */
	public void setExtensionSubstitutionMap(final Map<String, String> extensionSubstitutionMap)
	{
		this.extensionSubstitutionMap = extensionSubstitutionMap;
	}

}
