/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.strategy;

import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Objects;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.model.ServiceNoticeModel;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


public class SetAffectedRouteBasedOnSelectedSubscriptionsStrategy extends AbstractSetAffectedRouteStrategy
{
	@Override
	public void setAffectedRoutes(final ServiceNoticeModel serviceNotice)
	{
		if (Objects.nonNull(serviceNotice.getSubscriptionGroups()))
		{

			final List<TravelRouteModel> travelRoutes = getSubscriptionsMasterDetailsService()
					.getRoutesForSubscriptionGroupsAndType(Lists.newArrayList(serviceNotice.getSubscriptionGroups()),
							CustomerSubscriptionType.SERVICE_NOTICE);
			serviceNotice.setAffectedRoutes(Sets.newHashSet(travelRoutes));
		}
		else if (Objects.nonNull(serviceNotice.getSubscriptionMasterDetails()))
		{
			final List<TravelRouteModel> travelRoutes = getSubscriptionsMasterDetailsService()
					.getRoutesForSubscriptions(Lists.newArrayList(serviceNotice.getSubscriptionMasterDetails()));
			serviceNotice.setAffectedRoutes(Sets.newHashSet(travelRoutes));
		}
	}
}
