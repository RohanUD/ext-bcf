/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.strategy;

import java.util.List;
import java.util.Objects;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.model.BusinessOpportunityModel;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ElectronicNewsLetterModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.google.common.collect.Sets;


/**
 * This strategy is used to set subscription details for all the release center item types other than Service Notice
 */
public class SetAffectedRouteBasedOnAllSubscriptionsStrategy extends AbstractSetAffectedRouteStrategy
{
	@Override
	public void setAffectedRoutes(final ServiceNoticeModel serviceNotice)
	{
		CustomerSubscriptionType subscriptionType = null;
		if (serviceNotice instanceof BusinessOpportunityModel)
		{
			subscriptionType = getSubscriptionType(BusinessOpportunityModel.class);
		}
		else
		{
			subscriptionType = getSubscriptionType(serviceNotice.getClass());
		}
		if (Objects.nonNull(subscriptionType))
		{
			final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetails = getSubscriptionsMasterDetailsService()
					.getSubscriptionsMasterDetailsForType(subscriptionType);
			serviceNotice.setSubscriptionMasterDetails(Sets.newHashSet(crmSubscriptionMasterDetails));
		}
	}

	public CustomerSubscriptionType getSubscriptionType(final Class clazz)
	{
		if (NewsModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.NEWS_RELEASE;
		}
		else if (BusinessOpportunityModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.BUSINESS_OPPORTUNITY;
		}
		else if (ElectronicNewsLetterModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.ELECTRONIC_NEWS_LETTER;
		}
		return null;
	}
}
