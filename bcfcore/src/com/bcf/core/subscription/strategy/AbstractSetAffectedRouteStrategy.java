/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.strategy;

import de.hybris.platform.enumeration.EnumerationService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;


public abstract class AbstractSetAffectedRouteStrategy implements SetAffectedRouteStrategy
{
	private BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService;
	private EnumerationService enumerationService;

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	public BcfSubscriptionsMasterDetailsService getSubscriptionsMasterDetailsService()
	{
		return subscriptionsMasterDetailsService;
	}

	@Required
	public void setSubscriptionsMasterDetailsService(final BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService)
	{
		this.subscriptionsMasterDetailsService = subscriptionsMasterDetailsService;
	}
}
