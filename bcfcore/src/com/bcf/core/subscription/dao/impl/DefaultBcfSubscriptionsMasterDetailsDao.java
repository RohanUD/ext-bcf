/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.subscription.dao.BcfSubscriptionsMasterDetailsDao;


public class DefaultBcfSubscriptionsMasterDetailsDao extends DefaultGenericDao<CRMSubscriptionMasterDetailModel> implements
		BcfSubscriptionsMasterDetailsDao
{
	private static final String SUBSCRIPTION_GROUPS = "Subscription groups";

	private static final String CONDITION_ACTIVE = " AND {s." + CRMSubscriptionMasterDetailModel.ACTIVE + "}=1";

	private static final String QUERY_SUBSCRIPTIONS_FOR_GROUPS =
			"SELECT {s." + CRMSubscriptionMasterDetailModel.PK + "} FROM {" + CRMSubscriptionMasterDetailModel._TYPECODE
					+ " AS s} WHERE {s."
					+ CRMSubscriptionMasterDetailModel.SUBSCRIPTIONGROUP + "} IN (?"
					+ CRMSubscriptionMasterDetailModel.SUBSCRIPTIONGROUP + ")"
					+ CONDITION_ACTIVE;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	public DefaultBcfSubscriptionsMasterDetailsDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> findSubscriptionsForGroups(final List<SubscriptionGroup> subscriptionGroups)
	{
		validateParameterNotNullStandardMessage(SUBSCRIPTION_GROUPS, subscriptionGroups);
		final Map<String, Object> params = new HashMap<>();
		params.put(CRMSubscriptionMasterDetailModel.SUBSCRIPTIONGROUP, subscriptionGroups);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SUBSCRIPTIONS_FOR_GROUPS, params);
		final SearchResult<CRMSubscriptionMasterDetailModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> findAllSubscriptions()
	{
		final List<CRMSubscriptionMasterDetailModel> allSubscriptionMasterModels = find();
		if (CollectionUtils.isNotEmpty(allSubscriptionMasterModels))
		{
			return allSubscriptionMasterModels;
		}

		return Collections.emptyList();
	}

	@Override
	public CRMSubscriptionMasterDetailModel findSubscriptionGroupForSubscriptionCode(final String subscriptionCode)
	{
		validateParameterNotNullStandardMessage(CRMSubscriptionMasterDetailModel.SUBSCRIPTIONCODE, subscriptionCode);
		final Map<String, Object> params = new HashMap<>();
		params.put(CRMSubscriptionMasterDetailModel.SUBSCRIPTIONCODE, subscriptionCode);
		final List<CRMSubscriptionMasterDetailModel> allSubscriptionMasterModels = find(params);
		if (CollectionUtils.isNotEmpty(allSubscriptionMasterModels))
		{
			return allSubscriptionMasterModels.get(0);
		}

		return null;
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> findSubscriptionsMasterDetailsForType(
			final CustomerSubscriptionType subscriptionType)
	{
		validateParameterNotNullStandardMessage(CRMSubscriptionMasterDetailModel.SUBSCRIPTIONTYPE, subscriptionType);
		final Map<String, Object> params = new HashMap<>();
		params.put(CRMSubscriptionMasterDetailModel.SUBSCRIPTIONTYPE, subscriptionType);
		final List<CRMSubscriptionMasterDetailModel> allSubscriptionMasterModels = find(params);
		if (CollectionUtils.isNotEmpty(allSubscriptionMasterModels))
		{
			return allSubscriptionMasterModels;
		}

		return Collections.emptyList();
	}

}
