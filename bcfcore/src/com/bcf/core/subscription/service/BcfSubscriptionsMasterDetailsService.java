/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.service;

import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;


public interface BcfSubscriptionsMasterDetailsService
{
	List<TravelRouteModel> getRoutesForSubscriptionGroups(final List<SubscriptionGroup> subscriptionGroups);

	List<TravelRouteModel> getRoutesForSubscriptions(final List<CRMSubscriptionMasterDetailModel> subscriptions);

	List<CRMSubscriptionMasterDetailModel> getAllSubscriptionsMasterDetails();

	CRMSubscriptionMasterDetailModel getSubscriptionDetailForSubscriptionCode(final String subscriptionCode);

	List<TravelRouteModel> getRoutesForSubscriptionGroupsAndType(List<SubscriptionGroup> allSubscriptionGroups,
			CustomerSubscriptionType type);

	List<CRMSubscriptionMasterDetailModel> getSubscriptionsMasterDetailsForType(final CustomerSubscriptionType subscriptionType);

	List<CRMSubscriptionMasterDetailModel> getSubscriptionMasterDetailsForSubscriptionGroups(final List<SubscriptionGroup> subscriptionGroups);
}
