/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.subscription.service.impl;

import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.stream.Collectors;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.subscription.dao.BcfSubscriptionsMasterDetailsDao;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;


public class DefaultBcfSubscriptionsMasterDetailsService implements BcfSubscriptionsMasterDetailsService
{
	private BcfSubscriptionsMasterDetailsDao bcfSubscriptionsMasterDetailsDao;

	@Override
	public List<TravelRouteModel> getRoutesForSubscriptionGroups(final List<SubscriptionGroup> subscriptionGroups)
	{
		return getRoutesForSubscriptionGroupsAndType(subscriptionGroups, null);
	}

	@Override
	public List<TravelRouteModel> getRoutesForSubscriptionGroupsAndType(final List<SubscriptionGroup> subscriptionGroups,
			final CustomerSubscriptionType type)
	{
		final List<CRMSubscriptionMasterDetailModel> subscriptions = bcfSubscriptionsMasterDetailsDao
				.findSubscriptionsForGroups(subscriptionGroups);

		List<CRMSubscriptionMasterDetailModel> filteredSubscriptions = subscriptions;
		if (type != null)
		{
			filteredSubscriptions = subscriptions.stream()
					.filter(s -> type.equals(s.getSubscriptionType())).collect(Collectors.toList());
		}
		return getRoutesForSubscriptions(filteredSubscriptions);
	}

	@Override
	public List<TravelRouteModel> getRoutesForSubscriptions(final List<CRMSubscriptionMasterDetailModel> subscriptions)
	{
		return subscriptions.stream().flatMap(s -> s.getRoutes().stream()).distinct()
				.collect(Collectors.toList());
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> getAllSubscriptionsMasterDetails()
	{
		return bcfSubscriptionsMasterDetailsDao.findAllSubscriptions();
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> getSubscriptionsMasterDetailsForType(
			final CustomerSubscriptionType subscriptionType)
	{
		return bcfSubscriptionsMasterDetailsDao.findSubscriptionsMasterDetailsForType(subscriptionType);
	}

	@Override
	public CRMSubscriptionMasterDetailModel getSubscriptionDetailForSubscriptionCode(final String subscriptionCode)
	{
		return bcfSubscriptionsMasterDetailsDao.findSubscriptionGroupForSubscriptionCode(subscriptionCode);
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> getSubscriptionMasterDetailsForSubscriptionGroups(
			final List<SubscriptionGroup> subscriptionGroups)
	{
		return bcfSubscriptionsMasterDetailsDao
				.findSubscriptionsForGroups(subscriptionGroups);
	}

	public void setBcfSubscriptionsMasterDetailsDao(final BcfSubscriptionsMasterDetailsDao bcfSubscriptionsMasterDetailsDao)
	{
		this.bcfSubscriptionsMasterDetailsDao = bcfSubscriptionsMasterDetailsDao;
	}

}
