/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.BcfCommentDao;
import com.bcf.core.model.accommodation.BcfCommentModel;


public class DefaultBcfCommentDao extends DefaultGenericDao<BcfCommentModel> implements BcfCommentDao
{

	public DefaultBcfCommentDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public BcfCommentModel findBcfComment(final String code)
	{
		validateParameterNotNull(code, "comment code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfCommentModel.CODE, code);
		final List<BcfCommentModel> bcfCommentModels = find(params);
		if (CollectionUtils.isNotEmpty(bcfCommentModels))
		{
			return bcfCommentModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
