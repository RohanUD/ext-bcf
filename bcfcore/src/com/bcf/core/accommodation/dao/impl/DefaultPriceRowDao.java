/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.accommodation.dao.PriceRowDao;
import com.bcf.core.util.BCFDateUtils;


public class DefaultPriceRowDao extends DefaultGenericDao<PriceRowModel> implements PriceRowDao
{
	private static final Logger LOG = Logger.getLogger(DefaultPriceRowDao.class);

	private static final String PRODUCT_MUST_NOT_NULL = "product must not be null!";
	private static final String PASSENGERTYPE_MUST_NOT_NULL = "passengerType must not be null!";
	private static final String ACCOMMODATIONOFFERINGCODE_MUST_NOT_NULL = "accommodationOfferingCode must not be null!";
	private static final String START_DATE_MUST_NOT_BE_NULL = "startDate must not be null!";
	private static final String END_DATE_MUST_NOT_BE_NULL = "endDate must not be null!";
	private static final String AND_PASSENGER_TYPE = " and passenger type: ";
	private static final String NO_PRICEROW_FOUND_FOR_PRODUCT = "No price row found for product: ";

	private static final String PRICE_ROW_FOR_PRODUCT = "SELECT {" + PriceRowModel.PK + "} from {" + PriceRowModel._TYPECODE
			+ "} WHERE {" + PriceRowModel.PRODUCT + "} = ?product AND ({" + PriceRowModel.PASSENGERTYPE + "}=?passengerCode OR {"
			+ PriceRowModel.PASSENGERTYPE + "} IS NULL) AND (({" + PriceRowModel.STARTTIME + "} <= ?commencementDate AND {"
			+ PriceRowModel.ENDTIME + "} >= ?commencementDate) OR ({" + PriceRowModel.STARTTIME + "} IS NULL AND {"
			+ PriceRowModel.ENDTIME + "} IS NULL))";

	private static final String PRICE_ROW_FOR_PRODUCT_WITH_STARTDATE_ENDDATE = "SELECT {" + PriceRowModel.PK + "} from {" + PriceRowModel._TYPECODE
			+ "} WHERE {" + PriceRowModel.PRODUCT + "} = ?product  AND (({" + PriceRowModel.STARTTIME + "} <= ?startDate AND {"
			+ PriceRowModel.ENDTIME + "} >= ?endDate) OR ({" + PriceRowModel.STARTTIME + "} IS NULL AND {"
			+ PriceRowModel.ENDTIME + "} IS NULL))";


	private static final String PASSENGER_TYPE_CONDITION =" AND ({" + PriceRowModel.PASSENGERTYPE + "}=?passengerCode OR {"
		+ PriceRowModel.PASSENGERTYPE + "} IS NULL)";

	public DefaultPriceRowDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<PriceRowModel> findPriceRow(final ProductModel product)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(PriceRowModel.PRODUCT, product);
		final List<PriceRowModel> priceRowModels = find(params);
		if (CollectionUtils.isNotEmpty(priceRowModels))
		{
			return priceRowModels;
		}

		return Collections.emptyList();
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final Long minQtd)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(minQtd, "minQtd must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(PriceRowModel.PRODUCT, product);
		params.put(PriceRowModel.MINQTD, minQtd);
		final List<PriceRowModel> priceRowModels = find(params);
		if (CollectionUtils.isNotEmpty(priceRowModels))
		{
			return priceRowModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final String passengerType,  final Date startDate,
			 final Date endDate)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(startDate, START_DATE_MUST_NOT_BE_NULL);
		validateParameterNotNull(endDate, END_DATE_MUST_NOT_BE_NULL);

		String query=PRICE_ROW_FOR_PRODUCT_WITH_STARTDATE_ENDDATE;

		if(passengerType!=null){
			 query=query+PASSENGER_TYPE_CONDITION;
		}

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		fQuery.addQueryParameter("product", product);
		fQuery.addQueryParameter("startDate", startDate);
		fQuery.addQueryParameter("endDate", endDate);

		if(passengerType!=null){
			fQuery.addQueryParameter("passengerCode", passengerType);
		}

		final SearchResult<PriceRowModel> result = getFlexibleSearchService().search(fQuery);
		final List<PriceRowModel> priceRows = result.getResult();

		if (CollectionUtils.isNotEmpty(priceRows))
		{
			return priceRows.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final String passengerType, final Date startDate,
			final Date endDate, final List<DayOfWeek> daysOfWeek)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(startDate, START_DATE_MUST_NOT_BE_NULL);
		validateParameterNotNull(endDate, END_DATE_MUST_NOT_BE_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(PriceRowModel.PRODUCT, product);
		params.put(PriceRowModel.PASSENGERTYPE, passengerType);
		params.put(PriceRowModel.STARTTIME, startDate);
		params.put(PriceRowModel.ENDTIME, endDate);
		params.put(PriceRowModel.DAYSOFWEEKCODE, BCFDateUtils.createDaysOfWeekCode(daysOfWeek));

		final SortParameters sortParameters = new SortParameters();
		sortParameters.addSortParameter(PriceRowModel.MODIFIEDTIME, SortParameters.SortOrder.DESCENDING);

		final List<PriceRowModel> priceRowModels = find(params, sortParameters);


		if (CollectionUtils.isNotEmpty(priceRowModels))
		{
			return priceRowModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final String accommodationOfferingCode,
			final String passengerTypeCode, final Date startDate, final Date endDate)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(accommodationOfferingCode, ACCOMMODATIONOFFERINGCODE_MUST_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(PriceRowModel.PRODUCT, product);
		params.put(PriceRowModel.ACCOMMODATIONOFFERINGCODE, accommodationOfferingCode);
		if (Objects.nonNull(passengerTypeCode))
		{
			params.put(PriceRowModel.PASSENGERTYPE, passengerTypeCode);
		}
		if (Objects.nonNull(startDate))
		{
			params.put(PriceRowModel.STARTTIME, startDate);
		}
		if (Objects.nonNull(endDate))
		{
			params.put(PriceRowModel.ENDTIME, endDate);
		}

		final List<PriceRowModel> priceRowModels = find(params);
		if (CollectionUtils.isNotEmpty(priceRowModels))
		{
			return priceRowModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final String passengerType)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(passengerType, PASSENGERTYPE_MUST_NOT_NULL);


		final Map<String, Object> params = new HashMap<>();
		params.put(PriceRowModel.PRODUCT, product);
		params.put(PriceRowModel.PASSENGERTYPE, passengerType);

		final List<PriceRowModel> priceRowModels = find(params);
		if (CollectionUtils.isNotEmpty(priceRowModels))
		{
			return priceRowModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public PriceRowModel findPriceRow(final ProductModel product, final String passengerType, final Date commencementDate)
	{
		validateParameterNotNull(product, PRODUCT_MUST_NOT_NULL);
		validateParameterNotNull(passengerType, PASSENGERTYPE_MUST_NOT_NULL);
		validateParameterNotNull(commencementDate, "Commencement date must not be null!");

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(PRICE_ROW_FOR_PRODUCT);
		fQuery.addQueryParameter("product", product);
		fQuery.addQueryParameter("passengerCode", passengerType);
		fQuery.addQueryParameter("commencementDate", commencementDate);

		final SearchResult<PriceRowModel> result = getFlexibleSearchService().search(fQuery);
		final List<PriceRowModel> priceRows = result.getResult();
		// no price row defined
		if (CollectionUtils.isEmpty(priceRows))
		{
			LOG.warn(NO_PRICEROW_FOUND_FOR_PRODUCT + product.getCode() + AND_PASSENGER_TYPE + passengerType + " and date: "
					+ commencementDate);
			return null;
		}
		//only one price row defined
		if (CollectionUtils.size(priceRows) == 1)
		{
			return priceRows.stream().findFirst().get();
		}
		// find price row with passenger type and date range applicable
		Optional<PriceRowModel> optionalPriceRow = priceRows.stream()
				.filter(priceRow -> (Objects.nonNull(priceRow.getPassengerType()) && Objects.nonNull(priceRow.getStartTime())
						&& Objects.nonNull(priceRow.getEndTime()) && BCFDateUtils
						.isDayOfWeekExistInDayOfWeekCode(commencementDate, priceRow.getDaysOfWeekCode())))
				.findFirst();
		if (optionalPriceRow.isPresent())
		{
			return optionalPriceRow.get();
		}
		// find price row with passenger type and applicable dayofweek
		optionalPriceRow = priceRows.stream().filter(priceRow -> Objects.nonNull(priceRow.getPassengerType()) && BCFDateUtils
				.isDayOfWeekExistInDayOfWeekCode(commencementDate, priceRow.getDaysOfWeekCode())).findFirst();
		if (optionalPriceRow.isPresent())
		{
			return optionalPriceRow.get();
		}
		// find price row with only passenger type applicable
		optionalPriceRow = priceRows.stream().filter(priceRow -> Objects.nonNull(priceRow.getPassengerType())).findFirst();
		if (optionalPriceRow.isPresent())
		{
			return optionalPriceRow.get();
		}
		// find price row with date range applicable and applicable dayofweek
		optionalPriceRow = priceRows.stream()
				.filter(priceRow -> Objects.nonNull(priceRow.getStartTime()) && Objects.nonNull(priceRow.getEndTime()) && BCFDateUtils
						.isDayOfWeekExistInDayOfWeekCode(commencementDate, priceRow.getDaysOfWeekCode())).findFirst();
		if (optionalPriceRow.isPresent())
		{
			return optionalPriceRow.get();
		}
		// find price row with only date range applicable
		optionalPriceRow = priceRows.stream()
				.filter(priceRow -> Objects.nonNull(priceRow.getStartTime()) && Objects.nonNull(priceRow.getEndTime())).findFirst();
		if (optionalPriceRow.isPresent())
		{
			return optionalPriceRow.get();
		}
		// find price row with no passenger type and no date range applicable
		return priceRows.stream().findFirst().orElse(null);
	}
}
