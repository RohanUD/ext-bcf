/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.AccommodationCancellationFeesDao;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public class DefaultAccommodationCancellationFeesDao extends DefaultGenericDao<AccommodationCancellationFeesModel>
		implements AccommodationCancellationFeesDao
{
	public DefaultAccommodationCancellationFeesDao(final String typecode)
	{
		super(typecode);
	}


	private static final String ACCOMMODATION_BCFNONREFUNDFEE_ROW_DATE =
			"SELECT {" + AccommodationCancellationFeesModel.PK + "} from {" + AccommodationCancellationFeesModel._TYPECODE
					+ " as VF join " + AccommodationCancellationPolicyModel._TYPECODE + " as VP on {VF:"
					+ AccommodationCancellationFeesModel.ACCOMMODATIONCANCELLATIONPOLICY + "}={VP:"
					+ AccommodationCancellationPolicyModel.PK + "}} where "
					+ "{VP:" + AccommodationCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate"
					+ " and {VP:" + AccommodationCancellationPolicyModel.ENDDATE + "} >= ?firstTravelDate and {VF:"
					+ AccommodationCancellationFeesModel.MINDAYS + "} <= ?noOfDays"
					+ "	and {VF:" + AccommodationCancellationFeesModel.MAXDAYS + "} >= ?noOfDays and {VP:"
					+ AccommodationCancellationPolicyModel.RATEPLAN + "} IN (?ratePlans)";


	@Override
	public AccommodationCancellationFeesModel findAccommodationCancellationFees(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy, final Integer minDays, final Integer maxDays)
	{
		validateParameterNotNull(accommodationCancellationPolicy, "accommodationCancellationPolicy must not be null!");
		validateParameterNotNull(minDays, "minDays must not be null!");
		validateParameterNotNull(maxDays, "maxDays must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationCancellationFeesModel.ACCOMMODATIONCANCELLATIONPOLICY, accommodationCancellationPolicy);
		params.put(AccommodationCancellationFeesModel.MINDAYS, minDays);
		params.put(AccommodationCancellationFeesModel.MAXDAYS, maxDays);
		final List<AccommodationCancellationFeesModel> accommodationCancellationFeesModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationCancellationFeesModels))
		{
			return accommodationCancellationFeesModels.stream().findFirst().orElse(null);
		}

		return null;
	}


	@Override
	public List<AccommodationCancellationFeesModel> findAccommodationNonRefundableChangeAndCancellationFee(Date firstTravelDate,
			int noOfDays, List<RatePlanModel> ratePlans)
	{

		final Map<String, Object> params = new HashMap<>();
		params.put("firstTravelDate", firstTravelDate);
		params.put("noOfDays", noOfDays);
		params.put("ratePlans", ratePlans);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(ACCOMMODATION_BCFNONREFUNDFEE_ROW_DATE);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<AccommodationCancellationFeesModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;
	}

}
