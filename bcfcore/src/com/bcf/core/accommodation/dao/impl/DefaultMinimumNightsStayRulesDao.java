/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.accommodation.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.bcf.core.accommodation.dao.MinimumNightsStayRulesDao;
import com.bcf.core.model.accommodation.MinimumNightsStayRulesModel;


public class DefaultMinimumNightsStayRulesDao  implements MinimumNightsStayRulesDao
{
	private FlexibleSearchService flexibleSearchService;


	private static final String MIN_NIGHTS_STAY_RULE_ROW =
			"SELECT {" + MinimumNightsStayRulesModel.PK + "} from {" + MinimumNightsStayRulesModel._TYPECODE
					+ " as VP } where {VP:" + MinimumNightsStayRulesModel.STARTDATE + "} = ?startDate"
					+ " and {VP:" + MinimumNightsStayRulesModel.ENDDATE + "} = ?endDate"
					+ " and {VP:" + MinimumNightsStayRulesModel.ACCOMMODATION + "} = ?accommodation";





	@Override
	public MinimumNightsStayRulesModel getMinimumNightsStayRules(final AccommodationModel accommodation, Date startDate, Date endDate){

		String query=MIN_NIGHTS_STAY_RULE_ROW;
		final Map<String, Object> params = new HashMap<>();
		params.put( MinimumNightsStayRulesModel.ACCOMMODATION, accommodation);
		params.put( MinimumNightsStayRulesModel.STARTDATE, startDate);
		params.put( MinimumNightsStayRulesModel.ENDDATE, endDate);


		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<MinimumNightsStayRulesModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult().get(0) : null;

	}


	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
