/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelBundleTemplateDao;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.jalo.warehouse.TransportOffering;
import de.hybris.platform.travelservices.model.deal.RouteBundleTemplateModel;
import de.hybris.platform.travelservices.model.travel.BundleTemplateTransportOfferingMappingModel;
import de.hybris.platform.travelservices.model.travel.CabinClassModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfTravelBundleTemplateDao;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;


public class DefaultBcfTravelBundleTemplateDao extends DefaultTravelBundleTemplateDao implements BcfTravelBundleTemplateDao
{
	private static final String AND = " AND {";
	private static final String FROM = "} FROM {";
	private static final String WHERE = "} WHERE {";
	private static final String SELECT = "SELECT {";
	private static final String RESTRICTION_ONLY_APPROVED_ARCHIVED = AND + BundleTemplateModel.STATUS + "} IN ( "
			+ " {{ SELECT {bts.pk} FROM { " + BundleTemplateStatusModel._TYPECODE + " AS bts JOIN " + EnumerationValueModel._TYPECODE
			+ " AS ev ON {bts.status} = {ev.pk} } WHERE {ev.Code} IN ('approved', 'archived') }} " + ")";
	private static final String FIND_BUNDLETEMPLATE_BY_CODE_AND_CATALOG_VERSION_QUERY = SELECT + BundleTemplateModel.PK + FROM
			+ BundleTemplateModel._TYPECODE + WHERE + BundleTemplateModel.ID + "}= ?uid" + AND + BundleTemplateModel.CATALOGVERSION
			+ "}=?" + BundleTemplateModel.CATALOGVERSION + RESTRICTION_ONLY_APPROVED_ARCHIVED;
	private static final String FIND_BUNDLETEMPLATE_BY_TYPE_QUERY = SELECT + BundleTemplateModel.PK + FROM
			+ BundleTemplateModel._TYPECODE + WHERE + BundleTemplateModel.TYPE + "}= ?" + BundleTemplateModel.TYPE + AND
			+ BundleTemplateModel.CATALOGVERSION + "}=?" + BundleTemplateModel.CATALOGVERSION;
	private static final String FIND_DEFAULT_BUNDLE_TEMPLATE = SELECT + BundleTemplateModel._TYPECODE + FROM
			+ BundleTemplateTransportOfferingMappingModel._TYPECODE + " as bmap JOIN " + BundleTemplateModel._TYPECODE
			+ " as btemplate ON {bmap.bundletemplate}={btemplate.pk} JOIN " + BundleType._TYPECODE
			+ " as btype on {btemplate.type}={btype.pk} }" + " WHERE {bmap."
			+ BundleTemplateTransportOfferingMappingModel.TRANSPORTOFFERING + "} IS NULL" + AND + "bmap.travelSector} IS NULL" + AND
			+ "bmap.travelRoute} IS NULL" + AND + "bmap.cabinClass}=?cabinClass" + AND + "btype.code} IN (?code)";
	private static final String FIND_BUNDLETEMPLATE_TRANSPORTOFFERING_MAPPING = SELECT
			+ BundleTemplateTransportOfferingMappingModel.PK + FROM + BundleTemplateTransportOfferingMappingModel._TYPECODE + WHERE
			+ BundleTemplateTransportOfferingMappingModel.CODE + "}=?" + BundleTemplateTransportOfferingMappingModel.CODE + AND
			+ BundleTemplateTransportOfferingMappingModel.CATALOGVERSION + "}=?"
			+ BundleTemplateTransportOfferingMappingModel.CATALOGVERSION;
	private static final String FIND_TRANPPORTOFFERING_FOR_BUNDLE_TEMPLATE =
			"select {tr." + TransportOffering.PK
					+ "} from {TransportOffering as tr join BundleTemplateTransportOfferingMapping as btt on {tr."
					+ TransportOfferingModel.PK + "}={btt." + BundleTemplateTransportOfferingMappingModel.TRANSPORTOFFERING
					+ "} join RouteBundleTemplate as bt on {btt."
					+ BundleTemplateTransportOfferingMappingModel.BUNDLETEMPLATE + "}={bt." + RouteBundleTemplateModel.PK
					+ "}} where {bt." + RouteBundleTemplateModel.ID + "}=?" + RouteBundleTemplateModel.ID;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public List<BundleTemplateModel> findBundleTemplateByType(final BundleType bundleType,
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(bundleType, "bundleType must not be null!");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLETEMPLATE_BY_TYPE_QUERY);
		query.addQueryParameter(BundleTemplateModel.TYPE, bundleType);
		query.addQueryParameter(BundleTemplateModel.CATALOGVERSION, catalogVersion);
		final SearchResult<BundleTemplateModel> searchResults = getFlexibleSearchService().search(query);
		return searchResults.getResult();
	}

	@Nonnull
	@Override
	public BundleTemplateModel findBundleTemplateById(final String bundleId, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("bundleId", bundleId);
		validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLETEMPLATE_BY_CODE_AND_CATALOG_VERSION_QUERY);
		query.addQueryParameter("uid", bundleId);
		query.addQueryParameter(BundleTemplateModel.CATALOGVERSION, catalogVersion);
		return searchUnique(query);
	}

	@Override
	public List<BundleTemplateModel> findDefaultBundleTemplates(final CabinClassModel cabinClassModel)
	{
		final HashMap<String, Object> params = new HashMap<>();
		final String propertyValue = getBcfConfigurablePropertiesService().getBcfPropertyValue("supportedBundleType");
		final List<String> supportedBundleType = Stream.of(propertyValue.split(",")).collect(Collectors.toList());
		params.put("code", supportedBundleType);
		params.put("cabinClass", cabinClassModel);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_DEFAULT_BUNDLE_TEMPLATE, params);
		final SearchResult<BundleTemplateModel> searchResults = getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResults.getResult();
	}

	@Override
	public BundleTemplateTransportOfferingMappingModel findBundleTemplateTransportOfferingMapping(final String mappingCode,
			final CatalogVersionModel catalogVersion)
	{
		final HashMap<String, Object> params = new HashMap<>();
		params.put(BundleTemplateTransportOfferingMappingModel.CODE, mappingCode);
		params.put(BundleTemplateTransportOfferingMappingModel.CATALOGVERSION, catalogVersion);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_BUNDLETEMPLATE_TRANSPORTOFFERING_MAPPING,
				params);
		final SearchResult<BundleTemplateTransportOfferingMappingModel> searchResults = search(flexibleSearchQuery);
		if (CollectionUtils.isNotEmpty(searchResults.getResult()))
		{
			return searchResults.getResult().stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public List<TransportOfferingModel> findTransportOfferingsForRouteBundleTemplate(final String id)
	{
		final HashMap<String, Object> params = new HashMap<>();
		params.put(RouteBundleTemplateModel.ID, id);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_TRANPPORTOFFERING_FOR_BUNDLE_TEMPLATE,
				params);
		final SearchResult<TransportOfferingModel> searchResults = search(flexibleSearchQuery);

		return searchResults.getResult();
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
