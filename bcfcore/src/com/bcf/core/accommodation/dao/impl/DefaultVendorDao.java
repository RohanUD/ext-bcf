/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.VendorDao;


public class DefaultVendorDao extends DefaultGenericDao<VendorModel> implements VendorDao
{
	public DefaultVendorDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public VendorModel findVendorModel(final String code)
	{
		validateParameterNotNull(code, "vendorName must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(VendorModel.CODE, code);
		final List<VendorModel> vendorModels = find(params);
		if (CollectionUtils.isNotEmpty(vendorModels))
		{
			return vendorModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
