/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.AccommodationCancellationPolicyDao;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public class DefaultAccommodationCancellationPolicyDao extends DefaultGenericDao<AccommodationCancellationPolicyModel>
		implements AccommodationCancellationPolicyDao
{
	public DefaultAccommodationCancellationPolicyDao(final String typecode)
	{
		super(typecode);
	}


	private static final String ACCOMMODATION_POLICY_ROW =
			"SELECT {" + AccommodationCancellationPolicyModel.PK + "} from {" + AccommodationCancellationPolicyModel._TYPECODE
					+ " as VP } where {VP:" + AccommodationCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate"
					+ " and {VP:" + AccommodationCancellationPolicyModel.ENDDATE + "} >= ?firstTravelDate and {VP:"
					+ AccommodationCancellationPolicyModel.RATEPLAN + "} IN (?ratePlans)";



	@Override
	public AccommodationCancellationPolicyModel findAccommodationCancellationPolicy(
			final String accommodationCancellationPolicyCode, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(accommodationCancellationPolicyCode, "accommodationCancellationPolicyCode must not be null!");
		validateParameterNotNull(catalogVersion, "catalogVersion must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationCancellationPolicyModel.CODE, accommodationCancellationPolicyCode);
		params.put(AccommodationCancellationPolicyModel.CATALOGVERSION, catalogVersion);
		final List<AccommodationCancellationPolicyModel> accommodationCancellationPolicyModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationCancellationPolicyModels))
		{
			return accommodationCancellationPolicyModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public List<AccommodationCancellationPolicyModel> findAccommodationNonRefundableChangeAndCancellationPolicy(
			Date firstTravelDate, List<RatePlanModel> ratePlans)
	{

		final Map<String, Object> params = new HashMap<>();
		params.put("firstTravelDate", firstTravelDate);
		params.put("ratePlans", ratePlans);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(ACCOMMODATION_POLICY_ROW);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<AccommodationCancellationPolicyModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;
	}

}
