/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.travelservices.dao.impl.DefaultGuestCountDao;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.BcfGuestCountDao;


public class DefaultBcfGuestCountDao extends DefaultGuestCountDao implements BcfGuestCountDao
{
	public DefaultBcfGuestCountDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public GuestCountModel findGuestCount(final String code)
	{
		validateParameterNotNull(code, "guestCountCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(GuestCountModel.CODE, code);
		final List<GuestCountModel> guestCounts = find(params);
		if (CollectionUtils.isNotEmpty(guestCounts))
		{
			return guestCounts.stream().findFirst().orElse(null);
		}

		return null;
	}

}
