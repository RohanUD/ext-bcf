/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.travelservices.dao.impl.DefaultAccommodationDao;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.BcfAccommodationDao;


public class DefaultBcfAccommodationDao extends DefaultAccommodationDao implements BcfAccommodationDao
{
	public DefaultBcfAccommodationDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public AccommodationModel findAccommodation(final String accommodationCode)
	{
		validateParameterNotNull(accommodationCode, "accommodation code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationModel.CODE, accommodationCode);
		final List<AccommodationModel> accommodationModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationModels))
		{
			return accommodationModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public AccommodationModel findAccommodation(final String accommodationCode, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(accommodationCode, "accommodation code must not be null!");
		validateParameterNotNull(catalogVersion, "catalogVersion must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationModel.CODE, accommodationCode);
		params.put(AccommodationModel.CATALOGVERSION, catalogVersion);
		final List<AccommodationModel> accommodationModels = find(params);
		if (CollectionUtils.isEmpty(accommodationModels))
		{
			return null;
		}
		else if (accommodationModels.size() > 1)
		{
			throw new AmbiguousIdentifierException("Found" + accommodationModels.size() + " results for the given query");
		}
		else
		{
			final Optional<AccommodationModel> accommodationModel = accommodationModels.stream().findFirst();
			return accommodationModel.orElse(null);
		}
	}
}
