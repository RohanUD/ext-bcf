/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.travelservices.enums.AccommodationFacilityType;
import de.hybris.platform.travelservices.model.facility.AccommodationFacilityModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.AccommodationFacilityDao;


public class DefaultAccommodationFacilityDao extends DefaultGenericDao<AccommodationFacilityModel>
		implements AccommodationFacilityDao
{

	public DefaultAccommodationFacilityDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public AccommodationFacilityModel findAccommodationFacility(final AccommodationFacilityType accommodationFacilityType)
	{
		validateParameterNotNull(accommodationFacilityType, "accommodationFacilityType must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationFacilityModel.TYPE, accommodationFacilityType);
		final List<AccommodationFacilityModel> accommodationFacilities = find(params);
		if (CollectionUtils.isNotEmpty(accommodationFacilities))
		{
			return accommodationFacilities.stream().findFirst().orElse(null);
		}

		return null;
	}

}
