/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelBundleTemplateStatusDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.BcfTravelBundleTemplateStatusDao;


public class DefaultBcfTravelBundleTemplateStatusDao extends DefaultTravelBundleTemplateStatusDao
		implements BcfTravelBundleTemplateStatusDao
{

	public DefaultBcfTravelBundleTemplateStatusDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public BundleTemplateStatusModel findBundleTemplateStatus(final String statusId, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(statusId, "Bundle Template Status Id must not be null!");
		validateParameterNotNull(catalogVersion, "Catalog Version must not be null!");

		final Map<String, Object> params = new HashMap<>();
		params.put(BundleTemplateStatusModel.ID, statusId);
		params.put(BundleTemplateStatusModel.CATALOGVERSION, catalogVersion);
		final List<BundleTemplateStatusModel> bundleTemplateStatusModels = find(params);
		if (CollectionUtils.isNotEmpty(bundleTemplateStatusModels))
		{
			return bundleTemplateStatusModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
