/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.BundleSelectionCriteriaDao;


public class DefaultBundleSelectionCriteriaDao extends DefaultGenericDao<BundleSelectionCriteriaModel>
		implements BundleSelectionCriteriaDao
{

	public DefaultBundleSelectionCriteriaDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public BundleSelectionCriteriaModel findBundleSelectionCriteria(final String id)
	{
		validateParameterNotNull(id, "bundle selection criteria's id must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(BundleSelectionCriteriaModel.ID, id);
		final List<BundleSelectionCriteriaModel> bundleSelectionCriteriaModels = find(params);
		if (CollectionUtils.isNotEmpty(bundleSelectionCriteriaModels))
		{
			return bundleSelectionCriteriaModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
