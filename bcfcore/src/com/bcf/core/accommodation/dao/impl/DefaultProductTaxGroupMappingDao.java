/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.bcf.core.accommodation.dao.ProductTaxGroupMappingDao;
import com.bcf.core.model.accommodation.ProductTaxGroupMappingModel;


public class DefaultProductTaxGroupMappingDao extends DefaultGenericDao<ProductTaxGroupMappingModel>
		implements ProductTaxGroupMappingDao
{

	public DefaultProductTaxGroupMappingDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public ProductTaxGroupMappingModel findProductTaxGroupMapping(final Boolean applyDMF, final Boolean applyMRDT,
			final Boolean applyGST, final Boolean applyPST)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(ProductTaxGroupMappingModel.APPLYDMF, BooleanUtils.isTrue(applyDMF));
		params.put(ProductTaxGroupMappingModel.APPLYMRDT, BooleanUtils.isTrue(applyMRDT));
		params.put(ProductTaxGroupMappingModel.APPLYGST, BooleanUtils.isTrue(applyGST));
		params.put(ProductTaxGroupMappingModel.APPLYPST, BooleanUtils.isTrue(applyPST));
		final List<ProductTaxGroupMappingModel> productTaxGroupMapping = find(params);
		if (CollectionUtils.isNotEmpty(productTaxGroupMapping))
		{
			return productTaxGroupMapping.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public ProductTaxGroupMappingModel findProductTaxGroupMapping(final ProductTaxGroup productTaxGroup)
	{
		validateParameterNotNull(productTaxGroup, "ProductTaxGroup must not null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ProductTaxGroupMappingModel.PRODUCTTAXGROUP, productTaxGroup);
		final List<ProductTaxGroupMappingModel> productTaxGroupMapping = find(params);
		if (CollectionUtils.isEmpty(productTaxGroupMapping))
		{
			return null;
		}
		if (CollectionUtils.size(productTaxGroupMapping) > 1)
		{
			throw new AmbiguousIdentifierException("Multiple product tax group mapping with same productTaxGroup found");
		}
		return productTaxGroupMapping.stream().findFirst().orElse(null);
	}

}
