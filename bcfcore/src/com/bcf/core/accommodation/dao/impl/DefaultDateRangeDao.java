/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.DateRangeDao;


public class DefaultDateRangeDao extends DefaultGenericDao<DateRangeModel> implements DateRangeDao
{
	public DefaultDateRangeDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public DateRangeModel findDateRangeModel(final Date startDate, final Date endDate)
	{
		validateParameterNotNull(startDate, "startDate must not be null!");
		validateParameterNotNull(endDate, "endDate must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(DateRangeModel.STARTINGDATE, startDate);
		params.put(DateRangeModel.ENDINGDATE, endDate);
		final List<DateRangeModel> dateRangeModels = find(params);
		if (CollectionUtils.isNotEmpty(dateRangeModels))
		{
			return dateRangeModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
