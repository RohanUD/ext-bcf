/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.accommodation.dao.GuestOccupancyDao;


public class DefaultGuestOccupancyDao extends DefaultGenericDao<GuestOccupancyModel> implements GuestOccupancyDao
{
	private static final String GET_GUEST_OCCUPANCY_MODEL_QUERY = "SELECT {" + GuestOccupancyModel.PK + "} FROM {"
			+ GuestOccupancyModel._TYPECODE + " AS go JOIN " + PassengerTypeModel._TYPECODE
			+ " AS pt ON {go." + GuestOccupancyModel.PASSENGERTYPE + "}={pt." + PassengerTypeModel.PK
			+ "}} WHERE {pt." + PassengerTypeModel.CODE + "}=?passengerType AND {go."
			+ GuestOccupancyModel.QUANTITYMAX + "}=?quantityMax AND{go." + GuestOccupancyModel.QUANTITYMIN + "}=?quantityMin";

	public DefaultGuestOccupancyDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public GuestOccupancyModel findGuestOccupancy(final String code)
	{
		validateParameterNotNull(code, "guestOccupancyCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(GuestOccupancyModel.CODE, code);
		final List<GuestOccupancyModel> guestOccupancies = find(params);
		if (CollectionUtils.isNotEmpty(guestOccupancies))
		{
			return guestOccupancies.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public List<GuestOccupancyModel> findGuestOccupancy(final String passengerType, final Integer quantityMax,
			final Integer quantityMin)
	{
		validateParameterNotNull(passengerType, "guestOccupancyPassengerType must not be null!");
		validateParameterNotNull(quantityMax, "guestOccupancyQuantityMax must not be null!");
		validateParameterNotNull(quantityMin, "guestOccupancyQuantityMin must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(GuestOccupancyModel.PASSENGERTYPE, passengerType);
		params.put(GuestOccupancyModel.QUANTITYMAX, quantityMax);
		params.put(GuestOccupancyModel.QUANTITYMIN, quantityMin);
		final SearchResult<GuestOccupancyModel> guestOccupancies = getFlexibleSearchService()
				.search(GET_GUEST_OCCUPANCY_MODEL_QUERY, params);
		if (CollectionUtils.isNotEmpty(guestOccupancies.getResult()))
		{
			return guestOccupancies.getResult();
		}
		return CollectionUtils.isNotEmpty(guestOccupancies.getResult()) ? guestOccupancies.getResult() :null;
	}
}
