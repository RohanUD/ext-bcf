/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.travelservices.dao.TravelBundleTemplateDao;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.model.travel.BundleTemplateTransportOfferingMappingModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.List;
import javax.annotation.Nonnull;


public interface BcfTravelBundleTemplateDao extends TravelBundleTemplateDao
{

	List<BundleTemplateModel> findBundleTemplateByType(BundleType bundleType, CatalogVersionModel catalogVersion);

	/**
	 * This method returns the {@code BundleTemplateModel} corresponding to the bundleID
	 *
	 * @param bundleId
	 * @return {@link BundleTemplateModel}
	 */
	@Nonnull
	BundleTemplateModel findBundleTemplateById(String bundleId, CatalogVersionModel catalogVersion);

	/**
	 * Find bundle template transport offering mapping.
	 *
	 * @param mappingCode    the mapping code
	 * @param catalogVersion the catalog version
	 * @return the bundle template transport offering mapping model
	 */
	BundleTemplateTransportOfferingMappingModel findBundleTemplateTransportOfferingMapping(String mappingCode,
			CatalogVersionModel catalogVersion);


	List<TransportOfferingModel> findTransportOfferingsForRouteBundleTemplate(String id);
}
