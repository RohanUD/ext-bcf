/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.europe1.enums.ProductTaxGroup;
import com.bcf.core.model.accommodation.ProductTaxGroupMappingModel;


public interface ProductTaxGroupMappingDao
{

	/**
	 * Find product tax group mapping.
	 *
	 * @param applyDMF  the apply DMF
	 * @param applyMRDT the apply MRDT
	 * @param applyGST  the apply GST
	 * @param applyPST  the apply PST
	 * @return the product tax group mapping model
	 */
	ProductTaxGroupMappingModel findProductTaxGroupMapping(Boolean applyDMF, Boolean applyMRDT, Boolean applyGST,
			Boolean applyPST);

	/**
	 * Find product tax group mapping.
	 *
	 * @param productTaxGroup
	 *           the product tax group
	 * @return the product tax group mapping model
	 */
	ProductTaxGroupMappingModel findProductTaxGroupMapping(ProductTaxGroup productTaxGroup);

}
