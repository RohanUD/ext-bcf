/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import java.util.List;


public interface GuestOccupancyDao
{

	/**
	 * Find guest occupancy.
	 *
	 * @param code the code
	 * @return the guest occupancy model
	 */
	GuestOccupancyModel findGuestOccupancy(String code);

	/**
	 * Find guest occupancy.
	 *
	 * @param passengerType the passengerType
	 * @param quantityMax   the quantityMax
	 * @param quantityMin   the quantityMin
	 * @return the guest occupancy model
	 */
	List<GuestOccupancyModel> findGuestOccupancy(String passengerType, Integer quantityMax, Integer quantityMin);
}
