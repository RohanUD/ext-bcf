/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.dao.AccommodationDao;
import de.hybris.platform.travelservices.model.product.AccommodationModel;


public interface BcfAccommodationDao extends AccommodationDao
{

	/**
	 * Find accommodation.
	 *
	 * @param accommodationCode the accommodation code
	 * @return the accommodation model
	 */
	AccommodationModel findAccommodation(String accommodationCode);

	/**
	 * Find accommodation.
	 *
	 * @param accommodationCode
	 *           the accommodation code
	 * @param catalogVersion
	 *           the catalog version
	 * @return the accommodation model
	 */
	AccommodationModel findAccommodation(String accommodationCode, CatalogVersionModel catalogVersion);
}
