/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.model.PriceRowModel;
import java.util.Date;
import java.util.List;


public interface PriceRowDao
{

	/**
	 * Find price row.
	 *
	 * @param product the product
	 * @return the price row model
	 */
	List<PriceRowModel> findPriceRow(ProductModel product);

	/**
	 * Find price row.
	 *
	 * @param product the product
	 * @param minQtd  the minQtd
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(ProductModel product, Long minQtd);

	/**
	 * Find price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(final ProductModel product, final String passengerType, final Date startDate,
			final Date endDate);

	/**
	 * Find price row with an additional dayOfWeek condition
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @param daysOfWeek
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(ProductModel product, String passengerType, Date startDate, Date endDate,
			final List<DayOfWeek> daysOfWeek);

	/**
	 * Find price row.
	 *
	 * @param product                   the product
	 * @param accommodationOfferingCode the accommodation offering code
	 * @param passengerTypeCode         the passenger type code
	 * @param startDate                 the start date
	 * @param endDate                   the end date
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(ProductModel product, String accommodationOfferingCode, String passengerTypeCode, Date startDate,
			Date endDate);


	/**
	 * Find price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(ProductModel product, String passengerType);

	/**
	 * Find price row.
	 *
	 * @param product          the product
	 * @param passengerType    the passenger type
	 * @param commencementDate the commencement date
	 * @return the price row model
	 */
	PriceRowModel findPriceRow(ProductModel product, String passengerType, Date commencementDate);
}
