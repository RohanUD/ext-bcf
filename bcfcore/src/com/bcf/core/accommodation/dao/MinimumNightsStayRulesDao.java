/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.Date;
import com.bcf.core.model.accommodation.MinimumNightsStayRulesModel;


public interface MinimumNightsStayRulesDao
{

	/**
	 * Find accommodation.
	 *
	 * @param accommodationCode the accommodation code
	 * @return the accommodation model
	 */
	MinimumNightsStayRulesModel getMinimumNightsStayRules(final AccommodationModel accommodation, Date startDate, Date endDate);


}
