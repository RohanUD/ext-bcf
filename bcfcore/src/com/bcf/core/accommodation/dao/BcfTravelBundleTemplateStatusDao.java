/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.travelservices.dao.TravelBundleTemplateStatusDao;

public interface BcfTravelBundleTemplateStatusDao extends TravelBundleTemplateStatusDao
{
	/**
	 * Find bundle template status.
	 *
	 * @param statusId
	 *           the status id
	 * @param catalogVersion
	 *           the catalog version
	 * @return the bundle template status model
	 */
	BundleTemplateStatusModel findBundleTemplateStatus(String statusId, CatalogVersionModel catalogVersion);

}
