/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.List;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public interface AccommodationCancellationPolicyDao
{

	/**
	 * Find Accommodation Cancellation Policy.
	 *
	 * @param accommodationCancellationPolicyCode the Accommodation Cancellation Policy Code
	 * @return the AccommodationCancellationPolicyModel
	 */
	AccommodationCancellationPolicyModel findAccommodationCancellationPolicy(String accommodationCancellationPolicyCode, CatalogVersionModel catalogVersion);

	List<AccommodationCancellationPolicyModel> findAccommodationNonRefundableChangeAndCancellationPolicy(
			Date firstTravelDate, List<RatePlanModel> ratePlans);

}
