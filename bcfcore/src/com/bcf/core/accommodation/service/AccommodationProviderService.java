/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.travelservices.model.accommodation.AccommodationProviderModel;


public interface AccommodationProviderService
{

	/**
	 * Gets the accommodation provider.
	 *
	 * @param providerName the provider name
	 * @return the accommodation provider
	 */
	AccommodationProviderModel getAccommodationProvider(String providerName);
}
