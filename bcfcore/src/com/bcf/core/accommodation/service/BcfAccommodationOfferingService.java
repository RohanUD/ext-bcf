/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.List;
import java.util.Set;


public interface BcfAccommodationOfferingService extends AccommodationOfferingService
{

	/**
	 * To provide the set of hotels associated to the rooms.
    *
	 * @param roomRateProducts
	 * @return set of hotels
	 */
	Set<AccommodationOfferingModel> getAccommodationOfferingByProducts(final Set<String> roomRateProducts);


	/**
	 * Retrieves the accommodation from a given room rate product.
	 * @param roomRateProduct
	 *
	 * @return accommodation
	 */
	AccommodationModel getAccommodation(RoomRateProductModel roomRateProduct);

   /**
    *
    * @param roomRateProducts
    * @return
    */
	List<AccommodationModel> getAccommodationsByRoomRateProducts(Set<String> roomRateProducts);

}
