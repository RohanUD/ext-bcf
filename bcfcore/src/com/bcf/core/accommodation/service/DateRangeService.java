/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import java.util.Date;


public interface DateRangeService
{

	/**
	 * Gets the date range.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @return the date range
	 */
	DateRangeModel getDateRange(Date startDate, Date endDate);
}
