/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.services.AccommodationService;


public interface BcfAccommodationService extends AccommodationService
{
	/**
	 * Gets the accommodation.
	 *
	 * @param accommodationCode the accommodation Code
	 * @param catalogVersion    *           the catalog Version
	 * @return the accommodation
	 */
	AccommodationModel getAccommodation(final String accommodationCode, final CatalogVersionModel catalogVersion);

	/**
	 * Gets the accommodation.
	 *
	 * @param accommodationCode the accommodation code
	 * @return the accommodation
	 */
	AccommodationModel getAccommodation(final String accommodationCode);
}
