/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.europe1.enums.ProductTaxGroup;
import com.bcf.core.model.accommodation.ProductTaxGroupMappingModel;


public interface ProductTaxGroupMappingService
{

	/**
	 * Gets the product tax group mapping.
	 *
	 * @param applyDMF  the apply DMF
	 * @param applyMRDT the apply MRDT
	 * @param applyGST  the apply GST
	 * @param applyPST  the apply PST
	 * @return the product tax group
	 */
	ProductTaxGroup getProductTaxGroupMapping(Boolean applyDMF, Boolean applyMRDT, Boolean applyGST, Boolean applyPST);

	/**
	 * Gets the product tax group mapping.
	 *
	 * @param productTaxGroup
	 *           the product tax group
	 * @return the product tax group mapping
	 */
	ProductTaxGroupMappingModel getProductTaxGroupMapping(ProductTaxGroup productTaxGroup);
}
