/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;



import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.List;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public interface AccommodationCancellationFeesService
{

	/**
	 * Gets the accommodation cancellation fees.
	 *
	 * @param accommodationCancellationPolicy the accommodation Cancellation Policy
	 * @param minDays                         the min days
	 * @param maxDays                         the max days
	 * @return the AccommodationCancellationFeesModel
	 */
	AccommodationCancellationFeesModel getAccommodationCancellationFees(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy, final Integer minDays,
			final Integer maxDays);

	List<AccommodationCancellationFeesModel> findAccommodationNonRefundableChangeAndCancellationFee(Date firstTravelDate,
			int noOfDays, List<RatePlanModel> ratePlans);

	}
