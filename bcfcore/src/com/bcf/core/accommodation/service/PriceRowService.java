/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.model.PriceRowModel;
import java.util.Date;
import java.util.List;


public interface PriceRowService
{

	/**
	 * Gets the price row.
	 *
	 * @param product the product
	 * @return the price row
	 */
	List<PriceRowModel> getPriceRow(ProductModel product);

	/**
	 * Gets the price row.
	 *
	 * @param product the product
	 * @param minQtd  the min Qtd
	 * @return the price row
	 */
	PriceRowModel getPriceRow(ProductModel product, Long minQtd);

	/**
	 * Gets the price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @return the price row
	 */
	PriceRowModel getPriceRow(ProductModel product, String passengerType, Date startDate,Date endDate);

	/**
	 * Gets the price row.
	 *
	 * @param product                   the product
	 * @param accommodationOfferingCode the accommodation offering code
	 * @param passengerTypeCode         the passenger type code
	 * @param startDate                 the start date
	 * @param endDate                   the end date
	 * @return the price row
	 */
	PriceRowModel getPriceRow(ProductModel product, String accommodationOfferingCode, String passengerTypeCode, Date startDate,
			Date endDate);

	/**
	 * Gets the price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @return the price row
	 */
	PriceRowModel getPriceRow(ProductModel product, String passengerType);

	/**
	 * Gets the price row.
	 *
	 * @param product          the product
	 * @param passengerType    the passenger type
	 * @param commencementDate the commencement date
	 * @return the price row
	 */
	PriceRowModel getPriceRow(ProductModel product, String passengerType, Date commencementDate);

	PriceRowModel getPriceRow(final ProductModel product, final String passengerType, final Date startDate,
			final Date endDate, final List<DayOfWeek> daysOfWeek);
}
