/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import com.bcf.core.model.accommodation.BcfCommentModel;


public interface BcfCommentService
{

	/**
	 * Gets the bcf comment.
	 *
	 * @param code
	 *           the code
	 * @return the bcf comment
	 */
	BcfCommentModel getBcfComment(String code);
}
