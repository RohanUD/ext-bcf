/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import java.util.Collection;
import java.util.List;


public interface GuestOccupancyService
{

	/**
	 * Gets the guest occupancies.
	 *
	 * @param guestOccupancies the guest occupancies
	 * @return the guest occupancies
	 */
	List<GuestOccupancyModel> getGuestOccupancies(Collection<String> guestOccupancies);

	/**
	 * Gets the guest occupancy.
	 *
	 * @param passengerType the passengerType
	 * @param quantityMax   the quantityMax
	 * @param quantityMin   the quantityMin
	 * @return the guest occupancies
	 */
	List<GuestOccupancyModel> getGuestOccupancies(final String passengerType, final Integer quantityMax,
			final Integer quantityMin);
}
