/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.travelservices.bundle.TravelBundleTemplateService;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.model.travel.BundleTemplateTransportOfferingMappingModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.List;


public interface BcfTravelBundleTemplateService extends TravelBundleTemplateService
{

	/**
	 * Gets the bundle template.
	 *
	 * @param bundleType     the bundle type
	 * @param productType    the product type
	 * @param catalogVersion the catalog version
	 * @return the bundle template
	 */
	BundleTemplateModel getBundleTemplate(final BundleType bundleType, String productType, CatalogVersionModel catalogVersion);


	/**
	 * Gets the bundle selection criteria.
	 *
	 * @param id the id
	 * @return the bundle selection criteria
	 */
	BundleSelectionCriteriaModel getBundleSelectionCriteria(String id);

	/**
	 * Gets the bundle template by id.
	 *
	 * @param bundleId       the bundle id
	 * @param catalogVersion the catalog version
	 * @return the bundle template by id
	 */
	BundleTemplateModel getBundleTemplateById(String bundleId, CatalogVersionModel catalogVersion);

	/**
	 * Gets the bundle template transport offering mapping by id.
	 *
	 * @param mappingCode    the mapping code
	 * @param catalogVersion the catalog version
	 * @return the bundle template transport offering mapping by id
	 */
	BundleTemplateTransportOfferingMappingModel getBundleTemplateTransportOfferingMappingById(String mappingCode,
			CatalogVersionModel catalogVersion);

	List<TransportOfferingModel> getRouteBundleTemplateTransportOfferings(String id);
}
