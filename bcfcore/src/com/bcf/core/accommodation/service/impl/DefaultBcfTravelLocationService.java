/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.services.impl.DefaultTravelLocationService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.dao.BcfTravelLocationDao;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class DefaultBcfTravelLocationService extends DefaultTravelLocationService implements BcfTravelLocationService
{
	private BcfTravelLocationDao bcfTravelLocationDao;
	private ModelService modelService;


	@Override
	public List<String> getLocationCodesInHierarchyByLocationTypes(final LocationModel location,
			final List<LocationType> locationTypes)
	{
		if (Objects.isNull(location))
		{
			return Collections.emptyList();
		}

		final List<String> locationCodes = new ArrayList<>();
		locationTypes.forEach(locationType -> {
			final LocationModel locationModel = Objects.equals(locationType, location.getLocationType()) ? location
					: getLocationWithLocationType(location.getSuperlocations(), locationType);
			if (Objects.nonNull(locationModel))
			{
				locationCodes.add(locationModel.getCode());
			}
		});
		return locationCodes;
	}

	/**
	 * Gets the location with location type.
	 *
	 * @param locations    the locations
	 * @param locationType the location type
	 * @return the locations with location type
	 */
	@Override
	public LocationModel getLocationWithLocationType(final List<LocationModel> locations, final LocationType locationType)
	{
		if (CollectionUtils.isEmpty(locations))
		{
			return null;
		}
		final Optional<LocationModel> optionalLocation = locations.stream().filter(Objects::nonNull)
				.filter(location -> Objects.equals(locationType, location.getLocationType())).findFirst();

		if (optionalLocation.isPresent())
		{
			return optionalLocation.get();
		}

		final List<LocationModel> superLocations = locations.stream().filter(Objects::nonNull)
				.flatMap(location -> location.getSuperlocations().stream())
				.collect(Collectors.toList());
		return getLocationWithLocationType(superLocations, locationType);
	}

	public LocationModel getLocationForCodeAndType(final String code, final LocationType locationType)
	{
		return getBcfTravelLocationDao().findLocationForCodeAndType(code, locationType);
	}

	@Override
	public LocationModel getOrCreateLocationFromVacationLocation(final BcfVacationLocationModel vacationLocationModel)
	{
		if (Objects.isNull(vacationLocationModel) || Objects.isNull(vacationLocationModel.getLocationType()))
		{
			return null;
		}
		LocationModel locationModel = getLocationForCodeAndType(vacationLocationModel.getCode(),
				vacationLocationModel.getLocationType());
		if (Objects.isNull(locationModel) || !vacationLocationModel.getLocationType().equals(locationModel.getLocationType()))
		{
			locationModel = getModelService().create(LocationModel.class);
			locationModel.setCode(vacationLocationModel.getCode());
			locationModel.setName(vacationLocationModel.getName());
			locationModel.setLocationType(vacationLocationModel.getLocationType());
			getModelService().save(locationModel);
		}
		return locationModel;
	}

	@Override
	public BcfVacationLocationModel getBcfVacationLocationWithLocationType(final List<BcfVacationLocationModel> locations,
			final LocationType locationType)
	{
		if (CollectionUtils.sizeIsEmpty(locations))
		{
			return null;
		}
		final Optional<BcfVacationLocationModel> optionalLocation = locations.stream()
				.filter(location -> Objects.equals(locationType, location.getLocationType())).findFirst();

		if (optionalLocation.isPresent())
		{
			return optionalLocation.get();
		}

		final BcfVacationLocationModel parentLocation = locations.stream().findFirst().get().getParentLocation();
		if (Objects.nonNull(parentLocation))
		{
			return getBcfVacationLocationWithLocationType(
					Collections.singletonList(parentLocation), locationType);
		}
		return null;
	}

	@Override
	public List<BcfVacationLocationModel> getBcfVacationSubLocations(final BcfVacationLocationModel locationModel)
	{
		return getBcfTravelLocationDao().getBcfVacationSubLocations(locationModel);
	}

	@Override
	public List<BcfVacationLocationModel> getGeographicalAreasForFerry()
	{
		return getBcfTravelLocationDao().getLocationsByType(LocationType.GEOGRAPHICAL_AREA.getCode());
	}

	@Override
	public List<BcfVacationLocationModel> getAllBcfVacationLocations()
	{
		return getBcfTravelLocationDao().findAllBcfVacationLocations();
	}

	@Override
	public BcfVacationLocationModel findBcfVacationLocationByCode(final String code)
	{
		return getBcfTravelLocationDao().findBcfVacationLocationByCode(code);
	}


	@Override
	public BcfVacationLocationModel findBcfVacationLocationByOriginalLocation(final LocationModel location)
	{
		return getBcfTravelLocationDao().findBcfVacationLocationByOriginalLocation(location);
	}

	@Override
	public List<LocationModel> getSubLocations(final LocationModel locationModel)
	{
		return getBcfTravelLocationDao().getSubLocations(locationModel);
	}

	@Override
	public List<BcfVacationLocationModel> getVacationLocationsByType(final String locationType)
	{
		return getBcfTravelLocationDao().getLocationsByType(locationType);
	}

	@Override
	public SearchPageData<BcfVacationLocationModel> getPaginatedLocationsByType(final String locationType, final int pageSize,
			final int currentPage)
	{
		return getBcfTravelLocationDao().getPaginatedLocationsByType(locationType, pageSize, currentPage);
	}

	@Override
	public SearchPageData<BcfVacationLocationModel> getPaginatedCitiesByRegion(final String regionCode, final int pageSize,
			final int currentPage)
	{
		return getBcfTravelLocationDao().getPaginatedCitiesByRegion(regionCode, pageSize, currentPage);
	}

	@Override
	public BcfVacationLocationModel getBcfVacationLocationByCode(final String code)
	{
		return getBcfTravelLocationDao().findBcfVacationLocationByCode(code);
	}

	@Override
	public PointOfServiceModel getPointOfServiceForName(final BaseStoreModel baseStore, final String name)
	{
		return getBcfTravelLocationDao().findPointOfServiceForName(name);
	}

	@Override
	public PointOfServiceModel getOrCreatePointOfServiceForName(final String name)
	{
		PointOfServiceModel pointOfService = getBcfTravelLocationDao().findPointOfServiceForName(name);

		if (Objects.isNull(pointOfService))
		{
			pointOfService = getModelService().create(PointOfServiceModel.class);
			pointOfService.setName(name);
		}
		return pointOfService;
	}
	/**
	 * @return the bcfTravelLocationDao
	 */
	protected BcfTravelLocationDao getBcfTravelLocationDao()
	{
		return bcfTravelLocationDao;
	}

	/**
	 * @param bcfTravelLocationDao the bcfTravelLocationDao to set
	 */
	@Required
	public void setBcfTravelLocationDao(final BcfTravelLocationDao bcfTravelLocationDao)
	{
		this.bcfTravelLocationDao = bcfTravelLocationDao;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


}
