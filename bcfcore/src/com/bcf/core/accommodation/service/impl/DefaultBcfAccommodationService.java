/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.impl.DefaultAccommodationService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfAccommodationDao;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;


public class DefaultBcfAccommodationService extends DefaultAccommodationService implements BcfAccommodationService
{
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationDao bcfAccommodationDao;

	@Override
	public AccommodationModel getAccommodation(final String accommodationCode, final CatalogVersionModel catalogVersion)
	{
		return getBcfAccommodationDao().findAccommodation(accommodationCode, catalogVersion);
	}

	@Override
	public AccommodationModel getAccommodationForAccommodationOffering(String accommodationOfferingCode, String accommodationCode) {
		AccommodationOfferingModel accommodationOfferingModel=bcfAccommodationOfferingService.getAccommodationOffering(accommodationOfferingCode);
		if(accommodationOfferingModel!=null && CollectionUtils.isNotEmpty(accommodationOfferingModel.getAccommodations()) && accommodationOfferingModel.getAccommodations().contains(accommodationCode)){
				return getAccommodation(accommodationCode);
		}
		return null;
	}

	@Override
	public AccommodationModel getAccommodation(final String accommodationCode)
	{
		return getBcfAccommodationDao().findAccommodation(accommodationCode);
	}

	/**
	 * @return the bcfAccommodationDao
	 */
	protected BcfAccommodationDao getBcfAccommodationDao()
	{
		return bcfAccommodationDao;
	}

	/**
	 * @param bcfAccommodationDao the bcfAccommodationDao to set
	 */
	@Required
	public void setBcfAccommodationDao(final BcfAccommodationDao bcfAccommodationDao)
	{
		this.bcfAccommodationDao = bcfAccommodationDao;
	}


	public BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}
}
