/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import java.util.Date;
import com.bcf.core.accommodation.dao.DateRangeDao;
import com.bcf.core.accommodation.service.DateRangeService;


public class DefaultDateRangeService implements DateRangeService
{
	private DateRangeDao dateRangeDao;

	@Override
	public DateRangeModel getDateRange(final Date startDate, final Date endDate)
	{
		return getDateRangeDao().findDateRangeModel(startDate, endDate);
	}

	/**
	 * @return the dateRangeDao
	 */
	public DateRangeDao getDateRangeDao()
	{
		return dateRangeDao;
	}

	/**
	 * @param dateRangeDao the dateRangeDao to set
	 */
	public void setDateRangeDao(final DateRangeDao dateRangeDao)
	{
		this.dateRangeDao = dateRangeDao;
	}

}
