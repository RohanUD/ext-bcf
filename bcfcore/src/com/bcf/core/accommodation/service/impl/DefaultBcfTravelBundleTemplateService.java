/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.travelservices.bundle.impl.DefaultTravelBundleTemplateService;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.model.travel.BundleTemplateTransportOfferingMappingModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfTravelBundleTemplateDao;
import com.bcf.core.accommodation.dao.BundleSelectionCriteriaDao;
import com.bcf.core.accommodation.service.BcfTravelBundleTemplateService;


public class DefaultBcfTravelBundleTemplateService extends DefaultTravelBundleTemplateService
		implements BcfTravelBundleTemplateService
{
	private BcfTravelBundleTemplateDao bcfTravelBundleTemplateDao;
	private BundleSelectionCriteriaDao bundleSelectionCriteriaDao;

	@Override
	public BundleTemplateModel getBundleTemplate(final BundleType bundleType, final String productType,
			final CatalogVersionModel catalogVersion)
	{
		final List<BundleTemplateModel> bundleTemplates = getBcfTravelBundleTemplateDao()
				.findBundleTemplateByType(bundleType, catalogVersion);
		return getBundleWithProductType(bundleTemplates, productType);
	}

	/**
	 * Gets the bundle with product type.
	 *
	 * @param bundleTemplates the bundle templates
	 * @param productType
	 * @return the bundle with product type
	 */
	protected BundleTemplateModel getBundleWithProductType(final List<BundleTemplateModel> bundleTemplates,
			final String productType)
	{
		if (CollectionUtils.isEmpty(bundleTemplates))
		{
			return null;
		}
		final List<BundleTemplateModel> filteredBundleTemplates = bundleTemplates.stream()
				.filter(bundleTemplate -> (CollectionUtils.isNotEmpty(bundleTemplate.getProducts()) && bundleTemplate.getProducts()
						.stream().anyMatch(product -> Objects.equals(productType, product.getItemtype()))))
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(filteredBundleTemplates))
		{
			return filteredBundleTemplates.stream().findFirst().orElse(null);
		}
		final List<BundleTemplateModel> childBundleTemplates = bundleTemplates.stream()
				.filter(bundleTemplate -> (CollectionUtils.isNotEmpty(bundleTemplate.getChildTemplates())))
				.flatMap(bundleTemplate -> bundleTemplate.getChildTemplates().stream()).collect(Collectors.toList());

		return getBundleWithProductType(childBundleTemplates, productType);
	}

	@Override
	public BundleSelectionCriteriaModel getBundleSelectionCriteria(final String id)
	{
		return getBundleSelectionCriteriaDao().findBundleSelectionCriteria(id);
	}

	@Override
	public BundleTemplateTransportOfferingMappingModel getBundleTemplateTransportOfferingMappingById(final String mappingCode,
			final CatalogVersionModel catalogVersion)
	{
		return getBcfTravelBundleTemplateDao().findBundleTemplateTransportOfferingMapping(mappingCode, catalogVersion);
	}

	@Override
	public BundleTemplateModel getBundleTemplateById(final String bundleId, final CatalogVersionModel catalogVersion)
	{
		return getBcfTravelBundleTemplateDao().findBundleTemplateById(bundleId, catalogVersion);
	}

	public List<TransportOfferingModel> getRouteBundleTemplateTransportOfferings(String id){
		return getBcfTravelBundleTemplateDao().findTransportOfferingsForRouteBundleTemplate(id);
	}

	/**
	 * @return the bcfTravelBundleTemplateDao
	 */
	protected BcfTravelBundleTemplateDao getBcfTravelBundleTemplateDao()
	{
		return bcfTravelBundleTemplateDao;
	}

	/**
	 * @param bcfTravelBundleTemplateDao the bcfTravelBundleTemplateDao to set
	 */
	@Required
	public void setBcfTravelBundleTemplateDao(final BcfTravelBundleTemplateDao bcfTravelBundleTemplateDao)
	{
		this.bcfTravelBundleTemplateDao = bcfTravelBundleTemplateDao;
	}

	/**
	 * @return the bundleSelectionCriteriaDao
	 */
	protected BundleSelectionCriteriaDao getBundleSelectionCriteriaDao()
	{
		return bundleSelectionCriteriaDao;
	}

	/**
	 * @param bundleSelectionCriteriaDao the bundleSelectionCriteriaDao to set
	 */
	@Required
	public void setBundleSelectionCriteriaDao(final BundleSelectionCriteriaDao bundleSelectionCriteriaDao)
	{
		this.bundleSelectionCriteriaDao = bundleSelectionCriteriaDao;
	}

}
