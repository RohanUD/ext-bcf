/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.AccommodationCancellationPolicyDao;
import com.bcf.core.accommodation.service.AccommodationCancellationPolicyService;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public class DefaultAccommodationCancellationPolicyService implements AccommodationCancellationPolicyService
{
	private AccommodationCancellationPolicyDao accommodationCancellationPolicyDao;

	@Override
	public AccommodationCancellationPolicyModel getAccommodationCancellationPolicy(
			final String accommodationCancellationPolicyCode,final CatalogVersionModel catalogVersion)
	{
		return getAccommodationCancellationPolicyDao().findAccommodationCancellationPolicy(accommodationCancellationPolicyCode,catalogVersion);
	}

	@Override
	public List<AccommodationCancellationPolicyModel> findAccommodationNonRefundableChangeAndCancellationPolicy(
			Date firstTravelDate,   List<RatePlanModel> ratePlans){

		return getAccommodationCancellationPolicyDao().findAccommodationNonRefundableChangeAndCancellationPolicy(firstTravelDate,ratePlans);

	}

	protected AccommodationCancellationPolicyDao getAccommodationCancellationPolicyDao()
	{
		return accommodationCancellationPolicyDao;
	}

	@Required
	public void setAccommodationCancellationPolicyDao(
			final AccommodationCancellationPolicyDao accommodationCancellationPolicyDao)
	{
		this.accommodationCancellationPolicyDao = accommodationCancellationPolicyDao;
	}
}
