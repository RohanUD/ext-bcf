/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfCommentDao;
import com.bcf.core.accommodation.service.BcfCommentService;
import com.bcf.core.model.accommodation.BcfCommentModel;


public class DefaultBcfCommentService implements BcfCommentService
{
	private BcfCommentDao bcfCommentDao;

	@Override
	public BcfCommentModel getBcfComment(final String code)
	{
		return getBcfCommentDao().findBcfComment(code);
	}

	/**
	 * @return the bcfCommentDao
	 */
	protected BcfCommentDao getBcfCommentDao()
	{
		return bcfCommentDao;
	}

	/**
	 * @param bcfCommentDao
	 *           the bcfCommentDao to set
	 */
	@Required
	public void setBcfCommentDao(final BcfCommentDao bcfCommentDao)
	{
		this.bcfCommentDao = bcfCommentDao;
	}

}
