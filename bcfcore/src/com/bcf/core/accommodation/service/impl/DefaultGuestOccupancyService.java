/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.GuestOccupancyDao;
import com.bcf.core.accommodation.service.GuestOccupancyService;


public class DefaultGuestOccupancyService implements GuestOccupancyService
{
	private GuestOccupancyDao guestOccupancyDao;

	@Override
	public List<GuestOccupancyModel> getGuestOccupancies(final Collection<String> guestOccupancies)
	{
		if (CollectionUtils.isEmpty(guestOccupancies))
		{
			return Collections.emptyList();
		}
		return guestOccupancies.stream()
				.map(guestOccupancy -> getGuestOccupancyDao().findGuestOccupancy(guestOccupancy)).collect(Collectors.toList());
	}

	@Override
	public List<GuestOccupancyModel> getGuestOccupancies(final String passengerType, final Integer quantityMax,
			final Integer quantityMin)
	{
		return getGuestOccupancyDao().findGuestOccupancy(passengerType, quantityMax, quantityMin);
	}


	/**
	 * @return the guestOccupancyDao
	 */
	protected GuestOccupancyDao getGuestOccupancyDao()
	{
		return guestOccupancyDao;
	}

	/**
	 * @param guestOccupancyDao the guestOccupancyDao to set
	 */
	@Required
	public void setGuestOccupancyDao(final GuestOccupancyDao guestOccupancyDao)
	{
		this.guestOccupancyDao = guestOccupancyDao;
	}

}
