/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.ordersplitting.model.VendorModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.VendorDao;
import com.bcf.core.accommodation.service.VendorService;


public class DefaultVendorService implements VendorService
{
	private VendorDao vendorDao;

	@Override
	public VendorModel getVendor(final String code)
	{
		return getVendorDao().findVendorModel(code);
	}

	/**
	 * @return the vendorDao
	 */
	protected VendorDao getVendorDao()
	{
		return vendorDao;
	}

	/**
	 * @param vendorDao the vendorDao to set
	 */
	@Required
	public void setVendorDao(final VendorDao vendorDao)
	{
		this.vendorDao = vendorDao;
	}

}
