/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.enums.RoomType;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.RoomTypeMappingDao;
import com.bcf.core.accommodation.service.RoomTypeMappingService;
import com.bcf.core.model.accommodation.GuestOccupancyCountRoomTypeMappingModel;


public class DefaultRoomTypeMappingService implements RoomTypeMappingService
{
	private RoomTypeMappingDao roomTypeMappingDao;

	@Override
	public RoomType getRoomType(final Integer guestOccupancyCount)
	{
		final GuestOccupancyCountRoomTypeMappingModel guestOccupancyCountRoomTypeMapping = getRoomTypeMappingDao()
				.findRoomTypeByCount(guestOccupancyCount);
		if (Objects.nonNull(guestOccupancyCountRoomTypeMapping))
		{
			return guestOccupancyCountRoomTypeMapping.getRoomType();
		}
		return null;
	}

	/**
	 * @return the roomTypeMappingDao
	 */
	protected RoomTypeMappingDao getRoomTypeMappingDao()
	{
		return roomTypeMappingDao;
	}

	/**
	 * @param roomTypeMappingDao the roomTypeMappingDao to set
	 */
	@Required
	public void setRoomTypeMappingDao(final RoomTypeMappingDao roomTypeMappingDao)
	{
		this.roomTypeMappingDao = roomTypeMappingDao;
	}

}
