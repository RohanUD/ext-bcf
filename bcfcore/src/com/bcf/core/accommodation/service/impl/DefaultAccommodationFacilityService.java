/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.enums.AccommodationFacilityType;
import de.hybris.platform.travelservices.model.facility.AccommodationFacilityModel;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.AccommodationFacilityDao;
import com.bcf.core.accommodation.service.AccommodationFacilityService;


public class DefaultAccommodationFacilityService implements AccommodationFacilityService
{
	private AccommodationFacilityDao accommodationFacilityDao;

	@Override
	public Collection<AccommodationFacilityModel> getAccommodationFacilities(
			final Collection<AccommodationFacilityType> facilities)
	{
		if (CollectionUtils.isEmpty(facilities))
		{
			return Collections.emptyList();
		}
		return facilities.stream()
				.map(facility -> getAccommodationFacilityDao().findAccommodationFacility(facility))
				.collect(Collectors.toList());
	}

	/**
	 * @return the accommodationFacilityDao
	 */
	protected AccommodationFacilityDao getAccommodationFacilityDao()
	{
		return accommodationFacilityDao;
	}

	/**
	 * @param accommodationFacilityDao the accommodationFacilityDao to set
	 */
	@Required
	public void setAccommodationFacilityDao(final AccommodationFacilityDao accommodationFacilityDao)
	{
		this.accommodationFacilityDao = accommodationFacilityDao;
	}

}
