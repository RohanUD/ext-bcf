/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.model.accommodation.AccommodationProviderModel;
import com.bcf.core.accommodation.dao.AccommodationProviderDao;
import com.bcf.core.accommodation.service.AccommodationProviderService;


public class DefaultAccommodationProviderService implements AccommodationProviderService
{
	private AccommodationProviderDao accommodationProviderDao;

	@Override
	public AccommodationProviderModel getAccommodationProvider(final String providerName)
	{
		return getAccommodationProviderDao().findAccommodationProvider(providerName);
	}

	/**
	 * Gets the accommodation provider dao.
	 *
	 * @return the accommodation provider dao
	 */
	protected AccommodationProviderDao getAccommodationProviderDao()
	{
		return accommodationProviderDao;
	}

	/**
	 * Sets the accommodation provider dao.
	 *
	 * @param accommodationProviderDao the new accommodation provider dao
	 */
	public void setAccommodationProviderDao(final AccommodationProviderDao accommodationProviderDao)
	{
		this.accommodationProviderDao = accommodationProviderDao;
	}
}
