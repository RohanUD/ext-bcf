/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.europe1.enums.ProductTaxGroup;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.ProductTaxGroupMappingDao;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.core.model.accommodation.ProductTaxGroupMappingModel;


public class DefaultProductTaxGroupMappingService implements ProductTaxGroupMappingService
{
	private ProductTaxGroupMappingDao productTaxGroupMappingDao;

	@Override
	public ProductTaxGroup getProductTaxGroupMapping(final Boolean applyDMF, final Boolean applyMRDT, final Boolean applyGST,
			final Boolean applyPST)
	{
		final ProductTaxGroupMappingModel productTaxGroupMapping = getProductTaxGroupMappingDao()
				.findProductTaxGroupMapping(applyDMF, applyMRDT, applyGST, applyPST);
		if (Objects.nonNull(productTaxGroupMapping))
		{
			return productTaxGroupMapping.getProductTaxGroup();
		}
		return null;
	}

	@Override
	public ProductTaxGroupMappingModel getProductTaxGroupMapping(final ProductTaxGroup productTaxGroup)
	{
		return getProductTaxGroupMappingDao().findProductTaxGroupMapping(productTaxGroup);
	}

	/**
	 * @return the productTaxGroupMappingDao
	 */
	protected ProductTaxGroupMappingDao getProductTaxGroupMappingDao()
	{
		return productTaxGroupMappingDao;
	}

	/**
	 * @param productTaxGroupMappingDao
	 *           the productTaxGroupMappingDao to set
	 */
	@Required
	public void setProductTaxGroupMappingDao(final ProductTaxGroupMappingDao productTaxGroupMappingDao)
	{
		this.productTaxGroupMappingDao = productTaxGroupMappingDao;
	}
}
