/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.model.PriceRowModel;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.PriceRowDao;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.util.BCFDateUtils;


public class DefaultPriceRowService implements PriceRowService
{
	PriceRowDao priceRowDao;

	@Override
	public List<PriceRowModel> getPriceRow(final ProductModel product)
	{
		return getPriceRowDao().findPriceRow(product);
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final Long minQtd)
	{
		return getPriceRowDao().findPriceRow(product, minQtd);
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final String passengerType, final Date startDate,
			final Date endDate)
	{
		return getPriceRowDao().findPriceRow(product, passengerType,BCFDateUtils.setToEndOfDay(startDate), BCFDateUtils.setToStartOfDay(endDate));
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final String passengerType, final Date startDate,
			final Date endDate,final List<DayOfWeek> daysOfWeek)
	{
		return getPriceRowDao().findPriceRow(product, passengerType, startDate, endDate, daysOfWeek);
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final String accommodationOfferingCode,
			final String passengerTypeCode, final Date startDate,
			final Date endDate)
	{
		return getPriceRowDao().findPriceRow(product, accommodationOfferingCode, passengerTypeCode, startDate, endDate);
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final String passengerType)
	{
		return getPriceRowDao().findPriceRow(product, passengerType);
	}

	@Override
	public PriceRowModel getPriceRow(final ProductModel product, final String passengerType, final Date commencementDate)
	{
		return getPriceRowDao().findPriceRow(product, passengerType, commencementDate);
	}

	/**
	 * @return the priceRowDao
	 */
	protected PriceRowDao getPriceRowDao()
	{
		return priceRowDao;
	}

	/**
	 * @param priceRowDao the priceRowDao to set
	 */
	@Required
	public void setPriceRowDao(final PriceRowDao priceRowDao)
	{
		this.priceRowDao = priceRowDao;
	}
}
