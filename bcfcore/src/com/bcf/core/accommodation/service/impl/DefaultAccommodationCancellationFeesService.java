/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.AccommodationCancellationFeesDao;
import com.bcf.core.accommodation.service.AccommodationCancellationFeesService;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;


public class DefaultAccommodationCancellationFeesService implements AccommodationCancellationFeesService
{
	private AccommodationCancellationFeesDao accommodationCancellationFeesDao;

	@Override
	public AccommodationCancellationFeesModel getAccommodationCancellationFees(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy, final Integer minDays, final Integer maxDays)
	{
		return getAccommodationCancellationFeesDao()
				.findAccommodationCancellationFees(accommodationCancellationPolicy, minDays, maxDays);
	}

	public List<AccommodationCancellationFeesModel> findAccommodationNonRefundableChangeAndCancellationFee(Date firstTravelDate, int noOfDays,  List<RatePlanModel> ratePlans){

		return getAccommodationCancellationFeesDao()
				.findAccommodationNonRefundableChangeAndCancellationFee(firstTravelDate, noOfDays, ratePlans);

	}


	protected AccommodationCancellationFeesDao getAccommodationCancellationFeesDao()
	{
		return accommodationCancellationFeesDao;
	}

	@Required
	public void setAccommodationCancellationFeesDao(
			final AccommodationCancellationFeesDao accommodationCancellationFeesDao)
	{
		this.accommodationCancellationFeesDao = accommodationCancellationFeesDao;
	}
}
