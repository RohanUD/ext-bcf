/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.impl.DefaultAccommodationOfferingService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfRoomRateProductDao;
import com.bcf.core.service.BcfTravelStockService;


public class DefaultBcfAccommodationOfferingService extends DefaultAccommodationOfferingService
		implements BcfAccommodationOfferingService
{
	private BcfAccommodationService bcfAccommodationService;
	private BcfTravelStockService bcfTravelStockService;
	private CatalogVersionService catalogVersionService;
	private BcfRoomRateProductDao bcfRoomRateProductDao;

	private static final Logger LOG = Logger.getLogger(DefaultBcfAccommodationOfferingService.class);

	@Override
	public Set<AccommodationOfferingModel> getAccommodationOfferingByProducts(final Set<String> roomRateProducts)
	{
		final Set<AccommodationOfferingModel> accommodationOfferings = new HashSet<>();
		if (CollectionUtils.isEmpty(getCatalogVersionService().getSessionCatalogVersions()))
		{
			final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG,
					CatalogManager.ONLINE_VERSION);
			getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		}

		final List<AccommodationModel> accommodations = getAccommodationsByRoomRateProducts(roomRateProducts);
		if (CollectionUtils.isEmpty(accommodations))
		{
			return accommodationOfferings;
		}

		for (final AccommodationModel accommodation : accommodations)
		{
			if(accommodation!=null)
			{
				final StockLevelModel stock = getBcfTravelStockService().fetchProductStockLevel(accommodation.getCode());
				if (Objects.nonNull(stock) && (stock.getWarehouse() instanceof AccommodationOfferingModel))
				{
					accommodationOfferings.add((AccommodationOfferingModel) stock.getWarehouse());
				}

			}
		}

		return accommodationOfferings;
	}

	@Override
	public AccommodationModel getAccommodation(final RoomRateProductModel roomRateProduct)
	{
		if (Objects.isNull(roomRateProduct) || CollectionUtils.isEmpty(roomRateProduct.getSupercategories()))
		{
			return null;
		}
		final RatePlanModel ratePlan = (RatePlanModel) roomRateProduct.getSupercategories().stream()
				.filter(RatePlanModel.class::isInstance).findFirst().orElse(null);
		if (Objects.nonNull(ratePlan))
		{
			return ratePlan.getAccommodation().stream().findFirst().orElse(null);
		}
		return null;
	}

	@Override
	public List<AccommodationModel> getAccommodationsByRoomRateProducts(final Set<String> roomRateProducts)
	{
		final List<AccommodationModel> accommodations = new ArrayList<>();
		if (CollectionUtils.isEmpty(roomRateProducts)) {
			return accommodations;
		}
		for (final String roomRateProduct : roomRateProducts)
		{
			final RoomRateProductModel roomRateProductModel = getBcfRoomRateProductDao().findRoomRateProductByCode(roomRateProduct);
			if (roomRateProductModel != null && CollectionUtils.isNotEmpty(roomRateProductModel.getSupercategories()))
			{
				final AccommodationModel accommodation = getAccommodation(roomRateProductModel);
				if (accommodation != null)
				{
					accommodations.add(accommodation);
				} else {
					LOG.error("No accommodation found for the roomRateProduct:" + roomRateProduct);
				}
			}
		}
		return accommodations;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected BcfTravelStockService getBcfTravelStockService()
	{
		return bcfTravelStockService;
	}

	@Required
	public void setBcfTravelStockService(final BcfTravelStockService bcfTravelStockService)
	{
		this.bcfTravelStockService = bcfTravelStockService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BcfRoomRateProductDao getBcfRoomRateProductDao()
	{
		return bcfRoomRateProductDao;
	}

	@Required
	public void setBcfRoomRateProductDao(final BcfRoomRateProductDao bcfRoomRateProductDao)
	{
		this.bcfRoomRateProductDao = bcfRoomRateProductDao;
	}
}
