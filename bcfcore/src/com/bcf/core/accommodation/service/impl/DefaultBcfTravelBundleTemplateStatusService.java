/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.services.impl.DefaultTravelBundleTemplateStatusService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfTravelBundleTemplateStatusDao;
import com.bcf.core.accommodation.service.BcfTravelBundleTemplateStatusService;


public class DefaultBcfTravelBundleTemplateStatusService extends DefaultTravelBundleTemplateStatusService
		implements BcfTravelBundleTemplateStatusService
{
	private BcfTravelBundleTemplateStatusDao bcfTravelBundleTemplateStatusDao;
	private SearchRestrictionService searchRestrictionService;
	private SessionService sessionService;

	@Override
	public BundleTemplateStatusModel getBundleTemplateStatusForId(final String statusId, final CatalogVersionModel catalogVersion)
	{
		return getSessionService().executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public BundleTemplateStatusModel execute()
			{
				getSearchRestrictionService().disableSearchRestrictions();
				return getBcfTravelBundleTemplateStatusDao().findBundleTemplateStatus(statusId, catalogVersion);
			}
		});
	}

	/**
	 * @return the bcfTravelBundleTemplateStatusDao
	 */
	protected BcfTravelBundleTemplateStatusDao getBcfTravelBundleTemplateStatusDao()
	{
		return bcfTravelBundleTemplateStatusDao;
	}

	/**
	 * @param bcfTravelBundleTemplateStatusDao the bcfTravelBundleTemplateStatusDao to set
	 */
	@Required
	public void setBcfTravelBundleTemplateStatusDao(final BcfTravelBundleTemplateStatusDao bcfTravelBundleTemplateStatusDao)
	{
		this.bcfTravelBundleTemplateStatusDao = bcfTravelBundleTemplateStatusDao;
	}

	public SearchRestrictionService getSearchRestrictionService()
	{
		return searchRestrictionService;
	}

	@Required
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
