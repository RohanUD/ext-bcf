/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service.impl;

import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.services.impl.DefaultGuestCountService;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.dao.BcfGuestCountDao;
import com.bcf.core.accommodation.service.BcfGuestCountService;


public class DefaultBcfGuestCountService extends DefaultGuestCountService implements BcfGuestCountService
{
	private BcfGuestCountDao bcfGuestCountDao;

	@Override
	public List<GuestCountModel> getGuestCounts(final Collection<String> guestCounts)
	{
		if (CollectionUtils.isEmpty(guestCounts))
		{
			return Collections.emptyList();
		}
		return guestCounts.stream().map(guestCount -> getBcfGuestCountDao().findGuestCount(guestCount))
				.collect(Collectors.toList());
	}

	/**
	 * @return the bcfGuestCountDao
	 */
	protected BcfGuestCountDao getBcfGuestCountDao()
	{
		return bcfGuestCountDao;
	}

	/**
	 * @param bcfGuestCountDao the bcfGuestCountDao to set
	 */
	@Required
	public void setBcfGuestCountDao(final BcfGuestCountDao bcfGuestCountDao)
	{
		this.bcfGuestCountDao = bcfGuestCountDao;
	}

}
