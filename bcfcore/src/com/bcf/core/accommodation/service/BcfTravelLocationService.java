/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.accommodation.service;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.services.TravelLocationService;
import java.util.List;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public interface BcfTravelLocationService extends TravelLocationService
{
	/**
	 * Gets the location codes in hierarchy by location types.
	 *
	 * @param location the location
	 * @return the location codes
	 */
	List<String> getLocationCodesInHierarchyByLocationTypes(LocationModel location, List<LocationType> locationTypes);


	/**
	 * Gets the location with location type.
	 *
	 * @param locations    the locations
	 * @param locationType the location type
	 * @return the locations with location type
	 */
	LocationModel getLocationWithLocationType(List<LocationModel> locations, LocationType locationType);

	LocationModel getOrCreateLocationFromVacationLocation(final BcfVacationLocationModel vacationLocationModel);

	/**
	 * Gets the all bcf vacation locations.
	 *
	 * @return the all bcf vacation locations
	 */
	List<BcfVacationLocationModel> getAllBcfVacationLocations();

	List<BcfVacationLocationModel> getVacationLocationsByType(String locationType);

	List<LocationModel> getSubLocations(LocationModel locationModel);

	BcfVacationLocationModel findBcfVacationLocationByCode(String code);

	BcfVacationLocationModel getBcfVacationLocationWithLocationType(List<BcfVacationLocationModel> locations,
			LocationType locationType);

	List<BcfVacationLocationModel> getBcfVacationSubLocations(BcfVacationLocationModel locationModel);

	List<BcfVacationLocationModel> getGeographicalAreasForFerry();

	BcfVacationLocationModel getBcfVacationLocationByCode(final String code);

	public BcfVacationLocationModel findBcfVacationLocationByOriginalLocation(final LocationModel location);

	SearchPageData<BcfVacationLocationModel> getPaginatedLocationsByType(String locationType, int pageSize,
			int currentPage);

	SearchPageData<BcfVacationLocationModel> getPaginatedCitiesByRegion(String regionCode, int pageSize,
			int currentPage);

	PointOfServiceModel getPointOfServiceForName(final BaseStoreModel baseStore, final String name);

	PointOfServiceModel getOrCreatePointOfServiceForName(final String name);
}
