/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.customer.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.List;
import java.util.Map;
import com.bcf.integration.data.BCF_Account;
import com.bcf.integration.data.BCF_Customer;
import com.bcf.integration.data.SubscriptionList;
import com.bcf.integration.data.SystemIdentifier;


public interface CustomerUpdateService
{

	/**
	 * Updates customer profile in Hybris DB
	 *  @param customerId  customer id of the customer need to be updated
	 * @param bcfCustomer customer data object received from CRM server
	 */
	CustomerModel updateCustomerProfile(String customerId, BCF_Customer bcfCustomer);

	/**
	 *
	 * @param customer
	 * @param systemIdentifiers
	 */
	void updateSystemIdentifiers(CustomerModel customer, List<SystemIdentifier> systemIdentifiers);

	/**
	 * Associate the System Identifiers to the Principal.
	 *
	 * @param principal
	 * @param systemIdentifiers
	 * @param systemIdentifiersMap
	 */
	void updateSystemIdentifiers(PrincipalModel principal, List<SystemIdentifier> systemIdentifiers, final Map<String, String> systemIdentifiersMap);

	/**
	 * Creates customer profile in Hybris DB
	 *
	 * @param customerId  customer id of the customer need to be updated
	 * @param bcf_customer customer data object received from CRM server
	 */
	CustomerModel createCustomerProfile(String customerId, BCF_Customer bcf_customer);

	/**
	 *
	 * @param customerModel
	 * @param subscriptionList
	 */
	void updateCustomerSubscriptions(CustomerModel customerModel, SubscriptionList subscriptionList);

	/**
	 * Update Customer Account Details
	 * @param response
	 * @param b2BUnitModel
	 * @return
	 */
	B2BUnitModel updateCustomerAccount(BCF_Account account, B2BUnitModel b2BUnitModel);
}
