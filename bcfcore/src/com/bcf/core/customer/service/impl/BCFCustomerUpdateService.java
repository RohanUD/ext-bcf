/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.customer.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.customer.TravelCustomerAccountService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.customer.service.CustomerUpdateService;
import com.bcf.core.dao.BcfB2BUnitDao;
import com.bcf.core.enums.CustomerStatus;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.core.model.account.AccountTaxonomyModel;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;
import com.bcf.core.util.BCFCreateCustomerUtil;
import com.bcf.integration.data.AccountTaxonomy;
import com.bcf.integration.data.Address;
import com.bcf.integration.data.AssociatedAccount;
import com.bcf.integration.data.BCF_Account;
import com.bcf.integration.data.BCF_Customer;
import com.bcf.integration.data.EmailAddress;
import com.bcf.integration.data.PhoneNumber;
import com.bcf.integration.data.Subscription;
import com.bcf.integration.data.SubscriptionList;
import com.bcf.integration.data.SystemIdentifier;


public class BCFCustomerUpdateService implements CustomerUpdateService
{
	private static final Logger LOG = Logger.getLogger(BCFCustomerUpdateService.class);

	private UserService userService;
	private ModelService modelService;
	private CustomerNameStrategy customerNameStrategy;
	private CommonI18NService commonI18NService;
	private TravelCustomerAccountService customerAccountService;
	private EnumerationService enumerationService;
	private BcfB2BUnitDao bcfB2BUnitDao;
	private static final String MAILING = "Mailing";
	private static final String HYPHEN = "-";
	private static final String BILLING = "Billing";

	@Resource(name = "subscriptionsMasterDetailsService")
	private BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfCreateCustomerUtil")
	private BCFCreateCustomerUtil bcfCreateCustomerUtil;

	@Override
	public CustomerModel updateCustomerProfile(final String userName, final BCF_Customer bcfCustomer)
	{
		CustomerModel customer = null;
		try
		{
			try
			{
				customer = getUserService().getUserForUID(userName, CustomerModel.class);
			}
			catch (final UnknownIdentifierException unEx)
			{
				LOG.info("Creating a new customer with the ID " + userName + " because the customer not found in Hybris.");
				customer = createCustomer(userName);
			}
			if (Objects.nonNull(bcfCustomer.getCustomerStatus()) && StringUtils
					.isNotEmpty(bcfCustomer.getCustomerStatus().getStatus()))
			{
				customer.setStatus(CustomerStatus.valueOf(bcfCustomer.getCustomerStatus().getStatus().toUpperCase()));
			}

			//			if(Objects.nonNull(bcfCustomer.getGuestInd())) {
			//				customer.setAccountType(bcfCustomer.getGuestInd() ? AccountType.SUBSCRIPTION_ONLY : AccountType.FULL_ACCOUNT);
			//			}

			if (Objects.nonNull(bcfCustomer.getWebEnabledInd()))
			{
				customer.setWebEnabled(bcfCustomer.getWebEnabledInd());
			}

			updateCustomerNames(customer, bcfCustomer);
			populateCustomerPhone(customer, bcfCustomer);
			updateCustomerAddress(customer, bcfCustomer);
			updateSystemIdentifiers(customer, bcfCustomer.getSystemIdentifier());
			createOrAssociateB2BUnitAccount(customer, bcfCustomer);
			getModelService().save(customer);
		}
		catch (final ModelSavingException e)
		{
			LOG.error(e);
			return customer;
		}

		return customer;
	}

	private CustomerModel createCustomer(final String userName)
	{
		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		newCustomer.setRegistrationSource(RegistrationSource.CRM);
		newCustomer.setName(userName);
		bcfCreateCustomerUtil.setUidForRegister(userName, newCustomer);

		final String webUserGroup = bcfConfigurablePropertiesService.getBcfPropertyValue("defaultWebUserGroup");
		newCustomer.setGroups(bcfCreateCustomerUtil.assignUserGroup(webUserGroup));

		try
		{
			getCustomerAccountService().register(newCustomer, "");
		}
		catch (final DuplicateUidException e)
		{
			LOG.error(e.getMessage(), e);
		}
		return newCustomer;
	}

	@Override
	public B2BUnitModel updateCustomerAccount(final BCF_Account account, final B2BUnitModel b2BUnitModel)
	{

		b2BUnitModel.setUid(account.getAccountName().getOrganizationName());
		b2BUnitModel.setName(account.getAccountName().getOrganizationName());
		b2BUnitModel.setLocName(account.getAccountName().getOrganizationName());
		updateSystemIdentifiers(b2BUnitModel, account.getSystemIdentifier(),
				b2BUnitModel.getSystemIdentifiers());

		updateCustomerAccountAddress(b2BUnitModel, account);

		if (account.getPhoneNumber().isEmpty())
		{
			b2BUnitModel.setPhoneType(null);
			b2BUnitModel.setPhoneNumber(null);
		}
		else
		{
			for (final PhoneNumber phoneNumber : account.getPhoneNumber())
			{
				if (phoneNumber.isPrimaryInd())
				{
					b2BUnitModel.setPhoneNumber(phoneNumber.getPhoneNumber());
					b2BUnitModel.setPhoneType(phoneNumber.getPhoneType());
				}
			}
		}

		b2BUnitModel.setPreferredCommunicationMethod(account.getPreferredCommunicationMethod());
		b2BUnitModel.setAgencyId(account.getAgencyId());

		if (null != account.getAccountTaxonomy())
		{
			final AccountTaxonomyModel accountTaxonomyModel;
			if (null != b2BUnitModel.getAccountTaxonomy())
			{
				accountTaxonomyModel = b2BUnitModel.getAccountTaxonomy();
			}
			else
			{
				accountTaxonomyModel = getModelService().create(AccountTaxonomyModel.class);
			}
			final AccountTaxonomy accountTaxonomy = account.getAccountTaxonomy();
			accountTaxonomyModel.setAccountGroup(accountTaxonomy.getAccountGroup());
			accountTaxonomyModel.setAccountSinceDate(accountTaxonomy.getAccountSinceDate());
			accountTaxonomyModel.setAccountSubType(accountTaxonomy.getAccountSubType());
			accountTaxonomyModel.setAccountType(accountTaxonomy.getAccountType());
			accountTaxonomyModel.setAccountWebType(accountTaxonomy.getAccountWebType());
			accountTaxonomyModel.setDropTrailerInd(accountTaxonomy.getDropTrailerInd());
			accountTaxonomyModel.setPricingGroup(accountTaxonomy.getPricingGroup());
			b2BUnitModel.setAccountTaxonomy(accountTaxonomyModel);
		}
		else if (b2BUnitModel.getAccountTaxonomy() != null)
		{
			getModelService().removeAll(b2BUnitModel.getAccountTaxonomy());
		}

		if(Objects.nonNull(account.getEmailAddress())) {
			for (final EmailAddress emailAddress : account.getEmailAddress())
			{
				if (emailAddress.isPrimaryInd())
				{
					b2BUnitModel.setEmail(emailAddress.getEmailAddress());
				}
			}
		}
		modelService.saveAll();
		return b2BUnitModel;
	}

	/**
	 * Create or associate the Unit and account.
	 *
	 * @param customer
	 * @param bcfCustomer
	 */
	protected void createOrAssociateB2BUnitAccount(final CustomerModel customer, final BCF_Customer bcfCustomer)
	{
		if (CollectionUtils.isNotEmpty(bcfCustomer.getAssociatedAccount()))
		{
			for (final AssociatedAccount associatedAccount : bcfCustomer.getAssociatedAccount())
			{
				if (associatedAccount.getAccountName() != null && associatedAccount.getAccountName().getOrganizationName() != null)
				{
					final B2BUnitModel unit = getBcfB2BUnitDao()
							.getB2BUnitByUid(associatedAccount.getAccountName().getOrganizationName());

					final Set<PrincipalGroupModel> groups = new HashSet<>();
					groups.addAll(customer.getGroups());
					if (unit != null)
					{
						// Use the existing unit.
						groups.add(unit);
						updateSystemIdentifiers(unit, associatedAccount.getSystemIdentifier(), unit.getSystemIdentifiers());
						getModelService().save(unit);
					}
					else
					{
						final B2BUnitModel defaultUnit = getBcfB2BUnitDao().getDefaultB2BUnit();
						final Set<PrincipalGroupModel> defaultUnitSet = new HashSet<>();
						defaultUnitSet.add(defaultUnit);

						// Add new Unit.
						final B2BUnitModel newUnit = getModelService().create(B2BUnitModel.class);
						newUnit.setUid(associatedAccount.getAccountName().getOrganizationName());
						newUnit.setName(associatedAccount.getAccountName().getOrganizationName());
						newUnit.setLocName(associatedAccount.getAccountName().getOrganizationName());
						newUnit.setGroups(defaultUnitSet);
						groups.add(newUnit);
						updateSystemIdentifiers(newUnit, associatedAccount.getSystemIdentifier(), newUnit.getSystemIdentifiers());
						getModelService().save(newUnit);
					}

					customer.setGroups(groups);
					getModelService().save(customer);
				}
			}
		}
	}

	@Override
	public void updateSystemIdentifiers(final PrincipalModel principal, final List<SystemIdentifier> systemIdentifiers,
			final Map<String, String> unitSystemIdentifiersMap)
	{
		if (CollectionUtils.isNotEmpty(systemIdentifiers))
		{
			final Map<String, String> systemIdentifiersMap = new HashMap<>();
			if (MapUtils.isNotEmpty(unitSystemIdentifiersMap))
			{
				systemIdentifiersMap.putAll(unitSystemIdentifiersMap);
			}
			for (final SystemIdentifier systemIdentifier : systemIdentifiers)
			{
				systemIdentifiersMap.put(systemIdentifier.getSourceSystem(), systemIdentifier.getID());
			}
			principal.setSystemIdentifiers(systemIdentifiersMap);
		}
	}

	@Override
	public void updateSystemIdentifiers(final CustomerModel customer, final List<SystemIdentifier> systemIdentifiers)
	{
		final Map<String, String> systemIdentifiersMap = new HashMap<>();
		updateSystemIdentifiers(customer, systemIdentifiers, systemIdentifiersMap);
	}

	@Override
	public CustomerModel createCustomerProfile(final String customerId, final BCF_Customer bcfCustomer)
	{
		final CustomerModel customerModel = getModelService().create(CustomerModel.class);
		final String firstName = bcfCustomer.getPersonName().getGivenName();
		final String lastName = bcfCustomer.getPersonName().getSurname();

		customerModel.setUid(customerId);
		customerModel.setName(getCustomerNameStrategy().getName(firstName, lastName));
		customerModel.setFirstName(firstName);
		customerModel.setLastName(lastName);
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setLoginDisabled(Boolean.TRUE);
		customerModel.setCustomerID(UUID.randomUUID().toString());
		customerModel.setRegistrationSource(RegistrationSource.CRM);

		final Map<String, String> systemIdentifiersMap = new HashMap<>();

		bcfCustomer.getSystemIdentifier().stream().forEach(systemIdentifier ->
			systemIdentifiersMap.put(systemIdentifier.getSourceSystem(), systemIdentifier.getID()));
		customerModel.setSystemIdentifiers(systemIdentifiersMap);

		populateCustomerPhone(customerModel, bcfCustomer);
		final Address primaryAdd = bcfCustomer.getAddress().stream().filter(e -> e.isPrimaryInd()).findFirst().orElse(null);
		if (null != primaryAdd)
		{
			populateAddressFields(primaryAdd, customerModel, null);
		}
		getModelService().saveAll();
		return customerModel;
	}

	@Override
	public void updateCustomerSubscriptions(final CustomerModel customerModel, final SubscriptionList subscriptionList)
	{
		final List<CustomerSubscriptionModel> customerModelSubscriptionList = new ArrayList<>(customerModel.getSubscriptionList());
		final List<CustomerSubscriptionModel> upatedCustomerModelSubscriptionList = new ArrayList<>();
		subscriptionList.getSubscription().forEach(subscription -> {
			try
			{
				CustomerSubscriptionModel customerSubscription = this
						.getCustomerSubscription(subscription.getType(), customerModelSubscriptionList);

				if (Objects.isNull(customerSubscription))
				{
					customerSubscription = getModelService().create(CustomerSubscriptionModel.class);
				}
				this.updateCustomerSubscription(customerSubscription, subscription);
				getModelService().save(customerSubscription);
				upatedCustomerModelSubscriptionList.add(customerSubscription);
			}
			catch (final UnknownIdentifierException ukEx)
			{
				LOG.error("Could not find the CustomerSubscriptionType for the code " + subscription.getType(), ukEx);
			}

		});
		customerModel.setSubscriptionList(upatedCustomerModelSubscriptionList);
		getModelService().save(customerModel);
	}

	private void updateCustomerSubscription(final CustomerSubscriptionModel customerSubscription, final Subscription subscription)
	{
		final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel = subscriptionsMasterDetailsService
				.getSubscriptionDetailForSubscriptionCode(subscription.getCode());
		customerSubscription.setSubscriptionMasterData(crmSubscriptionMasterDetailModel);
		if (Objects.nonNull(subscription.getOptInDetail()) && StringUtils.isNotEmpty(subscription.getOptInDetail().getDateTime()))
		{
			customerSubscription.setIsSubscribed(Boolean.TRUE);
			customerSubscription.setSource(subscription.getOptInDetail().getSource());
			customerSubscription
					.setSubscribedDate(TravelDateUtils.convertStringDateToDate(subscription.getOptInDetail().getDateTime(),
							BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
		}

		if (Objects.nonNull(subscription.getOptOutDetail()) && StringUtils
				.isNotEmpty(subscription.getOptOutDetail().getDateTime()))
		{
			customerSubscription.setIsSubscribed(Boolean.FALSE);
			customerSubscription.setSource(subscription.getOptOutDetail().getSource());
			customerSubscription
					.setUnSubscribedDate(TravelDateUtils.convertStringDateToDate(subscription.getOptOutDetail().getDateTime(),
							BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
		}
	}

	private CustomerSubscriptionType getSubscriptionCode(final String type)
	{
		final String typeCode = StringUtils.replace(type, " ", "_").toUpperCase();
		return getEnumerationService().getEnumerationValue(CustomerSubscriptionType.class, typeCode);
	}

	private CustomerSubscriptionModel getCustomerSubscription(final String subscriptionCode,
			final List<CustomerSubscriptionModel> customerModelSubscriptionList)
	{
		final CustomerSubscriptionType customerSubscriptionType = this.getSubscriptionCode(subscriptionCode);
		for (final CustomerSubscriptionModel customerSubscriptionModel : customerModelSubscriptionList)
		{
			if (customerSubscriptionType.equals(customerSubscriptionModel.getSubscriptionMasterData().getSubscriptionCode()))
			{
				return customerSubscriptionModel;
			}
		}
		return null;
	}

	private void updateCustomerAddress(final CustomerModel customerModel, final BCF_Customer bcfCustomer)
	{
		// Checking addresses in response, if no address found, delete all addresses in hybris.
		if (bcfCustomer.getAddress().isEmpty())
		{
			getModelService().removeAll(customerModel.getAddresses());
			return;
		}

		// Checking addresses in response, if no address is marked as primary indicator, delete all addresses in hybris.
		final List<Address> bcfAddresses = bcfCustomer.getAddress();
		final Address crmAddress = bcfAddresses.stream().filter(Address::isPrimaryInd).findFirst().orElse(null);
		if (Objects.isNull(crmAddress))
		{
			getModelService().removeAll(customerModel.getAddresses());
			return;
		}

		// If customer has no address create a new one
		final Collection<AddressModel> customerAddresses = customerModel.getAddresses();
		if (customerAddresses.isEmpty())
		{
			createNewAddress(customerModel, crmAddress);
			return;
		}

		//Find an existing address which is marked as primary indicator, if finds one update otherwise create new one
		final AddressModel customerAddressModel = customerAddresses.stream().filter(AddressModel::getPrimaryInd).findFirst()
				.orElse(null);
		if (Objects.isNull(customerAddressModel))
		{
			createNewAddress(customerModel, crmAddress);
			return;
		}

		final AddressModel addressModel = populateAddressFields(crmAddress, customerModel, customerAddressModel);
		if (Objects.isNull(customerModel.getDefaultShipmentAddress()))
		{
			customerModel.setDefaultShipmentAddress(addressModel);
		}
		getModelService().save(addressModel);

	}

	private void createNewAddress(final CustomerModel customerModel, final Address crmAddress)
	{
		final AddressModel newAddress = populateAddressFields(crmAddress, customerModel, null);
		final List<AddressModel> addressModels = new ArrayList<>(customerModel.getAddresses());
		addressModels.add(newAddress);
		if (Objects.isNull(customerModel.getDefaultShipmentAddress()))
		{
			customerModel.setDefaultShipmentAddress(newAddress);
		}
		getModelService().saveAll(customerModel, newAddress);
	}

	private void updateCustomerAccountAddress(final B2BUnitModel b2BUnitModel, final BCF_Account bcfAccount)
	{
		if (bcfAccount.getAddress().isEmpty())
		{
			getModelService().removeAll(b2BUnitModel.getAddresses());
			return;
		}
		for (final Address crmAddress : bcfAccount.getAddress())
		{
			if (crmAddress.isPrimaryInd())
			{
				if (b2BUnitModel.getAddresses().isEmpty())
				{
					createNewAcountAddress(crmAddress, b2BUnitModel, null);
				}
				else
				{
					//Find an existing address which is marked as primary indicator, if finds one update otherwise create new one
					final AddressModel primaryAddressModel = b2BUnitModel.getAddresses().stream().filter(AddressModel::getPrimaryInd)
							.findFirst().orElse(null);

					if (Objects.nonNull(primaryAddressModel))
					{
						createNewAcountAddress(crmAddress, b2BUnitModel, primaryAddressModel);
					}
					else
					{
						createNewAcountAddress(crmAddress, b2BUnitModel, null);
					}
				}
			}
		}
		getModelService().saveAll();
	}

	private AddressModel populateAddressFields(final Address crmAddress, final CustomerModel customerModel,
			AddressModel addressModel)
	{
		if (null == addressModel)
		{
			addressModel = getModelService().create(AddressModel.class);
			addressModel.setOwner(customerModel);
		}
		addressModel.setLine1(crmAddress.getAddressLine1());
		addressModel.setPostalcode(crmAddress.getPostalCode());
		addressModel.setLine2(crmAddress.getAddressLine2());
		addressModel.setTown(crmAddress.getCity());
		addressModel.setPrimaryInd(crmAddress.isPrimaryInd());

		if (crmAddress.getCountryCode() != null)
		{
			final String isocode = crmAddress.getCountryCode();
			final CountryModel countryModel = getCommonI18NService().getCountry(isocode);
			addressModel.setCountry(countryModel);
		}

		if (crmAddress.getProvinceStateCode() != null && crmAddress.getCountryCode() != null)
		{
			final String isocode = crmAddress.getCountryCode() + HYPHEN + crmAddress.getProvinceStateCode();
			final RegionModel regionModel = getCommonI18NService()
					.getRegion(getCommonI18NService().getCountry(crmAddress.getCountryCode()), isocode);
			addressModel.setRegion(regionModel);
		}

		if (StringUtils.isEmpty(crmAddress.getAddressUse()) || StringUtils.equals(MAILING, crmAddress.getAddressUse()))
		{
			addressModel.setShippingAddress(Boolean.TRUE);
		}
		else if (StringUtils.equals(BILLING, crmAddress.getAddressUse()))
		{
			addressModel.setBillingAddress(Boolean.TRUE);
		}

		return addressModel;
	}

	private AddressModel createNewAcountAddress(final Address crmAddress, final B2BUnitModel b2BUnitModel,
			AddressModel addressModel)
	{
		if (null == addressModel)
		{
			addressModel = getModelService().create(AddressModel.class);
			addressModel.setOwner(b2BUnitModel);
			final List<AddressModel> addressModels = new ArrayList<>();
			addressModels.add(addressModel);
			b2BUnitModel.setAddresses(addressModels);
		}
		addressModel.setLine1(crmAddress.getAddressLine1());
		addressModel.setPostalcode(crmAddress.getPostalCode());
		addressModel.setLine2(crmAddress.getAddressLine2());
		addressModel.setTown(crmAddress.getCity());
		if (crmAddress.isPrimaryInd())
			addressModel.setPrimaryInd(crmAddress.isPrimaryInd());

		if (crmAddress.getCountryCode() != null)
		{
			final String isocode = crmAddress.getCountryCode();
			final CountryModel countryModel = getCommonI18NService().getCountry(isocode);
			addressModel.setCountry(countryModel);
		}

		if (crmAddress.getProvinceStateCode() != null && crmAddress.getCountryCode() != null)
		{
			final String isocode = crmAddress.getCountryCode() + HYPHEN + crmAddress.getProvinceStateCode();
			final RegionModel regionModel = getCommonI18NService()
					.getRegion(getCommonI18NService().getCountry(crmAddress.getCountryCode()), isocode);
			addressModel.setRegion(regionModel);
		}

		if (StringUtils.isEmpty(crmAddress.getAddressUse()) || StringUtils.equals(MAILING, crmAddress.getAddressUse()))
		{
			addressModel.setShippingAddress(Boolean.TRUE);
		}
		else if (StringUtils.equals(BILLING, crmAddress.getAddressUse()))
		{
			addressModel.setBillingAddress(Boolean.TRUE);
		}
		getModelService().save(addressModel);
		return addressModel;
	}

	private void updateCustomerNames(final CustomerModel customer, final BCF_Customer bcfCustomer)
	{
		boolean isChanged = false;
		if (!StringUtils.equals(customer.getFirstName(), bcfCustomer.getPersonName().getGivenName()))
		{
			customer.setFirstName(bcfCustomer.getPersonName().getGivenName());
			isChanged = true;
		}
		if (!StringUtils.equals(customer.getLastName(), bcfCustomer.getPersonName().getSurname()))
		{
			customer.setLastName(bcfCustomer.getPersonName().getSurname());
			isChanged = true;
		}
		if (isChanged)
		{
			customer.setName(getCustomerNameStrategy().getName(customer.getFirstName(), customer.getLastName()));
		}
	}

	private void populateCustomerPhone(final CustomerModel customerModel, final BCF_Customer bcfCustomer)
	{
		if (CollectionUtils.isEmpty(bcfCustomer.getPhoneNumber()))
		{
			return;
		}

		for (final PhoneNumber phoneNumber : bcfCustomer.getPhoneNumber())
		{
			if (phoneNumber.isPrimaryInd())
			{
				customerModel.setPhoneNo(phoneNumber.getPhoneNumber());
				customerModel.setPhoneType(phoneNumber.getPhoneType());
			}
		}
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected TravelCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final TravelCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	public BcfB2BUnitDao getBcfB2BUnitDao()
	{
		return bcfB2BUnitDao;
	}

	@Required
	public void setBcfB2BUnitDao(final BcfB2BUnitDao bcfB2BUnitDao)
	{
		this.bcfB2BUnitDao = bcfB2BUnitDao;
	}
}
