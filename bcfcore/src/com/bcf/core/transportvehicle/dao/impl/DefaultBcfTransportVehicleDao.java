/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.transportvehicle.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.transportvehicle.dao.BcfTransportVehicleDao;


public class DefaultBcfTransportVehicleDao extends DefaultGenericDao<TransportVehicleModel> implements BcfTransportVehicleDao
{
	public DefaultBcfTransportVehicleDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Method returns a TransportVehicleModel from the database
	 *
	 * @param code
	 * @return transportVehicleModel
	 */
	@Override
	public TransportVehicleModel findTransportVehicle(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap();
		params.put("code", code);
		final List<TransportVehicleModel> transportVehicleModel = this.find(params);
		return CollectionUtils.isNotEmpty(transportVehicleModel) ? transportVehicleModel.get(0) : null;
	}

}
