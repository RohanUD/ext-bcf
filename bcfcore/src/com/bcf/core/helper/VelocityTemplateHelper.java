/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.helper;

import java.util.Map;
import javax.annotation.Resource;


public class VelocityTemplateHelper
{
	@Resource(name = "eventTypeConsumerTypeMapping")
	private Map<String, String> eventTypeConsumerTypeMapping;

	@Resource(name = "consumerTypeTemplateTypeMapping")
	private Map<String, String> consumerTypeTemplateTypeMapping;

	public String getEmailTemplateId(final String eventType)
	{
		final String consumerType = eventTypeConsumerTypeMapping.get(eventType);
		return consumerTypeTemplateTypeMapping.get(consumerType);
	}
}
