/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 10/08/19, 12:50 PM
 */

package com.bcf.core.helper;

import static com.bcf.core.constants.BcfCoreConstants.AMEND_ORDER_PROCESS_NAME;
import static com.bcf.core.constants.BcfCoreConstants.ONREQUEST_INITIAL_ORDER_PROCESS_NAME;
import static com.bcf.core.constants.BcfCoreConstants.ORDER_PROCESS_NAME;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;


public class OrderBusinessProcessHelper
{
	private static final Logger LOG = Logger.getLogger(OrderBusinessProcessHelper.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	public String getBusinessProcessId(final OrderModel order)
	{
		//if on_request order and create order financial data is not send and order not verified by ASM
		if (AvailabilityStatus.ON_REQUEST.name().equals(order.getAvailabilityStatus()) && !order.isCreateOrderNotificationSent()
				&& (OrderStatus.PENDING_APPROVAL_FROM_MERCHANT.equals(
				order.getStatus())) || OrderStatus.REJECTED_BY_MERCHANT.equals(
				order.getStatus()))
		{
			return ONREQUEST_INITIAL_ORDER_PROCESS_NAME;
		}
		//if on_request order and create order notification is not send but order verified by ASM
		if (AvailabilityStatus.ON_REQUEST.name().equals(order.getAvailabilityStatus()) && !order.isCreateOrderNotificationSent()
				&& OrderStatus.APPROVED_BY_MERCHANT.equals(
				order.getStatus()))
		{
			return ORDER_PROCESS_NAME;
		}
		if (Objects.nonNull(order.getOriginalOrder()))
		{
			return AMEND_ORDER_PROCESS_NAME;
		}
		return ORDER_PROCESS_NAME;
	}

	public void startOrderProcess(final OrderModel order)
	{
		String businessProcessId = getBusinessProcessId(order);
		LOG.info(
				String.format("Initiating business process [%s] for order [%s], order status [%s]", businessProcessId,
						order.getCode(),
						order.getStatus().getCode()));
		final OrderProcessModel orderProcessModel = getBusinessProcessService()
				.createProcess(businessProcessId + "-" + order.getCode() + "-" + System.currentTimeMillis(),
						businessProcessId);

		orderProcessModel.setOrder(order);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}


	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
