/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solrfacetsearch.indexer.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.SolrConfig;
import de.hybris.platform.solrfacetsearch.config.SolrServerMode;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.impl.TravelIndexer;
import de.hybris.platform.solrfacetsearch.indexer.spi.Exporter;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.utils.RatePlanUtils;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;


public class BcfDateRangeSolrIndexer extends TravelIndexer
{
	private static final Logger LOG = Logger.getLogger(BcfDateRangeSolrIndexer.class);

	private SessionService sessionService;
	private ModelService modelService;
	private Map<String, String> dateRangeIndexedTypesMap;

	@Override
	public Collection<SolrInputDocument> indexItems(final Collection<ItemModel> items, final FacetSearchConfig facetSearchConfig,
			final IndexedType indexedType) throws
			IndexerException, InterruptedException
	{
		final String stringDate = BcfCoreConstants.SOLR_INDEXER_DATE;
		final LocalDate dateTime = LocalDate
				.parse(stringDate, DateTimeFormatter.ofPattern(BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN));
		final Date dummyDate = Date.from(dateTime.atStartOfDay(ZoneId.of("UTC")).toInstant());

		final DateRangeModel dummyDateRangeModel = getModelService().create(DateRangeModel.class);
		dummyDateRangeModel.setStartingDate(dummyDate);
		dummyDateRangeModel.setEndingDate(dummyDate);

		if (!getDateRangeIndexedTypesMap().containsKey(indexedType.getCode()))
		{
			return super.indexItems(items, facetSearchConfig, indexedType);
		}
		else if (items == null)
		{
			return Collections.emptyList();
		}

		final IndexConfig indexConfig = facetSearchConfig.getIndexConfig();
		final SolrConfig solrConfig = facetSearchConfig.getSolrConfig();
		final List documents = new ArrayList(CollectionUtils.size(items));

		for (final ItemModel item : items)
		{
			if (Thread.interrupted())
			{
				throw new InterruptedException();
			}
			final List<DateRangeModel> dateRangeModels = modelService
					.getAttributeValue(item, getDateRangeIndexedTypesMap().get(item.getItemtype()));

			if (CollectionUtils.isNotEmpty(dateRangeModels))
			{
				for (final DateRangeModel dateRange : dateRangeModels)
				{
					documents.add(createDocument(dateRange, item, indexConfig, indexedType));
				}
			}
			else
			{
				documents.add(createDocument(dummyDateRangeModel, item, indexConfig, indexedType));
			}

		}

		final SolrServerMode serverMode1 = solrConfig.getMode();
		final Exporter exporter1 = this.getExporter(serverMode1);
		exporter1.exportToUpdateIndex(documents, facetSearchConfig, indexedType);
		getSessionService().removeAttribute(BcfCoreConstants.SOLR_CURRENT_DATE_RANGE);
		return documents;

	}

	protected SolrInputDocument createDocument(final DateRangeModel dateRange, final ItemModel item, final IndexConfig indexConfig,
			final IndexedType indexedType) throws IndexerException
	{
		SolrInputDocument document = null;
		LOG.warn("No date range found for " + getDateRangeIndexedTypesMap().get(indexedType.getCode()) + " with PK: "
				+ item.getPk()
				+ "hence, setting the default date");
		try
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Indexing item with PK " + item.getPk());
			}
			getSessionService().setAttribute(BcfCoreConstants.SOLR_CURRENT_DATE_RANGE, dateRange);
			document = getSolrDocumentFactory().createInputDocument(item, indexConfig, indexedType);
		}
		catch (final RuntimeException | FieldValueProviderException err)
		{
			final String message = "Failed to index item with PK " + item.getPk() + ": " + err.getMessage();
			handleError(indexConfig, indexedType, message, err);
		}
		return document;
	}

	@Override
	protected boolean isRoomRateAvailableOnRatePlans(final Collection<RatePlanConfigModel> ratePlanConfigs, final Date currentDate)
	{
		return ratePlanConfigs.stream().anyMatch(
				ratePlanConfig -> Objects.nonNull(RatePlanUtils.getRoomRateForRatePlan(ratePlanConfig.getRatePlan(), currentDate)));
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected Map<String, String> getDateRangeIndexedTypesMap()
	{
		return dateRangeIndexedTypesMap;
	}

	@Required
	public void setDateRangeIndexedTypesMap(final Map<String, String> dateRangeIndexedTypesMap)
	{
		this.dateRangeIndexedTypesMap = dateRangeIndexedTypesMap;
	}
}
