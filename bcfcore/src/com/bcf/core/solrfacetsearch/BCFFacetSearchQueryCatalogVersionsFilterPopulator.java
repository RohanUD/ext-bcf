/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryCatalogVersionsFilterPopulator;
import org.apache.solr.client.solrj.SolrQuery;


public class BCFFacetSearchQueryCatalogVersionsFilterPopulator extends FacetSearchQueryCatalogVersionsFilterPopulator
{
	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target)
	{
		if (source.getSearchQuery().getIndexedType().getComposedType().getCatalogItemType() != null)
		{
			super.populate(source, target);
		}
	}
}
