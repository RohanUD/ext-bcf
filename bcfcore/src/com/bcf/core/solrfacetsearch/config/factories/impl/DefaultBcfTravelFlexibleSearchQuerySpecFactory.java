/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.solrfacetsearch.config.factories.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.IndexedTypeFlexibleSearchQuery;
import de.hybris.platform.solrfacetsearch.config.factories.impl.DefaultTravelFlexibleSearchQuerySpecFactory;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelBundleTemplateStatusService;


public class DefaultBcfTravelFlexibleSearchQuerySpecFactory extends DefaultTravelFlexibleSearchQuerySpecFactory
{
	private static final String SOLD_INDIVIDUALLY = "soldIndividually";
	private static final String STATUS = "status";
	private static final String DEFAULT_STATUS = "Approved_Status";
	private static final String DELETE_STATUS = "InActive_Status";
	private static final String PRODUCTCATALOG = "bcfProductCatalog";
	private static final String APPROVAL_STATUS = "approvalStatus";
	private static final String CATALOGVERSION = CatalogManager.ONLINE_VERSION;

	private BcfTravelBundleTemplateStatusService bcfTravelBundleTemplateStatusService;
	private CatalogVersionService catalogVersionService;

	@Override
	protected void populateRuntimeParameters(final IndexedTypeFlexibleSearchQuery indexTypeFlexibleSearchQueryData,
			final IndexedType indexedType, final FacetSearchConfig facetSearchConfig) throws SolrServiceException
	{
		super.populateRuntimeParameters(indexTypeFlexibleSearchQueryData, indexedType, facetSearchConfig);
		final Map<String, Object> parameters = indexTypeFlexibleSearchQueryData.getParameters();
		if (indexTypeFlexibleSearchQueryData.isActive())
		{
			parameters.put(SOLD_INDIVIDUALLY, Boolean.TRUE);
			final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);
			if (!indexTypeFlexibleSearchQueryData.getType().equals(IndexOperation.DELETE))
			{
				parameters.put(STATUS,
						getBcfTravelBundleTemplateStatusService().getBundleTemplateStatusForId(DEFAULT_STATUS, catalogVersion));
				parameters.put(APPROVAL_STATUS, ArticleApprovalStatus.APPROVED);
			}
			else
			{
				parameters.put(STATUS,
						getBcfTravelBundleTemplateStatusService().getBundleTemplateStatusForId(DELETE_STATUS, catalogVersion));
				parameters.put(APPROVAL_STATUS, ArticleApprovalStatus.UNAPPROVED);
			}
		}

	}

	/**
	 * @return the bcfTravelBundleTemplateStatusService
	 */
	protected BcfTravelBundleTemplateStatusService getBcfTravelBundleTemplateStatusService()
	{
		return bcfTravelBundleTemplateStatusService;
	}

	/**
	 * @param bcfTravelBundleTemplateStatusService the bcfTravelBundleTemplateStatusService to set
	 */
	@Required
	public void setBcfTravelBundleTemplateStatusService(
			final BcfTravelBundleTemplateStatusService bcfTravelBundleTemplateStatusService)
	{
		this.bcfTravelBundleTemplateStatusService = bcfTravelBundleTemplateStatusService;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

}
