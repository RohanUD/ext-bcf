/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.acceleratorservices.urlresolver.impl.DefaultContentPageUrlResolver;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.apache.commons.lang3.StringUtils;


public class BcfDefaultContentPageUrlResolver extends DefaultContentPageUrlResolver
{
	@Override
	protected String resolveInternal(final ContentPageModel source)
	{
		final String url = getPattern();
		final String label = source.getLabel();
		if (StringUtils.isEmpty(label))
		{
			return url;
		}
		return url + (label.startsWith("/") ? label.replaceFirst("/", "") : label);
	}
}
