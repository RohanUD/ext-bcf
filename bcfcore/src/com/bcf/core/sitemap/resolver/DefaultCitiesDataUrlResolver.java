/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for TerminalDetailsData instances.
 * The pattern could be of the form:
 * /terminal-details/{code}
 */
public class DefaultCitiesDataUrlResolver extends AbstractUrlResolver<BcfVacationLocationModel>
{
	private final String CACHE_KEY = DefaultCitiesDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final BcfVacationLocationModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final BcfVacationLocationModel source)
	{
		String url = getPattern();

		if (url.contains("{cityName}"))
		{
			url = url.replace("{cityName}", BcfTravelRouteUtil.translateName(source.getName()));
		}
		if (url.contains("{cityCode}"))
		{
			url = url.replace("{cityCode}", source.getCode());
		}

		return url;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
