/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for TerminalDetailsData instances.
 * The pattern could be of the form:
 * /terminal-details/{code}
 */
public class DefaultTerminalDataUrlResolver extends AbstractUrlResolver<TransportFacilityModel>
{
	private final String CACHE_KEY = DefaultTerminalDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final TransportFacilityModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final TransportFacilityModel source)
	{
		String url = getPattern();

		if (url.contains("{terminalName}"))
		{
			url = url.replace("{terminalName}", BcfTravelRouteUtil.translateName(source.getName()));
		}

		if (url.contains("{code}"))
		{
			url = url.replace("{code}", getTransportFacilityCode(source));
		}

		return url;
	}

	protected String getTransportFacilityCode(final TransportFacilityModel transportFacility)
	{
		return transportFacility.getCode();
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
