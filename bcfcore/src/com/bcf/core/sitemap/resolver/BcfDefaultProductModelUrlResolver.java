/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;


public class BcfDefaultProductModelUrlResolver extends DefaultProductModelUrlResolver
{
	@Override
	protected String resolveInternal(final ProductModel source)
	{
		String url = getPattern();
		if (url.contains("{product-code}"))
		{
			url = url.replace("{product-code}", urlEncode(source.getCode()));
		}
		return url;
	}
}
