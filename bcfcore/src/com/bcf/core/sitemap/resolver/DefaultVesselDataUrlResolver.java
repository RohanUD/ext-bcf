/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import java.util.Objects;
import org.apache.log4j.Logger;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for FerryDetailsData instances.
 * The pattern could be of the form:
 * /ferrydetails/{code}
 */
public class DefaultVesselDataUrlResolver extends AbstractUrlResolver<TransportVehicleModel>
{
	private static final Logger LOG = Logger.getLogger(DefaultVesselDataUrlResolver.class);

	private final String CACHE_KEY = DefaultVesselDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final TransportVehicleModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final TransportVehicleModel source)
	{
		String url = getPattern();

		final String vesselName = getVesselName(source);
		if (url.contains("{vesselName}") && Objects.nonNull(vesselName))
		{
			url = url.replace("{vesselName}", BcfTravelRouteUtil.translateName(vesselName));
		}

		final String vesselCode = getVesselCode(source);
		if (url.contains("{code}") && Objects.nonNull(vesselCode))
		{
			return url.replace("{code}", vesselCode);
		}

		return url;
	}

	private String getVesselName(final TransportVehicleModel transportVehicle)
	{
		if (Objects.nonNull(transportVehicle.getTransportVehicleInfo()) && Objects
				.nonNull(transportVehicle.getTransportVehicleInfo().getName()))
		{
			return transportVehicle.getTransportVehicleInfo().getName().replaceAll(" ", "-");
		}
		else
		{
			LOG.error("Transport Vehicle Info name is empty on Transport Vehicle " + transportVehicle.getCode());
		}
		return "-";
	}

	protected String getVesselCode(final TransportVehicleModel transportVehicle)
	{
		if (Objects.nonNull(transportVehicle.getTransportVehicleInfo()) && Objects
				.nonNull(transportVehicle.getTransportVehicleInfo().getCode()))
		{
			return transportVehicle.getTransportVehicleInfo().getCode();
		}
		else
		{
			LOG.error("Transport Vehicle Info code is empty on Transport Vehicle " + transportVehicle.getCode());
		}
		return "";
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
