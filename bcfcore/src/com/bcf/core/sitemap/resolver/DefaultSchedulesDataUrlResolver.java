/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for TerminalDetailsData instances.
 * The pattern could be of the form:
 * /terminal-details/{code}
 */
public class DefaultSchedulesDataUrlResolver extends AbstractUrlResolver<TravelRouteModel>
{
	private final String CACHE_KEY = DefaultSchedulesDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final TravelRouteModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final TravelRouteModel source)
	{
		String url = getPattern();

		if (url.contains("{routeName}"))
		{
			final String routeName = BcfTravelRouteUtil.translateName(source.getOrigin().getName()) + "-" + BcfTravelRouteUtil
					.translateName(source.getDestination().getName());
			url = url.replace("{routeName}", routeName);
		}

		if (url.contains("{routeCode}"))
		{
			url = url.replace("{routeCode}", source.getOrigin().getCode() + "-" + source.getDestination().getCode());
		}

		return url;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
