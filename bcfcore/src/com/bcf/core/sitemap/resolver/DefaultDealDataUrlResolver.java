/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.resolver;


import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for ProductData instances.
 * The pattern could be of the form:
 * /{category-path}/{product-name}/p/{product-code}
 */
public class DefaultDealDataUrlResolver extends AbstractUrlResolver<PromotionSourceRuleModel>
{
	private final String CACHE_KEY = DefaultVesselDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final PromotionSourceRuleModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final PromotionSourceRuleModel source)
	{
		String url = getPattern();

		if (url.contains("{dealName}"))
		{
			url = url.replace("{dealName}",
					StringUtils.isNotEmpty(source.getName()) ? BcfTravelRouteUtil.translateName(source.getName()) : "");
		}

		if (url.contains("{code}"))
		{
			url = url.replace("{code}", source.getCode());
		}

		return url;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
