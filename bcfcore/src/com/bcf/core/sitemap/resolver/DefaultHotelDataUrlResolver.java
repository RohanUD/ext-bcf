/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.resolver;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bcf.core.util.BcfTravelRouteUtil;


/**
 * URL resolver for TerminalDetailsData instances.
 * The pattern could be of the form:
 * /terminal-details/{code}
 */
public class DefaultHotelDataUrlResolver extends AbstractUrlResolver<AccommodationOfferingModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHotelDataUrlResolver.class);

	private final String CACHE_KEY = DefaultHotelDataUrlResolver.class.getName();

	private String defaultPattern;

	protected String getPattern()
	{
		return getDefaultPattern();
	}

	@Override
	protected String getKey(final AccommodationOfferingModel source)
	{
		return CACHE_KEY + "." + source.getPk().toString();
	}

	@Override
	protected String resolveInternal(final AccommodationOfferingModel source)
	{
		String url = getPattern();

		if (url.contains("{cityName}"))
		{
			if (Objects.nonNull(source.getLocation()) && CollectionUtils.isNotEmpty(source.getLocation().getSuperlocations()))
			{
				final List<LocationModel> superLocations = source.getLocation().getSuperlocations();
				for (final LocationModel locationModel : superLocations) {
					if(Objects.nonNull(locationModel.getName())) {
						url = url.replace("{cityName}", BcfTravelRouteUtil.translateName(locationModel.getName()));
						break;
					}
				}
			}
		}

		if (url.contains("{hotelName}"))
		{
			if (StringUtils.isNotEmpty(source.getName()))
			{
				url = url.replace("{hotelName}", BcfTravelRouteUtil.translateName(source.getName()));
			}
			else
			{
				url = url.replace("{hotelName}", " ");
				LOG.error("hotelName not found on the accommodation offering " + source.getCode());
			}
		}

		if (url.contains("{hotelCode}"))
		{
			url = url.replace("{hotelCode}", source.getCode());
		}

		return url;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}
}
