/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.core.sitemap.renderer;

import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext;
import de.hybris.platform.cms2.model.site.CMSSiteModel;


public class BcfSiteMapContext extends SiteMapContext
{
	@Override
	public void init(final CMSSiteModel site, final SiteMapPageEnum siteMapPageEnum)
	{
		super.init(site, siteMapPageEnum);

		final String currentUrlEncodingPattern = getUrlEncoderService().getCurrentUrlEncodingPattern();
		this.put(BASE_URL, getSiteBaseUrlResolutionService().getWebsiteUrlForSite(site, currentUrlEncodingPattern, true, ""));
		this.put(MEDIA_URL, getSiteBaseUrlResolutionService().getMediaUrlForSite(site, true, ""));
	}
}
