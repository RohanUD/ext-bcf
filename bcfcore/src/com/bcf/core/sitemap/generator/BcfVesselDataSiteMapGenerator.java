/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.generator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import java.util.List;


public class BcfVesselDataSiteMapGenerator extends BcfAbstractSiteMapGenerator<TransportVehicleModel>
{

	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<TransportVehicleModel> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	@Override
	protected List<TransportVehicleModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {tv.pk} FROM {TransportVehicle AS tv JOIN ShipInfo AS si ON {si.pk}={tv.transportVehicleInfo}} WHERE {si.shipDisplay}=1";

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
		final SearchResult<TransportVehicleModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}
