/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.generator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ContentPageModelSiteMapGenerator;
import de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.FlexibleSearchUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;


public class BcfContentPageModelSiteMapGenerator extends ContentPageModelSiteMapGenerator
{
	private ApplicationContext applicationContext;

	@Override
	public List<ContentPageModel> getData(final CMSSiteModel site)
	{
		final ImpersonationContext context = new ImpersonationContext();
		context.setSite(site);

		return getImpersonationService().executeInContext(context, () -> getDataInternal(site));
	}

	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(ContentPageModel.INDEXABLE, Boolean.TRUE);

		final Collection<CatalogVersionModel> sessionCatalogVersions = getCatalogVersionService().getSessionCatalogVersions();
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT {cp." + ContentPageModel.PK + "} FROM {" + ContentPageModel._TYPECODE
				+ " AS cp} WHERE ");
		queryBuilder
				.append(FlexibleSearchUtils.buildOracleCompatibleCollectionStatement("{cp." + ContentPageModel.CATALOGVERSION
								+ "} in (?catalogVersions)", "catalogVersions", "OR",
						sessionCatalogVersions,
						params));
		queryBuilder.append(" AND {cp." + ContentPageModel.INDEXABLE + "}  = ?indexable");
		return doSearch(queryBuilder.toString(), params, ContentPageModel.class);
	}

	@Override
	public File render(final CMSSiteModel site, final CurrencyModel currencyModel, final LanguageModel languageModel,
			final RendererTemplateModel rendererTemplateModel, final List models, final String filePrefix, final Integer index)
			throws IOException
	{
		final String prefix = (index != null) ? String.format(filePrefix + "-%s-", index) : filePrefix + "-";

		final String dirPathString = Config.getString("HYBRIS_DATA_DIR", "").concat("/media").concat("/sys_master").concat("/sitemap");
		final File dirPath = new File(dirPathString);
		if(!dirPath.exists()) {
			dirPath.mkdir();
		}

		final File siteMap = File.createTempFile(prefix, ".xml", dirPath);

		final ImpersonationContext context = new ImpersonationContext();
		context.setSite(site);
		context.setCurrency(currencyModel);
		context.setLanguage(languageModel);

		return getImpersonationService().executeInContext(context, () -> {
			final List<SiteMapUrlData> siteMapUrlDataList = getSiteMapUrlData(models);
			final SiteMapContext siteMapContext = (SiteMapContext) applicationContext.getBean("siteMapContext");
			siteMapContext.init(site, getSiteMapPageEnum());
			siteMapContext.setSiteMapUrlData(siteMapUrlDataList);
			final BufferedWriter output = new BufferedWriter(new FileWriter(siteMap));
			try
			{
				// the template media is loaded only for english language.
				getCommonI18NService().setCurrentLanguage(getCommonI18NService().getLanguage("en"));
				getRendererService().render(rendererTemplateModel, siteMapContext, output);
			}
			finally
			{
				IOUtils.closeQuietly(output);
			}

			return siteMap;
		});
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}
}
