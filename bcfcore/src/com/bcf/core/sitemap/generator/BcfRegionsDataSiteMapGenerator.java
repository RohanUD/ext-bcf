/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.generator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.List;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class BcfRegionsDataSiteMapGenerator extends BcfAbstractSiteMapGenerator<BcfVacationLocationModel>
{

	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<BcfVacationLocationModel> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	@Override
	protected List<BcfVacationLocationModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {pk} FROM {BcfVacationLocation AS loc JOIN LocationType as lt ON {lt.pk}={loc.locationType}} WHERE {lt.code}='GEOGRAPHICAL_AREA'";

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
		final SearchResult<BcfVacationLocationModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}
