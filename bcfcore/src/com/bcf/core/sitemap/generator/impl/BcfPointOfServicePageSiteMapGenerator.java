/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.PointOfServicePageSiteMapGenerator;
import de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.util.Config;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;


public class BcfPointOfServicePageSiteMapGenerator extends PointOfServicePageSiteMapGenerator
{
	private ApplicationContext applicationContext;

	@Override
	public File render(final CMSSiteModel site, final CurrencyModel currencyModel, final LanguageModel languageModel,
			final RendererTemplateModel rendererTemplateModel, final List models, final String filePrefix, final Integer index)
			throws IOException
	{

		final String prefix = (index != null) ? String.format(filePrefix + "-%s-", index) : filePrefix + "-";

		final String dirPathString = Config.getString("HYBRIS_DATA_DIR", "").concat("/media").concat("/sys_master").concat("/sitemap");
		final File dirPath = new File(dirPathString);
		if(!dirPath.exists()) {
			dirPath.mkdir();
		}

		final File siteMap = File.createTempFile(prefix, ".xml", dirPath);

		final ImpersonationContext context = new ImpersonationContext();
		context.setSite(site);
		context.setCurrency(currencyModel);
		context.setLanguage(languageModel);

		return getImpersonationService().executeInContext(context, () -> {
			final List<SiteMapUrlData> siteMapUrlDataList = getSiteMapUrlData(models);
			final SiteMapContext siteMapContext = (SiteMapContext) applicationContext.getBean("siteMapContext");
			siteMapContext.init(site, getSiteMapPageEnum());
			siteMapContext.setSiteMapUrlData(siteMapUrlDataList);
			final BufferedWriter output = new BufferedWriter(new FileWriter(siteMap));
			try
			{
				// the template media is loaded only for english language.
				getCommonI18NService().setCurrentLanguage(getCommonI18NService().getLanguage("en"));
				getRendererService().render(rendererTemplateModel, siteMapContext, output);
			}
			finally
			{
				IOUtils.closeQuietly(output);
			}

			return siteMap;
		});
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}
}
