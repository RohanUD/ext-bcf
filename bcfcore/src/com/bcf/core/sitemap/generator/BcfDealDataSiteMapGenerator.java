/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.generator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.List;


public class BcfDealDataSiteMapGenerator extends BcfAbstractSiteMapGenerator<PromotionSourceRuleModel>
{

	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<PromotionSourceRuleModel> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	@Override
	protected List<PromotionSourceRuleModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {pk} FROM {PromotionSourceRule} where {DealPromotion} = 1";

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
		final SearchResult<PromotionSourceRuleModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}
