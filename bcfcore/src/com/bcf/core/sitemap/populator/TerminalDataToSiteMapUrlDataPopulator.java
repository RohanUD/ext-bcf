/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.populator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import org.apache.commons.lang.StringEscapeUtils;


public class TerminalDataToSiteMapUrlDataPopulator implements Populator<TransportFacilityModel, SiteMapUrlData>
{
	private UrlResolver<TransportFacilityModel> urlResolver;

	@Override
	public void populate(final TransportFacilityModel model, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		final String relUrl = StringEscapeUtils.escapeXml(getUrlResolver().resolve(model));
		siteMapUrlData.setLoc(relUrl);
	}

	public UrlResolver<TransportFacilityModel> getUrlResolver()
	{
		return urlResolver;
	}

	public void setUrlResolver(
			final UrlResolver<TransportFacilityModel> urlResolver)
	{
		this.urlResolver = urlResolver;
	}
}
