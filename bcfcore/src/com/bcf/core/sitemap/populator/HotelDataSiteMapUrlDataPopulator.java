/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.core.sitemap.populator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import javax.annotation.Resource;
import org.apache.commons.lang.StringEscapeUtils;


public class HotelDataSiteMapUrlDataPopulator implements Populator<AccommodationOfferingModel, SiteMapUrlData>
{
	@Resource(name = "hotelDataUrlResolver")
	private UrlResolver<AccommodationOfferingModel> urlResolver;

	@Override
	public void populate(final AccommodationOfferingModel accommodationOfferingModel, final SiteMapUrlData siteMapUrlData)
			throws ConversionException
	{
		final String relUrl = StringEscapeUtils.escapeXml(urlResolver.resolve(accommodationOfferingModel));
		siteMapUrlData.setLoc(relUrl);
	}
}
