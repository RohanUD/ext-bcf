/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.sitemap.populator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import javax.annotation.Resource;
import org.apache.commons.lang.StringEscapeUtils;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class CitiesSiteMapUrlDataPopulator implements Populator<BcfVacationLocationModel, SiteMapUrlData>
{
	@Resource(name = "citiesDataUrlResolver")
	private UrlResolver<BcfVacationLocationModel> urlResolver;

	@Override
	public void populate(final BcfVacationLocationModel locationModel, final SiteMapUrlData siteMapUrlData)
			throws ConversionException
	{
		final String relUrl = StringEscapeUtils.escapeXml(urlResolver.resolve(locationModel));
		siteMapUrlData.setLoc(relUrl);
	}
}
