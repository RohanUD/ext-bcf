/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.sitemap.populator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import org.apache.commons.lang.StringEscapeUtils;


public class VesselDataToSiteMapUrlDataPopulator implements Populator<TransportVehicleModel, SiteMapUrlData>
{
	private UrlResolver<TransportVehicleModel> urlResolver;

	@Override
	public void populate(final TransportVehicleModel model, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		final String relUrl = StringEscapeUtils.escapeXml(getUrlResolver().resolve(model));
		siteMapUrlData.setLoc(relUrl);
	}

	public UrlResolver<TransportVehicleModel> getUrlResolver()
	{
		return urlResolver;
	}

	public void setUrlResolver(
			final UrlResolver<TransportVehicleModel> urlResolver)
	{
		this.urlResolver = urlResolver;
	}
}
