/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travel.category.dao.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelCategoryDao;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.core.travel.category.BCFTravelCategoryDao;


/**
 * Dao to handle persistence layer operations for fetching category and product related to {@link TravelSectorModel} and
 * {@link TransportVehicleModel}
 */
public class DefaultBCFTravelCategoryDao extends DefaultTravelCategoryDao implements BCFTravelCategoryDao
{

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	private static final String CATEGORIES_FOR_PRODUCTS_QUERY = "SELECT DISTINCT {ca.pk} FROM {Category AS ca JOIN categoryProductRelation AS cpr ON {ca.PK} = {cpr.source} JOIN Product AS p ON {p.PK} = {cpr.target} } WHERE {p.CODE} IN (?productCodes)";
	private static final String ASSISTANCE_PRODUCTS_FOR_VEHICLES_QUERY = "SELECT DISTINCT {p.pk} FROM {Product AS p JOIN categoryProductRelation as cpr ON {cpr.target}={p.pk} JOIN Category as c ON {cpr.source}={c.PK} JOIN TransportVehicleProductRelation AS tvp ON {tvp.target}={p.pk} JOIN TransportVehicle AS tv ON {tv.PK} = {tvp.source} JOIN ProductType AS pt on {p.producttype} = {pt.pk}} WHERE {pt.code} = ?ancillaryCategoryCode AND {c.code} = ?assitanceCategory AND {tv.code} in (?vehicleCodes)";
	private static final String PRODUCTS_FOR_CATEGORY_CODE_QUERY = "SELECT {p.PK} FROM {Product AS p JOIN categoryProductRelation as cpr ON {cpr.target}={p.pk} JOIN Category as c ON {cpr.source}={c.PK}  JOIN CatalogVersion AS cv ON {p.catalogVersion}={cv.PK}} WHERE {p.PK} IN (?productCodes) AND {c.code} = ?categoryCode AND {cv.version}='Online'";
	private static final String ALL_PRODUCTS_FOR_VEHICLES = "SELECT DISTINCT {p.pk} FROM {Product AS p JOIN TransportVehicleProductRelation AS tvp ON {tvp.target}={p.pk} JOIN TransportVehicle AS tv ON {tv.PK} = {tvp.source}} WHERE {tv.code} in (?vehicleCodes)";
	private static final String ASSISTANCE_CATEGORY_CODE_KEY = "assistance.category.code";
	private static final String ANCILLARY_CATEGORY_CODE_KEY = "ancillary.category.code";

	@Override
	public List<CategoryModel> fetchCategoriesForProducts(final List<String> productCodes)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("productCodes", productCodes);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(CATEGORIES_FOR_PRODUCTS_QUERY, params);
		final SearchResult<CategoryModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<ProductModel> fetchAssistanceProductsForVehicle(final List<String> vehicleCodes)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("ancillaryCategoryCode", getConfigurationService().getConfiguration().getString(ANCILLARY_CATEGORY_CODE_KEY));
		params.put("assitanceCategory", getConfigurationService().getConfiguration().getString(ASSISTANCE_CATEGORY_CODE_KEY));
		params.put("vehicleCodes", vehicleCodes);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(ASSISTANCE_PRODUCTS_FOR_VEHICLES_QUERY, params);
		final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<ProductModel> getProductsForOriginDestinationAndCategory(final String categoryCode,
			final List<ProductModel> productCodes)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("categoryCode", categoryCode);
		params.put("productCodes", productCodes);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PRODUCTS_FOR_CATEGORY_CODE_QUERY, params);
		final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public List<ProductModel> fetchAllProductsForVehicles(final List<String> vehicleCodes)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("vehicleCodes", vehicleCodes);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(ALL_PRODUCTS_FOR_VEHICLES, params);
		final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
