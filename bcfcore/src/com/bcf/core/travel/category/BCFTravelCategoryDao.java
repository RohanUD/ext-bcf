/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travel.category;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.dao.TravelCategoryDao;
import java.util.List;


public interface BCFTravelCategoryDao extends TravelCategoryDao
{
	/**
	 * fetches unique categories for the specified products
	 *
	 * @param products list of product codes
	 * @return list of {@link CategoryModel}
	 */
	List<CategoryModel> fetchCategoriesForProducts(final List<String> products);

	/**
	 * Retrieves a list of assistance products for the provided vehicle and valid stocklevel
	 *
	 * @param vehicleCodes
	 * @return list of type {@link ProductModel}
	 */
	List<ProductModel> fetchAssistanceProductsForVehicle(final List<String> vehicleCodes);

	/**
	 * Retrieves a list of products for the provided category
	 *
	 * @param vehicleCode
	 * @return list of type {@link ProductModel}
	 */
	List<ProductModel> getProductsForOriginDestinationAndCategory(final String categoryCode,
			final List<ProductModel> productsCode);

	/**
	 * Retrieves all the products that are either mapped to provided list of vehicles or provided list of sectors
	 *
	 * @param vehicleCodes list of vehicle codes
	 * @return list of products
	 */
	List<ProductModel> fetchAllProductsForVehicles(List<String> vehicleCodes);

}
