/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.core.restrictions;

import de.hybris.platform.cms2.model.restrictions.B2BUnitGroupTypeRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class B2BUnitGroupTypeRestrictionEvaluator implements CMSRestrictionEvaluator<B2BUnitGroupTypeRestrictionModel>
{
	private static final Logger LOG = Logger.getLogger(B2BUnitGroupTypeRestrictionEvaluator.class);

	private UserService userService;

	@Override
	public boolean evaluate(final B2BUnitGroupTypeRestrictionModel b2BUnitGroupTypeRestrictionModel, final RestrictionData context)
	{
		return checkWhetherUserIsPartOfRestrictedGroup(getUserService().getCurrentUser(), b2BUnitGroupTypeRestrictionModel);
	}

	/**
	 * Checks Whether the supplied User is part of the business groups
	 *
	 * @param userModel
	 * @param b2BUnitGroupTypeRestrictionModel
	 * @return true or false
	 */
	protected boolean checkWhetherUserIsPartOfRestrictedGroup(final UserModel userModel, final B2BUnitGroupTypeRestrictionModel b2BUnitGroupTypeRestrictionModel)
	{
		final String groupType = b2BUnitGroupTypeRestrictionModel.getGroupType();
		final Set<PrincipalGroupModel> userGroups = new HashSet<>(userModel.getGroups());

		if (b2BUnitGroupTypeRestrictionModel.isIncludeSubgroups())
		{
			userGroups.addAll(getSubgroups(userGroups));
		}

		for (final PrincipalGroupModel userGroup : userGroups)
		{
			if (StringUtils.equalsIgnoreCase(userGroup.getItemtype(), groupType))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Fetches all Subgroups of the supplied groups.
	 *
	 * @param groups
	 * @return the groups
	 */
	protected List<PrincipalGroupModel> getSubgroups(final Collection<PrincipalGroupModel> groups)
	{
		final List<PrincipalGroupModel> ret = new ArrayList<>(groups);

		for (final PrincipalGroupModel principalGroup : groups)
		{
			ret.addAll(getSubgroups(principalGroup.getGroups()));
		}
		return ret;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}
}
