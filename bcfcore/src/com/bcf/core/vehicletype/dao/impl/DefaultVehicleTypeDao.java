/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.vehicletype.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.enums.VehicleCategoryType;
import com.bcf.core.enums.VehicleTypeCode;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.vehicletype.dao.VehicleTypeDao;



/**
 * Class is responsible for providing concrete implementation of the VehicleTypeDao interface. The class uses the
 * FlexibleSearchService to query the database and return as list of List<VehicleTypeModel> types.
 */
public class DefaultVehicleTypeDao extends DefaultGenericDao<VehicleTypeModel> implements VehicleTypeDao
{
	@Resource
	private EnumerationService enumerationService;

	public DefaultVehicleTypeDao(final String typecode)
	{
		super(typecode);
	}

	private static final String GET_VEHICLETYPE_FOR_CODE = "SELECT {" + VehicleTypeModel.PK + "} FROM {"
			+ VehicleTypeModel._TYPECODE + "}" + " WHERE {"
			+ VehicleTypeModel.VEHICLECATEGORYTYPE + "}=?" + VehicleTypeModel.VEHICLECATEGORYTYPE;

	private static final String GET_VEHICLE_FOR_CODE = "SELECT {" + VehicleTypeModel.PK + "} FROM {" + VehicleTypeModel._TYPECODE
			+ "}" + " WHERE {" + VehicleTypeModel.TYPE + "}=?" + VehicleTypeModel.TYPE;

	@Override
	public List<VehicleTypeModel> getVehicleTypesByCategoryType(final String categoryTypeCode)
	{
		validateParameterNotNull(categoryTypeCode, "category type code must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(VehicleTypeModel.VEHICLECATEGORYTYPE, VehicleCategoryType.valueOf(categoryTypeCode));
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_VEHICLETYPE_FOR_CODE, queryParams);
		final SearchResult<VehicleTypeModel> searchResult = getFlexibleSearchService().search(fsq);
		if (searchResult != null)
		{
			return searchResult.getResult();
		}
		return Collections.emptyList();
	}

	@Override
	public List<VehicleTypeModel> getAllVehicleTypes()
	{
		return find();
	}

	@Override
	public VehicleTypeModel getVehicleByCode(final String code)
	{
		validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		final HybrisEnumValue value = enumerationService.getEnumerationValue(VehicleTypeCode.class, code);
		if (Objects.nonNull(value))
		{
			queryParams.put(VehicleTypeModel.TYPE, value);
			final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_VEHICLE_FOR_CODE, queryParams);
			final SearchResult<VehicleTypeModel> searchResult = getFlexibleSearchService().search(fsq);
			if (searchResult.getTotalCount() == 0)
			{
				return null;
			}
			if (searchResult.getTotalCount() > 1)
			{
				throw new AmbiguousIdentifierException(
						"Found " + CollectionUtils.size(searchResult) + " results for the given vehicle code");
			}
			return searchResult.getResult().get(0);
		}
		return null;
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
