/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.vehicletype.dao;

import java.util.List;
import com.bcf.core.model.traveller.VehicleTypeModel;


/**
 * Interface that exposes Vehicle Type specific DAO services
 */
public interface VehicleTypeDao
{

	/**
	 * Gets the vehicle types by category type.
	 *
	 * @param categoryTypeCode the category type code
	 * @return List<VehicleTypeModel> list
	 */
	List<VehicleTypeModel> getVehicleTypesByCategoryType(String categoryTypeCode);

	/**
	 * returns all vehicle types.
	 *
	 * @return the all vehicle types
	 */
	List<VehicleTypeModel> getAllVehicleTypes();

	/**
	 * Gets the vehicle by code.
	 *
	 * @param vehicleCode the vehicle code
	 * @return the vehicle by code
	 */
	VehicleTypeModel getVehicleByCode(String vehicleCode);

}
