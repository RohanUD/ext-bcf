/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.vehicletype.impl;

import java.util.List;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.vehicletype.VehicleTypeService;
import com.bcf.core.vehicletype.dao.VehicleTypeDao;


/**
 * Class is responsible for providing concrete implementation of the VehicleTypeService interface. The class uses the
 * VehicleTypeDao class to query the database and return as list of List<VehicleTypeModel> types.
 */
public class DefaultVehicleTypeService implements VehicleTypeService
{

	private VehicleTypeDao VehicleTypeDao;

	public VehicleTypeDao getVehicleTypeDao()
	{
		return VehicleTypeDao;
	}

	public void setVehicleTypeDao(final VehicleTypeDao vehicleTypeDao)
	{
		VehicleTypeDao = vehicleTypeDao;
	}


	@Override
	public List<VehicleTypeModel> getVehicleTypesForCategoryType(final String categoryTypeCode)
	{
		return getVehicleTypeDao().getVehicleTypesByCategoryType(categoryTypeCode);
	}

	@Override
	public List<VehicleTypeModel> getAllVehicleTypes()
	{
		return getVehicleTypeDao().getAllVehicleTypes();
	}

	@Override
	public VehicleTypeModel getVehicleForCode(final String vehicleCode)
	{
		return getVehicleTypeDao().getVehicleByCode(vehicleCode);
	}
}
