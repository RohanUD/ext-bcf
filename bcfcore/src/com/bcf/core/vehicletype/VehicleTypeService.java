/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.vehicletype;

import java.util.List;
import com.bcf.core.model.traveller.VehicleTypeModel;


/**
 * Interface that exposes Vehicle Type specific services
 */
public interface VehicleTypeService
{

	/**
	 * Service which returns a list of VehicleTypeModel types
	 *
	 * @return List<VehicleTypeModel> Vehicle types
	 */
	List<VehicleTypeModel> getAllVehicleTypes();

	/**
	 * Gets the vehicle types for category type.
	 *
	 * @param categoryTypeCode the category type code
	 * @return the vehicle types for code
	 */
	List<VehicleTypeModel> getVehicleTypesForCategoryType(String categoryTypeCode);

	/**
	 * Gets the vehicle for code.
	 *
	 * @param vehicleCode the vehicle code
	 * @return the vehicle for code
	 */
	VehicleTypeModel getVehicleForCode(String vehicleCode);
}
