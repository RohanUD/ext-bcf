/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.url.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.Objects;
import com.bcf.core.service.BcfDealBundleTemplateService;


public class ValidateDealBundleTemplateSeoUrlInterceptor implements ValidateInterceptor<DealBundleTemplateModel>
{
	private BcfDealBundleTemplateService dealBundleTemplateService;


	@Override
	public void onValidate(final DealBundleTemplateModel dealBundleTemplateModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if(Objects.nonNull(dealBundleTemplateModel.getSeoUrl()) ){
		final DealBundleTemplateModel dealBundleBySeoUrl = getDealBundleTemplateService().getDealBundleTemplateBySeoUrl(dealBundleTemplateModel.getSeoUrl(),dealBundleTemplateModel.getCatalogVersion());
			if(Objects.nonNull(dealBundleBySeoUrl) && !(dealBundleTemplateModel.getPk().equals(dealBundleBySeoUrl.getPk()))){
				throw new InterceptorException("SEO URL is not Unique");
			}
		}
	}

	public BcfDealBundleTemplateService getDealBundleTemplateService()
	{
		return dealBundleTemplateService;
	}

	public void setDealBundleTemplateService(final BcfDealBundleTemplateService dealBundleTemplateService)
	{
		this.dealBundleTemplateService = dealBundleTemplateService;
	}

}
