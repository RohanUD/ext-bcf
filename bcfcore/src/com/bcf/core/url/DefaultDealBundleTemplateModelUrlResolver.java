/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.url;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;


public class DefaultDealBundleTemplateModelUrlResolver extends AbstractUrlResolver<DealBundleTemplateModel>
{
	private I18NService i18NService;
	private String defaultPattern;
	@Override
	protected String resolveInternal(final DealBundleTemplateModel source)
	{
		String url = getDefaultPattern();

		if (url.contains("{bundle-id}"))
		{
			url = url.replace("{bundle-id}", urlSafe(source.getId()));
		}
		if (url.contains("{bundle-name}"))
		{
			url = url.replace("{bundle-name}", urlEncode(source.getName(getI18NService().getCurrentLocale())));
		}
		return url;
	}
	public I18NService getI18NService()
	{
		return i18NService;
	}

	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}

}
