/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.constants;

import de.hybris.platform.catalog.jalo.CatalogManager;


/**
 * Global class for all BcfCore constants. You can add global constants for your extension into this class.
 */
public final class BcfCoreConstants extends GeneratedBcfCoreConstants
{
	public static final String EXTENSIONNAME = "bcfcore";
	public static final String PACKAGE_ON_REQUEST = "ON_REQUEST";
	public static final String ON_REQUEST_ORDER = "ON_REQUEST_ORDER";
	public static final String PAYMENT_TOKEN_RECEIVE_ONLY = "receiveTokenOnly";
	public static final String DECLARE_DEPOSIT = "DEPOSIT";
	public static final String RESERVERD_SAILING = "RESERVERD_SAILING";
	public static final String OPEN_TICKET = "OPEN_TICKET";


	private BcfCoreConstants()
	{
		//empty
	}

	public static final String DEFAULT_SITE_ID = "bcferries";
	public static final String VENDOR_BCFERRIES = "bcferries";

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
	public static final String TRAVELLER_TYPE_VEHICLE = "VEHICLE";
	public static final String TRAVELLER_TYPE_PASSENGER = "PASSENGER";
	public static final String IS_B2B_CUSTOMER = "isB2bCustomer";

	public static final String PRICE_MAP = "priceMap";
	public static final String CACHING_KEY = "cachingKey";
	public static final String BOOKING_REFERENCE = "bookingReference";
	public static final String MAKE_BOOKING_RESPONSE = "makeBookingResponse";
	public static final String ADDTOCART_DATE_PATTERN = "MM/dd/yyyy HH:mm";
	public static final String DAY_OF_WEEK_PATTERN = "EEEE";
	public static final String SIMPLE_DATE_FORMAT = "ddMMyyyy";
	public static final String DATE_PATTERN_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String CREDIT_CARD_INFO_SECURITY_CODE = "credit_card_info_security_code";

	public static final String CART_ENTRY_JOURNEY_REF_NUM = "journeyRefNumber";

	public static final String EBOOKING_MAX_QUOTATION_EXPIRATION_MINUTES = "ebookingMaxQuotationExpirationMinutes";
	public static final String EBOOKING_MAX_SAILING_DEPARTURE_EXPIRATION_SECONDS = "ebookingMaxSailingDepartureExpirationSeconds";

	public static final String DEFAULT_TRANSPORT_BOOKING_THRESHOLD_MINUTES = "defaultTransportBookingThresholdMinutes";
	public static final String COMMERCIAL_VEHICLE_TRANSPORT_BOOKING_THRESHOLD_MINUTES = "commercialVehicleTransportBookingThresholdMinutes";

	public static final String ADD_TO_CART_THRESHOLD_OVERRIDE = "addToCartThresholdOverride";

	public static final String AMEND_EBOOKING_AMENDMENT_BOOKING_FEES = "bcfAmendBookingFeesAccess";
	public static final String SAILINGS_REMOVED_FROM_CART = "sailingsRemovedFromCart";

	public static final String CARRYING_DANGEROUS_GOODS = "carryingDangerousGoods";
	public static final String DEFAULT_FARE_PRODUCT_CODE = "DEFAULT_FARE";
	public static final int OUTBOUND_REFERENCE_NUMBER = 0;
	public static final int INBOUND_REFERENCE_NUMBER = 1;
	public static final String SOLR_INDEXER_DATE = "1970-01-01T00:00:00Z";
	public static final String SOLR_INDEXER_DATE_PATTERN = "yyyy-MM-dd'T'hh:mm:ss'Z'";
	public static final String SOLR_CURRENT_DATE_RANGE = "currentDateRange";
	public static final String DEFAULT_WAREHOUSE = "default";

	public static final String DEFAULT_TRAVEL_PROVIDER = "defaulttravelprovider";
	public static final String DEFAULT_TRANSPORT_VEHICLE_PROVIDER = "defaulttransportvehicleprovider";
	public static final String IS_EBOOKING_INTEGRATION_JOURNEY = "useEbookingResponse";
	public static final String DEFAULT_RELEASE_INVENTORY_BLOCK_DAYS = "maxReleaseInventoryDays";
	public static final String FULL_ACCOUNT = "FULL_ACCOUNT";
	public static final String SUBSCRIPTION_ONLY = "SUBSCRIPTION_ONLY";
	public static final String SUBSCRIPTION_TIME_STAMP = "yyyy-MM-dd'T'hh:mm:ss";
	public static final String UNSUBSCRIBE_ALL = "unsubscribeAll";

	public static final String SELECTED_JOURNEY_REF_NO = "selectedJourneyRefNumber";
	public static final String SESSION_BOOKING_JOURNEY = "sessionBookingJourney";
	public static final String DEPOSIT = "Deposit";
	public static final String BOOKING_TRANSPORT_ONLY = "BOOKING_TRANSPORT_ONLY";
	public static final String BOOKING_PACKAGE = "BOOKING_PACKAGE";
	public static final String BOOKING_ACCOMMODATION_ONLY = "BOOKING_ACCOMMODATION_ONLY";
	public static final String BOOKING_TRANSPORT_ACCOMMODATION = "BOOKING_TRANSPORT_ACCOMMODATION";
	public static final String DEFAULT_DYNAMIC_DEAL_PREFIX = "defaultdynamicdealprefix";
	public static final String DEFAULT_PAYMENT_OPTION = "default";
	public static final String PARTIAL_PAYMENT_OPTION = "partialpayment";
	public static final String SALES_CHANNEL = "salesChannel";
	public static final String SALES_CHANNEL_NAME = "salesChannelName";

	public static final String LEFT_ANGLE_BRACKET = "(";
	public static final String RIGHT_ANGLE_BRACKET = ")";
	public static final String LEFT_SQUARE_BRACKET = "[";
	public static final String RIGHT_SQUARE_BRACKET = "]";
	public static final String ASTERIK_SIGN = "*";
	public static final String OR_OPERATOR = "OR";
	public static final String AND_OPERATOR = "AND";
	public static final String SOLR_TO_OPERATOR = " TO ";
	public static final String DATE_TYPE_SOLR_LITERAL = "_date:";
	public static final String UNDERSCORE = "_";

	public static final String SOLR_CURRENT_DATE_INCLUSIVE_PARAM_FOR_SOLD_FROM = "NOW+1DAY";
	public static final String SOLR_CURRENT_DATE_INCLUSIVE_PARAM_FOR_SOLD_TO = "NOW-1DAY";

	public static final String APPLY_DEFAULT_DEAL_MARGIN_ON_FARE = "applyDefaultDealMarginOnFare";
	public static final String DEFAULT_DEAL_MARGIN_ON_FARE_PRICE = "defaultDealMarginOnFarePrice";
	public static final String ORIGINDEST_TRAVELLER_FARE_MAP = "originDestTravellerFareMap";

	public static final String CART_ENTRY_TYPE = "cartEntryType";

	public static final String PRODUCTCATALOG = "bcfProductCatalog";
	public static final String CATALOG_VERSION = CatalogManager.ONLINE_VERSION;
	public static final String STAGED_CATALOG_VERSION = "Staged";
	public static final String TRANSACTION_CATALOG_ID = "transactionalContentCatalog";
	public static final String MASTER_CATALOG_ID = "masterContentCatalog";
	public static final String MOTORCYCLE_STANDARD = "MC";
	public static final String MOTORCYCLE_WITH_TRAILER = "MST";
	public static final String UNDER_HEIGHT = "UH";
	public static final String UNDER_HEIGHT_WITH_LIVESTOCK = "UHL";
	public static final String OVERSIZE = "OS";
	public static final String OVERSIZE_WITH_LIVESTOCK = "OSL";
	public static final int PERCENTAGE_SCALE_FOR_CALCULATIONS = 2;

	public static final String SITE_LOGO = "BC-Ferries-Logo.png";
	public static final String PAYMENT_ERROR = "checkout.error.paymentethod.formentry.invalid";
	public static final String INVALID_PAYMENT_ERROR = "text.invalid.payment.form.amount";
	public static final String INVALID_DEPOSIT_ERROR = "text.invalid.deposit.form.amount";

	public static final String MYBCF_SOURCE_SYSTEM = "MYBCF";
	public static final String CRM_SOURCE_SYSTEM = "CRM";
	public static final String EBOOKING = "EBOOKING";
	public static final String HYBRIS_SOURCE_SYSTEM = "HYBRIS";

	public final static String B2B_CHECKOUT_CONTEXT_DATA = "b2bCheckoutContextData";

	public static final String SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE = "ServiceNoticeWorkflow";
	public static final String SERVICE_NOTICE_VALIDATE_ACTION = "ValidateSNAction";
	public static final String SERVICE_NOTICE_UPDATE_ACTION = "UpdateSNAction";
	public static final String SERVICE_NOTICE_KEEP_DECISION = "SNKeepDecision";
	public static final String SERVICE_NOTICE_REJECT_DECISION = "SNRejectDecision";
	public static final String SERVICE_NOTICE_UPDATE_REJECT_DECISION = "SNNUpdationRejectedDecision";

	public static final String TRAVEL_ADVISORY_WORKFLOW_TEMPLATE_CODE = "TravelAdvisoryWorkflow";
	public static final String TRAVEL_ADVISORY_VALIDATE_ACTION = "ValidateTAAction";
	public static final String TRAVEL_ADVISORY_UPDATE_ACTION = "UpdateTAAction";
	public static final String TRAVEL_ADVISORY_KEEP_DECISION = "TAKeepDecision";
	public static final String TRAVEL_ADVISORY_REJECT_DECISION = "TARejectDecision";
	public static final String TRAVEL_ADVISORY_UPDATE_REJECT_DECISION = "TANUpdationRejectedDecision";

	public static final String BACKOFFICE_WORKFLOW_ADMIN_USER = "admin";
	public static final String BCF_HTTPS_URL = "website.bcferries.https";
	public static final String REGISTRATION_VERIFYACCOUNT_URL = "customer.registration.verfiyaccount.url";
	public static final String PASSWORD_RESET_URL = "customer.passwordreset.url";

	public static final String MAKE_BOOKING_REQUEST = "MakeBooking";
	public static final String CONVERT_QUOTATION_TO_OPTION_REQUEST = "ConvertQuotationToOption";

	public static final String CATEGORY_OTHER = "OTHER";
	public static final String CATEGORY_PASSENGER = "PASSENGER";
	public static final String CATEGORY_VEHICLE = "VEHICLE";
	public static final String CATEGORY_STOWAGE = "STOWAGE";
	public static final String CATEGORY_AMENITY = "AMENITY";
	public static final String OTHER_ACCESSIBILITY_REQUIREMENT_CODE = "OTHR";
	public static final String SOLR_FIELD_MAX_COUNT = "maxOccupancy";
	public static final String SOLR_FIELD_MAX_ADULT_COUNT = "numberOfAdults";
	public static final String SOLR_FIELD_MINIMUM_LENGTH_OF_STAY = "minimumStayLength";

	public final static String SEASONAL_SCHEDULE_DEPARTURE_PORT = "departurePort";
	public final static String SEASONAL_SCHEDULE_ARRIVAL_PORT = "arrivalPort";
	public final static String SEASONAL_SCHEDULE_DEPARTURE_DATE_FROM = "departureDateFrom";
	public final static String SEASONAL_SCHEDULE_DEPARTURE_DATE_TO = "departureDateTo";
	public final static String SEASONAL_SCHEDULE_DATE_FORMAT_WITH_HOURS_MIN = "yyyyMMddHHmm";
	public final static String SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD = "yyyyMMdd";

	public final static String CURRENT_CONDITIONS_SAILING_DURATION_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public final static String SAVE_QUOTE_URL = "customer.save.quote.url";

	public static final String DMF = "DMF";
	public static final String MRDT = "MRDT";
	public static final String PST = "PST";
	public static final String GST = "GST";
	public static final String PASSENGER_TYPE_CODE_ADULT = "adult";
	public static final String ALCARTE_SALES_CHANNELS = "alacarteSalesChannels";
	public static final String OPTION_BOOKING_SALES_CHANNELS = "optionBookingSalesChannels";

	public static final String CONSIDER_NON_BOOKABLE_ONLINE_SALE_STATUS = "considerNonBookableOnlineSaleStatus";
	public static final String BCF_INTERNAL_CARD = "BCF Internal Card";
	public static final String BCF_CTC_TC_CARD_TYPE = "CTC/TC";
	public static final String CTC_CARD_NUMBER = "ctcCardNo";
	public static final String CTC_TC_CARD = "CTCTCCard";
	public static final String CREDIT_CARD = "CreditCard";

	public static final String HYPHEN = "-";
	public static final String TEXT_YEARS = " years";
	public static final String TEXT_PLUS = "+";

	public static final String NORTHERN_PACKAGE_CHANGE_FEE_PRODUCT = "NPCF";
	public static final String SOUTHERN_PACKAGE_CHANGE_FEE_PRODUCT = "SPCF";
	public static final String COMPONENT_CHANGE_FEE = "NRCF";

	public static final String NORTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT = "NPXF";
	public static final String SOUTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT = "SPXF";
	public static final String COMPONENT_CANCELLATION_FEE = "NRXF";
	public static final String ANONYMOUS = "Anonymous";

	public static final String CANONICAL_URL = "canonicalUrl";
	public static final String DISPLAY_ORDER_ID = "displayOrderId";
	public static final String OG_GRAPH_DATA = "ogGraphData";

	public static final String IS_AMEND_ORDER = "isAmendOrder";
	public static final String AMEND_ORDER_CODE = "amendOrderCode";
	public static final String CARD_REFUND_WITH_PIN_PAD = "cardRefundWithPinPad";
	public static final String COMMERCIAL_VEHICLE_CODES = "commercialVehicleCodes";
	public static final String STANDARD_VEHICLE_CODES = "standardVehicleCodes";
	public static final String CHANGE_AND_CANCEL_FEE_PRODUCTS = "changeAndCancelFeeProducts";
	public static final String CHANGE_FEE_PRODUCTS = "changeFeeProducts";
	public static final String OPTION_BOOKING_DEFAULT_EXPIRY_TIME_IN_DAYS = "optionBookingExpiryTimeInDays";
	public static final String OPTION_BOOKING_PURGE_TIME_IN_DAYS = "optionBookingPurgeTimeInDays";
	public static final String REGIONS_REQUIRED_FOR_COUNTRIES = "regionsRequiredForCountries";


	public static final String GUEST_BOOKING_DETAILS_PAGE = "/manage-booking/guest-booking-details?guestBookingIdentifier=";

	public static final String OPTION_BOOKING_PROCESS_NAME = "option-booking-process";
	public static final String OPTION_BOOKING_CANCEL_PROCESS_NAME = "cancel-optionBooking-process";

	public static final String EXPIRED_OPTION_BOOKING_NOTIFICATION_URL = "expiredOptionBookingServiceUrl";

	public static final String REFERER_REDIRECT = "refererRedirect";

	public static final int DEFAULT_MIN_UH_OS_LENGTH = 20;
	public static final int DEFAULT_MIN_UH_OS_HEIGHT = 7;
	public static final int DEFAULT_MC_MST_LENGTH = 1;
	public static final int DEFAULT_MC_MST_HEIGHT = 4;
	public static final double OVER5500KG_MIN_WIDTH = 9.0;
	public static final String C_TYPE_VEHICLE_PREFIX = "C";
	public static final String S_TYPE_VEHICLE_PREFIX = "S";
	public static final String IS_FERRY_OPTION_BOOKING = "isFerryOptionBooking";

	public static final String CONVERT_QUOTATION_TO_OPTION_FAILED = "convertQuotationToOptionFailed";

	public static final String ORDER_PROCESS_NAME = "order-process";
	public static final String AMEND_ORDER_PROCESS_NAME = "amend-order-process";
	public static final String ONREQUEST_INITIAL_ORDER_PROCESS_NAME = "onrequest-order-process";

	public static final String PENDING_ORDERS_ORDER_LOCKED = "text.page.pendingorders.order.locked";
	public static final String PENDING_ORDERS_ORDER_UNLOCKED = "text.page.pendingorders.order.unlocked";
	public static final String PENDING_ORDERS_LOCK_REQUIRED = "text.page.pendingorders.order.action.lock.required";
	public static final String ORDER_LOCK_SUCCESSFUL = "text.page.pendingorders.lock.action.update.successful";
	public static final String ORDER_UNLOCK_SUCCESSFUL = "text.page.pendingorders.unlock.action.update.successful";
	public static final String ORDER_ALREADY_APPROVED = "text.page.pendingorders.already.approved";
	public static final String ORDER_ALREADY_REJECTED = "text.page.pendingorders.already.rejected";
	public static final String PENDING_ORDERS_APPROVE_SUCCESSFUL = "text.page.pendingorders.order.actions.successful";
	public static final String PENDING_ORDERS_UPDATE_NOT_SUCCESSFUL = "text.page.pendingorders.order.actions.not.successful";
	public static final String PENDING_ORDERS_UPDATE_FAILED = "text.page.pendingorders.order.actions.update.failure";
	public static final String PENDING_ORDERS_UPDATE_EXCEPTION = "text.page.pendingorders.order.actions.exception";
	public static final String PENDING_ORDERS_REJECT_SUCCESSFUL = "text.page.pendingorders.order.reject.successful";
	public static final String PENDING_ORDER_ENTRY_UPDATE_SUCCESSFUL = "text.page.pendingorders.entry.actions.update.successful";
	public static final String PENDING_ORDERS_ENTRY_UPDATE_FAILED = "text.page.pendingorders.entry.actions.update.failure";
	public static final String CTC_CARD_INACTIVE = "INACTIVE";

}
