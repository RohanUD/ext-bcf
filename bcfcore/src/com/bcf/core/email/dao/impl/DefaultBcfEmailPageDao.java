/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.email.dao.impl;

import de.hybris.platform.acceleratorservices.email.dao.impl.DefaultEmailPageDao;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.email.dao.BcfEmailPageDao;


public class DefaultBcfEmailPageDao extends DefaultEmailPageDao implements BcfEmailPageDao
{

	protected static final String QUERY_FIND_EMAIL_TEMPLATE_FOR_ID_AND_VERSION =
			"SELECT {" + EmailPageTemplateModel.PK + "} " + "FROM {" + EmailPageTemplateModel._TYPECODE + " AS t} " + "WHERE {t:"
					+ EmailPageTemplateModel.UID + "}=?uid AND {t:" + EmailPageTemplateModel.CATALOGVERSION
					+ "}=?catalogVersion AND {t:" + EmailPageTemplateModel.VERSION + "}=?version";

	protected static final String QUERY_FIND_LATEST_EMAIL_TEMPLATE_FOR_ID =
			"SELECT {" + EmailPageTemplateModel.PK + "} " + "FROM {" + EmailPageTemplateModel._TYPECODE + " AS t} " + "WHERE {t:"
					+ EmailPageTemplateModel.UID + "}=?uid AND {t:" + EmailPageTemplateModel.CATALOGVERSION
					+ "}=?catalogVersion ORDER BY {t." + EmailPageTemplateModel.VERSION + "} DESC";

	@Override
	public EmailPageTemplateModel findEmailPageTemplateForTemplateId(final String templateId, final int version,
			final CatalogVersionModel catalogVersion)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(EmailPageTemplateModel.UID, templateId);
		params.put(EmailPageTemplateModel.CATALOGVERSION, catalogVersion);
		params.put(EmailPageTemplateModel.VERSION, version);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(QUERY_FIND_EMAIL_TEMPLATE_FOR_ID_AND_VERSION, params);
		final SearchResult<EmailPageTemplateModel> result = getFlexibleSearchService().search(fsq);

		return CollectionUtils.isNotEmpty(result.getResult()) ? result.getResult().get(0) : null;
	}

	@Override
	public EmailPageTemplateModel findLatestEmailPageTemplatesForTemplateId(final String templateId,
			final CatalogVersionModel catalogVersion)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(EmailPageTemplateModel.UID, templateId);
		params.put(EmailPageTemplateModel.CATALOGVERSION, catalogVersion);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(QUERY_FIND_LATEST_EMAIL_TEMPLATE_FOR_ID, params);
		final SearchResult<EmailPageTemplateModel> result = getFlexibleSearchService().search(fsq);

		return CollectionUtils.isNotEmpty(result.getResult()) ? result.getResult().get(0) : null;
	}
}
