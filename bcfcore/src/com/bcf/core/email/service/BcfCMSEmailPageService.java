/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.email.service;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;


public interface BcfCMSEmailPageService
{
	EmailPageTemplateModel getEmailPageTemplateForTemplateId(final String templateId, final int version,
			final CatalogVersionModel catalogVersion);

	EmailPageTemplateModel getLatestEmailPageTemplateForTemplateId(final String templateId);
}
