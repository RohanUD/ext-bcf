/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.core.email.service.impl;

import reactor.util.CollectionUtils;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.email.impl.DefaultTravelEmailGenerationService;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;


public class DefaultBcfTravelEmailGenerationService extends DefaultTravelEmailGenerationService
{
	private static final Logger LOG = Logger.getLogger(DefaultTravelEmailGenerationService.class);

	@Override
	public List<EmailMessageModel> generateEmails(final BusinessProcessModel businessProcessModel,
			final EmailPageModel emailPageModel)
	{
		ServicesUtil.validateParameterNotNull(emailPageModel, "EmailPageModel cannot be null");
		Assert.isInstanceOf(EmailPageTemplateModel.class, emailPageModel.getMasterTemplate(),
				"MasterTemplate associated with EmailPageModel should be EmailPageTemplate");

		final EmailPageTemplateModel emailPageTemplateModel = (EmailPageTemplateModel) emailPageModel.getMasterTemplate();
		final RendererTemplateModel bodyRenderTemplate = emailPageTemplateModel.getHtmlTemplate();
		Assert.notNull(bodyRenderTemplate, "HtmlTemplate associated with MasterTemplate of EmailPageModel cannot be null");
		final RendererTemplateModel subjectRenderTemplate = emailPageTemplateModel.getSubject();
		Assert.notNull(subjectRenderTemplate, "Subject associated with MasterTemplate of EmailPageModel cannot be null");

		final List<EmailMessageModel> emailMessageModels = new ArrayList<>();
		//This call creates the context to be used for rendering of subject and body templates.
		final AbstractEmailContext<BusinessProcessModel> emailContext = getEmailContextFactory().create(businessProcessModel,
				emailPageModel, bodyRenderTemplate);

		if (emailContext == null)
		{
			LOG.error("Failed to create email context for businessProcess [" + businessProcessModel + "]");
			throw new IllegalStateException("Failed to create email context for businessProcess [" + businessProcessModel + "]");
		}
		else
		{
			if (!validate(emailContext))
			{
				LOG.error("Email context for businessProcess [" + businessProcessModel + "] is not valid: "
						+ ReflectionToStringBuilder.toString(emailContext));
				throw new IllegalStateException("Email context for businessProcess [" + businessProcessModel + "] is not valid: "
						+ ReflectionToStringBuilder.toString(emailContext));
			}

			final StringWriter subject = new StringWriter();
			getRendererService().render(subjectRenderTemplate, emailContext, subject);

			final StringWriter body = new StringWriter();
			getRendererService().render(bodyRenderTemplate, emailContext, body);

			if (CollectionUtils.isEmpty(businessProcessModel.getEmails()))
			{
				emailMessageModels.addAll(createEmailMessages(subject.toString(), body.toString(), emailContext));
			}
			else
			{
				final EmailMessageModel emailAddress = businessProcessModel.getEmails().stream().findFirst().get();
				emailMessageModels.addAll(createEmailMessages(emailAddress, subject.toString(), body.toString(), emailContext));
			}

		}

		return emailMessageModels;
	}

	/**
	 * Create email messages list.
	 *
	 * @param emailSubject the email subject
	 * @param emailBody    the email body
	 * @param emailContext the email context
	 * @return the list
	 */

	protected List<EmailMessageModel> createEmailMessages(final EmailMessageModel emailAddress, final String emailSubject,
			final String emailBody,
			final AbstractEmailContext<BusinessProcessModel> emailContext)
	{
		final List<EmailMessageModel> emailMessageModels = new ArrayList<>();
		final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
				emailContext.getFromDisplayName());

		final List<EmailAddressModel> toAddresses = emailAddress.getToAddresses();
		emailMessageModels.add(getEmailService().createEmailMessage(toAddresses, new ArrayList<>(),
				new ArrayList<>(), fromAddress, emailContext.getFromEmail(), emailSubject, emailBody, null));

		createEmailMessagesForAdditionalNotifications(emailSubject, emailBody, emailContext, emailMessageModels, fromAddress);

		return emailMessageModels;
	}
}
