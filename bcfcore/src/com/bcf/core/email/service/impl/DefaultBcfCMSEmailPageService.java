/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.email.service.impl;

import static com.bcf.core.constants.BcfCoreConstants.CATALOG_VERSION;
import static com.bcf.core.constants.BcfCoreConstants.TRANSACTION_CATALOG_ID;

import de.hybris.platform.acceleratorservices.email.impl.DefaultCMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import javax.annotation.Resource;
import com.bcf.core.email.dao.BcfEmailPageDao;
import com.bcf.core.email.service.BcfCMSEmailPageService;


public class DefaultBcfCMSEmailPageService extends DefaultCMSEmailPageService implements BcfCMSEmailPageService
{
	@Resource(name = "bcfEmailPageDao")
	BcfEmailPageDao bcfEmailPageDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Override
	public EmailPageTemplateModel getEmailPageTemplateForTemplateId(final String templateId, final int version,
			final CatalogVersionModel catalogVersion)
	{
		return bcfEmailPageDao.findEmailPageTemplateForTemplateId(templateId, version, catalogVersion);
	}

	public EmailPageTemplateModel getLatestEmailPageTemplateForTemplateId(final String templateId)
	{
		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(TRANSACTION_CATALOG_ID, CATALOG_VERSION);
		return bcfEmailPageDao.findLatestEmailPageTemplatesForTemplateId(templateId, catalogVersion);
	}

}
