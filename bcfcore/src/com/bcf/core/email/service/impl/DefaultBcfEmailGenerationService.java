/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.core.email.service.impl;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.services.BookingService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.email.service.BcfEmailGenerationService;


public class DefaultBcfEmailGenerationService implements BcfEmailGenerationService
{
	private static final String CATALOG_ID = "transactionalContentCatalog";
	private static final String CATALOG_VERSION_NAME = "Staged";
	private static final String FRONTEND_TEMPLATE_NAME = "OrderConfirmationEmailTemplate";

	private BookingService bookingService;
	private BusinessProcessService businessProcessService;
	private ModelService modelService;
	private CMSEmailPageService cmsEmailPageService;
	private CatalogVersionService catalogVersionService;
	private EmailService emailService;

	@Override
	public void sendEmail(final String bookingReference, final HashMap<String, String> emailDetails)
	{
		final OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		final OrderProcessModel orderProcessModel = getBusinessProcessService().createProcess(
				"orderConfirmationEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
				"orderConfirmationEmailProcess");
		orderProcessModel.setOrder(orderModel);

		final List<EmailMessageModel> emailMessages = new ArrayList<>();
		final EmailMessageModel emailMessage = new EmailMessageModel();
		final List<EmailAddressModel> emailAddresses = new ArrayList<>();

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(CATALOG_ID, CATALOG_VERSION_NAME);
		final EmailPageModel emailPage = getCmsEmailPageService().getEmailPageForFrontendTemplate(FRONTEND_TEMPLATE_NAME,
				catalogVersion);

		emailDetails.forEach((additionalEmail, displayName) -> {
			final EmailAddressModel additionalAddress = getEmailService().getOrCreateEmailAddressForEmail(additionalEmail,
					displayName);
			emailAddresses.add(additionalAddress);
		});

		emailMessage.setToAddresses(emailAddresses);
		final String subject = ((EmailPageTemplateModel) emailPage.getMasterTemplate()).getSubject().getDescription();
		emailMessage.setSubject(subject);
		emailMessage.setReplyToAddress(emailPage.getFromEmail());
		emailMessages.add(emailMessage);
		orderProcessModel.setEmails(emailMessages);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	protected BookingService getBookingService()
	{
		return bookingService;
	}

	@Required
	public void setBookingService(final BookingService bookingService)
	{
		this.bookingService = bookingService;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CMSEmailPageService getCmsEmailPageService()
	{
		return cmsEmailPageService;
	}

	@Required
	public void setCmsEmailPageService(final CMSEmailPageService cmsEmailPageService)
	{
		this.cmsEmailPageService = cmsEmailPageService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected EmailService getEmailService()
	{
		return emailService;
	}

	@Required
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}
}
