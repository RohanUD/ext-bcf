/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.impl;

import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_LOCK_REQUIRED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_ORDER_LOCKED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_ORDER_UNLOCKED;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.comments.model.CommentTypeModel;
import de.hybris.platform.comments.model.ComponentModel;
import de.hybris.platform.comments.model.DomainModel;
import de.hybris.platform.comments.services.impl.DefaultCommentService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultOrderService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.util.Config;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.AsmAgentStatus;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.helper.OrderBusinessProcessHelper;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.order.dao.BcfOrderDao;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.order.UpdatePendingOrderEntryRequestData;


public class DefaultBcfOrderService extends DefaultOrderService implements BcfOrderService
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOrderService.class);

	private static final String REVIEW_DECISION_EVENT_NAME = "_CSAOrderVerified";

	private BcfOrderDao bcfOrderDao;
	private BusinessProcessService businessProcessService;
	private CustomerAccountService customerAccountService;
	private BaseStoreService baseStoreService;
	private UserService userService;
	private AssistedServiceService assistedServiceService;
	private EventService eventService;
	private BcfCalculationService bcfCalculationService;
	private OrderBusinessProcessHelper orderBusinessProcessHelper;

	@Resource(name = "commentService")
	private DefaultCommentService commentService;

	private ConfigurationService configurationService;

	@Override
	public SearchPageData<AbstractOrderModel> getPagedPendingOrdersForInventoryApproval(final PageableData pageableData)
	{
		return getBcfOrderDao().findPagedPendingOrdersForInventoryApproval(pageableData);
	}

	@Override
	public AbstractOrderModel getOrderByEBookingCode(final String eBookingCode)
	{
		final List<OrderEntryModel> orderEntries = getBcfOrderDao().findOrderEntriesByEBookingCode(eBookingCode);
		if (CollectionUtils.isNotEmpty(orderEntries))
		{
			return orderEntries.get(0).getOrder();
		}
		return null;
	}

	/**
	 * gives the latest order (i.e. which has eBooking version number highest)
	 *
	 * @param orderCode
	 * @return
	 */
	@Override
	public OrderModel getOrderByCode(final String orderCode)
	{
		final List<OrderModel> orders = getBcfOrderDao().findOrderByCode(orderCode);
		if (CollectionUtils.isNotEmpty(orders))
		{
			return orders.get(0);
		}
		return null;
	}

	@Override
	public OrderModel getOrderByCode(final String orderCode, final String versionId)
	{
		final List<OrderModel> orders = getBcfOrderDao().findOrderByCode(orderCode, versionId);
		if (CollectionUtils.isNotEmpty(orders))
		{
			return orders.get(0);
		}
		return null;
	}

	@Override
	public List<OrderModel> getOrdersByCode(final String orderCode)
	{
		final List<OrderModel> orders = getBcfOrderDao().findAllOrderByCode(orderCode);
		if (CollectionUtils.isNotEmpty(orders))
		{
			return orders;
		}
		return null;
	}

	@Override
	public List<OrderModel> findVacationsForCurrentUser(final UserModel user)
	{
		return getBcfOrderDao().findVacationsForCurrentUser(user);
	}

	@Override
	public List<OrderEntryModel> findOrderEntriesByEBookingCode(final String eBookingCode)
	{
		return getBcfOrderDao().findOrderEntriesByEBookingCode(eBookingCode);
	}

	@Override
	public AbstractOrderModel getOrderByCartId(final String cartId)
	{
		if (StringUtils.isNotBlank(cartId))
		{
			return getBcfOrderDao().findOrderByCartId(cartId);
		}
		return null;
	}

	@Override
	public boolean updateOrderEntry(final UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequestData,
			final String agentId)
	{
		final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		final OrderModel orderModel = getCustomerAccountService()
				.getOrderForCode(updatePendingOrderEntryRequestData.getOrderCode(), baseStoreModel);
		final AsmAgentStatus approvalStatus = StringUtils
				.equals(AsmAgentStatus.APPROVED.getCode(), updatePendingOrderEntryRequestData.getSelectedAction()) ?
				AsmAgentStatus.APPROVED : AsmAgentStatus.REJECTED;
		final DomainModel domainModel = commentService
				.getDomainForCode(getConfigurationService().getConfiguration().getString("code.domain.model.asmagent"));
		final ComponentModel componentModel = commentService.getComponentForCode(domainModel,
				getConfigurationService().getConfiguration().getString("code.component.model.asmagent"));
		final CommentTypeModel commentTypeModel = commentService.getCommentTypeForCode(componentModel,
				getConfigurationService().getConfiguration().getString("code.comment.type.model.asmagent"));

		final Optional<AbstractOrderEntryModel> orderEntryModelOptional = orderModel.getEntries().stream()
				.filter(entry -> entry.getEntryNumber() == updatePendingOrderEntryRequestData.getEntryNumber()).findFirst();
		if (!orderEntryModelOptional.isPresent())
		{
			return false;
		}

		final AbstractOrderEntryModel orderEntryModel = orderEntryModelOptional.get();

		final CommentModel commentModel = getModelService().create(CommentModel.class);
		commentModel.setComponent(componentModel);
		commentModel.setCommentType(commentTypeModel);
		commentModel.setText(updatePendingOrderEntryRequestData.getAgentComments());
		commentModel.setAuthor(userService.getUserForUID(agentId));
		commentModel.setCreationtime(new Date());
		final List<CommentModel> commentList = new ArrayList<>();
		commentList.add(commentModel);
		commentList.addAll(orderEntryModel.getComments());
		orderEntryModel.setComments(commentList);
		orderEntryModel.setAsmAgentStatus(approvalStatus);
		if (AsmAgentStatus.REJECTED.equals(approvalStatus))
		{
			orderEntryModel.setActive(Boolean.FALSE);
			try
			{
				getBcfCalculationService().recalculate(orderModel);
			}
			catch (final CalculationException ex)
			{
				LOG.error("error recalculating order " + orderModel.getCode(), ex);
				return false;
			}
		}
		getModelService().saveAll();
		return true;
	}

	@Override
	public String lockOrder(final OrderModel order)
	{
		getModelService().refresh(order);
		if (Objects.equals(OrderStatus.PENDING_APPROVAL_FROM_MERCHANT, order.getStatus()))
		{
			order.setStatus(OrderStatus.LOCKED);
			order.setLockedBy(assistedServiceService.getAsmSession().getAgent().getDisplayName());
			getModelService().save(order);
			return Boolean.TRUE.toString();
		}
		return PENDING_ORDERS_ORDER_LOCKED;
	}

	@Override
	public String lockOrderAndGetAgentName(final OrderModel order) throws BcfOrderUpdateException
	{
		getModelService().refresh(order);
		if (Objects.equals(OrderStatus.PENDING_APPROVAL_FROM_MERCHANT, order.getStatus()))
		{
			order.setStatus(OrderStatus.LOCKED);
			final String lockedBy = assistedServiceService.getAsmSession().getAgent().getDisplayName();
			order.setLockedBy(lockedBy);
			getModelService().save(order);
			return lockedBy;
		}
		throw new BcfOrderUpdateException(PENDING_ORDERS_ORDER_LOCKED);
	}

	@Override
	public String unlockOrder(final OrderModel order)
	{
		getModelService().refresh(order);
		if (Objects.equals(OrderStatus.LOCKED, order.getStatus()))
		{
			order.setStatus(OrderStatus.PENDING_APPROVAL_FROM_MERCHANT);
			order.setLockedBy("");
			getModelService().save(order);
			return Boolean.TRUE.toString();
		}
		return PENDING_ORDERS_ORDER_UNLOCKED;
	}

	@Override
	public String rejectOrder(final OrderModel order, final String comments, final String agentId) throws BcfOrderUpdateException
	{
		getModelService().refresh(order);
		if (Objects.equals(OrderStatus.LOCKED, order.getStatus()))
		{
			setComments(order, comments, agentId);
			order.setStatus(OrderStatus.REJECTED_BY_MERCHANT);
			getModelService().save(order);
			getOrderBusinessProcessHelper().startOrderProcess(order);
			return Boolean.TRUE.toString();
		}
		throw new BcfOrderUpdateException(PENDING_ORDERS_LOCK_REQUIRED);
	}

	@Override
	public String acceptOrder(final OrderModel order, final String comments, final String agentId) throws BcfOrderUpdateException
	{
		getModelService().refresh(order);

		if (Objects.equals(OrderStatus.LOCKED, order.getStatus()))
		{
			setComments(order, comments, agentId);
			order.setStatus(OrderStatus.APPROVED_BY_MERCHANT);
			getModelService().saveAll();
			getModelService().refresh(order);
			getOrderBusinessProcessHelper().startOrderProcess(order);
			return Boolean.TRUE.toString();
		}
		throw new BcfOrderUpdateException(PENDING_ORDERS_LOCK_REQUIRED);
	}

	@Override
	public OrderModel getOrderByBookingReference(final String bookingReference, final UserModel customer)
	{
		return getBcfOrderDao().getOrderByBookingReference(bookingReference, customer);
	}

	/**
	 * Added an additional check, in case the on_request order is amended
	 * and financial data is not sent to BCF than we need to include all the entries for processing
	 *
	 * @param abstractOrderModel abstract order
	 * @return entries by entry type
	 */
	@Override
	public Map<OrderEntryType, List<AbstractOrderEntryModel>> getDeltaOrderEntries(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel instanceof OrderModel)
		{
			final OrderModel order = ((OrderModel) abstractOrderModel);
			if (!order.isCreateOrderFinancialDataSent())
			{
				LOG.info(String.format("Sending all order entries for processing for Declare Order for order [%s] status [%s]",
						order.getCode(), order.getStatus()));
				return StreamUtil.safeStream(abstractOrderModel.getEntries())
						.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
			}
		}
		LOG.info(String.format("Sending delta order entries for processing for Declare Order for order [%s] status [%s]",
				abstractOrderModel.getCode(), abstractOrderModel.getStatus()));
		return StreamUtil.safeStream(abstractOrderModel.getEntries()).filter
				(orderEntryModel -> (!AmendStatus.SAME.equals(orderEntryModel.getAmendStatus())))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
	}

	@Override
	public boolean isAmendOrderProcess(final AbstractOrderModel orderModel)
	{
		if (Objects.nonNull(orderModel.getOriginalOrder()))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean isRefundOrder(final List<PaymentTransactionEntryModel> paymentTransactionEntryModels)
	{
		if (paymentTransactionEntryModels.stream().anyMatch(
				paymentTransactionEntryModel -> PaymentTransactionType.REFUND.equals(paymentTransactionEntryModel.getType())))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public List<OrderModel> findOrdersForSpecificSailingDate(final Date startDate, final Date endDate)
	{
		return getBcfOrderDao().findOrdersForSpecificSailingDate(startDate, endDate);
	}

	@Override
	public void deActivateOrderEntries(final OrderModel order, final List<AbstractOrderEntryModel> entries)
	{
		if (CollectionUtils.isNotEmpty(entries))
		{
			entries.forEach(entry -> {
				entry.setActive(Boolean.FALSE);
				entry.setAmendStatus(AmendStatus.CHANGED);
			});
			getModelService().saveAll(entries);
			getModelService().refresh(order);
		}
	}

	@Override
	public Boolean updateEntryMarkers(final AbstractOrderModel order)
	{
		if (Objects.nonNull(order))
		{
			if (CollectionUtils.isNotEmpty(order.getEntries()))
			{
				order.getEntries().stream().forEach(entry -> {
					entry.setScheduleModified(Boolean.FALSE);
					entry.setVesselModified(Boolean.FALSE);
					entry.setModifiedFromEbooking(Boolean.FALSE);
					getModelService().save(entry);
				});
				getModelService().refresh(order);
				return true;
			}
			//nothing to update
			return false;
		}
		//nothing to update
		return false;
	}

	private void setComments(final OrderModel orderModel, final String comments, final String agentId)
	{

		final CommentModel commentModel = getModelService().create(CommentModel.class);
		final DomainModel domainModel = commentService.getDomainForCode(Config.getParameter("code.domain.model.asmagent"));
		final ComponentModel componentModel = commentService
				.getComponentForCode(domainModel, Config.getParameter("code.component.model.asmagent"));
		final CommentTypeModel commentTypeModel = commentService
				.getCommentTypeForCode(componentModel, Config.getParameter("code.comment.type.model.asmagent"));
		commentModel.setText(comments);
		commentModel.setAuthor(userService.getUserForUID(agentId));
		commentModel.setCommentType(commentTypeModel);
		commentModel.setComponent(componentModel);
		commentModel.setCreationtime(new Date());
		final List<CommentModel> commentList = new ArrayList<CommentModel>();
		commentList.add(commentModel);
		commentList.addAll(orderModel.getComments());
		orderModel.setComments(commentList);
	}

	protected BcfOrderDao getBcfOrderDao()
	{
		return bcfOrderDao;
	}

	@Required
	public void setBcfOrderDao(final BcfOrderDao bcfOrderDao)
	{
		this.bcfOrderDao = bcfOrderDao;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected <T extends CustomerAccountService> T getCustomerAccountService()
	{
		return (T) customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService service)
	{
		this.baseStoreService = service;
	}

	@Override
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	@Required
	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	public OrderBusinessProcessHelper getOrderBusinessProcessHelper()
	{
		return orderBusinessProcessHelper;
	}

	public void setOrderBusinessProcessHelper(final OrderBusinessProcessHelper orderBusinessProcessHelper)
	{
		this.orderBusinessProcessHelper = orderBusinessProcessHelper;
	}
}
