/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.order.impl.DefaultAccommodationCommerceCartService;
import de.hybris.platform.travelservices.services.GuestCountService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfAccommodationCommerceCartService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFCommerceAddToCartStrategy;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.OptionBookingUtil;



public class DefaultBcfAccommodationCommerceCartService extends DefaultAccommodationCommerceCartService implements
		BcfAccommodationCommerceCartService
{

	private BcfBookingService bcfBookingService;
	private GuestCountService guestCountService;
	private BCFTravelCartService bcfTravelCartService;
	private CommerceCartService commerceCartService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	private static final Logger LOG = Logger.getLogger(DefaultBcfAccommodationCommerceCartService.class);

	@Override
	public int getNumberOfEntryGroupsInCart()
	{
		if (BcfCoreConstants.BOOKING_ACCOMMODATION_ONLY
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			return super.getNumberOfEntryGroupsInCart();
		}

		final Object selectedJourneyRefNumber = getSessionService().getAttribute(BcfCoreConstants.SELECTED_JOURNEY_REF_NO);
		if(selectedJourneyRefNumber==null){
			return 0;
		}
		final DealOrderEntryGroupModel dealOrderEntryGroupModel = getBcfBookingService()
				.getAccommodationDealOrderEntryGroup(getCartService().getSessionCart(), (Integer)selectedJourneyRefNumber);
		return Objects.isNull(dealOrderEntryGroupModel) ? 0 : CollectionUtils.size(dealOrderEntryGroupModel.getAccommodationEntryGroups());
	}

	@Override
	public int getMaxRoomStayRefNumber(final Integer journeyRefNumber)
	{
		final CartModel sessionCart = getCartService().getSessionCart();

		final List<AccommodationOrderEntryGroupModel> entryGroups = getBcfBookingService()
				.getAccommodationOrderEntryGroups(sessionCart, journeyRefNumber);
		if (CollectionUtils.isEmpty(entryGroups))
		{
			return -1;
		}
		else
		{
			final OptionalInt maxRefNumberOptional = entryGroups.stream().mapToInt(AccommodationOrderEntryGroupModel::getRoomStayRefNumber)
					.max();
			return maxRefNumberOptional.isPresent() ? maxRefNumberOptional.getAsInt() : -1;
		}
	}


	@Override
	public void cleanupCartBeforeAddition(final String accommodationOfferingCode, final String checkInDateTime,
			final String checkOutDateTime)
	{
		// TODO add a validation while adding a room to the cart for Booking Accommodation Journey only and remove entries if not validated
		// TODO add a validation while adding a room to the cart for Booking Transport Accommodation Journey only and remove entries if not validated
	}

	@Override
	public List<AccommodationOrderEntryGroupModel> getNewDealOrderEntryGroups(final String accommodationOfferingCode,
			final String accommodationCode,
			final String ratePlanCode, final Integer journeyRefNumber)
	{
		if (!getCartService().hasSessionCart())
		{
			return Collections.emptyList();
		}
		else
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			final DealOrderEntryGroupModel dealOrderEntryGroup = getBcfBookingService()
					.getAccommodationDealOrderEntryGroup(sessionCart, journeyRefNumber);
			final List<AccommodationOrderEntryGroupModel> newAccommodationEntryGroups = new ArrayList<>();
			if (Objects.nonNull(dealOrderEntryGroup))
			{
				final List<AccommodationOrderEntryGroupModel> matchingAccommodationOrderEntryGroups = dealOrderEntryGroup.getAccommodationEntryGroups().stream().filter(entryGroup ->
						BooleanUtils.isTrue(entryGroup.getActive()) && StringUtils.equalsIgnoreCase(entryGroup.getAccommodation().getCode(), accommodationCode)
							&& StringUtils
							.equalsIgnoreCase(entryGroup.getAccommodationOffering().getCode(), accommodationOfferingCode)
							&& StringUtils.equalsIgnoreCase(entryGroup.getRatePlan().getCode(), ratePlanCode)
				).collect(Collectors.toList());

				matchingAccommodationOrderEntryGroups.forEach(entryGroup -> {
					final Optional optionalNotNewEntry = entryGroup.getEntries().stream().filter(entry ->
						!AmendStatus.NEW.equals(entry.getAmendStatus())
					).findAny();
					if (!optionalNotNewEntry.isPresent())
					{
						newAccommodationEntryGroups.add(entryGroup);
					}

				});

			}
			return newAccommodationEntryGroups;
		}
	}

	@Override
	public AccommodationOrderEntryGroupModel getNewAccommodationOrderEntryGroup(final List<AbstractOrderEntryModel> dealAccommodationEntries, final Date checkInDate, final Date checkOutDate, final String accOffCode, final String accCode, final String ratePlanCode, final int refNumber)
	{
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getModelService().create(AccommodationOrderEntryGroupModel.class);
		accommodationOrderEntryGroup.setRoomStayRefNumber(refNumber);
		accommodationOrderEntryGroup.setAccommodation((AccommodationModel)getProductService().getProductForCode(accCode));
		accommodationOrderEntryGroup.setAccommodationOffering(getAccommodationOfferingService().getAccommodationOffering(accOffCode));
		accommodationOrderEntryGroup.setRatePlan((RatePlanModel)getCategoryService().getCategoryForCode(ratePlanCode));
		accommodationOrderEntryGroup.setStartingDate(checkInDate);
		accommodationOrderEntryGroup.setEndingDate(checkOutDate);
		accommodationOrderEntryGroup.setDealAccommodationEntries(dealAccommodationEntries);
		if(Objects.equals(BookingJourneyType.BOOKING_PACKAGE,getCartService().getSessionCart().getBookingJourneyType()))
		{
			final List<GuestCountModel> guestCounts=getGuestCounts(getCartService().getSessionCart());
			accommodationOrderEntryGroup.setGuestCounts(guestCounts);
		}
		getModelService().save(accommodationOrderEntryGroup);
		return accommodationOrderEntryGroup;
	}

	protected List<GuestCountModel> getGuestCounts(final AbstractOrderModel abstractOrderModel)
	{
		final List<AbstractOrderEntryModel> cartEntries=abstractOrderModel.getEntries().stream().filter(entry->entry.getActive() && Objects.equals(OrderEntryType.TRANSPORT,entry.getType())).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(cartEntries))
		{
			return Collections.emptyList();
		}
		final List<TravellerModel> travellers=cartEntries.stream()
				.filter(entry-> (0==entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
				.flatMap(entry->entry.getTravelOrderEntryInfo()
						.getTravellers().stream()).collect(Collectors.toList());
		final List<TravellerModel> passengers= travellers.stream()
				.filter(traveller->Objects.equals(TravellerType.PASSENGER,traveller.getType())).collect(Collectors.toList());
		final List<PassengerTypeModel> passengerTypeModels=passengers.stream().map(passenger->((PassengerInformationModel)passenger.getInfo()).getPassengerType()).collect(Collectors.toList());

		final List<GuestCountModel> guestCounts=new ArrayList<>();
		for(final PassengerTypeModel passengerTypeModel:passengerTypeModels)
		{
			final GuestCountModel guestCountModel = getGuestCountService().getGuestCount(passengerTypeModel.getCode(),1);
			if(Objects.nonNull(guestCountModel)) {
				guestCounts.add(guestCountModel);
			}
		}
		return guestCounts;
	}

	@Override
	public void removeDealOrderEntryGroups(final List<DealOrderEntryGroupModel> accommodationDealEntryGroups)
	{
		  List<AbstractOrderEntryModel> otherEntries=null;

		for(final DealOrderEntryGroupModel entryGroup:accommodationDealEntryGroups){

			boolean removeGroup = true;

			if (entryGroup.getEntries().stream()
					.anyMatch(entry -> !entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType())))
			{

				removeGroup = false;
			}
			final List<AbstractOrderEntryModel> accommodationEntries = entryGroup.getEntries().stream()
					.filter(entry ->entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType())).collect(
							Collectors.toList());

			otherEntries = entryGroup.getEntries().stream()
					.filter(entry ->entry.getActive() && !OrderEntryType.ACCOMMODATION.equals(entry.getType())).collect(
							Collectors.toList());

			if (OptionBookingUtil.isOptionBooking(accommodationEntries))
			{
				getBcfOptionBookingNotificationService().startCancellationProcessForOptionBooking(accommodationEntries);
			}

			bcfTravelCommerceStockService.releaseStocks(accommodationEntries);

			getModelService().removeAll(accommodationEntries);

			if (removeGroup)
			{
				getModelService().removeAll(entryGroup);


				if (CollectionUtils.isNotEmpty(otherEntries))
				{
					otherEntries.forEach(entry -> {//NOSONAR
						getModelService().refresh(entry);

					});
				}
			}
		}






	}

	protected Boolean validateCartForDealEntryGroups(final String accommodationOfferingCode, final String checkInDateTime,
			final String checkOutDateTime,
			final List<DealOrderEntryGroupModel> dealEntryGroups)
	{
		final Date checkInDate = TravelDateUtils.convertStringDateToDate(checkInDateTime, "dd/MM/yyyy");
		final Date checkOutDate = TravelDateUtils.convertStringDateToDate(checkOutDateTime, "dd/MM/yyyy");
		return !CollectionUtils.isEmpty(dealEntryGroups) && checkInDate != null && checkOutDate != null ?
				dealEntryGroups.get(0).getAccommodationEntryGroups().stream().allMatch(entryGroup -> StringUtils.equalsIgnoreCase(entryGroup.getAccommodationOffering().getCode(), accommodationOfferingCode))
				 &&
						dealEntryGroups.get(0).getAccommodationEntryGroups().stream().allMatch(entryGroup -> DateUtils.isSameDay(checkInDate, entryGroup.getCheckInTime()))
						 && dealEntryGroups.get(0).getAccommodationEntryGroups().stream().allMatch(entryGroup -> DateUtils.isSameDay(checkOutDate, entryGroup.getEndingDate())):
				Boolean.TRUE;

	}

	@Override
	public Boolean removeAccommodationOrderEntryGroup(final int roomStayReference)
	{
		final CartModel cartModel = this.getCartService().getSessionCart();
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = this.getBookingService()
				.getAccommodationOrderEntryGroup(roomStayReference, cartModel);
		if (accommodationOrderEntryGroupModel == null)
		{
			LOG.error("AccommodationOrderEntryGroupModel not present for cart :"+cartModel.getCode() +" and room reference :"+roomStayReference);
			return Boolean.FALSE;
		}
		else
		{
			if (bcfTravelCartService.isAmendmentCart(cartModel))
			{
				inActivateOrRemoveAccommodationEntryGroup(accommodationOrderEntryGroupModel);
				try
				{
					final CommerceCartParameter recalcParam = new CommerceCartParameter();
					recalcParam.setEnableHooks(true);
					recalcParam.setCart(cartModel);
					commerceCartService.recalculateCart(recalcParam);
				}
				catch (final CalculationException e)
				{
					LOG.error("Problem occured while calculating the cart: "+cartModel.getCode() +" while trying to remove room reference :"+roomStayReference, e);
					return Boolean.FALSE;
				}
				this.normalizeRoomStayRefNumbers(cartModel);

			}
			else
			{
				if (OptionBookingUtil.isOptionBooking(cartModel))
				{
					final List<AbstractOrderEntryModel> modifiedOrderEntries = accommodationOrderEntryGroupModel.getEntries();
					getBcfOptionBookingNotificationService().startCancellationProcessForOptionBooking(modifiedOrderEntries);
				}
				this.removeAccommodationOrderEntryGroups(Collections.singletonList(accommodationOrderEntryGroupModel), 1);
				getModelService().refresh(cartModel);
				BCFCommerceAddToCartStrategy.class.cast(getCommerceAddToCartStrategy()).normalizeEntryNumbersIfNeeded(cartModel);
			}

			return Boolean.TRUE;
		}
	}

	@Override
	public void inActivateOrRemoveAccommodationEntryGroup(final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{
		final List<ItemModel> itemsToSave=new ArrayList<>();
		final List<AbstractOrderEntryModel> entriesToRemove=new ArrayList<>();


			final List<AbstractOrderEntryModel> entries = accommodationOrderEntryGroupModel.getEntries();
			if (CollectionUtils.isNotEmpty(entries))
			{
				for (final AbstractOrderEntryModel entry : entries)
				{
					if (entry.getActive() && (entry.getAmendStatus().equals(AmendStatus.NEW) || entry.getAmendStatus()
							.equals(AmendStatus.CHANGED)))
					{
						entriesToRemove.add(entry);
					}
					else
					{
						entry.setActive(false);
						entry.setAmendStatus(AmendStatus.CHANGED);
						itemsToSave.add(entry);
					}

				}


			}

		final int entriesToRemoveSize=entriesToRemove.size();
		final int entriesSize=entries.size();

		if (CollectionUtils.isNotEmpty(entriesToRemove))
		{
			bcfTravelCommerceStockService.releaseStocks(entriesToRemove);
			getModelService().removeAll(entriesToRemove);
		}

		if(entriesSize==entriesToRemoveSize){
			getModelService().remove(accommodationOrderEntryGroupModel);
		}else if(entriesSize==itemsToSave.size()){
			accommodationOrderEntryGroupModel.setActive(false);
			itemsToSave.add(accommodationOrderEntryGroupModel);

		}
		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}

	}

	@Override
	public void inActivateOrRemoveAccommodationEntries(final List<AbstractOrderEntryModel> entries)
	{

		if(CollectionUtils.isNotEmpty(entries))
		{
			final List<ItemModel> itemsToSave = new ArrayList<>();
			final List<AbstractOrderEntryModel> entriesToRemove = new ArrayList<>();
			final List<AccommodationOrderEntryGroupModel> entryGroupsToRemove = new ArrayList<>();
			final List<AbstractOrderEntryGroupModel> entryGroupsToRefresh = new ArrayList<>();
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels=entries.stream().map(entry-> bcfTravelCommerceCartService
					.getAccommodationOrderEntryGroup(entry)).distinct().collect(Collectors.toList());

			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry.getActive() && (entry.getAmendStatus().equals(AmendStatus.NEW) || entry.getAmendStatus()
						.equals(AmendStatus.CHANGED)))
				{
					entriesToRemove.add(entry);
				}
				else
				{
					entry.setActive(false);
					entry.setAmendStatus(AmendStatus.CHANGED);
					itemsToSave.add(entry);
				}

			}

			for(final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel :accommodationOrderEntryGroupModels){

				if(CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getDealAccommodationEntries())){
					if(entriesToRemove.containsAll(accommodationOrderEntryGroupModel.getDealAccommodationEntries())){
						entryGroupsToRemove.add(accommodationOrderEntryGroupModel);
						entryGroupsToRefresh
								.add(accommodationOrderEntryGroupModel.getDealAccommodationEntries().iterator().next().getEntryGroup());

					}else if(itemsToSave.containsAll(accommodationOrderEntryGroupModel.getDealAccommodationEntries())){
						accommodationOrderEntryGroupModel.setActive(false);
						itemsToSave.add(accommodationOrderEntryGroupModel);
					}
				}else{
					if(entriesToRemove.containsAll(accommodationOrderEntryGroupModel.getEntries())){
						entryGroupsToRemove.add(accommodationOrderEntryGroupModel);
						entryGroupsToRefresh.add(accommodationOrderEntryGroupModel.getEntries().iterator().next().getEntryGroup());
					}else if(itemsToSave.containsAll(accommodationOrderEntryGroupModel.getEntries())){
						accommodationOrderEntryGroupModel.setActive(false);
						itemsToSave.add(accommodationOrderEntryGroupModel);
					}

				}

			}


			if (CollectionUtils.isNotEmpty(entriesToRemove))
			{

				bcfTravelCommerceStockService.releaseStocks(entriesToRemove);
				getModelService().removeAll(entriesToRemove);
			}
			if (CollectionUtils.isNotEmpty(entryGroupsToRemove))
			{

				getModelService().removeAll(entryGroupsToRemove);

				for (final AbstractOrderEntryGroupModel entryGroupToRefresh : entryGroupsToRefresh)
				{
					getModelService().refresh(entryGroupToRefresh);

				}


			}

			if (CollectionUtils.isNotEmpty(itemsToSave))
			{
				getModelService().saveAll(itemsToSave);
			}
		}
	}



	@Override
	public void removeAccommodationOrderEntryGroups(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups, final int numberToRemove) {
		if (numberToRemove == CollectionUtils.size(accommodationOrderEntryGroups)) {
			this.removeEntryGroupsFromCart(accommodationOrderEntryGroups);
		} else {
			final int listSize = CollectionUtils.size(accommodationOrderEntryGroups);
			final List<AccommodationOrderEntryGroupModel> entryGroupsToRemove = accommodationOrderEntryGroups.subList(listSize - numberToRemove, listSize);
			this.removeEntryGroupsFromCart(entryGroupsToRemove);
		}

		final CartModel sessionCart = this.getCartService().getSessionCart();


			getModelService().refresh(sessionCart);


		this.getCommerceCartCalculationStrategy().calculateCart(sessionCart);
		this.normalizeRoomStayRefNumbers(sessionCart);
	}



	@Override
	public void removeAccommodationOrderEntriesForDeal(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		if (CollectionUtils.isEmpty(abstractOrderEntries))
		{
			return;
		}

		final List<AccommodationOrderEntryInfoModel> accommodationOrderEntryInfoModels = abstractOrderEntries.stream()
				.map(AbstractOrderEntryModel::getAccommodationOrderEntryInfo).distinct().collect(
						Collectors.toList());
		getModelService().removeAll(accommodationOrderEntryInfoModels);
		getModelService().removeAll(abstractOrderEntries);
	}

	@Override
	protected void removeEntryGroupsFromCart(final List<AccommodationOrderEntryGroupModel> entryGroupsToRemove)
	{

		if (CollectionUtils.isNotEmpty(entryGroupsToRemove))
		{

			final List<AbstractOrderEntryModel> entries = entryGroupsToRemove.stream()
					.flatMap(entryGroup -> entryGroup.getEntries().stream()).collect(Collectors.toList());
			bcfTravelCommerceStockService.releaseStocks(entries);
			this.getModelService().removeAll(entries);
			this.getModelService().removeAll(entryGroupsToRemove);
		}

	}


	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected GuestCountService getGuestCountService() {
		return guestCountService;
	}

	@Required
	public void setGuestCountService(final GuestCountService guestCountService) {
		this.guestCountService = guestCountService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}


	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}
}
