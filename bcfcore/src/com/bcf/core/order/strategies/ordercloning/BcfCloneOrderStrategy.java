/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.strategies.ordercloning;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;


public interface BcfCloneOrderStrategy
{

	/**
	 * Gets the new cart from order.
	 *
	 * @param order the order
	 * @return the new cart from order
	 */
	CartModel getNewCartFromOrder(OrderModel order);

	/**
	 * Creates the history snapshot.
	 *
	 * @param originalOrder the original order
	 * @return the order model
	 */
	OrderModel createHistorySnapshot(OrderModel originalOrder);

	/**
	 * Creates the history snapshot.
	 *
	 * @param originalOrder the original order
	 * @return the order model
	 */
	OrderModel createHistorySnapshot(OrderModel sanpshotOrder, OrderModel newOrder);

	/**
	 * Creates the new order history entry.
	 *
	 * @param snapshot      the snapshot
	 * @param originalOrder the original order
	 * @return the order history entry model
	 */
	OrderHistoryEntryModel createNewOrderHistoryEntry(OrderModel snapshot, OrderModel originalOrder);


	void cloneOrderRelatedAttributes(AbstractOrderModel abstractOrder, AbstractOrderModel originalOrder);

	void cloneOrderTravelRelatedAttributes(final AbstractOrderModel orderModel, final AbstractOrderModel abstractOrderModel);

	void cloneCartTravelRelatedAttributes(final AbstractOrderModel cartModel, final AbstractOrderModel abstractOrderModel);

	void cloneActivityOrderEntryInfo(AbstractOrderModel clonedOrder);

	void cloneAbstractOrderEntryGroup(final AbstractOrderModel originalOrder, final AbstractOrderModel clonedOrder);

	AbstractOrderModel cloneCart(final AbstractOrderModel abstractOrderModel);
}
