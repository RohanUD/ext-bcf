/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.strategies.ordercloning.impl;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.strategies.ordercloning.CloneAbstractOrderStrategy;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.dao.AbstractOrderEntryGroupDao;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialRequestDetailModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.user.TravellerPreferenceModel;
import de.hybris.platform.travelservices.services.BookingService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class DefaultBcfCloneOrderStrategy implements BcfCloneOrderStrategy
{
	private ModelService modelService;
	private CloneAbstractOrderStrategy cloneAbstractOrderStrategy;
	private KeyGenerator orderCodeGenerator;
	private TimeService timeService;
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;
	private BookingService bookingService;
	private OrderHistoryService orderHistoryService;
	private PersistentKeyGenerator refundCodeGenerator;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private AbstractOrderEntryGroupDao abstractOrderEntryGroupDao;

	@Override
	public CartModel getNewCartFromOrder(final OrderModel orderModel)
	{
		final CartModel cartModel = getCloneAbstractOrderStrategy().clone(null, null, orderModel,
				getOrderCodeGenerator().generate().toString(), CartModel.class, CartEntryModel.class);

		cloneOrderRelatedAttributes(cartModel, orderModel);
		cloneOrderTravelRelatedAttributes(cartModel, orderModel);
		this.cloneAbstractOrderEntryGroup(orderModel, cartModel);
		getCommerceCartCalculationStrategy().calculateCart(cartModel);

		return cartModel;
	}

	@Override
	public OrderModel createHistorySnapshot(final OrderModel originalOrder)
	{
		final OrderModel snapshot = getOrderHistoryService().createHistorySnapshot(originalOrder);
		snapshot.setStatus(OrderStatus.AMENDED);
		cloneOrderHistoryEntries(snapshot, originalOrder);
		getOrderHistoryService().saveHistorySnapshot(snapshot);

		cloneOrderRelatedAttributes(snapshot, originalOrder);
		cloneCartTravelRelatedAttributes(snapshot, originalOrder);


		getModelService().save(snapshot);
		getModelService().detachAll();
		return snapshot;
	}

	@Override
	public OrderModel createHistorySnapshot(final OrderModel sanpshotOrder, final OrderModel newOrder)
	{
		final OrderModel snapshot = getOrderHistoryService().createHistorySnapshot(sanpshotOrder);
		newOrder.setCode(sanpshotOrder.getCode());
		newOrder.setEBookingVersion(sanpshotOrder.getEBookingVersion());
		snapshot.setStatus(OrderStatus.AMENDED);
		cloneOrderHistoryEntries(snapshot, sanpshotOrder);
		cloneGoodWillRefundAndInternalComments(snapshot, sanpshotOrder);
		newOrder.setOriginalOrder(snapshot);
		createNewOrderHistoryEntry(snapshot, newOrder);

		getOrderHistoryService().saveHistorySnapshot(snapshot);
		getBcfTravelCommerceCartService().removeAllTravelOrderEntries(sanpshotOrder.getEntries());
		getModelService().remove(sanpshotOrder);
		getModelService().save(snapshot);

		getModelService().detachAll();
		return snapshot;
	}


	/**
	 * Clone order related attributes.
	 *
	 * @param abstractOrder the abstract order
	 * @param originalOrder the original order
	 */
	@Override
	public void cloneOrderRelatedAttributes(final AbstractOrderModel abstractOrder, final AbstractOrderModel originalOrder)
	{
		if (originalOrder instanceof OrderModel)
		{
			abstractOrder.setOriginalOrder((OrderModel) originalOrder);
		}
		abstractOrder.setBillingTime(originalOrder.getBillingTime());

		final Date currentTime = getTimeService().getCurrentTime();
		abstractOrder.setDate(currentTime);

		if (Objects.nonNull(originalOrder.getDeliveryAddress()))
		{
			abstractOrder.setDeliveryAddress(getModelService().clone(originalOrder.getDeliveryAddress()));
		}
		else if (Objects.nonNull(originalOrder.getPaymentAddress()))
		{
			abstractOrder.setDeliveryAddress(getModelService().clone(originalOrder.getPaymentAddress()));
		}

		if (Objects.nonNull(originalOrder.getPaymentAddress()))
		{
			abstractOrder.setPaymentAddress(getModelService().clone(originalOrder.getPaymentAddress()));
		}
		if (Objects.nonNull(originalOrder.getPaymentInfo()))
		{
			abstractOrder.setPaymentInfo(getModelService().clone(originalOrder.getPaymentInfo()));
		}

		// If the modified order's payment info has been removed, the owner attribute will be null after clone.
		if (Objects.nonNull(abstractOrder.getPaymentAddress()) && Objects.nonNull(abstractOrder.getPaymentInfo()) &&
				Objects.nonNull(abstractOrder.getPaymentInfo().getBillingAddress()) && Objects
				.isNull(abstractOrder.getPaymentInfo().getBillingAddress().getOwner()))
		{
			abstractOrder.getPaymentInfo().setBillingAddress(abstractOrder.getPaymentAddress());
		}
		cloneGoodWillRefundAndInternalComments(abstractOrder, originalOrder);

		abstractOrder.setCreateOrderNotificationSent(originalOrder.isCreateOrderNotificationSent());
		abstractOrder.setCreateOrderFinancialDataSent(originalOrder.isCreateOrderFinancialDataSent());
	}

	private void cloneGoodWillRefundAndInternalComments(final AbstractOrderModel abstractOrder,
			final AbstractOrderModel originalOrder)
	{
		if (CollectionUtils.isNotEmpty(originalOrder.getGoodwillRefundOrderInfos()))
		{
			final List<GoodWillRefundOrderInfoModel> goodwillRefundOrderInfos = new ArrayList<>();

			for (final GoodWillRefundOrderInfoModel goodwillRefundOrderInfo : originalOrder.getGoodwillRefundOrderInfos())
			{
				final GoodWillRefundOrderInfoModel clonedGoodwillRefundOrderInfo = getModelService().clone(goodwillRefundOrderInfo);
				clonedGoodwillRefundOrderInfo.setCode(String.valueOf(refundCodeGenerator.generate()));
				clonedGoodwillRefundOrderInfo.setOrder(abstractOrder);
				goodwillRefundOrderInfos.add(clonedGoodwillRefundOrderInfo);
			}

			abstractOrder.setGoodwillRefundOrderInfos(goodwillRefundOrderInfos);
		}

	}

	/**
	 * Clone travel related attributes.
	 *
	 * @param cartModel          the abstract order
	 * @param abstractOrderModel the order model
	 */
	@Override
	public void cloneOrderTravelRelatedAttributes(final AbstractOrderModel cartModel, final AbstractOrderModel abstractOrderModel)
	{

		final Map<TravellerModel, TravellerModel> clonedTravellerModelMap = cloneTravellers(cartModel, cartModel.getCode());

		if (MapUtils.isNotEmpty(clonedTravellerModelMap))
		{
			cartModel.getEntries().forEach(entry -> {
				cloneTravelOrderEntryInfo(entry, clonedTravellerModelMap);
				entry.setAmendStatus(AmendStatus.SAME);
			});
		}

	}

	@Override
	public void cloneCartTravelRelatedAttributes(final AbstractOrderModel orderModel, final AbstractOrderModel abstractOrderModel)
	{

		final Map<TravellerModel, TravellerModel> clonedTravellerModelMap = cloneTravellers(orderModel, orderModel.getCode());

		if (MapUtils.isNotEmpty(clonedTravellerModelMap))
		{
			orderModel.getEntries().forEach(entry -> {
				cloneTravelOrderEntryInfo(entry, clonedTravellerModelMap);

			});
		}
	}


	/**
	 * Clone travellers.
	 *
	 * @param abstractOrder the abstract order
	 * @param orderCode     the order code
	 * @return the map
	 */
	protected Map<TravellerModel, TravellerModel> cloneTravellers(final AbstractOrderModel abstractOrder, final String orderCode)
	{
		final Map<TravellerModel, TravellerModel> clonedTravellersMap = new HashMap<>();
		final Set<TravellerModel> travellers = abstractOrder.getEntries().stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravellers()))
				.flatMap(entry -> entry.getTravelOrderEntryInfo().getTravellers().stream()).collect(Collectors.toSet());

		for (final TravellerModel traveller : travellers)
		{
			final String travellerVersionId = getOrderCodeGenerator().generate() + "-"
					+ ((abstractOrder instanceof OrderModel) ? ((OrderModel) abstractOrder).getVersionID() : orderCode);
			cloneTraveller(traveller, travellerVersionId, clonedTravellersMap);
		}
		return clonedTravellersMap;
	}

	/**
	 * Clone traveller.
	 *
	 * @param traveller           the traveller
	 * @param travellerVersionId  the travellerVersionId
	 * @param clonedTravellersMap the cloned travellers map
	 */
	protected void cloneTraveller(final TravellerModel traveller, final String travellerVersionId,
			final Map<TravellerModel, TravellerModel> clonedTravellersMap)
	{
		final TravellerModel clonedTraveller = getModelService().clone(traveller, TravellerModel.class);

		if (Objects.nonNull(traveller.getInfo()))
		{
			if (traveller.getInfo() instanceof PassengerInformationModel)
			{
				clonedTraveller.setInfo(getModelService().clone(traveller.getInfo(), PassengerInformationModel.class));
			}
			else
			{
				clonedTraveller.setInfo(getModelService().clone(traveller.getInfo()));
			}
		}

		if (Objects.nonNull(traveller.getSpecialRequestDetail()))
		{
			clonedTraveller.setSpecialRequestDetail(
					getModelService().clone(traveller.getSpecialRequestDetail(), SpecialRequestDetailModel.class));
		}

		if (CollectionUtils.isNotEmpty(traveller.getTravellerPreference()))
		{
			final Collection<TravellerPreferenceModel> clonedTravellerPreference = new LinkedList<>();
			traveller.getTravellerPreference().forEach(
					travellerPreferenceModel -> clonedTravellerPreference.add(getModelService().clone(travellerPreferenceModel)));
			clonedTraveller.setTravellerPreference(clonedTravellerPreference);
		}

		clonedTraveller.setTravelOrderEntryInfo(Collections.emptyList());

		clonedTraveller.setVersionID(travellerVersionId);

		clonedTravellersMap.put(traveller, clonedTraveller);
	}

	/**
	 * Clone travel order entry info.
	 *
	 * @param orderEntry              the order entry
	 * @param clonedTravellerModelMap the cloned traveller model map
	 */
	protected void cloneTravelOrderEntryInfo(final AbstractOrderEntryModel orderEntry,
			final Map<TravellerModel, TravellerModel> clonedTravellerModelMap)
	{
		if (OrderEntryType.TRANSPORT.equals(orderEntry.getType()))
		{
			final TravelOrderEntryInfoModel clonedInfo = getModelService().clone(orderEntry.getTravelOrderEntryInfo(),
					TravelOrderEntryInfoModel.class);

			cloneTravelOrderEntryInfoAttributes(clonedInfo, orderEntry.getTravelOrderEntryInfo());

			if (CollectionUtils.isEmpty(clonedInfo.getTravellers()))
			{
				clonedInfo.setTravellers(null);
			}
			else
			{
				final List<TravellerModel> newTravellers = new LinkedList<>();
				clonedInfo.getTravellers().forEach(travellerModel -> newTravellers.add(clonedTravellerModelMap.get(travellerModel)));
				clonedInfo.setTravellers(newTravellers);
			}

			orderEntry.setTravelOrderEntryInfo(clonedInfo);
		}

	}

	/**
	 * Clone travel order entry info attributes.
	 *
	 * @param clonedInfo           the cloned info
	 * @param travelOrderEntryInfo the travel order entry info
	 */
	protected void cloneTravelOrderEntryInfoAttributes(final TravelOrderEntryInfoModel clonedInfo,
			final TravelOrderEntryInfoModel travelOrderEntryInfo)
	{
		if (Objects.nonNull(travelOrderEntryInfo.getSpecialRequestDetail()))
		{
			clonedInfo.setSpecialRequestDetail(
					getModelService().clone(travelOrderEntryInfo.getSpecialRequestDetail(), SpecialRequestDetailModel.class));
		}

		if (CollectionUtils.isNotEmpty(travelOrderEntryInfo.getComments()))
		{
			final List<CommentModel> clonedComments = new LinkedList<>();
			travelOrderEntryInfo.getComments()
					.forEach(comment -> clonedComments.add(getModelService().clone(comment, CommentModel.class)));
			clonedInfo.setComments(clonedComments);
		}
	}

	@Override
	public void cloneAbstractOrderEntryGroup(final AbstractOrderModel originalOrder, final AbstractOrderModel clonedOrder)
	{
		final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups = getAbstractOrderEntryGroupDao()
				.findAbstractOrderEntryGroups(originalOrder);
		final List<ItemModel> itemsToSave = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(abstractOrderEntryGroups))
		{
			abstractOrderEntryGroups.forEach(originalEntryGroup -> {
				final AbstractOrderEntryGroupModel clonedEntryGroup = getModelService()
						.clone(originalEntryGroup);

				if (clonedEntryGroup instanceof DealOrderEntryGroupModel)
				{
					final Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = ((DealOrderEntryGroupModel) clonedEntryGroup)
							.getAccommodationEntryGroups();

					setDealAccommodationEntriesOnClonedDealEntryGroup(clonedOrder, accommodationOrderEntryGroupModels);
				}
				itemsToSave.add(clonedEntryGroup);
				clonedOrder.getEntries().stream().filter(entry -> originalEntryGroup.equals(entry.getEntryGroup()))
						.forEach(entry ->
								entry.setEntryGroup(clonedEntryGroup)
						);
			});
		}

		for (final AbstractOrderEntryModel entry : clonedOrder.getEntries())
		{
			if (entry.getAccommodationOrderEntryInfo() != null)
			{
				final AccommodationOrderEntryInfoModel accommodationOrderEntryInfoModel = originalOrder.getEntries().stream()
						.filter(originalEntry -> originalEntry.getEntryNumber() == entry.getEntryNumber()).findAny().get()
						.getAccommodationOrderEntryInfo();

				if (accommodationOrderEntryInfoModel != null)
				{
					entry.setAccommodationOrderEntryInfo(getModelService()
							.clone(accommodationOrderEntryInfoModel));
				}
			}

			if (entry.getActivityOrderEntryInfo() != null)
			{
				final Optional<AbstractOrderEntryModel> any = originalOrder.getEntries().stream()
						.filter(originalEntry -> originalEntry.getEntryNumber() == entry.getEntryNumber()).findAny();

				ActivityOrderEntryInfoModel activityOrderEntryInfoModel = null;
				if (any.isPresent())
				{
					activityOrderEntryInfoModel = any.get().getActivityOrderEntryInfo();
				}

				if (activityOrderEntryInfoModel != null)
				{
					entry.setActivityOrderEntryInfo(getModelService()
							.clone(activityOrderEntryInfoModel));

				}
			}

		}
		getModelService().saveAll(clonedOrder.getEntries());


	}



	private void setDealAccommodationEntriesOnClonedDealEntryGroup(final AbstractOrderModel clonedOrder,
			final Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels)
	{
		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{

			accommodationOrderEntryGroupModels.forEach(accommodationOrderEntryGroupModel -> {

				if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getDealAccommodationEntries()))
				{

					final List<Integer> entryNumbers = accommodationOrderEntryGroupModel.getDealAccommodationEntries().stream()
							.map(entry -> entry.getEntryNumber()).collect(
									Collectors.toList());
					accommodationOrderEntryGroupModel.setDealAccommodationEntries(
							clonedOrder.getEntries().stream().filter(entry -> entryNumbers.contains(entry.getEntryNumber())).collect(
									Collectors.toList()));
				}

			});

		}
	}


	/**
	 * Clone order history entries.
	 *
	 * @param snapshot      the snapshot
	 * @param originalOrder the original order
	 */
	protected void cloneOrderHistoryEntries(final OrderModel snapshot, final OrderModel originalOrder)
	{
		if (CollectionUtils.isNotEmpty(originalOrder.getHistoryEntries()))
		{
			final List<OrderHistoryEntryModel> originalOrderHistoryEntries = originalOrder.getHistoryEntries();
			final List<OrderHistoryEntryModel> snapShotHistoryEntries = new ArrayList<>(originalOrderHistoryEntries.size());
			originalOrderHistoryEntries.forEach(orderHistoryEntryModel ->
					snapShotHistoryEntries.add(getModelService().clone(orderHistoryEntryModel))
			);
			snapshot.setHistoryEntries(snapShotHistoryEntries);
		}
	}

	@Override
	public OrderHistoryEntryModel createNewOrderHistoryEntry(final OrderModel snapshot, final OrderModel originalOrder)
	{
		final OrderHistoryEntryModel entry = getModelService().create(OrderHistoryEntryModel.class);
		entry.setTimestamp(getTimeService().getCurrentTime());
		entry.setOrder(originalOrder);
		entry.setDescription("Amended order on "
				+ TravelDateUtils.convertDateToStringDate(entry.getTimestamp(), TravelservicesConstants.DATE_TIME_PATTERN));
		entry.setPreviousOrderVersion(snapshot);
		return entry;
	}

	@Override
	public void cloneActivityOrderEntryInfo(final AbstractOrderModel clonedOrder)
	{
		clonedOrder.getEntries().forEach(entry -> {
			if (OrderEntryType.ACTIVITY.equals(entry.getType()))
			{
				final ActivityOrderEntryInfoModel clonedInfo = getModelService()
						.clone(entry.getActivityOrderEntryInfo(), ActivityOrderEntryInfoModel.class);
				entry.setActivityOrderEntryInfo(clonedInfo);
			}

		});
	}

	@Override
	public AbstractOrderModel cloneCart(final AbstractOrderModel abstractOrderModel)
	{
		final CartModel cartModel = getCloneAbstractOrderStrategy().clone(null, null, abstractOrderModel,
				abstractOrderModel.getCode() + "_cloned" + "_" + String.valueOf(System.currentTimeMillis()), CartModel.class,
				CartEntryModel.class);

		cloneOrderRelatedAttributes(cartModel, abstractOrderModel);
		cloneOrderTravelRelatedAttributes(cartModel, abstractOrderModel);
		this.cloneAbstractOrderEntryGroup(abstractOrderModel, cartModel);

		return cartModel;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the cloneAbstractOrderStrategy
	 */
	protected CloneAbstractOrderStrategy getCloneAbstractOrderStrategy()
	{
		return cloneAbstractOrderStrategy;
	}

	/**
	 * @param cloneAbstractOrderStrategy the cloneAbstractOrderStrategy to set
	 */
	@Required
	public void setCloneAbstractOrderStrategy(final CloneAbstractOrderStrategy cloneAbstractOrderStrategy)
	{
		this.cloneAbstractOrderStrategy = cloneAbstractOrderStrategy;
	}

	/**
	 * @return the orderCodeGenerator
	 */
	protected KeyGenerator getOrderCodeGenerator()
	{
		return orderCodeGenerator;
	}

	/**
	 * @param orderCodeGenerator the orderCodeGenerator to set
	 */
	@Required
	public void setOrderCodeGenerator(final KeyGenerator orderCodeGenerator)
	{
		this.orderCodeGenerator = orderCodeGenerator;
	}

	/**
	 * Gets time service.
	 *
	 * @return the time service
	 */
	protected TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * Sets time service.
	 *
	 * @param timeService the time service
	 */
	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * @return the commerceCartCalculationStrategy
	 */
	protected CommerceCartCalculationStrategy getCommerceCartCalculationStrategy()
	{
		return commerceCartCalculationStrategy;
	}

	/**
	 * @param commerceCartCalculationStrategy the commerceCartCalculationStrategy to set
	 */
	@Required
	public void setCommerceCartCalculationStrategy(final CommerceCartCalculationStrategy commerceCartCalculationStrategy)
	{
		this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
	}

	/**
	 * @return the bookingService
	 */
	protected BookingService getBookingService()
	{
		return bookingService;
	}

	/**
	 * @param bookingService the bookingService to set
	 */
	@Required
	public void setBookingService(final BookingService bookingService)
	{
		this.bookingService = bookingService;
	}

	/**
	 * @return the orderHistoryService
	 */
	protected OrderHistoryService getOrderHistoryService()
	{
		return orderHistoryService;
	}

	/**
	 * @param orderHistoryService the orderHistoryService to set
	 */
	@Required
	public void setOrderHistoryService(final OrderHistoryService orderHistoryService)
	{
		this.orderHistoryService = orderHistoryService;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	protected AbstractOrderEntryGroupDao getAbstractOrderEntryGroupDao()
	{
		return this.abstractOrderEntryGroupDao;
	}

	@Required
	public void setAbstractOrderEntryGroupDao(final AbstractOrderEntryGroupDao abstractOrderEntryGroupDao)
	{
		this.abstractOrderEntryGroupDao = abstractOrderEntryGroupDao;
	}

	public PersistentKeyGenerator getRefundCodeGenerator()
	{
		return refundCodeGenerator;
	}

	public void setRefundCodeGenerator(final PersistentKeyGenerator refundCodeGenerator)
	{
		this.refundCodeGenerator = refundCodeGenerator;
	}

}
