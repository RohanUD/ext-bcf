/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.strategies.ordercloning.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.impl.DefaultCreateOrderFromCartStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;


public class DefaultBcfCreateOrderFromCartStrategy extends DefaultCreateOrderFromCartStrategy
{
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private ModelService modelService;



	@Override
	public OrderModel createOrderFromCart(final CartModel cart) throws InvalidCartException
	{
		final OrderModel order = super.createOrderFromCart(cart);
		getBcfCloneOrderStrategy().cloneActivityOrderEntryInfo(order);
		getBcfCloneOrderStrategy().cloneOrderRelatedAttributes(order, cart);
		getBcfCloneOrderStrategy().cloneCartTravelRelatedAttributes(order, cart);
		getBcfCloneOrderStrategy().cloneAbstractOrderEntryGroup(cart, order);
		getModelService().save(order);
		return order;
	}

	protected BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
