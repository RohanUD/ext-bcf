/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.strategies.update.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.configurablebundleservices.constants.ConfigurableBundleServicesConstants;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerInfoModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.tx.Transaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.amend.order.helper.AbstractOrderEntriesHelper;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.order.strategies.update.BcfUpdateOrderFromEBookingStrategy;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.common.data.Product;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.booking.response.Booking;
import com.bcf.integrations.search.booking.response.Itinerary;


public class DefaultBcfUpdateOrderFromEBookingStrategy implements BcfUpdateOrderFromEBookingStrategy
{
	private ModelService modelService;
	private ProductService productService;
	private CatalogVersionService catalogVersionService;
	private CalculationService calculationService;
	private AbstractOrderEntriesHelper abstractOrderEntriesHelper;
	private UserService userService;
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private OrderService orderService;

	private static final Logger LOG = Logger.getLogger(DefaultBcfUpdateOrderFromEBookingStrategy.class);

	@Override
	public void updateExistingOrder(final OrderModel order, final SearchBookingResponseDTO searchBookingResponseDTO)
			throws BcfOrderUpdateException, CalculationException
	{
		if (CollectionUtils.isEmpty(searchBookingResponseDTO.getSearchResult()))
		{
			throw new BcfOrderUpdateException("search booking response has no results for " + order.getCode());
		}

		final Map<String, List<Itinerary>> eItinerariesGroupByBookingRef = searchBookingResponseDTO.getSearchResult().stream()
				.collect(Collectors.groupingBy(itinerary -> itinerary.getBooking().getBookingReference().getBookingReference()));

		final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByBookingRef = order.getEntries().stream()
				.filter(orderEntry -> Objects.nonNull(orderEntry.getProduct())
						&& Objects.nonNull(orderEntry.getTravelOrderEntryInfo()) && Objects.nonNull(orderEntry.getActive())
						&& orderEntry.getActive())
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getBookingReference));

		//remove order entries which are not in EBooking
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();

			//add/update order entries according to EBooking
			updateOrderEntriesFromEBooking(order, eItinerariesGroupByBookingRef, orderEntriesGroupByBookingRef);

			final UserModel user = order.getUser();

			if (CollectionUtils.isNotEmpty(searchBookingResponseDTO.getSearchResult()))
			{
				final Itinerary itinerary = searchBookingResponseDTO.getSearchResult().stream()
						.filter(itineraryData -> itineraryData.getBooking() != null).findFirst().orElse(null);
				if (itinerary != null)
				{
					updateUserSystemIdentifier(itinerary.getBooking(), user);
				}
			}

			//save all changes
			getModelService().saveAll();
			getModelService().refresh(order);

			getCalculationService().calculate(order);
			tx.commit();
		}
		catch (final BcfOrderUpdateException | CalculationException ex)
		{
			tx.rollback();
			throw ex;
		}
	}

	@Override
	public void updateVersionedOrder(final OrderModel order, final SearchBookingResponseDTO response)
			throws CalculationException
	{
		//Get Transport & Active entries from original order
		final Map<String, List<AbstractOrderEntryModel>> originalTransportEntriesByBookingRef = order.getEntries().stream()
				.filter(orderEntryModel -> (OrderEntryType.TRANSPORT.equals(orderEntryModel.getType())))
				.filter(orderEntryModel -> orderEntryModel.getActive())
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getBookingReference));

		final Map<String, List<String>> originalTransportProductsByBookingRef = new HashMap<String, List<String>>();
		for (final String bookingReference : originalTransportEntriesByBookingRef.keySet())
		{
			final List<AbstractOrderEntryModel> orderEntries = originalTransportEntriesByBookingRef.get(bookingReference);

			final List<String> bookingTransportProducts = new ArrayList<String>();
			for (final AbstractOrderEntryModel orderEntryModel : orderEntries)
			{
				final String commodityCode = getEntryProductCode(orderEntryModel);
				bookingTransportProducts.add(commodityCode);
			}
			originalTransportProductsByBookingRef.put(bookingReference, bookingTransportProducts);
		}

		//Group e-booking response based on Booking Ref and list of products
		final Map<String, List<String>> ebookingReferenceProducts = new HashMap<String, List<String>>();
		final Map<String, Map<String, String>> ebookingReferenceProductCategory = new HashMap<String, Map<String, String>>();
		final Map<String, Boolean> waitListedBookingReferenceList = new HashMap<String, Boolean>();
		final Map<String, Itinerary> bookingReferenceItineraryList = new HashMap<String, Itinerary>();

		final List<Itinerary> searchResult = response.getSearchResult();
		if (CollectionUtils.isNotEmpty(searchResult))
		{
			for (final Itinerary itinerary : searchResult)
			{
				final String bookingReference = itinerary.getBooking().getBookingReference().getBookingReference();

				final List<ProductFare> productsFromEbooking = itinerary.getSailing().getLine().get(0).getSailingPrices()
						.get(0).getProductFares();

				if(CollectionUtils.isNotEmpty(productsFromEbooking))
				{
					final List<Product> products = productsFromEbooking.stream().map(ProductFare::getProduct)
							.collect(Collectors.toList());
					final List<String> productCodes = products.stream().map(Product::getProductId).collect(Collectors.toList());
					final Map<String, String> productCategoryMap = new HashMap<String, String>();
					for (final Product product : products)
					{
						productCategoryMap.put(product.getProductId(), product.getProductCategory());
					}

					ebookingReferenceProducts.put(bookingReference, productCodes);
					ebookingReferenceProductCategory.put(bookingReference, productCategoryMap);
				}
				waitListedBookingReferenceList.put(bookingReference, itinerary.getBooking().getIsWaitListed());
				bookingReferenceItineraryList.put(bookingReference, itinerary);
			}
		}

		Boolean bookingModified = Boolean.FALSE;
		for (final String bookingReference : originalTransportEntriesByBookingRef.keySet())
		{
			final List<String> ebookingProductsForBookingReference = ebookingReferenceProducts.get(bookingReference);
			if(CollectionUtils.isEmpty(ebookingProductsForBookingReference))
			{
				continue;
			}
			final List<String> originalProductsForBookingReference = originalTransportProductsByBookingRef.get(bookingReference);


			if (!new HashSet<>(ebookingProductsForBookingReference).equals(new HashSet<>(originalProductsForBookingReference)))
			{
				bookingModified = Boolean.TRUE;
				break;
			}
		}

		final List<ItemModel> itemsToSave = new ArrayList<ItemModel>();

		//Clone only changes noticed in ebooking response i.e. bookingModified - TRUE
		if (BooleanUtils.isTrue(bookingModified))
		{
			final Transaction tx = Transaction.current();
			try
			{
				tx.begin();

				final OrderModel newOrderModel = getBcfCloneOrderStrategy().createHistorySnapshot(order);
				final OrderHistoryEntryModel orderHistoryEntry = getBcfCloneOrderStrategy()
						.createNewOrderHistoryEntry(newOrderModel, order);
				itemsToSave.add(orderHistoryEntry);

				//Group AmountToPayInfoModel by booking Reference
				final Map<String, List<AmountToPayInfoModel>> amountToPayByEbookingReferences = order.getAmountToPayInfos().stream()
						.collect(Collectors.groupingBy(AmountToPayInfoModel::getBookingReference));

				for (final String bookingReference : originalTransportEntriesByBookingRef.keySet())
				{
					final List<String> ebookingProductsForBookingReference = ebookingReferenceProducts.get(bookingReference);

					if(CollectionUtils.isEmpty(ebookingProductsForBookingReference))
					{
						continue;
					}
					final List<AbstractOrderEntryModel> orderEntries = originalTransportEntriesByBookingRef.get(bookingReference);
					final List<AmountToPayInfoModel> amountToPayInfoList = amountToPayByEbookingReferences.get(bookingReference);
					final Boolean isWaitListedBooking = waitListedBookingReferenceList.get(bookingReference);

					for (final AbstractOrderEntryModel orderEntryModel : orderEntries)
					{
						final String commodityCode = getEntryProductCode(orderEntryModel);

						if (ebookingProductsForBookingReference.contains(commodityCode))
						{
							ebookingProductsForBookingReference.remove(commodityCode);
						}
						else
						{
							//Existing OrderEntry not present in e-booking response - set it to inactive
							orderEntryModel.setActive(Boolean.FALSE);
							orderEntryModel.setModifiedFromEbooking(Boolean.TRUE);
							itemsToSave.add(orderEntryModel);
						}
					}

					//if any products contained in ebookingProductsForBookingReference - we create new active orderEntry with that product
					for (final String ebookingProductCode : ebookingProductsForBookingReference)
					{

						OrderEntryModel entry = new OrderEntryModel();
						final Map<String, String> productCategoryMap = ebookingReferenceProductCategory.get(bookingReference);
						final String eBookingCategory = productCategoryMap.get(ebookingProductCode);

						final Itinerary itinerary = bookingReferenceItineraryList.get(bookingReference);
						final List<ProductFare> productFareList = getProductFaresFromItinerary(itinerary);
						final List<TransportOfferingModel> eTransportOfferings = getAbstractOrderEntriesHelper()
								.fetchTransportOfferings(itinerary);
						final TravelRouteModel eTravelRoute = getAbstractOrderEntriesHelper().fetchTravelRoute(itinerary);
						final String tariffType = itinerary.getSailing().getLine().stream().findFirst().get().getSailingPrices()
								.stream().findFirst().get().getTariffType();
						final TravelOrderEntryInfoModel travelOrderEntryInfo = getAbstractOrderEntriesHelper()
								.createTravelOrderEntryInfo(eTravelRoute, eTransportOfferings);

						final int journeyReferenceNumber = orderEntries.get(0).getJourneyReferenceNumber();
						final int index = 0;

						final Optional<ProductFare> currentProductFare = productFareList.stream().filter(
								productFare -> ((productFare.getProduct().getProductId().equals(ebookingProductCode)) && (productFare
										.getProduct().getProductCategory().equals(eBookingCategory)))).findFirst();

						if (currentProductFare.isPresent())
						{
							final ProductFare productFare = currentProductFare.get();

							final OrderEntryModel orderEntry = new OrderEntryModel();

							if (TravellerType.PASSENGER.getCode().equals(eBookingCategory))
							{
								travelOrderEntryInfo.setTravellers(getAbstractOrderEntriesHelper()
										.createPassengerTraveller(productFare, journeyReferenceNumber, index, order.getCode()));
								entry = createOrderEntry(1, journeyReferenceNumber, productFare,
										bookingReference,
										travelOrderEntryInfo, tariffType);
							}
							else if (TravellerType.VEHICLE.getCode().equals(eBookingCategory))
							{
								travelOrderEntryInfo.setTravellers(getAbstractOrderEntriesHelper()
										.createVehicleTraveller(productFare, journeyReferenceNumber, index, order.getCode()));
								entry = createOrderEntry(1, journeyReferenceNumber, productFare,
										bookingReference,
										travelOrderEntryInfo, tariffType);
							}
							else
							{
								entry = createNewEntryForProduct(order, bookingReference,
										productFare,
										travelOrderEntryInfo, productFare.getProduct().getProductNumber(), journeyReferenceNumber,
										tariffType);
							}

							if (Objects.nonNull(entry))
								entry.setBundleNo(Integer.valueOf(ConfigurableBundleServicesConstants.NO_BUNDLE));
							entry.setOrder(order);
							entry.setProductFromEbooking(Boolean.TRUE);
							entry.setModifiedFromEbooking(Boolean.TRUE);
							itemsToSave.add(travelOrderEntryInfo);
							itemsToSave.add(entry);
						}
					}
				}

				itemsToSave.add(order);

				modelService.saveAll(itemsToSave);
				modelService.refresh(order);

				LOG.info(" Before calculation (" + order.getCode() + ") : Amount paid " + order.getAmountPaid()
						+ " : Amount To Pay " + order.getAmountToPay());

				getCalculationService().calculate(order);

				//Amount to pay needs to be rectified
				order.setAmountToPay(PrecisionUtil.round(BigDecimal.valueOf(order.getTotalPrice())
						.subtract(BigDecimal.valueOf(order.getAmountPaid())).doubleValue()));

				modelService.saveAll(order);
				modelService.refresh(order);

				LOG.info(" After calculation (" + order.getCode() + ") : Amount paid " + order.getAmountPaid()
						+ " : Amount To Pay " + order.getAmountToPay());

				tx.commit();
			}
			catch (final CalculationException ex)
			{
				tx.rollback();
				throw ex;
			}
			LOG.info(" Created new order for ebooking changes -> Order: (" + order.getCode() + ")");
		}
		else
		{
			LOG.info(" No changes in order from ebooking -> Order: (" + order.getCode() + ")");

			//update waitlisted products
			//Group AmountToPayInfoModel by booking Reference
			final Map<String, List<AmountToPayInfoModel>> amountToPayByEbookingReferences = order.getAmountToPayInfos().stream()
					.filter(infos -> StringUtils.isNotEmpty(infos.getBookingReference()))
					.collect(Collectors.groupingBy(AmountToPayInfoModel::getBookingReference));
			for (final String bookingReference : originalTransportEntriesByBookingRef.keySet())
			{
				final List<AmountToPayInfoModel> amountToPayInfoList = amountToPayByEbookingReferences.get(bookingReference);
				final Boolean isWaitListedBooking = waitListedBookingReferenceList.get(bookingReference);

				if (CollectionUtils.isNotEmpty(amountToPayInfoList))
				{
					for (final AmountToPayInfoModel amountToPayInfoModel : amountToPayInfoList)
					{
						amountToPayInfoModel.setIsWaitListed(isWaitListedBooking);
						modelService.save(amountToPayInfoModel);
					}
				}
			}
		}
	}

	private String getEntryProductCode(final AbstractOrderEntryModel orderEntryModel)
	{
		String commodityCode = orderEntryModel.getProduct().getCode();
		final Optional<TravellerModel> traveller = orderEntryModel.getTravelOrderEntryInfo().getTravellers().stream()
				.findFirst();
		if (traveller.isPresent())
		{
			final TravellerInfoModel travellerInfoModel = traveller.get().getInfo();
			if (travellerInfoModel instanceof PassengerInformationModel)
			{
				final PassengerInformationModel passengerInformationModel = (PassengerInformationModel) travellerInfoModel;
				commodityCode = passengerInformationModel.getPassengerType().getEBookingCode();
			}
			if (travellerInfoModel instanceof BCFVehicleInformationModel)
			{
				final BCFVehicleInformationModel vehicleInformationModel = (BCFVehicleInformationModel) travellerInfoModel;
				commodityCode = vehicleInformationModel.getVehicleType().getType().getCode();
			}
		}
		return commodityCode;
	}

	@Override
	public void updateUserSystemIdentifier(final Booking booking, final UserModel user)
	{
		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			final boolean isAnonymous =
					getUserService().isAnonymousUser(customer) || CustomerType.GUEST
							.equals(customer.getType());

			if (isAnonymous)
			{
				final Map<String, String> systemIdentifiersMap = new HashMap<>();
				if (!MapUtils.isEmpty(user.getSystemIdentifiers()))
				{
					systemIdentifiersMap.putAll(user.getSystemIdentifiers());
				}
				if (booking.getBookingHolderInfo() != null && booking.getBookingHolderInfo().getSourceSystemName() != null)
				{
					systemIdentifiersMap.put(booking.getBookingHolderInfo().getSourceSystemName(),
							booking.getBookingHolderInfo().getCustomerNumber());
					user.setSystemIdentifiers(systemIdentifiersMap);
					getModelService().save(user);
				}
			}
		}
	}

	/**
	 * Update order entries from E booking.
	 *
	 * @param order                         the order
	 * @param eItinerariesGroupByBookingRef the e itineraries group by booking ref
	 * @param orderEntriesGroupByBookingRef the order entries group by booking ref
	 * @throws BcfOrderUpdateException
	 */
	protected void updateOrderEntriesFromEBooking(final OrderModel order,
			final Map<String, List<Itinerary>> eItinerariesGroupByBookingRef,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByBookingRef) throws BcfOrderUpdateException
	{
		for (final Map.Entry<String, List<Itinerary>> eItinerariesGroupByBookingRefEntry : eItinerariesGroupByBookingRef.entrySet())
		{
			final String bookingReference = eItinerariesGroupByBookingRefEntry.getKey();
			final List<Itinerary> eItineraries = eItinerariesGroupByBookingRefEntry.getValue().stream()
					.filter(itinerary -> Objects.nonNull(itinerary.getSailing())
							&& CollectionUtils.isNotEmpty(itinerary.getSailing().getLine()))
					.collect(Collectors.toList());
			final List<AbstractOrderEntryModel> orderEntriesByBookingRef = orderEntriesGroupByBookingRef.get(bookingReference);

			final Itinerary itinerary = eItineraries.stream().findFirst().get();
			final List<ProductFare> productFareList = getProductFaresFromItinerary(itinerary);
			final List<TransportOfferingModel> eTransportOfferings = getAbstractOrderEntriesHelper()
					.fetchTransportOfferings(itinerary);
			final TravelRouteModel eTravelRoute = getAbstractOrderEntriesHelper().fetchTravelRoute(itinerary);
			final String tariffType = itinerary.getSailing().getLine().stream().findFirst().get().getSailingPrices().stream()
					.findFirst().get().getTariffType();
			final int journeyReferenceNumber = orderEntriesByBookingRef.stream().findFirst().get().getJourneyReferenceNumber();
			if (CollectionUtils.isEmpty(productFareList) || CollectionUtils.isEmpty(eTransportOfferings)
					|| Objects.isNull(eTravelRoute))
			{
				throw new BcfOrderUpdateException(
						"Either no product fare found,no matching transportOffering found or no matching travel route found for the "
								+ bookingReference);
			}
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode = getOrderEntriesGroupByProductCode(
					orderEntriesByBookingRef);

			//remove order entries which are not in Itinerary products list
			deActivateOrderEntriesNotInItineraryProducts(orderEntriesGroupByProductCode, productFareList);

			//add/update order entries which are not in Itinerary products list
			updateOrderEntriesFromItinerary(order, bookingReference, orderEntriesGroupByProductCode, productFareList,
					eTransportOfferings, eTravelRoute, journeyReferenceNumber, tariffType);

		}
	}

	private List<ProductFare> getProductFaresFromItinerary(final Itinerary itinerary)
	{
		final List<ProductFare> productFareList = StreamUtil.safeStream(itinerary.getSailing().getLine())
				.map(SailingLine::getSailingPrices)
				.flatMap(sailingPrices -> StreamUtil.safeStream(sailingPrices)).map(SailingPrices::getProductFares)
				.flatMap(productFares -> StreamUtil.safeStream(productFares)).collect(Collectors.toList());
		final List<ProductFare> otherCharges = StreamUtil.safeStream(itinerary.getSailing().getOtherCharges())
				.collect(Collectors.toList());
		productFareList.addAll(otherCharges);
		final List<ProductFare> consolidatedFareList = new ArrayList<>();
		StreamUtil.safeStream(productFareList).collect(Collectors.groupingBy(o -> o.getProduct().getProductId())).entrySet()
				.forEach(stringListEntry -> {
					final ProductFare productFare = stringListEntry.getValue().get(0);
					productFare.getProduct().setProductNumber(stringListEntry.getValue().size());
					consolidatedFareList.add(productFare);
				});
		return consolidatedFareList;
	}

	/**
	 * Update order entries from itinerary.
	 *
	 * @param order                          the order
	 * @param bookingReference               the booking reference
	 * @param orderEntriesGroupByProductCode the order entries group by product code
	 * @param productFares                   the product fares
	 * @param eTransportOfferings            the e transport offerings
	 * @param eTravelRoute                   the e travel route
	 * @param journeyReferenceNumber         the journey reference number
	 */
	protected void updateOrderEntriesFromItinerary(final OrderModel order, final String bookingReference,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode, final List<ProductFare> productFares,
			final List<TransportOfferingModel> eTransportOfferings, final TravelRouteModel eTravelRoute,
			final int journeyReferenceNumber, final String tariffType)
	{
		for (final ProductFare productFare : productFares)
		{
			if (productFare.getProduct().getProductCategory().equals(TravellerType.PASSENGER.getCode()))
			{
				updatePassengerEntries(order, bookingReference, orderEntriesGroupByProductCode, productFare, eTransportOfferings,
						eTravelRoute, journeyReferenceNumber, tariffType);
			}
			else if (productFare.getProduct().getProductCategory().equals(TravellerType.VEHICLE.getCode()))
			{
				updateVehicleEntries(order, bookingReference, orderEntriesGroupByProductCode, productFare, eTransportOfferings,
						eTravelRoute, journeyReferenceNumber, tariffType);
			}
			else
			{
				updateAncillaries(order, bookingReference, productFare, orderEntriesGroupByProductCode, eTransportOfferings,
						eTravelRoute, journeyReferenceNumber, tariffType);
			}
		}
	}

	/**
	 * De activate order entries not in itinerary products.
	 *
	 * @param orderEntriesGroupByProductCode the order entries group by product code
	 * @param productFares                   the product fares
	 */
	protected void deActivateOrderEntriesNotInItineraryProducts(
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode, final List<ProductFare> productFares)
	{
		final List<AbstractOrderEntryModel> orderEntriesToRemove = new ArrayList<>();
		orderEntriesGroupByProductCode.forEach((productCode, orderEntries) -> {
			final boolean productExists = productFares.stream()
					.anyMatch(productFare -> StringUtils.equals(productFare.getProduct().getProductId(), productCode));
			if (!productExists)
			{
				orderEntriesToRemove.addAll(orderEntries);
			}
		});
		deActivateOrderEntries(orderEntriesToRemove);
	}

	/**
	 * Gets the order entries group by product code.
	 *
	 * @param orderEntriesByBookingRef the order entries by booking ref
	 * @return the order entries group by product code
	 */
	protected Map<String, List<AbstractOrderEntryModel>> getOrderEntriesGroupByProductCode(
			final List<AbstractOrderEntryModel> orderEntriesByBookingRef)
	{
		final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode = new HashMap<>();
		for (final AbstractOrderEntryModel orderEntry : orderEntriesByBookingRef)
		{
			final String productCode = getEProductCode(orderEntry);
			List<AbstractOrderEntryModel> orderEntriesByProductCode = orderEntriesGroupByProductCode.get(productCode);
			if (Objects.isNull(orderEntriesByProductCode))
			{
				orderEntriesByProductCode = new ArrayList<>();
				orderEntriesGroupByProductCode.put(productCode, orderEntriesByProductCode);
			}
			orderEntriesByProductCode.add(orderEntry);
		}

		return orderEntriesGroupByProductCode;
	}

	/**
	 * Gets the e product code.
	 *
	 * @param orderEntry the order entry
	 * @return the e product code
	 */
	protected String getEProductCode(final AbstractOrderEntryModel orderEntry)
	{
		String productCode = null;
		if (Objects.equals(orderEntry.getProduct().getItemtype(), FareProductModel._TYPECODE))
		{
			final TravellerModel traveller = orderEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get();
			if (traveller.getInfo() instanceof PassengerInformationModel)
			{
				productCode = ((PassengerInformationModel) traveller.getInfo()).getPassengerType().getEBookingCode();
			}
			else if (traveller.getInfo() instanceof BCFVehicleInformationModel)
			{
				productCode = ((BCFVehicleInformationModel) traveller.getInfo()).getVehicleType().getType().getCode();
			}
		}
		else
		{
			productCode = orderEntry.getProduct().getCode();
		}
		return productCode;
	}

	/**
	 * De activate order entries not in E booking.
	 *
	 * @param eItinerariesGroupByBookingRef the e itineraries group by booking ref
	 * @param orderEntriesGroupByBookingRef the order entries group by booking ref
	 */
	protected void deActivateOrderEntriesNotInEBooking(final Map<String, List<Itinerary>> eItinerariesGroupByBookingRef,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByBookingRef)
	{
		final List<AbstractOrderEntryModel> orderEntriesToRemove = new ArrayList<>();
		orderEntriesGroupByBookingRef.forEach((bookingReference, orderEntries) -> {
			if (!eItinerariesGroupByBookingRef.containsKey(bookingReference))
			{
				orderEntriesToRemove.addAll(orderEntries);
			}
		});
		deActivateOrderEntries(orderEntriesToRemove);
	}



	/**
	 * Update passenger entries.
	 *
	 * @param orderModel                     the order model
	 * @param bookingReference               the booking reference
	 * @param orderEntriesGroupByProductCode the order entries group by product code
	 * @param productFare                    the product fare
	 * @param eTransportOfferings            the e transport offerings
	 * @param eTravelRoute                   the e travel route
	 * @param journeyReferenceNumber         the journey reference number
	 */
	protected void updatePassengerEntries(final OrderModel orderModel, final String bookingReference,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode, final ProductFare productFare,
			final List<TransportOfferingModel> eTransportOfferings, final TravelRouteModel eTravelRoute,
			final int journeyReferenceNumber, final String tariffType)
	{
		final List<AbstractOrderEntryModel> orderEntriesByProductCode = orderEntriesGroupByProductCode
				.get(productFare.getProduct().getProductId());
		if (Objects.nonNull(orderEntriesByProductCode))
		{
			final int passengersChangeFactor = (int) (productFare.getProduct().getProductNumber()
					- CollectionUtils.size(orderEntriesByProductCode));
			if (passengersChangeFactor > 0)
			{
				//create new passengers
				final List<AbstractOrderEntryModel> newOrderEntries = createNewEntriesForPassengers(orderModel, bookingReference,
						productFare, passengersChangeFactor, eTransportOfferings, eTravelRoute, journeyReferenceNumber, tariffType);
				setOrderEntriesToOrder(orderModel, newOrderEntries);
			}
			else if (passengersChangeFactor < 0)
			{
				//remove extra passengers entries
				final List<AbstractOrderEntryModel> orderEntriesToRemove = new ArrayList<>();
				IntStream.range(passengersChangeFactor, 0)
						.forEach(index -> orderEntriesToRemove.add(orderEntriesByProductCode.get(-index)));

				deActivateOrderEntries(orderEntriesToRemove);
			}

			orderEntriesByProductCode.stream().filter(AbstractOrderEntryModel::getActive).forEach(
					orderEntryToUpdate -> updateOrderEntry(orderEntryToUpdate, eTransportOfferings, productFare, eTravelRoute));
		}
		else
		{
			//create new passengers
			final List<AbstractOrderEntryModel> newOrderEntries = createNewEntriesForPassengers(orderModel, bookingReference,
					productFare, (int) productFare.getProduct().getProductNumber(), eTransportOfferings, eTravelRoute,
					journeyReferenceNumber, tariffType);
			setOrderEntriesToOrder(orderModel, newOrderEntries);
		}
	}

	/**
	 * Creates the new entries for passengers.
	 *
	 * @param orderModel             the order model
	 * @param bookingReference       the booking reference
	 * @param productFare            the product fare
	 * @param noOfPassengers         the no of passengers
	 * @param transportOfferings     the transport offerings
	 * @param travelRoute            the travel route
	 * @param journeyReferenceNumber the journey reference number
	 * @return the list
	 */
	protected List<AbstractOrderEntryModel> createNewEntriesForPassengers(final OrderModel orderModel,
			final String bookingReference, final ProductFare productFare, final int noOfPassengers,
			final List<TransportOfferingModel> transportOfferings, final TravelRouteModel travelRoute,
			final int journeyReferenceNumber, final String tariffType)
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
		IntStream.range(0, noOfPassengers).forEach(index -> {
			final TravelOrderEntryInfoModel travelOrderEntryInfo = getAbstractOrderEntriesHelper()
					.createTravelOrderEntryInfo(travelRoute, transportOfferings);
			travelOrderEntryInfo.setTravellers(getAbstractOrderEntriesHelper()
					.createPassengerTraveller(productFare, journeyReferenceNumber, index, orderModel.getCode()));
			final OrderEntryModel orderEntry = createOrderEntry(1, journeyReferenceNumber, productFare, bookingReference,
					travelOrderEntryInfo, tariffType);
			orderEntry.setBundleNo(Integer.valueOf(ConfigurableBundleServicesConstants.NO_BUNDLE));
			orderEntry.setOrder(orderModel);
			orderEntries.add(orderEntry);
		});
		return orderEntries;
	}


	/**
	 * Update vehicle entries.
	 *
	 * @param orderModel                     the order model
	 * @param bookingReference               the booking reference
	 * @param orderEntriesGroupByProductCode the order entries group by product code
	 * @param productFare                    the product fare
	 * @param eTransportOfferings            the e transport offerings
	 * @param eTravelRoute                   the e travel route
	 * @param journeyReferenceNumber         the journey reference number
	 */
	protected void updateVehicleEntries(final OrderModel orderModel, final String bookingReference,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode, final ProductFare productFare,
			final List<TransportOfferingModel> eTransportOfferings, final TravelRouteModel eTravelRoute,
			final int journeyReferenceNumber, final String tariffType)
	{
		final List<AbstractOrderEntryModel> orderEntriesByProductCode = orderEntriesGroupByProductCode
				.get(productFare.getProduct().getProductId());
		if (Objects.nonNull(orderEntriesByProductCode))
		{
			final int vehiclesChangeFactor = (int) (productFare.getProduct().getProductNumber()
					- CollectionUtils.size(orderEntriesByProductCode));
			if (vehiclesChangeFactor > 0)
			{
				//create new vehicles
				final List<AbstractOrderEntryModel> newOrderEntries = createNewEntriesForVehicles(orderModel, bookingReference,
						productFare, vehiclesChangeFactor, eTransportOfferings, eTravelRoute, journeyReferenceNumber, tariffType);
				setOrderEntriesToOrder(orderModel, newOrderEntries);
			}
			else if (vehiclesChangeFactor < 0)
			{
				//remove extra vehicle entries
				final List<AbstractOrderEntryModel> orderEntriesToRemove = new ArrayList<>();
				IntStream.range(vehiclesChangeFactor, 0)
						.forEach(index -> orderEntriesToRemove.add(orderEntriesByProductCode.get(-index)));

				deActivateOrderEntries(orderEntriesToRemove);
			}

			orderEntriesByProductCode.stream().filter(AbstractOrderEntryModel::getActive).forEach(
					orderEntryToUpdate -> updateOrderEntry(orderEntryToUpdate, eTransportOfferings, productFare, eTravelRoute));
		}
		else
		{
			//create new vehicles
			final List<AbstractOrderEntryModel> newOrderEntries = createNewEntriesForVehicles(orderModel, bookingReference,
					productFare, (int) productFare.getProduct().getProductNumber(), eTransportOfferings, eTravelRoute,
					journeyReferenceNumber, tariffType);
			setOrderEntriesToOrder(orderModel, newOrderEntries);
		}
	}

	/**
	 * Creates the new entries for passengers.
	 *
	 * @param orderModel             the order model
	 * @param bookingReference       the booking reference
	 * @param productFare            the product fare
	 * @param noOfVehicles           the no of vehicles
	 * @param transportOfferings     the transport offerings
	 * @param travelRoute            the travel route
	 * @param journeyReferenceNumber the journey reference number
	 * @return the list
	 */
	protected List<AbstractOrderEntryModel> createNewEntriesForVehicles(final OrderModel orderModel, final String bookingReference,
			final ProductFare productFare, final int noOfVehicles, final List<TransportOfferingModel> transportOfferings,
			final TravelRouteModel travelRoute, final int journeyReferenceNumber, final String tariffType)
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
		IntStream.range(0, noOfVehicles).forEach(index -> {
			final TravelOrderEntryInfoModel travelOrderEntryInfo = getAbstractOrderEntriesHelper()
					.createTravelOrderEntryInfo(travelRoute, transportOfferings);
			travelOrderEntryInfo.setTravellers(getAbstractOrderEntriesHelper()
					.createVehicleTraveller(productFare, journeyReferenceNumber, index, orderModel.getCode()));
			final OrderEntryModel orderEntry = createOrderEntry(1, journeyReferenceNumber, productFare, bookingReference,
					travelOrderEntryInfo, tariffType);
			orderEntry.setBundleNo(Integer.valueOf(ConfigurableBundleServicesConstants.NO_BUNDLE));
			orderEntry.setOrder(orderModel);
			orderEntries.add(orderEntry);
		});
		return orderEntries;
	}

	/**
	 * Update ancillaries.
	 *
	 * @param orderModel                     the order model
	 * @param bookingReference               the booking reference
	 * @param productFare                    the product fare
	 * @param orderEntriesGroupByProductCode the order entries group by product code
	 * @param eTransportOfferings            the e transport offerings
	 * @param eTravelRoute                   the e travel route
	 * @param journeyReferenceNumber         the journey reference number
	 */
	protected void updateAncillaries(final OrderModel orderModel, final String bookingReference, final ProductFare productFare,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByProductCode,
			final List<TransportOfferingModel> eTransportOfferings, final TravelRouteModel eTravelRoute,
			final int journeyReferenceNumber, final String tariffType)
	{
		final String eProductCode = productFare.getProduct().getProductId();
		final int eQuantity = (int) productFare.getProduct().getProductNumber();
		final List<AbstractOrderEntryModel> orderEntries = orderEntriesGroupByProductCode.get(eProductCode);

		if (Objects.nonNull(orderEntries))
		{
			final long quantity = orderEntries.stream().mapToLong(AbstractOrderEntryModel::getQuantity).sum();
			final int ancillariesChangeFactor = (int) (eQuantity - quantity);
			final AbstractOrderEntryModel orderEntry = orderEntries.stream().findAny().get();
			if (ancillariesChangeFactor != 0)
			{
				//increment/decrement ancillary quantity
				orderEntry.setQuantity(orderEntry.getQuantity() + ancillariesChangeFactor);
			}
			orderEntries.stream().filter(AbstractOrderEntryModel::getActive).forEach(
					orderEntryToUpdate -> updateOrderEntry(orderEntryToUpdate, eTransportOfferings, productFare, eTravelRoute));
		}
		else
		{

			final TravelOrderEntryInfoModel travelOrderEntryInfo = getAbstractOrderEntriesHelper()
					.getExistingTravelOrderEntryInfoForAncillary(orderModel,
							journeyReferenceNumber);
			//create new ancillary entry
			final AbstractOrderEntryModel newOrderEntry = createNewEntryForProduct(orderModel, bookingReference, productFare,
					travelOrderEntryInfo, productFare.getProduct().getProductNumber(), journeyReferenceNumber, tariffType);
			newOrderEntry.setBundleNo(Integer.valueOf(ConfigurableBundleServicesConstants.NO_BUNDLE));
			setOrderEntriesToOrder(orderModel, Collections.singletonList(newOrderEntry));
		}

	}

	/**
	 * Creates the new entry for product.
	 *
	 * @param orderModel             the order model
	 * @param bookingReference       the booking reference
	 * @param productFare            the product fare
	 * @param travelOrderEntryInfo   the passenger
	 * @param quantity               the quantity
	 * @param journeyReferenceNumber the journey reference number
	 * @return the order entry model
	 */
	protected OrderEntryModel createNewEntryForProduct(final OrderModel orderModel, final String bookingReference,
			final ProductFare productFare, final TravelOrderEntryInfoModel travelOrderEntryInfo, final long quantity,
			final int journeyReferenceNumber, final String tariffType)
	{
		final OrderEntryModel orderEntry = createOrderEntry(quantity, journeyReferenceNumber, productFare, bookingReference,
				travelOrderEntryInfo, tariffType);
		orderEntry.setOrder(orderModel);
		return orderEntry;
	}

	/**
	 * Creates the order entry.
	 *
	 * @param quantity               the quantity
	 * @param journeyReferenceNumber the journey reference number
	 * @param productFare            the product fare
	 * @param bookingReference       the booking reference
	 * @param travelOrderEntryInfo   the travel order entry info
	 * @return the order entry model
	 */
	protected OrderEntryModel createOrderEntry(final long quantity, final int journeyReferenceNumber,
			final ProductFare productFare, final String bookingReference, final TravelOrderEntryInfoModel travelOrderEntryInfo,
			final String tariffType)
	{
		final OrderEntryModel orderEntry = getModelService().create(OrderEntryModel.class);
		orderEntry.setQuantity(quantity);
		orderEntry.setJourneyReferenceNumber(journeyReferenceNumber);
		orderEntry.setBasePrice(getAbstractOrderEntriesHelper().getProductBasePrice(productFare));
		final ProductModel product = getAbstractOrderEntriesHelper().getProduct(productFare, tariffType);
		orderEntry.setProduct(product);
		orderEntry.setBundleTemplate(getAbstractOrderEntriesHelper().fetchBundleTemplate(product));

		UnitModel unit = product.getUnit();
		if (Objects.isNull(unit))
		{
			unit = getProductService().getOrderableUnit(product);
		}

		orderEntry.setUnit(unit);
		orderEntry.setCalculated(false);
		orderEntry.setActive(Boolean.TRUE);
		orderEntry.setAmendStatus(AmendStatus.NEW);
		orderEntry.setType(OrderEntryType.TRANSPORT);
		orderEntry.setBookingReference(bookingReference);
		orderEntry.setTravelOrderEntryInfo(travelOrderEntryInfo);
		return orderEntry;
	}

	/**
	 * Test and update order entry.
	 *
	 * @param orderEntry          the order entry
	 * @param eTransportOfferings the e transport offerings
	 * @param productFare         the product fare
	 * @param eTravelRoute        the e travel route
	 */
	protected void updateOrderEntry(final AbstractOrderEntryModel orderEntry,
			final Collection<TransportOfferingModel> eTransportOfferings, final ProductFare productFare,
			final TravelRouteModel eTravelRoute)
	{
		final Collection<TransportOfferingModel> transportOfferings = orderEntry.getTravelOrderEntryInfo().getTransportOfferings();
		final boolean allTOMatched = eTransportOfferings.stream().allMatch(transportOfferings::contains);
		final Double basePrice = getAbstractOrderEntriesHelper().getProductBasePrice(productFare);

		if (!allTOMatched || basePrice != orderEntry.getBasePrice())
		{
			orderEntry.getTravelOrderEntryInfo().setTransportOfferings(eTransportOfferings);
			orderEntry.getTravelOrderEntryInfo().setTravelRoute(eTravelRoute);
			orderEntry.setBasePrice(basePrice);
			orderEntry.setMarginCalculated(false);
		}

		if (Objects.isNull(orderEntry.getBundleTemplate()))
		{
			orderEntry.setBundleTemplate(getAbstractOrderEntriesHelper().fetchBundleTemplate(orderEntry.getProduct()));
		}
	}

	/**
	 * Sets the order entries to order.
	 *
	 * @param orderModel      the order model
	 * @param newOrderEntries the new order entries
	 */
	protected void setOrderEntriesToOrder(final OrderModel orderModel, final List<AbstractOrderEntryModel> newOrderEntries)
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>(orderModel.getEntries());
		orderEntries.addAll(newOrderEntries);
		orderModel.setEntries(orderEntries);
	}

	/**
	 * De activate order entries.
	 *
	 * @param orderEntriesToRemove the order entries to remove
	 */
	protected void deActivateOrderEntries(final List<AbstractOrderEntryModel> orderEntriesToRemove)
	{
		orderEntriesToRemove.forEach(orderEntry -> {
			orderEntry.setActive(false);
		});
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected CalculationService getCalculationService()
	{
		return calculationService;
	}

	@Required
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}


	protected AbstractOrderEntriesHelper getAbstractOrderEntriesHelper()
	{
		return abstractOrderEntriesHelper;
	}

	@Required
	public void setAbstractOrderEntriesHelper(final AbstractOrderEntriesHelper abstractOrderEntriesHelper)
	{
		this.abstractOrderEntriesHelper = abstractOrderEntriesHelper;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	public OrderService getOrderService()
	{
		return orderService;
	}

	public void setOrderService(final OrderService orderService)
	{
		this.orderService = orderService;
	}
}
