/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.strategies.update;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.booking.response.Booking;


public interface BcfUpdateOrderFromEBookingStrategy
{

	/**
	 * Update existing order.
	 *
	 * @param order                    the order
	 * @param searchBookingResponseDTO the search booking response DTO
	 * @throws BcfOrderUpdateException
	 * @throws CalculationException
	 */
	void updateExistingOrder(OrderModel order, SearchBookingResponseDTO searchBookingResponseDTO)
			throws BcfOrderUpdateException, CalculationException;

	void updateVersionedOrder(OrderModel order, SearchBookingResponseDTO searchBookingResponseDTO)
			throws BcfOrderUpdateException, CalculationException;

	public void updateUserSystemIdentifier(final Booking booking, final UserModel user);
}
