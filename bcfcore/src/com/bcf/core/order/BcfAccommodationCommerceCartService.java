/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.order.AccommodationCommerceCartService;
import java.util.Date;
import java.util.List;
import com.bcf.core.model.order.DealOrderEntryGroupModel;


public interface BcfAccommodationCommerceCartService extends AccommodationCommerceCartService
{

	/**
	 * method returns new DealOrderEntryGroupModel from the cart for the same journeyRefNumber. Will handle a scenario when a person has already added a room to the cart
	 * and then a person again selects the same room but with a different quantity and in that case we need to add the correct number of rooms to the cart
	 *
	 * @param accommodationOfferingCode
	 * @param accommodationCode
	 * @param ratePlanCode
	 * @param journeyRefNumber
	 * @return
	 */
	List<AccommodationOrderEntryGroupModel> getNewDealOrderEntryGroups(String accommodationOfferingCode, String accommodationCode, String ratePlanCode, Integer journeyRefNumber);

	/**
	 * returns the max room stay reference number while adding a new accommodation to the cart for the linked journey reference number coming from the ferry
	 *
	 * @param journeyRefNumber
	 * @return
	 */
	int getMaxRoomStayRefNumber(Integer journeyRefNumber);


	void removeAccommodationOrderEntriesForDeal(List<AbstractOrderEntryModel> abstractOrderEntries);

	void removeDealOrderEntryGroups(final List<DealOrderEntryGroupModel> accommodationDealEntryGroups);

	/**
	 *
	 * @param entryNumbers
	 * @param checkInDate
	 * @param checkOutDate
	 * @param accOffCode
	 * @param accCode
	 * @param ratePlanCode
	 * @param refNumber
	 *
	 * @return AccommodationOrderEntryGroupModel
	 */
	AccommodationOrderEntryGroupModel getNewAccommodationOrderEntryGroup(List<AbstractOrderEntryModel> dealAccommodationEntries, Date checkInDate, Date checkOutDate, String accOffCode, String accCode, String ratePlanCode, int refNumber);

	void inActivateOrRemoveAccommodationEntryGroup(final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel);

	void inActivateOrRemoveAccommodationEntries(final List<AbstractOrderEntryModel> entries);
}
