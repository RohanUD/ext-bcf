/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.hook;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.travelservices.order.hook.TravelBundleAddToCartMethodHook;
import javax.annotation.Nonnull;


public class BcfTravelBundleAddToCartMethodHook extends TravelBundleAddToCartMethodHook
{
	@Override
	protected void checkBundleParameters(@Nonnull final CommerceCartParameter parameter)
	{
		//Deliberately Left Empty
	}

	@Override
	protected void checkIsProductInComponentProductList(@Nonnull final CommerceCartParameter parameter)
	{
		//Deliberately Left Empty
	}
}
