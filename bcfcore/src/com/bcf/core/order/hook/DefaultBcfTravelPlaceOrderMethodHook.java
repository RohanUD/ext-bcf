/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.hook;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.order.hook.DefaultTravelPlaceOrderMethodHook;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.service.BcfBookingService;


public class DefaultBcfTravelPlaceOrderMethodHook extends DefaultTravelPlaceOrderMethodHook
{

	private BcfBookingService bcfBookingService;

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
	{
		if (result.getOrder() != null)
		{
			final OrderModel orderModel = result.getOrder();
			this.setBookingJourney(orderModel, parameter.getCart());
			this.setDeliveryAddress(orderModel);
			this.calculcateTotalAndTaxes(orderModel);

			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups = getBookingService()
					.getAccommodationOrderEntryGroups(orderModel);
			if (CollectionUtils.isNotEmpty(accommodationEntryGroups))
			{
				accommodationEntryGroups.forEach(entryGroup -> {
					final List<AbstractOrderEntryModel> orderEntries = entryGroup.getEntries().stream().filter(entry -> entry instanceof OrderEntryModel).collect(Collectors.toList());
					entryGroup.setEntries(orderEntries);
					getModelService().save(entryGroup);
				});
			}
			final List<DealOrderEntryGroupModel> dealOrderEntryGroups = getBcfBookingService().getDealOrderEntryGroups(orderModel);
			if(CollectionUtils.isNotEmpty(dealOrderEntryGroups))
			{
				dealOrderEntryGroups.forEach(entryGroup -> {
					final List<AbstractOrderEntryModel> orderEntries = entryGroup.getEntries().stream().filter(entry -> entry instanceof OrderEntryModel).collect(Collectors.toList());
					entryGroup.setEntries(orderEntries);
					getModelService().save(entryGroup);
				});
			}
			getModelService().refresh(orderModel);
			result.setOrder(orderModel);
		}
	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter)
	{
		final CartModel cart = parameter.getCart();

		if (this.isAmendFlow(cart))
		{
			final OrderModel originalOrder = cart.getOriginalOrder();
			originalOrder.setStatus(OrderStatus.AMENDMENTINPROGRESS);
			this.getModelService().save(originalOrder);
		}

	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

}
