/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.facades.order.UpdatePendingOrderEntryRequestData;


public interface BcfOrderService extends OrderService
{
	SearchPageData<AbstractOrderModel> getPagedPendingOrdersForInventoryApproval(PageableData pageableData);

	List<OrderEntryModel> findOrderEntriesByEBookingCode(String eBookingCode);

	boolean updateOrderEntry(UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequestData, String agentId);

	String lockOrder(OrderModel order);

	String unlockOrder(OrderModel order);

	String lockOrderAndGetAgentName(final OrderModel order) throws BcfOrderUpdateException;

	String rejectOrder(OrderModel order, final String comments, String agentId) throws BcfOrderUpdateException;

	String acceptOrder(OrderModel order, final String comments, String agentId) throws BcfOrderUpdateException;

	AbstractOrderModel getOrderByEBookingCode(String eBookingCode);

	OrderModel getOrderByBookingReference(final String bookingReference, final UserModel customer);

	void deActivateOrderEntries(OrderModel order, List<AbstractOrderEntryModel> entries);

	AbstractOrderModel getOrderByCartId(String cartId);

	List<OrderModel> findVacationsForCurrentUser(UserModel user);

	OrderModel getOrderByCode(final String orderCode);

	List<OrderModel> findOrdersForSpecificSailingDate(Date startDate, Date endDate);

	Map<OrderEntryType, List<AbstractOrderEntryModel>> getDeltaOrderEntries(AbstractOrderModel abstractOrderModel);

	boolean isAmendOrderProcess(AbstractOrderModel orderModel);

	boolean isRefundOrder(List<PaymentTransactionEntryModel> paymentTransactionEntryModels);

	Boolean updateEntryMarkers(AbstractOrderModel order);

	OrderModel getOrderByCode(final String orderCode, final String versionId);

	List<OrderModel> getOrdersByCode(final String orderCode);
}
