/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.daos.OrderDao;
import java.util.Date;
import java.util.List;


public interface BcfOrderDao extends OrderDao
{
	SearchPageData<AbstractOrderModel> findPagedPendingOrdersForInventoryApproval(PageableData pageableData);

	List<OrderEntryModel> findOrderEntriesByEBookingCode(String eBookingCode);

	OrderModel getOrderByBookingReference(final String bookingReference, final UserModel customer);

	OrderModel findOrderByCartId(String cartId);

	List<OrderModel> findVacationsForCurrentUser(final UserModel user);

	List<OrderModel> findOrderByCode(String orderCode);

	List<OrderModel> findOrdersForSpecificSailingDate(Date startDate, Date endDate);

	List<OrderModel> findOrderByCode(final String orderCode, String versionId);

	List<OrderModel> findAllOrderByCode(final String orderCode);
}
