/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import java.util.Date;
import java.util.List;


public interface BcfCommerceCartDao extends CommerceCartDao
{
	List<CartModel> findCartsBySiteAndQuote(BaseSiteModel site, boolean isQuote);

	List<CartModel> findCartsByQuoteAndThreshold(boolean isQuote, Date threshold);

	CartModel getCartForCodeAndSite(String code, BaseSiteModel site);
}
