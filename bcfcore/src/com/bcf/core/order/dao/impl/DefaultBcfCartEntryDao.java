/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.dao.impl.DefaultCartEntryDao;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.order.dao.BcfCartEntryDao;


public class DefaultBcfCartEntryDao extends DefaultCartEntryDao implements BcfCartEntryDao
{
	private static final String FIND_ENTRIES_BY_ENTRY_NUMBERS = "select {" + AbstractOrderEntryModel.PK
			+ "} from {AbstractOrderEntry} where {" + AbstractOrderEntryModel.ENTRYNUMBER + "} in (?"
			+ AbstractOrderEntryModel.ENTRYNUMBER
			+ ") and {" + AbstractOrderEntryModel.ORDER + "}=?" + AbstractOrderEntryModel.ORDER;

	@Override
	public List<AbstractOrderEntryModel> findEntriesForOrder(final CartModel order, final List<Integer> entryNumbers)
	{
		validateParameterNotNull(order, "order must not be null!");
		validateParameterNotNull(entryNumbers, "entryNumbers must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AbstractOrderEntryModel.ENTRYNUMBER, entryNumbers);
		params.put(AbstractOrderEntryModel.ORDER, order);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ENTRIES_BY_ENTRY_NUMBERS, params);
		final SearchResult<AbstractOrderEntryModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}
