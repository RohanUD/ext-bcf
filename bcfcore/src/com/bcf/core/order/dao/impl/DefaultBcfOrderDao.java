/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.daos.impl.DefaultOrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.dao.BcfOrderDao;


public class DefaultBcfOrderDao extends DefaultOrderDao implements BcfOrderDao
{

	private static final String FIND_PENDING_ORDERS_QUERY = "SELECT {pk} FROM {Order AS o} WHERE {o.status} IN ({{SELECT {pk} "
			+ "FROM {OrderStatus} WHERE {code} IN ('PENDING_APPROVAL_FROM_MERCHANT','LOCKED')}}) ORDER BY {o.creationtime} DESC";
	private static final String SELECT = "SELECT {";
	private static final String FROM = "} from {";
	private static final String WHERE = "} WHERE {";
	private static final String VALUE = "} = ?";

	private static final String FIND_ORDERENTRIES_BY_EBOOKING_CODE_QUERY =
			SELECT + OrderEntryModel.PK + FROM + OrderEntryModel._TYPECODE + WHERE
					+ OrderEntryModel.BOOKINGREFERENCE + VALUE + OrderEntryModel.BOOKINGREFERENCE;

	private static final String FIND_ORDER_BY_CART_ID_QUERY =
			SELECT + OrderModel.PK + FROM + OrderModel._TYPECODE + WHERE
					+ OrderModel.CARTID + VALUE + OrderModel.CARTID;

	private static final String FIND_ORDER_BY_CODE_QUERY =
			SELECT + OrderModel.PK + FROM + OrderModel._TYPECODE + WHERE
					+ OrderModel.CODE + VALUE + OrderModel.CODE + " ORDER BY {" + OrderModel.EBOOKINGVERSION + "} DESC";

	private static final String FIND_ORDER_BY_CODE_CREATED_QUERY =
			SELECT + OrderModel.PK + FROM + OrderModel._TYPECODE + WHERE
					+ OrderModel.CODE + VALUE + OrderModel.CODE + " ORDER BY {" + OrderModel.CREATIONTIME + "}";

	private static final String FIND_VACATION_ORDERS_BY_USER_QUERY =
			"select {o.pk} from {order as o join BookingJourneyType as bj on {o.bookingJourneyType}={bj.pk}} "
					+ "where {bj.code} in ('BOOKING_PACKAGE','BOOKING_ALACARTE','BOOKING_ACTIVITY_ONLY','BOOKING_TRANSPORT_ACCOMMODATION')AND {o:user}= ?user";

	private static final String FIND_ORDER_FOR_SPECIFIC_DATE = "SELECT distinct {oe." + OrderEntryModel.ORDER + "} FROM {"
			+ OrderEntryModel._TYPECODE + " AS oe JOIN " + OrderModel._TYPECODE + " AS o ON {oe." + OrderEntryModel.ORDER + "} = {o."
			+ OrderModel.PK + "}"
			+ " and {o." + OrderModel.BOOKINGJOURNEYTYPE + "} = ?bookingJourneyType"
			+ " JOIN " + TravelOrderEntryInfoModel._TYPECODE + " AS toi ON {oe." + OrderEntryModel.TRAVELORDERENTRYINFO + "} = {toi."
			+ TravelOrderEntryInfoModel.PK + "}"
			+ " JOIN TravelOrderEntryInfoTransportOfferingRelation AS rel ON {toi." + TravelOrderEntryInfoModel.PK
			+ "} = {rel.source} JOIN " + TransportOfferingModel._TYPECODE + " AS to ON {to." + TransportOfferingModel.PK
			+ "} = {rel.target} and "
			+ "{to." + TransportOfferingModel.DEPARTURETIME + "} <= ?rangeEndTime and " + "{to."
			+ TransportOfferingModel.DEPARTURETIME + "} >= ?rangeStartTime}";

	// Order Version Query
	private static final String FIND_ORDERS_BY_CODE_VERSIONID_QUERY =
			"SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE + "} WHERE {" + OrderModel.CODE
					+ "} = ?code AND {" + OrderModel.VERSIONID + "} =?versionId";


	private PagedFlexibleSearchService pagedFlexibleSearchService;

	@Override
	public SearchPageData<AbstractOrderModel> findPagedPendingOrdersForInventoryApproval(final PageableData pageableData)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_PENDING_ORDERS_QUERY);
		return getPagedFlexibleSearchService().search(query, pageableData);
	}

	@Override
	public List<OrderModel> findVacationsForCurrentUser(final UserModel user)
	{
		validateParameterNotNull(user, "user must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderModel.USER, user);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_VACATION_ORDERS_BY_USER_QUERY, params);
		final SearchResult<OrderModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getCount() < 1 ? null : searchResult.getResult();
	}

	@Override
	public OrderModel getOrderByBookingReference(final String bookingReference, final UserModel customer)
	{
		final StringBuilder buf = new StringBuilder();

		// select the chosen order using his code
		buf.append("SELECT  {p:" + AbstractOrderEntryModel.PK + "} ");
		buf.append("FROM {" + AbstractOrderEntryModel._TYPECODE + " as p } ");
		buf.append("WHERE {p:" + AbstractOrderEntryModel.BOOKINGREFERENCE + "} = ?bookingReference ");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(buf.toString());
		query.addQueryParameter("bookingReference", bookingReference);
		final List<AbstractOrderEntryModel> matchedOrderEntry = getFlexibleSearchService().<AbstractOrderEntryModel>search(query)
				.getResult();
		if (CollectionUtils.isEmpty(matchedOrderEntry))
		{
			return null;
		}
		OrderModel order = null;
		if (matchedOrderEntry.size() > 1)
		{
			for (final AbstractOrderEntryModel orderEntry : matchedOrderEntry)
			{
				if (orderEntry.getOrder() instanceof OrderModel)
				{
					order = (OrderModel) orderEntry.getOrder();
				}
			}
		}
		return order;
	}

	@Override
	public List<OrderModel> findOrdersForSpecificSailingDate(final Date startDate, final Date endDate)
	{
		final Map<String, Object> params = new HashMap<>();

		params.put("rangeStartTime", startDate);
		params.put("rangeEndTime", endDate);
		params.put("bookingJourneyType", BookingJourneyType.BOOKING_PACKAGE);

		return doSearch(FIND_ORDER_FOR_SPECIFIC_DATE, params, OrderModel.class);
	}

	protected <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class<T> resultClass)
	{
		final SearchResult<T> searchResult = search(createSearchQuery(query, params, resultClass));
		return searchResult.getResult();
	}

	protected <T> FlexibleSearchQuery createSearchQuery(final String query, final Map<String, Object> params,
			final Class<T> resultClass)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		if (params != null)
		{
			fQuery.addQueryParameters(params);
		}
		fQuery.setResultClassList(Collections.singletonList(resultClass));
		return fQuery;
	}

	@Override
	public List<OrderEntryModel> findOrderEntriesByEBookingCode(final String eBookingCode)
	{
		validateParameterNotNull(eBookingCode, "eBookingCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderEntryModel.BOOKINGREFERENCE, eBookingCode);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ORDERENTRIES_BY_EBOOKING_CODE_QUERY, params);
		final SearchResult<OrderEntryModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getCount() < 1 ? null : searchResult.getResult();
	}

	@Override
	public OrderModel findOrderByCartId(final String cartId)
	{
		validateParameterNotNull(cartId, "cartId must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderModel.CARTID, cartId);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ORDER_BY_CART_ID_QUERY, params);
		final SearchResult<OrderModel> searchResult = this.getFlexibleSearchService().search(fsq);
		if (searchResult.getCount() < 1)
		{
			return null;
		}
		return searchResult.getResult().get(0);
	}

	@Override
	public List<OrderModel> findOrderByCode(final String orderCode)
	{
		validateParameterNotNull(orderCode, "orderCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderModel.CODE, orderCode);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ORDER_BY_CODE_QUERY, params);
		final SearchResult<OrderModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<OrderModel> findAllOrderByCode(final String orderCode)
	{
		validateParameterNotNull(orderCode, "orderCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderModel.CODE, orderCode);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ORDER_BY_CODE_CREATED_QUERY, params);
		final SearchResult<OrderModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<OrderModel> findOrderByCode(final String orderCode, final String versionId)
	{
		validateParameterNotNull(orderCode, "orderCode must not be null!");
		validateParameterNotNull(versionId, "versionId must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(OrderModel.CODE, orderCode);
		params.put(OrderModel.VERSIONID, versionId);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ORDERS_BY_CODE_VERSIONID_QUERY, params);
		final SearchResult<OrderModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	protected PagedFlexibleSearchService getPagedFlexibleSearchService()
	{
		return pagedFlexibleSearchService;
	}

	@Required
	public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}

}

