/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.model.SailingReturnPairMappingModel;
import com.bcf.core.order.dao.SailingReturnPairMappingDao;


public class DefaultSailingReturnPairMappingDao extends DefaultGenericDao<SailingReturnPairMappingModel>
		implements SailingReturnPairMappingDao
{
	public DefaultSailingReturnPairMappingDao(final String typecode)
	{
		super(typecode);
	}

	private static final String BOOKING_REFERENCE_PARAM = "<BOOKINGREFERENCE>";
	private static final String GET_ORDER_USING_BOOKING_REFRENCE =
			"SELECT {" + SailingReturnPairMappingModel.PK + "} FROM {" + SailingReturnPairMappingModel._TYPECODE + "} WHERE {"
					+ SailingReturnPairMappingModel.INOUTREFERENCEPAIRS + "} LIKE '%" + BOOKING_REFERENCE_PARAM + "%'";

	@Override
	public SailingReturnPairMappingModel getMappingItemUsingBookingReference(final String bookingReference)
	{
		validateParameterNotNull(bookingReference, "bookingReference must not be null");

		final SearchResult<SailingReturnPairMappingModel> result = getFlexibleSearchService()
				.search(GET_ORDER_USING_BOOKING_REFRENCE.replace(BOOKING_REFERENCE_PARAM, bookingReference));

		if (result.getCount() > 1)
		{
			throw new AmbiguousIdentifierException(
					"More than one SailingReturnPairMappingModel found for bookingReference " + bookingReference);
		}
		if(result.getCount()<1)
		{
			return null;
		}

		return result.getResult().get(0);
	}

	@Override
	public SailingReturnPairMappingModel findMappingItem(final String orderNumber)
	{
		validateParameterNotNull(orderNumber, "orderNumber must not be null!");
		SailingReturnPairMappingModel mappingModel = null;
		final Map<String, Object> params = new HashMap<>();
		params.put(SailingReturnPairMappingModel.ORDERNUMBER, orderNumber);
		final List<SailingReturnPairMappingModel> mappingModelList = find(params);
		if (CollectionUtils.isNotEmpty(mappingModelList))
		{
			if (mappingModelList.size() > 1)
			{
				throw new AmbiguousIdentifierException(
						"More than one SailingReturnPairMappingModel found for orderNumber " + orderNumber);
			}
			mappingModel = mappingModelList.get(0);
		}
		return mappingModel;
	}
}
