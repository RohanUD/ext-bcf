/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.order.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.order.dao.BcfCommerceCartDao;


public class DefaultBcfCommerceCartDao extends DefaultCommerceCartDao implements BcfCommerceCartDao
{
	private static final String FROM = "} FROM {";
	private static final String FIND_CARTS_FOR_REMOVAL = "SELECT {ce." + CartEntryModel.ORDER + FROM
			+ CartEntryModel._TYPECODE + " AS ce}, " + "{" + TravelOrderEntryInfoModel._TYPECODE
			+ " AS toi JOIN TravelOrderEntryInfoTransportOfferingRelation AS rel ON {toi." + TravelOrderEntryInfoModel.PK
			+ "} = {rel.source} }, {" + TransportOfferingModel._TYPECODE + " AS to } WHERE { to." + TransportOfferingModel.PK
			+ "} = {rel.target} and {to." + TransportOfferingModel.DEPARTURETIME + "} <= ?currentDateTime";

	private static final String FIND_CARTS_BY_SITE_QUOTE = "SELECT {" + CartModel.PK
			+ FROM + CartModel._TYPECODE + "} WHERE {" + CartModel.SITE
			+ "} = ?site AND {" + CartModel.QUOTE + "} = ?quote";

	private static final String FIND_CARTS_BY_QUOTE_THRESHOLD =
			"SELECT {" + CartModel.PK + FROM + CartModel._TYPECODE + "} WHERE{" + CartModel.QUOTE
					+ "}=?quote AND{" + CartModel.MODIFIEDTIME + "}<?modifiedtime";

	protected static final String FIND_CART_FOR_CARTCODE_AND_SITE = SELECTCLAUSE + "WHERE {" + CartModel.CODE + "} = ?code AND {"
			+ CartModel.SITE + "} = ?site " + ORDERBYCLAUSE;

	protected final static String FIND_CART_FOR_GUID_AND_USER_AND_SITE = SELECTCLAUSE + "WHERE {" + CartModel.GUID + "} = ?guid"
			+ " AND {" + CartModel.USER + "} = ?user AND {" + CartModel.SITE + "} = ?site AND {" + CartModel.QUOTE + "} = ?quote "
			+ ORDERBYCLAUSE;

	protected final static String FIND_CART_FOR_USER_AND_SITE = SELECTCLAUSE + "WHERE {" + CartModel.USER + "} = ?user AND {"
			+ CartModel.SITE + "} = ?site " + NOT_SAVED_CART_CLAUSE + NOT_QUOTE_CART_CLAUSE + "AND {" + CartModel.QUOTE
			+ "} = ?quote " + ORDERBYCLAUSE;


	@Override
	public List<CartModel> getCartsForRemovalForSiteAndUser(final Date modifiedBefore, final BaseSiteModel site,
			final UserModel user)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("currentDateTime", new Date());

		return doSearch(FIND_CARTS_FOR_REMOVAL, params, CartModel.class);
	}

	@Override
	public List<CartModel> findCartsBySiteAndQuote(final BaseSiteModel site, final boolean isQuote)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(CartModel.SITE, site);
		params.put(CartModel.QUOTE, isQuote ? 1 : 0);
		return doSearch(FIND_CARTS_BY_SITE_QUOTE, params, CartModel.class);
	}


	@Override
	public List<CartModel> findCartsByQuoteAndThreshold(final boolean isQuote, final Date threshold)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(CartModel.QUOTE, isQuote ? 1 : 0);
		params.put(CartModel.MODIFIEDTIME, threshold);
		return doSearch(FIND_CARTS_BY_QUOTE_THRESHOLD, params, CartModel.class);
	}


	@Override
	public CartModel getCartForCodeAndSite(final String code, final BaseSiteModel site)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("code", code);
		params.put("site", site);
		final List<CartModel> carts = doSearch(FIND_CART_FOR_CARTCODE_AND_SITE, params, CartModel.class, 1);
		if (carts != null && !carts.isEmpty())
		{
			return carts.get(0);
		}
		return null;
	}


	@Override
	public CartModel getCartForGuidAndSiteAndUser(final String guid, final BaseSiteModel site, final UserModel user)
	{
		if (guid != null)
		{
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("guid", guid);
			params.put("site", site);
			params.put("user", user);
			params.put("quote", 0);
			final List<CartModel> carts = doSearch(FIND_CART_FOR_GUID_AND_USER_AND_SITE, params, CartModel.class, 1);
			if (carts != null && !carts.isEmpty())
			{
				return carts.get(0);
			}
			return null;
		}
		else
		{
			return getCartForSiteAndUser(site, user);
		}
	}

	
	@Override
	public CartModel getCartForSiteAndUser(final BaseSiteModel site, final UserModel user)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", site);
		params.put("user", user);
		params.put("quote", 0);
		final List<CartModel> carts = doSearch(FIND_CART_FOR_USER_AND_SITE, params, CartModel.class, 1);
		if (carts != null && !carts.isEmpty())
		{
			return carts.get(0);
		}
		return null;
	}
}
