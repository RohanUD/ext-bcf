/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.TransportOfferingStatus;
import de.hybris.platform.travelservices.enums.TransportOfferingType;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.VendorService;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.services.BcfTravelProviderService;


public class TravelSectorPrepareInterceptor implements PrepareInterceptor
{
	private static final String DEFAULT = "defaultTransportOffering";
	private ModelService modelService;
	private BcfTransportOfferingService transportOfferingService;
	private BcfTransportVehicleService bcfTransportVehicleService;
	private BcfTravelProviderService bcfTravelProviderService;
	private VendorService vendorService;

	@Override
	public void onPrepare(final Object model, final InterceptorContext ctx)
	{
		if (model instanceof TravelSectorModel && getModelService().isNew(model))
		{
			final TravelSectorModel travelSectorModel = (TravelSectorModel) model;
			if (Objects.isNull(travelSectorModel.getOrigin()) || Objects.isNull(travelSectorModel.getDestination()))
			{
				return;
			}
			TransportOfferingModel transportOfferingModel = null;

			if (Objects.nonNull(travelSectorModel))
			{
				transportOfferingModel = getTransportOfferingService().getDefaultTransportOfferingForSector(travelSectorModel.getCode());
			}
			if (Objects.isNull(transportOfferingModel))
			{
				final List<TransportOfferingModel> transportOfferingModelList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(travelSectorModel.getTransportOffering()))
				{
					transportOfferingModelList.addAll(travelSectorModel.getTransportOffering());
				}
				transportOfferingModel = createTransportOfferingModel(travelSectorModel);
				transportOfferingModelList.add(transportOfferingModel);

				travelSectorModel.setTransportOffering(transportOfferingModelList);
			}
		}
	}

	protected TransportOfferingModel createTransportOfferingModel(final TravelSectorModel travelSector)
	{
		final TransportOfferingModel transportOffering = getModelService().create(TransportOfferingModel.class);
		transportOffering.setCode(travelSector.getCode());
		transportOffering.setActive(Boolean.TRUE);
		transportOffering.setOriginTransportFacility(travelSector.getOrigin());
		transportOffering.setDestinationTransportFacility(travelSector.getDestination());
		transportOffering.setNumber(DEFAULT);
		transportOffering.setArrivalTime(new Date());
		transportOffering.setDepartureTime(new Date());
		transportOffering.setTravelSector(travelSector);
		transportOffering.setTravelProvider(getBcfTravelProviderService().getDefaultProviderModel());
		transportOffering.setVendor(getVendorService().getVendor("bcferries"));
		transportOffering.setTransportVehicle(getBcfTransportVehicleService().getDefaultTransportVehicle());
		transportOffering.setName(travelSector.getCode(), Locale.ENGLISH);
		transportOffering.setName(travelSector.getCode(), Locale.FRENCH);
		transportOffering.setDefault(Boolean.TRUE);
		transportOffering.setType(TransportOfferingType.CROSSING);
		transportOffering.setStatus(TransportOfferingStatus.SCHEDULED);
		return transportOffering;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BcfTransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final BcfTransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	protected VendorService getVendorService()
	{
		return vendorService;
	}

	@Required
	public void setVendorService(final VendorService vendorService)
	{
		this.vendorService = vendorService;
	}

	protected BcfTransportVehicleService getBcfTransportVehicleService()
	{
		return bcfTransportVehicleService;
	}

	@Required
	public void setBcfTransportVehicleService(final BcfTransportVehicleService bcfTransportVehicleService)
	{
		this.bcfTransportVehicleService = bcfTransportVehicleService;
	}

	protected BcfTravelProviderService getBcfTravelProviderService()
	{
		return bcfTravelProviderService;
	}

	@Required
	public void setBcfTravelProviderService(final BcfTravelProviderService bcfTravelProviderService)
	{
		this.bcfTravelProviderService = bcfTravelProviderService;
	}

}
