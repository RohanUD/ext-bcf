/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.workflow.BcfWorkflowService;


public class ServiceNoticeRemoveInterceptor implements RemoveInterceptor<ServiceNoticeModel>
{
	@Resource(name = "bcfWorkflowService")
	BcfWorkflowService bcfWorkflowService;

	@Override
	public void onRemove(final ServiceNoticeModel model, final InterceptorContext interceptorContext)
	{
		if (Objects.nonNull(model))
		{
			List<WorkflowModel> existingWorkflows = bcfWorkflowService.getActiveWorkflowForServiceNotice(model);

			if (Objects.nonNull(existingWorkflows))
			{
				bcfWorkflowService.terminateWorkflow(existingWorkflows);
			}
		}
	}
}
