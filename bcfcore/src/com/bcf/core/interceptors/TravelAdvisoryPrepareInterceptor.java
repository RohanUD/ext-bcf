/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.interceptors;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.interceptors.helper.TextFieldCleanupHelper;
import com.bcf.core.model.TravelAdvisoryModel;


public class TravelAdvisoryPrepareInterceptor implements PrepareInterceptor<TravelAdvisoryModel>
{
	private I18NService i18NService;

	@Override
	public void onPrepare(final TravelAdvisoryModel model, final InterceptorContext ctx)
	{
		if (ctx.isModified(model) && ctx.getDirtyAttributes(model).containsKey(TravelAdvisoryModel.ADVISORYDESCRIPTION))
		{
			getI18NService().getSupportedLocales().forEach(locale ->
			{
				model.setAdvisoryDescription(TextFieldCleanupHelper.getTextWithStrippedPTag(model.getAdvisoryDescription(locale)),
						locale);
			});
		}
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
