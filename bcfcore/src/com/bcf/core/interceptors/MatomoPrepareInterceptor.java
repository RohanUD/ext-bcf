/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.interceptors;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.interceptors.helper.TextFieldCleanupHelper;
import com.bcf.core.model.MatomoModel;


public class MatomoPrepareInterceptor implements PrepareInterceptor<MatomoModel>
{
	private I18NService i18NService;

	@Override
	public void onPrepare(final MatomoModel model, final InterceptorContext ctx)
	{
		getI18NService().getSupportedLocales().forEach(locale ->
		{
			model.setHeaderScript(TextFieldCleanupHelper.getTextWithStrippedPTag(model.getHeaderScript(locale)), locale);
			model.setBodyScript(TextFieldCleanupHelper.getTextWithStrippedPTag(model.getBodyScript(locale)), locale);
		});
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
