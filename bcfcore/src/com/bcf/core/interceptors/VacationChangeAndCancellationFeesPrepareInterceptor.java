/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.model.vacation.VacationChangeAndCancellationFeesModel;


public class VacationChangeAndCancellationFeesPrepareInterceptor implements PrepareInterceptor<VacationChangeAndCancellationFeesModel>
{

	PersistentKeyGenerator vacationChangeAndCancellationFeesCodeGenerator;

	@Override
	public void onPrepare(final VacationChangeAndCancellationFeesModel model, final InterceptorContext ctx)
	{
		if(StringUtils.isEmpty(model.getCode())){
			model.setCode(vacationChangeAndCancellationFeesCodeGenerator.generate().toString());
		}
	}

	public PersistentKeyGenerator getVacationChangeAndCancellationFeesCodeGenerator()
	{
		return vacationChangeAndCancellationFeesCodeGenerator;
	}

	public void setVacationChangeAndCancellationFeesCodeGenerator(
			final PersistentKeyGenerator vacationChangeAndCancellationFeesCodeGenerator)
	{
		this.vacationChangeAndCancellationFeesCodeGenerator = vacationChangeAndCancellationFeesCodeGenerator;
	}
}
