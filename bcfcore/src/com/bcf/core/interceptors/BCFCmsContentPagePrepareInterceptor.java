/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.core.interceptors;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.interceptor.impl.CmsContentPagePrepareInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import java.util.Collection;
import java.util.Objects;


public class BCFCmsContentPagePrepareInterceptor extends CmsContentPagePrepareInterceptor
{
	@Override
	protected void resetHomepageFlag(final Collection<AbstractPageModel> contentPages, final ContentPageModel currentPageModel,
			final InterceptorContext ctx)
	{
		contentPages.forEach(page ->
		{
			if (page instanceof ContentPageModel && ((ContentPageModel) page).isHomepage() && !Objects
					.equals(page.getUid(), currentPageModel.getUid()))
			{
				((ContentPageModel) page).setHomepage(false);
				ctx.getModelService().save(page);
			}
		});
	}
}
