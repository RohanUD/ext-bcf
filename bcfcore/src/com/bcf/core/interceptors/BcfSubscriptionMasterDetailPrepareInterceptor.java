/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.interceptors;

import reactor.util.StringUtils;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import javax.annotation.Resource;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;


public class BcfSubscriptionMasterDetailPrepareInterceptor implements PrepareInterceptor<CRMSubscriptionMasterDetailModel>
{
	@Resource
	private EnumerationService enumerationService;
	@Override
	public void onPrepare(final CRMSubscriptionMasterDetailModel model,
			final InterceptorContext ctx)
	{
		if (model != null && model.getSubscriptionType() != null && StringUtils.isEmpty(model.getSubscriptionTypeName()))
		{
			model.setSubscriptionTypeName(enumerationService.getEnumerationName(model.getSubscriptionType()));
		}
	}
}
