/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.interceptors.helper;

import org.apache.commons.lang.StringUtils;


public class TextFieldCleanupHelper
{
	private static final String P_TAG_START = "<p>";
	private static final String P_TAG_END = "</p>";

	public static String getTextWithStrippedPTag(final String propertyValue)
	{
		if (StringUtils.isEmpty(propertyValue))
		{
			return propertyValue;
		}
		String updatedPropertyValue = propertyValue.trim();
		if (updatedPropertyValue.startsWith(P_TAG_START))
		{
			updatedPropertyValue = updatedPropertyValue.replaceFirst(P_TAG_START, StringUtils.EMPTY);
		}
		final int lastPTagIndex = updatedPropertyValue.lastIndexOf(P_TAG_END);
		if (lastPTagIndex > 0)
		{
			updatedPropertyValue = updatedPropertyValue.substring(0, lastPTagIndex);
		}
		return updatedPropertyValue;
	}
}
