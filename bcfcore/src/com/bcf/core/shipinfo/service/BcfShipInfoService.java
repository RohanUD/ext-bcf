/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.shipinfo.service;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import java.util.List;


public interface BcfShipInfoService
{
	ShipInfoModel getShipInfoForCode(String code);

	List<ShipInfoModel> getShipInfos();

	List<ShipInfoModel> getShipInfos(int startIndex, int pageNumber,
			int recordsPerPage);

	ShipInfoModel getShipInfoByVesselCode(String code);

	ShipInfoModel getShipInfoByOSSVesselCode(String ossVesselCode);

	SearchPageData<ShipInfoModel> getPaginatedShipInfos(final int pageSize, final int currentPage);
}
