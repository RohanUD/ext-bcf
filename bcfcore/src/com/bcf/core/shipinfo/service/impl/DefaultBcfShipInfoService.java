/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.shipinfo.service.impl;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;
import com.bcf.core.shipinfo.dao.BcfShipInfoDao;
import com.bcf.core.shipinfo.service.BcfShipInfoService;


public class DefaultBcfShipInfoService implements BcfShipInfoService
{

	GenericDao<ShipInfoModel> shipInfoModelGenericDao;
	private BcfShipInfoDao bcfShipInfoDao;

	@Override
	public ShipInfoModel getShipInfoForCode(final String code)
	{
		final Map<String, String> params = new HashMap<>();
		params.put(ShipInfoModel.CODE, code);
		final List<ShipInfoModel> shipInfoModels = getShipInfoModelGenericDao().find(params);
		if (CollectionUtils.isEmpty(shipInfoModels))
		{
			return null;
		}

		return shipInfoModels.get(0);
	}

	@Override
	public ShipInfoModel getShipInfoByVesselCode(final String code)
	{
		return getBcfShipInfoDao().findShipInfoByVesselCode(code);
	}

	@Override
	public ShipInfoModel getShipInfoByOSSVesselCode(final String ossVesselCode)
	{
		return getBcfShipInfoDao().findShipInfoByOSSVesselCode(ossVesselCode);
	}

	@Override
	public List<ShipInfoModel> getShipInfos()
	{
		return getBcfShipInfoDao().findShipInfos();
	}

	@Override
	public SearchPageData<ShipInfoModel> getPaginatedShipInfos(final int pageSize, final int currentPage)
	{
		return getBcfShipInfoDao().getPaginatedShipInfos(pageSize, currentPage);
	}

	@Override
	public List<ShipInfoModel> getShipInfos(final int startIndex, final int pageNumber,
			final int recordsPerPage)
	{
		return getBcfShipInfoDao().findShipInfos(startIndex, pageNumber, recordsPerPage);
	}

	protected GenericDao<ShipInfoModel> getShipInfoModelGenericDao()
	{
		return shipInfoModelGenericDao;
	}

	@Required
	public void setShipInfoModelGenericDao(
			final GenericDao<ShipInfoModel> shipInfoModelGenericDao)
	{
		this.shipInfoModelGenericDao = shipInfoModelGenericDao;
	}

	protected BcfShipInfoDao getBcfShipInfoDao()
	{
		return bcfShipInfoDao;
	}

	@Required
	public void setBcfShipInfoDao(final BcfShipInfoDao bcfShipInfoDao)
	{
		this.bcfShipInfoDao = bcfShipInfoDao;
	}
}
