/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.shipinfo.dao;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import java.util.List;


/**
 * Interface that exposes Transport Vehicle Info specific DAO services
 */
public interface BcfShipInfoDao
{
	List<ShipInfoModel> findShipInfos();

	List<ShipInfoModel> findShipInfos(int startIndex, int pageNumber,
			int recordsPerPage);

	ShipInfoModel findShipInfoByVesselCode(String code);

	ShipInfoModel findShipInfoByOSSVesselCode(String ossVesselCode);

	SearchPageData<ShipInfoModel> getPaginatedShipInfos(final int pageSize, final int currentPage);
}
