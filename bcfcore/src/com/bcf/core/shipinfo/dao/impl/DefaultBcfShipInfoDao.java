/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.shipinfo.dao.impl;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.pagination.GlobalPaginationResultsBuilder;
import com.bcf.core.shipinfo.dao.BcfShipInfoDao;


public class DefaultBcfShipInfoDao extends DefaultGenericDao<ShipInfoModel>
		implements BcfShipInfoDao
{
	private static final String FIND_ALL_SHIP_INFOS = "SELECT {PK} FROM {ShipInfo} WHERE {shipDisplay}=?shipDisplay ORDER BY {name}";
	private GlobalPaginationResultsBuilder globalPaginationResultsBuilder;

	public DefaultBcfShipInfoDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ShipInfoModel> findShipInfos()
	{

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("shipDisplay", 1);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ALL_SHIP_INFOS, queryParams);
		final SearchResult<ShipInfoModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public SearchPageData<ShipInfoModel> getPaginatedShipInfos(final int pageSize, final int currentPage)
	{
		return getGlobalPaginationResultsBuilder().createPaginatedResults(FIND_ALL_SHIP_INFOS,
				Collections.singletonMap(ShipInfoModel.SHIPDISPLAY, 1), pageSize, currentPage);
	}

	@Override
	public List<ShipInfoModel> findShipInfos(final int startIndex, final int pageNumber,
			final int recordsPerPage)
	{
		final String query =
				"SELECT {" + ShipInfoModel.PK + "} FROM {" + ShipInfoModel._TYPECODE + "} WHERE {" + ShipInfoModel.SHIPDISPLAY
						+ "}=1 ORDER BY {" + ShipInfoModel.NAME + "}";
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		fQuery.setCount(recordsPerPage);
		fQuery.setNeedTotal(true);
		fQuery.setStart(startIndex);
		final SearchResult<ShipInfoModel> searchResult = getFlexibleSearchService().search(fQuery);
		return searchResult.getResult();
	}

	/**
	 * Method returns a TransportVehicleModel from the database
	 *
	 * @param code
	 * @return transportVehicleModel
	 */
	@Override
	public ShipInfoModel findShipInfoByVesselCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap();
		params.put(ShipInfoModel.CODE, code);
		final List<ShipInfoModel> shipInfoModels = this.find(params);
		return CollectionUtils.isNotEmpty(shipInfoModels) ? shipInfoModels.get(0) : null;
	}

	@Override
	public ShipInfoModel findShipInfoByOSSVesselCode(final String ossVesselCode)
	{
		ServicesUtil.validateParameterNotNull(ossVesselCode, "ossVesselCode must not be null!");
		final Map<String, Object> params = new HashMap();
		params.put(ShipInfoModel.OSSVESSELCODE, ossVesselCode);
		final List<ShipInfoModel> shipInfoModels = this.find(params);
		return CollectionUtils.isNotEmpty(shipInfoModels) ? shipInfoModels.get(0) : null;
	}

	public GlobalPaginationResultsBuilder getGlobalPaginationResultsBuilder()
	{
		return globalPaginationResultsBuilder;
	}

	@Required
	public void setGlobalPaginationResultsBuilder(final GlobalPaginationResultsBuilder globalPaginationResultsBuilder)
	{
		this.globalPaginationResultsBuilder = globalPaginationResultsBuilder;
	}
}
