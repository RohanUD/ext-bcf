/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.user.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.user.BcfUserService;


public class BcfDefaultUserService extends DefaultUserService implements BcfUserService
{
	private DefaultGenericDao<CustomerModel> bcfCustomerGenericDao;

	@Resource(name = "agentGroupRoleMap")
	private Map<String, String> agentGroupRoleMap;

	@Override
	public UserModel getUserForUpdatedEmailRequest(final String email)
	{
		final List<CustomerModel> customers = getBcfCustomerGenericDao()
				.find(Collections.singletonMap(CustomerModel.REQUESTEDUPDATEEMAIL, email));
		return customers.iterator().hasNext() ? customers.iterator().next() : null;
	}

	@Override
	public boolean isB2BCustomer()
	{
		return Objects.nonNull(getSessionService().getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA));
	}

	protected DefaultGenericDao<CustomerModel> getBcfCustomerGenericDao()
	{
		return bcfCustomerGenericDao;
	}

	@Required
	public void setBcfCustomerGenericDao(
			final DefaultGenericDao<CustomerModel> bcfCustomerGenericDao)
	{
		this.bcfCustomerGenericDao = bcfCustomerGenericDao;
	}

	@Override
	public Map<String, String> findUserRoles()
	{
		final Map<String, String> grantedRoles = new HashMap<>();
		if (Objects.nonNull(getCurrentUser()) && CollectionUtils.isNotEmpty(getCurrentUser().getGroups()))
		{
			getCurrentUser().getGroups().forEach(group ->
			{
				if (agentGroupRoleMap.containsKey(group.getUid()))
				{
					grantedRoles.put(agentGroupRoleMap.get(group.getUid()), group.getUid());
				}
			});
		}
		return grantedRoles;
	}
}
