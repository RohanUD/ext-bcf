/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.events.SubmitOrderEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.event.SubmitOrderEventListener;
import org.apache.log4j.Logger;


/**
 * Listener for order submits.
 */
public class BcfSubmitOrderEventListener extends SubmitOrderEventListener
{
	public static final String ORDER_PROCESS_NAME = "order-process";

	private static final Logger LOG = Logger.getLogger(BcfSubmitOrderEventListener.class);

	@Override
	protected void onSiteEvent(final SubmitOrderEvent event)
	{
		final OrderModel order = event.getOrder();

		final OrderProcessModel orderProcessModel = getBusinessProcessService().createProcess(ORDER_PROCESS_NAME+"-" + order.getCode()+"-"+System.currentTimeMillis(),
				ORDER_PROCESS_NAME);

		orderProcessModel.setOrder(order);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);

		LOG.info("Order Approved");
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final SubmitOrderEvent event)
	{
		final OrderModel order = event.getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}
}
