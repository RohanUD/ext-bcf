/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.cronjob.UpdateStockLevelsToAccommodationOfferingJob;
import de.hybris.platform.travelservices.model.cronjob.UpdateStockLevelsToAccommodationOfferingCronJobModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.IntRange;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelCommerceStockService;



public class BcfUpdateStockLevelsToAccommodationOfferingJob extends UpdateStockLevelsToAccommodationOfferingJob
{
	private static final Logger LOG = Logger.getLogger(BcfUpdateStockLevelsToAccommodationOfferingJob.class);
	private static final String CATALOG_NAME = "bcfProductCatalog";
	private static final String SEPARATOR = ";";
	private static final String LIST_SEPARATOR = ",";
	private static final int DEF_AVAILABILITY = 50;
	public static final String DATE_PATTERN = "MMM dd yyyy";
	private Long scheduledDays;
	private Boolean setForceInStock;

	private AccommodationOfferingService accommodationOfferingService;
	private ProductService productService;
	private CatalogVersionService catalogVersionService;
	private String baseExtDir;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	@Override
	public PerformResult perform(
			final UpdateStockLevelsToAccommodationOfferingCronJobModel updateStockLevelsToAccommodationOfferingCronJobModel)
	{
		LOG.info("Start performing UpdateStockLevelsToAccommodationOfferingJob...");

		final List<AccommodationOfferingModel> accommodationOfferings = getAccommodationOfferingService()
				.getAccommodationOfferings();
		final List<StockLevelModel> stockLevelsToSave = new ArrayList<>();

		stockLevelsToSave
				.addAll(getAccommodationStockLevels(updateStockLevelsToAccommodationOfferingCronJobModel, accommodationOfferings));

		stockLevelsToSave
				.addAll(getProductStockLevels(updateStockLevelsToAccommodationOfferingCronJobModel, accommodationOfferings));

		getBcfTravelCommerceStockService().batchSavingOfStockLevels(stockLevelsToSave);

		LOG.info("UpdateStockLevelsToAccommodationOfferingJob completed.");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * Gets the accommodation stock levels.
	 *
	 * @param updateStockLevelsToAccommodationOfferingCronJobModel the update stock levels to accommodation offering cron job model
	 * @param accommodationOfferings                               the accommodation offerings
	 * @return the accommodation stock levels
	 */
	@Override
	protected List<StockLevelModel> getAccommodationStockLevels(
			final UpdateStockLevelsToAccommodationOfferingCronJobModel updateStockLevelsToAccommodationOfferingCronJobModel,
			final List<AccommodationOfferingModel> accommodationOfferings)
	{
		final Integer maxAvailability = Objects.nonNull(updateStockLevelsToAccommodationOfferingCronJobModel.getMaxAvailability())
				? updateStockLevelsToAccommodationOfferingCronJobModel.getMaxAvailability()
				: Integer.valueOf(DEF_AVAILABILITY);

		final List<StockLevelModel> stockLevels = new ArrayList<>();

		accommodationOfferings.forEach(accommodationOfferingModel ->
				updateStockLevelForAccommodation(true, maxAvailability, stockLevels, accommodationOfferingModel,
						accommodationOfferingModel.getAccommodations()));
		return stockLevels;
	}

	/**
	 * Update stock level for accommodation.
	 *
	 * @param SIMULATION_MODE       the simulation mode
	 * @param maxAvailability       the max availability
	 * @param stockLevels           the stock levels
	 * @param accommodationOffering the accommodation offering
	 * @param accommodationCodes    the accommodationCodes
	 */
	protected void updateStockLevelForAccommodation(final boolean SIMULATION_MODE, final Integer maxAvailability,
			final List<StockLevelModel> stockLevels, final AccommodationOfferingModel accommodationOffering,
			final List<String> accommodationCodes)
	{
		final LocalDateTime startDate = LocalDateTime.now();
		final LocalDateTime endDate = startDate.plusDays(Objects.nonNull(getScheduledDays()) ? getScheduledDays() : 365);

		final List<AccommodationModel> accommodations = accommodationCodes.stream()
				.map(code -> (AccommodationModel) getProductService()
						.getProductForCode(getCatalogVersionService().getCatalogVersion(CATALOG_NAME, "Staged"), code))
				.collect(Collectors.toList());

		Stream.iterate(startDate, date -> date.plusDays(1)).limit(ChronoUnit.DAYS.between(startDate, endDate) + 1)
				.forEach(date -> setStockLevelForAccommodation(accommodationOffering, accommodations, date, maxAvailability, null,
						stockLevels, SIMULATION_MODE));
		setStockLevelForRoomRates(accommodationOffering, accommodations, stockLevels);
	}

	@Override
	protected List<StockLevelModel> getProductStockLevels(
			final UpdateStockLevelsToAccommodationOfferingCronJobModel updateStockLevelsToAccommodationOfferingCronJobModel,
			final List<AccommodationOfferingModel> accommodationOfferings)
	{

		final List<StockLevelModel> stockLevels = new ArrayList<>();
		final Map<String, Integer> stockMap = updateStockLevelsToAccommodationOfferingCronJobModel
				.getServiceProductsStockLevels();

		final LocalDateTime startDate = LocalDateTime.now();
		final LocalDateTime endDate = startDate.plusDays(Objects.nonNull(getScheduledDays()) ? getScheduledDays() : 365);

		for (final AccommodationOfferingModel accommodationOffering : accommodationOfferings)
		{
			stockMap.entrySet().forEach(entry -> Stream.iterate(startDate, date -> date.plusDays(1))
					.limit(ChronoUnit.DAYS.between(startDate, endDate) + 1).forEach(date -> {
						final Date dateToSet = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
						if (CollectionUtils.isNotEmpty(accommodationOffering.getStockLevels()))
						{
							final Optional<StockLevelModel> stockLevelOptional = accommodationOffering.getStockLevels().stream()
									.filter(stockLevel -> StringUtils.equalsIgnoreCase(stockLevel.getProductCode(), entry.getKey())
											&& TravelDateUtils.isSameDate(stockLevel.getDate(), dateToSet))
									.findFirst();
							if (stockLevelOptional.isPresent())
							{
								stockLevelOptional.get().setAvailable(entry.getValue());
								stockLevels.add(stockLevelOptional.get());
								return;
							}
						}
						stockLevels.add(createStockLevel(accommodationOffering, entry, dateToSet));
					}));
		}
		return stockLevels;
	}

	@Override
	protected Map<String, List<String>> parseMappingFile(final String mappingFilePath)
	{
		final String completePath = baseExtDir + mappingFilePath;
		LOG.info("Start parsing file " + completePath);
		try (final BufferedReader reader = getReader(completePath))
		{
			return reader.lines().filter(StringUtils::isNotBlank)
					.collect(Collectors.toMap(this::getAccommodationOffering, this::getAccommodations));
		}
		catch (final IOException e)
		{
			throw new UncheckedIOException(e);
		}
	}

	@Override
	protected BufferedReader getReader(final String completePath) throws FileNotFoundException
	{
		return new BufferedReader(new FileReader(completePath));
	}

	@Override
	protected Map<String, Integer> parseAvailabilityFile(final String availabilityDataFilePath)
	{
		final String completePath = baseExtDir + availabilityDataFilePath;
		LOG.info("Start parsing availability file " + completePath);
		try (final BufferedReader reader = getReader(completePath))
		{
			return reader.lines().filter(StringUtils::isNotBlank)
					.collect(Collectors.toMap(this::getAccommodation, this::getAvailability));
		}
		catch (final IOException e)
		{
			throw new UncheckedIOException(e);
		}
	}

	/**
	 * If a mapping file is provided stock levels are populated according to its content otherwise their availability is
	 * simulated by a populating algorithm
	 *
	 * @param accommodationOffering AccommodationOfferingModel to which the stockLevel has to be updated.
	 * @param accommodations        list of accommodations for the selected offering
	 * @param date                  the date stock level will refer to
	 * @param maxAvailability       the max availability that can be set in simulation mode
	 * @param availabilityMap       a map linking accommodations codes and their actual availability (when provided)
	 * @param stockLevelsToSave
	 */
	protected void setStockLevelForAccommodation(final AccommodationOfferingModel accommodationOffering,
			final List<AccommodationModel> accommodations, final LocalDateTime date, final Integer maxAvailability,
			final Map<String, Integer> availabilityMap, final List<StockLevelModel> stockLevelsToSave, final boolean SIMULATION_MODE)
	{

		for (final AccommodationModel accommodation : accommodations)
		{
			updateStockLevelForAccommodation(accommodationOffering, accommodation, SIMULATION_MODE, maxAvailability,
					availabilityMap,
					date, stockLevelsToSave);
		}
	}

	private void updateStockLevelForAccommodation(final AccommodationOfferingModel accommodationOffering,
			final AccommodationModel accommodation, final boolean SIMULATION_MODE, final Integer maxAvailability,
			final Map<String, Integer> availabilityMap, final LocalDateTime date, final List<StockLevelModel> stockLevelsToSave)
	{
		final Set<StockLevelModel> stockLevels = accommodationOffering.getStockLevels();
		final Date dateToSet = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
		final boolean isStockCodePresent =
				CollectionUtils.isNotEmpty(stockLevels) && stockLevels.stream().anyMatch(stockLevel -> {
					if (stockLevel.getProductCode().equals(accommodation.getCode())
							&& TravelDateUtils.isSameDate(stockLevel.getDate(), dateToSet))
					{
						stockLevel.setAvailable(
								SIMULATION_MODE ? maxAvailability : getAvailabilityFromMap(availabilityMap, accommodation.getCode()));
						if (SIMULATION_MODE)
						{
							stockLevel.setReserved(maxAvailability - setupSimulatedAvailability(date, maxAvailability));
						}
						stockLevelsToSave.add(stockLevel);
						return true;
					}
					return false;
				});
		if (!isStockCodePresent)
		{
			updateIfStockCodeNotPresent(accommodationOffering, accommodation, SIMULATION_MODE, maxAvailability, availabilityMap,
					date, dateToSet, stockLevelsToSave);
		}
	}

	private void updateIfStockCodeNotPresent(final AccommodationOfferingModel accommodationOffering,
			final AccommodationModel accommodation, final boolean SIMULATION_MODE, final Integer maxAvailability,
			final Map<String, Integer> availabilityMap, final LocalDateTime date, final Date dateToSet,
			final List<StockLevelModel> stockLevelsToSave)
	{
		final StockLevelModel stockLevelModel = getModelService().create(StockLevelModel.class);
		stockLevelModel.setWarehouse(accommodationOffering);
		stockLevelModel.setProductCode(accommodation.getCode());
		stockLevelModel
				.setAvailable(
						SIMULATION_MODE ? maxAvailability : getAvailabilityFromMap(availabilityMap, accommodation.getCode()));
		if (SIMULATION_MODE)
		{
			stockLevelModel.setReserved(maxAvailability - setupSimulatedAvailability(date, maxAvailability));
		}
		stockLevelModel.setStockLevelType(StockLevelType.STANDARD);
		stockLevelModel.setDate(dateToSet);
		stockLevelsToSave.add(stockLevelModel);
	}

	@Override
	protected int setupSimulatedAvailability(final LocalDateTime date, final Integer maxAvailability)
	{
		final Random random = new Random();
		final int correctedMean = maxAvailability / 2;
		final int baseAvailability = (int) (random.nextGaussian() * correctedMean) + correctedMean;
		final int dateCorrection = (int) (ChronoUnit.DAYS.between(date, LocalDateTime.now())
				* (correctedMean / (Objects.nonNull(getScheduledDays()) ? getScheduledDays() : 365)));
		return normalizeValue(baseAvailability + dateCorrection, maxAvailability);
	}


	@Override
	protected int normalizeValue(final int value, final Integer maxAvailability)
	{
		if (!new IntRange(0, maxAvailability.intValue()).containsInteger(value))
		{
			if (value < 0)
			{
				return 0;
			}
			else
			{
				return maxAvailability;
			}
		}
		return value;
	}



	@Override
	protected int getAvailabilityFromMap(final Map<String, Integer> availabilityMap, final String accommodation)
	{
		return Objects.nonNull(availabilityMap.get(accommodation)) ? availabilityMap.get(accommodation) : 0;
	}


	@Override
	protected List<String> getAccommodations(final String line)
	{
		return Arrays.asList(line.split(SEPARATOR)[1].split(LIST_SEPARATOR));
	}

	@Override
	protected String getAccommodationOffering(final String line)
	{
		return line.split(SEPARATOR)[0];
	}


	@Override
	protected Integer getAvailability(final String line)
	{
		return Integer.parseInt(line.split(SEPARATOR)[1]);
	}

	@Override
	protected String getAccommodation(final String line)
	{
		return line.split(SEPARATOR)[0];
	}

	/**
	 * This method creates a stock level entry for each room rate product belonging to a rate plan associated with any
	 * accommodations that belongs to the accommodation offering, only if SET_FORCE_IN_STOCK is set to true. In that case
	 * the stock level status will be set accordingly.
	 *
	 * @param accommodationOffering the accommodation offering where stock will be created
	 * @param accommodations        the list of products a stock level will be created for
	 * @param stockLevelsToSave
	 */
	@Override
	protected void setStockLevelForRoomRates(final AccommodationOfferingModel accommodationOffering,
			final List<AccommodationModel> accommodations, final List<StockLevelModel> stockLevelsToSave)
	{
		if (Objects.nonNull(isSetForceInStock()) ? isSetForceInStock() : Boolean.TRUE)
		{
			final List<String> roomRateCodes = accommodations.stream()
					.flatMap(accommodation -> accommodation.getRatePlan().stream())
					.flatMap(plan -> plan.getProducts().stream()).map(ProductModel::getCode).collect(Collectors.toList());

			roomRateCodes.stream().distinct().forEach(code -> {
				final StockLevelModel stockLevelModel = getModelService().create(StockLevelModel.class);
				stockLevelModel.setWarehouse(accommodationOffering);
				stockLevelModel.setProductCode(code);
				stockLevelModel.setAvailable(50);
				stockLevelModel.setStockLevelType(StockLevelType.STANDARD);
				stockLevelModel.setInStockStatus(InStockStatus.FORCEINSTOCK);
				stockLevelsToSave.add(stockLevelModel);
			});
		}

	}

	/**
	 * @param accommodationOffering
	 * @param entry
	 * @param dateToSet
	 * @return the new stockLevelModel
	 */

	private StockLevelModel createStockLevel(final AccommodationOfferingModel accommodationOffering,
			final Entry<String, Integer> entry, final Date dateToSet)
	{
		final StockLevelModel stockLevelModel = getModelService().create(StockLevelModel.class);
		stockLevelModel.setWarehouse(accommodationOffering);
		stockLevelModel.setProductCode(entry.getKey());
		stockLevelModel.setStockLevelType(StockLevelType.STANDARD);
		stockLevelModel.setAvailable(entry.getValue());
		stockLevelModel.setDate(dateToSet);
		return stockLevelModel;
	}

	/**
	 * @return the modelService
	 */
	@Override
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the accommodationOfferingService
	 */
	@Override
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	/**
	 * @param accommodationOfferingService the accommodationOfferingService to set
	 */
	@Override
	@Required
	public void setAccommodationOfferingService(final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	/**
	 * @return the baseExtDir
	 */
	@Override
	protected String getBaseExtDir()
	{
		return baseExtDir;
	}

	/**
	 * @param baseExtDir the baseExtDir to set
	 */
	@Override
	@Resource
	public void setBaseExtDir(final String baseExtDir)
	{
		this.baseExtDir = baseExtDir;
	}

	/**
	 * @return the productService
	 */
	@Override
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService the productService to set
	 */
	@Override
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @return the catalogVersionService
	 */
	@Override
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Override
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the scheduledDays
	 */
	@Override
	protected Long getScheduledDays()
	{
		return scheduledDays;
	}

	/**
	 * @param scheduledDays the scheduledDays to set
	 */
	@Override
	@Required
	public void setScheduledDays(final Long scheduledDays)
	{
		this.scheduledDays = scheduledDays;
	}

	/**
	 * @return the setForceInStock
	 */
	@Override
	protected Boolean isSetForceInStock()
	{
		return setForceInStock;
	}

	/**
	 * @param setForceInStock the setForceInStock to set
	 */
	@Override
	@Required
	public void setSetForceInStock(final Boolean setForceInStock)
	{
		this.setForceInStock = setForceInStock;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
