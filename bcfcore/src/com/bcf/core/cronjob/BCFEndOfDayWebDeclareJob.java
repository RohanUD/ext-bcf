/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.cronjob;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import org.apache.log4j.Logger;
import com.bcf.core.model.cronjob.financial.BCFEndOfDayDeclareCronJobModel;
import com.bcf.core.service.BcfEndOfDayReportService;
import com.bcf.integrations.core.exception.IntegrationException;


public class BCFEndOfDayWebDeclareJob extends AbstractJobPerformable<BCFEndOfDayDeclareCronJobModel>
{
	private BcfEndOfDayReportService bcfEndOfDayReportService;

	private static final Logger LOG = Logger.getLogger(BCFEndOfDayWebDeclareJob.class);

	@Override
	public PerformResult perform(final BCFEndOfDayDeclareCronJobModel cronJobModel)
	{
		try
		{
			Date endofdayDeclareStartTime = cronJobModel.getSessionStartDate();
			Date endofdayDeclareEndTime = cronJobModel.getSessionEndDate();

			if (Objects.isNull(endofdayDeclareStartTime))
			{
				endofdayDeclareStartTime = getPreviousDate();
			}
			if (Objects.isNull(endofdayDeclareEndTime))
			{
				endofdayDeclareEndTime = new Date();
			}

			CurrencyModel currencyModel = cronJobModel.getSessionCurrency();

			getBcfEndOfDayReportService()
					.getEndOfDayReportDataForWeb(endofdayDeclareStartTime, endofdayDeclareEndTime, currencyModel.getDigits());
			LOG.debug("Succesfully executed BCFEndOfDayWebDeclareJob");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final IntegrationException integrationException)
		{
			LOG.error("Integration error generation end of day report generating Web Declare " + integrationException.getMessage());
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catch (Exception e)
		{
			LOG.error("Error while generating Web Declare ", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

	private Date getPreviousDate()
	{
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	public BcfEndOfDayReportService getBcfEndOfDayReportService()
	{
		return bcfEndOfDayReportService;
	}

	public void setBcfEndOfDayReportService(final BcfEndOfDayReportService bcfEndOfDayReportService)
	{
		this.bcfEndOfDayReportService = bcfEndOfDayReportService;
	}
}
