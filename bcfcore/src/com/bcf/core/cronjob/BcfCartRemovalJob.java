/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.acceleratorservices.cronjob.CartRemovalJob;
import de.hybris.platform.acceleratorservices.model.CartRemovalCronJobModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.quote.service.BcfQuoteService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;


public class BcfCartRemovalJob extends CartRemovalJob
{
	private static final Logger LOG = Logger.getLogger(BcfCartRemovalJob.class);

	private BCFTravelCartService bcfTravelCartService;
	private BcfQuoteService quoteService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	@Override
	public PerformResult perform(final CartRemovalCronJobModel job)
	{
		try
		{
			for (final BaseSiteModel site : job.getSites())
			{
				for (final CartModel oldCart : getQuoteService().getQuoteListBySiteAndQuote(site, false))
				{
					bcfTravelCommerceStockService.releaseStocks(oldCart);
					getBcfTravelCartService().removeCart(oldCart);
				}
			}
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during cart cleanup", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfQuoteService getQuoteService()
	{
		return quoteService;
	}

	@Required
	public void setQuoteService(final BcfQuoteService quoteService)
	{
		this.quoteService = quoteService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
