/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.ContainerCreationStatus;
import com.bcf.core.media.conversiongroup.service.ConversionGroupService;
import com.bcf.core.media.service.BcfMediaService;
import com.bcf.core.util.StreamUtil;


public class MediaContainerCreationJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(MediaContainerCreationJob.class);
	private static final String DEFAULT_MEDIA_CONVERSION_CRONJOB = "default.media.conversion.cronjob";

	private BcfMediaService mediaService;
	private ConversionGroupService conversionGroupService;
	private CronJobService cronjobService;
	private ConfigurationService configurationService;
	private ModelService modelService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.info("Start performing MediaContainerCreationJob...");
		try
		{
			final List<MediaModel> mediaModelsToSave = updateContainer(getMediaService().findMediaItemsForFormatConversion());
			if (CollectionUtils.isNotEmpty(mediaModelsToSave))
			{
				modelService.saveAll(mediaModelsToSave);
				final CronJobModel bcfMediaConversionCronJob = cronjobService
						.getCronJob(getConfigurationService().getConfiguration().getString(DEFAULT_MEDIA_CONVERSION_CRONJOB));
				cronjobService.performCronJob(bcfMediaConversionCronJob, true);
			}
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Job failed with error", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

	private List<MediaModel> updateContainer(final List<MediaModel> medias)
	{
		return StreamUtil.safeStream(medias)
				.filter(this::hasValidConversionGroup)
				.map(this::attachMediaContainer)
				.collect(Collectors.toList());
	}

	private boolean hasValidConversionGroup(final MediaModel mediaModel)
	{
		return Objects.nonNull(getConversionGroup(mediaModel));
	}

	private MediaModel attachMediaContainer(final MediaModel mediaModel)
	{
		final MediaContainerModel mediaContainer = modelService.create(MediaContainerModel.class);
		mediaContainer.setQualifier(mediaModel.getCode() + "-container_" + System.currentTimeMillis());
		mediaContainer.setCatalogVersion(mediaModel.getCatalogVersion());
		mediaContainer.setConversionGroup(getConversionGroup(mediaModel));
		mediaModel.setContainerCreationStatus(ContainerCreationStatus.SUCCESS);
		mediaModel.setMediaContainer(mediaContainer);
		LOG.debug("Created MediaContainer for media with code " + mediaModel.getCode());
		return mediaModel;
	}

	private ConversionGroupModel getConversionGroup(final MediaModel mediaModel)
	{
		return Optional.ofNullable(mediaModel.getConversionGroup())
				.map(getConversionGroupService()::getConversionGroupByCode)
				.orElse(null);
	}

	protected BcfMediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final BcfMediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	protected ConversionGroupService getConversionGroupService()
	{
		return conversionGroupService;
	}

	@Required
	public void setConversionGroupService(final ConversionGroupService conversionGroupService)
	{
		this.conversionGroupService = conversionGroupService;
	}

	protected CronJobService getCronjobService()
	{
		return cronjobService;
	}

	@Required
	public void setCronjobService(final CronJobService cronjobService)
	{
		this.cronjobService = cronjobService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
