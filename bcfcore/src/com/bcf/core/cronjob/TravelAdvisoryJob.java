/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.services.traveladvisory.impl.TravelAdvisoryService;
import com.bcf.core.util.StreamUtil;


public class TravelAdvisoryJob extends AbstractJobPerformable<CronJobModel>
{
	private TravelAdvisoryService travelAdvisoryService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		upgradeTravelAdvisoriesToLive();
		downgradeTravelAdvisoriesToExpired();
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void upgradeTravelAdvisoriesToLive()
	{
		final List<TravelAdvisoryModel> travelAdvisoryListToPublish = getTravelAdvisoriesWithUpdatedStatus(
				getTravelAdvisoryService().findTravelAdvisoriesToPublish(), TravelAdvisoryStatus.LIVE);
		saveTravelAdvisories(travelAdvisoryListToPublish);
	}

	private void downgradeTravelAdvisoriesToExpired()
	{
		final List<TravelAdvisoryModel> travelAdvisoryListToExpire = getTravelAdvisoriesWithUpdatedStatus(
				getTravelAdvisoryService().findExpiredTravelAdvisories(), TravelAdvisoryStatus.EXPIRED);
		saveTravelAdvisories(travelAdvisoryListToExpire);
	}

	private void saveTravelAdvisories(final List<TravelAdvisoryModel> travelAdvisoryListToExpire)
	{
		if (CollectionUtils.isNotEmpty(travelAdvisoryListToExpire))
		{
			modelService.saveAll(travelAdvisoryListToExpire);
		}
	}

	private List<TravelAdvisoryModel> getTravelAdvisoriesWithUpdatedStatus(
			final List<TravelAdvisoryModel> travelAdvisoriesToPublish, final TravelAdvisoryStatus travelAdvisoryStatus)
	{
		return StreamUtil
				.safeStream(travelAdvisoriesToPublish)
				.map(travelAdvisoryModel -> getTravelAdvisoryWithUpdatedStatus(travelAdvisoryModel, travelAdvisoryStatus))
				.collect(Collectors.toList());
	}

	private TravelAdvisoryModel getTravelAdvisoryWithUpdatedStatus(final TravelAdvisoryModel travelAdvisoryModel,
			final TravelAdvisoryStatus travelAdvisoryStatus)
	{
		travelAdvisoryModel.setStatus(travelAdvisoryStatus);
		return travelAdvisoryModel;
	}

	public TravelAdvisoryService getTravelAdvisoryService()
	{
		return travelAdvisoryService;
	}

	@Required
	public void setTravelAdvisoryService(final TravelAdvisoryService travelAdvisoryService)
	{
		this.travelAdvisoryService = travelAdvisoryService;
	}
}
