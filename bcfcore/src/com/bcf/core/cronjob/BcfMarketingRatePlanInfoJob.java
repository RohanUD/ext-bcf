/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.cronjob.MarketingRatePlanInfoJob;
import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;


/**
 * Cronjob to create and populate the MarketingRatePlanInfo and the RatePlanConfig. This cronjob will correctly
 * populated the items only if the GuestOccupancy are set just for the Accommodation. In case that different ratePlans
 * are used for different GuestOccupancies, the logic should be extended and customised.
 */
public class BcfMarketingRatePlanInfoJob extends MarketingRatePlanInfoJob
{
	private static final Logger LOG = Logger.getLogger(BcfMarketingRatePlanInfoJob.class);
	private static final String CHILD = "child";
	private static final String ADULT = "adult";
	private static final int DEFAULT_RATE_PLAN_CONFIG_QUANTITY = 1;

	private BcfAccommodationService bcfAccommodationService;

	@Override
	protected void createMarketingRatePlanInfoList(final List<AccommodationOfferingModel> accommodationOfferings,
			final String ratePlanCode)
	{
		final List<MarketingRatePlanInfoModel> marketingRatePlanInfos = new ArrayList<>();
		for (final AccommodationOfferingModel accommodationOffering : accommodationOfferings)
		{
			final List<AccommodationModel> accommodations = accommodationOffering.getAccommodations().stream()
					.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
					.collect(Collectors.toList());

			final List<GuestOccupancyModel> guestOccupancies = accommodations.stream()
					.flatMap(accommodation -> accommodation.getGuestOccupancies().stream()).distinct().collect(Collectors.toList());
			final List<GuestOccupancyModel> adultGuestOccupancies = guestOccupancies.stream()
					.filter(guestOccupancy -> StringUtils.equalsIgnoreCase(guestOccupancy.getPassengerType().getCode(), ADULT))
					.collect(Collectors.toList());

			for (final GuestOccupancyModel adultGuestOccupancyModel : adultGuestOccupancies)
			{
				final MarketingRatePlanInfoModel marketingRatePlanInfo = createNewMarketingRatePlanInfo(accommodationOffering,
						accommodations, adultGuestOccupancyModel, ratePlanCode);

				if (marketingRatePlanInfo != null)
				{
					marketingRatePlanInfos.add(marketingRatePlanInfo);
				}
			}
		}
		getModelService().saveAll(marketingRatePlanInfos);
	}

	@Override
	protected MarketingRatePlanInfoModel createNewMarketingRatePlanInfo(final AccommodationOfferingModel accommodationOffering,
			final List<AccommodationModel> accommodations, final GuestOccupancyModel adultGuestOccupancyModel,
			final String ratePlanCode)
	{
		final List<AccommodationModel> filteredAccommodations = accommodations.stream()
				.filter(accommodation -> accommodation.getGuestOccupancies().contains(adultGuestOccupancyModel))
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(filteredAccommodations))
		{
			return null;
		}

		final String code = adultGuestOccupancyModel.getCode() + "_" + accommodationOffering.getCode();
		MarketingRatePlanInfoModel marketingRatePlanInfo = null;
		try
		{
			marketingRatePlanInfo = getMarketingRatePlanInfoService().getMarketingRatePlanInfoForCode(code);
			if (marketingRatePlanInfo == null)
			{
				marketingRatePlanInfo = getModelService().create(MarketingRatePlanInfoModel.class);
			}
			marketingRatePlanInfo.setCode(code);
			marketingRatePlanInfo.setAccommodationOffering(accommodationOffering);
			marketingRatePlanInfo.setNumberOfAdults(adultGuestOccupancyModel.getQuantityMax());

			final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion("bcfProductCatalog", "Staged");
			marketingRatePlanInfo.setCatalogVersion(catalogVersion);

			final List<RatePlanConfigModel> ratePlanConfigs = createRatePlanConfigs(filteredAccommodations, ratePlanCode,
					catalogVersion);
			marketingRatePlanInfo.setRatePlanConfig(ratePlanConfigs);
			final Optional<GuestOccupancyModel> childGuestOccupancyOptional = filteredAccommodations.stream()
					.flatMap(accommodationModel -> accommodationModel.getGuestOccupancies().stream())
					.filter(occupancy -> StringUtils.equalsIgnoreCase(occupancy.getPassengerType().getCode(), CHILD))
					.findFirst();
			if (childGuestOccupancyOptional.isPresent())
			{
				marketingRatePlanInfo.setExtraGuests(Arrays.asList(childGuestOccupancyOptional.get()));
			}
		}
		catch (final NoSuchElementException e)
		{
			LOG.debug("No marketingRatePlanInfo found for given code", e);
		}

		return marketingRatePlanInfo;
	}

	/**
	 * Creates the rate plan configs.
	 *
	 * @param accommodations the list of accommodation model
	 * @param ratePlanCode   the rate plan code
	 * @param catalogVersion the catalog version
	 * @return the list
	 */
	protected List<RatePlanConfigModel> createRatePlanConfigs(final List<AccommodationModel> accommodations,
			final String ratePlanCode, final CatalogVersionModel catalogVersion)
	{
		final List<RatePlanConfigModel> ratePlanConfigs = new ArrayList<>();
		for (final AccommodationModel accommodationModel : accommodations)
		{
			final Collection<RatePlanModel> ratePlans = accommodationModel.getRatePlan();
			if (CollectionUtils.isEmpty(ratePlans))
			{
				LOG.debug("No ratePlan found for given model");
				return Collections.emptyList();
			}
			List<RatePlanModel> selectedRatePlans = ratePlans.stream()
					.filter(ratePlan -> StringUtils.containsIgnoreCase(ratePlan.getCode(), ratePlanCode)).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(selectedRatePlans))
			{
				selectedRatePlans = new ArrayList<>(ratePlans);
			}
			for (final RatePlanModel ratePlanModel : selectedRatePlans)
			{
				final String code = accommodationModel.getCode() + "_" + ratePlanModel.getCode();
				RatePlanConfigModel ratePlanConfig = null;
				try
				{
					ratePlanConfig = getRatePlanConfigService().getRatePlanConfigForCode(code);
				}
				catch (final NoSuchElementException e)
				{
					LOG.debug("No ratePlanConfig found for given code", e);
				}
				updateRatePlanConfig(ratePlanConfig, code, accommodationModel, ratePlanModel, catalogVersion, ratePlanConfigs);
			}
		}
		return ratePlanConfigs;
	}

	private void updateRatePlanConfig(RatePlanConfigModel ratePlanConfig, final String code,
			final AccommodationModel accommodationModel, final RatePlanModel ratePlanModel, final CatalogVersionModel catalogVersion,
			List<RatePlanConfigModel> ratePlanConfigs)
	{
		if (Objects.isNull(ratePlanConfig))
		{
			ratePlanConfig = getModelService().create(RatePlanConfigModel.class);
		}
		if (ratePlanConfig != null)
		{
			ratePlanConfig.setCode(code);
			ratePlanConfig.setAccommodation(accommodationModel);
			ratePlanConfig.setRatePlan(ratePlanModel);
			ratePlanConfig.setQuantity(DEFAULT_RATE_PLAN_CONFIG_QUANTITY);
			ratePlanConfig.setCatalogVersion(catalogVersion);
			ratePlanConfigs.add(ratePlanConfig);
		}
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}
}
