/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import reactor.util.CollectionUtils;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelStockService;


public class UpdateStockBlockReleaseJob
		extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(UpdateStockBlockReleaseJob.class);
	private BcfTravelStockService bcfTravelStockService;


	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.info("Start Performing Update Stock Block Release Job...");
		final List<StockLevelModel> stockLevelDataModels = getBcfTravelStockService().getStocks(new Date());
		if (CollectionUtils.isEmpty(stockLevelDataModels))
		{
			return completeJob();
		}

		stockLevelDataModels.forEach(stockLevelModel -> {
			stockLevelModel.setAvailable(0);
			stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
		});
		getModelService().saveAll(stockLevelDataModels);
		return completeJob();

	}

	protected PerformResult completeJob()
	{
		LOG.info("Update Stock Block Release Job Completed.");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	protected BcfTravelStockService getBcfTravelStockService()
	{
		return bcfTravelStockService;
	}

	@Required
	public void setBcfTravelStockService(final BcfTravelStockService bcfTravelStockService)
	{
		this.bcfTravelStockService = bcfTravelStockService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}
}
