/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import java.util.List;
import org.apache.log4j.Logger;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.services.releasecenter.ReleaseCenterService;
import com.bcf.core.services.servicenotice.ServiceNoticesService;


public class BcfReleaseCenterJob extends AbstractJobPerformable<CronJobModel>
{

	private static final Logger LOG = Logger.getLogger(BcfReleaseCenterJob.class);

	private ServiceNoticesService serviceNoticesService;
	private ReleaseCenterService releaseCenterService;

	@Override
	public PerformResult perform(CronJobModel cronJobModel)
	{
		try
		{
			List<ServiceNoticeModel> nonPublishedNotices = serviceNoticesService.getNotPublishedServiceNotices();
			Boolean allPublished = releaseCenterService.publishAll(nonPublishedNotices);

			if (allPublished)
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			else
			{
				LOG.error("There are service notices that are not published successfully");
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while publishing service notices to Notification Center.", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

	public void setServiceNoticesService(ServiceNoticesService serviceNoticesService)
	{
		this.serviceNoticesService = serviceNoticesService;
	}

	public void setReleaseCenterService(final ReleaseCenterService releaseCenterService)
	{
		this.releaseCenterService = releaseCenterService;
	}
}
