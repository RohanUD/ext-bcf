/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.List;
import org.apache.log4j.Logger;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.services.releasecenter.ReleaseCenterService;
import com.bcf.core.services.servicenotice.ServiceNoticesService;
import com.bcf.core.workflow.BcfWorkflowService;


public class BcfServiceNoticeCleanupJob extends AbstractJobPerformable<CronJobModel>
{

	private static final Logger LOG = Logger.getLogger(BcfServiceNoticeCleanupJob.class);

	private ServiceNoticesService serviceNoticesService;
	private ReleaseCenterService releaseCenterService;
	private BcfWorkflowService bcfWorkflowService;

	@Override
	public PerformResult perform(CronJobModel cronJobModel)
	{
		try
		{
			List<ServiceNoticeModel> expiredServiceNotices = serviceNoticesService.getExpiredServiceNotices();
			Boolean allUpdated = releaseCenterService.updateStatus(expiredServiceNotices, ServiceNoticeStatus.EXPIRED);

			List<WorkflowModel> expiredWorkflows = bcfWorkflowService.getWorkflowsWithExpiredAttachments();
			bcfWorkflowService.terminateWorkflow(expiredWorkflows);

			if (allUpdated)
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			else
			{
				LOG.error("There are service notices whose status are not updated successfully");
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while updating status of service notices to EXPIRED", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

	public void setServiceNoticesService(ServiceNoticesService serviceNoticesService)
	{
		this.serviceNoticesService = serviceNoticesService;
	}

	public void setReleaseCenterService(final ReleaseCenterService releaseCenterService)
	{
		this.releaseCenterService = releaseCenterService;
	}

	public void setBcfWorkflowService(final BcfWorkflowService bcfWorkflowService)
	{
		this.bcfWorkflowService = bcfWorkflowService;
	}
}
