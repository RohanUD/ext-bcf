/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 10/7/19 1:15 PM
 */

package com.bcf.core.cronjob;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import com.bcf.core.service.BcfOptionBookingService;
import com.bcf.core.util.StreamUtil;


public class PurgeOptionBookingJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(OptionBookingReleaseInventoryJob.class);

	private BcfOptionBookingService bcfOptionBookingService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.info("Start PurgeOptionBookingJob...");

		try
		{
			List<CartModel> expiredOptionBookings = getBcfOptionBookingService().getExpiredOptionBookingsForPurging();

			String purgedOptionBookingCodes = StreamUtil.safeStream(expiredOptionBookings).map(AbstractOrderModel::getCode)
					.collect(Collectors.joining(","));

			LOG.info(String.format("Purging following option bookings: %s", purgedOptionBookingCodes));

			bcfOptionBookingService.purgeOptionBookings(expiredOptionBookings);

			LOG.info("PurgeOptionBookingJob Completed.");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (Exception e)
		{
			LOG.error("Error while PurgeOptionBookingJob job.", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

	public BcfOptionBookingService getBcfOptionBookingService()
	{
		return bcfOptionBookingService;
	}

	public void setBcfOptionBookingService(final BcfOptionBookingService bcfOptionBookingService)
	{
		this.bcfOptionBookingService = bcfOptionBookingService;
	}
}
