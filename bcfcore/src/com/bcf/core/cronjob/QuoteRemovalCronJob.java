/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.quote.service.BcfQuoteService;
import com.bcf.core.service.BCFTravelCartService;


public class QuoteRemovalCronJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(QuoteRemovalCronJob.class);

	private BcfQuoteService quoteService;
	private BaseSiteService baseSiteService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final String quoteExpiryStr = getBcfConfigurablePropertiesService().getBcfPropertyValue("quoteExpiry");
		int quoteExpiry = 30;
		if (StringUtils.isNotEmpty(quoteExpiryStr) && NumberUtils.isNumber(quoteExpiryStr))
		{
			quoteExpiry = Integer.parseInt(quoteExpiryStr);
		}
		final Date modifiedTimeThreshold = TravelDateUtils.addDays(new Date(), quoteExpiry * -1);
		final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		try
		{
			final Date modifiedTimeThresholdDate = dateFormat.parse(dateFormat.format(modifiedTimeThreshold));
			for (final CartModel cart : getQuoteService().getQuoteListByQuoteAndThreshold(true, modifiedTimeThresholdDate))
			{
				LOG.info("Remove cart");
				getBcfTravelCartService().removeCart(cart);
			}
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during cart cleanup", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}

	protected BcfQuoteService getQuoteService()
	{
		return quoteService;
	}

	@Required
	public void setQuoteService(final BcfQuoteService quoteService)
	{
		this.quoteService = quoteService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
