/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.cronjob;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.cronjob.ActivityProductJob.UpdateStockLevelsToActivityProductCronJobModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.ActivityProductService;
import com.bcf.core.service.BcfTravelCommerceStockService;


public class BcfUpdateStockLevelsToActivityProductJob
		extends AbstractJobPerformable<UpdateStockLevelsToActivityProductCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(BcfUpdateStockLevelsToActivityProductJob.class);

	private static final String DEFAULT = "default";

	private Long scheduledDays;
	private ActivityProductService activityProductService;
	private CatalogVersionService catalogVersionService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private WarehouseService warehouseService;

	@Override
	public PerformResult perform(
			final UpdateStockLevelsToActivityProductCronJobModel updateStockLevelsToActivityProductCronJobModel)
	{
		LOG.info("Start performing UpdateStockLevelsToActiviytyProductJob...");

		final List<ActivityProductModel> activityProducts = getActivityProductService().findAllActivityProducts();
		final WarehouseModel defaultWarehouse = getWarehouseService().getWarehouseForCode(DEFAULT);

		final List<StockLevelModel> stockLevelsToSave = new ArrayList<>();
		for(final ActivityProductModel activityProduct:activityProducts)
		{

			final Date startDate = new Date();
			final Date endingDate = DateUtils.addDays(startDate,Objects.nonNull(getScheduledDays()) ? getScheduledDays().intValue() : 365);
			for (Date date = startDate; !TravelDateUtils.isSameDate(date, endingDate); date = DateUtils.addDays(date,
					1))
			{
				final StockLevelModel stockLevelModel = getModelService().create(StockLevelModel.class);
				stockLevelModel.setWarehouse(defaultWarehouse);
				stockLevelModel.setProductCode(activityProduct.getCode());
				stockLevelModel.setAvailable(updateStockLevelsToActivityProductCronJobModel.getMaxAvailability());
				stockLevelModel.setProduct(activityProduct);
				stockLevelModel.setDate(date);
				stockLevelsToSave.add(stockLevelModel);
			}
		}
		getBcfTravelCommerceStockService().batchSavingOfStockLevels(stockLevelsToSave);

		LOG.info("UpdateStockLevelsToActivityProductJob completed.");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	protected Long getScheduledDays()
	{
		return scheduledDays;
	}

	@Required
	public void setScheduledDays(final Long scheduledDays)
	{
		this.scheduledDays = scheduledDays;
	}

	protected ActivityProductService getActivityProductService()
	{
		return activityProductService;
	}

	@Required
	public void setActivityProductService(final ActivityProductService activityProductService)
	{
		this.activityProductService = activityProductService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}
}
