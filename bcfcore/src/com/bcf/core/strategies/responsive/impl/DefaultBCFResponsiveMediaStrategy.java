/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.strategies.responsive.impl;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;


public class DefaultBCFResponsiveMediaStrategy implements BCFResponsiveMediaStrategy
{
	private static final String DOUBLE_QUOTE = "\"";
	private static final String DOUBLE_QUOTE_COLON = "\":\"";
	private static final String COMMA = ",";
	private static final String CURLY_START = "{";
	private static final String CURLY_END = "}";
	private static final String SINGLE_MEDIA_WIDTH = "1";

	@Resource
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Override
	public String getResponsiveJson(final MediaModel mediaModel)
	{
		return Optional.ofNullable(mediaModel).map(this::createResponsiveJson).orElse(StringUtils.EMPTY);
	}

	@Override
	public String getResponsiveJson(final MediaContainerModel mediaContainerModel)
	{
		return Optional.ofNullable(mediaContainerModel).map(this::createResponsiveJson).orElse(StringUtils.EMPTY);
	}

	@Override
	public String getDataMediaForFormat(final MediaModel mediaModel, final String mediaFormatCode)
	{
		return Optional.ofNullable(mediaModel).map(media -> createJsonForMediaFormat(media, mediaFormatCode))
				.orElse(StringUtils.EMPTY);
	}

	@Override
	public String getDataMediaForFormat(final MediaContainerModel mediaContainerModel, final String mediaFormatCode)
	{
		return Optional.ofNullable(mediaContainerModel).map(container -> createJsonForMediaFormat(container, mediaFormatCode))
				.orElse(StringUtils.EMPTY);
	}

	private String createJsonForMediaFormat(final MediaModel mediaModel, final String mediaFormatCode)
	{
		return Optional.ofNullable(mediaModel.getMediaContainer())
				.map(mediaContainer -> createJsonForMediaFormat(mediaContainer, mediaFormatCode))
				.orElse(createSingleMediaJson(mediaModel));
	}

	private String createJsonForMediaFormat(final MediaContainerModel mediaContainerModel, //NOSONAR
			final String mediaFormatCode)
	{
		return StreamUtil.safeStream(mediaContainerModel.getMedias())
				.filter(getMediaFormatCheckPredicate(mediaFormatCode))
				.findAny()
				.map(this::createSingleMediaJson)
				.orElse(createPrimaryImageJson(mediaContainerModel));
	}

	private String createPrimaryImageJson(final MediaContainerModel mediaContainerModel)
	{
		if (Objects.nonNull(mediaContainerModel) && CollectionUtils.isNotEmpty(mediaContainerModel.getMedias()))
		{
			return createSingleMediaJson(mediaContainerModel.getMedias().iterator().next());
		}
		return StringUtils.EMPTY;
	}

	private Predicate<MediaModel> getMediaFormatCheckPredicate(final String mediaFormatCode)
	{
		return media -> Objects.nonNull(media.getMediaFormat()) && mediaFormatCode.equals(media.getMediaFormat().getQualifier());
	}

	private String createResponsiveJson(final MediaModel mediaModel)
	{
		return Optional.ofNullable(mediaModel.getMediaContainer()).map(this::getResponsiveJson)
				.orElse(createSingleMediaJson(mediaModel));
	}

	private String createSingleMediaJson(final MediaModel mediaModel)
	{
		return new StringJoiner(DOUBLE_QUOTE_COLON, CURLY_START + DOUBLE_QUOTE, DOUBLE_QUOTE + CURLY_END)
				.add(SINGLE_MEDIA_WIDTH)
				.add(mediaModel.getURL())
				.toString();
	}

	private String createResponsiveJson(final MediaContainerModel mediaContainerModel)
	{
		return StreamUtil.safeStream(getResponsiveMediaFacade().getImagesFromMediaContainer(mediaContainerModel))
				.map(this::createResponsiveMediaItem)
				.collect(Collectors.joining(COMMA, CURLY_START, CURLY_END));
	}

	private String createResponsiveMediaItem(final ImageData imageData)
	{
		return new StringJoiner(DOUBLE_QUOTE_COLON, DOUBLE_QUOTE, DOUBLE_QUOTE)
				.add(String.valueOf(imageData.getWidth() != null ? imageData.getWidth() : SINGLE_MEDIA_WIDTH))
				.add(imageData.getUrl())
				.toString();
	}

	public ResponsiveMediaFacade getResponsiveMediaFacade()
	{
		return responsiveMediaFacade;
	}

	@Required
	public void setResponsiveMediaFacade(final ResponsiveMediaFacade responsiveMediaFacade)
	{
		this.responsiveMediaFacade = responsiveMediaFacade;
	}
}
