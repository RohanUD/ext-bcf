/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.transportFacility.dao;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.travelservices.dao.TransportFacilityDao;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.List;


public interface BcfTransportFacilityDao extends TransportFacilityDao
{
	TransportFacilityModel findTransportFacilityForLocation(LocationModel location);

	List<TransportFacilityModel> findTransportFacilityForTerminalCodes(List<String> terminalCodes);

	List<TransportFacilityModel> findAllTransportFacilities();

	SearchPageData<TransportFacilityModel> getPaginatedTransportFacilities(int pageSize, int currentPage);
}
