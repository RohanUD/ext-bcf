/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.transportFacility.dao.impl;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.dao.impl.DefaultTransportFacilityDao;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.pagination.GlobalPaginationResultsBuilder;
import com.bcf.core.transportFacility.dao.BcfTransportFacilityDao;


public class DefaultBcfTransportFacilityDao extends DefaultTransportFacilityDao implements BcfTransportFacilityDao
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfTransportFacilityDao.class);

	private static final String FIND_TERMINALS_FOR_CODES = "SELECT {tf.PK} FROM {TransportFacility AS tf} WHERE {tf.CODE} IN (?terminalCodes)";
	private static final String FIND_ALL_TERMINALS = "SELECT {PK} FROM {TransportFacility} where {active}=?active ORDER BY {name}";

	public DefaultBcfTransportFacilityDao(final String typecode)
	{
		super(typecode);
	}

	private GlobalPaginationResultsBuilder globalPaginationResultsBuilder;

	@Override
	public TransportFacilityModel findTransportFacilityForLocation(final LocationModel location)
	{
		ServicesUtil.validateParameterNotNull(location, "location must not be null!");
		final Map<String, Object> params = new HashMap();
		params.put(TransportFacilityModel.LOCATION, location);
		final List<TransportFacilityModel> transportFacilityModel = this.find(params);
		return CollectionUtils.isNotEmpty(transportFacilityModel) ? transportFacilityModel.get(0) : null;
	}

	@Override
	public List<TransportFacilityModel> findTransportFacilityForTerminalCodes(final List<String> terminalCodes)
	{
		ServicesUtil.validateParameterNotNull(terminalCodes, "terminalCodes must not be null!");
		if (CollectionUtils.isEmpty(terminalCodes))
		{
			LOG.error("terminalCodes must not be empty");
			return Collections.emptyList();
		}
		final Map<String, Object> params = new HashMap();
		params.put("terminalCodes", terminalCodes);

		final SearchResult<TransportFacilityModel> searchResult = getFlexibleSearchService()
				.search(FIND_TERMINALS_FOR_CODES, params);
		if (searchResult.getResult().isEmpty())
		{
			return Collections.emptyList();
		}
		return searchResult.getResult();
	}

	@Override
	public List<TransportFacilityModel> findAllTransportFacilities()
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("active", 1);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ALL_TERMINALS, queryParams);
		final SearchResult<TransportFacilityModel> searchResult = getFlexibleSearchService().search(fsq);

		return searchResult.getResult();
	}

	@Override
	public SearchPageData<TransportFacilityModel> getPaginatedTransportFacilities(final int pageSize, final int currentPage)
	{
		return getGlobalPaginationResultsBuilder().createPaginatedResults(FIND_ALL_TERMINALS,
				Collections.singletonMap(TransportFacilityModel.ACTIVE, 1), pageSize, currentPage);
	}

	public GlobalPaginationResultsBuilder getGlobalPaginationResultsBuilder()
	{
		return globalPaginationResultsBuilder;
	}

	@Required
	public void setGlobalPaginationResultsBuilder(final GlobalPaginationResultsBuilder globalPaginationResultsBuilder)
	{
		this.globalPaginationResultsBuilder = globalPaginationResultsBuilder;
	}
}
