/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.workflow.impl;

import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.workflow.daos.impl.DefaultWorkflowDao;
import de.hybris.platform.workflow.model.WorkflowItemAttachmentModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.workflow.BcfWorkflowDao;


public class DefaultBcfWorkflowDao extends DefaultWorkflowDao implements BcfWorkflowDao
{
	private static final String QUERY_WORKFLOW_ATTACHMENTS_FOR_SERVICE_NOTICE =
			"SELECT {pk} FROM {WorkflowItemAttachment as att} where {att.item} = ?" + ServiceNoticeModel._TYPECODE;

	private static final String QUERY_WORKFLOW_ATTACHMENTS_FOR_TRAVEL_ADVISORY =
			"SELECT {pk} FROM {WorkflowItemAttachment as att} where {att.item} = ?" + TravelAdvisoryModel._TYPECODE;

	public DefaultBcfWorkflowDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<WorkflowModel> findWorkflowForServiceNotice(final ServiceNoticeModel serviceNotice)
	{
		final Map params = new HashMap();
		params.put(ServiceNoticeModel._TYPECODE, serviceNotice);
		final SearchResult<WorkflowItemAttachmentModel> res = getFlexibleSearchService()
				.search(QUERY_WORKFLOW_ATTACHMENTS_FOR_SERVICE_NOTICE, params);

		List<WorkflowModel> workflowModels = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(res.getResult()))
		{
			workflowModels = res.getResult().stream().map(att -> att.getWorkflow()).collect(Collectors.toList());
		}
		return workflowModels;
	}

	@Override
	public List<WorkflowModel> findWorkflowForTravelAdvisory(final TravelAdvisoryModel travelAdvisoryModel)
	{
		final Map params = new HashMap();
		params.put(TravelAdvisoryModel._TYPECODE, travelAdvisoryModel);
		final SearchResult<WorkflowItemAttachmentModel> res = getFlexibleSearchService()
				.search(QUERY_WORKFLOW_ATTACHMENTS_FOR_TRAVEL_ADVISORY, params);

		List<WorkflowModel> workflowModels = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(res.getResult()))
		{
			workflowModels = res.getResult().stream().map(att -> att.getWorkflow()).collect(Collectors.toList());
		}
		return workflowModels;
	}
}
