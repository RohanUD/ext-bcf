/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.workflow.impl;

import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_KEEP_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_REJECT_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_UPDATE_ACTION;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_UPDATE_REJECT_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_VALIDATE_ACTION;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_KEEP_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_REJECT_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_UPDATE_ACTION;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_UPDATE_REJECT_DECISION;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_VALIDATE_ACTION;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_WORKFLOW_TEMPLATE_CODE;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import de.hybris.platform.workflow.strategies.impl.DefaultDecideActionStrategy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.BusinessOpportunityModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.services.releasecenter.ReleaseCenterService;


public class DefaultBcfDecideActionStrategy extends DefaultDecideActionStrategy
{
	private ModelService modelService;
	private ReleaseCenterService releaseCenterService;

	@Override
	public void doAfterDecisionMade(final WorkflowActionModel action, final WorkflowDecisionModel selDec)
	{
		super.doAfterDecisionMade(action, selDec);

		if (isServiceNoticeWorkflow(action))
		{
			updateServiceNoticeStatusBasedOnDecision(action, selDec);
		}
		if (isTravelAdvisoryWorkflow(action))
		{
			updateTravelAdvisoryStatusBasedOnDecision(action, selDec);
		}
	}

	private boolean isServiceNoticeWorkflow(final WorkflowActionModel action)
	{
		return SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE.equals(action.getWorkflow().getJob().getCode());
	}

	private boolean isTravelAdvisoryWorkflow(final WorkflowActionModel action)
	{
		return TRAVEL_ADVISORY_WORKFLOW_TEMPLATE_CODE.equals(action.getWorkflow().getJob().getCode());
	}

	private void updateTravelAdvisoryStatusBasedOnDecision(final WorkflowActionModel action, final WorkflowDecisionModel selDec)
	{
		final List<ItemModel> itemsToSave = new ArrayList<>();
		if (TRAVEL_ADVISORY_VALIDATE_ACTION.equals(action.getTemplate().getCode()) && TRAVEL_ADVISORY_KEEP_DECISION
				.equals(selDec.getCode()))
		{
			action.getWorkflow().getAttachments().stream().forEach(item -> {
				final TravelAdvisoryModel travelAdvisoryModel = (TravelAdvisoryModel) item.getItem();
				travelAdvisoryModel.setStatus(TravelAdvisoryStatus.APPROVED);
				final Date currentDateTime = new Date();
				if (currentDateTime.after(travelAdvisoryModel.getPublishDateTime()) && currentDateTime
						.before(travelAdvisoryModel.getExpiryDateTime()))
				{
					travelAdvisoryModel.setStatus(TravelAdvisoryStatus.LIVE);
				}
				itemsToSave.add(item);
			});
		}
		if ((TRAVEL_ADVISORY_VALIDATE_ACTION.equals(action.getTemplate().getCode()) && TRAVEL_ADVISORY_REJECT_DECISION
				.equals(selDec.getCode()))
				|| (
				TRAVEL_ADVISORY_UPDATE_ACTION.equals(action.getTemplate().getCode()) && TRAVEL_ADVISORY_UPDATE_REJECT_DECISION
						.equals(selDec.getCode())))
		{
			action.getWorkflow().getAttachments().stream().forEach(item -> {
				((TravelAdvisoryModel) item.getItem()).setStatus(TravelAdvisoryStatus.REJECTED);
				itemsToSave.add(item);
			});
		}

		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
	}

	private void updateServiceNoticeStatusBasedOnDecision(final WorkflowActionModel action, final WorkflowDecisionModel selDec)
	{
		final List<ItemModel> itemsToSave = new ArrayList<>();
		if (SERVICE_NOTICE_VALIDATE_ACTION.equals(action.getTemplate().getCode()) && SERVICE_NOTICE_KEEP_DECISION
				.equals(selDec.getCode()))
		{
			action.getWorkflow().getAttachments().stream().forEach(item -> {
				updateStatusForServiceNotice(item.getItem(), ServiceNoticeStatus.APPROVED, itemsToSave);
				getReleaseCenterService().publishInstantlyIfInstantPublish((ServiceNoticeModel) item.getItem());
			});
		}
		if ((SERVICE_NOTICE_VALIDATE_ACTION.equals(action.getTemplate().getCode()) && SERVICE_NOTICE_REJECT_DECISION
				.equals(selDec.getCode()))
				|| (
				SERVICE_NOTICE_UPDATE_ACTION.equals(action.getTemplate().getCode()) && SERVICE_NOTICE_UPDATE_REJECT_DECISION
						.equals(selDec.getCode())))
		{
			action.getWorkflow().getAttachments().stream().forEach(item -> {
				updateStatusForServiceNotice(item.getItem(), ServiceNoticeStatus.REJECTED, itemsToSave);

			});
		}

		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
	}

	private void updateStatusForServiceNotice(final ItemModel item, final ServiceNoticeStatus status,
			final List<ItemModel> itemsToSave)
	{
		if (item instanceof ServiceModel)
		{
			((ServiceModel) item).setStatus(status);
			itemsToSave.add(item);
		}
		else if (item instanceof NewsModel)
		{
			((NewsModel) item).setStatus(status);
			itemsToSave.add(item);
		}
		else if (item instanceof BusinessOpportunityModel)
		{
			((BusinessOpportunityModel) item).setStatus(status);
			itemsToSave.add(item);
		}
	}

	@Override
	public void setModelService(final ModelService modelService)
	{
		super.setModelService(modelService);
		this.modelService = modelService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public ReleaseCenterService getReleaseCenterService()
	{
		return releaseCenterService;
	}

	@Required
	public void setReleaseCenterService(final ReleaseCenterService releaseCenterService)
	{
		this.releaseCenterService = releaseCenterService;
	}
}
