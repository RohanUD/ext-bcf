/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.workflow.impl;

import static com.bcf.core.constants.BcfCoreConstants.BACKOFFICE_WORKFLOW_ADMIN_USER;
import static com.bcf.core.constants.BcfCoreConstants.SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE;
import static com.bcf.core.constants.BcfCoreConstants.TRAVEL_ADVISORY_WORKFLOW_TEMPLATE_CODE;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowStatus;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.impl.DefaultWorkflowService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowItemAttachmentModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.workflow.BcfWorkflowDao;
import com.bcf.core.workflow.BcfWorkflowService;


public class DefaultBcfWorkflowService extends DefaultWorkflowService implements BcfWorkflowService
{

	@Resource
	private WorkflowTemplateService workflowTemplateService;

	@Resource
	private UserService userService;

	@Resource
	private WorkflowProcessingService workflowProcessingService;

	@Resource
	private ModelService modelService;

	@Resource
	private BcfWorkflowDao bcfWorkflowDao;

	@Override
	public void startWorkflowForServiceNotice(final ServiceNoticeModel serviceNotice)
	{
		startWorkflow(serviceNotice, serviceNotice.getTitle(), SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE);
	}

	@Override
	public void startWorkflowForTravelAdvisory(final TravelAdvisoryModel travelAdvisoryModel)
	{
		startWorkflow(travelAdvisoryModel, travelAdvisoryModel.getAdvisoryTitle(), TRAVEL_ADVISORY_WORKFLOW_TEMPLATE_CODE);
	}

	private void startWorkflow(final ItemModel itemModel, final String workflowName, final String workflowTemplateCode)
	{
		final WorkflowModel workflow = createWorkflow(getWorkflowTemplateForCode(workflowTemplateCode),
				userService.getUserForUID(BACKOFFICE_WORKFLOW_ADMIN_USER));
		workflow.setName(workflowName);
		final WorkflowItemAttachmentModel attachment = getModelService().create(WorkflowItemAttachmentModel.class);
		attachment.setName(workflowName + " attachment");
		attachment.setItem(itemModel);
		attachment.setWorkflow(workflow);
		attachment.setCode(workflowName + "_attachment");
		workflow.setAttachments(Collections.singletonList(attachment));
		getModelService().saveAll(workflow, attachment);
		workflow.getActions().forEach(action -> action.setAttachments(workflow.getAttachments()));
		getModelService().saveAll(workflow.getActions());

		workflowProcessingService.startWorkflow(workflow);
	}

	public WorkflowTemplateModel getWorkflowTemplateForCode(final String code)
	{
		try
		{
			return workflowTemplateService.getWorkflowTemplateForCode(code);
		}
		catch (final UnknownIdentifierException uie)
		{
			return null;
		}
	}

	/**
	 * This is restricted to Service notice workflows only at the moment.
	 * It is assumed that each workflow will contain atmost 1 attachment only
	 *
	 * @return
	 */
	@Override
	public List<WorkflowModel> getWorkflowsWithExpiredAttachments()
	{
		final EnumSet<WorkflowStatus> statuses = EnumSet.of(WorkflowStatus.RUNNING);
		final List<WorkflowModel> allWorkflows = getAllWorkflows(statuses, null, null);

		return allWorkflows.stream()
				.filter(w -> SERVICE_NOTICE_WORKFLOW_TEMPLATE_CODE.equals(w.getJob().getCode()) && isExpiryDateOfAttachmentPassed(w))
				.collect(
						Collectors.toList());
	}

	private boolean isExpiryDateOfAttachmentPassed(final WorkflowModel w)
	{
		if (CollectionUtils.isNotEmpty(w.getAttachments()))
		{
			final ServiceNoticeModel attachment = (ServiceNoticeModel) w.getAttachments().get(0).getItem();
			if (Objects.nonNull(attachment))
			{
				return attachment.getExpiryDate().before(new Date());
			}
		}
		return false;
	}

	@Override
	public void terminateWorkflow(final List<WorkflowModel> workflows)
	{
		final List<ItemModel> modelsToSave = new ArrayList<>();
		workflows.forEach(expiredWorkflow -> {
			expiredWorkflow.setStatus(CronJobStatus.FINISHED);

			final List<WorkflowActionModel> actionsToUpdated = expiredWorkflow.getActions().stream().filter(
					action -> WorkflowActionStatus.IN_PROGRESS.equals(action.getStatus()) || WorkflowActionStatus.PENDING
							.equals(action.getStatus()) || WorkflowActionStatus.PAUSED.equals(action.getStatus()))
					.collect(Collectors.toList());

			actionsToUpdated.forEach(action -> action.setStatus(WorkflowActionStatus.ENDED_THROUGH_END_OF_WORKFLOW));
			modelsToSave.add(expiredWorkflow);
			modelsToSave.addAll(actionsToUpdated);
		});

		if (CollectionUtils.isNotEmpty(modelsToSave))
		{
			getModelService().saveAll(modelsToSave);
		}
	}

	@Override
	public List<WorkflowModel> getActiveWorkflowForServiceNotice(final ServiceNoticeModel serviceNotice)
	{
		final List<WorkflowModel> workflows = bcfWorkflowDao.findWorkflowForServiceNotice(serviceNotice);
		return workflows.stream().filter(workflow -> !isFinished(workflow)).collect(Collectors.toList());
	}

	@Override
	public List<WorkflowModel> getActiveWorkflowForTravelAdvisory(final TravelAdvisoryModel travelAdvisoryModel)
	{
		final List<WorkflowModel> workflows = bcfWorkflowDao.findWorkflowForTravelAdvisory(travelAdvisoryModel);
		return workflows.stream().filter(workflow -> !isFinished(workflow)).collect(Collectors.toList());
	}

	public WorkflowTemplateService getWorkflowTemplateService()
	{
		return workflowTemplateService;
	}

	@Override
	public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService)
	{
		this.workflowTemplateService = workflowTemplateService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public WorkflowProcessingService getWorkflowProcessingService()
	{
		return workflowProcessingService;
	}

	public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService)
	{
		this.workflowProcessingService = workflowProcessingService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		super.setModelService(modelService);
		this.modelService = modelService;
	}
}
