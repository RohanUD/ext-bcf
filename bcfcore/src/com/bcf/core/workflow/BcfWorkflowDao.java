/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.workflow;

import de.hybris.platform.workflow.daos.WorkflowDao;
import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.List;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.TravelAdvisoryModel;


public interface BcfWorkflowDao extends WorkflowDao
{
	List<WorkflowModel> findWorkflowForServiceNotice(final ServiceNoticeModel serviceNotice);

	List<WorkflowModel> findWorkflowForTravelAdvisory(final TravelAdvisoryModel travelAdvisoryModel);

}
