/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.amend.order.helper;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.travelsector.service.TravelSectorService;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integrations.search.booking.response.Itinerary;


public class AbstractOrderEntriesHelper
{
	private BcfTransportOfferingService transportOfferingService;
	private TravelSectorService travelSectorService;
	private BCFTravelRouteService travelRouteService;
	private BundleTemplateService bundleTemplateService;
	private BcfProductService bcfProductService;
	private CatalogVersionService catalogVersionService;
	private BCFTravellerService travellerService;
	private BCFPassengerTypeService passengerTypeService;
	private ModelService modelService;

	public List<TransportOfferingModel> fetchTransportOfferings(final Itinerary itinerary)
	{
		final List<SailingLine> lines = itinerary.getSailing().getLine();
		final List<TransportOfferingModel> transportOfferings = new ArrayList<>();
		lines.forEach(line -> {
			final String origin = line.getLineInfo().getDeparturePortCode();
			final String destination = line.getLineInfo().getArrivalPortCode();
			final List<TravelSectorModel> travelSectors = getTravelSectorService().getTravelSectors(origin, destination);
			final Date departureDate = TravelDateUtils
					.getDate(line.getLineInfo().getDepartureDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
			final Date arrivalDate = TravelDateUtils
					.getDate(line.getLineInfo().getArrivalDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
			if (CollectionUtils.isNotEmpty(travelSectors))
			{
				TransportOfferingModel transportOffering = getTransportOfferingService()
						.getTransportOfferings(departureDate, arrivalDate, travelSectors.get(0));
				if (Objects.isNull(transportOffering))
				{
					transportOffering = getTransportOfferingService()
							.getDefaultTransportOfferingForSector(travelSectors.get(0).getCode());
				}
				transportOfferings.add(transportOffering);
			}
		});
		return transportOfferings;
	}

	public TravelRouteModel fetchTravelRoute(final Itinerary itinerary)
	{
		final List<SailingLine> lines = itinerary.getSailing().getLine();
		final String originCode = lines.stream().findFirst().get().getLineInfo().getDeparturePortCode();
		final String destinationCode = lines.stream().reduce((a, b) -> b).get().getLineInfo().getArrivalPortCode();
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
		return travelRoutes.stream().findFirst().orElse(null);
	}

	public ProductModel getProduct(final ProductFare productFare, final String tariffType)
	{
		final ProductModel productModel;
		final CatalogVersionModel catalogVersion = getCatalogVersionService()
				.getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG, BcfCoreConstants.CATALOG_VERSION);
		switch (productFare.getProduct().getProductCategory())
		{
			case BcfCoreConstants.CATEGORY_AMENITY:
				productModel = getBcfProductService().getProductForCode(catalogVersion, productFare.getProduct().getProductId());
				break;
			case BcfCoreConstants.CATEGORY_OTHER:
				productModel = getBcfProductService().getProductForCode(catalogVersion, productFare.getProduct().getProductId());
				break;
			case BcfCoreConstants.CATEGORY_PASSENGER:
				productModel = getBcfProductService()
						.getFareProductForFareBasisCode(tariffType, FareProductType.valueOf(BcfCoreConstants.CATEGORY_PASSENGER));
				break;
			case BcfCoreConstants.CATEGORY_VEHICLE:
				productModel = getBcfProductService()
						.getFareProductForFareBasisCode(tariffType, FareProductType.valueOf(BcfCoreConstants.CATEGORY_VEHICLE));
				break;
			case BcfCoreConstants.CATEGORY_STOWAGE:
				productModel = getBcfProductService().getProductForCode(catalogVersion, productFare.getProduct().getProductId());
				break;
			default:
				productModel = getBcfProductService().getProductForCode(catalogVersion, BcfCoreConstants.DEFAULT_FARE_PRODUCT_CODE);
		}
		return productModel;
	}

	public BundleTemplateModel fetchBundleTemplate(final ProductModel productModel)
	{
		final List<BundleTemplateModel> bundleTemplates = getBundleTemplateService().getBundleTemplatesByProduct(productModel);
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(bundleTemplates))
		{
			final Optional<BundleTemplateModel> optionalBundleTemplate = bundleTemplates.stream()
					.filter(bundleTemplate -> Objects.nonNull(bundleTemplate.getType())).findFirst();
			if (optionalBundleTemplate.isPresent())
			{
				return optionalBundleTemplate.get();
			}
		}
		return null;
	}

	public int getMaxIntAsBundleNumber(final AbstractOrderModel abstractOrderModel)
	{
		final Optional<Integer> currentBundleNumber = abstractOrderModel.getEntries().stream()
				.map(AbstractOrderEntryModel::getBundleNo)
				.distinct().collect(Collectors.toList()).stream().reduce(Integer::min);
		if (currentBundleNumber.isPresent())
		{
			return (Math.abs(currentBundleNumber.get()) + 2);
		}
		return -1;
	}

	public Double getProductBasePrice(final ProductFare productFare)
	{
		return (productFare.getFareDetail().stream().filter(fareDetail ->productFare.getProduct().getProductId().equals(fareDetail.getFareCode())).mapToDouble(fareDetail -> fareDetail.getGrossAmountInCents()).sum() / 100.0d);
	}

	public List<TravellerModel> createPassengerTraveller(final ProductFare product, final int journeyRefNumber, final int count,
			final String orderOrCartCode)
	{
		final TravellerModel traveller;
		final PassengerTypeModel passengerType = getPassengerTypeService()
				.getPassengerForEBookingCode(product.getProduct().getProductId());
		final String passengerTypeCode = Objects.nonNull(passengerType) ? passengerType.getCode() : StringUtils.EMPTY;
		final StringBuilder travellerCode = new StringBuilder(passengerTypeCode).append(count)
				.append(journeyRefNumber);
		traveller = getTravellerService()
				.createTraveller(BcfCoreConstants.TRAVELLER_TYPE_PASSENGER, passengerTypeCode, travellerCode.toString(), count,
						orderOrCartCode, null);
		return Arrays.asList(traveller);
	}

	public List<TravellerModel> createVehicleTraveller(final ProductFare product, final int journeyRefNumber, final int count,
			final String orderOrCartCode)
	{
		final TravellerModel traveller;
		final StringBuilder travellerCode = new StringBuilder(product.getProduct().getProductId()).append(count)
				.append(journeyRefNumber);
		final VehicleInformationData vehicleData = createVehicleInformationData(product);
		traveller = getTravellerService()
				.createVehicleInformation(BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, vehicleData, travellerCode.toString(), count,
						orderOrCartCode, orderOrCartCode);
		return Arrays.asList(traveller);
	}

	public VehicleInformationData createVehicleInformationData(final ProductFare product)
	{
		final VehicleInformationData vehicleData = new VehicleInformationData();
		final VehicleTypeData vehicleType = new VehicleTypeData();
		vehicleType.setCode(product.getProduct().getProductId());
		vehicleData.setVehicleType(vehicleType);
		return vehicleData;
	}

	public TravelOrderEntryInfoModel createTravelOrderEntryInfo(final TravelRouteModel travelRoute,
			final List<TransportOfferingModel> transportOfferings)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfoModel = getModelService().create(TravelOrderEntryInfoModel.class);
		travelOrderEntryInfoModel.setTravelRoute(travelRoute);
		travelOrderEntryInfoModel.setOriginDestinationRefNumber(0);
		travelOrderEntryInfoModel.setTransportOfferings(transportOfferings);
		return travelOrderEntryInfoModel;
	}

	public TravelOrderEntryInfoModel getExistingTravelOrderEntryInfoForAncillary(final AbstractOrderModel abstractOrderModel,
			final int journeyRefNumber)
	{
		final Optional<AbstractOrderEntryModel> optionalOrderEntryModel = abstractOrderModel.getEntries().stream()
				.filter(cartEntry ->
						FareProductModel._TYPECODE.equals(cartEntry.getProduct().getItemtype())
								&& cartEntry.getJourneyReferenceNumber() == journeyRefNumber
								&& Objects.nonNull(cartEntry.getTravelOrderEntryInfo())
								&& TravellerType.PASSENGER
								.equals(cartEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType()))
				.findFirst();
		if (optionalOrderEntryModel.isPresent())
		{
			return optionalOrderEntryModel.get().getTravelOrderEntryInfo();
		}
		return null;
	}

	protected BcfTransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final BcfTransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	protected TravelSectorService getTravelSectorService()
	{
		return travelSectorService;
	}

	@Required
	public void setTravelSectorService(final TravelSectorService travelSectorService)
	{
		this.travelSectorService = travelSectorService;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BcfProductService getBcfProductService()
	{
		return bcfProductService;
	}

	@Required
	public void setBcfProductService(final BcfProductService bcfProductService)
	{
		this.bcfProductService = bcfProductService;
	}

	protected BCFTravellerService getTravellerService()
	{
		return travellerService;
	}

	@Required
	public void setTravellerService(final BCFTravellerService travellerService)
	{
		this.travellerService = travellerService;
	}

	protected BCFPassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
