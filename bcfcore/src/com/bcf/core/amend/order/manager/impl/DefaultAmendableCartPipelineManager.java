/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.amend.order.manager.impl;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.amend.order.handlers.CartCreationHandler;
import com.bcf.core.amend.order.manager.AmendableCartPipelineManager;
import com.bcf.integration.data.SearchBookingResponseDTO;


public class DefaultAmendableCartPipelineManager implements AmendableCartPipelineManager
{

	private List<CartCreationHandler> handlers;
	private ModelService modelService;
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;
	private CommonI18NService commonI18NService;

	@Override
	public CartModel executePipeline(SearchBookingResponseDTO searchBookingResponse)
	{
		CartModel cart = getModelService().create(CartModel.class);
		getHandlers().forEach(handler -> {
			handler.handle(searchBookingResponse, cart);
		});

		cart.setCurrency(getCommonI18NService().getCurrentCurrency());
		cart.setCreatedFromEBooking(Boolean.TRUE);

		getModelService().saveAll();
		getCommerceCartCalculationStrategy().calculateCart(cart);

		return cart;
	}

	protected List<CartCreationHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(List<CartCreationHandler> handlers)
	{
		this.handlers = handlers;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CommerceCartCalculationStrategy getCommerceCartCalculationStrategy()
	{
		return commerceCartCalculationStrategy;
	}

	@Required
	public void setCommerceCartCalculationStrategy(CommerceCartCalculationStrategy commerceCartCalculationStrategy)
	{
		this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
