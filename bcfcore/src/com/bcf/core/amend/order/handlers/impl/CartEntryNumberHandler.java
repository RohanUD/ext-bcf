/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.amend.order.handlers.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.amend.order.handlers.CartCreationHandler;
import com.bcf.integration.data.SearchBookingResponseDTO;


public class CartEntryNumberHandler implements CartCreationHandler
{

	@Override
	public void handle(final SearchBookingResponseDTO searchBookingResponse, final AbstractOrderModel abstractOrderModel)
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return;
		}
		AtomicInteger atomicInteger = new AtomicInteger(0);
		abstractOrderModel.getEntries().forEach(entry -> entry.setEntryNumber(atomicInteger.getAndIncrement()));
	}
}
