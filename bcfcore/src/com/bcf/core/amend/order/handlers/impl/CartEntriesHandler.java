/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.amend.order.handlers.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.configurablebundleservices.constants.ConfigurableBundleServicesConstants;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.amend.order.handlers.CartCreationHandler;
import com.bcf.core.amend.order.helper.AbstractOrderEntriesHelper;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.search.booking.response.Itinerary;


public class CartEntriesHandler implements CartCreationHandler
{
	private ModelService modelService;
	private BCFTravelRouteService travelRouteService;
	private BcfTransportOfferingService transportOfferingService;
	private BCFPassengerTypeService passengerTypeService;
	private CatalogVersionService catalogVersionService;
	private BcfProductService bcfProductService;
	private AbstractOrderEntriesHelper abstractOrderEntriesHelper;

	@Override
	public void handle(final SearchBookingResponseDTO searchBookingResponse, final AbstractOrderModel abstractOrderModel)
	{
		for (int journeyRefNum = 0; journeyRefNum < searchBookingResponse.getSearchResult().size(); journeyRefNum++)
		{
			final Itinerary itinerary = searchBookingResponse.getSearchResult().get(journeyRefNum);
			final List<ProductFare> allProducts = new ArrayList<>();
			itinerary.getSailing().getLine().stream().forEach(sailingLine -> {
				sailingLine.getSailingPrices().stream().forEach(sailingPrices -> {
					allProducts.addAll(sailingPrices.getProductFares());
				});
			});
			final List<ProductFare> otherCharges = StreamUtil.safeStream(itinerary.getSailing().getOtherCharges())
					.collect(Collectors.toList());
			allProducts.addAll(otherCharges);
			for (final ProductFare product : allProducts)
			{
				final long entriesToBeCreatedSize = product.getProduct().getProductNumber();
				for (int count = 1; count <= entriesToBeCreatedSize; count++)
				{
					final AbstractOrderEntryModel cartEntry = createEntryPerProduct(abstractOrderModel, itinerary, product,
							journeyRefNum, count, abstractOrderModel.getCode());
					if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
					{
						final List<AbstractOrderEntryModel> entries = new ArrayList<>();
						entries.add(cartEntry);
						abstractOrderModel.setEntries(entries);
					}
					else
					{
						abstractOrderModel.getEntries().add(cartEntry);
					}
					cartEntry.setOrder(abstractOrderModel);
				}
			}
		}
	}

	protected AbstractOrderEntryModel createEntryPerProduct(final AbstractOrderModel abstractOrderModel, final Itinerary itinerary,
			final ProductFare product, final int journeyRefNumber, final int count, final String orderOrCartCode)
	{
		final AbstractOrderEntryModel cartEntry = getModelService().create(CartEntryModel.class);
		updateCartEntryWithTravelData(abstractOrderModel, cartEntry, itinerary, product, journeyRefNumber, count, orderOrCartCode);
		updateCartEntryWithMiscDetails(itinerary, journeyRefNumber, cartEntry);
		final String tariffType = itinerary.getSailing().getLine().stream().findFirst().get().getSailingPrices().stream()
				.findFirst().get().getTariffType();
		updateCartEntryWithProductAndPrice(abstractOrderModel, cartEntry, product, tariffType);
		return cartEntry;
	}

	protected void updateCartEntryWithProductAndPrice(final AbstractOrderModel abstractOrderModel,
			final AbstractOrderEntryModel cartEntry, final ProductFare product, final String tariffType)
	{
		final ProductModel productModel = getAbstractOrderEntriesHelper().getProduct(product, tariffType);
		final Integer bundleNo = BcfCoreConstants.CATEGORY_OTHER.equals(product.getProduct().getProductCategory()) ?
				0 :
				getBundleNumber(abstractOrderModel);
		UnitModel unit = productModel.getUnit();
		if (Objects.isNull(unit))
		{
			unit = getBcfProductService().getOrderableUnit(productModel);
		}
		cartEntry.setProduct(productModel);
		cartEntry.setBundleTemplate(getAbstractOrderEntriesHelper().fetchBundleTemplate(productModel));
		cartEntry.setUnit(unit);
		cartEntry.setQuantity(1L);
		cartEntry.setBundleNo(Integer.valueOf(ConfigurableBundleServicesConstants.NO_BUNDLE));
		cartEntry.setEntryNumber(bundleNo);
		cartEntry.setBasePrice(getAbstractOrderEntriesHelper().getProductBasePrice(product));
	}

	protected Integer getBundleNumber(final AbstractOrderModel abstractOrderModel)
	{
		return CollectionUtils.isEmpty(abstractOrderModel.getEntries()) ?
				2 :
				getAbstractOrderEntriesHelper().getMaxIntAsBundleNumber(abstractOrderModel);
	}



	/**
	 * @param itinerary
	 * @param journeyRefNumber
	 * @param cartEntry
	 */
	protected void updateCartEntryWithMiscDetails(final Itinerary itinerary, final int journeyRefNumber,
			final AbstractOrderEntryModel cartEntry)
	{
		cartEntry.setJourneyReferenceNumber(journeyRefNumber);
		cartEntry.setType(OrderEntryType.TRANSPORT);
		cartEntry.setActive(Boolean.TRUE);
		cartEntry.setAmendStatus(AmendStatus.SAME);
		cartEntry.setCalculated(Boolean.FALSE);
		cartEntry.setBookingReference(itinerary.getBooking().getBookingReference().getBookingReference());
	}

	/**
	 * @param itinerary
	 */
	protected void updateCartEntryWithTravelData(final AbstractOrderModel abstractOrderModel,
			final AbstractOrderEntryModel cartEntry, final Itinerary itinerary, final ProductFare product,
			final int journeyRefNumber, final int count, final String orderOrCartCode)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo;
		if (BcfCoreConstants.CATEGORY_OTHER.equals(product.getProduct().getProductCategory()) || BcfCoreConstants.CATEGORY_AMENITY
				.equals(product.getProduct().getProductCategory()))
		{
			travelOrderEntryInfo = CollectionUtils.isNotEmpty(abstractOrderModel.getEntries()) ?
					getAbstractOrderEntriesHelper().getExistingTravelOrderEntryInfoForAncillary(abstractOrderModel, journeyRefNumber) :
					null;
		}
		else
		{
			List<TravellerModel> travellerModels = new ArrayList<>();
			if (BcfCoreConstants.TRAVELLER_TYPE_PASSENGER.equals(product.getProduct().getProductCategory()))
			{
				travellerModels = getAbstractOrderEntriesHelper()
						.createPassengerTraveller(product, journeyRefNumber, count, orderOrCartCode);
			}
			else if (BcfCoreConstants.TRAVELLER_TYPE_VEHICLE.equals(product.getProduct().getProductCategory()))
			{
				travellerModels = getAbstractOrderEntriesHelper()
						.createVehicleTraveller(product, journeyRefNumber, count, orderOrCartCode);
			}
			travelOrderEntryInfo = getAbstractOrderEntriesHelper()
					.createTravelOrderEntryInfo(getAbstractOrderEntriesHelper().fetchTravelRoute(itinerary),
							getAbstractOrderEntriesHelper().fetchTransportOfferings(itinerary));
			travelOrderEntryInfo.setTravellers(travellerModels);
		}
		cartEntry.setTravelOrderEntryInfo(travelOrderEntryInfo);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected BcfTransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final BcfTransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	protected BCFPassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected BcfProductService getBcfProductService()
	{
		return bcfProductService;
	}

	@Required
	public void setBcfProductService(final BcfProductService bcfProductService)
	{
		this.bcfProductService = bcfProductService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected AbstractOrderEntriesHelper getAbstractOrderEntriesHelper()
	{
		return abstractOrderEntriesHelper;
	}

	@Required
	public void setAbstractOrderEntriesHelper(final AbstractOrderEntriesHelper abstractOrderEntriesHelper)
	{
		this.abstractOrderEntriesHelper = abstractOrderEntriesHelper;
	}
}
