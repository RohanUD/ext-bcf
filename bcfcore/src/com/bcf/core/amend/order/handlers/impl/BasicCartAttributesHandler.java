/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.amend.order.handlers.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Date;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.amend.order.handlers.CartCreationHandler;
import com.bcf.core.order.strategies.update.BcfUpdateOrderFromEBookingStrategy;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.search.booking.response.Itinerary;


public class BasicCartAttributesHandler implements CartCreationHandler
{
	private KeyGenerator orderCodeGenerator;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private UserService userService;
	private TimeService timeService;
	private BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy;


	@Override
	public void handle(final SearchBookingResponseDTO searchBookingResponse, final AbstractOrderModel abstractOrderModel)
	{
		abstractOrderModel.setCode(getOrderCodeGenerator().generate().toString());
		((CartModel) abstractOrderModel).setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel());
		abstractOrderModel.setUser(getUserService().getCurrentUser());
		abstractOrderModel.setQuoteExpirationDate(null);
		final Date currentTime = getTimeService().getCurrentTime();
		abstractOrderModel.setDate(currentTime);
		final long previouslyPaidFareInCents = StreamUtil.safeStream(searchBookingResponse.getSearchResult()).mapToLong(
				itinerary ->
						Objects.nonNull(itinerary.getBooking()) && Objects.nonNull(itinerary.getBooking().getPaidFaresInCents()) ?
								itinerary.getBooking().getPaidFaresInCents().longValue() :
								0).sum();
		abstractOrderModel.setAmountPaid(previouslyPaidFareInCents > 0 ? previouslyPaidFareInCents / 100d : 0d);

		if (CollectionUtils.isNotEmpty(searchBookingResponse.getSearchResult()))
		{
			final Itinerary itinerary = searchBookingResponse.getSearchResult().stream()
					.filter(itineraryData -> itineraryData.getBooking() != null).findFirst().orElse(null);
			if (itinerary != null)
			{
				bcfUpdateOrderFromEBookingStrategy
						.updateUserSystemIdentifier(itinerary.getBooking(),
								getUserService().getCurrentUser());
			}
		}

	}

	protected KeyGenerator getOrderCodeGenerator()
	{
		return orderCodeGenerator;
	}

	@Required
	public void setOrderCodeGenerator(final KeyGenerator orderCodeGenerator)
	{
		this.orderCodeGenerator = orderCodeGenerator;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	public BcfUpdateOrderFromEBookingStrategy getBcfUpdateOrderFromEBookingStrategy()
	{
		return bcfUpdateOrderFromEBookingStrategy;
	}

	public void setBcfUpdateOrderFromEBookingStrategy(
			final BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy)
	{
		this.bcfUpdateOrderFromEBookingStrategy = bcfUpdateOrderFromEBookingStrategy;
	}
}
