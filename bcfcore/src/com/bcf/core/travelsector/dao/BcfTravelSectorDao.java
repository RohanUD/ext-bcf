/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelsector.dao;

import de.hybris.platform.travelservices.dao.TravelSectorDao;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.List;


public interface BcfTravelSectorDao extends TravelSectorDao
{
	List<TravelSectorModel> findAllTravelSectors();

	List<TravelSectorModel> findTravelSectors(String origin, String destination);

	TravelSectorModel findTravelSector(String travelSectorCode, String travelRouteCode);

	TravelSectorModel findTravelSectorForCode(String sectorCode);
}
