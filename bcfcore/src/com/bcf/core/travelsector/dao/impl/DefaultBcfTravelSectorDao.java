/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelsector.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelSectorDao;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.travelsector.dao.BcfTravelSectorDao;


public class DefaultBcfTravelSectorDao extends DefaultTravelSectorDao implements BcfTravelSectorDao
{

	private static final String FIND_TRAVEL_SECTOR_FOR_CODE =
			"Select {" + TravelSectorModel.PK + "} from {" + TravelSectorModel._TYPECODE + " as ts} where {ts."+ TravelSectorModel.CODE +"}=?" + TravelSectorModel.CODE;

	private static final String FIND_TRAVEL_SECTOR_FOR_CODE_TRAVEL_ROUTE =
			"Select {" + TravelSectorModel.PK + "} from {TravelSector as ts join TravelRouteTravelSectorRelation as trtsr on {ts."
					+ TravelSectorModel.PK
					+ "}={trtsr.target} join TravelRoute as tr on {trtsr.source}={tr." + TravelRouteModel.PK + "}} where {tr."
					+ TravelRouteModel.CODE + "}=?" + TravelRouteModel.CODE + " and {ts.code}=?" + TravelSectorModel.CODE;

	public DefaultBcfTravelSectorDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<TravelSectorModel> findAllTravelSectors()
	{
		return find();
	}

	@Override
	public List<TravelSectorModel> findTravelSectors(final String origin, final String destination)
	{
		validateParameterNotNull(origin, "Parameter origin cannot be null");
		validateParameterNotNull(destination, "Parameter destination cannot be null");

		final StringBuilder sb = new StringBuilder();

		sb.append("SELECT {ts.pk} FROM { ").append(TravelSectorModel._TYPECODE).append(" AS ts JOIN ")
				.append(TransportFacilityModel._TYPECODE).append(" AS tfo ON {ts.").append(TravelSectorModel.ORIGIN).append("}={tfo.")
				.append(TransportFacilityModel.PK).append("} join ").append(TransportFacilityModel._TYPECODE).append(" AS tfd ON ")
				.append("{ts.").append(TravelSectorModel.DESTINATION).append("}={tfd.").append(TransportFacilityModel.PK)
				.append("}} WHERE {tfo.").append(TransportFacilityModel.CODE).append("} = ?originCode").append(" AND {tfd.")
				.append(TransportFacilityModel.CODE).append("} = ?destinationCode");

		final Map<String, Object> params = new HashMap<>();
		params.put("originCode", origin);
		params.put("destinationCode", destination);

		final SearchResult<TravelSectorModel> searchResult = getFlexibleSearchService().search(sb.toString(), params);

		return searchResult.getResult();
	}

	@Override
	public TravelSectorModel findTravelSector(final String travelSectorCode, final String travelRouteCode)
	{
		validateParameterNotNull(travelSectorCode, "Parameter travelSectorCode cannot be null");
		validateParameterNotNull(travelRouteCode, "Parameter travelRouteCode cannot be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(TravelRouteModel.CODE, travelRouteCode);
		params.put(TravelSectorModel.CODE, travelSectorCode);

		final SearchResult<TravelSectorModel> searchResult = getFlexibleSearchService()
				.search(FIND_TRAVEL_SECTOR_FOR_CODE_TRAVEL_ROUTE, params);

		if (CollectionUtils.size(searchResult.getResult()) > 1)
		{
			throw new AmbiguousIdentifierException("Multiple TravelSectors with same code exist");
		}

		if (CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return null;
		}

		return searchResult.getResult().get(0);
	}

	@Override
	public TravelSectorModel findTravelSectorForCode(final String sectorCode)
	{
		validateParameterNotNull(sectorCode, "Parameter sectorCode cannot be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(TravelSectorModel.CODE, sectorCode);

		final SearchResult<TravelSectorModel> searchResult = getFlexibleSearchService().search(FIND_TRAVEL_SECTOR_FOR_CODE, params);

		if (CollectionUtils.size(searchResult.getResult()) > 1)
		{
			throw new AmbiguousIdentifierException("Multiple TravelSectors with same code exist");
		}

		if (CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return null;
		}

		return searchResult.getResult().get(0);
	}


}
