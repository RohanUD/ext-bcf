/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelsector.service.impl;

import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.List;
import com.bcf.core.travelsector.dao.BcfTravelSectorDao;
import com.bcf.core.travelsector.service.TravelSectorService;


public class DefaultTravelSectorService implements TravelSectorService
{

	private BcfTravelSectorDao travelSectorDao;

	@Override
	public List<TravelSectorModel> getTravelSectors()
	{

		return travelSectorDao.findAllTravelSectors();
	}

	@Override
	public List<TravelSectorModel> getTravelSectors(final String originCode, final String destinationCode)
	{
		return getTravelSectorDao().findTravelSectors(originCode, destinationCode);
	}

	@Override
	public TravelSectorModel getTravelSector(final String travelSectorCode, final String travelRouteCode)
	{
		return getTravelSectorDao().findTravelSector(travelSectorCode, travelRouteCode);
	}

	@Override
	public TravelSectorModel getTravelSectorForCode(final String sectorCode)
	{
		return getTravelSectorDao().findTravelSectorForCode(sectorCode);
	}

	protected BcfTravelSectorDao getTravelSectorDao()
	{
		return travelSectorDao;
	}

	public void setTravelSectorDao(final BcfTravelSectorDao travelSectorDao)
	{
		this.travelSectorDao = travelSectorDao;
	}

}
