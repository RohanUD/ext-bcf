/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.travelsector.service;

import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.List;


public interface TravelSectorService
{

	List<TravelSectorModel> getTravelSectors();

	List<TravelSectorModel> getTravelSectors(String originCode, String destinationCode);

	TravelSectorModel getTravelSector(String travelSectorCode, String travelRouteCode);

	TravelSectorModel getTravelSectorForCode(String sectorCode);
}
