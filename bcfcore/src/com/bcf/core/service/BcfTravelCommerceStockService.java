/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.stock.TravelCommerceStockService;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import com.bcf.core.enums.StockLevelType;


public interface BcfTravelCommerceStockService extends TravelCommerceStockService
{

	/**
	 * Release stocks.
	 *
	 * @param cart the cart
	 */
	void releaseStocks(CartModel cart);

	StockLevelType getStockLevelType(ProductModel product);

	void batchSavingOfStockLevels(List<StockLevelModel> stockLevelsToSave);

	Collection<WarehouseModel> getWarehouses(AbstractOrderEntryModel entry);

	void releaseActivityStock(AbstractOrderEntryModel activityEntry);

	Integer getStockAvailableForDate(ProductModel product, Date date, Collection<WarehouseModel> warehouses);

	StockLevelModel getStockLevelModelForDate(ProductModel product, Date date, Collection<WarehouseModel> warehouses);

	Integer getDefaultStockValue();

	int getStockHoldTimeInMs(SalesApplication channel);

	boolean getEarlyStockAllocation(SalesApplication channel);

	long getCartStockExpiryTime(CartModel cart);

	long getStockHoldExpiryTimeInMs(SalesApplication channel);

	int getStockAboutToExpireTimeInMs(final SalesApplication channel);

	void releaseStocks(final List<AbstractOrderEntryModel> entries);

	boolean checkOtherEntriesPresent(CartModel cart, final List<AbstractOrderEntryModel> entries);

	void reservePerDateTimeProduct(final ProductModel product, String selectedTime, final Date date, final int quantity,
			final Collection<WarehouseModel> warehouses) throws InsufficientStockLevelException;

	void releasePerDateTimeProduct(final ProductModel product, String selectedTime, final Date date, final int quantity,
			final Collection<WarehouseModel> warehouses);

}
