/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;



public interface BcfAsmCommentsService
{
	List<CommentModel> getAsmComments(final String orderCode);

	public List<CommentModel> getAsmAccommodationSupplierComments(final String code, String ref, String orderCode);

	public List<CommentModel> getAsmActivitySupplierComments(final String code,String orderCode);

	void addAsmComment(final String orderCode, String comment,  String agentId);

	void addAsmAccommodationSupplierComment(final String code, String ref, String comment, String agentId, String orderCode);

	void addAsmActivitySupplierComment(final String code, final String comment,  final String agentId,String orderCode);

	public List<CommentModel> getAsmAccommodationSupplierComments(final String code, String ref,
			final AbstractOrderModel abstractOrder);

	public List<CommentModel> getAsmAccommodationSupplierComments(final String code,
			final AbstractOrderModel abstractOrder);

	List<CommentModel> getAsmActivitySupplierComments(final String code,  final AbstractOrderModel abstractOrder);
}
