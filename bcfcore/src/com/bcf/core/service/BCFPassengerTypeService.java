/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.List;


public interface BCFPassengerTypeService extends PassengerTypeService
{
	PassengerTypeModel getPassengerForEBookingCode(String eBookingCode);

	List<PassengerTypeModel> getPassengerTypesForCodes(final List<String> codes);

	void deletePassengerTypeForCode(String code);

	/**
	 * gets age range for a particular passenger type.
	 * <br>
	 *    In case if the passengerType is not SpecializedPassengetType then use name
	 *    else
	 *    calculate based on minAge and maxAge
	 * @param passengerType
	 * @return
	 */
	String getAgeRangeForPassengerType(final PassengerTypeModel passengerType);


	Integer getMinAgeByPassengerTypeCode(String code);

	PassengerTypeModel getPassengerTypesForCode(String code);
}
