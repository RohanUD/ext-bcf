/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.deals.solrfacetsearch.strategies;

import de.hybris.platform.solrfacetsearch.search.RawQuery;
import com.bcf.core.solr.query.ArguementData;


public interface RawQueryStrategy
{
	/**
	 * method to generate arguement to be used while creating RawQuery for Solr
	 *
	 * @param rawfilterValue
	 * @return
	 */
	ArguementData generateArguementForRawQuery(String rawFilterKey, String rawfilterValue);

	/**
	 * method to create RawQuery for Solr
	 *
	 * @param arguement
	 * @return
	 */
	RawQuery createRawQuery(ArguementData arguement);
}
