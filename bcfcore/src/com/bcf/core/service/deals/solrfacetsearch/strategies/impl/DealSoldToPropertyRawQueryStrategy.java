/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.deals.solrfacetsearch.strategies.impl;

import de.hybris.platform.solrfacetsearch.search.RawQuery;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.deals.solrfacetsearch.strategies.RawQueryStrategy;
import com.bcf.core.solr.query.ArguementData;


public class DealSoldToPropertyRawQueryStrategy extends DealValidityRawQueryStrategy implements RawQueryStrategy
{
	@Override
	public ArguementData generateArguementForRawQuery(final String rawFilterKey, final String rawfilterValue)
	{
		final String startDateParamName = getConfigurationService().getConfiguration().getString("deals.raw.query.param."+rawFilterKey.toLowerCase());
		return createArguementData(startDateParamName, BcfCoreConstants.SOLR_CURRENT_DATE_INCLUSIVE_PARAM_FOR_SOLD_TO);
	}

	@Override
	public RawQuery createRawQuery(final ArguementData arguementData)
	{
		return new RawQuery(createQuery(arguementData.getStartDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL,arguementData.getFieldArguement(), BcfCoreConstants.ASTERIK_SIGN));
	}

}
