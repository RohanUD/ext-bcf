/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.deals.solrfacetsearch.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.deals.solrfacetsearch.strategies.RawQueryStrategy;
import com.bcf.core.solr.query.ArguementData;


public class RawQueryUsingDateForDealsStrategy implements RawQueryStrategy
{
	private ConfigurationService configurationService;

	@Override
	public ArguementData generateArguementForRawQuery(final String rawFilterKey, final String rawfilterValue)
	{
		final String startDateParamName = getConfigurationService().getConfiguration().getString("deals.raw.query.param.startdate."+rawFilterKey.toLowerCase());
		final String endDateParamName = getConfigurationService().getConfiguration().getString("deals.raw.query.param.enddate."+rawFilterKey.toLowerCase());

		final ArguementData arguementData = new ArguementData();
		arguementData.setStartDateFieldName(startDateParamName);
		arguementData.setEndDateFieldName(endDateParamName);
		arguementData.setFieldArguement(getFieldArguement(rawfilterValue));

		return arguementData;
	}

	protected String getFieldArguement(final String rawfilterValue)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(BcfintegrationcoreConstants.BCF_CALANDER_DATE_FORMAT);
		final LocalDate localDate = LocalDate.parse(rawfilterValue, formatter);
		DateTimeFormatter solrFormatterWithoutTime = DateTimeFormatter.ofPattern(
				BcfintegrationcoreConstants.BCF_SAILING_DATE_TIME_PATTERN);
		localDate.format(solrFormatterWithoutTime);//NOSONAR
		final Date departureDateToCheck = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

		final String solrFormattedDateWithTime = DateFormatUtils
				.format(getDateForTime(departureDateToCheck), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
		final StringBuilder formattedDateString = new StringBuilder();
		return formattedDateString.append(solrFormattedDateWithTime).append("Z").toString();
	}

	@Override
	public RawQuery createRawQuery(final ArguementData arguementData)
	{
		final String fixedDummyDate = getConfigurationService().getConfiguration().getString("deals.dummy.daterange.date");
		StringBuilder formattedDate = new StringBuilder();
		formattedDate.append(BcfCoreConstants.LEFT_ANGLE_BRACKET);
		formattedDate.append(arguementData.getStartDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL);
		formattedDate.append(BcfCoreConstants.LEFT_SQUARE_BRACKET);
		formattedDate.append(BcfCoreConstants.ASTERIK_SIGN);
		formattedDate.append(BcfCoreConstants.SOLR_TO_OPERATOR);
		formattedDate.append(arguementData.getFieldArguement());
		formattedDate.append(BcfCoreConstants.RIGHT_SQUARE_BRACKET);
		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(BcfCoreConstants.OR_OPERATOR);
		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(arguementData.getStartDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL);
		formattedDate.append("\"");
		formattedDate.append(fixedDummyDate);
		formattedDate.append("\"");
		formattedDate.append(BcfCoreConstants.RIGHT_ANGLE_BRACKET);
		formattedDate.append(StringUtils.SPACE);

		formattedDate.append(BcfCoreConstants.AND_OPERATOR);

		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(BcfCoreConstants.LEFT_ANGLE_BRACKET);
		formattedDate.append(arguementData.getEndDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL);
		formattedDate.append(BcfCoreConstants.LEFT_SQUARE_BRACKET);
		formattedDate.append(arguementData.getFieldArguement());
		formattedDate.append(BcfCoreConstants.SOLR_TO_OPERATOR);
		formattedDate.append(BcfCoreConstants.ASTERIK_SIGN);
		formattedDate.append(BcfCoreConstants.RIGHT_SQUARE_BRACKET);
		formattedDate.append(StringUtils.SPACE);

		formattedDate.append(BcfCoreConstants.OR_OPERATOR);

		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(arguementData.getEndDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL);
		formattedDate.append("\"");
		formattedDate.append(fixedDummyDate);
		formattedDate.append("\"");
		formattedDate.append(BcfCoreConstants.RIGHT_ANGLE_BRACKET);

		return new RawQuery(formattedDate.toString());
	}

	protected Date getDateForTime(Date date)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(11, 0);
		c.set(12, 0);
		c.set(13, 0);
		return c.getTime();
	}

	protected ConfigurationService getConfigurationService() {
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
