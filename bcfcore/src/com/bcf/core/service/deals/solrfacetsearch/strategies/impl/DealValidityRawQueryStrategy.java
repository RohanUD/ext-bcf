/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.deals.solrfacetsearch.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.solr.query.ArguementData;


public class DealValidityRawQueryStrategy
{
	private ConfigurationService configurationService;

	public ArguementData createArguementData(String fieldName, String fieldValue)
	{
		final ArguementData arguementData = new ArguementData();
		arguementData.setStartDateFieldName(fieldName);
		arguementData.setFieldArguement(fieldValue);

		return arguementData;
	}

	public String createQuery(final String fieldName, final String leftOperand, final String rightOperand)
	{
		final String fixedDummyDate = getConfigurationService().getConfiguration().getString("deals.dummy.daterange.date");
		final StringBuilder formattedDate = new StringBuilder();
		formattedDate.append(BcfCoreConstants.LEFT_ANGLE_BRACKET);
		formattedDate.append(fieldName);
		formattedDate.append(BcfCoreConstants.LEFT_SQUARE_BRACKET);
		formattedDate.append(leftOperand);
		formattedDate.append(BcfCoreConstants.SOLR_TO_OPERATOR);
		formattedDate.append(rightOperand);
		formattedDate.append(BcfCoreConstants.RIGHT_SQUARE_BRACKET);
		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(BcfCoreConstants.OR_OPERATOR);
		formattedDate.append(StringUtils.SPACE);
		formattedDate.append(fieldName);
		formattedDate.append("\"");
		formattedDate.append(fixedDummyDate);
		formattedDate.append("\"");
		formattedDate.append(BcfCoreConstants.RIGHT_ANGLE_BRACKET);
		formattedDate.append(StringUtils.SPACE);

		return formattedDate.toString();
	}

	protected ConfigurationService getConfigurationService() {
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
