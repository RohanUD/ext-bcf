/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 12/7/19 8:51 AM
 */

package com.bcf.core.service;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import java.util.List;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * This class is responsible for creation/manipulation of business processes that needs to be sent to notification engine
 */
public interface BcfOptionBookingNotificationService
{
	/**
	 * starts create option booking email
	 *
	 * @param abstractOrder option booking cart
	 */
	void startOptionBookingEmailProcess(final AbstractOrderModel abstractOrder);

	/**
	 * start cancel option booking email
	 *
	 * @param abstractOrder option booking cart
	 */
	void startCancelOrderEmailProcess(final AbstractOrderModel abstractOrder);

	/**
	 * This method first create clone of the cart from the order entries provided. And send these cloned cart and entries in the option booing cancel business process.
	 * And then triggers cancel option booing cancel email business process.
	 *
	 * <p> Note: These cloned items are only used to create request for  notification engine and will later be deleted after creation of request.</p>
	 *
	 * @param orderEntryModels modified order entries
	 */
	void startCancellationProcessForOptionBooking(List<AbstractOrderEntryModel> orderEntryModels);

	/**
	 * starts cancel option booking email for the specific entries provided as parameter
	 *
	 * @param abstractOrder          option booking cart
	 * @param abstractOrderEntryList modified cart entries
	 */
	void startCancelOrderEmailProcess(final AbstractOrderModel abstractOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryList);

	/**
	 * starts create option booking email for the specific line entries provided as parameter
	 *
	 * @param abstractOrder            option booking cart
	 * @param cartModificationDataList modified entries list
	 */
	void startOptionBookingEmailProcess(final AbstractOrderModel abstractOrder,
			final List<CartModificationData> cartModificationDataList);

	/**
	 * starts create option booking email for the specific line entries provided as parameter
	 *
	 * @param abstractOrder          option booking cart
	 * @param abstractOrderEntryList modified entries list
	 */
	void startOptionBookingForEntriesEmailProcess(final AbstractOrderModel abstractOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryList);

	/**
	 * sends notification for the provided expired bookings
	 *
	 * @param expiredOptionBookings
	 */
	public void sendExpiredBookingNotifications(List<CartModel> expiredOptionBookings)
			throws IntegrationException;
}
