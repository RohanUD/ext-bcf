/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.BcfUserGroupDao;
import com.bcf.core.service.BcfUserGroupService;


public class DefaultBcfUserGroupService implements BcfUserGroupService
{
	BcfUserGroupDao bcfUserGroupDao;

	@Override
	public List<UserGroupModel> getUserGroups(final List<String> groupIds)
	{
		return getBcfUserGroupDao().getUserGroups(groupIds);
	}

	@Override
	public List<UserModel> getUsersByGroup(final List<String> salesAgentGroupList)
	{
		return getBcfUserGroupDao().getUsersByGroup(salesAgentGroupList);
	}

	protected BcfUserGroupDao getBcfUserGroupDao()
	{
		return bcfUserGroupDao;
	}

	@Required
	public void setBcfUserGroupDao(final BcfUserGroupDao bcfUserGroupDao)
	{
		this.bcfUserGroupDao = bcfUserGroupDao;
	}

}
