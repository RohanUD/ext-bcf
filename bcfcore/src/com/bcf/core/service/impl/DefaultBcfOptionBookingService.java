/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 9/7/19 1:21 PM
 */

package com.bcf.core.service.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfOptionBookingDao;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfOptionBookingService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfOptionBookingService implements BcfOptionBookingService
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOptionBookingService.class);

	@Resource(name = "optionBookingDao")
	private BcfOptionBookingDao bcfOptionBookingDao;

	@Resource(name = "bcfTravelCommerceCartService")
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	@Resource(name = "bcfTravelCommerceStockService")
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	@Resource(name = "travelCartService")
	BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "cancelBookingService")
	private CancelBookingService cancelBookingService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "bcfOptionBookingNotificationService")
	BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Override
	public CartModel getCartForCode(final String optionBookingId)
	{
		return getBcfTravelCommerceCartService()
				.getCartForCodeAndSite(optionBookingId, getBaseSiteService().getCurrentBaseSite());
	}

	@Override
	public SearchPageData<CartModel> getOptionBookings(final PageableData pageableData)
	{
		return getBcfOptionBookingDao().findPagedOptionBookings(pageableData);
	}

	@Override
	public void cancelAndReleaseOptionBookingInventory(final CartModel optionBookingCart) throws Exception
	{
		cancelAndReleaseTransportInventories(optionBookingCart);
		bcfTravelCommerceStockService.releaseStocks(optionBookingCart);
		updateOptionBookingCancellationStatus(optionBookingCart);
		getModelService().save(optionBookingCart);
	}

	@Override
	public void cancelAndReleaseInventoryForOptionBookings(final List<CartModel> optionBookingCarts)
	{
		List<ItemModel> itemsToSave = new ArrayList<>();
		for (CartModel optionBookingCart : optionBookingCarts)
		{
			try
			{
				cancelAndReleaseTransportInventories(optionBookingCart);
				bcfTravelCommerceStockService.releaseStocks(optionBookingCart);
				updateOptionBookingCancellationStatus(optionBookingCart);
				itemsToSave.add(optionBookingCart);
			}
			catch (final Exception e)
			{
				LOG.error(String.format("Error in cancelling and releasing inventory for option booking %s",
						optionBookingCart.getCode()), e);
			}
		}
		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
	}

	private CancelBookingResponseForCart cancelAndReleaseTransportInventories(final AbstractOrderModel optionBookingCart)
			throws IntegrationException
	{
		List<AbstractOrderEntryModel> transportOrderEntryModels = StreamUtil.safeStream(optionBookingCart.getEntries())
				.filter(entry -> entry.getTravelOrderEntryInfo() != null).collect(
						Collectors.toList());
		if (CollectionUtils.isNotEmpty(transportOrderEntryModels))
		{
			String previousBookingJourneyInSession = getSessionService()
					.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

			UserModel currentUser = getUserService().getCurrentUser();
			getSessionService()
					.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, optionBookingCart.getBookingJourneyType().getCode());
			getUserService().setCurrentUser(optionBookingCart.getUser());
			CancelBookingResponseForCart cancelBookingResponseForCart = getCancelBookingService()
					.cancelBooking((CartModel) optionBookingCart,
							transportOrderEntryModels);
			getSessionService()
					.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, previousBookingJourneyInSession);
			getUserService().setCurrentUser(currentUser);
			return cancelBookingResponseForCart;
		}
		return null;
	}

	private void updateOptionBookingCancellationStatus(final CartModel optionBookingCart)
	{
		optionBookingCart.setAmountToPay(0d);
		optionBookingCart.setPayAtTerminal(0d);
		optionBookingCart.setOptionBookingReleaseDate(new Date());
		optionBookingCart.setStatus(OrderStatus.CANCELLED);
	}

	@Override
	public List<CartModel> getExpiredOptionBookings()
	{
		return getBcfOptionBookingDao().findActiveOptionBookingsThatAreExpired();
	}

	@Override
	public List<CartModel> getExpiredOptionBookingsForPurging()
	{
		Integer purgeTimeInDays = Integer.valueOf(getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfCoreConstants.OPTION_BOOKING_PURGE_TIME_IN_DAYS));

		Date purgingDate = new Date();
		purgingDate = TravelDateUtils.addDays(purgingDate, -purgeTimeInDays);
		purgingDate = BCFDateUtils.setToEndOfDay(purgingDate);

		return getBcfOptionBookingDao().findExpiredOptionBookingsAfterDate(purgingDate);
	}

	@Override
	public void purgeOptionBookings(final List<CartModel> expiredOptionBookings)
	{
		for (CartModel expiredOptionBooking : expiredOptionBookings)
		{
			try
			{
				bcfTravelCartService.removeAssociatedItemsFromCart(expiredOptionBooking);
				getModelService().remove(expiredOptionBooking);
			}
			catch (final Exception ex)
			{
				LOG.error(String.format("Error in removing option booking %s", expiredOptionBooking.getCode()), ex);
			}
		}
	}

	public BcfOptionBookingDao getBcfOptionBookingDao()
	{
		return bcfOptionBookingDao;
	}

	public void setBcfOptionBookingDao(final BcfOptionBookingDao bcfOptionBookingDao)
	{
		this.bcfOptionBookingDao = bcfOptionBookingDao;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public CancelBookingService getCancelBookingService()
	{
		return cancelBookingService;
	}

	public void setCancelBookingService(final CancelBookingService cancelBookingService)
	{
		this.cancelBookingService = cancelBookingService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
