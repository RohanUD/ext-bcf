/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.BcfCustomerDao;
import com.bcf.core.service.BcfCustomerService;


public class DefaultBcfCustomerService implements BcfCustomerService
{
	private BcfCustomerDao bcfCustomerDao;

	@Override
	public CustomerModel getCustomerForPasswordRecoveryKey(final String key)
	{
		return getBcfCustomerDao().getCustomerforPasswordRecoveryKey(key);
	}

	@Override
	public CustomerModel getCustomerAccountValidationKey(final String accountValidationKey)
	{
		return getBcfCustomerDao().findCustomerForAccountValidationKey(accountValidationKey);
	}

	@Override
	public CustomerModel getCustomerForCustomerId(final String customerId)
	{
		return getBcfCustomerDao().findCustomerForCustomerId(customerId);
	}

	@Override
	public CustomerModel getCustomerForUId(final String uId)
	{
		return getBcfCustomerDao().findCustomerForUId(uId);
	}

	public BcfCustomerDao getBcfCustomerDao()
	{
		return bcfCustomerDao;
	}

	@Required
	public void setBcfCustomerDao(final BcfCustomerDao bcfCustomerDao)
	{
		this.bcfCustomerDao = bcfCustomerDao;
	}
}
