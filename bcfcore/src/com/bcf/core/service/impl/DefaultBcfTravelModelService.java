/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;


public class DefaultBcfTravelModelService extends DefaultModelService
{
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;

	@Override
	public <T> T clone(final T original)
	{
		final T clone = super.clone(original);
		if (!(clone instanceof AbstractOrderModel))
		{
			return clone;
		}
		else
		{
			final AbstractOrderModel clonedOrder = (AbstractOrderModel) clone;
			getBcfCloneOrderStrategy().cloneActivityOrderEntryInfo(clonedOrder);
			getBcfCloneOrderStrategy().cloneOrderRelatedAttributes(clonedOrder, (AbstractOrderModel) original);
			getBcfCloneOrderStrategy().cloneOrderTravelRelatedAttributes(clonedOrder, (AbstractOrderModel) original);

			return clone;
		}
	}

	protected BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}
}
