/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.EndOfDayReportDao;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.enums.GiftCardType;
import com.bcf.core.model.DeclareOrderEntryModel;
import com.bcf.core.service.BcfEndOfDayReportService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.EODPackageDetails;
import com.bcf.notification.request.payment.EODPaymentInfo;
import com.bcf.notification.request.payment.EODTransactionStatistics;
import com.bcf.notification.request.payment.EndOfDayReportData;


public class DefaultBcfEndOfDayReportService implements BcfEndOfDayReportService
{
	public static final String GIFT_CARD_DECLARED_TYPES = "GiftCardDeclaredTypes";
	public static final String CARD_DECLARED_TYPES = "CardDeclaredTypes";
	private static final String DECLARE_END = "DeclareEnd";
	private static final String SESSION_START = "SessionStart";
	private static final String USER_ID = "UserId";
	private static final String CASH_DECLARED_AMOUNT = "CashDeclaredAmount";
	private static final String END_OF_THE_DAY_REPORT_URL = "endOfTheDayReportUrl";
	public static final String DATE_FORMAT = "dd/MM/YYYY'T'HH:mm:ss";
	public static final String SUPPORTED_CARD_TYPES = "supportedCardTypes";
	EndOfDayReportDao endOfDayReportDao;
	private NotificationEngineRestService notificationEngineRestService;
	private Map<String, String> orderTypeDeclareMap;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForAgent(final UserModel agent,
			final Date declareStartTime,
			final Date declareEndTime)
	{
		return getEndOfDayReportDao().findPaymentTransactionEntriesForAgent(agent, declareStartTime, declareEndTime);
	}

	@Override
	public List<DeclareOrderEntryModel> findDeclareOrderEntriesForAgent(final UserModel agent, final Date declareStartTime,
			final Date declareEndTime)
	{
		return getEndOfDayReportDao().findDeclareOrderEntriesForAgent(agent, declareStartTime, declareEndTime);
	}

	@Override
	public List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForWebJob(final Date endofdayDeclareStartTime,
			final Date endofdayDeclareEndTime)
	{
		return getEndOfDayReportDao().findPaymentTransactionEntriesForWebJob(endofdayDeclareStartTime, endofdayDeclareEndTime);
	}


	@Override
	public List<DeclareOrderEntryModel> findDeclareOrderEntriesForWebJob(final Date endofdayDeclareStartTime,
			final Date endofdayDeclareEndTime)
	{
		return getEndOfDayReportDao().findDeclareOrderEntriesForWebJob(endofdayDeclareStartTime, endofdayDeclareEndTime);
	}

	protected EndOfDayReportDao getEndOfDayReportDao()
	{
		return endOfDayReportDao;
	}

	@Required
	public void setEndOfDayReportDao(final EndOfDayReportDao endOfDayReportDao)
	{
		this.endOfDayReportDao = endOfDayReportDao;
	}

	@Override
	public void getEndOfDayReportDataForWeb(final Date endofdayDeclareStartTime, final Date endofdayDeclareEndTime,
			final Integer currencyRoundingDigits) throws IntegrationException
	{
		final List<PaymentTransactionEntryModel> paymentTransactionEntryModel = findPaymentTransactionEntriesForWebJob(
				endofdayDeclareStartTime, endofdayDeclareEndTime);
		final List<DeclareOrderEntryModel> declareOrderEntries = findDeclareOrderEntriesForWebJob(endofdayDeclareStartTime,
				endofdayDeclareEndTime);

		final Map<String, Object> agentDeclareDataMap = new HashMap<>();
		agentDeclareDataMap.put(SESSION_START, endofdayDeclareStartTime);
		agentDeclareDataMap.put(DECLARE_END, endofdayDeclareEndTime);

		final EndOfDayReportData endOfDayReportData = populateEndOfDayReportData(paymentTransactionEntryModel, declareOrderEntries,
				agentDeclareDataMap, currencyRoundingDigits);

		submitEndOfDayReport(endOfDayReportData);
	}

	@Override
	public EndOfDayReportData getEndOfDayReportDataForAgent(final UserModel agent,
			final Map<String, Object> agentDeclareDataMap, final Integer currencyRoundingDigits)
	{
		final Date declareStartTime = (Date) agentDeclareDataMap.get(SESSION_START);
		final Date declareEndTime = (Date) agentDeclareDataMap.get(DECLARE_END);

		final List<PaymentTransactionEntryModel> paymentTransactionEntryModel = findPaymentTransactionEntriesForAgent(agent,
				declareStartTime, declareEndTime);
		final List<DeclareOrderEntryModel> declareOrderEntries = findDeclareOrderEntriesForAgent(agent, declareStartTime,
				declareEndTime);

		return populateEndOfDayReportData(paymentTransactionEntryModel, declareOrderEntries, agentDeclareDataMap,
				currencyRoundingDigits);
	}

	private EndOfDayReportData populateEndOfDayReportData(final List<PaymentTransactionEntryModel> paymentTransactionEntryModel,
			final List<DeclareOrderEntryModel> declareOrderEntries, final Map<String, Object> agentDeclareDataMap,
			final Integer currencyRoundingDigits)
	{
		final EndOfDayReportData endOfDayReportData = createEndOfDayReportData(paymentTransactionEntryModel, declareOrderEntries
				, agentDeclareDataMap, currencyRoundingDigits);
		return endOfDayReportData;
	}

	@Override
	public void submitEndOfDayReport(final EndOfDayReportData endOfDayReportData) throws IntegrationException
	{
		getNotificationEngineRestService()
				.sendRestRequest(endOfDayReportData, BaseResponseDTO.class, HttpMethod.POST,
						END_OF_THE_DAY_REPORT_URL);


		if (StringUtils.isNotBlank(endOfDayReportData.getEmployeeID()))
		{
			final EmployeeModel agent = (EmployeeModel) userService.getUserForUID(endOfDayReportData.getEmployeeID());
			if (Objects.nonNull(agent))
			{
				agent.setEndofdayDeclareTime(new Date());
				modelService.save(agent);
			}
		}
	}

	private EndOfDayReportData createEndOfDayReportData(final List<PaymentTransactionEntryModel> paymentTransactionEntryModels,
			final List<DeclareOrderEntryModel> declareOrderEntries, final Map<String, Object> agentDeclareDataMap,
			final Integer currencyRoundingDigits)
	{
		final EndOfDayReportData endOfDayReportData = new EndOfDayReportData();

		if (!CollectionUtils.sizeIsEmpty(agentDeclareDataMap))
		{
			endOfDayReportData.setEmployeeID(String.valueOf(agentDeclareDataMap.get(USER_ID)));

			final Date sessionStart = (Date) agentDeclareDataMap.get(SESSION_START);
			final Date sessionEnd = (Date) agentDeclareDataMap.get(DECLARE_END);
			final String declareStart = new SimpleDateFormat(DATE_FORMAT).format(sessionStart);
			final String declareEnd = new SimpleDateFormat(DATE_FORMAT).format(sessionEnd);

			endOfDayReportData.setSessionStart(declareStart);
			endOfDayReportData.setSessionEnd(declareEnd);
		}

		if (CollectionUtils.isNotEmpty(paymentTransactionEntryModels))
		{
			populatePackageDetails(paymentTransactionEntryModels, endOfDayReportData, currencyRoundingDigits);
			populateTransactionStatistics(paymentTransactionEntryModels, endOfDayReportData, currencyRoundingDigits);
		}

		populatePaymentInfo(paymentTransactionEntryModels, endOfDayReportData, agentDeclareDataMap, currencyRoundingDigits);

		final Map<String, Object> totalDeclareOrderByType = new HashMap<String, Object>();

		BigDecimal totalDepositValue = BigDecimal.ZERO;
		if (!CollectionUtils.isEmpty(paymentTransactionEntryModels))
		{
			final BigDecimal totalDeposit = paymentTransactionEntryModels.stream()
					.filter(transactionEntryModel -> transactionEntryModel.isDepositPayment()).
							map(PaymentTransactionEntryModel::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

			if (totalDeposit.compareTo(totalDepositValue) > 0)
			{
				totalDepositValue = totalDeposit.negate();
			}

			final OrderModel orderModel = (OrderModel) paymentTransactionEntryModels.get(0).getPaymentTransaction().getOrder();

			endOfDayReportData.setBookingChannel(orderModel.getSalesApplication().getCode());
		}

		totalDepositValue = totalDepositValue.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
		totalDeclareOrderByType.put(getOrderTypeDeclareMap().get(BcfCoreConstants.DECLARE_DEPOSIT), totalDepositValue);

		if (!CollectionUtils.isEmpty(declareOrderEntries))
		{
			populateDeclareAmount(declareOrderEntries, endOfDayReportData, totalDeclareOrderByType, currencyRoundingDigits);
		}

		endOfDayReportData.setAmountDeclared(totalDeclareOrderByType);

		return endOfDayReportData;
	}

	private void populatePackageDetails(final List<PaymentTransactionEntryModel> paymentTransactionEntryModels,
			final EndOfDayReportData endOfDayReportData, final Integer currencyRoundingDigits)
	{

		final Map<PaymentTransactionType, List<PaymentTransactionEntryModel>> entriesGroupByTransactionType =
				paymentTransactionEntryModels.stream()
						.collect(Collectors.groupingBy(PaymentTransactionEntryModel::getType));

		final List<EODPackageDetails> packageDetailsList = new ArrayList<EODPackageDetails>();

		for (final Map.Entry<PaymentTransactionType, List<PaymentTransactionEntryModel>> entriesByTransactionType : entriesGroupByTransactionType
				.entrySet())
		{
			final BigDecimal totalAmount = entriesByTransactionType.getValue().stream().map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add).setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

			final EODPackageDetails packageDetails = new EODPackageDetails();

			packageDetails.setPaymentType(entriesByTransactionType.getKey().getCode());
			packageDetails.setTotalTransactions(entriesByTransactionType.getValue().size());
			packageDetails.setTotalAmount(totalAmount);
			packageDetailsList.add(packageDetails);
		}
		endOfDayReportData.setPackageDetails(packageDetailsList);
	}

	private void populateTransactionStatistics(final List<PaymentTransactionEntryModel> paymentTransactionEntryModels,
			final EndOfDayReportData endOfDayReportData, final Integer currencyRoundingDigits)
	{

		final PaymentTransactionEntryModel highestSaleEntry
				= paymentTransactionEntryModels.stream().max(Comparator.comparing(PaymentTransactionEntryModel::getAmount)).get();

		BigDecimal highestSale = highestSaleEntry.getAmount();
		highestSale = highestSale.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

		final PaymentTransactionEntryModel lowestSaleEntry
				= paymentTransactionEntryModels.stream().min(Comparator.comparing(PaymentTransactionEntryModel::getAmount)).get();

		BigDecimal lowestSale = lowestSaleEntry.getAmount();
		lowestSale = lowestSale.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

		final BigDecimal transactionTotal = paymentTransactionEntryModels.stream().map(PaymentTransactionEntryModel::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		final int totalTransactions = paymentTransactionEntryModels.size();
		BigDecimal averageSale = transactionTotal.divide(new BigDecimal(totalTransactions), currencyRoundingDigits);
		averageSale = averageSale.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

		final EODTransactionStatistics transactionStatistics = new EODTransactionStatistics();
		transactionStatistics.setHighestSale(highestSale);
		transactionStatistics.setLowestSale(lowestSale);
		transactionStatistics.setAverageSale(averageSale);
		endOfDayReportData.setTransactionStatistics(transactionStatistics);
	}

	private void populatePaymentInfo(final List<PaymentTransactionEntryModel> paymentTransactionEntryModels,
			final EndOfDayReportData endOfDayReportData, final Map<String, Object> agentDeclareDataMap,
			final Integer currencyRoundingDigits)
	{
		final List<EODPaymentInfo> paymentInfoList = new ArrayList<EODPaymentInfo>();

		if (!CollectionUtils.isEmpty(paymentTransactionEntryModels))
		{
			final Map<BCFPaymentMethodType, List<PaymentTransactionEntryModel>> entriesGroupByType = paymentTransactionEntryModels
					.stream().collect(Collectors.groupingBy(PaymentTransactionEntryModel::getTransactionType));

			final Map<String, List<PaymentTransactionEntryModel>> giftCardEntries = new HashMap<String, List<PaymentTransactionEntryModel>>();
			final Map<String, List<PaymentTransactionEntryModel>> cardEntries = new HashMap<String, List<PaymentTransactionEntryModel>>();

			for (final Map.Entry<BCFPaymentMethodType, List<PaymentTransactionEntryModel>> entriesByType : entriesGroupByType
					.entrySet())
			{
				if (BCFPaymentMethodType.GIFTCARD.equals(entriesByType.getKey()))
				{
					for (final PaymentTransactionEntryModel giftCardPaymentTransactionEntryModel : entriesByType.getValue())
					{
						if (Objects.nonNull(giftCardPaymentTransactionEntryModel.getPaymentTransaction()) && Objects
								.nonNull(giftCardPaymentTransactionEntryModel.getPaymentTransaction().getInfo()))
						{
							final GiftCardType giftCardType = ((GiftCardPaymentInfoModel) giftCardPaymentTransactionEntryModel
									.getPaymentTransaction().getInfo()).getGiftCardType();

							List<PaymentTransactionEntryModel> giftCardTypeEntries = new ArrayList<PaymentTransactionEntryModel>();

							final String code = giftCardType.getCode();
							if (giftCardEntries.containsKey(code))
							{
								giftCardTypeEntries = giftCardEntries.get(code);
							}
							giftCardTypeEntries.add(giftCardPaymentTransactionEntryModel);
							giftCardEntries.put(code, giftCardTypeEntries);
						}
					}
				}
				else if (BCFPaymentMethodType.CARD.equals(entriesByType.getKey()))
				{
					for (final PaymentTransactionEntryModel cardPaymentTransactionEntryModel : entriesByType.getValue())
					{
						if (Objects.nonNull(cardPaymentTransactionEntryModel.getPaymentTransaction()) && Objects
								.nonNull(cardPaymentTransactionEntryModel.getPaymentTransaction().getInfo()))
						{
							final CreditCardType creditCardType = ((CreditCardPaymentInfoModel) cardPaymentTransactionEntryModel
									.getPaymentTransaction().getInfo()).getType();
							final String cardTypeCode = creditCardType.getCode().toLowerCase();

							List<PaymentTransactionEntryModel> cardTypeEntries = new ArrayList<PaymentTransactionEntryModel>();
							if (cardEntries.containsKey(cardTypeCode))
							{
								cardTypeEntries = cardEntries.get(cardTypeCode);
							}
							cardTypeEntries.add(cardPaymentTransactionEntryModel);
							cardEntries.put(cardTypeCode, cardTypeEntries);
						}
					}
				}
				else
				{
					populateEntryDetails(endOfDayReportData, paymentInfoList, entriesByType, agentDeclareDataMap,
							currencyRoundingDigits);
				}
			}

			populateCardEntryDetails(paymentInfoList, cardEntries, agentDeclareDataMap,
					currencyRoundingDigits);

			populateGiftCardEntryDetails(paymentInfoList, giftCardEntries, agentDeclareDataMap,
					currencyRoundingDigits);
		}
		endOfDayReportData.setPaymentTransactions(paymentInfoList);
	}

	private void populateCardEntryDetails(final List<EODPaymentInfo> paymentInfoList,
			final Map<String, List<PaymentTransactionEntryModel>> entriesByType, final Map<String, Object> agentDeclareDataMap,
			final Integer currencyRoundingDigits)
	{
		final List<String> allowedCardTypes = BCFPropertiesUtils
				.convertToList(
						bcfConfigurablePropertiesService.getBcfPropertyValue(SUPPORTED_CARD_TYPES));

		final Map<String, String> creditCardTypeMap = new TreeMap<String, String>();
		enumerationService.getEnumerationValues(CreditCardType.class).stream()
				.filter(creditCardType -> allowedCardTypes.contains(creditCardType.getCode())).forEach(
				creditCardType -> {
					creditCardTypeMap
							.put(creditCardType.getCode().toLowerCase(), enumerationService.getEnumerationName(creditCardType));
				}
		);

		final Map<String, BigDecimal> cardDeclaredTypes = (Map<String, BigDecimal>) agentDeclareDataMap
				.get(CARD_DECLARED_TYPES);

		for (final Map.Entry<String, List<PaymentTransactionEntryModel>> cardEntries : entriesByType.entrySet())
		{
			final BigDecimal totalPurchaseByType = cardEntries.getValue().stream()
					.filter(transactionEntryModel -> !PaymentTransactionType.REFUND.equals(transactionEntryModel.getType()))
					.map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			final BigDecimal totalRefundByType = cardEntries.getValue().stream()
					.filter(transactionEntryModel -> PaymentTransactionType.REFUND.equals(transactionEntryModel.getType()))
					.map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			final BigDecimal totalByType = totalPurchaseByType.subtract(totalRefundByType);

			final EODPaymentInfo reportPaymentInfo = new EODPaymentInfo();
			final String itemType = cardEntries.getKey();
			reportPaymentInfo.setTenderType(creditCardTypeMap.get(itemType));
			reportPaymentInfo.setSystemAmount(totalByType.setScale(currencyRoundingDigits, RoundingMode.HALF_UP));

			if (Objects.nonNull(cardEntries.getValue()))
			{
				reportPaymentInfo.setTotalTransactions(cardEntries.getValue().size());
			}

			BigDecimal convertedCartAmountBD = BigDecimal.ZERO;
			if (Objects.nonNull(cardDeclaredTypes) && Objects.nonNull(cardDeclaredTypes.get(itemType.toLowerCase())))
			{
				final BigDecimal cartAmount = cardDeclaredTypes.get(itemType.toLowerCase());
				convertedCartAmountBD = cartAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
			}

			reportPaymentInfo.setDeclaredAmount(convertedCartAmountBD);

			BigDecimal varianceAmount = BigDecimal.ZERO;
			if (Objects.nonNull(convertedCartAmountBD) && Objects.nonNull(totalByType))
			{
				varianceAmount = totalByType.subtract(convertedCartAmountBD);
				if (varianceAmount.compareTo(BigDecimal.ZERO) == 0)
					varianceAmount = BigDecimal.valueOf(0.00);

				varianceAmount = varianceAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

				reportPaymentInfo.setVarianceAmount(varianceAmount);
			}

			paymentInfoList.add(reportPaymentInfo);
			if (Objects.nonNull(cardDeclaredTypes))
			{
				cardDeclaredTypes.remove(itemType);
			}
		}

		if (Objects.nonNull(cardDeclaredTypes))
		{
			cardDeclaredTypes.entrySet().forEach(cardType -> {
				final EODPaymentInfo reportPaymentInfo = new EODPaymentInfo();
				reportPaymentInfo.setTenderType(creditCardTypeMap.get(cardType.getKey()));
				final BigDecimal declaredAmount = cardType.getValue().setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
				final BigDecimal systemAmount = BigDecimal.ZERO;
				reportPaymentInfo.setDeclaredAmount(declaredAmount);
				reportPaymentInfo.setSystemAmount(systemAmount);
				reportPaymentInfo.setVarianceAmount(systemAmount.subtract(declaredAmount));
				reportPaymentInfo.setTotalTransactions(0);
				paymentInfoList.add(reportPaymentInfo);
			});
		}
	}

	private void populateGiftCardEntryDetails(final List<EODPaymentInfo> paymentInfoList,
			final Map<String, List<PaymentTransactionEntryModel>> entriesByType, final Map<String, Object> agentDeclareDataMap,
			final Integer currencyRoundingDigits)
	{
		final Map<String, String> giftCardTypeMap = new TreeMap<String, String>();
		enumerationService.getEnumerationValues(GiftCardType.class).stream().forEach(
				giftCardType -> {
					giftCardTypeMap.put(giftCardType.getCode(), enumerationService.getEnumerationName(giftCardType));
				}
		);

		final Map<String, BigDecimal> giftCardDeclaredTypes = (Map<String, BigDecimal>) agentDeclareDataMap
				.get(GIFT_CARD_DECLARED_TYPES);

		for (final Map.Entry<String, List<PaymentTransactionEntryModel>> giftCardEntries : entriesByType.entrySet())
		{
			final BigDecimal totalPurchaseByType = giftCardEntries.getValue().stream()
					.filter(transactionEntryModel -> !PaymentTransactionType.REFUND.equals(transactionEntryModel.getType()))
					.map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			final BigDecimal totalRefundByType = giftCardEntries.getValue().stream()
					.filter(transactionEntryModel -> PaymentTransactionType.REFUND.equals(transactionEntryModel.getType()))
					.map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			final BigDecimal totalByType = totalPurchaseByType.subtract(totalRefundByType);

			final EODPaymentInfo reportPaymentInfo = new EODPaymentInfo();
			final String itemType = giftCardEntries.getKey();
			reportPaymentInfo.setTenderType(giftCardTypeMap.get(itemType));
			reportPaymentInfo.setSystemAmount(totalByType.setScale(currencyRoundingDigits, RoundingMode.HALF_UP));

			if (Objects.nonNull(giftCardEntries.getValue()))
			{
				reportPaymentInfo.setTotalTransactions(giftCardEntries.getValue().size());
			}

			BigDecimal convertedGiftCartAmountBD = BigDecimal.ZERO;
			if (Objects.nonNull(giftCardDeclaredTypes) && Objects.nonNull(giftCardDeclaredTypes.get(itemType.toLowerCase())))
			{
				final BigDecimal giftCartAmount = giftCardDeclaredTypes.get(itemType.toLowerCase());
				convertedGiftCartAmountBD = giftCartAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
			}

			reportPaymentInfo.setDeclaredAmount(convertedGiftCartAmountBD);

			BigDecimal varianceAmount = BigDecimal.ZERO;
			if (Objects.nonNull(convertedGiftCartAmountBD) && Objects.nonNull(totalByType))
			{
				varianceAmount = totalByType.subtract(convertedGiftCartAmountBD);
				if (varianceAmount.compareTo(BigDecimal.ZERO) == 0)
					varianceAmount = BigDecimal.valueOf(0.00);

				varianceAmount = varianceAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

				reportPaymentInfo.setVarianceAmount(varianceAmount);
			}

			paymentInfoList.add(reportPaymentInfo);
			if (Objects.nonNull(giftCardDeclaredTypes))
			{
				giftCardDeclaredTypes.remove(itemType);
			}
		}

		if (Objects.nonNull(giftCardDeclaredTypes))
		{
			giftCardDeclaredTypes.entrySet().forEach(giftCardType -> {
				final EODPaymentInfo reportPaymentInfo = new EODPaymentInfo();
				reportPaymentInfo.setTenderType(giftCardTypeMap.get(giftCardType.getKey()));
				final BigDecimal declaredAmount = giftCardType.getValue().setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
				final BigDecimal systemAmount = BigDecimal.ZERO;
				reportPaymentInfo.setDeclaredAmount(declaredAmount);
				reportPaymentInfo.setSystemAmount(systemAmount);
				reportPaymentInfo.setVarianceAmount(systemAmount.subtract(declaredAmount));
				reportPaymentInfo.setTotalTransactions(0);
				paymentInfoList.add(reportPaymentInfo);
			});
		}
	}

	private void populateEntryDetails(final EndOfDayReportData endOfDayReportData,
			final List<EODPaymentInfo> paymentInfoList,
			final Map.Entry<BCFPaymentMethodType, List<PaymentTransactionEntryModel>> entriesByType,
			final Map<String, Object> agentDeclareDataMap, final Integer currencyRoundingDigits)
	{
		final BigDecimal totalByType = entriesByType.getValue().stream().map(PaymentTransactionEntryModel::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		final EODPaymentInfo reportPaymentInfo = new EODPaymentInfo();
		final BCFPaymentMethodType itemType = entriesByType.getKey();
		reportPaymentInfo.setTenderType(itemType.getCode());
		reportPaymentInfo.setSystemAmount(totalByType.setScale(currencyRoundingDigits, RoundingMode.HALF_UP));

		if (Objects.nonNull(entriesByType.getValue()))
		{
			reportPaymentInfo.setTotalTransactions(entriesByType.getValue().size());
		}

		final BigDecimal cashDeclaredAmount = (BigDecimal) agentDeclareDataMap.get(CASH_DECLARED_AMOUNT);
		if (Objects.nonNull(cashDeclaredAmount))
		{
			final BigDecimal cashDeclaredAmountBD = cashDeclaredAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);
			reportPaymentInfo.setDeclaredAmount(cashDeclaredAmountBD);
		}

		BigDecimal varianceAmount = BigDecimal.ZERO;
		if (Objects.nonNull(cashDeclaredAmount) && Objects.nonNull(totalByType))
		{
			varianceAmount = totalByType.subtract(cashDeclaredAmount);

			if (varianceAmount.compareTo(BigDecimal.ZERO) == 0)
				varianceAmount = BigDecimal.valueOf(0.00);

			varianceAmount = varianceAmount.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

			reportPaymentInfo.setVarianceAmount(varianceAmount);
		}

		paymentInfoList.add(reportPaymentInfo);
	}

	private void populateDeclareAmount(final List<DeclareOrderEntryModel> declareOrderEntries,
			final EndOfDayReportData endOfDayReportData, final Map<String, Object> totalDeclareOrderByType,
			final Integer currencyRoundingDigits)
	{
		final Map<String, List<DeclareOrderEntryModel>> declareOrderEntriesGroupByType = declareOrderEntries
				.stream().collect(Collectors.groupingBy(DeclareOrderEntryModel::getComponentType));
		final Map<String, BigDecimal> ferryMap = new HashMap<String, BigDecimal>();
		for (final Map.Entry<String, List<DeclareOrderEntryModel>> declareOrderEntriesByType : declareOrderEntriesGroupByType
				.entrySet())
		{
			final String orderEntryType = declareOrderEntriesByType.getKey();
			String componentEntryType = orderEntryType;

			if (getOrderTypeDeclareMap().containsKey(orderEntryType))
			{
				componentEntryType = getOrderTypeDeclareMap().get(orderEntryType);
			}

			final Double totalByType = declareOrderEntriesByType.getValue().stream()
					.mapToDouble(DeclareOrderEntryModel::getTotalPrice)
					.sum();

			final BigDecimal convertedTotalByTypeBD = new BigDecimal(totalByType)
					.setScale(currencyRoundingDigits, RoundingMode.HALF_UP);

			if (orderEntryType.equals(BcfCoreConstants.RESERVERD_SAILING) || orderEntryType.equals(BcfCoreConstants.OPEN_TICKET))
			{
				ferryMap.put(componentEntryType, convertedTotalByTypeBD);
			}
			else
			{
				totalDeclareOrderByType.put(componentEntryType, convertedTotalByTypeBD);
			}
		}

		totalDeclareOrderByType.put(getOrderTypeDeclareMap().get(OrderEntryType.TRANSPORT.getCode()), ferryMap);
	}

	public Map<String, String> getOrderTypeDeclareMap()
	{
		return orderTypeDeclareMap;
	}

	public void setOrderTypeDeclareMap(final Map<String, String> orderTypeDeclareMap)
	{
		this.orderTypeDeclareMap = orderTypeDeclareMap;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}
