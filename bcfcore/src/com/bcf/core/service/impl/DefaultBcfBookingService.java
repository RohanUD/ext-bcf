/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.impl.DefaultBookingService;
import de.hybris.platform.travelservices.strategies.stock.TravelManageStockByEntryTypeStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfAccommodationCommerceCartService;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.order.strategies.update.BcfUpdateOrderFromEBookingStrategy;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.data.SearchBookingResponseDTO;


public class DefaultBcfBookingService extends DefaultBookingService implements BcfBookingService
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfBookingService.class);
	private CalculationService calculationService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private BusinessProcessService businessProcessService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private BcfAccommodationCommerceCartService accommodationCommerceCartService;
	private OrderService orderService;
	private ProductService productService;
	private TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private CartService cartService;
	private static final String NORTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT = "NPXF";
	private static final String SOUTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT = "SPXF";
	private static final String COMPONENT_CANCELLATION_FEE = "NRXF";

	@Override
	public AbstractOrderEntryModel getExistingAncillaryOrderEntry(final AbstractOrderModel abstractOrderModel, final String productCode,
			final String travelRouteCode,
			final List<String> transportOfferingCodes, final List<String> travellerCodes, final int journeyRefNumber, final int odRefNumber)
	{
		final Optional<AbstractOrderEntryModel> existingOrderEntryOptional =  abstractOrderModel.getEntries().stream().filter(entry -> entry.getActive() && entry.getBundleNo() == 0
				&& productCode.equals(entry.getProduct().getCode())
				&& StringUtils.isNotBlank(travelRouteCode)
				&& entry.getJourneyReferenceNumber() == journeyRefNumber
				&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute())
				&& travelRouteCode.equals(entry.getTravelOrderEntryInfo().getTravelRoute().getCode())
				&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNumber
				&& CollectionUtils.isNotEmpty(transportOfferingCodes)
				&& CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTransportOfferings())
				&& entry.getTravelOrderEntryInfo().getTransportOfferings().stream().map(TransportOfferingModel::getCode).collect(Collectors.toList()).containsAll(transportOfferingCodes)).findFirst();

		if(existingOrderEntryOptional.isPresent())
		{
			return existingOrderEntryOptional.get();
		}
		return null;
	}

	@Override
	public AbstractOrderEntryModel getOrderEntry(final AbstractOrderModel abstractOrderModel, final String productCode,
			final String travelRouteCode,
			final List<String> transportOfferingCodes, final List<String> travellerCodes, final boolean bundleNoCheckRequired)
	{
		final Optional<AbstractOrderEntryModel> existingOrderEntryOptional =  abstractOrderModel.getEntries().stream().filter(entry -> entry.getBundleNo() == 0
				&& productCode.equals(entry.getProduct().getCode())
				&& StringUtils.isNotBlank(travelRouteCode)
				&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute())
				&& travelRouteCode.equals(entry.getTravelOrderEntryInfo().getTravelRoute().getCode())
				&& CollectionUtils.isNotEmpty(transportOfferingCodes)
				&& CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTransportOfferings())
				&& entry.getTravelOrderEntryInfo().getTransportOfferings().stream().map(TransportOfferingModel::getCode).collect(Collectors.toList()).containsAll(transportOfferingCodes)).findFirst();

		if(existingOrderEntryOptional.isPresent())
		{
			return existingOrderEntryOptional.get();
		}
		return null;
	}

	@Override
	public boolean cancelPartialOrder(final OrderModel order, final Set<AbstractOrderEntryModel> orderEntriesToRemove)
	{
		if (CollectionUtils.isEmpty(orderEntriesToRemove))
		{
			return true;
		}

		final OrderModel snapshot = createHistorySnapshot(order);
		final OrderHistoryEntryModel orderHistoryEntry = getBcfCloneOrderStrategy().createNewOrderHistoryEntry(snapshot, order);
		getModelService().save(orderHistoryEntry);

		orderEntriesToRemove.forEach(entry -> {
			entry.setActive(Boolean.FALSE);
			entry.setAmendStatus(AmendStatus.CHANGED);
		});

		getModelService().saveAll(orderEntriesToRemove);
		getModelService().refresh(order);
		try
		{
			calculateOrderTotal(order);
		}
		catch (final CalculationException e)
		{
			removeOrder(snapshot, orderHistoryEntry);
			LOG.error(e.getMessage(), e);
			return false;
		}
		startRefundOrderProcess(order);
		return true;
	}

	@Override
	public OrderModel createHistorySnapshot(final OrderModel originalOrder)
	{
		final OrderModel snapshot = getBcfCloneOrderStrategy().createHistorySnapshot(originalOrder);
		originalOrder.setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel());
		originalOrder.setUser(getUserService().getCurrentUser());
		getModelService().save(originalOrder);
		return snapshot;
	}

	@Override
	public OrderModel getOrderModelFromStore(final String bookingReference)
	{
		final OrderModel orderModel = getOrder(bookingReference);
		if (Objects.isNull(orderModel))
		{
			return orderModel;
		}

		return orderModel;
	}

	/**
	 * Calculates the order total.
	 *
	 * @param amendmentOrder the amendment order
	 * @throws CalculationException the calculation exception
	 */
	protected void calculateOrderTotal(final OrderModel amendmentOrder) throws CalculationException
	{
		getCalculationService().calculate(amendmentOrder);
	}

	protected void removeOrder(final OrderModel order, final OrderHistoryEntryModel newOrderHistoryEntry)
	{
		if (!getModelService().isNew(order))
		{
			getModelService().refresh(order);
			if (checkIfAnyOrderEntryByType(order, OrderEntryType.TRANSPORT))
			{
				final Set<AbstractOrderEntryModel> transportOrderEntries = order.getEntries().stream()
						.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())).collect(Collectors.toSet());
				getBcfTravelCartService().removeEntriesForTransport(transportOrderEntries);
			}

			if (checkIfAnyOrderEntryByType(order, OrderEntryType.ACCOMMODATION))
			{
				getBcfTravelCartService().removeEntriesForAccommodation(order);
			}
			if (CollectionUtils.isNotEmpty(order.getHistoryEntries()))
			{
				order.getHistoryEntries().forEach(orderHistoryEntry -> getModelService().remove(orderHistoryEntry));
			}
			getModelService().remove(newOrderHistoryEntry);
			getModelService().remove(order);
		}
	}

	@Override
	public void startCancelOrderEmailProcess(final OrderModel order)
	{
		final OrderProcessModel orderProcessModel = getBusinessProcessService()
				.createProcess("cancel-order-process-" + order.getCode(), "cancel-order-process");
		orderProcessModel.setOrder(order);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	/**
	 * Start refund order process.
	 *
	 * @param order the order
	 */
	@Override
	public void startRefundOrderProcess(final OrderModel order)
	{
		final OrderProcessModel orderProcessModel = getBusinessProcessService()
				.createProcess("orderRefundProcess-" + order.getCode() + "-" + System.currentTimeMillis(), "orderRefundProcess");
		orderProcessModel.setOrder(order);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	@Override
	public BigDecimal getOrderTotalPriceByJourney(final OrderModel order, final int journeyRefNum, final int odRefNum)
	{
		final List<AbstractOrderEntryModel> orderEntries = order.getEntries().stream()
				.filter(entry -> entry.getActive() && Objects.equals(OrderEntryType.TRANSPORT, entry.getType())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo()) && entry.getJourneyReferenceNumber() == journeyRefNum
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNum)
				.collect(Collectors.toList());

		return BigDecimal.valueOf(getTotalPriceForEntries(orderEntries));
	}

	@Override
	public Double getTotalPriceForRemovedTransportEntries(final OrderModel order)
	{
		final List<AbstractOrderEntryModel> orderEntries = order
				.getEntries().stream().filter(entry -> Objects.equals(OrderEntryType.TRANSPORT, entry.getType())
						&& BooleanUtils.isFalse(entry.getActive()) && Objects.equals(AmendStatus.CHANGED, entry.getAmendStatus()))
				.collect(Collectors.toList());

		return getTotalPriceForEntries(orderEntries);
	}

	/**
	 * Gets the total price for entries.
	 *
	 * @param orderEntries the order entries
	 * @return the total price for entries
	 */
	protected Double getTotalPriceForEntries(final List<AbstractOrderEntryModel> orderEntries)
	{
		Double totalPrice = 0d;

		for (final AbstractOrderEntryModel orderEntry : orderEntries)
		{
			totalPrice += orderEntry.getTotalPrice();
			final Optional<Double> totalTaxPrice = orderEntry.getTaxValues().stream()
					.map(taxValue -> Double.valueOf(taxValue.getAppliedValue())).reduce(Double::sum);
			if (totalTaxPrice.isPresent())
			{
				totalPrice += totalTaxPrice.get();
			}
		}
		return totalPrice;
	}

	@Override
	public void removeEntriesForDeal(final List<DealOrderEntryGroupModel> dealOrderEntryGroupModelList, final boolean releaseStock)
			throws ModelRemovalException
	{
		if (CollectionUtils.isEmpty(dealOrderEntryGroupModelList))
		{
			return;
		}
		final List<AbstractOrderEntryModel> dealEntries = dealOrderEntryGroupModelList.stream()
				.flatMap(model -> model.getEntries().stream()).collect(
						Collectors.toList());

		if(releaseStock) {

			bcfTravelCommerceStockService.releaseStocks(dealEntries);
		}

		if (CollectionUtils.isNotEmpty(dealEntries))
		{
			final List<AbstractOrderEntryModel> transportEntries = dealEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())).collect(
							Collectors.toList());
			final List<AbstractOrderEntryModel> accommodationEntries = dealEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getAccommodationOrderEntryInfo())).collect(
							Collectors.toList());
			final List<AbstractOrderEntryModel> activityEntries = dealEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getActivityOrderEntryInfo()) && entry.getType()
							.equals(OrderEntryType.ACTIVITY)).collect(
							Collectors.toList());
			getBcfTravelCommerceCartService().removeAllTravelOrderEntries(transportEntries);
			getAccommodationCommerceCartService().removeAccommodationOrderEntriesForDeal(accommodationEntries);
			getBcfTravelCartService().removeActivityOrderEntries(activityEntries,false);
			getModelService().removeAll(dealOrderEntryGroupModelList);
			getModelService().removeAll(dealEntries);
		}
	}

	@Override
	public List<AbstractOrderEntryGroupModel> getOrderEntryGroups(final AbstractOrderModel abstractOrder)
	{
		return StreamUtil.safeStream(abstractOrder.getEntries()).filter(entry->entry.getActive())
				.map(AbstractOrderEntryModel::getEntryGroup).distinct()
				.collect(Collectors.toList());
	}


	@Override
	public List<DealOrderEntryGroupModel> getDealOrderEntryGroups(final AbstractOrderModel abstractOrder)
	{
		return StreamUtil.safeStream(getOrderEntryGroups(abstractOrder))
				.filter(entry -> entry instanceof DealOrderEntryGroupModel && ((DealOrderEntryGroupModel) entry).getActive())
				.map(DealOrderEntryGroupModel.class::cast)
				.collect(Collectors.toList());
	}

	@Override
	public List<DealOrderEntryGroupModel> getDealOrderEntryGroups(
			final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups)
	{
		return CollectionUtils.isEmpty(abstractOrderEntryGroups) ?
				Collections.emptyList() :
				abstractOrderEntryGroups.stream().filter(entry -> entry instanceof DealOrderEntryGroupModel && ((DealOrderEntryGroupModel) entry).getActive())
						.map(DealOrderEntryGroupModel.class::cast).collect(Collectors.toList());
	}

	@Override
	public List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(
			final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups)
	{
		return CollectionUtils.isEmpty(abstractOrderEntryGroups) ?
				Collections.emptyList() :
				abstractOrderEntryGroups.stream().filter(entry -> entry instanceof AccommodationOrderEntryGroupModel && ((AccommodationOrderEntryGroupModel) entry).getActive())
						.map(AccommodationOrderEntryGroupModel.class::cast).collect(Collectors.toList());
	}

	@Override
	public void updateOrderFromEBooking(final OrderModel order, final SearchBookingResponseDTO searchBookingResponseDTO)
			throws BcfOrderUpdateException, CalculationException//NOSONAR
	{
		if(CollectionUtils.isNotEmpty(getTransportEntries(order)))
		{
			getBcfUpdateOrderFromEBookingStrategy().updateExistingOrder(order, searchBookingResponseDTO);
		}
	}

	@Override
	public DealOrderEntryGroupModel getAccommodationDealOrderEntryGroup(final AbstractOrderModel abstractOrder, final Integer journeyReferenceNumber)
	{
		if(Objects.isNull(abstractOrder))
		{
			return null;
		}
		else
		{
			final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getDealOrderEntryGroups(abstractOrder);
			if(CollectionUtils.isEmpty(dealOrderEntryGroupModels))
			{
				return null;
			}

			final Optional<DealOrderEntryGroupModel> dealOrderEntryGroupsForSelectedJourney = dealOrderEntryGroupModels.stream().filter(dealOrderEntryGroup ->dealOrderEntryGroup.getActive() && dealOrderEntryGroup.getJourneyRefNumber()==journeyReferenceNumber).findFirst();
			if(dealOrderEntryGroupsForSelectedJourney.isPresent())
			{
				return dealOrderEntryGroupsForSelectedJourney.get();
			}
			return null;
		}
	}

	@Override
	public List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(final AbstractOrderModel abstractOrder, final Integer journeyReferenceNumber)
	{
		final List<AbstractOrderEntryModel> abstractOrderEntries = StreamUtil.safeStream(abstractOrder.getEntries()).filter(
				entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getEntryGroup() != null
						&& entry.getJourneyReferenceNumber() == journeyReferenceNumber)
				.collect(Collectors.toList());

		if(CollectionUtils.isNotEmpty(abstractOrderEntries)){

			final AbstractOrderEntryModel abstractOrderEntryModel = abstractOrderEntries.stream()
					.filter(entry -> entry.getEntryGroup() instanceof DealOrderEntryGroupModel).findAny().orElse(null);
			if(abstractOrderEntryModel!=null){
				return StreamUtil.safeStream(((DealOrderEntryGroupModel)abstractOrderEntryModel.getEntryGroup()).getAccommodationEntryGroups()).collect(
						Collectors.toList());
			}
			else{
				return abstractOrderEntries.stream().map(entry->(AccommodationOrderEntryGroupModel)entry.getEntryGroup()).collect(Collectors.toList());
			}
		}
		return Collections.emptyList();
	}

	@Override
	public AccommodationOrderEntryGroupModel getAccommodationOrderEntryGroup(final int roomStayRefNum,
			final AbstractOrderModel abstractOrder)
	{
		final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups = this.getAbstractOrderEntryGroupDao()
				.findAbstractOrderEntryGroups(abstractOrder);
		if (CollectionUtils.isNotEmpty(abstractOrderEntryGroups)) {
			final Optional<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroup = abstractOrderEntryGroups.stream()
					.filter((entry) -> {
				return entry instanceof AccommodationOrderEntryGroupModel  &&((AccommodationOrderEntryGroupModel)entry).getRoomStayRefNumber() == roomStayRefNum;
			}).map((entry) -> {
				return (AccommodationOrderEntryGroupModel)entry;
			}).findAny();
			if (accommodationOrderEntryGroup.isPresent()) {
				return accommodationOrderEntryGroup.get();
			}
		}

		return null;
	}


	@Override
	public boolean checkIfOrderEntriesExistOfType(final AbstractOrderModel abstractOrderModel,
			final OrderEntryType orderEntryType)
	{
		return abstractOrderModel.getEntries().stream().anyMatch(orderEntry ->
				Objects.equals(orderEntryType, orderEntry.getType())
		);
	}

	@Override
	public boolean checkIfActivityEntriesExist(final AbstractOrderModel abstractOrderModel)
	{
		return abstractOrderModel.getEntries().stream().anyMatch(orderEntry ->
				Objects.nonNull(orderEntry.getActivityOrderEntryInfo()) && Objects
						.equals(OrderEntryType.ACTIVITY, orderEntry.getType())
		);
	}

	@Override
	public List<AbstractOrderEntryModel> getActivityEntries(final AbstractOrderModel abstractOrderModel)
	{
		return StreamUtil.safeStream(abstractOrderModel.getEntries()).filter(orderEntry ->
				Objects.nonNull(orderEntry.getActivityOrderEntryInfo()) && Objects
						.equals(OrderEntryType.ACTIVITY, orderEntry.getType())).collect(Collectors.toList());
	}

	@Override
	public List<AbstractOrderEntryModel> getTransportEntries(final AbstractOrderModel abstractOrderModel)
	{
		return StreamUtil.safeStream(abstractOrderModel.getEntries()).filter(orderEntry ->
				 Objects.nonNull(orderEntry.getTravelOrderEntryInfo()) && Objects
						.equals(OrderEntryType.TRANSPORT, orderEntry.getType())).collect(Collectors.toList());
	}

	@Override
	public List<AbstractOrderEntryModel> getAccommodationEntries(final AbstractOrderModel abstractOrderModel)
	{
		return StreamUtil.safeStream(abstractOrderModel.getEntries()).filter(orderEntry ->
				Objects.nonNull(orderEntry.getAccommodationOrderEntryInfo()) && Objects
						.equals(OrderEntryType.ACCOMMODATION, orderEntry.getType())).collect(Collectors.toList());
	}

	@Override
	public BigDecimal getRefundAmount(final OrderModel orderModel) {
		final List<OrderEntryType> orderEntryTypeList = orderModel.getEntries().stream().filter((entry) -> {
			return entry.getActive() && !Objects.equals(entry.getAmendStatus(), AmendStatus.CHANGED)
					&& entry.getQuantity() > 0L;
		}).map((entry) -> {
			return entry.getType();
		}).distinct().collect(Collectors.toList());
		return orderEntryTypeList.stream().map((orderEntryType) -> {
			return Objects.nonNull(getTotalRefundCalculationStrategyMap().get(orderEntryType)) ?
					getTotalRefundCalculationStrategyMap().get(orderEntryType)
							.getTotalToRefund(orderModel) :
					getTotalRefundCalculationStrategyMap()
							.get(OrderEntryType.DEFAULT).getTotalToRefund(orderModel);
		}).reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	@Override
	public  boolean isPaymentTransactionEntrySuccess(final PaymentTransactionEntryModel paymentTransactionEntryModel)
	{

		return getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode").equals(paymentTransactionEntryModel.getBcfResponseCode());
	}
	@Override
	public BigDecimal calculateTotalToRefund(final OrderModel orderModel)
	{

		BigDecimal totalToRefund = BigDecimal.ZERO;

		final List<PaymentTransactionModel> paymentTransactionModels = orderModel.getPaymentTransactions();
		if(CollectionUtils.isNotEmpty(paymentTransactionModels)){

			for (final PaymentTransactionModel paymentTransactionModel : paymentTransactionModels)
			{

				if(paymentTransactionModel.getInfo()!=null && paymentTransactionModel.getInfo() instanceof CreditCardPaymentInfoModel){

					final double totalPayedAmount =paymentTransactionModel.getEntries().stream()
							.filter(
									entry -> PaymentTransactionType.PURCHASE.equals(entry.getType()) && isPaymentTransactionEntrySuccess(entry))
							.mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

					totalToRefund=totalToRefund.add(BigDecimal.valueOf(totalPayedAmount));

					final double totalRefundedAmount = paymentTransactionModel.getEntries().stream()
							.filter(entry -> PaymentTransactionType.REFUND.equals(entry.getType()) && isPaymentTransactionEntrySuccess(entry))
							.mapToDouble(entry -> entry.getAmount().doubleValue()).sum();
					totalToRefund=totalToRefund.subtract(BigDecimal.valueOf(totalRefundedAmount));
				}else if (paymentTransactionModel.getInfo()!=null && (paymentTransactionModel.getInfo() instanceof GiftCardPaymentInfoModel || paymentTransactionModel.getInfo() instanceof CashPaymentInfoModel)){

					final double totalPayedAmount =paymentTransactionModel.getEntries().stream()
							.filter(
									entry -> !PaymentTransactionType.REFUND.equals(entry.getType()) && (
											BCFPaymentMethodType.CASH.equals(entry.getTransactionType()) || BCFPaymentMethodType.GIFTCARD
													.equals(entry.getTransactionType())))
							.mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

					totalToRefund=totalToRefund.add(BigDecimal.valueOf(totalPayedAmount));

					final double totalRefundedAmount = paymentTransactionModel.getEntries().stream()
							.filter(entry -> PaymentTransactionType.REFUND.equals(entry.getType()))
							.mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

					totalToRefund=totalToRefund.subtract(BigDecimal.valueOf(totalRefundedAmount));

				}else{

					final double totalRefundedAmount = paymentTransactionModel.getEntries().stream()
							.filter(entry -> PaymentTransactionType.REFUND.equals(entry.getType()))
							.mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

					totalToRefund=totalToRefund.subtract(BigDecimal.valueOf(totalRefundedAmount));
				}
			}


		}

		return totalToRefund.doubleValue() < 0.0D ? BigDecimal.ZERO : totalToRefund;
	}


	@Override
	public void cancelAccommodationAndActivityEntries(final OrderModel order,
			final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)
			throws CalculationException
	{

		deActivateVacationEntriesAndReleaseStock(order, bcfChangeCancellationVacationFeeData);

		calculationService.calculate(order);

	}

	@Override
	public void deActivateVacationEntriesAndReleaseStock(final OrderModel order,
			final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)
	{
		addAccommodationCancellationFeeProductToCart(order, bcfChangeCancellationVacationFeeData);

		final List<AbstractOrderEntryModel> activityAndAccEntries = order.getEntries().stream()
				.filter(entry -> entry.getActive() && (OrderEntryType.ACCOMMODATION.equals(entry.getType()) || OrderEntryType.ACTIVITY.equals(entry.getType())))
				.collect(Collectors.toList());

		if(CollectionUtils.isNotEmpty(activityAndAccEntries)){
			final List<ItemModel> itemsToSave = new ArrayList<>();

			activityAndAccEntries.forEach(entry->{
				if(entry.getType().getCode().equals(OrderEntryType.ACCOMMODATION))
				{
					getAccommodationManageStockStrategy().release(entry);
				}
				if(entry.getType().getCode().equals(OrderEntryType.ACTIVITY))
				{
					bcfTravelCommerceStockService.releaseActivityStock(entry);
				}
			});


			getModelService().saveAll(itemsToSave);
		}
	}

	@Override
	public void deActivateVacationEntriesAndReleaseStock(final CartModel cartModel)
	{
		final List<AbstractOrderEntryModel> activityAndAccEntries = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && (OrderEntryType.ACCOMMODATION.equals(entry.getType()) || OrderEntryType.ACTIVITY.equals(entry.getType())))
				.collect(Collectors.toList());

		if(CollectionUtils.isNotEmpty(activityAndAccEntries)){
			final List<ItemModel> itemsToSave = new ArrayList<>();

			activityAndAccEntries.forEach(entry->{
				if(entry.getType().getCode().equals(OrderEntryType.ACCOMMODATION))
				{
					getAccommodationManageStockStrategy().release(entry);
				}
				if(entry.getType().getCode().equals(OrderEntryType.ACTIVITY))
				{
					bcfTravelCommerceStockService.releaseActivityStock(entry);
				}
			});


			getModelService().saveAll(itemsToSave);
		}
	}

	@Override
	public void addAccommodationCancellationFeeProductToCart(final AbstractOrderModel order,   final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)
	{

		BigDecimal packageFee = BigDecimal.ZERO;
		BigDecimal compFee = BigDecimal.ZERO;
		final List<ItemModel> itemsToSave = new ArrayList<>();

		if(bcfChangeCancellationVacationFeeData!=null){
			packageFee = packageFee.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getPackageFees()));
			compFee = compFee.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getComponentFees()));

		}

		if (packageFee!=null && packageFee.compareTo(BigDecimal.ZERO) > 0 && bcfChangeCancellationVacationFeeData!=null)
		{
			final ProductModel product = productService.getProductForCode(bcfChangeCancellationVacationFeeData.isSouthernRoute()?SOUTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT:NORTHERN_PACKAGE_CANCELLATION_FEE_PRODUCT);
			final AbstractOrderEntryModel entry = addCancellationFeeProduct(order, packageFee, product);
			itemsToSave.add(entry);

		}

		if (compFee!=null && compFee.compareTo(BigDecimal.ZERO) > 0)
		{
			final ProductModel product = productService.getProductForCode(COMPONENT_CANCELLATION_FEE);
			final AbstractOrderEntryModel entry = addCancellationFeeProduct(order, compFee, product);
			itemsToSave.add(entry);
		}

		if(CollectionUtils.isNotEmpty(itemsToSave)){
			itemsToSave.add(order);
			getModelService().saveAll(itemsToSave);

		}

	}



	private AbstractOrderEntryModel addCancellationFeeProduct(final AbstractOrderModel order, final BigDecimal packageFee, final ProductModel product)
	{
		AbstractOrderEntryModel entry = null;
		if (order instanceof OrderModel)
		{
			entry = orderService.addNewEntry((OrderModel) order, product, 1, null);
		}
		else
		{
			entry = cartService.addNewEntry((CartModel) order, product, 1, null);
		}
		entry.setActive(true);
		entry.setType(OrderEntryType.FEE);
		entry.setBasePrice(packageFee.doubleValue());
		entry.setAmendStatus(AmendStatus.NEW);
		entry.setCalculated(false);
		return entry;
	}

	@Override
	public List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(final AbstractOrderModel abstractOrder)
	{
		if (Objects.isNull(abstractOrder)) {
			return Collections.emptyList();
		}
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new ArrayList<>();
		final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups = getAbstractOrderEntryGroupDao()
				.findAbstractOrderEntryGroups(abstractOrder);
		updateAccommodationOrderEntryGroup(accommodationOrderEntryGroupModels, abstractOrderEntryGroups);
		return accommodationOrderEntryGroupModels;
	}

	private void updateAccommodationOrderEntryGroup(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels, final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups)
	{
		if(CollectionUtils.isNotEmpty(abstractOrderEntryGroups))
		{
			for (final AbstractOrderEntryGroupModel abstractOrderEntryGroupModel : abstractOrderEntryGroups)
			{
				if (abstractOrderEntryGroupModel instanceof AccommodationOrderEntryGroupModel)
				{
					if(!((AccommodationOrderEntryGroupModel) abstractOrderEntryGroupModel).getActive()){
						continue;
					}
					accommodationOrderEntryGroupModels.add((AccommodationOrderEntryGroupModel) abstractOrderEntryGroupModel);
				}
				else if (abstractOrderEntryGroupModel instanceof DealOrderEntryGroupModel)
				{
					if(!((DealOrderEntryGroupModel) abstractOrderEntryGroupModel).getActive()){
						continue;
					}
					accommodationOrderEntryGroupModels
							.addAll(((DealOrderEntryGroupModel) abstractOrderEntryGroupModel).getAccommodationEntryGroups());
				}
			}
		}
	}

	/**
	 * @return the calculationService
	 */
	protected CalculationService getCalculationService()
	{
		return calculationService;
	}

	/**
	 * @param calculationService the calculationService to set
	 */
	@Required
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	/**
	 * @return the bcfTravelCartService
	 */
	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	/**
	 * @return the bcfSalesApplicationResolverService
	 */
	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	/**
	 * @param bcfSalesApplicationResolverService the bcfSalesApplicationResolverService to set
	 */
	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	/**
	 * @return the bcfCloneOrderStrategy
	 */
	protected BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	/**
	 * @param bcfCloneOrderStrategy the bcfCloneOrderStrategy to set
	 */
	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the bcfUpdateOrderFromEBookingStrategy
	 */
	protected BcfUpdateOrderFromEBookingStrategy getBcfUpdateOrderFromEBookingStrategy()
	{
		return bcfUpdateOrderFromEBookingStrategy;
	}

	/**
	 * @param bcfUpdateOrderFromEBookingStrategy the bcfUpdateOrderFromEBookingStrategy to set
	 */
	@Required
	public void setBcfUpdateOrderFromEBookingStrategy(final BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy)
	{
		this.bcfUpdateOrderFromEBookingStrategy = bcfUpdateOrderFromEBookingStrategy;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}


	protected BcfAccommodationCommerceCartService getAccommodationCommerceCartService()
	{
		return accommodationCommerceCartService;
	}

	@Required
	public void setAccommodationCommerceCartService(
			final BcfAccommodationCommerceCartService accommodationCommerceCartService)
	{
		this.accommodationCommerceCartService = accommodationCommerceCartService;
	}

	@Override
	public OrderService getOrderService()
	{
		return orderService;
	}

	@Override
	public void setOrderService(final OrderService orderService)
	{
		this.orderService = orderService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public TravelManageStockByEntryTypeStrategy getAccommodationManageStockStrategy()
	{
		return accommodationManageStockStrategy;
	}

	public void setAccommodationManageStockStrategy(
			final TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy)
	{
		this.accommodationManageStockStrategy = accommodationManageStockStrategy;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;


	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
