/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 12/7/19 8:50 AM
 */

package com.bcf.core.service.impl;

import static com.bcf.core.constants.BcfCoreConstants.EXPIRED_OPTION_BOOKING_NOTIFICATION_URL;
import static com.bcf.core.constants.BcfCoreConstants.HYPHEN;
import static com.bcf.core.constants.BcfCoreConstants.OPTION_BOOKING_CANCEL_PROCESS_NAME;
import static com.bcf.core.constants.BcfCoreConstants.OPTION_BOOKING_PROCESS_NAME;
import static com.bcf.core.constants.BcfCoreConstants.PACKAGE_ON_REQUEST;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.OptionBookingProcessModel;
import com.bcf.core.optionbooking.manager.ExpiredOptionBookingNotificationPipelineManager;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.optionbooking.ExpiredOptionBookingNotificationData;


/**
 * Class responsible for handling notifications for option bookings
 */
public class DefaultBcfOptionBookingNotificationService implements BcfOptionBookingNotificationService
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOptionBookingNotificationService.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "bcfCloneOrderStrategy")
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;

	@Resource(name = "expiredOptionBookingNotificationPipelineManager")
	private ExpiredOptionBookingNotificationPipelineManager expiredOptionBookingNotificationPipelineManager;

	@Resource(name = "notificationEngineRestService")
	private NotificationEngineRestService notificationEngineRestService;

	@Override
	public void startCancellationProcessForOptionBooking(List<AbstractOrderEntryModel> orderEntryModels)
	{
		AbstractOrderEntryModel abstractOrderEntryModel = StreamUtil.safeStream(orderEntryModels).filter(Objects::nonNull)
				.findFirst()
				.orElse(null);

		if (Objects.nonNull(abstractOrderEntryModel))
		{
			AbstractOrderModel abstractOrderModel = abstractOrderEntryModel.getOrder();

			if (OptionBookingUtil.isClonedOptionBooking(abstractOrderModel))
			{
				return;
			}
			AbstractOrderModel clonedCart = getBcfCloneOrderStrategy().cloneCart(abstractOrderModel);
			LOG.info("Created cloned cart for Option Booking Cancellation :" + clonedCart.getCode());

			List<Integer> modifiedOrderEntryNumbers = orderEntryModels.stream().map(AbstractOrderEntryModel::getEntryNumber)
					.collect(Collectors.toList());

			List<AbstractOrderEntryModel> clonedOrderEntryModels = clonedCart.getEntries().stream()
					.filter(ce -> modifiedOrderEntryNumbers.contains(ce.getEntryNumber())).collect(
							Collectors.toList());
			startCancelOrderEmailProcess(clonedCart, clonedOrderEntryModels);
		}
	}

	@Override
	public void startOptionBookingEmailProcess(final AbstractOrderModel abstractOrder)
	{
		if (PACKAGE_ON_REQUEST.equals(abstractOrder.getAvailabilityStatus()))
		{
			return;
		}
		createAndStartBusinessProcess(OPTION_BOOKING_PROCESS_NAME, abstractOrder, null);
	}

	@Override
	public void startOptionBookingEmailProcess(final AbstractOrderModel abstractOrder,
			final List<CartModificationData> cartModificationDataList)
	{
		if (CollectionUtils.isEmpty(cartModificationDataList))
		{
			return;
		}

		if (PACKAGE_ON_REQUEST.equals(abstractOrder.getAvailabilityStatus()))
		{
			return;
		}

		List<Integer> modifiedEntryNumbers = cartModificationDataList.stream().map(m -> m.getEntry().getEntryNumber())
				.collect(Collectors.toList());
		List<AbstractOrderEntryModel> modifiedOrderEntries = StreamUtil.safeStream(abstractOrder.getEntries())
				.filter(entry -> modifiedEntryNumbers.contains(entry.getEntryNumber())).collect(
						Collectors.toList());

		createAndStartBusinessProcess(OPTION_BOOKING_PROCESS_NAME, abstractOrder, modifiedOrderEntries);
	}

	@Override
	public void startOptionBookingForEntriesEmailProcess(final AbstractOrderModel abstractOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		if (CollectionUtils.isEmpty(abstractOrderEntryList))
		{
			return;
		}

		if (PACKAGE_ON_REQUEST.equals(abstractOrder.getAvailabilityStatus()))
		{
			LOG.info(String.format("Option Booking %s is onRequest.Skipping Create Order Notification process",
					abstractOrder.getCode()));
			return;
		}
		createAndStartBusinessProcess(OPTION_BOOKING_PROCESS_NAME, abstractOrder, abstractOrderEntryList);
	}

	@Override
	public void startCancelOrderEmailProcess(final AbstractOrderModel abstractOrder)
	{
		createAndStartBusinessProcess(OPTION_BOOKING_CANCEL_PROCESS_NAME, abstractOrder, null);
	}

	@Override
	public void startCancelOrderEmailProcess(final AbstractOrderModel abstractOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		createAndStartBusinessProcess(OPTION_BOOKING_CANCEL_PROCESS_NAME, abstractOrder, abstractOrderEntryList);
	}

	@Override
	public void sendExpiredBookingNotifications(List<CartModel> expiredOptionBookings)
			throws IntegrationException
	{
		List<AbstractOrderModel> abstractOrderModelList = expiredOptionBookings.stream().map(AbstractOrderModel.class::cast)
				.collect(
						Collectors.toList());

		final ExpiredOptionBookingNotificationData expiredOBContext = getExpiredOptionBookingNotificationPipelineManager()
				.executePipeline(abstractOrderModelList);

		getNotificationEngineRestService()
				.sendRestRequest(expiredOBContext, ExpiredOptionBookingNotificationData.class, HttpMethod.POST,
						EXPIRED_OPTION_BOOKING_NOTIFICATION_URL);
	}

	private String makeOptionBookingBusinessProcessName(final String processName, final String abstractOrderCode,
			final String timestamp)
	{
		return new StringBuilder().append(processName).append(HYPHEN)
				.append(abstractOrderCode).append(HYPHEN).append(timestamp).toString();
	}

	private void createAndStartBusinessProcess(final String optionBookingProcessName, final AbstractOrderModel abstractOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		String timestamp = String.valueOf(System.currentTimeMillis());
		String optionBookingBusinessProcessName = makeOptionBookingBusinessProcessName(optionBookingProcessName,
				abstractOrder.getCode(),
				timestamp);
		final OptionBookingProcessModel optionBookingProcessModel = getBusinessProcessService()
				.createProcess(optionBookingBusinessProcessName,
						optionBookingProcessName);
		optionBookingProcessModel.setCart(abstractOrder);
		if (CollectionUtils.isNotEmpty(abstractOrderEntryList))
		{
			optionBookingProcessModel.setCartEntries(abstractOrderEntryList);
		}
		getModelService().save(optionBookingProcessModel);
		getBusinessProcessService().startProcess(optionBookingProcessModel);
		LOG.info(String.format("Triggered option booking business process [%s] with name [%s] ", optionBookingProcessName,
				optionBookingBusinessProcessName));
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	public ExpiredOptionBookingNotificationPipelineManager getExpiredOptionBookingNotificationPipelineManager()
	{
		return expiredOptionBookingNotificationPipelineManager;
	}

	public void setExpiredOptionBookingNotificationPipelineManager(
			final ExpiredOptionBookingNotificationPipelineManager expiredOptionBookingNotificationPipelineManager)
	{
		this.expiredOptionBookingNotificationPipelineManager = expiredOptionBookingNotificationPipelineManager;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}

