/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.impl.DefaultCartEntryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.dao.BcfCartEntryDao;
import com.bcf.core.service.BcfCartEntryService;


public class DefaultBcfCartEntryService extends DefaultCartEntryService implements BcfCartEntryService
{
	private BcfCartEntryDao bcfCartEntryDao;

	@Override
	public List<AbstractOrderEntryModel> getAbstractOrderEntryModel(final CartModel order,
			final List<Integer> entryNumbers)
	{

		return getBcfCartEntryDao().findEntriesForOrder(order, entryNumbers);
	}

	protected BcfCartEntryDao getBcfCartEntryDao()
	{
		return bcfCartEntryDao;
	}

	@Required
	public void setBcfCartEntryDao(final BcfCartEntryDao bcfCartEntryDao)
	{
		this.bcfCartEntryDao = bcfCartEntryDao;
	}
}
