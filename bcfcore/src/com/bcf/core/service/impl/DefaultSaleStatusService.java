/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import static com.bcf.core.constants.BcfCoreConstants.CONSIDER_NON_BOOKABLE_ONLINE_SALE_STATUS;

import java.util.Collection;
import java.util.Date;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.core.service.BcfSaleStatusService;
import com.bcf.core.util.BCFDateUtils;


public class DefaultSaleStatusService implements BcfSaleStatusService
{
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	public boolean toConsiderNonBookableOnlineSaleStatus()
	{
		return Boolean
				.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue(CONSIDER_NON_BOOKABLE_ONLINE_SALE_STATUS));
	}

	@Override
	public SaleStatusType getSaleStatusMatchedWithDate(Collection<SaleStatusModel> saleStatuses, Date date)
	{
		if (org.apache.commons.collections4.CollectionUtils.isEmpty(saleStatuses))
		{
			return SaleStatusType.OPEN;
		}

		for (final SaleStatusModel saleStatus : saleStatuses)
		{
			if (BCFDateUtils.isBetweenDatesInclusive(date, saleStatus.getStartDate(), saleStatus.getEndDate()))
			{
				return saleStatus.getSaleStatusType();
			}
		}
		return SaleStatusType.OPEN;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

}
