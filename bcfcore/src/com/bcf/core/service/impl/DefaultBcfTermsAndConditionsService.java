/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.dao.BcfTermsAndConditionsDao;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.core.service.BcfTermsAndConditionsService;


public class DefaultBcfTermsAndConditionsService implements BcfTermsAndConditionsService
{
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfTermsAndConditionsDao bcfTermsAndConditionsDao;
	private static final String DEFAULT_TERMS_CONDITIONS_CODE = "default.terms.and.conditions.code";

	@Override
	public FerryTandCModel getTermsAndConditionsModel(final String code)
	{
		if (StringUtils.isBlank(code))
		{
			return getDefaultTermsAndConditionsModel();
		}
		final FerryTandCModel ferryTandCModel = getBcfTermsAndConditionsDao().findTandCModelByCode(code);
		if (Objects.nonNull(ferryTandCModel))
		{
			return ferryTandCModel;
		}
		return getDefaultTermsAndConditionsModel();
	}

	@Override
	public List<FerryTandCModel> getTermsAndConditionsModels(final List<String> codes)
	{
		List<String> ferryTandCCodes = codes;
		List<FerryTandCModel> ferryTandCModels = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(ferryTandCCodes))
		{
			ferryTandCModels = getBcfTermsAndConditionsDao().findTandCModelsByCodes(ferryTandCCodes);
		}
		if (CollectionUtils.isEmpty(ferryTandCModels))
		{
			ferryTandCCodes = Collections
					.singletonList(getBcfConfigurablePropertiesService().getBcfPropertyValue(DEFAULT_TERMS_CONDITIONS_CODE));
			ferryTandCModels = getBcfTermsAndConditionsDao().findTandCModelsByCodes(ferryTandCCodes);
		}

		return ferryTandCModels;
	}

	private FerryTandCModel getDefaultTermsAndConditionsModel()
	{
		return getBcfTermsAndConditionsDao()
				.findTandCModelByCode(getBcfConfigurablePropertiesService().getBcfPropertyValue(DEFAULT_TERMS_CONDITIONS_CODE));
	}

	protected BcfTermsAndConditionsDao getBcfTermsAndConditionsDao()
	{
		return bcfTermsAndConditionsDao;
	}

	@Required
	public void setBcfTermsAndConditionsDao(final BcfTermsAndConditionsDao bcfTermsAndConditionsDao)
	{
		this.bcfTermsAndConditionsDao = bcfTermsAndConditionsDao;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
