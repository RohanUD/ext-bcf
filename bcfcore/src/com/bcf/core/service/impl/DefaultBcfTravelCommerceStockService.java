/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.stock.impl.DefaultTravelCommerceStockService;
import de.hybris.platform.travelservices.strategies.stock.TravelManageStockByEntryTypeStrategy;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.service.BcfTravelStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class DefaultBcfTravelCommerceStockService extends DefaultTravelCommerceStockService
		implements BcfTravelCommerceStockService
{
	private static final String DEFAULT_FORCED_VALUE_STOCK = "defaultforcedinstockvalue";
	private static final String DEFAULT_STOCK_VALUE = "defaultstockvalue";
	private static final Logger LOG = Logger.getLogger(DefaultBcfTravelCommerceStockService.class);
	private List<OrderEntryType> orderEntryTypes=Arrays.asList(OrderEntryType.ACCOMMODATION,OrderEntryType.ACTIVITY);

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private ConfigurationService configurationService;
	private SessionService sessionService;
	private BcfTravelStockService bcfTravelStockService;

   private final String STOCK_HOLD_TIME_IN_MS = "stockallocation.hold.time.in.ms.";
   private final String STOCK_ALLOCATION_EARLY = "stockallocation.early.";
	private final String STOCK_ABOUT_TO_EXPIRE_TIME_IN_MS = "stockallocation.about.to.expire.time.in.ms.";


   @Override
   public void reservePerDateProduct(final ProductModel product, final Date date, final int quantity,
         final Collection<WarehouseModel> warehouses) throws InsufficientStockLevelException
   {
      final StockLevelType stockLevelType = getStockLevelType(product);
      if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
      {
         super.reservePerDateProduct(product, date, quantity, warehouses);
      }
   }

	@Override
	public void reservePerDateTimeProduct(final ProductModel product, String selectedTime, final Date date, final int quantity,
			final Collection<WarehouseModel> warehouses) throws InsufficientStockLevelException
	{
		final StockLevelType stockLevelType = getStockLevelType(product);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final List<StockLevelModel> stockLevels = bcfTravelStockService
					.getStockLevelsForProductAndDate(product.getCode(), date,
							getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE));

			if (CollectionUtils.isEmpty(stockLevels))
			{
				throw new InsufficientStockLevelException(String.format("Impossible to reserve the desired quantity for the product %s on %s", product.getCode(), TravelDateUtils.convertDateToStringDate(date, "dd/MM/yyyy")));

			}
			StockLevelModel stockLevelWithMatchedDateAndTime = stockLevels.stream()
					.filter(s -> StringUtils.isNotEmpty(s.getTime()) && s.getTime().equals(selectedTime)).findFirst()
					.orElse(null);
			if (Objects.isNull(stockLevelWithMatchedDateAndTime))
			{
				throw new InsufficientStockLevelException(String.format("Impossible to reserve the desired quantity for the product %s on %s", product.getCode(), TravelDateUtils.convertDateToStringDate(date, "dd/MM/yyyy")));
			}

			 if (quantity > stockLevelWithMatchedDateAndTime.getAvailable() - stockLevelWithMatchedDateAndTime.getReserved()) {
				throw new InsufficientStockLevelException(String.format("Impossible to reserve the desired quantity for the product %s on %s", product.getCode(), TravelDateUtils.convertDateToStringDate(date, "dd/MM/yyyy")));
			} else {
				 stockLevelWithMatchedDateAndTime.setReserved(stockLevelWithMatchedDateAndTime.getReserved() + quantity);
				this.getModelService().save(stockLevelWithMatchedDateAndTime);
			}
		}

	}

	@Override
	public void releasePerDateProduct(final ProductModel product, final Date date, final int quantity,
			final Collection<WarehouseModel> warehouses)
	{
		final StockLevelType stockLevelType = getStockLevelType(product);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			super.releasePerDateProduct(product, date, quantity, warehouses);
		}
	}

	@Override
	public void releasePerDateTimeProduct(final ProductModel product,String selectedTime, final Date date, final int quantity,
			final Collection<WarehouseModel> warehouses)
	{

		final StockLevelType stockLevelType = getStockLevelType(product);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final List<StockLevelModel> stockLevels = bcfTravelStockService
					.getStockLevelsForProductAndDate(product.getCode(), date,
							getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE));

			if(CollectionUtils.isNotEmpty(stockLevels)){

				StockLevelModel stockLevelWithMatchedDateAndTime = stockLevels.stream()
						.filter(s -> StringUtils.isNotEmpty(s.getTime()) && s.getTime().equals(selectedTime)).findFirst()
						.orElse(null);

				if(stockLevelWithMatchedDateAndTime!=null){
					stockLevelWithMatchedDateAndTime.setReserved(stockLevelWithMatchedDateAndTime.getReserved() - quantity);
					this.getModelService().save(stockLevelWithMatchedDateAndTime);
				}
			}

		}
	}


	@Override
	public Integer getStockForDate(final ProductModel product, Date date, final Collection<WarehouseModel> warehouses)
	{
		final StockLevelType stockLevelType = getStockLevelType(product);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			if (Objects.isNull(date))
			{
				date = new Date();
			}
			final StockLevelModel stockLevelModel = getStockLevelModelForDate(product, date, warehouses);
			if (Objects.isNull(stockLevelModel))
			{
				return null;
			}

			if (Objects.equals(StockLevelType.STANDARD, stockLevelModel.getStockLevelType()))
			{
				return stockLevelModel.getAvailable() - stockLevelModel.getReserved();
			}
		}
		return getDefaultStockValue();
	}

	@Override
	public StockLevelModel getStockLevelModelForDate(final ProductModel product, final Date date,
			final Collection<WarehouseModel> warehouses)
	{
		return this.getTravelStockService().getStockLevelForDate(product, warehouses, date);
	}

   @Override
   public void releaseStocks(final CartModel cart)
   {
   	List<ItemModel> itemsToSave=new ArrayList<>();
		List<AbstractOrderEntryModel> entries=cart.getEntries();
		entries.stream()
            .filter(entry -> entry.isStockAllocated() && !entry.getAmendStatus().equals(AmendStatus.SAME) && entry.getActive() && Objects
                  .equals(OrderEntryType.ACCOMMODATION, entry.getType()))
            .forEach(getAccommodationManageStockStrategy()::release);

		entries.stream().filter(entry ->
            entry.isStockAllocated() && entry.getActive() && !entry.getAmendStatus().equals(AmendStatus.SAME) && Objects.equals(OrderEntryType.ACTIVITY, entry.getType()))
            .forEach(this::releaseActivityStock);


      for (AbstractOrderEntryModel entry :entries)
      {
         entry.setStockAllocated(false);

      }
		itemsToSave.addAll(entries);
		cart.setStockHoldTime(0);
		cart.setTimerDisplayed(false);
		cart.setAboutToExpireDisplayed(false);
		itemsToSave.add(cart);
		getModelService().saveAll(itemsToSave);
   }


	@Override
	public void releaseStocks(final List<AbstractOrderEntryModel> entries)
	{
		if (CollectionUtils.isNotEmpty(entries))
		{
			List<ItemModel> itemsToSave = new ArrayList<>();

			entries.stream()
					.filter(entry -> entry.isStockAllocated() && entry.getActive() && Objects
							.equals(OrderEntryType.ACCOMMODATION, entry.getType()))
					.forEach(getAccommodationManageStockStrategy()::release);

			entries.stream().filter(entry ->
					entry.isStockAllocated() && entry.getActive() && Objects.equals(OrderEntryType.ACTIVITY, entry.getType()))
					.forEach(this::releaseActivityStock);

			for (AbstractOrderEntryModel entry : entries)
			{
				entry.setStockAllocated(false);
			}
			itemsToSave.addAll(entries);
			CartModel cart = (CartModel) entries.get(0).getOrder();
			boolean otherEntriesPresent = checkOtherEntriesPresent(cart, entries);
			if (!otherEntriesPresent)
			{
				cart.setStockHoldTime(0);
				cart.setTimerDisplayed(false);
				cart.setAboutToExpireDisplayed(false);
				itemsToSave.add(cart);

			}


			getModelService().saveAll(itemsToSave);

		}
	}

	@Override
	public boolean checkOtherEntriesPresent(CartModel cart,final List<AbstractOrderEntryModel> entries)
	{

		List<AbstractOrderEntryModel> allEntries=new ArrayList<>(cart.getEntries());
		allEntries.removeAll(entries);
		if (CollectionUtils.isNotEmpty(allEntries) && !BookingJourneyType.BOOKING_TRANSPORT_ONLY
				.equals(cart.getBookingJourneyType()))
		{
			return allEntries.stream().anyMatch(
					entry -> entry.getActive() && !AmendStatus.SAME.equals(entry.getAmendStatus()) && ((orderEntryTypes.contains(entry.getType())) || (
							Objects.equals(OrderEntryType.TRANSPORT, entry.getType()) && entry.isStockAllocated())));

		}
		return false;
	}



	/**
	 * Batch saving of stock levels.
	 *
	 * @param stockLevelsToSave
	 *           the stock levels to save
	 */
	@Override
	public void batchSavingOfStockLevels(final List<StockLevelModel> stockLevelsToSave)
	{
		final int stockLevelSize = CollectionUtils.size(stockLevelsToSave);
		LOG.info("Start saving... Number of stock levels to be saved: " + stockLevelSize);
		final int batchSize = getConfigurationService().getConfiguration().getInt("stocksave.batch.size");
		final int numberOfBatches = (int) Math.ceil(stockLevelSize*1d / batchSize);

		LOG.info("Start saving...Batch Size: " + batchSize);
		LOG.info("Start saving... Number of Batches: " + numberOfBatches);

		for (int i = 0; i < numberOfBatches; i++)
		{
			if (i != numberOfBatches - 1)
			{
				getModelService().saveAll(stockLevelsToSave.subList(i * batchSize, (i + 1) * batchSize));
			}
			else
			{
				getModelService().saveAll(stockLevelsToSave.subList(i * batchSize, stockLevelSize));
			}
			LOG.info("Saved batch number: " + i);
		}

		LOG.info("Saving finished");
	}

	@Override
	public StockLevelType getStockLevelType(final ProductModel product)
	{
		StockLevelType stockLevelType = StockLevelType.STANDARD;
		if (product.getItemtype().equals(AccommodationModel._TYPECODE))
		{
			final AccommodationModel accommodation = (AccommodationModel) product;
			stockLevelType = accommodation.getStockLevelType();
		}
		else if (product.getItemtype().equals(RoomRateProductModel._TYPECODE)){
			stockLevelType = ((RatePlanModel) product
					.getSupercategories().stream().findFirst().get()).getAccommodation().stream().findFirst().get()
					.getStockLevelType();
		}
		else if (product.getItemtype().equals(ActivityProductModel._TYPECODE))
		{
			final ActivityProductModel activityProduct = (ActivityProductModel) product;
			stockLevelType = activityProduct.getStockLevelType();
		}
		return stockLevelType;
	}

	@Override
	public Collection<WarehouseModel> getWarehouses(final AbstractOrderEntryModel entry)
	{
		WarehouseModel warehouseModel = null;
		if (Objects.equals(OrderEntryType.ACCOMMODATION, entry.getType()))
		{
			final AccommodationOrderEntryGroupModel accommodationEntryGroup = getBcfTravelCommerceCartService()
					.getAccommodationOrderEntryGroup(entry);
			warehouseModel = accommodationEntryGroup.getAccommodationOffering();
		}
		else if (Objects.equals(OrderEntryType.ACTIVITY, entry.getType()))
		{
			warehouseModel = getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE);
		}
		return Objects.nonNull(warehouseModel) ? Collections.singletonList(warehouseModel) : Collections.emptyList();
	}

   @Override
   public void releaseActivityStock(final AbstractOrderEntryModel activityEntry)
   {
      final int qty = activityEntry.getQuantity().intValue();
      if (qty > 0)
      {
      	if(StringUtils.isNotBlank(activityEntry.getActivityOrderEntryInfo().getActivityTime())){

				releasePerDateTimeProduct(activityEntry.getProduct(),activityEntry.getActivityOrderEntryInfo().getActivityTime(), activityEntry.getActivityOrderEntryInfo().getActivityDate(), qty,
						Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
			}else
			{
				releasePerDateProduct(activityEntry.getProduct(), activityEntry.getActivityOrderEntryInfo().getActivityDate(), qty,
						Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
			}
         activityEntry.setStockAllocated(false);

         getModelService().save(activityEntry);
      }
   }

	@Override
	public Integer getDefaultStockValue()
	{
		return Integer.valueOf(getBcfConfigurablePropertiesService().getBcfPropertyValue(DEFAULT_FORCED_VALUE_STOCK));
	}

	@Override
	public Integer getStockAvailableForDate(final ProductModel product, final Date date,
			final Collection<WarehouseModel> warehouses)
	{
		final StockLevelType stockLevelType = getStockLevelType(product);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final StockLevelModel stockLevelModel = getStockLevelModelForDate(product, date, warehouses);
			return Objects.isNull(stockLevelModel) ? 0 : (stockLevelModel.getAvailable() - stockLevelModel.getReserved());
		}
		return Integer.valueOf(getBcfConfigurablePropertiesService().getBcfPropertyValue(DEFAULT_STOCK_VALUE));
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected TravelManageStockByEntryTypeStrategy getAccommodationManageStockStrategy()
	{
		return accommodationManageStockStrategy;
	}

	@Required
	public void setAccommodationManageStockStrategy(final TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy)
	{
		this.accommodationManageStockStrategy = accommodationManageStockStrategy;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	@Override
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

   @Required
   @Override
   public void setConfigurationService(final ConfigurationService configurationService)
   {
      this.configurationService = configurationService;
   }

   @Override
   public long getCartStockExpiryTime(final CartModel cart)
   {
      return cart.getStockHoldTime();
   }


   @Override
   public long getStockHoldExpiryTimeInMs(final SalesApplication channel)
   {
      final Date now = new Date();
      long expiryTime = now.getTime() + getStockHoldTimeInMs(channel);
      return expiryTime;
   }

   @Override
   public int getStockHoldTimeInMs(final SalesApplication channel)
   {
      return configurationService.getConfiguration().getInt(STOCK_HOLD_TIME_IN_MS + channel.getCode());
   }

	@Override
	public int getStockAboutToExpireTimeInMs(final SalesApplication channel)
	{
		return configurationService.getConfiguration().getInt(STOCK_ABOUT_TO_EXPIRE_TIME_IN_MS + channel.getCode());
	}

   @Override
   public boolean getEarlyStockAllocation(final SalesApplication channel)
   {
      return configurationService.getConfiguration().getBoolean(STOCK_ALLOCATION_EARLY + channel.getCode());
   }

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfTravelStockService getBcfTravelStockService()
	{
		return bcfTravelStockService;
	}

	public void setBcfTravelStockService(final BcfTravelStockService bcfTravelStockService)
	{
		this.bcfTravelStockService = bcfTravelStockService;
	}
}
