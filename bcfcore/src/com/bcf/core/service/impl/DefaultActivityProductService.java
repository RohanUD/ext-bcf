/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import java.util.List;
import com.bcf.core.dao.ActivityProductDao;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.ActivityProductService;


public class DefaultActivityProductService implements ActivityProductService
{
	ActivityProductDao activityProductDao;

	@Override
	public List<ActivityProductModel> findAllActivityProducts()
	{
		return getActivityProductDao().getActivityProducts();
	}

	@Override
	public ActivityProductModel getActivityProduct(final String activityCode, final CatalogVersionModel catalogVersion)
	{
		return getActivityProductDao().findActivityProduct(activityCode, catalogVersion);
	}

	@Override
	public List<ActivityProductModel> getActivityProductsByDestination(final List<String> destinationCode,
			final CatalogVersionModel catalogVersion)
	{
		return getActivityProductDao().getActivityProductsByDestination(destinationCode, catalogVersion);
	}


	public ActivityProductDao getActivityProductDao()
	{
		return activityProductDao;
	}

	public void setActivityProductDao(final ActivityProductDao activityProductDao)
	{
		this.activityProductDao = activityProductDao;
	}
}
