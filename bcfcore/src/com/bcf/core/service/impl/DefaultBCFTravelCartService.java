/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.user.TravellerInfoModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.order.impl.DefaultTravelCartService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfStockLevelDao;
import com.bcf.core.enums.AccountType;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFCommerceAddToCartStrategy;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;


public class DefaultBCFTravelCartService extends DefaultTravelCartService implements BCFTravelCartService
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFTravelCartService.class);
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfStockLevelDao bcfStockLevelDao;
	private BcfBookingService bcfBookingService;
	private CommerceAddToCartStrategy commerceAddToCartStrategy;
	private SessionService sessionService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;
	private BcfCalculationService bcfCalculationService;


	private static final String ASM_SESSION_PARAMETER = "ASM";


	@Override
	public void disableAmendedCartEntries(final CartFilterParamsDto cartFilterParams)
	{
		if (hasSessionCart())
		{
			final CartModel cart = getSessionCart();
			final List<AbstractOrderEntryModel> filteredEntries = cart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()))
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& entry.getJourneyReferenceNumber() == cartFilterParams.getJourneyRefNo()
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartFilterParams.getOdRefNo()
							&& AmendStatus.SAME.equals(entry.getAmendStatus()))
					.collect(Collectors.toList());

			filteredEntries.forEach(entry -> {
				entry.setActive(Boolean.FALSE);
				entry.setAmendStatus(AmendStatus.CHANGED);
			});

			getModelService().saveAll();
		}
	}


	@Override
	public Boolean isAmendmentCart(AbstractOrderModel sessionCart)
	{

		if (sessionCart == null)
		{
			if (!hasSessionCart())
			{
				return Boolean.FALSE;
			}
			sessionCart = getSessionCart();
		}


		if (Objects.nonNull(sessionCart) && Objects.nonNull(sessionCart.getOriginalOrder()))
		{
			return Boolean.TRUE;
		}
		return sessionCart.getCreatedFromEBooking() != null ? sessionCart.getCreatedFromEBooking() : Boolean.FALSE;
	}


	@Override
	public double getOriginalAmountPaid(final CartModel cart)
	{
		return cart.getOriginalOrder().getAmountPaid();
	}


	@Override
	public int getNumberOfGuestsInCart(final CartModel cart, final int journeyRef)
	{
		int noOfGuests = 0;
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = bcfBookingService
				.getAccommodationOrderEntryGroups(cart, journeyRef);
		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{
			for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel : accommodationOrderEntryGroupModels)
			{
				if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getGuestCounts()))
				{
					noOfGuests = noOfGuests + accommodationOrderEntryGroupModel.getGuestCounts().stream()
							.mapToInt(guestCount -> guestCount.getQuantity()).sum();
				}
			}
		}
		return noOfGuests;
	}




	@Override
	public void deleteCurrentCart()
	{
		if (hasSessionCart())
		{
			removeCart(getSessionCart());
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
		}
	}


	@Override
	public void removeCart(final CartModel cartModel)
	{
		if (cartModel != null)
		{
			bcfTravelCommerceStockService.releaseStocks(cartModel);
		}
		if (!cartModel.isQuote())
		{
			removeAssociatedItemsFromCart(cartModel);
			getModelService().remove(cartModel);
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
		}

	}

	@Override
	public void removeAssociatedItemsFromCart(final CartModel cartModel)
	{

		if (getBcfBookingService().checkIfOrderEntriesExistOfType(cartModel, OrderEntryType.TRANSPORT))
		{
			removeEntriesForTransportOnlyJourney(cartModel, Collections.emptyList());
		}
		if (getBcfBookingService().checkIfActivityEntriesExist(cartModel))
		{
			removeActivityOrderEntries(cartModel, false);
		}
		getModelService().refresh(cartModel);

		final List<AbstractOrderEntryGroupModel> abstractOrderEntryGroupModels = getBcfBookingService()
				.getOrderEntryGroups(cartModel);
		if (CollectionUtils.isNotEmpty(abstractOrderEntryGroupModels))
		{
			final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService()
					.getDealOrderEntryGroups(abstractOrderEntryGroupModels);
			if (CollectionUtils.isNotEmpty(dealOrderEntryGroupModels))
			{
				getBcfBookingService().removeEntriesForDeal(dealOrderEntryGroupModels, false);
			}
			else
			{

				final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = getBcfBookingService()
						.getAccommodationOrderEntryGroups(abstractOrderEntryGroupModels);

				removeEntriesForAccommodation(accommodationOrderEntryGroupModels);
			}

		}
	}

	protected void removeEntriesForTransportOnlyJourney(final AbstractOrderModel cartModel,
			final List<String> transportOfferingCodes)
	{
		Set<AbstractOrderEntryModel> transportCartEntries = cartModel.getEntries().stream()
				.filter(entry -> Objects.isNull(entry.getEntryGroup()) && OrderEntryType.TRANSPORT.equals(entry.getType()))
				.collect(Collectors.toSet());
		if (CollectionUtils.isNotEmpty(transportOfferingCodes))
		{
			final Set<AbstractOrderEntryModel> cartEntriesToBeRemoved = new HashSet<>();
			for (final AbstractOrderEntryModel cartEntry : transportCartEntries)
			{
				final List<String> cartEntryTransportOfferings = cartEntry.getTravelOrderEntryInfo().getTransportOfferings().stream()
						.map(TransportOfferingModel::getCode).collect(Collectors.toList());
				if (CollectionUtils.containsAny(cartEntryTransportOfferings, transportOfferingCodes))
				{
					cartEntriesToBeRemoved.add(cartEntry);
				}
			}
			transportCartEntries = cartEntriesToBeRemoved;
		}
		removeEntriesForTransport(transportCartEntries);
		getModelService().refresh(cartModel);
	}


	@Override
	public void removeEntriesForTransportOfferingCodes(final AbstractOrderModel cartModel,
			final List<String> transportOfferingCodes)
	{
		Set<AbstractOrderEntryModel> transportCartEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())).collect(Collectors.toSet());
		if (CollectionUtils.isNotEmpty(transportOfferingCodes))
		{
			final Set<AbstractOrderEntryModel> cartEntriesToBeRemoved = new HashSet<>();
			for (final AbstractOrderEntryModel cartEntry : transportCartEntries)
			{
				final List<String> cartEntryTransportOfferings = cartEntry.getTravelOrderEntryInfo().getTransportOfferings().stream()
						.map(TransportOfferingModel::getCode).collect(Collectors.toList());
				if (CollectionUtils.containsAny(cartEntryTransportOfferings, transportOfferingCodes))
				{
					cartEntriesToBeRemoved.add(cartEntry);
				}
			}
			transportCartEntries = cartEntriesToBeRemoved;
		}
		removeEntriesForTransport(transportCartEntries);
		getModelService().refresh(cartModel);
	}


	@Override
	public void removeEntriesForAccommodation(final AbstractOrderModel abstractOrderModel)
	{
		final Set<AbstractOrderEntryModel> accommodationCartEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType())).collect(Collectors.toSet());

		final Set<AccommodationOrderEntryInfoModel> accommodationOrderEntryInfos = accommodationCartEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getAccommodationOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getAccommodationOrderEntryInfo).collect(Collectors.toSet());

		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = getBookingService()
				.getAccommodationOrderEntryGroups(abstractOrderModel);

		final Set<AbstractOrderEntryModel> accommodationOrderEntryGroupEntries = accommodationOrderEntryGroups.stream()
				.flatMap(accommodationOrderEntryGroup -> accommodationOrderEntryGroup.getEntries().stream())
				.collect(Collectors.toSet());

		getModelService().removeAll(accommodationOrderEntryGroupEntries);
		getModelService().removeAll(accommodationOrderEntryGroups);
		getModelService().removeAll(accommodationOrderEntryInfos);
		getModelService().removeAll(accommodationCartEntries);
		getModelService().refresh(abstractOrderModel);
	}

	public void removeEntriesForAccommodation(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups)
	{
		final Set<AbstractOrderEntryModel> accommodationOrderEntryGroupEntries = accommodationOrderEntryGroups.stream()
				.flatMap(accommodationOrderEntryGroup -> accommodationOrderEntryGroup.getEntries().stream())
				.collect(Collectors.toSet());

		final Set<AccommodationOrderEntryInfoModel> accommodationOrderEntryInfos = accommodationOrderEntryGroupEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getAccommodationOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getAccommodationOrderEntryInfo).collect(Collectors.toSet());

		getModelService().removeAll(accommodationOrderEntryGroupEntries);
		getModelService().removeAll(accommodationOrderEntryGroups);
		getModelService().removeAll(accommodationOrderEntryInfos);
	}


	@Override
	public void removeEntriesForTransport(final Set<AbstractOrderEntryModel> transportEntries)
	{
		final Set<TravelOrderEntryInfoModel> travelOrderEntryInfos = transportEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getTravelOrderEntryInfo).collect(Collectors.toSet());

		Set<TravellerModel> travellers = travelOrderEntryInfos.stream()
				.flatMap(travelOrderEntryInfo -> travelOrderEntryInfo.getTravellers().stream())
				.filter(traveller -> StringUtils.isEmpty(traveller.getSavedTravellerUid())).collect(Collectors.toSet());
		travellers = travellers.stream().filter(traveller -> travelOrderEntryInfos.containsAll(traveller.getTravelOrderEntryInfo()))
				.collect(Collectors.toSet());

		final Set<TravellerInfoModel> travellersInfo = travellers.stream().map(TravellerModel::getInfo).collect(Collectors.toSet());


		getModelService().removeAll(travellersInfo);
		getModelService().removeAll(travellers);
		getModelService().removeAll(travelOrderEntryInfos);
		getModelService().removeAll(transportEntries);
	}

	protected void removeActivityOrderEntries(final AbstractOrderModel orderModel, final boolean releaseStock)
	{
		if (Objects.isNull(orderModel))
		{
			return;
		}

		removeActivityOrderEntries(orderModel.getEntries().stream()
				.filter(
						entry -> Objects.nonNull(entry.getActivityOrderEntryInfo()) && entry.getType().equals(OrderEntryType.ACTIVITY))
				.collect(
						Collectors.toList()), releaseStock);
	}


	@Override
	public void removeActivityOrderEntries(final List<AbstractOrderEntryModel> abstractOrderEntries, final boolean releaseStock)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderEntries))
		{
			if (releaseStock)
			{
				bcfTravelCommerceStockService.releaseStocks(abstractOrderEntries);
			}

			if (OptionBookingUtil.isOptionBooking(abstractOrderEntries))
			{
				getBcfOptionBookingNotificationService().startCancellationProcessForOptionBooking(abstractOrderEntries);
			}

			final List<ActivityOrderEntryInfoModel> activityOrderEntryInfoModels = abstractOrderEntries.stream()
					.map(AbstractOrderEntryModel::getActivityOrderEntryInfo).collect(Collectors.toList());
			getModelService().removeAll(activityOrderEntryInfoModels);
			getModelService().removeAll(abstractOrderEntries);
		}
	}


	@Override
	public void removeAmountToPayInfos(final AbstractOrderModel abstractOrderModel)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getAmountToPayInfos()))
		{
			getModelService().removeAll(abstractOrderModel.getAmountToPayInfos());
			getModelService().save(abstractOrderModel);
			getModelService().refresh(abstractOrderModel);
		}
	}

	@Override
	public boolean updateEntriesForJourneyODRefNo(final AbstractOrderModel cartModel, final CartFilterParamsDto cartFilterParams)
	{
		final int newJourneyRefNum = cartModel.getCurrentJourneyRefNum() + 1;
		final List<AbstractOrderEntryModel> outboundCartEntries = getAllCartEntriesForRefNo(cartFilterParams.getJourneyRefNo(), 0);

		final List<AbstractOrderEntryModel> inboundCartEntries = getAllCartEntriesForRefNo(cartFilterParams.getJourneyRefNo(), 1);

		if (cartFilterParams.getOdRefNo() == TravelservicesConstants.INBOUND_REFERENCE_NUMBER)
		{
			updateLeg(inboundCartEntries, newJourneyRefNum);
		}
		else
		{
			updateLeg(outboundCartEntries, newJourneyRefNum);
			convertToOutboundLeg(inboundCartEntries);
		}
		cartModel.setCurrentJourneyRefNum(newJourneyRefNum);
		getModelService().save(cartModel);
		if (cartFilterParams.isCommit())
		{
			getModelService().refresh(cartModel);
		}
		return true;
	}


	@Override
	public AbstractOrderEntryModel addNewEntry(final ComposedTypeModel entryType, final CartModel order,
			final ProductModel product,
			final long qty, final UnitModel unit, final int number, final boolean addToPresent)
	{
		AbstractOrderEntryModel cartEntry = null;
		if (unit != null)
		{
			cartEntry = super.addNewEntry(entryType, order, product, qty, unit, number, addToPresent);
		}
		else
		{
			LOG.warn("Found the passed unit model as null, no using the unit associated with the Product:" + product.getUnit());
			cartEntry = super.addNewEntry(entryType, order, product, qty, product.getUnit(), number, addToPresent);
		}

		cartEntry.setAmendStatus(AmendStatus.NEW);
		cartEntry.setType(OrderEntryType.DEFAULT);
		cartEntry.setActive(Boolean.TRUE);
		getModelService().save(cartEntry);
		return cartEntry;
	}

	private void updateLeg(final List<AbstractOrderEntryModel> cartEntries, final int newJourneyRefNum)
	{
		final ListIterator<AbstractOrderEntryModel> iterator = cartEntries.listIterator();
		while (iterator.hasNext())
		{
			final AbstractOrderEntryModel entryToUpdate = iterator.next();
			entryToUpdate.setJourneyReferenceNumber(newJourneyRefNum);
			final TravelOrderEntryInfoModel travelorderEntryInfo = entryToUpdate.getTravelOrderEntryInfo();
			travelorderEntryInfo.setOriginDestinationRefNumber(0);
			getModelService().saveAll(travelorderEntryInfo, entryToUpdate);
		}
	}

	private void convertToOutboundLeg(final List<AbstractOrderEntryModel> cartEntries)
	{
		final ListIterator<AbstractOrderEntryModel> cartIterator = cartEntries.listIterator();
		while (cartIterator.hasNext())
		{
			final AbstractOrderEntryModel entryToUpdate = cartIterator.next();
			final TravelOrderEntryInfoModel travelorderEntryInfo = entryToUpdate.getTravelOrderEntryInfo();
			travelorderEntryInfo.setOriginDestinationRefNumber(0);
			getModelService().saveAll(travelorderEntryInfo, entryToUpdate);
		}
	}

	/**
	 * Removes the entries for cart.
	 * <p>
	 * the cart model journeyRefNo odRefNo
	 *
	 * @return true, if successful
	 */


	@Override
	public boolean removeNewEntriesForJourneyODRefNo(final AbstractOrderModel abstractOrderModel,
			final CartFilterParamsDto cartFilterParams, final boolean isSingle, final boolean removeInboundEntries)
	{
		final List<AbstractOrderEntryModel> journeyCartEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getAmendStatus().equals(AmendStatus.NEW) && OrderEntryType.TRANSPORT
						.equals(entry.getType()))
				.filter(entry -> entry.getJourneyReferenceNumber() == cartFilterParams.getJourneyRefNo())
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(journeyCartEntries))
		{
			removeCartEntries(abstractOrderModel, cartFilterParams, isSingle, removeInboundEntries, journeyCartEntries);
		}


		return true;
	}


	@Override
	public boolean removeEntriesForJourneyODRefNo(final AbstractOrderModel abstractOrderModel,
			final CartFilterParamsDto cartFilterParams, final boolean isSingle, final boolean removeInboundEntries)
	{
		final List<AbstractOrderEntryModel> journeyCartEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entry -> entry.getJourneyReferenceNumber() == cartFilterParams.getJourneyRefNo())
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(journeyCartEntries))
		{
			removeCartEntries(abstractOrderModel, cartFilterParams, isSingle, removeInboundEntries, journeyCartEntries);
		}

		return true;
	}

	private void removeCartEntries(final AbstractOrderModel abstractOrderModel, final CartFilterParamsDto cartFilterParams,
			final boolean isSingle, final boolean removeInboundEntries, final List<AbstractOrderEntryModel> journeyCartEntries)
	{
		final List<AbstractOrderEntryModel> selectedODCartEntries = journeyCartEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()) && Objects
						.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartFilterParams.getOdRefNo())
				.collect(Collectors.toList());

		final List<TravelOrderEntryInfoModel> travelOrderEntryInfos = selectedODCartEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getTravelOrderEntryInfo).collect(Collectors.toList());
		if (isSingle)
		{
			removetravellers(travelOrderEntryInfos, isSingle);
		}
		else
		{
			final List<AbstractOrderEntryModel> leftODCartEntries = journeyCartEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()) && Objects
							.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != cartFilterParams.getOdRefNo())
					.collect(Collectors.toList());

			final List<TravelOrderEntryInfoModel> selectedTravelOrderEntryInfos = selectedODCartEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
					.map(AbstractOrderEntryModel::getTravelOrderEntryInfo).collect(Collectors.toList());

			List<TravellerModel> selectedTravellers = selectedTravelOrderEntryInfos.stream()
					.flatMap(travelOrderEntryInfo -> travelOrderEntryInfo.getTravellers().stream())
					.filter(traveller -> StringUtils.isEmpty(traveller.getSavedTravellerUid())).collect(Collectors.toList());

			selectedTravellers = selectedTravellers.stream()
					.filter(traveller -> selectedTravelOrderEntryInfos.containsAll(traveller.getTravelOrderEntryInfo()))
					.collect(Collectors.toList());
			final List<TravellerModel> selectedVehicleTravellers = selectedTravellers.stream()
					.filter(travel -> travel.getType().equals(TravellerType.VEHICLE)).collect(Collectors.toList());

			final List<TravelOrderEntryInfoModel> leftTravelOrderEntryInfos = leftODCartEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
					.map(AbstractOrderEntryModel::getTravelOrderEntryInfo).collect(Collectors.toList());

			List<TravellerModel> leftTravellers = leftTravelOrderEntryInfos.stream()
					.flatMap(travelOrderEntryInfo -> travelOrderEntryInfo.getTravellers().stream())
					.filter(traveller -> StringUtils.isEmpty(traveller.getSavedTravellerUid())).collect(Collectors.toList());

			leftTravellers = leftTravellers.stream()
					.filter(traveller -> leftTravelOrderEntryInfos.containsAll(traveller.getTravelOrderEntryInfo()))
					.collect(Collectors.toList());
			final List<TravellerModel> leftVehicleTravellers = leftTravellers.stream()
					.filter(travel -> travel.getType().equals(TravellerType.VEHICLE)).collect(Collectors.toList());

			if (cartFilterParams.getOdRefNo() == 0)
			{
				removetravellers(travelOrderEntryInfos, isSingle);
			}

			updateVehicleTravellers(selectedVehicleTravellers, leftVehicleTravellers);

			if (removeInboundEntries && cartFilterParams.getOdRefNo() == 0 && CollectionUtils.isNotEmpty(leftODCartEntries))
			{
				leftODCartEntries.stream().findFirst()
						.ifPresent(entry -> {
							if (Objects.nonNull(entry.getCacheKey()) || Objects.nonNull(entry.getBookingReference()))
							{
								removeAmountInfo(abstractOrderModel, entry.getCacheKey(), entry.getBookingReference());
							}
						});
				bcfTravelCommerceStockService.releaseStocks(leftODCartEntries);
				getModelService().removeAll(leftODCartEntries);
			}
		}

		getModelService().removeAll(travelOrderEntryInfos);

		if (CollectionUtils.isNotEmpty(selectedODCartEntries))
		{
			selectedODCartEntries.stream().findFirst()
					.ifPresent(entry -> {
						if (Objects.nonNull(entry.getCacheKey()) || Objects.nonNull(entry.getBookingReference()))
						{
							removeAmountInfo(abstractOrderModel, entry.getCacheKey(), entry.getBookingReference());
						}
					});
		}

		bcfTravelCommerceStockService.releaseStocks(selectedODCartEntries);
		getModelService().removeAll(selectedODCartEntries);
		getModelService().refresh(abstractOrderModel);
		((BCFCommerceAddToCartStrategy) getCommerceAddToCartStrategy())
				.normalizeEntryNumbersIfNeeded((CartModel) abstractOrderModel);
		if (cartFilterParams.isCommit())
		{
			getModelService().refresh(abstractOrderModel);
		}
	}

	void removetravellers(final List<TravelOrderEntryInfoModel> travelOrderEntryInfos, final boolean isSingle)
	{
		List<TravellerModel> travellers = travelOrderEntryInfos.stream()
				.flatMap(travelOrderEntryInfo -> travelOrderEntryInfo.getTravellers().stream())
				.filter(traveller -> StringUtils.isEmpty(traveller.getSavedTravellerUid())).collect(Collectors.toList());

		travellers = travellers.stream()
				.filter(traveller -> travelOrderEntryInfos.containsAll(traveller.getTravelOrderEntryInfo()))
				.collect(Collectors.toList());
		if (!isSingle)
		{
			travellers = travellers.stream().filter(travellerModel -> TravellerType.PASSENGER.equals(travellerModel.getType()))
					.collect(
							Collectors.toList());
		}

		final List<TravellerInfoModel> travellersInfo = travellers.stream().map(TravellerModel::getInfo)
				.collect(Collectors.toList());
		getModelService().removeAll(travellersInfo);
		getModelService().removeAll(travellers);
	}


	void removeAmountInfo(final AbstractOrderModel abstractOrderModel, final String cacheKey, final String bookingRef)
	{
		AmountToPayInfoModel deleteAmountInfo = null;
		if (cacheKey != null)
		{
			deleteAmountInfo = abstractOrderModel.getAmountToPayInfos().stream()
					.filter(amountToPayInfoModel -> cacheKey.equals(amountToPayInfoModel.getCacheKey())).findFirst().orElse(null);
		}
		else
		{
			deleteAmountInfo = abstractOrderModel.getAmountToPayInfos().stream()
					.filter(amountToPayInfoModel -> bookingRef.equals(amountToPayInfoModel.getBookingReference())).findFirst()
					.orElse(null);
		}

		if (deleteAmountInfo != null)
		{
			getModelService().remove(deleteAmountInfo);
		}
	}

	/**
	 * update the entries for cart.
	 * <p>
	 * the cart model journeyRefNo odRefNo
	 *
	 * @return true, if successful
	 */
	private void updateVehicleTravellers(final List<TravellerModel> selectedVehicleTravellers,
			final List<TravellerModel> leftVehicleTravellers)
	{
		if (!selectedVehicleTravellers.isEmpty() && !leftVehicleTravellers.isEmpty())
		{
			final BCFVehicleInformationModel selectedTravellersInfo = (BCFVehicleInformationModel) selectedVehicleTravellers.get(0)
					.getInfo();

			final BCFVehicleInformationModel leftTravellersInfo = (BCFVehicleInformationModel) leftVehicleTravellers.get(0)
					.getInfo();

			if (selectedTravellersInfo.getVehicleType().getType() != leftTravellersInfo.getVehicleType().getType())
			{
				getModelService().removeAll(selectedTravellersInfo);
				getModelService().removeAll(selectedVehicleTravellers);
			}

		}
	}

	@Override
	public void saveAdditionalEmails(final List<String> emails)
	{
		final CartModel cartModel = getSessionCart();
		cartModel.setExtraEmailAddress(emails);
		getModelService().save(cartModel);
	}


	@Override
	public CartModel getSessionCart()
	{
		final CartModel cart = (CartModel) getUserSessionCart().orElse(super.getSessionCart());
		final SalesApplication salesApplication = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		final String sessionBookingJourneyType = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		final BookingJourneyType bookingJourneyType = getEnumerationService()
				.getEnumerationValue(BookingJourneyType.class, sessionBookingJourneyType);
		if (Objects.equals(salesApplication, cart.getSalesApplication()) && Objects
				.equals(bookingJourneyType, cart.getBookingJourneyType()))
		{
			return cart;
		}

		cart.setSalesApplication(salesApplication);
		if (bookingJourneyType != null && cart.getBookingJourneyType() == null)
		{
			cart.setBookingJourneyType(bookingJourneyType);
		}

		getModelService().save(cart);
		return cart;
	}


	@Override
	public Boolean saveAgentComment(final String agentComment)
	{
		final CartModel cartModel = getSessionCart();
		cartModel.setAgentComment(agentComment);
		getModelService().save(cartModel);
		return true;
	}


	@Override
	public Boolean saveReferenceCodes(final List<String> referenceCodes, final List<String> bookingRefs)
	{
		final CartModel cartModel = getSessionCart();

		if (CollectionUtils.isNotEmpty(referenceCodes))
		{
			for (final String bookingRef : bookingRefs)
			{
				final AmountToPayInfoModel amountToPayInfoModel = cartModel.getAmountToPayInfos().stream()
						.filter(amountToPayInfo -> bookingRef
								.equals(amountToPayInfo.getBookingReference())).findFirst().orElse(null);
				if (amountToPayInfoModel != null)
				{
					amountToPayInfoModel.setReferenceCode(referenceCodes.get(bookingRefs.indexOf(bookingRef)));
					getModelService().save(amountToPayInfoModel);
				}
			}
			getModelService().refresh(cartModel);
		}
		return true;
	}


	@Override
	public String getAgentComment()
	{
		final CartModel cartModel = getSessionCart();
		return cartModel.getAgentComment();
	}


	@Override
	public List<AbstractOrderEntryModel> getCartEntriesForRefNo(final int journeyRefNo, final int odRefNo)
	{
		return getUserSessionCart()
				.map(abstractOrderModel -> getCartEntriesForRefNo(journeyRefNo, odRefNo, abstractOrderModel))
				.orElse(Collections.emptyList());
	}



	@Override
	public List<AbstractOrderEntryModel> getAllTransportCartEntries()
	{
		if (!hasSessionCart())
		{

			return Collections.emptyList();
		}
		return Optional.ofNullable(getSessionCart().getEntries()).orElse(Collections.emptyList()).stream()
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()) && !(entry
						.getProduct() instanceof FeeProductModel))
				.collect(Collectors.toList());
	}



	@Override
	public List<AbstractOrderEntryModel> getAllCartEntriesForRefNo(final int journeyRefNo, final int odRefNo)
	{
		return getUserSessionCart()
				.map(abstractOrderModel -> getAllCartEntriesForRefNo(journeyRefNo, odRefNo, abstractOrderModel))
				.orElse(Collections.emptyList());
	}


	@Override
	public CartModel getExistingSessionAmendedCart()
	{
		if (hasSessionCart())
		{
			final CartModel sessionCart = getSessionCart();
			if (Objects.nonNull(sessionCart.getOriginalOrder()) || (Objects.nonNull(sessionCart) && Objects
					.nonNull(sessionCart.getCreatedFromEBooking()) && Boolean.TRUE.equals(sessionCart.getCreatedFromEBooking())))
			{
				return sessionCart;
			}
		}
		return null;
	}


	@Override
	public CartModel createCartFromOrder(final String orderCode, final String guid)
	{
		final OrderModel orderModel = getOrder(orderCode);
		changeUserSessionCurrency(orderModel);
		final List<ItemModel> itemModels = new ArrayList<>();

		final List<CartModel> oldCarts = getTravelCartDao().findCartsForOriginalOrder(orderModel);
		if (CollectionUtils.isNotEmpty(oldCarts))
		{
			oldCarts.forEach(this::removeCart);
		}

		final CartModel cartModel = getBcfCloneOrderStrategy().getNewCartFromOrder(orderModel);
		cartModel.setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel());
		cartModel.setUser(getUserService().getUserForUID(guid));
		cartModel.setQuoteExpirationDate(null);
		cartModel.setQuote(false);
		cartModel.setQuoteExpiryDate(null);
		cartModel.setCreatedFromEBooking(Boolean.FALSE);
		cartModel.setStockHoldTime(0);
		cartModel.setTimerDisplayed(false);
		cartModel.setAboutToExpireDisplayed(false);
		getModelService().save(cartModel);

		cartModel.getEntries().forEach(entry -> {
			entry.setCacheKey(null);
			entry.setAmendStatus(AmendStatus.SAME);

		});
		itemModels.addAll(cartModel.getEntries());

		itemModels.add(cartModel);


		if (CollectionUtils.isNotEmpty(cartModel.getPaymentTransactions()))
		{
			cartModel.getPaymentTransactions().forEach(transaction ->
			{
				transaction.setExisting(true);
				transaction.getEntries().forEach(transactionEntry -> transactionEntry.setExisting(true));
			});
			itemModels.addAll(
					cartModel.getPaymentTransactions().stream().flatMap(transaction -> transaction.getEntries().stream()).collect(
							Collectors.toList()));

			itemModels.addAll(cartModel.getPaymentTransactions());

		}

		getModelService().saveAll(itemModels);

		return cartModel;
	}


	@Override
	public void changeUserSessionCurrency(final AbstractOrderModel orderModel)
	{
		final String currentCurrencyIsocode = getCommonI18NService().getCurrentCurrency().getIsocode();
		final String orderCurrency = orderModel.getCurrency().getIsocode();
		if (!StringUtils.equalsIgnoreCase(currentCurrencyIsocode, orderCurrency))
		{
			forceCurrencyToOriginalOrderCurrency(orderCurrency);
		}
	}


	@Override
	public List<String> getOrderCachingKeys(final AbstractOrderModel order)
	{
		if (Objects.isNull(order) || CollectionUtils.isEmpty(order.getEntries()))
		{
			return Collections.emptyList();
		}

		return order.getEntries()
				.stream()
				.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE))
				.map(AbstractOrderEntryModel::getCacheKey)
				.distinct()
				.collect(Collectors.toList());
	}


	@Override
	public void updateCart(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingResponse response)

	{
		final List<ItemModel> itemstoSave = new ArrayList<>();
		for (final SailingLine sailingLine : response.getItinerary().getSailing().getLine())
		{
			for (final SailingPrices sailingPrice : sailingLine.getSailingPrices())
			{
				sailingPrice.getProductFares().forEach(productFare -> {
					if (Objects.nonNull(makeBookingRequestData.getProductCodeEntryMap().get(productFare.getProduct().getProductId())))
					{
						final List<AbstractOrderEntryModel> productEntries = makeBookingRequestData.getProductCodeEntryMap()
								.get(productFare.getProduct().getProductId());
						itemstoSave.addAll(productEntries);
						if (makeBookingRequestData.isBookingRefUpdate())
						{
							final String newBookingRef = response.getItinerary().getBooking().getBookingReference()
									.getBookingReference();
							makeBookingRequestData.getCartModel().getAmountToPayInfos().stream()
									.forEach(amountToPayInfoModel -> {
												if (StringUtils.equals(amountToPayInfoModel.getBookingReference(),
														productEntries.get(0).getBookingReference()))
												{
													amountToPayInfoModel.setBookingReference(newBookingRef);
													itemstoSave.add(amountToPayInfoModel);
												}
											}
									);
						}
						updateProductEntriesAndMakeBookingRequestData(productEntries, productFare, response, makeBookingRequestData);
					}
				});
			}
		}

		if (OptionBookingUtil.isOptionBooking(makeBookingRequestData.getCartModel()))
		{
			makeBookingRequestData.getCartModel()
					.setQuoteExpirationDate(OptionBookingUtil.getExpiryDate(makeBookingRequestData.getCartModel()));
		}
		else
		{
			final int expirationTime = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.EBOOKING_MAX_QUOTATION_EXPIRATION_MINUTES));
			makeBookingRequestData.getCartModel().setQuoteExpirationDate(BCFDateUtils.addMinutes(new Date(), expirationTime));
		}


		if (CollectionUtils.isNotEmpty(itemstoSave))
		{
			getModelService().saveAll(itemstoSave);
		}
		getModelService().save(makeBookingRequestData.getCartModel());

		try
		{
			bcfCalculationService.recalculate(makeBookingRequestData.getCartModel());
		}
		catch (final CalculationException ex)
		{
			LOG.error("error while calculating cart", ex);
		}
	}

	private void updateProductEntriesAndMakeBookingRequestData(final List<AbstractOrderEntryModel> productEntries,
			final ProductFare productFare, final BCFMakeBookingResponse response,
			final MakeBookingRequestData makeBookingRequestData)
	{
		productEntries.forEach(productEntry -> {
			productEntry.setBasePrice(productFare.getFareDetail().stream().findFirst().get().getNetAmountInCents()
					/ (100.0 * productFare.getProduct().getProductNumber()));
			productEntry.setMarginCalculated(false);
			if (response.getItinerary().getBooking().getBookingReference().getCachingKey() != null)
			{
				productEntry.setCacheKey(response.getItinerary().getBooking().getBookingReference().getCachingKey());
			}
			if (response.getItinerary().getBooking().getBookingReference().getBookingReference() != null)
			{
				productEntry.setBookingReference(
						response.getItinerary().getBooking().getBookingReference().getBookingReference());
			}

			if (!makeBookingRequestData.getCachingKeys()
					.contains(response.getItinerary().getBooking().getBookingReference().getCachingKey()))
			{
				makeBookingRequestData.getCachingKeys()
						.add(response.getItinerary().getBooking().getBookingReference().getCachingKey());
			}
		});
	}


	@Override
	public void updateAmountToPayInfo(final CartModel cartModel)
	{
		cartModel.getAmountToPayInfos().stream().forEach(info -> {

			if (info.getAmountToPay() != null && info.getAmountToPay() > 0)
			{
				info.setPreviouslyPaid(info.getAmountToPay());
			}

			info.setAmountToPay(0.0);
		});
		getModelService().saveAll(cartModel.getAmountToPayInfos());
	}


	@Override
	public void addPropertiesToCartEntry(
			final CartModel masterCartModel, final int orderEntryNo, final ProductModel product,
			final Map<String, Object> propertiesMap)
	{
		ServicesUtil.validateParameterNotNull(product, "Parameter productModel must not be null");
		ServicesUtil.validateParameterNotNull(orderEntryNo, "Parameter uniqueIdentifier must not be null");
		ServicesUtil.validateParameterNotNull(masterCartModel, "Parameter masterCartModel must not be null");
		ServicesUtil.validateParameterNotNull(propertiesMap, "Parameter propertiesMap must not be null");

		final AbstractOrderEntryModel orderEntryModel = getEntryForNumber(masterCartModel, orderEntryNo);

		if (Objects.nonNull(orderEntryModel))
		{
			final AbstractOrderEntryModel orderEntry = orderEntryModel;

			if (!MapUtils.isEmpty(propertiesMap) && !Objects.isNull(orderEntry))
			{
				if (propertiesMap.containsKey(AbstractOrderEntryModel.ACTIVE))
				{
					this.getModelService().setAttributeValue(orderEntry, AbstractOrderEntryModel.ACTIVE,
							propertiesMap.get(AbstractOrderEntryModel.ACTIVE));
					propertiesMap.remove(AbstractOrderEntryModel.ACTIVE);
				}

				if (propertiesMap.containsKey(AbstractOrderEntryModel.AMENDSTATUS))
				{
					getModelService().setAttributeValue(orderEntry, AbstractOrderEntryModel.AMENDSTATUS,
							propertiesMap.get(AbstractOrderEntryModel.AMENDSTATUS));
					propertiesMap.remove(AbstractOrderEntryModel.AMENDSTATUS);
				}

				final ActivityOrderEntryInfoModel orderEntryInfo = orderEntry.getActivityOrderEntryInfo() == null ?
						(ActivityOrderEntryInfoModel) this.getModelService().create(ActivityOrderEntryInfoModel.class) :
						orderEntry.getActivityOrderEntryInfo();
				propertiesMap.entrySet().stream().forEach(entry ->
						getModelService().setAttributeValue(orderEntryInfo, entry.getKey(), entry.getValue())
				);
				orderEntry.setActivityOrderEntryInfo(orderEntryInfo);
				getModelService().save(orderEntry);
			}



		}
	}


	@Override
	public void updateDealOrderEntryGroup(final CartModel cartModel, final int journeyRefNumber, final List<Integer> entryNumbers)
	{

		final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getJourneyReferenceNumber() == journeyRefNumber)
				.findFirst().get().getEntryGroup();
		final List<ItemModel> itemsToSave = new ArrayList<>();
		for (final Integer entryNumber : entryNumbers)
		{
			final AbstractOrderEntryModel orderEntryModel = getEntryForNumber(cartModel, entryNumber);
			orderEntryModel.setEntryGroup(dealOrderEntryGroupModel);
			itemsToSave.add(orderEntryModel);
		}

		getModelService().saveAll(itemsToSave);
	}


	@Override
	public boolean hasValidCacheKeyOrBookingRef(final CartModel cartModel)
	{

		final List<AbstractOrderEntryModel> updatedTransportEntries = cartModel.getEntries().stream().filter(
				entry -> entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
						&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
				Collectors.toList());

		if (CollectionUtils.isNotEmpty(updatedTransportEntries))
		{
			final AbstractOrderEntryModel abstractOrderEntryModel = updatedTransportEntries.stream()
					.filter(entry -> entry.getBookingReference() != null || entry.getCacheKey() != null).findAny().orElse(null);
			return abstractOrderEntryModel != null;
		}
		return false;

	}


	@Override
	public boolean hasValidCacheKeyOrBookingRef(final CartModel cartModel, final AbstractOrderEntryModel entry)
	{
		if (isAmendmentCart(cartModel) || CustomerType.GUEST.equals(((CustomerModel) cartModel.getUser()).getType())
				|| AccountType.SUBSCRIPTION_ONLY.equals(((CustomerModel) cartModel.getUser()).getAccountType()))
		{
			return entry.getCacheKey() != null;
		}
		else
		{

			return entry.getBookingReference() != null;
		}
	}


	@Override
	public boolean useCacheKey(final AbstractOrderModel cartModel)
	{

		final List<AbstractOrderEntryModel> updatedTransportEntries = cartModel.getEntries().stream().filter(
				entry -> entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
						&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
				Collectors.toList());

		if (CollectionUtils.isNotEmpty(updatedTransportEntries))
		{
			final AbstractOrderEntryModel abstractOrderEntryModel = updatedTransportEntries.stream()
					.filter(entry -> entry.getBookingReference() != null).findAny().orElse(null);
			return abstractOrderEntryModel == null;
		}

		return true;

	}


	@Override
	public boolean isGuestUserCart(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel != null)
		{
			final ItemModel customer = abstractOrderModel.getUser();
			return customer != null && customer instanceof CustomerModel && CustomerType.GUEST
					.equals(((CustomerModel) customer).getType());
		}
		return false;
	}

	private List<AbstractOrderEntryModel> getCartEntriesForRefNo(final int journeyRefNo, final int odRefNo,
			final AbstractOrderModel abstractOrderModel)
	{
		return StreamUtil.safeStream(getAllCartEntriesForRefNo(journeyRefNo, odRefNo, abstractOrderModel))
				.filter(entry -> !(entry.getProduct() instanceof FeeProductModel))
				.collect(Collectors.toList());
	}

	private List<AbstractOrderEntryModel> getAllCartEntriesForRefNo(final int journeyRefNo, final int odRefNo,
			final AbstractOrderModel abstractOrderModel)
	{
		return StreamUtil.safeStream(abstractOrderModel.getEntries())
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo()) && entry.getJourneyReferenceNumber() == journeyRefNo
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNo)
				.collect(Collectors.toList());
	}




	@Override
	public Map<String, List<AbstractOrderEntryModel>> groupEntriesByCacheKeyOrBookingRef(final AbstractOrderModel source)
	{


		final List<AbstractOrderEntryModel> updatedTransportEntries = StreamUtil.safeStream(source.getEntries()).filter(
				entry -> entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
						&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
				Collectors.toList());

		if (CollectionUtils.isNotEmpty(updatedTransportEntries))
		{

			final List<AbstractOrderEntryModel> bookingRefEntries = updatedTransportEntries.stream()
					.filter(entry -> entry.getBookingReference() != null).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(bookingRefEntries))
			{
				return bookingRefEntries.stream().collect(Collectors.groupingBy(entry -> entry.getBookingReference()));
			}
			else
			{
				return updatedTransportEntries.stream().filter(entry -> entry.getCacheKey() != null)
						.collect(Collectors.groupingBy(entry -> entry.getCacheKey()));
			}
		}

		return Collections.emptyMap();
	}


	@Override
	public AbstractOrderModel getOrCreateSessionCart()
	{
		AbstractOrderModel sessionCart = null;
		if (hasSessionCart())
		{
			sessionCart = getSessionService().getAttribute(SESSION_CART_PARAMETER_NAME);
		}
		sessionCart = getSessionCart();

		final AssistedServiceSession asmSession = getSessionService().getAttribute(ASM_SESSION_PARAMETER);
		if (Objects.nonNull(sessionCart) && Objects.nonNull(asmSession) && Objects.nonNull(asmSession.getAgent()))
		{
			final EmployeeModel asmAgent = (EmployeeModel) asmSession.getAgent();
			if (asmAgent != null)
			{
				sessionCart.setAgent(asmAgent);
				getModelService().save(sessionCart);
			}
		}

		return sessionCart;
	}

	/**
	 * This is minimum requirement to be a valid cart.
	 *
	 * @param cart
	 * @return
	 */

	@Override
	public boolean isCartValidWithMinimumRequirements(final AbstractOrderModel cart)
	{
		return cart != null && !CollectionUtils.isEmpty(cart.getEntries());
	}

	private Optional<AbstractOrderModel> getUserSessionCart()
	{
		if (hasSessionCart())
		{
			return Optional.ofNullable(getSessionService().getAttribute(SESSION_CART_PARAMETER_NAME));
		}
		return Optional.ofNullable(null);
	}



	@Override
	public boolean isAlacateFlow()
	{

		final Object sessionJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		final String salesChannel = bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode();

		if (salesChannel != null && BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(
				BcfCoreConstants.ALCARTE_SALES_CHANNELS)).contains(salesChannel))
		{

			if (sessionJourney == null)
			{
				return true;
			}

			if (sessionJourney.equals(BcfCoreConstants.BOOKING_TRANSPORT_ONLY))
			{

				if (!isAmendmentCart(getSessionCart()))
				{
					return true;
				}

			}
			else if (!sessionJourney
					.equals(BcfCoreConstants.BOOKING_PACKAGE))
			{
				return true;
			}


		}
		return false;

	}

	@Override
	public boolean hasCommercialVehiclesOnly()
	{
		final CartModel cartModel = getSessionCart();
		final List<TravellerModel> travellerModels = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()))
				.flatMap(entry -> entry.getTravelOrderEntryInfo().getTravellers().stream())
				.filter(e -> e.getType().equals(TravellerType.VEHICLE)).collect(Collectors.toList());
		final List<TravellerInfoModel> travellersInfo = travellerModels.stream().map(TravellerModel::getInfo)
				.collect(Collectors.toList());
		final long vehicleCount = travellersInfo.stream().count();
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(cartModel.getBookingJourneyType()) && vehicleCount > 0
				&& cartModel.getAmountToPayInfos().size() == vehicleCount)
		{
			int diff = 0;
			final List<String> commercialVehicleCodes = BCFPropertiesUtils
					.convertToList(
							getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
			for (final TravellerInfoModel travellerInfoModel : travellersInfo)
			{
				final BCFVehicleInformationModel bcfVehicleInformationModel = ((BCFVehicleInformationModel) travellerInfoModel);
				final String vehicleCode = bcfVehicleInformationModel.getVehicleType().getType().getCode();
				if (commercialVehicleCodes.contains(vehicleCode) || BooleanUtils
						.toBoolean(bcfVehicleInformationModel.getCarryingLivestock()))
				{
					diff++;
				}
			}
			return vehicleCount == diff;
		}
		return false;
	}

	@Override
	public boolean hasCommercialVehicle(final AbstractOrderModel order)
	{
		final List<TravellerModel> travellerModels = order.getEntries().stream()
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()))
				.flatMap(entry -> entry.getTravelOrderEntryInfo().getTravellers().stream())
				.filter(e -> e.getType().equals(TravellerType.VEHICLE)).collect(Collectors.toList());
		final List<TravellerInfoModel> travellersInfo = travellerModels.stream().map(TravellerModel::getInfo)
				.collect(Collectors.toList());
		final long vehicleCount = travellersInfo.stream().count();
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(order.getBookingJourneyType()) && vehicleCount > 0)
		{
			final List<String> commercialVehicleCodes = BCFPropertiesUtils.convertToList(
					getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
			for (final TravellerInfoModel travellerInfoModel : travellersInfo)
			{
				final BCFVehicleInformationModel bcfVehicleInformationModel = ((BCFVehicleInformationModel) travellerInfoModel);
				final String vehicleCode = bcfVehicleInformationModel.getVehicleType().getType().getCode();
				if (commercialVehicleCodes.contains(vehicleCode) || BooleanUtils
						.toBoolean(bcfVehicleInformationModel.getCarryingLivestock()))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int getThresholdTimeGap(final AbstractOrderModel order)
	{
		final int defaultThreshold = Integer.parseInt(getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfCoreConstants.DEFAULT_TRANSPORT_BOOKING_THRESHOLD_MINUTES));
		if (hasCommercialVehicle(order))
		{
			final int commercialVehThreshold = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_TRANSPORT_BOOKING_THRESHOLD_MINUTES));
			return Math.min(defaultThreshold, commercialVehThreshold);
		}
		return defaultThreshold;
	}

	@Override
	public void removeSessionCart()
	{
		if (hasSessionCart())
		{
			removeCart(getSessionCart());
		}
	}

	@Override
	public Map<OrderEntryType, List<AbstractOrderEntryModel>> splitCartEntriesByType(final AbstractOrderModel orderModel)
	{
		final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries = new HashMap<>();
		for (final AbstractOrderEntryModel e : orderModel.getEntries())
		{
			if (typeWiseEntries.containsKey(e.getType()))
			{
				typeWiseEntries.get(e.getType()).add(e);
			}
			else
			{
				typeWiseEntries.put(e.getType(), new ArrayList<>(Arrays.asList(e)));
			}
		}
		return typeWiseEntries;
	}

	@Override
	public boolean hasCacheEntriesToConvert()
	{
		if (hasSessionCart())
		{
			final CartModel cartModel = (CartModel) getOrCreateSessionCart();
			if (!getUserService().isAnonymousUser(cartModel.getUser())
					&& AccountType.FULL_ACCOUNT.equals(((CustomerModel) cartModel.getUser()).getAccountType())
					&& CollectionUtils.isNotEmpty(cartModel.getEntries())
					&& CollectionUtils.isNotEmpty(StreamUtil.safeStream(cartModel.getEntries()).filter(entry ->
					entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
							&& Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& StringUtils.isNotBlank(entry.getCacheKey())).collect(Collectors.toList())))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void resetAmountToPay(final CartModel cartModel)
	{
		if (isAmendmentCart(getSessionCart()))
		{
			cartModel.setAmountPaid(cartModel.getOriginalOrder().getAmountPaid());
		}
		else
		{
			cartModel.setAmountPaid(0d);
		}

		getModelService().save(cartModel);
		try
		{
			bcfCalculationService.calculateTotals(cartModel, false);
		}
		catch (final CalculationException e)
		{
			LOG.error("Failed to calculate cart totals [" + cartModel.getCode() + "]", e);
		}

	}

	@Override
	public boolean isCardPayment(final AbstractOrderModel abstractOrderModel)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			final PaymentTransactionModel paymentTransactionModel = abstractOrderModel.getPaymentTransactions()
					.get(abstractOrderModel.getPaymentTransactions().size() - 1);
			return (paymentTransactionModel.getInfo() instanceof CreditCardPaymentInfoModel
					|| paymentTransactionModel.getInfo() instanceof CTCTCCardPaymentInfoModel) && CollectionUtils
					.isEmpty(paymentTransactionModel.getEntries());
		}
		return false;
	}

	@Override
	public boolean isOriginalOrderOnRequest(final AbstractOrderModel abstractOrderModel)
	{
		final OrderModel originalOrder = abstractOrderModel.getOriginalOrder();
		if (originalOrder != null && isCardPayment(originalOrder)
				&& BcfCoreConstants.PACKAGE_ON_REQUEST
				.equals(originalOrder.getAvailabilityStatus()))
		{
			return true;
		}
		return false;
	}


	public BcfStockLevelDao getBcfStockLevelDao()
	{
		return bcfStockLevelDao;
	}

	public void setBcfStockLevelDao(final BcfStockLevelDao bcfStockLevelDao)
	{
		this.bcfStockLevelDao = bcfStockLevelDao;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}


	/**
	 * @return the bcfSalesApplicationResolverService
	 */
	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	/**
	 * @param bcfSalesApplicationResolverService the bcfSalesApplicationResolverService to set
	 */
	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	/**
	 * @return the bcfCloneOrderStrategy
	 */
	protected BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	/**
	 * @param bcfCloneOrderStrategy the bcfCloneOrderStrategy to set
	 */
	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public CommerceAddToCartStrategy getCommerceAddToCartStrategy()
	{
		return commerceAddToCartStrategy;
	}

	public void setCommerceAddToCartStrategy(final CommerceAddToCartStrategy commerceAddToCartStrategy)
	{
		this.commerceAddToCartStrategy = commerceAddToCartStrategy;
	}


	@Override
	public SessionService getSessionService()
	{
		return sessionService;
	}


	@Override
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}
}
