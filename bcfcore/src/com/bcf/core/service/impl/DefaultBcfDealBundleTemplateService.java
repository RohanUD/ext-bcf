/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.services.impl.DefaultDealBundleTemplateService;
import com.bcf.core.dao.DealBundleTemplateDao;
import com.bcf.core.service.BcfDealBundleTemplateService;


public class DefaultBcfDealBundleTemplateService extends DefaultDealBundleTemplateService implements BcfDealBundleTemplateService
{

	private DealBundleTemplateDao dealBundleTemplateDao;


	@Override
	public DealBundleTemplateModel getDealBundleTemplateBySeoUrl(final String seoUrl, final CatalogVersionModel catalogVersion)
	{
		return getDealBundleTemplateDao().findDealBundleTemplateBySeoUrl(seoUrl,catalogVersion);
	}

	public DealBundleTemplateDao getDealBundleTemplateDao()
	{
		return dealBundleTemplateDao;
	}

	public void setDealBundleTemplateDao(final DealBundleTemplateDao dealBundleTemplateDao)
	{
		this.dealBundleTemplateDao = dealBundleTemplateDao;
	}
}
