/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.services.BookingService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.dao.BCFGoodWillRefundDao;
import com.bcf.core.model.DeclareOrderEntryModel;
import com.bcf.core.model.GoodWillRefundModel;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BCFGoodWillRefundService;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.integration.payment.exceptions.BcfRefundException;
import com.bcf.integration.payment.exceptions.InvalidPaymentInfoException;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public class DefaultBCFGoodWillRefundService implements BCFGoodWillRefundService
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFGoodWillRefundService.class);
	private static final String GOODWILL = "GOODWILL";
	public static final String FAILED_WITH_REASON_CODE = "processing refund failed with reason code: ";

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	private BcfRefundIntegrationService bcfRefundIntegrationService;

	private BCFGoodWillRefundDao bcfGoodWillRefundDao;

	private PersistentKeyGenerator refundCodeGenerator;


	public BcfRefundIntegrationService getBcfRefundIntegrationService()
	{
		return bcfRefundIntegrationService;
	}

	public void setBcfRefundIntegrationService(final BcfRefundIntegrationService bcfRefundIntegrationService)
	{
		this.bcfRefundIntegrationService = bcfRefundIntegrationService;
	}

	public PersistentKeyGenerator getRefundCodeGenerator()
	{
		return refundCodeGenerator;
	}

	public void setRefundCodeGenerator(final PersistentKeyGenerator refundCodeGenerator)
	{
		this.refundCodeGenerator = refundCodeGenerator;
	}

	private AssistedServiceService assistedServiceService;

	private ModelService modelService;

	private BookingService bookingService;

	private CalculationService calculationService;


	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public BookingService getBookingService()
	{
		return bookingService;
	}

	public void setBookingService(final BookingService bookingService)
	{
		this.bookingService = bookingService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BCFGoodWillRefundDao getBcfGoodWillRefundDao()
	{
		return bcfGoodWillRefundDao;
	}

	public void setBcfGoodWillRefundDao(final BCFGoodWillRefundDao bcfGoodWillRefundDao)
	{
		this.bcfGoodWillRefundDao = bcfGoodWillRefundDao;
	}

	@Override
	public List<GoodWillRefundModel> getAllGoodWillRefunds()
	{
		return bcfGoodWillRefundDao.find();
	}

	@Override
	public GoodWillRefundModel getGoodWillRefund(final String code)
	{

		return bcfGoodWillRefundDao.getGoodWillRefund(code);
	}

	@Override
	public void processGoodWillRefund(final String bookingReference, final String reasonCode, final String comment,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas) throws BcfRefundException, Exception
	{
			Transaction.current().execute(new TransactionBody()
			{
				@Override
				public Object execute() throws BcfRefundException
				{
					if(CollectionUtils.isNotEmpty(refundTransactionInfoDatas))
					{
						final List<ItemModel> itemsToSave = new ArrayList<>();
						final OrderModel orderModel = bookingService.getOrderModelFromStore(bookingReference);
						final GoodWillRefundOrderInfoModel goodWillRefundOrderInfo = modelService
								.create(GoodWillRefundOrderInfoModel.class);
						final Double goodwillRefundAmount = refundTransactionInfoDatas.stream()
								.mapToDouble(refundTransactionInfoData -> refundTransactionInfoData.getAmount()).sum();
						goodWillRefundOrderInfo.setAmount(goodwillRefundAmount);
						goodWillRefundOrderInfo.setReasonCode(reasonCode);
						goodWillRefundOrderInfo.setComment(comment);
						goodWillRefundOrderInfo.setCode(String.valueOf(refundCodeGenerator.generate()));
						goodWillRefundOrderInfo.setOrder(orderModel);
						if (assistedServiceService.getAsmSession() != null && assistedServiceService.getAsmSession().getAgent() != null)
						{
							goodWillRefundOrderInfo.setAgent(assistedServiceService.getAsmSession().getAgent());
						}
						itemsToSave.add(goodWillRefundOrderInfo);

						final List<GoodWillRefundOrderInfoModel> refundInfos = new ArrayList<>();
						refundInfos.add(goodWillRefundOrderInfo);
						final Collection<GoodWillRefundOrderInfoModel> orderRefundInfos = orderModel.getGoodwillRefundOrderInfos();
						if (CollectionUtils.isNotEmpty(orderRefundInfos))
						{
							refundInfos.addAll(orderRefundInfos);
						}
						if (orderModel.getAmountPaid() != null && (orderModel.getAmountPaid() - goodwillRefundAmount) > 0)
						{
							orderModel.setAmountPaid(PrecisionUtil.round(orderModel.getAmountPaid() - goodwillRefundAmount));
						}
						orderModel.setGoodwillRefundOrderInfos(refundInfos);
						orderModel.setCalculated(Boolean.FALSE);
						itemsToSave.add(orderModel);

						final DeclareOrderEntryModel declareOrderEntryModel = modelService.create(DeclareOrderEntryModel.class);
						declareOrderEntryModel.setComponentType(GOODWILL);
						declareOrderEntryModel.setTotalPrice(goodwillRefundAmount);
						declareOrderEntryModel.setAgent(orderModel.getAgent());
						declareOrderEntryModel.setSalesApplication(orderModel.getSalesApplication());
						itemsToSave.add(declareOrderEntryModel);

						modelService.saveAll(itemsToSave);

						try
						{
							calculationService.calculate(orderModel);
							final List<PaymentResponseDTO> paymentResponseDTOs = bcfRefundIntegrationService
									.processRefund(orderModel, refundTransactionInfoDatas);
							validatePaymentResponse(paymentResponseDTOs);

						}
						catch (final CalculationException ex)
						{
							LOG.error("Failed to calculate order [" + orderModel + "] for good will refund", ex);
							throw new BcfRefundException("Failed to calculate order [" + orderModel + "] for good will refund");

						}
						catch (final IntegrationException ex)
						{
							LOG.error("Payment Integration exception encountered while processing good will refund ", ex);
							throw new BcfRefundException("Payment Integration exception encountered while processing good will refund ");
						}
						catch (final InvalidPaymentInfoException ex)
						{
							throw new BcfRefundException(ex.getMessage());
						}
					}

					return null;
				}
			}
		);

	}

	private void validatePaymentResponse(final List<PaymentResponseDTO> paymentResponseDTOs) throws BcfRefundException
	{
		if (CollectionUtils.isNotEmpty(paymentResponseDTOs))
		{

			for (final PaymentResponseDTO paymentResponseDTO : paymentResponseDTOs)
			{
				final String bcfResponseCode = paymentResponseDTO.getResponseStatus()
						.getBcfResponseCode();
				if (!isPaymentSuccessful(paymentResponseDTO))
				{
					LOG.error(FAILED_WITH_REASON_CODE + bcfResponseCode);
					throw new BcfRefundException(
							FAILED_WITH_REASON_CODE + bcfResponseCode);
				}
			}
		}
	}


	protected boolean isPaymentSuccessful(final PaymentResponseDTO paymentResponseDTO)
	{
		return paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& StringUtils.equals(paymentResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode"));
	}

}
