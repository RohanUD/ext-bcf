/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfSalesApplicationResolverService;


public class DefaultBcfSalesApplicationResolverService implements BcfSalesApplicationResolverService
{
	private SessionService sessionService;

	@Override
	public SalesApplication getCurrentSalesChannel()
	{
		final String salesChannel = getSessionService().getAttribute(BcfCoreConstants.SALES_CHANNEL);
		if (StringUtils.isBlank(salesChannel))
		{
			return SalesApplication.WEB;
		}
		return SalesApplication.valueOf(salesChannel);
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
