/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.comments.model.CommentTypeModel;
import de.hybris.platform.comments.model.ComponentModel;
import de.hybris.platform.comments.model.DomainModel;
import de.hybris.platform.comments.services.CommentService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfAsmCommentsService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;



public class DefaultBcfAsmCommentsService implements BcfAsmCommentsService
{
	private BCFTravelCartService bcfTravelCartService;
	private BcfBookingService bcfBookingService;
	private ModelService modelService;
	private UserService userService;
	private CommentService commentService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	@Override
	public List<CommentModel> getAsmComments(final String orderCode)
	{


		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isEmpty(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}

		if (abstractOrder != null && CollectionUtils.isNotEmpty(abstractOrder.getComments()))
		{
			return abstractOrder.getComments().stream().collect(Collectors.toList());
		}

		return Collections.emptyList();

	}

	@Override
	public List<CommentModel> getAsmAccommodationSupplierComments(final String code, final String ref, final String orderCode)
	{

		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isBlank(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}
		final AbstractOrderEntryModel entry = getAccommodationEntry(abstractOrder, ref, code);

		if (entry != null && CollectionUtils.isNotEmpty(entry.getComments()))
		{
			return entry.getComments().stream().collect(Collectors.toList());
		}

		return Collections.emptyList();

	}

	@Override
	public List<CommentModel> getAsmAccommodationSupplierComments(final String code, final String ref,
			final AbstractOrderModel abstractOrder)
	{
		if (StringUtils.isBlank(code) || Objects.isNull(abstractOrder))
		{
			return Collections.emptyList();
		}
		final AbstractOrderEntryModel entry = getAccommodationEntry(abstractOrder, ref, code);

		if (entry != null && CollectionUtils.isNotEmpty(entry.getComments()))
		{
			return entry.getComments();
		}
		return Collections.emptyList();
	}


	@Override
	public List<CommentModel> getAsmAccommodationSupplierComments(final String code,
			final AbstractOrderModel abstractOrder)
	{

		if (StringUtils.isBlank(code) || Objects.isNull(abstractOrder))
		{
			return Collections.emptyList();
		}
		final List<AbstractOrderEntryModel> entries = getAccommodationEntries(abstractOrder, code);

		if (CollectionUtils.isNotEmpty(entries))
		{
			return entries.stream().filter(entry -> CollectionUtils.isNotEmpty(entry.getComments()))
					.flatMap(entry -> entry.getComments().stream()).collect(
							Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Override
	public List<CommentModel> getAsmActivitySupplierComments(final String code, final String orderCode)
	{

		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isBlank(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}

		final AbstractOrderEntryModel entry = getActivityEntry(abstractOrder, code);

		if (entry != null && CollectionUtils.isNotEmpty(entry.getComments()))
		{
			return entry.getComments().stream().collect(Collectors.toList());
		}
		return Collections.emptyList();

	}

	@Override
	public List<CommentModel> getAsmActivitySupplierComments(final String code,  final AbstractOrderModel abstractOrder)
	{
		if (StringUtils.isBlank(code) || Objects.isNull(abstractOrder))
		{
			return Collections.emptyList();
		}
		final AbstractOrderEntryModel entry = getActivityEntry(abstractOrder, code);

		if (entry != null && CollectionUtils.isNotEmpty(entry.getComments()))
		{
			return entry.getComments();
		}
		return Collections.emptyList();
	}

	private AbstractOrderEntryModel getAccommodationEntry(final AbstractOrderModel abstractOrder,
			final String ref, final String accommodationCode)
	{

		final List<AbstractOrderEntryModel> accommodationEntries = ChangeAndCancellationFeeCalculationHelper
				.getAccommodationEntries(abstractOrder.getEntries());

		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{
			for (final AbstractOrderEntryModel accommodationEntry : accommodationEntries)
			{
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = bcfTravelCommerceCartService
						.getAccommodationOrderEntryGroup(accommodationEntry);

				if (accommodationOrderEntryGroupModel.getAccommodation().getCode().equals(accommodationCode)
						&& accommodationOrderEntryGroupModel.getRoomStayRefNumber() == Integer.valueOf(ref))
				{

					return accommodationEntry;
				}

			}
		}
		return null;
	}

	private List<AbstractOrderEntryModel> getAccommodationEntries(final AbstractOrderModel abstractOrder,
			final String accommodationOfferingCode)
	{

		final List<AbstractOrderEntryModel> accommodationEntries = ChangeAndCancellationFeeCalculationHelper
				.getAccommodationEntries(abstractOrder.getEntries());

		final List<AbstractOrderEntryModel> entries = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{
			for (final AbstractOrderEntryModel accommodationEntry : accommodationEntries)
			{
				if (ChangeAndCancellationFeeCalculationHelper.getAccommodationOffering(accommodationEntry).getCode()
						.equals(accommodationOfferingCode))
				{

					entries.add(accommodationEntry);
				}

			}
		}
		return entries;
	}


	private AbstractOrderEntryModel getActivityEntry(final AbstractOrderModel abstractOrder, final String activityCode)
	{

		final List<AbstractOrderEntryModel> activityEntries = ChangeAndCancellationFeeCalculationHelper
				.getActivityEntries(abstractOrder.getEntries());

		if (CollectionUtils.isNotEmpty(activityEntries))
		{
			for (final AbstractOrderEntryModel activityEntry : activityEntries)
			{
				if (activityEntry.getProduct().getCode().equals(activityCode))
				{
					return activityEntry;
				}
			}
		}
		return null;
	}


	private List<AbstractOrderEntryModel> getActivityEntries(final AbstractOrderModel abstractOrder, final String activityCode)
	{
		final List<AbstractOrderEntryModel> entries = new ArrayList<>();

		final List<AbstractOrderEntryModel> activityEntries = ChangeAndCancellationFeeCalculationHelper
				.getActivityEntries(abstractOrder.getEntries());

		if (CollectionUtils.isNotEmpty(activityEntries))
		{
			for (final AbstractOrderEntryModel activityEntry : activityEntries)
			{
				if (activityEntry.getProduct().getCode().equals(activityCode))
				{
					entries.add(activityEntry);
				}
			}
		}
		return entries;
	}



	private List<AbstractOrderEntryModel> getAccommodationEntries(final AbstractOrderModel abstractOrder, final String ref,
			final String code)
	{
		final List<AbstractOrderEntryModel> entries = new ArrayList<>();

		final List<AbstractOrderEntryModel> accommodationEntries = ChangeAndCancellationFeeCalculationHelper
				.getAccommodationEntries(abstractOrder.getEntries());

		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{

			for (final AbstractOrderEntryModel accommodationEntry : accommodationEntries)
			{
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = bcfTravelCommerceCartService
						.getAccommodationOrderEntryGroup(accommodationEntry);

				if (accommodationOrderEntryGroupModel.getAccommodation().getCode().equals(code)
						&& accommodationOrderEntryGroupModel.getRoomStayRefNumber() == Integer.valueOf(ref))
				{

					entries.add(accommodationEntry);
				}
			}
		}
		return entries;
	}

	@Override
	public void addAsmAccommodationSupplierComment(final String code, final String ref, final String comment, final String agentId,
			final String orderCode)
	{

		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isEmpty(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}


		final List<AbstractOrderEntryModel> entries = getAccommodationEntries(abstractOrder, ref, code);
		if (CollectionUtils.isNotEmpty(entries))
		{
			saveSupplierComment(comment, agentId, entries);
		}

	}

	@Override
	public void addAsmActivitySupplierComment(final String code, final String comment, final String agentId,
			final String orderCode)
	{

		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isEmpty(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}


		final List<AbstractOrderEntryModel> entries = getActivityEntries(abstractOrder, code);

		if (CollectionUtils.isNotEmpty(entries))
		{
			saveSupplierComment(comment, agentId, entries);
		}

	}

	private void saveSupplierComment(final String comment, final String agentId, final List<AbstractOrderEntryModel> entries)
	{
		final List<ItemModel> itemsToSave = new ArrayList<>();

		for (final AbstractOrderEntryModel entry : entries)
		{
			if (entry != null)
			{
				final List<CommentModel> supplierComments = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(entry.getComments()))
				{
					supplierComments.addAll(entry.getComments());
				}
				final CommentModel asmCommentModel = new CommentModel();

				if (agentId != null)
				{
					asmCommentModel.setAuthor(userService.getUserForUID(agentId));
				}
				asmCommentModel.setText(comment);
				final DomainModel domainModel = commentService.getDomainForCode("asmAgentDomain");
				final ComponentModel componentModel = commentService.getComponentForCode(domainModel, "asmAgentComponent");
				final CommentTypeModel commentTypeModel = commentService.getCommentTypeForCode(componentModel, "asmAgentComment");
				asmCommentModel.setCommentType(commentTypeModel);
				asmCommentModel.setComponent(componentModel);
				itemsToSave.add(asmCommentModel);
				supplierComments.add(asmCommentModel);
				entry.setComments(supplierComments);
				itemsToSave.add(entry);

			}

			modelService.saveAll(itemsToSave);
		}
	}


	@Override
	public void addAsmComment(final String orderCode, final String comment, final String agentId)
	{
		final List<ItemModel> itemsToSave = new ArrayList<>();
		AbstractOrderModel abstractOrder = null;
		if (StringUtils.isEmpty(orderCode))
		{
			abstractOrder = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrder = bcfBookingService.getOrder(orderCode);
		}
		final List<CommentModel> internalComments = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(abstractOrder.getComments()))
		{
			internalComments.addAll(abstractOrder.getComments());
		}
		final CommentModel asmCommentModel = new CommentModel();
		if (agentId != null)
		{
			asmCommentModel.setAuthor(userService.getUserForUID(agentId));
		}
		asmCommentModel.setText(comment);
		final DomainModel domainModel = commentService.getDomainForCode("asmAgentDomain");
		final ComponentModel componentModel = commentService.getComponentForCode(domainModel, "asmAgentComponent");
		final CommentTypeModel commentTypeModel = commentService.getCommentTypeForCode(componentModel, "asmAgentComment");
		asmCommentModel.setCommentType(commentTypeModel);
		asmCommentModel.setComponent(componentModel);
		itemsToSave.add(asmCommentModel);
		internalComments.add(asmCommentModel);
		abstractOrder.setComments(internalComments);
		itemsToSave.add(abstractOrder);
		modelService.saveAll(itemsToSave);

	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public CommentService getCommentService()
	{
		return commentService;
	}

	@Required
	public void setCommentService(final CommentService commentService)
	{
		this.commentService = commentService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}
}
