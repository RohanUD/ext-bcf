/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import static com.bcf.core.constants.BcfCoreConstants.HYPHEN;
import static com.bcf.core.constants.BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT;
import static com.bcf.core.constants.BcfCoreConstants.TEXT_PLUS;
import static com.bcf.core.constants.BcfCoreConstants.TEXT_YEARS;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.impl.DefaultPassengerTypeService;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.BCFPassengerTypeDao;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.service.BCFPassengerTypeService;


public class DefaultBCFPassengerTypeService extends DefaultPassengerTypeService implements BCFPassengerTypeService
{
	private ModelService modelService;

	@Override
	public PassengerTypeModel getPassengerForEBookingCode(final String eBookingCode)
	{
		final PassengerTypeModel passenger = ((BCFPassengerTypeDao) getPassengerTypeDao())
				.findPassengerModelForEBookingCode(eBookingCode);
		if (Objects.nonNull(passenger))
		{
			return passenger;
		}
		return null;
	}

	@Override
	public List<PassengerTypeModel> getPassengerTypes()
	{
		return getPassengerTypeDao().findPassengerTypes();
	}

	@Override
	public List<PassengerTypeModel> getPassengerTypesForCodes(final List<String> codes)
	{
		return ((BCFPassengerTypeDao) getPassengerTypeDao()).findPassengerTypesForCodes(codes);
	}

	@Override
	public PassengerTypeModel getPassengerTypesForCode(final String code)
	{
		return ((BCFPassengerTypeDao) getPassengerTypeDao()).findPassengerTypesForCode(code);
	}

	@Override
	public void deletePassengerTypeForCode(final String passengerTypeCode)
	{
		ServicesUtil.validateParameterNotNull(passengerTypeCode, "passengerTypeCode must not be null");
		final List<PassengerTypeModel> passengerTypesForCodes = ((BCFPassengerTypeDao) getPassengerTypeDao())
				.findPassengerTypesForCodes(Arrays.asList(passengerTypeCode));
		if (CollectionUtils.isEmpty(passengerTypesForCodes))
		{
			throw new UnknownIdentifierException("No Passengers found for the give code " + passengerTypeCode);
		}
		passengerTypesForCodes.stream().forEach(passengerTypeModel -> {
			passengerTypeModel.setActive(Boolean.FALSE);
			getModelService().save(passengerTypeModel);
		});
	}

	public String getAgeRangeForPassengerType(final PassengerTypeModel passengerType)
	{
		String ageRange;

		if (passengerType instanceof SpecializedPassengerTypeModel)
		{
			if (Objects.nonNull(passengerType.getMinAge()) && Objects.nonNull(passengerType.getMaxAge()))
			{

				if (PASSENGER_TYPE_CODE_ADULT
						.equals(((SpecializedPassengerTypeModel) passengerType).getBasePassengerType().getCode()))
				{
					ageRange = new StringBuilder().append(passengerType.getMinAge()).append(TEXT_PLUS).append(TEXT_YEARS).toString();
				}
				else
				{
					ageRange = new StringBuilder().append(passengerType.getMinAge()).append(HYPHEN).append(passengerType.getMaxAge())
							.append(TEXT_YEARS).toString();
				}
			}
			else
			{
				ageRange = ((SpecializedPassengerTypeModel) passengerType).getBasePassengerType().getName();
			}
		}
		else
		{
			ageRange = passengerType.getName();
		}

		return ageRange;
	}

	@Override
	public Integer getMinAgeByPassengerTypeCode(final String code)
	{
		return ((BCFPassengerTypeDao) getPassengerTypeDao()).findMinAgeByPassengerTypeCode(code);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
