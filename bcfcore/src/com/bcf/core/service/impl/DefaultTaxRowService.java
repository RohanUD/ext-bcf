/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.dao.TaxRowDao;
import com.bcf.core.service.TaxRowService;

public class DefaultTaxRowService implements TaxRowService
{
	private TaxRowDao taxRowDao;
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	public List<TaxRowModel> getTaxRow(final List<String> taxCodes, final LocationModel location)
	{
		final List<String> locationCodes = getBcfTravelLocationService().getLocationCodesInHierarchyByLocationTypes(location,
				Arrays.asList(LocationType.PROPERTY, LocationType.CITY, LocationType.PROVINCE));

		return getTaxRowDao().findTaxRow(taxCodes, locationCodes);
	}

	@Override
	public List<TaxRowModel> getTaxRow(final LocationModel location)
	{
		final List<String> locationCodes = getBcfTravelLocationService().getLocationCodesInHierarchyByLocationTypes(location,
				Arrays.asList(LocationType.PROPERTY, LocationType.CITY, LocationType.PROVINCE));
		return getTaxRowDao().findTaxRow(locationCodes);
	}

	/**
	 * @return the taxRowDao
	 */
	protected TaxRowDao getTaxRowDao()
	{
		return taxRowDao;
	}

	/**
	 * @param taxRowDao
	 *           the taxRowDao to set
	 */
	@Required
	public void setTaxRowDao(final TaxRowDao taxRowDao)
	{
		this.taxRowDao = taxRowDao;
	}

	/**
	 * @return the bcfTravelLocationService
	 */
	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	/**
	 * @param bcfTravelLocationService
	 *           the bcfTravelLocationService to set
	 */
	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
