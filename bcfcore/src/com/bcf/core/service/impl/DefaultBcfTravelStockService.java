/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.travelservices.stock.impl.DefaultTravelStockService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfStockLevelDao;
import com.bcf.core.service.BcfTravelStockService;
import com.bcf.core.util.BCFDateUtils;


public class DefaultBcfTravelStockService extends DefaultTravelStockService implements BcfTravelStockService
{
	private BcfStockLevelDao bcfStockLevelDao;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;


	@Override
	public StockLevelModel fetchProductStockLevel(final String productCode)
	{
		return bcfStockLevelDao.fetchProductStockLevel(productCode);
	}

	@Override
	public List<StockLevelModel> getStocks(final Date releaseStockDate)
	{
		final int maxReleaseStockDays = Integer.parseInt(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.DEFAULT_RELEASE_INVENTORY_BLOCK_DAYS));
		Date startDate = BCFDateUtils.setToStartOfDay(releaseStockDate);
		Date endDate = TravelDateUtils.addDays(startDate, maxReleaseStockDays);
		return getBcfStockLevelDao().fetchStocks(startDate, endDate);
	}

	@Override
	public List<StockLevelModel> getStockLevelsForProductAndDate(final String productCode, final Date date,
			final WarehouseModel warehouse)
	{
		return getBcfStockLevelDao().fetchStockLevelsForProductAndDate(productCode, date, warehouse);
	}

	@Override
	public List<StockLevelModel> getAccommodationStockLevels(final Date startDate, final Date endDate)
	{
		return getBcfStockLevelDao().fetchAccommodationStockLevels(startDate, endDate);
	}

	@Override
	public List<StockLevelModel> getActivityStockLevels(final Date startDate, final Date endDate)
	{
		return getBcfStockLevelDao().fetchActivityStockLevels(startDate, endDate);
	}

	protected BcfStockLevelDao getBcfStockLevelDao()
	{
		return bcfStockLevelDao;
	}

	@Required
	public void setBcfStockLevelDao(final BcfStockLevelDao bcfStockLevelDao)
	{
		this.bcfStockLevelDao = bcfStockLevelDao;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
