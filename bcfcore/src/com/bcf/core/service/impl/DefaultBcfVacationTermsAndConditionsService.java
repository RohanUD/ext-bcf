/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfVacationTermsAndConditionsService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.util.OptionBookingUtil;


public class DefaultBcfVacationTermsAndConditionsService implements BcfVacationTermsAndConditionsService
{
	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfChangeFeeCalculationService")
	private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Resource(name = "orderService")
	BcfOrderService bcfOrderService;

	@Resource(name = "searchRestrictionService")
	private SearchRestrictionService searchRestrictionService;

	public List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(String bookingReference)
	{
		if (assistedServiceService.getAsmSession() != null && assistedServiceService.getAsmSession().getAgent() != null)
		{
			AbstractOrderModel abstractOrderModel = null;
			if (bookingReference == null)
			{
				abstractOrderModel = bcfTravelCartService.getSessionCart();
			}
			else
			{
				abstractOrderModel = bcfBookingService.getOrderModelFromStore(bookingReference);
			}

			return bcfChangeFeeCalculationService.getBCFNonRefundableCostTermsAndConditions(abstractOrderModel);
		}
		return Collections.emptyList();
	}

	public List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditionsForEmails(String bookingReference)
	{
		AbstractOrderModel abstractOrderModel = null;
		if (bookingReference == null)
		{
			abstractOrderModel = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrderModel = bcfOrderService.getOrderByCode(bookingReference);
		}

		if (Objects.nonNull(abstractOrderModel))
		{
			searchRestrictionService.disableSearchRestrictions();
			List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTermsAndConditions = bcfChangeFeeCalculationService
					.getBCFNonRefundableCostTermsAndConditions(abstractOrderModel);
			searchRestrictionService.enableSearchRestrictions();
			return bcfNonRefundableCostTermsAndConditions;
		}
		return Collections.emptyList();
	}

	public VacationPolicyTAndCData getVacationPolicyTermsAndConditions(String bookingReference)
	{
		AbstractOrderModel abstractOrderModel = null;
		if (bookingReference == null)
		{
			abstractOrderModel = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrderModel = bcfBookingService.getOrder(bookingReference);
		}
		return bcfChangeFeeCalculationService.getVacationPolicyTermsAndConditions(abstractOrderModel);
	}

	public VacationPolicyTAndCData getVacationPolicyTermsAndConditionsForEmails(String bookingReference,
			final AbstractOrderModel abstractOrderModel2)
	{

		AbstractOrderModel abstractOrderModel = null;

		if (Objects.nonNull(abstractOrderModel2))
		{
			abstractOrderModel = abstractOrderModel2;
		}
		else if (bookingReference == null)
		{
			abstractOrderModel = bcfTravelCartService.getSessionCart();
		}
		else
		{
			abstractOrderModel = bcfOrderService.getOrderByCode(bookingReference);
		}

		searchRestrictionService.disableSearchRestrictions();
		VacationPolicyTAndCData vacationPolicyTermsAndConditions = bcfChangeFeeCalculationService
				.getVacationPolicyTermsAndConditions(abstractOrderModel);
		searchRestrictionService.enableSearchRestrictions();
		return vacationPolicyTermsAndConditions;
	}

}
