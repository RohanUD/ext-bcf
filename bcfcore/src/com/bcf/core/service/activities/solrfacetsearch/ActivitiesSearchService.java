/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.activities.solrfacetsearch;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import com.bcf.facades.search.facetdata.ActivitySearchPageData;


public interface ActivitiesSearchService<STATE, ITEM, RESULT extends ActivitySearchPageData<STATE, ITEM>>
{
	RESULT doSearch(final SolrSearchQueryData searchQueryData, final PageableData pageableData);
}
