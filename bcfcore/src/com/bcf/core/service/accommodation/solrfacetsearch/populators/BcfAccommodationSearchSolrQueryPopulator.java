/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.core.service.accommodation.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.daos.SolrIndexedTypeDao;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.search.SolrIndexedTypeDefaultSortOrderMappingModel;
import de.hybris.platform.travelservices.search.solrfacetsearch.populators.AccommodationSearchSolrQueryPopulator;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;


public class BcfAccommodationSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
      extends AccommodationSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
{

   private SolrIndexedTypeDao solrIndexedTypeDao;

   @Override
   protected FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException
   {
      return getFacetSearchConfig(MarketingRatePlanInfoModel._TYPECODE);
   }

   /**
    * Resolves suitable {@link FacetSearchConfig} for the query based on the configured strategy bean.<br>
    *
    * @return {@link FacetSearchConfig} that is converted from {@link SolrFacetSearchConfigModel}
    * @throws NoValidSolrConfigException, FacetConfigServiceException
    */
   protected FacetSearchConfig getFacetSearchConfig(final String simpleClassName)
         throws FacetConfigServiceException
   {
      final List<SolrIndexedTypeModel> solrIndexes = getSolrIndexedTypeDao().findAllIndexedTypes();

      final String solrFacetSearchConfigName = solrIndexes.stream()
            .filter(solrIndex -> simpleClassName.equalsIgnoreCase(solrIndex.getType().getCode()))
            .map(solrIndex -> solrIndex.getSolrFacetSearchConfig().getName()).findFirst().get();

      final FacetSearchConfig configuration = getFacetSearchConfigService()
            .getConfiguration(solrFacetSearchConfigName);

      final SolrIndexedTypeDefaultSortOrderMappingModel defaultSortOrderMapping = getSolrIndexSortService()
            .getDefaultSortOrderMapping(simpleClassName);

      if (defaultSortOrderMapping != null)
      {
         configuration.getSearchConfig().setDefaultSortOrder(defaultSortOrderMapping.getDefaultSortOrder());
      }

      return configuration;
   }

   public SolrIndexedTypeDao getSolrIndexedTypeDao()
   {
      return solrIndexedTypeDao;
   }

   @Required
   public void setSolrIndexedTypeDao(final SolrIndexedTypeDao solrIndexedTypeDao)
   {
      this.solrIndexedTypeDao = solrIndexedTypeDao;
   }

}
