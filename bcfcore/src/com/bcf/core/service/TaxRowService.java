/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.List;


public interface TaxRowService
{

	/**
	 * Gets the tax row.
	 *
	 * @param taxCodes
	 *           the tax code
	 * @param location
	 *           the location
	 * @return the tax row
	 */
	List<TaxRowModel> getTaxRow(List<String> taxCodes, LocationModel location);

	List<TaxRowModel> getTaxRow(LocationModel location);
}
