/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.travelservices.stock.TravelStockService;
import java.util.Date;
import java.util.List;


public interface BcfTravelStockService extends TravelStockService
{
	List<StockLevelModel> getStocks(Date releaseStockDate);

	StockLevelModel fetchProductStockLevel(String productCode);

	List<StockLevelModel> getStockLevelsForProductAndDate(final String productCode, final Date date,
			final WarehouseModel warehouseForCode);

	List<StockLevelModel> getAccommodationStockLevels(Date startDate, Date endDate);

	List<StockLevelModel> getActivityStockLevels(Date startDate, Date endDate);
}
