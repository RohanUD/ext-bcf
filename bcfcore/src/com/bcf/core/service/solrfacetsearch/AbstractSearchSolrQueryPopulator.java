/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service.solrfacetsearch;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.daos.SolrIndexedTypeDao;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.travelservices.model.search.SolrIndexedTypeDefaultSortOrderMappingModel;
import de.hybris.platform.travelservices.services.SolrIndexSortService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.deals.solrfacetsearch.strategies.RawQueryStrategy;
import com.bcf.core.solr.query.ArguementData;


public class AbstractSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
{
   private CommonI18NService commonI18NService;
   private FacetSearchService facetSearchService;
   private FacetSearchConfigService facetSearchConfigService;
   private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
   private SolrIndexSortService solrIndexSortService;
   private Map<String, RawQueryStrategy> rawQueryByFilterKeyStrategyMap;
   private SolrIndexedTypeDao solrIndexedTypeDao;


   public void populateQuery(final SearchQueryPageableData<SolrSearchQueryData> source,
         final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE>
               target, final String simpleClassName)
   {
      // Setup the SolrSearchRequest
      target.setSearchQueryData(source.getSearchQueryData());
      target.setPageableData(source.getPageableData());
      try
      {
         target.setFacetSearchConfig(getFacetSearchConfig(simpleClassName));
      }
      catch (final FacetConfigServiceException e)
      {
         throw new ConversionException(e.getMessage(), e);
      }

      // We can only search one core so select the indexed type
      target.setIndexedType(getIndexedType(target.getFacetSearchConfig(), simpleClassName));

      // Create the solr search query for the config and type (this sets-up the default page size and sort order)
      final SearchQuery searchQuery = createSearchQuery(target.getFacetSearchConfig(), target.getIndexedType(),
            source.getSearchQueryData().getFreeTextSearch());
      searchQuery.setCurrency(getCommonI18NService().getCurrentCurrency().getIsocode());
      searchQuery.setLanguage(getCommonI18NService().getCurrentLanguage().getIsocode());

      // enable spell checker
      searchQuery.setEnableSpellcheck(true);

      final List<RawQuery> rawQueries = new ArrayList<>();
      if (CollectionUtils.isNotEmpty(source.getSearchQueryData().getRawQueryFilterTerms()))
      {
         source.getSearchQueryData().getRawQueryFilterTerms().forEach(rawQueryFilterTerm -> {
            final RawQueryStrategy strategy = getRawQueryByFilterKeyStrategyMap()
                  .get(target.getIndexedType().getCode() + "_" + rawQueryFilterTerm.getKey());
            final ArguementData rawQueryArguement = strategy
                  .generateArguementForRawQuery(rawQueryFilterTerm.getKey(), rawQueryFilterTerm.getValue());
            rawQueries.add(strategy.createRawQuery(rawQueryArguement));
         });
      }

      if (CollectionUtils.isNotEmpty(rawQueries))
      {
         searchQuery.getFilterRawQueries().addAll(rawQueries);
      }

      target.setSearchQuery(searchQuery);
   }

   /**
    * Resolves suitable {@link FacetSearchConfig} for the query based on the configured strategy bean.<br>
    *
    * @return {@link FacetSearchConfig} that is converted from {@link SolrFacetSearchConfigModel}
    * @throws NoValidSolrConfigException, FacetConfigServiceException
    */
   protected FacetSearchConfig getFacetSearchConfig(final String simpleClassName)
         throws FacetConfigServiceException
   {
      final List<SolrIndexedTypeModel> solrIndexes = getSolrIndexedTypeDao().findAllIndexedTypes();

      final String solrFacetSearchConfigName = solrIndexes.stream()
            .filter(solrIndex -> simpleClassName.equalsIgnoreCase(solrIndex.getType().getCode()))
            .map(solrIndex -> solrIndex.getSolrFacetSearchConfig().getName()).findFirst().get();

      final FacetSearchConfig configuration = getFacetSearchConfigService()
            .getConfiguration(solrFacetSearchConfigName);

      final SolrIndexedTypeDefaultSortOrderMappingModel defaultSortOrderMapping = getSolrIndexSortService()
            .getDefaultSortOrderMapping(simpleClassName);

      if (defaultSortOrderMapping != null)
      {
         configuration.getSearchConfig().setDefaultSortOrder(defaultSortOrderMapping.getDefaultSortOrder());
      }

      return configuration;
   }

   protected IndexedType getIndexedType(final FacetSearchConfig config, final String simpleClassName)
   {
      final IndexConfig indexConfig = config.getIndexConfig();

      // Strategy for working out which of the available indexed types to use
      final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
      if (CollectionUtils.isNotEmpty(indexedTypes))
      {
         for (final IndexedType indexedType : indexedTypes)
         {
            if (StringUtils.equalsIgnoreCase(indexedType.getCode(), simpleClassName))
            {
               return indexedType;
            }
         }
      }

      // No indexed types
      return null;
   }

   protected SearchQuery createSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
         final String freeTextSearch)
   {
      return getFacetSearchService().createFreeTextSearchQuery(facetSearchConfig, indexedType, freeTextSearch);
   }

   /**
    * @return CommonI18NService
    */
   protected CommonI18NService getCommonI18NService()
   {
      return commonI18NService;
   }

   /**
    * @param commonI18NService
    */
   @Required
   public void setCommonI18NService(final CommonI18NService commonI18NService)
   {
      this.commonI18NService = commonI18NService;
   }

   /**
    * @return FacetSearchService
    */
   protected FacetSearchService getFacetSearchService()
   {
      return facetSearchService;
   }

   /**
    * @param facetSearchService
    */
   @Required
   public void setFacetSearchService(final FacetSearchService facetSearchService)
   {
      this.facetSearchService = facetSearchService;
   }

   /**
    * @return FacetSearchConfigService
    */
   protected FacetSearchConfigService getFacetSearchConfigService()
   {
      return facetSearchConfigService;
   }

   /**
    * @param facetSearchConfigService
    */
   @Required
   public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService)
   {
      this.facetSearchConfigService = facetSearchConfigService;
   }

   /**
    * @return SolrFacetSearchConfigSelectionStrategy
    */
   protected SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy()
   {
      return solrFacetSearchConfigSelectionStrategy;
   }

   /**
    * @param solrFacetSearchConfigSelectionStrategy
    */
   @Required
   public void setSolrFacetSearchConfigSelectionStrategy(
         final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy)
   {
      this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
   }

   protected SolrIndexSortService getSolrIndexSortService()
   {
      return solrIndexSortService;
   }

   @Required
   public void setSolrIndexSortService(final SolrIndexSortService solrIndexSortService)
   {
      this.solrIndexSortService = solrIndexSortService;
   }

   protected Map<String, RawQueryStrategy> getRawQueryByFilterKeyStrategyMap()
   {
      return rawQueryByFilterKeyStrategyMap;
   }

   @Required
   public void setRawQueryByFilterKeyStrategyMap(
         final Map<String, RawQueryStrategy> rawQueryByFilterKeyStrategyMap)
   {
      this.rawQueryByFilterKeyStrategyMap = rawQueryByFilterKeyStrategyMap;
   }


   public SolrIndexedTypeDao getSolrIndexedTypeDao()
   {
      return solrIndexedTypeDao;
   }

   @Required
   public void setSolrIndexedTypeDao(final SolrIndexedTypeDao solrIndexedTypeDao)
   {
      this.solrIndexedTypeDao = solrIndexedTypeDao;
   }

}
