/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;


public interface BcfVacationTermsAndConditionsService
{
	List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(String bookingReference);

	List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditionsForEmails(String bookingReference);

	VacationPolicyTAndCData getVacationPolicyTermsAndConditions(String bookingReference);

	VacationPolicyTAndCData getVacationPolicyTermsAndConditionsForEmails(String bookingReference,
			final AbstractOrderModel abstractOrderModel);
}
