/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 9/7/19 1:21 PM
 */

package com.bcf.core.service;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import java.util.List;


public interface BcfOptionBookingService
{

	CartModel getCartForCode(final String optionBookingId);

	SearchPageData<CartModel> getOptionBookings(final PageableData pageableData);

	List<CartModel> getExpiredOptionBookings();

	void cancelAndReleaseInventoryForOptionBookings(final List<CartModel> optionBookingCarts);

	void cancelAndReleaseOptionBookingInventory(final CartModel optionBooking) throws Exception;

	List<CartModel> getExpiredOptionBookingsForPurging();


	void purgeOptionBookings(List<CartModel> expiredOptionBookings);
}
