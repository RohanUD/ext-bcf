/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import java.util.List;
import com.bcf.core.model.GoodWillRefundModel;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.integration.payment.exceptions.BcfRefundException;


public interface BCFGoodWillRefundService
{

	 List<GoodWillRefundModel> getAllGoodWillRefunds();


	GoodWillRefundModel getGoodWillRefund(String code);

	void processGoodWillRefund(final String bookingReference, String reasonCode, String comment,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas) throws BcfRefundException, Exception;
}
