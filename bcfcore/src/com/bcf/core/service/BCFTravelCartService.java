/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.order.TravelCartService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;


public interface BCFTravelCartService extends TravelCartService
{

	/**
	 * Checks if is amendment cart.
	 *
	 * @return the boolean
	 */
	Boolean isAmendmentCart(AbstractOrderModel sessionCart);

	/**
	 * Removes the cart.
	 *
	 * @param cartModel the cart model
	 * @return
	 */
	void removeCart(CartModel cartModel);

	/**
	 * Removes associated items in cart first before removing the cart
	 *
	 * @param cartModel
	 */
	void removeAssociatedItemsFromCart(final CartModel cartModel);

	/**
	 * Removes the entries for transport offering codes.
	 *
	 * @param cartModel              the cart model
	 * @param transportOfferingCodes the transport offering codes
	 * @return true, if successful
	 */
	void removeEntriesForTransportOfferingCodes(AbstractOrderModel cartModel, List<String> transportOfferingCodes);

	/**
	 * Removes the entries for transport.
	 *
	 * @param transportEntries the transport entries
	 */
	void removeEntriesForTransport(Set<AbstractOrderEntryModel> transportEntries);

	/**
	 * Updates originDestinationRefNo for the entries for transport offering codes.
	 *
	 * @param cartModel              the cart model
	 * @param transportOfferingCodes the transport offering codes
	 * @param commit                 the commit
	 * @return true, if successful
	 */

	boolean updateEntriesForJourneyODRefNo(AbstractOrderModel cartModel, CartFilterParamsDto cartFilterParams);


	boolean removeEntriesForJourneyODRefNo(final AbstractOrderModel abstractOrderModel, CartFilterParamsDto cartFilterParams,
			final boolean isSingle, final boolean removeInboundEntries);

	List<AbstractOrderEntryModel> getCartEntriesForRefNo(final int journeyRefNo, final int odRefNo);

	List<AbstractOrderEntryModel> getAllCartEntriesForRefNo(final int journeyRefNo, final int odRefNo);


	/**
	 * Removes the entries for accommodation.
	 *
	 * @param abstractOrderModel the abstract order model
	 * @return true, if successful
	 */
	void removeEntriesForAccommodation(AbstractOrderModel abstractOrderModel);

	void removeAmountToPayInfos(final AbstractOrderModel abstractOrderModel);

	/**
	 * Save additional Email addresses
	 *
	 * @param emails the List of String
	 * @return void
	 */
	void saveAdditionalEmails(List<String> emails);

	/**
	 * save Agent Comment
	 *
	 * @param agentComment
	 * @return Boolean true on success
	 */
	Boolean saveAgentComment(String agentComment);

	/**
	 * @param referenceCodes
	 * @return Boolean true on success
	 */
	Boolean saveReferenceCodes(List<String> referenceCodes, List<String> bookingRefs);

	/**
	 * get Agent Comment
	 *
	 * @return String
	 */
	String getAgentComment();

	/**
	 * Gets the existing session amended cart.
	 *
	 * @return the existing session amended cart
	 */
	CartModel getExistingSessionAmendedCart();

	List<String> getOrderCachingKeys(AbstractOrderModel abstractOrderModel);

	void updateCart(MakeBookingRequestData makeBookingRequest, BCFMakeBookingResponse response);

	/**
	 * set the active status to false and amend status to modified for all the cart entries that match the selected journey ref number and origin destination number
	 *
	 * @param cartFilterParams
	 */
	void disableAmendedCartEntries(CartFilterParamsDto cartFilterParams);

	/**
	 * Change user session currency.
	 *
	 * @param orderModel the order model
	 */
	void changeUserSessionCurrency(AbstractOrderModel orderModel);

	Map<String, List<AbstractOrderEntryModel>> groupEntriesByCacheKeyOrBookingRef(final AbstractOrderModel source);

	/**
	 * Updates AmountToPayInfos associated with the cart
	 *
	 * @param CartModel
	 */
	void updateAmountToPayInfo(CartModel cartModel);

	void addPropertiesToCartEntry(
			final CartModel masterCartModel, final int orderEntryNo, final ProductModel product,
			final Map<String, Object> propertiesMap);

	void removeActivityOrderEntries(final List<AbstractOrderEntryModel> abstractOrderEntries, boolean releaseStock);

	/**
	 * Gets the user cart from session.
	 *
	 * @return
	 */
	AbstractOrderModel getOrCreateSessionCart();

	void updateDealOrderEntryGroup(CartModel cartModel, int journeyRefNumber, List<Integer> entryNumbers);

	boolean hasValidCacheKeyOrBookingRef(CartModel cartModel);

	boolean hasValidCacheKeyOrBookingRef(CartModel cartModel, AbstractOrderEntryModel entry);

	boolean useCacheKey(final AbstractOrderModel cartModel);

	double getOriginalAmountPaid(CartModel cart);

	int getNumberOfGuestsInCart(CartModel cart, int journeyRef);

	List<AbstractOrderEntryModel> getAllTransportCartEntries();

	boolean isAlacateFlow();

	boolean isGuestUserCart(final AbstractOrderModel abstractOrderModel);

	boolean isCartValidWithMinimumRequirements(final AbstractOrderModel cart);

	boolean hasCommercialVehiclesOnly();

	boolean hasCommercialVehicle(final AbstractOrderModel order);

	boolean removeNewEntriesForJourneyODRefNo(final AbstractOrderModel abstractOrderModel,
			final CartFilterParamsDto cartFilterParams, final boolean isSingle, final boolean removeInboundEntries);

	Map<OrderEntryType, List<AbstractOrderEntryModel>> splitCartEntriesByType(AbstractOrderModel orderModel);

	boolean hasCacheEntriesToConvert();

	public void resetAmountToPay(CartModel cartModel);

	public boolean isCardPayment(AbstractOrderModel abstractOrderModel);

	public boolean isOriginalOrderOnRequest(final AbstractOrderModel abstractOrderModel);

	int getThresholdTimeGap(final AbstractOrderModel order);

}
