/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.services.BookingService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.integration.data.SearchBookingResponseDTO;


public interface BcfBookingService extends BookingService
{

	/**
	 * Cancel partial order.
	 *
	 * @param order                        the order
	 * @param filteredOrderEntriesToRemove the filtered order entries to remove
	 * @return true, if successful
	 */
	boolean cancelPartialOrder(OrderModel order, Set<AbstractOrderEntryModel> filteredOrderEntriesToRemove);

	/**
	 * Creates the history snapshot.
	 *
	 * @param originalOrder the original order
	 * @return the order model
	 */
	OrderModel createHistorySnapshot(OrderModel originalOrder);

	/**
	 * Gets the order total price by journey.
	 *
	 * @param order         the order
	 * @param journeyRefNum the journey ref num
	 * @param odRefNum      the od ref num
	 * @return the order total price by journey
	 */
	BigDecimal getOrderTotalPriceByJourney(OrderModel order, int journeyRefNum, int odRefNum);

	/**
	 * Gets the total price for removed transport entries.
	 *
	 * @param order the order
	 * @return the amount for removed transport entries
	 */
	Double getTotalPriceForRemovedTransportEntries(OrderModel order);

	void startRefundOrderProcess(OrderModel order);

	/**
	 * Update order from E booking.
	 *
	 * @param order                    the order
	 * @param searchBookingResponseDTO
	 * @throws BcfOrderUpdateException
	 * @throws CalculationException
	 */
	void updateOrderFromEBooking(OrderModel order, SearchBookingResponseDTO searchBookingResponseDTO)
			throws BcfOrderUpdateException, CalculationException;//NOSONAR

	List<DealOrderEntryGroupModel> getDealOrderEntryGroups(AbstractOrderModel abstractOrder);

	List<AbstractOrderEntryGroupModel> getOrderEntryGroups(AbstractOrderModel abstractOrder);

	List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(
			List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups);

	List<DealOrderEntryGroupModel> getDealOrderEntryGroups(
			List<AbstractOrderEntryGroupModel> abstractOrderEntryGroups);

	void removeEntriesForDeal(List<DealOrderEntryGroupModel> dealOrderEntryGroupModelList, boolean releaseStock)
			throws ModelRemovalException;


	boolean checkIfOrderEntriesExistOfType(AbstractOrderModel abstractOrderModel,
			OrderEntryType orderEntryType);

	boolean checkIfActivityEntriesExist(AbstractOrderModel abstractOrderModel);
	DealOrderEntryGroupModel getAccommodationDealOrderEntryGroup(AbstractOrderModel abstractOrder, Integer journeyReferenceNumber);

	BigDecimal getRefundAmount(OrderModel order);

	AbstractOrderEntryModel getExistingAncillaryOrderEntry(AbstractOrderModel abstractOrderModel, String productCode,
			String travelRouteCode,
			List<String> transportOfferingCodes, List<String> travellerCodes, int journeyRefNumber, int odRefNumber);

	BigDecimal calculateTotalToRefund(OrderModel orderModel);

	void cancelAccommodationAndActivityEntries(final OrderModel order, BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData) throws CalculationException;

	void addAccommodationCancellationFeeProductToCart(final AbstractOrderModel order,  BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData);

	void startCancelOrderEmailProcess(final OrderModel order);

	void deActivateVacationEntriesAndReleaseStock(OrderModel order,  BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData);

	void deActivateVacationEntriesAndReleaseStock(final CartModel cartModel);

	List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(final AbstractOrderModel abstractOrder,
			final Integer journeyReferenceNumber);

	boolean isPaymentTransactionEntrySuccess(final PaymentTransactionEntryModel paymentTransactionEntryModel);

	/**
	 * gets the activity order entries
	 * @param abstractOrderModel abstract order
	 * @return activity order entries
	 */
	List<AbstractOrderEntryModel> getActivityEntries(final AbstractOrderModel abstractOrderModel);

	/**
	 * gets the transport order entries
	 * @param abstractOrderModel abstract order
	 * @return transport order entries
	 */
	List<AbstractOrderEntryModel> getTransportEntries(final AbstractOrderModel abstractOrderModel);

	/**
	 * gets the accommodation order entries
	 * @param abstractOrderModel abstract order
	 * @return accommodation order entries
	 */
	List<AbstractOrderEntryModel> getAccommodationEntries(final AbstractOrderModel abstractOrderModel);

}
