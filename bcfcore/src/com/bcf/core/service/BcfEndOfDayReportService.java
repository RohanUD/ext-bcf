/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.service;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.bcf.core.model.DeclareOrderEntryModel;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.EndOfDayReportData;


public interface BcfEndOfDayReportService
{
	List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForAgent(UserModel agent, Date declareStartTime,
			Date declareEndTime);

	List<DeclareOrderEntryModel> findDeclareOrderEntriesForAgent(UserModel agent, Date declareStartTime, Date declareEndTime);

	List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForWebJob(Date endofdayDeclareStartTime,
			Date endofdayDeclareEndTime);

	List<DeclareOrderEntryModel> findDeclareOrderEntriesForWebJob(Date endofdayDeclareStartTime, Date endofdayDeclareEndTime);

	EndOfDayReportData getEndOfDayReportDataForAgent(UserModel agent, Map<String, Object> agentDeclareDataMap,
			Integer currencyRoundingDigits);

	void getEndOfDayReportDataForWeb(Date endofdayDeclareStartTime, Date endofdayDeclareEndTime, Integer currencyRoundingDigits)
			throws IntegrationException;

	void submitEndOfDayReport(EndOfDayReportData endOfDayReportData) throws IntegrationException;
}
