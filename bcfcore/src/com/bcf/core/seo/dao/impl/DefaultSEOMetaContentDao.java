/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.seo.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.model.content.SEOMetaContentModel;
import com.bcf.core.seo.dao.SEOMetaContentDao;


public class DefaultSEOMetaContentDao extends DefaultGenericDao<SEOMetaContentModel> implements SEOMetaContentDao
{

	private static final String GET_SEO_META_CONTENT = "Select {pk} from {SEOMetaContent} where {uid}=?uid and {catalogVersion} in (?catalogVersions) ";

	public DefaultSEOMetaContentDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public SEOMetaContentModel findMetaContent(final String uid, final List<CatalogVersionModel> activeContentCatalogs)
	{
		validateParameterNotNull(uid, "uid must not be null!");
		validateParameterNotNull(activeContentCatalogs, "activeContentCatalogs must not be null!");

		SEOMetaContentModel metaContent = null;

		final Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("catalogVersions", activeContentCatalogs);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_SEO_META_CONTENT);
		query.addQueryParameters(params);
		final SearchResult<SEOMetaContentModel> searchResult = getFlexibleSearchService().search(query);

		if (CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			final List<SEOMetaContentModel> metaContents = searchResult.getResult();
			if (metaContents.size() > 1)
			{
				throw new AmbiguousIdentifierException("More than one seocontent found for uid " + uid);
			}
			metaContent = metaContents.get(0);
		}

		return metaContent;
	}

}
