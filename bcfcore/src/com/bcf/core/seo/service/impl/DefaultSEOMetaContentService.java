/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.seo.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import java.util.List;
import com.bcf.core.model.content.SEOMetaContentModel;
import com.bcf.core.seo.dao.SEOMetaContentDao;
import com.bcf.core.seo.service.SEOMetaContentService;


public class DefaultSEOMetaContentService implements SEOMetaContentService
{

	private SEOMetaContentDao metaContentDao;

	@Override
	public SEOMetaContentModel getMetaContent(String uid, final List<CatalogVersionModel> activeContentCatalogs)
	{
		return getMetaContentDao().findMetaContent(uid, activeContentCatalogs);
	}

	public SEOMetaContentDao getMetaContentDao()
	{
		return metaContentDao;
	}

	public void setMetaContentDao(SEOMetaContentDao metaContentDao)
	{
		this.metaContentDao = metaContentDao;
	}


}
