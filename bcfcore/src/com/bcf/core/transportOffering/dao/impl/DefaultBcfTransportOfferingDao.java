/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.transportOffering.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.dao.impl.DefaultTransportOfferingDao;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.transportOffering.dao.BcfTransportOfferingDao;

public class DefaultBcfTransportOfferingDao extends DefaultTransportOfferingDao implements BcfTransportOfferingDao {
   private static final String FROM = "} FROM {";

   private static final String SELECT_TRANSPORTOFFERING = "SELECT {" + TransportOfferingModel.PK + FROM + TransportOfferingModel._TYPECODE + "} WHERE {";

   private static final String VALUE = "} = ?";

   private static final String FIND_TRANSPORTOFFERING_BY_IDENTIFIER_CODE =
         SELECT_TRANSPORTOFFERING + TransportOfferingModel.TRANSFERIDENTIFIER + VALUE + TransportOfferingModel.TRANSFERIDENTIFIER;

   private static final String FIND_TRANSPORTOFFERING_BY_EBOOKING_CODE =
         SELECT_TRANSPORTOFFERING + TransportOfferingModel.EBOOKINGSAILINGCODE + VALUE + TransportOfferingModel.EBOOKINGSAILINGCODE;

   private static final String FIND_BY_DATE_AND_SECTOR =
         SELECT_TRANSPORTOFFERING + TransportOfferingModel.DEPARTURETIME + "} = ?departureDate and {" + TransportOfferingModel.ARRIVALTIME + "} = ?arrivalDate "
               + "and {" + TransportOfferingModel.TRAVELSECTOR + "} = ?travelSector";

   private static final String FIND_BY_SECTOR =
         "SELECT {toff." + TransportOfferingModel.PK + FROM + TransportOfferingModel._TYPECODE + " as toff join TravelSector as ts on {toff."
               + TransportOfferingModel.TRAVELSECTOR + "}={ts.pk}} WHERE {ts." + TravelSectorModel.CODE + VALUE + TravelSectorModel._TYPECODE;

   private static final String TRANSPORTOFFERING_DEFAULT = " and {toff." + TransportOfferingModel.DEFAULT + VALUE + TransportOfferingModel.DEFAULT;

   private static final String FIND_TRANSPORTOFFERING_BY_CODE_AND_SECTOR =
         "SELECT {toff." + TransportOfferingModel.PK + FROM + TransportOfferingModel._TYPECODE + " as toff join TravelSector as ts on {toff."
               + TransportOfferingModel.TRAVELSECTOR + "}={ts.pk}} WHERE {toff." + TransportOfferingModel.CODE + VALUE + TransportOfferingModel.CODE
               + " and {ts." + TravelSectorModel.CODE + VALUE + TravelSectorModel._TYPECODE;

   String FIND_ENTRIES_BY_OFFERINGCODE = "SELECT {aoe.pk} FROM {Order AS ao JOIN OrderEntry AS aoe ON {ao.pk} = {aoe.order} JOIN "
         + " TravelOrderEntryInfo AS toei ON {aoe.travelOrderEntryInfo} = {toei.pk} JOIN "
         + " TravelOrderEntryInfoTransportOfferingRelation AS toeitoff ON {toeitoff.source} = {toei.pk} JOIN "
         + " TransportOffering AS toff ON {toeitoff.target} = {toff.pk} JOIN " + " BookingJourneyType AS bkgtype ON {bkgtype.pk} = {ao.bookingJourneyType}} "
         + " WHERE {toff.code} =?code AND {aoe.active} = true" + BOOKING_JOURNEY_FILTER_QUERY;

   private static final String BOOKING_JOURNEY_FILTER_QUERY =
         " AND {bkgtype.code} " + "IN ('BOOKING_PACKAGE', 'BOOKING_ALACARTE')";

   public DefaultBcfTransportOfferingDao(final String typecode) {
      super(typecode);
   }

   @Override public TransportOfferingModel getTransportOfferings(final Date departureDate, final Date arrivalDate, final TravelSectorModel travelsector) {
      final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_BY_DATE_AND_SECTOR);
      fQuery.addQueryParameter("departureDate", departureDate);
      fQuery.addQueryParameter("arrivalDate", arrivalDate);
      fQuery.addQueryParameter("travelSector", travelsector);
      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(fQuery);
      if (searchResult.getTotalCount() == 0) {
         return null;
      }
      if (searchResult.getTotalCount() > 1) {
         throw new AmbiguousIdentifierException("Found " + searchResult.getTotalCount() + " results for the given departure and arrival date");
      }
      return searchResult.getResult().get(0);
   }

   @Override public TransportOfferingModel findTransportOffering(final String code, final String travelSector) {
      validateParameterNotNull(code, "Parameter code cannot be null");
      validateTravelSector(travelSector);

      final Map<String, Object> params = new HashMap<>();
      params.put(TransportOfferingModel.CODE, code);
      params.put(TravelSectorModel._TYPECODE, travelSector);

      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(FIND_TRANSPORTOFFERING_BY_CODE_AND_SECTOR, params);

      if (CollectionUtils.size(searchResult.getResult()) > 1) {
         throw new AmbiguousIdentifierException("Multiple TransportOfferings with same code exist");
      }

      if (CollectionUtils.isEmpty(searchResult.getResult())) {
         return null;
      }

      return searchResult.getResult().get(0);
   }

   @Override public List<TransportOfferingModel> findDefaultTransportOfferingBySector(final String travelSectorCode) {
      validateTravelSector(travelSectorCode);

      final Map<String, Object> params = new HashMap<>();
      params.put(TravelSectorModel._TYPECODE, travelSectorCode);
      params.put(TransportOfferingModel.DEFAULT, Boolean.TRUE);

      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(FIND_BY_SECTOR + TRANSPORTOFFERING_DEFAULT, params);

      return searchResult.getResult();
   }

   @Override public List<TransportOfferingModel> findTransportOfferingBySector(final String travelSectorCode) {
      validateTravelSector(travelSectorCode);

      final Map<String, Object> params = new HashMap<>();
      params.put(TravelSectorModel._TYPECODE, travelSectorCode);

      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(FIND_BY_SECTOR, params);

      return searchResult.getResult();
   }

   @Override public List<TransportOfferingModel> findTransportOfferingForTransferIdentifier(final String transferIdentifier) {
      validateParameterNotNull(transferIdentifier, "transferIdentifier code cannot be null");

      final Map<String, Object> params = new HashMap<>();
      params.put(TransportOfferingModel.TRANSFERIDENTIFIER, transferIdentifier);

      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(FIND_TRANSPORTOFFERING_BY_IDENTIFIER_CODE, params);
      return searchResult.getResult();
   }

   @Override public List<TransportOfferingModel> findTransportOfferingsForSailingCode(final String sailingCode) {
      validateParameterNotNull(sailingCode, "sailingCode code cannot be null");

      final Map<String, Object> params = new HashMap<>();
      params.put(TransportOfferingModel.EBOOKINGSAILINGCODE, sailingCode);

      final SearchResult<TransportOfferingModel> searchResult = getFlexibleSearchService().search(FIND_TRANSPORTOFFERING_BY_EBOOKING_CODE, params);
      return searchResult.getResult();
   }

   private void validateTravelSector(final String travelSector) {
      validateParameterNotNull(travelSector, "Parameter travelSector cannot be null");
   }

   @Override public List<AbstractOrderEntryModel> findModifiedOrderEntriesForOfferingCode(final String offeringCode) {
      validateParameterNotNull(offeringCode, "offeringCode code cannot be null");

      final Map<String, Object> params = new HashMap<>();
      params.put(TransportOfferingModel.CODE, offeringCode);

      final SearchResult<AbstractOrderEntryModel> searchResult = getFlexibleSearchService().search(FIND_ENTRIES_BY_OFFERINGCODE, params);
      return searchResult.getResult();
   }
}