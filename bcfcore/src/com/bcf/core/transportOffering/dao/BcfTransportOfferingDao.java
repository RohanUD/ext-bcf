/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.transportOffering.dao;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.dao.TransportOfferingDao;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Date;
import java.util.List;


public interface BcfTransportOfferingDao extends TransportOfferingDao
{
	TransportOfferingModel getTransportOfferings(final Date departureDate, final Date arrivalDate,
			final TravelSectorModel travelsector);

	TransportOfferingModel findTransportOffering(String code, String travelSector);

	List<TransportOfferingModel> findTransportOfferingForTransferIdentifier(String transferIdentifier);
	List<TransportOfferingModel> findTransportOfferingsForSailingCode(String sailingCode);

	List<TransportOfferingModel> findTransportOfferingBySector(final String travelSectorCode);

	List<TransportOfferingModel> findDefaultTransportOfferingBySector(final String travelSectorCode);

	List<AbstractOrderEntryModel> findModifiedOrderEntriesForOfferingCode(final String offeringCode);
}
