/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.service.impl;

import java.util.Date;
import java.util.List;
import com.bcf.core.activity.dao.ActivityCancellationPolicyDao;
import com.bcf.core.activity.service.ActivityCancellationPolicyService;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;


public class DefaultActivityCancellationPolicyService implements ActivityCancellationPolicyService
{

	private ActivityCancellationPolicyDao activityCancellationPolicyDao;
	@Override
	public ActivityCancellationPolicyModel getActivityCancellationPolicyModel(final String code)
	{
		return getActivityCancellationPolicyDao().findCancellationPolicyModel(code);
	}

	@Override
	public List<ActivityCancellationPolicyModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate,  List<ActivityProductModel> activityProducts)
	{
		return getActivityCancellationPolicyDao().findActivityNonRefundableChangeAndCancellationFee(firstTravelDate ,activityProducts);

	}

		public ActivityCancellationPolicyDao getActivityCancellationPolicyDao()
	{
		return activityCancellationPolicyDao;
	}

	public void setActivityCancellationPolicyDao(final ActivityCancellationPolicyDao activityCancellationPolicyDao)
	{
		this.activityCancellationPolicyDao = activityCancellationPolicyDao;
	}
}
