/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.service.impl;

import java.util.Date;
import java.util.List;
import com.bcf.core.activity.dao.ActivityCancellationFeesDao;
import com.bcf.core.activity.service.ActivityCancellationFeesService;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.product.ActivityProductModel;


public class DefaultActivityCancellationFeesService implements ActivityCancellationFeesService
{

	ActivityCancellationFeesDao activityCancellationFeesDao;

	public List<ActivityCancellationFeesModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate, int noOfDays,  List<ActivityProductModel> activityProducts)
	{
		return activityCancellationFeesDao.findActivityNonRefundableChangeAndCancellationFee(firstTravelDate,noOfDays,activityProducts);

	}

	public ActivityCancellationFeesDao getActivityCancellationFeesDao()
	{
		return activityCancellationFeesDao;
	}

	public void setActivityCancellationFeesDao(final ActivityCancellationFeesDao activityCancellationFeesDao)
	{
		this.activityCancellationFeesDao = activityCancellationFeesDao;
	}
}
