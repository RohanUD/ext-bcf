/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.service;

import java.util.Date;
import java.util.List;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.product.ActivityProductModel;


public interface ActivityCancellationFeesService
{

	List<ActivityCancellationFeesModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate, int noOfDays,
			List<ActivityProductModel> activityProducts);


}
