/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.activity.dao.ActivityCancellationPolicyDao;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;


public class DefaultActivityCancellationPolicyDao extends DefaultGenericDao<ActivityCancellationPolicyModel> implements
		ActivityCancellationPolicyDao
{
	public DefaultActivityCancellationPolicyDao(final String typecode)
	{
		super(typecode);
	}


	private static final String ACTIVITY_POLICY_ROW = "SELECT {" + ActivityCancellationPolicyModel.PK + "} from {" + ActivityCancellationPolicyModel._TYPECODE
			+ " as VP } where {VP:"+ActivityCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate"
			+" and {VP:"+ActivityCancellationPolicyModel.ENDDATE+ "} >= ?firstTravelDate and {VP:"+ActivityCancellationPolicyModel.ACTIVITYPRODUCT +"} IN (?activityProducts)";



	@Override
	public ActivityCancellationPolicyModel findCancellationPolicyModel(final String code)
	{
		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivityCancellationPolicyModel.CODE, code);
		final List<ActivityCancellationPolicyModel> activityCancellationPolicyModels = find(params);
		if (!activityCancellationPolicyModels.isEmpty())
		{
			return activityCancellationPolicyModels.get(0);
		}
		return null;
	}


	@Override
	public List<ActivityCancellationPolicyModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate,  List<ActivityProductModel> activityProducts){

		final Map<String, Object> params = new HashMap<>();
		params.put("firstTravelDate",firstTravelDate);
		params.put("activityProducts", activityProducts);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(ACTIVITY_POLICY_ROW);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<ActivityCancellationPolicyModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;
	}
}
