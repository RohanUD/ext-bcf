/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.activity.dao.ActivityCancellationFeesDao;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;


public class DefaultActivityCancellationFeesDao extends DefaultGenericDao<ActivityCancellationFeesModel> implements
		ActivityCancellationFeesDao
{
	public DefaultActivityCancellationFeesDao(final String typecode)
	{
		super(typecode);
	}


	private static final String ACTIVITY_BCFNONREFUNDFEE_ROW_DATE = "SELECT {" + ActivityCancellationFeesModel.PK + "} from {" + ActivityCancellationFeesModel._TYPECODE
			+ " as VF join "+ ActivityCancellationPolicyModel._TYPECODE + " as VP on {VF:"+ActivityCancellationFeesModel.CANCELLATIONPOLICY+ "}={VP:"+ActivityCancellationPolicyModel.PK+"}} where "
			+"{VP:"+ActivityCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate"
			+" and {VP:"+ActivityCancellationPolicyModel.ENDDATE+ "} >= ?firstTravelDate and {VF:"+ActivityCancellationFeesModel.MINDAYS + "} <= ?noOfDays"
			+"	and {VF:"+ActivityCancellationFeesModel.MAXDAYS + "} >= ?noOfDays  and {VP:"+ActivityCancellationPolicyModel.ACTIVITYPRODUCT +"} IN (?activityProducts)";



	@Override
	public List<ActivityCancellationFeesModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate, int noOfDays,  List<ActivityProductModel> activityProducts){

		final Map<String, Object> params = new HashMap<>();
		params.put("firstTravelDate",firstTravelDate);
		params.put("noOfDays",noOfDays);
		params.put("activityProducts", activityProducts);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(ACTIVITY_BCFNONREFUNDFEE_ROW_DATE);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<ActivityCancellationFeesModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;
	}
}
