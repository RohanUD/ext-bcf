/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.activity.dao;

import java.util.Date;
import java.util.List;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;


public interface ActivityCancellationPolicyDao
{
	ActivityCancellationPolicyModel findCancellationPolicyModel(String code);

	List<ActivityCancellationPolicyModel> findActivityNonRefundableChangeAndCancellationFee(Date firstTravelDate,
			List<ActivityProductModel> activityProducts);


	}

