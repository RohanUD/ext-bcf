/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.connector.dao.HotelPickupInfoDao;
import com.bcf.core.model.HotelPickupInfoModel;


public class DefaultHotelPickupInfoDao  extends DefaultGenericDao<HotelPickupInfoModel> implements HotelPickupInfoDao
{
	public DefaultHotelPickupInfoDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public HotelPickupInfoModel findHotelPickupInfoModel(final String hotelName)
	{
		validateParameterNotNull(hotelName, "hotel name must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(HotelPickupInfoModel.HOTELNAME, hotelName);
		final List<HotelPickupInfoModel> hotelPickupInfoModelList = find(params);
		if (!hotelPickupInfoModelList.isEmpty())
		{
			return hotelPickupInfoModelList.get(0);
		}
		return null;
	}
}
