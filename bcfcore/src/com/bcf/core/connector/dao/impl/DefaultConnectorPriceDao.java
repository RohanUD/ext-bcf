/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.connector.dao.ConnectorPriceDao;
import com.bcf.core.model.ConnectorPriceModel;
import com.bcf.core.model.ConnectorScheduleModel;


public class DefaultConnectorPriceDao extends DefaultGenericDao<ConnectorPriceModel> implements ConnectorPriceDao
{
	public DefaultConnectorPriceDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public ConnectorPriceModel findConnectorPriceModel(final ConnectorScheduleModel schedule)
	{
		validateParameterNotNull(schedule, "schedule must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ConnectorPriceModel.CONNECTORSCHEDULE, schedule);
		final List<ConnectorPriceModel> connectorPriceModels = find(params);
		if (!connectorPriceModels.isEmpty())
		{
			return connectorPriceModels.get(0);
		}
		return null;
	}
}
