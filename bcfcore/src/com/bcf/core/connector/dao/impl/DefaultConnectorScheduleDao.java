/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.connector.dao.ConnectorScheduleDao;
import com.bcf.core.model.ConnectorScheduleModel;


public class DefaultConnectorScheduleDao extends DefaultGenericDao<ConnectorScheduleModel>  implements ConnectorScheduleDao
{
	public DefaultConnectorScheduleDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public ConnectorScheduleModel findConnectorScheduleModel(final String code)
	{
		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ConnectorScheduleModel.CODE, code);
		final List<ConnectorScheduleModel> connectorScheduleModels = find(params);
		if (!connectorScheduleModels.isEmpty())
		{
			return connectorScheduleModels.get(0);
		}
		return null;
	}
}
