/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.service.impl;

import com.bcf.core.connector.dao.HotelPickupInfoDao;
import com.bcf.core.connector.service.HotelPickupInfoService;
import com.bcf.core.model.HotelPickupInfoModel;


public class DefaultHotelPickupInfoService implements HotelPickupInfoService
{
	private HotelPickupInfoDao hotelPickupInfoDao;
	@Override
	public HotelPickupInfoModel getHotelPickupInfoModel(final String hotelName)
	{
		return getHotelPickupInfoDao().findHotelPickupInfoModel(hotelName);
	}

	public HotelPickupInfoDao getHotelPickupInfoDao()
	{
		return hotelPickupInfoDao;
	}

	public void setHotelPickupInfoDao(final HotelPickupInfoDao hotelPickupInfoDao)
	{
		this.hotelPickupInfoDao = hotelPickupInfoDao;
	}
}
