/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.service.impl;

import com.bcf.core.connector.dao.ConnectorPriceDao;
import com.bcf.core.connector.service.ConnectorPriceService;
import com.bcf.core.model.ConnectorPriceModel;
import com.bcf.core.model.ConnectorScheduleModel;


public class DefaultConnectorPriceService implements ConnectorPriceService
{
	private ConnectorPriceDao connectorPriceDao;
	@Override
	public ConnectorPriceModel getConnectorPriceModel(final ConnectorScheduleModel schedule)
	{
		return getConnectorPriceDao().findConnectorPriceModel(schedule);
	}

	public ConnectorPriceDao getConnectorPriceDao()
	{
		return connectorPriceDao;
	}

	public void setConnectorPriceDao(final ConnectorPriceDao connectorPriceDao)
	{
		this.connectorPriceDao = connectorPriceDao;
	}
}
