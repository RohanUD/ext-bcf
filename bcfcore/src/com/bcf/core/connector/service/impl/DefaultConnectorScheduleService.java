/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.connector.service.impl;

import com.bcf.core.connector.dao.ConnectorScheduleDao;
import com.bcf.core.connector.service.ConnectorScheduleService;
import com.bcf.core.model.ConnectorScheduleModel;


public class DefaultConnectorScheduleService implements ConnectorScheduleService
{
	private ConnectorScheduleDao connectorScheduleDao;

	@Override
	public ConnectorScheduleModel getConnectorScheduleModel(final String code)
	{
		return getConnectorScheduleDao().findConnectorScheduleModel(code);
	}

	public ConnectorScheduleDao getConnectorScheduleDao()
	{
		return connectorScheduleDao;
	}

	public void setConnectorScheduleDao(final ConnectorScheduleDao connectorScheduleDao)
	{
		this.connectorScheduleDao = connectorScheduleDao;
	}
}
