/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.container.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaContainerService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.media.container.BcfMediaContainerService;
import com.bcf.core.media.container.dao.BcfMediaContainerDao;


public class DefaultBcfMediaContainerService extends DefaultMediaContainerService implements BcfMediaContainerService
{
	private BcfMediaContainerDao bcfMediaContainerDao;

	@Override
	public MediaContainerModel getMediaContainer(final String qualifier, final CatalogVersionModel catalogVersion)
	{
		return getBcfMediaContainerDao().findMediaContainer(qualifier, catalogVersion);
	}

	protected BcfMediaContainerDao getBcfMediaContainerDao()
	{
		return bcfMediaContainerDao;
	}

	@Required
	public void setBcfMediaContainerDao(final BcfMediaContainerDao bcfMediaContainerDao)
	{
		this.bcfMediaContainerDao = bcfMediaContainerDao;
	}


}
