/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.container.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.impl.DefaultMediaContainerDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.media.container.dao.BcfMediaContainerDao;


public class DefaultBcfMediaContainerDao extends DefaultMediaContainerDao implements BcfMediaContainerDao
{
	private static final String MEDIA_CONTAINER_WITHCV_QUERY = "SELECT {" + MediaContainerModel.PK + "} FROM {"
			+ MediaContainerModel._TYPECODE
			+ "} WHERE {" + MediaContainerModel.QUALIFIER + "}=?" + MediaContainerModel.QUALIFIER + " AND {"
			+ MediaContainerModel.CATALOGVERSION + "}=?" + MediaContainerModel.CATALOGVERSION;

	@Override
	public MediaContainerModel findMediaContainer(final String qualifier, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(qualifier, "qualifier must not be null!");
		validateParameterNotNull(catalogVersion, "catalogVersion must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(MediaContainerModel.QUALIFIER, qualifier);
		params.put(MediaContainerModel.CATALOGVERSION, catalogVersion);
		final SearchResult<MediaContainerModel> mediaContainerSearchResult = getFlexibleSearchService()
				.search(MEDIA_CONTAINER_WITHCV_QUERY, params);
		if (CollectionUtils.isNotEmpty(mediaContainerSearchResult.getResult()))
		{
			return mediaContainerSearchResult.getResult().stream().findFirst().orElse(null);
		}

		return null;
    }
}
