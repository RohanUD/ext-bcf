/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.strategy.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.mediaconversion.MediaConversionService;
import de.hybris.platform.mediaconversion.conversion.MediaConversionException;
import de.hybris.platform.mediaconversion.imagemagick.ImageMagickMediaConversionStrategy;
import de.hybris.platform.mediaconversion.model.ConversionMediaFormatModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;


public class BCFImageMagickMediaConversionStrategy extends ImageMagickMediaConversionStrategy
{
	private ModelService modelService;

	@Override
	public MediaModel convert(final MediaConversionService mediaConversionService, final MediaModel input,
			final ConversionMediaFormatModel format) throws MediaConversionException
	{
		return getConvertedMediaWithCustomAttributes(input, super.convert(mediaConversionService, input, format));
	}

	private MediaModel getConvertedMediaWithCustomAttributes(final MediaModel input, final MediaModel convertedMedia)
	{
		convertedMedia.setLocationTag(input.getLocationTag());
		convertedMedia.setIconText(input.getIconText());
		convertedMedia.setPromoText(input.getPromoText());
		convertedMedia.setAltText(input.getAltText());
		convertedMedia.setCaption(input.getCaption());
		convertedMedia.setFeaturedText(input.getFeaturedText());
		getModelService().save(convertedMedia);
		return convertedMedia;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
