/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.dao.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.ContainerCreationStatus;
import com.bcf.core.media.dao.BcfMediaDao;


public class BcfDefaultMediaDao extends DefaultMediaDao implements BcfMediaDao
{
	private static final String MIME_TYPES_FOR_MEDIA_CONVERSION = "supportedMimeTypesForMediaConversion";
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public List<MediaModel> findMediaItemsForFormatConversion()
	{
		final String supportedMimeTypesForMediaConversion = bcfConfigurablePropertiesService
				.getBcfPropertyValue(MIME_TYPES_FOR_MEDIA_CONVERSION);
		final String query =
				"SELECT {pk} FROM {media} WHERE {conversionGroup} is not NULL"
						+ " AND {mediaContainer} is NULL AND {mime} IN (" + supportedMimeTypesForMediaConversion
						+ ") AND {containerCreationStatus} = ({{SELECT {PK} FROM {ContainerCreationStatus} WHERE {code}='"
						+ ContainerCreationStatus.PENDING + "'}})";
		final SearchResult result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
