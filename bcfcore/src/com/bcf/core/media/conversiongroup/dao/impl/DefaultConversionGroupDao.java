/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.conversiongroup.dao.impl;

import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.media.conversiongroup.dao.ConversionGroupDao;


public class DefaultConversionGroupDao extends DefaultGenericDao<ConversionGroupModel> implements ConversionGroupDao
{
	private static final Logger LOG = Logger.getLogger(DefaultConversionGroupDao.class);

	public DefaultConversionGroupDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public ConversionGroupModel findConversionGroup(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "ConversionGroup code must not be null!");
		final List conversionGroupModels = this.find(Collections.singletonMap("code", code));
		if (CollectionUtils.isEmpty(conversionGroupModels))
		{
			LOG.warn("No ConversionGroupModel found");
			return null;
		}
		else if (conversionGroupModels.size() > 1)
		{
			LOG.warn("Found " + conversionGroupModels.size() + " ConversionGroupModel for the given query, returning first");
		}
		return (ConversionGroupModel) conversionGroupModels.get(0);
	}
}
