/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.conversiongroup.service.impl;

import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.media.conversiongroup.dao.ConversionGroupDao;
import com.bcf.core.media.conversiongroup.service.ConversionGroupService;


public class DefaultConversionGroupService implements ConversionGroupService
{
	private ConversionGroupDao conversionGroupDao;
	private ConfigurationService configurationService;
	private static final String DEFAULT_MEDIA_CONVERSION_GROUP = "default.media.conversion.group";

	@Override
	public ConversionGroupModel getDefaultConversionGroup()
	{
		return getConversionGroupDao()
				.findConversionGroup(getConfigurationService().getConfiguration().getString(DEFAULT_MEDIA_CONVERSION_GROUP));
	}

	public ConversionGroupModel getConversionGroupByCode(final String conversionGroupCode)
	{
		return getConversionGroupDao().findConversionGroup(conversionGroupCode);
	}

	protected ConversionGroupDao getConversionGroupDao()
	{
		return conversionGroupDao;
	}

	@Required
	public void setConversionGroupDao(final ConversionGroupDao conversionGroupDao)
	{
		this.conversionGroupDao = conversionGroupDao;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
