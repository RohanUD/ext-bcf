/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.service.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.media.dao.BcfMediaDao;
import com.bcf.core.media.service.BcfMediaService;


public class BcfDefaultMediaService extends DefaultMediaService implements BcfMediaService
{
	private BcfMediaDao bcfMediaDao;

	@Override
	public List<MediaModel> findMediaItemsForFormatConversion()
	{
		return getBcfMediaDao().findMediaItemsForFormatConversion();
	}

	protected BcfMediaDao getBcfMediaDao()
	{
		return bcfMediaDao;
	}

	@Required
	public void setBcfMediaDao(final BcfMediaDao bcfMediaDao)
	{
		this.bcfMediaDao = bcfMediaDao;
	}
}
