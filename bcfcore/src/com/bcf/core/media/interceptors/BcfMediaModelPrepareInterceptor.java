/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.interceptors;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.media.interceptors.MediaModelPrepareInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.core.enums.ContainerCreationStatus;


public class BcfMediaModelPrepareInterceptor extends MediaModelPrepareInterceptor
{
	private static final Logger LOG = Logger.getLogger(BcfMediaModelPrepareInterceptor.class);
	private static final String DEFAULT_CATALOG_ID = "Default";

	@Override
	public void onPrepare(final MediaModel mediaModel, final InterceptorContext ctx) throws InterceptorException
	{
		super.onPrepare(mediaModel, ctx);
		if (isValidCatalogAndMedia(mediaModel) && ((ctx.isNew(mediaModel)) || (ctx.isModified(mediaModel) && !skipContainerCreation(
				mediaModel, ctx))))
		{
			LOG.debug("Updating ContainerCreationStatus to PENDING for media with code " + mediaModel.getCode());
			mediaModel.setContainerCreationStatus(ContainerCreationStatus.PENDING);
		}
	}

	private boolean skipContainerCreation(final MediaModel mediaModel, final InterceptorContext ctx)
	{
		return (ctx.isModified(mediaModel) && ctx.getDirtyAttributes(mediaModel)
				.containsKey(MediaModel.CONTAINERCREATIONSTATUS));
	}

	private boolean isValidCatalogAndMedia(final MediaModel mediaModel)
	{
		return (Objects.nonNull(mediaModel.getCatalogVersion()) && !StringUtils
				.equals(mediaModel.getCatalogVersion().getCatalog().getId(), DEFAULT_CATALOG_ID) && mediaModel.getOriginal() == null
				&& Objects.nonNull(mediaModel.getURL()) && StringUtils
				.equals(MediaModel._TYPECODE, mediaModel.getItemModelContext().getItemType()) && !ContainerCreationStatus.SUCCESS
				.equals(mediaModel.getContainerCreationStatus()));
	}
}
