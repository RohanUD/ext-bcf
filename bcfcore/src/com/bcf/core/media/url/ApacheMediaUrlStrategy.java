/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.media.url;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.MediaURLStrategy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


public class ApacheMediaUrlStrategy implements MediaURLStrategy
{
	private static final Logger LOG = Logger.getLogger(ApacheMediaUrlStrategy.class);

	private String mediaHostName;

	@Override
	public String getUrlForMedia(final MediaStorageConfigService.MediaFolderConfig mediaFolderConfig,
			final MediaSource mediaSource)
	{
		final String location = mediaSource.getLocation();

		LOG.debug("Create static URL for {}" + location);

		final String url = new StringBuilder(mediaHostName)
				.append('/').append(location).toString();
		LOG.debug("Media URL: {}" + url);
		return url;
	}

	@Required
	public void setMediaHostName(final String mediaHostName)
	{
		this.mediaHostName = mediaHostName;
	}

	public String getMediaHostName()
	{
		return this.mediaHostName;
	}
}
