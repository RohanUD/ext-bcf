/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.quote.dao;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;
import java.util.List;


public interface BcfQuoteDao
{
	/**
	 * Returns a paged list of maximum version (i.e. active quotes) of each quote for the specified user & store,
	 * filtered by accessible quote states.
	 *
	 * @param customerModel the customer to retrieve quotes for
	 * @param store         the store to retrieve quotes for
	 * @param isQuote       is quote
	 * @return the paged search result
	 * @throws IllegalArgumentException if any of the parameters is null or the set of quote states is empty
	 */
	List<CartModel> findCartsByCustomerAndStore(CustomerModel customerModel, BaseStoreModel store,
			final boolean isQuote);

}
