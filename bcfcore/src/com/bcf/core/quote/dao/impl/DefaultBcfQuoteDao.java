/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.quote.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.quote.dao.BcfQuoteDao;


public class DefaultBcfQuoteDao extends DefaultGenericDao<CartModel> implements BcfQuoteDao
{
	private static final String FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY =
			"SELECT {pk} from {cart} where {quote}=?quote and {user}=?user and {store}=?store";

	public DefaultBcfQuoteDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<CartModel> findCartsByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store,
			final boolean isQuote)
	{
		validateUserAndStore(store, customerModel);

		final Map<String, Object> queryParams = populateBasicQueryParams(store, customerModel, isQuote);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, queryParams);
		final SearchResult<CartModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	private void validateUserAndStore(final BaseStoreModel store, final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "Customer must not be null");
		validateParameterNotNull(store, "Store must not be null");
	}

	protected Map<String, Object> populateBasicQueryParams(final BaseStoreModel store, final CustomerModel customerModel,
			final boolean isQuote)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CartModel.USER, customerModel);
		queryParams.put(CartModel.STORE, store);
		queryParams.put(CartModel.QUOTE, isQuote ? 1 : 0);
		return queryParams;
	}

	protected SortQueryData createSortQueryData(final String sortCode, final String query)
	{
		final SortQueryData result = new SortQueryData();
		result.setSortCode(sortCode);
		result.setQuery(query);
		return result;
	}

	protected String createQuery(final String... queryClauses)
	{
		final StringBuilder queryBuilder = new StringBuilder();

		for (final String queryClause : queryClauses)
		{
			queryBuilder.append(queryClause);
		}

		return queryBuilder.toString();
	}
}
