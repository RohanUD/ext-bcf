/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.quote.service;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.store.BaseStoreModel;
import java.util.Date;
import java.util.List;


public interface BcfQuoteService extends QuoteService
{
	/**
	 * Retrieves the paged list of quotes according to the provided selection criteria.
	 *
	 * @param customerModel the customer to be used for selecting the quotes
	 * @param store         the store to be used for selecting the quotes
	 * @param isQuote       is quote
	 * @return the paged search results
	 * @throws IllegalArgumentException if any of the parameters is null
	 */
	List<CartModel> getQuoteList(CustomerModel customerModel, BaseStoreModel store, boolean isQuote);

	List<CartModel> getQuoteListBySiteAndQuote(BaseSiteModel site, boolean isQuote);

	List<CartModel> getQuoteListByQuoteAndThreshold(boolean isQuote, Date threshold);
}
