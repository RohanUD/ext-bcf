/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.quote.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.impl.DefaultQuoteService;
import de.hybris.platform.store.BaseStoreModel;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.dao.BcfCommerceCartDao;
import com.bcf.core.quote.dao.BcfQuoteDao;
import com.bcf.core.quote.service.BcfQuoteService;


public class DefaultBcfQuoteService extends DefaultQuoteService implements BcfQuoteService
{
	private BcfQuoteDao bcfQuoteDao;
	private BcfCommerceCartDao commerceCartDao;

	@Override
	public List<CartModel> getQuoteList(final CustomerModel customerModel,
			final BaseStoreModel store, final boolean isQuote)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		validateParameterNotNullStandardMessage("store", store);
		return getBcfQuoteDao().findCartsByCustomerAndStore(customerModel, store, isQuote);
	}

	@Override
	public List<CartModel> getQuoteListBySiteAndQuote(final BaseSiteModel site, final boolean isQuote)
	{
		validateParameterNotNullStandardMessage("site", site);
		return getCommerceCartDao().findCartsBySiteAndQuote(site, isQuote);
	}

	@Override
	public List<CartModel> getQuoteListByQuoteAndThreshold(final boolean isQuote, final Date threshold)
	{
		return getCommerceCartDao().findCartsByQuoteAndThreshold(isQuote, threshold);
	}

	protected BcfQuoteDao getBcfQuoteDao()
	{
		return bcfQuoteDao;
	}

	@Required
	public void setBcfQuoteDao(final BcfQuoteDao bcfQuoteDao)
	{
		this.bcfQuoteDao = bcfQuoteDao;
	}

	protected BcfCommerceCartDao getCommerceCartDao()
	{
		return commerceCartDao;
	}

	@Required
	public void setCommerceCartDao(final BcfCommerceCartDao commerceCartDao)
	{
		this.commerceCartDao = commerceCartDao;
	}
}
