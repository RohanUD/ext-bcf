/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.specialassistance.dao.impl;

import de.hybris.platform.travelservices.dao.impl.DefaultSpecialServiceRequestDao;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import java.util.List;
import com.bcf.core.specialassistance.dao.BCFSpecialServiceRequestDao;


/**
 * Handles the manipulation of Special Service Request model objects to and from persistence layer. Default
 * implementation of the {@link BCFSpecialServiceRequestDao} interface.
 */
public class DefaultBCFSpecialServiceRequestDao extends DefaultSpecialServiceRequestDao implements BCFSpecialServiceRequestDao
{

	public DefaultBCFSpecialServiceRequestDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<SpecialServiceRequestModel> getAllSpecialServiceRequest()
	{
		return find();
	}

}
