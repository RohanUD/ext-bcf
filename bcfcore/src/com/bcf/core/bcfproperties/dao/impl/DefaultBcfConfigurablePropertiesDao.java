/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.bcfproperties.dao.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.type.TypeService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.dao.BcfConfigurablePropertiesDao;
import com.bcf.core.model.BCFConfigurablePropertiesModel;
import com.bcf.core.service.BcfSalesApplicationResolverService;


/**
 * Implementation of BcfConfigurablePropertiesDao to retrieve properties in BcfConfigurableProperties
 */
public class DefaultBcfConfigurablePropertiesDao extends AbstractItemDao implements BcfConfigurablePropertiesDao
{
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private TypeService typeService;

	private static final String GET_BCF_CONFIGURABLE_PROPERTY_BY_NAME_CHANNEL_QUERY = "SELECT {"
			+ BCFConfigurablePropertiesModel.PK + "} FROM {" + BCFConfigurablePropertiesModel._TYPECODE + "}" + " WHERE {"
			+ BCFConfigurablePropertiesModel.PROPERTYNAME + "}=?" + BCFConfigurablePropertiesModel.PROPERTYNAME + " AND {"
			+ BCFConfigurablePropertiesModel.CHANNEL + "} IN (?" + BCFConfigurablePropertiesModel.CHANNEL + ")";

	/**
	 * provides implementation to Retrieve property in BcfConfigurableProperties for given property name.
	 *
	 * @return BCFConfigurablePropertiesModel
	 */
	@Override
	public BCFConfigurablePropertiesModel getBcfProperties(final String propertyName)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(BCFConfigurablePropertiesModel.PROPERTYNAME, propertyName);
		final SalesApplication channel = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		final List<EnumerationValueModel> channels = Arrays.asList(
				getTypeService().getEnumerationValue(SalesApplication._TYPECODE, channel.getCode()),
				getTypeService().getEnumerationValue(SalesApplication._TYPECODE, SalesApplication.ALL.getCode()));
		params.put(BCFConfigurablePropertiesModel.CHANNEL, channels);
		final SearchResult<BCFConfigurablePropertiesModel> searchResults = getFlexibleSearchService()
				.search(GET_BCF_CONFIGURABLE_PROPERTY_BY_NAME_CHANNEL_QUERY, params);
		if (searchResults.getResult().size() > 1)
		{
			throw new AmbiguousIdentifierException("More than one configuration found");
		}
		return searchResults.getResult().isEmpty() ? null : searchResults.getResult().get(0);
	}

	@Override
	public BCFConfigurablePropertiesModel getBcfProperties(final String propertyName, final String salesChannel)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(BCFConfigurablePropertiesModel.PROPERTYNAME, propertyName);
		final List<EnumerationValueModel> channels = Arrays.asList(
				getTypeService().getEnumerationValue(SalesApplication._TYPECODE, salesChannel));
		params.put(BCFConfigurablePropertiesModel.CHANNEL, channels);
		final SearchResult<BCFConfigurablePropertiesModel> searchResults = getFlexibleSearchService()
				.search(GET_BCF_CONFIGURABLE_PROPERTY_BY_NAME_CHANNEL_QUERY, params);
		if (searchResults.getResult().size() > 1)
		{
			throw new AmbiguousIdentifierException("More than one configuration found");
		}
		return searchResults.getResult().isEmpty() ? null : searchResults.getResult().get(0);
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}
}
