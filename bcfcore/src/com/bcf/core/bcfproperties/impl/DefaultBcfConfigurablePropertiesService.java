/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.bcfproperties.impl;

import java.util.Objects;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.bcfproperties.dao.BcfConfigurablePropertiesDao;
import com.bcf.core.model.BCFConfigurablePropertiesModel;


/**
 * Implementation of BcfConfigurablePropertiesService(also -
 * Provides business logic for retrieving BcfConfigurableProperties.
 */
public class DefaultBcfConfigurablePropertiesService implements BcfConfigurablePropertiesService
{

	private BcfConfigurablePropertiesDao bcfConfigurablePropertiesDao;

	/**
	 * implements logic to retrieve propertyName based on PropertyValue.
	 *
	 * @return String PropertyValue
	 */

	@Override
	public String getBcfPropertyValue(final String propertyName)
	{
		final BCFConfigurablePropertiesModel property = getBcfConfigurablePropertiesDao()
				.getBcfProperties(propertyName);
		return Objects.nonNull(property) ? property.getPropertyValue() : null;
	}

	@Override
	public String getBcfPropertyValue(final String propertyName, final String salesChannel)
	{
		final BCFConfigurablePropertiesModel property = getBcfConfigurablePropertiesDao()
				.getBcfProperties(propertyName, salesChannel);
		return Objects.nonNull(property) ? property.getPropertyValue() : null;
	}

	public BcfConfigurablePropertiesDao getBcfConfigurablePropertiesDao()
	{
		return bcfConfigurablePropertiesDao;
	}

	public void setBcfConfigurablePropertiesDao(final BcfConfigurablePropertiesDao bcfConfigurablePropertiesDao)
	{
		this.bcfConfigurablePropertiesDao = bcfConfigurablePropertiesDao;
	}
}
