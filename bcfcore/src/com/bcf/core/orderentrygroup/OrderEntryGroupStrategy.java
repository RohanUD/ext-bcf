/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.orderentrygroup;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.List;
import com.bcf.notification.request.order.createorder.HotelInfo;


public interface OrderEntryGroupStrategy
{
	AbstractOrderEntryGroupModel getEntryGroup(AbstractOrderEntryModel abstractOrderEntry);

	AccommodationOrderEntryGroupModel getAccommodationEntryGroup(AbstractOrderEntryModel abstractOrderEntry);

	List<AccommodationOrderEntryGroupModel> getAccommodationEntryGroup(final List<AbstractOrderEntryModel> abstractOrderEntries);

	HotelInfo populateHotelInfo(List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroup, AbstractOrderEntryModel abstractOrderEntry);

	AccommodationOfferingModel getAccommodationOffering(final AbstractOrderEntryModel abstractOrderEntry);
}
