/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.orderentrygroup.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.orderentrygroup.AbstractOrderEntryGroupStrategy;
import com.bcf.notification.request.order.createorder.HotelInfo;
import com.bcf.notification.request.order.createorder.RoomInfo;


public class AccommodationOrderEntryGroupStrategy extends AbstractOrderEntryGroupStrategy
{
	@Override
	public AbstractOrderEntryGroupModel getEntryGroup(final AbstractOrderEntryModel abstractOrderEntry)
	{
		//return getAccommodationEntryGroup(abstractOrderEntry);
		return null;
	}

	@Override
	public AccommodationOrderEntryGroupModel getAccommodationEntryGroup(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return (AccommodationOrderEntryGroupModel) abstractOrderEntry.getEntryGroup();
	}

	@Override
	public AccommodationOfferingModel getAccommodationOffering(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return ((AccommodationOrderEntryGroupModel) abstractOrderEntry.getEntryGroup()).getAccommodationOffering();
	}

	@Override
	public List<AccommodationOrderEntryGroupModel> getAccommodationEntryGroup(
			final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		Set<AccommodationOrderEntryGroupModel> accommodationEntryGroups = new HashSet<>();

		for (AbstractOrderEntryModel abstractOrderEntry : abstractOrderEntries)
		{
			List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = null;

			accommodationOrderEntryGroupModels = Collections
					.singletonList((AccommodationOrderEntryGroupModel) abstractOrderEntry.getEntryGroup());
			if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
			{
				accommodationEntryGroups.addAll(accommodationOrderEntryGroupModels);
			}
		}
		return new ArrayList<>(accommodationEntryGroups);
	}

	@Override
	public HotelInfo populateHotelInfo(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups,
			final AbstractOrderEntryModel abstractOrderEntry)
	{
		final HotelInfo hotelInfo = super.populateHotelInfo(accommodationOrderEntryGroups, abstractOrderEntry);
		populateRoomStayCandidate(hotelInfo, accommodationOrderEntryGroups);
		return hotelInfo;
	}

	protected void populateRoomStayCandidate(final HotelInfo hotelinfo,
			final List<AccommodationOrderEntryGroupModel> orderEntryGroups)
	{

		for (AccommodationOrderEntryGroupModel orderEntryGroup : orderEntryGroups)
		{
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = orderEntryGroup;
			final List<GuestCountModel> guestCounts = accommodationOrderEntryGroupModel.getGuestCounts();

			Optional<RoomInfo> roomInfo = hotelinfo.getRoomInfoList().stream().filter(
					r -> accommodationOrderEntryGroupModel.getRoomStayRefNumber() == r.getRoomStayRefNumber()
							&& accommodationOrderEntryGroupModel.getAccommodation().getName().equals(r.getRoomName()) && r
							.getCheckInDate().equals(accommodationOrderEntryGroupModel.getStartingDate()) && r.getCheckOutDate()
							.equals(accommodationOrderEntryGroupModel.getEndingDate())).findFirst();

			if (roomInfo.isPresent())
			{
				RoomInfo roomInfo1 = roomInfo.get();
				for (final GuestCountModel guestCount : guestCounts)
				{
					if (BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT.equals(guestCount.getPassengerType().getCode()))
					{
						roomInfo1.setNumberOfAdults(guestCount.getQuantity());
					}
					if (TravelservicesConstants.PASSENGER_TYPE_CODE_CHILD.equals(guestCount.getPassengerType().getCode()))
					{
						roomInfo1.setNumberOfChildren(guestCount.getQuantity());
					}
					if (TravelservicesConstants.PASSENGER_TYPE_CODE_INFANT.equals(guestCount.getPassengerType().getCode()))
					{
						roomInfo1.setNumberOfInfants(guestCount.getQuantity());
					}
				}
			}
		}
	}
}
