/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.orderentrygroup.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.orderentrygroup.AbstractOrderEntryGroupStrategy;
import com.bcf.notification.request.order.createorder.HotelInfo;
import com.bcf.notification.request.order.createorder.RoomInfo;


public class DealOrderEntryGroupStrategy extends AbstractOrderEntryGroupStrategy
{
	@Override
	public AbstractOrderEntryGroupModel getEntryGroup(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return abstractOrderEntry.getEntryGroup();
	}

	@Override
	public AccommodationOrderEntryGroupModel getAccommodationEntryGroup(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return ((DealOrderEntryGroupModel) abstractOrderEntry.getEntryGroup()).getAccommodationEntryGroups().stream()
				.findFirst().get();
	}

	@Override
	public AccommodationOfferingModel getAccommodationOffering(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return ((DealOrderEntryGroupModel) abstractOrderEntry.getEntryGroup()).getAccommodationEntryGroups().iterator().next()
				.getAccommodationOffering();
	}

	public List<AccommodationOrderEntryGroupModel> getAccommodationEntryGroup(
			final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		Set<AccommodationOrderEntryGroupModel> accommodationEntryGroups = new HashSet<>();

		for (AbstractOrderEntryModel abstractOrderEntry : abstractOrderEntries)
		{
			List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = ((DealOrderEntryGroupModel) abstractOrderEntry
					.getEntryGroup())
					.getAccommodationEntryGroups().stream()
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
			{
				accommodationEntryGroups.addAll(accommodationOrderEntryGroupModels);
			}
		}
		return new ArrayList<>(accommodationEntryGroups);
	}

	@Override
	public HotelInfo populateHotelInfo(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups,
			final AbstractOrderEntryModel abstractOrderEntry)
	{
		final HotelInfo hotelInfo = super.populateHotelInfo(accommodationOrderEntryGroups, abstractOrderEntry);
		populateRoomStayCandidate(hotelInfo, accommodationOrderEntryGroups);
		return hotelInfo;
	}

	protected void populateRoomStayCandidate(final HotelInfo hotelinfo,
			final List<AccommodationOrderEntryGroupModel> orderEntryGroup)
	{
		for (AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel : orderEntryGroup)
		{
			final DealOrderEntryGroupModel dealOrderEntryGroupModel = accommodationOrderEntryGroupModel.getDealOrderEntryGroup();

			Optional<RoomInfo> roomInfo = hotelinfo.getRoomInfoList().stream().filter(
					r -> accommodationOrderEntryGroupModel.getAccommodation().getName().equals(r.getRoomName()) && r.getCheckInDate()
							.equals(accommodationOrderEntryGroupModel.getStartingDate()) && r.getCheckOutDate()
							.equals(accommodationOrderEntryGroupModel.getEndingDate())).findFirst();

			if (roomInfo.isPresent())
			{
				RoomInfo roomInfo1 = roomInfo.get();
				roomInfo1.setNumberOfAdults(dealOrderEntryGroupModel.getNumberOfAdults());
				roomInfo1.setNumberOfChildren(
						Objects.nonNull(dealOrderEntryGroupModel.getNumberOfChild()) ? dealOrderEntryGroupModel.getNumberOfChild() : 0);
				roomInfo1.setNumberOfInfants(
						Objects.nonNull(dealOrderEntryGroupModel.getNumberOfInfant()) ?
								dealOrderEntryGroupModel.getNumberOfInfant() :
								0);
			}
		}
	}
}
