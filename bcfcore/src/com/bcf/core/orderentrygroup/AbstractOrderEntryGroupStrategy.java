/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.orderentrygroup;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.util.StreamUtil;
import com.bcf.notification.email.AddressData;
import com.bcf.notification.request.order.createorder.HotelInfo;
import com.bcf.notification.request.order.createorder.PassengerInfo;
import com.bcf.notification.request.order.createorder.RoomInfo;


public abstract class AbstractOrderEntryGroupStrategy implements OrderEntryGroupStrategy
{
	@Resource(name = "bcfAccommodationOfferingService")
	BcfAccommodationOfferingService bcfAccommodationOfferingService;

	@Override
	public HotelInfo populateHotelInfo(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups,
			final AbstractOrderEntryModel abstractOrderEntry)
	{
		final HotelInfo hotelInfo = new HotelInfo();

		hotelInfo.setName(accommodationOrderEntryGroups.get(0).getAccommodationOffering().getName());
		hotelInfo.setProviderEmails(
				accommodationOrderEntryGroups.get(0).getAccommodationOffering().getProvider().getEmails().stream().collect(
						Collectors.toList()));

		if(CollectionUtils.isNotEmpty(abstractOrderEntry.getComments())){
			hotelInfo.setNotes(abstractOrderEntry.getComments().stream().map(comment -> comment.getText()).collect(
					Collectors.toList()));
		}
		populateAddressAndContactDetails(hotelInfo, accommodationOrderEntryGroups);

		populateHotelPolicies(hotelInfo, accommodationOrderEntryGroups);
		populatePromotions(hotelInfo, abstractOrderEntry);

		final List<RoomInfo> roomInfos = new ArrayList<>();
		for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup : accommodationOrderEntryGroups)
		{
			final RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomName(accommodationOrderEntryGroup.getAccommodation().getName());
			roomInfo.setRoomStayRefNumber(accommodationOrderEntryGroup.getRoomStayRefNumber());
			roomInfo.setCheckInDate(accommodationOrderEntryGroup.getStartingDate());
			roomInfo.setCheckOutDate(accommodationOrderEntryGroup.getEndingDate());
			roomInfo.setNumberofNights(calculateNight(accommodationOrderEntryGroup.getEndingDate(),
					accommodationOrderEntryGroup.getStartingDate()));

			roomInfo.setNumberofGuests(accommodationOrderEntryGroup.getGuestCounts().size());

			final PassengerInfo travellerInfo = new PassengerInfo();
			travellerInfo.setFirstName(accommodationOrderEntryGroup.getFirstName());
			travellerInfo.setLastName(accommodationOrderEntryGroup.getLastName());
			travellerInfo.setLeadTraveller(Boolean.TRUE);
			roomInfo.setRoomPassengers(Collections.singletonList(travellerInfo));

			roomInfos.add(roomInfo);

		}
		hotelInfo.setRoomInfoList(roomInfos);

		return hotelInfo;
	}


	private void populateAddressAndContactDetails(final HotelInfo hotelInfo,
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups)
	{

		final Set<AccommodationOfferingModel> hotels = StreamUtil.safeStream(accommodationOrderEntryGroups)
				.map(AccommodationOrderEntryGroupModel::getAccommodationOffering).collect(Collectors.toSet());

		final LocationModel locationModel = StreamUtil.safeStream(hotels).findFirst().get().getLocation();
		final AddressModel addressModel = StreamUtil.safeStream(locationModel.getPointOfService()).findFirst().get().getAddress();
		final AddressData addressData = new AddressData();
		addressData.setStreetName(addressModel.getLine1());
		addressData.setStreetNumber(addressModel.getLine2());
		addressData.setCity(addressModel.getTown());
		addressData.setCountry(addressModel.getCountry().getName());
		addressData.setPostalCode(addressModel.getPostalcode());
		hotelInfo.setAddress(addressData);
		hotelInfo.setContactNumber(addressModel.getPhone1());
	}

	protected int calculateNight(final Date startingDate, final Date endingDate)
	{
		final long difference = startingDate.getTime() - endingDate.getTime();
		return (int) (difference / (1000 * 60 * 60 * 24));
	}

	private void populateHotelPolicies(final HotelInfo hotelInfo,
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups)
	{
		final Map<String, String> hotelPoliciesByAccOfferingMap = new HashMap<>();

		//since we can have more than 1 accommodationOrderEntryGroups entries for a BOOKING_PACKAGE, we need to extract hotel policies only once
		accommodationOrderEntryGroups.forEach(accommodationOrderEntryGroup -> {
			final String hotelCode = accommodationOrderEntryGroup.getAccommodationOffering().getCode();
			if (StringUtils.isBlank(hotelPoliciesByAccOfferingMap.get(hotelCode)))
			{
				hotelPoliciesByAccOfferingMap
						.put(hotelCode, accommodationOrderEntryGroup.getAccommodationOffering().getTermsAndConditions());
			}
		});
		hotelInfo.setHotelPolicies(new ArrayList<>(hotelPoliciesByAccOfferingMap.values()));
	}


	private void populatePromotions(final HotelInfo hotelInfo,
			final AbstractOrderEntryModel abstractOrderEntry)
	{
		final AbstractOrderModel order = abstractOrderEntry.getOrder();

		if (CollectionUtils.isNotEmpty(order.getAllPromotionResults()))
		{
			final Map<String, String> promotions = new HashMap<>();
			for (final PromotionResultModel promotionResultModel : order.getAllPromotionResults())
			{
				final AbstractPromotionModel promotion = promotionResultModel.getPromotion();
				if (Objects.nonNull(promotion) && promotionResultModel.getCertainty() >= 1)
				{
					final String description;
					String name = promotion.getName();
					if (promotion instanceof RuleBasedPromotionModel)
					{
						description = ((RuleBasedPromotionModel) promotion).getPromotionDescription();
					}
					else
					{
						description = promotion.getDescription();
					}

					if (StringUtils.isBlank(name))
					{
						name = promotion.getCode();
					}
					promotions.put(name, description);
				}
			}
			hotelInfo.setPromotions(promotions);
		}
	}
}
