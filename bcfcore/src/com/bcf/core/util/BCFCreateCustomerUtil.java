package com.bcf.core.util;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;


public class BCFCreateCustomerUtil
{
	@Resource(name = "userService")
	private UserService userService;

	public void setUidForRegister(final String userName, final CustomerModel customer)
	{
		customer.setUid(userName.toLowerCase());
		customer.setOriginalUid(userName);
	}


	public Set<PrincipalGroupModel> assignUserGroup(final String webUserGroup)
	{
		final Set<PrincipalGroupModel> principalGroupModels = new HashSet<>();
		if (StringUtils.isNotEmpty(webUserGroup))
		{
			final UserGroupModel userGroupModel = userService.getUserGroupForUID(webUserGroup);
			principalGroupModels.add(userGroupModel);
		}
		return principalGroupModels;
	}
}
