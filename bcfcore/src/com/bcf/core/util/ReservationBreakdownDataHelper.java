/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfqrcodegenerator.service.BcfQrCodeGeneratorService;
import com.google.zxing.WriterException;


public class ReservationBreakdownDataHelper
{
	private BcfQrCodeGeneratorService bcfQrCodeGeneratorService;

	public Map<String, String> getQRImageMap(final AbstractOrderModel order) throws IOException, WriterException
	{
		final Map<String, String> qrImageMap = new HashMap<>();
		final List<AbstractOrderEntryModel> orderEntries = order.getEntries().stream()
				.filter(entry -> entry.getType().equals(OrderEntryType.TRANSPORT)).collect(Collectors
						.toList());
		final Map<Integer, List<AbstractOrderEntryModel>> orderEntryByJourneyMap = orderEntries.stream()
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> orderEntryByJourneyRefNo : orderEntryByJourneyMap
				.entrySet())
		{
			final Map<Integer, List<AbstractOrderEntryModel>> orderEntryByOdNumberMap = orderEntryByJourneyRefNo.getValue()
					.stream()
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
			for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> orderEntryByOdMapEntry : orderEntryByOdNumberMap
					.entrySet())
			{
				final byte[] qrCodeBytes = getBcfQrCodeGeneratorService()
						.generateQRCode(order, orderEntryByOdMapEntry.getValue(), OrderEntryType.TRANSPORT);
				final String encodedImage = Base64.getEncoder().encodeToString(qrCodeBytes);
				qrImageMap.put(String.valueOf(orderEntryByJourneyRefNo.getKey())
						.concat(String.valueOf(orderEntryByOdMapEntry.getKey())), encodedImage);
			}
		}
		return qrImageMap;
	}

	protected BcfQrCodeGeneratorService getBcfQrCodeGeneratorService()
	{
		return bcfQrCodeGeneratorService;
	}

	@Required
	public void setBcfQrCodeGeneratorService(final BcfQrCodeGeneratorService bcfQrCodeGeneratorService)
	{
		this.bcfQrCodeGeneratorService = bcfQrCodeGeneratorService;
	}
}
