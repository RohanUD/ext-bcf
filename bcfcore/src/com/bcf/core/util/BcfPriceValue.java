/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import java.util.List;


public class BcfPriceValue extends PriceValue
{
	private double marginRate;
	private List<TaxValue> taxValues;

	public BcfPriceValue(final String currencyIso, final double price, final boolean netto)
	{
		super(currencyIso, price, netto);
	}

	public BcfPriceValue(final String currencyIso, final double price, final double marginRate, final boolean netto)
	{
		super(currencyIso, price, netto);
		this.marginRate = marginRate;
	}

	public BcfPriceValue(final String currencyIso, final double price, final boolean netto, final double marginRate,
			final List<TaxValue> taxValues)
	{
		super(currencyIso, price, netto);
		this.marginRate = marginRate;
		this.taxValues = taxValues;
	}

	public double getMarginRate()
	{
		return marginRate;
	}

	public List<TaxValue> getTaxValues()
	{
		return taxValues;
	}
}
