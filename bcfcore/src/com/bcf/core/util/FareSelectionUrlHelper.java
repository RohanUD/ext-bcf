/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.model.traveller.VehicleTypeModel;


public class FareSelectionUrlHelper
{
	private static final String DEPARTURELOCATION = "departureLocation";
	private static final String ARRIVALLOCATION = "arrivalLocation";
	private static final String DEPARTURELOCATIONNAME = "departureLocationName";
	private static final String ARRIVALLOCATIONNAME = "arrivalLocationName";
	private static final String DEPARTINGDATETIME = "departingDateTime";
	private static final String DEPARTURELOCATIONSUGGESTIONTYPE = "departureLocationSuggestionType";
	private static final String ARRIVALLOCATIONSUGGESTIONTYPE = "arrivalLocationSuggestionType";
	private static final String TRIPTYPE = "tripType";
	private static final String CABINCLASS = "cabinClass";
	private static final String VEHICLE_HEIGHT = "height";
	private static final String VEHICLE_LENGTH = "length";
	private static final String VEHICLE_WIDTH = "width";
	private static final String VEHICLE_TYPE = "vehicleType";
	private static final String VEHICLE_CODE = "vehicleCode";
	private static final String VEHICLE_QTY = "vehicleQty";
	private static final String VEHICLE_ROUTE = "route";
	private static final String VEHICLE = "Vehicle_";
	private static final String QUESTION = "?";
	private static final String AMP = "&";
	private static final String EQUAL = "=";
	private static final String COLON = ":";
	private static final String COMMA = ",";
	private static final String M = "M";
	private static final String JOURNEYREFNO = "journeyRefNum";
	private static final String REDIRECT_PREFIX = "redirect:";
	private static final String CARRYING_DANGEROUS_GOODS_OUTBOUND = "carryingDangerousGoodsInOutbound";
	private static final String DATE_PATTERN = "MMM dd yyyy";
	private static final String FARE_SELECTION_ROOT_URL = "/fare-selection";
	private static final String CHANGE_FARE_OR_REPLAN = "changeFareOrReplan";
	private static final String CHANGE_FARE = "changeFare";
	private static final String ORIGIN_DESTINATION_REF_NUM = "odRefNum";
	private static final String BUNDLE_TYPE_NAME = "bundleType";
	private static final String TRANSPORT_OFFERINGS = "transportOfferings";
	private static final String TRAVELLING_WITH_VEHICLE = "travellingWithVehicle";
	private static final String RETURN_WITH_DIFFERENT_VEHICLE = "returnWithDifferentVehicle";

	public static final String SUGGESTION_TYPE = "suggestionType";
	public static final String DESTINATION_LOCATION = "destinationLocation";
	public static final String DESTINATION_LOCATION_NAME = "destinationLocationName";
	public static final String ACCOMMODATION_SELECTION_ROOT_URL = "/accommodation-search";

	public String getFareSelectionUrl(final List<AbstractOrderEntryModel> abstractOrderEntry, final int refNumber,
			final int journeyRefNo, final String changeFareOrReplan)
	{

		final StringBuilder requestParams = new StringBuilder(REDIRECT_PREFIX + FARE_SELECTION_ROOT_URL);
		requestParams.append(QUESTION).append(TRIPTYPE).append(EQUAL).append(TripType.SINGLE);
		requestParams.append(AMP).append(CABINCLASS).append(EQUAL).append(M);
		requestParams.append(AMP).append(DEPARTURELOCATIONSUGGESTIONTYPE).append(EQUAL).append(SuggestionType.AIRPORTGROUP.name());
		requestParams.append(AMP).append(ARRIVALLOCATIONSUGGESTIONTYPE).append(EQUAL).append(SuggestionType.AIRPORTGROUP.name());
		requestParams.append(AMP).append(JOURNEYREFNO).append(EQUAL).append(journeyRefNo);
		requestParams.append(AMP).append(RETURN_WITH_DIFFERENT_VEHICLE).append(EQUAL).append(false);
		requestParams.append(AMP).append(CHANGE_FARE_OR_REPLAN).append(EQUAL).append(changeFareOrReplan);
		if (changeFareOrReplan.equals(CHANGE_FARE))
		{
			final List<String> offeringcodes = new ArrayList<>();
			abstractOrderEntry.get(0).getTravelOrderEntryInfo().getTransportOfferings()
					.forEach(offering -> offeringcodes.add(offering.getCode()));
			final String transportOfferingsCodes = String.join(",", offeringcodes);
			final String bundleCode = abstractOrderEntry.get(0).getBundleTemplate().getType().getCode();
			requestParams.append(AMP).append(ORIGIN_DESTINATION_REF_NUM).append(EQUAL).append(refNumber);
			requestParams.append(AMP).append(TRANSPORT_OFFERINGS).append(EQUAL).append(transportOfferingsCodes);
			requestParams.append(AMP).append(BUNDLE_TYPE_NAME).append(EQUAL).append(bundleCode);
		}
		requestParams.append(AMP).append(CARRYING_DANGEROUS_GOODS_OUTBOUND).append(EQUAL)
				.append(abstractOrderEntry.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods());
		requestParams.append(getRequestParamsForItinerary(abstractOrderEntry.get(0)));


		return requestParams.append(getRequestParamsForPassengers(abstractOrderEntry))
				.append(getRequestParamsForVehicles(abstractOrderEntry)).toString();
	}

	public String getFareSelectionUrlDuringAmendment(final List<AbstractOrderEntryModel> abstractOrderEntries, final int refNumber,
			final int journeyRefNo, final String changeFareOrReplan, final String departureTime, final String arrivalLocation,
			final String arrivalLocationName)
	{
		final StringBuilder requestParams = new StringBuilder(REDIRECT_PREFIX + FARE_SELECTION_ROOT_URL);
		requestParams.append(QUESTION).append(TRIPTYPE).append(EQUAL).append(TripType.SINGLE);
		requestParams.append(AMP).append(CABINCLASS).append(EQUAL).append(M);
		requestParams.append(AMP).append(DEPARTURELOCATIONSUGGESTIONTYPE).append(EQUAL).append(SuggestionType.AIRPORTGROUP.name());
		requestParams.append(AMP).append(ARRIVALLOCATIONSUGGESTIONTYPE).append(EQUAL).append(SuggestionType.AIRPORTGROUP.name());
		requestParams.append(AMP).append(JOURNEYREFNO).append(EQUAL).append(journeyRefNo);
		requestParams.append(AMP).append(RETURN_WITH_DIFFERENT_VEHICLE).append(EQUAL).append(false);
		requestParams.append(AMP).append(CHANGE_FARE_OR_REPLAN).append(EQUAL).append(changeFareOrReplan);
		requestParams.append(AMP).append(ORIGIN_DESTINATION_REF_NUM).append(EQUAL).append(refNumber);

		requestParams.append(AMP).append(CARRYING_DANGEROUS_GOODS_OUTBOUND).append(EQUAL)
				.append(abstractOrderEntries.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods());
		requestParams.append(getRequestParamsForItineraryDuringAmendment(abstractOrderEntries.get(0), departureTime,
				arrivalLocation, arrivalLocationName));

		final List<AbstractOrderEntryModel> fareProductEntries = abstractOrderEntries.stream()
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		return requestParams.append(getRequestParamsForPassengers(fareProductEntries))
				.append(getRequestParamsForVehicles(fareProductEntries)).toString();
	}

	/**
	 * Gets the request params for itinerary.
	 *
	 * @param abstractOrderEntryModel the cart data
	 * @return the request params for itinerary
	 */
	protected StringBuilder getRequestParamsForItinerary(final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo = abstractOrderEntryModel.getTravelOrderEntryInfo();
		final List<TransportOfferingModel> transportOfferings = travelOrderEntryInfo.getTransportOfferings().stream()
				.collect(Collectors.toList());
		final TravelRouteModel travelRoute = travelOrderEntryInfo.getTravelRoute();
		final StringBuilder requestParams = new StringBuilder();
		requestParams.append(AMP).append(DEPARTURELOCATION).append(EQUAL).append(travelRoute.getOrigin().getCode());
		requestParams.append(AMP).append(ARRIVALLOCATION).append(EQUAL).append(travelRoute.getDestination().getCode());
		requestParams.append(AMP).append(DEPARTURELOCATIONNAME).append(EQUAL).append(travelRoute.getOrigin().getName());
		requestParams.append(AMP).append(ARRIVALLOCATIONNAME).append(EQUAL).append(travelRoute.getDestination().getName());
		requestParams.append(AMP).append(DEPARTINGDATETIME).append(EQUAL)
				.append(TravelDateUtils.convertDateToStringDate(transportOfferings.get(0).getDepartureTime(), DATE_PATTERN));
		return requestParams;
	}

	/**
	 * Gets the request params for itinerary.
	 *
	 * @param abstractOrderEntryModel the cart data
	 * @return the request params for itinerary
	 */
	protected StringBuilder getRequestParamsForItineraryDuringAmendment(final AbstractOrderEntryModel abstractOrderEntryModel,
			final String departureTime, final String arrivalLocation, final String arrivalLocationName)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo = abstractOrderEntryModel.getTravelOrderEntryInfo();
		final TravelRouteModel travelRoute = travelOrderEntryInfo.getTravelRoute();
		final StringBuilder requestParams = new StringBuilder();
		requestParams.append(AMP).append(DEPARTURELOCATION).append(EQUAL).append(travelRoute.getOrigin().getCode());
		requestParams.append(AMP).append(ARRIVALLOCATION).append(EQUAL).append(arrivalLocation);
		requestParams.append(AMP).append(DEPARTURELOCATIONNAME).append(EQUAL).append(travelRoute.getOrigin().getName());
		requestParams.append(AMP).append(ARRIVALLOCATIONNAME).append(EQUAL).append(arrivalLocationName);
		requestParams.append(AMP).append(DEPARTINGDATETIME).append(EQUAL).append(departureTime);
		return requestParams;
	}

	/**
	 * Gets the request params for passengers.
	 *
	 * @param abstractOrderEntries the cart entries
	 * @return the request params for passengers
	 */
	protected StringBuilder getRequestParamsForPassengers(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{

		final StringBuilder passengerDataUrl = new StringBuilder();
		final List<PassengerTypeModel> passengerTypes = new ArrayList<>();
		abstractOrderEntries.forEach(entry -> {
			final TravellerModel traveller = entry.getTravelOrderEntryInfo().getTravellers().stream().collect(Collectors.toList())
					.get(0);
			if (TravellerType.PASSENGER.equals(traveller.getType()))
			{
				final PassengerInformationModel passengerInformation = (PassengerInformationModel) traveller.getInfo();
				passengerTypes.add(passengerInformation.getPassengerType());
			}
		});

		final Map<String, List<PassengerTypeModel>> passengerTypesMap = passengerTypes.stream()
				.collect(Collectors.groupingBy(PassengerTypeModel::getCode));
		passengerTypesMap.forEach((passengerCode, passengers) -> passengerDataUrl.append(AMP).append(passengerCode).append(EQUAL)
				.append(passengers.size()));

		return passengerDataUrl;

	}

	/**
	 * Gets the request params for vehicles.
	 *
	 * @param abstractOrderEntries the cart entries
	 * @return the request params for vehiclesgetPassengerType
	 */
	protected String getRequestParamsForVehicles(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		final List<BCFVehicleInformationModel> infoModel = new ArrayList<>();
		abstractOrderEntries.forEach(entry -> {
			final TravellerModel traveller = entry.getTravelOrderEntryInfo().getTravellers().stream().collect(Collectors.toList())
					.get(0);
			if (TravellerType.VEHICLE.equals(traveller.getType()))
			{
				infoModel.add((BCFVehicleInformationModel) traveller.getInfo());
			}
		});

		final Map<VehicleTypeModel, List<BCFVehicleInformationModel>> vehicleTypesMap = infoModel.stream()
				.collect(Collectors.groupingBy(BCFVehicleInformationModel::getVehicleType));

		final TravelOrderEntryInfoModel travelOrderEntryInfo = abstractOrderEntries.get(0).getTravelOrderEntryInfo();
		final TravelRouteModel travelRoute = travelOrderEntryInfo.getTravelRoute();
		final StringBuilder vehicleDataUrl = new StringBuilder();
		int index = 0;

		for (final Entry<VehicleTypeModel, List<BCFVehicleInformationModel>> vehicleTypesMapEntry : vehicleTypesMap.entrySet())
		{
			vehicleDataUrl.append(AMP).append(TRAVELLING_WITH_VEHICLE).append(EQUAL).append(true);
			vehicleDataUrl.append(AMP).append(VEHICLE + index++).append(EQUAL)
					.append(getVehiclePerTrip(vehicleTypesMapEntry, travelRoute));
		}
		return vehicleDataUrl.toString();
	}

	private StringBuilder getVehiclePerTrip(final Entry<VehicleTypeModel, List<BCFVehicleInformationModel>> vehicleTypesMapEntry,
			final TravelRouteModel travelRoute)
	{
		final VehicleTypeModel vehicleType = vehicleTypesMapEntry.getKey();
		final StringBuilder vehiclePerTrip = new StringBuilder();
		final List<BCFVehicleInformationModel> vehicleInfoDatas = vehicleTypesMapEntry.getValue();
		if (Objects.nonNull(vehicleType.getVehicleCategoryType()))
		{
			vehiclePerTrip.append(VEHICLE_TYPE + COLON + vehicleType.getVehicleCategoryType().getCode().toLowerCase() + COMMA);
		}

		final BCFVehicleInformationModel vehicleInfoData = vehicleInfoDatas.stream().findFirst().get();

		if (vehicleInfoData.getHeight() != 0)
		{
			vehiclePerTrip.append(VEHICLE_HEIGHT + COLON + String.valueOf(vehicleInfoData.getHeight()) + COMMA);
		}
		if (vehicleInfoData.getLength() != 0)
		{
			vehiclePerTrip.append(VEHICLE_LENGTH + COLON + String.valueOf(vehicleInfoData.getLength()) + COMMA);
		}
		if (vehicleInfoData.getWidth() != 0)
		{
			vehiclePerTrip.append(VEHICLE_WIDTH + COLON + vehicleInfoData.getWidth() + COMMA);
		}
		if (Objects.nonNull(vehicleType.getType()))
		{
			vehiclePerTrip.append(VEHICLE_CODE + COLON + vehicleType.getType() + COMMA);
		}
		if (CollectionUtils.isNotEmpty(vehicleInfoDatas))
		{
			vehiclePerTrip.append(VEHICLE_QTY + COLON + vehicleInfoDatas.size() + COMMA);
		}
		if (StringUtils.isNotEmpty(travelRoute.getCode()))
		{
			vehiclePerTrip.append(VEHICLE_ROUTE + COLON + travelRoute.getCode());
		}
		return vehiclePerTrip;
	}

	public String getAccommodationsSearchUrl(final List<AbstractOrderEntryModel> cartEntries)
	{
		if (CollectionUtils.isEmpty(cartEntries))
		{
			return StringUtils.EMPTY;
		}

		final AbstractOrderEntryModel transportOrderEntry = cartEntries.stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTransportOfferings())).findFirst()
				.orElse(null);
		if (Objects.isNull(transportOrderEntry))
		{
			return StringUtils.EMPTY;
		}

		final Collection<TransportOfferingModel> transportOfferings = transportOrderEntry.getTravelOrderEntryInfo()
				.getTransportOfferings();
		final TransportOfferingModel lastTransportOffering = transportOfferings.stream().reduce((a, b) -> b).get();
		final LocationModel transportFacilityLocation = lastTransportOffering.getDestinationTransportFacility().getLocation();
		final String checkInTime = lastTransportOffering.getArrivalTime().toString();
		final LocalDate dateTime = LocalDate
				.parse(checkInTime, DateTimeFormatter.ofPattern(BcfintegrationcoreConstants.HYBRIS_DATE_PATTERN_FOR_FERRY));
		final String formattedCheckInTime = dateTime
				.format(DateTimeFormatter.ofPattern(BcfintegrationcoreConstants.BCF_HOTEL_CALANDER_DATE_FORMAT));
		final StringBuilder urlParameters = new StringBuilder();


		urlParameters.append(DESTINATION_LOCATION);
		urlParameters.append(EQUAL);
		// We need to encode '|' character because it is not allowed in Spring Security 4
		String encodedDestinationLocation = createLocationCodes(transportFacilityLocation);
		encodedDestinationLocation = encodedDestinationLocation.replaceAll("\\|", "%7C");
		urlParameters.append(encodedDestinationLocation);
		urlParameters.append(AMP);

		urlParameters.append(DESTINATION_LOCATION_NAME);
		urlParameters.append(EQUAL);
		urlParameters.append(createLocationNames(transportFacilityLocation));
		urlParameters.append(AMP);

		urlParameters.append(SUGGESTION_TYPE);
		urlParameters.append(EQUAL);
		urlParameters.append(SuggestionType.LOCATION.toString());
		urlParameters.append(AMP);


		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
		urlParameters.append(EQUAL);
		urlParameters.append(formattedCheckInTime);
		urlParameters.append(AMP);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS);
		urlParameters.append(EQUAL);
		urlParameters.append(1);
		urlParameters.append(AMP);

		String urlParametersString = urlParameters.toString();
		urlParametersString = urlParametersString.substring(0, urlParametersString.length() - 1);
		return "/bcfstorefront"+ACCOMMODATION_SELECTION_ROOT_URL + "?" + urlParametersString;
	}

	protected String createLocationCodes(LocationModel transportFacilityLocation)
	{
		final StringBuilder locationCodesChain = new StringBuilder();
		if(transportFacilityLocation != null) {
			int index = 0;

			do {
					locationCodesChain.append(index == 0?transportFacilityLocation.getCode():"|" + transportFacilityLocation.getCode());
					++index;
					transportFacilityLocation = CollectionUtils.isNotEmpty(transportFacilityLocation.getSuperlocations())?
							transportFacilityLocation.getSuperlocations().get(0) :null;
			} while(transportFacilityLocation != null);
		}

		return locationCodesChain.toString();
	}

	protected String createLocationNames(LocationModel transportFacilityLocation)
	{
		final StringBuilder locationNamesChain = new StringBuilder();
		if(transportFacilityLocation != null) {
			int index = 0;

			do {
				locationNamesChain.append(index == 0 ? transportFacilityLocation.getName() : "," + transportFacilityLocation.getName());
				++index;
				transportFacilityLocation = CollectionUtils.isNotEmpty(transportFacilityLocation.getSuperlocations()) ?
						transportFacilityLocation.getSuperlocations().get(0) : null;
			} while(transportFacilityLocation != null);
		}

		return locationNamesChain.toString();
	}

}
