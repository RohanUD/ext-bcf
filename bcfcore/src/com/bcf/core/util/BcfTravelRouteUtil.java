/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import org.apache.commons.lang.StringUtils;


public class BcfTravelRouteUtil
{
	private static final String regex = "[^a-zA-Z0-9-]";
	private BcfTravelRouteUtil(){
		//do nothing
	}
	public static String translateName(final String name)
	{
		if (StringUtils.isEmpty(name))
		{
			return "";
		}

		return name.replace(" ", "-").replace("(", "-").replaceAll(regex, "").replaceAll("--", "-").toLowerCase();
	}
}
