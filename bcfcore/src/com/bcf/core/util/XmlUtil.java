/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class XmlUtil
{
	private static final Logger LOG = Logger.getLogger(XmlUtil.class);

	private static final String INDENT = "2";
	private static final String YES = "yes";
	private static final String XML_INDENT_AMOUNT = "{http://xml.apache.org/xslt}indent-amount";
	private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
	private static final String DELIM = " = ";
	private static final String NEW_LINE = "<br>";
	private static final String ALL_TAGS = "*";
	private static final String COMMA = ",";

	private XmlUtil()
	{

	}

	public static String format(final String body)
	{
		return format(body, false);
	}

	public static String format(final String body, final boolean removeEmptyNodes)
	{
		try
		{
			final DocumentBuilder builder = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
			final Document document = builder.parse(new InputSource(new StringReader(body)));
			if (removeEmptyNodes)
			{
				removeEmptyNodes(document);
			}
			return transformDocument(document);
		}
		catch (final TransformerException | IOException | ParserConfigurationException | SAXException e)
		{
			LOG.error("Exception formatting xml body", e);
			return body;
		}
	}

	public static String transformDocument(final Document document) throws TransformerException
	{
		final StreamResult xmlOutput = new StreamResult(new StringWriter());
		final TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		final Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, YES);
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, YES);
		transformer.setOutputProperty(XML_INDENT_AMOUNT, INDENT);
		transformer.transform(new DOMSource(document), xmlOutput);
		return xmlOutput.getWriter().toString();
	}

	public static Node findNode(final Document document, final String nodeName)
	{
		final NodeList nodeList = document.getElementsByTagName(nodeName);
		if (nodeList != null && nodeList.getLength() > 0)
		{
			return nodeList.item(0);
		}
		return null;
	}

	public static String nodeToString(final Node node)
	{
		final StringWriter stringWriter = new StringWriter();
		try
		{
			final Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, YES);
			t.setOutputProperty(OutputKeys.INDENT, YES);
			t.transform(new DOMSource(node), new StreamResult(stringWriter));
		}
		catch (final TransformerException e)
		{
			LOG.error(e);
		}
		return stringWriter.toString();
	}

	public static String convertToNodeNameValuePairs(final String body, final String ignoreNodes)
	{
		final List<String> ignoreList = Optional.ofNullable(ignoreNodes)
				.map(nodesToIgnore -> Arrays.asList(nodesToIgnore.split(COMMA)))
				.orElse(Collections.emptyList());

		final StringBuilder nodeNameValuePairsBuilder = new StringBuilder();
		try
		{
			final DocumentBuilder builder = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
			final Document document = builder.parse(new InputSource(new StringReader(body)));
			final NodeList items = document.getElementsByTagName(ALL_TAGS);
			for (int i = 0; i < items.getLength(); i++)
			{
				final Node item = items.item(i);
				if (item.getChildNodes().getLength() > 1 || item.getTextContent().trim().isEmpty())
				{
					continue;
				}
				if (ignoreList.contains(item.getNodeName()))
				{
					continue;
				}
				nodeNameValuePairsBuilder.append(item.getNodeName());
				nodeNameValuePairsBuilder.append(DELIM);
				nodeNameValuePairsBuilder.append(item.getTextContent());
				nodeNameValuePairsBuilder.append(NEW_LINE);
			}
			return nodeNameValuePairsBuilder.toString();
		}
		catch (final IOException | ParserConfigurationException | SAXException e)
		{
			LOG.error("Exception formatting xml body", e);
			return body;
		}
	}

	private static void removeEmptyNodes(final Node node)
	{
		final NodeList nodeList = node.getChildNodes();
		final List<Node> nodesToRecursivelyCall = new ArrayList<>();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			nodesToRecursivelyCall.add(nodeList.item(i));
		}
		for (final Node tempNode : nodesToRecursivelyCall)
		{
			removeEmptyNodes(tempNode);
		}
		final boolean emptyElement = node.getNodeType() == Node.ELEMENT_NODE && node.getChildNodes().getLength() == 0;
		final boolean emptyText = node.getNodeType() == Node.TEXT_NODE && node.getNodeValue().trim().isEmpty();
		if (emptyElement || emptyText)
		{
			node.getParentNode().removeChild(node);
		}
	}
}
