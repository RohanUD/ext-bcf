/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;


public final class BCFPropertiesUtils
{
	private BCFPropertiesUtils()
	{
		//Sonar Issue fix
	}

	public static List<String> convertToList(final String propertyValue)
	{
		if (StringUtils.isBlank(propertyValue))
		{
			return Collections.emptyList();
		}
		return Stream.of(propertyValue.trim().split("\\s*,\\s*")).collect(Collectors.toList());
	}
}
