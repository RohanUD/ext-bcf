/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import static com.bcf.core.constants.BcfCoreConstants.DATE_PATTERN_DD_MM_YYYY;
import static com.bcf.core.constants.BcfCoreConstants.DAY_OF_WEEK_PATTERN;

import de.hybris.platform.commerceservices.util.QuoteExpirationTimeUtils;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import com.bcf.core.constants.BcfCoreConstants;


public final class BCFDateUtils
{
	private BCFDateUtils()
	{
		//not called
	}

	/**
	 * Increase the minutes in the {@link Date} by the specified amount
	 *
	 * @param date    the {@link Date} object to increment by x minutes
	 * @param minutes the number of minutes to increment by
	 * @return Date date
	 */
	public static Date addMinutes(final Date date, final int minutes)
	{
		return DateUtils.addMinutes(date, minutes);
	}

	/**
	 * Increase the seconds in the {@link Date} by the specified amount
	 *
	 * @param date    the {@link Date} object to increment by x seconds
	 * @param seconds the number of seconds to increment by
	 * @return Date date
	 */
	public static Date addSeconds(final Date date, final int seconds)
	{
		return DateUtils.addSeconds(date, seconds);
	}

	/**
	 * Converts quote expiration time from {@link Date} to {@link String}.
	 *
	 * @param date    the date to be converted
	 * @param pattern the date pattern to be used for conversion
	 * @param locale  the locale to be used for conversion
	 * @return null or the string representation of the date as per provided date pattern
	 */
	public static String convertDateToString(final Date date, final String pattern, final Locale locale)
	{
		if (date == null)
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, locale);
		return dateFormat.format(date);
	}

	public static String convertDateToString(final Date date, final String pattern)
	{
		if (date == null)
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}

	/**
	 * Converts quote expiration time from {@link String} to {@link Date} by adjusting the time part to end of day
	 * (23:59:59).
	 *
	 * @param date    the string representation of quote expiration time
	 * @param pattern the date pattern to be used for conversion
	 * @param locale  the locale to be used for conversion
	 * @return null if the string representation of expiration time is null or empty, otherwise the {@link Date} object
	 */
	public static Date convertStringToDate(final String date, final String pattern, final Locale locale)
	{
		if (StringUtils.isEmpty(date))
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, locale);

		try
		{
			return QuoteExpirationTimeUtils.getEndOfDay(dateFormat.parse(date));
		}
		catch (final ParseException e)
		{
			throw new IllegalArgumentException(String.format("Failed to parse date [%s] using [%s] parsing format.", date, pattern),
					e);
		}
	}

	public static Date convertStringToDate(final String date, final String pattern)
	{
		if (StringUtils.isEmpty(date))
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		try
		{
			return QuoteExpirationTimeUtils.getEndOfDay(dateFormat.parse(date));
		}
		catch (final ParseException e)
		{
			throw new IllegalArgumentException(String.format("Failed to parse date [%s] using [%s] parsing format.", date, pattern),
					e);
		}
	}

	public static Date convertStringToDateTime(final String date, final String pattern)
	{
		if (StringUtils.isEmpty(date))
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		try
		{
			return dateFormat.parse(date);
		}
		catch (final ParseException e)
		{
			throw new IllegalArgumentException(String.format("Failed to parse date [%s] using [%s] parsing format.", date, pattern),
					e);
		}
	}

	public static boolean isDateInDateRange(final Date departureDate, final List<DateRangeModel> dateRangeModelList)
	{
		return dateRangeModelList.stream().anyMatch(dateRangeModel -> dateRangeModel.getStartingDate().compareTo(departureDate) <= 0
				&& dateRangeModel.getEndingDate().compareTo(departureDate) >= 0);
	}

	public static boolean isDateInDateRange(final Date departureDate, final Date startDate, final Date endDate)
	{
		return startDate.compareTo(departureDate) <= 0
				&& endDate.compareTo(departureDate) >= 0;
	}

	public static Date getDummyDate()
	{
		final String stringDate = BcfCoreConstants.SOLR_INDEXER_DATE;
		final LocalDate dateTime = LocalDate
				.parse(stringDate, DateTimeFormatter.ofPattern(BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN));
		return Date.from(dateTime.atStartOfDay(ZoneId.of("UTC")).toInstant());
	}

	public static boolean isBetweenDatesInclusive(final Date dateToCheck, final Date startDate, final Date endDate)
	{
		if (Objects.isNull(dateToCheck) || Objects.isNull(startDate) || Objects.isNull(endDate))
		{
			return false;
		}
		return startDate.compareTo(dateToCheck) * dateToCheck.compareTo(endDate) >= 0;
	}

	public static String createDaysOfWeekCode(final List<DayOfWeek> daysOfWeek)
	{
		return daysOfWeek.stream().sorted()
				.map(d -> String.valueOf(d.ordinal()))
				.collect(Collectors.joining());
	}

	public static boolean isDayOfWeekExistInDayOfWeekCode(final Date date, final String daysOfWeekCode)
	{
		final String dayOfWeekStrFromDate = new SimpleDateFormat(DAY_OF_WEEK_PATTERN).format(date);
		final int dayOfWeekOrdinal = DayOfWeek.valueOf(dayOfWeekStrFromDate.toUpperCase()).ordinal();

		return daysOfWeekCode
				.contains(String.valueOf(dayOfWeekOrdinal));
	}

	public static Date setToStartOfDay(final Date date)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date setToEndOfDay(final Date date)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 59);
		return cal.getTime();
	}

	public static String convertIntoHoursAndMin(final long timeInMin)
	{
		String hoursAndMinString = timeInMin / 60 + "h ";
		hoursAndMinString += timeInMin % 60 + "m";
		return hoursAndMinString;
	}

	public static boolean checkIfDatesOverlapped(Date start1, Date end1, Date start2, Date end2)
	{
		return (start1.equals(end2) || start1.before(end2)) && (start2.equals(end1) || start2.before(end1));
	}

	/**
	 * checks if the date part of the dates provided are equal or not
	 *
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isDatePartEqual(Date d1, Date d2)
	{
		Date date1 = DateUtils.truncate(d1, Calendar.DATE);
		Date date2 = DateUtils.truncate(d2, Calendar.DATE);
		return DateUtils.isSameDay(date1, date2);
	}

	public static boolean isDatePartEqual(final String dateStr1, final String dateStr2)
	{
		Date date1 = TravelDateUtils.getDate(dateStr1, DATE_PATTERN_DD_MM_YYYY);
		Date date2 = TravelDateUtils.getDate(dateStr2, DATE_PATTERN_DD_MM_YYYY);

		return isDatePartEqual(date1, date2);
	}

	public static boolean isDateInPast(final Date date)
	{
		Date currentDate = setToStartOfDay(new Date());
		return date.before(currentDate);
	}
}
