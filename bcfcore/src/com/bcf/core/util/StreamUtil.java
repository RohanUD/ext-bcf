/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class StreamUtil
{
	private StreamUtil()
	{

	}

	public static <T> Stream<T> safeStream(final Collection<T> collection)
	{
		return Optional.ofNullable(collection).map(Collection::stream).orElse(Stream.empty());
	}

	public static <T> Predicate<T> distinctByKey(final Function<? super T, Object> keyExtractor)
	{
		final Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	public static <T> List<T> concatenate(final List<T>... lists)
	{
		Stream<T> stream = Stream.of();
		for (final List<T> l : lists)
			stream = Stream.concat(stream, l.stream());
		return stream.collect(Collectors.toList());
	}
}
