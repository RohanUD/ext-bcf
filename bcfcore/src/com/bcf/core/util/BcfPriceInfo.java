/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import java.util.List;


public class BcfPriceInfo
{
	private final String currencyIso;
	private final double basePrice;
	private final double marginValue;
	private final List<DiscountValue> discountValues;
	private final double costToBcf;
	private final List<TaxValue> hiddenTaxValues;
	private final double netPriceVal;
	private final TaxValue gstTaxValue;
	private final double totalPrice;
	private long quantity;

	public BcfPriceInfo(final String currencyIso, final double basePrice, final double marginValue,
			final List<DiscountValue> discountValues, final double costToBcf, final List<TaxValue> hiddenTaxValues,
			final double netPriceVal, final TaxValue gstTaxValue, final double totalPrice, long quantity)
	{
		this.currencyIso = currencyIso;
		this.basePrice = basePrice;
		this.marginValue = marginValue;
		this.discountValues = discountValues;
		this.costToBcf = costToBcf;
		this.hiddenTaxValues = hiddenTaxValues;
		this.netPriceVal = netPriceVal;
		this.gstTaxValue = gstTaxValue;
		this.totalPrice = totalPrice;
		this.quantity = quantity;
	}

	public String getCurrencyIso()
	{
		return currencyIso;
	}

	public double getBasePrice()
	{
		return basePrice;
	}

	public double getMarginValue()
	{
		return marginValue;
	}

	public List<DiscountValue> getDiscountValues()
	{
		return discountValues;
	}

	public double getCostToBcf()
	{
		return costToBcf;
	}

	public List<TaxValue> getHiddenTaxValues()
	{
		return hiddenTaxValues;
	}

	public double getNetPriceVal()
	{
		return netPriceVal;
	}

	public TaxValue getGstTaxValue()
	{
		return gstTaxValue;
	}

	public double getTotalPrice()
	{
		return totalPrice;
	}

	public long getQuantity()
	{
		return quantity;
	}
}
