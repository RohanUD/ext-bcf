package com.bcf.core.util;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import java.util.List;
import com.bcf.core.enums.BCFPaymentMethodType;


public class BCFPaymentMethodUtil
{
	public static BCFPaymentMethodType getBCFPaymentMethodType(final AbstractOrderModel abstractOrderModel)
	{
		List<PaymentTransactionModel> paymentTransactionModels = abstractOrderModel.getPaymentTransactions();
		for (PaymentTransactionModel paymentTransactionModel : paymentTransactionModels)
		{
			List<PaymentTransactionEntryModel> paymentTransactionEntryModels = paymentTransactionModel.getEntries();
			for (PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionEntryModels)
			{
				return paymentTransactionEntryModel.getTransactionType();
			}
		}

		return null;
	}
}
