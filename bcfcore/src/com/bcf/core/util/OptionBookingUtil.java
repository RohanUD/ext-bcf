/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 5/7/19 12:19 PM
 */

package com.bcf.core.util;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class OptionBookingUtil
{
	private OptionBookingUtil()
	{
		//deliberately left empty
	}

	public static boolean isOptionBooking(AbstractOrderModel cart)
	{
		if (cart instanceof CartModel)
		{
			return ((CartModel) cart).isVacationOptionBooking();
		}
		return false;
	}

	public static boolean isOptionBooking(List<AbstractOrderEntryModel> orderEntryModels)
	{
		AbstractOrderEntryModel abstractOrderEntryModel = StreamUtil.safeStream(orderEntryModels).filter(Objects::nonNull)
				.findFirst()
				.orElse(null);

		if (Objects.nonNull(abstractOrderEntryModel))
		{
			AbstractOrderModel abstractOrderModel = abstractOrderEntryModel.getOrder();

			return OptionBookingUtil.isOptionBooking(abstractOrderModel);
		}
		return false;
	}

	public static boolean wasOptionBookingAtAnyTime(final AbstractOrderModel cart)
	{
		if (cart instanceof CartModel)
		{
			return ((CartModel) cart).isAlreadyCreatedFromOptionBooking();
		}
		return false;
	}

	public static boolean isOptionBookingUpdate(final CartModel cart)
	{
		return cart.isAlreadyCreatedFromOptionBooking();
	}

	public static Date getExpiryDate(final CartModel cartModel)
	{
		return cartModel.getOptionBookingExpiryDate();
	}

	public static Date getDepartureDate(final AbstractOrderModel abstractOrder)
	{
		AbstractOrderEntryModel abstractOrderEntryModel = StreamUtil.safeStream(abstractOrder.getEntries())
				.filter(entry -> entry.getType().equals(
						OrderEntryType.TRANSPORT)).findFirst().orElse(null);
		if (abstractOrderEntryModel != null && Objects.nonNull(abstractOrderEntryModel.getTravelOrderEntryInfo()))
		{
			Optional<TransportOfferingModel> firstTransportOffering = abstractOrderEntryModel.getTravelOrderEntryInfo()
					.getTransportOfferings().stream().findFirst();
			if (firstTransportOffering.isPresent())
			{
				return firstTransportOffering.get().getDepartureTime();
			}
		}

		AbstractOrderEntryModel accommodationOrderEntryModel = StreamUtil.safeStream(abstractOrder.getEntries())
				.filter(entry -> entry.getType().equals(OrderEntryType.ACCOMMODATION)).findFirst().orElse(null);
		if (Objects.nonNull(accommodationOrderEntryModel) && Objects
				.nonNull(accommodationOrderEntryModel.getAccommodationOrderEntryInfo()))
		{
			Optional<Date> minDate = accommodationOrderEntryModel.getAccommodationOrderEntryInfo().getDates().stream()
					.min(Date::compareTo);

			if (minDate.isPresent())
			{
				return minDate.get();
			}
		}

		AbstractOrderEntryModel activityOrderEntryModel = StreamUtil.safeStream(abstractOrder.getEntries())
				.filter(entry -> entry.getType().equals(OrderEntryType.ACTIVITY)).findFirst().orElse(null);
		if (Objects.nonNull(activityOrderEntryModel) && Objects
				.nonNull(activityOrderEntryModel.getActivityOrderEntryInfo()))
		{
			return activityOrderEntryModel.getActivityOrderEntryInfo().getActivityDate();
		}
		return null;
	}

	public static boolean isClonedOptionBooking(final AbstractOrderModel abstractOrder)
	{
		return Objects.nonNull(abstractOrder) && abstractOrder.getCode().contains("_cloned");
	}

	public static int getHoldTimeInSeconds(final CartModel cartModel)
	{
		Date d1 = new Date();
		Date d2 = cartModel.getOptionBookingExpiryDate();
		return (int) ((d2.getTime() - d1.getTime()) / 1000);
	}
}
