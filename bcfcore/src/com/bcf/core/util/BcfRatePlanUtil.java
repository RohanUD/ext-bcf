/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.accommodation.MinimumNightsStayRulesModel;


public final class BcfRatePlanUtil
{
	private BcfRatePlanUtil(){
		//do nothing
	}
	public static boolean isValidRatePlan(final Date dateOfStay, final RatePlanModel ratePlan)
	{
		final RoomRateProductModel roomRateProduct = getValidRoomRateProduct(dateOfStay, ratePlan);
		return Objects.nonNull(roomRateProduct);
	}

	public static RoomRateProductModel getValidRoomRateProduct(final Date dateOfStay, final RatePlanModel ratePlan)
	{
		if (Objects.isNull(ratePlan) || CollectionUtils.isEmpty(ratePlan.getProducts()))
		{
			return null;
		}

		final List<RoomRateProductModel> roomRateProducts = ratePlan.getProducts().stream()
				.filter(RoomRateProductModel.class::isInstance).map(RoomRateProductModel.class::cast)
				.collect(Collectors.toList());

		return roomRateProducts.stream()
				.filter(roomRateProductModel -> isValidForDateRanges(dateOfStay, roomRateProductModel)).findFirst().orElse(null);
	}

	public static boolean isValidForDateRanges(final Date dateOfStay, final RoomRateProductModel roomRateProduct)
	{
		for (final DateRangeModel dateRangeModel : roomRateProduct.getDateRanges())
		{
			if (BCFDateUtils.isBetweenDatesInclusive(dateOfStay, dateRangeModel.getStartingDate(), dateRangeModel.getEndingDate())
					&& isValidForDayOfWeek(dateOfStay, roomRateProduct.getDaysOfWeek()))
			{
				return true;
			}
		}
		return false;
	}

	public static boolean isValidForDayOfWeek(final Date date, final List<DayOfWeek> daysOfWeek)
	{
		final String dayOfWeek = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
				.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).toUpperCase();
		return daysOfWeek.contains(DayOfWeek.valueOf(dayOfWeek));
	}

	public static Set<RatePlanModel> getValidRatePlansAgainstDates(final Collection<RatePlanModel> ratePlans,
			final Date checkInDate, final Date checkOutDate)
	{
		final Set<RatePlanModel> validRatePlans = new HashSet<>();
		for (Date date = checkInDate; !TravelDateUtils.isSameDate(date, checkOutDate); date = DateUtils
				.addDays(date, 1))
		{
			final RatePlanModel ratePlan = getValidRatePlansForDate(ratePlans, date);
			if (ratePlan != null)
			{
				validRatePlans.add(ratePlan);
			}
		}
		return validRatePlans;
	}

	public static boolean isValidForMinimumNightsStayRule(final Collection<MinimumNightsStayRulesModel> minimumNightsStayRulesModels,
			final StayDateRangeData stayDateRangeData)
	{
		if(CollectionUtils.isEmpty(minimumNightsStayRulesModels)){
			return true;
		}

		for(MinimumNightsStayRulesModel minimumNightsStayRulesModel: minimumNightsStayRulesModels)
		{
			for (Date dateToCheck = stayDateRangeData.getStartTime(); dateToCheck
					.before(stayDateRangeData.getEndTime()); dateToCheck = TravelDateUtils.addDays(dateToCheck, 1))
			{
				if (BCFDateUtils.isBetweenDatesInclusive(dateToCheck, minimumNightsStayRulesModel.getStartDate(),
						minimumNightsStayRulesModel.getEndDate()) && isValidForDayOfWeek(dateToCheck,
						minimumNightsStayRulesModel.getDaysOfWeek()))
				{
					if (getLengthOfStay(stayDateRangeData,minimumNightsStayRulesModel) < minimumNightsStayRulesModel.getMinimumNightsStay())
					{
						return false;
					}

				}
			}
		}

		return true;
	}


	public static boolean checkIfEligibleToAdd(
			AccommodationOfferingDayRateData accommodationOfferingDayRateData, List<RoomStayCandidateData> roomStayCandidates)
	{

		for(RoomStayCandidateData roomStayCandidate:roomStayCandidates)
		{
			int totalCount = 0;
			int adultCount = 0;
			final Optional<Integer> totalCountOption = roomStayCandidate.getPassengerTypeQuantityList().stream()
					.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum);

			if (totalCountOption.isPresent())
			{
				totalCount = totalCountOption.get().intValue();
			}

			final Optional<Integer> adultCountOption = roomStayCandidate.getPassengerTypeQuantityList().stream()
					.filter(passengerTypeQuantity -> passengerTypeQuantity.getPassengerType().getCode().equals(
							BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT))
					.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum);
			if (adultCountOption.isPresent())
			{
				adultCount = adultCountOption.get().intValue();
			}

			int adultMaxCount =accommodationOfferingDayRateData.getAdultsCount();
			int totalMaxCount =accommodationOfferingDayRateData.getMaxOccupancy();

			if (adultMaxCount < adultCount || totalMaxCount < totalCount)
			{

				return false;
			}


		}

		return true;
	}


	private static int getLengthOfStay( StayDateRangeData stayDateRangeData, MinimumNightsStayRulesModel minimumNightsStayRulesModel){

		if (BCFDateUtils.isBetweenDatesInclusive(stayDateRangeData.getStartTime(), minimumNightsStayRulesModel.getStartDate(),
				minimumNightsStayRulesModel.getEndDate()) && BCFDateUtils.isBetweenDatesInclusive(stayDateRangeData.getEndTime(), minimumNightsStayRulesModel.getStartDate(),
						minimumNightsStayRulesModel.getEndDate())){

			return stayDateRangeData.getLengthOfStay();
		}


		if (BCFDateUtils.isBetweenDatesInclusive(stayDateRangeData.getStartTime(), minimumNightsStayRulesModel.getStartDate(),
				minimumNightsStayRulesModel.getEndDate())){
			return (int) TravelDateUtils.getDaysBetweenDates(stayDateRangeData.getStartTime(), minimumNightsStayRulesModel.getEndDate());


		}

		if (BCFDateUtils.isBetweenDatesInclusive(stayDateRangeData.getEndTime(), minimumNightsStayRulesModel.getStartDate(),
				minimumNightsStayRulesModel.getEndDate())){
			return (int) TravelDateUtils.getDaysBetweenDates(minimumNightsStayRulesModel.getStartDate(), stayDateRangeData.getEndTime());


		}

		return 1;
	}


	protected static RatePlanModel getValidRatePlansForDate(final Collection<RatePlanModel> ratePlans, final Date date)
	{
		return ratePlans.stream().filter(ratePlan -> isValidRatePlan(date, ratePlan)).findFirst().orElse(null);
	}
}
