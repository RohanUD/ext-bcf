/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import java.math.BigDecimal;
import java.util.Objects;


public final class PrecisionUtil
{
	private static final int DEFAULT_SCALE = 2;
	private PrecisionUtil(){
		//do nothing
	}
	private static final double POSITIVE_ZERO = 0d;

	public static double round(final double x, final int scale)
	{
		return round(x, scale, BigDecimal.ROUND_HALF_UP);
	}

	public static double round(final double x)
	{
		return round(x, DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
	}

	public static double round(final double x, final int scale, final int roundingMethod)
	{
		try
		{
			final double rounded = (new BigDecimal(Double.toString(x)).setScale(scale, roundingMethod)).doubleValue();
			return rounded == POSITIVE_ZERO ? POSITIVE_ZERO * x : rounded;
		}
		catch (final NumberFormatException ex)
		{
			return (Double.isInfinite(x)) ? x : Double.NaN;
		}
	}

	public static Double nickelRound(final Double num)
	{
		return Objects.nonNull(num) ? Double.valueOf(Math.round(num * 20) / 20.0) : num;
	}
}
