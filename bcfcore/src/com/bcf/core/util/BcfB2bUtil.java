/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.util;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class BcfB2bUtil
{
	private SessionService sessionService;

	public B2BUnitModel getB2bUnitInContext()
	{
		final Object b2bCheckoutContextDataObject = sessionService
				.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);

		if (b2bCheckoutContextDataObject != null && (b2bCheckoutContextDataObject instanceof B2BCheckoutContextData))
		{
			final B2BCheckoutContextData b2bCheckoutContextData = (B2BCheckoutContextData) b2bCheckoutContextDataObject;

			final B2BUnitModel b2bUnit = b2bCheckoutContextData.getB2bUnit();

			return b2bUnit;
		}
		return null;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
