/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.commercefacades.user.data.CountryData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.CollectionUtils;


public class CountryDropDownUtil
{
	public List<CountryData> getAllCountry(final List<CountryData> allCountry)
	{
		final List<CountryData> preferredCountries = allCountry.stream().filter(
				countryData -> StringUtils.equals("Canada", countryData.getName()) || StringUtils
						.equals("United States", countryData.getName())).collect(
				Collectors.toList());
		final Collection<CountryData> otherCountries = CollectionUtils.subtract(allCountry, preferredCountries);
		final List<CountryData> arrangedCountries = new ArrayList<>(preferredCountries);
		arrangedCountries.addAll(otherCountries);
		return arrangedCountries;
	}




}
