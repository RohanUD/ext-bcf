/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.util;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.site.BaseSiteService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;


/**
 * Helper class for CmsSite related utilities
 */
public class CMSSiteHelper
{
	private CMSSiteService cmsSiteService;
	private BaseSiteService baseSiteService;

	/**
	 * gets all the active content catalog versions from the current site
	 *
	 * @return list of type {@link CatalogVersionModel}
	 */
	public static List<CatalogVersionModel> getAllActiveContentCatalogVersions()
	{
		final CMSSiteHelper cmsSiteHelper = Registry.getApplicationContext().getBean("cmsSiteHelper", CMSSiteHelper.class);
		return cmsSiteHelper.getActiveContentCatalogVersions();
	}

	public List<CatalogVersionModel> getActiveContentCatalogVersions()
	{
		final List<CatalogVersionModel> activeCatalogVersions = new ArrayList<>();
		CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite == null)
		{
			currentSite = (CMSSiteModel) baseSiteService.getBaseSiteForUID(BcfCoreConstants.DEFAULT_SITE_ID);
		}
		final List<ContentCatalogModel> cotentCatalogs = currentSite.getContentCatalogs();

		for (final CatalogModel catalogModel : cotentCatalogs)
		{
			activeCatalogVersions.add(catalogModel.getActiveCatalogVersion());
		}
		return activeCatalogVersions;
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}


}
