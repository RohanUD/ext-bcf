/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.stock.accommodation.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.strategies.stock.accommodation.impl.RoomRateManageStockStrategy;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class BcfRoomRateManageStockStrategy extends RoomRateManageStockStrategy
{
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;


	@Override
	public void reserve(final AbstractOrderEntryModel abstractOrderEntry) throws InsufficientStockLevelException
	{
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getBcfTravelCommerceCartService()
				.getAccommodationOrderEntryGroup(abstractOrderEntry);
		reserveStockForAccommodation(accommodationOrderEntryGroup, abstractOrderEntry.getAccommodationOrderEntryInfo().getDates());
	}

	@Override
	public void release(final AbstractOrderEntryModel abstractOrderEntry)
	{
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getBcfTravelCommerceCartService()
				.getAccommodationOrderEntryGroup(abstractOrderEntry);
		final int qty = abstractOrderEntry.getQuantity().intValue();
		if (qty > 0)
		{
			releaseStockForAccommodation(accommodationOrderEntryGroup,
					abstractOrderEntry.getAccommodationOrderEntryInfo().getDates());
		}
	}

	/**
	 * @return the bcfTravelCommerceCartService
	 */
	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	/**
	 * @param bcfTravelCommerceCartService
	 *           the bcfTravelCommerceCartService to set
	 */
	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}
}
