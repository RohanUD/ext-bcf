/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import java.util.Map;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.integration.payment.data.TransactionDetails;


public interface BcfPaymentTransactionStrategy
{
	boolean createPaymentTransactions(CustomerModel customerModel, TransactionDetails transactionDetails,
			PaymentTransactionType paymentTransactionType, BCFPaymentMethodType bcfPaymentMethodType,
			AbstractOrderModel abstractOrderModel, final Map<String, String> resultMap, String refundReason,
			boolean newTransaction);

	boolean createPaymentTransactions(CustomerModel customerModel, Double amountToPay,
			PaymentTransactionType paymentTransactionType,
			AbstractOrderModel abstractOrderModel, BCFPaymentMethodType bcfPaymentMethodType ,final Map<String, String> resultMap);

	void createPaymentTransactionEntry(final PaymentTransactionModel transaction, final CustomerModel customerModel,
			final Double amountToPay,
			final PaymentTransactionType paymentTransactionType, final AbstractOrderModel abstractOrderModel,
			final Map<String, String> resultMap, String refundReason);

	boolean createPaymentTransactionEntry(final CustomerModel customerModel, final TransactionDetails transactionDetails,
			final PaymentTransactionType paymentTransactionType, final BCFPaymentMethodType bcfPaymentMethodType,
			final AbstractOrderModel abstractOrderModel,
			final Map<String, String> resultMap, final PaymentTransactionModel transaction, String refundReason);

	BCFPaymentMethodType getPaymentMethodType(final PaymentTransactionModel transaction, final Map<String, String> resultMap);
}
