/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;


public class DefaultBcfPriceInfoCalculationStrategy implements FindBcfPriceInfoStrategy
{
	private CommonI18NService commonI18NService;

	@Override
	public BcfPriceInfo findBcfPriceInfo(final AbstractOrderEntryModel entry)
	{
		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		final Double basePrice = PrecisionUtil.round(entry.getBasePrice());
		return new BcfPriceInfo(currencyIsoCode, basePrice, 0d, null, 0d, Collections.emptyList(), 0d, null, 0d,entry.getQuantity());
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
