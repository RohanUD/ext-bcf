/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.strategies.stock.TravelManageStockByEntryTypeStrategy;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.strategies.StockReleaseStrategy;


public class AccommodationStockReleaseStrategy implements StockReleaseStrategy
{
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy;

	@Override
	public void releaseStock(List<AbstractOrderEntryModel> abstractOrderEntryModels){

		final List<AbstractOrderEntryModel> accommodationEntries = abstractOrderEntryModels.stream()
				.filter(entry -> entry.getProduct().getItemtype().equals(RoomRateProductModel.ITEMTYPE) && !entry.getActive() && entry.isStockAllocated() && AmendStatus.CHANGED.getCode().equals(entry.getAmendStatus().getCode())
						&& isStockTypeStandard(entry)).collect(Collectors.toList());
		for (final AbstractOrderEntryModel accommodationEntry : accommodationEntries)
		{
			getAccommodationManageStockStrategy().release(accommodationEntry);
		}

	}

	private boolean isStockTypeStandard(final AbstractOrderEntryModel entry)
	{
		return Objects.equals(StockLevelType.STANDARD, getBcfTravelCommerceStockService().getStockLevelType(entry.getProduct()));
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public TravelManageStockByEntryTypeStrategy getAccommodationManageStockStrategy()
	{
		return accommodationManageStockStrategy;
	}

	public void setAccommodationManageStockStrategy(
			final TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy)
	{
		this.accommodationManageStockStrategy = accommodationManageStockStrategy;
	}
}
