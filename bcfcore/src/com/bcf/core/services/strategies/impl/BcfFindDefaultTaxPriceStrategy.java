/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.bcf.core.services.strategies.BcfFindTaxByProductTypeStrategy;


public class BcfFindDefaultTaxPriceStrategy implements BcfFindTaxByProductTypeStrategy
{
	private List<FindTaxValuesStrategy> findTaxesStrategies;

	@Override
	public List<TaxValue> findTaxValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (getFindTaxesStrategies().isEmpty())
		{
			return Arrays.asList(new TaxValue("CA-GST", 0.0D, true, entry.getOrder().getCurrency().getIsocode()));
		}
		else
		{
			final List<TaxValue> result = new ArrayList<TaxValue>();
			for (final FindTaxValuesStrategy findStrategy : getFindTaxesStrategies())
			{
				result.addAll(findStrategy.findTaxValues(entry));
			}
			return result;
		}
	}

	public List<FindTaxValuesStrategy> getFindTaxesStrategies()
	{
		return findTaxesStrategies;
	}

	public void setFindTaxesStrategies(final List<FindTaxValuesStrategy> findTaxesStrategies)
	{
		this.findTaxesStrategies = findTaxesStrategies;
	}

}
