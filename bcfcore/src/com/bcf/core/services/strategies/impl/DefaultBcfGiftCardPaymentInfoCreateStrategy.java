/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.GiftCardType;
import com.bcf.core.services.strategies.BcfGiftCardPaymentInfoCreateStrategy;


public class DefaultBcfGiftCardPaymentInfoCreateStrategy implements BcfGiftCardPaymentInfoCreateStrategy
{
	public static final String GIFT_CARD_TYPE = "giftCardType";
	private UserService userService;
	private ModelService modelService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Override
	public GiftCardPaymentInfoModel savePaymentInfo(final CustomerModel customerModel, final Map<String, String> resultMap,
			final AbstractOrderModel abstractOrderModel)
	{
		return createGiftCardPaymentInfo(resultMap, customerModel);
	}

	protected GiftCardPaymentInfoModel createGiftCardPaymentInfo(final Map<String, String> resultMap, final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		final GiftCardPaymentInfoModel giftCardPaymentInfoModel = getModelService().create(GiftCardPaymentInfoModel.class);
		giftCardPaymentInfoModel.setUser(customerModel);
		giftCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
		giftCardPaymentInfoModel.setGiftCardNumber(resultMap.get("giftCardNo"));
		if (Objects.nonNull(resultMap.get(GIFT_CARD_TYPE)))
		{
			giftCardPaymentInfoModel
					.setGiftCardType(enumerationService.getEnumerationValue(GiftCardType.class, resultMap.get("giftCardType")));
		}
		giftCardPaymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
		return giftCardPaymentInfoModel;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
