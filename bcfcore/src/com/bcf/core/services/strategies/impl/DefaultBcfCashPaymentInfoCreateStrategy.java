/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.strategies.BcfCashPaymentInfoCreateStrategy;


public class DefaultBcfCashPaymentInfoCreateStrategy implements BcfCashPaymentInfoCreateStrategy
{
	private UserService userService;
	private ModelService modelService;

	@Override
	public CashPaymentInfoModel savePaymentInfo(final CustomerModel customerModel, final Map<String, String> resultMap,
			final AbstractOrderModel abstractOrderModel)
	{
		return createCashPaymentInfo(resultMap, customerModel);
	}

	protected CashPaymentInfoModel createCashPaymentInfo(final Map<String, String> resultMap, final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		final CashPaymentInfoModel cashPaymentInfoModel = getModelService().create(CashPaymentInfoModel.class);
		cashPaymentInfoModel.setUser(customerModel);
		cashPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
		cashPaymentInfoModel.setReceiptNumber(resultMap.get("receiptNo"));
		cashPaymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
		return cashPaymentInfoModel;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
