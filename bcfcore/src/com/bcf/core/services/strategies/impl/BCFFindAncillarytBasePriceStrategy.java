/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.util.PriceValue;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.services.strategies.BcfFindBasePriceByProductTypeStrategy;
import com.bcf.core.util.PrecisionUtil;


public class BCFFindAncillarytBasePriceStrategy implements BcfFindBasePriceByProductTypeStrategy
{
	private BCFTravelCommercePriceService bcfTravelCommercePriceService;

	@Override
	public PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (Objects.isNull(entry.getTravelOrderEntryInfo()) || Objects.isNull(entry.getTravelOrderEntryInfo().getTravelRoute()))
		{
			throw new CalculationException("travel order entry info  is null or travel route not specified");
		}

		final Double basePrice = Objects.nonNull(entry.getBasePrice()) ? PrecisionUtil.round(entry.getBasePrice()) : 0.0d;
		if (entry.isMarginCalculated()
				|| !(entry.getEntryGroup() instanceof DealOrderEntryGroupModel))
		{
			return new PriceValue(entry.getOrder().getCurrency().getIsocode(), basePrice, true);
		}

		final TravelRouteModel travelRoute = entry.getTravelOrderEntryInfo().getTravelRoute();
		final PriceValue basePriceValue = new PriceValue(entry.getOrder().getCurrency().getIsocode(), basePrice, true);
		entry.setMarginCalculated(true);
		return getBcfTravelCommercePriceService().applyMargin(basePriceValue, travelRoute);
	}

	/**
	 * @return the bcfTravelCommercePriceService
	 */
	protected BCFTravelCommercePriceService getBcfTravelCommercePriceService()
	{
		return bcfTravelCommercePriceService;
	}

	/**
	 * @param bcfTravelCommercePriceService the bcfTravelCommercePriceService to set
	 */
	@Required
	public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
	{
		this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
	}
}
