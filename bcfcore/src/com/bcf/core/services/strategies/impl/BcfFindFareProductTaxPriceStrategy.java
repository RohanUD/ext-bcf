/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.services.strategies.BcfFindTaxByProductTypeStrategy;


public class BcfFindFareProductTaxPriceStrategy implements BcfFindTaxByProductTypeStrategy
{

	@Override
	public List<TaxValue> findTaxValues(final AbstractOrderEntryModel entry)
	{
		return CollectionUtils.isNotEmpty(entry.getTaxValues()) ?
				new ArrayList<>(entry.getTaxValues()) :
				Collections.emptyList();
	}

}
