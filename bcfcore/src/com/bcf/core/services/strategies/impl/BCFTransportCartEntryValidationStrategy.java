/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;


import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.strategies.cart.validation.impl.TransportCartEntryValidationStrategy;
import java.util.Objects;


public class BCFTransportCartEntryValidationStrategy extends TransportCartEntryValidationStrategy
{

	@Override
	public CommerceCartModification validate(CartEntryModel cartEntryModel)
	{

		if (Objects.nonNull(cartEntryModel.getOrder().getOriginalOrder()))
		{
			super.validate(cartEntryModel);
		}
		long cartEntryLevel1 = cartEntryModel.getQuantity().longValue();
		return this.createModification("success", cartEntryLevel1, cartEntryLevel1, cartEntryModel);
	}

	@Override
	protected long getCartLevel(CartEntryModel cartEntryModel, CartModel cartModel)
	{
		if (Objects.isNull(cartModel.getOriginalOrder()))
		{
			return 1L;
		}
		return super.getCartLevel(cartEntryModel, cartModel);
	}
}
