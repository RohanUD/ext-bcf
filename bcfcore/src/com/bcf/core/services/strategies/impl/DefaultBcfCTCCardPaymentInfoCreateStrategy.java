/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.model.ModelCloningContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.services.strategies.BcfCTCCardPaymentInfoCreateStrategy;
import com.bcf.core.user.BcfUserService;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class DefaultBcfCTCCardPaymentInfoCreateStrategy implements BcfCTCCardPaymentInfoCreateStrategy
{
	private BcfUserService userService;
	private ModelService modelService;
	private BcfCustomerAccountService customerAccountService;
	private ModelCloningContext paymentInfoCloningContext;
	@Resource(name="sessionService")
	private SessionService sessionService;
	public static final String SAVED_CTC_CARD_CODE = "savedCTCcardCode";
	public static final String SAVE_CTC_CARD = "saveCTCcard";
	public static final String CTC_CARD = "ctcCardNo";

	@Override
	public CTCTCCardPaymentInfoModel savePaymentInfo(final CustomerModel customerModel, final Map<String, String> resultMap,
			final AbstractOrderModel abstractOrderModel)
	{
		CTCTCCardPaymentInfoModel ctcCardPaymentInfo = null;
		if (Objects.nonNull(resultMap.get(SAVED_CTC_CARD_CODE)))
		{
			final CTCTCCardPaymentInfoModel paymentInfo = getCustomerAccountService()
					.getCtcTcCardPaymentInfoForCode(customerModel, resultMap.get(SAVED_CTC_CARD_CODE));
			ctcCardPaymentInfo = clonePaymentInfoForOrder(paymentInfo, abstractOrderModel);
		}
		else
		{
			ctcCardPaymentInfo = createCTCCardPaymentInfo(resultMap, customerModel);
		}
		return ctcCardPaymentInfo;
	}

	protected CTCTCCardPaymentInfoModel createCTCCardPaymentInfo(final Map<String, String> resultMap,
			final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		final CTCTCCardPaymentInfoModel ctcCardPaymentInfoModel = getModelService().create(CTCTCCardPaymentInfoModel.class);
		ctcCardPaymentInfoModel.setUser(customerModel);
		ctcCardPaymentInfoModel.setCode(resultMap.get(CTC_CARD));
		if (userService.isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = sessionService
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			ctcCardPaymentInfoModel.setB2bUnit(b2bCheckoutContextData.getB2bUnit());
		}
		ctcCardPaymentInfoModel.setCardNumber(resultMap.get(CTC_CARD));
		ctcCardPaymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
		if (Boolean.TRUE.equals(Boolean.valueOf(resultMap.get(SAVE_CTC_CARD))))
		{
			ctcCardPaymentInfoModel.setSaved(true);
		}
		return ctcCardPaymentInfoModel;
	}

	private CTCTCCardPaymentInfoModel clonePaymentInfoForOrder(final CTCTCCardPaymentInfoModel paymentInfo,
			final AbstractOrderModel order)
	{
		validateParameterNotNullStandardMessage("order", order);
		validateParameterNotNullStandardMessage("paymentInfo", paymentInfo);
		final CTCTCCardPaymentInfoModel newPaymentInfo = getModelService().clone(paymentInfo, this.paymentInfoCloningContext);
		newPaymentInfo.setOwner(order);
		newPaymentInfo.setDuplicate(Boolean.TRUE);
		newPaymentInfo.setOriginal(paymentInfo);
		return newPaymentInfo;
	}

	protected BcfUserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final BcfUserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public ModelCloningContext getPaymentInfoCloningContext()
	{
		return paymentInfoCloningContext;
	}

	public void setPaymentInfoCloningContext(final ModelCloningContext paymentInfoCloningContext)
	{
		this.paymentInfoCloningContext = paymentInfoCloningContext;
	}

	public BcfCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	public void setCustomerAccountService(final BcfCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}
}
