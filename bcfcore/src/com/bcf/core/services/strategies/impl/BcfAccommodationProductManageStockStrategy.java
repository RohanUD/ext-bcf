/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.strategies.stock.accommodation.impl.DefaultAccommodationProductManageStockStrategy;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Collections;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class BcfAccommodationProductManageStockStrategy extends DefaultAccommodationProductManageStockStrategy
{

	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	@Override
	public void release(AbstractOrderEntryModel abstractOrderEntry) {
		int qty = abstractOrderEntry.getQuantity().intValue();
		if (qty > 0) {
			AccommodationOrderEntryGroupModel accommodationEntryGroup = bcfTravelCommerceCartService.getAccommodationOrderEntryGroup(abstractOrderEntry);
			Date date = accommodationEntryGroup.getStartingDate();

			for(Date endingDate = accommodationEntryGroup.getEndingDate(); !TravelDateUtils.isSameDate(date, endingDate); date = DateUtils
					.addDays(date, 1)) {
				this.getCommerceStockService().releasePerDateProduct(abstractOrderEntry.getProduct(), date, abstractOrderEntry.getQuantity().intValue(), Collections
						.singletonList(accommodationEntryGroup.getAccommodationOffering()));
			}
		}

	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}
}
