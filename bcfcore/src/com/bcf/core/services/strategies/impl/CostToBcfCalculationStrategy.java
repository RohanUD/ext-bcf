/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.accommodation.costtobcf.strategies.FindCostToBcfStrategy;


public class CostToBcfCalculationStrategy implements FindCostToBcfStrategy
{
	private CommonI18NService commonI18NService;

	@Override
	public PriceValue findCostToBcfValues(final AbstractOrderEntryModel entry)
	{
		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		return new PriceValue(currencyIsoCode, entry.getBasePrice(), true);
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
