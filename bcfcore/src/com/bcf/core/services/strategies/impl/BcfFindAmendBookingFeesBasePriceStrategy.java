/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.util.PriceValue;
import java.util.Objects;
import com.bcf.core.services.strategies.BcfFindBasePriceByProductTypeStrategy;


public class BcfFindAmendBookingFeesBasePriceStrategy implements BcfFindBasePriceByProductTypeStrategy
{

	@Override
	public PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (Objects.isNull(entry.getBasePrice()))
		{
			throw new CalculationException("Fee should have base price.");
		}
		return new PriceValue(entry.getOrder().getCurrency().getIsocode(), entry.getBasePrice(), true);
	}

	protected PriceValue getTemporaryZeroPriceForEntry(final AbstractOrderEntryModel entry)
	{
		return new PriceValue(entry.getOrder().getCurrency().getIsocode(), 0.0d, true);
	}

}

