/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.util.PriceValue;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.strategies.BcfFindBasePriceByProductTypeStrategy;


public class BcfFindDefaultBasePriceStrategy implements BcfFindBasePriceByProductTypeStrategy
{
	private FindPriceStrategy findPriceStrategy;

	@Override
	public PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return getFindPriceStrategy().findBasePrice(entry);
	}

	protected FindPriceStrategy getFindPriceStrategy()
	{
		return findPriceStrategy;
	}

	@Required
	public void setFindPriceStrategy(final FindPriceStrategy findPriceStrategy)
	{
		this.findPriceStrategy = findPriceStrategy;
	}
}
