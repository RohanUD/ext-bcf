/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.services.TravelCategoryService;
import java.util.List;


public interface BCFTravelCategoryService extends TravelCategoryService
{
	/**
	 * Retrieves a list of ancillary categories for route
	 *
	 * @param route route for which products need to fetch
	 * @return list of relevant ancillary categories
	 */
	List<CategoryModel> getAncillaryCategoriesForProducts(final List<ProductModel> products);

	/**
	 * retrives a list of assistance products for the provided vehicle and valid stocklevel
	 *
	 * @param vehicleCodes vehicle code for which products needed
	 * @return list id type {@link ProductModel}
	 */
	List<ProductModel> getAssistanceProductsForVehicle(List<String> vehicleCodes);

	/**
	 * retrives a list of assistance products for the provided category
	 *
	 * @param productCode products for which category needed
	 * @return list id type {@link ProductModel}
	 */
	List<ProductModel> getProductModelForProductCodesAndCategory(String categoryCode, List<ProductModel> productCodes);

	/**
	 * gets all the products that are either mapped to provided list of vehicles or provided list of sectors
	 *
	 * @param vehicleCodes list of vehicle codes
	 * @param sectorCodes  list of sector codes
	 * @return list of products
	 */
	List<ProductModel> getAllProductsForVehicles(final List<String> vehicleCodes);
}
