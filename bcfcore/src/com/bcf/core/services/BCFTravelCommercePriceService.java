/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.price.TravelCommercePriceService;
import de.hybris.platform.util.PriceValue;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public interface BCFTravelCommercePriceService extends TravelCommercePriceService
{
   /**
    * gets the valid price row for the provided ancillary product with the matching criteria of the provided inputs
    *
    * @param offerGroupCode
    */
	PriceInformation getPriceInfoForAncillary(int originDestinationRefNumber, String productCode,
			String transportOfferingCode, String routeCode, String offerGroupCode);

   /**
    * gets the product price rows for the provided product and search criteria
    *
    * @param productCode
    * @param searchCriteria
    * @return
    */
   PriceInformation getPriceInformation(String productCode, Map<String, String> searchCriteria);

   /**
    * @param transportOfferingCode
    * @param sectorCode
    * @param productCode
    * @param originDestinationRefNumber
    * @return
    */
   PriceInformation getPriceInformationFromTransportOfferingOrSector(String transportOfferingCode, String sectorCode,
         String productCode, int originDestinationRefNumber);

   /**
    * adds the start date and end date from the transport offering in the search criteria, <br>
    * this will be mainly used to get filtered price rows so that the valid price row for the travel journey can be
    * fetched
    *
    * @param transportOfferings
    * @param searchCriteria
    * @param transportOfferingCode
    */
   void updateSearchCriteria(List<TransportOfferingModel> transportOfferings, Map<String, String> searchCriteria,
         String transportOfferingCode);

   /**
    * Apply margin.
    *
    * @param basePrice   the base price
    * @param travelRoute the travel route
    * @return the price value
    */
   PriceValue applyMargin(PriceValue basePrice, TravelRouteModel travelRoute);

   /**
    * Gets the margin rate.
    *
    * @param transportFacility the transport facility
    * @return the margin rate
    */
   Double getMarginRate(TransportFacilityModel transportFacility);

   /**
    * Find cost to BCF.
    *
    * @param product   the product
    * @param basePrice the base price
    * @param location  the location
    * @return the price value
    */
   PriceValue findCostToBCF(ProductModel product, PriceValue basePrice, LocationModel location);

   /**
    * get BcfPriceInfo for room rate product
    *
    * @param roomRateProduct
    * @param quantity
    * @param ratePlan
    * @param location
    * @return
    */
   BcfPriceInfo getBcfPriceInfo(RoomRateProductModel roomRateProduct, long quantity, RatePlanModel ratePlan,
         LocationModel location);

   /**
    * get BcfPriceInfo for room rate product and includes discounts from order entry
    *
    *
    * @param entry
    * @param roomRateProduct
    * @param quantity
    * @param ratePlan
    * @param location
    * @return
    */
   BcfPriceInfo getBcfPriceInfo(final AbstractOrderEntryModel entry,
         final RoomRateProductModel roomRateProduct, final long quantity,
         final RatePlanModel ratePlan, final LocationModel location);
   /**
    * get BcfPriceInfo for extra guest occupancy product
    *
    * @param extraGuestOccupancyProduct
    * @param passengerType
    * @param ratePlan
    * @param location
    * @return
    */
	BcfPriceInfo getBcfPriceInfo(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, long quantity,
			final String passengerType, final RatePlanModel ratePlan, final LocationModel location, Date stayDate);

   /**
    * get BcfPriceInfo for extra product
    *
    * @param extraProductModel
    * @param ratePlan
    * @param location
    * @return
    */
   BcfPriceInfo getBcfPriceInfo(ExtraProductModel extraProductModel, long quantity, RatePlanModel ratePlan,
         LocationModel location);

   /**
    * get BcfPriceInfo for activity product
    *
    * @param activityProduct
    * @param passengerType
    * @param commencementDate
    * @return
    */
   BcfPriceInfo getBcfPriceInfo(ActivityProductModel activityProduct, long quantity, PassengerTypeModel passengerType,
         Date commencementDate);

   /**
    * Get the Bcf Price Info.
    *
    * @param extraProductModel
    * @param quantity
    * @param location
    * @return
    */
   BcfPriceInfo getBcfPriceInfo(ExtraProductModel extraProductModel, long quantity,
         LocationModel location);

   BcfPriceInfo getBcfPriceInfo(final AbstractOrderEntryModel entry,
			final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, final long quantity,
			final String passengerType, final RatePlanModel ratePlan, final LocationModel location, final Date stayDate);

}
