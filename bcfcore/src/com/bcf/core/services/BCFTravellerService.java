/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.services.TravellerService;
import java.util.List;
import java.util.Map;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


public interface BCFTravellerService extends TravellerService
{
	/**
	 * updates the TravellerModel with BCFVehicleInformationModel plus some other details
	 */
	TravellerModel updateAndGetVehicleTraveller(TravellerData travellerData, TravellerModel travellerModel);

	TravellerModel createVehicleInformation(String travellerType, VehicleTypeQuantityData vehicleData, String travellerCode,
			int number, String travellerUidPrefix, String orderOrCartCode);

	TravellerModel createVehicleInformation(String travellerType, VehicleInformationData vehicleData, String travellerCode,
			int number, String travellerUidPrefix, String orderOrCartCode);

	/**
	 * fetches travelers per journey and then group it by origin destination reference number
	 */
	Map<Integer, List<TravellerModel>> getTravellersPerJourneyPerLeg(AbstractOrderModel abstractOrderModel, int jouneyRefNumber);

	TravellerModel createTravellerWithSpecialServiceRequest(String travellerType, String type, String travellerCode, int number,
			String travellerUidPrefix, String cartCode, List<SpecialServicesData> specialServicesDataList);

	void updateTravellerWithSpecialServiceRequest(TravellerModel traveller,
			final List<SpecialServicesData> specialServicesDataList);

}
