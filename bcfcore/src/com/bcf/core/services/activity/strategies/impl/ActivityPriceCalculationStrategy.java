/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.activity.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.Date;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.GuestType;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.services.price.strategies.impl.AbstractPriceCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.BcfPriceValue;
import com.bcf.core.util.PrecisionUtil;


public class ActivityPriceCalculationStrategy extends AbstractPriceCalculationStrategy implements FindBcfPriceInfoStrategy
{
	/**
	 * Gets the base price.
	 *
	 * @param activityProduct  the activity product
	 * @param passengerType    the passenger type
	 * @param commencementDate the commencement date
	 * @return the base price
	 */
	public BcfPriceValue getBasePrice(final ActivityProductModel activityProduct, final PassengerTypeModel passengerType,
			final Date commencementDate)
	{
		final String passengerTypeCode = Objects.nonNull(passengerType) ?
				passengerType.getCode() :
				BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT;
		final PriceRowModel priceRow = getPriceRowService().getPriceRow(activityProduct, passengerTypeCode, commencementDate);
		if (Objects.isNull(priceRow) || Objects.isNull(priceRow.getPrice()))
		{
			return null;
		}

		final double basePrice = PrecisionUtil.round(priceRow.getPrice());
		final double marginRate = Objects.isNull(priceRow.getActivityMarginRate()) ? 0d : priceRow.getActivityMarginRate();
		return new BcfPriceValue(priceRow.getCurrency().getIsocode(), basePrice, marginRate, true);
	}

	@Override
	public BcfPriceInfo findBcfPriceInfo(final AbstractOrderEntryModel entry)
	{
		final ActivityProductModel activityProduct = (ActivityProductModel) entry.getProduct();
		final ActivityOrderEntryInfoModel activityOrderEntryInfo = entry.getActivityOrderEntryInfo();
		final GuestType guestType = activityOrderEntryInfo.getGuestType();
		final String guestTypeCode = Objects.nonNull(guestType) ?
				guestType.getCode().toLowerCase() :
				BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT;
		final Date commencementDate = activityOrderEntryInfo.getActivityDate();
		final SpecializedPassengerTypeModel specializedPassengerTypeModel = activityProduct.getSpecializedPassengerTypes().stream()
				.filter(
						specializedPassengerType -> StringUtils
								.equalsIgnoreCase(guestTypeCode, specializedPassengerType.getBasePassengerType().getCode())).findAny()
				.orElse(null);
		final PassengerTypeModel passengerType;
		if (Objects.nonNull(specializedPassengerTypeModel))
		{
			passengerType = specializedPassengerTypeModel.getBasePassengerType();
		}
		else
		{
			passengerType = getPassengerTypeService().getPassengerType(guestTypeCode);
		}

		return getBcfTravelCommercePriceService()
				.getBcfPriceInfo(activityProduct, entry.getQuantity(), passengerType, commencementDate);
	}
}
