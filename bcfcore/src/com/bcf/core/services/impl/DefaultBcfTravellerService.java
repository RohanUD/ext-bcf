/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialRequestDetailModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.services.SpecialServiceRequestService;
import de.hybris.platform.travelservices.services.impl.DefaultTravellerService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.vehicletype.VehicleTypeService;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


public class DefaultBcfTravellerService extends DefaultTravellerService implements BCFTravellerService
{
	private VehicleTypeService vehicleTypeService;
	private Converter<VehicleInformationData, BCFVehicleInformationModel> vehicleInformationReverseConverter;
	private I18NService i18nService;
	private SpecialServiceRequestService specialServiceRequestService;

	@Override
	public TravellerModel createTraveller(final String travellerType, final String type, final String travellerCode,
			final int number, final String travellerUidPrefix, final String orderOrCartCode)
	{
		final TravellerModel traveller = getModelService().create(TravellerModel._TYPECODE);
		setTravellerDetails(traveller, travellerType, type, travellerCode, orderOrCartCode);

		saveTraveller(traveller, number, travellerUidPrefix, 0);
		return traveller;
	}

	@Override
	public TravellerModel createTravellerWithSpecialServiceRequest(final String travellerType, final String type,
			final String travellerCode,
			final int number, final String travellerUidPrefix, final String cartCode,
			final List<SpecialServicesData> specialServicesDataList)
	{
		final TravellerModel traveller = getModelService().create(TravellerModel._TYPECODE);
		final SpecialRequestDetailModel specialRequestDetailModel = getModelService().create(SpecialRequestDetailModel._TYPECODE);
		final List<SpecialServiceRequestModel> specialServiceRequestModels = new ArrayList<>();

		for (final SpecialServicesData specialService : specialServicesDataList)
		{
			specialServiceRequestModels.add(getSpecialServiceRequestService().getSpecialServiceRequest(specialService.getCode()));
			if (StringUtils.isNotEmpty(specialService.getDescription()))
			{
				specialRequestDetailModel.setDescription(specialService.getDescription());
			}
		}
		if (CollectionUtils.isNotEmpty(specialServiceRequestModels))
		{
			specialRequestDetailModel.setSpecialServiceRequest(specialServiceRequestModels);
			getModelService().save(specialRequestDetailModel);
			traveller.setSpecialRequestDetail(specialRequestDetailModel);
		}
		setTravellerDetails(traveller, travellerType, type, travellerCode, cartCode);
		saveTraveller(traveller, number, travellerUidPrefix, 0);
		return traveller;
	}

	@Override
	public void updateTravellerWithSpecialServiceRequest(final TravellerModel traveller,
			final List<SpecialServicesData> specialServicesDataList)
	{

		final SpecialRequestDetailModel specialRequestDetailModel = getModelService().create(SpecialRequestDetailModel._TYPECODE);
		final List<SpecialServiceRequestModel> specialServiceRequestModels = new ArrayList<>();

		for (final SpecialServicesData specialService : specialServicesDataList)
		{
			specialServiceRequestModels.add(getSpecialServiceRequestService().getSpecialServiceRequest(specialService.getCode()));
			if (StringUtils.isNotEmpty(specialService.getDescription()))
			{
				specialRequestDetailModel.setDescription(specialService.getDescription());
			}
		}
		if (CollectionUtils.isNotEmpty(specialServiceRequestModels))
		{
			specialRequestDetailModel.setSpecialServiceRequest(specialServiceRequestModels);
			traveller.setSpecialRequestDetail(specialRequestDetailModel);
			getModelService().saveAll(specialRequestDetailModel, traveller);

		}
	}

	protected void setTravellerDetails(final TravellerModel traveller, final String travellerType, final String type,
			final String travellerCode, final String cartCode)
	{
		traveller.setLabel(travellerCode);
		traveller.setType(getEnumerationService().getEnumerationValue(TravellerType._TYPECODE, travellerType));
		traveller.setVersionID(cartCode);

		if (TravelservicesConstants.TRAVELLER_TYPE_PASSENGER.equalsIgnoreCase(travellerType))
		{
			final PassengerInformationModel passengerInfo = getModelService().create(PassengerInformationModel._TYPECODE);
			passengerInfo.setPassengerType(getPassengerTypeService().getPassengerType(type));
			traveller.setInfo(passengerInfo);
		}

	}

	@Override
	public TravellerModel createVehicleInformation(final String travellerType, final VehicleTypeQuantityData vehicleData,
			final String travellerCode,
			final int number, final String travellerUidPrefix, final String orderOrCartCode)
	{
		final TravellerModel traveller = getModelService().create(TravellerModel._TYPECODE);
		traveller.setLabel(travellerCode);
		traveller.setType(getEnumerationService().getEnumerationValue(TravellerType._TYPECODE, travellerType));
		traveller.setVersionID(orderOrCartCode);

		if (BcfCoreConstants.TRAVELLER_TYPE_VEHICLE.equalsIgnoreCase(travellerType))
		{
			final BCFVehicleInformationModel vehicleInfo = getModelService().create(BCFVehicleInformationModel._TYPECODE);
			vehicleInfo.setVehicleType(getVehicleTypeService().getVehicleForCode(vehicleData.getVehicleType().getCode()));
			vehicleInfo.setLength(vehicleData.getLength());
			vehicleInfo.setWidth(vehicleData.getWidth());
			vehicleInfo.setHeight(vehicleData.getHeight());
			vehicleInfo.setWeight(vehicleData.getWeight());
			vehicleInfo.setGroundClearance(vehicleData.getGroundClearance());
			vehicleInfo.setAdjustable(BooleanUtils.toBoolean(vehicleData.getAdjustable()));
			vehicleInfo.setNumberOfAxis(vehicleData.getNumberOfAxis());
			vehicleInfo.setCarryingLivestock(vehicleData.isCarryingLivestock());
			vehicleInfo.setVehicleWithSidecarOrTrailer(vehicleData.isVehicleWithSidecarOrTrailer());
			traveller.setInfo(vehicleInfo);
		}

		saveTraveller(traveller, number, travellerUidPrefix, 0);
		return traveller;
	}

	@Override
	public TravellerModel createVehicleInformation(final String travellerType, final VehicleInformationData vehicleData,
			final String travellerCode, final int number, final String travellerUidPrefix, final String orderOrCartCode)
	{
		final TravellerModel traveller = getModelService().create(TravellerModel._TYPECODE);
		traveller.setLabel(travellerCode);
		traveller.setType(getEnumerationService().getEnumerationValue(TravellerType._TYPECODE, travellerType));
		traveller.setVersionID(orderOrCartCode);

		if (BcfCoreConstants.TRAVELLER_TYPE_VEHICLE.equalsIgnoreCase(travellerType))
		{
			final BCFVehicleInformationModel vehicleInfo = getModelService().create(BCFVehicleInformationModel._TYPECODE);
			vehicleInfo.setVehicleType(getVehicleTypeService().getVehicleForCode(vehicleData.getVehicleType().getCode()));
			vehicleInfo.setLength(vehicleData.getLength());
			vehicleInfo.setWidth(vehicleData.getWidth());
			vehicleInfo.setHeight(vehicleData.getHeight());
			vehicleInfo.setWeight(vehicleData.getWeight());
			vehicleInfo.setGroundClearance(vehicleData.getGroundClearance());
			vehicleInfo.setAdjustable(vehicleData.isAdjustable());
			vehicleInfo.setNumberOfAxis(vehicleData.getNumberOfAxis());
			vehicleInfo.setCarryingLivestock(vehicleData.isCarryingLivestock());
			vehicleInfo.setVehicleWithSidecarOrTrailer(vehicleData.isVehicleWithSidecarOrTrailer());
			traveller.setInfo(vehicleInfo);
		}

		saveTraveller(traveller, number, travellerUidPrefix, 0);
		return traveller;
	}


	@Override
	public TravellerModel updateAndGetVehicleTraveller(final TravellerData travellerData, final TravellerModel travellerModel)
	{
		final BCFVehicleInformationModel vehicleInformationModel = Objects.nonNull(travellerModel.getInfo())
				? (BCFVehicleInformationModel) travellerModel.getInfo() : new BCFVehicleInformationModel();

		final VehicleInformationData vehicleInformationData = (VehicleInformationData) travellerData.getTravellerInfo();
		getVehicleInformationReverseConverter().convert(vehicleInformationData, vehicleInformationModel);
		travellerModel.setInfo(vehicleInformationModel);
		travellerModel.setSavedTravellerUid(travellerData.getSavedTravellerUid());
		travellerModel.setBooker(travellerData.isBooker());
		travellerModel.setLabel(travellerData.getLabel());

		getModelService().saveAll(vehicleInformationModel, travellerModel);

		return travellerModel;
	}

	@Override
	public Map<Integer, List<TravellerModel>> getTravellersPerJourneyPerLeg(final AbstractOrderModel abstractOrderModel,
			final int jouneyRefNumber)
	{
		final Map<Integer, List<TravellerModel>> travellersMap = new HashMap<>();
		final List<AbstractOrderEntryModel> activeEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getJourneyReferenceNumber() == jouneyRefNumber)
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && entry.getActive()).collect(Collectors.toList());
		activeEntries.forEach(entry -> {
			if (MapUtils.isEmpty(travellersMap)
					|| !travellersMap.containsKey(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
			{
				final List<TravellerModel> travellers = new ArrayList<>();
				travellers.addAll(entry.getTravelOrderEntryInfo().getTravellers());
				travellersMap.put(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber(), travellers);
			}
			else
			{
				if (!travellersMap.get(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
						.containsAll(entry.getTravelOrderEntryInfo().getTravellers()))
				{
					final List<TravellerModel> travellers = getTravellerListForEntry(travellersMap, entry);
					travellersMap.put(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber(), travellers);
				}
			}
		});
		return travellersMap;
	}

	protected VehicleTypeService getVehicleTypeService()
	{
		return vehicleTypeService;
	}

	@Required
	public void setVehicleTypeService(final VehicleTypeService vehicleTypeService)
	{
		this.vehicleTypeService = vehicleTypeService;
	}

	protected Converter<VehicleInformationData, BCFVehicleInformationModel> getVehicleInformationReverseConverter()
	{
		return vehicleInformationReverseConverter;
	}

	@Required
	public void setVehicleInformationReverseConverter(
			final Converter<VehicleInformationData, BCFVehicleInformationModel> vehicleInformationReverseConverter)
	{
		this.vehicleInformationReverseConverter = vehicleInformationReverseConverter;
	}

	protected I18NService getI18nService()
	{
		return i18nService;
	}

	@Required
	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

	protected SpecialServiceRequestService getSpecialServiceRequestService()
	{
		return specialServiceRequestService;
	}

	@Required
	public void setSpecialServiceRequestService(
			final SpecialServiceRequestService specialServiceRequestService)
	{
		this.specialServiceRequestService = specialServiceRequestService;
	}

}
