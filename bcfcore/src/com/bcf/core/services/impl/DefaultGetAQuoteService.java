/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.ticket.enums.CsInterventionType;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketBusinessService;
import de.hybris.platform.ticket.service.TicketService;
import de.hybris.platform.ticketsystem.data.CsTicketParameter;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.GetAQuoteDao;
import com.bcf.core.services.GetAQuoteService;
import com.bcf.facades.getaquote.data.CsTicketListAndCountData;


public class DefaultGetAQuoteService implements GetAQuoteService
{
	private ModelService modelService;
	private TicketBusinessService ticketBusinessService;
	private TicketService ticketService;
	private GetAQuoteDao getAQuoteDao;
	private AssistedServiceService assistedServiceService;

	@Override
	public void createTicketForQuote(final StringBuilder travellerInformation, final String firstName,
			final String lastName, final String email, final String phoneNumber, final String title)
	{
		final CsTicketModel ticket = ticketBusinessService
				.createTicket(createCsTicketParameter(travellerInformation.toString(), title));
		final CsCustomerEventModel csCustomerEvent = (CsCustomerEventModel) (ticketService.getEventsForTicket(ticket).get(0));
		csCustomerEvent.setContactNumber(phoneNumber);
		csCustomerEvent.setName(firstName + " " + lastName);
		csCustomerEvent.setEmail(email);
		modelService.save(csCustomerEvent);
	}

	private CsTicketParameter createCsTicketParameter(final String notes, final String title)
	{
		final CsTicketParameter ticketParameter = new CsTicketParameter();
		ticketParameter.setPriority(CsTicketPriority.HIGH);
		ticketParameter.setCategory(CsTicketCategory.CREATEQUOTE);
		ticketParameter.setInterventionType(CsInterventionType.TICKETMESSAGE);
		ticketParameter.setCreationNotes(notes);
		ticketParameter.setHeadline(title);
		return ticketParameter;
	}

	@Override
	public void changeTicketStatusForQuote(final String status, final String ticketId)
	{
		final CsTicketModel ticket = ticketService.getTicketForTicketId(ticketId);
		ticket.setAssignedAgent((EmployeeModel) assistedServiceService.getAsmSession().getAgent());
		ticket.setState(CsTicketState.valueOf(status));

		modelService.save(ticket);
	}

	@Override
	public CsTicketListAndCountData getOpenTickets(final int startIndex, final int pageNumber,
			final int recordsPerPage, final List<String> status, final String category)
	{
		return getGetAQuoteDao().getOpenTickets(startIndex, pageNumber, recordsPerPage, status, category);
	}

	public GetAQuoteDao getGetAQuoteDao()
	{
		return getAQuoteDao;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setTicketBusinessService(final TicketBusinessService ticketBusinessService)
	{
		this.ticketBusinessService = ticketBusinessService;
	}

	@Required
	public void setTicketService(final TicketService ticketService)
	{
		this.ticketService = ticketService;
	}

	@Required
	public void setGetAQuoteDao(final GetAQuoteDao getAQuoteDao)
	{
		this.getAQuoteDao = getAQuoteDao;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}
}
