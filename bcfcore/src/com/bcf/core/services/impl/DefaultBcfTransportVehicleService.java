/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.transportvehicle.dao.BcfTransportVehicleDao;


public class DefaultBcfTransportVehicleService implements BcfTransportVehicleService
{
	private BcfTransportVehicleDao bcfTransportVehicleDao;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public TransportVehicleModel getTransportVehicle(final String code)
	{
		return getBcfTransportVehicleDao().findTransportVehicle(code);
	}

	@Override
	public TransportVehicleModel getDefaultTransportVehicle()
	{
		return getTransportVehicle(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.DEFAULT_TRANSPORT_VEHICLE_PROVIDER));
	}

	public BcfTransportVehicleDao getBcfTransportVehicleDao()
	{
		return bcfTransportVehicleDao;
	}

	@Required
	public void setBcfTransportVehicleDao(final BcfTransportVehicleDao bcfTransportVehicleDao)
	{
		this.bcfTransportVehicleDao = bcfTransportVehicleDao;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
