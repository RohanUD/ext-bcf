/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.services.accommodation.costtobcf.strategies.FindCostToBcfStrategy;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.strategies.BcfFindBasePriceByProductTypeStrategy;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;


public class DefaultBcfCalculationService extends DefaultCalculationService implements BcfCalculationService
{
	private static final String DEFAULT = "DEFAULT";
	public static final int CENTS_TO_DOLLAR_CONVERSION = 100;

	private Map<String, BcfFindBasePriceByProductTypeStrategy> findBasePriceByProductTypeStrategyMap;
	private Map<String, FindCostToBcfStrategy> findCostToBcfStrategyMap;
	private Map<String, FindBcfPriceInfoStrategy> findBcfPriceInfoStrategyMap;

	private OrderRequiresCalculationStrategy bcfOrderRequiresCalculationStrategy;
	private BcfBookingService bcfBookingService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;


	@Override
	public void calculate(final AbstractOrderModel order) throws CalculationException
	{
		if (getBcfOrderRequiresCalculationStrategy().requiresCalculation(order))
		{

			calculateEntries(order, false);
			calculateTotals(order);
			if (BookingJourneyType.BOOKING_PACKAGE.equals(order.getBookingJourneyType()))
			{
				calculateIndividualEntryPackagePrice(order);
			}

		}
	}

	@Override
	protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		final PriceValue basePriceValue = findBasePrice(entry);
		entry.setBasePrice(basePriceValue.getValue());
		final PriceValue costToBcf = findCostToBcf(entry);
		entry.setCostToBCF(costToBcf.getValue());
	}

	@Override
	protected PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		BcfFindBasePriceByProductTypeStrategy bcfFindBasePriceByProductTypeStrategy = getFindBasePriceByProductTypeStrategyMap()
				.get(entry.getProduct().getItemtype());
		if (Objects.isNull(bcfFindBasePriceByProductTypeStrategy))
		{
			bcfFindBasePriceByProductTypeStrategy = getFindBasePriceByProductTypeStrategyMap().get(DEFAULT);
		}

		return bcfFindBasePriceByProductTypeStrategy.findBasePrice(entry);
	}

	/**
	 * Find cost to bcf.
	 *
	 * @param entry the entry
	 * @return the price value
	 */
	protected PriceValue findCostToBcf(final AbstractOrderEntryModel entry)
	{
		FindCostToBcfStrategy findCostToBcfStrategy = getFindCostToBcfStrategyMap().get(entry.getProduct().getItemtype());
		if (Objects.isNull(findCostToBcfStrategy))
		{
			findCostToBcfStrategy = getFindCostToBcfStrategyMap().get(DEFAULT);
		}

		return findCostToBcfStrategy.findCostToBcfValues(entry);
	}

	@Override
	protected List<DiscountValue> findGlobalDiscounts(final AbstractOrderModel order)
	{
		return Collections.emptyList();
	}

	@Override
	public void recalculate(final AbstractOrderModel order) throws CalculationException
	{

		calculateEntries(order, true);
		calculateTotals(order);
	}

	@Override
	public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
	{
		final List<AbstractOrderEntryModel> activeOrderEntries = order.getEntries().stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive()) && !AmendStatus.SAME.equals(entry.getAmendStatus()))
				.collect(Collectors.toList());
		final List<AbstractOrderEntryModel> activeOrderEntriesRequireRecalculation = activeOrderEntries.stream()
				.filter(entry -> (forceRecalculate || getBcfOrderRequiresCalculationStrategy().requiresCalculation(entry)))
				.collect(Collectors.toList());

		for (final AbstractOrderEntryModel entry : activeOrderEntriesRequireRecalculation)
		{
			calculateEntry(entry);
		}
	}

	protected void calculateEntry(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (Objects.equals(OrderEntryType.TRANSPORT, entry.getType()))
		{
			resetAllValues(entry);
			calculateTransportEntryTotal(entry);
		}
		else
		{
			FindBcfPriceInfoStrategy findBcfPriceInfoStrategy = getFindBcfPriceInfoStrategyMap()
					.get(entry.getProduct().getItemtype());
			if (Objects.isNull(findBcfPriceInfoStrategy))
			{
				findBcfPriceInfoStrategy = getFindBcfPriceInfoStrategyMap().get(DEFAULT);
			}
			final BcfPriceInfo bcfPriceInfo = findBcfPriceInfoStrategy.findBcfPriceInfo(entry);
			entry.setContractPrice(bcfPriceInfo.getBasePrice());
			entry.setCostToBCF(bcfPriceInfo.getCostToBcf());
			entry.setBasePrice(bcfPriceInfo.getBasePrice());
			entry.setMargin(bcfPriceInfo.getMarginValue());

			if (CollectionUtils.isNotEmpty(bcfPriceInfo.getDiscountValues()))
			{
				entry.setDiscountValues(
						bcfPriceInfo.getDiscountValues().stream().filter(Objects::nonNull).collect(Collectors.toList()));
			}
			final List<TaxValue> entryTaxes = getEntryTaxes(bcfPriceInfo, entry.getQuantity());
			entry.setTaxValues(entryTaxes);
			entry.setTotalPrice(bcfPriceInfo.getTotalPrice());
		}
		setCalculatedStatus(entry);
	}

	@Override
	public void calculateFeeEntries(final List<AbstractOrderEntryModel> feeEntries) throws CalculationException
	{
		for (final AbstractOrderEntryModel entry : feeEntries)
		{

			calculateEntry(entry);
		}
	}

	protected List<TaxValue> getEntryTaxes(final BcfPriceInfo bcfPriceInfo, final long quantity)
	{
		final List<TaxValue> entryTaxes = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(bcfPriceInfo.getHiddenTaxValues()))
		{
			entryTaxes.addAll(bcfPriceInfo.getHiddenTaxValues());
		}
		if (Objects.nonNull(bcfPriceInfo.getGstTaxValue()))
		{
			final TaxValue originalGSTTaxValue = bcfPriceInfo.getGstTaxValue();

			final BcfPriceInfo gstBcfPriceInfo = new BcfPriceInfo(bcfPriceInfo.getCurrencyIso(), bcfPriceInfo.getBasePrice(),
					bcfPriceInfo.getMarginValue(),
					bcfPriceInfo.getDiscountValues(), bcfPriceInfo.getCostToBcf(), bcfPriceInfo.getHiddenTaxValues(),
					bcfPriceInfo.getNetPriceVal(), bcfPriceInfo.getGstTaxValue(), bcfPriceInfo.getTotalPrice(),
					bcfPriceInfo.getQuantity());


			final TaxValue modifiedQithQuantityGSTTaxValue = new TaxValue(originalGSTTaxValue.getCode(),
					originalGSTTaxValue.getValue() * bcfPriceInfo.getQuantity(), true, originalGSTTaxValue.getCurrencyIsoCode());

			entryTaxes.add(modifiedQithQuantityGSTTaxValue);
		}
		return entryTaxes;
	}

	/**
	 * Calculate transport entry total.
	 *
	 * @param entry the entry
	 */
	protected void calculateTransportEntryTotal(final AbstractOrderEntryModel entry)
	{
		final AbstractOrderModel order = entry.getOrder();
		final CurrencyModel curr = order.getCurrency();
		final int digits = curr.getDigits().intValue();
		final double quantity = entry.getQuantity().doubleValue();
		final double totalPriceWithoutDiscount = PrecisionUtil.round(entry.getBasePrice() * quantity);

		final List appliedDiscounts = DiscountValue.apply(quantity, totalPriceWithoutDiscount, digits,
				convertDiscountValues(order, entry.getDiscountValues()), curr.getIsocode());
		entry.setDiscountValues(appliedDiscounts);

		double discountAppliedVal = 0;
		if (CollectionUtils.isNotEmpty(entry.getDiscountValues()))
		{
			discountAppliedVal = entry.getDiscountValues().stream().mapToDouble(DiscountValue::getAppliedValue).sum();
		}

		double totalTaxValue = 0.0;
		if (CollectionUtils.isNotEmpty(entry.getTaxValues()))
		{
			final TaxValue lastGstTaxValue = entry.getTaxValues().stream()
					.filter(taxValue -> StringUtils.equals(BcfCoreConstants.GST, taxValue.getCode())).reduce((a, b) -> b).get();
			totalTaxValue = lastGstTaxValue.getValue();
		}

		final double totalPrice = totalPriceWithoutDiscount - discountAppliedVal + totalTaxValue;
		entry.setTotalPrice(totalPrice);
		calculateTotalTaxValues(entry);
	}

	@Override
	protected Map<TaxValue, Map<Set<TaxValue>, Double>> calculateSubtotal(final AbstractOrderModel order,
			final boolean recalculate)
	{
		if (recalculate || Boolean.FALSE.equals(order.getCalculated()))
		{
			double subtotal = 0.0;
			final List<AbstractOrderEntryModel> entries = order.getEntries().stream()
					.filter(entry -> Objects.nonNull(entry.getActive()) && entry.getActive()).collect(Collectors.toList());
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new LinkedHashMap<>(entries.size() * 2);

			for (final AbstractOrderEntryModel entry : entries)
			{
				calculateTotals(entry, recalculate);
				final double entryTotal = entry.getTotalPrice().doubleValue();
				subtotal += entryTotal;
				// use un-applied version of tax values!!!
				final Collection<TaxValue> allTaxValues = entry.getTaxValues().stream()
						.filter(taxValue -> StringUtils.equals(BcfCoreConstants.GST, taxValue.getCode())).collect(Collectors.toList());
				final Set<TaxValue> relativeTaxGroupKey = getUnappliedRelativeTaxValues(allTaxValues);
				for (final TaxValue taxValue : allTaxValues)
				{
					if (taxValue.isAbsolute())
					{
						addAbsoluteEntryTaxValue(entry.getQuantity().longValue(), taxValue.unapply(), taxValueMap);
					}
					else
					{
						addRelativeEntryTaxValue(entryTotal, taxValue.unapply(), relativeTaxGroupKey, taxValueMap);
					}
				}
			}
			// store subtotal
			subtotal = PrecisionUtil.round(subtotal);
			order.setSubtotal(subtotal);
			return taxValueMap;
		}
		return Collections.emptyMap();
	}

	@Override
	protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException
	{

		super.calculateTotals(order, recalculate, taxValueMap);
		calculateTotals(order);

	}


	/**
	 * Calculate totals.
	 *
	 * @param order the order
	 */
	protected void calculateTotals(final AbstractOrderModel order)
	{
		final double totalRoundedPrice = getTotalPrice(order);
		final List<AbstractOrderEntryModel> activeOrderEntries = order.getEntries().stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive())).collect(Collectors.toList());
		order.setTotalPrice(totalRoundedPrice);

		if (bcfTravelCartService.isAlacateFlow())
		{
			setAmountToPay(order, totalRoundedPrice);
		}
		else if (order.getBookingJourneyType() != null && StringUtils
				.equalsIgnoreCase(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), order.getBookingJourneyType().getCode())
				&& CollectionUtils.isNotEmpty(order.getAmountToPayInfos()))
		{
			setAmountToPayAndPayAtTerminal(order);
		}
		else
		{
			setAmountToPay(order, totalRoundedPrice);
		}

		final double totalTaxes = activeOrderEntries.stream().mapToDouble(entry ->
				entry.getTaxValues().stream().filter(taxValue -> StringUtils.equals(BcfCoreConstants.GST, taxValue.getCode()))
						.mapToDouble(TaxValue::getValue).sum()).sum();
		final double totalRoundedTaxes = PrecisionUtil.round(totalTaxes);
		order.setTotalTax(totalRoundedTaxes);

		applyGuarantee(order);
		setCalculatedStatus(order);
		saveOrder(order);
	}

	@Override
	public double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final List<AbstractOrderEntryModel> activeOrderEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive())).collect(Collectors.toList());

		return getTotalPrice(abstractOrderModel, activeOrderEntries);
	}


	@Override
	public double getTotalPriceWithOutGoodWillRefund(final AbstractOrderModel abstractOrderModel)
	{
		final List<AbstractOrderEntryModel> activeOrderEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive())).collect(Collectors.toList());

		return getTotalPriceWithOutGoodWillRefund(abstractOrderModel, activeOrderEntries);
	}

	private double getTotalPriceWithOutGoodWillRefund(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> activeOrderEntries)
	{
		double totalPrice = activeOrderEntries.stream().mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getGlobalDiscountValues()))
		{
			final Double discountAmount = abstractOrderModel.getGlobalDiscountValues().stream()
					.mapToDouble(discountValue -> discountValue.getAppliedValue()).sum();
			totalPrice = BigDecimal.valueOf(totalPrice).subtract(BigDecimal.valueOf(discountAmount)).doubleValue();
		}
		return PrecisionUtil.round(totalPrice);
	}


	private double getTotalPrice(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> activeOrderEntries)
	{
		double totalPrice = activeOrderEntries.stream().mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();
		final Collection<GoodWillRefundOrderInfoModel> refundInfos = abstractOrderModel.getGoodwillRefundOrderInfos();
		if (CollectionUtils.isNotEmpty(refundInfos))
		{
			final Double refundAmount = refundInfos.stream().mapToDouble(refundInfo -> refundInfo.getAmount()).sum();
			totalPrice = BigDecimal.valueOf(totalPrice).subtract(BigDecimal.valueOf(refundAmount)).doubleValue();
		}
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getGlobalDiscountValues()))
		{
			final Double discountAmount = abstractOrderModel.getGlobalDiscountValues().stream()
					.mapToDouble(discountValue -> discountValue.getAppliedValue()).sum();
			totalPrice = BigDecimal.valueOf(totalPrice).subtract(BigDecimal.valueOf(discountAmount)).doubleValue();
		}
		return PrecisionUtil.round(totalPrice);
	}

	@Override
	public double getTotalPriceWithOutFee(final AbstractOrderModel abstractOrderModel)
	{

		final List<String> changeFeeProducts = BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.CHANGE_AND_CANCEL_FEE_PRODUCTS));


		final List<AbstractOrderEntryModel> activeOrderEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive()) && !changeFeeProducts.contains(entry.getProduct().getCode()))
				.collect(Collectors.toList());

		return getTotalPrice(abstractOrderModel, activeOrderEntries);
	}


	private void setAmountToPay(final AbstractOrderModel order, final double totalRoundedPrice)
	{
		order.setPayAtTerminal(0.0d);
		if (order instanceof CartModel)
		{
			if (order.getOriginalOrder() != null)
			{
				if (!BcfCoreConstants.PACKAGE_ON_REQUEST
						.equals(order.getAvailabilityStatus()) && bcfTravelCartService.isOriginalOrderOnRequest(order))
				{
					order.setAmountToPay(PrecisionUtil.round(BigDecimal.valueOf(totalRoundedPrice).doubleValue()));
					order.setAmountPaid(0d);
				}
				else
				{

					order.setAmountToPay(PrecisionUtil.round(BigDecimal.valueOf(totalRoundedPrice)
							.subtract(BigDecimal.valueOf(order.getOriginalOrder().getAmountPaid())).doubleValue()));
				}

			}
			else
			{
				double actualAmountToPay = totalRoundedPrice;
				if (Objects.nonNull(order.getAmountPaid()))
				{
					actualAmountToPay -= order.getAmountPaid();
				}
				order.setAmountToPay(PrecisionUtil.round(actualAmountToPay));
			}
		}
		else
		{
			order.setAmountToPay(PrecisionUtil.round(BigDecimal.valueOf(totalRoundedPrice)
					.subtract(BigDecimal.valueOf(order.getAmountPaid())).doubleValue()));

		}
	}

	private void setAmountToPayAndPayAtTerminal(final AbstractOrderModel order)
	{
		final double amountToPayNow;
		final double payAtTerminal;
		if (getBcfTravelCartService().isAmendmentCart(order))
		{
			final double currentOrderTotal = order.getTotalPrice();
			final double paidAmount = order.getAmountPaid();
			if (currentOrderTotal > paidAmount)
			{
				final double totalOutstandingAmount = PrecisionUtil.round(currentOrderTotal - paidAmount);
				amountToPayNow = PrecisionUtil.round(order.getAmountToPayInfos().stream().filter(amountToPayInfoModel ->
						StringUtils.isNotBlank(amountToPayInfoModel.getCacheKey())).mapToDouble(AmountToPayInfoModel::getAmountToPay)
						.sum()) / CENTS_TO_DOLLAR_CONVERSION;
				payAtTerminal = PrecisionUtil.round(totalOutstandingAmount - amountToPayNow);
			}
			else if (currentOrderTotal < paidAmount)
			{
				final double totalOverpaidAmount = PrecisionUtil.round(paidAmount - currentOrderTotal);
				amountToPayNow = PrecisionUtil.round(-totalOverpaidAmount);
				payAtTerminal = 0d;
			}
			else
			{
				amountToPayNow = 0d;
				payAtTerminal = 0d;
			}
		}
		else
		{
			amountToPayNow =
					PrecisionUtil.round(order.getAmountToPayInfos().stream().mapToDouble(AmountToPayInfoModel::getAmountToPay).sum())
							/ CENTS_TO_DOLLAR_CONVERSION;
			payAtTerminal = order.getAmountToPayInfos().stream().mapToDouble(AmountToPayInfoModel::getPayAtTerminal).sum()
					/ CENTS_TO_DOLLAR_CONVERSION;
		}
		order.setAmountToPay(amountToPayNow);
		order.setPayAtTerminal(payAtTerminal);
	}

	@Override
	public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate)
	{
		if (recalculate || getBcfOrderRequiresCalculationStrategy().requiresCalculation(entry))
		{
			try
			{
				calculateEntry(entry);
			}
			catch (final CalculationException e)
			{
				e.printStackTrace();
			}
			setCalculatedStatus(entry);
			getModelService().save(entry);
		}
	}

	protected void calculateIndividualEntryPackagePrice(final AbstractOrderModel order)
	{
		final Map<AbstractOrderEntryGroupModel, List<AbstractOrderEntryModel>> dealOrderEntryGroupMap = order.getEntries().stream()
				.filter(entry -> entry.getActive() && (entry.getEntryGroup() instanceof DealOrderEntryGroupModel))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getEntryGroup));

		for (final Map.Entry<AbstractOrderEntryGroupModel, List<AbstractOrderEntryModel>> dealOrderEntryGroupMapEntry : dealOrderEntryGroupMap
				.entrySet())
		{
			final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) dealOrderEntryGroupMapEntry
					.getKey();
			final List<AbstractOrderEntryModel> dealEntries = dealOrderEntryGroupMapEntry.getValue();

			final Map<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByType = dealEntries.stream()
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
			for (final Map.Entry<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByTypeEntry : dealEntriesByType
					.entrySet())
			{
				final OrderEntryType orderEntryType = dealEntriesByTypeEntry.getKey();
				if (Objects.equals(OrderEntryType.TRANSPORT, orderEntryType))
				{
					final List<AbstractOrderEntryModel> transportEntries = dealEntriesByTypeEntry.getValue();
					final double transportTotalPrice = transportEntries.stream().mapToDouble(AbstractOrderEntryModel::getTotalPrice)
							.sum();
					dealOrderEntryGroupModel.setFerryPrice(transportTotalPrice);
				}
				else if (Objects.equals(OrderEntryType.ACCOMMODATION, orderEntryType))
				{
					final List<AbstractOrderEntryModel> accommodationEntries = dealEntriesByTypeEntry.getValue();
					final double accommodationTotalPrice = accommodationEntries.stream()
							.mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();
					dealOrderEntryGroupModel.setHotelPrice(accommodationTotalPrice);

				}
				else
				{
					final List<AbstractOrderEntryModel> activityEntries = dealEntriesByTypeEntry.getValue();
					final double activityTotalPrice = activityEntries.stream().mapToDouble(AbstractOrderEntryModel::getTotalPrice)
							.sum();
					dealOrderEntryGroupModel.setActivityPrice(activityTotalPrice);
				}
			}
		}
		getModelService().saveAll(dealOrderEntryGroupMap.keySet());
	}


	protected void applyGuarantee(final AbstractOrderModel abstractOrderModel)
	{
		final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService()
				.getDealOrderEntryGroups(abstractOrderModel);
		double dealsTotalValue = 0.0d;
		double dealsTotalAmountToPay = 0.0d;
		if (CollectionUtils.isEmpty(dealOrderEntryGroupModels))
		{
			return;
		}
		for (final DealOrderEntryGroupModel dealOrderEntryGroupModel : dealOrderEntryGroupModels)
		{
			double totalDealValue = dealOrderEntryGroupModel.getEntries().stream().filter(AbstractOrderEntryModel::getActive)
					.mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();
			totalDealValue = PrecisionUtil.round(totalDealValue);
			dealOrderEntryGroupModel.setTotalAmount(totalDealValue);
			dealsTotalValue = dealsTotalValue + totalDealValue;

			double amountToPay = getTotalToPay(totalDealValue);
			amountToPay = PrecisionUtil.round(amountToPay);
			dealOrderEntryGroupModel.setAmountToPay(amountToPay);
			dealsTotalAmountToPay = dealsTotalAmountToPay + amountToPay;

		}
		getModelService().saveAll(dealOrderEntryGroupModels);

		final double amountToPayOrderLevel = abstractOrderModel.getAmountToPay().doubleValue() - dealsTotalValue
				+ dealsTotalAmountToPay;
		abstractOrderModel.setAmountToPay(PrecisionUtil.round(amountToPayOrderLevel));

	}

	@Override
	public Double getTotalToPay(final double totalDealValue)
	{
		return totalDealValue;
	}

	/**
	 * @return the findBasePriceByProductTypeStrategyMap
	 */
	protected Map<String, BcfFindBasePriceByProductTypeStrategy> getFindBasePriceByProductTypeStrategyMap()
	{
		return findBasePriceByProductTypeStrategyMap;
	}

	/**
	 * @param findBasePriceByProductTypeStrategyMap the findBasePriceByProductTypeStrategyMap to set
	 */
	@Required
	public void setFindBasePriceByProductTypeStrategyMap(
			final Map<String, BcfFindBasePriceByProductTypeStrategy> findBasePriceByProductTypeStrategyMap)
	{
		this.findBasePriceByProductTypeStrategyMap = findBasePriceByProductTypeStrategyMap;
	}

	/**
	 * @return the bcfOrderRequiresCalculationStrategy
	 */
	protected OrderRequiresCalculationStrategy getBcfOrderRequiresCalculationStrategy()
	{
		return bcfOrderRequiresCalculationStrategy;
	}

	/**
	 * @param bcfOrderRequiresCalculationStrategy the bcfOrderRequiresCalculationStrategy to set
	 */
	@Required
	public void setBcfOrderRequiresCalculationStrategy(final OrderRequiresCalculationStrategy bcfOrderRequiresCalculationStrategy)
	{
		this.bcfOrderRequiresCalculationStrategy = bcfOrderRequiresCalculationStrategy;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	/**
	 * @return the findCostToBcfStrategyMap
	 */
	protected Map<String, FindCostToBcfStrategy> getFindCostToBcfStrategyMap()
	{
		return findCostToBcfStrategyMap;
	}

	/**
	 * @param findCostToBcfStrategyMap the findCostToBcfStrategyMap to set
	 */
	@Required
	public void setFindCostToBcfStrategyMap(final Map<String, FindCostToBcfStrategy> findCostToBcfStrategyMap)
	{
		this.findCostToBcfStrategyMap = findCostToBcfStrategyMap;
	}

	protected Map<String, FindBcfPriceInfoStrategy> getFindBcfPriceInfoStrategyMap()
	{
		return findBcfPriceInfoStrategyMap;
	}

	@Required
	public void setFindBcfPriceInfoStrategyMap(final Map<String, FindBcfPriceInfoStrategy> findBcfPriceInfoStrategyMap)
	{
		this.findBcfPriceInfoStrategyMap = findBcfPriceInfoStrategyMap;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
