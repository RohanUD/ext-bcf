/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.services.impl.DefaultTravelCategoryService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.services.BCFTravelCategoryService;
import com.bcf.core.travel.category.BCFTravelCategoryDao;


/**
 * Implementation class of {@link BCFTravelCategoryService}
 */
public class DefaultBCFTravelCategoryService extends DefaultTravelCategoryService implements BCFTravelCategoryService
{

	private static final Logger LOG = Logger.getLogger(DefaultBCFTravelCategoryService.class);

	@Override
	public List<CategoryModel> getAncillaryCategoriesForProducts(final List<ProductModel> products)
	{
		ServicesUtil.validateParameterNotNull(products, "Products code must not be null");

		if (CollectionUtils.isEmpty(products))
		{
			LOG.warn("Product list is empty!! Returning empty list of categories.");
			return new ArrayList<>();
		}
		return ((BCFTravelCategoryDao) getTravelCategoryDao()).fetchCategoriesForProducts(getProductCodes(products));
	}

	@Override
	public List<ProductModel> getAllProductsForVehicles(final List<String> vehicleCodes)
	{
		ServicesUtil.validateParameterNotNull(vehicleCodes, "Vehicle code must not be null");

		if (CollectionUtils.isEmpty(vehicleCodes))
		{
			LOG.warn("Cannot get Ancillary products!! Either vehicle codes or sector codes list is empty!!");
			return Collections.emptyList();
		}

		return ((BCFTravelCategoryDao) getTravelCategoryDao()).fetchAllProductsForVehicles(vehicleCodes);
	}

	private List<String> getProductCodes(final List<ProductModel> products)
	{
		final List<String> productCodes = new ArrayList<>();
		for (final ProductModel product : products)
		{
			productCodes.add(product.getCode());
		}
		return productCodes;
	}

	@Override
	public List<ProductModel> getAssistanceProductsForVehicle(final List<String> vehicleCodes)
	{
		ServicesUtil.validateParameterNotNull(vehicleCodes, "Vehicle codes must not be null");
		if (CollectionUtils.isEmpty(vehicleCodes))
		{
			LOG.warn("Cannot get products for provided vehicle codes. Vehicle code list is empty!!");
			return Collections.emptyList();
		}
		return ((BCFTravelCategoryDao) getTravelCategoryDao()).fetchAssistanceProductsForVehicle(vehicleCodes);
	}

	@Override
	public List<ProductModel> getProductModelForProductCodesAndCategory(final String categoryCode,
			final List<ProductModel> productCodes)
	{
		ServicesUtil.validateParameterNotNull(categoryCode, "Category code must not be null");
		ServicesUtil.validateParameterNotNull(productCodes, "Product codes must not be null");

		if (CollectionUtils.isEmpty(productCodes))
		{
			LOG.warn("Cannot get products for category code: " + categoryCode + ". Product code list is empty!!");
			return Collections.emptyList();
		}
		return ((BCFTravelCategoryDao) getTravelCategoryDao()).getProductsForOriginDestinationAndCategory(categoryCode,
				productCodes);
	}

}
