/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SERVICE_SUPPLIES;
import com.bcf.core.model.BusinessOpportunitiesModel;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.core.services.BCFCustomerSubscriptionService;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;
import com.bcf.facades.customer.data.BusinessOpportunitiesSubscriptionsData;


public class DefaultBCFCustomerSubscriptionService implements BCFCustomerSubscriptionService
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFCustomerSubscriptionService.class);


	private ModelService modelService;
	private UserService userService;

	@Resource(name = "subscriptionsMasterDetailsService")
	private BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Override
	public Collection<CustomerSubscriptionModel> getAllCustomerSubscriptions()
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		return currentUser.getSubscriptionList();
	}

	@Override
	public boolean createNewsReleaseSubscriptions(final CustomerModel currentUser, final boolean serviceNotice)
	{
		final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(currentUser.getSubscriptionList());
		final CustomerSubscriptionModel existingCustomerSubscriptionModel = getExistingSubscriptionForType(customerSubscriptions,
				CustomerSubscriptionType.NEWS_RELEASE);

		if (Objects.isNull(existingCustomerSubscriptionModel) && !serviceNotice)
		{
			return false;
		}

		if (Objects.isNull(existingCustomerSubscriptionModel))
		{
			final List<CRMSubscriptionMasterDetailModel> subscriptionsMasterDetails = subscriptionsMasterDetailsService
					.getSubscriptionsMasterDetailsForType(CustomerSubscriptionType.NEWS_RELEASE);
			if (CollectionUtils.isEmpty(subscriptionsMasterDetails))
			{
				throw new ModelNotFoundException(
						"No master subscription details have been found for the type " + CustomerSubscriptionType.NEWS_RELEASE
								.getCode());
			}
			createNewCustomerSubscription(currentUser, subscriptionsMasterDetails.get(0));
		}
		else
		{
			modifyCustomerSubscription(existingCustomerSubscriptionModel, serviceNotice);
		}
		return true;
	}

	@Override
	public boolean createOffersAndPromotionsSubscriptions(final CustomerModel currentUser, final boolean offersAndPromotions)
	{
		final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(currentUser.getSubscriptionList());
		final CustomerSubscriptionModel existingCustomerSubscriptionModel = getExistingSubscriptionForType(customerSubscriptions,
				CustomerSubscriptionType.VACATION_NEWSLETTER);

		if (Objects.isNull(existingCustomerSubscriptionModel) && !offersAndPromotions)
		{
			return false;
		}

		if (Objects.isNull(existingCustomerSubscriptionModel))
		{
			final List<CRMSubscriptionMasterDetailModel> subscriptionsMasterDetails = subscriptionsMasterDetailsService
					.getSubscriptionsMasterDetailsForType(CustomerSubscriptionType.VACATION_NEWSLETTER);
			if (CollectionUtils.isEmpty(subscriptionsMasterDetails))
			{
				throw new ModelNotFoundException(
						"No master subscription details have been found for the type " + CustomerSubscriptionType.VACATION_NEWSLETTER
								.getCode());
			}
			createNewCustomerSubscription(currentUser, subscriptionsMasterDetails.get(0));
		}
		else
		{
			modifyCustomerSubscription(existingCustomerSubscriptionModel, offersAndPromotions);
		}
		return true;
	}

	private void modifyCustomerSubscription(final CustomerSubscriptionModel customerSubscriptionModel, final boolean optedIn)
	{
		customerSubscriptionModel.setIsSubscribed(optedIn);

		if (optedIn)
		{
			customerSubscriptionModel.setSubscribedDate(new Date());
		}
		else
		{
			customerSubscriptionModel.setUnSubscribedDate(new Date());
		}
		customerSubscriptionModel.setSource(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
	}

	@Override
	public boolean createBusinessOpportunitiesSubscriptions(final CustomerModel currentUser,
			final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData)
	{
		final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(currentUser.getSubscriptionList());
		final CustomerSubscriptionModel existingCustomerSubscriptionModel = getExistingSubscriptionForType(customerSubscriptions,
				CustomerSubscriptionType.BUSINESS_OPPORTUNITY);

		if (Objects.isNull(existingCustomerSubscriptionModel) && !businessOpportunitiesSubscriptionsData.isOptedIn())
		{
			return false;
		}

		if (Objects.isNull(existingCustomerSubscriptionModel))
		{
			final List<CRMSubscriptionMasterDetailModel> subscriptionsMasterDetails = subscriptionsMasterDetailsService
					.getSubscriptionsMasterDetailsForType(CustomerSubscriptionType.BUSINESS_OPPORTUNITY);
			if (CollectionUtils.isEmpty(subscriptionsMasterDetails))
			{
				throw new ModelNotFoundException(
						"No master subscription details have been found for the type " + CustomerSubscriptionType.BUSINESS_OPPORTUNITY
								.getCode());
			}
			final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel = subscriptionsMasterDetails.get(0);
			createNewBusinessOpportunitySubscription(currentUser, crmSubscriptionMasterDetailModel,
					businessOpportunitiesSubscriptionsData);
		}
		else
		{
			updateBusinessOpportunitySubscription(currentUser, existingCustomerSubscriptionModel,
					businessOpportunitiesSubscriptionsData);
		}

		return true;
	}

	private void updateBusinessOpportunitySubscription(final CustomerModel currentUser,
			final CustomerSubscriptionModel existingCustomerSubscriptionModel,
			final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData)
	{
		existingCustomerSubscriptionModel.setIsSubscribed(businessOpportunitiesSubscriptionsData.isOptedIn());
		if (businessOpportunitiesSubscriptionsData.isOptedIn())
		{
			existingCustomerSubscriptionModel.setSubscribedDate(new Date());
			existingCustomerSubscriptionModel.setSource(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
			BusinessOpportunitiesModel businessOpportunities = existingCustomerSubscriptionModel.getBusinessOpportunities();
			if (Objects.isNull(businessOpportunities))
			{
				businessOpportunities = getModelService().create(BusinessOpportunitiesModel.class);
			}
			businessOpportunities.setCompanyName(businessOpportunitiesSubscriptionsData.getCompanyName());
			businessOpportunities.setJobTitle(businessOpportunitiesSubscriptionsData.getJobTitle());
			final List<SERVICE_SUPPLIES> serviceSupplies = getServiceSupplies(
					businessOpportunitiesSubscriptionsData.getServiceSupplies(), currentUser);
			businessOpportunities.setServiceSupplies(serviceSupplies);
			existingCustomerSubscriptionModel.setBusinessOpportunities(businessOpportunities);
		}
		else
		{
			existingCustomerSubscriptionModel.setUnSubscribedDate(new Date());
			final BusinessOpportunitiesModel businessOpportunities = existingCustomerSubscriptionModel.getBusinessOpportunities();
			getModelService().remove(businessOpportunities);
		}
	}

	private void createNewBusinessOpportunitySubscription(final CustomerModel currentUser,
			final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel,
			final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData)
	{
		{
			final CustomerSubscriptionModel customerSubscriptionModel = createCustomerSubscription(currentUser);
			customerSubscriptionModel.setSubscriptionMasterData(crmSubscriptionMasterDetailModel);

			final BusinessOpportunitiesModel businessOpportunitiesModel = getModelService().create(BusinessOpportunitiesModel.class);
			businessOpportunitiesModel.setCompanyName(businessOpportunitiesSubscriptionsData.getCompanyName());
			businessOpportunitiesModel.setJobTitle(businessOpportunitiesSubscriptionsData.getJobTitle());
			businessOpportunitiesModel
					.setServiceSupplies(getServiceSupplies(businessOpportunitiesSubscriptionsData.getServiceSupplies(), currentUser));
			customerSubscriptionModel.setBusinessOpportunities(businessOpportunitiesModel);
			final List<CustomerSubscriptionModel> customerSubscriptionModelList = new ArrayList<>(currentUser.getSubscriptionList());
			customerSubscriptionModelList.add(customerSubscriptionModel);
			currentUser.setSubscriptionList(customerSubscriptionModelList);
		}
	}

	private List<SERVICE_SUPPLIES> getServiceSupplies(final List<String> serviceSupplies, final CustomerModel currentUser)
	{
		final List<SERVICE_SUPPLIES> enumerationValues = enumerationService.getEnumerationValues(SERVICE_SUPPLIES.class);
		final Map<String, SERVICE_SUPPLIES> serviceSulliesEnums = enumerationValues.stream()
				.collect(Collectors.toMap(o -> o.getCode(), o -> o));

		final List<SERVICE_SUPPLIES> selectedServiceSupplies = new ArrayList<>();
		serviceSupplies.stream().forEach(s -> {
			if (serviceSulliesEnums.containsKey(s))
			{
				selectedServiceSupplies.add(serviceSulliesEnums.get(s));
			}
			else
			{
				LOG.error("Can not find the SERVICE_SUPPLIES enum for the value " + s + ", for the customer " + currentUser.getUid());
			}
		});

		return selectedServiceSupplies;
	}

	private CustomerSubscriptionModel createCustomerSubscription(final CustomerModel currentUser)
	{
		final CustomerSubscriptionModel customerSubscriptionModel = getModelService().create(CustomerSubscriptionModel.class);
		customerSubscriptionModel.setOwner(currentUser);
		customerSubscriptionModel.setIsSubscribed(Boolean.TRUE);
		customerSubscriptionModel.setSubscribedDate(new Date());
		customerSubscriptionModel.setSource(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
		return customerSubscriptionModel;
	}

	private CustomerSubscriptionModel getExistingSubscriptionForType(final List<CustomerSubscriptionModel> customerSubscriptions,
			final CustomerSubscriptionType customerSubscriptionType)
	{
		for (final CustomerSubscriptionModel customerSubscriptionModel : customerSubscriptions)
		{
			if (Objects.nonNull(customerSubscriptionModel.getSubscriptionMasterData()) && customerSubscriptionModel
					.getSubscriptionMasterData().getSubscriptionType().equals(customerSubscriptionType))
			{
				return customerSubscriptionModel;
			}
		}
		return null;
	}

	@Override
	public void saveCustomerSubscription(final CustomerModel currentCustomerModel, final List<String> optInSubscriptionCodes)
	{
		// Update Opt-In Data
		optInSubscriptionCodes.stream().forEach(optInSubscriptionCode -> {

			final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(
					currentCustomerModel.getSubscriptionList());

			final CustomerSubscriptionModel existingCustomerSubscriptionModel = getExistingSubscriptionByCode(customerSubscriptions,
					optInSubscriptionCode);

			if (Objects.isNull(existingCustomerSubscriptionModel))
			{
				createNewCustomerSubscription(currentCustomerModel, optInSubscriptionCode);
			}
			else
			{
				modifyCustomerSubscription(existingCustomerSubscriptionModel);
			}
		});

		// Update Opt-Out Data
		this.optOutServiceNoticesSubscriptions(currentCustomerModel, optInSubscriptionCodes);

	}

	@Override
	public void unsubscribeAll(final CustomerModel customerModel)
	{
		ServicesUtil.validateParameterNotNull(customerModel, "customerModel can not be null.");
		if (CollectionUtils.isEmpty(customerModel.getSubscriptionList()))
		{
			return;
		}

		getModelService().removeAll(customerModel.getSubscriptionList());
		getModelService().save(customerModel);
	}

	@Override
	public void unsubscribeByNames(final String unsubscribeNames)
	{
		ServicesUtil.validateParameterNotNull(unsubscribeNames, "unsubscribeNames can not be null or empty.");
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		if (CollectionUtils.isEmpty(customerModel.getSubscriptionList()))
		{
			return;
		}

		final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(customerModel.getSubscriptionList());
		final String[] unSubCodes = unsubscribeNames.split(",");
		for (final String unSubCode : unSubCodes)
		{
			final CustomerSubscriptionModel existingSubscription = getExistingSubscriptionByCode(customerSubscriptions, unSubCode);
			if (Objects.nonNull(existingSubscription))
			{
				optOut(existingSubscription);
			}
		}
	}

	@Override
	public void unsubscribeByCodes(final CustomerModel customerModel, final String unsubscribeCodes)
	{
		ServicesUtil.validateParameterNotNull(unsubscribeCodes, "unsubscribeCodes can not be null or empty.");
		ServicesUtil.validateParameterNotNull(customerModel, "customerModel can not be null or empty.");
		if (CollectionUtils.isEmpty(customerModel.getSubscriptionList()))
		{
			return;
		}

		final List<CustomerSubscriptionModel> customerSubscriptions = new ArrayList<>(customerModel.getSubscriptionList());
		final String[] unSubCodes = unsubscribeCodes.split(",");
		for (final String unSubCode : unSubCodes)
		{
			final CustomerSubscriptionModel existingSubscription = getExistingSubscriptionByCode(customerSubscriptions, unSubCode);
			if (Objects.nonNull(existingSubscription))
			{
				optOut(existingSubscription);
			}
		}
	}

	private void optOutServiceNoticesSubscriptions(final CustomerModel currentUser, final List<String> optInSubscriptionCodes)
	{
		currentUser.getSubscriptionList().stream().forEach(customerSubscriptionModel -> {
			if (customerSubscriptionModel.getSubscriptionMasterData().getSubscriptionType()
					.equals(CustomerSubscriptionType.SERVICE_NOTICE) && customerSubscriptionModel.getIsSubscribed())
			{
				if (isNotInOptInList(optInSubscriptionCodes,
						customerSubscriptionModel.getSubscriptionMasterData().getSubscriptionCode()))
				{
					optOut(customerSubscriptionModel);
				}
			}
		});
	}

	private void optOut(final CustomerSubscriptionModel customerSubscriptionModel)
	{
		if (!Objects.equals(customerSubscriptionModel.getIsSubscribed(), Boolean.FALSE))
		{
			customerSubscriptionModel.setIsSubscribed(Boolean.FALSE);
			customerSubscriptionModel.setUnSubscribedDate(new Date());
			customerSubscriptionModel.setSubscribedDate(null);
		}
	}

	private boolean isNotInOptInList(final List<String> optInSubscriptionList, final String compareString)
	{
		return CollectionUtils.isEmpty(optInSubscriptionList) || !optInSubscriptionList.contains(compareString);
	}

	private void modifyCustomerSubscription(final CustomerSubscriptionModel customerSubscriptionModel)
	{
		if (!customerSubscriptionModel.getIsSubscribed())
		{
			customerSubscriptionModel.setIsSubscribed(true);
			customerSubscriptionModel.setSubscribedDate(new Date());
			customerSubscriptionModel.setSource(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
		}
	}

	private void createNewCustomerSubscription(final CustomerModel currentCustomerModel, final String optInSubscriptionCode)
	{
		final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel = subscriptionsMasterDetailsService
				.getSubscriptionDetailForSubscriptionCode(optInSubscriptionCode);
		if (Objects.isNull(crmSubscriptionMasterDetailModel))
		{
			LOG.error("Could not find the master subscription details for the given name " + optInSubscriptionCode);
			return;
		}

		this.createNewCustomerSubscription(currentCustomerModel, crmSubscriptionMasterDetailModel);
	}

	private void createNewCustomerSubscription(final CustomerModel currentCustomerModel,
			final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel)
	{
		final CustomerSubscriptionModel customerSubscriptionModel = createCustomerSubscription(currentCustomerModel);

		customerSubscriptionModel.setSubscriptionMasterData(crmSubscriptionMasterDetailModel);
		final List<CustomerSubscriptionModel> customerSubscriptionModelList = new ArrayList<>(
				currentCustomerModel.getSubscriptionList());
		customerSubscriptionModelList.add(customerSubscriptionModel);
		currentCustomerModel.setSubscriptionList(customerSubscriptionModelList);
	}

	private CustomerSubscriptionModel getExistingSubscriptionByCode(final List<CustomerSubscriptionModel> customerSubscriptions,
			final String optInSubscriptionCode)
	{

		for (final CustomerSubscriptionModel customerSubscriptionModel : customerSubscriptions)
		{
			if (Objects.nonNull(customerSubscriptionModel.getSubscriptionMasterData()) && optInSubscriptionCode.equals(customerSubscriptionModel
					.getSubscriptionMasterData().getSubscriptionCode()))
			{
				return customerSubscriptionModel;
			}
		}
		return null;
	}

	private CustomerSubscriptionModel getExistingSubscriptionById(final List<CustomerSubscriptionModel> customerSubscriptions,
			final String optInSubscriptionCode)
	{

		for (final CustomerSubscriptionModel customerSubscriptionModel : customerSubscriptions)
		{
			if (Objects.nonNull(customerSubscriptionModel.getSubscriptionMasterData()) && optInSubscriptionCode.equals(customerSubscriptionModel
				.getSubscriptionMasterData().getSubscriptionCode()))
			{
				return customerSubscriptionModel;
			}
		}
		return null;
	}



	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	private UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
