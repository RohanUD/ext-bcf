/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.travelservices.model.travel.TravelProviderModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.BcfTravelProviderService;
import com.bcf.core.travelprovider.dao.BcfTravelProviderDao;


public class DefaultBcfTravelProviderService implements BcfTravelProviderService
{
	private BcfTravelProviderDao bcfTravelProviderDao;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public TravelProviderModel getTravelProviderModel(final String code)
	{
		return getBcfTravelProviderDao().findTravelProviderModel(code);
	}

	@Override
	public TravelProviderModel getDefaultProviderModel()
	{
		return getTravelProviderModel(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.DEFAULT_TRAVEL_PROVIDER));
	}

	protected BcfTravelProviderDao getBcfTravelProviderDao()
	{
		return bcfTravelProviderDao;
	}

	@Required
	public void setBcfTravelProviderDao(final BcfTravelProviderDao bcfTravelProviderDao)
	{
		this.bcfTravelProviderDao = bcfTravelProviderDao;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

}
