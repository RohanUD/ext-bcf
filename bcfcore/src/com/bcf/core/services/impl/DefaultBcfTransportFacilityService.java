/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.services.impl.DefaultTransportFacilityService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.transportFacility.dao.BcfTransportFacilityDao;


public class DefaultBcfTransportFacilityService extends DefaultTransportFacilityService implements BcfTransportFacilityService
{
	private BcfTransportFacilityDao bcfTransportFacilityDao;

	@Override
	public LocationModel getCountry(final TransportFacilityModel transportFacility)
	{
		LocationModel locationModel = transportFacility.getLocation();
		if (locationModel == null)
		{
			return null;
		}

		while (!LocationType.COUNTRY.equals(locationModel.getLocationType()) && CollectionUtils
				.isNotEmpty(locationModel.getSuperlocations()))
		{
			locationModel = locationModel.getSuperlocations().get(0);
		}
		return LocationType.COUNTRY.equals(locationModel.getLocationType()) ? locationModel : null;
	}

	@Override
	public LocationModel getCity(final TransportFacilityModel transportFacility)
	{
		LocationModel locationModel = transportFacility.getLocation();
		if (locationModel == null)
		{
			return null;
		}

		while (!LocationType.CITY.equals(locationModel.getLocationType()) && CollectionUtils
				.isNotEmpty(locationModel.getSuperlocations()))
		{
			locationModel = locationModel.getSuperlocations().get(0);
		}
		return LocationType.CITY.equals(locationModel.getLocationType()) ? locationModel : null;
	}

	@Override
	public TransportFacilityModel getTransportFacilityForLocation(final LocationModel location)
	{
		validateParameterNotNull(location, "location must not be null!");
		final Map<String, LocationModel> params = new HashMap<>();
		params.put(TransportFacilityModel.LOCATION, location);
		return getBcfTransportFacilityDao().findTransportFacilityForLocation(location);
	}

	@Override
	public TransportFacilityModel getTransportFacilityForCode(final String travelFacilityCode)
	{
		validateParameterNotNull(travelFacilityCode, "travelFacilityCode must not be null!");
		final Map<String, String> params = new HashMap<>();
		params.put(TransportFacilityModel.CODE, travelFacilityCode);
		return getBcfTransportFacilityDao().findTransportFacility(travelFacilityCode);
	}

	@Override
	public List<TransportFacilityModel> getTransportFacilityForTerminalCodes(final List<String> terminalCodes)
	{
		validateParameterNotNull(terminalCodes, "terminalCodes must not be null!");
		return getBcfTransportFacilityDao().findTransportFacilityForTerminalCodes(terminalCodes);

	}

	@Override
	public List<TransportFacilityModel> getAllTransportFacilities()
	{
		return getBcfTransportFacilityDao().findAllTransportFacilities();
	}

	@Override
	public SearchPageData<TransportFacilityModel> getPaginatedTransportFacilities(final int pageSize, final int currentPage)
	{
		return getBcfTransportFacilityDao().getPaginatedTransportFacilities(pageSize, currentPage);
	}

	public BcfTransportFacilityDao getBcfTransportFacilityDao()
	{
		return bcfTransportFacilityDao;
	}

	@Required
	public void setBcfTransportFacilityDao(final BcfTransportFacilityDao bcfTransportFacilityDao)
	{
		this.bcfTransportFacilityDao = bcfTransportFacilityDao;
	}
}
