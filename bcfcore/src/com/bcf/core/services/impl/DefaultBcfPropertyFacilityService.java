/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.travelservices.enums.PropertyFacilityType;
import de.hybris.platform.travelservices.model.facility.PropertyFacilityModel;
import de.hybris.platform.travelservices.services.impl.DefaultPropertyFacilityService;
import com.bcf.core.dao.BcfPropertyFacilityDao;
import com.bcf.core.services.BcfPropertyFacilityService;


public class DefaultBcfPropertyFacilityService extends DefaultPropertyFacilityService implements BcfPropertyFacilityService
{

	private BcfPropertyFacilityDao bcfPropertyFacilityDao;

	@Override
	public PropertyFacilityModel getPropertyFacilityFromType(final PropertyFacilityType type)
	{
		return getBcfPropertyFacilityDao().findPropertyFacilityFromType(type);
	}

	public BcfPropertyFacilityDao getBcfPropertyFacilityDao()
	{
		return bcfPropertyFacilityDao;
	}

	public void setBcfPropertyFacilityDao(final BcfPropertyFacilityDao bcfPropertyFacilityDao)
	{
		this.bcfPropertyFacilityDao = bcfPropertyFacilityDao;
	}
}
