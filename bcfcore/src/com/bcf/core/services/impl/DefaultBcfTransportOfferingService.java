/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.impl.DefaultTransportOfferingService;
import java.util.Date;
import java.util.List;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.transportOffering.dao.BcfTransportOfferingDao;

public class DefaultBcfTransportOfferingService extends DefaultTransportOfferingService implements BcfTransportOfferingService {
   private BcfTransportOfferingDao transportOfferingDao;

   @Override public TransportOfferingModel getTransportOfferings(final Date departure, final Date arrival, final TravelSectorModel sector) {
      return getTransportOfferingDao().getTransportOfferings(departure, arrival, sector);
   }

   @Override public TransportOfferingModel getDefaultTransportOfferingForSector(final String travelSector) {
      return getTransportOfferingDao().findTransportOffering(travelSector, travelSector);
   }

   @Override public List<TransportOfferingModel> getTransportOfferingsForTransferIdentifier(final String transferIdentifier) {
      return getTransportOfferingDao().findTransportOfferingForTransferIdentifier(transferIdentifier);
   }

   @Override public List<TransportOfferingModel> getTransportOfferingsForSailingCode(final String sailingCode) {
      return getTransportOfferingDao().findTransportOfferingsForSailingCode(sailingCode);
   }

   @Override public TransportOfferingModel getTransportOfferingForSailingCodeAndSector(final String eBookingCode, final String travelSector) {
      return getTransportOfferingDao().findTransportOffering(eBookingCode, travelSector);
   }

   @Override public TransportOfferingModel getDefaultTransportOfferingByRoute(final String travelRouteCode) {
      return getTransportOfferingDao().findTransportOffering(travelRouteCode);
   }

   @Override public List<TransportOfferingModel> getDefaultTransportOfferingBySector(final String travelSectorCode) {
      return getTransportOfferingDao().findDefaultTransportOfferingBySector(travelSectorCode);
   }

   @Override public List<AbstractOrderEntryModel> getModifiedOrderEntriesForOfferingCode(final String offeringCode) {
      return getTransportOfferingDao().findModifiedOrderEntriesForOfferingCode(offeringCode);
   }

   @Override public BcfTransportOfferingDao getTransportOfferingDao() {
      return transportOfferingDao;
   }

   public void setTransportOfferingDao(final BcfTransportOfferingDao transportOfferingDao) {
      this.transportOfferingDao = transportOfferingDao;
   }

}
