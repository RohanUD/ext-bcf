/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.price.impl.DefaultTravelCommercePriceService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.services.accommodation.costtobcf.strategies.impl.VacationCostToBcfCalculationStrategy;
import com.bcf.core.services.accommodation.discount.strategies.impl.VacationDiscountCalculationStrategy;
import com.bcf.core.services.accommodation.strategies.impl.ExtraOccupancyPriceCalculationStrategy;
import com.bcf.core.services.accommodation.strategies.impl.ExtraProductPriceCalculationStrategy;
import com.bcf.core.services.accommodation.strategies.impl.RoomRatePriceCalculationStrategy;
import com.bcf.core.services.activity.strategies.impl.ActivityPriceCalculationStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.services.vacation.tax.strategies.impl.VacationTaxCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.BcfPriceValue;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class DefaultBCFTravelCommercePriceService extends DefaultTravelCommercePriceService
		implements BCFTravelCommercePriceService
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFTravelCommercePriceService.class);
	private static final String JOURNEY_DATE = "journeyDepartureDate";
	private static final String PRICE_INFO_LOG = "Price Info for ";

	private RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy;
	private ExtraOccupancyPriceCalculationStrategy extraOccupancyPriceCalculationStrategy;
	private ExtraProductPriceCalculationStrategy extraProductPriceCalculationStrategy;
	private ActivityPriceCalculationStrategy activityPriceCalculationStrategy;
	private VacationPriceCalculationStrategy vacationPriceCalculationStrategy;
	private VacationDiscountCalculationStrategy vacationDiscountCalculationStrategy;
	private VacationTaxCalculationStrategy vacationTaxCalculationStrategy;
	private VacationCostToBcfCalculationStrategy vacationCostToBcfCalculationStrategy;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	/**
	 * Method to return price level info as a map, with price level as key and code as value.
	 *
	 * @param originDestinationRefNumber the origin destination ref number
	 * @param productCode                the product code
	 * @param transportOfferingCode      the transport offering code
	 * @param routeCode                  the route code
	 * @param offerGroupCode             the offer group code
	 * @return price level info
	 */
	@Override
	public PriceInformation getPriceInfoForAncillary(final int originDestinationRefNumber, final String productCode,
			final String transportOfferingCode, final String routeCode, final String offerGroupCode)
	{
		final Map<String, String> searchCriteria = new HashMap<>();
		PriceInformation priceInfo = null;

		final String offerGroupType = getOfferGroupToOriginDestinationMapping().getOrDefault(offerGroupCode,
				getOfferGroupToOriginDestinationMapping().getOrDefault(TravelservicesConstants.DEFAULT_OFFER_GROUP_TO_OD_MAPPING,
						TravelservicesConstants.TRAVEL_ROUTE));

		final List<TransportOfferingModel> transportOfferings = getTransportOfferingsFromCartForOriginDestRefNumAndCode(
				originDestinationRefNumber, transportOfferingCode);

		final Optional<TransportOfferingModel> transportOfferingModelOptional = transportOfferings.stream().findFirst();
		String sectorCode = null;
		if (transportOfferingModelOptional.isPresent())
		{
			sectorCode = transportOfferingModelOptional.get().getTravelSector().getCode();
		}

		updateSearchCriteria(transportOfferings, searchCriteria, transportOfferingCode);

		if (TravelservicesConstants.TRAVEL_ROUTE.equalsIgnoreCase(offerGroupType))
		{
			searchCriteria.put(PriceRowModel.TRAVELROUTECODE, routeCode);
			priceInfo = getPriceInformation(productCode, searchCriteria);
		}

		//either no valid price found for route or the the offer group type if transportOffering
		if (!Optional.ofNullable(priceInfo).isPresent()
				|| TravelservicesConstants.TRANSPORT_OFFERING.equalsIgnoreCase(offerGroupType))
		{
			searchCriteria.remove(PriceRowModel.TRAVELROUTECODE);
			priceInfo = getPriceInformationFromTransportOfferingOrSector(transportOfferingCode, sectorCode, productCode,
					originDestinationRefNumber);
		}

		//If still no price found, get the default price.
		if (!Optional.ofNullable(priceInfo).isPresent())
		{
			searchCriteria.clear();
			updateSearchCriteria(transportOfferings, searchCriteria, transportOfferingCode);
			priceInfo = getPriceInformation(productCode, searchCriteria);
		}

		return priceInfo;
	}

	private List<TransportOfferingModel> getTransportOfferingsFromCartForOriginDestRefNumAndCode(
			final int originDestinationRefNumber, final String transportOfferingCode)
	{
		final CartModel cart = getCartService().getSessionCart();
		final List<TransportOfferingModel> transportOfferings = new ArrayList<>();

		final List<AbstractOrderEntryModel> abstractOrderEntryModels = cart.getEntries().stream()
				.filter(aoem -> aoem.getTravelOrderEntryInfo() != null
						&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
						&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber().intValue() == originDestinationRefNumber)
				.collect(Collectors.toList());

		abstractOrderEntryModels.forEach(aoem -> aoem.getTravelOrderEntryInfo().getTransportOfferings().forEach(to -> {
			if (to != null && to.getCode().equals(transportOfferingCode))
			{
				transportOfferings.add(to);
			}
		}));

		return transportOfferings;
	}

	@Override
	public PriceInformation getPriceInformationFromTransportOfferingOrSector(final String transportOfferingCode,
			final String sectorCode, final String productCode, final int originDestinationRefNumber)
	{
		final Map<String, String> searchCriteria = new HashMap<>();
		PriceInformation priceInfo = null;

		final List<TransportOfferingModel> transportOfferings = getTransportOfferingsFromCartForOriginDestRefNumAndCode(
				originDestinationRefNumber, transportOfferingCode);

		updateSearchCriteria(transportOfferings, searchCriteria, transportOfferingCode);

		searchCriteria.put(PriceRowModel.TRANSPORTOFFERINGCODE, transportOfferingCode);
		priceInfo = getPriceInformation(productCode, searchCriteria);

		if (!Optional.ofNullable(priceInfo).isPresent())
		{
			searchCriteria.remove(PriceRowModel.TRANSPORTOFFERINGCODE);
			searchCriteria.put(PriceRowModel.TRAVELSECTORCODE, sectorCode);

			priceInfo = getPriceInformation(productCode, searchCriteria);
		}
		return priceInfo;
	}

	@Override
	public void updateSearchCriteria(final List<TransportOfferingModel> transportOfferings,
			final Map<String, String> searchCriteria, final String transportOfferingCode)
	{
		String departureDate = null;

		//this should be already check prior to this method, still checking to be sure
		final Optional<TransportOfferingModel> matchingTransportOfferingData = transportOfferings.stream()
				.filter(transportOffering -> transportOfferingCode.equals(transportOffering.getCode())).findFirst();

		if (matchingTransportOfferingData.isPresent())
		{
			departureDate = TravelDateUtils.convertDateToStringDate(matchingTransportOfferingData.get().getDepartureTime(),
					TravelservicesConstants.DATE_PATTERN);
		}
		searchCriteria.put(JOURNEY_DATE, departureDate);
	}

	@Override
	public PriceInformation getPriceInformation(final String productCode, final Map<String, String> searchCriteria)
	{
		validateParameterNotNull(searchCriteria, "Search Criteria must not be null");

		final Map<String, String> filteredSearchCriteria = new HashMap<>();
		final ProductModel product = getProductService().getProductForCode(productCode);

		for (final Entry<String, String> entry : searchCriteria.entrySet())
		{
			if (entry.getKey() != null)
			{
				filteredSearchCriteria.put(entry.getKey(), entry.getValue());
			}
		}

		LOG.debug("Getting price information for productData (code: " + productCode + ") and searchKey (key: "
				+ StringUtils.join(searchCriteria.keySet(), ",") + ")" + "), searchValues ( "
				+ StringUtils.join(searchCriteria.values(), ","));
		final PriceInformation priceInfo = getProductWebPrice(product, filteredSearchCriteria);

		if (priceInfo == null)
		{
			LOG.debug("No price information for productData (code: " + productCode + ") and searchKey (key: "
					+ StringUtils.join(searchCriteria.keySet(), ",") + "), searchValues ( "
					+ StringUtils.join(searchCriteria.values(), ","));
		}
		return priceInfo;
	}

	@Override
	public PriceValue applyMargin(final PriceValue basePrice, final TravelRouteModel travelRoute)
	{
		final Double marginRate = getMarginRate(travelRoute.getDestination());
		if (Objects.isNull(marginRate) || marginRate == 0)
		{
			return basePrice;
		}
		final double marginVal = basePrice.getValue() * marginRate / 100;
		final double effectiveBasePrice = basePrice.getValue() + marginVal;
		return new PriceValue(basePrice.getCurrencyIso(), effectiveBasePrice, true);
	}

	@Override
	public Double getMarginRate(final TransportFacilityModel transportFacility)
	{
		Double marginRate = transportFacility.getMarginRate();
		if (Objects.isNull(marginRate))
		{
			final boolean applyDefaultDealMarginOnFare = Boolean.parseBoolean(
					getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.APPLY_DEFAULT_DEAL_MARGIN_ON_FARE));
			if (applyDefaultDealMarginOnFare)
			{
				final String defaultDealMarginOnFarePriceStr = getBcfConfigurablePropertiesService()
						.getBcfPropertyValue(BcfCoreConstants.DEFAULT_DEAL_MARGIN_ON_FARE_PRICE);
				if (NumberUtils.isParsable(defaultDealMarginOnFarePriceStr))
				{
					final double defaultDealMarginOnFarePrice = Double.parseDouble(defaultDealMarginOnFarePriceStr);
					marginRate = defaultDealMarginOnFarePrice;
				}
			}
		}
		return marginRate;
	}

	@Override
	public BcfPriceValue findCostToBCF(final ProductModel product, final PriceValue basePrice, final LocationModel location)
	{
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		final List<TaxValue> taxValues = getVacationTaxCalculationStrategy().getTaxValues(product, basePrice.getValue(), location);
		taxValues.forEach(taxValue -> LOG
				.info("TaxValue For Tax " + taxValue.getCode() + " : " + location.getCode() + " is : " + taxValue.getValue()));
		final double taxValue = taxValues.stream().mapToDouble(TaxValue::getValue).sum();
		final double productEffectivePrice = PrecisionUtil.round(basePrice.getValue() + taxValue);
		LOG.info("Cost To BCF for " + product.getCode() + " is : " + productEffectivePrice);

		return new BcfPriceValue(basePrice.getCurrencyIso(), productEffectivePrice, true, 0d, taxValues);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final RoomRateProductModel roomRateProduct, final long quantity,
			final RatePlanModel ratePlan, final LocationModel location)
	{
		LOG.info(PRICE_INFO_LOG + roomRateProduct.getCode() + ":");
		final PriceValue basePrice = getRoomRatePriceCalculationStrategy().getBasePrice(roomRateProduct, quantity);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with quantity " + quantity + " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(roomRateProduct, basePrice, location);
		final PriceValue marginValue = getVacationPriceCalculationStrategy().getMargin(ratePlan, costToBcf);
		final DiscountValue discountVal = getVacationDiscountCalculationStrategy().getDiscount(ratePlan, costToBcf.getValue(),
				costToBcf.getCurrencyIso());

		final List<DiscountValue> discountValues = new ArrayList<>();
		discountValues.add(discountVal);

		return getBcfPriceInfo(roomRateProduct, quantity, basePrice, costToBcf, marginValue, discountValues, location);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final AbstractOrderEntryModel entry,
			final RoomRateProductModel roomRateProduct, final long quantity,
			final RatePlanModel ratePlan, final LocationModel location)
	{
		LOG.info("Price Info for " + roomRateProduct.getCode() + ":");
		final PriceValue basePrice = getRoomRatePriceCalculationStrategy().getBasePrice(roomRateProduct, quantity);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with quantity " + quantity + " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(roomRateProduct, basePrice, location);
		final PriceValue marginValue = getVacationPriceCalculationStrategy().getMargin(ratePlan, costToBcf);
		final DiscountValue discountVal = getVacationDiscountCalculationStrategy().getDiscount(ratePlan, costToBcf.getValue(),
				costToBcf.getCurrencyIso());

		final List<DiscountValue> discountValues = new ArrayList<>();
		List<DiscountValue> entryLevelDiscountValues = new ArrayList<>();
		if (Objects.nonNull(entry) && CollectionUtils.isNotEmpty(entry.getDiscountValues()))
		{
			if (Objects.nonNull(discountVal))
			{
				entryLevelDiscountValues = entry.getDiscountValues().stream().filter(dv -> !dv.equals(discountVal))
						.collect(Collectors.toList());
			}
			else
			{
				entryLevelDiscountValues = entry.getDiscountValues();
			}
		}
		discountValues.add(discountVal);

		return getBcfPriceInfo(roomRateProduct, quantity, basePrice, costToBcf, marginValue, discountValues,
				entryLevelDiscountValues, location);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final AbstractOrderEntryModel entry ,final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, final long quantity,
			final String passengerType, final RatePlanModel ratePlan, final LocationModel location, final Date stayDate)
	{

		LOG.info("Price Info for " + extraGuestOccupancyProduct.getCode() + ":");
		final PriceValue basePrice = getExtraOccupancyPriceCalculationStrategy().getBasePrice(extraGuestOccupancyProduct, passengerType, stayDate);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with quantity " + quantity + " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(extraGuestOccupancyProduct, basePrice, location);
		final PriceValue marginValue = getVacationPriceCalculationStrategy().getMargin(ratePlan, costToBcf);
		final DiscountValue discountVal = getVacationDiscountCalculationStrategy().getDiscount(ratePlan, costToBcf.getValue(),
				costToBcf.getCurrencyIso());

		final List<DiscountValue> discountValues = new ArrayList<>();
		List<DiscountValue> entryLevelDiscountValues = new ArrayList<>();
		if (Objects.nonNull(entry) && CollectionUtils.isNotEmpty(entry.getDiscountValues()))
		{
			if (Objects.nonNull(discountVal))
			{
				entryLevelDiscountValues = entry.getDiscountValues().stream().filter(dv -> !dv.equals(discountVal))
						.collect(Collectors.toList());
			}
			else
			{
				entryLevelDiscountValues = entry.getDiscountValues();
			}
		}
		discountValues.add(discountVal);

		return getBcfPriceInfo(extraGuestOccupancyProduct, quantity, basePrice, costToBcf, marginValue, discountValues,
				entryLevelDiscountValues, location);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, final long quantity,
			final String passengerType, final RatePlanModel ratePlan, final LocationModel location, final Date stayDate)
	{
		LOG.info(PRICE_INFO_LOG + extraGuestOccupancyProduct.getCode() + ":");
		final PriceValue basePrice = getExtraOccupancyPriceCalculationStrategy().getBasePrice(extraGuestOccupancyProduct,
				passengerType, stayDate);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with passengerType " + passengerType + " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(extraGuestOccupancyProduct, basePrice, location);
		final PriceValue marginValue = getVacationPriceCalculationStrategy().getMargin(ratePlan, costToBcf);
		final DiscountValue discountVal = getVacationDiscountCalculationStrategy().getDiscount(ratePlan, costToBcf.getValue(),
				costToBcf.getCurrencyIso());
		final List<DiscountValue> discountValues = new ArrayList<>();
		discountValues.add(discountVal);
		return getBcfPriceInfo(extraGuestOccupancyProduct, quantity, basePrice, costToBcf, marginValue, discountValues, location);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final ExtraProductModel extraProductModel, final long quantity,
			final LocationModel location)
	{
		LOG.info(PRICE_INFO_LOG + extraProductModel.getCode() + ":");
		final PriceValue basePrice = getExtraProductPriceCalculationStrategy().getBasePrice(extraProductModel);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with ExtraProduct " + extraProductModel.getCode() + " : " + basePrice.getValue());

		BcfPriceValue costToBcf = null;

		if (location != null)
		{
			costToBcf = findCostToBCF(extraProductModel, basePrice, location);
		}

		return getBcfPriceInfo(extraProductModel, quantity, basePrice, costToBcf, null, null, location);
	}


	@Override
	public BcfPriceInfo getBcfPriceInfo(final ExtraProductModel extraProductModel, final long quantity,
			final RatePlanModel ratePlan, final LocationModel location)
	{
		LOG.info(PRICE_INFO_LOG + extraProductModel.getCode() + ":");
		final PriceValue basePrice = getExtraProductPriceCalculationStrategy().getBasePrice(extraProductModel);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with ExtraProduct " + extraProductModel.getCode() + " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(extraProductModel, basePrice, location);
		final PriceValue marginValue = getVacationPriceCalculationStrategy().getMargin(ratePlan, costToBcf);
		final DiscountValue discountVal = getVacationDiscountCalculationStrategy().getDiscount(ratePlan, costToBcf.getValue(),
				costToBcf.getCurrencyIso());
		final List<DiscountValue> discountValues = new ArrayList<>();
		discountValues.add(discountVal);
		return getBcfPriceInfo(extraProductModel, quantity, basePrice, costToBcf, marginValue, discountValues, location);
	}

	@Override
	public BcfPriceInfo getBcfPriceInfo(final ActivityProductModel activityProduct, final long quantity,
			final PassengerTypeModel passengerType, final Date commencementDate)
	{
		LOG.info(PRICE_INFO_LOG + activityProduct.getCode() + ":");
		final LocationModel location = activityProduct.getDestination();
		final BcfPriceValue basePrice = getActivityPriceCalculationStrategy().getBasePrice(activityProduct, passengerType,
				commencementDate);
		if (Objects.isNull(basePrice))
		{
			return null;
		}
		LOG.info("Base Price Value with passengerType " + passengerType.getCode() + " and commencementDate " + commencementDate
				+ " : " + basePrice.getValue());

		final BcfPriceValue costToBcf = findCostToBCF(activityProduct, basePrice, location);
		final PriceValue marginValue = findMargin(basePrice.getMarginRate(), costToBcf);

		return getBcfPriceInfo(activityProduct, quantity, basePrice, costToBcf, marginValue, null, location);
	}

	protected PriceValue findMargin(final double marginRate, final PriceValue priceValue)
	{
		final double marginVal = priceValue.getValue() * marginRate / 100;
		final double marginValRounded = PrecisionUtil.round(marginVal);
		return new PriceValue(priceValue.getCurrencyIso(), marginValRounded, true);
	}

	protected BcfPriceInfo getBcfPriceInfo(final ProductModel product, final long quantity, final PriceValue basePriceVal,
			final BcfPriceValue costToBcfPriceVal, final PriceValue marginPriceValue, final List<DiscountValue> discountValues,
			final LocationModel location)
	{
		final double marginValue = Objects.isNull(marginPriceValue) ? 0.0d : PrecisionUtil.round(marginPriceValue.getValue());
		final double discountVal = CollectionUtils.isEmpty(discountValues) ?
				0 :
				PrecisionUtil.round(discountValues.stream().filter(Objects::nonNull).mapToDouble(DiscountValue::getValue).sum());
		final double costValue = Objects.isNull(costToBcfPriceVal) ? 0 : costToBcfPriceVal.getValue();
		final List<TaxValue> taxValues = Objects.isNull(costToBcfPriceVal) ? new ArrayList<>() : costToBcfPriceVal.getTaxValues();
		LOG.info("Margin Value : " + marginValue);
		LOG.info("Discount Value : " + discountVal);

		final double netPriceVal = PrecisionUtil.round(costValue + marginValue - discountVal);
		LOG.info("Net Price : " + netPriceVal);
		final List<TaxValue> hiddenTaxValues = taxValues;

		TaxValue gstTaxValue = null;
		double totalPrice = netPriceVal;
		if (location != null)
		{
			final List<String> applicableTaxCodes = getVacationTaxCalculationStrategy().getApplicableTaxCodes(product, location);
			if (applicableTaxCodes.contains(BcfCoreConstants.GST))
			{
				final List<TaxValue> gstTaxValues = getVacationTaxCalculationStrategy()
						.getTaxValues(Collections.singletonList(BcfCoreConstants.GST), netPriceVal, location, false);
				gstTaxValue = gstTaxValues.stream().findFirst().orElse(null);
			}
			if (gstTaxValue != null)
			{
				LOG.info("Final GstTaxValue for location " + location.getCode() + " : " + gstTaxValue.getValue());
				totalPrice = totalPrice + gstTaxValue.getValue();
			}
		}
		LOG.info("Total Price : " + PrecisionUtil.round(totalPrice));
		totalPrice = PrecisionUtil.round(totalPrice * quantity);
		LOG.info("Total Price for quantity " + quantity + " : " + totalPrice + System.lineSeparator());

		final String currencyIso = basePriceVal.getCurrencyIso();
		return new BcfPriceInfo(currencyIso, basePriceVal.getValue(), marginValue, discountValues,
				costValue, hiddenTaxValues, netPriceVal, gstTaxValue, totalPrice,quantity);
	}



	private BcfPriceInfo getBcfPriceInfo(final RoomRateProductModel product, final long quantity, final PriceValue basePriceVal,
			final BcfPriceValue costToBcfPriceVal, final PriceValue marginPriceValue, final List<DiscountValue> discountValues,
			final List<DiscountValue> entryLevelDiscountValues, final LocationModel location)
	{
		return getAccommodationBcfPriceInfo(product, quantity, basePriceVal, costToBcfPriceVal, marginPriceValue, discountValues,
				entryLevelDiscountValues, location);

	}

	private BcfPriceInfo getBcfPriceInfo(final ExtraGuestOccupancyProductModel product, final long quantity, final PriceValue basePriceVal,
			final BcfPriceValue costToBcfPriceVal, final PriceValue marginPriceValue, final List<DiscountValue> discountValues,
			final List<DiscountValue> entryLevelDiscountValues, final LocationModel location)
	{
		return getAccommodationBcfPriceInfo(product, quantity, basePriceVal, costToBcfPriceVal, marginPriceValue, discountValues,
				entryLevelDiscountValues, location);

	}

	private BcfPriceInfo getAccommodationBcfPriceInfo(final ProductModel product, final long quantity,
			final PriceValue basePriceVal, final BcfPriceValue costToBcfPriceVal, final PriceValue marginPriceValue,
			final List<DiscountValue> discountValues, final List<DiscountValue> entryLevelDiscountValues,
			final LocationModel location)
	{
		final double marginValue = Objects.isNull(marginPriceValue) ? 0 : PrecisionUtil.round(marginPriceValue.getValue());
		final double discountVal = CollectionUtils.isEmpty(discountValues) ?
				0 :
				PrecisionUtil.round(discountValues.stream().filter(Objects::nonNull).mapToDouble(DiscountValue::getValue).sum());

		final double entryLevelDiscountVal = CollectionUtils.isEmpty(entryLevelDiscountValues) ?
				0 :
				PrecisionUtil.round(entryLevelDiscountValues.stream().filter(Objects::nonNull).mapToDouble(DiscountValue::getValue).sum());

		final double totalDiscountVal = discountVal + entryLevelDiscountVal;

		final double costValue = Objects.isNull(costToBcfPriceVal) ? 0 : PrecisionUtil.round(costToBcfPriceVal.getValue());
		final List<TaxValue> taxValues = Objects.isNull(costToBcfPriceVal) ? new ArrayList<>() : costToBcfPriceVal.getTaxValues();
		LOG.info("Margin Value : " + marginValue);
		LOG.info("Discount Value(rate plan) : " + discountVal);


		final double newBasePriceVal = PrecisionUtil.round(costValue + marginValue - discountVal);
		LOG.info("New Base Value : " + newBasePriceVal);

		LOG.info("Discount Value(entry-level) : " + entryLevelDiscountVal);

		final double netPriceVal = PrecisionUtil.round(costValue + marginValue - totalDiscountVal);
		LOG.info("Net Price : " + netPriceVal);
		final List<TaxValue> hiddenTaxValues = taxValues;

		TaxValue gstTaxValue = null;
		double totalPrice = netPriceVal;
		if (location != null)
		{
			final List<String> applicableTaxCodes = getVacationTaxCalculationStrategy().getApplicableTaxCodes(product, location);
			if (applicableTaxCodes.contains(BcfCoreConstants.GST))
			{
				final List<TaxValue> gstTaxValues = getVacationTaxCalculationStrategy()
						.getTaxValues(Collections.singletonList(BcfCoreConstants.GST), netPriceVal, location, false);
				gstTaxValue = gstTaxValues.stream().findFirst().orElse(null);
			}
			if (gstTaxValue != null)
			{
				LOG.info("Final GstTaxValue for location " + location.getCode() + " : " + gstTaxValue.getValue());
				totalPrice = totalPrice + gstTaxValue.getValue();
			}
		}
		LOG.info("Total Price : " + PrecisionUtil.round(totalPrice));
		totalPrice = PrecisionUtil.round(totalPrice * quantity);
		LOG.info("Total Price for quantity " + quantity + " : " + totalPrice + System.lineSeparator());

		final String currencyIso = basePriceVal.getCurrencyIso();
		return new BcfPriceInfo(currencyIso, newBasePriceVal, marginValue,
				Stream.concat(discountValues.stream(), entryLevelDiscountValues.stream()).collect(
						Collectors.toList()),
				costValue, hiddenTaxValues, netPriceVal, gstTaxValue, totalPrice,quantity);
	}


	/**
	 * @return the roomRatePriceCalculationStrategy
	 */
	protected RoomRatePriceCalculationStrategy getRoomRatePriceCalculationStrategy()
	{
		return roomRatePriceCalculationStrategy;
	}

	/**
	 * @param roomRatePriceCalculationStrategy the roomRatePriceCalculationStrategy to set
	 */
	@Required
	public void setRoomRatePriceCalculationStrategy(final RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy)
	{
		this.roomRatePriceCalculationStrategy = roomRatePriceCalculationStrategy;
	}

	/**
	 * @return the extraOccupancyPriceCalculationStrategy
	 */
	protected ExtraOccupancyPriceCalculationStrategy getExtraOccupancyPriceCalculationStrategy()
	{
		return extraOccupancyPriceCalculationStrategy;
	}

	/**
	 * @param extraOccupancyPriceCalculationStrategy the extraOccupancyPriceCalculationStrategy to set
	 */
	@Required
	public void setExtraOccupancyPriceCalculationStrategy(
			final ExtraOccupancyPriceCalculationStrategy extraOccupancyPriceCalculationStrategy)
	{
		this.extraOccupancyPriceCalculationStrategy = extraOccupancyPriceCalculationStrategy;
	}

	/**
	 * @return extraProductPriceCalculationStrategy
	 */
	protected ExtraProductPriceCalculationStrategy getExtraProductPriceCalculationStrategy()
	{
		return extraProductPriceCalculationStrategy;
	}

	/**
	 * @param extraProductPriceCalculationStrategy
	 */
	@Required
	public void setExtraProductPriceCalculationStrategy(
			final ExtraProductPriceCalculationStrategy extraProductPriceCalculationStrategy)
	{
		this.extraProductPriceCalculationStrategy = extraProductPriceCalculationStrategy;
	}

	/**
	 * @return the activityPriceCalculationStrategy
	 */
	protected ActivityPriceCalculationStrategy getActivityPriceCalculationStrategy()
	{
		return activityPriceCalculationStrategy;
	}

	/**
	 * @param activityPriceCalculationStrategy the activityPriceCalculationStrategy to set
	 */
	@Required
	public void setActivityPriceCalculationStrategy(final ActivityPriceCalculationStrategy activityPriceCalculationStrategy)
	{
		this.activityPriceCalculationStrategy = activityPriceCalculationStrategy;
	}

	/**
	 * @return the vacationPriceCalculationStrategy
	 */
	protected VacationPriceCalculationStrategy getVacationPriceCalculationStrategy()
	{
		return vacationPriceCalculationStrategy;
	}

	/**
	 * @param vacationPriceCalculationStrategy the vacationPriceCalculationStrategy to set
	 */
	@Required
	public void setVacationPriceCalculationStrategy(final VacationPriceCalculationStrategy vacationPriceCalculationStrategy)
	{
		this.vacationPriceCalculationStrategy = vacationPriceCalculationStrategy;
	}

	/**
	 * @return the vacationDiscountCalculationStrategy
	 */
	protected VacationDiscountCalculationStrategy getVacationDiscountCalculationStrategy()
	{
		return vacationDiscountCalculationStrategy;
	}

	/**
	 * @param vacationDiscountCalculationStrategy the vacationDiscountCalculationStrategy to set
	 */
	@Required
	public void setVacationDiscountCalculationStrategy(
			final VacationDiscountCalculationStrategy vacationDiscountCalculationStrategy)
	{
		this.vacationDiscountCalculationStrategy = vacationDiscountCalculationStrategy;
	}

	/**
	 * @return the vacationTaxCalculationStrategy
	 */
	protected VacationTaxCalculationStrategy getVacationTaxCalculationStrategy()
	{
		return vacationTaxCalculationStrategy;
	}

	/**
	 * @param vacationTaxCalculationStrategy the vacationTaxCalculationStrategy to set
	 */
	@Required
	public void setVacationTaxCalculationStrategy(final VacationTaxCalculationStrategy vacationTaxCalculationStrategy)
	{
		this.vacationTaxCalculationStrategy = vacationTaxCalculationStrategy;
	}

	/**
	 * @return the bcfConfigurablePropertiesService
	 */
	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	/**
	 * @param bcfConfigurablePropertiesService the bcfConfigurablePropertiesService to set
	 */
	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	/**
	 * @return the vacationCostToBcfCalculationStrategy
	 */
	protected VacationCostToBcfCalculationStrategy getVacationCostToBcfCalculationStrategy()
	{
		return vacationCostToBcfCalculationStrategy;
	}

	/**
	 * @param vacationCostToBcfCalculationStrategy the vacationCostToBcfCalculationStrategy to set
	 */
	@Required
	public void setVacationCostToBcfCalculationStrategy(
			final VacationCostToBcfCalculationStrategy vacationCostToBcfCalculationStrategy)
	{
		this.vacationCostToBcfCalculationStrategy = vacationCostToBcfCalculationStrategy;
	}
}
