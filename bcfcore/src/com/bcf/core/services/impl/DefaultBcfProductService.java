/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.impl;

import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.BcfProductDao;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.services.BcfProductService;


public class DefaultBcfProductService extends DefaultProductService implements BcfProductService
{

	private BcfProductDao bcfProductDao;
	private GenericDao<FeeProductModel> feeProductGenericDao;

	@Override
	public FareProductModel getFareProductForFareBasisCode(final String fareBasisCode, final FareProductType fareProductType)
	{
		return getBcfProductDao().findFareProductForFareBasisCode(fareBasisCode, fareProductType);
	}

	@Override
	public List<AncillaryProductModel> getAncillaryProductsForCodes(final List<String> productCodes)
	{
		if (CollectionUtils.isNotEmpty(productCodes))
		{
			return getBcfProductDao().findAncillaryProductsForCodes(productCodes);
		}

		return Collections.emptyList();
	}

	@Override
	public List<FeeProductModel> getAllFeeProducts()
	{
		return getFeeProductGenericDao().find();
	}

	protected BcfProductDao getBcfProductDao()
	{
		return bcfProductDao;
	}

	@Required
	public void setBcfProductDao(final BcfProductDao bcfProductDao)
	{
		this.bcfProductDao = bcfProductDao;
	}

	protected GenericDao<FeeProductModel> getFeeProductGenericDao()
	{
		return feeProductGenericDao;
	}

	@Required
	public void setFeeProductGenericDao(
			final GenericDao<FeeProductModel> feeProductGenericDao)
	{
		this.feeProductGenericDao = feeProductGenericDao;
	}
}
