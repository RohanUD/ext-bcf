package com.bcf.core.services.matomo;

import com.bcf.core.model.MatomoModel;


public interface MatomoService
{
	MatomoModel getMatomoScript();
}
