package com.bcf.core.services.matomo.impl;

import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.matomo.MatomoDao;
import com.bcf.core.model.MatomoModel;
import com.bcf.core.services.matomo.MatomoService;


public class MatomoServiceImpl implements MatomoService
{
	private MatomoDao matomoDao;

	@Override
	public MatomoModel getMatomoScript()
	{
		return getMatomoDao().getMatomoScript();
	}

	public MatomoDao getMatomoDao()
	{
		return matomoDao;
	}

	@Required
	public void setMatomoDao(final MatomoDao matomoDao)
	{
		this.matomoDao = matomoDao;
	}
}
