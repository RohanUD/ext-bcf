/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.servicenotice.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.dao.servicenotice.dao.ServiceNoticesDao;
import com.bcf.core.model.BusinessOpportunityModel;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.ElectronicNewsLetterModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;
import com.bcf.core.services.servicenotice.ServiceNoticesService;


public class ServiceNoticesServiceImpl implements ServiceNoticesService
{

	public static final String SERVICE_NOTICE_COUNT_KEY = "serviceNoticeCount";
	public static final String NEWS_RELEASE_COUNT_KEY = "newsReleaseCount";
	public static final String BUSINESS_OPPORTUNITY_COUNT_KEY = "businessOpportunityCount";
	public static final String ELECTRONIC_NEWS_LETTER_COUNT_KEY = "electronicNewsLetterCount";

	@Resource(name = "serviceNoticesDao")
	private ServiceNoticesDao serviceNoticesDao;

	@Override
	public ServiceNoticeModel getServiceNoticeForCode(final String serviceNoticeCode)
	{
		return serviceNoticesDao.findServiceNoticeForCode(serviceNoticeCode);
	}

	@Override
	public List<ServiceNoticeModel> getServiceNoticesForAllRoutes()
	{
		return serviceNoticesDao.findServiceNotices();
	}

	@Override
	public List<NewsModel> findNews()
	{
		return serviceNoticesDao.findNews();
	}

	@Override
	public List<CompetitionModel> findCompetitions()
	{
		return serviceNoticesDao.findCompetitions();
	}

	@Override
	public List<SubstantialPerformanceNoticeModel> findSubstantialPerformanceNotices()
	{
		return serviceNoticesDao.findSubstantialPerformanceNotices();
	}

	@Override
	public CompetitionModel findCompetitionForCode(final String code)
	{
		return serviceNoticesDao.findCompetitionForCode(code);
	}

	@Override
	public SubstantialPerformanceNoticeModel findSubstantialPerformanceNoticeForCode(final String code)
	{
		return serviceNoticesDao.findSubstantialPerformanceNoticeForCode(code);
	}

	@Override
	public List<ServiceNoticeModel> getServiceNoticesForRoute(final String sourceTerminalCode, final String destinationServiceCode)
	{

		final List<ServiceNoticeModel> serviceNoticesForAllRoutes = getServiceNoticesForAllRoutes();
		final List<ServiceNoticeModel> serviceNotices = new ArrayList<>();
		for (final ServiceNoticeModel serviceNoticeModel : serviceNoticesForAllRoutes)
		{
			for (final TravelRouteModel travelRouteModel : serviceNoticeModel.getAffectedRoutes())
			{
				if (StringUtils.equalsIgnoreCase(travelRouteModel.getOrigin().getCode(), sourceTerminalCode) &&
						StringUtils.equalsIgnoreCase(travelRouteModel.getDestination().getCode(), destinationServiceCode))
				{
					serviceNotices.add(serviceNoticeModel);
					break;
				}
			}
		}
		return serviceNotices;
	}

	@Override
	public SearchPageData<ServiceNoticeModel> getPaginatedServiceNoticesForAllRoutes(final PageableData paginationData)
	{
		return serviceNoticesDao.findPaginatedServiceNotices(paginationData);
	}

	@Override
	public SearchPageData<NewsModel> getPaginatedAllNewsRelease(final PageableData pageableData)
	{
		return serviceNoticesDao.findPaginatedNewsRelease(pageableData);
	}

	@Override
	public List<ServiceNoticeModel> getNotPublishedServiceNotices()
	{
		return serviceNoticesDao.findNonPublishedServiceNotices();
	}

	@Override
	public Map<String, Long> getServiceAndNewsCount()
	{
		final Map<String, Long> countMap = new HashMap<>();
		final List<ServiceNoticeModel> serviceAndNewsModels = serviceNoticesDao.findPublishedServiceNoticeAndNewsRelease();

		countMap.put(SERVICE_NOTICE_COUNT_KEY,
				serviceAndNewsModels.stream().filter(ServiceModel.class::isInstance).count());
		countMap.put(NEWS_RELEASE_COUNT_KEY,
				serviceAndNewsModels.stream().filter(NewsModel.class::isInstance).count());
		countMap.put(BUSINESS_OPPORTUNITY_COUNT_KEY,
				serviceAndNewsModels.stream().filter(BusinessOpportunityModel.class::isInstance).count());
		countMap.put(ELECTRONIC_NEWS_LETTER_COUNT_KEY,
				serviceAndNewsModels.stream().filter(ElectronicNewsLetterModel.class::isInstance).count());
		return countMap;
	}

	@Override
	public List<ServiceNoticeModel> getExpiredServiceNotices()
	{
		return serviceNoticesDao.findLiveButExpiredServiceNotices();
	}

	public void setServiceNoticesDao(final ServiceNoticesDao serviceNoticesDao)
	{
		this.serviceNoticesDao = serviceNoticesDao;
	}
}
