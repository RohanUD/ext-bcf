/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.servicenotice;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import java.util.List;
import java.util.Map;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;


public interface ServiceNoticesService
{
	ServiceNoticeModel getServiceNoticeForCode(final String serviceNoticeCode);

	List<ServiceNoticeModel> getServiceNoticesForAllRoutes();

	List<ServiceNoticeModel> getServiceNoticesForRoute(String sourceTerminalCode, String destinationServiceCode);

	List<ServiceNoticeModel> getNotPublishedServiceNotices();

	SearchPageData<ServiceNoticeModel> getPaginatedServiceNoticesForAllRoutes(PageableData paginationData);

	Map<String, Long> getServiceAndNewsCount();

	List<ServiceNoticeModel> getExpiredServiceNotices();

	SearchPageData<NewsModel> getPaginatedAllNewsRelease(PageableData pageableData);

	List<NewsModel> findNews();

	List<CompetitionModel> findCompetitions();

	List<SubstantialPerformanceNoticeModel> findSubstantialPerformanceNotices();

	CompetitionModel findCompetitionForCode(String code);

	SubstantialPerformanceNoticeModel findSubstantialPerformanceNoticeForCode(String code);
}
