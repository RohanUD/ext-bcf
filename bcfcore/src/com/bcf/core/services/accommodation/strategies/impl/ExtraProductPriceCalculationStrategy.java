/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.accommodation.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import de.hybris.platform.util.PriceValue;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;


public class ExtraProductPriceCalculationStrategy extends VacationPriceCalculationStrategy implements FindBcfPriceInfoStrategy
{
	public PriceValue getBasePrice(final ExtraProductModel extraProduct)
	{
		final List<PriceRowModel> priceRows = getPriceRowService().getPriceRow(extraProduct);
		if (CollectionUtils.isNotEmpty(priceRows))
		{
			final PriceRowModel priceRow = priceRows.stream().findFirst().get();
			final double basePrice = PrecisionUtil.round(priceRow.getPrice());
			return new PriceValue(priceRow.getCurrency().getIsocode(), basePrice, true);
		}

		return null;
	}

	@Override
	public BcfPriceInfo findBcfPriceInfo(final AbstractOrderEntryModel entry)
	{
		final BcfPriceInfo bcfPriceInfo = getBcfTravelCommercePriceService()
				.getBcfPriceInfo((ExtraProductModel) entry.getProduct(), entry.getQuantity(), getLocation(entry));

		if (Objects.nonNull(bcfPriceInfo))
		{
			return bcfPriceInfo;
		}

		return getDefaultPriceRow(entry);
	}
}
