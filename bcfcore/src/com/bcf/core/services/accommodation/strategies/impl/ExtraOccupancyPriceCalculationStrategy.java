/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.accommodation.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import de.hybris.platform.util.PriceValue;
import java.util.Date;
import java.util.Objects;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class ExtraOccupancyPriceCalculationStrategy extends VacationPriceCalculationStrategy implements FindBcfPriceInfoStrategy
{

	@Override
	public BcfPriceInfo findBcfPriceInfo(final AbstractOrderEntryModel entry)
	{
		final AccommodationOrderEntryInfoModel accommodationOrderEntryInfoModel = entry.getAccommodationOrderEntryInfo();

		return getBcfTravelCommercePriceService()
				.getBcfPriceInfo(entry,(ExtraGuestOccupancyProductModel) entry.getProduct(), entry.getQuantity(),
						accommodationOrderEntryInfoModel.getPassengerType(),
						getRatePlan(entry), getLocation(entry), accommodationOrderEntryInfoModel.getDates().get(0));

	}


	public PriceValue getBasePrice(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct,
			final String passengerType, final Date stayDate)
	{
		final PriceRowModel priceRow = getPriceRowService().getPriceRow(extraGuestOccupancyProduct, passengerType, stayDate);

		if (Objects.isNull(priceRow) || Objects.isNull(priceRow.getPrice()))
		{
			return null;
		}

		final double basePrice = PrecisionUtil.round(priceRow.getPrice());
		return new PriceValue(priceRow.getCurrency().getIsocode(), basePrice, true);
	}



}
