/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.accommodation.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.util.PriceValue;
import java.util.Objects;
import com.bcf.core.services.accommodation.bcfpriceinfo.strategies.FindBcfPriceInfoStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;


public class RoomRatePriceCalculationStrategy extends VacationPriceCalculationStrategy
		implements FindBcfPriceInfoStrategy
{
	/**
	 * Gets the base price.
	 *
	 * @param roomRateProduct the room rate product
	 * @param quantity        the quantity
	 * @return the base price
	 */
	public PriceValue getBasePrice(final RoomRateProductModel roomRateProduct, final long quantity)
	{
		PriceRowModel priceRow = getPriceRowService().getPriceRow(roomRateProduct, quantity);
		if (Objects.isNull(priceRow) || Objects.isNull(priceRow.getPrice()))
		{
			priceRow = getPriceRowService().getPriceRow(roomRateProduct).stream().findFirst().orElse(null);
		}
		if (Objects.isNull(priceRow) || Objects.isNull(priceRow.getPrice()))
		{
			return null;
		}

		final double basePrice = PrecisionUtil.round(priceRow.getPrice());
		return new PriceValue(priceRow.getCurrency().getIsocode(), basePrice, true);
	}

	@Override
	public BcfPriceInfo findBcfPriceInfo(final AbstractOrderEntryModel entry)
	{
		final ProductModel product = entry.getProduct();
		return getBcfTravelCommercePriceService()
				.getBcfPriceInfo(entry,(RoomRateProductModel) product, entry.getQuantity(), getRatePlan(entry), getLocation(entry));
	}
}
