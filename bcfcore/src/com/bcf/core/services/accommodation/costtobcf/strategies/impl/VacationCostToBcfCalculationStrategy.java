/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.accommodation.costtobcf.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.accommodation.costtobcf.strategies.FindCostToBcfStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.services.vacation.tax.strategies.impl.VacationTaxCalculationStrategy;


public class VacationCostToBcfCalculationStrategy extends VacationPriceCalculationStrategy implements FindCostToBcfStrategy
{
	VacationTaxCalculationStrategy vacationTaxCalculationStrategy;

	@Override
	public PriceValue findCostToBcfValues(final AbstractOrderEntryModel entry)
	{
		if (Objects.isNull(entry.getBasePrice()))
		{
			return null;
		}
		final LocationModel location = getLocation(entry);
		return getCostToBcfValues(entry.getProduct(), entry.getBasePrice(), location);
	}

	/**
	 * Gets the cost to bcf values.
	 *
	 * @param product   the product
	 * @param basePrice the base price
	 * @param location  the location
	 * @return the cost to bcf values
	 */
	public PriceValue getCostToBcfValues(final ProductModel product, final double basePrice, final LocationModel location)
	{
		final List<TaxValue> taxValues = getVacationTaxCalculationStrategy().getTaxValues(product, basePrice, location);
		final double taxValue = taxValues.stream().mapToDouble(TaxValue::getValue).sum();
		final double productNetPrice = basePrice + taxValue;

		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		return new PriceValue(currencyIsoCode, productNetPrice, true);
	}

	/**
	 * @return the vacationTaxCalculationStrategy
	 */
	protected VacationTaxCalculationStrategy getVacationTaxCalculationStrategy()
	{
		return vacationTaxCalculationStrategy;
	}

	/**
	 * @param vacationTaxCalculationStrategy the vacationTaxCalculationStrategy to set
	 */
	@Required
	public void setVacationTaxCalculationStrategy(final VacationTaxCalculationStrategy vacationTaxCalculationStrategy)
	{
		this.vacationTaxCalculationStrategy = vacationTaxCalculationStrategy;
	}

}
