/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.accommodation.discount.strategies.impl;

import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.util.DiscountValue;
import java.util.Objects;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;


public class VacationDiscountCalculationStrategy extends VacationPriceCalculationStrategy
{
	/**
	 * Gets the discount.
	 *
	 * @param ratePlan        the rate plan
	 * @param costToBcf       the cost to bcf
	 * @param currencyIsoCode the currency iso code
	 * @return the discount
	 */
	public DiscountValue getDiscount(final RatePlanModel ratePlan, final Double costToBcf, final String currencyIsoCode)
	{
		if (Objects.nonNull(ratePlan) && Objects.nonNull(ratePlan.getDiscount()) && ratePlan.getDiscount() > 0)
		{
			final DiscountType discountType = ratePlan.getDiscountType();
			final String discountCode = "DISCOUNT_" + ratePlan.getCode() + "_" + ratePlan.getDiscount() + "_" + discountType;
			final double discountVal = Objects.equals(DiscountType.PERCENT, discountType)
					? (costToBcf * ratePlan.getDiscount() / 100)
					: ratePlan.getDiscount();
			return new DiscountValue(discountCode, discountVal, true, currencyIsoCode);
		}
		return null;
	}
}
