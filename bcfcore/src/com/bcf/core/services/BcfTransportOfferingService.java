/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.TransportOfferingService;
import java.util.Date;
import java.util.List;


public interface BcfTransportOfferingService extends TransportOfferingService
{
	TransportOfferingModel getTransportOfferings(Date departureTime, Date arrivalTime,
			TravelSectorModel sector);

	TransportOfferingModel getDefaultTransportOfferingForSector(String travelSector);

	List<TransportOfferingModel> getTransportOfferingsForTransferIdentifier(String transferIdentifier);
	List<TransportOfferingModel> getTransportOfferingsForSailingCode(String sailingCode);

	TransportOfferingModel getTransportOfferingForSailingCodeAndSector(String eBookingCode, String travelSector);

	TransportOfferingModel getDefaultTransportOfferingByRoute(String travelRouteCode);

	List<TransportOfferingModel> getDefaultTransportOfferingBySector(final String travelSectorCode);

	List<AbstractOrderEntryModel> getModifiedOrderEntriesForOfferingCode(String offeringCode);
}
