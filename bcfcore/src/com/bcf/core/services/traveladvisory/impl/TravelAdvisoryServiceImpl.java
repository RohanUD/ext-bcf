/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.services.traveladvisory.impl;

import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.traveladvisory.TravelAdvisoryDao;
import com.bcf.core.enums.TravelAdvisoryDisplayType;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.StreamUtil;


public class TravelAdvisoryServiceImpl implements TravelAdvisoryService
{
	private TravelAdvisoryDao travelAdvisoryDao;
	private BCFTravelRouteService bcfTravelRouteService;

	@Override
	public List<TravelAdvisoryModel> findTravelAdvisoriesToPublish()
	{
		return getTravelAdvisoryDao().findTravelAdvisoriesToPublish();
	}

	@Override
	public List<TravelAdvisoryModel> findExpiredTravelAdvisories()
	{
		return getTravelAdvisoryDao().findExpiredTravelAdvisories();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisories()
	{
		return getTravelAdvisoryDao().findActiveTravelAdvisoriesByStatus(TravelAdvisoryStatus.LIVE);
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesForHomePageByRouteRegion(final Optional<String> departureLocation,
			final Optional<String> arrivalLocation)
	{
		final List<TravelAdvisoryModel> activeTravelAdvisoriesByStatus = getTravelAdvisoryDao()
				.findActiveTravelAdvisoriesByStatus(TravelAdvisoryStatus.LIVE);
		return getFilteredTravelAdvisoriesByRouteRegion(activeTravelAdvisoriesByStatus, departureLocation, arrivalLocation);
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion()
	{
		return getTravelAdvisoryDao().findActiveTravelAdvisoriesByStatusAndDisplayTypeWithNoRouteRegion(TravelAdvisoryStatus.LIVE,
				TravelAdvisoryDisplayType.SITE_WIDE_TAKEOVER);
	}

	@Override
	public Optional<TravelAdvisoryModel> findHomePageTakeOverAdvisory()
	{
		return StreamUtil.safeStream(getTravelAdvisoryDao().findActiveTravelAdvisoriesByStatusAndDisplayTypeWithNoRouteRegion(
				TravelAdvisoryStatus.LIVE, TravelAdvisoryDisplayType.HOME_PAGE_TAKEOVER))
				.findAny();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesForHomePageWithoutRouteRegion()
	{
		return getTravelAdvisoryDao()
				.findActiveTravelAdvisoriesByStatusAndInverseDisplayTypeWithNoRouteRegion(TravelAdvisoryStatus.LIVE,
						TravelAdvisoryDisplayType.RIBBON_ONLY);
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesForOtherPagesByRouteRegion(final Optional<String> departureLocation,
			final Optional<String> arrivalLocation)
	{
		final List<TravelAdvisoryModel> activeTravelAdvisoriesForOtherPages = getTravelAdvisoryDao()
				.findActiveTravelAdvisoriesByStatusAndInverseDisplayType(TravelAdvisoryStatus.LIVE,
						TravelAdvisoryDisplayType.HOME_PAGE_TAKEOVER);
		return getFilteredTravelAdvisoriesByRouteRegion(activeTravelAdvisoriesForOtherPages, departureLocation, arrivalLocation);
	}

	@Override
	public Optional<TravelAdvisoryModel> getTravelAdvisoryByCode(final String code)
	{
		return getTravelAdvisoryDao().findTravelAdvisoryByCode(code);
	}

	@Override
	public String getRoutesAtGlanceContent()
	{
		return getTravelAdvisoryDao().getRoutesAtGlanceContent();
	}

	private List<TravelAdvisoryModel> getFilteredTravelAdvisoriesByRouteRegion(
			final List<TravelAdvisoryModel> travelAdvisoryModels, final Optional<String> departureLocation,
			final Optional<String> arrivalLocation)
	{
		if (departureLocation.isPresent() && arrivalLocation.isPresent())
		{
			return StreamUtil.safeStream(travelAdvisoryModels)
					.filter(applyRouteRegionFilter(getSessionTravelRoute(departureLocation.get(), arrivalLocation.get())))
					.collect(Collectors.toList());
		}
		return StreamUtil.safeStream(travelAdvisoryModels).filter(this::filterTravelAdvisoriesWithNoRouteRegion)
				.collect(Collectors.toList());
	}

	private Optional<TravelRouteModel> getSessionTravelRoute(final String departureLocation,
			final String arrivalLocation)
	{
		return StreamUtil
				.safeStream(getBcfTravelRouteService().getDirectTravelRoutes(departureLocation, arrivalLocation))
				.findAny();
	}

	private Predicate<TravelAdvisoryModel> applyRouteRegionFilter(final Optional<TravelRouteModel> travelRouteInSession)
	{
		return travelAdvisoryModel -> filterTravelAdvisoriesByMatchingRouteRegion(travelAdvisoryModel, travelRouteInSession);
	}

	private boolean filterTravelAdvisoriesByMatchingRouteRegion(final TravelAdvisoryModel travelAdvisoryModel,
			final Optional<TravelRouteModel> sessionTravelRouteOpt)
	{
		if (!sessionTravelRouteOpt.isPresent())
		{
			return false;
		}
		final boolean routeRegionEmpty = isRouteRegionEmpty(travelAdvisoryModel);
		final boolean travelRouteMatched = isTravelRouteMatched(travelAdvisoryModel, sessionTravelRouteOpt.get());
		final boolean regionMatched = isRegionMatched(travelAdvisoryModel, sessionTravelRouteOpt.get());
		return routeRegionEmpty || travelRouteMatched || regionMatched;
	}

	private boolean isRouteRegionEmpty(final TravelAdvisoryModel travelAdvisoryModel)
	{
		return CollectionUtils.isEmpty(travelAdvisoryModel.getRoutes()) && CollectionUtils
				.isEmpty(travelAdvisoryModel.getRegions());
	}

	private boolean isTravelRouteMatched(final TravelAdvisoryModel travelAdvisoryModel,
			final TravelRouteModel travelRouteInSession)
	{
		return StreamUtil.safeStream(travelAdvisoryModel.getRoutes())
				.anyMatch(travelRouteModel -> travelRouteModel.getCode().equals(travelRouteInSession.getCode()));
	}

	private boolean isRegionMatched(final TravelAdvisoryModel travelAdvisoryModel,
			final TravelRouteModel travelRouteInSession)
	{
		return StreamUtil.safeStream(travelAdvisoryModel.getRegions())
				.anyMatch(routeRegion -> routeRegion.equals(travelRouteInSession.getRouteRegion()));
	}

	private boolean filterTravelAdvisoriesWithNoRouteRegion(final TravelAdvisoryModel travelAdvisoryModel)
	{
		return CollectionUtils.isEmpty(travelAdvisoryModel.getRoutes()) && CollectionUtils
				.isEmpty(travelAdvisoryModel.getRegions());
	}

	@Required
	public void setTravelAdvisoryDao(final TravelAdvisoryDao travelAdvisoryDao)
	{
		this.travelAdvisoryDao = travelAdvisoryDao;
	}

	public TravelAdvisoryDao getTravelAdvisoryDao()
	{
		return travelAdvisoryDao;
	}

	public BCFTravelRouteService getBcfTravelRouteService()
	{
		return bcfTravelRouteService;
	}

	@Required
	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		this.bcfTravelRouteService = bcfTravelRouteService;
	}
}

