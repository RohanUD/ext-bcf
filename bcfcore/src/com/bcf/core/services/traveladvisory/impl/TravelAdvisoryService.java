/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.services.traveladvisory.impl;

import java.util.List;
import java.util.Optional;
import com.bcf.core.model.TravelAdvisoryModel;


public interface TravelAdvisoryService
{
	List<TravelAdvisoryModel> findTravelAdvisoriesToPublish();

	List<TravelAdvisoryModel> findExpiredTravelAdvisories();

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesForHomePageByRouteRegion(Optional<String> departureLocation,
			Optional<String> arrivalLocation);

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion();

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesForHomePageWithoutRouteRegion();

	List<TravelAdvisoryModel> findActiveTravelAdvisories();

	Optional<TravelAdvisoryModel> getTravelAdvisoryByCode(String code);

	String getRoutesAtGlanceContent();

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesForOtherPagesByRouteRegion(Optional<String> departureLocation,
			Optional<String> arrivalLocation);

	Optional<TravelAdvisoryModel> findHomePageTakeOverAdvisory();
}
