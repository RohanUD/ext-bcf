/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.core.model.user.CustomerModel;
import java.util.Collection;
import java.util.List;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.facades.customer.data.BusinessOpportunitiesSubscriptionsData;


public interface BCFCustomerSubscriptionService
{
	Collection<CustomerSubscriptionModel> getAllCustomerSubscriptions();

	boolean createOffersAndPromotionsSubscriptions(CustomerModel currentUser, boolean offersAndPromotions);

	boolean createNewsReleaseSubscriptions(CustomerModel currentUser, boolean serviceNotice);

	void saveCustomerSubscription(CustomerModel currentCustomerModel, final List<String> optInSubscriptionCodes);

	void unsubscribeAll(CustomerModel customerModel);

	void unsubscribeByNames(String unsubscribeNames);

	boolean createBusinessOpportunitiesSubscriptions(CustomerModel currentUser,
			BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData);

	void unsubscribeByCodes(final CustomerModel currentUser, String unsubscribeCodes);
}
