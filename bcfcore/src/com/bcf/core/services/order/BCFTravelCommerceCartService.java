/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.order.TravelCommerceCartService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.bcf.core.entrygroup.EntryGroupData;


public interface BCFTravelCommerceCartService extends TravelCommerceCartService
{
	/**
	 * Cleans up the cart from bundles before a new bundle addition. It removes all the entries of type TRANSPORT with
	 * originDestinationRefNumber greater than the given odRefNum.
	 *
	 * @param odRefNum as the minimum originDestinationRefNumber
	 */
	void removeCartEntriesForMinODRefNumber(Integer journeyRefNumber, Integer odRefNum) throws ModelRemovalException;

	void persistAdditionalPropertiesForCartEntry(CartModel masterCartModel, int orderEntryNo, ProductModel product,
			Map<String, Map<String, Object>> propertiesMap);

	void removeAllTravelOrderEntries(List<AbstractOrderEntryModel> abstractOrderEntries);

	void createOrderGroup(EntryGroupData entryGroupData, CartModel cartModel);

	EntryGroupData createEntryGroupWithAccommodation(List<Integer> entryNumbers, List<AccommodationOrderEntryGroupModel> accommodationEntryGroups, Integer journeyRefNumber);

	void createOrUpdateOrderGroup(EntryGroupData entryGroupData, CartModel cartModel);

	/**
	 * Gets the accommodation order entry group.
	 *
	 * @param abstractOrderEntry
	 *           the abstract order entry
	 * @return the accommodation order entry group
	 */
	AccommodationOrderEntryGroupModel getAccommodationOrderEntryGroup(AbstractOrderEntryModel abstractOrderEntry);

	EntryGroupData createEntryGroupForPromotions(List<Integer> entryNumbers, List<AccommodationOrderEntryGroupModel> accommodationEntryGroups, Integer journeyRefNumber, String promotionCode);

	double getPriceFromListSailingSessionMap(Integer originDestinationRefNumber, String code);

	CartModel getCartForCodeAndSite(String code, BaseSiteModel site);

	public Set<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(
			final List<AbstractOrderEntryModel> abstractOrderEntries);

}
