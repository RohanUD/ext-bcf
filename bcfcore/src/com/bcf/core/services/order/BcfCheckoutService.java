/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.travelservices.order.TravelCommerceCheckoutService;
import java.util.List;
import com.bcf.facades.ebooking.ConfirmBookingServiceSuccessData;


public interface BcfCheckoutService extends TravelCommerceCheckoutService
{
	void createHistorySnapshot(final OrderModel order);

	void postProcessOrder(final CartModel cartModel, final OrderModel orderModel);

	boolean updateCartWithEbookingDetails(ConfirmBookingServiceSuccessData confirmBookingServiceSuccessData,
			final CartModel cartModel);

	boolean checkSailingTimeThresholdOverrideApplicable();

	void saveAgentComment(String agentComment);

	void saveReferenceCodes(List<String> referenceCodes, List<String> bookingRefs);

	void updateAmountToPay(AbstractOrderModel orderModel);
}
