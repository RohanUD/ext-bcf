/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.order.impl.TravelBundleCommerceAddToCartStrategy;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.FareDetailModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.services.order.BCFCommerceAddToCartStrategy;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.ExternalPriceMapData;
import com.bcf.facades.cart.ExternalPriceTaxes;
import com.bcf.facades.cart.FareDetail;


public class BcfBundleCommerceAddToCartStrategy extends TravelBundleCommerceAddToCartStrategy implements
		BCFCommerceAddToCartStrategy
{
	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final CartModel cartModel = parameter.getCart();
		final CommerceCartModification modification = doAddToCart(parameter);
		normalizeEntryNumbersIfNeeded(cartModel);
		final String sessionBookingJourneyType=getSessionService().getAttribute(TravelservicesConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.equals(sessionBookingJourneyType, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode())
				|| (Objects.nonNull(getSessionService().getAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY))
				&& Boolean.TRUE.equals(getSessionService().getAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY))))
		{
			updateEntryWithExternalResponse(modification.getEntry());
			updateCurrentJourneyRefNum(cartModel, modification.getEntry());
			parameter.setCart(cartModel);
		}

		afterAddToCart(parameter, modification);

		return modification;
	}

	protected void updateCurrentJourneyRefNum(final AbstractOrderModel abstractOrder, final AbstractOrderEntryModel entry)
	{
		if (abstractOrder.getCurrentJourneyRefNum() < entry.getJourneyReferenceNumber())
		{
			abstractOrder.setCurrentJourneyRefNum(entry.getJourneyReferenceNumber());
		}
	}

	/**
	 * updates a cart entry with make booking response
	 */
	protected void updateEntryWithExternalResponse(final AbstractOrderEntryModel entry) throws CommerceCartModificationException
	{
		final Map<String, Object> makeBookingResponseMap = getSessionService().getAttribute(BcfCoreConstants.MAKE_BOOKING_RESPONSE);
		if (Objects.isNull(makeBookingResponseMap))
		{
			return;
		}
		updateEntryWithCachingKeyAndBookingReference(entry, makeBookingResponseMap);
		updateOrderEntryWithBasePrice(entry, makeBookingResponseMap);
		updateOrderEntryWithEbookingPriceForAncillary(entry, makeBookingResponseMap);
		updateOrderEntryWithEbookingPriceForAmendBookingFees(entry, makeBookingResponseMap);
	}

	protected void updateOrderEntryWithEbookingPriceForAncillary(final AbstractOrderEntryModel orderEntry,
			final Map<String, Object> makeBookingResponseMap)
	{
		if (!AncillaryProductModel._TYPECODE.equals(orderEntry.getProduct().getItemtype()))
		{
			return;
		}

		if (Objects.nonNull(orderEntry.getProduct()))
		{
			final String code = orderEntry.getProduct().getCode();
			orderEntry.setBasePrice(getPrice(makeBookingResponseMap, code));
		}
	}

	protected void updateOrderEntryWithEbookingPriceForAmendBookingFees(final AbstractOrderEntryModel orderEntry,
			final Map<String, Object> makeBookingResponseMap)
	{
		if (!FeeProductModel._TYPECODE.equals(orderEntry.getProduct().getItemtype()))
		{
			return;
		}
		if (Objects.nonNull(orderEntry.getProduct()))
		{
			final String code = orderEntry.getProduct().getCode();
			orderEntry.setBasePrice(getPrice(makeBookingResponseMap, code));
		}
	}

	protected void updateEntryWithCachingKeyAndBookingReference(final AbstractOrderEntryModel entry,
			final Map<String, Object> makeBookingResponseMap)
	{

		final String cachingKey = (String) makeBookingResponseMap.get(BcfCoreConstants.CACHING_KEY);
		if (Objects.nonNull(cachingKey) && StringUtils.isNotEmpty(cachingKey))
		{
			entry.setCacheKey(cachingKey);
		}

		final String bookingReference = (String) makeBookingResponseMap.get(BcfCoreConstants.BOOKING_REFERENCE);
		if (Objects.nonNull(bookingReference) && StringUtils.isNotEmpty(bookingReference))
		{
			entry.setBookingReference(bookingReference);
		}
	}

	/**
	 * This method updates the just created cart entry with the base price coming from an external system
	 */
	protected void updateOrderEntryWithBasePrice(final AbstractOrderEntryModel orderEntry,
			final Map<String, Object> makeBookingResponseMap) throws CommerceCartModificationException
	{
		if (!StringUtils.equals("FareProduct", orderEntry.getProduct().getItemtype()))
		{
			return;
		}
		final Map<String, Object> addBundleToCartParamsMap = getSessionService()
				.getAttribute(TravelservicesConstants.ADDBUNDLE_TO_CART_PARAM_MAP);
		if (Objects.isNull(addBundleToCartParamsMap))
		{
			throw new CommerceCartModificationException("couldn't add product to the cart");
		}
		final TravellerModel traveller = (TravellerModel) addBundleToCartParamsMap
				.get(TravelservicesConstants.CART_ENTRY_TRAVELLER);

		if (Objects.isNull(traveller) || Objects.isNull(traveller.getInfo()))
		{
			throw new CommerceCartModificationException("couldn't add product to the cart");
		}


		String code = StringUtils.EMPTY;
		if (traveller.getInfo() instanceof PassengerInformationModel)
		{
			code = ((PassengerInformationModel) traveller.getInfo()).getPassengerType().getEBookingCode();
		}

		if (traveller.getInfo() instanceof BCFVehicleInformationModel)
		{
			code = ((BCFVehicleInformationModel) traveller.getInfo()).getVehicleType().getType().getCode();
		}

		orderEntry.setTaxValues(getTaxes(makeBookingResponseMap, code, orderEntry.getOrder().getCurrency().getIsocode()));
		orderEntry.setFareDetailList(getFareDetails(makeBookingResponseMap, code));
		orderEntry.setBasePrice(getPrice(makeBookingResponseMap, code));

		updateEntryWithJourneyreferenceNumber(orderEntry, addBundleToCartParamsMap);
	}

	protected Collection<FareDetailModel> getFareDetails(final Map<String, Object> makeBookingResponseMap,
			final String productCode)
	{
		final List<ExternalPriceMapData> priceMaps = (List<ExternalPriceMapData>) makeBookingResponseMap
				.get(BcfCoreConstants.PRICE_MAP);
		final Collection<FareDetailModel> getFareDetails = new ArrayList<>();
		final Optional<ExternalPriceMapData> optionalPriceData = priceMaps.stream().filter(
				externalPriceMapData -> productCode.equals(externalPriceMapData.getProductId()) && !externalPriceMapData.isConsumed())
				.findFirst();
		if (optionalPriceData.isPresent())
		{
			for (final FareDetail fareDetailData : optionalPriceData.get().getFareDetails())
			{
				final FareDetailModel fareDetail = getModelService().create(FareDetailModel.class);
				fareDetail.setCode(fareDetailData.getCode());
				fareDetail.setAmount(fareDetailData.getAmount());
				fareDetail.setDescription(fareDetailData.getDescription());
				getFareDetails.add(fareDetail);
			}
		}
		return getFareDetails;
	}

	protected Collection<TaxValue> getTaxes(final Map<String, Object> makeBookingResponseMap, final String productCode,
			final String isoCode)
	{
		final List<ExternalPriceMapData> priceMaps = (List<ExternalPriceMapData>) makeBookingResponseMap
				.get(BcfCoreConstants.PRICE_MAP);
		final Collection<TaxValue> bcfTaxValues = new ArrayList<>();
		final Optional<ExternalPriceMapData> optionalPriceData = priceMaps.stream().filter(
				externalPriceMapData -> productCode.equals(externalPriceMapData.getProductId()) && !externalPriceMapData.isConsumed())
				.findFirst();
		if (optionalPriceData.isPresent())
		{
			for (final ExternalPriceTaxes priceTax : optionalPriceData.get().getTaxes())
			{
				final TaxValue bcfTaxValue = new TaxValue(priceTax.getTaxCode(), priceTax.getAmountInCents(), true,
						priceTax.getAmountInCents(), isoCode);
				bcfTaxValues.add(bcfTaxValue);
			}
		}
		return bcfTaxValues;
	}

	protected Double getPrice(final Map<String, Object> makeBookingResponseMap, final String productCode)
	{
		final List<ExternalPriceMapData> priceMaps = (List<ExternalPriceMapData>) makeBookingResponseMap
				.get(BcfCoreConstants.PRICE_MAP);
		final Optional<ExternalPriceMapData> optionalPriceData = priceMaps.stream().filter(
				externalPriceMapData -> productCode.equals(externalPriceMapData.getProductId()) && !externalPriceMapData.isConsumed())
				.findFirst();
		if (optionalPriceData.isPresent())
		{
			final ExternalPriceMapData priceMapData = optionalPriceData.get();
			priceMapData.setConsumed(true);
			return priceMapData.getValue();
		}
		return Double.valueOf(0);
	}

	protected void updateEntryWithJourneyreferenceNumber(final AbstractOrderEntryModel orderEntry,
			final Map<String, Object> addBundleToCartParamsMap)
	{
		final int journeyRefNumber = Objects.nonNull(addBundleToCartParamsMap.get(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM))
				? (Integer) addBundleToCartParamsMap.get(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM)
				: 0;
		orderEntry.setJourneyReferenceNumber(journeyRefNumber);
	}

	@Override
	public void normalizeEntryNumbersIfNeeded(final CartModel cartModel)
	{
		if (isEntryNumbersNotInSequence(cartModel))
		{
			super.normalizeEntryNumbers(cartModel);
		}
	}

	private boolean isEntryNumbersNotInSequence(final CartModel cartModel)
	{
		final AtomicInteger cartEntryNumberInitialIndex = new AtomicInteger(0);
		return StreamUtil.safeStream(cartModel.getEntries()).map(AbstractOrderEntryModel::getEntryNumber)
				.filter(entryNumber -> entryNumber == null || entryNumber != cartEntryNumberInitialIndex.getAndIncrement()).findAny()
				.isPresent();
	}
}
