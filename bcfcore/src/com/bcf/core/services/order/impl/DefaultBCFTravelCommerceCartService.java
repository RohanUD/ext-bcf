/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.ProductType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerInfoModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.order.impl.DefaultTravelCommerceCartService;
import de.hybris.platform.travelservices.price.data.PriceLevel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.AccommodationService;
import de.hybris.platform.travelservices.services.GuestCountService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.entrygroup.EntryGroupData;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.order.dao.BcfCommerceCartDao;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfCartEntryService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class DefaultBCFTravelCommerceCartService extends DefaultTravelCommerceCartService implements
		BCFTravelCommerceCartService
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFTravelCommerceCartService.class);

	private BcfCartEntryService cartEntryService;
	private AccommodationOfferingService accommodationOfferingService;
	private AccommodationService accommodationService;
	private CategoryService categoryService;
	private CatalogVersionService catalogVersionService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfBookingService bcfBookingService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private GuestCountService guestCountService;
	private static final String PASSENGER_TYPE_CODE_INFANT = "infant";
	private static final String PASSENGER_TYPE_CODE_CHILD = "child";
	private static final String PASSENGER_TYPE_CODE_ADULT = "adult";
	private BcfCommerceCartDao bcfCommerceCartDao;


	@Override
	protected void updateCartEntryWithTravelDetails(final AbstractOrderEntryModel orderEntryModel, final PriceLevel priceLevel,
			final List<TransportOfferingModel> transportOfferings, final boolean saveTravellerDetails)
	{
		final Map<String, Object> addBundleToCartParamsMap = getSessionService()
				.getAttribute(TravelservicesConstants.ADDBUNDLE_TO_CART_PARAM_MAP);

		List<TransportOfferingModel> transportOfferingModels = transportOfferings;
		if (transportOfferingModels == null
				&& addBundleToCartParamsMap.get(TravelservicesConstants.CART_ENTRY_TRANSPORT_OFFERINGS) != null)
		{
			transportOfferingModels = (List<TransportOfferingModel>) addBundleToCartParamsMap
					.get(TravelservicesConstants.CART_ENTRY_TRANSPORT_OFFERINGS);
		}
		final TravelRouteModel travelRouteModel = (TravelRouteModel) addBundleToCartParamsMap
				.get(TravelservicesConstants.CART_ENTRY_TRAVEL_ROUTE);

		final Boolean active = (Boolean) addBundleToCartParamsMap.get(TravelservicesConstants.CART_ENTRY_ACTIVE);
		final AmendStatus amendStatus = (AmendStatus) addBundleToCartParamsMap.get(TravelservicesConstants.CART_ENTRY_AMEND_STATUS);

		final Map<String, Object> params = new HashMap<>();

		// priceLevel will be null for fare products and not null for auto pick products.
		// for fare products the priceLevel is set in addBundleToCartParamsMap
		if (priceLevel == null)
		{
			// Price level will be set for fare product in add to cart flow
			final String priceLevelCode = (String) addBundleToCartParamsMap.get(TravelservicesConstants.CART_ENTRY_PRICELEVEL);
			params.put(TravelOrderEntryInfoModel.PRICELEVEL, priceLevelCode);
		}
		else
		{
			populatePriceLevelForEntry(priceLevel, params);
		}
		final int originDestinationRefNumber = (Integer) addBundleToCartParamsMap
				.get(TravelservicesConstants.CART_ENTRY_ORIG_DEST_REF_NUMBER);
		params.put(TravelOrderEntryInfoModel.ORIGINDESTINATIONREFNUMBER, originDestinationRefNumber);

		populateTransportOfferingForEntry(transportOfferingModels, params);
		populateTravelRouteForEntry(travelRouteModel, params);
		populateActiveFlagForEntry(active, params);
		populateAmendStatusForEntry(amendStatus, params);

		if (saveTravellerDetails)
		{
			final TravellerModel travellerModel = (TravellerModel) addBundleToCartParamsMap
					.get(TravelservicesConstants.CART_ENTRY_TRAVELLER);
			populateTravellerForEntry(orderEntryModel, travellerModel, params);
		}

		if (Objects.nonNull(addBundleToCartParamsMap.get(BcfCoreConstants.CARRYING_DANGEROUS_GOODS)))
		{
			final boolean carryingDangerousGoods = (boolean) addBundleToCartParamsMap.get(BcfCoreConstants.CARRYING_DANGEROUS_GOODS);
			params.put(BcfCoreConstants.CARRYING_DANGEROUS_GOODS, carryingDangerousGoods);
		}
		populateEntryType(orderEntryModel, (OrderEntryType) addBundleToCartParamsMap
				.get(BcfCoreConstants.CART_ENTRY_TYPE));
		persistProperties(params, orderEntryModel);
	}

	protected void populateEntryType(final AbstractOrderEntryModel abstractOrderEntryModel, final OrderEntryType orderEntryType)
	{
		abstractOrderEntryModel.setType(orderEntryType);
	}

	@Override
	public void removeCartEntriesForMinODRefNumber(final Integer journeyRefNumber, final Integer odRefNum)
			throws ModelRemovalException
	{
		if (!getCartService().hasSessionCart())
		{
			return;
		}

		final CartModel sessionCart = getCartService().getSessionCart();
		final List<AbstractOrderEntryModel> abstractOrderEntries = sessionCart.getEntries().stream()
				.filter(entry -> entry.getActive() && !entry.getAmendStatus().equals(AmendStatus.SAME) && entry.getJourneyReferenceNumber() == journeyRefNumber).collect(Collectors.toList());

		if (CollectionUtils.isEmpty(abstractOrderEntries))
		{
			return;
		}

		if (odRefNum.equals(0))
		{
			removeTravellers(abstractOrderEntries);
			removeFeeOrderEntries(abstractOrderEntries);
		}

		final List<AbstractOrderEntryModel> cartEntries = abstractOrderEntries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
						&& !FeeProductModel._TYPECODE.equals(entry.getProduct().getItemtype())
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber().compareTo(odRefNum) >= 0)
				.collect(Collectors.toList());

		removeTravelOrderEntries(cartEntries);

		if (cartEntries.stream().findFirst().isPresent())
		{
			if (bcfTravelCartService.useCacheKey(sessionCart))
			{
				final AmountToPayInfoModel amountToPayInfoModel = sessionCart.getAmountToPayInfos().stream()
						.filter(amountToPayInfo -> cartEntries.stream().findFirst().get().getCacheKey()
								.equals(amountToPayInfo.getCacheKey())).findFirst().orElse(null);
				if (amountToPayInfoModel != null)
				{
					getModelService().remove(amountToPayInfoModel);
				}

			}
			else
			{
				final AmountToPayInfoModel amountToPayInfoModel = sessionCart.getAmountToPayInfos().stream()
						.filter(amountToPayInfo -> cartEntries.stream().findFirst().get().getBookingReference()
								.equals(amountToPayInfo.getBookingReference())).findFirst().orElse(null);
				if (amountToPayInfoModel != null)
				{
					getModelService().remove(amountToPayInfoModel);
				}

			}

		}
		getModelService().refresh(sessionCart);

		sessionCart.setCurrentJourneyRefNum(getCurrentJouneyRefNumber(sessionCart.getEntries()));
		getModelService().save(sessionCart);
	}

	protected int getCurrentJouneyRefNumber(final List<AbstractOrderEntryModel> entries)
	{
		return entries.stream().mapToInt(AbstractOrderEntryModel::getJourneyReferenceNumber).distinct().max().orElse(0);
	}

	/**
	 * Removes the given list of {@link AbstractOrderEntryModel} and their {@link TravelOrderEntryInfoModel}.
	 *
	 * @param abstractOrderEntries the list of abstract order entry models
	 */
	@Override
	public void removeAllTravelOrderEntries(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderEntries))
		{

			final List<TravelOrderEntryInfoModel> travelOrderEntryInfos = abstractOrderEntries.stream().filter(entry->entry.getTravelOrderEntryInfo()!=null)
					.map(AbstractOrderEntryModel::getTravelOrderEntryInfo).collect(Collectors.toList());
			removeTravellers(abstractOrderEntries);
			getModelService().removeAll(travelOrderEntryInfos);
			getModelService().removeAll(abstractOrderEntries);
		}
	}


	/**
	 * Removes the list of {@link TravellerModel} and
	 * {@link de.hybris.platform.travelservices.model.user.TravellerInfoModel} linked to the given
	 * {@link AbstractOrderModel}.
	 *
	 * @param abstractOrderEntries the abstract order model
	 */
	protected void removeTravellers(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		final List<TravellerModel> travellers = getTravellerService().getTravellers(abstractOrderEntries);
		if (CollectionUtils.isEmpty(travellers))
		{
			return;
		}
		getModelService().removeAll(travellers.stream().map(TravellerModel::getInfo).collect(Collectors.toList()));
		getModelService().removeAll(travellers);
	}

	/**
	 * Removes the list of {@link AbstractOrderEntryModel} that corresponds to a product of {@link ProductType} FEE.
	 *
	 * @param abstractOrderEntries the abstract order model
	 */
	protected void removeFeeOrderEntries(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		final List<AbstractOrderEntryModel> feeEntries = abstractOrderEntries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
						&& FeeProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());
		getModelService().removeAll(feeEntries);
	}

	@Override
	public void persistAdditionalPropertiesForCartEntry(final CartModel masterCartModel, final int orderEntryNo,
			final ProductModel product, final Map<String, Map<String, Object>> propertiesMap)
	{
		validateParameterNotNull(product, "Parameter productModel must not be null");
		validateParameterNotNull(orderEntryNo, "Parameter uniqueIdentifier must not be null");
		validateParameterNotNull(masterCartModel, "Parameter masterCartModel must not be null");
		validateParameterNotNull(propertiesMap, "Parameter propertiesMap must not be null");

		final Optional<AbstractOrderEntryModel> orderEntryModel = CollectionUtils.isNotEmpty(masterCartModel.getEntries())
				? masterCartModel.getEntries().stream().filter(entry -> entry.getEntryNumber().equals(Integer.valueOf(orderEntryNo)))
				.findFirst()
				: null;
		if (Objects.nonNull(orderEntryModel) && orderEntryModel.isPresent())
		{
			persistPropertiesFromMaps(propertiesMap, orderEntryModel.get());
		}
	}

	@Override
	protected void persistProperties(final Map<String, Object> propertiesMap, final AbstractOrderEntryModel orderEntry)
	{
		if (!MapUtils.isEmpty(propertiesMap) && !Objects.isNull(orderEntry))
		{
			if (propertiesMap.containsKey(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER))
			{
				getModelService()
						.setAttributeValue(orderEntry, AbstractOrderEntryModel.JOURNEYREFERENCENUMBER,
								propertiesMap.get(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER));
				propertiesMap.remove(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER);
			}
			super.persistProperties(propertiesMap, orderEntry);
		}
	}

	private void persistPropertiesFromMaps(final Map<String, Map<String, Object>> propertiesMap,
			final AbstractOrderEntryModel orderEntry)
	{
		if (MapUtils.isEmpty(propertiesMap) || Objects.isNull(orderEntry))
		{
			return;
		}

		if (MapUtils.isNotEmpty(propertiesMap.get(AbstractOrderEntryModel._TYPECODE)))
		{
			propertiesMap.get(AbstractOrderEntryModel._TYPECODE).entrySet().stream()
					.filter(entry -> Objects.nonNull(entry.getValue()))
					.forEach(entry -> getModelService().setAttributeValue(orderEntry, entry.getKey(), entry.getValue()));
		}

		final TravelOrderEntryInfoModel orderEntryInfo = (orderEntry.getTravelOrderEntryInfo() == null)
				? getModelService().create(TravelOrderEntryInfoModel.class)
				: orderEntry.getTravelOrderEntryInfo();

		final List<Object> itemsToSave = new ArrayList<>();
		if (getModelService().isNew(orderEntryInfo))
		{
			propertiesMap.get(TravelOrderEntryInfoModel._TYPECODE).entrySet().stream()
					.forEach(entry -> getModelService().setAttributeValue(orderEntryInfo, entry.getKey(), entry.getValue()));
			itemsToSave.add(orderEntryInfo);
		}

		orderEntry.setTravelOrderEntryInfo(orderEntryInfo);
		itemsToSave.add(orderEntry);

		getModelService().saveAll(itemsToSave);
	}

	@Override
	public void createOrderGroup(final EntryGroupData entryGroupData, final CartModel cartModel)
	{
		final DealOrderEntryGroupModel dealOrderEntryGroupModel = getModelService().create(DealOrderEntryGroupModel.class);
		dealOrderEntryGroupModel.setPackageId(entryGroupData.getPackageId());
		dealOrderEntryGroupModel.setChildPackageId(entryGroupData.getChildPackageId());
		dealOrderEntryGroupModel.setTravelRoute(entryGroupData.getTravelRoute());
		dealOrderEntryGroupModel.setAccommodationEntryGroups(entryGroupData.getAccommodationOrderEntryGroups());
		dealOrderEntryGroupModel.setDepartureDate(entryGroupData.getDepartureDate());
		dealOrderEntryGroupModel.setReturnDate(entryGroupData.getReturnDate());

		final Map<String, Integer> passengerTypesCount = getPassengerTypesCount(cartModel);
		dealOrderEntryGroupModel.setNumberOfAdults(passengerTypesCount.get(PASSENGER_TYPE_CODE_ADULT));
		dealOrderEntryGroupModel.setNumberOfChild(passengerTypesCount.get(PASSENGER_TYPE_CODE_CHILD));
		dealOrderEntryGroupModel.setNumberOfInfant(passengerTypesCount.get(PASSENGER_TYPE_CODE_INFANT));
		final List<AbstractOrderEntryModel> dealEntries = getCartEntryService()
				.getAbstractOrderEntryModel(cartModel, entryGroupData.getEntryNumbers());
		dealEntries.forEach(dealEntry -> dealEntry.setEntryGroup(dealOrderEntryGroupModel));
		dealOrderEntryGroupModel.setEntries(dealEntries);

		dealOrderEntryGroupModel.setPackageName(StringUtils.isNotBlank(entryGroupData.getPackageId()) ?
				entryGroupData.getPackageName() :
				entryGroupData.getTravelRoute().getName() + ":" + entryGroupData.getAccommodationOrderEntryGroups().get(0)
						.getAccommodationOffering().getName());
		dealOrderEntryGroupModel.setJourneyRefNumber(entryGroupData.getJourneyReferenceNumber());
		getModelService().save(dealOrderEntryGroupModel);
		getModelService().saveAll(dealEntries);
	}

	@Override
	public void createOrUpdateOrderGroup(final EntryGroupData entryGroupData, final CartModel cartModel)
	{
		final DealOrderEntryGroupModel dealOrderEntryGroup = getBcfBookingService()
				.getAccommodationDealOrderEntryGroup(cartModel, entryGroupData.getJourneyReferenceNumber());
		if (Objects.isNull(dealOrderEntryGroup))
		{
			createOrderGroup(entryGroupData, cartModel);
		}
		else
		{
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new ArrayList<>();
			accommodationOrderEntryGroupModels.addAll(dealOrderEntryGroup.getAccommodationEntryGroups());
			accommodationOrderEntryGroupModels.addAll(entryGroupData.getAccommodationOrderEntryGroups());
			dealOrderEntryGroup.setAccommodationEntryGroups(accommodationOrderEntryGroupModels);
			final List<AbstractOrderEntryModel> dealEntries = getCartEntryService()
					.getAbstractOrderEntryModel(cartModel, entryGroupData.getEntryNumbers());
			dealEntries.forEach(dealEntry -> dealEntry.setEntryGroup(dealOrderEntryGroup));
			final Set<AbstractOrderEntryModel> entryModels = new HashSet<>();
			if (CollectionUtils.isNotEmpty(dealOrderEntryGroup.getEntries()))
			{
				entryModels.addAll(dealOrderEntryGroup.getEntries());
			}
			entryModels.addAll(dealEntries);
			dealOrderEntryGroup.setEntries(entryModels.stream().collect(Collectors.toList()));
			getModelService().save(dealOrderEntryGroup);
			getModelService().saveAll(dealEntries);
		}

	}

	@Override
	public EntryGroupData createEntryGroupWithAccommodation(final List<Integer> entryNumbers,
			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups, final Integer journeyRefNumber)
	{
		final EntryGroupData entryGroupData = new EntryGroupData();
		entryGroupData.setPackageName(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.DEFAULT_DYNAMIC_DEAL_PREFIX));
		entryGroupData.setEntryNumbers(getTravelAndActivityEntryNumbers(entryNumbers, journeyRefNumber));
		setTransportDetailsToEntryGroup(entryGroupData, journeyRefNumber);
		entryGroupData.setAccommodationOrderEntryGroups(accommodationEntryGroups);
		return entryGroupData;
	}

	@Override
	public EntryGroupData createEntryGroupForPromotions(final List<Integer> entryNumbers,
			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups, final Integer journeyRefNumber, final String promotionCode)
	{
		final EntryGroupData entryGroupData = createEntryGroupWithAccommodation(entryNumbers, accommodationEntryGroups, journeyRefNumber);
		entryGroupData.setPackageId(promotionCode);
		return entryGroupData;
	}

	protected List<Integer> getTravelAndActivityEntryNumbers(final List<Integer> entryNumbers, final Integer journeyRefNumber)
	{
		final CartModel sessionCart = getBcfTravelCartService().getSessionCart();
		if (Objects.isNull(sessionCart))
		{
			return entryNumbers;
		}
		final List<AbstractOrderEntryModel> transportEntries = sessionCart.getEntries().stream()
				.filter(entry -> entry.getActive() && (OrderEntryType.TRANSPORT.equals(entry.getType()) ||  OrderEntryType.ACTIVITY.equals(entry.getType()))
						&& journeyRefNumber == entry.getJourneyReferenceNumber()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(transportEntries))
		{
			return entryNumbers;
		}
		final List<Integer> transportEntryNumbers = transportEntries.stream().map(AbstractOrderEntryModel::getEntryNumber).collect(
				Collectors.toList());
		entryNumbers.addAll(transportEntryNumbers);
		return entryNumbers;
	}

	protected void setTransportDetailsToEntryGroup(final EntryGroupData entryGroupData, final Integer journeyRefNumber)
	{
		final AbstractOrderEntryModel departureEntry = getCartEntryBasedOnRefNo(journeyRefNumber, 0);
		final AbstractOrderEntryModel returnEntry = getCartEntryBasedOnRefNo(journeyRefNumber, 1);

		final List<TransportOfferingModel> transportOfferings = new ArrayList<>();
		if(departureEntry!=null){
			entryGroupData.setDepartureDate(departureEntry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDepartureTime());
			entryGroupData.setTravelRoute(departureEntry.getTravelOrderEntryInfo().getTravelRoute());
			transportOfferings.add(departureEntry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next());

		}
		if(returnEntry!=null){
			entryGroupData.setReturnDate(returnEntry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDepartureTime());
			transportOfferings.add(returnEntry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next());
		}
		entryGroupData.setTransportOfferings(
				transportOfferings);
		entryGroupData.setJourneyReferenceNumber(journeyRefNumber);
	}

	private AbstractOrderEntryModel getCartEntryBasedOnRefNo(final int journeyRefNumber, final int odRefNo)
	{

		final List<AbstractOrderEntryModel> cartEntries = getBcfTravelCartService().getCartEntriesForRefNo(journeyRefNumber, odRefNo);

		if (CollectionUtils.isEmpty(cartEntries))
		{
			return null;
		}
		return cartEntries.get(0);
	}




	@Override
	public AccommodationOrderEntryGroupModel getAccommodationOrderEntryGroup(final AbstractOrderEntryModel abstractOrderEntry)
	{
		AccommodationOrderEntryGroupModel accommodationEntryGroup = null;
		if (!Objects.equals(OrderEntryType.ACCOMMODATION, abstractOrderEntry.getType()))
		{
			return accommodationEntryGroup;
		}

		if (abstractOrderEntry.getEntryGroup() instanceof DealOrderEntryGroupModel)
		{
			accommodationEntryGroup = getAccommodationEntryGroupFromDealGroup(abstractOrderEntry);
		}
		else
		{
			accommodationEntryGroup = (AccommodationOrderEntryGroupModel) abstractOrderEntry.getEntryGroup();
		}
		return accommodationEntryGroup;
	}

	/**
	 * Gets the accommodation entry group from deal group.
	 *
	 * @param accommodationEntry the accommodation entry
	 * @return the accommodation entry group from deal group
	 */
	protected AccommodationOrderEntryGroupModel getAccommodationEntryGroupFromDealGroup(
			final AbstractOrderEntryModel accommodationEntry)
	{
		return ((DealOrderEntryGroupModel) accommodationEntry.getEntryGroup()).getAccommodationEntryGroups().stream()
				.filter(entryGroup -> entryGroup.getDealAccommodationEntries().contains(accommodationEntry)).findFirst().orElse(null);
	}

	@Override
	public List<CommerceCartModification> addToCart(final CartModel masterCartModel, final ProductModel productModel,
			final long quantityToAdd, final UnitModel unit, final boolean forceNewEntry, final int bundleNo,
			final BundleTemplateModel bundleTemplateModel, final boolean removeCurrentProducts, final String xmlProduct)
			throws CommerceCartModificationException
	{
		if (removeCurrentProducts)
		{
			final List<CartEntryModel> bundleEntries = getBundleCartEntryDao()
					.findEntriesByMasterCartAndBundleNoAndTemplate(masterCartModel, bundleNo, bundleTemplateModel);
			checkAndRemoveDependentComponents(masterCartModel, bundleNo, bundleTemplateModel);
			removeCartEntriesWithChildren(masterCartModel, bundleEntries);
		}

		List<CommerceCartModification> autoPicks = null;

		if (bundleNo != NO_BUNDLE)
		{
			checkAutoPickAddToCart(bundleTemplateModel, productModel);
		}

		final CommerceCartModification modification = addTravelProductToCart(masterCartModel, productModel, quantityToAdd, unit,
				forceNewEntry, bundleNo, bundleTemplateModel, removeCurrentProducts, xmlProduct);

		final Map<String, Object> addBundleToCartParamsMap = getSessionService()
				.getAttribute(TravelservicesConstants.ADDBUNDLE_TO_CART_PARAM_MAP);

		if (modification.getQuantityAdded() > 0 && modification.getEntry() != null)
		{
			if (Objects.nonNull(addBundleToCartParamsMap))
			{
				updateCartEntryWithTravelDetails(modification.getEntry(), null);
				updateBasePriceForFareProduct(modification.getEntry());
			}
			// Change done to accept auto pick bundle
			if (bundleNo <= NEW_BUNDLE)
			{
				final int newBundleNo = modification.getEntry().getBundleNo().intValue();
				autoPicks = addAutoPickProductsToCart(masterCartModel, newBundleNo, bundleTemplateModel, unit);
			}

		}

		final List<CommerceCartModification> modificationList = new ArrayList<>();
		modificationList.add(modification);
		if (CollectionUtils.isNotEmpty(autoPicks))
		{
			modificationList.addAll(autoPicks);
		}

		updateLastModifiedEntriesList(masterCartModel, modificationList);

		return modificationList;
	}

	protected void updateBasePriceForFareProduct(final AbstractOrderEntryModel entry)
	{
		if (StringUtils.equals(FareProductModel._TYPECODE, entry.getProduct().getItemtype()))
		{
			final TravelOrderEntryInfoModel travelOrderEntryInfoModel = entry.getTravelOrderEntryInfo();
			if (Objects.nonNull(travelOrderEntryInfoModel) && CollectionUtils
					.isNotEmpty(travelOrderEntryInfoModel.getTransportOfferings()))
			{
				final boolean anyDefaultTransportOfferingExists = travelOrderEntryInfoModel.getTransportOfferings().stream()
						.anyMatch(TransportOfferingModel::getDefault);
				if (anyDefaultTransportOfferingExists)
				{
					final Integer originDestinationRefNumber = travelOrderEntryInfoModel.getOriginDestinationRefNumber();
					final TravellerModel travellerModel = travelOrderEntryInfoModel.getTravellers().stream().findFirst().orElse(null);
					final double price = getMaxPricedItineraryFromSession(originDestinationRefNumber, travellerModel);
					entry.setBasePrice(price);
					entry.setMarginCalculated(true);
				}
			}
		}
	}

	protected Map<String, Integer> getPassengerTypesCount(final AbstractOrderModel abstractOrderModel)
	{
		final Map<String, Integer> passengerTypeIntCount = new HashMap<>();
		final List<AbstractOrderEntryModel> cartEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> Objects.equals(OrderEntryType.TRANSPORT, entry.getType())).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(cartEntries))
		{
			return passengerTypeIntCount;
		}
		final List<TravellerModel> travellers = cartEntries.stream()
				.filter(entry -> (0 == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
				.flatMap(entry -> entry.getTravelOrderEntryInfo()
						.getTravellers().stream()).collect(Collectors.toList());
		final List<TravellerModel> passengers = travellers.stream()
				.filter(traveller -> Objects.equals(TravellerType.PASSENGER, traveller.getType())).collect(Collectors.toList());
		final List<PassengerTypeModel> passengerTypeModels = passengers.stream()
				.map(passenger -> ((PassengerInformationModel) passenger.getInfo()).getPassengerType())
				.collect(Collectors.toList());

		final Map<String, Long> passengerTypeCount = passengerTypeModels.stream()
				.collect(Collectors.groupingBy(PassengerTypeModel::getCode, Collectors.counting()));
		passengerTypeCount.forEach((passengerType, count) -> {
			passengerTypeIntCount.put(passengerType, count.intValue());
		});
		return passengerTypeIntCount;

	}

	/**
	 * Gets the max priced itinerary from session.
	 *
	 * @param originDestinationRefNumber the origin destination ref number
	 * @return the max priced itinerary from session
	 */
	protected double getMaxPricedItineraryFromSession(final Integer originDestinationRefNumber,
			final TravellerModel travellerModel)
	{
		final TravellerInfoModel travellerInfoModel = travellerModel.getInfo();
		final String travellerCode = (travellerInfoModel instanceof PassengerInformationModel) ?
				((PassengerInformationModel) travellerInfoModel).getPassengerType().getCode() :
				((BCFVehicleInformationModel) travellerInfoModel).getVehicleType().getType().getCode();
		return getPriceFromListSailingSessionMap(originDestinationRefNumber, travellerCode);
	}

	@Override
	public double getPriceFromListSailingSessionMap(final Integer originDestinationRefNumber, final String code)
	{
		final Map<Integer, Map<String, BigDecimal>> originDestTravellerFareMap = getSessionService()
				.getAttribute(BcfCoreConstants.ORIGINDEST_TRAVELLER_FARE_MAP);
		if (MapUtils.isEmpty(originDestTravellerFareMap))
		{
			LOG.error("originDestTravellerFareMap in session is Empty ");
			return 0d;
		}
		final Map<String, BigDecimal> travellerFareMap = originDestTravellerFareMap.get(originDestinationRefNumber);
		if (MapUtils.isEmpty(travellerFareMap))
		{
			LOG.error("travellerFareMap in session is Empty");
			return 0d;
		}

		final BigDecimal farePrice = travellerFareMap.get(code);
		return Objects.nonNull(farePrice) ? farePrice.doubleValue() : 0d;
	}

	@Override
	public Set<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroups(
			final List<AbstractOrderEntryModel> abstractOrderEntries)
	{
		final Set<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new HashSet<>();
		for (final AbstractOrderEntryModel abstractOrderEntry : abstractOrderEntries)
		{
			accommodationOrderEntryGroupModels.add(getAccommodationOrderEntryGroup(abstractOrderEntry));
		}
		return accommodationOrderEntryGroupModels;
	}



	@Override
	public CartModel getCartForCodeAndSite(final String code, final BaseSiteModel site)
	{
		validateParameterNotNull(code, "code cannot be null");
		validateParameterNotNull(site, "site cannot be null");

		return getBcfCommerceCartDao().getCartForCodeAndSite(code, site);
	}

	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected AccommodationService getAccommodationService()
	{
		return accommodationService;
	}

	@Required
	public void setAccommodationService(final AccommodationService accommodationService)
	{
		this.accommodationService = accommodationService;
	}

	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfCartEntryService getCartEntryService()
	{
		return cartEntryService;
	}

	@Required
	public void setCartEntryService(final BcfCartEntryService cartEntryService)
	{
		this.cartEntryService = cartEntryService;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}


	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected GuestCountService getGuestCountService()
	{
		return guestCountService;
	}

	@Required
	public void setGuestCountService(final GuestCountService guestCountService)
	{
		this.guestCountService = guestCountService;
	}

	protected BcfCommerceCartDao getBcfCommerceCartDao()
	{
		return bcfCommerceCartDao;
	}

	@Required
	public void setBcfCommerceCartDao(final BcfCommerceCartDao bcfCommerceCartDao)
	{
		this.bcfCommerceCartDao = bcfCommerceCartDao;
	}
}
