/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order.impl;

import de.hybris.platform.commerceservices.order.impl.CommerceCartFactory;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class DefaultBcfCartFactory extends CommerceCartFactory
{
   private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
   private SessionService sessionService;

   @Override
   protected CartModel createCartInternal()
   {
      final CartModel cart = super.createCartInternal();
      cart.setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel());

      B2BCheckoutContextData b2bCheckoutContextData = sessionService
            .getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);

      if (b2bCheckoutContextData != null && b2bCheckoutContextData.getB2bUnit() != null)
      {
         cart.setUnit(b2bCheckoutContextData.getB2bUnit());
      }

      return cart;
   }

   /**
    * @return the bcfSalesApplicationResolverService
    */
   protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
   {
      return bcfSalesApplicationResolverService;
   }

   /**
    * @param bcfSalesApplicationResolverService the bcfSalesApplicationResolverService to set
    */
   @Required
   public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
   {
      this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
   }

   public SessionService getSessionService()
   {
      return sessionService;
   }

   @Required
   public void setSessionService(final SessionService sessionService)
   {
      this.sessionService = sessionService;
   }
}
