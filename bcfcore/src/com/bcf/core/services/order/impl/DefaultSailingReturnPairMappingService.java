/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.SailingReturnPairMappingModel;
import com.bcf.core.order.dao.SailingReturnPairMappingDao;
import com.bcf.core.services.order.SailingReturnPairMappingService;

public class DefaultSailingReturnPairMappingService implements SailingReturnPairMappingService
{
	private SailingReturnPairMappingDao sailingReturnPairMappingDao;
	private ModelService modelService;

	private static final String HYPHEN = "-";
	private static final String COMMA = ",";

	@Override
	public SailingReturnPairMappingModel getMappingItemUsingBookingReference(final String bookingReference)
	{
		return getSailingReturnPairMappingDao().getMappingItemUsingBookingReference(bookingReference);
	}

	@Override
	public void saveOutBoundInBoundJourneyPairReferences(final OrderModel orderModel)
	{
		if (!Objects.equals(BookingJourneyType.BOOKING_TRANSPORT_ONLY, orderModel.getBookingJourneyType()))
		{
			return;
		}
		final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupedByJourneyRefMap = orderModel.getEntries().stream()
				.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		SailingReturnPairMappingModel sailingReturnPairMapping = getSailingReturnPairMappingDao()
				.findMappingItem(orderModel.getCode());
		if (Objects.isNull(sailingReturnPairMapping))
		{
			sailingReturnPairMapping = getModelService().create(SailingReturnPairMappingModel.class);
			sailingReturnPairMapping.setOrderNumber(orderModel.getCode());
		}

		final StringBuilder bookingReferencePairString = new StringBuilder();
		for (final Entry<Integer, List<AbstractOrderEntryModel>> entriesGroupedByJourneyRef : entriesGroupedByJourneyRefMap
				.entrySet())
		{
			final List<AbstractOrderEntryModel> entriesForJourneyRef = entriesGroupedByJourneyRef.getValue();
			final List<AbstractOrderEntryModel> outBoundEntries = entriesForJourneyRef.stream()
					.filter(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0)
					.collect(Collectors.toList());
			String bookingReferenceNum = outBoundEntries.stream().findFirst().get().getBookingReference();

			final List<AbstractOrderEntryModel> inBoundEntries = entriesForJourneyRef.stream()
					.filter(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 1)
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(inBoundEntries))
			{
				final String inboundBookingReferenceNum = inBoundEntries.stream().findFirst().get().getBookingReference();
				bookingReferenceNum += HYPHEN + inboundBookingReferenceNum;
			}
			bookingReferencePairString.append(COMMA).append(bookingReferenceNum);
		}

		bookingReferencePairString.deleteCharAt(0);
		sailingReturnPairMapping.setInOutReferencePairs(bookingReferencePairString.toString());
		getModelService().save(sailingReturnPairMapping);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected SailingReturnPairMappingDao getSailingReturnPairMappingDao()
	{
		return sailingReturnPairMappingDao;
	}

	@Required
	public void setSailingReturnPairMappingDao(final SailingReturnPairMappingDao sailingReturnPairMappingDao)
	{
		this.sailingReturnPairMappingDao = sailingReturnPairMappingDao;
	}
}
