/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.order.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.order.impl.DefaultTravelCommerceCheckoutService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.services.order.SailingReturnPairMappingService;
import com.bcf.core.services.strategies.StockReleaseStrategy;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.ebooking.ConfirmBookingServiceSuccessData;
import com.bcf.integration.data.confirm.booking.response.ConfirmResult;
import com.bcf.policies.RoundingPolicyService;


public class DefaultBcfCheckoutService extends DefaultTravelCommerceCheckoutService implements BcfCheckoutService
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfCheckoutService.class);

	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private SailingReturnPairMappingService sailingReturnPairMappingService;
	private BCFTravelCartService bcfTravelCartService;
	private Map<String, StockReleaseStrategy> findStockReleaseStrategyMap;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private RoundingPolicyService ceilingRoundingPolicyService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	@Override
	public void createHistorySnapshot(final OrderModel order)
	{
		getBcfCloneOrderStrategy().createHistorySnapshot(order.getOriginalOrder(), order);
	}

	/**
	 * Updates journey pair references in order and removes cart
	 *
	 * @param cartModel
	 * @param orderModel
	 */
	@Override
	public void postProcessOrder(final CartModel cartModel, final OrderModel orderModel) //use service
	{

		releaseInactiveEntriesStock(cartModel);

		if (orderModel != null)
		{
			getModelService().refresh(cartModel);
			getBcfTravelCartService().removeAssociatedItemsFromCart(cartModel);
			getModelService().remove(cartModel);
			getModelService().refresh(orderModel);
		}
	}


	private void releaseInactiveEntriesStock(final CartModel cart)
	{
		final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries = bcfTravelCartService
				.splitCartEntriesByType(cart);
		if (MapUtils.isNotEmpty(typeWiseEntries))
		{
			for (final Map.Entry<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntry : typeWiseEntries.entrySet())
			{
				if (findStockReleaseStrategyMap.containsKey(typeWiseEntry.getKey().getCode()))
				{
					findStockReleaseStrategyMap.get(typeWiseEntry.getKey().getCode()).releaseStock(typeWiseEntry.getValue());
				}

			}
		}
	}

	@Override
	public void updateAmountToPay(final AbstractOrderModel orderModel)
	{
		if (orderModel != null && orderModel.getTotalPrice() != null && orderModel.getAmountPaid() != null)
		{
			final double amountToPay = PrecisionUtil.round(
					BigDecimal.valueOf(orderModel.getTotalPrice()).subtract(BigDecimal.valueOf(orderModel.getAmountPaid()))
							.doubleValue());
			orderModel.setAmountToPay(amountToPay);
			if (amountToPay == 0)
			{
				orderModel.setPaymentStatus(PaymentStatus.PAID);
			}
			else if (amountToPay > 0 && amountToPay < orderModel.getTotalPrice())
			{
				orderModel.setPaymentStatus(PaymentStatus.PARTPAID);
			}
			else if (amountToPay < 0)
			{
				orderModel.setPaymentStatus(PaymentStatus.PARTREFUND);
			}
			else
			{
				orderModel.setPaymentStatus(PaymentStatus.NOTPAID);
			}
		}
	}

	@Override
	public boolean checkSailingTimeThresholdOverrideApplicable()
	{
		final String propertyValues = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfCoreConstants.ADD_TO_CART_THRESHOLD_OVERRIDE);
		final List<String> values = Arrays.asList(propertyValues.split(","));
		return values.contains(bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode());
	}

	@Override
	public boolean updateCartWithEbookingDetails(final ConfirmBookingServiceSuccessData confirmBookingServiceSuccessData,
			final CartModel cartModel)
	{
		if (Objects.nonNull(confirmBookingServiceSuccessData.getConfirmBookingResponse()))
		{
			final List<ConfirmResult> confirmResult = confirmBookingServiceSuccessData.getConfirmBookingResponse()
					.getConfirmResult();

			if (CollectionUtils.isNotEmpty(confirmResult))
			{

				final Map<String, List<AbstractOrderEntryModel>> groupedEntries = groupEntriesByJourneyRef(cartModel);

				final List<ItemModel> itemsToSave = new ArrayList<>();
				getItemsToSave(groupedEntries, itemsToSave, confirmResult);
				updateGuestCustomer(cartModel, confirmBookingServiceSuccessData);
				try
				{
					getModelService().saveAll(itemsToSave);
				}
				catch (final ModelSavingException ex)
				{
					LOG.error("Order No. " + cartModel.getCode() + " Not Saved Successfully", ex);
					return Boolean.FALSE;
				}
			}

			return Boolean.TRUE;
		}
		return Boolean.TRUE;
	}

	private void updateGuestCustomer(final CartModel cartModel,
			final ConfirmBookingServiceSuccessData confirmBookingServiceSuccessData)
	{
		if (CustomerType.GUEST.equals(((CustomerModel) cartModel.getUser()).getType()))
		{
			final List<ConfirmResult> confirmResults = confirmBookingServiceSuccessData.getConfirmBookingResponse()
					.getConfirmResult();
			for (int j = 0; j < confirmResults.size(); j++)
			{
				if (StringUtils.isNotEmpty(confirmBookingServiceSuccessData.getConfirmBookingResponse().getPaymentInfo()
						.getPaymentProcessed().getPaymentDistribution().get(j).getBookingReference().getCachingKey()))
				{
					final String cacheKey = confirmBookingServiceSuccessData.getConfirmBookingResponse().getPaymentInfo()
							.getPaymentProcessed().getPaymentDistribution().get(j).getBookingReference().getCachingKey();

					final String bookingReference = confirmResults.get(j).getBookingReference().getBookingReference();
					final Optional<AmountToPayInfoModel> amountToPayInfoModel = cartModel.getAmountToPayInfos().stream()
							.filter(amountToPayInfoModels -> StringUtils.equals(cacheKey, amountToPayInfoModels.getCacheKey()))
							.findFirst();

					if (amountToPayInfoModel.isPresent())
					{

						final AmountToPayInfoModel amountToPayInfo = amountToPayInfoModel.get();
						amountToPayInfo.setBookingReference(bookingReference);
					}
				}
			}
		}

	}

	private void getItemsToSave(final Map<String, List<AbstractOrderEntryModel>> groupedEntries, final List<ItemModel> itemsToSave,
			final List<ConfirmResult> confirmResult)
	{
		int i = 0;
		if (MapUtils.isNotEmpty(groupedEntries))
		{
			for (final Map.Entry<String, List<AbstractOrderEntryModel>> groupedMapEntry : groupedEntries.entrySet())
			{

				if (CollectionUtils.isNotEmpty(groupedMapEntry.getValue()) && confirmResult.size() > i)
				{
					updateGroupedMapEntry(groupedMapEntry, itemsToSave, i, confirmResult);
					i++;
				}
			}
		}
	}

	private void updateGroupedMapEntry(final Map.Entry<String, List<AbstractOrderEntryModel>> groupedMapEntry,
			final List<ItemModel> itemsToSave, final int i,
			final List<ConfirmResult> confirmResult)
	{
		for (final AbstractOrderEntryModel entry : groupedMapEntry.getValue())
		{
			entry.setBookingReference(confirmResult.get(i).getBookingReference().getBookingReference());
			if (entry.getCacheKey() == null)
			{
				entry.setCacheKey(confirmResult.get(i).getBookingReference().getCachingKey());
			}
			itemsToSave.add(entry);
		}
	}

	private Map<String, List<AbstractOrderEntryModel>> groupEntriesByJourneyRef(final AbstractOrderModel source)
	{

		return source.getEntries().stream()
				.filter(entry -> entry.getActive()
						&& OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects.isNull(entry.getBookingReference()))
				.collect(Collectors.groupingBy(entry -> String
						.valueOf(entry.getJourneyReferenceNumber() + entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())));

	}

	@Override
	public void saveAgentComment(final String agentComment)
	{
		if (StringUtils.isNotBlank(agentComment))
		{
			final StringBuilder builder = new StringBuilder();
			if (StringUtils.isNotBlank(getBcfTravelCartService().getAgentComment()))
			{
				builder.append(getBcfTravelCartService().getAgentComment());
				builder.append(System.lineSeparator());
			}
			builder.append(agentComment);
			getBcfTravelCartService().saveAgentComment(builder.toString());
		}
	}



	@Override
	public void saveReferenceCodes(final List<String> referenceCodes, final List<String> bookingRefs)
	{
		getBcfTravelCartService().saveReferenceCodes(referenceCodes, bookingRefs);
	}

	protected BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	protected SailingReturnPairMappingService getSailingReturnPairMappingService()
	{
		return sailingReturnPairMappingService;
	}

	@Required
	public void setSailingReturnPairMappingService(final SailingReturnPairMappingService sailingReturnPairMappingService)
	{
		this.sailingReturnPairMappingService = sailingReturnPairMappingService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	public Map<String, StockReleaseStrategy> getFindStockReleaseStrategyMap()
	{
		return findStockReleaseStrategyMap;
	}

	public void setFindStockReleaseStrategyMap(
			final Map<String, StockReleaseStrategy> findStockReleaseStrategyMap)
	{
		this.findStockReleaseStrategyMap = findStockReleaseStrategyMap;
	}

	public RoundingPolicyService getCeilingRoundingPolicyService()
	{
		return ceilingRoundingPolicyService;
	}

	public void setCeilingRoundingPolicyService(final RoundingPolicyService ceilingRoundingPolicyService)
	{
		this.ceilingRoundingPolicyService = ceilingRoundingPolicyService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
