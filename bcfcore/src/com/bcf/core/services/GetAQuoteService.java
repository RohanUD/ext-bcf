/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import java.util.List;
import com.bcf.facades.getaquote.data.CsTicketListAndCountData;


public interface GetAQuoteService
{
	void createTicketForQuote(StringBuilder travellerInformation, String firstName, String lastName, String email,
			String phoneNumber, String title);

	void changeTicketStatusForQuote(String status, String ticketId);

	CsTicketListAndCountData getOpenTickets(int startIndex, int pageNumber,
			int recordsPerPage, List<String> status, String category);
}
