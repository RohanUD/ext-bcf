/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.releasecenter;

import java.util.List;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.integration.releasecenter.strategy.ReleaseCenterNotificationStrategy;
import com.bcf.integrations.core.exception.IntegrationException;


public interface ReleaseCenterService
{
	ReleaseCenterNotificationStrategy getStrategy(ServiceNoticeModel model);

	/**
	 * @param serviceNoticeToPublish
	 */
	void publishInstantlyIfInstantPublish(ServiceNoticeModel serviceNoticeToPublish);

	/**
	 * @param serviceNoticeToPublish
	 * @return true if published without errors, otherwise false
	 */
	boolean publish(ServiceNoticeModel serviceNoticeToPublish);

	/**
	 * @param serviceNoticesToPublish
	 * @return true if published without errors, otherwise false
	 */
	boolean publishAll(final List<ServiceNoticeModel> serviceNoticesToPublish) throws IntegrationException;

	boolean updateStatus(final List<ServiceNoticeModel> serviceNotices,final ServiceNoticeStatus status);
}
