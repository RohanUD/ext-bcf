/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.releasecenter.impl;

import reactor.util.CollectionUtils;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.model.BusinessOpportunityModel;
import com.bcf.core.model.ElectronicNewsLetterModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.services.releasecenter.ReleaseCenterService;
import com.bcf.integration.releasecenter.strategy.ReleaseCenterNotificationStrategy;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultReleaseCenterService implements ReleaseCenterService
{
	private static final Logger LOG = Logger.getLogger(DefaultReleaseCenterService.class);

	private Map<String, ReleaseCenterNotificationStrategy> releaseCenterNotificationStrategyMap;

	private ModelService modelService;

	@Override
	public ReleaseCenterNotificationStrategy getStrategy(final ServiceNoticeModel serviceNotice)
	{
		return getReleaseCenterNotificationStrategyMap().get(serviceNotice.getClass().getTypeName());
	}

	@Override
	public void publishInstantlyIfInstantPublish(final ServiceNoticeModel item)
	{
		if (needToPublishInstantly(item))
		{
			publish(item);
		}
	}

	@Override
	public boolean publish(final ServiceNoticeModel serviceNotice)
	{
		final boolean result = publishWithoutPersisting(serviceNotice);
		getModelService().save(serviceNotice);
		return result;
	}

	@Override
	public boolean publishAll(final List<ServiceNoticeModel> serviceNoticesToPublish)
	{
		if (CollectionUtils.isEmpty(serviceNoticesToPublish))
		{
			return true;
		}

		final List<Object> serviceNoticeToSave = new ArrayList<>();
		serviceNoticesToPublish.stream().filter(sn -> isServiceNoticeItemType(sn))
				.forEach(serviceNoticeToPublish -> {
					publishWithoutPersisting(serviceNoticeToPublish);
					serviceNoticeToSave.add(serviceNoticeToPublish);
				});
		getModelService().saveAll(serviceNoticeToSave);
		return serviceNoticesToPublish.size() == serviceNoticeToSave.size();
	}

	private boolean isServiceNoticeItemType(final ServiceNoticeModel sn)
	{
		boolean isServiceNotice = false;

		if (sn instanceof ServiceModel || sn instanceof NewsModel || sn instanceof BusinessOpportunityModel
				|| sn instanceof ElectronicNewsLetterModel)
		{
			isServiceNotice = true;
		}

		return isServiceNotice;
	}

	@Override
	public boolean updateStatus(final List<ServiceNoticeModel> serviceNotices, final ServiceNoticeStatus status)
	{
		if (CollectionUtils.isEmpty(serviceNotices))
		{
			return true;
		}

		try
		{
			serviceNotices.forEach(notice -> notice.setStatus(status));
		}
		catch (final Exception e)
		{
			LOG.error(String.format("Error in updating status %s of Service Notices.Error:%s", status.getCode(),
					e.getLocalizedMessage()));
			return false;
		}
		finally
		{
			getModelService().saveAll(serviceNotices);
		}
		return true;
	}

	private boolean publishWithoutPersisting(final ServiceNoticeModel serviceNotice)
	{
		boolean result = true;
		if (serviceNotice.getSendEmail())
		{
			try
			{
				final ReleaseCenterNotificationStrategy strategy = getStrategy(serviceNotice);
				if (strategy != null)
				{
					strategy.sendNotice(serviceNotice);
				}
				serviceNotice.setPublished(Boolean.TRUE);
				serviceNotice.setPublishedDate(new Date());
			}
			catch (final IntegrationException e)
			{
				result = false;
				LOG.error(String.format(
						"Error while publishing service notice with PK %s to Notification Center.Change log to debug to get more info on exception.",
						serviceNotice.getPk().getLongValueAsString()));
				if (LOG.isDebugEnabled())
				{
					LOG.debug(String.format("Detailed Exception for service notice %s:", serviceNotice.getPk().getLongValueAsString()),
							e);
				}
			}
		}
		serviceNotice.setStatus(ServiceNoticeStatus.LIVE);
		return result;
	}

	private boolean needToPublishInstantly(final ServiceNoticeModel serviceNotice)
	{
		final Date date = new Date();
		return serviceNotice.getInstantPublish() || (date.after(serviceNotice.getPublishDate()) && date
				.before(serviceNotice.getExpiryDate()));
	}

	public Map<String, ReleaseCenterNotificationStrategy> getReleaseCenterNotificationStrategyMap()
	{
		return releaseCenterNotificationStrategyMap;
	}

	public void setReleaseCenterNotificationStrategyMap(
			final Map<String, ReleaseCenterNotificationStrategy> releaseCenterNotificationStrategyMap)
	{
		this.releaseCenterNotificationStrategyMap = releaseCenterNotificationStrategyMap;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
