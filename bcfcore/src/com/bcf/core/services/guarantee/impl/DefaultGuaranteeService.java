/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.guarantee.impl;

import de.hybris.platform.travelservices.model.accommodation.GuaranteeModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.dao.GuaranteeDao;
import com.bcf.core.services.guarantee.GuaranteeService;


public class DefaultGuaranteeService implements GuaranteeService
{
	private GuaranteeDao guaranteeDao;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public GuaranteeModel getGuaranteeByCodeNew(final String code)
	{
		return getGuaranteeDao().findGuaranteeByCode(code);
	}

	protected GuaranteeDao getGuaranteeDao()
	{
		return guaranteeDao;
	}

	@Required
	public void setGuaranteeDao(final GuaranteeDao guaranteeDao)
	{
		this.guaranteeDao = guaranteeDao;
	}


	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
