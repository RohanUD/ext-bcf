/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.price.calculation.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import java.util.List;


/**
 * The Interface BcfCalculationService.
 */
public interface BcfCalculationService extends CalculationService
{


	double getTotalPrice(AbstractOrderModel abstractOrderModel);

	public double getTotalPriceWithOutGoodWillRefund(final AbstractOrderModel abstractOrderModel);

	Double getTotalToPay(double totalDealValue);

	void calculateFeeEntries(final List<AbstractOrderEntryModel> feeEntries) throws CalculationException;

	double getTotalPriceWithOutFee(final AbstractOrderModel abstractOrderModel);
}
