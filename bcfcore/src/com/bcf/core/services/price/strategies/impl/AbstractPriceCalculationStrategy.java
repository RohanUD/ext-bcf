/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.price.strategies.impl;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public abstract class AbstractPriceCalculationStrategy
{
	private PriceRowService priceRowService;
	private CommonI18NService commonI18NService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private BCFTravelCommercePriceService bcfTravelCommercePriceService;
	private PassengerTypeService passengerTypeService;

	/**
	 * @return the priceRowService
	 */
	protected PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	/**
	 * @param priceRowService the priceRowService to set
	 */
	@Required
	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the bcfTravelCommerceCartService
	 */
	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	/**
	 * @param bcfTravelCommerceCartService the bcfTravelCommerceCartService to set
	 */
	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	protected BCFTravelCommercePriceService getBcfTravelCommercePriceService()
	{
		return bcfTravelCommercePriceService;
	}

	@Required
	public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
	{
		this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}
}
