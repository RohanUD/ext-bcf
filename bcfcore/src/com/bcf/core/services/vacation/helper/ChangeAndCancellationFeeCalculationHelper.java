/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.helper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.accommodation.data.ComponentTAndCData;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.FeeTypeEnum;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationFeesModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationPolicyModel;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.StreamUtil;


public class ChangeAndCancellationFeeCalculationHelper
{

	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	private CommonI18NService commerceI18NService;

	public static RouteType getRouteType(final List<AbstractOrderEntryModel> entries)
	{

		final AbstractOrderEntryModel abstractOrderEntryModel = entries.stream().filter(
				entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && entry.getTravelOrderEntryInfo() != null && entry
						.getTravelOrderEntryInfo().getTravelRoute().getRouteType().equals(RouteType.LONG)).findAny().orElse(null);

		if (abstractOrderEntryModel != null)
		{
			return RouteType.LONG;
		}
		else
		{
			return RouteType.SHORT;
		}
	}

	public static Boolean isNorthernRoute(final List<AbstractOrderEntryModel> entries)
	{
		final RouteType routeType = getRouteType(entries);
		if (RouteType.LONG.equals(routeType))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public Date getFirstTravelDate(final List<AbstractOrderEntryModel> entries,
			final List<BcfChangeAccommodationData> changeAccommodationDatas)
	{

		final List<AbstractOrderEntryModel> transportEntries = entries.stream().filter(
				entry -> (entry.getActive() || !entry.getAmendStatus().equals(AmendStatus.SAME)) && OrderEntryType.TRANSPORT
						.equals(entry.getType()) && entry.getTravelOrderEntryInfo() != null && !entry.getTravelOrderEntryInfo()
						.getTransportOfferings().iterator().next().getDefault()).collect(
				Collectors.toList());

		final Date firstTransportDate = StreamUtil.safeStream(transportEntries)
				.map(entry -> entry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDepartureTime())
				.min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> accommodationEntries = entries.stream().filter(
				entry -> (entry.getActive() || !entry.getAmendStatus().equals(AmendStatus.SAME)) && OrderEntryType.ACCOMMODATION
						.equals(entry.getType())).collect(
				Collectors.toList());

		final Date firstAccommodationDate = getAccommodationOrderEntryGroup(accommodationEntries).stream()
				.map(entryGroup -> entryGroup.getStartingDate()).min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> activityEntries = entries.stream().filter(
				entry -> (entry.getActive() || !entry.getAmendStatus().equals(AmendStatus.SAME)) && OrderEntryType.ACTIVITY
						.equals(entry.getType())).collect(
				Collectors.toList());

		final Date firstActivityDate = StreamUtil.safeStream(activityEntries)
				.map(entry -> entry.getActivityOrderEntryInfo().getActivityDate()).min(Date::compareTo).orElse(null);

		final Date firstPotentialAccommodationDate = StreamUtil.safeStream(changeAccommodationDatas)
				.map(changeAccommodationData -> changeAccommodationData.getStartDate()).min(Date::compareTo).orElse(null);


		return Stream.of(firstTransportDate, firstAccommodationDate, firstActivityDate, firstPotentialAccommodationDate)
				.filter(Objects::nonNull).min(Date::compareTo).orElse(null);

	}


	public Integer getNumberOfDays(final Date firstTravelDate)
	{

		if (firstTravelDate != null)
		{
			final LocalDateTime localFirstTravelDateTime = firstTravelDate.toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			final LocalDateTime currentDateTime = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

			final double noOfDays = (double) ChronoUnit.HOURS.between(currentDateTime, localFirstTravelDateTime) / 24;

			return (int) Math.ceil(noOfDays);
		}
		return -1;

	}

	public double getFee(final AbstractOrderModel abstractOrderModel, final Double fee, final FeeTypeEnum feeTypeEnum,
			final double totalPrice)
	{

		if (FeeTypeEnum.AMOUNT.getCode()
				.equals(feeTypeEnum.getCode()))
		{
			return fee;
		}
		else
		{
			return commerceI18NService
					.roundCurrency((totalPrice * fee) / 100.0D, abstractOrderModel.getCurrency().getDigits().intValue());
		}

	}


	public List<AbstractOrderEntryModel> getEntriesFromGroup(
			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups)
	{

		return accommodationEntryGroups.stream().flatMap(entryGroup -> getEntries(entryGroup).stream())
				.collect(Collectors.toList());
	}

	private Collection<AbstractOrderEntryModel> getEntries(final AccommodationOrderEntryGroupModel accommodationEntryGroup)
	{

		return CollectionUtils.isNotEmpty(accommodationEntryGroup.getDealAccommodationEntries()) ?
				accommodationEntryGroup.getDealAccommodationEntries() :
				accommodationEntryGroup.getEntries();

	}

	public Double getComponentFees(final AbstractOrderModel abstractOrderModel,
			final List<VacationChangeAndCancellationFeesModel> vacationChangeAndCancellationFeeModels,
			final List<AbstractOrderEntryModel> inActiveAccommodationEntries,
			final List<AbstractOrderEntryModel> inActiveActivityEntries,
			final boolean changeFee)
	{

		final Double totalFee = 0d;

		for (final VacationChangeAndCancellationFeesModel vacationChangeAndCancellationFeesModel : vacationChangeAndCancellationFeeModels)
		{
			calculateTotalFee(changeFee,
					vacationChangeAndCancellationFeesModel, inActiveAccommodationEntries, inActiveActivityEntries, totalFee);
		}
		return commerceI18NService.roundCurrency(totalFee, abstractOrderModel.getCurrency().getDigits());

	}

	private void calculateTotalFee(final boolean changeFee,
			final VacationChangeAndCancellationFeesModel vacationChangeAndCancellationFeesModel,
			final List<AbstractOrderEntryModel> inActiveAccommodationEntries,
			final List<AbstractOrderEntryModel> inActiveActivityEntries, final Double totalFee)
	{
		List<AbstractOrderEntryModel> applicableAccommodationEntries = null;
		List<AbstractOrderEntryModel> applicableActivityEntries = null;
		Double fee = null;
		FeeTypeEnum feeTypeEnum = null;
		boolean feeTypeAmount = false;
		if (changeFee)
		{
			fee = vacationChangeAndCancellationFeesModel.getChangeFee();
			feeTypeEnum = vacationChangeAndCancellationFeesModel.getChangeFeeType();
		}
		else
		{
			fee = vacationChangeAndCancellationFeesModel.getCancellationFee();
			feeTypeEnum = vacationChangeAndCancellationFeesModel.getCancellationFeeType();
		}

		if (FeeTypeEnum.AMOUNT.getCode()
				.equals(feeTypeEnum.getCode()))
		{
			feeTypeAmount = true;
		}

		final Collection<AccommodationOfferingModel> accommodationOfferingModels = vacationChangeAndCancellationFeesModel
				.getComponentVacationChangeAndCancellationPolicy().getAccommodationOfferings();
		final Collection<ActivityProductModel> activityProductModels = vacationChangeAndCancellationFeesModel
				.getComponentVacationChangeAndCancellationPolicy().getActivityProducts();

		if (CollectionUtils.isNotEmpty(accommodationOfferingModels))
		{
			applicableAccommodationEntries = inActiveAccommodationEntries.stream()
					.filter(entry -> accommodationOfferingModels.contains(getAccommodationOffering(entry))).collect(
							Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(activityProductModels))
		{
			applicableActivityEntries = inActiveActivityEntries.stream()
					.filter(entry -> activityProductModels.contains(entry.getProduct())).collect(
							Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(applicableAccommodationEntries))
		{

			calculateTotalFeeForApplicableEntries(applicableAccommodationEntries, feeTypeAmount, totalFee, fee);

		}
		if (CollectionUtils.isNotEmpty(applicableActivityEntries))
		{
			calculateTotalFeeForApplicableEntries(applicableActivityEntries, feeTypeAmount, totalFee, fee);
		}

	}

	private void calculateTotalFeeForApplicableEntries(
			final List<AbstractOrderEntryModel> applicableEntries, final boolean feeTypeAmount,
			Double totalFee, final Double fee)
	{
		for (final AbstractOrderEntryModel entry : applicableEntries)
		{
			if (feeTypeAmount)
			{
				totalFee = totalFee + fee;
			}
			else
			{
				totalFee = totalFee + ((entry.getTotalPrice() * fee) / 100.0D);
			}
		}
	}


	public double getNonRefundCostToBcf(
			final AbstractOrderEntryModel entry, final double fee, final FeeTypeEnum feeTypeEnum, final int noOfDigits)
	{

		if (FeeTypeEnum.AMOUNT.getCode()
				.equals(feeTypeEnum.getCode()))
		{
			return fee;
		}
		else
		{
			return commerceI18NService.roundCurrency(((entry.getTotalPrice() * fee) / 100.0D), noOfDigits);
		}
	}

	public List<ComponentTAndCData> getVacationPolicyTermsAndConditions(
			final List<VacationChangeAndCancellationPolicyModel> vacationChangeAndCancellationPolicyModels,
			final Collection<AccommodationOfferingModel> accommodationModels,
			final Collection<ActivityProductModel> activityProductModels)
	{


		final List<ComponentTAndCData> componentTAndCDatas = new ArrayList<>();
		ComponentTAndCData componentTAndCData = null;
		List<AccommodationOfferingModel> applicableAccommodations = null;
		List<ActivityProductModel> applicableActivities = null;

		for (final VacationChangeAndCancellationPolicyModel vacationChangeAndCancellationPolicyModel : vacationChangeAndCancellationPolicyModels)
		{

			componentTAndCData = new ComponentTAndCData();
			componentTAndCData.setTermsAndConditions(vacationChangeAndCancellationPolicyModel.getTermsAndConditions());
			componentTAndCDatas.add(componentTAndCData);
			final Collection<AccommodationOfferingModel> policyAccommodationOfferingModels = vacationChangeAndCancellationPolicyModel
					.getAccommodationOfferings();
			final Collection<ActivityProductModel> policyActivityModels = vacationChangeAndCancellationPolicyModel
					.getActivityProducts();

			if (CollectionUtils.isNotEmpty(policyAccommodationOfferingModels))
			{
				applicableAccommodations = StreamUtil.safeStream(accommodationModels)
						.filter(accommodationModel -> policyAccommodationOfferingModels.contains(accommodationModel)).collect(
								Collectors.toList());

				componentTAndCData.setAccommodationOfferings(StreamUtil.safeStream(applicableAccommodations)
						.map(applicableAccommodation -> applicableAccommodation.getName()).collect(
								Collectors.toList()));
			}
			if (CollectionUtils.isNotEmpty(policyActivityModels))
			{
				applicableActivities = StreamUtil.safeStream(activityProductModels)
						.filter(activityProductModel -> policyActivityModels.contains(activityProductModel)).collect(
								Collectors.toList());
				componentTAndCData
						.setActivityProducts(StreamUtil.safeStream(applicableActivities).map(activity -> activity.getName()).collect(
								Collectors.toList()));
			}



		}

		return componentTAndCDatas;
	}


	public static List<AbstractOrderEntryModel> getAccommodationEntries(final List<AbstractOrderEntryModel> entries)
	{

		return entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype()
						.equals(
								RoomRateProductModel._TYPECODE)).collect(
				Collectors.toList());
	}

	public List<RatePlanModel> getRatePlans(final List<AbstractOrderEntryModel> entries)
	{

		return entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype()
						.equals(
								RoomRateProductModel._TYPECODE))
				.map(entry -> bcfTravelCommerceCartService.getAccommodationOrderEntryGroup(entry).getRatePlan())
				.collect(Collectors.toList());
	}

	public List<AccommodationOfferingModel> getAccommodationOfferings(final List<AbstractOrderEntryModel> accommodationEntries)
	{

		return StreamUtil.safeStream(accommodationEntries).map(entry -> getAccommodationOffering(entry)).collect(
				Collectors.toList());

	}

	public List<AccommodationOfferingModel> getAccommodationOfferingsFromGroup(
			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups)
	{

		return StreamUtil.safeStream(accommodationEntryGroups)
				.map(accommodationEntryGroup -> accommodationEntryGroup.getAccommodationOffering()).collect(
						Collectors.toList());

	}


	public List<AbstractOrderEntryModel> getInactiveActivityEntries(final List<AbstractOrderEntryModel> entries)
	{
		final Map<Boolean, List<AbstractOrderEntryModel>> entriesMap = entries.stream().filter(
				entry -> (entry.getAmendStatus().equals(AmendStatus.CHANGED) || entry.getAmendStatus().equals(AmendStatus.NEW))
						&& OrderEntryType.ACTIVITY.equals(entry.getType()))
				.collect(Collectors.partitioningBy(entry -> entry.getActive()));


		final List<AbstractOrderEntryModel> activeEntries = entriesMap.get(Boolean.TRUE);
		final List<AbstractOrderEntryModel> inActiveEntries = entriesMap.get(Boolean.FALSE);

		final List<AbstractOrderEntryModel> applicableInActiveEntries = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(inActiveEntries))
		{

			//group inactive entries based on productcode, guesttype code and activity date
			final Map<String, List<AbstractOrderEntryModel>> inactiveEntryProductGuestTypeActivityDateMap = inActiveEntries.stream()
					.collect(Collectors.groupingBy(entry -> getUniqueActivityDetails(entry)));

			//group active entries based on productcode, guesttype code and activity date
			final Map<String, List<AbstractOrderEntryModel>> activeEntryProductGuestTypeActivityDateMap = Optional
					.ofNullable(activeEntries).orElse(
							Collections.emptyList()).stream().collect(Collectors.groupingBy(entry -> getUniqueActivityDetails(entry)));

			getApplicableInActiveEntries(applicableInActiveEntries, inactiveEntryProductGuestTypeActivityDateMap,
					activeEntryProductGuestTypeActivityDateMap);
		}

		return applicableInActiveEntries;

	}


	private String getUniqueActivityDetails(final AbstractOrderEntryModel entry)
	{

		final StringBuilder uniqueActivityDetails = new StringBuilder();
		uniqueActivityDetails.append(entry.getProduct().getCode()).append(BcfCoreConstants.HYPHEN)
				.append(entry.getActivityOrderEntryInfo().getGuestType().getCode()).append(BcfCoreConstants.HYPHEN)
				.append(entry.getActivityOrderEntryInfo().getActivityDate().getTime()).append(BcfCoreConstants.HYPHEN).append(
				entry.getActivityOrderEntryInfo().getActivityTime() != null ?
						entry.getActivityOrderEntryInfo().getActivityTime() :
						Strings.EMPTY);
		return uniqueActivityDetails.toString();
	}

	private String getUniqueSailingDetails(final AbstractOrderEntryModel entry)
	{

		final StringBuilder uniqueSailingDetails = new StringBuilder();
		uniqueSailingDetails.append(entry.getTravelOrderEntryInfo().getTravelRoute().getOrigin().getCode())
				.append(entry.getTravelOrderEntryInfo().getTravelRoute().getDestination().getCode()).append(BcfCoreConstants.HYPHEN)
				.append(entry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDepartureTime().getTime());
		return uniqueSailingDetails.toString();
	}

	private String getUniqueAccommodationDetails(final AccommodationOrderEntryGroupModel entryGroup)
	{

		final StringBuilder uniqueAccommodationDetails = new StringBuilder();
		uniqueAccommodationDetails.append(entryGroup.getAccommodationOffering().getCode()).append(BcfCoreConstants.HYPHEN)
				.append(TravelDateUtils.convertDateToStringDate(entryGroup.getStartingDate(),
						BcfintegrationcoreConstants.BCF_HOTEL_CALANDER_DATE_FORMAT)).append(BcfCoreConstants.HYPHEN)
				.append(TravelDateUtils.convertDateToStringDate(entryGroup.getEndingDate(),
						BcfintegrationcoreConstants.BCF_HOTEL_CALANDER_DATE_FORMAT));
		return uniqueAccommodationDetails.toString();
	}


	private String getChangeFeeAccommodationDataDetails(final BcfChangeAccommodationData changeAccommodationData)
	{

		final StringBuilder uniqueAccommodationDetails = new StringBuilder();
		uniqueAccommodationDetails.append(changeAccommodationData.getAccommodationOffering()).append(BcfCoreConstants.HYPHEN)
				.append(TravelDateUtils.convertDateToStringDate(changeAccommodationData.getStartDate(),
						BcfintegrationcoreConstants.BCF_HOTEL_CALANDER_DATE_FORMAT)).append(BcfCoreConstants.HYPHEN)
				.append(TravelDateUtils.convertDateToStringDate(changeAccommodationData.getEndDate(),
						BcfintegrationcoreConstants.BCF_HOTEL_CALANDER_DATE_FORMAT));
		return uniqueAccommodationDetails.toString();
	}

	public List<AbstractOrderEntryModel> getInactiveSailingEntries(final List<AbstractOrderEntryModel> entries)
	{
		final List<AbstractOrderEntryModel> applicableInActiveEntries = new ArrayList<>();

		final Map<Boolean, List<AbstractOrderEntryModel>> entriesMap = entries.stream().filter(
				entry -> (entry.getAmendStatus().equals(AmendStatus.CHANGED) || entry.getAmendStatus().equals(AmendStatus.NEW))
						&& OrderEntryType.TRANSPORT.equals(entry.getType()) && FareProductModel._TYPECODE
						.equals(entry.getProduct().getItemtype()) && entry.getTravelOrderEntryInfo() != null).collect(
				Collectors.partitioningBy(entry -> entry.getActive()));

		final List<AbstractOrderEntryModel> activeEntries = entriesMap.get(Boolean.TRUE);
		final List<AbstractOrderEntryModel> inActiveEntries = entriesMap.get(Boolean.FALSE);

		if (CollectionUtils.isNotEmpty(inActiveEntries))
		{
			final Map<String, List<AbstractOrderEntryModel>> inActiveEntryMap = inActiveEntries.stream()
					.collect(Collectors.groupingBy(inActiveEntry -> getUniqueSailingDetails(inActiveEntry)));
			final Map<String, List<AbstractOrderEntryModel>> activeEntryMap = activeEntries.stream()
					.collect(Collectors.groupingBy(activeEntry -> getUniqueSailingDetails(activeEntry)));


			getApplicableInActiveEntries(applicableInActiveEntries, inActiveEntryMap, activeEntryMap);

		}

		return applicableInActiveEntries;
	}

	private void getApplicableInActiveEntries(final List<AbstractOrderEntryModel> applicableInActiveEntries,
			final Map<String, List<AbstractOrderEntryModel>> inActiveEntryMap,
			final Map<String, List<AbstractOrderEntryModel>> activeEntryMap)
	{
		for (final Map.Entry<String, List<AbstractOrderEntryModel>> inActiveEntryMapEntry : inActiveEntryMap.entrySet())
		{

			if (MapUtils.isEmpty(activeEntryMap))
			{

				applicableInActiveEntries.addAll(inActiveEntryMapEntry.getValue());
				continue;
			}

			if (!activeEntryMap.containsKey(inActiveEntryMapEntry.getKey()))
			{
				applicableInActiveEntries.addAll(inActiveEntryMapEntry.getValue());
				continue;
			}

			if (inActiveEntryMapEntry.getValue().stream().mapToInt(entry -> entry.getQuantity().intValue()).sum() <= activeEntryMap
					.get(inActiveEntryMapEntry.getKey()).stream().mapToInt(activeEntry -> activeEntry.getQuantity().intValue()).sum())
			{
				continue;
			}

			for (final AbstractOrderEntryModel entry : inActiveEntryMapEntry.getValue())
			{



				if (!activeEntryMap.containsKey(inActiveEntryMapEntry.getKey()) || entry.getQuantity() > activeEntryMap
						.get(inActiveEntryMapEntry.getKey()).stream().mapToInt(activeEntry -> activeEntry.getQuantity().intValue())
						.sum())
				{

					applicableInActiveEntries.add(entry);
				}
				else
				{

					removeMatchedActiveEntries(activeEntryMap, inActiveEntryMapEntry, entry);

				}
			}

		}
	}

	private void removeMatchedActiveEntries(final Map<String, List<AbstractOrderEntryModel>> activeEntryMap,
			final Map.Entry<String, List<AbstractOrderEntryModel>> inActiveEntryMapEntry, final AbstractOrderEntryModel entry)
	{
		List<AbstractOrderEntryModel> activeEntries = activeEntryMap.get(inActiveEntryMapEntry.getKey());
		activeEntries = StreamUtil.safeStream(activeEntries)
				.sorted((entry1, entry2) -> entry1.getQuantity().compareTo(entry2.getQuantity())).collect(Collectors.toList());
		int qty = 0;
		final List<AbstractOrderEntryModel> entriesTobeRemoved = new ArrayList<>();
		for (final AbstractOrderEntryModel abstractOrderEntryModel : activeEntries)
		{
			qty = qty + abstractOrderEntryModel.getQuantity().intValue();
			entriesTobeRemoved.add(abstractOrderEntryModel);
			if (qty >= entry.getQuantity())
			{
				break;
			}

		}
		activeEntries.removeAll(entriesTobeRemoved);
		if (CollectionUtils.isNotEmpty(activeEntries))
		{
			activeEntryMap.put(inActiveEntryMapEntry.getKey(), activeEntries);
		}
		else
		{
			activeEntryMap.remove(inActiveEntryMapEntry.getKey());
		}
	}

	public List<AccommodationOrderEntryGroupModel> getInactiveAccommodationEntryGroups(final List<AbstractOrderEntryModel> entries)
	{

		final Map<Boolean, List<AbstractOrderEntryModel>> entriesMap = entries.stream().filter(
				entry -> (entry.getAmendStatus().equals(AmendStatus.CHANGED) || entry.getAmendStatus().equals(AmendStatus.NEW))
						&& OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype().equals(
						RoomRateProductModel._TYPECODE)).collect(Collectors.partitioningBy(entry -> entry.getActive()));

		final List<AccommodationOrderEntryGroupModel> applicableInActiveEntryGroups = new ArrayList<>();

		final List<AbstractOrderEntryModel> activeEntries = entriesMap.get(Boolean.TRUE);
		final List<AbstractOrderEntryModel> inActiveEntries = entriesMap.get(Boolean.FALSE);

		if (CollectionUtils.isNotEmpty(inActiveEntries))
		{

			final List<AccommodationOrderEntryGroupModel> inActiveAccommodationOrderEntryGroupModels = getAccommodationOrderEntryGroup(
					inActiveEntries);

			final List<AccommodationOrderEntryGroupModel> activeAccommodationOrderEntryGroupModels = getAccommodationOrderEntryGroup(
					activeEntries);

			final Map<String, List<AccommodationOrderEntryGroupModel>> inActiveEntryGroupMap = inActiveAccommodationOrderEntryGroupModels
					.stream().collect(Collectors.groupingBy(entryGroup -> getUniqueAccommodationDetails(entryGroup)));
			final Map<String, List<AccommodationOrderEntryGroupModel>> activeEntryGroupMap = activeAccommodationOrderEntryGroupModels
					.stream().collect(Collectors.groupingBy(entryGroup -> getUniqueAccommodationDetails(entryGroup)));


			for (final Map.Entry<String, List<AccommodationOrderEntryGroupModel>> inActiveEntryGroupMapEntry : inActiveEntryGroupMap
					.entrySet())
			{

				//if active hotel entry group map is empty ->apply cancellation policy to inactive hotel
				if (MapUtils.isEmpty(activeEntryGroupMap))
				{

					applicableInActiveEntryGroups.addAll(inActiveEntryGroupMapEntry.getValue());
					continue;
				}
				//if active hotel entry group map does not contain the same accommodatio offering and date range  ->apply cancellation policy to inactive hotel
				if (!activeEntryGroupMap.containsKey(inActiveEntryGroupMapEntry.getKey()))
				{
					applicableInActiveEntryGroups.addAll(inActiveEntryGroupMapEntry.getValue());
					continue;
				}
				//if active hotel entry group map no of rooms is less than  -> apply cancellation policy to inactive hotel
				if (inActiveEntryGroupMapEntry.getValue().size() > activeEntryGroupMap.get(inActiveEntryGroupMapEntry.getKey())
						.size())
				{

					applicableInActiveEntryGroups.addAll(inActiveEntryGroupMapEntry.getValue());
					continue;
				}
				else
				{
					activeEntryGroupMap.remove(inActiveEntryGroupMapEntry.getKey());
				}


			}


		}
		return applicableInActiveEntryGroups;
	}


	public List<AccommodationOrderEntryGroupModel> getPotentialInactiveAccommodationEntryGroups(
			final List<AbstractOrderEntryModel> accommodationEntriesApplicable,
			final List<AbstractOrderEntryModel> activeAccommodationEntries, final BcfChangeAccommodationData changeAccommodationData)
	{


		final List<AccommodationOrderEntryGroupModel> applicableEntryGroups = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(accommodationEntriesApplicable))
		{

			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroupsApplicable = getAccommodationOrderEntryGroup(
					accommodationEntriesApplicable);

			final Map<String, List<AccommodationOrderEntryGroupModel>> accommodationEntryGroupsApplicableMap = accommodationEntryGroupsApplicable
					.stream().collect(Collectors.groupingBy(entryGroup -> getUniqueAccommodationDetails(entryGroup)));
			final Map<String, Integer> changeFeeAccommodationDataMap = new HashMap<>();

			changeFeeAccommodationDataMap
					.put(getChangeFeeAccommodationDataDetails(changeAccommodationData), changeAccommodationData.getNoofRooms());
			final List<AccommodationOrderEntryGroupModel> activeAccommodationOrderEntryGroupModels = getAccommodationOrderEntryGroup(
					activeAccommodationEntries);

			final Map<String, List<AccommodationOrderEntryGroupModel>> activeAccommodationOrderEntryGroupMap = activeAccommodationOrderEntryGroupModels
					.stream().collect(Collectors.groupingBy(entryGroup -> getUniqueAccommodationDetails(entryGroup)));

			activeAccommodationOrderEntryGroupMap.entrySet().stream().forEach(
					activeAccommodationOrderEntryGroupMapEntry -> changeFeeAccommodationDataMap
							.merge(activeAccommodationOrderEntryGroupMapEntry.getKey(),
									activeAccommodationOrderEntryGroupMapEntry.getValue().size(), (key, value) -> key + value));

			for (final Map.Entry<String, List<AccommodationOrderEntryGroupModel>> accommodationEntryGroupsApplicableMapEntry : accommodationEntryGroupsApplicableMap
					.entrySet())
			{

				//if active hotel entry group map is empty ->apply cancellation policy to inactive hotel
				if (MapUtils.isEmpty(changeFeeAccommodationDataMap))
				{

					applicableEntryGroups.addAll(accommodationEntryGroupsApplicableMapEntry.getValue());
					continue;
				}
				//if active hotel entry group map does not contain the same accommodatio offering and date range  ->apply cancellation policy to inactive hotel
				if (!changeFeeAccommodationDataMap.containsKey(accommodationEntryGroupsApplicableMapEntry.getKey()))
				{
					applicableEntryGroups.addAll(accommodationEntryGroupsApplicableMapEntry.getValue());
					continue;
				}
				//if active hotel entry group map no of rooms is less than  -> apply cancellation policy to inactive hotel
				if (accommodationEntryGroupsApplicableMapEntry.getValue().size() > changeFeeAccommodationDataMap
						.get(accommodationEntryGroupsApplicableMapEntry.getKey()))
				{

					applicableEntryGroups.addAll(accommodationEntryGroupsApplicableMapEntry.getValue());
					continue;
				}
				else
				{
					changeFeeAccommodationDataMap.remove(accommodationEntryGroupsApplicableMapEntry.getKey());
				}


			}


		}
		return applicableEntryGroups;
	}


	public static List<AbstractOrderEntryModel> getActivityEntries(final List<AbstractOrderEntryModel> entries)
	{

		return entries.stream().filter(entry -> entry.getActive() && OrderEntryType.ACTIVITY.equals(entry.getType())).collect(
				Collectors.toList());

	}

	public List<ActivityProductModel> getActivityProducts(final List<AbstractOrderEntryModel> activityEntries)
	{

		return StreamUtil.safeStream(activityEntries).map(entry -> (ActivityProductModel) entry.getProduct()).collect(
				Collectors.toList());

	}

	public static AccommodationOfferingModel getAccommodationOffering(final AbstractOrderEntryModel entry)
	{
		if (entry.getEntryGroup() instanceof DealOrderEntryGroupModel)
		{
			return ((DealOrderEntryGroupModel) entry.getEntryGroup()).getAccommodationEntryGroups().iterator().next()
					.getAccommodationOffering();
		}
		else
		{
			return ((AccommodationOrderEntryGroupModel) entry.getEntryGroup()).getAccommodationOffering();

		}
	}

	public List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroup(final List<AbstractOrderEntryModel> entries)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new ArrayList<>();

		for (final AbstractOrderEntryModel entry : entries)
		{

			accommodationOrderEntryGroupModels.add(bcfTravelCommerceCartService.getAccommodationOrderEntryGroup(entry));
		}

		return accommodationOrderEntryGroupModels;
	}




	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}
}
