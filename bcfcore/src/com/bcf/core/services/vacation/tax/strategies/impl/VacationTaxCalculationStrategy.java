/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.tax.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.accommodation.ProductTaxGroupMappingModel;
import com.bcf.core.service.TaxRowService;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.util.PrecisionUtil;


public class VacationTaxCalculationStrategy extends VacationPriceCalculationStrategy
{
	private static final Logger LOG = Logger.getLogger(VacationTaxCalculationStrategy.class);
	private ProductTaxGroupMappingService productTaxGroupMappingService;
	private TaxRowService taxRowService;

	/**
	 * Gets the product tax group mapping.
	 *
	 * @param product  the product
	 * @param location the location
	 * @return the product tax group mapping
	 */
	protected ProductTaxGroupMappingModel getProductTaxGroupMapping(final ProductModel product, final LocationModel location)
	{
		final ProductTaxGroup productTaxGroup = product.getEurope1PriceFactory_PTG();
		return getProductTaxGroupMappingService().getProductTaxGroupMapping(productTaxGroup);
	}

	/**
	 * Gets the applicable tax codes.
	 *
	 * @param product  the product
	 * @param location the location
	 * @return the applicable tax codes
	 */
	public List<String> getApplicableTaxCodes(final ProductModel product, final LocationModel location)
	{
		if (Objects.isNull(product.getEurope1PriceFactory_PTG()))
		{
			return Collections.emptyList();
		}

		final ProductTaxGroupMappingModel productTaxGroupMapping = getProductTaxGroupMapping(product, location);
		if (Objects.isNull(productTaxGroupMapping))
		{
			return Collections.emptyList();
		}

		final List<String> applicableTaxCodes = new ArrayList<>();
		if (productTaxGroupMapping.getApplyDMF())
		{
			applicableTaxCodes.add(BcfCoreConstants.DMF);
		}
		if (productTaxGroupMapping.getApplyMRDT())
		{
			applicableTaxCodes.add(BcfCoreConstants.MRDT);
		}
		if (productTaxGroupMapping.getApplyPST())
		{
			applicableTaxCodes.add(BcfCoreConstants.PST);
		}
		if (productTaxGroupMapping.getApplyGST())
		{
			applicableTaxCodes.add(BcfCoreConstants.GST);
		}
		return applicableTaxCodes;
	}

	/**
	 * Gets the tax values.
	 *
	 * @param product   the product
	 * @param basePrice the product base price
	 * @param location  the location
	 * @return the tax values
	 */
	public List<TaxValue> getTaxValues(final ProductModel product, final double basePrice, final LocationModel location)
	{
		final List<String> applicableTaxCodes = getApplicableTaxCodes(product, location);
		final List<TaxValue> taxValues = new ArrayList<>();
		double productNetPrice = basePrice;
		if (applicableTaxCodes.contains(BcfCoreConstants.DMF))
		{
			final List<TaxValue> dmfTaxValues = getTaxValues(Collections.singletonList(BcfCoreConstants.DMF), productNetPrice,
					location, true);
			taxValues.addAll(dmfTaxValues);
			final double dmfTaxValue = dmfTaxValues.stream().mapToDouble(TaxValue::getValue).sum();
			productNetPrice = productNetPrice + dmfTaxValue;
		}
		final List<String> applicableTaxCodesMrdtPstGst = new ArrayList<>();
		if (applicableTaxCodes.contains(BcfCoreConstants.MRDT))
		{
			applicableTaxCodesMrdtPstGst.add(BcfCoreConstants.MRDT);
		}
		if (applicableTaxCodes.contains(BcfCoreConstants.PST))
		{
			applicableTaxCodesMrdtPstGst.add(BcfCoreConstants.PST);
		}
		if (applicableTaxCodes.contains(BcfCoreConstants.GST))
		{
			applicableTaxCodesMrdtPstGst.add(BcfCoreConstants.GST);
		}

		if (CollectionUtils.isNotEmpty(applicableTaxCodesMrdtPstGst))
		{
			final List<TaxValue> applicableTaxValues = getTaxValues(applicableTaxCodesMrdtPstGst, productNetPrice, location, true);
			taxValues.addAll(applicableTaxValues);
		}
		return taxValues;
	}

	/**
	 * Gets the tax values.
	 *
	 * @param applicableTaxCodes the applicable tax codes
	 * @param productNetPrice    the product net price
	 * @param location           the location
	 * @param hiddenTax          is hidden tax
	 * @return the tax values
	 */
	public List<TaxValue> getTaxValues(final List<String> applicableTaxCodes, final double productNetPrice,
			final LocationModel location, final boolean hiddenTax)
	{
		if (Objects.isNull(location) || CollectionUtils.isEmpty(applicableTaxCodes))
		{
			return Collections.emptyList();
		}

		final Collection<TaxRowModel> taxRows = getTaxRowService().getTaxRow(applicableTaxCodes, location);
		if (CollectionUtils.isEmpty(taxRows))
		{
			return Collections.emptyList();
		}

		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		return taxRows.stream().map(taxRow -> getTaxValue(taxRow, productNetPrice, currencyIsoCode, hiddenTax))
				.collect(Collectors.toList());
	}

	/**
	 * Gets the tax value.
	 *
	 * @param taxRow          the tax row
	 * @param productNetPrice the product net price
	 * @param currencyIsoCode the currency iso code
	 * @param hiddenTax       is hidden tax
	 * @return the tax value
	 */
	protected TaxValue getTaxValue(final TaxRowModel taxRow, final double productNetPrice, final String currencyIsoCode,
			final boolean hiddenTax)
	{
		LOG.debug("Tax Row Info: PK=" + taxRow.getPk().getLongValueAsString() + " Tax Code=" + taxRow.getTax().getCode()
				+ " Tax Rate=" + taxRow.getValue());
		final double taxAmount = Objects.isNull(taxRow.getCurrency()) ? productNetPrice / 100 * taxRow.getValue()
				: taxRow.getValue();
		String taxCode = taxRow.getTax().getCode();
		taxCode = hiddenTax ? taxCode + "-hidden" : taxCode;
		return new TaxValue(taxCode, PrecisionUtil.round(taxAmount), true, currencyIsoCode);
	}

	/**
	 * @return the productTaxGroupMappingService
	 */
	protected ProductTaxGroupMappingService getProductTaxGroupMappingService()
	{
		return productTaxGroupMappingService;
	}

	/**
	 * @param productTaxGroupMappingService the productTaxGroupMappingService to set
	 */
	@Required
	public void setProductTaxGroupMappingService(final ProductTaxGroupMappingService productTaxGroupMappingService)
	{
		this.productTaxGroupMappingService = productTaxGroupMappingService;
	}

	/**
	 * @return the taxRowService
	 */
	protected TaxRowService getTaxRowService()
	{
		return taxRowService;
	}

	/**
	 * @param taxRowService the taxRowService to set
	 */
	@Required
	public void setTaxRowService(final TaxRowService taxRowService)
	{
		this.taxRowService = taxRowService;
	}
}
