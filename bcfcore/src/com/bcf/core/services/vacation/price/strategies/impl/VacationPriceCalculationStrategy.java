/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.price.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.util.PriceValue;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.services.price.strategies.impl.AbstractPriceCalculationStrategy;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.core.util.PrecisionUtil;


public class VacationPriceCalculationStrategy extends AbstractPriceCalculationStrategy
{
	/**
	 * Gets the rate plan.
	 *
	 * @param abstractOrderEntry the abstract order entry
	 * @return the rate plan
	 */
	protected RatePlanModel getRatePlan(final AbstractOrderEntryModel abstractOrderEntry)
	{
		AbstractOrderEntryGroupModel entryGroupModel=abstractOrderEntry.getEntryGroup();
		if(entryGroupModel!=null ){

			if(entryGroupModel instanceof AccommodationOrderEntryGroupModel){

				return ((AccommodationOrderEntryGroupModel)entryGroupModel).getRatePlan();
			}else if(entryGroupModel instanceof DealOrderEntryGroupModel){

				Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels=((DealOrderEntryGroupModel)entryGroupModel).getAccommodationEntryGroups();
				AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel= accommodationOrderEntryGroupModels.stream().filter(entryGroup->entryGroup.getDealAccommodationEntries().contains(abstractOrderEntry)).findAny().orElse(null);
				if(accommodationOrderEntryGroupModel!=null){
					return accommodationOrderEntryGroupModel.getRatePlan();
				}

			}
	}
	return null;
	}

	/**
	 * Gets the margin.
	 *
	 * @param ratePlan   the rate plan
	 * @param priceValue the price value
	 * @return the margin
	 */
	public PriceValue getMargin(final RatePlanModel ratePlan, final PriceValue priceValue)
	{
		double marginVal = 0;
		if (Objects.nonNull(ratePlan.getMarginRate()) && ratePlan.getMarginRate() > 0)
		{
			marginVal = priceValue.getValue() * ratePlan.getMarginRate() / 100;
		}
		return new PriceValue(priceValue.getCurrencyIso(), marginVal, true);
	}

	/**
	 * Gets the location.
	 *
	 * @param entry the entry
	 * @return the location
	 */
	protected LocationModel getLocation(final AbstractOrderEntryModel entry)
	{
		LocationModel location = null;
		if (Objects.equals(OrderEntryType.ACCOMMODATION, entry.getType()))
		{
			final AccommodationOrderEntryGroupModel accommodationEntryGroup = getBcfTravelCommerceCartService()
					.getAccommodationOrderEntryGroup(entry);
			if (Objects.nonNull(accommodationEntryGroup))
			{
				location = accommodationEntryGroup.getAccommodationOffering().getLocation();
			}
		}
		else if (Objects.equals(OrderEntryType.ACTIVITY, entry.getType()))
		{
			location = ((ActivityProductModel) entry.getProduct()).getDestination();
		}
		return location;
	}

	protected BcfPriceInfo getDefaultPriceRow(final AbstractOrderEntryModel entry)
	{
		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		final Double basePrice = PrecisionUtil.round(entry.getBasePrice());
		return new BcfPriceInfo(currencyIsoCode, basePrice, 0d, null, 0d, Collections.emptyList(), 0d, null, 0d, entry.getQuantity());
	}
}
