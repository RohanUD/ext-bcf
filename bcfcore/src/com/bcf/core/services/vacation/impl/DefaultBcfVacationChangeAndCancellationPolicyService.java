/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.impl;

import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Date;
import java.util.List;
import com.bcf.core.dao.BcfVacationChangeAndCancellationPolicyDao;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationPolicyModel;
import com.bcf.core.services.vacation.BcfVacationChangeAndCancellationPolicyService;


public class DefaultBcfVacationChangeAndCancellationPolicyService implements BcfVacationChangeAndCancellationPolicyService
{
	BcfVacationChangeAndCancellationPolicyDao bcfVacationChangeAndCancellationPolicyDao;

	public VacationChangeAndCancellationPolicyModel getVacationPolicy(String code){

	return bcfVacationChangeAndCancellationPolicyDao.getVacationPolicy(code);
	}


	public VacationChangeAndCancellationPolicyModel findPackageVacationChangeAndCancellationPolicy(
			Date firstTravelDate, RouteType routeType){

		return bcfVacationChangeAndCancellationPolicyDao.findPackageVacationChangeAndCancellationPolicy( firstTravelDate, routeType);
	}

	public List<VacationChangeAndCancellationPolicyModel> findComponentVacationChangeAndCancellationPolicy(
			Date firstTravelDate, List<AccommodationOfferingModel> accommodationOfferings ,  List<ActivityProductModel> activityProducts){

		return bcfVacationChangeAndCancellationPolicyDao.findComponentVacationChangeAndCancellationPolicy(firstTravelDate,accommodationOfferings,activityProducts);
	}


	public BcfVacationChangeAndCancellationPolicyDao getBcfVacationChangeAndCancellationPolicyDao()
	{
		return bcfVacationChangeAndCancellationPolicyDao;
	}

	public void setBcfVacationChangeAndCancellationPolicyDao(
			final BcfVacationChangeAndCancellationPolicyDao bcfVacationChangeAndCancellationPolicyDao)
	{
		this.bcfVacationChangeAndCancellationPolicyDao = bcfVacationChangeAndCancellationPolicyDao;
	}

}
