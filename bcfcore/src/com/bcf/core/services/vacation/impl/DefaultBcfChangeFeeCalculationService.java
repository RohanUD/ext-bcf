/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.accommodation.service.AccommodationCancellationPolicyService;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.activity.service.ActivityCancellationPolicyService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.services.vacation.BcfVacationChangeAndCancellationPolicyService;
import com.bcf.core.services.vacation.strategies.CartChangeFeeStrategy;
import com.bcf.core.services.vacation.strategies.PotentialChangeFeeStrategy;
import com.bcf.core.util.BCFPropertiesUtils;


public class DefaultBcfChangeFeeCalculationService implements BcfChangeFeeCalculationService
{

	private BCFTravelCartService bcfTravelCartService;
	private CartChangeFeeStrategy bcfCartChangeFeeStrategy;
	private PotentialChangeFeeStrategy bcfPotentialChangeFeeStrategy;
	private ProductService productService;
	private CartService cartService;
	private ModelService modelService;
	private BcfCalculationService bcfCalculationService;
	private BcfBookingService bcfBookingService;
	private BcfVacationChangeAndCancellationPolicyService bcfVacationChangeAndCancellationPolicyService;
	private AccommodationCancellationPolicyService accommodationCancellationPolicyService;
	private ActivityCancellationPolicyService activityCancellationPolicyService;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private static final Logger LOG = Logger.getLogger(DefaultBcfChangeFeeCalculationService.class);
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void calculateChangeFees(final AbstractOrderModel abstractOrderModel)
	{

		if (bcfTravelCartService.isAmendmentCart(abstractOrderModel) && (
				abstractOrderModel.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || abstractOrderModel
						.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || bcfTravelCartService.isAlacateFlow()))
		{
			List<AbstractOrderEntryModel> existingChangeFeeProductEntries = getNewChangeFeeEntries(abstractOrderModel);

			if (CollectionUtils.isNotEmpty(existingChangeFeeProductEntries))
			{
				modelService.removeAll(existingChangeFeeProductEntries);
				modelService.refresh(abstractOrderModel);
			}

			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = bcfCartChangeFeeStrategy
					.calculateChangeFee(abstractOrderModel);
			addAccommodationChangeFeeProductToCart(abstractOrderModel, bcfChangeCancellationVacationFeeData);
		}


	}

	private List<AbstractOrderEntryModel> getNewChangeFeeEntries(final AbstractOrderModel abstractOrderModel)
	{
		final List<String> changeFeeProducts = BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.CHANGE_FEE_PRODUCTS));

		return abstractOrderModel.getEntries().stream().filter(
				entry -> entry.getActive() && !AmendStatus.SAME.equals(entry.getAmendStatus()) && changeFeeProducts
						.contains(entry.getProduct().getCode())).collect(
				Collectors.toList());
	}



	public List<AbstractOrderEntryModel> getExistingChangeAndCancelFeeEntries(final AbstractOrderModel abstractOrderModel)
	{

		final List<String> changeFeeProducts = BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.CHANGE_AND_CANCEL_FEE_PRODUCTS));

		return abstractOrderModel.getEntries().stream().filter(
				entry -> entry.getActive() && changeFeeProducts
						.contains(entry.getProduct().getCode())).collect(
				Collectors.toList());
	}

	@Override
	public Map<String, BigDecimal> calculateChangeFee(AbstractOrderModel order,
			List<BcfChangeAccommodationData> changeAccommodationDatas, boolean accommodationChange)
	{

		if (bcfTravelCartService.isAmendmentCart(order) && (
				order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType()
						.equals(BookingJourneyType.BOOKING_PACKAGE) || bcfTravelCartService.isAlacateFlow()))
		{
			return bcfPotentialChangeFeeStrategy.calculateChangeFee(order, changeAccommodationDatas, accommodationChange);
		}

		return null;
	}

	@Override
	public void addNonRefundableCostToBcf(AbstractOrderModel order)
	{

		if (order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			bcfCartChangeFeeStrategy.addNonRefundableCostToBcf(order);
		}

	}

	private void addAccommodationChangeFeeProductToCart(final AbstractOrderModel cart,
			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)

	{
		Double packageFee;
		Double compFee;
		List<AbstractOrderEntryModel> feeEntries = new ArrayList<>();
		List<ItemModel> itemsToSave = new ArrayList<>();

		if (bcfChangeCancellationVacationFeeData != null)
		{
			packageFee = bcfChangeCancellationVacationFeeData.getPackageFees();
			compFee = bcfChangeCancellationVacationFeeData.getComponentFees();

			if (packageFee != null && packageFee > 0)
			{
				final ProductModel packageChangeFeeProduct = productService.getProductForCode(
						bcfChangeCancellationVacationFeeData.isSouthernRoute() ?
								BcfCoreConstants.SOUTHERN_PACKAGE_CHANGE_FEE_PRODUCT :
								BcfCoreConstants.NORTHERN_PACKAGE_CHANGE_FEE_PRODUCT);
				AbstractOrderEntryModel entry = addUpdateChangeFeeProduct((CartModel) cart, packageFee, packageChangeFeeProduct,
						feeEntries);
				itemsToSave.add(entry);

			}

			if (compFee != null && compFee > 0)
			{
				final ProductModel compChangeFeeProduct = productService.getProductForCode(BcfCoreConstants.COMPONENT_CHANGE_FEE);
				AbstractOrderEntryModel entry = addUpdateChangeFeeProduct((CartModel) cart, compFee, compChangeFeeProduct,
						feeEntries);
				itemsToSave.add(entry);
			}



			if (CollectionUtils.isNotEmpty(itemsToSave))
			{
				itemsToSave.add(cart);
				getModelService().saveAll(itemsToSave);

			}

			try
			{
				bcfCalculationService.calculateFeeEntries(feeEntries);
				bcfCalculationService.calculate(cart);
			}
			catch (CalculationException ex)
			{
				LOG.error("error while calculating cart", ex);
			}
		}
	}

	private AbstractOrderEntryModel addUpdateChangeFeeProduct(final CartModel cart, final Double fee,
			ProductModel changeFeeProduct, List<AbstractOrderEntryModel> feeEntries)
	{
		AbstractOrderEntryModel abstractOrderEntryModel = cartService
				.addNewEntry(cart, changeFeeProduct, 1, changeFeeProduct.getUnit(),-1,false);
		abstractOrderEntryModel.setActive(true);
		abstractOrderEntryModel.setType(OrderEntryType.FEE);
		abstractOrderEntryModel.setBasePrice(fee.doubleValue());
		abstractOrderEntryModel.setAmendStatus(AmendStatus.NEW);
		abstractOrderEntryModel.setCalculated(false);
		feeEntries.add(abstractOrderEntryModel);
		return abstractOrderEntryModel;
	}


	@Override
	public VacationPolicyTAndCData getVacationPolicyTermsAndConditions(AbstractOrderModel order)
	{

		if (order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			return bcfCartChangeFeeStrategy.getVacationPolicyTermsAndConditions(order);
		}
		return null;
	}

	public List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(AbstractOrderModel order)
	{

		if (order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			return bcfCartChangeFeeStrategy.getBCFNonRefundableCostTermsAndConditions(order);
		}
		return Collections.emptyList();

	}


	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	public CartChangeFeeStrategy getBcfCartChangeFeeStrategy()
	{
		return bcfCartChangeFeeStrategy;
	}

	public void setBcfCartChangeFeeStrategy(final CartChangeFeeStrategy bcfCartChangeFeeStrategy)
	{
		this.bcfCartChangeFeeStrategy = bcfCartChangeFeeStrategy;
	}

	public PotentialChangeFeeStrategy getBcfPotentialChangeFeeStrategy()
	{
		return bcfPotentialChangeFeeStrategy;
	}

	public void setBcfPotentialChangeFeeStrategy(
			final PotentialChangeFeeStrategy bcfPotentialChangeFeeStrategy)
	{
		this.bcfPotentialChangeFeeStrategy = bcfPotentialChangeFeeStrategy;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BcfVacationChangeAndCancellationPolicyService getBcfVacationChangeAndCancellationPolicyService()
	{
		return bcfVacationChangeAndCancellationPolicyService;
	}

	public void setBcfVacationChangeAndCancellationPolicyService(
			final BcfVacationChangeAndCancellationPolicyService bcfVacationChangeAndCancellationPolicyService)
	{
		this.bcfVacationChangeAndCancellationPolicyService = bcfVacationChangeAndCancellationPolicyService;
	}

	public AccommodationCancellationPolicyService getAccommodationCancellationPolicyService()
	{
		return accommodationCancellationPolicyService;
	}

	public void setAccommodationCancellationPolicyService(
			final AccommodationCancellationPolicyService accommodationCancellationPolicyService)
	{
		this.accommodationCancellationPolicyService = accommodationCancellationPolicyService;
	}

	public ActivityCancellationPolicyService getActivityCancellationPolicyService()
	{
		return activityCancellationPolicyService;
	}

	public void setActivityCancellationPolicyService(
			final ActivityCancellationPolicyService activityCancellationPolicyService)
	{
		this.activityCancellationPolicyService = activityCancellationPolicyService;
	}

	public BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
