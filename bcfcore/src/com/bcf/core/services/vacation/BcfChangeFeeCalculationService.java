/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;


public interface BcfChangeFeeCalculationService
{

	void calculateChangeFees(AbstractOrderModel abstractOrderModel);

	Map<String, BigDecimal> calculateChangeFee(AbstractOrderModel order, List<BcfChangeAccommodationData> changeAccommodationDatas,
			boolean accommodationChange);

	void addNonRefundableCostToBcf(AbstractOrderModel order);

	List<AbstractOrderEntryModel> getExistingChangeAndCancelFeeEntries(final AbstractOrderModel abstractOrderModel);

	VacationPolicyTAndCData getVacationPolicyTermsAndConditions(AbstractOrderModel order);

	List<BCFNonRefundableCostTAndCData>  getBCFNonRefundableCostTermsAndConditions(AbstractOrderModel order);

}
