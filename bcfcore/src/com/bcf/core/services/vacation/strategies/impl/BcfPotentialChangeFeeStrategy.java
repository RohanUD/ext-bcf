/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.core.services.vacation.strategies.PotentialChangeFeeStrategy;


public class BcfPotentialChangeFeeStrategy extends BaseFeeStrategy implements PotentialChangeFeeStrategy
{

	private SessionService sessionService;
	private BcfCalculationService bcfCalculationService;

	public Map<String, BigDecimal> calculateChangeFee(AbstractOrderModel order, List<BcfChangeAccommodationData> changeAccommodationDatas, boolean accommodationChange){

		List<AbstractOrderEntryModel> entries=order.getEntries();
		Pair<Double,String> packageFeePolicyCodePair=null;
		boolean packageFeeCalculated=false;

		Double componentFee=0d;

		Map<String, BigDecimal> changeFeeAccommodationMap=new HashMap<>();
		RouteType routeType =ChangeAndCancellationFeeCalculationHelper.getRouteType(entries);

		Date firstTravelDate=getChangeAndCancellationFeeCalculationHelper().getFirstTravelDate(entries,changeAccommodationDatas);

		if(firstTravelDate!=null)
		{
			double totalPrice=bcfCalculationService.getTotalPriceWithOutFee(order.getOriginalOrder());
			int numberOfDays = getChangeAndCancellationFeeCalculationHelper().getNumberOfDays(firstTravelDate);

			List<AbstractOrderEntryModel> inactiveSailingEntries = getChangeAndCancellationFeeCalculationHelper()
					.getInactiveSailingEntries(entries);

			List<AbstractOrderEntryModel> inactiveActivityEntries = getChangeAndCancellationFeeCalculationHelper()
					.getInactiveActivityEntries(entries);

			boolean isNewPackageChangeFeeEntryExists=isNewPackageChangeFeeEntryExists(order,routeType);

			final List<AbstractOrderEntryModel> accommodationEntriesApplicable =entries.stream().filter(entry-> !entry.getActive() && (entry.getAmendStatus().equals(AmendStatus.CHANGED)|| entry.getAmendStatus().equals(AmendStatus.NEW)) && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype().equals(
							RoomRateProductModel._TYPECODE)).collect(Collectors.toList());

			final List<AbstractOrderEntryModel> activeAccommodationEntries =new ArrayList<>();
			updateActiveAccommodationEntries(accommodationChange,accommodationEntriesApplicable, entries,activeAccommodationEntries);
			for(BcfChangeAccommodationData changeAccommodationData :changeAccommodationDatas){

				List<AccommodationOrderEntryGroupModel> accommodationEntryGroupsToBeChanged = getChangeAndCancellationFeeCalculationHelper()
						.getPotentialInactiveAccommodationEntryGroups(accommodationEntriesApplicable,activeAccommodationEntries,changeAccommodationData);

				if(!isNewPackageChangeFeeEntryExists){
					if ( (CollectionUtils.isNotEmpty(inactiveSailingEntries) || CollectionUtils.isNotEmpty(inactiveActivityEntries)
							|| CollectionUtils.isNotEmpty(accommodationEntryGroupsToBeChanged)))
					{

						packageFeePolicyCodePair = getPackageChangeFee(order,totalPrice, routeType, firstTravelDate, numberOfDays);

					}
				}

				if (CollectionUtils.isNotEmpty(accommodationEntryGroupsToBeChanged) || CollectionUtils.isNotEmpty(inactiveActivityEntries))
				{
					componentFee = getComponentChangeFee(order, firstTravelDate, numberOfDays, accommodationEntryGroupsToBeChanged,
							inactiveActivityEntries);
				}

				changeFeeAccommodationMap.put(changeAccommodationData.getAccommodationOffering(),getTotalPrice(packageFeePolicyCodePair,componentFee));
				packageFeePolicyCodePair=null;
				componentFee=null;
			}

			return changeFeeAccommodationMap;
		}
		return null;
	}

	private boolean isNewPackageChangeFeeEntryExists(final AbstractOrderModel abstractOrderModel, RouteType routeType )
	{
		String changeFeeProduct =  routeType.equals(RouteType.LONG)?BcfCoreConstants.NORTHERN_PACKAGE_CHANGE_FEE_PRODUCT:BcfCoreConstants.SOUTHERN_PACKAGE_CHANGE_FEE_PRODUCT;

		 AbstractOrderEntryModel entryModel= abstractOrderModel.getEntries().stream().filter(
				entry -> entry.getActive() && !AmendStatus.SAME.equals(entry.getAmendStatus()) && changeFeeProduct.equalsIgnoreCase(
						 entry.getProduct().getCode())).findAny().orElse(null);

		return entryModel != null;
	}



	private void updateActiveAccommodationEntries(boolean accommodationChange,
			List<AbstractOrderEntryModel> accommodationEntriesApplicable, List<AbstractOrderEntryModel> entries,
			List<AbstractOrderEntryModel> activeAccommodationEntries)
	{
		if(accommodationChange)
		{
			int journeyRefNo=sessionService.getAttribute("journeyRefNum");
			accommodationEntriesApplicable.addAll(entries.stream()
					.filter(entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType())
							&& entry.getJourneyReferenceNumber() == journeyRefNo && entry.getProduct().getItemtype().equals(
							RoomRateProductModel._TYPECODE))
					.collect(Collectors.toList()));
			activeAccommodationEntries.addAll(entries.stream().filter(entry-> entry.getActive() &&  entry.getJourneyReferenceNumber() != journeyRefNo && (entry.getAmendStatus().equals(AmendStatus.CHANGED)|| entry.getAmendStatus().equals(AmendStatus.NEW)) && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype().equals(
					RoomRateProductModel._TYPECODE)).collect(Collectors.toList()));
		}else{

			activeAccommodationEntries.addAll(entries.stream().filter(entry-> entry.getActive() &&  (entry.getAmendStatus().equals(AmendStatus.CHANGED)|| entry.getAmendStatus().equals(AmendStatus.NEW)) && OrderEntryType.ACCOMMODATION.equals(entry.getType()) && entry.getProduct().getItemtype().equals(
					RoomRateProductModel._TYPECODE)).collect(Collectors.toList()));
		}
	}

	private BigDecimal getTotalPrice(final Pair<Double,String> packageFeePolicyCodePair,
			 Double  componentFee)
	{
		BigDecimal totalChangeFee=BigDecimal.ZERO;

		if(packageFeePolicyCodePair!=null && packageFeePolicyCodePair.getLeft()!=null){

			totalChangeFee=totalChangeFee.add(BigDecimal.valueOf(packageFeePolicyCodePair.getLeft()));
		}
		if(componentFee!=null){

			totalChangeFee=totalChangeFee.add(BigDecimal.valueOf(componentFee));
		}

		return totalChangeFee;
	}


	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}
}

