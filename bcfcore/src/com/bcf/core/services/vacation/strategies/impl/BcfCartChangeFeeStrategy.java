/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.strategies.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.accommodation.data.ComponentTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.accommodation.service.AccommodationCancellationPolicyService;
import com.bcf.core.activity.service.ActivityCancellationPolicyService;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationPolicyModel;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.core.services.vacation.strategies.CartChangeFeeStrategy;
import com.bcf.core.util.StreamUtil;


public class BcfCartChangeFeeStrategy extends BaseFeeStrategy implements CartChangeFeeStrategy
{


	private ModelService modelService;

	private BcfCalculationService bcfCalculationService;

	private ActivityCancellationPolicyService activityCancellationPolicyService;

	private AccommodationCancellationPolicyService accommodationCancellationPolicyService;

	public BcfChangeCancellationVacationFeeData calculateChangeFee(AbstractOrderModel order)
	{

		List<AbstractOrderEntryModel> entries = order.getEntries();
		boolean southernRoute = true;
		Pair<Double,String> packageFeePolicyCodePair=null;
		Double componentFee=0d;


		RouteType routeType = ChangeAndCancellationFeeCalculationHelper.getRouteType(entries);

		if (routeType.equals(RouteType.LONG))
		{
			southernRoute = false;
		}


		Date firstTravelDate = getChangeAndCancellationFeeCalculationHelper().getFirstTravelDate(entries,null);

		if (firstTravelDate != null)
		{
			double totalPrice=bcfCalculationService.getTotalPriceWithOutFee(order.getOriginalOrder());
			int numberOfDays = getChangeAndCancellationFeeCalculationHelper().getNumberOfDays(firstTravelDate);

			List<AbstractOrderEntryModel> inactiveSailingEntries = getChangeAndCancellationFeeCalculationHelper()
					.getInactiveSailingEntries(entries);

			List<AbstractOrderEntryModel> inactiveActivityEntries = getChangeAndCancellationFeeCalculationHelper()
					.getInactiveActivityEntries(entries);
			List<AccommodationOrderEntryGroupModel> inactiveAccommodationEntryGroups = getChangeAndCancellationFeeCalculationHelper()
					.getInactiveAccommodationEntryGroups(entries);

			if (CollectionUtils.isNotEmpty(inactiveSailingEntries) || CollectionUtils.isNotEmpty(inactiveActivityEntries)
					|| CollectionUtils.isNotEmpty(inactiveAccommodationEntryGroups))
			{
				 packageFeePolicyCodePair = getPackageChangeFee(order, totalPrice, routeType, firstTravelDate, numberOfDays);

			}

			if (CollectionUtils.isNotEmpty(inactiveAccommodationEntryGroups) || CollectionUtils.isNotEmpty(inactiveActivityEntries))
			{
				componentFee=getComponentChangeFee(order, firstTravelDate, numberOfDays, inactiveAccommodationEntryGroups,
						inactiveActivityEntries);
			}
			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData=new BcfChangeCancellationVacationFeeData();

			if(packageFeePolicyCodePair!=null &&  packageFeePolicyCodePair.getLeft()!=null){

				bcfChangeCancellationVacationFeeData.setPackageFees(packageFeePolicyCodePair.getLeft());

			}
			if(componentFee!=null){

				bcfChangeCancellationVacationFeeData.setComponentFees(componentFee);

			}
			bcfChangeCancellationVacationFeeData.setSouthernRoute(southernRoute);

			return bcfChangeCancellationVacationFeeData;
		}
		return null;
	}

	@Override
	public VacationPolicyTAndCData getVacationPolicyTermsAndConditions(AbstractOrderModel order){

		List<AbstractOrderEntryModel> entries = order.getEntries().stream().filter(entry->entry.getActive()).collect(Collectors.toList());

		VacationPolicyTAndCData vacationPolicyTAndCData=new VacationPolicyTAndCData();

		if(CollectionUtils.isNotEmpty(entries))
		{
			List<ComponentTAndCData> componentTAndCDatas = null;
			Date firstTravelDate = getChangeAndCancellationFeeCalculationHelper().getFirstTravelDate(entries, null);

			if (firstTravelDate != null)
			{

				RouteType routeType = ChangeAndCancellationFeeCalculationHelper.getRouteType(entries);

				VacationChangeAndCancellationPolicyModel vacationChangeAndCancellationPolicyModel = getBcfVacationChangeAndCancellationPolicyService()
						.findPackageVacationChangeAndCancellationPolicy(firstTravelDate, routeType);

				if (vacationChangeAndCancellationPolicyModel != null)
				{

					vacationPolicyTAndCData
							.setPackageTermsAndConditions(vacationChangeAndCancellationPolicyModel.getTermsAndConditions());
					vacationPolicyTAndCData
							.setTitle(vacationChangeAndCancellationPolicyModel.getTitle());
				}
				List<AbstractOrderEntryModel> activityEntries = getChangeAndCancellationFeeCalculationHelper()
						.getActivityEntries(entries);

				List<AbstractOrderEntryModel> accommodationEntries = getChangeAndCancellationFeeCalculationHelper()
						.getAccommodationEntries(entries);

				List<AccommodationOfferingModel> accommodationOfferingModels = getChangeAndCancellationFeeCalculationHelper()
						.getAccommodationOfferings(accommodationEntries);

				List<ActivityProductModel> activityProductModels = getChangeAndCancellationFeeCalculationHelper()
						.getActivityProducts(activityEntries);

				if(CollectionUtils.isNotEmpty(accommodationOfferingModels) || CollectionUtils.isNotEmpty(activityProductModels) )
				{
					List<VacationChangeAndCancellationPolicyModel> vacationChangeAndCancellationPolicyModels = getBcfVacationChangeAndCancellationPolicyService()
							.findComponentVacationChangeAndCancellationPolicy(firstTravelDate, accommodationOfferingModels,
									activityProductModels);


					if (CollectionUtils.isNotEmpty(vacationChangeAndCancellationPolicyModels))
					{
						componentTAndCDatas = getChangeAndCancellationFeeCalculationHelper()
								.getVacationPolicyTermsAndConditions(vacationChangeAndCancellationPolicyModels,
										accommodationOfferingModels,
										activityProductModels);
						vacationPolicyTAndCData.setComponentTermsAndConditions(componentTAndCDatas);
					}
				}

			}
		}
		return vacationPolicyTAndCData;
	}

	@Override
	public void addNonRefundableCostToBcf(AbstractOrderModel order){
		List<AbstractOrderEntryModel> entries = order.getEntries();

		Date firstTravelDate =  getChangeAndCancellationFeeCalculationHelper().getFirstTravelDate(entries,null);
		int numberOfDays = getChangeAndCancellationFeeCalculationHelper().getNumberOfDays(firstTravelDate);

		List<AbstractOrderEntryModel> inactiveActivityEntries = getChangeAndCancellationFeeCalculationHelper()
				.getInactiveActivityEntries(entries);
		List<AccommodationOrderEntryGroupModel> inactiveAccommodationEntryGroups = getChangeAndCancellationFeeCalculationHelper()
				.getInactiveAccommodationEntryGroups(entries);

		if (CollectionUtils.isNotEmpty(inactiveAccommodationEntryGroups) || CollectionUtils.isNotEmpty(inactiveActivityEntries))
		{
			addNonRefundableCostToBcf(firstTravelDate,numberOfDays,inactiveAccommodationEntryGroups,inactiveActivityEntries,order.getCurrency().getDigits());


		}

	}

	protected  void addNonRefundableCostToBcf(Date firstTravelDate,int numberOfDays,List<AccommodationOrderEntryGroupModel> inactiveAccommodationEntryGroups,List<AbstractOrderEntryModel> inactiveActivityEntries, int noOfDigits){

		List<ItemModel> itemsToSave=new ArrayList<>();

		if(CollectionUtils.isNotEmpty(inactiveAccommodationEntryGroups)){

			List<RatePlanModel> ratePlans=inactiveAccommodationEntryGroups.stream().map(inactiveAccommodationEntryGroup->inactiveAccommodationEntryGroup.getRatePlan()).collect(
					Collectors.toList());
			List<AccommodationCancellationFeesModel> accommodationCancellationFeesModels=getAccommodationCancellationFeesService().findAccommodationNonRefundableChangeAndCancellationFee(firstTravelDate,numberOfDays,ratePlans);

			if(CollectionUtils.isNotEmpty(accommodationCancellationFeesModels)){
				filteredAccommodationOrderEntry(accommodationCancellationFeesModels,inactiveAccommodationEntryGroups,itemsToSave,noOfDigits);
			}
		}

		if(CollectionUtils.isNotEmpty(inactiveActivityEntries)){
			List<ActivityProductModel> activityProductModels= getChangeAndCancellationFeeCalculationHelper().getActivityProducts(inactiveActivityEntries);
			List<ActivityCancellationFeesModel> activityCancellationFeesModels=getActivityCancellationFeesService().findActivityNonRefundableChangeAndCancellationFee(firstTravelDate,numberOfDays,  activityProductModels);

			if(CollectionUtils.isNotEmpty(activityCancellationFeesModels)){

				for(ActivityCancellationFeesModel activityCancellationFeesModel :activityCancellationFeesModels){

					List<AbstractOrderEntryModel> filteredActivityEntries=inactiveActivityEntries.stream().filter(entry->entry.getProduct().equals(activityCancellationFeesModel.getCancellationPolicy().getActivityProduct())).collect(
							Collectors.toList());
					if(CollectionUtils.isNotEmpty(filteredActivityEntries)){
						filteredActivityEntries.forEach(filteredActivityEntry->{
							filteredActivityEntry.setNonRefundBcfCost(getChangeAndCancellationFeeCalculationHelper().getNonRefundCostToBcf(filteredActivityEntry,activityCancellationFeesModel.getChangeFee(),activityCancellationFeesModel.getChangeFeeType(),noOfDigits));
							itemsToSave.add(filteredActivityEntry);
						});
					}


				}

			}
		}
		if(CollectionUtils.isNotEmpty(itemsToSave)){
			modelService.saveAll(itemsToSave);

		}
	}

	private void filteredAccommodationOrderEntry(
			final List<AccommodationCancellationFeesModel> accommodationCancellationFeesModels,
			final List<AccommodationOrderEntryGroupModel> inactiveAccommodationEntryGroups, final List<ItemModel> itemsToSave,
			final int noOfDigits)
	{
		for(AccommodationCancellationFeesModel accommodationCancellationFeesModel :accommodationCancellationFeesModels){

			List<AccommodationOrderEntryGroupModel> filteredAccommodationOrderEntryGroupModels=inactiveAccommodationEntryGroups.stream().filter(entry->entry.getRatePlan().equals(accommodationCancellationFeesModel.getAccommodationCancellationPolicy().getRatePlan())).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(filteredAccommodationOrderEntryGroupModels)){
				filteredAccommodationOrderEntryGroupModels.forEach(filteredAccommodationOrderEntryGroupModel->{

					if(CollectionUtils.isNotEmpty(filteredAccommodationOrderEntryGroupModel.getDealAccommodationEntries())){

						filteredAccommodationOrderEntryGroupModel.getDealAccommodationEntries().forEach(entry->{
							entry.setNonRefundBcfCost(getChangeAndCancellationFeeCalculationHelper().getNonRefundCostToBcf(entry,accommodationCancellationFeesModel.getChangeFee(),accommodationCancellationFeesModel.getChangeFeeType(),noOfDigits));
							itemsToSave.add(entry);
						});

					}else{
						filteredAccommodationOrderEntryGroupModel.getEntries().forEach(entry->{
							entry.setNonRefundBcfCost(getChangeAndCancellationFeeCalculationHelper().getNonRefundCostToBcf(entry,accommodationCancellationFeesModel.getChangeFee(),accommodationCancellationFeesModel.getChangeFeeType(),noOfDigits));
							itemsToSave.add(entry);
						});
					}

				});
			}
		}
	}

	public List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(AbstractOrderModel order){

		List<AbstractOrderEntryModel> entries = order.getEntries().stream().filter(entry->entry.getActive()).collect(Collectors.toList());

		List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTAndCDatas = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(entries))
		{

			Date firstTravelDate = getChangeAndCancellationFeeCalculationHelper().getFirstTravelDate(entries, null);

			if (firstTravelDate != null)
			{
				List<AbstractOrderEntryModel> accommodationEntries = getChangeAndCancellationFeeCalculationHelper()
						.getAccommodationEntries(entries);
				List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = getChangeAndCancellationFeeCalculationHelper()
						.getAccommodationOrderEntryGroup(accommodationEntries);

				List<RatePlanModel> ratePlans = accommodationOrderEntryGroupModels.stream()
						.map(accommodationOrderEntryGroup -> accommodationOrderEntryGroup.getRatePlan()).collect(
								Collectors.toList());

				List<AbstractOrderEntryModel> activityEntries = getChangeAndCancellationFeeCalculationHelper()
						.getActivityEntries(entries);
				List<ActivityProductModel> activityProductModels = getChangeAndCancellationFeeCalculationHelper()
						.getActivityProducts(activityEntries);

					if(CollectionUtils.isNotEmpty(activityProductModels))
					{
						updateNonRefundableCostTAndCDatas(firstTravelDate,activityProductModels,bcfNonRefundableCostTAndCDatas);

					}

				if(CollectionUtils.isNotEmpty(ratePlans))
				{
					updateNonRefundableCostTAndCDataIfRatePlansNotEmpty(firstTravelDate,ratePlans,bcfNonRefundableCostTAndCDatas,accommodationOrderEntryGroupModels);

				}

			}
		}
		return bcfNonRefundableCostTAndCDatas;
	}

	private void updateNonRefundableCostTAndCDataIfRatePlansNotEmpty(Date firstTravelDate, List<RatePlanModel> ratePlans,
			final List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTAndCDatas,
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels)
	{
		List<AccommodationCancellationPolicyModel> accommodationCancellationPolicyModels = accommodationCancellationPolicyService
				.findAccommodationNonRefundableChangeAndCancellationPolicy(firstTravelDate, ratePlans);
		if (CollectionUtils.isNotEmpty(accommodationCancellationPolicyModels))
		{

			for (AccommodationCancellationPolicyModel accommodationCancellationPolicyModel : accommodationCancellationPolicyModels)
			{

				BCFNonRefundableCostTAndCData bCFNonRefundableCostTAndCData = new BCFNonRefundableCostTAndCData();
				AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = StreamUtil
						.safeStream(accommodationOrderEntryGroupModels).filter(
								accommodationOrderEntryGroup -> accommodationOrderEntryGroup.getRatePlan()
										.equals(accommodationCancellationPolicyModel.getRatePlan())).findAny().orElse(null);
				if (accommodationOrderEntryGroupModel != null)
				{
					bCFNonRefundableCostTAndCData
							.setProduct(accommodationOrderEntryGroupModel.getAccommodationOffering().getName());

				}
				bCFNonRefundableCostTAndCData
						.setTermsAndConditions(accommodationCancellationPolicyModel.getCancellationPolicyDescription());
				bcfNonRefundableCostTAndCDatas.add(bCFNonRefundableCostTAndCData);


			}

		}
	}

	private void updateNonRefundableCostTAndCDatas(Date firstTravelDate,
			List<ActivityProductModel> activityProductModels,
			final List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTAndCDatas)
	{
		List<ActivityCancellationPolicyModel> activityCancellationPolicyModels = activityCancellationPolicyService
				.findActivityNonRefundableChangeAndCancellationFee(firstTravelDate, activityProductModels);


		if (CollectionUtils.isNotEmpty(activityCancellationPolicyModels))
		{

			for (ActivityCancellationPolicyModel activityCancellationPolicyModel : activityCancellationPolicyModels)
			{

				BCFNonRefundableCostTAndCData bCFNonRefundableCostTAndCData = new BCFNonRefundableCostTAndCData();
				bCFNonRefundableCostTAndCData.setProduct(activityCancellationPolicyModel.getActivityProduct().getName());
				bCFNonRefundableCostTAndCData
						.setTermsAndConditions(activityCancellationPolicyModel.getCancellationPolicyDescription());
				bcfNonRefundableCostTAndCDatas.add(bCFNonRefundableCostTAndCData);


			}
		}
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	public ActivityCancellationPolicyService getActivityCancellationPolicyService()
	{
		return activityCancellationPolicyService;
	}

	public void setActivityCancellationPolicyService(
			final ActivityCancellationPolicyService activityCancellationPolicyService)
	{
		this.activityCancellationPolicyService = activityCancellationPolicyService;
	}

	public AccommodationCancellationPolicyService getAccommodationCancellationPolicyService()
	{
		return accommodationCancellationPolicyService;
	}

	public void setAccommodationCancellationPolicyService(
			final AccommodationCancellationPolicyService accommodationCancellationPolicyService)
	{
		this.accommodationCancellationPolicyService = accommodationCancellationPolicyService;
	}
}

