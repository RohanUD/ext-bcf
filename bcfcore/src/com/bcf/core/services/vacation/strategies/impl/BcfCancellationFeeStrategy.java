/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.strategies.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.accommodation.service.AccommodationCancellationFeesService;
import com.bcf.core.activity.service.ActivityCancellationFeesService;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationFeesModel;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.BcfVacationChangeAndCancellationFeeService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.core.services.vacation.strategies.CancellationFeeStrategy;


public class BcfCancellationFeeStrategy implements CancellationFeeStrategy
{
	private BcfVacationChangeAndCancellationFeeService bcfVacationChangeAndCancellationFeeService;

	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;

	private BcfBookingService bcfBookingService;

	private ModelService modelService;

	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	private BcfCalculationService bcfCalculationService;

	private AccommodationCancellationFeesService accommodationCancellationFeesService;

	private ActivityCancellationFeesService activityCancellationFeesService;

	@Override
	public BcfChangeCancellationVacationFeeData calculateCancellationFee(AbstractOrderModel order)
	{

		if(order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			Pair<Double,String> packageFeePolicyCodePair =null;
			Double componentFee;

			List<AbstractOrderEntryModel> entries = order.getEntries();

			double totalPrice=bcfCalculationService.getTotalPriceWithOutFee(order);
			boolean southernRoute = true;
			RouteType routeType = ChangeAndCancellationFeeCalculationHelper.getRouteType(entries);

			Date firstTravelDate = changeAndCancellationFeeCalculationHelper.getFirstTravelDate(entries,null);

			int numberOfDays = changeAndCancellationFeeCalculationHelper.getNumberOfDays(firstTravelDate);

			if (routeType.equals(RouteType.LONG))
			{
				southernRoute = false;
			}
			packageFeePolicyCodePair = getPackageCancellationFee(order,totalPrice, routeType, firstTravelDate, numberOfDays);

			componentFee = getComponentCancellationFee(order, entries, firstTravelDate, numberOfDays);


			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData=new BcfChangeCancellationVacationFeeData();

			if(packageFeePolicyCodePair!=null){

				bcfChangeCancellationVacationFeeData.setPackageFees(packageFeePolicyCodePair.getLeft());
			}
			if(componentFee!=null){

				bcfChangeCancellationVacationFeeData.setComponentFees(componentFee);

			}
			bcfChangeCancellationVacationFeeData.setSouthernRoute(southernRoute);

			return bcfChangeCancellationVacationFeeData;
		}
		return null;
	}


	@Override
	public void addNonRefundableCostToBcf(AbstractOrderModel order){
		if(order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			List<AbstractOrderEntryModel> entries = order.getEntries();
			Date firstTravelDate = changeAndCancellationFeeCalculationHelper.getFirstTravelDate(entries,null);
			int numberOfDays = changeAndCancellationFeeCalculationHelper.getNumberOfDays(firstTravelDate);

			List<AbstractOrderEntryModel> accommodationEntries = changeAndCancellationFeeCalculationHelper
					.getAccommodationEntries(entries);
			List<AbstractOrderEntryModel> activityEntries = changeAndCancellationFeeCalculationHelper.getActivityEntries(entries);


			if (CollectionUtils.isNotEmpty(accommodationEntries) || CollectionUtils.isNotEmpty(activityEntries))
			{
				addNonRefundableCostToBcf(firstTravelDate, numberOfDays, accommodationEntries, activityEntries,order.getCurrency().getDigits());

			}
		}
	}

	private void addNonRefundableCostToBcf(Date firstTravelDate,int numberOfDays,List<AbstractOrderEntryModel> accommodationEntries,List<AbstractOrderEntryModel> activityEntries,int noOfDigits){

		List<ItemModel> itemsToSave=new ArrayList<>();

		if(CollectionUtils.isNotEmpty(accommodationEntries)){

			List<RatePlanModel> ratePlans=changeAndCancellationFeeCalculationHelper.getRatePlans(accommodationEntries);

			List<AccommodationCancellationFeesModel> accommodationCancellationFeesModels=accommodationCancellationFeesService.findAccommodationNonRefundableChangeAndCancellationFee(firstTravelDate,numberOfDays,ratePlans);

			if(CollectionUtils.isNotEmpty(accommodationCancellationFeesModels)){

				filteredAccommodationEntries(accommodationCancellationFeesModels,accommodationEntries, itemsToSave,noOfDigits);
			}
		}

		if(CollectionUtils.isNotEmpty(activityEntries)){
			List<ActivityProductModel> activityProductModels= changeAndCancellationFeeCalculationHelper.getActivityProducts(activityEntries);
			List<ActivityCancellationFeesModel> activityCancellationFeesModels=activityCancellationFeesService.findActivityNonRefundableChangeAndCancellationFee(firstTravelDate,numberOfDays,  activityProductModels);

			if(CollectionUtils.isNotEmpty(activityCancellationFeesModels)){

				for(ActivityCancellationFeesModel activityCancellationFeesModel :activityCancellationFeesModels){

					List<AbstractOrderEntryModel> filteredActivityEntries=activityEntries.stream().filter(entry->entry.getProduct().equals(activityCancellationFeesModel.getCancellationPolicy().getActivityProduct())).collect(
							Collectors.toList());
					if(CollectionUtils.isNotEmpty(filteredActivityEntries)){
						filteredActivityEntries.forEach(filteredActivityEntry->{
							filteredActivityEntry.setNonRefundBcfCost(changeAndCancellationFeeCalculationHelper.getNonRefundCostToBcf(filteredActivityEntry,activityCancellationFeesModel.getCancellationFee(),activityCancellationFeesModel.getCancellationFeeType(),noOfDigits));
							itemsToSave.add(filteredActivityEntry);
						});
					}
				}

			}
		}
		if(CollectionUtils.isNotEmpty(itemsToSave)){
			modelService.saveAll(itemsToSave);

		}
	}

	private void filteredAccommodationEntries(List<AccommodationCancellationFeesModel> accommodationCancellationFeesModels,
			List<AbstractOrderEntryModel> accommodationEntries,
			List<ItemModel> itemsToSave, int noOfDigits)
	{
		for(AccommodationCancellationFeesModel accommodationCancellationFeesModel :accommodationCancellationFeesModels){

			List<AbstractOrderEntryModel> filteredAccommodationEntries=accommodationEntries.stream().filter(entry->bcfTravelCommerceCartService.getAccommodationOrderEntryGroup(entry).getRatePlan().equals(accommodationCancellationFeesModel.getAccommodationCancellationPolicy().getRatePlan())).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(filteredAccommodationEntries)){
				filteredAccommodationEntries.forEach(filteredAccommodationEntry->{

					filteredAccommodationEntry.setNonRefundBcfCost(changeAndCancellationFeeCalculationHelper.getNonRefundCostToBcf(filteredAccommodationEntry,accommodationCancellationFeesModel.getCancellationFee(),accommodationCancellationFeesModel.getCancellationFeeType(),noOfDigits));
					itemsToSave.add(filteredAccommodationEntry);


				});
			}
		}
	}

	private Pair<Double,String> getPackageCancellationFee(final AbstractOrderModel order,double totalPrice, final RouteType routeType, final Date firstTravelDate,
			final int numberOfDays)
	{

		VacationChangeAndCancellationFeesModel vacationChangeAndCancellationFeeModel = bcfVacationChangeAndCancellationFeeService
				.findPackageVacationChangeAndCancellationFee(routeType, firstTravelDate, numberOfDays);

		if (vacationChangeAndCancellationFeeModel != null)
		{
			double cancellationFee=changeAndCancellationFeeCalculationHelper
					.getFee(order, vacationChangeAndCancellationFeeModel.getCancellationFee(),
							vacationChangeAndCancellationFeeModel.getCancellationFeeType(),totalPrice);
			String vacationPolicyCode=vacationChangeAndCancellationFeeModel.getPackageVacationChangeAndCancellationPolicy().getCode();
			return Pair.of(cancellationFee,vacationPolicyCode);

		}
		return null;

	}

	private  Double  getComponentCancellationFee(final AbstractOrderModel order, List<AbstractOrderEntryModel> entries,  final Date firstTravelDate,
			final int numberOfDays){


			List<AbstractOrderEntryModel> accommodationEntries=changeAndCancellationFeeCalculationHelper.getAccommodationEntries(entries);
			List<AbstractOrderEntryModel> activityEntries= changeAndCancellationFeeCalculationHelper.getActivityEntries(entries);

			if(CollectionUtils.isNotEmpty(accommodationEntries) || CollectionUtils.isNotEmpty(activityEntries) ){
				List<AccommodationOfferingModel> accommodationOfferings= changeAndCancellationFeeCalculationHelper.getAccommodationOfferings(accommodationEntries);
				List<ActivityProductModel> activityProductModels= changeAndCancellationFeeCalculationHelper.getActivityProducts(activityEntries);
				List<VacationChangeAndCancellationFeesModel> vacationChangeAndCancellationFeeModels=bcfVacationChangeAndCancellationFeeService.findComponentVacationChangeAndCancellationFee(firstTravelDate,numberOfDays,accommodationOfferings,activityProductModels);

				if(CollectionUtils.isNotEmpty(vacationChangeAndCancellationFeeModels)){

					return changeAndCancellationFeeCalculationHelper.getComponentFees(order, vacationChangeAndCancellationFeeModels,accommodationEntries,activityEntries,false);
				}
			}

		return null;
	}

	public BcfVacationChangeAndCancellationFeeService getBcfVacationChangeAndCancellationFeeService()
	{
		return bcfVacationChangeAndCancellationFeeService;
	}

	public void setBcfVacationChangeAndCancellationFeeService(
			final BcfVacationChangeAndCancellationFeeService bcfVacationChangeAndCancellationFeeService)
	{
		this.bcfVacationChangeAndCancellationFeeService = bcfVacationChangeAndCancellationFeeService;
	}

	public ChangeAndCancellationFeeCalculationHelper getChangeAndCancellationFeeCalculationHelper()
	{
		return changeAndCancellationFeeCalculationHelper;
	}

	public void setChangeAndCancellationFeeCalculationHelper(
			final ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper)
	{
		this.changeAndCancellationFeeCalculationHelper = changeAndCancellationFeeCalculationHelper;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}


	public AccommodationCancellationFeesService getAccommodationCancellationFeesService()
	{
		return accommodationCancellationFeesService;
	}

	public void setAccommodationCancellationFeesService(
			final AccommodationCancellationFeesService accommodationCancellationFeesService)
	{
		this.accommodationCancellationFeesService = accommodationCancellationFeesService;
	}

	public ActivityCancellationFeesService getActivityCancellationFeesService()
	{
		return activityCancellationFeesService;
	}

	public void setActivityCancellationFeesService(
			final ActivityCancellationFeesService activityCancellationFeesService)
	{
		this.activityCancellationFeesService = activityCancellationFeesService;
	}
}
