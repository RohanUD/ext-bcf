/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.service.AccommodationCancellationFeesService;
import com.bcf.core.activity.service.ActivityCancellationFeesService;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationFeesModel;
import com.bcf.core.services.vacation.BcfVacationChangeAndCancellationFeeService;
import com.bcf.core.services.vacation.BcfVacationChangeAndCancellationPolicyService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;


public abstract class BaseFeeStrategy
{

	private BcfVacationChangeAndCancellationFeeService bcfVacationChangeAndCancellationFeeService;
	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;
	private BcfVacationChangeAndCancellationPolicyService bcfVacationChangeAndCancellationPolicyService;
	private AccommodationCancellationFeesService accommodationCancellationFeesService;
	private ActivityCancellationFeesService activityCancellationFeesService;

	protected Pair<Double,String> getPackageChangeFee(AbstractOrderModel order,double totalPrice, RouteType routeType, Date firstTravelDate, int numberOfDays)
	{

		VacationChangeAndCancellationFeesModel vacationChangeAndCancellationFeeModel = bcfVacationChangeAndCancellationFeeService
				.findPackageVacationChangeAndCancellationFee(routeType, firstTravelDate, numberOfDays);

		if (vacationChangeAndCancellationFeeModel != null)
		{
			double packageChangeFee = changeAndCancellationFeeCalculationHelper
					.getFee(order, vacationChangeAndCancellationFeeModel.getChangeFee(),
							vacationChangeAndCancellationFeeModel.getChangeFeeType(),totalPrice);
			String vacationPolicyCode=vacationChangeAndCancellationFeeModel.getPackageVacationChangeAndCancellationPolicy().getCode();

			return Pair.of(packageChangeFee,vacationPolicyCode);
		}

		return null;
	}


	protected  Double getComponentChangeFee(AbstractOrderModel order, Date firstTravelDate, int numberOfDays,
			List<AccommodationOrderEntryGroupModel> inactiveAccommodationEntryGroups,
			List<AbstractOrderEntryModel> inactiveActivityEntries)
	{

		List<ActivityProductModel> activityProductModels = changeAndCancellationFeeCalculationHelper
				.getActivityProducts(inactiveActivityEntries);
		List<AccommodationOfferingModel> accommodationOfferings = changeAndCancellationFeeCalculationHelper
				.getAccommodationOfferingsFromGroup(inactiveAccommodationEntryGroups);

		List<VacationChangeAndCancellationFeesModel> vacationChangeAndCancellationFeeModels = bcfVacationChangeAndCancellationFeeService
				.findComponentVacationChangeAndCancellationFee(firstTravelDate, numberOfDays, accommodationOfferings,
						activityProductModels);

		if (CollectionUtils.isNotEmpty(vacationChangeAndCancellationFeeModels))
		{

			return  changeAndCancellationFeeCalculationHelper.getComponentFees(order, vacationChangeAndCancellationFeeModels,
					changeAndCancellationFeeCalculationHelper.getEntriesFromGroup(inactiveAccommodationEntryGroups),
					inactiveActivityEntries, true);

		}

		return null;
	}

	public BcfVacationChangeAndCancellationPolicyService getBcfVacationChangeAndCancellationPolicyService()
	{
		return bcfVacationChangeAndCancellationPolicyService;
	}

	public void setBcfVacationChangeAndCancellationPolicyService(
			final BcfVacationChangeAndCancellationPolicyService bcfVacationChangeAndCancellationPolicyService)
	{
		this.bcfVacationChangeAndCancellationPolicyService = bcfVacationChangeAndCancellationPolicyService;
	}

	public BcfVacationChangeAndCancellationFeeService getBcfVacationChangeAndCancellationFeeService()
	{
		return bcfVacationChangeAndCancellationFeeService;
	}

	public void setBcfVacationChangeAndCancellationFeeService(
			final BcfVacationChangeAndCancellationFeeService bcfVacationChangeAndCancellationFeeService)
	{
		this.bcfVacationChangeAndCancellationFeeService = bcfVacationChangeAndCancellationFeeService;
	}

	public ChangeAndCancellationFeeCalculationHelper getChangeAndCancellationFeeCalculationHelper()
	{
		return changeAndCancellationFeeCalculationHelper;
	}

	public void setChangeAndCancellationFeeCalculationHelper(
			final ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper)
	{
		this.changeAndCancellationFeeCalculationHelper = changeAndCancellationFeeCalculationHelper;
	}

	public AccommodationCancellationFeesService getAccommodationCancellationFeesService()
	{
		return accommodationCancellationFeesService;
	}

	public void setAccommodationCancellationFeesService(
			final AccommodationCancellationFeesService accommodationCancellationFeesService)
	{
		this.accommodationCancellationFeesService = accommodationCancellationFeesService;
	}

	public ActivityCancellationFeesService getActivityCancellationFeesService()
	{
		return activityCancellationFeesService;
	}

	public void setActivityCancellationFeesService(
			final ActivityCancellationFeesService activityCancellationFeesService)
	{
		this.activityCancellationFeesService = activityCancellationFeesService;
	}
}
