/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services.vacation.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;


public interface CartChangeFeeStrategy
{
	BcfChangeCancellationVacationFeeData calculateChangeFee(AbstractOrderModel order);

	void addNonRefundableCostToBcf(AbstractOrderModel order);

	VacationPolicyTAndCData getVacationPolicyTermsAndConditions(AbstractOrderModel order);

	List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(AbstractOrderModel order);

}
