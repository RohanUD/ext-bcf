/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.services;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.services.TransportFacilityService;
import java.util.List;


public interface BcfTransportFacilityService extends TransportFacilityService
{
	TransportFacilityModel getTransportFacilityForLocation(LocationModel location);

	TransportFacilityModel getTransportFacilityForCode(String travelFacilityCode);

	List<TransportFacilityModel> getTransportFacilityForTerminalCodes(List<String> terminalCodes);

	List<TransportFacilityModel> getAllTransportFacilities();

	SearchPageData<TransportFacilityModel> getPaginatedTransportFacilities(int pageSize, int currentPage);
}
