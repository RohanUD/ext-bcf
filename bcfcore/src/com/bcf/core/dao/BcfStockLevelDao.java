/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.impl.StockLevelDao;
import java.util.Date;
import java.util.List;


// it should extend TravelStockLevelDao and name should be BcfTravelStockLevelDao
public interface BcfStockLevelDao extends StockLevelDao
{
	StockLevelModel fetchProductStockLevel(String productCode);

	StockLevelModel fetchProductStockLevel(String code, Date date);

	List<StockLevelModel> fetchStocks(final Date startDate, final Date endDate);

	List<StockLevelModel> fetchStockLevelsForProductAndDate(final String productCode, final Date date,
			final WarehouseModel warehouse);

	List<StockLevelModel> fetchAccommodationStockLevels(Date startDate, Date endDate);

	List<StockLevelModel> fetchActivityStockLevels(Date startDate, Date endDate);
}
