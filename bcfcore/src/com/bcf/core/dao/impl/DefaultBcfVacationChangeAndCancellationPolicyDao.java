/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfVacationChangeAndCancellationPolicyDao;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationPolicyModel;


public class DefaultBcfVacationChangeAndCancellationPolicyDao extends DefaultGenericDao<VacationChangeAndCancellationPolicyModel>
		implements
		BcfVacationChangeAndCancellationPolicyDao
{
	public DefaultBcfVacationChangeAndCancellationPolicyDao(final String typecode)
	{
		super(typecode);
	}

	private static final String SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY =
			"SELECT {" + VacationChangeAndCancellationPolicyModel.PK + "} from {"
					+ VacationChangeAndCancellationPolicyModel._TYPECODE
					+ " as VP ";

	private static final String BETWEEN_STARTDATE_AND_ENDDATE =
			"{VP:" + VacationChangeAndCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate"
					+ " and {VP:" + VacationChangeAndCancellationPolicyModel.ENDDATE + "} >= ?firstTravelDate";

	private static final String VACATION_ROUTETYPE_POLICY_ROW =
			SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY + " } where {VP:"
					+ VacationChangeAndCancellationPolicyModel.ROUTETYPE + "} = ?" + VacationChangeAndCancellationPolicyModel.ROUTETYPE
					+ " and " + BETWEEN_STARTDATE_AND_ENDDATE;

	private static String VACATION_ACCOMMODATION_POLICY_ROW = SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ " join VacationChangeAndCancellationPolicy2AccommodationOfferingRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?accommodationOfferings)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE;

	private static String VACATION_ACTIVITY_POLICY_ROW = SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ " join VacationChangeAndCancellationPolicy2ActivityProductRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?activityProducts)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE;

	private static String VACATION_ACCOMMODATION_ACTIVITY_POLICY_ROW = "SELECT tbl.pk from ({{"
			+ SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ " join VacationChangeAndCancellationPolicy2AccommodationOfferingRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?accommodationOfferings)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE
			+ " }} UNION {{"
			+ SELECT_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ " join VacationChangeAndCancellationPolicy2ActivityProductRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?activityProducts)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE
			+ " }})tbl";


	@Override
	public VacationChangeAndCancellationPolicyModel getVacationPolicy(String code)
	{

		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(VacationChangeAndCancellationPolicyModel.CODE, code);
		final List<VacationChangeAndCancellationPolicyModel> vacationPolicies = find(params);
		if (!vacationPolicies.isEmpty())
		{
			return vacationPolicies.get(0);
		}
		return null;
	}


	@Override
	public List<VacationChangeAndCancellationPolicyModel> findComponentVacationChangeAndCancellationPolicy(Date firstTravelDate,
			List<AccommodationOfferingModel> accommodationOfferings, List<ActivityProductModel> activityProducts)
	{

		final Map<String, Object> params = new HashMap<>();
		String query = null;
		if (CollectionUtils.isEmpty(activityProducts))
		{
			query = VACATION_ACCOMMODATION_POLICY_ROW;
			params.put("accommodationOfferings", accommodationOfferings);
		}
		else if (CollectionUtils.isEmpty(accommodationOfferings))
		{
			query = VACATION_ACTIVITY_POLICY_ROW;
			params.put("activityProducts", activityProducts);
		}
		else
		{
			query = VACATION_ACCOMMODATION_ACTIVITY_POLICY_ROW;
			params.put("accommodationOfferings", accommodationOfferings);
			params.put("activityProducts", activityProducts);
		}
		params.put("firstTravelDate", firstTravelDate);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<VacationChangeAndCancellationPolicyModel> result = getFlexibleSearchService()
				.search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;

	}

	@Override
	public VacationChangeAndCancellationPolicyModel findPackageVacationChangeAndCancellationPolicy(Date firstTravelDate,
			RouteType routeType)
	{

		final Map<String, Object> params = new HashMap<>();
		params.put(VacationChangeAndCancellationPolicyModel.ROUTETYPE, routeType);
		params.put("firstTravelDate", firstTravelDate);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(VACATION_ROUTETYPE_POLICY_ROW, params);
		final SearchResult<VacationChangeAndCancellationPolicyModel> searchResult = this.getFlexibleSearchService()
				.search(flexibleSearchQuery);
		if (searchResult != null)
		{
			return searchResult.getResult().size() > 0 ? searchResult.getResult().get(0) : null;
		}
		return null;

	}

}
