/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.dao.TaxRowDao;


public class DefaultTaxRowDao extends DefaultGenericDao<TaxRowModel> implements TaxRowDao
{
	private static final Logger LOG = Logger.getLogger(DefaultTaxRowDao.class);

	private static final String TAX_CODES = "taxCodes";
	private static final String LOCATION_CODES = "locationCodes";
	private static final String CURRENT_DATE = "currentDate";

	private static final String SELECT_TAX_ROW_MODEL = "SELECT {" + TaxRowModel.PK + "} from {" + TaxRowModel._TYPECODE
			+ " as TR JOIN " + TaxModel._TYPECODE + " as T ON {TR:" + TaxRowModel.TAX + "}={T:" + TaxModel.PK + "}} WHERE ";

	private static final String BETWEEN_STARTDATE_AND_ENDDATE = "AND (({"
			+ TaxRowModel.STARTTIME + "} <= ?" + CURRENT_DATE + " AND {" + TaxRowModel.ENDTIME + "} >= ?" + CURRENT_DATE + ") OR ({"
			+ TaxRowModel.STARTTIME + "} IS NULL AND {" + TaxRowModel.ENDTIME + "} IS NULL))";

	private static final String TAX_ROW_LOCATION = "{TR:"
			+ TaxRowModel.LOCATION + "} IN (?" + LOCATION_CODES + ")";

	private static final String TAX_ROW_CODE = "{T:"
			+ TaxModel.CODE + "} IN (?" + TAX_CODES + ")";

	private static final String TAXROW_FOR_TAX_AND_LOCATION =
			SELECT_TAX_ROW_MODEL + " (" + TAX_ROW_CODE + " AND " + TAX_ROW_LOCATION + ")"
					+ BETWEEN_STARTDATE_AND_ENDDATE;

	private static final String TAXROW_FOR_LOCATION =
			SELECT_TAX_ROW_MODEL + " (" + TAX_ROW_LOCATION + ")" + BETWEEN_STARTDATE_AND_ENDDATE;

	public DefaultTaxRowDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<TaxRowModel> findTaxRow(final List<String> taxCodes, final List<String> locationCodes)
	{
		validateParameterNotNull(taxCodes, "tax code must not null!");
		validateParameterNotNull(locationCodes, "location code must not null!");

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(TAXROW_FOR_TAX_AND_LOCATION);
		fQuery.addQueryParameter(TAX_CODES, taxCodes);
		fQuery.addQueryParameter(LOCATION_CODES, locationCodes);
		fQuery.addQueryParameter(CURRENT_DATE, new Date());

		final SearchResult<TaxRowModel> result = getFlexibleSearchService().search(fQuery);
		final List<TaxRowModel> taxRows = result.getResult();
		// no tax row defined
		if (CollectionUtils.isEmpty(taxRows))
		{
			LOG.warn("No tax row found for tax codes: " + taxCodes.stream().collect(Collectors.joining(","))
					+ " and location codes: " + locationCodes.stream().collect(Collectors.joining(",")));
			return Collections.emptyList();
		}

		final List<TaxRowModel> filteredTaxRows = new ArrayList<>();
		//fetching tax row based on tax code
		for (final String taxCode : taxCodes)
		{
			// find price row with date range applicable
			TaxRowModel filteredTaxRow = taxRows.stream()
					.filter(taxRow -> ((StringUtils.equals(taxCode, taxRow.getTax().getCode()))
							&& (Objects.nonNull(taxRow.getStartTime()) && Objects.nonNull(taxRow.getEndTime()))))
					.findFirst().orElse(null);
			if (Objects.isNull(filteredTaxRow))
			{
				// find price row with no date range applicable
				filteredTaxRow = taxRows.stream().filter(taxRow -> StringUtils.equals(taxCode, taxRow.getTax().getCode())).findFirst()
						.orElse(null);
			}
			if (Objects.nonNull(filteredTaxRow))
			{
				filteredTaxRows.add(filteredTaxRow);
			}
		}
		return filteredTaxRows;
	}

	@Override
	public List<TaxRowModel> findTaxRow(final List<String> locationCodes)
	{
		validateParameterNotNull(locationCodes, "location code must not null!");

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(TAXROW_FOR_LOCATION);
		fQuery.addQueryParameter(LOCATION_CODES, locationCodes);
		fQuery.addQueryParameter(CURRENT_DATE, new Date());

		final SearchResult<TaxRowModel> result = getFlexibleSearchService().search(fQuery);
		final List<TaxRowModel> taxRows = result.getResult();
		// no tax row defined
		if (CollectionUtils.isEmpty(taxRows))
		{
			LOG.warn("No tax row found for location codes: " + locationCodes.stream().collect(Collectors.joining(",")));
			return Collections.emptyList();
		}

		final List<TaxRowModel> filteredTaxRows = new ArrayList<>();
		// find price row with date range applicable
		final TaxRowModel filteredTaxRow = taxRows.stream()
				.filter(taxRow -> (Objects.nonNull(taxRow.getStartTime()) && Objects.nonNull(taxRow.getEndTime())))
				.findFirst().orElse(null);
		if (Objects.nonNull(filteredTaxRow))
		{
			filteredTaxRows.add(filteredTaxRow);
		}
		return filteredTaxRows;
	}
}
