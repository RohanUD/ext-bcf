/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfProductDao;
import com.bcf.core.enums.FareProductType;


public class DefaultBcfProductDao extends DefaultGenericDao<FareProductModel> implements BcfProductDao
{
	private static final String ANCILLARY_PRODUCTS_BY_CODES_QUERY = "SELECT {"
			+ AncillaryProductModel.PK + "} FROM {" + AncillaryProductModel._TYPECODE + "}" + " WHERE {"
			+ AncillaryProductModel.CODE + "} IN (?" + AncillaryProductModel.CODE + ")";

	public DefaultBcfProductDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public FareProductModel findFareProductForFareBasisCode(final String fareBasisCode, final FareProductType fareProductType)
	{
		validateParameterNotNull(fareBasisCode, "fareBasisCode must not be null!");
		validateParameterNotNull(fareProductType, "fareProductType must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(FareProductModel.FAREPRODUCTTYPE, fareProductType);
		params.put(FareProductModel.FAREBASISCODE, fareBasisCode);

		final List<FareProductModel> fareProductModelList = find(params);
		return CollectionUtils.isNotEmpty(fareProductModelList) ? fareProductModelList.get(0) : null;
	}

	@Override
	public List<AncillaryProductModel> findAncillaryProductsForCodes(final List<String> productCodes)
	{
		validateParameterNotNull(productCodes, "productCodes must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AncillaryProductModel.CODE, productCodes);
		final SearchResult<AncillaryProductModel> searchResults = getFlexibleSearchService()
				.search(ANCILLARY_PRODUCTS_BY_CODES_QUERY, params);
		return searchResults.getResult();
	}
}
