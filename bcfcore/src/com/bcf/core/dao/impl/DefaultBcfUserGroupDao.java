/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserGroupDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.dao.BcfUserGroupDao;


public class DefaultBcfUserGroupDao extends DefaultUserGroupDao implements BcfUserGroupDao
{
	@Override
	public List<UserGroupModel> getUserGroups(final List<String> groupIds)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("groupIds", groupIds);
		final StringBuilder query = new StringBuilder(
				"SELECT {" + UserGroupModel.PK + "} FROM  {" + UserGroupModel._TYPECODE + "} WHERE {" + UserGroupModel.UID
						+ "} IN (?groupIds)");
		final SearchResult<UserGroupModel> results = getFlexibleSearchService().search(query.toString(),
				params);
		return results.getResult();
	}

	@Override
	public List<UserModel> getUsersByGroup(final List<String> groups)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("groups", groups);

		final StringBuilder query = new StringBuilder("SELECT {user.pk} FROM {PrincipalGroupRelation AS pgr "
				+ "JOIN User AS user ON {pgr.source} = {user.pk} "
				+ "JOIN Usergroup AS ug ON {pgr.target} = {ug.pk}} "
				+ "WHERE {ug.uid} IN (?groups)");

		final SearchResult<UserModel> results = getFlexibleSearchService().search(query.toString(),
				params);
		return results.getResult();
	}
}
