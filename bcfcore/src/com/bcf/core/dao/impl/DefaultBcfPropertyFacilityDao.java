/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.dao.impl.DefaultPropertyFacilityDao;
import de.hybris.platform.travelservices.enums.PropertyFacilityType;
import de.hybris.platform.travelservices.model.facility.PropertyFacilityModel;
import java.util.HashMap;
import java.util.Map;
import com.bcf.core.dao.BcfPropertyFacilityDao;


public class DefaultBcfPropertyFacilityDao extends DefaultPropertyFacilityDao implements BcfPropertyFacilityDao
{
	private static final String FIND_PROPERTY_FACILITY_FROM_TYPE = "SELECT {c.pk} FROM {PropertyFacility AS c JOIN EnumerationValue AS ev ON {c.type}={ev.PK}} WHERE {ev.code}=?code";

	public DefaultBcfPropertyFacilityDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public PropertyFacilityModel findPropertyFacilityFromType(final PropertyFacilityType type)
	{
		validateParameterNotNull(type, "type must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("code", type.getCode());
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_PROPERTY_FACILITY_FROM_TYPE, queryParams);
		final SearchResult<PropertyFacilityModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		if (searchResult != null)
		{
			return searchResult.getResult().size() > 0 ? searchResult.getResult().get(0) : null;
		}
		return null;
	}
}
