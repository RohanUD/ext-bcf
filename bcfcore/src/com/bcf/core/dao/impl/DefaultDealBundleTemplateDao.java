/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.dao.DealBundleTemplateDao;


public class DefaultDealBundleTemplateDao extends DefaultGenericDao<DealBundleTemplateModel> implements DealBundleTemplateDao
{

	public DefaultDealBundleTemplateDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public DealBundleTemplateModel findDealBundleTemplateBySeoUrl(final String seoUrl,final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(seoUrl, "seoUrl must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(DealBundleTemplateModel.SEOURL, seoUrl);
		params.put(DealBundleTemplateModel.CATALOGVERSION, catalogVersion);
		final List<DealBundleTemplateModel> dealBundleTemplateModels = find(params);
		if (!dealBundleTemplateModels.isEmpty())
		{
			return dealBundleTemplateModels.get(0);
		}
		return null;
	}
}
