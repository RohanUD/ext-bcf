/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.dao.impl.DefaultTravelLocationDao;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.dao.BcfTravelLocationDao;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.pagination.GlobalPaginationResultsBuilder;


public class DefaultBcfTravelLocationDao extends DefaultTravelLocationDao implements BcfTravelLocationDao
{
	private static final String FIND_BCFVACATIONLOCATION_ONLY = "SELECT {pk} FROM {BcfVacationLocation!} ORDER BY {sortOrder} ASC";
	private static final String FIND_BCFVACATIONLOCATION_BY_CODE = "SELECT {pk} FROM {BcfVacationLocation} where {code}=?code";
	private static final String FIND_BCFVACATIONLOCATION_BY_PARENT_LOCATION_CODE = "SELECT {pk} FROM {BcfVacationLocation as BV join BcfVacationLocation as PBV on {BV.parentLocation}={PBV.pk}} where {PBV.code}=?code";
	private static final String FIND_SUB_LOCATIONS = "SELECT {target} FROM {LocationLocationRelation} where {source} = ?source";
	private static final String FIND_BCF_VACATION_SUB_LOCATIONS = "SELECT {target} FROM {BcfVacationLocation} where {parentLocation} = ?source";
	private static final String FIND_BCFVACATIONLOCATION_BY_ORIGINAL_LOCATION = "SELECT {pk} FROM {BcfVacationLocation} where {originalLocation} = ?originalLocation";
	private static final String FIND_ACCOMMODATIONOFFERING_BY_BCF_LOCATION = "SELECT {pk} FROM {Accommo} where {originalLocation} = ?originalLocation";


	private static final String FIND_BCFLOCATIONS_ONLY =
			"SELECT {pk} FROM {" + BcfVacationLocationModel._TYPECODE + " AS l JOIN " + LocationType._TYPECODE
					+ " AS lt on {l.locationType}={lt.pk}} where {lt.code} = ?locationType";
	private static final String FIND_POS_FOR_NAME = "SELECT {pk} FROM {" + PointOfServiceModel._TYPECODE + " AS p} where {p.name} = ?name";;

	private GlobalPaginationResultsBuilder globalPaginationResultsBuilder;

	public DefaultBcfTravelLocationDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public LocationModel findLocationByName(final String name)
	{
		validateParameterNotNull(name, "location name must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(LocationModel.NAME, name);
		final List<LocationModel> locationModels = find(params);
		if (CollectionUtils.isNotEmpty(locationModels))
		{
			return locationModels.stream().findFirst().orElse(null);
		}

		return null;
	}



	@Override
	public List<BcfVacationLocationModel> findAllBcfVacationLocations()
	{
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_BCFVACATIONLOCATION_ONLY);
		final SearchResult<BcfVacationLocationModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<LocationModel> getSubLocations(final LocationModel locationModel)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("source", locationModel);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_SUB_LOCATIONS, params);
		final SearchResult<LocationModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<BcfVacationLocationModel> getBcfVacationSubLocations(final BcfVacationLocationModel locationModel)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("source", locationModel);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_BCF_VACATION_SUB_LOCATIONS, params);
		final SearchResult<BcfVacationLocationModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<BcfVacationLocationModel> getLocationsByType(final String locationType)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_BCFLOCATIONS_ONLY);
		fQuery.addQueryParameter("locationType", locationType);
		final SearchResult<BcfVacationLocationModel> searchResult = getFlexibleSearchService().search(fQuery);
		return searchResult.getResult();
	}

	@Override
	public SearchPageData<BcfVacationLocationModel> getPaginatedLocationsByType(final String locationType, final int pageSize,
			final int currentPage)
	{
		return getGlobalPaginationResultsBuilder()
				.createPaginatedResults(FIND_BCFLOCATIONS_ONLY,
						Collections.singletonMap(BcfVacationLocationModel.LOCATIONTYPE, locationType), pageSize,
						currentPage);
	}

	@Override
	public SearchPageData<BcfVacationLocationModel> getPaginatedCitiesByRegion(final String regionCode, final int pageSize,
			final int currentPage)
	{
		return getGlobalPaginationResultsBuilder().createPaginatedResults(FIND_BCFVACATIONLOCATION_BY_PARENT_LOCATION_CODE,
				Collections.singletonMap(BcfVacationLocationModel.CODE, regionCode), pageSize, currentPage);
	}

	@Override
	public BcfVacationLocationModel findBcfVacationLocationByCode(final String code)
	{
		validateParameterNotNull(code, "location name must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfVacationLocationModel.CODE, code);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_BCFVACATIONLOCATION_BY_CODE, params);
		final SearchResult<BcfVacationLocationModel> searchResult = getFlexibleSearchService().search(fsq);
		final List<BcfVacationLocationModel> locationModels = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(locationModels))
		{
			return locationModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public BcfVacationLocationModel findBcfVacationLocationByOriginalLocation(final LocationModel location)
	{
		validateParameterNotNull(location, "location name must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfVacationLocationModel.ORIGINALLOCATION, location);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_BCFVACATIONLOCATION_BY_ORIGINAL_LOCATION, params);
		final SearchResult<BcfVacationLocationModel> searchResult = getFlexibleSearchService().search(fsq);
		final List<BcfVacationLocationModel> locationModels = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(locationModels))
		{
			return locationModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public LocationModel findLocationForCodeAndType(final String code, final LocationType locationType)
	{
		ServicesUtil.validateParameterNotNull(code, "Location code must not be null!");
		ServicesUtil.validateParameterNotNull(locationType, "Location Type must not be null!");

		final Map<String, Object> params = new HashMap<>();
		params.put(LocationModel.CODE, code);
		params.put(LocationModel.LOCATIONTYPE, locationType);
		final List<LocationModel> locationModels = this.find(params);
		if (CollectionUtils.isNotEmpty(locationModels))
		{
			return locationModels.get(0);
		}
		return null;
	}

	@Override
	public PointOfServiceModel findPointOfServiceForName(final String name)
	{
		Assert.notNull(name,"pos name should not be null");

		final Map<String, Object> paramMap = new HashMap();
		paramMap.put(PointOfServiceModel.NAME, name);
		paramMap.put(PointOfServiceModel.TYPE, PointOfServiceTypeEnum.STORE);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_POS_FOR_NAME, paramMap);
		final SearchResult<PointOfServiceModel> searchResult =  getFlexibleSearchService().search(fsq);

		if (CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			return searchResult.getResult().get(0);
		}
		return null;
	}

	public GlobalPaginationResultsBuilder getGlobalPaginationResultsBuilder()
	{
		return globalPaginationResultsBuilder;
	}

	@Required
	public void setGlobalPaginationResultsBuilder(final GlobalPaginationResultsBuilder globalPaginationResultsBuilder)
	{
		this.globalPaginationResultsBuilder = globalPaginationResultsBuilder;
	}
}
