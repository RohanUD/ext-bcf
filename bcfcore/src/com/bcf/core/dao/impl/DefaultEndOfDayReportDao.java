/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.core.dao.EndOfDayReportDao;
import com.bcf.core.model.DeclareOrderEntryModel;


public class DefaultEndOfDayReportDao extends DefaultGenericDao<PaymentTransactionEntryModel> implements EndOfDayReportDao
{
	public static final String DECLARE_START_TIME = "declareStartTime";
	public static final String DECLARE_END_TIME = "declareEndTime";

	private static final String PAYMENT_ENTRY_PREFIX_QUERY = "SELECT {pte.PK} FROM "
			+ "{PaymentTransactionEntry AS pte JOIN PaymentTransaction AS pt ON {pt.pk} = {pte.paymenttransaction} "
			+ "JOIN Order AS ord ON {ord.pk} = {pt.order} "
			+ "JOIN BookingJourneyType AS bkgtype ON {bkgtype.pk} = {ord.bookingJourneyType}}";

	private static final String DECLARE_ENTRY_PREFIX_QUERY = "SELECT {doe.pk} FROM "
			+ "{DeclareOrderEntry AS doe JOIN Order AS ord ON {ord.code} = {doe.orderCode} "
			+ "JOIN BookingJourneyType AS bkgtype ON {bkgtype.pk} = {ord.bookingJourneyType}}";

	private static final String BOOKING_JOURNEY_FILTER_QUERY = " AND {bkgtype.code} "
			+ "IN ('BOOKING_PACKAGE', 'BOOKING_ALACARTE', 'BOOKING_ACTIVITY_ONLY', 'BOOKING_ACCOMMODATION_ONLY')";

	private static final String FIND_PAYMENTTRANSACTIONS_FOR_AGENT_DECLARETIME =
			PAYMENT_ENTRY_PREFIX_QUERY +
					" WHERE {pte." + PaymentTransactionEntryModel.AGENT + "}=?" + PaymentTransactionEntryModel.AGENT
					+ " AND {pte." + PaymentTransactionEntryModel.CREATIONTIME + "} >= ?declareStartTime"
					+ " AND {pte." + PaymentTransactionEntryModel.CREATIONTIME + "} <= ?declareEndTime"
					+ BOOKING_JOURNEY_FILTER_QUERY;

	private static final String FIND_ORDER_DECLARE_REPORT_FOR_AGENT_DECLARETIME =
			DECLARE_ENTRY_PREFIX_QUERY
					+ " WHERE {doe."
					+   DeclareOrderEntryModel.AGENT + "}=?" + PaymentTransactionEntryModel.AGENT
					+ " AND {doe." + DeclareOrderEntryModel.CREATIONTIME + "}>=?declareStartTime"
					+ " AND {doe." + DeclareOrderEntryModel.CREATIONTIME + "}<=?declareEndTime"
					+ BOOKING_JOURNEY_FILTER_QUERY;

	private static final String FIND_PAYMENTENTRIES_REPORT_FOR_WEB =
			PAYMENT_ENTRY_PREFIX_QUERY +
					" WHERE {pte."
					+ PaymentTransactionEntryModel.AGENT + "} IS NULL "
					+ " AND {pte." + PaymentTransactionEntryModel.CREATIONTIME + "}>=?endOfDayDeclareStartTime"
					+ " AND {pte." + PaymentTransactionEntryModel.CREATIONTIME + "}<=?endOfDayDeclareEndTime"
					+ BOOKING_JOURNEY_FILTER_QUERY;

	private static final String FIND_DECLARE_REPORT_FOR_WEB =
			DECLARE_ENTRY_PREFIX_QUERY
					+ " WHERE {doe."
					+ DeclareOrderEntryModel.AGENT + "} IS NULL "
					+ " AND {doe." + DeclareOrderEntryModel.CREATIONTIME + "}>=?endofdayDeclareStartTime"
					+ " AND {doe." + DeclareOrderEntryModel.CREATIONTIME + "}<=?endofdayDeclareEndTime"
					+ BOOKING_JOURNEY_FILTER_QUERY;

	public DefaultEndOfDayReportDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForAgent(final UserModel agent,
			final Date declareStartTime, final Date declareEndTime)
	{
		validateParameterNotNull(agent, "type must not be null!");

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(PaymentTransactionEntryModel.AGENT, agent);
		queryParams.put(DECLARE_START_TIME, declareStartTime);
		queryParams.put(DECLARE_END_TIME, declareEndTime);
		FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_PAYMENTTRANSACTIONS_FOR_AGENT_DECLARETIME,
				queryParams);
		final SearchResult<PaymentTransactionEntryModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult != null && searchResult.getResult().size() > 0 ? searchResult.getResult() : null;
	}

	@Override
	public List<DeclareOrderEntryModel> findDeclareOrderEntriesForAgent(final UserModel agent,
			final Date declareStartTime, final Date declareEndTime)
	{
		validateParameterNotNull(agent, "type must not be null!");

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(DeclareOrderEntryModel.AGENT, agent);
		queryParams.put(DECLARE_START_TIME, declareStartTime);
		queryParams.put(DECLARE_END_TIME, declareEndTime);
		FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_ORDER_DECLARE_REPORT_FOR_AGENT_DECLARETIME,
				queryParams);
		final SearchResult<DeclareOrderEntryModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult != null && searchResult.getResult().size() > 0 ? searchResult.getResult() : null;
	}

	@Override
	public List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForWebJob(final Date endofdayDeclareStartTime,
			final Date endofdayDeclareEndTime)
	{
		validateParameterNotNull(endofdayDeclareStartTime, "type must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("endOfDayDeclareStartTime", endofdayDeclareStartTime);
		queryParams.put("endOfDayDeclareEndTime", endofdayDeclareEndTime);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_PAYMENTENTRIES_REPORT_FOR_WEB, queryParams);
		final SearchResult<PaymentTransactionEntryModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult != null && searchResult.getResult().size() > 0 ? searchResult.getResult() : null;
	}

	@Override
	public List<DeclareOrderEntryModel> findDeclareOrderEntriesForWebJob(Date endofdayDeclareStartTime,
			Date endofdayDeclareEndTime)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("endOfDayDeclareStartTime", endofdayDeclareStartTime);
		queryParams.put("endofdayDeclareEndTime", endofdayDeclareEndTime);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_DECLARE_REPORT_FOR_WEB, queryParams);
		final SearchResult<DeclareOrderEntryModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult != null && searchResult.getResult().size() > 0 ? searchResult.getResult() : null;
	}
}
