/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.dao.impl.DefaultPassengerTypeDao;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BCFPassengerTypeDao;



public class DefaultBCFPassengerTypeDao extends DefaultPassengerTypeDao implements BCFPassengerTypeDao
{
	private static final String FIND_PASSENGER_TYPE_ONLY =
			"SELECT {pk} FROM {PassengerType!}";

	private static final String FIND_PASSENGER_PK = "SELECT {" + PassengerTypeModel.PK + "}";
	private static final String FROM_PASSENGER_MODEL = " FROM {" + PassengerTypeModel._TYPECODE + "}";

	private static final String FIND_PASSENGER_BY_CODES_QUERY =
			FIND_PASSENGER_PK + FROM_PASSENGER_MODEL + " WHERE {" + PassengerTypeModel.CODE + "} IN (?codes)";

	private static final String FIND_PASSENGER_BY_CODE_QUERY =
			FIND_PASSENGER_PK + FROM_PASSENGER_MODEL + " WHERE {" + PassengerTypeModel.CODE + "} = ?code";

	private static final String FIND_PASSENGER_BY_EBOOKING_CODE_QUERY =
			FIND_PASSENGER_PK + " FROM {" + PassengerTypeModel._TYPECODE
					+ "!} WHERE {" + PassengerTypeModel.EBOOKINGCODE + "} = ?eBookingCode";

	private static final String FIND_MIN_AGE_BY_PASSENGER_CODES_QUERY =
			"SELECT {" + PassengerTypeModel.MINAGE + "}" + FROM_PASSENGER_MODEL + " WHERE {" + PassengerTypeModel.CODE + "} = ?code";

	public DefaultBCFPassengerTypeDao(final String typecode)
	{
		super(typecode);

	}

	@Override
	public PassengerTypeModel findPassengerModelForEBookingCode(final String eBookingCode)
	{
		ServicesUtil.validateParameterNotNull(eBookingCode, "PassengerType EBookingCode eBookingCode must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("eBookingCode", eBookingCode);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_PASSENGER_BY_EBOOKING_CODE_QUERY, queryParams);
		final SearchResult<PassengerTypeModel> searchResult = this.getFlexibleSearchService().search(fsq);
		final List<PassengerTypeModel> passengerTypeModels = searchResult.getResult();
		if (passengerTypeModels.size() > 1)
		{
			throw new AmbiguousIdentifierException("More than one configuration found");
		}
		final Optional passengerTypeModel = CollectionUtils.isNotEmpty(passengerTypeModels)
				? passengerTypeModels.stream().findFirst() : null;
		return Objects.nonNull(passengerTypeModel) && passengerTypeModel.isPresent() ? (PassengerTypeModel) passengerTypeModel.get()
				: null;
	}

	@Override
	public List<PassengerTypeModel> findPassengerTypes()
	{
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_PASSENGER_TYPE_ONLY);
		final SearchResult<PassengerTypeModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<PassengerTypeModel> findPassengerTypesForCodes(final List<String> codes)
	{
		validateParameterNotNull(codes, "codes must not be null!");

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("codes", codes);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_PASSENGER_BY_CODES_QUERY, queryParams);
		final SearchResult<PassengerTypeModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public PassengerTypeModel findPassengerTypesForCode(final String code)
	{
		validateParameterNotNull(code, "codes must not be null!");

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("code", code);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_PASSENGER_BY_CODE_QUERY, queryParams);
		final SearchResult<PassengerTypeModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getCount() > 0 ? searchResult.getResult().get(0) : null;
	}

	@Override
	public Integer findMinAgeByPassengerTypeCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "PassengerType code must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("code", code);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_MIN_AGE_BY_PASSENGER_CODES_QUERY);
		fsq.addQueryParameters(queryParams);
		fsq.setResultClassList(Collections.singletonList(Integer.class));
		final SearchResult<Integer> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult().get(0);
	}
}
