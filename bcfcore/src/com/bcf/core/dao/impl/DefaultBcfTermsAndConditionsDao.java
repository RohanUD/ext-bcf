/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfTermsAndConditionsDao;
import com.bcf.core.model.ferry.FerryTandCModel;


public class DefaultBcfTermsAndConditionsDao extends DefaultGenericDao<FerryTandCModel> implements BcfTermsAndConditionsDao
{
	private static final String FIND_TERMS_CONDITIONS_BY_CODES_QUERY =
			"SELECT {" + FerryTandCModel.PK + "} FROM {" + FerryTandCModel._TYPECODE
					+ "} WHERE {" + FerryTandCModel.CODE + "} IN (?codes)";

	public DefaultBcfTermsAndConditionsDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public FerryTandCModel findTandCModelByCode(final String code)
	{
		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(FerryTandCModel.CODE, code);
		final List<FerryTandCModel> ferryTandCModels = this.find(params);
		return CollectionUtils.isNotEmpty(ferryTandCModels)
				? ferryTandCModels.stream().findFirst().get() : null;
	}

	@Override
	public List<FerryTandCModel> findTandCModelsByCodes(final List<String> codes)
	{
		validateParameterNotNull(codes, "codes must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("codes", codes);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_TERMS_CONDITIONS_BY_CODES_QUERY, queryParams);
		final SearchResult<FerryTandCModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}

