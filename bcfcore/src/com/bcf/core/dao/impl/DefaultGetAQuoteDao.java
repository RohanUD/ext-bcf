/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.ticket.model.CsTicketModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.GetAQuoteDao;
import com.bcf.facades.getaquote.data.CsTicketListAndCountData;


public class DefaultGetAQuoteDao implements GetAQuoteDao
{
	private static final String query = "SELECT {" + CsTicketModel.PK + "} FROM {" + CsTicketModel._TYPECODE
			+ " as ct JOIN CsTicketState as cs on {ct.state}={cs.pk} join CsTicketCategory as cc on {ct.Category}={cc.pk}} WHERE {cs.Code} IN (?state) AND {cc.Code}=?category";
	private FlexibleSearchService flexibleSearchService;

	@Override
	public CsTicketListAndCountData getOpenTickets(final int startIndex, final int pageNumber,
			final int recordsPerPage, final List<String> status, final String category)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		final CsTicketListAndCountData ticketsAndCountData = new CsTicketListAndCountData();
		fQuery.addQueryParameter(CsTicketModel.STATE, status);
		fQuery.addQueryParameter(CsTicketModel.CATEGORY, category);
		fQuery.setCount(recordsPerPage);
		fQuery.setNeedTotal(true);
		fQuery.setStart(startIndex);
		final SearchResult<CsTicketModel> searchResult = flexibleSearchService.search(fQuery);
		ticketsAndCountData.setCount(searchResult.getTotalCount());
		ticketsAndCountData.setCsTicketList(searchResult.getResult());
		return ticketsAndCountData;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
