/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 9/7/19 1:22 PM
 */

package com.bcf.core.dao.impl;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.core.dao.BcfOptionBookingDao;


public class DefaultBcfOptionBookingDao extends AbstractItemDao implements BcfOptionBookingDao
{

	private static final String ORDER_STATUS_NOT_IN_CONDITION = "AND ({c:status} IS NULL OR {c:status} NOT IN (?status))";

	private static final String ORDER_STATUS_IN_CONDITION = "AND {c:status} = ?status";

	private static final String FIND_OPTION_BOOKING_QUERY =
			"SELECT {pk} FROM {Cart AS c} WHERE {c.vacationOptionBooking} = ?isOptionBooking AND {c.optionBookingExpiryDate} > ?currentDateTime "
					+ ORDER_STATUS_NOT_IN_CONDITION;

	private static final String FIND_ACTIVE_BUT_EXPIRED_OPTION_BOOKING_QUERY =
			"SELECT {pk} FROM {Cart AS c} WHERE {c.vacationOptionBooking} = ?isOptionBooking AND {c.optionBookingExpiryDate} < ?currentDateTime "
					+ ORDER_STATUS_NOT_IN_CONDITION;

	private static final String FIND_EXPIRED_OPTION_BOOKING_QUERY =
			"SELECT {pk} FROM {Cart AS c} WHERE {c.vacationOptionBooking} = ?isOptionBooking AND {c.optionBookingReleaseDate} < ?purgeDate "
					+ ORDER_STATUS_IN_CONDITION;

	@Resource(name = "pagedFlexibleSearchService")
	private PagedFlexibleSearchService pagedFlexibleSearchService;

	@Override
	public SearchPageData<CartModel> findPagedOptionBookings(final PageableData pageableData)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("isOptionBooking", Boolean.TRUE);
		params.put("currentDateTime", new Date());
		params.put(AbstractOrderModel.STATUS, Collections.singletonList(OrderStatus.CANCELLED));

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_OPTION_BOOKING_QUERY, params);
		return getPagedFlexibleSearchService().search(query, pageableData);
	}

	@Override
	public List<CartModel> findActiveOptionBookingsThatAreExpired()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("isOptionBooking", Boolean.TRUE);
		params.put("currentDateTime", new Date());
		params.put(AbstractOrderModel.STATUS, Collections.singletonList(OrderStatus.CANCELLED));

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ACTIVE_BUT_EXPIRED_OPTION_BOOKING_QUERY, params);
		final SearchResult<CartModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<CartModel> findExpiredOptionBookingsAfterDate(final Date purgeDate)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("isOptionBooking", Boolean.TRUE);
		params.put("purgeDate", new Date());
		params.put(AbstractOrderModel.STATUS, Collections.singletonList(OrderStatus.CANCELLED));

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_EXPIRED_OPTION_BOOKING_QUERY, params);
		final SearchResult<CartModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	public PagedFlexibleSearchService getPagedFlexibleSearchService()
	{
		return pagedFlexibleSearchService;
	}

	public void setPagedFlexibleSearchService(
			final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}
}
