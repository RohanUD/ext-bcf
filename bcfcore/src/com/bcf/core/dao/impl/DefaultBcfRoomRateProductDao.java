/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfRoomRateProductDao;


public class DefaultBcfRoomRateProductDao extends DefaultGenericDao<RoomRateProductModel> implements BcfRoomRateProductDao
{
   public DefaultBcfRoomRateProductDao(final String typecode)
   {
      super(typecode);
   }

   @Override
   public RoomRateProductModel findRoomRateProductByCode(final String code)
   {
      validateParameterNotNull(code, "RoomRateProduct code must not be null!");
      final Map<String, Object> params = new HashMap<>();
      params.put(RoomRateProductModel.CODE, code);
      final List<RoomRateProductModel> roomRateProductModelList = find(params);
      return CollectionUtils.isNotEmpty(roomRateProductModelList) ? roomRateProductModelList.get(0) : null;
   }

}
