/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.ActivityProductDao;
import com.bcf.core.model.product.ActivityProductModel;


public class DefaultActivityProductDao extends DefaultGenericDao<ActivityProductModel> implements ActivityProductDao
{
	public DefaultActivityProductDao(final String typecode)
	{
		super(typecode);
	}


	private static final String FIND_ACTIVITY_PRODUCTS_BY_DESTINATION_QUERY =
			"select distinct {a.pk} from {ActivityProduct as a join Location as l on {a.destination}={l.pk}} "
					+ "where {l.code} in (?destination)";

	@Override
	public List<ActivityProductModel> getActivityProducts()
	{
		final List<ActivityProductModel> activityTypeModels = find();
		return CollectionUtils.isNotEmpty(activityTypeModels)
				? activityTypeModels : null;
	}

	@Override
	public ActivityProductModel findActivityProduct(final String activityCode, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(activityCode, "activity code must not be null!");
		validateParameterNotNull(catalogVersion, "catalog version must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivityProductModel.CODE, activityCode);
		params.put(ActivityProductModel.CATALOGVERSION, catalogVersion);
		final List<ActivityProductModel> activityProductModels = find(params);
		if (CollectionUtils.isNotEmpty(activityProductModels))
		{
			return activityProductModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public List<ActivityProductModel> getActivityProductsByDestination(final List<String> destinationCode,
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(destinationCode, "destination code must not be null!");
		validateParameterNotNull(catalogVersion, "catalog version must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivityProductModel.DESTINATION, destinationCode);
		params.put(ActivityProductModel.CATALOGVERSION, catalogVersion);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_ACTIVITY_PRODUCTS_BY_DESTINATION_QUERY, params);
		final SearchResult<ActivityProductModel> searchResult = this.getFlexibleSearchService().search(fsq);
		return searchResult.getCount() < 1 ? null : searchResult.getResult();
	}
}
