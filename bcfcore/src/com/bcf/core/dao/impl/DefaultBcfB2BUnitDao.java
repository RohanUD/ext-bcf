/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfB2BUnitDao;


public class DefaultBcfB2BUnitDao extends DefaultGenericDao<B2BUnitModel> implements BcfB2BUnitDao
{
   public static final String DEFAULT_UNIT = "Default";

   public DefaultBcfB2BUnitDao(final String typecode)
   {
      super(typecode);
   }

   @Override
   public B2BUnitModel getB2BUnitByUid(final String uid)
   {
      validateParameterNotNull(uid, "uid must not be null!");
      final Map<String, Object> params = new HashMap<>();
      params.put(B2BUnitModel.UID, uid);
      final List<B2BUnitModel> units = find(params);
      if (CollectionUtils.isNotEmpty(units))
      {
         return units.get(0);
      }
      return null;
   }

   @Override
   public B2BUnitModel getDefaultB2BUnit()
   {
      return getB2BUnitByUid(DEFAULT_UNIT);
   }

}
