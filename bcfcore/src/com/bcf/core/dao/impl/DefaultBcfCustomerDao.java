/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.dao.BcfCustomerDao;


public class DefaultBcfCustomerDao extends DefaultGenericDao<CustomerModel> implements BcfCustomerDao
{
	private static final String FIND_CUSTOMER_FOR_PASSWORD_RECOVERY_KEY = "SELECT {pk} FROM {customer} WHERE {passwordRecoveryKey}=?key";
	private static final String FIND_CUSTOMER_FOR_CUSTOMER_ID = "SELECT {pk} FROM {customer} WHERE {customerID}=?customerID";
	private static final String FIND_CUSTOMER_FOR_ACCOUNT_VALIDATION_KEY =
			"SELECT {pk} FROM {customer} WHERE {accountValidationKey}=?" + CustomerModel.ACCOUNTVALIDATIONKEY;

	public DefaultBcfCustomerDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public CustomerModel getCustomerforPasswordRecoveryKey(final String key)
	{
		validateParameterNotNull(key, "type must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("key", key);
		return getCustomerModel(queryParams, FIND_CUSTOMER_FOR_PASSWORD_RECOVERY_KEY);
	}

	@Override
	public CustomerModel findCustomerForAccountValidationKey(final String accountValidationKey)
	{
		validateParameterNotNull(accountValidationKey, "accountValidationKey must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CustomerModel.ACCOUNTVALIDATIONKEY, accountValidationKey);
		return getCustomerModel(queryParams, FIND_CUSTOMER_FOR_ACCOUNT_VALIDATION_KEY);
	}

	@Override
	public CustomerModel findCustomerForCustomerId(final String customerId)
	{
		validateParameterNotNull(customerId, "customerId must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CustomerModel.CUSTOMERID, customerId);
		return getCustomerModel(queryParams, FIND_CUSTOMER_FOR_CUSTOMER_ID);
	}

	@Override
	public CustomerModel findCustomerForUId(final String uId)
	{
		validateParameterNotNull(uId, "customerId must not be null!");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CustomerModel.UID, uId);

		return getCustomerModel(queryParams);
	}

	protected CustomerModel getCustomerModel(final Map<String, Object> queryParams)
	{
		final List<CustomerModel> searchResult = find(queryParams);
		if (CollectionUtils.isNotEmpty(searchResult))
		{
			return searchResult.size() > 0 ? searchResult.get(0) : null;
		}
		return null;
	}

	protected CustomerModel getCustomerModel(final Map<String, Object> queryParams, final String query)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query, queryParams);
		final SearchResult<CustomerModel> searchResult = this.getFlexibleSearchService().search(flexibleSearchQuery);
		if (searchResult != null)
		{
			return searchResult.getResult().size() > 0 ? searchResult.getResult().get(0) : null;
		}
		return null;
	}



}
