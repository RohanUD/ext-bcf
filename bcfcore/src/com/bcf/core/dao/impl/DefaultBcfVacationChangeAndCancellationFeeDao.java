/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfVacationChangeAndCancellationFeeDao;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationFeesModel;
import com.bcf.core.model.vacation.VacationChangeAndCancellationPolicyModel;


public class DefaultBcfVacationChangeAndCancellationFeeDao extends DefaultGenericDao<VacationChangeAndCancellationFeesModel>
		implements BcfVacationChangeAndCancellationFeeDao
{

	private static final String SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY =
			"SELECT {" + VacationChangeAndCancellationFeesModel.PK + "} from {" + VacationChangeAndCancellationFeesModel._TYPECODE
					+ " as VF join " + VacationChangeAndCancellationPolicyModel._TYPECODE + " as VP on {VF:";

	private static final String VACATION_CHANGE_AND_CANCELLATION_POLICY_PK = "}={VP:"
			+ VacationChangeAndCancellationPolicyModel.PK;

	private static final String BETWEEN_STARTDATE_AND_ENDDATE =
			" {VP:" + VacationChangeAndCancellationPolicyModel.STARTDATE + "} <= ?firstTravelDate and {VP:"
					+ VacationChangeAndCancellationPolicyModel.ENDDATE + "} >= ?firstTravelDate and {VF:"
					+ VacationChangeAndCancellationFeesModel.MINDAYS + "} <= ?noOfDays"
					+ " and {VF:" + VacationChangeAndCancellationFeesModel.MAXDAYS + "} >= ?noOfDays";

	private static final String VACATION_FEE_ROW_ROUTETYPE_DATE =
			SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
					+ VacationChangeAndCancellationFeesModel.PACKAGEVACATIONCHANGEANDCANCELLATIONPOLICY
					+ VACATION_CHANGE_AND_CANCELLATION_POLICY_PK + "}} where {VP:"
					+ VacationChangeAndCancellationPolicyModel.ROUTETYPE + "} = ?" + VacationChangeAndCancellationPolicyModel.ROUTETYPE
					+ " and " + BETWEEN_STARTDATE_AND_ENDDATE;


	private static String VACATION_ACCOMMODATION_ACTIVITY_FEE_ROW = "SELECT tbl.pk from ({{"
			+ SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ VacationChangeAndCancellationFeesModel.COMPONENTVACATIONCHANGEANDCANCELLATIONPOLICY
			+ VACATION_CHANGE_AND_CANCELLATION_POLICY_PK + "}"
			+ " join VacationChangeAndCancellationPolicy2AccommodationOfferingRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?accommodationOfferings)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE
			+ " and {VF:" + VacationChangeAndCancellationFeesModel.MAXDAYS + "} >= ?noOfDays"
			+ " }} UNION {{"
			+ SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
			+ VacationChangeAndCancellationFeesModel.COMPONENTVACATIONCHANGEANDCANCELLATIONPOLICY
			+ VACATION_CHANGE_AND_CANCELLATION_POLICY_PK + "}"
			+ " join VacationChangeAndCancellationPolicy2ActivityProductRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?activityProducts)} where"
			+ BETWEEN_STARTDATE_AND_ENDDATE + " }})tbl";

	private static String VACATION_ACCOMMODATION_FEE_ROW =
			SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
					+ VacationChangeAndCancellationFeesModel.COMPONENTVACATIONCHANGEANDCANCELLATIONPOLICY
					+ VACATION_CHANGE_AND_CANCELLATION_POLICY_PK + "}"
					+ " join VacationChangeAndCancellationPolicy2AccommodationOfferingRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?accommodationOfferings)} where"
					+ BETWEEN_STARTDATE_AND_ENDDATE;

	private static String VACATION_ACTIVITY_FEE_ROW =
			SELECT_PK_FROM_VACATION_CHANGE_AND_CANCELLATION_POLICY
					+ VacationChangeAndCancellationFeesModel.COMPONENTVACATIONCHANGEANDCANCELLATIONPOLICY
					+ VACATION_CHANGE_AND_CANCELLATION_POLICY_PK + "}"
					+ " join VacationChangeAndCancellationPolicy2ActivityProductRelation as rel ON {rel:source}={VP:pk} and {rel:target} IN (?activityProducts)} where"
					+ BETWEEN_STARTDATE_AND_ENDDATE;



	public DefaultBcfVacationChangeAndCancellationFeeDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public VacationChangeAndCancellationFeesModel findPackageVacationChangeAndCancellationFee(RouteType routeType,
			Date firstTravelDate, int noOfDays)
	{

		final Map<String, Object> params = new HashMap<>();
		params.put(VacationChangeAndCancellationPolicyModel.ROUTETYPE, routeType);
		params.put("firstTravelDate", firstTravelDate);
		params.put("noOfDays", noOfDays);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(VACATION_FEE_ROW_ROUTETYPE_DATE, params);
		final SearchResult<VacationChangeAndCancellationFeesModel> searchResult = this.getFlexibleSearchService()
				.search(flexibleSearchQuery);
		if (searchResult != null)
		{
			return searchResult.getResult().size() > 0 ? searchResult.getResult().get(0) : null;
		}
		return null;

	}

	@Override
	public List<VacationChangeAndCancellationFeesModel> findComponentVacationChangeAndCancellationFee(Date firstTravelDate,
			int noOfDays, List<AccommodationOfferingModel> accommodationOfferings, List<ActivityProductModel> activityProducts)
	{

		final Map<String, Object> params = new HashMap<>();
		String query = null;
		if (CollectionUtils.isEmpty(activityProducts))
		{
			query = VACATION_ACCOMMODATION_FEE_ROW;
			params.put("accommodationOfferings", accommodationOfferings);
		}
		else if (CollectionUtils.isEmpty(accommodationOfferings))
		{
			query = VACATION_ACTIVITY_FEE_ROW;
			params.put("activityProducts", activityProducts);
		}
		else
		{
			query = VACATION_ACCOMMODATION_ACTIVITY_FEE_ROW;
			params.put("accommodationOfferings", accommodationOfferings);
			params.put("activityProducts", activityProducts);
		}
		params.put("firstTravelDate", firstTravelDate);
		params.put("noOfDays", noOfDays);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.addQueryParameters(params);
		final SearchResult<VacationChangeAndCancellationFeesModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
		return result != null && result.getResult().size() > 0 ? result.getResult() : null;
	}




}
