/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.stock.impl.DefaultStockLevelDao;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.dao.BcfStockLevelDao;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.util.BCFDateUtils;


public class DefaultBcfStockLevelDao extends DefaultStockLevelDao implements BcfStockLevelDao
{
	private static final String STARTDATE = "startDate";
	private static final String ENDDATE = "endDate";


	private static final String SELECT_STOCKLEVEL = "SELECT {s." + StockLevelModel.PK + "} FROM {"
		+ StockLevelModel._TYPECODE;




	private static final String FIND_STOCKLEVEL_PK_WHERE_PRODUCTCODE = SELECT_STOCKLEVEL + " as s }" + " WHERE {" + StockLevelModel.PRODUCTCODE + "} = ?productCode AND {";

	private static final String BETWEEN_STARTDATE_AND_ENDDATE = StockLevelModel.DATE + "} BETWEEN ?" + STARTDATE + " AND ?"
		+ ENDDATE;

	private static final String GET_STOCKLEVEL_FOR_CODE =
			FIND_STOCKLEVEL_PK_WHERE_PRODUCTCODE + StockLevelModel.DATE + "} = ?activityDate";

	private static final String GET_STOCKLEVELS_FOR_CODE_WAREHOUSE_AND_DATE = FIND_STOCKLEVEL_PK_WHERE_PRODUCTCODE + BETWEEN_STARTDATE_AND_ENDDATE + " AND {" + StockLevelModel.WAREHOUSE + "} = ?" + StockLevelModel.WAREHOUSE;

	private static final String FETCH_STOCKLEVEL_FOR_RELEASEBLOCK_DAY ="SELECT {"+ StockLevelModel.PK + "} FROM {"
			+ StockLevelModel._TYPECODE+ "}" + " WHERE {"
			+ StockLevelModel.RELEASEBLOCKDAY + "} BETWEEN ?" + STARTDATE + " AND ?"
			+ ENDDATE +" AND {" + StockLevelModel.AVAILABLE
			+ "}>0";

	private static final String GET_SINGLE_STOCKLEVEL_FOR_CODE = FIND_STOCKLEVEL_PK_WHERE_PRODUCTCODE + StockLevelModel.WAREHOUSE + "} IS NOT NULL";

	private static final String GET_ACCOMMODATION_STOCKLEVELS_BETWEEN_DATES = SELECT_STOCKLEVEL + " as s JOIN " + AccommodationModel._TYPECODE + " as a ON {s." + StockLevelModel.PRODUCTCODE
			+ "}={a." + AccommodationModel.CODE + "}} WHERE {" + BETWEEN_STARTDATE_AND_ENDDATE + " ORDER BY {" + StockLevelModel.DATE + "}";

	private static final String GET_ACTIVITY_STOCKLEVELS_BETWEEN_DATES = SELECT_STOCKLEVEL + " as s JOIN " + ActivityProductModel._TYPECODE + " as a ON {s."
			+ StockLevelModel.PRODUCTCODE
			+ "}={a." + ActivityProductModel.CODE + "}} WHERE {" + BETWEEN_STARTDATE_AND_ENDDATE + " ORDER BY {" + StockLevelModel.DATE + "}";

	@Override
	public StockLevelModel fetchProductStockLevel(final String productCode)
	{
		validateProduct(productCode);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("productCode", productCode);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_SINGLE_STOCKLEVEL_FOR_CODE, queryParams);
		fsq.setCount(1);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);

		if (searchResult.getTotalCount() == 0)
		{
			return null;
		}
		return searchResult.getResult().get(0);
	}


	@Override
	public StockLevelModel fetchProductStockLevel(final String productCode, final Date activityDate)
	{
		validateProduct(productCode);
		validateParameterNotNull(activityDate, "activityDate type code must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("productCode", productCode);
		queryParams.put("activityDate", activityDate);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_STOCKLEVEL_FOR_CODE, queryParams);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);

		if (searchResult.getTotalCount() == 0)
		{
			return null;
		}
		if (searchResult.getTotalCount() > 1)
		{
			throw new AmbiguousIdentifierException(
					"Found " + CollectionUtils.size(searchResult) + " results for the given product code");
		}
		return searchResult.getResult().get(0);
	}

	@Override
	public List<StockLevelModel> fetchStocks(final Date startDate, final Date endDate)
	{
		validateParameterNotNull(startDate, "start date must not be null");
		validateParameterNotNull(endDate, "end date must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(STARTDATE, startDate);
		queryParams.put(ENDDATE, endDate);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FETCH_STOCKLEVEL_FOR_RELEASEBLOCK_DAY, queryParams);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<StockLevelModel> fetchStockLevelsForProductAndDate(final String productCode, final Date date,
			final WarehouseModel warehouse)
	{
		validateProduct(productCode);
		validateParameterNotNull(date, "date  must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(StockLevelModel.PRODUCTCODE, productCode);
		queryParams.put(StockLevelModel.WAREHOUSE, warehouse);
		queryParams.put(STARTDATE, BCFDateUtils.setToStartOfDay(date));
		queryParams.put(ENDDATE, BCFDateUtils.setToEndOfDay(date));

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_STOCKLEVELS_FOR_CODE_WAREHOUSE_AND_DATE, queryParams);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	private void validateProduct(final String productCode)
	{
		validateParameterNotNull(productCode, "productCode type code must not be null");
	}

	@Override
	public List<StockLevelModel> fetchAccommodationStockLevels(final Date startDate, final Date endDate)
	{
		validateParameterNotNull(endDate, "end date code must not be null");
		validateParameterNotNull(startDate, "start date must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(STARTDATE, BCFDateUtils.setToStartOfDay(startDate));
		queryParams.put(ENDDATE, BCFDateUtils.setToEndOfDay(endDate));

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_ACCOMMODATION_STOCKLEVELS_BETWEEN_DATES, queryParams);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}

	@Override
	public List<StockLevelModel> fetchActivityStockLevels(final Date startDate, final Date endDate)
	{
		validateParameterNotNull(endDate, "end date code must not be null");
		validateParameterNotNull(startDate, "start date must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(STARTDATE, BCFDateUtils.setToStartOfDay(startDate));
		queryParams.put(ENDDATE, BCFDateUtils.setToEndOfDay(endDate));

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(GET_ACTIVITY_STOCKLEVELS_BETWEEN_DATES, queryParams);
		final SearchResult<StockLevelModel> searchResult = getFlexibleSearchService().search(fsq);
		return searchResult.getResult();
	}
}
