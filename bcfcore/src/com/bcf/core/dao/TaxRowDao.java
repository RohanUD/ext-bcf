/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao;

import de.hybris.platform.europe1.model.TaxRowModel;
import java.util.List;


public interface TaxRowDao
{

	/**
	 * Find tax row.
	 *
	 * @param taxCodes
	 *           the tax code
	 * @param locationCodes
	 *           the location codes
	 * @param commencementDate
	 *           the commencement date
	 * @return the list
	 */
	List<TaxRowModel> findTaxRow(List<String> taxCodes, List<String> locationCodes);

	List<TaxRowModel> findTaxRow(List<String> locationCodes);
}
