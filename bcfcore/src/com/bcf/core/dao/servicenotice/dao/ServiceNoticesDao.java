/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.servicenotice.dao;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import java.util.List;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;


public interface ServiceNoticesDao
{
	List<ServiceNoticeModel> findServiceNotices();

	ServiceNoticeModel findServiceNoticeForCode(final String code);

	List<ServiceNoticeModel> findNonPublishedServiceNotices();

	SearchPageData<ServiceNoticeModel> findPaginatedServiceNotices(
			PageableData paginationData);

	List<ServiceNoticeModel> findPublishedServiceNoticeAndNewsRelease();

	List<ServiceNoticeModel> findLiveButExpiredServiceNotices();

	SearchPageData<NewsModel> findPaginatedNewsRelease(PageableData pageableData);

	List<NewsModel> findNews();

	List<CompetitionModel> findCompetitions();

	List<SubstantialPerformanceNoticeModel> findSubstantialPerformanceNotices();

	CompetitionModel findCompetitionForCode(String code);

	SubstantialPerformanceNoticeModel findSubstantialPerformanceNoticeForCode(String code);
}
