/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao.servicenotice.dao.impl;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.core.dao.servicenotice.dao.ServiceNoticesDao;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;


public class ServiceNoticesDaoImpl implements ServiceNoticesDao
{
	private static final String AND = " AND {sn.";
	private static final String CONDITION_PUBLISH_DATE = AND
			+ ServiceNoticeModel.PUBLISHDATE + "}<=?" + ServiceNoticeModel.PUBLISHDATE;

	private static final String CONDITION_STATUS = AND + ServiceNoticeModel.STATUS
			+ "}=?" + ServiceNoticeModel.STATUS;

	private static final String CONDITION_STATUSES = AND + ServiceNoticeModel.STATUS
			+ "} IN (?" + ServiceNoticeModel.STATUS + ")";

	private static final String SELECT_SERVICENOTICE_PK = "SELECT {sn." + ServiceNoticeModel.PK + "}";
	private static final String FROM = " FROM {";
	private static final String WHERE = " as sn} WHERE";
	private static final String SELECT_PK_FROM_SERVICENOTICE =
			SELECT_SERVICENOTICE_PK + FROM + ServiceNoticeModel._TYPECODE + WHERE;
	private static final String SERVICENOTICE_EXPIRYDATE_CHECK =
			" ({sn." + ServiceNoticeModel.EXPIRYDATE + "} IS NULL OR {sn." + ServiceNoticeModel.EXPIRYDATE + "}>?";

	private static final String QUERY_NEWS_RELEASE =
			SELECT_SERVICENOTICE_PK + FROM + NewsModel._TYPECODE + WHERE + SERVICENOTICE_EXPIRYDATE_CHECK
					+ ServiceNoticeModel.EXPIRYDATE + ")" + CONDITION_PUBLISH_DATE
					+ CONDITION_STATUS + " ORDER BY {sn." + ServiceNoticeModel.PUBLISHDATE + "} DESC";

	private static final String QUERY_SERVICE_NOTICE =
			SELECT_SERVICENOTICE_PK + FROM + ServiceModel._TYPECODE + WHERE + SERVICENOTICE_EXPIRYDATE_CHECK
					+ ServiceNoticeModel.EXPIRYDATE + ")" + CONDITION_PUBLISH_DATE
					+ CONDITION_STATUS + " ORDER BY {sn." + ServiceNoticeModel.PUBLISHDATE + "} DESC";

	private static final String QUERY_SERVICE_NOTICE_FOR_CODE =
			SELECT_PK_FROM_SERVICENOTICE + " {sn."
					+ ServiceNoticeModel.PK + "}=?" + ServiceNoticeModel.PK + CONDITION_PUBLISH_DATE + CONDITION_STATUS;

	private static final String QUERY_SERVICE_NOTICE_NON_PUBLISHED =
			SELECT_PK_FROM_SERVICENOTICE + SERVICENOTICE_EXPIRYDATE_CHECK + ServiceNoticeModel.EXPIRYDATE + ")"
					+ CONDITION_PUBLISH_DATE
					+ AND + ServiceNoticeModel.PUBLISHED + "}=0" + CONDITION_STATUSES;

	private static final String QUERY_SERVICE_NOTICE_AND_NEWS_RELEASE_COMMON =
			SELECT_PK_FROM_SERVICENOTICE + SERVICENOTICE_EXPIRYDATE_CHECK + ServiceNoticeModel.EXPIRYDATE + ")"
					+ CONDITION_PUBLISH_DATE;

	private static final String QUERY_PUBLISHED_SERVICE_NOTICE_AND_NEWS_RELEASE =
			QUERY_SERVICE_NOTICE_AND_NEWS_RELEASE_COMMON + CONDITION_STATUS;

	private static final String QUERY_EXPIRED_SERVICE_NOTICES =
			SELECT_PK_FROM_SERVICENOTICE + " {sn."
					+ ServiceNoticeModel.EXPIRYDATE + "}<=?" + ServiceNoticeModel.EXPIRYDATE + " AND  {sn." + ServiceNoticeModel.STATUS
					+ "}!=?" + ServiceNoticeModel.STATUS;

	private static final String FIND_COMPETITIONS = "SELECT {" + CompetitionModel.PK
			+ "} FROM {" + CompetitionModel._TYPECODE + "}"
			+ " WHERE {" + CompetitionModel.STATUS + "} = ?" + ServiceNoticeModel.STATUS
			+ " ORDER BY {" + CompetitionModel.PUBLISHDATE + "} DESC";

	private static final String FIND_COMPETITION_BY_CODE = "SELECT {" + CompetitionModel.PK
			+ "} FROM {" + CompetitionModel._TYPECODE + "}"
			+ " WHERE {" + ServiceNoticeModel.STATUS + "} = ?" + ServiceNoticeModel.STATUS
			+ " AND  {" + ServiceNoticeModel.PK + "} = ?" + ServiceNoticeModel.PK;

	private static final String FIND_SUBSTANTIAL_PERFORMANCE_NOTICE_BY_CODE = "SELECT {" + SubstantialPerformanceNoticeModel.PK
			+ "} FROM {" + SubstantialPerformanceNoticeModel._TYPECODE + "}"
			+ " WHERE {" + ServiceNoticeModel.STATUS + "} = ?" + ServiceNoticeModel.STATUS
			+ " AND  {" + ServiceNoticeModel.PK + "} = ?" + ServiceNoticeModel.PK;

	private static final String FIND_SUBSTANTIAL_PERFORMANCE_NOTICES = "SELECT {" + SubstantialPerformanceNoticeModel.PK
			+ "} FROM {" + SubstantialPerformanceNoticeModel._TYPECODE + "}"
			+ " WHERE {" + SubstantialPerformanceNoticeModel.STATUS + "} = ?" + ServiceNoticeModel.STATUS
			+ " ORDER BY {" + CompetitionModel.PUBLISHDATE + "} DESC";


	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	PagedFlexibleSearchService pagedFlexibleSearchService;

	@Override
	public List<ServiceNoticeModel> findServiceNotices()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SERVICE_NOTICE, params);
		final SearchResult<ServiceNoticeModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<NewsModel> findNews()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_NEWS_RELEASE, params);
		final SearchResult<NewsModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<CompetitionModel> findCompetitions()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_COMPETITIONS, params);
		final SearchResult<CompetitionModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<SubstantialPerformanceNoticeModel> findSubstantialPerformanceNotices()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_SUBSTANTIAL_PERFORMANCE_NOTICES, params);
		final SearchResult<SubstantialPerformanceNoticeModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public SearchPageData<ServiceNoticeModel> findPaginatedServiceNotices(
			final PageableData paginationData)
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SERVICE_NOTICE, params);

		return pagedFlexibleSearchService.search(query, paginationData);
	}

	@Override
	public ServiceNoticeModel findServiceNoticeForCode(final String code)
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.PK, code);
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SERVICE_NOTICE_FOR_CODE, params);
		return flexibleSearchService.searchUnique(query);
	}

	@Override
	public CompetitionModel findCompetitionForCode(final String code)
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.PK, code);
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_COMPETITION_BY_CODE, params);
		final SearchResult<CompetitionModel> searchResult = flexibleSearchService.search(query);
		if (searchResult.getCount() > 0)
		{
			return searchResult.getResult().get(0);
		}
		return null;
	}

	@Override
	public SubstantialPerformanceNoticeModel findSubstantialPerformanceNoticeForCode(final String code)
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.PK, code);
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_SUBSTANTIAL_PERFORMANCE_NOTICE_BY_CODE, params);
		final SearchResult<SubstantialPerformanceNoticeModel> searchResult = flexibleSearchService.search(query);
		if (searchResult.getCount() > 0)
		{
			return searchResult.getResult().get(0);
		}
		return null;
	}

	@Override
	public List<ServiceNoticeModel> findNonPublishedServiceNotices()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, Arrays.asList(ServiceNoticeStatus.APPROVED, ServiceNoticeStatus.LIVE));
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SERVICE_NOTICE_NON_PUBLISHED, params);
		final SearchResult<ServiceNoticeModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<ServiceNoticeModel> findPublishedServiceNoticeAndNewsRelease()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.PUBLISHDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_PUBLISHED_SERVICE_NOTICE_AND_NEWS_RELEASE, params);
		final SearchResult<ServiceNoticeModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<ServiceNoticeModel> findLiveButExpiredServiceNotices()
	{
		final Map<String, Object> params = new HashMap();
		params.put(ServiceNoticeModel.EXPIRYDATE, new Date());
		params.put(ServiceNoticeModel.STATUS, ServiceNoticeStatus.EXPIRED);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_EXPIRED_SERVICE_NOTICES, params);
		final SearchResult<ServiceNoticeModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public SearchPageData<NewsModel> findPaginatedNewsRelease(final PageableData pageableData)
	{
		final Map<String, Object> params = new HashMap();
		params.put(NewsModel.EXPIRYDATE, new Date());
		params.put(NewsModel.PUBLISHDATE, new Date());
		params.put(NewsModel.STATUS, ServiceNoticeStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_NEWS_RELEASE, params);

		return pagedFlexibleSearchService.search(query, pageableData);
	}
}
