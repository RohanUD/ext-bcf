/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 9/7/19 1:22 PM
 */

package com.bcf.core.dao;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import java.util.Date;
import java.util.List;


public interface BcfOptionBookingDao
{
	SearchPageData<CartModel> findPagedOptionBookings(final PageableData pageableData);

	List<CartModel> findActiveOptionBookingsThatAreExpired();

	List<CartModel> findExpiredOptionBookingsAfterDate(Date purgingDate);
}
