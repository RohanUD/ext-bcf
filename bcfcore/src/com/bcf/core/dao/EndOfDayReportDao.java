/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import java.util.Date;
import java.util.List;
import com.bcf.core.model.DeclareOrderEntryModel;


public interface EndOfDayReportDao
{
	List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForAgent(final UserModel agent,
			final Date declareStartTime, final Date declareEndTime);

	List<DeclareOrderEntryModel> findDeclareOrderEntriesForAgent(final UserModel agent, final Date declareStartTime,
			final Date declareEndTime);

	List<PaymentTransactionEntryModel> findPaymentTransactionEntriesForWebJob(final Date endofdayDeclareStartTime,
			final Date endofdayDeclareEndTime);

	List<DeclareOrderEntryModel> findDeclareOrderEntriesForWebJob(Date endofdayDeclareStartTime, Date endofdayDeclareEndTime);
}
