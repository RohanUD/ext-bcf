/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.dao;

import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.dao.TravelLocationDao;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.List;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public interface BcfTravelLocationDao extends TravelLocationDao
{

	/**
	 * Find location by name.
	 *
	 * @param name the name
	 * @return the location model
	 */
	LocationModel findLocationByName(final String name);

	/**
	 * Find all bcf vacation locations.
	 *
	 * @return the list
	 */
	List<BcfVacationLocationModel> findAllBcfVacationLocations();

	List<BcfVacationLocationModel> getLocationsByType(String locationType);

	List<LocationModel> getSubLocations(LocationModel locationModel);

	BcfVacationLocationModel findBcfVacationLocationByCode(final String name);

	List<BcfVacationLocationModel> getBcfVacationSubLocations(BcfVacationLocationModel locationModel);

	LocationModel findLocationForCodeAndType(String code, LocationType locationType);

	public BcfVacationLocationModel findBcfVacationLocationByOriginalLocation(final LocationModel location);

	SearchPageData<BcfVacationLocationModel> getPaginatedLocationsByType(String locationType, int pageSize,
			int currentPage);

	SearchPageData<BcfVacationLocationModel> getPaginatedCitiesByRegion(String regionCode, int pageSize,
			int currentPage);

	PointOfServiceModel findPointOfServiceForName(final String name);
}
