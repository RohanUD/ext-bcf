package com.bcf.core.dao.matomo;

import com.bcf.core.model.MatomoModel;


public interface MatomoDao
{
	MatomoModel getMatomoScript();
}
