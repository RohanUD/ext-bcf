package com.bcf.core.dao.matomo.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.matomo.MatomoDao;
import com.bcf.core.model.MatomoModel;


public class MatomoDaoImpl implements MatomoDao
{
	private FlexibleSearchService flexibleSearchService;

	private static final String FIND_MATOMO = "SELECT {" + MatomoModel.PK
			+ "} FROM {" + MatomoModel._TYPECODE + "}";

	@Override
	public MatomoModel getMatomoScript()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_MATOMO);
		final SearchResult<MatomoModel> searchResult = getFlexibleSearchService().search(query);

		if (searchResult.getCount() > 0)
		{
			return searchResult.getResult().get(0);
		}
		return null;
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
