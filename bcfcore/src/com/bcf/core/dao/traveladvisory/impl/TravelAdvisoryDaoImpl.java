/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.dao.traveladvisory.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.traveladvisory.TravelAdvisoryDao;
import com.bcf.core.enums.TravelAdvisoryDisplayType;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.RoutesAtGlanceModel;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.util.StreamUtil;


public class TravelAdvisoryDaoImpl implements TravelAdvisoryDao
{
	private static final String CURRENT_TIME = "CurrentTime";

	private static final String FIND_TRAVEL_ADVISORIES_TO_PUBLISH = "SELECT {" + TravelAdvisoryModel.PK
			+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
			+ " WHERE {" + TravelAdvisoryModel.PUBLISHDATETIME + "} < ?" + CURRENT_TIME
			+ " AND {" + TravelAdvisoryModel.EXPIRYDATETIME + "} > ?" + CURRENT_TIME
			+ " AND {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS;

	private static final String FIND_TRAVEL_ADVISORIES_BY_STATUS = "SELECT {" + TravelAdvisoryModel.PK
			+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
			+ " WHERE {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS
			+ " ORDER BY {" + TravelAdvisoryModel.PUBLISHDATETIME + "} DESC";

	private static final String FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_DISPLAY_TYPE_WITH_NO_ROUTE_REGION =
			"SELECT {" + TravelAdvisoryModel.PK
					+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
					+ " WHERE {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS
					+ " AND {" + TravelAdvisoryModel.DISPLAYTYPE + "} = ?" + TravelAdvisoryModel.DISPLAYTYPE
					+ " AND {" + TravelAdvisoryModel.ROUTES + "} IS NULL"
					+ " AND {" + TravelAdvisoryModel.REGIONS + "} IS NULL"
					+ " ORDER BY {" + TravelAdvisoryModel.PUBLISHDATETIME + "} DESC";

	private static final String FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_INVERSE_DISPLAY_TYPE = "SELECT {" + TravelAdvisoryModel.PK
			+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
			+ " WHERE {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS
			+ " AND {" + TravelAdvisoryModel.DISPLAYTYPE + "} != ?" + TravelAdvisoryModel.DISPLAYTYPE
			+ " ORDER BY {" + TravelAdvisoryModel.PUBLISHDATETIME + "} DESC";

	private static final String FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_INVERSE_DISPLAY_TYPE_WITH_NO_ROUTE_REGION =
			"SELECT {" + TravelAdvisoryModel.PK
					+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
					+ " WHERE {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS
					+ " AND {" + TravelAdvisoryModel.DISPLAYTYPE + "} != ?" + TravelAdvisoryModel.DISPLAYTYPE
					+ " AND {" + TravelAdvisoryModel.ROUTES + "} IS NULL"
					+ " AND {" + TravelAdvisoryModel.REGIONS + "} IS NULL"
					+ " ORDER BY {" + TravelAdvisoryModel.PUBLISHDATETIME + "} DESC";

	private static final String FIND_TRAVEL_ADVISORIES_BY_CODE = "SELECT {" + TravelAdvisoryModel.PK
			+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
			+ " WHERE {" + TravelAdvisoryModel.PK + "} = ?" + TravelAdvisoryModel.PK
			+ " AND {" + TravelAdvisoryModel.STATUS + "} = ?" + TravelAdvisoryModel.STATUS;

	private static final String FIND_TRAVEL_ADVISORIES_TO_EXPIRE = "SELECT {" + TravelAdvisoryModel.PK
			+ "} FROM {" + TravelAdvisoryModel._TYPECODE + "}"
			+ " WHERE {" + TravelAdvisoryModel.EXPIRYDATETIME + "} < ?" + CURRENT_TIME;

	private static final String GET_ROUTES_AT_GLANCE_CONTENT = "SELECT {" + RoutesAtGlanceModel.PK
			+ "} FROM {" + RoutesAtGlanceModel._TYPECODE + "}";


	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<TravelAdvisoryModel> findTravelAdvisoriesToPublish()
	{
		final Map<String, Object> params = new HashMap();
		params.put(CURRENT_TIME, new Date());
		params.put(TravelAdvisoryModel.STATUS, TravelAdvisoryStatus.APPROVED);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_TRAVEL_ADVISORIES_TO_PUBLISH, params);
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public List<TravelAdvisoryModel> findExpiredTravelAdvisories()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_TRAVEL_ADVISORIES_TO_EXPIRE,
				Collections.singletonMap(CURRENT_TIME, new Date()));
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatus(final TravelAdvisoryStatus travelAdvisoryStatus)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_TRAVEL_ADVISORIES_BY_STATUS,
				Collections.singletonMap(TravelAdvisoryModel.STATUS, travelAdvisoryStatus));
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndDisplayTypeWithNoRouteRegion(
			final TravelAdvisoryStatus travelAdvisoryStatus, final TravelAdvisoryDisplayType travelAdvisoryDisplayType)
	{
		final Map<String, Object> params = new HashMap();
		params.put(TravelAdvisoryModel.STATUS, travelAdvisoryStatus);
		params.put(TravelAdvisoryModel.DISPLAYTYPE, travelAdvisoryDisplayType);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_DISPLAY_TYPE_WITH_NO_ROUTE_REGION, params);
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndInverseDisplayType(
			final TravelAdvisoryStatus travelAdvisoryStatus, final TravelAdvisoryDisplayType travelAdvisoryDisplayType)
	{
		final Map<String, Object> params = new HashMap();
		params.put(TravelAdvisoryModel.STATUS, travelAdvisoryStatus);
		params.put(TravelAdvisoryModel.DISPLAYTYPE, travelAdvisoryDisplayType);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_INVERSE_DISPLAY_TYPE,
				params);
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndInverseDisplayTypeWithNoRouteRegion(
			final TravelAdvisoryStatus travelAdvisoryStatus, final TravelAdvisoryDisplayType travelAdvisoryDisplayType)
	{
		final Map<String, Object> params = new HashMap();
		params.put(TravelAdvisoryModel.STATUS, travelAdvisoryStatus);
		params.put(TravelAdvisoryModel.DISPLAYTYPE, travelAdvisoryDisplayType);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				FIND_TRAVEL_ADVISORIES_BY_STATUS_AND_INVERSE_DISPLAY_TYPE_WITH_NO_ROUTE_REGION,
				params);
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return search.getResult();
	}

	@Override
	public Optional<TravelAdvisoryModel> findTravelAdvisoryByCode(final String code)
	{
		final Map<String, Object> params = new HashMap();
		params.put(TravelAdvisoryModel.PK, code);
		params.put(TravelAdvisoryModel.STATUS, TravelAdvisoryStatus.LIVE);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_TRAVEL_ADVISORIES_BY_CODE, params);
		final SearchResult<TravelAdvisoryModel> search = getFlexibleSearchService().search(query);
		return StreamUtil.safeStream(search.getResult()).findAny();
	}

	@Override
	public String getRoutesAtGlanceContent()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ROUTES_AT_GLANCE_CONTENT);
		final SearchResult<RoutesAtGlanceModel> search = getFlexibleSearchService().search(query);
		return StreamUtil.safeStream(search.getResult()).findAny().map(RoutesAtGlanceModel::getContent).orElse(StringUtils.EMPTY);
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
