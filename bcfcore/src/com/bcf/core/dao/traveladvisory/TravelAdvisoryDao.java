/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.core.dao.traveladvisory;

import java.util.List;
import java.util.Optional;
import com.bcf.core.enums.TravelAdvisoryDisplayType;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.TravelAdvisoryModel;


public interface TravelAdvisoryDao
{
	List<TravelAdvisoryModel> findTravelAdvisoriesToPublish();

	List<TravelAdvisoryModel> findExpiredTravelAdvisories();

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatus(TravelAdvisoryStatus travelAdvisoryStatus);

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndDisplayTypeWithNoRouteRegion(
			TravelAdvisoryStatus travelAdvisoryStatus, TravelAdvisoryDisplayType travelAdvisoryDisplayType);

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndInverseDisplayType(
			TravelAdvisoryStatus travelAdvisoryStatus, TravelAdvisoryDisplayType travelAdvisoryDisplayType);

	List<TravelAdvisoryModel> findActiveTravelAdvisoriesByStatusAndInverseDisplayTypeWithNoRouteRegion(
			TravelAdvisoryStatus travelAdvisoryStatus, TravelAdvisoryDisplayType travelAdvisoryDisplayType);

	Optional<TravelAdvisoryModel> findTravelAdvisoryByCode(String code);

	String getRoutesAtGlanceContent();
}
