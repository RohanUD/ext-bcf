/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.policies;

import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao;
import java.math.BigDecimal;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;

/**
 * Contains policies for rounding
 */
public class RoundingPolicyService
{

   private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

   private int roundMethod;

   @Resource(name = "currencyDao")
   private CurrencyDao currencyDao;


   /**
    * Round the given value
    *
    * @param val   the value
    * @param scale the precision
    * @return the rounded value
    */
   public double round(final double val, final int scale)
   {
      return BigDecimal.valueOf(val).setScale(scale, roundMethod).doubleValue();
   }

   /**
    * Round the given value
    *
    * @param val   the value
    * @param scale the precision to use for the rounding
    * @return the rounded value
    */
   public BigDecimal round(final BigDecimal val, final int scale)
   {
      return val.setScale(scale, roundMethod);
   }


   /**
    * Round the given value
    *
    * @param val          the value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return the rounded value
    */
   public double round(final double val, final String currencyCode)
   {
      return BigDecimal.valueOf(val).setScale(getScaleByCurrencyCode(currencyCode), roundMethod).doubleValue();
   }


   /**
    * Round the given value
    *
    * @param val   the value
    * @param scale the precision
    * @return the rounded value
    */
   public BigDecimal getRoundedValue(final double val, final int scale)
   {
      return BigDecimal.valueOf(val).setScale(scale, roundMethod);
   }


   /**
    * Round the given value
    *
    * @param val          the value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return the rounded value
    */
   public BigDecimal getRoundedValue(final BigDecimal val, final String currencyCode)
   {
      return val.setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Round the given value
    *
    * @param val   the value
    * @param scale the precision
    * @return the rounded value
    */
   public BigDecimal getRoundedValue(final BigDecimal val, final int scale)
   {
      return val.setScale(scale, roundMethod);
   }


   /**
    * Round the given value
    *
    * @param val          the value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return the rounded value
    */
   public BigDecimal getRoundedValue(final double val, final String currencyCode)
   {
      return BigDecimal.valueOf(val).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Round the given value
    *
    * @param val          the value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return the rounded value
    */
   public BigDecimal getRoundedValue(final String val, final String currencyCode)
   {
      return new BigDecimal(val).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * This compare the two values by using the same rounding operations.
    *
    * @param firstValue   as first values
    * @param secondValue  second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of equal otherwise false
    */
   public boolean isEqual(final double firstValue, final double secondValue, final String currencyCode)
   {
      return round(BigDecimal.valueOf(firstValue), getScaleByCurrencyCode(currencyCode))
            .compareTo(round(BigDecimal.valueOf(secondValue), getScaleByCurrencyCode(currencyCode))) == 0;
   }

   /**
    * This compare the two values by using the same rounding operations.
    *
    * @param firstValue   as first values
    * @param secondValue  second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true if the first value is greater than second value
    */
   public boolean isGreaterThan(final BigDecimal firstValue, final BigDecimal secondValue, final String currencyCode)
   {
      return round(firstValue, getScaleByCurrencyCode(currencyCode))
            .compareTo(round(secondValue, getScaleByCurrencyCode(currencyCode))) > 0;
   }


   /**
    * This compare the two values by using the same rounding operations.
    * Uses an external scale that is independent from currency (e.g. for crossed operations like currency vs percentage, or others)
    *
    * @param firstValue  as first values
    * @param secondValue second value
    * @param scale       the precision
    * @return true in case of equal otherwise false
    */
   public boolean isEqual(final double firstValue, final double secondValue, final int scale)
   {
      return round(BigDecimal.valueOf(firstValue), scale).compareTo(round(BigDecimal.valueOf(secondValue), scale)) == 0;
   }


   /**
    * This compare the two values by using the same rounding operations.
    *
    * @param firstValue   as first values
    * @param secondValue  second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of equal otherwise false
    */
   public boolean isEqual(final BigDecimal firstValue, final BigDecimal secondValue, final String currencyCode)
   {
      return round(firstValue, getScaleByCurrencyCode(currencyCode))
            .compareTo(round(secondValue, getScaleByCurrencyCode(currencyCode))) == 0;
   }


   /**
    * This compare the two values by using the same rounding operations.
    *
    * @param firstValue   as first values
    * @param secondValue  second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public boolean isNotEqual(final double firstValue, final double secondValue, final String currencyCode)
   {
      return !isEqual(firstValue, secondValue, currencyCode);
   }


   /**
    * This compare the two values by using the same rounding operations.
    * Uses an external scale that is independent from currency (e.g. for crossed operations like currency vs percentage, or others)
    *
    * @param firstValue  as first values
    * @param secondValue second value
    * @param scale       the precision
    * @return true in case of not equal otherwise false
    */
   public boolean isNotEqual(final double firstValue, final double secondValue, final int scale)
   {
      return !isEqual(firstValue, secondValue, scale);
   }


   /**
    * This compare the two values by using the same rounding operations.
    * Uses an external scale that is independent from currency (e.g. for crossed operations like currency vs percentage, or others)
    *
    * @param firstValue   as first values
    * @param secondValue  second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public boolean isNotEqual(final BigDecimal firstValue, final BigDecimal secondValue, final String currencyCode)
   {
      return !isEqual(firstValue, secondValue, currencyCode);
   }

   /**
    * Divide first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal divideRounding(final BigDecimal first, final BigDecimal second, final String currencyCode)
   {
      return first.divide(second, getScaleByCurrencyCode(currencyCode), roundMethod);
   }

   /**
    * Divide first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal divideRounding(final BigDecimal first, final Integer second, final String currencyCode)
   {
      return first.divide(new BigDecimal(second.intValue()), getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Divide first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal divideRounding(final double first, final Integer second, final String currencyCode)
   {
      return getRoundedValue(first, getScaleByCurrencyCode(currencyCode)).divide(new BigDecimal(second.intValue()),
            currencyDao.findCurrenciesByCode(currencyCode).iterator().next().getDigits().intValue(), roundMethod);
   }

   /**
    * Divide first by second rounding the result
    *
    * @param first  as first values
    * @param second second value
    * @param scale
    * @return true in case of not equal otherwise false
    */
   public BigDecimal divideRounding(final double first, final Integer second, final int scale)
   {
      return getRoundedValue(first, scale).divide(new BigDecimal(second.intValue()), scale, roundMethod);
   }

   /**
    * Multiply first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal multiplyRounding(final BigDecimal first, final BigDecimal second, final String currencyCode)
   {
      return first.multiply(second).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Multiply first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal multiplyRounding(final double first, final int second, final String currencyCode)
   {
      final int scale = getScaleByCurrencyCode(currencyCode);
      return BigDecimal.valueOf(first).setScale(scale).multiply(BigDecimal.valueOf(second)).setScale(scale, roundMethod);
   }


   /**
    * Multiply first by second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal multiplyRounding(final BigDecimal first, final int second, final String currencyCode)
   {
      final int scale = getScaleByCurrencyCode(currencyCode);
      return first.setScale(scale, roundMethod).multiply(BigDecimal.valueOf(second)).setScale(scale, roundMethod);
   }


   /**
    * Divides param by 100 to get a percentage value with a scale equals to 2
    *
    * @param percentageValueBd
    * @return true in case of not equal otherwise false
    */
   public BigDecimal getZeroToOnePercentValue(final BigDecimal percentageValueBd)
   {
      return percentageValueBd.divide(ONE_HUNDRED, BcfCoreConstants.PERCENTAGE_SCALE_FOR_CALCULATIONS, roundMethod);
   }

   /**
    * Divides % by 100 and applies scale.
    *
    * @param percentageValue
    * @param scale
    * @return a bigdecimal value
    */
   public BigDecimal getZeroToOnePercentValue(final BigDecimal percentageValue, final int scale)
   {
      return percentageValue.divide(ONE_HUNDRED, scale, roundMethod);
   }

   /**
    * Divides param by 100 to get a percentage value with a scale equals to 2
    *
    * @param percentageValueBd
    * @return true in case of not equal otherwise false
    */
   public BigDecimal getZeroToOnePercentValue(final double percentageValueBd)
   {
      return getRoundedValue(percentageValueBd, BcfCoreConstants.PERCENTAGE_SCALE_FOR_CALCULATIONS).divide(ONE_HUNDRED,
            BcfCoreConstants.PERCENTAGE_SCALE_FOR_CALCULATIONS, roundMethod);
   }

   /**
    * Add first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal addRounding(final BigDecimal first, final BigDecimal second, final String currencyCode)
   {
      return first.add(second).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Add first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal addRounding(final BigDecimal first, final Double second, final String currencyCode)
   {
      return first.add(BigDecimal.valueOf(second.doubleValue())).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);

   }


   /**
    * Add first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal addRounding(final Double first, final Double second, final String currencyCode)
   {
      return BigDecimal.valueOf(first.doubleValue()).add(BigDecimal.valueOf(second.doubleValue()))
            .setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Add first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal addRounding(final BigDecimal first, final String second, final String currencyCode)
   {
      return first.add(getRoundedValue(second, currencyCode)).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Subtracts first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal subtractRounding(final double first, final double second, final String currencyCode)
   {
      return BigDecimal.valueOf(first).subtract(BigDecimal.valueOf(second))
            .setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Subtract first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal subtractRounding(final BigDecimal first, final BigDecimal second, final String currencyCode)
   {
      return first.subtract(second).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Subtract first to second rounding the result
    *
    * @param first        as first values
    * @param second       second value
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public BigDecimal subtractRounding(final BigDecimal first, final double second, final String currencyCode)
   {
      return first.subtract(getRoundedValue(second, currencyCode)).setScale(getScaleByCurrencyCode(currencyCode), roundMethod);
   }


   /**
    * Returns the scale associated with the currency code
    *
    * @param currencyCode the currency code to use in order to retrieve the precision
    * @return true in case of not equal otherwise false
    */
   public int getScaleByCurrencyCode(final String currencyCode)
   {
      return currencyDao.findCurrenciesByCode(currencyCode).iterator().next().getDigits().intValue();
   }

   /**
    * applies percentage to a given value rounding by the scales defined by the given currency code
    *
    * @param first
    * @param percentage
    * @return
    */
   public BigDecimal applyPercentage(final BigDecimal first, final double percentage)
   {
      return first.multiply(BigDecimal.valueOf(percentage));
   }


   public int getRoundMethod()
   {
      return roundMethod;
   }

   @Required
   public void setRoundMethod(final int roundMethod)
   {
      this.roundMethod = roundMethod;
   }
}

