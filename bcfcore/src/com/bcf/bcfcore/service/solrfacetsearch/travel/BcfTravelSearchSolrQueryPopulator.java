/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.solrfacetsearch.travel;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.travelservices.search.solrfacetsearch.populators.TravelSearchSolrQueryPopulator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.deals.solrfacetsearch.strategies.RawQueryStrategy;
import com.bcf.core.solr.query.ArguementData;


public class BcfTravelSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE> extends TravelSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
{
	private Map<String, RawQueryStrategy> rawQueryByFilterKeyStrategyMap;

	@Override
	public void populate(SearchQueryPageableData<SolrSearchQueryData> source, SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target)
	{
		super.populate(source, target);

		final List<RawQuery> rawQueries = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(source.getSearchQueryData().getRawQueryFilterTerms()))
		{
			source.getSearchQueryData().getRawQueryFilterTerms().forEach(rawQueryFilterTerm -> {
				final RawQueryStrategy strategy = getRawQueryByFilterKeyStrategyMap().get(target.getIndexedType().getCode()+"_"+rawQueryFilterTerm.getKey());
				final ArguementData rawQueryArguement = strategy
						.generateArguementForRawQuery(rawQueryFilterTerm.getKey(), rawQueryFilterTerm.getValue());
				rawQueries.add(strategy.createRawQuery(rawQueryArguement));
			});
		}

		if (CollectionUtils.isNotEmpty(rawQueries))
		{
			target.getSearchQuery().getFilterRawQueries().addAll(rawQueries);
		}

	}

	protected Map<String, RawQueryStrategy> getRawQueryByFilterKeyStrategyMap()
	{
		return rawQueryByFilterKeyStrategyMap;
	}

	@Required
	public void setRawQueryByFilterKeyStrategyMap(
			final Map<String, RawQueryStrategy> rawQueryByFilterKeyStrategyMap)
	{
		this.rawQueryByFilterKeyStrategyMap = rawQueryByFilterKeyStrategyMap;
	}

}
