/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.solrfacetsearch.travel;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryFacetsPopulator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import com.bcf.core.constants.BcfCoreConstants;


public class BcfTravelFacetSearchQueryFacetsPopulator extends FacetSearchQueryFacetsPopulator
{
	private Integer defaultFacetValuesMaxLimit;
	private Integer defaultFacetValuesMinCount;

	public void populate(SearchQueryConverterData source, SolrQuery target) throws ConversionException
	{
		super.populate(source, target);
		target.setFacetMinCount(this.getDefaultFacetValuesMinCount());
		target.setFacetLimit(this.getDefaultFacetValuesMaxLimit());
	}

	protected String convertQueryField(SearchQuery searchQuery, QueryField queryField) {
		String convertedField = getFieldNameTranslator().translate(searchQuery, queryField.getField(), FieldNameProvider.FieldType.INDEX);
		StringBuilder query = new StringBuilder();
		query.append(escapeQueryChars(convertedField));
		query.append(':');
		String value;
		String separator;
		if (queryField.getValues().size() == 1) {
			 value = queryField.getValues().iterator().next();

			 if(!(queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MAX_COUNT) || queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MAX_ADULT_COUNT) || queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MINIMUM_LENGTH_OF_STAY))){
				 value = escapeQueryChars(value);
			 }

			separator =getFacetSearchQueryOperatorTranslator().translate(value, queryField.getQueryOperator());
			query.append(separator);
		} else {
			List<String> convertedValues = new ArrayList(queryField.getValues().size());
			Iterator var12 = queryField.getValues().iterator();

			String convertedValue="";
			while(var12.hasNext()) {
				value = (String)var12.next();
				if(!(queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MAX_COUNT) || queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MAX_ADULT_COUNT) || queryField.getField().equalsIgnoreCase(BcfCoreConstants.SOLR_FIELD_MINIMUM_LENGTH_OF_STAY))){
					convertedValue = escapeQueryChars(value);
				}

				convertedValue = getFacetSearchQueryOperatorTranslator().translate(convertedValue, queryField.getQueryOperator());
				convertedValues.add(convertedValue);
			}

			SearchQuery.Operator operator = this.resolveQueryFieldOperator(searchQuery, queryField);
			separator = " " + operator + " ";
			convertedValue = StringUtils.join(convertedValues, separator);
			query.append('(');
			query.append(convertedValue);
			query.append(')');
		}

		return query.toString();
	}

	public static String escapeQueryChars(String s) {
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':' || c == '^' || c == '[' || c == ']' || c == '"' || c == '{' || c == '}' || c == '~' || c == '*' || c == '?' || c == '|' || c == '&' || c == ';' || c == '/' || Character.isWhitespace(c)) {
				sb.append('\\');
			}

			sb.append(c);
		}

		return sb.toString();
	}


	public Integer getDefaultFacetValuesMaxLimit()
	{
		return defaultFacetValuesMaxLimit;
	}

	public void setDefaultFacetValuesMaxLimit(final Integer defaultFacetValuesMaxLimit)
	{
		this.defaultFacetValuesMaxLimit = defaultFacetValuesMaxLimit;
	}

	public Integer getDefaultFacetValuesMinCount()
	{
		return defaultFacetValuesMinCount;
	}

	public void setDefaultFacetValuesMinCount(final Integer defaultFacetValuesMinCount)
	{
		this.defaultFacetValuesMinCount = defaultFacetValuesMinCount;
	}
}
