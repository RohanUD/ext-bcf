/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.travelservices.customer.impl.DefaultTravelCustomerAccountService;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.bcfcore.service.customer.dao.BcfCustomerAccountDao;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.BcfB2bUtil;

public class DefaultBcfCustomerAccountService extends DefaultTravelCustomerAccountService implements BcfCustomerAccountService
{
	private static final String CUSTOMER_NOT_NULL= "Customer model cannot be null";

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;

	private BcfCustomerAccountDao bcfCustomerAccountDao;

	@Override
	public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException {
		registerCustomer(customerModel, password);
	}

	@Override
	public void forgottenPassword(final CustomerModel customerModel) {
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken data = new SecureToken(customerModel.getUid(), timeStamp);
		final String token = getSecureTokenService().encryptData(data);
		customerModel.setToken(token);
		getModelService().save(customerModel);
	}

	@Override
	public OrderModel getGuestOrderForGUID(final String guid)
	{
		return getGuestOrderForGUID(guid,getBaseStoreService().getCurrentBaseStore());
	}

	@Override
	public List<CTCTCCardPaymentInfoModel> getCtcTcPaymentInfos(
			final CustomerModel customerModel, final boolean saved)
	{
		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (((BcfUserService) getUserService()).isB2BCustomer() && Objects.nonNull(b2bUnitInContext))
		{
			return getBcfCustomerAccountDao().findCtcTcPaymentInfosByB2bUnit(b2bUnitInContext, saved);
		}

		return bcfCustomerAccountDao.findCtcTcPaymentInfosByCustomer(customerModel, saved);
	}

	@Override
	public void deleteCtcTcCardPaymentInfo(final CustomerModel customerModel,
			final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel)
	{

		validateParameterNotNull(customerModel, CUSTOMER_NOT_NULL);
		validateParameterNotNull(ctcTcCardPaymentInfoModel, "CTCTCCardPaymentInfoModel model cannot be null");
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoForCode = this
				.getCtcTcCardPaymentInfoForCode(customerModel, ctcTcCardPaymentInfoModel.getPk().toString());
		getModelService().remove(ctcTcCardPaymentInfoForCode);
		getModelService().refresh(customerModel);
	}

	@Override
	public CTCTCCardPaymentInfoModel getCtcTcCardPaymentInfoForCode(final CustomerModel currentUser, final String paymentInfoId)
	{
		validateParameterNotNull(currentUser, CUSTOMER_NOT_NULL);
		return getBcfCustomerAccountDao().findCtcTcCardPaymentInfoByCustomerAndCode(currentUser, paymentInfoId);
	}

	@Override
	public List<CreditCardPaymentInfoModel> getCreditCardPaymentInfos(final CustomerModel customerModel, final boolean saved)
	{
		validateParameterNotNull(customerModel, CUSTOMER_NOT_NULL);

		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (((BcfUserService) getUserService()).isB2BCustomer() && Objects.nonNull(b2bUnitInContext))
		{
			return getBcfCustomerAccountDao().findCreditCardPaymentInfosByB2bUnit(b2bUnitInContext, saved);
		}

		return getBcfCustomerAccountDao().findCreditCardPaymentInfosByCustomer(customerModel, saved);
	}

	protected BcfCustomerAccountDao getBcfCustomerAccountDao()
	{
		return bcfCustomerAccountDao;
	}

	@Required
	public void setBcfCustomerAccountDao(final BcfCustomerAccountDao bcfCustomerAccountDao)
	{
		this.bcfCustomerAccountDao = bcfCustomerAccountDao;
	}
}
