/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.customer.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.travelservices.customer.dao.TravelCustomerAccountDao;
import java.util.List;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;


public interface BcfCustomerAccountDao extends TravelCustomerAccountDao
{

	List<CTCTCCardPaymentInfoModel> findCtcTcPaymentInfosByCustomer(CustomerModel customerModel, boolean saved);

	CTCTCCardPaymentInfoModel findCtcTcCardPaymentInfoByCustomerAndCode(CustomerModel currentUser, String paymentInfoId);

	List<CreditCardPaymentInfoModel> findCreditCardPaymentInfosByB2bUnit(B2BUnitModel b2bUnit, final boolean saved);

	List<CTCTCCardPaymentInfoModel> findCtcTcPaymentInfosByB2bUnit(B2BUnitModel b2bUnit, boolean saved);
}
