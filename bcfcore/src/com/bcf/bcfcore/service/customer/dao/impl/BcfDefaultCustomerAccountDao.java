/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.customer.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.travelservices.customer.dao.impl.DefaultTravelCustomerAccountDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.bcfcore.service.customer.dao.BcfCustomerAccountDao;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;


public class BcfDefaultCustomerAccountDao extends DefaultTravelCustomerAccountDao implements BcfCustomerAccountDao
{
	private static final String CUSTOMER = "customer";
	private static final String SAVED = "saved";
	private static final String DUPLICATE = "duplicate";
	private static final String AND = " AND ";

	private static final String SELECT_CTCTCCARD_PAYMENTINFO = "SELECT {" + CTCTCCardPaymentInfoModel.PK
			+ "} FROM {" + CTCTCCardPaymentInfoModel._TYPECODE + "} WHERE ";
	private static final String CTCTCCARD_PAYMENTINFO_DUPLICATE = "{" + CTCTCCardPaymentInfoModel.DUPLICATE
			+ "} = ?duplicate";
	private static final String CTCTCCARD_PAYMENTINFO_B2BUNIT = "{" + CTCTCCardPaymentInfoModel.B2BUNIT + "} = ?b2bUnit";
	private static final String CTCTCCARD_PAYMENTINFO_SAVED = "{" + CTCTCCardPaymentInfoModel.SAVED + "} = ?saved";
	private static final String CTCTCCARD_PAYMENTINFO_USER = "{" + CTCTCCardPaymentInfoModel.USER + "} = ?customer";

	private static final String SELECT_CREDITCARD_PAYMENTINFO = "SELECT {" + CreditCardPaymentInfoModel.PK
			+ "} FROM {" + CreditCardPaymentInfoModel._TYPECODE + "} WHERE ";
	private static final String CREDITCARD_PAYMENTINFO_DUPLICATE = "{" + CreditCardPaymentInfoModel.DUPLICATE
			+ "} = ?duplicate";
	private static final String CREDITCARD_PAYMENTINFO_SAVED = "{" + CreditCardPaymentInfoModel.SAVED + "} = ?saved";
	private static final String CREDITCARD_PAYMENTINFO_USER = "{" + CreditCardPaymentInfoModel.USER + "} = ?customer";

	private static final String FIND_SAVED_CTC_TC_PAYMENT_INFOS_BY_CUSTOMER_QUERY =
			SELECT_CTCTCCARD_PAYMENTINFO + CTCTCCARD_PAYMENTINFO_USER + AND + CTCTCCARD_PAYMENTINFO_SAVED + AND
					+ CTCTCCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_CTC_TC_PAYMENT_INFOS_BY_B2BUNIT_QUERY =
			SELECT_CTCTCCARD_PAYMENTINFO + CTCTCCARD_PAYMENTINFO_B2BUNIT + AND
					+ CTCTCCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_SAVED_CTC_TC_PAYMENT_INFOS_BY_B2BUNIT_QUERY =
			SELECT_CTCTCCARD_PAYMENTINFO + CTCTCCARD_PAYMENTINFO_B2BUNIT + AND + CTCTCCARD_PAYMENTINFO_SAVED + AND
					+ CTCTCCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_CTC_TC_PAYMENT_INFOS_BY_CUSTOMER_QUERY =
			SELECT_CTCTCCARD_PAYMENTINFO + CTCTCCARD_PAYMENTINFO_USER + AND
					+ CTCTCCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_CTC_TC_PAYMENT_INFO_BY_CUSTOMER_CODE_QUERY =
			SELECT_CTCTCCARD_PAYMENTINFO + CTCTCCARD_PAYMENTINFO_USER + AND
					+ "{" + CTCTCCardPaymentInfoModel.PK + "} = ?pk" + AND + CTCTCCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_SAVED_PAYMENT_INFOS_BY_B2BUNIT_QUERY =
			SELECT_CREDITCARD_PAYMENTINFO + "{" + CreditCardPaymentInfoModel.B2BUNIT
					+ "} = ?b2bUnit " + AND + CREDITCARD_PAYMENTINFO_SAVED + AND + CREDITCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_PAYMENT_INFOS_BY_B2BUNIT_QUERY =
			SELECT_CREDITCARD_PAYMENTINFO + "{" + CreditCardPaymentInfoModel.B2BUNIT + "} = ?b2bUnit" + AND
					+ CREDITCARD_PAYMENTINFO_DUPLICATE;

	private static final String FIND_SAVED_PAYMENT_INFOS_BY_CUSTOMER_QUERY =
			SELECT_CREDITCARD_PAYMENTINFO + CREDITCARD_PAYMENTINFO_USER + AND + CREDITCARD_PAYMENTINFO_SAVED + AND
					+ CREDITCARD_PAYMENTINFO_DUPLICATE + AND + "{" + CreditCardPaymentInfoModel.B2BUNIT + "} is NULL";

	private static final String FIND_PAYMENT_INFOS_BY_CUSTOMER_QUERY =
			SELECT_CREDITCARD_PAYMENTINFO + CREDITCARD_PAYMENTINFO_USER + AND
					+ CREDITCARD_PAYMENTINFO_DUPLICATE + AND + " {" + CreditCardPaymentInfoModel.B2BUNIT + "} is NULL";


	@Override
	public List<CTCTCCardPaymentInfoModel> findCtcTcPaymentInfosByCustomer(final CustomerModel customerModel, final boolean saved)
	{
		validateCustomer(customerModel);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		if (saved)
		{
			queryParams.put(SAVED, Boolean.TRUE);
		}
		queryParams.put(DUPLICATE, Boolean.FALSE);
		final SearchResult<CTCTCCardPaymentInfoModel> result = getFlexibleSearchService().search(
				saved ? FIND_SAVED_CTC_TC_PAYMENT_INFOS_BY_CUSTOMER_QUERY : FIND_CTC_TC_PAYMENT_INFOS_BY_CUSTOMER_QUERY, queryParams);
		return result.getResult();
	}

	@Override
	public CTCTCCardPaymentInfoModel findCtcTcCardPaymentInfoByCustomerAndCode(final CustomerModel customerModel,
			final String paymentInfoId)
	{
		validateCustomer(customerModel);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		queryParams.put(DUPLICATE, Boolean.FALSE);
		queryParams.put("pk", PK.parse(paymentInfoId));
		final SearchResult<CTCTCCardPaymentInfoModel> result = getFlexibleSearchService()
				.search(FIND_CTC_TC_PAYMENT_INFO_BY_CUSTOMER_CODE_QUERY, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	@Override
	public List<CTCTCCardPaymentInfoModel> findCtcTcPaymentInfosByB2bUnit(final B2BUnitModel b2bUnit, final boolean saved)
	{
		final Map<String, Object> queryParams = getParamMap(b2bUnit, saved);
		final SearchResult<CTCTCCardPaymentInfoModel> result = getFlexibleSearchService().search(
				saved ? FIND_SAVED_CTC_TC_PAYMENT_INFOS_BY_B2BUNIT_QUERY : FIND_CTC_TC_PAYMENT_INFOS_BY_B2BUNIT_QUERY, queryParams);
		return result.getResult();
	}

	@Override
	public List<CreditCardPaymentInfoModel> findCreditCardPaymentInfosByCustomer(final CustomerModel customerModel,
			final boolean saved)
	{
		validateCustomer(customerModel);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		if (saved)
		{
			queryParams.put(SAVED, Boolean.TRUE);
		}
		queryParams.put(DUPLICATE, Boolean.FALSE);
		final SearchResult<CreditCardPaymentInfoModel> result = getFlexibleSearchService().search(
				saved ? FIND_SAVED_PAYMENT_INFOS_BY_CUSTOMER_QUERY : FIND_PAYMENT_INFOS_BY_CUSTOMER_QUERY, queryParams);
		return result.getResult();
	}

	@Override
	public List<CreditCardPaymentInfoModel> findCreditCardPaymentInfosByB2bUnit(final B2BUnitModel b2bUnit, final boolean saved)
	{

		final Map<String, Object> queryParams = getParamMap(b2bUnit, saved);
		final SearchResult<CreditCardPaymentInfoModel> result = getFlexibleSearchService().search(
				saved ? FIND_SAVED_PAYMENT_INFOS_BY_B2BUNIT_QUERY : FIND_PAYMENT_INFOS_BY_B2BUNIT_QUERY, queryParams);
		return result.getResult();
	}

	private Map<String, Object> getParamMap(final B2BUnitModel b2bUnit, final boolean saved)
	{
		validateParameterNotNull(b2bUnit, "B2B Unit must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("b2bUnit", b2bUnit);
		if (saved)
		{
			queryParams.put(SAVED, Boolean.TRUE);
		}
		queryParams.put(DUPLICATE, Boolean.FALSE);
		return queryParams;
	}

	private void validateCustomer(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "Customer must not be null");
	}


}
