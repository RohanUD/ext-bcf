/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.customer;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.travelservices.customer.TravelCustomerAccountService;
import java.util.List;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;


public interface BcfCustomerAccountService extends TravelCustomerAccountService
{

	OrderModel getGuestOrderForGUID(final String guid);

	List<CTCTCCardPaymentInfoModel> getCtcTcPaymentInfos(final CustomerModel customerModel,
			final boolean saved);

	void deleteCtcTcCardPaymentInfo(CustomerModel currentUser, CTCTCCardPaymentInfoModel paymentInfoModel);

	CTCTCCardPaymentInfoModel getCtcTcCardPaymentInfoForCode(CustomerModel currentUser, String paymentInfoId);
}
