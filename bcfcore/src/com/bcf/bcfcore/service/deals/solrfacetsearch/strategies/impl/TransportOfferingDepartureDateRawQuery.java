/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcore.service.deals.solrfacetsearch.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.deals.solrfacetsearch.strategies.RawQueryStrategy;
import com.bcf.core.solr.query.ArguementData;


public class TransportOfferingDepartureDateRawQuery implements RawQueryStrategy
{
	private ConfigurationService configurationService;

	@Override
	public ArguementData generateArguementForRawQuery(String rawFilterKey, String rawfilterValue)
	{
		final String startDateParamName = getConfigurationService().getConfiguration().getString("transportoffering.raw.query.param."+rawFilterKey.toLowerCase());

		final ArguementData arguementData = new ArguementData();
		arguementData.setStartDateFieldName(startDateParamName);
		arguementData.setFieldArguement(rawfilterValue);

		return arguementData;
	}

	@Override
	public RawQuery createRawQuery(ArguementData arguementData)
	{
		String[] valueArray = new String[2];
		if(StringUtils.contains(arguementData.getFieldArguement(), BcfCoreConstants.UNDERSCORE))
		{
			valueArray = arguementData.getFieldArguement().split(BcfCoreConstants.UNDERSCORE);
		}
		StringBuilder formattedDateQuery = new StringBuilder();
		formattedDateQuery.append(BcfCoreConstants.LEFT_ANGLE_BRACKET);
		formattedDateQuery.append(arguementData.getStartDateFieldName()+BcfCoreConstants.DATE_TYPE_SOLR_LITERAL);
		formattedDateQuery.append(BcfCoreConstants.LEFT_SQUARE_BRACKET);
		formattedDateQuery.append(valueArray[0]);
		formattedDateQuery.append(BcfCoreConstants.SOLR_TO_OPERATOR);
		formattedDateQuery.append(valueArray[1]);
		formattedDateQuery.append(BcfCoreConstants.RIGHT_SQUARE_BRACKET);
		formattedDateQuery.append(StringUtils.SPACE);
		formattedDateQuery.append(BcfCoreConstants.RIGHT_ANGLE_BRACKET);

		return new RawQuery(formattedDateQuery.toString());
	}

	protected ConfigurationService getConfigurationService() {
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
