/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.seo.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.bcf.core.model.content.SEOMetaContentModel;
import com.bcf.core.seo.dao.SEOMetaContentDao;
import com.bcf.core.seo.dao.impl.DefaultSEOMetaContentDao;
import com.bcf.core.util.CMSSiteHelper;


/**
 * Unit tests for {@link DefaultSEOMetaContentServiceUnitTest}.
 */
@UnitTest
public class DefaultSEOMetaContentServiceUnitTest
{

	private DefaultSEOMetaContentService metaContentService;

	private SEOMetaContentDao metaContentDao;

	private List<CatalogVersionModel> catalogVarsions;


	@Before
	public void setUp()
	{
		// mock seo dao
		metaContentDao = Mockito.mock(DefaultSEOMetaContentDao.class);

		catalogVarsions = CMSSiteHelper.getAllActiveContentCatalogVersions();


		final SessionService sessionService = Mockito.mock(SessionService.class);

		final SearchRestrictionService srs = Mockito.mock(SearchRestrictionService.class);
		Mockito.doNothing().when(srs).clearSessionSearchRestrictions();

		metaContentService = new DefaultSEOMetaContentService();
		metaContentService.setMetaContentDao(metaContentDao);

		// create user service

		Mockito.when(sessionService.executeInLocalView(Mockito.any(SessionExecutionBody.class))).thenAnswer(new Answer<Object>()
		{
			@Override
			public Object answer(final InvocationOnMock invocation)
			{
				final SessionExecutionBody args = (SessionExecutionBody) invocation.getArguments()[0];
				return args.execute();
			}
		});
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSEOContentForNoUid()
	{
		metaContentService.getMetaContent(null, catalogVarsions);
		fail("NPE expected");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSEOContentForNoCatalogVersion()
	{
		metaContentService.getMetaContent("homepagemetacontent", null);
		fail("NPE expected");
	}

	@Test
	public void testFindSEOContent()
	{
		final SEOMetaContentModel seoContent = new SEOMetaContentModel();
		seoContent.setUid("homepagemetacontent");


		Mockito.when(metaContentDao.findMetaContent(seoContent.getUid(), catalogVarsions)).thenReturn(seoContent);

		assertEquals("not the same article model", seoContent,
				metaContentService.getMetaContent(seoContent.getUid(), catalogVarsions));
	}


}
