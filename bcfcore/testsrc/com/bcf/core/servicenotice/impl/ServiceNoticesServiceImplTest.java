/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.servicenotice.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.bcf.core.dao.servicenotice.dao.ServiceNoticesDao;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.services.servicenotice.impl.ServiceNoticesServiceImpl;


/**
 * JUnit test suite for {@link ServiceNoticesServiceImpl}
 */
@UnitTest
public class ServiceNoticesServiceImplTest
{

	@Mock
	private ServiceNoticesDao serviceNoticesDao;
	private ServiceNoticesServiceImpl serviceNoticesService;

	private String SERVICE_NOTICE_CODE = "testServiceNotice1";

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		serviceNoticesService = new ServiceNoticesServiceImpl();
		serviceNoticesService.setServiceNoticesDao(serviceNoticesDao);
	}

	@Test
	public void testGetServiceNoticeForRouteCode()
	{
		given(serviceNoticesDao.findServiceNoticeForCode(SERVICE_NOTICE_CODE)).willReturn(getTestServiceNoticeModel());
		ServiceNoticeModel actualModel = serviceNoticesService.getServiceNoticeForCode(SERVICE_NOTICE_CODE);
		Assert.assertEquals(getTestServiceNoticeModel(), actualModel);
	}

	@Test
	public void testGetServiceNoticesForAllRoutes()
	{
		given(serviceNoticesDao.findServiceNotices()).willReturn(getTestServiceNotices());
		List<ServiceNoticeModel> actualResult = serviceNoticesService.getServiceNoticesForAllRoutes();
		Assert.assertEquals(1, actualResult.size());
	}

	@Test
	public void testGetPaginatedServiceNoticesForAllRoutes()
	{
		PageableData paginationData = new PageableData();
		paginationData.setPageSize(1);
		paginationData.setCurrentPage(0);

		given(serviceNoticesDao.findPaginatedServiceNotices(paginationData)).willReturn(getTestSearchPageDataForServiceNotice());

		SearchPageData<ServiceNoticeModel> actualResult = serviceNoticesService
				.getPaginatedServiceNoticesForAllRoutes(paginationData);

		Assert.assertEquals(1, actualResult.getResults().size());
	}

	@Test
	public void testGetNotPublishedServiceNotices()
	{
		given(serviceNoticesDao.findNonPublishedServiceNotices()).willReturn(getTestServiceNotices());
		List<ServiceNoticeModel> actualResult = serviceNoticesService.getNotPublishedServiceNotices();
		Assert.assertEquals(1, actualResult.size());
	}

	private ServiceNoticeModel getTestServiceNoticeModel()
	{
		ServiceNoticeModel model = mock(ServiceNoticeModel.class);
		return model;
	}

	private List<ServiceNoticeModel> getTestServiceNotices()
	{
		ServiceNoticeModel model = mock(ServiceNoticeModel.class);
		return Collections.singletonList(model);
	}

	private SearchPageData<ServiceNoticeModel> getTestSearchPageDataForServiceNotice()
	{
		SearchPageData<ServiceNoticeModel> spd = new SearchPageData<>();
		spd.setResults(getTestServiceNotices());
		return spd;
	}

}
