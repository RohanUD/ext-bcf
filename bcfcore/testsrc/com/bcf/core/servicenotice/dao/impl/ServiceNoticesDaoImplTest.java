/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.core.servicenotice.dao.impl;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import com.bcf.core.dao.servicenotice.dao.impl.ServiceNoticesDaoImpl;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;


public class ServiceNoticesDaoImplTest
{

	@Resource
	private ModelService modelService;

	private final String TEST_SERVICE_NOTICE_CODE = "testServiceNotice1";
	private final String TEST_NEWS_NOTICE_CODE = "testNews1";
	private static final Date currentDate = new Date();

	@Resource
	private ServiceNoticesDaoImpl serviceNoticesDao;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	PagedFlexibleSearchService pagedFlexibleSearchService;

	private ServiceModel serviceModel1;
	private NewsModel newsModel1;
	private String serviceModel1Code;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		serviceModel1 = modelService.create(ServiceModel.class);
		serviceModel1.setTitle(TEST_SERVICE_NOTICE_CODE);
		serviceModel1.setPublishDate(currentDate);

		newsModel1 = modelService.create(NewsModel.class);
		newsModel1.setTitle(TEST_NEWS_NOTICE_CODE);
		newsModel1.setPublishDate(currentDate);
		newsModel1.setPublished(Boolean.FALSE);
		newsModel1.setSendEmail(Boolean.TRUE);

		modelService.saveAll(serviceModel1, newsModel1);
		modelService.refresh(serviceModel1);
		serviceModel1Code = serviceModel1.getPk().getLongValueAsString();
	}

	@After
	public void tearDown()
	{
		modelService.removeAll(serviceModel1, newsModel1);
	}

	@Test
	public void testFindServiceNotices()
	{
		List<ServiceNoticeModel> actualResult = serviceNoticesDao.findServiceNotices();
		Assert.assertTrue(actualResult.size() >= 2);
		Assert.assertTrue(actualResult.contains(serviceModel1));
	}

	@Test
	public void testFindPaginatedServiceNotices()
	{
		PageableData paginationData = new PageableData();
		paginationData.setCurrentPage(0);
		paginationData.setPageSize(5);

		SearchPageData<ServiceNoticeModel> actualResult = serviceNoticesDao.findPaginatedServiceNotices(paginationData);
		Assert.assertTrue(actualResult.getResults().size() >= 2);
		Assert.assertTrue(actualResult.getResults().contains(serviceModel1));
	}

	@Test
	public void testFindServiceNoticeForCode()
	{
		ServiceNoticeModel actualResult = serviceNoticesDao.findServiceNoticeForCode(serviceModel1Code);
		Assert.assertEquals(TEST_SERVICE_NOTICE_CODE, actualResult.getTitle());
	}

	@Test
	public void testFindNonPublishedServiceNotices()
	{
		List<ServiceNoticeModel> actualResult = serviceNoticesDao.findNonPublishedServiceNotices();
		Assert.assertTrue(actualResult.size() >= 1);
		Assert.assertTrue(actualResult.contains(newsModel1));
	}
}
