/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.constants;

/**
 * Global class for all Bcfaccommodationsaddon constants. You can add global constants for your extension into this class.
 */
public final class BcfaccommodationsaddonConstants extends GeneratedBcfaccommodationsaddonConstants
{
	public static final String EXTENSIONNAME = "bcfaccommodationsaddon";

	private BcfaccommodationsaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
