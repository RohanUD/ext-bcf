<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row ">
	<div class="input-required-wrap col-xs-6 col-md-6 col-sm-6">
		<c:if test="${empty travelRouteData}">
			<c:choose>
				<c:when test="${!showComponent}">
					<spring:theme code="text.cms.accommodationfinder.modify.title" text="Modify Search" />
				</c:when>
				<c:otherwise>
					<label class="font-weight-light">
						<spring:theme code="text.vacation.packagefinder.title" text="Destination" />
					</label>
				</c:otherwise>
			</c:choose>
			<label class="sr-only" for="y_travelFinderLocation">
				<spring:theme var="destinationLocationPlaceholderText" code="text.cms.accommodationfinder.destination.placeholder" text="Where are you going?" />
				${fn:escapeXml(destinationLocationPlaceholderText)}
			</label>
		</c:if>
		<form:hidden path="${fn:escapeXml(formPrefix)}destinationLocation" class="y_travelFinderLocationCode" />
		<form:hidden path="${fn:escapeXml(formPrefix)}suggestionType" class="y_accommodationFinderLocationSuggestionType" />
		<div id="y_travelFinderLocationSuggestions" class="hidden clearfix"></div>
		<div class="input-required-wrap ">
			<c:choose>
				<c:when test="${not empty destinationCode}">
					<select class="bc-dropdown bc-dropdown--big y_vacationDestination" style="display: none">
						<option value="${destinationCode}" disabled selected>${destinationName}</option>
					</select>
				</c:when>
				<c:otherwise>
							<select class="form-control bc-dropdown--big y_vacationDestination">
						<option value="" disabled selected>Select your option</option>
						<c:forEach var="bcfVacationLocationEntry" items="${bcfVacationLocations}">
							<c:set var="geoAreaVacationLocation" value="${bcfVacationLocationEntry.key}" />
							<c:set var="cityVacationLocations" value="${bcfVacationLocationEntry.value}" />
							<option value="${geoAreaVacationLocation.code}" disabled selected class="bold">${fn:toUpperCase(geoAreaVacationLocation.name)}</option>
							<c:forEach var="cityVacationLocation" items="${cityVacationLocations}">
								<option value="${cityVacationLocation.code}">${cityVacationLocation.name}</option>
							</c:forEach>
						</c:forEach>
					</select>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class=" checkInAndCheckOutDiv">
		<div class="input-required-wrap col-xs-6 col-md-3 col-lg-3 col-sm-3">
			<label class="font-weight-light">
				<spring:theme code="text.vacation.packagefinder.checkindate.title" text="Check-in date" />
			</label>
			<input id="y_maxAllowedCheckInCheckOutDateDifference" type="hidden" value="${fn:escapeXml(maxAllowedCheckInCheckOutDateDifference)}" />
			<label class="sr-only" for="y_${fn:escapeXml( idPrefix)}DatePickerCheckIn">
				<spring:theme var="checkInDatePlaceholderText" code="text.cms.accommodationfinder.checkin.date.placeholder" text="Check In" />
			</label>
			<form:input type="text" id="y_${fn:escapeXml( idPrefix)}DatePickerCheckIn" path="${fn:escapeXml(formPrefix)}checkInDateTime" class=" form-control datePickerDeparting  datepicker bc-dropdown bc-dropdown--big  y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckIn" placeholder="Check In" autocomplete="off" />
		</div>
		<div class="input-required-wrap col-xs-6 col-md-3 col-lg-3 col-sm-3">
			<label class="font-weight-light">
				<spring:theme code="text.vacation.packagefinder.checkoutdate.title" text="Check-out date" />
			</label>
			<label class="sr-only" for="y_${fn:escapeXml( idPrefix)}DatePickerCheckOut">
				<spring:theme var="checkOutDatePlaceholderText" code="text.cms.accommodationfinder.checkout.date.placeholder" text="Check Out" />
			</label>
			<form:input type="text" id="y_${fn:escapeXml( idPrefix)}DatePickerCheckOut" path="${fn:escapeXml(formPrefix)}checkOutDateTime" class="form-control datePickerReturning  datepicker bc-dropdown bc-dropdown--big  y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckOut" placeholder="Check Out" autocomplete="off" />
		</div>
	</div>
</div>
