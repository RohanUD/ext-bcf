<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationdetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ attribute name="property" required="true" type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<%@ taglib prefix="accommodationsearch" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationsearch"%>
<%@ attribute name="stayDateRange" required="true" type="de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="maxFractionDigits" value="1" />
<li class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-5 ${property.promoted ? 'promoted' : ''}">
	<div class="accommodation-image package-list-owl">
		<div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(property.images)}">
			<c:forEach var="image" items="${property.images}">
				<c:if test="${image.imageType eq 'PRIMARY'}">
					<c:set var="dataMedia" value="${image.url}" />
					<div class="image">
						<img class='js-responsive-image' alt='${altText}' title='${altText}' data-media='${dataMedia}' />
						<div class="slider-count">
							<div class="slider-img-thumb"></div>
							<div class="slider-num">1/5</div>
						</div>
					</div>
				</c:if>
			</c:forEach>
		 
		<c:if test="${property.availabilityStatus eq 'SOLD_OUT' && fn:length(property.images) gt 0}">
            <div class="overlay-sold-out">
                <div class="overlay-text">
                    <p><spring:theme code="message.package.hotel.not.available" /></p>
                    <a href="/#tabs-3" class="btn-white mt-4">Modify search</a>
                </div>
		    </div>
        </c:if>
        </div>
	</div>
	<div class="deal-details">
		<div class="row mb-2">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<accommodationdetails:propertyCategoryTypeDetails property="${property}" />
			</div>
		</div>
		<div class="row flex-box">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-6 mb-3 m-mb-2 deal-details-border">
				<h3 class="min-height-60 title-listing">
					${fn:escapeXml(property.accommodationOfferingName)}
					<c:if test="${property.isDealOfTheDay}">
						<c:out value="(DEAL OF THE DAY)" />
					</c:if>
				</h3>
                <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                    <spring:param name="locationId"  value="${property.reviewLocationId}"/>
                </spring:url>
				<div class="review-img">
					<h5 class="mb-2 trip-advisor-title">Trip Advisor Traveler Rating</h5>
					<p class="mb-2 trip-advisor-reviews ">
						<img src="${fn:escapeXml(property.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating" width="100" height="30">
						<a class="ratings--review-container tripAdvisorLink" href="${tripAdvisorURL}">${property.customerReviewData.numOfReviews} Reviews</a>
					</p>
				</div>
				<div class="text-reviews text-div mb-3 fnt-14">
					${fn:escapeXml(property.description)}
					<c:out value="${property.description}" />
				</div>
				<div class="learn-link">
					<a href="/accommodation-details/${property.accommodationOfferingCode}?${urlParameters}&tab=tab-01">
						Learn more <i class="fas bcf bcf-icon-right-arrow ml-1 fnt-28"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-6 text-center total-price-wrapper">
			    <c:choose>
			        <c:when test="${property.availabilityStatus eq 'SOLD_OUT'}">
			            <div class="sold-out-section">
                            <div class="sold-out-text box-align-center">
                                <spring:theme code="text.package.listing.hotel.sold.out" />
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${not empty property.totalPrice}">
                            <accommodationsearch:perPersonPrice perPersonPrice="${property.perPersonPrice}" lengthOfStay="${stayDateRange.lengthOfStay}" />
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-2 p-0">
                                <div class="total text-center">
                                    <c:choose>
                                        <c:when test="${isAmendment}">
                                            <c:choose>
                                                <c:when test="${property.totalPrice.value>0}">
                                                    <spring:theme code="text.accommodation.additional.per.person.payment.amount" />
                                                </c:when>
                                                <c:otherwise>
                                                    <spring:theme code="text.accommodation.per.person.refund.amount" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <spring:theme code="text.package.listing.total.price" text="Total price:" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <h4 class="text-center mt-0 mb-0 small-price">
                                    <span class="sr-only">
                                        <spring:theme code="text.page.deallisting.deal.price.sale.sr" text="Sale price" />
                                    </span>
                                    <strong><format:price priceData="${property.totalPrice}" /></strong>
                                </h4>
                                <div class="vacations-listing--section-title-small price-text-section">
                                    <spring:theme code="text.page.deallisting.deal.trip.price.extra" text="+ taxes and fees" />
                                </div>
                            </div>
                        </c:if>
                        <c:url var="detailsPageUrl" value="/accommodation-details/${property.accommodationOfferingCode}?${urlParameters}" />
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 text-center p-0 sold-out-text box-align-center">
                            <c:choose>
                                <c:when test="${property.availabilityStatus eq 'AVAILABLE'}">
                                    <a href="${detailsPageUrl}" class="btn btn-primary">
                                        <spring:theme code="text.package.listing.button.continue" />
                                    </a>
                                </c:when>
                                <c:when test="${property.availabilityStatus eq 'SOLD_OUT'}">
                                    <spring:theme code="text.package.listing.hotel.sold.out" />
                                </c:when>
                                <c:when test="${property.availabilityStatus eq 'ON_REQUEST'}">
                                    <a href="${detailsPageUrl}" class="btn btn-primary">
                                        <spring:theme code="text.package.listing.button.request.booking" />
                                    </a>
                                </c:when>
                                <c:when test="${property.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
                                    <a href="${fn:escapeXml(request.contextPath)}/get-quote?name=${property.accommodationOfferingName}" class="btn btn-primary">
                                        <spring:theme code="text.package.listing.button.notBookableOnline" />
                                    </a>
                                </c:when>
                            </c:choose>
                        </div>
                    </c:otherwise>
                </c:choose>
			</div>
		</div>
	</div>
</li>
