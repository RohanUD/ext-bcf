<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div id="y_accommodationListingShowMore" class="hidden"></div>
<c:if test="${totalNumberOfResults gt 0}">
   <div class="row mb-5 clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mb-3">
         <p class="y_shownResultId font-weight-light font-italic text-gray">
            <spring:theme code="text.accommodation.listing.shown.hotels.results" arguments="${startingNumberOfResults}, ${totalShownResults}, ${totalNumberOfResults}" />
         </p>
      </div>
      <pagination:jsonpagination paginationCss="y_showSpecificPageResults"/>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 text-center">
         <div class="back-top-btn">
            <a href="#">
               <span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span>
               <strong>
                  <spring:theme code="text.accommodation.lsiting.back.to.top" text="Back To Top" />
               </strong>
            </a>
         </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <hr>
      </div>
   </div>
</c:if>
