<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="travelfinder" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/travelfinder"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:url var="submitRequestUrl" value="/manage-booking/adjust-cost" />
<input type="hidden" name="adjustCostErrorFlag" id="adjustCostErrorFlag" value="${adjustCostErrorFlag}">

<div class="modal fade" id="y_AdjustCostModal" tabindex="-1" role="dialog" aria-labelledby="y_AdjustCostModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="adjustCostLabel">
					<spring:theme code="text.adjustcost.form.request" text="Adjust Cost" />
				</h3>
			</div>
            <div class="modal-body">
			    <form:form id="y_AdjustCostForm" modelAttribute="adjustCostForm" action="${submitRequestUrl}" method="POST" class="y_adjustCostForm">
                    <c:if test="${not empty adjustCostForm.adjustAccommodationCostDatas}">
                        <c:forEach items="${adjustCostForm.adjustAccommodationCostDatas}" var="adjustAccommodationCostData" varStatus="i">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    ${adjustAccommodationCostData.refDescription}

                                    <form:input type="hidden" id="adjustAccommodationRefDescription" path="adjustAccommodationCostDatas[${i.index}].refDescription" value="${adjustAccommodationCostData.refDescription}"/>
                                    <form:input type="text" path="adjustAccommodationCostDatas[${i.index}].cost" class="col-xs-12 input-grid form-control" id="cost" name="cost" ></form:input>
                                    <form:input type="hidden" id="accommodationRefNumber" path="adjustAccommodationCostDatas[${i.index}].refNumber" value="${adjustAccommodationCostData.refNumber}"/>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>

                     <c:if test="${not empty adjustCostForm.adjustActivityCostDatas}">

                        <c:forEach items="${adjustCostForm.adjustActivityCostDatas}" var="adjustActivityCostData" varStatus="j">
                            <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs-12">
                                    ${adjustActivityCostData.refDescription}
                                    <form:input type="hidden" id="adjustActivityRefDescription" path="adjustActivityCostDatas[${j.index}].refDescription" value="${adjustActivityCostData.refDescription}"/>
                                    <form:input type="text" path="adjustActivityCostDatas[${j.index}].cost" class="col-xs-12 input-grid form-control" id="cost" name="cost" ></form:input>
                                    <form:input type="hidden" id="activityEntryNo" path="adjustActivityCostDatas[${j.index}].refNumber" value="${adjustActivityCostData.refNumber}" />
                                </div>

                             </div>
                        </c:forEach>


                     </c:if>
                    <input type="hidden" name="bookingReference" id="bookingReference" value="${fn:escapeXml(bookingReference)}">
                    <spring:message code="${errorMsg}" />
                    <div class="col-md-6">
                        <button class="btn btn-primary btn-block" type="submit" id="adjustCostFormButton" value="Submit">
                            <spring:theme code="button.adjustcost.form.request" text="Adjust Cost" />
                        </button>
                    </div>
                </form:form>
 	        </div>
		</div>
	</div>
</div>



