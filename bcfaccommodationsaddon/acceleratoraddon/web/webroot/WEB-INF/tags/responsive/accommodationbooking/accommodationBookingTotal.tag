<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="totalRateFormattedValue" required="true" type="java.lang.String"%>
<%@ attribute name="totalRateValue" required="false" type="java.math.BigDecimal"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="panel-default total">
	<div class="panel-body">
		<div class="col-xs-12">
			<dl class="text-right">
				<dt>
					<spring:theme code="text.cms.accommodationbreakdown.grandTotal" text="Booking Total" />
				</dt>
				<dd>${fn:escapeXml(totalRateFormattedValue)}</dd>
			</dl>
		</div>
	</div>
</div>
