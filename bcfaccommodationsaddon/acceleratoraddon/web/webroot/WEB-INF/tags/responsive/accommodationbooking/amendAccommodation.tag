<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="travelfinder" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/travelfinder"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="travelFinderUrl" value="/view/PackageFinderComponentController/search" />
<div class="yCmsContentSlot">
   <c:if test="${not empty travelFinderForm}">
      <div class="modal fade" id="y_amendAccommodationModal" tabindex="-1"
         role="dialog" aria-labelledby="replanJourneyLabel">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"
                     aria-label="Close">
                  <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                  </button>
                  <h3 class="modal-title" id="amendAccommodationLabel">
                     <spring:theme code="text.page.managevacation.journey.modal.title" text="Amend Accommodation" />
                  </h3>
               </div>
               <c:choose>
                  <c:when test="${hasErrorFlag}">
                     <p>
                        <spring:message code="${errorMsg}" />
                     </p>
                  </c:when>
                  <c:otherwise>
                     <div class="modal-body">
                        <c:url var="fareFinderUrl" value="/manage-booking/amend-accommodation" />
                        <div class="custom-error-3 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>
                        <form:form commandName="travelFinderForm" action="${fareFinderUrl}" method="POST" class="fe-validate form-background form-booking-trip" id="y_travelFinderForm">
                           <fieldset>
                              <legend class="with-icon heading-booking-trip primary-legend" id="trip-finder-modal-title">
                                 <spring:theme code="text.cms.farefinder.title" text="Booking Trip" />
                              </legend>
                              <div id="accommodation-finder-panel" class="fieldset-inner-wrapper">
                                 <div class="row">
                                    <div class="input-required-wrap col-xs-6 col-md-3 col-lg-3 col-sm-3">
                                       <label for="fromLocation" class="home-label-mobile">
                                          <spring:theme code="text.vacation.packagefinder.title" text="Destination" />
                                          <h4 class="amend-booking-boundry">${travelFinderForm.accommodationFinderForm.destinationLocationName}</h4>
                                       </label>
                                    </div>
                                    <div class="col-xs-6 col-md-3 col-lg-3 col-sm-3">
                                       <label class="font-weight-light">
                                          <spring:theme code="text.vacation.packagefinder.route.title" text="Route"/>
                                       </label>
                                       <form:select name="travelRouteData" path="travelRoute" class="bc-dropdown bc-dropdown--big y_travelRoute">
                                          <c:forEach var="travelRoute" items="${travelRouteData}">
                                             <c:choose>
                                                <c:when test="${travelFinderForm.travelRoute==travelRoute.code}">
                                                   <form:option value="${travelRoute.code}" selected="selected">${travelRoute.name}</form:option>
                                                </c:when>
                                                <c:otherwise>
                                                   <form:option value="${travelRoute.code}">${travelRoute.name}</form:option>
                                                </c:otherwise>
                                             </c:choose>
                                          </c:forEach>
                                       </form:select>
                                    </div>
                                    <form:hidden path="accommodationFinderForm.destinationLocation" class="y_destinationLocation" />
                                    <form:hidden path="accommodationFinderForm.destinationLocationName" class="y_destinationLocationName" />
                                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 vacation-calender-section bc-accordion ferry-drop vacation-calender-section pull-right">
                                       <ul class="nav nav-tabs vacation-calender package-hotel-details-ul">
                                          <li class="tab-links tab-depart">
                                             <label>
                                                <spring:theme code="label.farefinder.vacation.date.checkin" text="Check-in-date" />
                                             </label>
                                             <a data-toggle="tab" href="#check-in" class="nav-link component-to-top">
                                                <div class="vacation-calen-box vacation-ui-depart amend-booking-boundry">
                                                   <span class="vacation-calen-date-txt">
                                                      <spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" />
                                                   </span>
                                                   <span class="vacation-calen-year-txt current-year-custom-amend">${travelFinderForm.accommodationFinderForm.checkInDateTime}</span>
                                                   <i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
                                                      aria-hidden="true"></i>
                                                </div>
                                             </a>
                                          </li>
                                          <li class="tab-links tab-return">
                                             <label>
                                                <spring:theme code="label.farefinder.vacation.date.checkout" text="Check-out-date" />
                                             </label>
                                             <a data-toggle="tab" href="#check-out" class="nav-link component-to-top">
                                                <div class="vacation-calen-box vacation-ui-return amend-booking-boundry">
                                                   <span class="vacation-calen-date-txt">
                                                      <spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" />
                                                   </span>
                                                   <span class="vacation-calen-year-txt current-year-custom-amend">${travelFinderForm.accommodationFinderForm.checkOutDateTime}</span>
                                                   <i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
                                                      aria-hidden="true"></i>
                                                </div>
                                             </a>
                                          </li>
                                       </ul>
                                       <div class="tab-content package-hotel-details-content">
                                          <div id="check-in" class="tab-pane input-required-wrap  depart-calendar">
                                             <div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
                                                <label>
                                                   <spring:theme code="label.farefinder.vacation.date.dateformat" text="Search date:mm/dd/yyyy" />
                                                </label>
                                                <form:input type="text"
                                                   path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkInDateTime"
                                                   class="datepicker bc-dropdown depart datepicker-input y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckIn"
                                                   placeholder="Check In"
                                                   autocomplete="off"/>
                                                <div id="datepicker" class="bc-dropdown--big">
                                                </div>
                                             </div>
                                          </div>
                                          <form:errors path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkInDateTime" />
                                          <div id="check-out" class="tab-pane input-required-wrap return-calendar">
                                             <div class="bc-dropdown calender-box tab-content-return" id="js-return">
                                                <label>
                                                   <spring:theme code="label.farefinder.vacation.date.dateformat" text="Search date:mm/dd/yyyy" />
                                                </label>
                                                <form:input type="text"
                                                   path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkOutDateTime"
                                                   class="datePickerReturning  datepicker arrive datepicker-input bc-dropdown  y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckOut"
                                                   placeholder="Check Out" autocomplete="off"/>
                                                <div id="datepicker1" class="bc-dropdown--big">
                                                </div>
                                             </div>
                                          </div>
                                          <form:errors path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkOutDateTime" />
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="row">
                                 <div
                                    class="y_passengerWrapper ">
                                    <label class="font-weight-light">
                                       <spring:theme
                                          code="text.vacation.packagefinder.numberofrooms.title"
                                          text="Number of rooms" />
                                    </label>
                                    <c:set var="noOfRoomsMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.number.rooms.max.count')}"/>
                                    <input type="hidden" id="noOfRoomsMax" name="noOfRoomsMax" value="${noOfRoomsMaxValue}"/>
                                    <div
                                       class="input-group input-btn-mp text-center mb-3 roomSelector">
                                       <span class="fa bcf bcf-icon-remove btn-number" disabled="disabled"
                                          data-type="minus"
                                          data-field="accommodationFinderForm.numberOfRooms"></span>
                                       <form:input type="text" id="y_roomQuantity"
                                          class="input-number min-input2"
                                          value="${travelFinderForm.accommodationFinderForm.numberOfRooms }"
                                          min="1" max="${noOfRoomsMaxValue}"
                                          path="accommodationFinderForm.numberOfRooms" />
                                       <span class="fa bcf bcf-icon-add btn-number" data-type="plus"
                                          data-field="accommodationFinderForm.numberOfRooms"></span>
                                    </div>
                                 </div>
                                 <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                                    <div class="row">
                                       <c:forEach var="roomCandidatesEntry"
                                          items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}"
                                          varStatus='idx'>
                                          <div
                                             class="col-xs-12 bc-accordion guest-types select-guest-custom package-edit-sec custom-room-all custom-room-${idx.index+1}">
                                             <label class="font-weight-light"> Room
                                             ${idx.index+1} </label>
                                             <div class="custom-accordion">
                                                <h3>
                                                   <spring:theme
                                                      code="label.packagedetails.guestinfo.guest.dropdown.heading"
                                                      text="Select guests" />
                                                   <span class="custom-arrow"></span>
                                                </h3>
                                                <div class="vacationGuestUsers">
                                                   <c:forEach var="entry"
                                                      items="${roomCandidatesEntry.passengerTypeQuantityList}"
                                                      varStatus="i">
                                                      <c:choose>
                                                         <c:when test="${entry.passengerType.code eq 'adult'}">
                                                            <c:set var="minQuantity" value="1" />
                                                         </c:when>
                                                         <c:otherwise>
                                                            <c:set var="minQuantity" value="0" />
                                                         </c:otherwise>
                                                      </c:choose>
                                                      <div
                                                         class="col-lg-6 col-md-6 col-sm-12 col-xs-6 margin-top-10 y_passengerWrapper ">
                                                         <label
                                                            for="y_${fn:escapeXml(entry.passengerType.code)}">
                                                            <c:choose>
                                                               <c:when test="${entry.passengerType.code eq 'adult'}">
                                                                  <spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 19 yrs+" />
                                                               </c:when>
                                                               <c:otherwise>
                                                                  <spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-18 yrs" />
                                                               </c:otherwise>
                                                            </c:choose>
                                                         </label>
                                                         <c:set var="adultMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.adult.max.count')}"/>
                                                         <c:set var="childMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.child.max.count')}"/>
                                                         <div class="input-group input-btn-mp">
                                                            <span class="fa bcf bcf-icon-remove btn-number"
                                                               disabled="disabled" data-type="minus"
                                                               data-field="accommodationFinderForm.roomStayCandidates[${idx.index}].passengerTypeQuantityList[${i.index}].quantity"></span>
                                                            <form:input type="text"
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${i.index}].quantity"
                                                               class="${fn:escapeXml(entry.passengerType.code)}-qty y_${fn:escapeXml(entry.passengerType.code)}Select"
                                                               value="${entry.quantity}" min="${minQuantity}"
                                                               max="${ entry.passengerType.code eq 'adult'?adultMaxValue:childMaxValue}" />
                                                            <div id="room-number${idx.index}" class="room-number hidden">${idx.index}</div>
                                                            <span class="fa bcf bcf-icon-add btn-number"
                                                               data-type="plus"
                                                               data-field="accommodationFinderForm.roomStayCandidates[${idx.index}].passengerTypeQuantityList[${i.index}].quantity"></span>
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].roomStayCandidateRefNumber"
                                                               id="y_roomStayCandidateRefNumber" type="hidden" readonly="true" />
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code"
                                                               id="y_passengerCode" type="hidden" readonly="true" />
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name"
                                                               id="y_passengerTypeName" type="hidden"
                                                               readonly="true" />
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge"
                                                               type="hidden" readonly="true" />
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge"
                                                               type="hidden" readonly="true" />
                                                            <form:input
                                                               path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.bcfCode"
                                                               type="hidden" readonly="true" />
                                                         </div>
                                                      </div>
                                                      <c:if test="${entry.passengerType.code eq 'child'}">
                                                         <div id="child-box-wrapper${ idx.index}${i.index}" class="mb-4 child-box-wrapper clearfix"></div>
                                                         <c:forEach var="childAge" items="${entry.childAges}" varStatus="k">
                                                            <div id="childAge${ idx.index}${i.index}${k.index}" class="hidden">${childAge}</div>
                                                         </c:forEach>
                                                      </c:if>
                                                   </c:forEach>
                                                </div>
                                             </div>
                                             <p>
                                                <strong>
                                                   <spring:theme code="text.page.pendingorders.header.order.placedby" text="Guest Name" />
                                                </strong>
                                                :
                                                <div class="row">
                                                    <form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].firstName" id="y_firstName" type="text" />
                                                    <form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].lastName" id="y_lastName" type="text" />
                                                </div>
                                             </p>
                                             <div class="vacation-guest-gray hidden">
                                                <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12">
                                                      <c:forEach var="entry"
                                                         items="${roomCandidatesEntry.passengerTypeQuantityList}"
                                                         varStatus="i">
                                                         <p>
                                                            <span
                                                               class="${fn:escapeXml(entry.passengerType.code)}-count">0</span>
                                                            x
                                                            <c:choose>
                                                               <c:when test="${entry.passengerType.code eq 'adult'}">
                                                                  <spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 12 yrs+" />
                                                               </c:when>
                                                               <c:otherwise>
                                                                  <spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-12 yrs" />
                                                               </c:otherwise>
                                                            </c:choose>
                                                         </p>
                                                      </c:forEach>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </c:forEach>
                                       <div class="snippets hidden">
                                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 child-box">
                                             <label class="child-label-name">
                                                <spring:theme code="label.farefinder.vacation.rooms.child.title" text="Child" />
                                                &nbsp;<span class="child-label-name-span">1</span>
                                                <spring:theme code="label.farefinder.vacation.rooms.age" text="age" />
                                             </label>
                                             <select class="form-control custom-select">
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-12 y_roomStayCandidatesError"></div>
                              <%-- End Guests input field section --%>
                     </div>
                     <div class="row mt-4">
                     <div class="col-xs-12">
                     <button class="btn btn-primary btn-block" type="submit" value="Submit">Submit</button>
                     </div>
                     </div>
                     <input type="hidden" id="bookingReference" name="bookingReference" value="${bookingReference}" />
                     <input type="hidden" id="journeyRef" name="journeyRef" value="${journeyRef}" />
                     <%-- / .fieldset-inner-wrapper --%>
                     </fieldset>
                     </form:form>
                     <script type="application/javascript">
                        function showRoomQuantity (){
                            var roomId = '#room';
                         var hide = 'hidden';
                            var quantity=document.getElementById('hotelAccommodationQuantity').value;
                            var i;
                         for (i = 1; i <= quantity; i++) {
                             $(roomId+'\\['+i+'\\]').removeClass(hide);
                         };
                         $(roomId+'\\['+(i-1)+'\\]').nextUntil('.y_roomStayCandidatesError').addClass(hide);
                        }
                     </script>
            </div>
            </c:otherwise>
            </c:choose>
         </div>
      </div>
</div>
</c:if>
</div>
