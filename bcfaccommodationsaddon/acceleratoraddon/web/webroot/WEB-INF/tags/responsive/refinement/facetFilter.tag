<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty facetData.values}">
	<c:choose>
		<c:when test="${fn:length(facetData.values) gt 10}">
			<li role="presentation" class="dropdown filter-li package-listing-filters full-width-dropdown">
		</c:when>
		<c:otherwise>
			<li role="presentation" class="dropdown filter-li package-listing-filters">
		</c:otherwise>
	</c:choose>
		<a class="filter-btn" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			<spring:theme code="accommodation.search.facet.${facetData.name}" />
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu filter-dropdown package-list-checkbox">
			<c:forEach items="${facetData.values}" var="facetValue" varStatus="fcIdx">
				<c:choose>
					<c:when test="${fn:length(facetData.values) gt 10}">
						<li>
					</c:when>
					<c:otherwise>
						<li>
					</c:otherwise>
				</c:choose>
					<c:if test="${facetData.multiSelect}">
						<div class="checkbox">
                          <label class="custom-checkbox-input show deal-details-icon" for="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}">
                             <input type="checkbox" class="y_packageListingFacet" ${facetValue.selected ? 'checked="checked"' : ''} id="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}" value=":${facetData.code}:${facetValue.code}" />
                             <input type="hidden" name="_passengerInfoForm.accessibilityNeedsCheckOutbound" value="on">
                             <span class="${fn:toUpperCase(facetValue.code)}"></span>
                             <spring:theme code="${facetValue.name}" text="${fn:escapeXml(facetValue.name)}" />
                             <span class="checkmark-checkbox"></span>
                          </label>
						</div>
					</c:if>
					<c:if test="${not facetData.multiSelect}">
						<label for="facet_${fn:escapeXml(facetData.code)}_${fn:escapeXml(fcIdx.count)}">
							<input name="facet_${fn:escapeXml(facetData.code)}_${fn:escapeXml(fcIdx.count)}" type="radio" ${facetValue.selected ? 'checked="checked"' : ''} class="y_accommodationFacetCheckbox" id="facet_${fn:escapeXml(facetData.code)}_${fn:escapeXml(fcIdx.count)}" value="${fn:escapeXml(facetValue.query.query.value)}">
							<spring:theme code="${facetValue.name}" text="${fn:escapeXml(facetValue.name)}" />
						</label>
					</c:if>
				</li>
			</c:forEach>
		</ul>
	</li>
</c:if>
