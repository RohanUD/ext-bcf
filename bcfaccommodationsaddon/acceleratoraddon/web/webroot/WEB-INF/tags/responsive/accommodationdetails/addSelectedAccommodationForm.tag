<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="accommodationAvailabilityResponse" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData"%>
<%@ attribute name="addAccommodationUrl" required="true" type="java.lang.String"%>
<c:url var="addAccommodationToCartUrl" value="${addAccommodationUrl}" />
<form:form id="y_accommodationAddToCartForm" commandName="accommodationAddToCartForm" name="accommodationAddToCartForm" class="y_accommodationAddToCartForm" action="${addAccommodationToCartUrl}" method="POST">
	<input type="hidden" name="accommodationOfferingCode" value="${fn:escapeXml(accommodationAvailabilityResponse.accommodationReference.accommodationOfferingCode)}" />
	<input type="hidden" id="y_numberOfRooms" name="numberOfRooms" value="${accommodationAvailabilityForm.numberOfRooms}"/>
	<input type="hidden" id="y_checkInDate" name="checkInDateTime" />
	<input type="hidden" id="y_checkOutDate" name="checkOutDateTime" />
	<input type="hidden" id="y_accommodationCode" name="accommodationCode" />
	<input type="hidden" id="y_roomRateCodes" name="roomRateCodes" />
	<input type="hidden" id="y_roomRateDates" name="roomRateDates" />
	<input type="hidden" id="y_ratePlanCode" name="ratePlanCode" />
	<input type="hidden" id="y_roomStayRefNumber" name="roomStayRefNumber" />
    <input type="hidden" id="y_changeVar" name="change" />
    <input type="hidden" id="y_modifyRoomRef" name="modifyRoomRef" />

	 <c:forEach var="roomCandidatesEntry" items="${accommodationAvailabilityForm.roomStayCandidates}" varStatus='idx'>
                <c:forEach var="entry" items="${roomCandidatesEntry.passengerTypeQuantityList}" varStatus="i">

        	<form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" value="${entry.passengerType.code}" id="y_passengerCode" type="hidden"  />
        	<form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" id="y_passengerQuantity" value="${entry.quantity}" type="hidden"  />
            <form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].childAges" id="y_childAges" value="${entry.childAgesArray}" type="hidden" readonly="true" />


                </c:forEach>
            </c:forEach>

</form:form>
