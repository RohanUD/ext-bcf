<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="property" required="true" type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-40 margin-top-20">
		<div class="accommodation-image">
			<div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(property.images)}">
				<c:forEach var="image" items="${property.images}" varStatus='idx'>
					<c:set var="mainImageUrl" value="${image.url}" />
					<c:if test="${image.format eq 'hotelGallery'}">
						<div class="item">
							<img class='js-responsive-image img-fluid' data-media='${mainImageUrl}' alt='${image.altText}' title='${image.altText}'>
							<div class="slider-count">
                                <div class="slider-img-thumb"></div>
                                <div class="slider-num">1/5</div>
                            </div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
