<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
	
	<div class="col-sm-9">
					<div id="shareThis" class=" col-sm-2 btn btn-secondary share-btn">share</div>
					<div class="col-xs-12  col-sm-9 text-left" style="display: none;">
						<div data-network="twitter" class="st-custom-button">Twitter</div>
						<div data-network="facebook" class="st-custom-button">Facebook</div>
						<div data-network="linkedin" class="st-custom-button">LinkedIn</div>
						<div data-network="email" class="st-custom-button">Email</div>
					</div>
				</div>
