<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="roomType" required="true" type="de.hybris.platform.commercefacades.accommodation.RoomTypeData"%>
<%@ attribute name="roomStay" required="false" type="de.hybris.platform.commercefacades.accommodation.RoomStayData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="accommodation-image">
	<c:if test="${not empty roomType.potentialPromotions}">
		<img class="promo-box-rooms" src="${commonResourcePath}/images/promotion_sticker.png" width="80" height="40" />
	</c:if>
	<div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(roomType.images)}">
		<c:forEach items="${roomType.images}" var="accommodationImage">
			<div class="image-img">
				<img class="js-responsive-image" data-media='${accommodationImage.url}' alt='${accommodationImage.altText}' title='${accommodationImage.altText}'>
				<div class="slider-count">
					<div class="slider-img-thumb"></div>
					<div class="slider-num">1/4</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<c:if test="${not empty roomStay}">
        <c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planIdx">
            <c:if test="${ratePlan.availabilityStatus eq 'SOLD_OUT'}">
				<div class="overlay-sold-out">
                <div class="overlay-text">
                    <p><spring:theme code="message.package.room.not.available" /></p>
                    <a href="/#tabs-3" class="btn-white mt-4 roomNotAvailable">Modify search</a>
                </div>
		    </div>
            </c:if>
        </c:forEach>
    </c:if>

</div>

