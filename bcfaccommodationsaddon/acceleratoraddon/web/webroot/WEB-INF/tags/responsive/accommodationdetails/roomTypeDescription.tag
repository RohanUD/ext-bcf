<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="roomType" required="true" type="de.hybris.platform.commercefacades.accommodation.RoomTypeData"%>
<%@ attribute name="roomStay" required="false" type="de.hybris.platform.commercefacades.accommodation.RoomStayData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<li>${roomType.description}</li>
<li>${roomStay.maxOccupancy} &nbsp;<spring:theme code="text.accommodation.details.roomstay.features.maxOccupancy"/></li>
<c:if test="${not empty roomType.sizeMeasurement}">
    <li>${fn:escapeXml(roomType.sizeMeasurement)} &nbsp;<spring:theme code="text.accommodation.details.roomstay.features.sizeMeasurement"/></li>
</c:if>
<c:forEach var="facility" items="${roomType.facilities}">
    <li>${fn:escapeXml(facility.shortDescription)}</li>
</c:forEach>
