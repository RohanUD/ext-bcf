<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationDetails"
	tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ attribute name="property" required="true"
	type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="mb-5 mt-0 vacation-h2 ">
            <spring:theme code="text.accommodation.details.hotel.description"
                text="Hotel Description" />
        </h2>
        <p class="fnt-14">${property.description}</p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 y_amenities-list">
        <h2 class="mb-4 mt-4 vacation-h2">
            <spring:theme code="text.accommodation.details.top.amenities"
                text="Top amenities" />
        </h2>
        <ul class="amenities-items package-details-ul fnt-14">
            <c:forEach var="amenity" items="${property.amenities}"
                varStatus="loop">
                <c:choose>
                    <c:when test="${loop.first}">
                        <c:set var="amenityType" value="${amenity.facilityType}" />
                        <li>${amenity.facilityType}
                        </li>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${amenityType!=amenity.facilityType}">
                                <c:set var="amenityType" value="${amenity.facilityType}" />
                                <li>
                                   ${amenity.facilityType}
                                </li>
                            </c:when>
                            <c:otherwise>
                                <ul>
                                    <li>${amenity.description}</li>
                                </ul>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
    </div>
</div>
<hr>
<div class="row mb-4 mt-5">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="vacation-h2 mt-3">
            <spring:theme code="asm.emulate.at" />
        </h2>
        <p class="mb-1 fnt-14">
            ${property.accommodationOfferingName}
        </p>
        <c:set var="addressDetail" value="" />
        <%-- TO display StreetNumber First in Address FFDE-3160  --%>
        <c:if test="${not empty property.address.line2}">
            <c:set var="addressDetail" value="${property.address.line2}" />
        </c:if>
        <c:if test="${not empty property.address.line1}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail} ${property.address.line1}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.line1}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if test="${not empty property.address.town}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail}, ${property.address.town}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.town}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if test="${not empty property.address.province}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail}, ${property.address.province}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.province}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if
            test="${not empty property.address.region && not empty property.address.region.name}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail}, ${property.address.region.name}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.region.name}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if test="${not empty property.address.postalCode}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail}, ${property.address.postalCode}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.postalCode}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if
            test="${not empty property.address.country && not empty property.address.country.name}">
            <c:choose>
                <c:when test="${not empty addressDetail}">
                    <c:set var="addressDetail"
                        value="${addressDetail}, ${property.address.country.name}" />
                </c:when>
                <c:otherwise>
                    <c:set var="addressDetail" value="${property.address.country.name}" />
                </c:otherwise>
            </c:choose>
        </c:if>
        <p class="fnt-14">
                ${addressDetail}
        </p>
        <p class="fnt-14">
            <a href="#"><spring:theme code="text.cc.directionsLink.label" /></a>
        </p>
    </div>
</div>

<div class="row mb-5">
    <c:if test="${not empty googleAPIKey}">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 mb-4">
            <div class="map-wrap y_propertyPositionMap"
                data-latitude="${fn:escapeXml(property.position.latitude)}"
                data-longitude="${fn:escapeXml(property.position.longitude)}"
                data-accommodationofferingname="${fn:escapeXml(property.accommodationOfferingName)}"
                data-googleapi="${fn:escapeXml(googleAPIKey)}"></div>
        </div>
    </c:if>
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 package-map-text">
        <div class="heading-accommodation mb-3">
            <spring:theme code="text.accommodation.details.popular.spots"
                text="Popular Spots Nearby" />
        </div>
        <p>
            <accommodationDetails:propertyInformation property="${property}" />
        </p>
    </div>
</div>
<hr>
<div class="row mb-4 mt-5">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2 class="mb-4 vacation-h2">
			<spring:theme code="text.accommodation.details.hotel.policies"
				text="Hotel Policies" />
		</h2>
		<div class="fnt-14">${property.termsAndConditions}</div>
	</div>
</div>
<div class="row">
	<c:if test="${not empty property.bcfComments}">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="mb-4 vacation-h2">
				<spring:theme code="text.accommodation.details.special.comments"
					text="Special comments" />
			</h2>
			<ul class="fnt-14">
				<c:forEach var="bcfComment" items="${property.bcfComments}"
					varStatus="loop">
					<li>${bcfComment.text}</li>
				</c:forEach>
			</ul>
		</div>
	</c:if>
</div>
