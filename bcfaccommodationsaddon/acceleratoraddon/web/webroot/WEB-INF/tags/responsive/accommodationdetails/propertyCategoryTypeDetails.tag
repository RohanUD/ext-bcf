<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="property" required="true" type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="deal-details-icon">
	<c:forEach items="${property.propertyCategoryTypes}" var="categoryType">
		<span class="${categoryType}"></span>
	</c:forEach> 
</div>
