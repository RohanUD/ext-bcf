<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ attribute name="viewDetailsButtonTextCode" required="true" type="java.lang.String" %>

<a class="btn btn-primary btn-block y_viewReservationDetails package-hotel-btn-hidden">
	<spring:theme code="${viewDetailsButtonTextCode}" text="View Details" />
</a>
<div class="modal fade y_bcfGlobalReservationModalComponent" role="dialog" aria-labelledby="y_bcfGlobalReservationModalComponent">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h4 class="modal-title">Modal title</h4>
		    </div>
				<div class="modal-body y_bcfGlobalReservationModalComponentBody">
					<cms:pageSlot position="BottomContent" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</div>
