<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationsearch" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationsearch"%>
<%@ attribute name="roomStays" required="true" type="java.util.List"%>
<%@ attribute name="roomStayRefNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="accommodationAvailabilityResponse" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="accommodation-items clearfix pl-0">
	<c:forEach var="roomStay" items="${roomStays}" varStatus="roomStayIdx">

		<c:if test="${roomStay.roomStayRefNumber eq roomStayRefNumber}">
			<div class="y_roomStayContainer">
				<fmt:formatDate value="${roomStay.checkInDate}" var="checkInDateFormatted" pattern="MM/dd/yyyy" />
				<fmt:formatDate value="${roomStay.checkOutDate}" var="checkOutDateFormatted" pattern="MM/dd/yyyy" />
				<input type="hidden" name="checkInDate" class="y_checkInDate" value="${fn:escapeXml(checkInDateFormatted)}">
				<input type="hidden" name="checkOutDate" class="y_checkOutDate" value="${fn:escapeXml(checkOutDateFormatted)}">
				<input type="hidden" name="roomStayRefNumber" class="y_roomStayRefNumber" value="${fn:escapeXml(roomStayRefNumber)}">
				<input type="hidden" name="numberOfRooms" class="y_numberOfRooms" value="${accommodationAvailabilityResponse.lengthOfStay}">
				<c:forEach var="roomType" items="${roomStay.roomTypes}">
					<input type="hidden" name="accommodationCode" class="y_accommodationCode" value="${fn:escapeXml(roomType.code)}" />
					<div class="package-gray-box price-option accommodation-grey-bg">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
								<div class="accommodation-details available-room-img">
									<accommodationDetails:roomTypeDetails roomType="${roomType}" roomStay="${roomStay}"/>
								</div>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
								<div class="accommodation-features">
									<div class="row">
										<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
											<h5 class="mt-0">
												<strong>${fn:escapeXml(roomType.name)}</strong>
											</h5>
											<ul class="ul-disc clearfix y_features mb-3">
									            <accommodationDetails:roomTypeDescription roomType="${roomType}" roomStay="${roomStay}"/>
											</ul>
											<a href="#" class="more hidden">
												<spring:theme code="text.accommodation.details.roomstay.features.more" />
												<i class="fas fa-angle-down pl-1"></i>
											</a>
											<a href="#" class="less hidden">
												<spring:theme code="text.accommodation.details.roomstay.features.less" />
												<i class="fas fa-angle-up pl-1"></i>
											</a>
										</div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 border-left m-text-center available-room-right">
                                            <legend class="sr-only">
                                                <spring:theme code="text.package.details.room.selection.sr" arguments="Room selection" />
                                            </legend>
                                            <c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planID">
                                                    <p>
                                                        <input type="hidden" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
                                                        <c:set var="roomRate" value="${ratePlan.roomRates[0]}" />
                                                        <fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
                                                        <input type="hidden" name="roomRate${roomRateIdx.index}" code="${fn:escapeXml(roomRate.code)}" value="${roomRateDateFormatted}" class="y_roomRate" />
                                                        <c:choose>
                                                            <c:when test="${not empty ratePlan.perPersonPlanPrice and empty showPerRoomPrice}">
                                                                <accommodationsearch:perPersonPrice perPersonPrice="${ratePlan.perPersonPlanPrice}" lengthOfStay="${accommodationAvailabilityResponse.lengthOfStay}" />
                                                            </c:when>
                                                        </c:choose>
                                                    </p>
                                                    <p class="mb-0 clearfix m-text-center">
                                                        <label for="room-${fn:escapeXml(roomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}">
                                                            <span class="totalPrice font-weight-bold">
                                                                <c:choose>
                                                                    <c:when test="${isAmendment}">
                                                                        <c:choose>
                                                                            <c:when test="${ratePlan.actualRate.value>0}">
                                                                                <spring:theme code="text.accommodation.additional.per.person.payment.amount" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <spring:theme code="text.accommodation.per.person.refund.amount" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <spring:theme code="text.accommodation.details.accommodation.total.price" />
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                &nbsp;
                                                                <p class="mt-0 m-text-center">
                                                                    <b>${ratePlan.actualRate.formattedValue}</b>
                                                                </p>
                                                            </span>
                                                            <span>${fn:escapeXml(ratePlan.priceDifference.formattedValue)}</span>
                                                        </label>
                                                    </p>
                                            </c:forEach>
                                            <c:set var="changeValue" value="false"/>
                                            <c:if test="${not empty modify}">
                                               <c:set var="changeValue" value="${modify}"/>
                                            </c:if>
                                            <input type="hidden" name="changeVar" id="changeVar" value="${changeValue}"/>


                                            <input type="hidden" name="modifyRoomRef" id="modifyRoomRef" value="${modifyRoomRef}"/>

                                            <div class="text-center">
                                                <fieldset>
                                                    <legend class="sr-only">
                                                        <spring:theme code="text.package.details.room.selection.sr" arguments="Room selection" />
                                                    </legend>
                                                    <c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planID">
                                                        <p class="y_ratePlanAttributes">

                                                                <input type="hidden" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
                                                                <c:forEach var="roomRate" items="${ratePlan.roomRates}" varStatus="roomRateIdx">
                                                                    <fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
                                                                    <input type="hidden" name="roomRate${roomRateIdx.index}" code="${fn:escapeXml(roomRate.code)}" value="${roomRateDateFormatted}" class="y_roomRate" />
                                                                </c:forEach>

                                                                <span class="price-select custom-btn margin-top-30">
                                                                    <c:choose>
                                                                        <c:when test="${ratePlan.availabilityStatus eq 'SOLD_OUT'}">
                                                                            <div class="sold-out-section">
                                                                                <div class="sold-out-text box-align-center">
                                                                                    <spring:theme code="text.room.sold.out" />
                                                                                </div>
                                                                            </div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <c:choose>
                                                                                <c:when test="${ratePlan.availabilityStatus eq 'ON_REQUEST'}">
                                                                                    <c:set var="bookingTitle" value="Request booking" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:set var="bookingTitle" value="Book This Room" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                            <input id="room-${fn:escapeXml(roomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}" type="radio" name="room_${fn:escapeXml(roomStayRefNumber)}" class="y_changeHotelAccommodationButton hidden" />
                                                                            <input id="bookNow_addAccommodationToCart" type="button" value="${bookingTitle}" class="btn btn-primary y_addHotelAccommodationToCart" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                        </p>
                                                        <div class="y_accommodationSelect controls" id="accommodationSelect">
                                                            <input type="hidden" id="ratePlanCode" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
                                                            <c:forEach var="roomRate" items="${ratePlan.roomRates}" varStatus="roomRateIdx">
                                                                <fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
                                                                <input type="hidden" id="roomRate" name="roomRate${fn:escapeXml(roomRateIdx.index)}" code="${fn:escapeXml(roomRate.code)}" value="${fn:escapeXml(roomRateDateFormatted)}" class="y_roomRate" />
                                                            </c:forEach>
                                                            <c:set var="reservedRatePlanQty" value="0" />
                                                            <c:forEach var="reservedRoomStay" items="${accommodationAvailabilityResponse.reservedRoomStays}">
                                                                <c:forEach var="reservedRoomType" items="${reservedRoomStay.roomTypes}">
                                                                    <c:if test="${roomTypeCode == reservedRoomType.code}">
                                                                        <c:set var="reservedRoomFlag" value="true" />
                                                                    </c:if>
                                                                </c:forEach>
                                                                <c:if test="${reservedRoomFlag}">
                                                                    <c:forEach var="reservedRatePlan" items="${reservedRoomStay.ratePlans}">
                                                                        <c:if test="${reservedRatePlan.code == ratePlan.code}">
                                                                            <c:set var="reservedRatePlanQty" value="${reservedRatePlanQty + 1}" />
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                                <c:set var="reservedRoomFlag" value="false" />
                                                            </c:forEach>
                                                        </div>
                                                    </c:forEach>
                                                </fieldset>
                                            </div>
                                        </div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-end">
						<div class="col-sm-12 col-md-6">
							<p>
								<input id="room-${fn:escapeXml(roomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}" type="radio" name="room_${fn:escapeXml(roomStayRefNumber)}" class="y_changeHotelAccommodationButton hidden" />
							</p>
						</div>
					</div>
				</c:forEach>
			</div>
		</c:if>
	</c:forEach>
</div>
