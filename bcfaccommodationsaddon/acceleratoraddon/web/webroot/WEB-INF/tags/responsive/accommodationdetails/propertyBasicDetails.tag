<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ attribute name="property" required="true" type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<accommodationDetails:propertyCategoryTypeDetails property="${property}" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<p class="mb-1">
				<span class="hotel--title text-blue"> ${fn:escapeXml(property.accommodationOfferingName)} </span>
			</p>
		</div>
		<c:set var="addressDetail" value="" />
		<%-- TO display StreetNUmber First In Address  --%>
		<c:if test="${not empty property.address.line2}">
			<c:set var="addressDetail" value="${property.address.line2}" />
		</c:if>
		<c:if test="${not empty property.address.line1}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail} ${property.address.line1}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.line1}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${not empty property.address.town}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail}, ${property.address.town}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.town}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${not empty property.address.province}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail}, ${property.address.province}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.province}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${not empty property.address.region && not empty property.address.region.name}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail}, ${property.address.region.name}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.region.name}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${not empty property.address.postalCode}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail}, ${property.address.postalCode}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.postalCode}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${not empty property.address.country && not empty property.address.country.name}">
			<c:choose>
				<c:when test="${not empty addressDetail}">
					<c:set var="addressDetail" value="${addressDetail}, ${property.address.country.name}" />
				</c:when>
				<c:otherwise>
					<c:set var="addressDetail" value="${property.address.country.name}" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<p class="mb-0">
				<span class="hotel--description fnt-14">
					${fn:escapeXml(addressDetail)}
				</span>
			</p>
			<p class="mb-2 fnt-14">
			    <a href="#">
                    <spring:theme code="text.package.details.view.map" />
                </a>
			</p>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <accommodationDetails:propertyCustomerReviewDetails property="${property}" />
	    </div>
