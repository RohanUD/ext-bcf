<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="property" required="true" type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 accommodation-star-rating">
		    <div class="fnt-14">
				<strong><spring:theme code="text.package.details.tripadvisor.rating.heading" /></strong>
			</div>
            <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                <spring:param name="locationId"  value="${property.reviewLocationId}"/>
            </spring:url>
			<p>
			    <img src="${fn:escapeXml(property.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating" width="100" height="30">
                <span class="rating-number">
                    <a class="tripAdvisorLink" href="${tripAdvisorURL}">${property.customerReviewData.numOfReviews} Reviews</a>
                </span>
			</p>
		</div>
	</div>
