<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="cancel" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/cancel"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
   <json:property name="shipInfoModalHtml">
      <div class="detailsModal modal fade" id="y_shipInfosModal" tabindex="-1" role="dialog" aria-labelledby="userReviews">
      <div class="modal-dialog" role="document">
      <div class="modal-content pb-20">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
            </button>
            <h3 class="modal-title" id="userReviews">
               <spring:theme code="text.ferry.detail.modal.header" />
            </h3>
         </div>
         <div class="modal-body padding-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
               <h3 class="mt-2 mb-3 text-dark-blue">
                  <strong>${shipInfo.name}</strong>
               </h3>
              <p>${shipInfo.description}</p>
            </div>
            <div class="text-center ferry-ship-icon col-md-12">
               <c:if test="${not empty shipInfo.shipIcon}">
                  <img class='js-responsive-image'
                     alt='${altText}' title='${altText}' data-media='${shipInfo.shipIcon}'  />
               </c:if>
            </div>
            <header>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
              <h4 class="text-blue"> <spring:theme code="label.ferry.details.onboard.amenities"
                  text="Onboard Amenities" /></h4>
               </div>
            </header>
            <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="tabel-ferry-build">
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                  <ul class="list-inline">
                     <c:forEach items="${shipInfo.shipFacilities}"
                        var="shipFacility" varStatus="status">
                        <c:set var="alignment" value="left" />
                        <c:if test="${not empty shipFacility.logo}">
                           <li><img class='img-rounded ferry-logo js-responsive-image' alt='${altText}'
                              title='${altText}' data-media='${shipFacility.logo}'
                              /></li>
                        </c:if>
                     </c:forEach>
                  </ul>
                  </div>
                  </div>
                  <div class="padding-override info-text ferrydetails-heading">
                     <spring:theme
                        code="label.ferry.details.available.onboard"
                        text="Also available onboard this ferry:" />
                  </div>
                  <c:forEach items="${shipInfo.shipFacilities}"
                     var="shipFacility" varStatus="status">
                     <c:if test="${empty shipFacility.logo}">
                        <div class="ferrydetails-onboard-sec padding-override">
                           <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">${shipFacility.shortDescription}</div>
                           <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center ferrydetails-onboard-right"><span class="bcf bcf-icon-checkmark"></span></div>
                        </div>
                     </c:if>
                  </c:forEach>
               </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-5">
              <p class="text-grey margin-top-20"> <i><spring:theme code="label.ferry.details.modal.info.message"/></i></p>
               <span class="scroll-fade"></span>
                </div>
            </div>
         </div>
          </div>
      </div>
   </json:property>
</json:object>
