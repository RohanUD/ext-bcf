<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="cancel" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/cancel"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
	<json:property name="hotelDetailModalHtml">
		<div class="detailsModal modal fade" id="y_hotelDetailsModal" tabindex="-1" role="dialog" aria-labelledby="userReviews">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
						</button>
						<h3 class="modal-title" id="userReviews">
							<spring:theme code="text.hotel.detail.modal.header" />
						</h3>
					</div>

					<div class="modal-body">
					    <c:if test="${not empty hotelDetail.images}">
                        <div class="js-owl-carousel js-owl-rotating-gallery owl-theme ferry-detail-owl">
                            <c:forEach var="image" items="${hotelDetail.images}">
                                <img data-media='${image.url}' alt='${image.altText}' style="width: 100%; height: 318px;" class='img-fluid  img-w-h js-responsive-image'/>
                            </c:forEach>
                        </div>
                        </c:if>
                        <c:forEach var="categoryType" items="${hotelDetail.propertyCategoryTypes}">
                            <span class="${categoryType}"></span>
                        </c:forEach><br/>
						<b>${hotelDetail.accommodationOfferingName}</b><br/><div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 accommodation-star-rating">
                            <div class="fnt-14">
                                <strong><spring:theme code="text.package.details.tripadvisor.rating.heading" /></strong>
                            </div>
                            <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                                <spring:param name="locationId"  value="${hotelDetail.reviewLocationId}"/>
                            </spring:url>
                            <p>
                                <img src="${fn:escapeXml(hotelDetail.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating" width="100" height="30">
                                <span class="rating-number">
                                    <a class="tripAdvisorLink" href="${tripAdvisorURL}">${hotelDetail.customerReviewData.numOfReviews} Reviews</a>
                                </span>
                            </p>
                        </div>
                    </div>
<br/>
						${hotelDetail.description}<br/>
						<b><spring:theme code="text.hotel.detail.top.amenities.header" /></b><br/>
						<c:forEach var="amenity" items="${hotelDetail.amenities}">
						    ${amenity.facilityType}
						</c:forEach>
						<span class="scroll-fade"></span>
					</div>
				</div>
			</div>
		</div>
		
		
		
	</json:property>
</json:object>
