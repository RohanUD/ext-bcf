<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="accommodationBooking" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationbooking"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${displayAccommodationBookingDetailsComponent}">
<c:choose>
	<c:when test="${empty pageNotAvailable}">
	<c:if test="${not empty accommodationReservationData}">
		<div class="row">
			<div class="panel button-wrap">
				<div class="col-xs-10 col-sm-6 col-lg-8">
					<dl class="booking status">
						<dt>
							<spring:theme code="text.page.managemybooking.bookingstatus" text="Booking Status:" />
						</dt>
						<dd>
							<bookingDetails:status code="${accommodationReservationData.bookingStatusCode}" name="${accommodationReservationData.bookingStatusName}" />
						</dd>
					</dl>
				</div>
				<div class="col-xs-10 col-sm-6 col-lg-4">
					<input type="hidden" value="${fn:escapeXml(accommodationReservationData.code)}" name="bookingReference" id="bookingReference">
					<accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="CANCEL_BOOKING" />
				</div>
				<div class="col-xs-10 col-sm-6 col-lg-4">
                    <input type="hidden" value="${fn:escapeXml(accommodationReservationData.code)}" name="bookingReference" id="bookingReference">
                    <accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_ACCOMMODATION" />

                </div>
                <div class="col-xs-10 col-sm-6 col-lg-4">
                        <input type="hidden" value="${fn:escapeXml(accommodationReservationData.code)}" name="bookingReference" id="bookingReference">
                        <accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_SAILING" />
                            <div class="y_amendFerrySection"></div>
                </div>
                <div class="col-xs-10 col-sm-6 col-lg-4">
                    <input type="hidden" value="${fn:escapeXml(accommodationReservationData.code)}" name="bookingReference" id="bookingReference">
                    <accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_ACTIVITY" />
                        <div class="y_amendActivitySection"></div>
                </div>
                <div class="col-xs-10 col-sm-6 col-lg-4">
                    <input type="hidden" value="${fn:escapeXml(accommodationReservationData.code)}" name="bookingReference" id="bookingReference">
                    <accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="ADJUST_COST" />
                        <div class="y_amendActivitySection"></div>
                </div>

			</div>
		</div>
		<bookingDetails:accommodationBookingDetails accommodationReservationData="${accommodationReservationData}" customerReviews="${customerReviews}" />

		<div class="y_cancelBookingConfirm"></div>
		</c:if>
	</c:when>
	<c:otherwise>
		<div class="alert alert-danger " role="alert">
			<p>
				<spring:theme code="${pageNotAvailable}" />
			</p>
		</div>
	</c:otherwise>
</c:choose>

</c:if>

