<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="nextUrl" value="${nextURL}" />
<input type="hidden" id="y_amendPackageDetailsPage" value="${fn:escapeXml(amend)}" />
<c:set var="accommodationAvailabilityResponse" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse}" />
<c:set var="property" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse.accommodationReference}" />
<div class="container-fluid back-bg-gray padding-top-20 padding-bottom-20 margin-bottom-30">
    <div class="container">
        <div class="row py-4 ">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-2 mt-2">
                <p class="mb-0 mt-0 ">
                    <a href="${previousPageUrl}" class="fnt-14">
                        <i class="fas fa-angle-left fnt-20 align-middle"></i>
                        <spring:theme code="text.page.managemybooking.backtoresults" text="Back To Results"/>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-xl-12">
			<c:if test="${not empty packageAvailabilityResponse.transportPackageResponse.errorMessage}">
				<spring:theme code="${packageAvailabilityResponse.transportPackageResponse.errorMessage}" />
			</c:if>
            <div class="row hidden">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="mb-4">
                        <spring:theme code="text.package.details.label.text" text="Package Details" />
                    </h1>
                </div>
            </div>

		</div>
	</div>
	<div class="margin-reset clearfix package-details-wrap">
		<div class="col-xs-12 col-sm-12 y_nonItineraryContentArea">
			<div class="row">
				<accommodationDetails:propertyDetails property="${property}" />
				<packageDetails:propertyInfoTabs property="${property}" accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" numroomtype="${fn:length(accommodationAvailabilityResponse.roomStays)}" />
				<c:if test="${!isPackageUnavailable}">
					<c:if test="${not empty isPackageInCart && !isPackageInCart}">
						<packageDetails:addSelectedAccommodationForm accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" addRoomToPackageUrl="${addRoomToPackageUrl}" />
						<%--<packageDetails:selectedTransportation isDynamicPackage="true" transportPackageResponse="${packageAvailabilityResponse.transportPackageResponse}" />--%>
					</c:if>
				</c:if>
			</div>
		</div>
	</div>
	<div aria-hidden="false" role="dialog" tabindex="-1" id="package-build-modal" class="modal fade in">
		<div class="modal-dialog" role="document">
			<div class="alert alert-info" role="alert">
				<p class="y_packageProcessingContent">
					<spring:theme code="text.package.details.package.processing" text="Please wait while we're building your package..." />
				</p>
			</div>
		</div>
	</div>
	<packageDetails:addPackageToCartErrorModal />
	<c:if test="${isPackageUnavailable}">
		<packageDetails:noPackageAvailabilityModal />
	</c:if>
	<accommodationDetails:bcfGlobalReservationModal viewDetailsButtonTextCode="text.deal.listing.selection" />
</div>
