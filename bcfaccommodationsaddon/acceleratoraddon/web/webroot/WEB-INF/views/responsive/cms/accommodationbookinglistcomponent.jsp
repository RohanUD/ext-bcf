<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="accommodationBooking" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationbooking"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
<spring:url value="/manage-booking/unlink-booking" var="unlinkBookingUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />

<style>
#my-bookings ul li:nth-child(n+${fn:escapeXml(pageSize+1)}) {
    display:none;
}
</style>
<div class="container">
<div class="panel-heading px-0">
    <p class="title title-collapse"><b><spring:theme code="text.page.accommodation.mybookings" /></b></p>
</div>
<div class="panel-body collapse in" id="my-bookings">
	<c:choose>
		<c:when test="${not empty myBookings}">
		<div class="row">
			<div class="accommodation-items y_myBookings my-bookings">
				<c:forEach items="${myBookings}" var="myBooking" varStatus="bIdx">
					<div class="ooking-list">
						<div class="col-md-5 col-xs-12 accommodation-image">

								<a href="#">
									<img src="${myBookingImages[myBooking.code].url}">
								</a>

						</div>
						<div class="accommodation-details  col-md-7  col-xs-12">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<div class="clearfix">
										<div class="col-xs-12">
											<div class="row">
												<accommodationBooking:accommodationBookingDetails myBooking="${myBooking}" accommodationRoomMapping="${accommodationRoomMapping}" />
												<p>
													<c:if test="${removeLinks[myBooking.code] eq true}">
														<a href="#" class="y_removeBooking" data-href="${unlinkBookingUrl}/${myBooking.code}" data-toggle="modal" data-target="#confirm-delete">
															<spring:theme code="button.page.mybookings.removeBooking" />
														</a>
													</c:if>
												</p>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-6 pull-right">
													<a href="${viewBookingUrl}/${myBooking.code}" class="btn btn-primary btn-block">
														<spring:theme code="button.page.mybookings.viewBookingDetails" />
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			</div>
			<c:if test="${fn:length(myBookings) > pageSize}">
				<div class="row">
					<div class="test col-xs-12">
						<div class="col-sm-5 col-sm-offset-7">
							<a href="#" class="col-xs-12 btn btn-primary y_myAccountMyBookingsShowMore" data-pagesize="${fn:escapeXml(pageSize)}">
								<spring:theme code="button.page.mybookings.showmore" text="Show More" />
							</a>
						</div>
					</div>
				</div>
			</c:if>
		</c:when>
		<c:otherwise>
			<spring:theme code="text.page.mybookings.not.found" />
		</c:otherwise>
	</c:choose>
</div>

<account:unlinkbooking />
</div>
