<%@page language="java" session="true" %>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="packagehotellisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagehotellisting"%>
<spring:htmlEscape defaultHtmlEscape="false" />
   <div class="y_packageListingPageParams" data-googleapi="${fn:escapeXml(googleAPIKey)}" data-resultViewType="${fn:escapeXml(resultsViewType)}"></div>
   <c:set var="resultsView" value="${fn:toUpperCase(resultsViewType)}" />
   <div class="container">
   <div class="row">
      <div class="accommodation-selection-wrap clearfix y_accommodationSelectionSection">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 y_nonItineraryContentArea">
             <h2 class="my-5 text-dark-blue">
                 <b><spring:theme code="text.package.hotel.listing.title" text="Participating hotels" /></b>
             </h2>
             <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 package-hotel-listing-results-wrapper">
               <c:set var="urlParameters" value="${urlParameters}" />
               <%-- Begin Package Results section --%>
               <div class="results-list">
                  <div id="y_packageResults" class=" deal-items deal-row" aria-label="package search results">
                     <packagehotellisting:packageHotelListView propertiesListParams="${packageSearchResponseProperties}" />
                  </div>
                  <%--<packagehotellisting:packageShowMore />--%>
               </div>
               
               <%-- End Package Results section --%>
            </div>
            </div>
         </div>
      </div>
      </div>
   </div>
   <div class="y_customerReviewsModal"></div>
