<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="nextUrl" value="/accommodation-details/next" />
<div class="back-bg-gray mb-5 padding-top-30 padding-bottom-30">
	<div class="container">
		<div class="row py-4 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-2 mt-2">
				<p class="mb-0 mt-0 ">
					<a href="#" class="fnt-14 backToResults">
						<i class="fas fa-angle-left fnt-20 align-middle"></i>
						<spring:theme code="text.page.managemybooking.backtoresults" text="Back To Results" />
					</a>
				</p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-2 mt-2">
				<button type="button" class="btn btn-primary pull-right select-room">
					<spring:theme code="text.package.details.button.select.room" />
				</button>
			</div>
		</div>
	</div>
	<c:if test="${availabilityStatus eq 'ON_REQUEST'}">
		<div class="booking-request-strip">
			<span class="icon-info"></span>
			<spring:theme code="text.package.booking.on.request.message" text="This booking is on request" />
			<input type="hidden" name="availabilityStatus" id="availabilityStatus" value="${availabilityStatus}" />
		</div>
	</c:if>
</div>
<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">
		<h1 class="mb-4">
			<spring:theme code="text.accommodation.details.label.text" text="Accommodation Details" />
		</h1>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="margin-reset package-details-wrap">
				<c:if test="${!amend}">
				</c:if>
				<accommodationDetails:propertyDetails property="${accommodationAvailabilityResponse.accommodationReference}" />
				<c:set var="numroomtype" value="0" scope="page" />
				<c:forEach var="roomStay" items="${accommodationAvailabilityResponse.roomStays}" varStatus="roomStayIdx">
					<c:if test="${roomStay.roomStayRefNumber eq 0}">
						<c:set var="numroomtype" value="${numroomtype + 1}" scope="page"/>
					</c:if>
				</c:forEach>
				<div class="y_roomStayContainer">
					<accommodationDetails:propertyInfoTabs property="${accommodationAvailabilityResponse.accommodationReference}" accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" numroomtype="${accommodationAvailabilityResponse.noOfAvailableRoomTypes}" />
					<c:if test="${not empty isAccommodationAvailable && isAccommodationAvailable}">
						<accommodationDetails:addSelectedAccommodationForm accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" addAccommodationUrl="${addAccommodationUrl}" />
					</c:if>
				</div>
				<div aria-hidden="false" role="dialog" tabindex="-1" id="accommodation-build-modal" class="modal fade in">
					<div class="modal-dialog" role="document">
						<div class="alert alert-info" role="alert">
							<p class="y_accommodationProcessingContent">
								<spring:theme code="text.accommodation.details.processing" text="Please wait while we're building your request..." />
							</p>
						</div>
					</div>
				</div>
				<accommodationDetails:addAccommodationToCartErrorModal />
				<accommodationDetails:ratePlanRoomSelectStockLevelErrorModal />
				<c:if test="${not empty isAccommodationAvailable && !isAccommodationAvailable}">
					<accommodationDetails:noAvailabilityModal />
				</c:if>
			</div>
		</div>
	</div>
