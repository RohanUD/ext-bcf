<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="y_packageListingPageParams" data-googleapi="${fn:escapeXml(googleAPIKey)}" data-resultViewType="${fn:escapeXml(resultsViewType)}"></div>
<c:set var="resultsView" value="${fn:toUpperCase(resultsViewType)}" />
<div class="container">
    <div class="accommodation-selection-wrap clearfix y_accommodationSelectionSection ${(resultsView eq null or totalNumberOfResults eq null or totalNumberOfResults eq 0) ? 'hidden' : ''}" id="y_packageListingSortSelection" >
        <div id="results-options" class="row margin-bottom-20">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 show-page-number font-italic y_totalNumberOfResults">
                        <c:choose>
                            <c:when test="${totalNumberOfResults gt 0}">
                                <spring:theme code="text.package.listing.found.package.number" arguments="${totalShownResults}, ${totalNumberOfResults}" />
                            </c:when>
                            <c:otherwise>
                                <spring:theme code="text.package.listing.empty.list" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-lg-5 col-md-5"></div>
                    <div class="col-lg-3 col-md-3 col-sm-8 col-xs-12 text-right m-text-left padding-0 mobile-sorter-drpdwn">
                        <packagelisting:sortSelect accommodationSearchParams="${accommodationSearchParams}" />
                    </div>
        </div>
        <div class="y_nonItineraryContentArea">
                <c:set var="urlParameters" value="${urlParameters}" />
                <%-- Begin Package Results section --%>
                <div class="results-list" id="y_packageResults" aria-label="package search results">
                <div class="row">
                    <packagelisting:packageListView propertiesListParams="${packageSearchResponseProperties}" />
                   </div>
                    <packagelisting:packageShowMore />
                </div>
                <%-- End Package Results section --%>
                			<div class="y_paginationContent">
                            </div>
        </div>
    </div>
</div>
<div class="y_customerReviewsModal"></div>

