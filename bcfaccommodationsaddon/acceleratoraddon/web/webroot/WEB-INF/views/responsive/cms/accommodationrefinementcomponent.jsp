<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="refinement" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/refinement"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
	<div class="row margin-top-40">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h2 class="vacation-h2 margin-top-30">
				<spring:theme code="text.package.listing.title" text="Accommodation" />
			</h2>
		</div>
	</div>
	<c:set var="displayFilters" value="false" />
	<c:if test="${not empty fn:escapeXml(filterPropertyName) or totalNumberOfResults gt 0}">
		<c:set var="displayFilters" value="true" />
	</c:if>
	<c:forEach items="${accommodationSearchResponse.criterion.facets}" var="facet">
		<c:if test="${not empty facet.values}">
			<c:forEach items="${facet.values}" var="facetValue" varStatus="fcIdx">
				<c:if test="${facetValue.selected}">
					<c:set var="displayFilters" value="true" />
				</c:if>
			</c:forEach>
		</c:if>
	</c:forEach>
	<c:if test="${displayFilters}">
		<c:if test="${not empty accommodationSearchParamsError}">
			<div class="col-xs-12">
				<div class="row">
					<div class="alert alert-danger alert-dismissable">
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
						<spring:theme code="${accommodationSearchParamsError}" />
					</div>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="filter-form clearfix col-lg-8 col-sm-12 col-md-8 col-xs-12">
				<div class="filter-facets">
					<h3 class="vacation-h3">
						<spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:" />
					</h3>
					<div class="accommodations_select mb-4">
						<ul class="nav nav-pills">
							<c:forEach items="${accommodationSearchResponse.criterion.facets}" var="facet">
								<refinement:facetFilter facetData="${facet}" />
							</c:forEach>
						</ul>
					</div>
				</div>
				<!-- <div class="facets-search-container clearfix y_packageListingSelectedFiltersContainer">
					<refinement:selectedFacetFilter />
				</div>  -->
			</div>
		</div>
		<div class="row sidebar-offcanvas" id="filter">
			<c:url value="/accommodation-search" var="accommodationSearchUrl" />
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form action="${accommodationSearchUrl}" method="GET" id="y_accommodationSearchFacetForm">
					<c:forEach items="${accommodationSearchParams}" var="paramDetail">
						<input type="hidden" name="${fn:escapeXml(paramDetail.key)}" value="${fn:escapeXml(paramDetail.value)}" />
					</c:forEach>
					<c:forEach items="${accommodationSearchResponse.criterion.sorts}" var="sort">
						<c:if test="${sort.selected }">
							<input type="hidden" name="sort" value="${fn:escapeXml(sort.code)}" />
						</c:if>
					</c:forEach>
					<input type="hidden" name="q" value="${fn:escapeXml(accommodationSearchResponse.criterion.query)}" /> <input type="hidden" name="priceRange" value="${fn:escapeXml(priceRange)}" /> <input id="y_resultsViewTypeForFacetForm" type="hidden" name="resultsViewType" value="${fn:escapeXml(resultsViewType)}" />
				</form>
			</div>
		</div>
		<hr class="hr-payment">
	</c:if>
</div>
