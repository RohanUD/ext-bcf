<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:set var="accommodationAvailabilityResponse" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse}" />
<c:set var="property" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse.accommodationReference}" />

<c:if test="${not empty accommodationAvailabilityResponse.roomStays}">
    <packageDetails:roomOptionsCollapse accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
</c:if>
