<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="nextUrl" value="${nextURL}" />
<input type="hidden" id="y_amendPackageDetailsPage" value="${fn:escapeXml(amend)}" />
<c:set var="accommodationAvailabilityResponse" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse}" />
<c:set var="property" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse.accommodationReference}" />
<div class="container">
    <c:if test="${!isPackageUnavailable}">

    </c:if>
</div>
<div class="container-fluid back-bg-gray padding-top-20 padding-bottom-20 margin-bottom-30">
        <div class="container">
            <div class="row">
                <c:if test="${not empty packageAvailabilityResponse.transportPackageResponse.errorMessage}">
                    <spring:theme code="${packageAvailabilityResponse.transportPackageResponse.errorMessage}" />
                </c:if>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="back-to-result">
                        <a href="#" class="fnt-14 backToResults"><i class="fas fa-angle-left fnt-20 align-middle"></i> <spring:theme code="text.page.managemybooking.backtoresults" text="Back To Results"/></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <button type="button" class="btn btn-primary pull-right select-room">
                        <spring:theme code="text.package.details.button.select.room" />
                    </button>
                </div>
            </div>
    </div>

</div>
<div class="container">
    <c:if test="${availabilityStatus eq 'ON_REQUEST'}">
        <div class="alert alert-info">
            <span class="bcf bcf-icon-notice-outline bcf-2x"></span>
            <spring:theme code="text.package.booking.on.request.message" text="This booking is on request"/>
            <input type="hidden" name="availabilityStatus" id="availabilityStatus" value="${availabilityStatus}" />
        </div>
    </c:if>
</div>

<div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">

            <h1 class="mb-4">
                <spring:theme code="text.package.details.label.text" text="Package Details" />
            </h1>
        </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="margin-reset package-details-wrap">
                <c:if test="${!amend}">
                </c:if>
                    <accommodationDetails:propertyDetails property="${property}" />

                    <packageDetails:propertyInfoTabs property="${property}" accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" numroomtype="${accommodationAvailabilityResponse.noOfAvailableRoomTypes}" />
                    <c:if test="${!isPackageUnavailable}">

                            <packageDetails:addSelectedAccommodationForm accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" addRoomToPackageUrl="${addRoomToPackageUrl}" />
                            <packageDetails:selectedTransportation isDynamicPackage="true" transportPackageResponse="${packageAvailabilityResponse.transportPackageResponse}" />

                    </c:if>
        </div>
        <div aria-hidden="false" role="dialog" tabindex="-1" id="package-build-modal" class="modal fade in">
            <div class="modal-dialog" role="document">
                <div class="alert alert-info" role="alert">
                    <p class="y_packageProcessingContent">
                        <spring:theme code="text.package.details.package.processing" text="Please wait while we're building your package..." />
                    </p>
                </div>
            </div>
        </div>
        <packageDetails:addPackageToCartErrorModal />
        <c:if test="${isPackageUnavailable}">
            <packageDetails:noPackageAvailabilityModal />
        </c:if>

        </div>
    </div>
</div>

