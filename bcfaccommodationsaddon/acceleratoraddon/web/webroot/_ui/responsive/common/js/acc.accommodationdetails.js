$(document).ready(function () {
    $("div.share-btn").click(function () {
        $(this).next().slideToggle("fast");
    });
});

ACC.accommodationdetails = {
    _autoloadTracc: [
        "init",
        "bindAmenitiesList",
        "bindHotelAddAccommodationToCartButton",
        "bindAccommodationDetailsReviewShowMore",
        "bindContinueButton",
        "enableContinueButton",
        "showNoAvailabilityModal",
        "bindFeaturesMoreLess",
        "bindSocialShare",
        "bindAmendAccommodationButton",
        "bindAdjustCostButton",
        "showRemoveRoomModal"
    ],

    map: null,

    marker: null,
    addHotelAccommodationToCartResult: true,

    init: function () {
        if ($(".y_propertyPositionMap").length > 0) {
            ACC.config.googleApiKey = $('.y_propertyPositionMap').data('googleapi');
            ACC.accommodationdetails.addGoogleMapsApi("ACC.accommodationdetails.loadGoogleMap");
        }
        ACC.accommodationdetails.getHotelRooms();
    },

    addGoogleMapsApi: function (callback) {
        if (callback != undefined && $(".js-googleMapsApi").length == 0) {
            $('head').append('<script async defer class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.config.googleApiKey + '&callback=' + callback + '"></script>');
        } else if (callback != undefined) {
            eval(callback + "()");
        }
    },

    getHotelRooms: function () {
        $("#searchRooms").click(function () {

            $('#y_buttonToClick').val("#searchRooms");
            if (ACC.travelfinder.vacationValidation('BOOKING_PACKAGE') ==  false) {
                return false;
            }

            var accommodationOfferingCode = $( "#promotionAccommodationOfferingCode" ).val();
            var roomRateProducts = $( "#promotionRoomRateProducts" ).val();
            var promotionCode = $( "#promotionCode" ).val();

            var options = {
                url: ACC.config.contextPath+'/search-rooms/'+accommodationOfferingCode+'?roomRateProducts='+roomRateProducts+'&promotionCode='+promotionCode,
                target: 'div.ajax-post-response',
                success: function (data)
                {
                    ACC.packagedetails.bindAccommodationChange();
                    ACC.packagedetails.bindAddPackageToCartButton();
                    ACC.accommodationdetails.bindFeaturesMoreLess();
                    ACC.global.initImager();
                },
                error: function (xht, textStatus, ex)
                {
                    alert("Failed to search rooms. Error details [" + xht + ", " + textStatus + ", " + ex + "] with status [" + xht.status + "]");
                }
            };

            $("#y_travelFinderForm").ajaxSubmit(options);

        });
    },
    bindAmendAccommodationButton: function () {
      if ($("#hasAmendAccommodation").val() == 'true' || $("#hasAmendAccommodationError").val() == 'true' ) {
        $('#y_amendAccommodationModal').modal('show');
      }
    },
    bindAdjustCostButton: function () {
      if ($("#hasAdjustCost").val() == 'true' || $("#hasAdjustCost").val() == 'true' ) {
        $('#y_AdjustCostModal').modal('show');
      }
      if($("#adjustCostErrorFlag").val() =='true') {
            $('#y_AdjustCostModal').modal('show');
       }
    },

    loadGoogleMap: function () {
        var mapDiv = $('.y_propertyPositionMap');
        var accommodationOfferingName = mapDiv.data('accommodationofferingname');

        var mapOptions = {
            zoom: 13,
            zoomControl: true,
            panControl: true,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: {lat: mapDiv.data('latitude'), lng: mapDiv.data('longitude')},
            styles: [
                {
                    featureType: 'poi.business',
                    stylers: [
                        {visibility: 'off'}
                    ]
                },
                {
                    featureType: 'poi.attraction',
                    stylers: [
                        {hue: '#ff0000'},
                        {saturation: 50},
                        {weight: 20}
                    ]
                }
            ]
        };

        ACC.accommodationdetails.map = new google.maps.Map(mapDiv.get(0), mapOptions);

        ACC.accommodationdetails.marker = new google.maps.Marker({
            position: ACC.accommodationdetails.map.getCenter(),
            map: ACC.accommodationdetails.map,
            title: accommodationOfferingName,
            icon: "https://maps.google.com/mapfiles/kml/pal2/icon28.png"
        });
    },

    bindAmenitiesList: function () {

        var amenities = '.y_amenities-list';

        $('a', amenities).on("click", function (e) {
            e.preventDefault();
            $(this).next('ul').slideToggle('fast', function () {
                if ($('.amenities-items li > ul:hidden').length == 0) {
                    $('.collapse-all').removeClass('hidden');
                    $('.show-all').addClass('hidden');
                } else if ($('.amenities-items li > ul:visible').length == 0) {
                    $('.show-all').removeClass('hidden');
                    $('.collapse-all').addClass('hidden');
                }
            });
        });

        $('.show-all', amenities).on("click", function (e) {
            e.preventDefault();
            $('.amenities-items li > ul').slideDown('fast');
            $(this).addClass('hidden');
            $('.collapse-all').removeClass('hidden');
        });

        $('.collapse-all', amenities).on("click", function (e) {
            e.preventDefault();
            $('.amenities-items li > ul').slideUp('fast');
            $(this).addClass('hidden');
            $('.show-all').removeClass('hidden');
        });

    },

    bindHotelAddAccommodationToCartButton: function() {
        $(".y_addHotelAccommodationToCart").on("click", function() {
            ACC.packagedetails.uncheckAll(".y_changeHotelAccommodationButton");
            $(this).parent().find(".y_changeHotelAccommodationButton").prop("checked", true);
            ACC.accommodationdetails.addHotelAccommodationToCartOnLoad();
        });
    },

    addHotelAccommodationToCartOnLoad: function() {
        if ($("#y_accommodationAddToCartForm").length !== 0) {
            // reveal spinner inside reservation component
            $('.y_spinner').removeClass('hidden');
            // trigger package build message
            $('#accommodation-build-modal').modal();

            setTimeout(function() {
                // remove accommodation build message
                ACC.accommodationdetails.hideAccomodationBuildModal();
            }, 2500);
        }

        $.when(ACC.accommodationdetails.addHotelAccommodationToCart()).then(function() {
            if (!ACC.accommodationdetails.addHotelAccommodationToCartResult) {
                $('.y_spinner').addClass('hidden');
                // hide spinner in reservation
                $("#y_addAccommodationToCartErrorModal").modal();
            } else {
                ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
                ACC.reservation.refreshAccommodationSummaryComponent($("#y_accommodationSummaryComponentId").val());

                $('.y_spinner').addClass('hidden');
                ACC.accommodationdetails.hideAccomodationBuildModal();
                var salesChannel=$("#salesChannel").val();
                if(salesChannel=='TravelCentre'){
                    window.location.href= ACC.config.contextPath+"/alacarte-review";
                }
                else{
                    var controller="/accommodation-details/next";
                    if ($('#availabilityStatus').val() == "ON_REQUEST") {
                        controller = controller+"?availabilityStatus=ON_REQUEST"
                    }
                    window.location.href= ACC.config.contextPath+controller;
                }


            }
        });
    },

    addHotelAccommodationToCart: function() {
        var dfd = $.Deferred();

        var selectedRoomStays = $('.y_changeHotelAccommodationButton:checked');
        ACC.accommodationdetails.addHotelAccommodationToCartResult = true;
        $.when(ACC.accommodationdetails.addHotelAccommodationToCartEntry(selectedRoomStays, 0)).then(function() {
            dfd.resolve();
        });
        return dfd.promise();
    },

    addHotelAccommodationToCartEntry: function(selectedRoomStays, index) {
        var hotelAccommodationAddToCartForm = ACC.accommodationdetails.buildHotelAccommodationAddToCartForm(selectedRoomStays[index], "/cart/accommodation/add");
    },

    showRemoveRoomModal:function(){

       $(".removeRoomButton").on("click", function(e) {
           $('#removeAccommodationUrl').val($(this).find("#removeRoomURL").val());
                $('#y_showRemoveRoomModal').modal('show');

           })
           $("#y_removeRoomConfirm").on('click',function(e){
               $('#y_showRemoveRoomModal').modal('hide');
               $("#removeRoomForm").attr('action', $('#removeAccommodationUrl').val());
                $("#removeRoomForm").submit();
               })
        },

    buildHotelAccommodationAddToCartForm: function(element, formAction) {
        var ratePlanAttributesDiv = $(element).closest(".y_ratePlanAttributes");
        var roomStayContainerDiv = $(element).closest(".y_roomStayContainer");

        var checkInDate = roomStayContainerDiv.find("input[class=y_checkInDate]").val();
        var checkOutDate = roomStayContainerDiv.find("input[class=y_checkOutDate]").val();
        var roomStayRefNumber = roomStayContainerDiv.find("input[class=y_roomStayRefNumber]").val();
        var accommodationCode = roomStayContainerDiv.find("input[class=y_accommodationCode]").val();
        var ratePlanCode = ratePlanAttributesDiv.find("input[class=y_ratePlanCode]").val();

        var listOfRoomRateCodes = [];
        var listOfRoomRateDates = [];
        ratePlanAttributesDiv.find("input[class=y_roomRate]").each(function() {
            listOfRoomRateCodes.push($(this).attr('code'));
            listOfRoomRateDates.push($(this).val());
        });

        var accommodationAddToCartForm = $("#y_accommodationAddToCartForm");
        var numberOfRoomsFromForm = roomStayContainerDiv.find("input[class=y_numberOfRooms]").val();
        accommodationAddToCartForm.find("#y_checkInDate").attr('value', checkInDate);
        accommodationAddToCartForm.find("#y_checkOutDate").attr('value', checkOutDate);
        accommodationAddToCartForm.find("#y_accommodationCode").attr('value', accommodationCode);
        accommodationAddToCartForm.find("#y_roomRateCodes").attr('value', listOfRoomRateCodes);
        accommodationAddToCartForm.find("#y_roomRateDates").attr('value', listOfRoomRateDates);
        accommodationAddToCartForm.find("#y_ratePlanCode").attr('value', ratePlanCode);
        accommodationAddToCartForm.find("#y_roomStayRefNumber").attr('value', roomStayRefNumber);
        accommodationAddToCartForm.find("#y_changeVar").attr('value', $("#changeVar").val());
        accommodationAddToCartForm.find("#y_modifyRoomRef").attr('value', $("#modifyRoomRef").val());

        var currentlySelectedOption = $(element);
        formAction = ACC.config.contextPath + formAction;

        $.when(ACC.services.addAccommodationToCartAjax(accommodationAddToCartForm, formAction)).then(
            function(response) {
                var jsonData = JSON.parse(response);

                if (jsonData.valid) {
                    ACC.accommodationdetails.refreshPage("#roomOptionsCollapse");
                } else {
                    ACC.accommodationdetails.addHotelAccommodationToCartResult = false;
                    var output = [];
                    jsonData.errors.forEach(function(error) {
                        output.push("<p>" + error + "</p>");
                    });
                    $("#y_addAccommodationToCartErrorModal").find(".y_addAccommodationToCartErrorBody").html(output.join(""));
                    $('#y_processingModal').modal("hide");
                    $("#y_addAccommodationToCartErrorModal").modal();
                }
            }
        );
    },

    refreshPage: function(elementID) {
        sessionStorage.setItem("elementToUncollapse", elementID);
        sessionStorage.setItem("scrollTop", $(this).scrollTop());
        location.reload(true);
    },

    hideAccomodationBuildModal: function() {
        if ($('#accommodation-build-modal').is(":visible")) {
            $('#accommodation-build-modal').modal('hide');
        }
    },

    sameRoomTypeDiffRatePlanStockAvailable: function (currentNoOfRoomsSelection, previousSelectedValue) {
        var currentNoOfRoomsSelectionId = currentNoOfRoomsSelection.attr('id');
        var maxStockQuantity = Number(currentNoOfRoomsSelection.data('maxstockquantity'));
        var currentNoOfRoomsSelectionIdArr = currentNoOfRoomsSelectionId.split('_');
        var roomQtySelectedAvailableInStock = true;

        if (currentNoOfRoomsSelectionIdArr.length == 3) {
            var numberOfAccomodationPrefix = currentNoOfRoomsSelectionIdArr[0];
            var roomStayIndex = currentNoOfRoomsSelectionIdArr[1];
            var similarRoomStayId = numberOfAccomodationPrefix + '_' + roomStayIndex + '_';
            var totalRoomSelectedForSameRoomType = Number(currentNoOfRoomsSelection.val());

            $("select[id^='" + similarRoomStayId + "']").each(function (index, element) {
                if (currentNoOfRoomsSelectionId != $(element).attr('id')) {
                    totalRoomSelectedForSameRoomType += Number($(element).val());
                    if (totalRoomSelectedForSameRoomType > maxStockQuantity) {
                        var plandescription = $(element).data('plandescription');
                        var errorModalBodyMsg = $(".y_ratePlanRoomSelectStockErrorModal .y_ratePlanRoomSelectStockErrorModalBody").data('errormodalbodymsg');
                        errorModalBodyMsg = errorModalBodyMsg.replace('{0}', maxStockQuantity);
                        errorModalBodyMsg = errorModalBodyMsg.replace('{1}', plandescription);
                        $(".y_ratePlanRoomSelectStockErrorModal .y_ratePlanRoomSelectStockErrorModalBody").html(errorModalBodyMsg);
                        $(".y_ratePlanRoomSelectStockErrorModal").modal();
                        roomQtySelectedAvailableInStock = false;
                        currentNoOfRoomsSelection.val(previousSelectedValue);
                        return false;
                    }
                }
            });
        }
        return roomQtySelectedAvailableInStock;
    },

    bindAccommodationDetailsReviewShowMore: function () {
        $(".y_accommodationCustomerReviewShowMore").on('click', function () {
            var accommodationOfferingCode = $(this).data('accommodationofferingcode');
            var pageNumber = $(this).data('pagenumber');
            var reviewsHeight = $('.reviews-container .row').outerHeight();

            $.when(ACC.services.getPagedAccommodationDetailsCustomerReviews(accommodationOfferingCode, ++pageNumber)).then(
                function (data) {
                    $(".y_customerReviewListItems").append(data.customerReviewsPagedHtml);
                    $(".reviews-container").animate({scrollTop: reviewsHeight}, 600);
                    if (!data.hasMoreReviews) {
                        $(".y_accommodationCustomerReviewShowMore").hide();
                    }
                });
            $(this).data('pagenumber', pageNumber);
            return false;
        });
    },

    bindContinueButton: function () {
        $(".y_accommodationDetailsContinue").on('click', function (e) {
            e.preventDefault();
            var form = $(this).closest(".y_continueForm");
            $.when(ACC.services.validateAccommodationCartAjax()).then(
                function (response) {
                    if (response.valid) {
                        $(form).submit();
                    } else {
                        $("#y_validateCartErrorModal").modal();
                    }
                });
        });
    },

    enableContinueButton: function () {
        $(".y_accommodationDetailsContinue").removeAttr("disabled");
    },

    showNoAvailabilityModal: function () {
        if ($("#y_noAccommodationAvailability")) {
            var noAccommodationAvailability = $("#y_noAccommodationAvailability").val();
            if (noAccommodationAvailability == "show") {
                $("#y_noAvailabilityModal").modal();
            }
        }
    },

    bindFeaturesMoreLess: function () {

        var features = '.y_features',
            sliceValue = 3,
            more = '.more',
            less = '.less';

        $(features).each(function () {
            var items = $(this).find('li'),
                hiddenItems = items.slice(sliceValue).hide();

            hiddenItems;

            if (items.length > sliceValue) {
                $(this).siblings(more).removeClass('hidden');
                $(more).each(function () {
                    $(this).on('click', function (e) {
                        e.preventDefault();
                        $(this).siblings(features).find('li').slideDown('fast');
                        $(this).addClass('hidden');
                        $(this).siblings(less).removeClass('hidden');
                    });
                });
            }

            $(less).each(function () {
                $(this).on('click', function (e) {
                    e.preventDefault();
                    $(this).siblings(features)
                        .find('li:nth-child(' + sliceValue + ')')
                        .nextAll()
                        .slideUp('fast');
                    $(this).addClass('hidden');
                    $(this).siblings(more).removeClass('hidden');
                });
            });
        });

    },

    bindSocialShare: function () {
        var shareButtons = document.querySelectorAll(".st-custom-button[data-network]");
        for (var i = 0; i < shareButtons.length; i++) {
            var shareButton = shareButtons[i];

            shareButton.addEventListener("click", function (e) {
                var elm = e.target;
                var network = elm.dataset.network;
            });
        }
    }

};
