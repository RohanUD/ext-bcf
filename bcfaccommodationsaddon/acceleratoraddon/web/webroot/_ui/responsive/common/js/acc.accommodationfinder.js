ACC.accommodationfinder = {

	_autoloadTracc : [
		  "bindaccommodationFinderValidationMessages",
	      "init"
	],

	componentParentSelector : '#y_accommodationFinderForm',

	 init: function () {
		 if($(ACC.accommodationfinder.componentParentSelector).is('form')){
			 $(ACC.accommodationfinder.componentParentSelector)[0].reset();
		 }
		 
	     ACC.accommodationfinder.populateCheckOutFieldInit();
	     ACC.accommodationfinder.calculateNumberOfNightsInit();
	     ACC.accommodationfinder.populateNumberOfNightsOnLoad();
	     ACC.accommodationfinder.bindRoomQuantity();
	     ACC.accommodationfinder.bindDestinationAutosuggest();
	     ACC.accommodationfinder.morePassengerDropDown();
	     ACC.accommodationfinder.bindAccommodationFinderLocationEvents();
	     ACC.accommodationfinder.addDatePickerForAccommodationFinder($("#accommodationdatepicker"), $("#accommodationdatepicker1"));
	     ACC.accommodationfinder.renderRoomsUI();
	},
	bindaccommodationFinderValidationMessages : function (){
		ACC.accommodationFinderValidationMessages = new validationMessages("ferry-accommodationFinderValidationMessages");
        ACC.accommodationFinderValidationMessages.getMessages("error");
	},
	populateCheckOutFieldInit : function(){

		var $checkInDateField = $(".y_accommodationFinderDatePickerCheckIn");
		var $checkOutDateField = $(".y_accommodationFinderDatePickerCheckOut");

		$checkInDateField.on("change keyup", function() {
			var code;
		    if(!e){
		    	var e = window.event;
		    }
		    if(e.keyCode){
		    	code = e.keyCode;
		    }
		    else if(e.which){
		    	code = e.which;
		    }

		    if(code == 8 || code == 46 || code == 37 || code == 39 ||code == undefined){
		    	if(!$(this).val()){
		    		var numberOfNightsValue = $('.nights-placeholder-text');
		    		 $("#y_numberOfNights").text('0');
		    		 numberOfNightsValue.last().addClass('hidden');
					 numberOfNightsValue.first().removeClass('hidden');
		    	}
		    	return false;
		    }
			ACC.accommodationfinder.populateCheckOutField($checkInDateField, $checkOutDateField);
			ACC.accommodationfinder.calculateNumberOfNights($checkInDateField, $checkOutDateField);
		});

	},

	calculateNumberOfNightsInit : function(){

		var $checkInDateField = $(".y_accommodationFinderDatePickerCheckIn");
		var $checkOutDateField = $(".y_accommodationFinderDatePickerCheckOut");

		$checkOutDateField.on("change keyup", function() {
			ACC.accommodationfinder.calculateNumberOfNights($checkInDateField, $checkOutDateField);
		});

	},

	populateNumberOfNightsOnLoad : function(){
    	if(!$(".y_accommodationFinderDatePickerCheckOut").val() && !$(".y_accommodationFinderDatePickerCheckIn").val()){
    		return;
    	}
    	ACC.accommodationfinder.populateNumberOfNightsOnListingPage($(".y_accommodationFinderDatePickerCheckIn"), $(".y_accommodationFinderDatePickerCheckOut"));
    },

    populateNumberOfNightsOnListingPage : function($checkInDateField, $checkOutDateField){
    	var checkOutDate = ACC.travelcommon.convertToUSDate($checkOutDateField.val());
		var checkInDate = ACC.travelcommon.convertToUSDate($checkInDateField.val());
		var numberOfNightsValue = $('.nights-placeholder-text');
		if(jQuery.type(checkOutDate)==='date' && jQuery.type(checkInDate)==='date'){
            var numberOfNights = Math.round((checkOutDate - checkInDate)/(1000*60*60*24));

				if(numberOfNights<=0){
					var date2 = $checkInDateField.datepicker('getDate', '+1d');
					if(date2 != undefined && date2 != null){
						date2.setDate(date2.getDate()+1);
						$checkOutDateField.datepicker('setDate', date2);
					}
					numberOfNights = 1;
				}
				if(isNaN(numberOfNights)){
            		numberOfNights=0;
            	}
				if(numberOfNights > 1){
					numberOfNightsValue.last().addClass('hidden');
					numberOfNightsValue.first().removeClass('hidden');
				} else if(numberOfNights <= 1) {
					numberOfNightsValue.last().removeClass('hidden');
					numberOfNightsValue.first().addClass('hidden');
				}
				$("#y_numberOfNights").text(numberOfNights);

		}
    },

    // Suggestions/autocompletion for Origin location
	bindDestinationAutosuggest: function () {
		if(!$(".y_accommodationFinderLocation" ).is('input')){
			return;
		}
		$(".y_accommodationFinderLocation").autosuggestion({
            //inputToClear: ".y_fareFinderDestinationLocation",
            autosuggestServiceHandler: function (locationText) {
                var suggestionSelect = "#y_accommodationFinderLocationSuggestions";
                // make AJAX call to get the suggestions
                $.when(ACC.services.getSuggestedAccommodationLocationsAjax(locationText)).then(
                    // success
                    function (data, textStatus, jqXHR) {
                        $(suggestionSelect).html(data.htmlContent);
                        if (data.htmlContent) {
                            $(suggestionSelect).removeClass("hidden");
                        }
                    }
                );
            },
            suggestionFieldChangedCallback: function () {
            	$(".y_accommodationFinderLocation").valid();
            },
            attributes : ["Code", "SuggestionType", "Latitude", "Longitude", "Radius"]
        });
	},

	bindAccommodationFinderLocationEvents : function(){
		if(!$(".y_accommodationFinderLocation").is('input')){
			return;
		}

		$(".y_accommodationFinderLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_accommodationFinderLocationCode").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				//select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a").first();
					firstSuggestion.click();
				}
				$(this).valid();

			};
		});

		$(".y_accommodationFinderLocation").blur(function (e) {
			$(".y_accommodationFinderLocation").valid();
		});

		$(".y_accommodationFinderLocation").focus(function (e) {
			$(this).select();
		});
	},

    bindRoomQuantity : function() {
	        roomId = '#room',
	        hide = 'hidden';
	        $('.y_accommodationRoomQuantity').change(function() {
	            var roomQuantity = $(this).val(),
	            i;
	            for (i = 1; i <= roomQuantity; i++) {
	                $(roomId+'\\['+i+'\\]').removeClass(hide);
	            };
	            $(roomId+'\\['+(i-1)+'\\]').nextUntil('.y_roomStayCandidatesError').addClass(hide);
	        });
	},

	 addDatePickerForAccommodationFinder : function($checkInDateField, $checkOutDateField){

	    	var todayDate = new Date();
	        $checkInDateField.datepicker({
                minDate: todayDate,
			    maxDate: '+1y',
			    dateFormat: "mm/dd/yy",
	            beforeShow: function (input) {

	            	setTimeout(function() {
	            		$checkInDateField.rules('remove')
	            		}, 0);

	            }

	        });

		 $checkOutDateField.datepicker({
            minDate: todayDate,
	            maxDate: '+1y',
	           dateFormat: "mm/dd/yy",
	            beforeShow: function (input) {

	            	setTimeout(function() {
	            		$checkOutDateField.rules('remove')
	            		}, 0);

	            }
	        });
	 },

	 addCheckInDateFieldValidation : function($checkInDateField) {
			var minDate = ACC.travelcommon.getTodayUKDate();

			$checkInDateField.rules("add",
							{
								dateUK : true,
								dateGreaterEqualTo : minDate,
								dateLessEqualTo : ACC.travelcommon
										.getHundredYearUKDate()
							});

	 },

	 addCheckOutDateFieldValidation : function($checkOutDateField, selMinDate) {
			  var minDate = ACC.travelcommon.getTodayUKDate();

		        if (selMinDate) {
		            minDate = selMinDate;
		        }
		    minDate = ACC.travelcommon.addDays(ACC.travelcommon.convertToUSDate(minDate),1);
			$checkOutDateField.rules("add",
							{
								dateUK : true,
								dateGreaterEqualTo : minDate,
								dateLessEqualTo : ACC.travelcommon
										.getHundredYearUKDate()
							});
	 },

	 calculateNumberOfNights : function($checkInDateField, $checkOutDateField){
				var checkOutDate = $checkOutDateField.datepicker('getDate'),
					checkInDate = $checkInDateField.datepicker('getDate'),
					numberOfNightsValue = $('.nights-placeholder-text');

				if(jQuery.type(checkOutDate)==='date' && jQuery.type(checkInDate)==='date'){
					var numberOfNights = Math.round((checkOutDate - checkInDate)/(1000*60*60*24));
					var date2 = ACC.travelcommon.convertToUSDate($checkInDateField.val());
					if(date2==null || isNaN(date2.getTime()) || date2.getTime()<0){
						$("#y_numberOfNights").text("0");
						numberOfNightsValue.last().addClass('hidden');
						numberOfNightsValue.first().removeClass('hidden');
						return;
					}

					if(numberOfNights > 1){
						numberOfNightsValue.last().addClass('hidden');
						numberOfNightsValue.first().removeClass('hidden');
					} else if(numberOfNights <= 1) {
						numberOfNightsValue.last().removeClass('hidden');
						numberOfNightsValue.first().addClass('hidden');
					}

					if(numberOfNights<=0){
						date2.setDate(date2.getDate()+1);
						$checkOutDateField.datepicker('setDate', date2);
						numberOfNights = 1;
					}
					else if(numberOfNights>ACC.config.maxAllowedDateDifference){
						date2.setDate(date2.getDate()+ACC.config.maxAllowedDateDifference);
						$checkOutDateField.datepicker('setDate', date2);
						numberOfNights = ACC.config.maxAllowedDateDifference;
					}

					$("#y_numberOfNights").text(numberOfNights);
				}else{
					$("#y_numberOfNights").text("0");
					numberOfNightsValue.last().addClass('hidden');
					numberOfNightsValue.first().removeClass('hidden');
				}

	 },

	 populateCheckOutField : function($checkInDateField, $checkOutDateField){

            if($checkOutDateField.datepicker('getDate') == null){
                var date2 = ACC.travelcommon.convertAccomDateToUSDate($checkInDateField.val());

                if(date2==null || isNaN(date2.getTime()) || date2.getTime()<0){
                    return;
                }
                date2.setDate(date2.getDate()+1);

                $checkOutDateField.datepicker('setDate', date2);
                $('label[id*="accommodationDatePickerCheckOut-error"]').text('');
                $("#y_numberOfNights").text("1");
            }else{
                var checkOutDate = $checkOutDateField.datepicker('getDate');
                var checkInDate = $checkInDateField.datepicker('getDate');
                if(jQuery.type(checkOutDate)==='date' && jQuery.type(checkInDate)==='date'){
                    var numberOfNights = Math.round((checkOutDate - checkInDate)/(1000*60*60*24));
                    if(numberOfNights<=0){
                        var date2 = ACC.travelcommon.convertAccomDateToUSDate($checkInDateField.val());

                        if(date2==null || isNaN(date2.getTime()) || date2.getTime()<0){
                            return;
                        }
                        date2.setDate(date2.getDate()+1);

                        $checkOutDateField.datepicker('setDate', date2);

                        numberOfNights = 1;
                    }
                    $("#y_numberOfNights").text(numberOfNights);
                }
            }

	  },


	 addRequiredValidationForAdults : function(formId){
			 $(formId+" "+".adult" ).each(function(){
		            $(this).rules( "add", {
		              adultGuestValue: true,
		              messages: {
		            	 adultGuestValue: ACC.accommodationFinderValidationMessages.message("error.accommodationfinder.guest.adult")
		              }
		            });
		        });
	 },

	 morePassengerDropDown: function(){
	    	$('#acmorePassenger').on('click', function () {
	    		if($(".acmorePassengersDiv").hasClass("sr-only")){
	    			$(".acmorePassengersDiv").removeClass("sr-only");
	    		}else{
	    			$(".acmorePassengersDiv").addClass("sr-only");
	    		}
	        });
	  },


	 reInitializeCheckInDate : function($checkInDateField){
        if($checkInDateField.val()!=null){
			var currDate = ACC.travelcommon.getTodayUKDate();
			if(ACC.travelcommon.convertToUSDate($checkInDateField.val()).getTime() > 0){
				if($checkInDateField.val() && ACC.travelcommon.convertToUSDate($checkInDateField.val()).getTime() < ACC.travelcommon.convertToUSDate(currDate).getTime()){
					$checkInDateField.datepicker('setDate', ACC.travelcommon.convertToUSDate(currDate));
	    		}
			}
	    }
	 },

     renderRoomsUI : function(){
        var dt = new Date();
        if($(".accommodation-current-year-custom").length && $(".accommodation-current-year-custom").text()==""){
            $(".accommodation-current-year-custom").html(dt.getFullYear());
        }

        //init accordion
        $("#vacation-accordion2").accordion();
    	$("#vacation-accordion2").accordion({
    		header : "h3",
    		collapsible : true,
            active : false,
            beforeActivate: function( event, ui ) {
                var wd = $(event.currentTarget).outerWidth();
                $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
            }
    	});

        //render accordion

        $("#accommodation-accordion1").find('.sub-link').click(ACC.travelfinder.sourceClickHander);
        $("#accommodation-accordion1").find('.sub-link').click(function(){
            $(".y_accommodationDestinationLocation").val($(this).data("code"));
        });


        $("#accommodation-accordion1").accordion({ header: "h3", collapsible: true, active: false, heightStyle: "content" });

        $("#vacation-accordion2").click(function(){
            $("#vacation-accordion1").closest(".ui-accordion").accordion({active:2});
            var destinationCode = document.getElementById('y_destinationCode').value;
            if(destinationCode.length != 0){
                ACC.travelfinder.sourceClickHander();
            }
        });

        $(".accommodation-rooms-wrapper").find("input").change(function(){
            var prev = $(this).data('val');
            var current = $(this).val();
            $(this).data('val',current);
            if(prev==undefined){
                ACC.accommodationfinder.addRoomWidget();
             }else if(parseInt(prev) < parseInt(current)){
                ACC.accommodationfinder.addRoomWidget();
             }else{
                ACC.accommodationfinder.removeRoomWidget();
             }

        })

        $(document).on("change",".accommodation-select-guest-accordion .accommodation-child-qty",function(){
            var prev = $(this).data('val');
            var current = $(this).val();
            $(this).data('val',current);

            if(prev==undefined){
                ACC.accommodationfinder.addChildBox($(this));
            }else if(parseInt(prev) < parseInt(current)){
                ACC.accommodationfinder.addChildBox($(this));
            }else{
                 ACC.accommodationfinder.removeChildBox($(this));
            }
        })


        var noOfRoom = $("#numberOfRooms").val();
        $(".accommodation-rooms-wrapper input").val(noOfRoom);
        if(noOfRoom>0){
            $(".accommodation-rooms-wrapper input").data("val",noOfRoom-1);
        }
        //add room widget based on number of room on load
        var noOfRoom = $(".accommodation-rooms-wrapper").find("input").val();
        for(var i = 0; i<noOfRoom ; i++){
            ACC.accommodationfinder.addRoomWidget(i+1);
        }

        setTimeout(function(){
            $("body").trigger("setSearchDropDown",$("#accommodation-accordion1"));
            $("body").trigger("setSearchDropDown",$("#vacation-accordion2"));
        },500)


        $("#y_accommodationFinderForm").on("submit",function(){

            var y_travelFinderForm_data = ACC.accommodationfinder.accommodationValidation('BOOKING_ACCOMMODATION_ONLY');
            ACC.carousel.adjustSearchHeight();
            return y_travelFinderForm_data;
         })


     },

     addChildBox : function(ele,index){

        if(index==null){
            var childNumber=parseInt($(ele).val())-1;
        }else{
            var childNumber=index;
        }
        var roomNo=parseInt(ele.parent().find(".room-number").text());
        var childAge = $('#childAge'+roomNo+'1'+childNumber).text();

        var childName = "roomStayCandidates["+(roomNo)+"].passengerTypeQuantityList[1].childAges["+(childNumber)+"]";
        var childWrapper = ele.closest(".ui-accordion-content").find(".accommodation-child-box-wrapper");
        var childNo = ele.closest(".ui-accordion-content").find(".accommodation-child-qty").val();
        var childBox = $(".snippets").find(".accommodation-child-box").clone();
        childBox.find(".custom-select").attr("name",childName);

        if(index == undefined){
            childBox.find(".accommodation-child-label-name-span").html(childNo);
        }
        else {
            childBox.find(".accommodation-child-label-name-span").html(index);
        }

        var childAgeOption = "";
        for (var j = 0; j <= 18; j++) {
            if(j==childAge){
                var option = '<option value="' + j + '" selected> ' + j + ' </option>';
            }else{
                var option = '<option value="' + j + '"> ' + j + ' </option>';
            }

            childAgeOption = childAgeOption + option;
        }
        childBox.find(".custom-select").append(childAgeOption);
        childWrapper.append(childBox);
     },

     removeChildBox : function(ele){
        var childWrapper = ele.closest(".ui-accordion-content").find(".accommodation-child-box-wrapper");
        childWrapper.find(".accommodation-child-box:last").remove();

     },
     addRoomWidget : function(index){
        var roomNo = $(".accommodation-rooms-wrapper").find("input").val();
        if(index !== undefined){
            roomNo = index;
        }
        var roomWidget = $(".snippets").find(".accommodation-room-widget").clone();
        var adultName = "roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[0].quantity";
        var childName = "roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[1].quantity";
        var adultCode = "roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[0].passengerType.code";
        var childCode = "roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[1].passengerType.code";
        var roomStayRefNumber = "roomStayCandidates["+(roomNo-1)+"].roomStayCandidateRefNumber";
        roomWidget.find(".room-title span").html(roomNo);
        roomWidget.find(".room-number").html(roomNo-1);
        roomWidget.find(".accommodation-adult-qty").attr("name",adultName);
        roomWidget.find(".accommodation-adult-passenger").attr("name",adultCode);
        roomWidget.find(".room-stay-ref-number").attr("name",roomStayRefNumber);
        roomWidget.find(".room-stay-ref-number").attr("value",roomNo-1);

        roomWidget.find(".accommodation-adult-qty").siblings(".btn-number").each(function(){
            $(this).attr("data-field",adultName);
        })

        roomWidget.find(".accommodation-child-qty").val("0");
        roomWidget.find(".accommodation-child-qty").attr("name",childName);
        roomWidget.find(".accommodation-child-passenger").attr("name",childCode);
        roomWidget.find(".accommodation-child-qty").siblings(".btn-number").each(function(){
            $(this).attr("data-field",childName);
        })


        var roomWidgetWrapper = $(".accommodation-room-widget-wrapper");
        roomWidgetWrapper.append(roomWidget);
        roomWidgetWrapper.find(".accommodation-select-guest-accordion:last").accordion({
            header : "h3",
            collapsible : true,
            heightStyle: "content",
            active : false,
            activate: function(event, ui) {
                if(ui.newHeader.length === 0){
                    ACC.accommodationfinder.renderAccordionState($(this),"close");
                }else{
                    ACC.accommodationfinder.renderAccordionState($(this),"open");
                }
            },
            beforeActivate: function( event, ui ) {
                var wd = $(event.currentTarget).outerWidth();
                $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
            }

        });
        setTimeout(function(){
            roomWidget.removeClass("hidden");
            $(window).trigger("resize");
         },500)
     },

     removeRoomWidget : function(){
        $(".accommodation-room-widget-wrapper").find(".accommodation-room-widget:last").remove();
        $(window).trigger("resize");
     },

     renderAccordionState : function(ele,state){
          var headerIcon = ele.find("h3 .ui-accordion-header-icon");
          var headerIconCustom = ele.find("h3 .custom-arrow");
          var msgWrapper = ele.closest(".select-guest-custom").find(".accommodation-guest-gray");
          ACC.accommodationfinder.renderAccordionMessage(ele);
          if(state == "open"){
            headerIcon.removeClass("ui-icon");
            headerIconCustom.addClass("hidden");
            msgWrapper.addClass("hidden");
            if(headerIcon.find('i').length==0)
              headerIcon.append('<i class="fa fa-check" aria-hidden="true">');
          }else{
            msgWrapper.removeClass("hidden");
            headerIcon.addClass("ui-icon");
            headerIcon.find('i').remove();
            headerIconCustom.removeClass("hidden");
          }
          $(window).trigger("resize");

     },

     renderAccordionMessage : function(ele){

        var adultNo = ele.find(".accommodation-adult-qty").val();
        var childNo = ele.find(".accommodation-child-qty").val();
        var msgWrapper = ele.closest(".select-guest-custom").find(".accommodation-guest-gray");
        msgWrapper.find(".accommodation-adult-count").html(adultNo);
        msgWrapper.find(".accommodation-child-count").html(childNo);
        if(ele.find(".accommodation-infant-qty").length !== 0){
            msgWrapper.find(".accommodation-infant-count").html(ele.find(".accommodation-infant-qty").val());
        }
     },

     addDatePickerForAccommodationFinder : function($departureDateField, $returnDateField){
        var todayDate = new Date();
         $departureDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'mm/dd/yy',

                beforeShow: function (input) {

                    setTimeout(function() {
                        if($returnDateField.length){
                            $departureDateField.rules('remove')
                        }

                        }, 0);
                },
                onSelect: function (selectedDate) {

                    // add validation to departure date
                    var ele = $(this).siblings(".datePickerDeparting");
                    $(ele).val(selectedDate);

                    $(".accommodation-ui-depart").find(".vacation-calen-year-txt").text(selectedDate);

                    //minimun date
                    $("#accommodationdatepicker1").datepicker("option",{ minDate: selectedDate });


                    $(".vacation-calender").find("li.tab-depart").removeClass("active");

                    $(".vacation-calender-section").find(".depart-calendar").removeClass("active");

                }
          });
         if($returnDateField != undefined)
         {
            $returnDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'mm/dd/yy',
                beforeShow: function (input) {

                    setTimeout(function() {
                        $returnDateField.rules('remove')
                        }, 0);
                },
                onSelect: function (selectedDate) {
                  var ele = $(this).siblings(".datePickerReturning");
                  $(ele).val(selectedDate);
                  $(".accommodation-ui-return").find(".vacation-calen-year-txt").text(selectedDate);
                  $(".ui-datepicker a").removeAttr("href");
                  $(ele).change();
                  $(".vacation-calender").find("li.tab-return").removeClass("active");
                  $(".vacation-calender-section").find(".return-calendar").removeClass("active");

                }
            });

              //binding input to datepicker
              $(".depart.datepicker-input").keyup(function(){
                $departureDateField.datepicker("setDate",$(this).val());
                var ele = $(this).closest(".tab-pane").attr("id");
                $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
                $(".ui-return").trigger("setDate",$(this).val());
              });

              $(".arrive.datepicker-input").keyup(function(){
                $returnDateField.datepicker("setDate",$(this).val());
                var ele = $(this).closest(".tab-pane").attr("id");
                $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
              })

         }

         $('#departingDateTime').keydown(function(e) {
               e.preventDefault();
               return false;
         });

         $('#returnDateTime').keydown(function(e) {
               e.preventDefault();
               return false;
         });

         $(document).mouseup(function(e)
         {
            var container = $(".vacation-calender-section");

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                $(".vacation-calender-section").find(".tab-links.active").removeClass("active");
                $(".vacation-calender-section").find(".tab-pane.active").removeClass("active");
            }
         });


     },


     accommodationValidation : function(journeyType){

          if ( ACC.common.shallCartBeRemoved(journeyType) == false) {
            return false;
          }

          if($("#accommodation-accordion1").find(".dropdown-text strong").text() == "Select a destination (city)"){
            ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.from.location"));
            return false;
          }

          if($("#accommodation-check-in .bc-dropdown.depart").val()==""){
            ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.checkin.date"));
            return false;
          }

          if($("#accommodation-check-out .bc-dropdown.arrive").val()==""){
            ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.checkout.date"));
            return false;
          }

          ACC.travelfinder.showErrorMessage("","hide");
          return true;
     }



};


