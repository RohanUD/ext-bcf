/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.validators;

import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.regex.Matcher;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfaccommodationsaddon.forms.cms.ChangeBookingForm;
import com.bcf.facades.constants.BcfFacadesConstants;


@Component("changeBookingRequestValidator")
public class ChangeBookingRequestValidator implements Validator

{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return ChangeBookingForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ChangeBookingForm changeBookingForm = (ChangeBookingForm) object;
		final String changeBookingDescription = changeBookingForm.getDescription();
		final String contactNumber = changeBookingForm.getContactnumber();
		final String emailAddress = changeBookingForm.getEmail();

		final String customerFirstName = changeBookingForm.getFirstname();
		final String customerLastName = changeBookingForm.getLastname();


		if (StringUtils.isBlank(changeBookingDescription))
		{
			errors.rejectValue("description", "emptyChangeBookingDescription");
		}

		if (StringUtils.isBlank(contactNumber))
		{
			errors.rejectValue("contactnumber", "emptyContactNumber");

		}else if(!validateContactNumber(contactNumber)){

			errors.rejectValue("contactnumber", "invalidContactNumber");
		}

		if (StringUtils.isBlank(emailAddress))
		{
			errors.rejectValue("email", "emptyEmailAddress");

		}else if  (StringUtils.length(emailAddress) > 255 || !validateEmailAddress(emailAddress)){

			errors.rejectValue("email", "invalidEmailAddress");
		}

		if (StringUtils.isBlank(customerFirstName))
		{
			errors.rejectValue("firstname", "emptyCustomerFirstName");

		}else if  (!validateName(customerFirstName)){

			errors.rejectValue("firstname", "invalidCustomerFirstName");
		}

		if (StringUtils.isBlank(customerLastName))
		{
			errors.rejectValue("lastname", "emptyCustomerLastName");

		}else if  (!validateName(customerLastName)){

			errors.rejectValue("lastname", "invalidCustomerLastName");

		}

	}

	protected boolean validateName(final String name)
	{
		return  name.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES_SPACES_FIRST_LETTER);

	}


	private boolean validateContactNumber(final String contactNumber)
	{
			return contactNumber.matches(TravelacceleratorstorefrontValidationConstants.REGEX_AT_LEAST_ONE_NUMBER)
					|| contactNumber.matches(TravelacceleratorstorefrontValidationConstants.REGEX_NUMBER_AND_SPECIAL_CHARS);
	}

	private boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = BcfFacadesConstants.EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

}
