/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.validators;

import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfaccommodationsaddon.forms.AccommodationBookingChangeDateForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;


@Component("accommodationBookingChangeDateValidator")
public class AccommodationBookingChangeDateValidator extends AbstractTravelValidator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return AccommodationBookingChangeDateValidator.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AccommodationBookingChangeDateForm form = (AccommodationBookingChangeDateForm) object;

		validateBlankField("bookingReference", form.getBookingReference(), errors);
		validateDates(form.getCheckInDateTime(), TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE,
				form.getCheckOutDateTime(), TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE, errors);
	}
}
