/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.validators;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.constants.BcfFacadesConstants;


@Component("accommodationFinderValidator")
public class AccommodationFinderValidator extends AbstractTravelValidator
{

	private static final Logger LOGGER = Logger.getLogger(AccommodationFinderValidator.class);
	private static final String ERROR_INVALID_LOCATION = "InvalidLocation";
	private static final String ERROR_TYPE_INVALID_FIELD_FORMAT = "InvalidFormat";
	private static final String ERROR_TYPE_INVALID_THRESHOLD_TIME = "InvalidThresholdTime";

	private static final String FIELD_DESTINATION_LOCATION = "destinationLocation";
	private static final String ERROR_INVALID_SUGGESTION_TYPE = "InvalidSuggestionType";
	public static final String REGEX_LETTERS_PIPELINE = "^([a-zA-Z]+((\\|[a-zA-Z_]+)?)*|[a-zA-Z0-9_]+)$";
	private static final String ERROR_PREFIX = "error.ferry.farefinder.vehicleInfo";


	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return AccommodationFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AccommodationFinderForm accommodationFinderForm = (AccommodationFinderForm) object;

		validateDates(accommodationFinderForm.getCheckInDateTime(), TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE,
				accommodationFinderForm.getCheckOutDateTime(), TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE, errors);
		validateGuestsQuantity(accommodationFinderForm.getNumberOfRooms(),
				accommodationFinderForm.getRoomStayCandidates(), errors);

		if(!assistedServiceFacade.isAssistedServiceAgentLoggedIn()){
			validateThresholdValue(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE,accommodationFinderForm.getCheckInDateTime(),errors);
		}


	}

	protected void validateDestinationLocation(final AccommodationFinderForm accommodationFinderForm, final Errors errors)
	{

		// destinationLocationName should always be populated
		if (validateBlankField(BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION_NAME,
				accommodationFinderForm.getDestinationLocationName(), errors) && validateFieldFormat(
				BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION_NAME, accommodationFinderForm.getDestinationLocationName(),
				TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_COMMAS_SPACES_DASHES_PARENTHESIS, errors))
		{
			final String suggestionType = accommodationFinderForm.getSuggestionType();
			final String destinationLocation = accommodationFinderForm.getDestinationLocation();
			if (StringUtils.isNotBlank(destinationLocation) && StringUtils.isNotBlank(suggestionType))
			{
				validateCodeAndSuggestionType(accommodationFinderForm, errors);
			}
			else
			{
				rejectValue(errors, BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION, ERROR_INVALID_LOCATION);
			}
		}

	}

	protected boolean validateCodeAndSuggestionType(final AccommodationFinderForm accommodationFinderForm, final Errors errors)
	{
		final String suggestionType = accommodationFinderForm.getSuggestionType();
		try
		{
			SuggestionType.valueOf(suggestionType);
		}
		catch (final IllegalArgumentException e)
		{
			LOGGER.debug("No SuggestionType found with code " + suggestionType);
			rejectValue(errors, BcfaccommodationsaddonWebConstants.SUGGESTION_TYPE, ERROR_INVALID_SUGGESTION_TYPE);
			return false;
		}
		return validateFieldFormat(FIELD_DESTINATION_LOCATION, accommodationFinderForm.getDestinationLocation(),
				REGEX_LETTERS_PIPELINE, errors);
	}

	/**
	 * Method runs the regex against the fieldValue and sets an InvalidFormat error against the field if the regex fails
	 *
	 * @param fieldName
	 * @param fieldValue
	 * @param regex
	 * @param errors
	 */
	protected Boolean validateFieldFormat(final String fieldName, final String fieldValue, final String regex, final Errors errors)
	{
		if (!Pattern.matches(regex, fieldValue))
		{
			rejectValue(errors, fieldName, ERROR_TYPE_INVALID_FIELD_FORMAT);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	protected Boolean validateThresholdValue(final String fieldName,final String checkInDate,final Errors errors)
	{

		if (!errors.hasErrors()){

			double thresholdValue = Double.parseDouble(bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfFacadesConstants.SAME_DAY_HOTEL_BOOKING_THRESHOLD_HOURS));

		final Date minDateTimeForBooking = DateUtils
				.addMinutes(new Date(), (int) Math.round(thresholdValue * 60));

		final DateFormat dateFormat = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		Date startDate=null;
		dateFormat.setLenient(false);
		try
		{
			startDate=dateFormat.parse(checkInDate);
		}
		catch (final ParseException e)
		{
			return Boolean.FALSE;
		}

		final LocalTime hour = ZonedDateTime.now().toLocalTime();
		final Date selectedDateTime = DateUtils
				.addSeconds(DateUtils.addMinutes(DateUtils.addHours(startDate, hour.getHour()), hour.getMinute()),
						hour.getSecond());
		if (!selectedDateTime.after(minDateTimeForBooking))
		{

			final Object[] localizationArguments = { thresholdValue };
			rejectValue(errors, fieldName,  ERROR_TYPE_INVALID_THRESHOLD_TIME,localizationArguments);

			return Boolean.FALSE;
		}
	}

		return Boolean.TRUE;
	}

}
