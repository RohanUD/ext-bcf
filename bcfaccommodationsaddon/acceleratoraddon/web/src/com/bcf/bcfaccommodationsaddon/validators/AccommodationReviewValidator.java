/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.validators;

import de.hybris.platform.util.Config;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationReviewForm;


@Component("accommodationReviewValidator")
public class AccommodationReviewValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return AccommodationReviewForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AccommodationReviewForm form = (AccommodationReviewForm) object;

		final String headline = form.getHeadline();
		if (StringUtils.isBlank(headline))
		{
			errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_HEADLINE,
					BcfaccommodationsaddonWebConstants.REVIEW_BLANK_HEADLINE_ERROR_CODE);
		}

		if (Objects.nonNull(headline) && !new IntRange(Config.getInt("accommodation.review.headline.min.size", 1),
				Config.getInt("accommodation.review.headline.max.size", 255)).containsInteger((headline.length())))
		{
			errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_HEADLINE,
					BcfaccommodationsaddonWebConstants.REVIEW_HEADLINE_ERROR_CODE);
		}

		final String comment = form.getComment();
		if (StringUtils.isBlank(comment))
		{
			errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_COMMENT,
					BcfaccommodationsaddonWebConstants.REVIEW_BLANK_COMMENT_ERROR_CODE);
		}

		if (Objects.nonNull(comment) && comment.length() < (Config.getInt("accommodation.review.comment.min.size", 1)))
		{
			errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_COMMENT,
					BcfaccommodationsaddonWebConstants.REVIEW_COMMENT_ERROR_CODE);
		}


		try
		{
			final Double rating = form.getRating();
			if (!new DoubleRange(Config.getDouble("customerreview.minimalrating", 0),
					Config.getDouble("customerreview.maximalrating", 10)).containsDouble(rating))
			{
				errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_RATING,
						BcfaccommodationsaddonWebConstants.REVIEW_RATING_ERROR_CODE);
			}
		}
		catch (final NumberFormatException NFex)
		{
			errors.rejectValue(BcfaccommodationsaddonWebConstants.REVIEW_RATING,
					BcfaccommodationsaddonWebConstants.REVIEW_RATING_ERROR_CODE);
		}

	}

}
