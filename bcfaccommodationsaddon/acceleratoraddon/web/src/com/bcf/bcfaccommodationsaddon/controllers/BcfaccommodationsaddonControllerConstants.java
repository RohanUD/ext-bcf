/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers;

import com.bcf.bcfaccommodationsaddon.model.components.AccommodationBookingDetailsComponentModel;
import com.bcf.bcfaccommodationsaddon.model.components.AccommodationBookingListComponentModel;
import com.bcf.bcfaccommodationsaddon.model.components.AccommodationBreakdownComponentModel;
import com.bcf.bcfaccommodationsaddon.model.components.AccommodationFinderComponentModel;
import com.bcf.bcfaccommodationsaddon.model.components.AccommodationReservationComponentModel;
import com.bcf.bcfaccommodationsaddon.model.components.AccommodationSummaryComponentModel;


public interface BcfaccommodationsaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/bcfaccommodationsaddon/";

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Pages
		{
			interface Hotel
			{
				String PackageDetailsResponse = ADDON_PREFIX + "cms/packagedetailsroomoptions";
				String CustomerReviewJsonResponse = ADDON_PREFIX + "pages/hotel/accommodationOfferingCustomerReviewsJsonResponse";
				String HotelDetailJsonResponse = ADDON_PREFIX + "pages/hotel/hotelDetailJsonResponse";
				String AccommodationListingJsonResponse = ADDON_PREFIX + "pages/hotel/accommodationListingJsonResponse";
				String AccommodationResultsViewJsonResponse = ADDON_PREFIX + "pages/hotel/accommodationResultsViewJsonResponse";

				String CustomerReviewPagedJsonResponse =
						ADDON_PREFIX + "pages/hotel/accommodationOfferingDetailsCustomerReviewsJsonResponse";
				String removeRoomJsonResponse = ADDON_PREFIX + "pages/hotel/removeRoomJsonResponse";
				String AddAccommodationToCartResponse = ADDON_PREFIX + "pages/hotel/addAccommodationToCartResponse";
				String AddExtraToCartResponse = ADDON_PREFIX + "pages/hotel/addExtraToCartResponse";
				String ValidateAccommodationCartResponse = ADDON_PREFIX + "pages/hotel/validateAccommodationCartResponse";
				String UpdateBookingDatesPageJsonResponse = ADDON_PREFIX
						+ "pages/hotel/updateAccommodationBookingDatesJsonResponse";
			}
			interface ShipInfo
			{
				String ShipInfoJsonResponse = ADDON_PREFIX + "pages/shipinfo/shipInfoJsonResponse";
			}

			interface FareSelection
			{

				String AddBundleToCartResponse = ADDON_PREFIX + "pages/booking/addBundleToCartResponse";
			}

			interface FormErrors
			{
				String FormErrorsResponse = ADDON_PREFIX + "pages/form/formErrorsResponse";
			}

			interface Suggestions
			{
				String SuggestionsSearchJsonResponse = ADDON_PREFIX + "pages/suggestions/suggestionsSearchJsonResponse";
			}

		}
	}

	interface Actions
	{
		interface Cms
		{
			String _Prefix = "/view/";

			String _Suffix = "Controller";

			/**
			 * CMS components that have specific handlers
			 */
			String AccommodationSummaryComponent = _Prefix + AccommodationSummaryComponentModel._TYPECODE + _Suffix;
			String AccommodationFinderComponent = _Prefix + AccommodationFinderComponentModel._TYPECODE + _Suffix;
			String AccommodationBreakdownComponent = _Prefix + AccommodationBreakdownComponentModel._TYPECODE + _Suffix;
			String AccommodationBookingDetailsComponent = _Prefix + AccommodationBookingDetailsComponentModel._TYPECODE + _Suffix;
			String AccommodationBookingListComponent = _Prefix + AccommodationBookingListComponentModel._TYPECODE + _Suffix;

			String AccommodationReservationComponent = _Prefix + AccommodationReservationComponentModel._TYPECODE + _Suffix;
		}
	}
}
