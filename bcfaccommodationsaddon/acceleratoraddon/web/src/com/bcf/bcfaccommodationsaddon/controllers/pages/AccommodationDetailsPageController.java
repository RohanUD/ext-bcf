/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomStayGroupData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.accommodation.search.AccommodationSearchFacade;
import de.hybris.platform.travelfacades.order.AccommodationCartFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.AccommodationAddToCartBookingForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationAvailabilityForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.BcfAccommodationAddToCartForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationAvailabilityValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.bcffacades.BcfAccommodationOfferingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.helper.impl.DefaultBcfAccommodationFacadeHelper;
import com.bcf.facades.utils.BcfFacadeUtil;


/**
 * Controller for Accommodation Details page
 */
@Controller
@RequestMapping({ "/accommodation-details", "/manage-booking/accommodation-details" })
public class AccommodationDetailsPageController extends AbstractAccommodationPageController
{
   private static final Logger LOG = Logger.getLogger(AccommodationDetailsPageController.class);
   private static final String ACCOMMODATION_DETAILS_CMS_PAGE = "accommodationDetailsPage";
   private static final String BOOKING_DETAILS_URL = "/manage-booking/booking-details/";
   private static final String ERROR_AMEND_BOOKING_EMPTY = "empty";
   private static final String ERROR_AMEND_BOOKING_DATES = "dates";
   private static final String ERROR_AMEND_BOOKING_ORDER_ID = "booking.reference";
   private static final String ACCOMMODATION_DETAILS_ROOT = "/accommodation-details/";
   private final String [] hotelPassengers={"adult","child"};

   @Resource(name = "accommodationAvailabilityValidator")
   private AccommodationAvailabilityValidator accommodationAvailabilityValidator;

   @Resource(name = "userFacade")
   private UserFacade userFacade;

   @Resource(name = "accommodationCartFacade")
   private AccommodationCartFacade accommodationCartFacade;

   @Resource(name = "cartFacade")
   private TravelCartFacade travelCartFacade;

   @Resource(name = "bookingFacade")
   private BookingFacade bookingFacade;

   @Resource(name = "priceDataFactory")
   private PriceDataFactory priceDataFactory;

   @Resource(name = "accommodationSearchFacade")
   private AccommodationSearchFacade accommodationSearchFacade;

   @Resource(name = "sessionService")
   private SessionService sessionService;

   @Resource(name = "bcfAccommodationOfferingFacade")
   private BcfAccommodationOfferingFacade bcfAccommodationOfferingFacade;

   @Resource(name = "bcfSalesApplicationResolverFacade")
   private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

   @Resource(name = "salesChannelBookingJourneySummaryUrlsMap")
   private Map<String, String> salesChannelBookingJourneySummaryUrlsMap;

   @Resource(name = "accommodationOfferingService")
   AccommodationOfferingService accommodationOfferingService;

   @Resource(name = "bcfTravelLocationService")
   private BcfTravelLocationService bcfTravelLocationService;

   @Resource(name = "passengerTypeFacade")
   private BCFPassengerTypeFacade passengerTypeFacade;

   @Resource(name = "bcfAccommodationFacadeHelper")
   private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

   @RequestMapping(value = "/{accommodationOfferingCode}", method = RequestMethod.GET)
   public String getAccommodationDetailsPage(@PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
         @Valid @ModelAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM) final
         AccommodationAvailabilityForm accommodationAvailabilityForm,
         final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response,
         final RedirectAttributes redirectModel, final Model model) throws CMSItemNotFoundException
   {
      disableCachingForResponse(response);
      if (travelCartFacade.isAmendmentCart())
      {
         if (isAmendmentJourney(request))
         {
            //In case of amendments user should be prevented from selecting rooms from a different accommodation offering
            final String currentAccommodationOffering = accommodationCartFacade.getCurrentAccommodationOffering();
            if (!StringUtils.equalsIgnoreCase(currentAccommodationOffering, accommodationOfferingCode))
            {
               return REDIRECT_PREFIX + "/";
            }
            sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
                  BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
         }

      }
      else
      {
         adjustSessionBookingJourney();
      }

      final String sessionBookingJourney = sessionService
            .getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
      final String newQueryString = populateQueryString(accommodationOfferingCode, accommodationAvailabilityForm, request, model,
            sessionBookingJourney);
      if (newQueryString != null)
         return newQueryString;

      model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, travelCartFacade.isAmendmentCart());

      accommodationAvailabilityForm.setRoomStayCandidates(createRoomStayCandidates(request));

      accommodationAvailabilityValidator.validate(accommodationAvailabilityForm, bindingResult);

      if (bindingResult.hasErrors())
      {
         redirectModel
               .addFlashAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM_BINDING_RESULT,
                     bindingResult);
         return REDIRECT_PREFIX + "/";
      }

      if (!travelCartFacade.isAmendmentCart())
      {
         populateBestCombinationsForAccommodation(accommodationOfferingCode, accommodationAvailabilityForm, model, request);
      }

      final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData =
            createAccommodationAvailabilityRequestData(
                  accommodationAvailabilityForm, accommodationOfferingCode);

      final SearchPageData<ReviewData> customerReviewsSearchPageData = getPagedAccommodationOfferingCustomerReviews(
            accommodationOfferingCode, 0);
      model.addAttribute(BcfaccommodationsaddonWebConstants.CUSTOMER_REVIEW_SEARCH_PAGE_DATA, customerReviewsSearchPageData);
      model.addAttribute(BcfaccommodationsaddonWebConstants.ADD_ACCOMMODATION_URL,
            BcfstorefrontaddonWebConstants.ADD_ACCOMMODATION_DETAILS_PAGE);
      try
      {
         final AccommodationAvailabilityResponseData accommodationAvailabilityResponse = getAccommodationOfferingFacade()
               .getAccommodationOfferingDetails(accommodationAvailabilityRequestData);
         model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_RESPONSE,
               accommodationAvailabilityResponse);
         model.addAttribute(BcfaccommodationsaddonWebConstants.IS_ACCOMMODATION_AVAILABLE,
               getAccommodationOfferingFacade().checkAvailability(accommodationAvailabilityResponse));
         model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM, accommodationAvailabilityForm);
         model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
               getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));
         final int maxStockLevel = getConfigurationService().getConfiguration()
               .getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
         model.addAttribute("accommodationAddToCartForm", new BcfAccommodationAddToCartForm());
         model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_MAX_STOCK_LEVEL, maxStockLevel);

         final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOMMODATION_DETAILS_CMS_PAGE);
         storeCmsPageInModel(model, contentPageModel);
         setUpMetaDataForContentPage(model, contentPageModel);

         String hotelName = "";
         String hotelCityName = "";

         final PropertyData accommodationReference = accommodationAvailabilityResponse.getAccommodationReference();
         if (Objects.nonNull(accommodationReference))
         {
            hotelName = accommodationAvailabilityResponse.getAccommodationReference().getAccommodationOfferingName();
            if (Objects.nonNull(accommodationReference.getAddress()))
            {
               hotelCityName = accommodationReference.getAddress().getTown();
            }
         }
         final String[] arguments = { hotelName, hotelCityName };
			setUpMetaDataForContentPage(model, contentPageModel, arguments);
         setUpDynamicOGGraphData(model, contentPageModel, request, arguments);

         createOrUpdateCanonicalUrl(model, accommodationReference);

         final AccommodationFinderForm accommodationFinderForm =new AccommodationFinderForm();
         accommodationFinderForm.setCheckOutDateTime(accommodationAvailabilityForm.getCheckOutDateTime());
         accommodationFinderForm.setCheckInDateTime(accommodationAvailabilityForm.getCheckInDateTime());
         accommodationFinderForm.setNumberOfRooms(accommodationAvailabilityForm.getNumberOfRooms());
         accommodationFinderForm.setRoomStayCandidates(filterRoomStayCandidates(accommodationAvailabilityForm.getRoomStayCandidates()));
         accommodationFinderForm.setDestinationLocation(bcfTravelLocationService.getLocationWithLocationType(Collections.singletonList(accommodationOfferingService.getAccommodationOffering(accommodationReference.getAccommodationOfferingCode()).getLocation()),
               LocationType.CITY).getCode());

         accommodationFinderForm.setModify(request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY)!=null && request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY).equals("true"));
         if(request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY_ROOM_REF)!=null)
         {
            accommodationFinderForm
                  .setModifyRoomRef(Integer.valueOf(request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY_ROOM_REF)));
         }

            final int maxPassengersAllowed = getConfigurationService().getConfiguration()
               .getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
         final int maxChildrenAllowed = getConfigurationService().getConfiguration()
               .getInt(BcfFacadesConstants.MAX_CHILD_GUEST_QUANTITY);
         model.addAttribute(BcfaccommodationsaddonWebConstants.MAX_PASSENGER_QUANTITY, maxPassengersAllowed);
         model.addAttribute(BcfaccommodationsaddonWebConstants.MAX_CHILD_QUANTITY, maxChildrenAllowed);

         model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);

         if(request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY)!=null && request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY).equals("true"))
         {

            model.addAttribute(DefaultBcfAccommodationFacadeHelper.MODIFY, true);
            model.addAttribute(DefaultBcfAccommodationFacadeHelper.MODIFY_ROOM_REF, request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY_ROOM_REF));


         }
         model.addAttribute("salesChannel", bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
         return getViewForPage(model);
      }
      catch (final ModelNotFoundException mnfEx)
      {
         GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
               "accommodation.offering.not.found.error.message");
         return REDIRECT_PREFIX + "/";
      }
   }

   private List<RoomStayCandidateData> filterRoomStayCandidates(final List<RoomStayCandidateData> roomStayCandidates)
   {


      final List<RoomStayCandidateData> filteredRoomStayCandidateDatas = new ArrayList<>();
      for (final RoomStayCandidateData roomStayCandidateData : roomStayCandidates)
      {
         final RoomStayCandidateData filteredRoomStayCandidateData = new RoomStayCandidateData();

         final List<PassengerTypeQuantityData> passengerTypeQuantityDatas =roomStayCandidateData.getPassengerTypeQuantityList().stream().filter(passengerType->Arrays.asList(hotelPassengers).contains(passengerType.getPassengerType().getCode())).collect(
               Collectors.toList());
         if(passengerTypeQuantityDatas.size()==1){
            passengerTypeQuantityDatas.add(bcfAccommodationFacadeHelper.getChildPassengerTypeQuantity());
         }
         filteredRoomStayCandidateData.setPassengerTypeQuantityList(passengerTypeQuantityDatas);
         filteredRoomStayCandidateData.setRoomStayCandidateRefNumber(roomStayCandidateData.getRoomStayCandidateRefNumber());
         filteredRoomStayCandidateData.setRatePlanCode(roomStayCandidateData.getRatePlanCode());
         filteredRoomStayCandidateData.setAccommodationCode(roomStayCandidateData.getAccommodationCode());
         filteredRoomStayCandidateData.setRoomStayCandidateRefNumber(roomStayCandidateData.getRoomStayCandidateRefNumber());
         filteredRoomStayCandidateDatas.add(filteredRoomStayCandidateData);



      }
      final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
            .getInt(BcfFacadesConstants.ALACARTE_MAX_ROOMS);
      for (int i = 0; i < maxAccommodationsQuantity; i++)
      {
         final int j=i;
         final boolean roomStayPresent = filteredRoomStayCandidateDatas.stream()
               .anyMatch(filteredRoomStayCandidateData -> filteredRoomStayCandidateData.getRoomStayCandidateRefNumber() == j);
         if(!roomStayPresent){
            final RoomStayCandidateData roomStayCandidateData = super.createRoomStayCandidate();
            roomStayCandidateData.setRoomStayCandidateRefNumber(i);
            roomStayCandidateData.setPassengerTypeQuantityList(roomStayCandidateData.getPassengerTypeQuantityList().stream().filter(passengerType-> Arrays
                  .asList(hotelPassengers).contains(passengerType.getPassengerType().getCode())).collect(
                  Collectors.toList()));

            filteredRoomStayCandidateDatas.add(roomStayCandidateData);
         }

      }

      return filteredRoomStayCandidateDatas;

   }



   private String populateQueryString(final String accommodationOfferingCode, final AccommodationAvailabilityForm accommodationAvailabilityForm,
         final HttpServletRequest request, final Model model, final String sessionBookingJourney)
   {
      if (sessionBookingJourney != null)
      {
         model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
         if (StringUtils
               .equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))

         {
            final String checkInDate = accommodationAvailabilityForm.getCheckInDateTime();
            final String checkOutDate = accommodationAvailabilityForm.getCheckOutDateTime();

            final String newQueryString = checkDatesAndGetNewQueryString(checkInDate, checkOutDate, request.getQueryString());
            if (StringUtils.isNotBlank(newQueryString))
            {
               return REDIRECT_PREFIX + ACCOMMODATION_DETAILS_ROOT + accommodationOfferingCode + "?" + newQueryString;
            }
         }
      }
      else
      {
         sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
               BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
         model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
               BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
      }
      return null;
   }

   private void createOrUpdateCanonicalUrl(final Model model,
         final PropertyData accommodationReference)
   {
		model.asMap().remove(BcfCoreConstants.CANONICAL_URL);
      final String[] pathParams = { accommodationReference.getAddress().getTown(),
            accommodationReference.getAccommodationOfferingName(),
            accommodationReference.getAccommodationOfferingCode() };

      final String relativeSiteUrl =
            getConfigurationService().getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL) + BcfFacadeUtil
                  .getRelativeSiteUrl(BcfFacadesConstants.PACKAGE_HOTEL_DETAILS_PAGE_PATH, Collections.emptyMap(), pathParams);
      model.addAttribute(BcfCoreConstants.CANONICAL_URL, StringUtils.replace(relativeSiteUrl, " ", "-"));
   }

   private boolean isAmendmentJourney(final HttpServletRequest request)
   {
      return StringUtils
            .containsIgnoreCase(request.getRequestURL().toString(), BcfstorefrontaddonWebConstants.MANAGE_BOOKING_URL);
   }

   /**
    * Redirects user to the next checkout page which is guest details (or checkout login)
    *
    * @return next page
    */
   @RequestMapping(value = "/next", method = RequestMethod.GET)
   public String nextPage()
   {
      String nextPageurl=BcfstorefrontaddonWebConstants.BasketSummaryPage;
      final String salesChannel = bcfSalesApplicationResolverFacade.getCurrentSalesChannel();

      final Object sessionJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

      if(salesChannel!=null && sessionJourney!=null){

         nextPageurl= salesChannelBookingJourneySummaryUrlsMap.get(salesChannel+BcfstorefrontaddonWebConstants.HYPHEN+sessionJourney);
      }

      if (userFacade.isAnonymousUser() && !travelCartFacade.isAmendmentCart())
      {
         return REDIRECT_PREFIX + nextPageurl;
      }
      if (travelCartFacade.isAmendmentCart())
      {
         if (!travelCartFacade.hasCartBeenAmended())
         {
            return REDIRECT_PREFIX + BOOKING_DETAILS_URL + travelCartFacade.getOriginalOrderCode();
         }
         if (userFacade.isAnonymousUser())
         {
            getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
         }
      }
      final String sessionBookingJourney = getSessionService()
            .getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
      if (StringUtils.isNotEmpty(sessionBookingJourney) && StringUtils
            .equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))
      {
         return REDIRECT_PREFIX + nextPageurl;
      }
      return REDIRECT_PREFIX +nextPageurl;
   }

   /**
    * Validates if the cart contains any AccommodationOrderEntryGroups
    *
    * @return
    */
   @RequestMapping(value = "/validate-cart", method = RequestMethod.GET, produces = "application/json")
   public String validateCart(final Model model)
   {
      model.addAttribute(BcfaccommodationsaddonWebConstants.IS_VALID, accommodationCartFacade.validateAccommodationCart());
      return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.ValidateAccommodationCartResponse;
   }

   @RequestMapping("/customer-review")
   public String getPagedCustomerReviewData(
         @RequestParam(value = "accommodationOfferingCode", required = true) final String accommodationOfferingCode,
         @RequestParam(value = "pageNumber", required = false, defaultValue = "0") final int pageNumber, final Model model)
   {
      final SearchPageData<ReviewData> customerReviewsSearchPageData = getPagedAccommodationOfferingCustomerReviews(
            accommodationOfferingCode, pageNumber);

      model.addAttribute(BcfaccommodationsaddonWebConstants.CUSTOMER_REVIEW_SEARCH_PAGE_DATA, customerReviewsSearchPageData);

      return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.CustomerReviewPagedJsonResponse;
   }

   protected void populateBestCombinationsForAccommodation(final String accommodationOfferingCode,
         final AccommodationAvailabilityForm accommodationAvailabilityForm, final Model model, final HttpServletRequest request)
   {
      final AccommodationSearchRequestData accommodationSearchRequestData = prepareAccommodationSearchRequestData(
            accommodationOfferingCode, accommodationAvailabilityForm.getCheckInDateTime(),
            accommodationAvailabilityForm.getCheckOutDateTime(), accommodationAvailabilityForm.getRoomStayCandidates());
      final AccommodationSearchResponseData accommodationSearchResponseData = accommodationSearchFacade
            .doSearch(accommodationSearchRequestData);

      if (CollectionUtils.isEmpty(accommodationSearchResponseData.getProperties()))
      {
         return;
      }

      final PropertyData bestCombinationProperty = accommodationSearchResponseData.getProperties().get(0);

      if (CollectionUtils.isEmpty(bestCombinationProperty.getRatePlanConfigs()))
      {
         return;
      }

      final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData =
            createAccommodationAvailabilityRequestData(
                  accommodationSearchResponseData,accommodationAvailabilityForm);
      if (accommodationAvailabilityRequestData == null)
      {
         return;
      }
      final AccommodationAvailabilityResponseData accommodationAvailabilityResponse = getAccommodationOfferingFacade()
            .getSelectedAccommodationOfferingDetails(accommodationAvailabilityRequestData);
      final boolean isAccommodationAvailableForQuickSelection = getAccommodationOfferingFacade()
            .isAccommodationAvailableForQuickSelection(accommodationAvailabilityResponse);
      if (!isAccommodationAvailableForQuickSelection || accommodationAvailabilityResponse.getRoomStays()
            .stream().anyMatch(roomStay -> roomStay.getRatePlans().stream().anyMatch(ratePlan -> ratePlan
                  .getAvailableQuantity() < accommodationAvailabilityRequestData.getCriterion().getRoomStayCandidates().size())))
      {
         return;
      }
      final List<RoomStayGroupData> roomStayGroupDatas = createRoomStayGroupDatas(accommodationAvailabilityResponse);

      if (CollectionUtils.isEmpty(roomStayGroupDatas))
      {
         return;
      }
      model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_ROOM_STAY_GROUP_LIST, roomStayGroupDatas);

      final StringBuilder urlParameters = new StringBuilder();
      urlParameters.append(BcfaccommodationsaddonWebConstants.ACCOMMODATION_DETAILS_ROOT_URL);
      urlParameters.append(BcfstorefrontaddonWebConstants.BACK_SLASH);
      urlParameters.append(accommodationOfferingCode);
      urlParameters.append(BcfstorefrontaddonWebConstants.QUESTION_MARK);
      urlParameters.append(request.getQueryString());
      model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_BEST_COMBINATION_AVAILABILITY_RESPONSE,
            accommodationAvailabilityResponse);
      model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_BEST_COMBINATION_PRICE_DATA,
            getTotalPriceForBestOfferings(accommodationAvailabilityResponse));
      model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_ADD_TO_CART_BOOKING_FORM,
            new AccommodationAddToCartBookingForm());
      model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_DETAILS_PAGE_URL_DATA, urlParameters.toString());

   }

   protected PriceData getTotalPriceForBestOfferings(
         final AccommodationAvailabilityResponseData accommodationAvailabilityResponse)
   {
      BigDecimal totalPrice = BigDecimal.valueOf(0);
      for (final RoomStayData roomStay : accommodationAvailabilityResponse.getRoomStays())
      {
         final ReservedRoomStayData reservedRoomStay = (ReservedRoomStayData) roomStay;
         totalPrice = totalPrice.add(reservedRoomStay.getTotalRate().getActualRate().getValue());
      }
      final String currencyIso = accommodationAvailabilityResponse.getRoomStays().get(0).getFromPrice().getCurrencyIso();

      return priceDataFactory.create(PriceDataType.BUY, totalPrice, currencyIso);
   }

   protected AccommodationAvailabilityRequestData createAccommodationAvailabilityRequestData(
         final AccommodationAvailabilityForm accommodationAvailabilityForm, final String accommodationOfferingCode)
   {
      final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData = new
            AccommodationAvailabilityRequestData();
      final CriterionData criterion = new CriterionData();
      final StayDateRangeData stayDateRange = new StayDateRangeData();
      stayDateRange.setStartTime(TravelDateUtils
            .convertStringDateToDate(accommodationAvailabilityForm.getCheckInDateTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
      stayDateRange.setEndTime(TravelDateUtils
            .convertStringDateToDate(accommodationAvailabilityForm.getCheckOutDateTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
      stayDateRange
            .setLengthOfStay((int) TravelDateUtils.getDaysBetweenDates(stayDateRange.getStartTime(), stayDateRange.getEndTime()));
      criterion.setStayDateRange(stayDateRange);
      criterion.setRoomStayCandidates(accommodationAvailabilityForm.getRoomStayCandidates());
      final PropertyData accommodationReferenceData = new PropertyData();
      accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);
      accommodationReferenceData.setPropertyAccomodationCodes(
            bcfAccommodationOfferingFacade.getAccommodationsLinkedToAccommodationOffering(accommodationOfferingCode));
      criterion.setAccommodationReference(accommodationReferenceData);
      accommodationAvailabilityRequestData.setCriterion(criterion);

      final List<ReservedRoomStayData> reservedRoomStays = bookingFacade.getNewReservedRoomStays();
      if ((CollectionUtils.isNotEmpty(reservedRoomStays))
            && (DateUtils.isSameDay(reservedRoomStays.get(0).getCheckInDate(), stayDateRange.getStartTime())
            && DateUtils.isSameDay(reservedRoomStays.get(0).getCheckOutDate(), stayDateRange.getEndTime())))
      {
         accommodationAvailabilityRequestData.setReservedRoomStays(reservedRoomStays);
      }

      return accommodationAvailabilityRequestData;
   }

   protected AccommodationAvailabilityRequestData createAccommodationAvailabilityRequestData(
         final AccommodationReservationData accommodationReservationData, final String checkInDate, final String checkOutDate)
   {
      final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData = new
            AccommodationAvailabilityRequestData();
      final CriterionData criterion = new CriterionData();
      criterion.setAccommodationReference(accommodationReservationData.getAccommodationReference());
      final StayDateRangeData stayDateRange = new StayDateRangeData();
      stayDateRange.setStartTime(TravelDateUtils.convertStringDateToDate(checkInDate, TravelservicesConstants.DATE_PATTERN));
      stayDateRange.setEndTime(TravelDateUtils.convertStringDateToDate(checkOutDate, TravelservicesConstants.DATE_PATTERN));
      criterion.setStayDateRange(stayDateRange);

      final List<RoomStayCandidateData> roomStayCandidateDatas = new ArrayList<>(
            accommodationReservationData.getRoomStays().size());
      accommodationReservationData.getRoomStays().forEach(roomStay ->
      {
         final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
         roomStayCandidateData.setRatePlanCode(roomStay.getRatePlans().get(0).getCode());
         roomStayCandidateData.setAccommodationCode(roomStay.getRoomTypes().get(0).getCode());
         roomStayCandidateData.setPassengerTypeQuantityList(roomStay.getGuestCounts());
         roomStayCandidateData.setRoomStayCandidateRefNumber(roomStay.getRoomStayRefNumber());
         roomStayCandidateData.setServices(roomStay.getServices());
         roomStayCandidateDatas.add(roomStayCandidateData);
      });
      criterion.setRoomStayCandidates(roomStayCandidateDatas);
      accommodationAvailabilityRequestData.setCriterion(criterion);

      return accommodationAvailabilityRequestData;
   }

   protected AccommodationAvailabilityRequestData createAccommodationAvailabilityRequestData(
         final AccommodationSearchResponseData accommodationSearchResponseData, final AccommodationAvailabilityForm accommodationAvailabilityForm)
   {
      final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData = new
            AccommodationAvailabilityRequestData();
      final CriterionData criterion = new CriterionData();
      final PropertyData propertyData = accommodationSearchResponseData.getCriterion().getAccommodationReference();
      propertyData.setPropertyAccomodationCodes(bcfAccommodationOfferingFacade
            .getAccommodationsLinkedToAccommodationOffering(propertyData.getAccommodationOfferingCode()));
      criterion.setAccommodationReference(accommodationSearchResponseData.getCriterion().getAccommodationReference());
      criterion.setStayDateRange(accommodationSearchResponseData.getCriterion().getStayDateRange());

      final List<RoomStayCandidateData> roomStayCandidateDatas = new ArrayList<>();

      for (final PropertyData property : accommodationSearchResponseData.getProperties())
      {
         int refNum = 0;
         for (int j=0;j<property.getRatePlanConfigs().size();j++)
         {
            final String ratePlanConfig =property.getRatePlanConfigs().get(j);
            final String[] ratePlanConfigSplit = ratePlanConfig.split("\\|", 3);

            final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
            roomStayCandidateData.setRatePlanCode(ratePlanConfigSplit[0]);
            roomStayCandidateData.setAccommodationCode(ratePlanConfigSplit[1]);
            roomStayCandidateData.setRoomStayCandidateRefNumber(refNum++);
            if(accommodationAvailabilityForm.getRoomStayCandidates().size()>j){
               roomStayCandidateData.setPassengerTypeQuantityList(accommodationAvailabilityForm.getRoomStayCandidates().get(j).getPassengerTypeQuantityList());
            }

            try
            {
               for (int i = 0; i < Integer.parseInt(ratePlanConfigSplit[2]); i++)
               {
                  roomStayCandidateDatas.add(roomStayCandidateData);
               }
            }
            catch (final NumberFormatException ex)
            {
               LOG.error("Unable to parse quantity of rateplan config with code :\t" + ratePlanConfig);
               return null;
            }
         }
      }
      criterion.setRoomStayCandidates(roomStayCandidateDatas);
      accommodationAvailabilityRequestData.setCriterion(criterion);

      return accommodationAvailabilityRequestData;
   }

   protected String validateUpdateAccommodationBookingDates(final String reservationCode, final String checkInDate,
         final String checkOutDate, final AccommodationReservationData accommodationReservationData)
   {
      if (accommodationReservationData == null)
      {
         return ERROR_AMEND_BOOKING_ORDER_ID;
      }

      if (StringUtils.isEmpty(reservationCode) || StringUtils.isEmpty(checkInDate))
      {
         return ERROR_AMEND_BOOKING_EMPTY;
      }
      final Date dCheckInDate = TravelDateUtils.convertStringDateToDate(checkInDate, TravelservicesConstants.DATE_PATTERN);
      final Date dCheckOutDate = TravelDateUtils.convertStringDateToDate(checkOutDate, TravelservicesConstants.DATE_PATTERN);
      final Date orderCheckInDate = accommodationReservationData.getRoomStays().get(0).getCheckInDate();
      final Date orderCheckOutDate = accommodationReservationData.getRoomStays().get(0).getCheckOutDate();
      final long maxAllowedDateDifference = getConfigurationService().getConfiguration()
            .getInt(BcfstorefrontaddonWebConstants.MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE);
      if (dCheckInDate == null || dCheckOutDate == null || (orderCheckInDate.compareTo(dCheckInDate) == 0
            && orderCheckOutDate.compareTo(dCheckOutDate) == 0) || dCheckInDate.compareTo(dCheckOutDate) > 0
            || TravelDateUtils.getDaysBetweenDates(dCheckInDate, dCheckOutDate) > maxAllowedDateDifference)
      {
         return ERROR_AMEND_BOOKING_DATES;
      }

      return StringUtils.EMPTY;
   }

   protected BigDecimal getTotalPriceForBooking(final AccommodationAvailabilityResponseData accommodationAvailabilityResponse)
   {
      BigDecimal totalPayablePrice = BigDecimal.valueOf(0);
      for (final RoomStayData roomStayData : accommodationAvailabilityResponse.getRoomStays())
      {
         final ReservedRoomStayData reservedRoomStayData = (ReservedRoomStayData) roomStayData;
         if (reservedRoomStayData.getTotalRate() != null)
         {
            totalPayablePrice = totalPayablePrice.add(reservedRoomStayData.getTotalRate().getActualRate().getValue());
         }
      }
      return totalPayablePrice;
   }

   protected List<RoomStayGroupData> createRoomStayGroupDatas(
         final AccommodationAvailabilityResponseData accommodationAvailabilityResponse)
   {
      if (accommodationAvailabilityResponse == null || CollectionUtils.isEmpty(accommodationAvailabilityResponse.getRoomStays()))
      {
         return Collections.emptyList();
      }

      final List<RoomStayGroupData> roomStayGroupDatas = new ArrayList<>(accommodationAvailabilityResponse.getRoomStays().size
            ());

      for (final RoomStayData roomStayData : accommodationAvailabilityResponse.getRoomStays())
      {
         final ReservedRoomStayData reservedRoomStayData = (ReservedRoomStayData) roomStayData;
         if (CollectionUtils.isEmpty(reservedRoomStayData.getRatePlans()) || CollectionUtils
               .isEmpty(reservedRoomStayData.getRoomTypes()))
         {
            return Collections.emptyList();
         }
         final String accommodationCode = reservedRoomStayData.getRoomTypes().get(0).getCode();
         final String ratePlanCode = reservedRoomStayData.getRatePlans().get(0).getCode();

         populateRoomStayData(roomStayGroupDatas, reservedRoomStayData, accommodationCode, ratePlanCode);
      }
      return roomStayGroupDatas;
   }

   private void populateRoomStayData(final List<RoomStayGroupData> roomStayGroupDatas,
         final ReservedRoomStayData reservedRoomStayData, final String accommodationCode, final String ratePlanCode)
   {
      if (CollectionUtils.isEmpty(roomStayGroupDatas))
      {
         roomStayGroupDatas.add(createRoomStayGroupData(accommodationCode, ratePlanCode));
         reservedRoomStayData.setGroupIndex(0);
      }
      else
      {
         boolean isSuccess = false;
         for (final RoomStayGroupData roomStayGroupData : roomStayGroupDatas)
         {
            if (StringUtils.equals(roomStayGroupData.getAccommodationCode(), accommodationCode) && StringUtils
                  .equals(roomStayGroupData.getRatePlanCode(), ratePlanCode))
            {
               roomStayGroupData.setQuantity(roomStayGroupData.getQuantity() + 1);
               isSuccess = true;
            }
         }

         if (!isSuccess)
         {
            final RoomStayGroupData roomStayGroupData = createRoomStayGroupData(accommodationCode, ratePlanCode);
            roomStayGroupDatas.add(roomStayGroupData);
            reservedRoomStayData.setGroupIndex(roomStayGroupDatas.indexOf(roomStayGroupData));
         }

      }
   }

   protected RoomStayGroupData createRoomStayGroupData(final String accommodationCode, final String ratePlanCode)
   {
      final RoomStayGroupData roomStayGroupData = new RoomStayGroupData();
      roomStayGroupData.setRatePlanCode(ratePlanCode);
      roomStayGroupData.setAccommodationCode(accommodationCode);
      roomStayGroupData.setQuantity(1);

      return roomStayGroupData;
   }

   protected void disableCachingForResponse(final HttpServletResponse response)
   {
      response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
      response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
   }
}
