/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.cms;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.custom.propertysource.message.code.resolver.CustomMessageCodesResolver;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.impl.DefaultBcfAccommodationFacadeHelper;
import com.bcf.facades.location.BcfTravelLocationFacade;


/**
 * Controller for Accommodation Finder Component
 */
@Controller("AccommodationFinderComponentController")
@RequestMapping(value = BcfaccommodationsaddonControllerConstants.Actions.Cms.AccommodationFinderComponent)
public class AccommodationFinderComponentController extends AbstractFinderComponentController
{
	@Resource(name = "accommodationFinderValidator")
	private AbstractTravelValidator accommodationFinderValidator;

	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Resource(name = "bcfTravelLocationService")
	private BcfTravelLocationService bcfTravelLocationService;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "messageCodesResolver")
	private CustomMessageCodesResolver customMessageCodesResolver;

	protected static final String SEARCH = "/search";

	private static final String ROOM = "r";
	private static final int DEFAULT_ACCOMMODATION_QUANTITY = 1;

	private static final String SHOW_ACC_DESTINATION = "showAccommodationDestination";
	private static final String SHOW_CHECKIN_CHECKOUT = "showCheckInCheckOut";
	private static final String SHOW_COMPONENT = "showComponent";
	private static final String BCF_VACATION_LOCATIONS = "bcfVacationLocations";

	private static final Logger LOG = Logger.getLogger(AccommodationFinderComponentController.class);


	@Override
	@ModelAttribute(SHOW_COMPONENT)
	public boolean setCollapseExpandFlag()
	{
		return Objects
				.isNull(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES));
	}

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{
		AccommodationFinderForm accommodationFinderForm = null;

		if (model.containsAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM))
		{
			accommodationFinderForm = (AccommodationFinderForm) model.asMap()
					.get(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		}

		if (accommodationFinderForm == null
				&& request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) != null)
		{
			accommodationFinderForm = (AccommodationFinderForm) request
					.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		}

		if (accommodationFinderForm == null || (Objects.nonNull(accommodationFinderForm) && CollectionUtils.isEmpty(accommodationFinderForm.getRoomStayCandidates())))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
					initializeAccommodationFinderForm(accommodationFinderForm));
		}
		else
		{
			if (!model.containsAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM)
					&& request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) != null)
			{
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_FINDER_FORM_BINDING_RESULT,
						request.getAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_FINDER_FORM_BINDING_RESULT));

				accommodationFinderForm = (AccommodationFinderForm) request
						.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);

				model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
			}
		}

		model.addAttribute(SHOW_ACC_DESTINATION, Boolean.TRUE);
		model.addAttribute(SHOW_CHECKIN_CHECKOUT, Boolean.TRUE);
		model.addAttribute(BCF_VACATION_LOCATIONS, bcfTravelLocationFacade.getGeoAreaVacationLocationsMap());

	}


	protected AccommodationFinderForm initializeAccommodationFinderForm(AccommodationFinderForm accommodationFinderForm)
	{
		if(Objects.isNull(accommodationFinderForm))
		{
			accommodationFinderForm = new AccommodationFinderForm();
		}
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 0; i < maxAccommodationsQuantity; i++)
		{
			final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
			roomStayCandidateData.setRoomStayCandidateRefNumber(i);
			roomStayCandidates.add(roomStayCandidateData);
		}
		accommodationFinderForm.setRoomStayCandidates(roomStayCandidates);
		accommodationFinderForm.setNumberOfRooms(DEFAULT_ACCOMMODATION_QUANTITY);
		return accommodationFinderForm;
	}

	@InitBinder(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM)
	private void initBinder(final WebDataBinder binder)
	{
		binder.setValidator(accommodationFinderValidator);
	}

	@RequestMapping(value = SEARCH, method = RequestMethod.POST)
	public String performSearch(final AccommodationFinderForm accommodationFinderForm, final RedirectAttributes redirectModel,
			final BindingResult bindingResult)
	{
		initializeForms(accommodationFinderForm);
		((BeanPropertyBindingResult) bindingResult).setMessageCodesResolver(customMessageCodesResolver);
		validateForm(accommodationFinderValidator, accommodationFinderForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
			redirectModel
					.addFlashAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + "/?hotel=true";
		}

		if (Objects.isNull(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)) || Objects
				.isNull(getSessionService().getAttribute(
				BcfFacadesConstants.SELECTED_JOURNEY_REF_NO)))
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
		}
		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);


		final String urlParameters = buildUrlParameters(accommodationFinderForm);
		final String redirectUrl = createRedirectUrl(accommodationFinderForm);

		return urlParameters.isEmpty() ? redirectUrl : redirectUrl + urlParameters;
	}

	protected void initializeForms(final AccommodationFinderForm accommodationFinderForm)
	{
		if (StringUtils.isNotEmpty(accommodationFinderForm.getDestinationLocation()))
		{
			final LocationModel location = bcfTravelLocationService.getLocation(accommodationFinderForm.getDestinationLocation());
			accommodationFinderForm.setDestinationLocationName(location.getName());
		}
	}

	private String createRedirectUrl(final AccommodationFinderForm accommodationFinderForm)
	{
		final String redirectUrl;
		if (StringUtils.equalsIgnoreCase(accommodationFinderForm.getSuggestionType(), SuggestionType.PROPERTY.toString()))
		{
			redirectUrl = REDIRECT_PREFIX + BcfaccommodationsaddonWebConstants.ACCOMMODATION_DETAILS_ROOT_URL + "/"
					+ accommodationFinderForm.getDestinationLocation();
		}
		else
		{
			redirectUrl = REDIRECT_PREFIX + BcfaccommodationsaddonWebConstants.ACCOMMODATION_SELECTION_ROOT_URL;
		}
		return redirectUrl;
	}


	@RequestMapping(value = "/validate-accommodation-finder-form", method = RequestMethod.POST)
	public String validateAccommodationFinderForm(@Valid final AccommodationFinderForm accommodationFinderForm,
			final BindingResult bindingResult, final Model model)
	{
		validateForm(accommodationFinderValidator, accommodationFinderForm, bindingResult);

		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfaccommodationsaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			model.addAttribute(BcfaccommodationsaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.FormErrors.FormErrorsResponse;
	}

	private void validateForm(final AbstractTravelValidator accommodationFinderValidator,
			final AccommodationFinderForm accommodationFinderForm, final BindingResult bindingResult)
	{
		accommodationFinderValidator.setTargetForm(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.setAttributePrefix("");
		accommodationFinderValidator.validate(accommodationFinderForm, bindingResult);
	}

	protected String buildUrlParameters(final AccommodationFinderForm accommodationFinderForm)
	{
		final StringBuilder urlParameters = new StringBuilder();

		urlParameters.append(BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		// We need to encode '|' character because it is not allowed in Spring Security 4
		String encodedDestinationLocation = accommodationFinderForm.getDestinationLocation();
		encodedDestinationLocation = encodedDestinationLocation.replaceAll("\\|", "%7C");
		urlParameters.append(encodedDestinationLocation);
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		urlParameters.append(BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION_NAME);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getDestinationLocationName());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(BcfaccommodationsaddonWebConstants.SUGGESTION_TYPE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getSuggestionType());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getCheckInDateTime());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getCheckOutDateTime());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getNumberOfRooms());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);


		if(accommodationFinderForm.isModify())
		{

			urlParameters.append(DefaultBcfAccommodationFacadeHelper.MODIFY);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			urlParameters.append("true");
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
			urlParameters.append(DefaultBcfAccommodationFacadeHelper.MODIFY_ROOM_REF);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			urlParameters.append(accommodationFinderForm.getModifyRoomRef());
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		}



		try
		{
			final int numberOfRooms = accommodationFinderForm.getNumberOfRooms();
			for (int i = 0; i < numberOfRooms; i++)
			{
				final StringBuilder guestsStringPerRoom = new StringBuilder();
				final List<PassengerTypeQuantityData> guestCounts = accommodationFinderForm.getRoomStayCandidates().get(i)
						.getPassengerTypeQuantityList();
				for (int j = 0; j < guestCounts.size(); j++)
				{
					final String passengerType = guestCounts.get(j).getPassengerType().getCode();
					final int passengerQuantity = guestCounts.get(j).getQuantity();
					StringBuilder ages=new StringBuilder();
					if(CollectionUtils.isNotEmpty(guestCounts.get(j).getChildAges())){
                  for(int age: guestCounts.get(j).getChildAges()){
							ages.append(age).append("-");
						}
						ages.deleteCharAt(ages.length() - 1);
					}

					StringBuilder guestParam=new StringBuilder( String.valueOf(passengerQuantity)).append("-").append(passengerType);
					if(ages.length()>0){
						guestParam.append("-").append(ages.toString());
					}
					guestsStringPerRoom.append(guestParam.toString());
					guestsStringPerRoom.append(",");
				}
				String result = guestsStringPerRoom.toString();
				result = result.substring(0, result.length() - 1);

				urlParameters.append(ROOM + i);
				urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
				urlParameters.append(result);
				urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

			}
		}
		catch (final NumberFormatException e)
		{
			LOG.error("Cannot parse number of rooms string to integer");
			LOG.error(e.getClass().getName() + " : " + e.getMessage());
			LOG.debug(e);
			return REDIRECT_PREFIX + "/";
		}
		String urlParametersString = urlParameters.toString();
		urlParametersString = urlParametersString.substring(0, urlParametersString.length() - 1);
		return "?" + urlParametersString;
	}

	@Override
	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<String> codes = Arrays.asList(BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_ADULT,
				BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_CHILD);
		final List<PassengerTypeData> sortedPassengerTypes = getTravellerSortStrategy()
				.sortPassengerTypes(getPassengerTypeFacade().getPassengerTypesForCodes(codes));
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}

}
