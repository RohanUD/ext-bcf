/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.RoomRateCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.facades.accommodation.RoomPreferenceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.forms.AccommodationAddToCartForm;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.order.CommerceBundleCartModificationException;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.AccommodationAddToCartBookingForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AddExtraToCartForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.BcfAccommodationAddToCartForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationAddToCartValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.BcfAccommodationCartFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller providing functionality of adding accommodation specific products to cart
 */
@Controller
public class AccommodationAddToCartController extends AbstractController
{
	private static final String ADD_TO_CART_RESPONSE = "addToCartResponse";
	private static final String AMEND_CART_ERROR = "accommodation.booking.details.page.request.cart.error";
	private static final String NEXT_PAGE_REDIRECT_URL = "/accommodation-details/next";
	private static final String ERROR_ADD_ROOM_ROOM_REFERENCE_CODE = "error.page.bookingdetails.add.room.roomRefCode";
	private static final String ERROR_ADD_ROOM_QUANTITY = "error.page.bookingdetails.add.room.quantity";
	private static final String REMOVE_ROOM_FAILED = "accommodation.remove.room.error";
	private static final Logger LOG = Logger.getLogger(AccommodationAddToCartController.class);
	private static final String ADD_BUNDLE_TO_CART_RESPONSE = "addBundleToCartResponse";
	private static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";
	private static final String MAKE_BOOKING_ERROR = "makebooking.errorcode.";

	@Resource(name = "accommodationCartFacade")
	private BcfAccommodationCartFacade accommodationCartFacade;

	@Resource(name = "roomPreferenceFacade")
	RoomPreferenceFacade roomPreferenceFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "accommodationAddToCartValidator")
	private AccommodationAddToCartValidator accommodationAddToCartValidator;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;


	@RequestMapping(value = "/cart/accommodation/add", method = RequestMethod.POST, produces = "application/json")
	public String addAccommodationToCart(@Valid final BcfAccommodationAddToCartForm form, final Model model)
	{
		final List<RoomRateCartData> rates = accommodationCartFacade.collectRoomRates(form);

		if (CollectionUtils.isEmpty(rates))
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
			return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;
		}

		if (!bcfTravelCartFacadeHelper.isAlacateFlow())
		{
			accommodationCartFacade.cleanUpCartBeforeAddition(form.getAccommodationOfferingCode(), form.getCheckInDateTime(),
					form.getCheckOutDateTime());
		}


		try
		{
			final Date checkInDate = TravelDateUtils.convertStringDateToDate(form.getCheckInDateTime(),
					BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
			final Date checkOutDate = TravelDateUtils.convertStringDateToDate(form.getCheckOutDateTime(),
					BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
			accommodationCartFacade.addAccommodationsToCart(checkInDate, checkOutDate, form.getAccommodationOfferingCode(),
					form.getAccommodationCode(), rates, form.getNumberOfRooms(), form.getRatePlanCode(),form.getRoomStayCandidates(),form.getModifyRoomRef(),form.isChange());
			bcfTravelCartFacade.recalculateCart();
			model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(true, null));
		}
		catch (final CommerceCartModificationException |CalculationException e)
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;
	}

	@RequestMapping(value = "/cart/accommodation/package-add", method = RequestMethod.POST, produces = "application/json")
	public String addPackageAccommodationToCart(@Valid final BcfAccommodationAddToCartForm form, final BindingResult bindingResult,
			final Model model)
	{
			accommodationAddToCartValidator.validate(form, bindingResult);

		if (bindingResult.hasErrors())
		{
			return getAddToCartResponse(Boolean.FALSE, BASKET_ERROR_OCCURRED, model);
		}

		return addAccommodationAndTransportBundleToPackage(form, bindingResult, model);

	}

	@RequestMapping(value = "/cart/accommodation/amend-package-add", method = RequestMethod.POST, produces = "application/json")
	public String addAmendPackageAccommodationToCart(@Valid final BcfAccommodationAddToCartForm form,
			final BindingResult bindingResult, final Model model)
	{
		accommodationAddToCartValidator.validate(form, bindingResult);

		if (bindingResult.hasErrors() || !bcfTravelCartFacade.isCurrentCartValid())
		{
			return getAddToCartResponse(Boolean.FALSE, ERROR_ADD_ROOM_ROOM_REFERENCE_CODE, model);
		}

		final List<Integer> oldAccommodationOrderEntryGroupRefs = bookingFacade.getOldAccommodationOrderEntryGroupRefs();

		final boolean isValid = CollectionUtils.isNotEmpty(oldAccommodationOrderEntryGroupRefs)
				&& !oldAccommodationOrderEntryGroupRefs.contains(form.getRoomStayRefNumber());

		if (!isValid)
		{
			return getAddToCartResponse(isValid, ERROR_ADD_ROOM_ROOM_REFERENCE_CODE, model);
		}

		final AccommodationReservationData accommodationReservationData = reservationFacade
				.getCurrentAccommodationReservationSummary();

		final String orderCheckInDate = TravelDateUtils.convertDateToStringDate(
				accommodationReservationData.getRoomStays().get(0).getCheckInDate(), TravelservicesConstants.DATE_PATTERN);
		final String orderCheckOutDate = TravelDateUtils.convertDateToStringDate(
				accommodationReservationData.getRoomStays().get(0).getCheckOutDate(), TravelservicesConstants.DATE_PATTERN);

		if (StringUtils.isEmpty(form.getCheckInDateTime()) || StringUtils.isEmpty(form.getCheckOutDateTime())
				|| !StringUtils.equals(form.getCheckInDateTime(), orderCheckInDate)
				|| !StringUtils.equals(form.getCheckOutDateTime(), orderCheckOutDate))
		{
			return getAddToCartResponse(Boolean.FALSE, ERROR_ADD_ROOM_ROOM_REFERENCE_CODE, model);
		}

		return addAccommodationAndTransportBundleToPackage(form, bindingResult, model);

	}

	protected String addAccommodationToPackage(final BcfAccommodationAddToCartForm form,
			final BindingResult bindingResult,
			final Model model) throws CalculationException, CommerceCartModificationException
	{

		Integer journeyRefNumber = sessionService.getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		journeyRefNumber = Objects.isNull(journeyRefNumber) ? 0 : journeyRefNumber;

		final boolean isModifyingSameAccommodation = accommodationCartFacade
				.isModifyingSameAccommodation(form.getAccommodationOfferingCode(), form.getAccommodationCode(),
						form.isOnlyAccommodationChange(), journeyRefNumber);

		if (isModifyingSameAccommodation)
		{

			return getAddToCartResponse(Boolean.TRUE, null, model);


		}
		final int numberOfRoomsInBooking = (bcfTravelCartFacade.isAmendmentCart() || form.isChange())?CollectionUtils.size(accommodationCartFacade.getAccommodationOrderEntryGroupRefsForOtherJourneys(journeyRefNumber)):CollectionUtils.size(bookingFacade.getAccommodationOrderEntryGroupRefs());
		final int maxAccommodationsQuantity = configurationService.getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

		if (maxAccommodationsQuantity < (numberOfRoomsInBooking + form.getNumberOfRooms()))
		{
			return getAddToCartResponse(Boolean.FALSE, "accommodation.add.to.cart.quantity.exceeded.allowed.amount", model);
		}

		final List<RoomRateCartData> rates = accommodationCartFacade.collectRoomRates(form);

		if (CollectionUtils.isEmpty(rates))
		{
			throw new CommerceCartModificationException(BASKET_ERROR_OCCURRED);

		}

		final Date checkInDate = TravelDateUtils.convertStringDateToDate(form.getCheckInDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final Date checkOutDate = TravelDateUtils.convertStringDateToDate(form.getCheckOutDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		accommodationCartFacade.addAccommodationsToCartForPromotions(checkInDate, checkOutDate, form.getAccommodationOfferingCode(),
				form.getAccommodationCode(), rates, form.getNumberOfRooms(), form.getRatePlanCode(), form.getPromotionCode(),
				form.getRoomStayCandidates(), form.isChange());

		sessionService.removeAttribute("fareSelectionData");

		return getAddToCartResponse(Boolean.TRUE, null, model);
	}

	protected String addAccommodationAndTransportBundleToPackage(final BcfAccommodationAddToCartForm form,
			final BindingResult bindingResult,
			final Model model)
	{

		final Transaction currentTransaction = Transaction.current();
		boolean executed = false;

		try
		{
			currentTransaction.begin();

			if (!(form.isChange() || form.isOnlyAccommodationChange()))
			{
				sessionService.removeAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY);

				String addBundleToCartErrorMessage = addBundleToCart(0);

				if (org.apache.commons.lang3.StringUtils.isEmpty(addBundleToCartErrorMessage))
				{
					addBundleToCartErrorMessage = addBundleToCart(1);

					if (org.apache.commons.lang3.StringUtils.isNotEmpty(addBundleToCartErrorMessage))
					{
						return getAddToCartResponse(Boolean.FALSE, BASKET_ERROR_OCCURRED, model);
					}
				}
				else
				{
					return getAddToCartResponse(Boolean.FALSE, BASKET_ERROR_OCCURRED, model);
				}
			}

			final String response = addAccommodationToPackage(form, bindingResult, model);
			executed = true;

			return response;

		}
		catch (final IntegrationException e)
		{
			return getAddToCartResponse(Boolean.FALSE, bcfTravelCartFacadeHelper
					.getErrorMessage(MAKE_BOOKING_ERROR, e.getErorrListDTO().getErrorDto().get(0).getErrorCode(),
							e.getErorrListDTO().getErrorDto().get(0).getErrorDetail()), model);

		}
		catch (final CommerceBundleCartModificationException | UnknownIdentifierException ex)
		{
			LOG.info(ex.getMessage(), ex);

			return getAddToCartResponse(Boolean.FALSE, BASKET_ERROR_OCCURRED, model);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			return getAddToCartResponse(Boolean.FALSE, BASKET_ERROR_OCCURRED, model);
		}

		finally
		{
			if (executed)
			{
				currentTransaction.commit();
			}
			else
			{
				currentTransaction.rollback();
			}
		}


	}


	protected String addBundleToCart(final int originDestinationRef)
			throws CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException
	{

		final AddBundleToCartRequestData addBundleToCartRequestData = bcfTravelCartFacadeHelper
				.getAddBundleToCartRequestData(originDestinationRef);


		final String addBundleToCartErrorMessage = bcfTravelCartFacadeHelper.addBundleToCart(addBundleToCartRequestData,
				true, false);
		bcfTravelCartFacadeHelper
				.manageOriginDestTravellerSessionMap(Integer.valueOf(addBundleToCartRequestData.getSelectedOdRefNumber()));

		if (org.apache.commons.lang3.StringUtils.isEmpty(addBundleToCartErrorMessage))
		{
			sessionService.setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM,
					addBundleToCartRequestData.getSelectedJourneyRefNumber());

		}

		return addBundleToCartErrorMessage;
	}



	 
	protected String getAddToCartResponse(final boolean isValid, final String errorMessage, final Model model)
	{
		model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(isValid, errorMessage));
		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;

	}

	@RequestMapping(value = "/cart/accommodation/package-change", method = RequestMethod.POST, produces = "application/json")
	public String changePackageAccommodationInCart(@Valid final AccommodationAddToCartForm form, final Model model)
	{
		final List<Integer> accommodationOrderEntryGroupsInCart = bookingFacade.getAccommodationOrderEntryGroupRefs();
		final List<Integer> oldAccommodationOrderEntryGroupsInCart = bookingFacade.getOldAccommodationOrderEntryGroupRefs();

		if (form.getNumberOfRooms() > 1)
		{
			return getAddToCartResponse(false, ERROR_ADD_ROOM_QUANTITY, model);
		}

		if (!accommodationOrderEntryGroupsInCart.contains(form.getRoomStayRefNumber())
				|| oldAccommodationOrderEntryGroupsInCart.contains(form.getRoomStayRefNumber()))
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
			return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;

		}

		final List<RoomRateCartData> rates = accommodationCartFacade.collectRoomRates(form);

		if (CollectionUtils.isEmpty(rates))
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
			return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;
		}

		try
		{
			final Date checkInDate = TravelDateUtils.convertStringDateToDate(form.getCheckInDateTime(),
					TravelservicesConstants.DATE_PATTERN);
			final Date checkOutDate = TravelDateUtils.convertStringDateToDate(form.getCheckOutDateTime(),
					TravelservicesConstants.DATE_PATTERN);
			accommodationCartFacade.replaceAccommodationInCart(checkInDate, checkOutDate, form.getAccommodationOfferingCode(),
					form.getAccommodationCode(), rates, form.getNumberOfRooms(), form.getRatePlanCode(), form.getRoomStayRefNumber());
			bcfTravelCartFacade.recalculateCart();
			model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(true, null));
		}
		catch (final CommerceCartModificationException e)
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
			bcfTravelCartFacade.removeSessionCart();
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddAccommodationToCartResponse;
	}

	@RequestMapping(value = "/cart/best-offer-accommodations/add", method = RequestMethod.POST)
	public String addBestOfferedAccommodationsToCart(
			@ModelAttribute("accommodationAddToCartBookingForm") final AccommodationAddToCartBookingForm accommodationAddToCartBookingForm,
			@RequestParam(value = "accommodationDetailsPageURL", required = true) final String accommodationDetailsPageURL,
			final RedirectAttributes redirectModel, final Model model, final BindingResult bindingResult)
	{
		if (CollectionUtils.isEmpty(accommodationAddToCartBookingForm.getAccommodationAddToCartForms()))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, AMEND_CART_ERROR);
			return REDIRECT_PREFIX + accommodationDetailsPageURL;
		}

		int totalNumberOfRoomsForBookingRequested = 0;
		int index = 0;

		for (final AccommodationAddToCartForm accommodationAddToCartForm : accommodationAddToCartBookingForm
				.getAccommodationAddToCartForms())
		{
			totalNumberOfRoomsForBookingRequested += accommodationAddToCartForm.getNumberOfRooms();
			validateForm(accommodationAddToCartValidator, accommodationAddToCartForm, index++, bindingResult,
					BcfaccommodationsaddonWebConstants.ACCOMMODATION_ADD_TO_CART_FORM);
		}

		final int maxBookingAllowed = configurationService.getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

		if (bindingResult.hasErrors()
				|| !(totalNumberOfRoomsForBookingRequested > 0 && totalNumberOfRoomsForBookingRequested <= maxBookingAllowed))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, AMEND_CART_ERROR);
			return REDIRECT_PREFIX + accommodationDetailsPageURL;
		}

		accommodationCartFacade.emptyCart();

		for (final AccommodationAddToCartForm form : accommodationAddToCartBookingForm.getAccommodationAddToCartForms())
		{
			final List<RoomRateCartData> rates = accommodationCartFacade.collectRoomRates(form);

			if (CollectionUtils.isEmpty(rates))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, AMEND_CART_ERROR);
				return REDIRECT_PREFIX + accommodationDetailsPageURL;
			}
			try
			{
				final Date checkInDate = TravelDateUtils.convertStringDateToDate(form.getCheckInDateTime(),
						TravelservicesConstants.DATE_PATTERN);
				final Date checkOutDate = TravelDateUtils.convertStringDateToDate(form.getCheckOutDateTime(),
						TravelservicesConstants.DATE_PATTERN);
				accommodationCartFacade.addAccommodationToCart(checkInDate, checkOutDate, form.getAccommodationOfferingCode(),
						form.getAccommodationCode(), rates, form.getNumberOfRooms(), form.getRatePlanCode());
			}
			catch (final CommerceCartModificationException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, AMEND_CART_ERROR);
				return REDIRECT_PREFIX + accommodationDetailsPageURL;
			}
		}
		bcfTravelCartFacade.recalculateCart();
		model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(true, null));
		return REDIRECT_PREFIX + NEXT_PAGE_REDIRECT_URL;
	}

	protected AddToCartResponseData createAddToCartResponse(final boolean valid, final String errorMessage)
	{
		final AddToCartResponseData response = new AddToCartResponseData();
		response.setValid(valid);
		response.setErrors(Collections.singletonList(errorMessage));
		return response;
	}

	@RequestMapping(value = "/cart/accommodation/add-extra", method = RequestMethod.POST, produces = "application/json")
	public String addExtraToCart(final AddExtraToCartForm addExtraToCartForm, final Model model)
	{
		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		if (!validator.validate(addExtraToCartForm).isEmpty())
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
			return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddExtraToCartResponse;
		}

		try
		{
			accommodationCartFacade.addProductToCart(addExtraToCartForm.getProductCode(),
					addExtraToCartForm.getRoomStayReferenceNumber(), addExtraToCartForm.getQuantity());
			bcfTravelCartFacade.recalculateCart();
			model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(true, null));
		}
		catch (final CommerceCartModificationException e)
		{
			model.addAttribute(ADD_TO_CART_RESPONSE,
					createAddToCartResponse(false, BcfaccommodationsaddonWebConstants.BASKET_ERROR_OCCURRED));
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddExtraToCartResponse;
	}

	/**
	 * Saves the room bed preference against the room stay ref number. If any of the information is incorrect, nothing is
	 * saved.
	 */
	@RequestMapping(value = "/cart/accommodation/save-room-preference", method = RequestMethod.POST, produces = "application/json")
	public String saveRoomPreference(final HttpServletRequest request, final HttpServletResponse response,
			@ModelAttribute(value = "roomStayRefNum") final String roomStayRefNum,
			@ModelAttribute(value = "roomPreferenceCode") final String roomPreferenceCode, final Model model)
	{
		if (StringUtils.isNotEmpty(roomPreferenceCode) || StringUtils.isNotEmpty(roomStayRefNum))
		{
			try
			{
				final int roomStayRefNumber = Integer.parseInt(roomStayRefNum);
				final boolean isSuccess = roomPreferenceFacade.saveRoomPreference(roomStayRefNumber,
						Collections.singletonList(roomPreferenceCode));
				model.addAttribute(ADD_TO_CART_RESPONSE, createAddToCartResponse(isSuccess,
						isSuccess ? StringUtils.EMPTY : BcfaccommodationsaddonWebConstants.ERROR_ROOM_BED_PREFERENCE_ADD));
			}
			catch (final NumberFormatException ex)
			{
				model.addAttribute(ADD_TO_CART_RESPONSE,
						createAddToCartResponse(Boolean.FALSE, BcfaccommodationsaddonWebConstants.ERROR_ROOM_BED_PREFERENCE_ADD));
			}
		}
		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AddExtraToCartResponse;
	}

	@RequestMapping(value = "/cart/remove-room/{roomStayReference}", method = RequestMethod.POST)
	public String removeRoom(@PathVariable("roomStayReference") final String roomStayReference, final Model model, final RedirectAttributes redirectModel)
	{

			final int roomStayRef = Integer.parseInt(roomStayReference);

				final Boolean wasRemoved = accommodationCartFacade.removeAccommodationOrderEntryGroup(roomStayRef);

				if(!wasRemoved){
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_ROOM_FAILED);

				}else{
					bcfTravelCartFacadeHelper.updateAvailabilityStatus();
					bcfTravelCartFacadeHelper.calculateChangeFees();
				}

		return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.ALACARTE_REVIEW_PAGE_PATH;
	}

	/**
	 * Validate Form method
	 *
	 * @param accommodationAddToCartValidator
	 * @param accommodationAddToCartForm
	 * @param index
	 * @param bindingResult
	 * @param formName
	 */
	protected void validateForm(final AbstractTravelValidator accommodationAddToCartValidator,
			final AccommodationAddToCartForm accommodationAddToCartForm, final int index, final BindingResult bindingResult,
			final String formName)
	{
		accommodationAddToCartValidator.setTargetForm(BcfaccommodationsaddonWebConstants.ACCOMMODATION_ADD_TO_CART_FORM);
		accommodationAddToCartValidator.setAttributePrefix("accommodationAddToCartForms[" + index + "]");
		accommodationAddToCartValidator.validate(accommodationAddToCartForm, bindingResult);
	}
}
