/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.cms;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.BookingActionRequestData;
import de.hybris.platform.commercefacades.travel.BookingActionResponseData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationOfferingCustomerReviewFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Arrays;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationReviewForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AddRequestForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractBookingDetailsComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractBookingDetailsComponentModel;


/**
 * Controller for Accommodation Booking Details component
 */
@Controller("AccommodationBookingDetailsComponentController")
@RequestMapping(value = BcfaccommodationsaddonControllerConstants.Actions.Cms.AccommodationBookingDetailsComponent)
public class AccommodationBookingDetailsComponentController extends AbstractBookingDetailsComponentController
{

	private static final String ACCOMMODATION_RESERVATION_DATA = "accommodationReservationData";

	@Resource(name = "accommodationOfferingCustomerReviewFacade")
	private AccommodationOfferingCustomerReviewFacade accommodationOfferingCustomerReviewFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AbstractBookingDetailsComponentModel component)
	{
		final String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);

		final boolean isAccommodationBookingJourney = getBookingFacade()
				.checkBookingJourneyType(bookingReference, Arrays.asList(BookingJourneyType.BOOKING_ACCOMMODATION_ONLY));
		if (isAccommodationBookingJourney)
		{
			try
			{
				final AccommodationReservationData reservationData = getBookingFacade().getFullAccommodationBooking(bookingReference);
				model.addAttribute(ACCOMMODATION_RESERVATION_DATA, reservationData);
				// BookingActions
				final BookingActionRequestData bookingActionRequest = createAccommodationBookingActionRequest(bookingReference);
				final BookingActionResponseData bookingActionResponse = getActionFacade()
						.getAccommodationBookingAction(bookingActionRequest, reservationData);
				model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_ACTION_RESPONSE, bookingActionResponse);
				model.addAttribute(BcfaccommodationsaddonWebConstants.ADD_REQUEST_FORM, new AddRequestForm());
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_REVIEW_FORM, new AccommodationReviewForm());
				model.addAttribute(BcfaccommodationsaddonWebConstants.SUBMITTED_REVIEWS,
						accommodationOfferingCustomerReviewFacade.retrieveCustomerReviewByBooking(bookingReference,
								reservationData.getAccommodationReference().getAccommodationOfferingCode()));
				final PriceData amountPaid = getTravelCommercePriceFacade().getPaidAmount(reservationData);
				model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_PAID, amountPaid);
				model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_DUE,
						getTravelCommercePriceFacade().getDueAmount(reservationData, amountPaid));
			}
			catch (final JaloObjectNoLongerValidException ex)
			{
				model.addAttribute(PAGE_NOT_AVAILABLE, BcfstorefrontaddonWebConstants.PAGE_TEMPORARY_NOT_AVAILABLE);
			}
		}
		model.addAttribute("displayAccommodationBookingDetailsComponent", isAccommodationBookingJourney);
	}

	@Override
	protected BookingActionRequestData createAccommodationBookingActionRequest(final String bookingReference)
	{
		final BookingActionRequestData bookingActionRequestData = super.createAccommodationBookingActionRequest(bookingReference);
		bookingActionRequestData.getRequestActions().add(ActionTypeOption.CANCEL_BOOKING);
		return bookingActionRequestData;
	}

}
