/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.pages;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.facades.bcffacades.hotelinfo.BcfHotelInfoFacade;


@Controller
public class HotelDetailsController
{
	private static final String HOTEL_DETAIL = "hotelDetail";

	@Resource(name = "bcfHotelInfoFacade")
	private BcfHotelInfoFacade bcfHotelInfoFacade;

	@RequestMapping(value = "/hotel-details", method = RequestMethod.GET, produces = "application/json")
	public String hotelDetails(@RequestParam(value = "code") final String code, final Model model)
	{
		model.addAttribute(HOTEL_DETAIL, bcfHotelInfoFacade.hotelDetails(code));
		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.HotelDetailJsonResponse;
	}
}
