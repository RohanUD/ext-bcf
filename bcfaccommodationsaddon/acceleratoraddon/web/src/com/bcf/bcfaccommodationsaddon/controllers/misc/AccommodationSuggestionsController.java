/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationSuggestionFacade;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;


/**
 * Controller for accommodation AutoSuggestion functionality
 */
@Controller
public class AccommodationSuggestionsController extends AbstractController
{

	@Resource(name = "accommodationSuggestionFacade")
	private AccommodationSuggestionFacade accommodationSuggestionFacade;

	private static final String SUGGESTION_RESULT = "suggestionResult";

	/**
	 * @param text  the search text
	 * @param model
	 * @return the location of the jsp with the json response for the autosuggestion
	 */
	@RequestMapping(value = "/accommodation-suggestions", method = RequestMethod.GET, produces = "application/json")
	public String locationSuggestion(@RequestParam(value = "text") final String text, final Model model)
	{
		final List<GlobalSuggestionData> suggestionResults = accommodationSuggestionFacade.getLocationSuggestions(text);
		model.addAttribute(SUGGESTION_RESULT, suggestionResults);
		return BcfaccommodationsaddonControllerConstants.Views.Pages.Suggestions.SuggestionsSearchJsonResponse;
	}

}
