/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.property.FacilityData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.accommodation.user.data.SearchAddressData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.accommodation.search.AccommodationSearchFacade;
import de.hybris.platform.travelfacades.facades.accommodation.strategies.EncodeSearchUrlToMapStrategy;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.impl.DefaultBcfAccommodationFacadeHelper;
import com.bcf.facades.location.BcfTravelLocationFacade;


/**
 * Controller for Accommodation Search page
 */
@Controller
@RequestMapping("/accommodation-search")
public class AccommodationSearchPageController extends AbstractAccommodationPageController
{
	private static final Logger LOG = Logger.getLogger(AccommodationSearchPageController.class);

	private static final String ACCOMMODATION_SEARCH_CMS_PAGE = "accommodationSearchPage";
	private static final String ROOM_QUERY_STRING_INDICATOR = "r";
	private static final String STARTING_NUMBER_OF_RESULTS = "startingNumberOfResults";
	private static final String TOTAL_NUMBER_OF_RESULTS = "totalNumberOfResults";
	private static final String TOTAL_SHOWN_RESULTS = "totalShownResults";
	private static final String PAGE_NUM = "pageNum";
	private static final String HAS_MORE_RESULTS = "hasMoreResults";
	private static final String ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES = "accommodationSearchResponseProperties";
	private static final String ACCOMMODATION_DETAILS_URL_PARAMS = "accommodationDetailsUrlParams";
	private static final String ACCOMMODATION_SEARCH_ROOT = "/accommodation-search";
	private static final String PROPERTY_NAME = "propertyName";
	private static final String QUERY = "q";

	@Resource(name = "accommodationFinderValidator")
	private Validator accommodationFinderValidator;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "accommodationSearchFacade")
	private AccommodationSearchFacade accommodationSearchFacade;

	@Resource(name = "encodeSearchUrlToMapStrategy")
	private EncodeSearchUrlToMapStrategy encodeSearchUrlToMapStrategy;

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	/**
	 * Gets accommodation search page.
	 *
	 * @param accommodationFinderForm the accommodation finder form
	 * @param propertyName            the property name
	 * @param query                   the query
	 * @param sortCode                the sort code
	 * @param pageNumber              the page number
	 * @param bindingResult           the binding result
	 * @param model                   the model
	 * @param request                 the request
	 * @return the accommodation search page
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAccommodationSearchPage(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) AccommodationFinderForm accommodationFinderForm,
			@RequestParam(value = "propertyName", required = false) final String propertyName,
			@RequestParam(value = "q", required = false) final String query,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOMMODATION_SEARCH_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOMMODATION_SEARCH_CMS_PAGE));
		model.addAttribute("showPrice", true);
		adjustSessionBookingJourney();

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		final String newQueryString = populateModelParams(accommodationFinderForm, model, request, sessionBookingJourney);
		if (newQueryString != null)
			return newQueryString;

		/*
		 * Below block is to re-set the facets whenever there is a change in the currency
		 */
		if (!StringUtils.equals(sessionService.getAttribute(BcfstorefrontaddonWebConstants.SESSION_PREVIOUS_CURRENCY),
				storeSessionFacade.getCurrentCurrency().getIsocode()))
		{
			return getModifiedQueryString(request);
		}

		if (request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY) != null && request
				.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY).equals("true"))
		{

			accommodationFinderForm = createAccommodationFinderForm(request);

		}

		/*
		 * below code is required because if a person hits accommodation-search url directly, roomStayCandidates will be
		 * empty as it is not set in the url as per its respective form field name
		 */
		if (accommodationFinderForm.getRoomStayCandidates() == null)
		{
			accommodationFinderForm.setRoomStayCandidates(createRoomStayCandidatesForSearchPage(request));
		}

		accommodationFinderValidator.validate(accommodationFinderForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			request.setAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_FINDER_FORM_BINDING_RESULT, bindingResult);
		}
		else
		{
			final String encodedPropertyName;
			if (StringUtils.isNotBlank(propertyName) && !validateFieldPattern(propertyName,
					TravelacceleratorstorefrontValidationConstants.REGEX_SPECIAL_LETTERS_NUMBER_SPACES))
			{
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
						BcfaccommodationsaddonWebConstants.FILTER_PROPERTY_ERROR_MESSAGE);
				encodedPropertyName = StringUtils.EMPTY;
			}
			else
			{
				encodedPropertyName = StringUtils.trim(XSSFilterUtil.filter(propertyName));
			}
			model.addAttribute(BcfaccommodationsaddonWebConstants.FILTER_PROPERTY_NAME, encodedPropertyName);

			String validateSortCode = sortCode;
			if (StringUtils.isNotBlank(sortCode)
					&& !validateFieldPattern(sortCode, TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES))
			{
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
						BcfaccommodationsaddonWebConstants.SORT_CODE_ERROR_MESSAGE);
				validateSortCode = null;
			}

			String validateQuery = query;
			if (StringUtils.isNotBlank(query)
					&& !validateFieldPattern(query, TravelacceleratorstorefrontValidationConstants.REGEX_QUERY_PARAMETER))
			{
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
						BcfaccommodationsaddonWebConstants.FILTER_QUERY_ERROR_MESSAGE);
				validateQuery = null;
			}

			resolveDestinationLocation(accommodationFinderForm);
			final AccommodationSearchRequestData accommodationSearchRequestData = prepareAccommodationSearchRequestData(
					accommodationFinderForm, request, encodedPropertyName, validateQuery, validateSortCode);


			model.addAttribute(BcfFacadesConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS,
					encodeSearchUrlToMapStrategy.encode(accommodationSearchRequestData));

			final AccommodationSearchResponseData accommodationSearchResponseData = accommodationSearchFacade
					.doSearch(accommodationSearchRequestData);

			final List<PropertyData> searchResponseProperties = accommodationSearchResponseData.getProperties();
			sessionService.setAttribute(ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES, searchResponseProperties);

			final int accommodationSearchPageSize = getAccommodationSearchResultsPageSize();

			accommodationSearchResponseData.setProperties(searchResponseProperties.subList(0,
					Math.min(pageNumber * accommodationSearchPageSize, searchResponseProperties.size())));

			model.addAttribute(START_PAGE_NUMBER,
					getJsonPaginationStartPageNumber(searchResponseProperties.size(), accommodationSearchPageSize,
							pageNumber));
			model.addAttribute(END_PAGE_NUMBER,
					getJsonPaginationEndPageNumber(searchResponseProperties.size(), accommodationSearchPageSize, pageNumber));

			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE, accommodationSearchResponseData);
			sessionService.setAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE,
					accommodationSearchResponseData);
			populateModelWithShowMoreInfo(pageNumber, model, searchResponseProperties, accommodationSearchPageSize);

			if (request.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY) != null && request
					.getParameter(DefaultBcfAccommodationFacadeHelper.MODIFY).equals("true"))
			{


				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_OFFERING_DETAILS_URL_PARAMETERS,
						request.getQueryString());

				sessionService.setAttribute(ACCOMMODATION_DETAILS_URL_PARAMS, request.getQueryString());

			}
			else
			{

				final String accommodationDetailsUrlParams = buildAccommodationOfferingDetailsPageUrlParameters(
						accommodationSearchRequestData.getCriterion());

				sessionService.setAttribute(ACCOMMODATION_DETAILS_URL_PARAMS, accommodationDetailsUrlParams);


				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_OFFERING_DETAILS_URL_PARAMETERS,
						accommodationDetailsUrlParams);
			}



			model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
					getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));
		}

		request.setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
		sessionService.setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);

		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE,
				getResultViewType(resultsViewType));

		return getViewForPage(model);

	}

	protected void resolveDestinationLocation(final AccommodationFinderForm accommodationFinderForm)
	{
		final LocationData location = bcfTravelLocationFacade.getLocation(accommodationFinderForm.getDestinationLocation());

		if (Objects.nonNull(location))
		{
			accommodationFinderForm.setDestinationLocation(location.getCode());
			accommodationFinderForm.setDestinationLocationName(location.getName());
			accommodationFinderForm.setSuggestionType(SuggestionType.LOCATION.toString());
		}
	}

	private String populateModelParams(final AccommodationFinderForm accommodationFinderForm,
			final Model model, final HttpServletRequest request, final String sessionBookingJourney)
	{
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
			if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))
			{
				final String checkInDate = accommodationFinderForm.getCheckInDateTime();
				final String checkOutDate = accommodationFinderForm.getCheckOutDateTime();

				final String newQueryString = checkDatesAndGetNewQueryString(checkInDate, checkOutDate, request.getQueryString());
				if (StringUtils.isNotBlank(newQueryString))
				{
					return REDIRECT_PREFIX + ACCOMMODATION_SEARCH_ROOT + "?" + newQueryString;
				}
			}
		}
		else
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
		}
		return null;
	}

	protected String getResultViewType(final String resultViewType)
	{
		if (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_DEFAULT,
				resultViewType)
				|| StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_GRID,
				resultViewType)
				|| StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_MAP,
				resultViewType))
		{
			return resultViewType;
		}
		return BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_DEFAULT;
	}

	/**
	 * Below method clears the q and propertyName request parameters from the query string as they are supposed to be
	 * cleared on currency change
	 *
	 * @param request
	 * @return
	 */
	protected String getModifiedQueryString(final HttpServletRequest request)
	{
		final StringBuilder urlParameters = new StringBuilder();
		final Map<String, String[]> map = request.getParameterMap();
		final Set<String> keys = map.keySet();
		for (final String key : keys)
		{
			if (StringUtils.equals(QUERY, key) || StringUtils.equals(PROPERTY_NAME, key))
			{
				continue;
			}
			urlParameters.append(key);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			String value = map.get(key)[0];
			if (value.contains("|"))
			{
				value = value.replaceAll("\\|", "%7C");
			}
			urlParameters.append(value);
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		}
		String urlParametersString = urlParameters.toString();
		urlParametersString = urlParametersString.substring(0, urlParametersString.length() - 1);
		return REDIRECT_PREFIX + BcfaccommodationsaddonWebConstants.ACCOMMODATION_SELECTION_ROOT_URL + "?" + urlParametersString;
	}


	/**
	 * Prepare accommodation search request data accommodation search request data.
	 *
	 * @param accommodationFinderForm the accommodation finder form
	 * @param request                 the request
	 * @param propertyName            the property name
	 * @param query                   the query
	 * @param sortCode                the sort code
	 * @return the accommodation search request data
	 */
	protected AccommodationSearchRequestData prepareAccommodationSearchRequestData(
			final AccommodationFinderForm accommodationFinderForm, final HttpServletRequest request, final String propertyName,
			final String query, final String sortCode)
	{
		final AccommodationSearchRequestData accommodationSearchRequestData = new AccommodationSearchRequestData();

		//create Criterion Data
		final CriterionData criterionData = new CriterionData();

		//set Address Data
		final SearchAddressData addressData = new SearchAddressData();
		addressData.setFormattedIndicator(false);
		addressData.setLine1(accommodationFinderForm.getDestinationLocation());
		addressData.setLine2(accommodationFinderForm.getDestinationLocationName());
		criterionData.setAddress(addressData);

		// set Facility Data
		final List<FacilityData> amenities = new ArrayList<>();
		criterionData.setAmenities(amenities);

		// set Stay Range Data
		final StayDateRangeData stayRangeData = new StayDateRangeData();
		stayRangeData.setStartTime(TravelDateUtils.convertStringDateToDate(accommodationFinderForm.getCheckInDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		stayRangeData.setEndTime(TravelDateUtils.convertStringDateToDate(accommodationFinderForm.getCheckOutDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		stayRangeData
				.setLengthOfStay((int) TravelDateUtils.getDaysBetweenDates(stayRangeData.getStartTime(), stayRangeData.getEndTime()));
		criterionData.setStayDateRange(stayRangeData);

		// set roomStayCandidates
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		try
		{
			if (!CollectionUtils.isEmpty(accommodationFinderForm.getRoomStayCandidates()))
			{
				IntStream.range(0, accommodationFinderForm.getNumberOfRooms()).forEach(index -> {
					final RoomStayCandidateData roomStayCandidateData = accommodationFinderForm.getRoomStayCandidates().get(index);
					roomStayCandidates.add(roomStayCandidateData);
				});
			}
		}
		catch (final NumberFormatException e)
		{
			LOG.error("Cannot parse number of rooms string to integer");
			LOG.error(e.getClass().getName() + " : " + e.getMessage());
			LOG.debug(e);
		}
		criterionData.setRoomStayCandidates(roomStayCandidates);

		criterionData.setPropertyFilterText(propertyName);

		criterionData.setQuery(query);
		criterionData.setSort(sortCode);
		criterionData.setSuggestionType(accommodationFinderForm.getSuggestionType());

		accommodationSearchRequestData.setCriterion(criterionData);

		return accommodationSearchRequestData;
	}

	/**
	 * Build accommodation offering details page url parameters string.
	 *
	 * @param criterion the criterion
	 * @return the string
	 */
	protected String buildAccommodationOfferingDetailsPageUrlParameters(final CriterionData criterion)
	{
		final StringBuilder urlParameters = new StringBuilder();

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(TravelDateUtils.convertDateToStringDate(criterion.getStayDateRange().getStartTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(TravelDateUtils.convertDateToStringDate(criterion.getStayDateRange().getEndTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(criterion.getRoomStayCandidates().size());

		criterion.getRoomStayCandidates().forEach(candidate -> {
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
			urlParameters.append(ROOM_QUERY_STRING_INDICATOR);
			urlParameters.append(candidate.getRoomStayCandidateRefNumber().toString());
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			final StringJoiner joiner = new StringJoiner(BcfstorefrontaddonWebConstants.COMMA);
			candidate.getPassengerTypeQuantityList().forEach(passenger -> {
				final StringBuilder ages = new StringBuilder();
				if (CollectionUtils.isNotEmpty(passenger.getChildAges()))
				{
					for (final int age : passenger.getChildAges())
					{
						ages.append(BcfstorefrontaddonWebConstants.HYPHEN + age);
					}
				}
				joiner.add(
						passenger.getQuantity() + BcfstorefrontaddonWebConstants.HYPHEN + passenger.getPassengerType().getCode() + ages
								.toString());
			});

			urlParameters.append(joiner.toString());
		});
		return urlParameters.toString();
	}

	/**
	 * Prepare customer review data json response.
	 *
	 * @param accommodationOfferingCode the accommodation offering code
	 * @param model                     the model
	 * @return the string
	 */
	@RequestMapping("/customer-review/{accommodationOfferingCode}")
	public String prepareCustomerReviewData(@PathVariable final String accommodationOfferingCode, final Model model)
	{

		final int noOfReviews = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.ACCOMMODATION_OFFERING_NUMBER_OF_REVIEWS);

		final PageableData pageableData = createPageableData(0, noOfReviews, "byDate");

		final SearchPageData<ReviewData> customerReviewsSearchPageData = getAccommodationOfferingCustomerReviewFacade()
				.getAccommodationOfferingCustomerReviewDetails(accommodationOfferingCode, pageableData);

		final PropertyData propertyData = getAccommodationOfferingFacade().getPropertyData(accommodationOfferingCode);
		model.addAttribute(BcfaccommodationsaddonWebConstants.PROPERTY, propertyData);

		model.addAttribute(BcfaccommodationsaddonWebConstants.CUSTOMER_REVIEW_SEARCH_PAGE_DATA, customerReviewsSearchPageData);

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.CustomerReviewJsonResponse;
	}

	/**
	 * Display method to return the properties to mark on Map.
	 *
	 * @return the string
	 */
	@RequestMapping("/display-view")
	public String displayView(final Model model,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber,
			final HttpServletRequest request)
	{
		final List<PropertyData> searchResponseProperties = sessionService.getAttribute(ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES);
		sessionService.setAttribute(ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES, searchResponseProperties);

		final int accommodationSearchPageSize = getAccommodationSearchResultsPageSize();
		final AccommodationSearchResponseData accommodationSearchResponseData = sessionService
				.getAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE);
		accommodationSearchResponseData.setProperties(searchResponseProperties.subList(0,
				Math.min(pageNumber * accommodationSearchPageSize, searchResponseProperties.size())));
		sessionService.setAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE,
				accommodationSearchResponseData);
		model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PROPERTIES, searchResponseProperties);
		model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_OFFERING_DETAILS_URL_PARAMETERS,
				sessionService.getAttribute(ACCOMMODATION_DETAILS_URL_PARAMS));
		model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE, accommodationSearchResponseData);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE,
				getResultViewType(resultsViewType));
		populateModelWithShowMoreInfo(pageNumber, model, searchResponseProperties, accommodationSearchPageSize);
		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AccommodationResultsViewJsonResponse;
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param pageNumber the page number
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping("/show-more")
	public String showMore(@RequestParam(value = "pageNumber", required = false) final int pageNumber,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			final Model model)
	{
		final List<PropertyData> accommodationSearchSessionProperties = sessionService
				.getAttribute(ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES);

		final int accommodationSearchPageSize = getAccommodationSearchResultsPageSize();

		if (CollectionUtils.isNotEmpty(accommodationSearchSessionProperties))
		{
			final int startIndex = pageNumber - 1;
			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PROPERTIES,
					accommodationSearchSessionProperties.subList(startIndex * accommodationSearchPageSize,
							Math.min(pageNumber * accommodationSearchPageSize, accommodationSearchSessionProperties.size())));
			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE,
					sessionService.getAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE));
		}
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(START_PAGE_NUMBER,
				getJsonPaginationStartPageNumber(accommodationSearchSessionProperties.size(), accommodationSearchPageSize,
						pageNumber));
		model.addAttribute(END_PAGE_NUMBER,
				getJsonPaginationEndPageNumber(accommodationSearchSessionProperties.size(), accommodationSearchPageSize, pageNumber));
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, accommodationSearchSessionProperties.size());
		model.addAttribute(STARTING_NUMBER_OF_RESULTS, (pageNumber - 1) * accommodationSearchPageSize + 1);
		final Boolean hasMoreResults = pageNumber * accommodationSearchPageSize < accommodationSearchSessionProperties.size();
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);
		model.addAttribute(TOTAL_SHOWN_RESULTS,
				hasMoreResults ? pageNumber * accommodationSearchPageSize : accommodationSearchSessionProperties.size());

		final String accommodationDetailsUrlParams = sessionService.getAttribute(ACCOMMODATION_DETAILS_URL_PARAMS);

		if (StringUtils.isNotEmpty(accommodationDetailsUrlParams))
		{
			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_OFFERING_DETAILS_URL_PARAMETERS,
					accommodationDetailsUrlParams);
		}
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE,
				getResultViewType(resultsViewType));
		if (bcfTravelCartService.hasSessionCart())
		{
			final CartModel sessionCart = bcfTravelCartService.getSessionCart();
			model.addAttribute("isAmendment", bcfTravelCartService.isAmendmentCart(sessionCart));
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.AccommodationListingJsonResponse;
	}

	/**
	 * Gets the accommodation search results page size.
	 *
	 * @return the accommodation search results page size
	 */
	protected int getAccommodationSearchResultsPageSize()
	{
		int accommodationSearchPageSize = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.ACCOMMODATION_SEARCH_RESULT_PAGE_SIZE);
		accommodationSearchPageSize = accommodationSearchPageSize > 0 ? accommodationSearchPageSize : MAX_PAGE_LIMIT;
		return accommodationSearchPageSize;
	}


	/**
	 * Populate model with show more info.
	 *
	 * @param pageNumber                  the page number
	 * @param model                       the model
	 * @param properties                  the properties
	 * @param accommodationSearchPageSize the accommodation search page size
	 */
	private void populateModelWithShowMoreInfo(final int pageNumber, final Model model, final List<PropertyData> properties,
			final int accommodationSearchPageSize)
	{
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, properties.size());
		model.addAttribute(STARTING_NUMBER_OF_RESULTS, (pageNumber - 1) * accommodationSearchPageSize + 1);
		final Boolean hasMoreResults = pageNumber * accommodationSearchPageSize < properties.size();
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);

		if (hasMoreResults)
		{
			model.addAttribute(TOTAL_SHOWN_RESULTS, pageNumber * accommodationSearchPageSize);
		}
		else
		{
			model.addAttribute(TOTAL_SHOWN_RESULTS, properties.size());
		}
	}

	/**
	 * Validate field pattern boolean.
	 *
	 * @param attribute the attribute
	 * @param pattern   the pattern
	 * @return the boolean
	 */
	protected Boolean validateFieldPattern(final String attribute, final String pattern)
	{
		if (!attribute.matches(pattern))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Gets accommodation search facade.
	 *
	 * @return the accommodation search facade
	 */
	public AccommodationSearchFacade getAccommodationSearchFacade()
	{
		return accommodationSearchFacade;
	}

	/**
	 * Sets accommodation search facade.
	 *
	 * @param accommodationSearchFacade the accommodation search facade
	 */
	public void setAccommodationSearchFacade(final AccommodationSearchFacade accommodationSearchFacade)
	{
		this.accommodationSearchFacade = accommodationSearchFacade;
	}

}
