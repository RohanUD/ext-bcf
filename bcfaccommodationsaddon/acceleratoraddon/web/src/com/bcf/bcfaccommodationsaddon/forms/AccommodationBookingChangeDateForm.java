/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms;

public class AccommodationBookingChangeDateForm
{
	private String checkInDateTime;

	private String checkOutDateTime;

	private String bookingReference;

	/**
	 * @return the checkInDateTime
	 */
	public String getCheckInDateTime()
	{
		return checkInDateTime;
	}

	/**
	 * @param checkInDateTime the checkInDateTime to set
	 */
	public void setCheckInDateTime(final String checkInDateTime)
	{
		this.checkInDateTime = checkInDateTime;
	}

	/**
	 * @return the checkOutDateTime
	 */
	public String getCheckOutDateTime()
	{
		return checkOutDateTime;
	}

	/**
	 * @param checkOutDateTime the checkOutDateTime to set
	 */
	public void setCheckOutDateTime(final String checkOutDateTime)
	{
		this.checkOutDateTime = checkOutDateTime;
	}

	/**
	 * @return the bookingReference
	 */
	public String getBookingReference()
	{
		return bookingReference;
	}

	/**
	 * @param bookingReference the bookingReference to set
	 */
	public void setBookingReference(final String bookingReference)
	{
		this.bookingReference = bookingReference;
	}


}
