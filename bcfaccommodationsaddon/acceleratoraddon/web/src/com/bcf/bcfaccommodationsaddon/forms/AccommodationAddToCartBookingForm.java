/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms;

import de.hybris.platform.travelfacades.facades.accommodation.forms.AccommodationAddToCartForm;
import java.util.List;


public class AccommodationAddToCartBookingForm
{
	List<AccommodationAddToCartForm> accommodationAddToCartForms;

	/**
	 * @return the accommodationAddToCartForms
	 */
	public List<AccommodationAddToCartForm> getAccommodationAddToCartForms()
	{
		return accommodationAddToCartForms;
	}

	/**
	 * @param accommodationAddToCartForms the accommodationAddToCartForms to set
	 */
	public void setAccommodationAddToCartForms(final List<AccommodationAddToCartForm> accommodationAddToCartForms)
	{
		this.accommodationAddToCartForms = accommodationAddToCartForms;
	}
}
