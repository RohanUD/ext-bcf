/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms.cms;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import java.util.List;
import java.util.Set;


public class AccommodationFinderForm
{
	private String destinationLocation;
	private String destinationLocationName;
	private String suggestionType;
	private String checkInDateTime;
	private String checkOutDateTime;
	private Set<String> promotionalRoomRateProducts;
	private int numberOfRooms;
	private List<RoomStayCandidateData> roomStayCandidates;
	private String travelRoute;
	private boolean modify;
	private int modifyRoomRef;

	public int getModifyRoomRef()
	{
		return modifyRoomRef;
	}

	public void setModifyRoomRef(final int modifyRoomRef)
	{
		this.modifyRoomRef = modifyRoomRef;
	}

	public boolean isModify()
	{
		return modify;
	}

	public void setModify(final boolean modify)
	{
		this.modify = modify;
	}

	public String getDestinationLocation()
	{
		return destinationLocation;
	}

	public void setDestinationLocation(final String destinationLocation)
	{
		this.destinationLocation = destinationLocation;
	}

	public String getDestinationLocationName()
	{
		return destinationLocationName;
	}

	public void setDestinationLocationName(final String destinationLocationName)
	{
		this.destinationLocationName = destinationLocationName;
	}

	public String getSuggestionType()
	{
		return suggestionType;
	}

	public void setSuggestionType(final String suggestionType)
	{
		this.suggestionType = suggestionType;
	}

	public String getCheckInDateTime()
	{
		return checkInDateTime;
	}

	public void setCheckInDateTime(final String checkInDateTime)
	{
		this.checkInDateTime = checkInDateTime;
	}

	public String getCheckOutDateTime()
	{
		return checkOutDateTime;
	}

	public void setCheckOutDateTime(final String checkOutDateTime)
	{
		this.checkOutDateTime = checkOutDateTime;
	}

	public int getNumberOfRooms()
	{
		return numberOfRooms;
	}

	public void setNumberOfRooms(final int numberOfRooms)
	{
		this.numberOfRooms = numberOfRooms;
	}

	public List<RoomStayCandidateData> getRoomStayCandidates()
	{
		return roomStayCandidates;
	}

	public void setRoomStayCandidates(final List<RoomStayCandidateData> roomStayCandidates)
	{
		this.roomStayCandidates = roomStayCandidates;
	}

	public Set<String> getPromotionalRoomRateProducts()
	{
		return promotionalRoomRateProducts;
	}

	public void setPromotionalRoomRateProducts(final Set<String> promotionalRoomRateProducts)
	{
		this.promotionalRoomRateProducts = promotionalRoomRateProducts;
	}

	public String getTravelRoute()
	{
		return travelRoute;
	}

	public void setTravelRoute(final String travelRoute)
	{
		this.travelRoute = travelRoute;
	}
}
