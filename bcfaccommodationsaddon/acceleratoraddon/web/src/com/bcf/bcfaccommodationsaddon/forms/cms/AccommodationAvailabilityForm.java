/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms.cms;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import java.util.List;


public class AccommodationAvailabilityForm
{
	private String checkInDateTime;
	private String checkOutDateTime;
	private int numberOfRooms;
	private List<RoomStayCandidateData> roomStayCandidates;


	public String getCheckInDateTime()
	{
		return checkInDateTime;
	}

	public void setCheckInDateTime(final String checkInDateTime)
	{
		this.checkInDateTime = checkInDateTime;
	}

	public String getCheckOutDateTime()
	{
		return checkOutDateTime;
	}

	public void setCheckOutDateTime(final String checkOutDateTime)
	{
		this.checkOutDateTime = checkOutDateTime;
	}

	public int getNumberOfRooms()
	{
		return numberOfRooms;
	}

	public void setNumberOfRooms(final int numberOfRooms)
	{
		this.numberOfRooms = numberOfRooms;
	}

	public List<RoomStayCandidateData> getRoomStayCandidates()
	{
		return roomStayCandidates;
	}

	public void setRoomStayCandidates(final List<RoomStayCandidateData> roomStayCandidates)
	{
		this.roomStayCandidates = roomStayCandidates;
	}

}
