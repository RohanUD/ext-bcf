/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms.cms;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.travelfacades.facades.accommodation.forms.AccommodationAddToCartForm;
import java.util.List;


public class BcfAccommodationAddToCartForm extends AccommodationAddToCartForm
{
	private String promotionCode;

	private boolean change;

	private Integer modifyRoomRef;

	private boolean onlyAccommodationChange;

	private List<RoomStayCandidateData> roomStayCandidates;

	public boolean isOnlyAccommodationChange()
	{
		return onlyAccommodationChange;
	}

	public void setOnlyAccommodationChange(final boolean onlyAccommodationChange)
	{
		this.onlyAccommodationChange = onlyAccommodationChange;
	}

	public Integer getModifyRoomRef()
	{
		return modifyRoomRef;
	}

	public void setModifyRoomRef(final Integer modifyRoomRef)
	{
		this.modifyRoomRef = modifyRoomRef;
	}

	public String getPromotionCode()
	{
		return promotionCode;
	}

	public void setPromotionCode(final String promotionCode)
	{
		this.promotionCode = promotionCode;
	}

	public List<RoomStayCandidateData> getRoomStayCandidates()
	{
		return roomStayCandidates;
	}

	public void setRoomStayCandidates(
			final List<RoomStayCandidateData> roomStayCandidates)
	{
		this.roomStayCandidates = roomStayCandidates;
	}

	public boolean isChange()
	{
		return change;
	}

	public void setChange(final boolean change)
	{
		this.change = change;
	}
}
