/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfaccommodationsaddon.forms.cms;

public class AddRequestForm
{
	private String requestMessage;
	private int roomStayRefNumber;

	public String getRequestMessage()
	{
		return requestMessage;
	}

	public void setRequestMessage(final String requestMessage)
	{
		this.requestMessage = requestMessage;
	}

	public int getRoomStayRefNumber()
	{
		return roomStayRefNumber;
	}

	public void setRoomStayRefNumber(final int roomStayRefNumber)
	{
		this.roomStayRefNumber = roomStayRefNumber;
	}


}
