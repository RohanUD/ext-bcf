/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = function() {

    return {
        targets: [
            'bcfsmartedit',
            'bcfsmarteditContainer',
            'bcfsmarteditcommons'
        ],
        config: function(data, conf) {
            return {
                bcfsmartedit: conf.unitSmartedit,
                bcfsmarteditContainer: conf.unitSmarteditContainer,
                bcfsmarteditcommons: {
                    options: {
                        configFile: global.smartedit.bundlePaths.external.generated.karma.smarteditCommons,
                    }
                }
            };
        }
    };

};
