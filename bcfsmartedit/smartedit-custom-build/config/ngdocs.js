/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = function() {

    return {
        targets: [
            'smartEdit',
            'smartEditContainer'
        ],
        config: function(data, conf) {
            return {
                options: {
                    dest: 'jsTarget/docs',
                    title: "bcfsmartedit API",
                    startPage: '/#/bcfsmartedit',
                },
                smartEdit: {
                    api: true,
                    src: [
                        'web/features/bcfsmartedit/**/*.+(js|ts)',
                        'web/features/bcfsmarteditcommons/**/*.+(js|ts)'
                    ],
                    title: 'bcfsmartedit'
                },
                smartEditContainer: {
                    api: true,
                    src: [
                        'web/features/bcfsmartedit/**/*.+(js|ts)',
                        'web/features/bcfsmarteditcommons/**/*.+(js|ts)'
                    ],
                    title: 'bcfsmarteditContainer'
                }
            };
        }
    };

};
