/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.sync;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.visitor.ItemVisitor;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfstorefrontaddon.model.components.BCFFooterComponentModel;
import com.bcf.core.util.StreamUtil;


public class BCFFooterComponentVisitor implements ItemVisitor<BCFFooterComponentModel>
{
	@Override
	public List<ItemModel> visit(final BCFFooterComponentModel source, final List<ItemModel> path,
			final Map<String, Object> ctx)
	{
		final List<ItemModel> items = StreamUtil.safeStream(source.getBottomLinks()).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(source.getLinksGroup()))
		{
			items.addAll(source.getLinksGroup());
		}
		if (Objects.nonNull(source.getNotice()))
		{
			items.add(source.getNotice());
		}
		return items;
	}
}
