/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.sync;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.visitor.ItemVisitor;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationNodeComponentModel;
import com.bcf.core.util.StreamUtil;


public class BCFNavigationNodeComponentVisitor implements ItemVisitor<BCFNavigationNodeComponentModel>
{
	@Override
	public List<ItemModel> visit(final BCFNavigationNodeComponentModel source, final List<ItemModel> path,
			final Map<String, Object> ctx)
	{
		final List<ItemModel> nestedItems = StreamUtil.safeStream(source.getChildLinks()).collect(Collectors.toList());
		Optional.ofNullable(source.getLink()).ifPresent(link ->
		{
			nestedItems.add(link);
		});
		Optional.ofNullable(source.getPromotionalLink()).ifPresent(promotionalLink ->
		{
			nestedItems.add(promotionalLink);
		});
		Optional.ofNullable(source.getPromotionalImage()).ifPresent(promotionalImage ->
		{
			nestedItems.add(promotionalImage);
		});
		return nestedItems;
	}
}
