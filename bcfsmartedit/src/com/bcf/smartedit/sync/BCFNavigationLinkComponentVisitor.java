/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.sync;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.visitor.ItemVisitor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationLinkComponentModel;


public class BCFNavigationLinkComponentVisitor implements ItemVisitor<BCFNavigationLinkComponentModel>
{
	@Override
	public List<ItemModel> visit(final BCFNavigationLinkComponentModel source, final List<ItemModel> path,
			final Map<String, Object> ctx)
	{
		final List<ItemModel> items = new ArrayList<>();
		if (Objects.nonNull(source.getLink()))
		{
			items.add(source.getLink());
		}
		if (Objects.nonNull(source.getMedia()))
		{
			items.add(source.getMedia());
		}
		return items;
	}
}
