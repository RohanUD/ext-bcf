/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.sync;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.visitor.ItemVisitor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.bcf.bcfstorefrontaddon.model.components.BCFMultimediaTextComponentModel;


public class BCFMultimediaTextComponentVisitor implements ItemVisitor<BCFMultimediaTextComponentModel>
{
	@Override
	public List<ItemModel> visit(final BCFMultimediaTextComponentModel source, final List<ItemModel> path,
			final Map<String, Object> ctx)
	{
		final List<ItemModel> items = new ArrayList<>();
		addRelatedItem(items, source.getMedia1());
		addRelatedItem(items, source.getMedia2());
		addRelatedItem(items, source.getMedia3());
		addRelatedItem(items, source.getMedia4());
		addRelatedItem(items, source.getMedia5());
		return items;
	}

	private void addRelatedItem(final List<ItemModel> items, final MediaModel mediaModel)
	{
		if (Objects.nonNull(mediaModel))
		{
			items.add(mediaModel);
		}
	}
}
