/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.sync;

import de.hybris.platform.acceleratorcms.model.components.LogoComponentModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.visitor.ItemVisitor;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class BCFLogoComponentVisitor implements ItemVisitor<LogoComponentModel>
{
	@Override
	public List<ItemModel> visit(final LogoComponentModel source, final List<ItemModel> path,
			final Map<String, Object> ctx)
	{
		if (Objects.nonNull(source.getLogo()))
		{
			return Arrays.asList(source.getLogo());
		}
		return Collections.emptyList();
	}
}
