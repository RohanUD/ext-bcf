/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.job;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.enums.CmsPageStatus;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.collector.RelatedItemsCollector;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.StreamUtil;
import com.bcf.smartedit.BCFCMSPageService;


public class BcfCmsItemsCleanupJob extends AbstractJobPerformable<CronJobModel>
{
	private RelatedItemsCollector relatedItemsCollector;
	private BCFCMSPageService bcfcmsPageService;
	private CatalogService catalogService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		final List<CatalogVersionModel> contentCatalogVersions = StreamUtil
				.safeStream(getCatalogService().getAllCatalogsOfType(ContentCatalogModel.class))
				.map(CatalogModel::getCatalogVersions)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
		final Collection<ContentPageModel> deletedContentPages = getBcfcmsPageService()
				.findAllContentPagesByCatalogVersionsAndPageStatuses(contentCatalogVersions, Arrays.asList(CmsPageStatus.DELETED));
		final List<Object> deletedPagesRelatedItems = StreamUtil.safeStream(deletedContentPages)
				.map(this::collectRelatedItems)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
		modelService.removeAll(deletedPagesRelatedItems);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private List<ItemModel> collectRelatedItems(final ContentPageModel contentPageModel)
	{
		return getRelatedItemsCollector().collect(contentPageModel, Collections.EMPTY_MAP);
	}

	public RelatedItemsCollector getRelatedItemsCollector()
	{
		return relatedItemsCollector;
	}

	@Required
	public void setRelatedItemsCollector(final RelatedItemsCollector relatedItemsCollector)
	{
		this.relatedItemsCollector = relatedItemsCollector;
	}

	public BCFCMSPageService getBcfcmsPageService()
	{
		return bcfcmsPageService;
	}

	@Required
	public void setBcfcmsPageService(final BCFCMSPageService bcfcmsPageService)
	{
		this.bcfcmsPageService = bcfcmsPageService;
	}

	public CatalogService getCatalogService()
	{
		return catalogService;
	}

	@Required
	public void setCatalogService(final CatalogService catalogService)
	{
		this.catalogService = catalogService;
	}
}
