/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.smartedit.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.smartedit.constants.BcfsmarteditConstants;

@SuppressWarnings("PMD")
public class BcfsmarteditManager extends GeneratedBcfsmarteditManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( BcfsmarteditManager.class.getName() );
	
	public static final BcfsmarteditManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfsmarteditManager) em.getExtension(BcfsmarteditConstants.EXTENSIONNAME);
	}
	
}
