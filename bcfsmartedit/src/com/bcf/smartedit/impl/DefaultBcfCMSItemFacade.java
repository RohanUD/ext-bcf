/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import bsh.EvalError;

import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cmsfacades.cmsitems.impl.DefaultCMSItemFacade;
import de.hybris.platform.cmsfacades.exception.ValidationException;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.workflow.ScriptEvaluationService;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.support.TransactionTemplate;


public class DefaultBcfCMSItemFacade extends DefaultCMSItemFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfCMSItemFacade.class);
	private static final String SAVE = "save";
	private static final String POSITION = "position";
	private static final String UID = "uid";
	private static final String CMSITEM_PREFIX = "cmsitem_";

	private ScriptEvaluationService scriptEvaluationService;
	private ConfigurationService configurationService;

	@Override
	protected Map<String, Object> saveItem(final Map<String, Object> itemMap)
	{
		return getSessionSearchRestrictionsDisabler().execute(() -> {
			try
			{
				setComponentPosition(itemMap);
				setCatalogInSession(itemMap);
				setCloneContext(itemMap);
				return new TransactionTemplate(getTransactionManager()).execute(status -> {
					final ItemModel itemModel = convertAndPopulate(itemMap);
					if (itemModel instanceof AbstractPageModel)
					{
						triggerWorkflowForPage((AbstractPageModel) itemModel);
					}
					getModelService().saveAll();
					return convertAndPopulate((CMSItemModel) itemModel);
				});
			}
			catch (final CMSItemNotFoundException e)
			{
				throw new RuntimeException(e);
			}
			catch (final ModelSavingException e)
			{
				LOG.info("Failed to save the item model", e);
				try
				{
					getValidationErrorsProvider().initializeValidationErrors();
					transformValidationException(e);
					throw new ValidationException(getValidationErrorsProvider().getCurrentValidationErrors());
				}
				finally
				{
					getValidationErrorsProvider().finalizeValidationErrors();
				}
			}
		});
	}

	@Override
	public Map<String, Object> createItem(final Map<String, Object> itemMap) throws CMSItemNotFoundException
	{
		if (!itemMap.containsKey(UID))
		{
			itemMap.put(UID, CMSITEM_PREFIX + getConfigurationService().getConfiguration()
					.getString("smartedit.cmsitems.env.prefix", StringUtils.EMPTY) + System.currentTimeMillis());
		}
		return super.createItem(itemMap);
	}

	private void setComponentPosition(final Map<String, Object> itemMap)
	{
		if (itemMap.get(POSITION) instanceof Integer && ((Integer) itemMap.get(POSITION)) < 0)
		{
			itemMap.put(POSITION, 0);
		}
	}

	private void triggerWorkflowForPage(final AbstractPageModel abstractPageModel)
	{
		if (!(abstractPageModel.getItemModelContext().getDirtyAttributes().contains(AbstractPageModel.APPROVALSTATUS)
				&& CmsApprovalStatus.CHECK.equals(abstractPageModel.getApprovalStatus())))
		{
			return;
		}
		final Map<String, CmsApprovalStatus> currentValues = new HashMap<>();
		currentValues.put(AbstractPageModel.APPROVALSTATUS, CmsApprovalStatus.CHECK);
		try
		{
			getScriptEvaluationService().evaluateActivationScripts(abstractPageModel, currentValues,
					Collections.emptyMap(), SAVE);
		}
		catch (final EvalError evalError)
		{
			LOG.error("Error in evaluating script for page " + abstractPageModel.getName(), evalError);
		}
	}



	public ScriptEvaluationService getScriptEvaluationService()
	{
		return scriptEvaluationService;
	}

	@Required
	public void setScriptEvaluationService(final ScriptEvaluationService scriptEvaluationService)
	{
		this.scriptEvaluationService = scriptEvaluationService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
