/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.cms2.cloning.service.impl.CMSItemModelCloneCreator;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.smartedit.constants.BcfsmarteditConstants;


public class BCFCmsItemModelCloneCreator extends CMSItemModelCloneCreator
{
	private ModelService modelService;

	public BCFCmsItemModelCloneCreator(final ModelService modelService, final I18NService i18nService,
			final TypeService typeService)
	{
		super(modelService, i18nService, typeService);
	}

	@Override
	protected void setAllAttributes(final ItemModel copy, final Collection<CopyAttribute> attributes)
	{
		super.setAllAttributes(copy, attributes);
		if (copy instanceof MediaModel)
		{
			final MediaModel mediaCopy = (MediaModel) copy;
			mediaCopy.setCode(getClonedAttributeValue(mediaCopy.getCode()));
		}
		if (copy instanceof CMSItemModel)
		{
			final CMSItemModel cmsItemCopy = (CMSItemModel) copy;
			cmsItemCopy.setUid(getClonedAttributeValue(cmsItemCopy.getUid()));
		}
	}

	private String getClonedAttributeValue(final String originalValue)
	{
		return String.join(BcfsmarteditConstants.CLONE_NAME_DELIM, originalValue, String.valueOf(System.currentTimeMillis()));
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
