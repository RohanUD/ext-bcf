/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.cmsfacades.catalogversions.impl.DefaultCatalogVersionFacade;
import de.hybris.platform.cmsfacades.data.CatalogVersionData;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import com.bcf.core.util.StreamUtil;


public class BCFCatalogVersionFacade extends DefaultCatalogVersionFacade
{
	@Override
	public List<CatalogVersionData> getWritableContentCatalogVersionTargets(final String siteId, final String catalogId,
			final String versionId)
	{
		return StreamUtil.safeStream(getCatalogService().getAllCatalogsOfType(ContentCatalogModel.class))
				.filter(catalogModel -> Objects.nonNull(catalogModel.getSuperCatalog()))
				.map(catalogModel -> super.getWritableContentCatalogVersionTargets(siteId, catalogModel.getId(), versionId))
				.flatMap(Collection::stream)
				.filter(catalogVersionData -> BooleanUtils.isFalse(catalogVersionData.getActive()))
				.collect(Collectors.toList());
	}
}
