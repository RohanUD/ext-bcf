/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import static de.hybris.platform.cmsfacades.common.validator.ValidationErrorBuilder.newValidationErrorBuilder;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_REQUIRED_L10N;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cmsfacades.cmsitems.validator.DefaultCMSLinkComponentValidator;
import de.hybris.platform.cmsfacades.constants.CmsfacadesConstants;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.util.StreamUtil;


public class BCFCmsLinkComponentValidator extends DefaultCMSLinkComponentValidator
{
	@Override
	public void validate(final CMSLinkComponentModel validatee)
	{
		verifyOnlyOneTypeProvided(validatee);
		StreamUtil.safeStream(getLanguageFacade().getLanguages())
				.filter(LanguageData::isRequired)
				.forEach(languageData -> {
					if (isBlank(validatee.getLinkName(getCommonI18NService().getLocaleForIsoCode(languageData.getIsocode()))))
					{
						getValidationErrorsProvider().getCurrentValidationErrors().add(
								newValidationErrorBuilder() //
										.field(CMSLinkComponentModel.LINKNAME) //
										.language(languageData.getIsocode())
										.errorCode(FIELD_REQUIRED_L10N) //
										.errorArgs(new Object[] { languageData.getIsocode() }) //
										.build()
						);
					}
				});
	}

	@Override
	protected void verifyOnlyOneTypeProvided(final CMSLinkComponentModel target)
	{
		final long count = Stream
				.of(target.getCategory(), target.getContentPage(), target.getProduct(), target.getUrl())
				.filter(item -> !isEmpty(item)).count();
		if (count > 1)
		{
			getValidationErrorsProvider().getCurrentValidationErrors().add(
					newValidationErrorBuilder() //
							.field(CMSLinkComponentModel.CONTENTPAGE)
							.errorCode(CmsfacadesConstants.LINK_ITEMS_EXCEEDED) //
							.build()
			);
			getValidationErrorsProvider().getCurrentValidationErrors().add(
					newValidationErrorBuilder() //
							.field(CMSLinkComponentModel.URL)
							.errorCode(CmsfacadesConstants.LINK_ITEMS_EXCEEDED) //
							.build()
			);
		}
		else if (count < 1)
		{
			getValidationErrorsProvider().getCurrentValidationErrors().add(
					newValidationErrorBuilder() //
							.field(CMSLinkComponentModel.CONTENTPAGE)
							.errorCode(CmsfacadesConstants.LINK_MISSING_ITEMS) //
							.build()
			);
			getValidationErrorsProvider().getCurrentValidationErrors().add(
					newValidationErrorBuilder() //
							.field(CMSLinkComponentModel.URL)
							.errorCode(CmsfacadesConstants.LINK_MISSING_ITEMS) //
							.build()
			);
		}
	}

	private boolean isEmpty(final Object item)
	{
		if (item instanceof String)
		{
			return StringUtils.isEmpty(item.toString());
		}
		return isNull(item);
	}
}
