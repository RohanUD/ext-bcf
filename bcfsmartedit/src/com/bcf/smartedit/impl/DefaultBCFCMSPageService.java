/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.enums.CmsPageStatus;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSPageService;
import java.util.Collection;
import java.util.List;
import com.bcf.smartedit.BCFCMSPageService;


public class DefaultBCFCMSPageService extends DefaultCMSPageService implements BCFCMSPageService
{
	@Override
	public Collection<ContentPageModel> findAllContentPagesByCatalogVersionsAndPageStatuses(
			final Collection<CatalogVersionModel> catalogVersions, final List<CmsPageStatus> pageStatuses)
	{
		return getCmsPageDao().findAllContentPagesByCatalogVersionsAndPageStatuses(catalogVersions, pageStatuses);
	}
}
