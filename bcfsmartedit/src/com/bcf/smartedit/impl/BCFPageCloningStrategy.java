/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.cms2.cloning.strategy.impl.PageCloningStrategy;
import de.hybris.platform.cms2.enums.CmsPageStatus;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.media.MediaModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;


public class BCFPageCloningStrategy extends PageCloningStrategy
{
	@Override
	protected Supplier<AbstractPageModel> clonePage(final AbstractPageModel sourcePageModel,
			final AbstractPageModel clonedPageModel,
			final Map<String, Object> context)
	{
		return () -> {
			final boolean cloningToDifferentCatalog = !Objects
					.equals(sourcePageModel.getCatalogVersion().getPk(), clonedPageModel.getCatalogVersion().getPk());
			if (cloningToDifferentCatalog)
			{
				sourcePageModel.setPageStatus(CmsPageStatus.DELETED);
			}
			clonedPageModel.setLocalizedPages(Collections.emptyList());
			final AbstractPageModel identicalPrimaryPage = this.getCmsAdminPageService()
					.getIdenticalPrimaryPageModel(clonedPageModel);
			final boolean shouldCloneComponents =
					getCmsItemCloningService().shouldCloneComponents(context) || cloningToDifferentCatalog;
			this.getCmsAdminContentSlotService().getContentSlotsForPage(sourcePageModel, false).forEach((contentSlotData) -> {
				this.cloneAndAddContentSlot(clonedPageModel, contentSlotData.getContentSlot(),
						shouldCloneComponents);
			});
			final Optional<MediaModel> previewImage = this.clonePreviewImage(sourcePageModel, clonedPageModel.getCatalogVersion());
			previewImage.ifPresent(clonedPageModel::setPreviewImage);
			if (identicalPrimaryPage != null && clonedPageModel.getDefaultPage())
			{
				identicalPrimaryPage.setPageStatus(CmsPageStatus.DELETED);
			}
			this.getModelService().saveAll();
			return clonedPageModel;
		};
	}

	@Override
	protected ContentSlotModel cloneAndAddContentSlot(final AbstractPageModel pageModel, final ContentSlotModel contentSlotModel,
			final boolean shouldCloneComponents)
	{
		final ContentSlotModel clonedContentSlot = this.getCmsAdminContentSlotService()
				.createContentSlot(pageModel, null, contentSlotModel.getName(), contentSlotModel.getCurrentPosition(),
						contentSlotModel.getActive(), contentSlotModel.getActiveFrom(), contentSlotModel.getActiveUntil());
		if (shouldCloneComponents)
		{
			this.getCmsItemCloningService()
					.cloneContentSlotComponents(contentSlotModel, clonedContentSlot, clonedContentSlot.getCatalogVersion());
		}
		else
		{
			clonedContentSlot.setCmsComponents(new ArrayList(contentSlotModel.getCmsComponents()));
		}
		return clonedContentSlot;
	}
}
