/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cmsfacades.data.PageContentSlotComponentData;
import de.hybris.platform.cmsfacades.pagescontentslotscomponents.impl.DefaultPageContentSlotComponentFacade;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Required;


public class BCFPageContentSlotComponentFacade extends DefaultPageContentSlotComponentFacade
{
	private CMSPageService cmsPageService;

	@Override
	public PageContentSlotComponentData moveComponent(final String pageUid, final String componentUid, final String originSlotUid,
			final PageContentSlotComponentData pageContentSlotComponentData) throws CMSItemNotFoundException
	{
		pageContentSlotComponentData.setPosition(getComponentPosition(pageContentSlotComponentData.getPosition(), originSlotUid));
		return super.moveComponent(pageUid, componentUid, originSlotUid, pageContentSlotComponentData);
	}

	@Override
	public PageContentSlotComponentData addComponentToContentSlot(final PageContentSlotComponentData pageContentSlotComponent)
			throws CMSItemNotFoundException
	{
		pageContentSlotComponent
				.setPosition(getComponentPosition(pageContentSlotComponent.getPosition(), pageContentSlotComponent.getSlotId()));
		return super.addComponentToContentSlot(pageContentSlotComponent);
	}

	private int getComponentPosition(final int existingPosition, final String contentSlotId)
	{
		if (existingPosition > 0)
		{
			return existingPosition;
		}
		final Collection<CatalogVersionModel> catalogVersions = getCatalogVersionService().getSessionCatalogVersions();
		return getAdminContentSlotService()
				.getContentSlotForIdAndCatalogVersions(contentSlotId, catalogVersions).getCmsComponents().size();
	}

	public CMSPageService getCmsPageService()
	{
		return cmsPageService;
	}

	@Required
	public void setCmsPageService(final CMSPageService cmsPageService)
	{
		this.cmsPageService = cmsPageService;
	}
}
