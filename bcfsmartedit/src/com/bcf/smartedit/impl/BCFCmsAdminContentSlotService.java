/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForPageModel;
import de.hybris.platform.cms2.servicelayer.services.admin.impl.DefaultCMSAdminContentSlotService;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;


public class BCFCmsAdminContentSlotService extends DefaultCMSAdminContentSlotService
{
	@Override
	public ContentSlotModel createContentSlot(
			final AbstractPageModel page, final String id, final String name, final String position, final boolean active,
			final Date activeFrom, final Date activeUntil)
	{
		final ContentSlotModel contentSlot = this.getModelService().create(ContentSlotModel.class);
		contentSlot.setUid(StringUtils.isNotBlank(id) ? id : generateUid(this.generateContentSlotUid(page, position)));
		contentSlot.setName(name);
		contentSlot.setActive(active);
		contentSlot.setActiveFrom(activeFrom);
		contentSlot.setActiveUntil(activeUntil);
		contentSlot.setCatalogVersion(page.getCatalogVersion());
		final ContentSlotForPageModel relation = this.getModelService()
				.create(ContentSlotForPageModel.class);
		relation.setUid(generateUid(this.generateContentSlotForPageUID()));
		relation.setCatalogVersion(page.getCatalogVersion());
		relation.setPosition(position);
		relation.setPage(page);
		relation.setContentSlot(contentSlot);
		return contentSlot;
	}

	private String generateUid(final String originalUid)
	{
		return getConfigurationService().getConfiguration()
				.getString("smartedit.cmsitems.env.prefix", org.apache.commons.lang.StringUtils.EMPTY) + System
				.currentTimeMillis() + originalUid;
	}
}
