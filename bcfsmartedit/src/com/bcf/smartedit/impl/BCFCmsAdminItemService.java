/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.servicelayer.services.admin.impl.DefaultCMSAdminItemService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.google.common.base.Strings;


public class BCFCmsAdminItemService extends DefaultCMSAdminItemService
{
	private ConfigurationService configurationService;

	@Override
	public CMSItemModel createItem(final Class modelClass)
	{
		final CMSItemModel model = this.getModelService().create(modelClass);
		if (Strings.isNullOrEmpty(model.getUid()))
		{
			model.setUid(
					getConfigurationService().getConfiguration().getString("smartedit.cmsitems.env.prefix", StringUtils.EMPTY) + System
							.currentTimeMillis() + this.generateCmsComponentUid());
		}
		return model;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
