/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.impl;

import static de.hybris.platform.cms2.model.contents.CMSItemModel.NAME;
import static de.hybris.platform.cms2.model.contents.CMSItemModel.UID;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_ALREADY_EXIST;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_CONTAINS_INVALID_CHARS;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_LENGTH_EXCEEDED;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_REQUIRED;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.logging.log4j.util.Strings.isBlank;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cmsfacades.cmsitems.validator.DefaultCreateCMSItemValidator;
import org.apache.commons.lang.StringUtils;


public class BCFCreateCMSItemValidator extends DefaultCreateCMSItemValidator
{
	@Override
	public void validate(final CMSItemModel itemModel)
	{
		if (isBlank(itemModel.getName()))
		{
			addError(itemModel, FIELD_REQUIRED, NAME, itemModel.getName());
		}

		if (isNotBlank(itemModel.getUid()) && !getOnlyHasSupportedCharactersPredicate().test(itemModel.getUid()))
		{
			addError(itemModel, FIELD_CONTAINS_INVALID_CHARS, UID, itemModel.getUid());
		}

		if (!getValidStringLengthPredicate().test(itemModel.getName()))
		{
			addError(itemModel, FIELD_LENGTH_EXCEEDED, NAME, itemModel.getName());
		}

		try
		{
			final CMSItemModel cmsItemByUid = getCmsAdminItemService().findByUid(itemModel.getUid(), itemModel.getCatalogVersion());

			if (!cmsItemByUid.getPk().equals(itemModel.getPk()))
			{
				addError(itemModel, FIELD_ALREADY_EXIST, UID, itemModel.getUid());
			}
		}
		catch (final CMSItemNotFoundException e)
		{
			// intentionally left empty
		}

		if (!StringUtils.isBlank(itemModel.getName()) && getCmsItemNameExistsPredicate().test(itemModel))
		{
			addError(itemModel, FIELD_ALREADY_EXIST, NAME, itemModel.getName());
		}
	}
}
