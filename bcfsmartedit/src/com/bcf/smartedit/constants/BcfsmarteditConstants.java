/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.smartedit.constants;

@SuppressWarnings("PMD")
public class BcfsmarteditConstants extends GeneratedBcfsmarteditConstants
{
	public static final String EXTENSIONNAME = "bcfsmartedit";

	private BcfsmarteditConstants()
	{
		// Intentionally left empty.
	}

	public static final String CLONE = "Clone";
	public static final String CLONE_NAME_DELIM = "_";

}
