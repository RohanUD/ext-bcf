/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.cloning;

import de.hybris.platform.core.model.ItemModel;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import org.springframework.beans.factory.annotation.Required;


public class BCFContentComponentsPartOfPredicate implements BiPredicate<ItemModel, String>
{
	private Map<String, List<String>> componentsCloningMap;

	@Override
	public boolean test(final ItemModel component, final String qualifier)
	{
		return getComponentsCloningMap().containsKey(component.getItemtype()) && getComponentsCloningMap()
				.get(component.getItemtype()).contains(qualifier);
	}

	public Map<String, List<String>> getComponentsCloningMap()
	{
		return componentsCloningMap;
	}

	@Required
	public void setComponentsCloningMap(final Map<String, List<String>> componentsCloningMap)
	{
		this.componentsCloningMap = componentsCloningMap;
	}
}
