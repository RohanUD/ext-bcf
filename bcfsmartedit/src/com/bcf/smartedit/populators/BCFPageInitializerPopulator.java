/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.smartedit.populators;

import static de.hybris.platform.cms2.enums.CmsApprovalStatus.UNAPPROVED;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cmsfacades.pages.service.PageInitializer;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;


public class BCFPageInitializerPopulator implements Populator<Map<String, Object>, AbstractPageModel>
{
	private PageInitializer pageInitializer;
	private ModelService modelService;

	@Override
	public void populate(final Map<String, Object> source, final AbstractPageModel abstractPageModel) throws ConversionException
	{
		if (getModelService().isNew(abstractPageModel))
		{
			abstractPageModel.setApprovalStatus(UNAPPROVED);
			getModelService().save(abstractPageModel);
			getPageInitializer().initialize(abstractPageModel);
		}
	}

	@Required
	public void setPageInitializer(final PageInitializer pageInitializer)
	{
		this.pageInitializer = pageInitializer;
	}

	protected PageInitializer getPageInitializer()
	{
		return pageInitializer;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}
}
