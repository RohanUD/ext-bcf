/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.smartedit.populators;

import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.cmsfacades.media.populator.CreateMediaPopulator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class BCFCreateMediaPopulator extends CreateMediaPopulator
{
	@Override
	public void populate(final MediaData source, final MediaModel target) throws ConversionException
	{
		super.populate(source, target);
		target.setCaption(source.getCaption());
		target.setLocationTag(source.getLocationTag());
		target.setFeaturedText(source.getFeaturedText());
		target.setIconText(source.getIconText());
		target.setPromoText(source.getPromoText());
		target.setConversionGroup(source.getConversionGroup());
	}
}
