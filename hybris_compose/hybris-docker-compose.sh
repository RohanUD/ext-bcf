#!/bin/bash
Tag=$1
HybirsImageName="nexus.stg.bcferries.corp:2021/ffdeihybris:$Tag"
export $HybirsImageName
export hostsystemip=$(hostname -i)
export SystemHostName=$(hostname -s)
checkimagenames=$(sed -n '4p'  docker-compose.yml)
if [[ -z "$checkimagenames" ]]
then 
 sed -i "4i\    image: "$HybirsImageName"" docker-compose.yml
else
sed -i '4d' docker-compose.yml
 sed -i "4i\    image: "$HybirsImageName"" docker-compose.yml
fi
checkhystatus=$(docker ps -a --filter "name=hybris"|awk {'print $1'}|awk 'FNR == 2 {print $1}')
if [[ -z "$checkhystatus" ]]
then
  echo "hybris is not  running we are starting up"
docker-compose up -d hybris
else
echo "hybris is alredy running we will stop it and remove it then start again"
docker-compose stop hybris
docker-compose kill hybris
docker-compose up -d hybris

fi
