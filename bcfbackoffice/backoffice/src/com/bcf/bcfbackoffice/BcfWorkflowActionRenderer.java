/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice;

import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import org.zkoss.zul.Listitem;
import com.hybris.backoffice.widgets.workflowactions.renderer.DefaultWorkflowActionsRenderer;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


public class BcfWorkflowActionRenderer extends DefaultWorkflowActionsRenderer
{
	@Override
	public void render(final Listitem listitem, final Object configuration, final WorkflowActionModel data,
			final DataType dataType,
			final WidgetInstanceManager wim)
	{
		if (CronJobStatus.ABORTED.equals(data.getWorkflow().getStatus()))
		{
			return;
		}
		super.render(listitem, configuration, data, dataType, wim);
	}
}
