/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfbackoffice;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.SaleStatusDataModel;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationOfferingCreateUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;


public class SaleStatusDataHelper
{
	private AccommodationOfferingCreateUtility accommodationOfferingCreateUtility;
	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;

	/**
	 * This method is required as we need to trigger AccommodationOfferingData upload service in order to update sale statuses to respective
	 * AccommodationOffering and Accommodation models.
	 *
	 * @param saleStatusData
	 * @throws ObjectSavingException
	 */
	public void updateAccommodationAndAccommodationOfferingWithSaleStatusChanges(final SaleStatusDataModel saleStatusData)
	{
		if (isSaleStatusAssociatedToAccommodation(saleStatusData))
		{
			AccommodationDataModel accommodationDataModel = saleStatusData.getAccommodationData();
			accommodationsDataUploadUtility.createOrUpdateSaleStatuses(accommodationDataModel, true);
		}
		else
		{
			AccommodationOfferingDataModel accommodationOfferingDataModel = saleStatusData.getAccommodationOfferingData();
			accommodationOfferingCreateUtility.createOrUpdateSaleStatuses(accommodationOfferingDataModel);
		}
	}

	private boolean isSaleStatusAssociatedToAccommodation(
			final SaleStatusDataModel saleStatusData)
	{
		return Objects.nonNull(saleStatusData.getAccommodationData());
	}

	@Required
	public void setAccommodationOfferingCreateUtility(
			final AccommodationOfferingCreateUtility accommodationOfferingCreateUtility)
	{
		this.accommodationOfferingCreateUtility = accommodationOfferingCreateUtility;
	}

	@Required
	public void setAccommodationsDataUploadUtility(
			final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}
}
