/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.integration.dataupload.utility.validator.accommodation.AccommodationCancellationFeesDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class AccommodationCancellationFeesDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final String MANDATORY = "mandatory.field";

	private AccommodationCancellationFeesDataValidator accommodationCancellationFeesDataValidator;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final AccommodationCancellationFeesDataModel accommodationCancellationFeesDataModel = (AccommodationCancellationFeesDataModel) currentObject;
		accommodationCancellationFeesDataModel.setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(accommodationCancellationFeesDataModel);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateAccommodationCancellationFeesData((AccommodationCancellationFeesDataModel) currentObject);
	}

	protected List<ValidationInfo> validateAccommodationCancellationFeesData(
			final AccommodationCancellationFeesDataModel accommodationCancellationFeesData)
	{
		final List<String> invalidAttributeList = getAccommodationCancellationFeesDataValidator()
				.validateAccommodationCancellationFeesData(accommodationCancellationFeesData);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (invalidAttributeList.isEmpty())
		{
			return invalidValues;
		}
		invalidAttributeList.stream()
				.forEach(invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, MANDATORY)));
		return invalidValues;
	}

	protected AccommodationCancellationFeesDataValidator getAccommodationCancellationFeesDataValidator()
	{
		return accommodationCancellationFeesDataValidator;
	}

	@Required
	public void setAccommodationCancellationFeesDataValidator(
			final AccommodationCancellationFeesDataValidator accommodationCancellationFeesDataValidator)
	{
		this.accommodationCancellationFeesDataValidator = accommodationCancellationFeesDataValidator;
	}
}
