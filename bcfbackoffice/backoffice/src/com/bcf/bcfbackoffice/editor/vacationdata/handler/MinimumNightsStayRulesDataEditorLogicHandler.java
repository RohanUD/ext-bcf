/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class MinimumNightsStayRulesDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		((MinimumNightsStayRulesDataModel) currentObject).setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(currentObject);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateMinimumNightsStayRulesDataModel((MinimumNightsStayRulesDataModel) currentObject);
	}

	protected List<ValidationInfo> validateMinimumNightsStayRulesDataModel(
			final MinimumNightsStayRulesDataModel minimumNightsStayRulesDataModel)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (Objects.isNull(minimumNightsStayRulesDataModel.getStartDate()))
		{
			invalidValues
					.add(getInvalidDataError(MinimumNightsStayRulesDataModel.STARTDATE, BcfbackofficeConstants.MANDATORY));
		}

		if (Objects.isNull(minimumNightsStayRulesDataModel.getEndDate()))
		{
			invalidValues
					.add(getInvalidDataError(MinimumNightsStayRulesDataModel.ENDDATE, BcfbackofficeConstants.MANDATORY));
		}

		if (Objects.isNull(minimumNightsStayRulesDataModel.getMinimumNightsStay()))
		{
			invalidValues
					.add(getInvalidDataError(MinimumNightsStayRulesDataModel.MINIMUMNIGHTSSTAY, BcfbackofficeConstants.MANDATORY));
		}

		return invalidValues;
	}
}
