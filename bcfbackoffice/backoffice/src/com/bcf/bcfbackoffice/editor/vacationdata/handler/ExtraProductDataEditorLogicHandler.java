/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.accommodations.ExtraProductDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.ExtraProductPricesDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ExtraProductDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Logger LOG = Logger.getLogger(ExtraProductDataEditorLogicHandler.class);
	private ExtraProductDataValidator extraProductDataValidator;
	private ExtraProductDataUploadUtility extraProductDataUploadUtility;
	private ExtraProductPricesDataUploadUtility extraProductPricesDataUploadUtility;
	private CatalogVersionService catalogVersionService;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final ExtraProductDataModel extraProductData = (ExtraProductDataModel) currentObject;
		extraProductData.setStatus(DataImportProcessStatus.PROCESSING);
		final List<ItemModel> items = new ArrayList<>();
		updateExtraProduct(extraProductData, items);
		extraProductData.getExtraProductPriceDataList()
				.forEach(extraProductPriceDataModel -> extraProductPricesDataUploadUtility.handleSuccess(extraProductPriceDataModel));
		extraProductDataUploadUtility.handleSuccess(extraProductData, items);
		getModelService().saveAll(extraProductData.getExtraProductPriceDataList());
		getModelService().save(extraProductData);
		return currentObject;
	}
	private void updateExtraProduct(final ExtraProductDataModel extraProductData, final List<ItemModel> items)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService()
				.getCatalogVersion(BcfbackofficeConstants.PRODUCTCATALOG, BcfbackofficeConstants.CATALOGVERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		if (extraProductData.isDeleted())
		{
			extraProductDataUploadUtility.markUnapproved(extraProductData, catalogVersion);
			return;
		}

		final Collection<ExtraProductPriceDataModel> extraProductPriceDataModelList = extraProductData.getExtraProductPriceDataList();
		extraProductPriceDataModelList.stream()
				.forEach(extraProductPriceDataModel -> extraProductPricesDataUploadUtility.truncateDates(extraProductPriceDataModel));

		final Transaction tx = Transaction.current();
		Boolean result = Boolean.FALSE;
		try
		{
			tx.begin();
			final List<ItemModel> itemsToSave = new ArrayList<>();

			for (final ExtraProductPriceDataModel extraProductPriceDataModel : extraProductPriceDataModelList)
			{
				extraProductPriceDataModel.setExtraProductData(extraProductData);
				itemsToSave.add(extraProductPriceDataModel);

			}

			itemsToSave.add(extraProductData);
			if(extraProductData.isPartialSave()){
				getModelService().saveAll(itemsToSave);
				tx.commit();
				result = Boolean.TRUE;
				return;
			}
			extraProductDataUploadUtility.uploadExtraProductData(Collections.singletonList(extraProductData),
					catalogVersion, items,itemsToSave,true);
			getModelService().saveAll(itemsToSave);
			tx.flushDelayedStore();
			extraProductPriceDataModelList.stream()
					.forEach(extraProductPriceDataModel -> {

						extraProductPricesDataUploadUtility
								.uploadExtraProductPrices(extraProductPriceDataModel, catalogVersion, items);
					});
			getModelService().saveAll();
			LOG.info("Transaction Commit");
			tx.commit();
			result = Boolean.TRUE;
		}
		catch (final ModelSavingException | IllegalArgumentException ex)
		{
			LOG.error("Transaction RollBack : " + ex);
			tx.rollback();
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}




	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateExtraProductData((ExtraProductDataModel) currentObject);
	}

	private List<ValidationInfo> validateExtraProductData(final ExtraProductDataModel extraProductData)
	{

		final List<String> invalidAttributeList = getExtraProductDataValidator().validateExtraProductData(extraProductData);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (!invalidAttributeList.isEmpty())
		{
			invalidAttributeList.stream().forEach(
					invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, BcfbackofficeConstants.MANDATORY)));
		}
		return invalidValues;
	}

	protected ExtraProductDataValidator getExtraProductDataValidator()
	{
		return extraProductDataValidator;
	}

	@Required
	public void setExtraProductDataValidator(
			final ExtraProductDataValidator extraProductDataValidator)
	{
		this.extraProductDataValidator = extraProductDataValidator;
	}

	protected ExtraProductDataUploadUtility getExtraProductDataUploadUtility()
	{
		return extraProductDataUploadUtility;
	}

	@Required
	public void setExtraProductDataUploadUtility(
			final ExtraProductDataUploadUtility extraProductDataUploadUtility)
	{
		this.extraProductDataUploadUtility = extraProductDataUploadUtility;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	public ExtraProductPricesDataUploadUtility getExtraProductPricesDataUploadUtility()
	{
		return extraProductPricesDataUploadUtility;
	}

	public void setExtraProductPricesDataUploadUtility(
			final ExtraProductPricesDataUploadUtility extraProductPricesDataUploadUtility)
	{
		this.extraProductPricesDataUploadUtility = extraProductPricesDataUploadUtility;
	}
}
