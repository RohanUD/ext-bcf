/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.widgets.vacationdata.handler.DealBundleDataCreationHandler;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.deals.DealsBundleDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class DealBundleDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final String VALIDATION_ERROR_TRANSPORTOFFERINGS_COUNT = "com.hybris.editorarea.validation.error.transportOfferings.count";
	private static final String VALIDATION_ERROR_INBOUND_TRAVELROUTE_NOT_CONFIG = "com.hybris.editorarea.validation.error.inBound.travelRoute.notConfig";
	private static final String VALIDATION_ERROR_TRANSPORTOFFERINGS_NOTMATCHING = "com.hybris.editorarea.validation.error.transportOfferings.travelRoute.notMatching";

	private DealsBundleDataUploadUtility dealsBundleDataUploadUtility;
	private ModelService modelService;
	private SessionService sessionService;
	private DealBundleDataCreationHandler dealBundleDataCreationHandler;
	private TravelRouteService travelRouteService;

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final DealsBundleDataModel dealBundleData = (DealsBundleDataModel) currentObject;
		dealBundleData.setStatus(DataImportProcessStatus.PENDING);
		getDealBundleDataCreationHandler().uploadData(widgetInstanceManager, dealBundleData);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		final DealsBundleDataModel dealBundleData = (DealsBundleDataModel) currentObject;
		return validateDealBundleData(dealBundleData);
	}

	/**
	 * Validate deal bundle data.
	 *
	 * @param dealBundleData the deal bundle data
	 * @return the list
	 */
	protected List<ValidationInfo> validateDealBundleData(final DealsBundleDataModel dealBundleData)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (StringUtils.isEmpty(dealBundleData.getBundleName(Locale.ENGLISH)))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.BUNDLENAME, BcfbackofficeConstants.MANDATORY));
		}
		if (StringUtils.isEmpty(dealBundleData.getStartingDatePattern()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.STARTINGDATEPATTERN, BcfbackofficeConstants.MANDATORY));
		}
		if (dealBundleData.getLength() <= 0)
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.LENGTH, BcfbackofficeConstants.MANDATORY));
		}
		if (CollectionUtils.isEmpty(dealBundleData.getGuestCounts()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.GUESTCOUNTS, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(dealBundleData.getTravelRoute()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.TRAVELROUTE, BcfbackofficeConstants.MANDATORY));
		}
		if (CollectionUtils.isNotEmpty(dealBundleData.getTransportOfferings()))
		{
			final String transportOfferingsValidationMsg = validateTransportOfferings(dealBundleData.getTravelRoute(),
					dealBundleData.getTransportOfferings());
			if (StringUtils.isNotEmpty(transportOfferingsValidationMsg))
			{
				invalidValues.add(getInvalidDataError(DealsBundleDataModel.TRANSPORTOFFERINGS, transportOfferingsValidationMsg));
			}
		}
		if (Objects.isNull(dealBundleData.getBundleType()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.BUNDLETYPE, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(dealBundleData.getAccommodationOffering()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.ACCOMMODATIONOFFERING, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(dealBundleData.getAccommodation()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.ACCOMMODATION, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(dealBundleData.getRatePlan()))
		{
			invalidValues.add(getInvalidDataError(DealsBundleDataModel.RATEPLAN, BcfbackofficeConstants.MANDATORY));
		}

		return invalidValues;
	}

	/**
	 * Validate transport offerings.
	 *
	 * @param outboundTravelRoute the outbound travel route
	 * @param transportOfferings  the transport offerings
	 * @return empty, if successful, else error code
	 */
	protected String validateTransportOfferings(final TravelRouteModel outboundTravelRoute,
			final Collection<TransportOfferingModel> transportOfferings)
	{
		if (CollectionUtils.size(transportOfferings) != 2)
		{
			return VALIDATION_ERROR_TRANSPORTOFFERINGS_COUNT;
		}
		final TransportOfferingModel outBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute().contains(outboundTravelRoute))
				.findFirst().get();

		final List<TravelRouteModel> travelRoutes = getTravelRouteService()
				.getTravelRoutes(outboundTravelRoute.getDestination().getCode(), outboundTravelRoute.getOrigin().getCode());
		if (CollectionUtils.isEmpty(travelRoutes))
		{
			return VALIDATION_ERROR_INBOUND_TRAVELROUTE_NOT_CONFIG;
		}
		final TravelRouteModel inboundTravelRoute = travelRoutes.stream().findFirst().get();
		final TransportOfferingModel inBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute().contains(inboundTravelRoute))
				.findFirst().get();

		if (Objects.isNull(outBoundTransportOffering) || Objects.isNull(inBoundTransportOffering))
		{
			return VALIDATION_ERROR_TRANSPORTOFFERINGS_NOTMATCHING;
		}
		return StringUtils.EMPTY;
	}

	/**
	 * @return the dealsBundleDataUploadUtility
	 */
	protected DealsBundleDataUploadUtility getDealsBundleDataUploadUtility()
	{
		return dealsBundleDataUploadUtility;
	}

	/**
	 * @param dealsBundleDataUploadUtility the dealsBundleDataUploadUtility to set
	 */
	@Required
	public void setDealsBundleDataUploadUtility(final DealsBundleDataUploadUtility dealsBundleDataUploadUtility)
	{
		this.dealsBundleDataUploadUtility = dealsBundleDataUploadUtility;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * Gets the session service.
	 *
	 * @return the session service
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Sets the session service.
	 *
	 * @param sessionService the new session service
	 */
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * Gets the deal bundle data creation handler.
	 *
	 * @return the deal bundle data creation handler
	 */
	protected DealBundleDataCreationHandler getDealBundleDataCreationHandler()
	{
		return dealBundleDataCreationHandler;
	}

	/**
	 * Sets the deal bundle data creation handler.
	 *
	 * @param dealBundleDataCreationHandler the new deal bundle data creation handler
	 */
	@Required
	public void setDealBundleDataCreationHandler(
			final DealBundleDataCreationHandler dealBundleDataCreationHandler)
	{
		this.dealBundleDataCreationHandler = dealBundleDataCreationHandler;
	}

	/**
	 * @return the travelRouteService
	 */
	protected TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	/**
	 * @param travelRouteService the travelRouteService to set
	 */
	@Required
	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}
}
