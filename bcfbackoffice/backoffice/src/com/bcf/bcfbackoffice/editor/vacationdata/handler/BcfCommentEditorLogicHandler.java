/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import com.bcf.core.model.accommodation.BcfCommentModel;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


public class BcfCommentEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final BcfCommentModel bcfComment = (BcfCommentModel) currentObject;
		final Date truncatedStartDate = DateUtils.truncate(bcfComment.getStartDate(), Calendar.DATE);
		bcfComment.setStartDate(truncatedStartDate);
		final java.util.Date truncatedEndDate = DateUtils.truncate(bcfComment.getEndDate(), Calendar.DATE);
		bcfComment.setEndDate(truncatedEndDate);
		return super.performSave(widgetInstanceManager, bcfComment);
	}
}
