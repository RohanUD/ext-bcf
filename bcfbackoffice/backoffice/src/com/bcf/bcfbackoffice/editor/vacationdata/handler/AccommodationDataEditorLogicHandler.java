/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.handler.AccommodationHandlerForWizard;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelStockService;
import com.bcf.integration.dataupload.dao.accommodations.RatePlanDataDao;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class AccommodationDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Logger LOG = Logger.getLogger(AccommodationDataEditorLogicHandler.class);

	private static final String STOCK_LEVEL_TYPE_ERROR = "stockleveltype.error";
	private static final String RATE_PLAN_NOT_UNIQUE_ERROR = "rateplan.not.unique.error";

	private AccommodationHandlerForWizard accommodationHandlerForWizard;

	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;

	private BcfTravelStockService bcfTravelStockService;

	private RatePlanDataDao ratePlanDataDao;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final AccommodationDataModel accommodationDataModel = ((AccommodationDataModel) currentObject);
		accommodationDataModel.setStatus(DataImportProcessStatus.PENDING);

		if (CollectionUtils.isNotEmpty(accommodationDataModel.getExtraGuestOccupancyProductDatas()))
		{
			accommodationDataModel.getExtraGuestOccupancyProductDatas().forEach(extraGuestOccupancyProductDataModel -> {
						extraGuestOccupancyProductDataModel.setAccommodationData(accommodationDataModel);
						extraGuestOccupancyProductDataModel
								.setCode(accommodationDataModel.getCode() + "_" + extraGuestOccupancyProductDataModel.getPassengerType()
										.getCode());
						getModelService().save(extraGuestOccupancyProductDataModel);
					}
			);
		}

		if (CollectionUtils.isNotEmpty(accommodationDataModel.getMinimumNightsStayRulesData()))
		{
			accommodationDataModel.getMinimumNightsStayRulesData().forEach(minimumNightsStayRulesData -> {
				minimumNightsStayRulesData.setAccommodationData(accommodationDataModel);
						getModelService().save(minimumNightsStayRulesData);
					}
			);
		}

		//In case of cloning
		if (getModelService().isNew(accommodationDataModel))
		{
			accommodationDataModel.getRatePlanDatas()
					.forEach(ratePlanDataModel -> ratePlanDataModel.setStatus((DataImportProcessStatus.PENDING)));
			final List<AccommodationDataModel> accommodationDataModelList = new ArrayList<>(
					accommodationDataModel.getAccommodationOffering().getAccommodationDatas());
			accommodationDataModelList.add(accommodationDataModel);
			accommodationDataModel.getAccommodationOffering().setAccommodationDatas(accommodationDataModelList);

			try
			{
				getAccommodationHandlerForWizard().uploadData(accommodationDataModel.getAccommodationOffering());
			}catch(ObjectSavingException ex){

				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
						NotificationEventTypes.EVENT_TYPE_OBJECT_UPDATE, NotificationEvent.Level.FAILURE,
						ex);
				throw ex;
			}
		}

		processStockLevelRemoveChanges(accommodationDataModel);
		processSaleStatusRemoveChanges(accommodationDataModel);

		getModelService().save(accommodationDataModel);
		getModelService().refresh(accommodationDataModel);
		accommodationsDataUploadUtility.uploadAccommodationsData();

		return currentObject;
	}

	/**
	 * This is the case when {@AccommodationStockLevelDataModel} is removed from AccommodationData from backoffice.
	 * In this case {@link com.bcf.integration.dataupload.utility.accommodations.AccommodationsStockLevelDataUploadUtility will not be able to detect these changes}.
	 * Thus, we need to handle this special case separately to remove the stock levels.
	 *
	 * @param accommodationDataModel
	 */
	private void processStockLevelRemoveChanges(final AccommodationDataModel accommodationDataModel)
	{
		if (accommodationDataModel.getItemModelContext().getDirtyAttributes()
				.contains(AccommodationDataModel.ACCOMMODATIONSTOCKLEVELDATALIST))
		{
			Collection<AccommodationStockLevelDataModel> originalStockLevelDatas = accommodationDataModel.getItemModelContext()
					.getOriginalValue(AccommodationDataModel.ACCOMMODATIONSTOCKLEVELDATALIST);

			Collection<AccommodationStockLevelDataModel> afterChangeStockLevelDatas = accommodationDataModel
					.getAccommodationStockLevelDataList();

			if (originalStockLevelDatas != null && afterChangeStockLevelDatas != null
					&& afterChangeStockLevelDatas.size() < originalStockLevelDatas.size())
			{
				List<AccommodationStockLevelDataModel> removedStockLevelDatas = originalStockLevelDatas.stream()
						.filter(o -> !afterChangeStockLevelDatas.contains(o))
						.collect(Collectors.toList());
				deleteRemovedStockLevels(removedStockLevelDatas);
			}
		}
	}

	private void deleteRemovedStockLevels(final List<AccommodationStockLevelDataModel> removedStockLevelDatas)
	{
		for (AccommodationStockLevelDataModel removedStockLevelData : removedStockLevelDatas)
		{
			Date endDate = Objects.nonNull(removedStockLevelData.getEndDate()) ?
					removedStockLevelData.getEndDate() :
					removedStockLevelData.getStartDate();

			List<StockLevelModel> accommodationStockLevels = bcfTravelStockService
					.getAccommodationStockLevels(removedStockLevelData.getStartDate(), endDate);

			AccommodationOfferingDataModel accommodationOffering = removedStockLevelData.getAccommodationData()
					.getAccommodationOffering();
			AccommodationDataModel accommodationData = removedStockLevelData.getAccommodationData();

			if (Objects.nonNull(accommodationOffering) && Objects.nonNull(accommodationData))
			{
				String stockCodeToDelete = accommodationOffering.getCode() + "_" + accommodationData.getCode();

				List<StockLevelModel> stocksToDelete = accommodationStockLevels.stream()
						.filter(stock -> stockCodeToDelete.equals(stock.getProductCode())).collect(Collectors.toList());

				LOG.info(String.format("Deleting %d stocks from Accommodation %s", accommodationStockLevels.size(),
						accommodationData.getCode()));
				getModelService().removeAll(stocksToDelete);
			}
		}
	}

	/**
	 * This is the case when {@SaleStatusData} is removed from AccommodationData from backoffice.
	 * In this case {@link com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility will not be able to detect these changes}.
	 * Thus, we need to handle this special case separately to remove the sale status.
	 *
	 * @param accommodationDataModel
	 */
	private void processSaleStatusRemoveChanges(final AccommodationDataModel accommodationDataModel)
	{
		if (accommodationDataModel.getItemModelContext().getDirtyAttributes()
				.contains(AccommodationDataModel.SALESTATUSES))
		{
			accommodationsDataUploadUtility.createOrUpdateSaleStatuses(accommodationDataModel, true);
		}
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateAccommodationData((AccommodationDataModel) currentObject);
	}

	protected List<ValidationInfo> validateAccommodationData(
			final AccommodationDataModel accommodationData)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (StringUtils.isEmpty(accommodationData.getCode()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.CODE, BcfbackofficeConstants.MANDATORY));
		}

		if (accommodationData.isPartialSave())
		{
			return invalidValues;
		}

		if (Objects.isNull(accommodationData.getAccommodationName()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.ACCOMMODATIONNAME, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationData.getBaseOccupancyCount()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.BASEOCCUPANCYCOUNT, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationData.getMaxOccupancyCount()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.MAXOCCUPANCYCOUNT, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationData.getStockLevelType()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.STOCKLEVELTYPE, BcfbackofficeConstants.MANDATORY));
		}

		if (!accommodationData.getStockLevelType().equals(StockLevelType.STANDARD) && CollectionUtils
				.isNotEmpty(accommodationData.getAccommodationStockLevelDataList()))
		{
			invalidValues.add(getInvalidDataError(AccommodationDataModel.STOCKLEVELTYPE, STOCK_LEVEL_TYPE_ERROR));
		}

		if (Objects.nonNull(accommodationData.getRatePlanDatas()))
		{
			for (final RatePlanDataModel ratePlanDataModel : accommodationData.getRatePlanDatas())
			{
				if (getModelService().isNew(ratePlanDataModel) && !hasUniqueCode(ratePlanDataModel))
				{
					invalidValues.add(getInvalidDataError(AccommodationDataModel.RATEPLANDATAS, RATE_PLAN_NOT_UNIQUE_ERROR));
				}
			}
		}

		return invalidValues;
	}

	private boolean hasUniqueCode(final RatePlanDataModel ratePlanDataModel)
	{
		return Objects.isNull(getRatePlanDataDao().findRatePlanData(ratePlanDataModel.getCode()));
	}

	public RatePlanDataDao getRatePlanDataDao()
	{
		return ratePlanDataDao;
	}

	public void setRatePlanDataDao(final RatePlanDataDao ratePlanDataDao)
	{
		this.ratePlanDataDao = ratePlanDataDao;
	}

	public AccommodationHandlerForWizard getAccommodationHandlerForWizard()
	{
		return accommodationHandlerForWizard;
	}

	public void setAccommodationHandlerForWizard(final AccommodationHandlerForWizard accommodationHandlerForWizard)
	{
		this.accommodationHandlerForWizard = accommodationHandlerForWizard;
	}

	@Required
	public void setAccommodationsDataUploadUtility(
			final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}

	@Required
	public void setBcfTravelStockService(final BcfTravelStockService bcfTravelStockService)
	{
		this.bcfTravelStockService = bcfTravelStockService;
	}
}
