/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.handler.AccommodationHandlerForWizard;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class AccommodationOfferingDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private AccommodationHandlerForWizard accommodationHandlerForWizard;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final AccommodationOfferingDataModel accommodationOfferingDataModel = (AccommodationOfferingDataModel) currentObject;
		accommodationOfferingDataModel.setStatus(DataImportProcessStatus.PENDING);


		accommodationOfferingDataModel
				.setDealOfTheDayStartDate(Objects.isNull(accommodationOfferingDataModel.getDealOfTheDayStartDate()) ?
						accommodationOfferingDataModel.getDealOfTheDayStartDate() :
						DateUtils.truncate(accommodationOfferingDataModel.getDealOfTheDayStartDate(), Calendar.DATE));
		accommodationOfferingDataModel
				.setDealOfTheDayEndDate(Objects.isNull(accommodationOfferingDataModel.getDealOfTheDayEndDate()) ?
						accommodationOfferingDataModel.getDealOfTheDayEndDate() :
						DateUtils.truncate(accommodationOfferingDataModel.getDealOfTheDayEndDate(), Calendar.DATE));
		getModelService().save(accommodationOfferingDataModel);

		if(CollectionUtils.isNotEmpty(accommodationOfferingDataModel.getAccommodationDatas())){
			Collection<AccommodationDataModel> accommodationDatas=accommodationOfferingDataModel.getAccommodationDatas();
			for (final AccommodationDataModel accommodationData : accommodationDatas)
			{
				accommodationData.setStatus(DataImportProcessStatus.PENDING);

				if(CollectionUtils.isNotEmpty(accommodationData.getRatePlanDatas())){

					for(RatePlanDataModel ratePlanDataModel:accommodationData.getRatePlanDatas()){

						ratePlanDataModel.setStatus(DataImportProcessStatus.PENDING);
					}
				}
			}
			getModelService().saveAll();
		}

		try
		{
			getAccommodationHandlerForWizard().uploadData(accommodationOfferingDataModel);
		}catch(ObjectSavingException ex){

			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
					NotificationEventTypes.EVENT_TYPE_OBJECT_UPDATE, NotificationEvent.Level.FAILURE,
					ex);
			throw ex;
		}

		if (DataImportProcessStatus.FAILED.equals(accommodationOfferingDataModel.getStatus()))
		{
			throw new ObjectSavingException(accommodationOfferingDataModel.getStatusDescription(), new Throwable());
		}
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateAccommodationOfferingData((AccommodationOfferingDataModel) currentObject);
	}

	protected List<ValidationInfo> validateAccommodationOfferingData(
			final AccommodationOfferingDataModel accommodationOfferingData)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (StringUtils.isEmpty(accommodationOfferingData.getCode()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.CODE, BcfbackofficeConstants.MANDATORY));
		}
		if (accommodationOfferingData.isPartialSave())
		{
			return invalidValues;
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getPropertyName(Locale.ENGLISH)))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.PROPERTYNAME, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationOfferingData.getPropertyType()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.PROPERTYTYPE, BcfbackofficeConstants.MANDATORY));
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getProvider()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.PROVIDER, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationOfferingData.getTown()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.TOWN, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(accommodationOfferingData.getCountry()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.COUNTRY, BcfbackofficeConstants.MANDATORY));
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getTimeZoneId()))
		{
			invalidValues.add(getInvalidDataError(AccommodationOfferingDataModel.TIMEZONEID, BcfbackofficeConstants.MANDATORY));
		}
		if (CollectionUtils.isEmpty(accommodationOfferingData.getAccommodationDatas()))
		{
			invalidValues
					.add(getInvalidDataError(AccommodationOfferingDataModel.ACCOMMODATIONDATAS, BcfbackofficeConstants.MANDATORY));
		}
		return invalidValues;
	}

	protected AccommodationHandlerForWizard getAccommodationHandlerForWizard()
	{
		return accommodationHandlerForWizard;
	}

	@Required
	public void setAccommodationHandlerForWizard(
			final AccommodationHandlerForWizard accommodationHandlerForWizard)
	{
		this.accommodationHandlerForWizard = accommodationHandlerForWizard;
	}
}
