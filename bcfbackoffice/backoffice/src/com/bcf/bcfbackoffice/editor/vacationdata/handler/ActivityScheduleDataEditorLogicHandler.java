/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityScheduleDataModel;
import com.bcf.integration.dataupload.utility.activities.ActivityScheduleDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ActivityScheduleDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Pattern TIME_REGEX = Pattern.compile("^([0-1][0-9]|[2][0-3]):([0-5][0-9])$");
	private static final String ACTIVITY_SCHEDULE_DATA_STARTTIME_ERROR_MSG = "com.hybris.cockpitng.widgets.configurableflow.create.ActivityScheduleDataModel.starttime.message";
	private static final String ACTIVITY_SCHEDULE_DATA_CHECKINTIME_ERROR_MSG = "com.hybris.cockpitng.widgets.configurableflow.create.ActivityScheduleDataModel.checkintime.message";
	private static final String ACTIVITY_SCHEDULE_DATA_VALUE_CHANGED ="com.hybris.cockpitng.widgets.configurableflow.create.ActivityScheduleDataModel.valuechanged";

	ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue(ACTIVITY_SCHEDULE_DATA_VALUE_CHANGED, Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final ActivityScheduleDataModel activityScheduleData = (ActivityScheduleDataModel) currentObject;
		final Date truncatedStartDate = DateUtils.truncate(activityScheduleData.getStartDate(), Calendar.DATE);
		activityScheduleData.setStartDate(truncatedStartDate);
		final Date truncatedEndDate = DateUtils.truncate(activityScheduleData.getEndDate(), Calendar.DATE);
		activityScheduleData.setEndDate(truncatedEndDate);
		activityScheduleData.setStartTime(activityScheduleData.getStartTime());
		activityScheduleData.setCheckinTime(activityScheduleData.getCheckinTime());

		activityScheduleData.setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(activityScheduleData);
		getModelService().refresh(activityScheduleData);
		activityScheduleDataUploadUtility.uploadActivityScheduleData();
		return activityScheduleData;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		final ActivityScheduleDataModel activityScheduleDataModel = (ActivityScheduleDataModel) currentObject;
		return activityScheduleData(activityScheduleDataModel);
	}

	protected List<ValidationInfo> activityScheduleData(final ActivityScheduleDataModel activityScheduleData)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		final String startTime = activityScheduleData.getStartTime();
		if (!validateTime(startTime))
		{
			invalidValues.add(getInvalidDataError(ActivityScheduleDataModel.STARTTIME,
					ACTIVITY_SCHEDULE_DATA_STARTTIME_ERROR_MSG));
		}
		final String checkInTime = activityScheduleData.getCheckinTime();
		if (!validateTime(checkInTime))
		{
			invalidValues.add(getInvalidDataError(ActivityScheduleDataModel.CHECKINTIME,
					ACTIVITY_SCHEDULE_DATA_CHECKINTIME_ERROR_MSG));
		}

		return invalidValues;
	}

	private boolean validateTime(final String time)
	{
		final Matcher matcher = TIME_REGEX.matcher(time);
		return matcher.matches();
	}

	@Required
	public void setActivityScheduleDataUploadUtility(
			final ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility)
	{
		this.activityScheduleDataUploadUtility = activityScheduleDataUploadUtility;
	}
}
