/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationFeesDataModel;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityCancellationFeesDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ActivityCancellationFeesDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{


	private static final String MANDATORY = "mandatory.field";


	private ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator;



	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}


	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		((ActivityCancellationFeesDataModel) currentObject).setStatus(DataImportProcessStatus.PENDING);
		return super.performSave(widgetInstanceManager,currentObject);
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateCancellationFees((ActivityCancellationFeesDataModel) currentObject);
	}

	private List<ValidationInfo> validateCancellationFees(final ActivityCancellationFeesDataModel cancellationFeesDataModel)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		final List<String> invalidAttributeList = getActivityCancellationFeesDataValidator()
				.validateCancellationFeesData(cancellationFeesDataModel);
		if (invalidAttributeList.isEmpty())
		{
			return invalidValues;
		}
		invalidAttributeList.stream()
				.forEach(invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, MANDATORY)));

		return invalidValues;

	}

	public ActivityCancellationFeesDataValidator getActivityCancellationFeesDataValidator()
	{
		return activityCancellationFeesDataValidator;
	}

	public void setActivityCancellationFeesDataValidator(
			final ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator)
	{
		this.activityCancellationFeesDataValidator = activityCancellationFeesDataValidator;
	}
}
