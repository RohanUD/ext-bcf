/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import com.hybris.cockpitng.validation.impl.DefaultValidationInfo;
import com.hybris.cockpitng.validation.model.ValidationSeverity;
import com.hybris.cockpitng.widgets.baseeditorarea.DefaultEditorAreaLogicHandler;


public class AbstractDataEditorLogicHandler extends DefaultEditorAreaLogicHandler
{
	private ModelService modelService;

	protected DefaultValidationInfo getInvalidDataError(final String propertyName, final String message)
	{
		final DefaultValidationInfo validationInfo = new DefaultValidationInfo();
		validationInfo.setConfirmed(false);
		validationInfo.setValidationSeverity(ValidationSeverity.ERROR);
		validationInfo.setInvalidPropertyPath(propertyName);
		validationInfo.setValidationMessage(Labels.getLabel(message));
		return validationInfo;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
