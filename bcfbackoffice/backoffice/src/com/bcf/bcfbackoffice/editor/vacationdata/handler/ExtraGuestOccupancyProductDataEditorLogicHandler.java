/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.bcfintegrationservice.model.utility.ExtraGuestOccupancyProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ExtraGuestOccupancyProductDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		((ExtraGuestOccupancyProductDataModel) currentObject).setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(currentObject);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateExtraOccupancyProduct((ExtraGuestOccupancyProductDataModel) currentObject);
	}

	protected List<ValidationInfo> validateExtraOccupancyProduct(
			final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (Objects.isNull(extraGuestOccupancyProductDataModel.getPassengerType()))
		{
			invalidValues
					.add(getInvalidDataError(ExtraGuestOccupancyProductDataModel.PASSENGERTYPE, BcfbackofficeConstants.MANDATORY));
		}

		if (Objects.isNull(extraGuestOccupancyProductDataModel.getPrice()) || extraGuestOccupancyProductDataModel.getPrice() <= 0d)
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.PRICE, BcfbackofficeConstants.MANDATORY));
		}

		return invalidValues;
	}
}
