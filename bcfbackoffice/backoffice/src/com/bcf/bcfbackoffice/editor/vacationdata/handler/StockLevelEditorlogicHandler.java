/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.handler.StockLevelHandler;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


public class StockLevelEditorlogicHandler extends AbstractDataEditorLogicHandler
{
	private StockLevelHandler stockLevelHandler;

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final StockLevelModel stockLevelModel = (StockLevelModel) currentObject;
		getStockLevelHandler().updateStockLevel(stockLevelModel);
		getModelService().save(stockLevelModel);
		return stockLevelModel;
	}

	protected StockLevelHandler getStockLevelHandler()
	{
		return stockLevelHandler;
	}

	@Required
	public void setStockLevelHandler(final StockLevelHandler stockLevelHandler)
	{
		this.stockLevelHandler = stockLevelHandler;
	}
}
