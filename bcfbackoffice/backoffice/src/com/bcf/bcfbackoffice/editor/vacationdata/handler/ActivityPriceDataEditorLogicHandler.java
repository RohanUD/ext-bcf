/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.activities.ActivityPricesDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityPriceDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ActivityPriceDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	ActivityPricesDataUploadUtility activityPricesDataUploadUtility;
	private ActivityPriceDataValidator activityPriceDataValidator;
	private ModelService modelService;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final ActivityPriceDataModel activityPriceDataModel = (ActivityPriceDataModel) currentObject;
		activityPriceDataModel.setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(activityPriceDataModel);
		getModelService().refresh(activityPriceDataModel);
		activityPricesDataUploadUtility.uploadActivityPriceData();
		return activityPriceDataModel;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateActivityPriceData((ActivityPriceDataModel) currentObject);
	}

	private List<ValidationInfo> validateActivityPriceData(final ActivityPriceDataModel activityPriceDataModel)
	{
		final List<String> invalidAttributeList = getActivityPriceDataValidator().validateActivityPriceData(activityPriceDataModel);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (invalidAttributeList.isEmpty())
		{
			return invalidValues;
		}
		invalidAttributeList.stream()
				.forEach(
						invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, BcfbackofficeConstants.MANDATORY)));
		return invalidValues;
	}

	protected ActivityPriceDataValidator getActivityPriceDataValidator()
	{
		return activityPriceDataValidator;
	}

	@Required
	public void setActivityPriceDataValidator(
			final ActivityPriceDataValidator activityPriceDataValidator)
	{
		this.activityPriceDataValidator = activityPriceDataValidator;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setActivityPricesDataUploadUtility(
			final ActivityPricesDataUploadUtility activityPricesDataUploadUtility)
	{
		this.activityPricesDataUploadUtility = activityPricesDataUploadUtility;
	}
}
