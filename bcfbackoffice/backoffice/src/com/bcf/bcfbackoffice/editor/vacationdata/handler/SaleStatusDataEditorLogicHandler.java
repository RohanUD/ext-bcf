/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.SaleStatusDataHelper;
import com.bcf.bcfintegrationservice.model.utility.SaleStatusDataModel;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


public class SaleStatusDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{

	SaleStatusDataHelper saleStatusDataHelper;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final SaleStatusDataModel saleStatusData = (SaleStatusDataModel) currentObject;
		final Date truncatedStartDate = DateUtils.truncate(saleStatusData.getStartDate(), Calendar.DATE);
		saleStatusData.setStartDate(truncatedStartDate);

		final Date truncateEndDate = DateUtils.truncate(saleStatusData.getEndDate(), Calendar.DATE);
		saleStatusData.setEndDate(truncateEndDate);

		getModelService().save(saleStatusData);
		try
		{
			getSaleStatusDataHelper().updateAccommodationAndAccommodationOfferingWithSaleStatusChanges(saleStatusData);
		}
		catch (final Exception ex)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
					NotificationEventTypes.EVENT_TYPE_OBJECT_UPDATE, NotificationEvent.Level.FAILURE,
					ex);
		}
		return saleStatusData;
	}

	public SaleStatusDataHelper getSaleStatusDataHelper()
	{
		return saleStatusDataHelper;
	}

	@Required
	public void setSaleStatusDataHelper(final SaleStatusDataHelper saleStatusDataHelper)
	{
		this.saleStatusDataHelper = saleStatusDataHelper;
	}
}
