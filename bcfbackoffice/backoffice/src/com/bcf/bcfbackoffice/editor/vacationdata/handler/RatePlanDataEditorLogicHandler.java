/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.handler.AccommodationHandlerForWizard;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.bcfintegrationservice.utilitydata.strategy.RatePlanCodeGenerateStrategy;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class RatePlanDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";
	private static final String UNDERSCORE = "_";

	private AccommodationHandlerForWizard accommodationHandlerForWizard;
	private RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final RatePlanDataModel ratePlanDataModel = (RatePlanDataModel) currentObject;
		ratePlanDataModel.setStatus(DataImportProcessStatus.PENDING);
		if (Objects.nonNull(ratePlanDataModel.getAccommodation()) && getModelService().isNew(ratePlanDataModel.getAccommodation()))
		{
			ratePlanDataModel.setAccommodation(null);
		}

		if (StringUtils.isEmpty(ratePlanDataModel.getCode())
				|| (StringUtils.countMatches(ratePlanDataModel.getCode(), UNDERSCORE) < 3))
		{
			ratePlanDataModel.setCode(getRatePlanCodeGenerateStrategy().getCode(ratePlanDataModel));
		}

		ratePlanDataModel.setRatePlanStartDate(Objects.isNull(ratePlanDataModel.getRatePlanStartDate()) ?
				ratePlanDataModel.getRatePlanStartDate() :
				DateUtils.truncate(ratePlanDataModel.getRatePlanStartDate(), Calendar.DATE));
		ratePlanDataModel.setRatePlanEndDate(Objects.isNull(ratePlanDataModel.getRatePlanEndDate()) ?
				ratePlanDataModel.getRatePlanEndDate() :
				DateUtils.truncate(ratePlanDataModel.getRatePlanEndDate(), Calendar.DATE));
		getModelService().save(ratePlanDataModel);

		//In case of cloning
		if (getModelService().isNew(ratePlanDataModel))
		{
			if (CollectionUtils.isNotEmpty(ratePlanDataModel.getAccommodation().getRatePlanDatas()))
			{
				final AccommodationDataModel accommodationData = ratePlanDataModel.getAccommodationOffering().getAccommodationDatas()
						.stream().filter(accommodationDataModel -> accommodationDataModel.equals(ratePlanDataModel.getAccommodation()))
						.findFirst().orElse(null);
				final List<RatePlanDataModel> ratePlanDataModels = new ArrayList<>(accommodationData.getRatePlanDatas());
				ratePlanDataModels.add(ratePlanDataModel);
				accommodationData.setRatePlanDatas(ratePlanDataModels);
				accommodationData.setStatus(DataImportProcessStatus.PENDING);
			}
			try
			{
				getAccommodationHandlerForWizard().uploadData(ratePlanDataModel.getAccommodationOffering());
			}catch(ObjectSavingException ex){

				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
						NotificationEventTypes.EVENT_TYPE_OBJECT_UPDATE, NotificationEvent.Level.FAILURE,
						ex);
				throw ex;
			}

		}
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateRatePlanData((RatePlanDataModel) currentObject);
	}

	protected List<ValidationInfo> validateRatePlanData(
			final RatePlanDataModel ratePlanData)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (StringUtils.isEmpty(ratePlanData.getCode()))
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.NAME, BcfbackofficeConstants.MANDATORY));
		}
		if (ratePlanData.isPartialSave())
		{
			return invalidValues;
		}
		if (Objects.isNull(ratePlanData.getRatePlanStartDate()))
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.RATEPLANSTARTDATE, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(ratePlanData.getRatePlanEndDate()))
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.RATEPLANENDDATE, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.nonNull(ratePlanData.getRatePlanEndDate()) && Objects.nonNull(ratePlanData.getRatePlanStartDate())
				&& ratePlanData.getRatePlanEndDate().before(ratePlanData.getRatePlanStartDate()))
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.RATEPLANENDDATE, END_DATE_BEFORE_START_DATE));
		}
		if (Objects.isNull(ratePlanData.getPrice()))
		{
			invalidValues.add(getInvalidDataError(RatePlanDataModel.PRICE, BcfbackofficeConstants.MANDATORY));
		}

		return invalidValues;
	}


	public AccommodationHandlerForWizard getAccommodationHandlerForWizard()
	{
		return accommodationHandlerForWizard;
	}

	public void setAccommodationHandlerForWizard(
			final AccommodationHandlerForWizard accommodationHandlerForWizard)
	{
		this.accommodationHandlerForWizard = accommodationHandlerForWizard;
	}

	/**
	 * @return the ratePlanCodeGenerateStrategy
	 */
	protected RatePlanCodeGenerateStrategy getRatePlanCodeGenerateStrategy()
	{
		return ratePlanCodeGenerateStrategy;
	}

	/**
	 * @param ratePlanCodeGenerateStrategy the ratePlanCodeGenerateStrategy to set
	 */
	@Required
	public void setRatePlanCodeGenerateStrategy(final RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy)
	{
		this.ratePlanCodeGenerateStrategy = ratePlanCodeGenerateStrategy;
	}
}
