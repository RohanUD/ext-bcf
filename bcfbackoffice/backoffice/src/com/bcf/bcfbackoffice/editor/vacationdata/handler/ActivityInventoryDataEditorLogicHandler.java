/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityInventoryDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ActivityInventoryDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final String MANDATORY = "mandatory.field";

	private ActivityInventoryDataValidator activityInventoryDataValidator;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final ActivityInventoryDataModel activityInventoryData = (ActivityInventoryDataModel) currentObject;
		activityInventoryData.setStatus(DataImportProcessStatus.PENDING);
		activityInventoryData.setStartDate(Objects.isNull(activityInventoryData.getStartDate()) ?
				activityInventoryData.getStartDate() :
				DateUtils.truncate(activityInventoryData.getStartDate(), Calendar.DATE));
		activityInventoryData.setEndDate(Objects.isNull(activityInventoryData.getEndDate()) ?
				activityInventoryData.getEndDate() :
				DateUtils.truncate(activityInventoryData.getEndDate(), Calendar.DATE));
		getModelService().save(activityInventoryData);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateActivityInventoryData((ActivityInventoryDataModel) currentObject);
	}

	private List<ValidationInfo> validateActivityInventoryData(final ActivityInventoryDataModel activityInventoryDataModel)
	{
		final List<String> invalidAttributeList = getActivityInventoryDataValidator()
				.validateActivityInventoryData(activityInventoryDataModel);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (invalidAttributeList.isEmpty())
		{
			return invalidValues;
		}
		invalidAttributeList.stream()
				.forEach(invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, MANDATORY)));
		return invalidValues;
	}

	protected ActivityInventoryDataValidator getActivityInventoryDataValidator()
	{
		return activityInventoryDataValidator;
	}

	@Required
	public void setActivityInventoryDataValidator(
			final ActivityInventoryDataValidator activityInventoryDataValidator)
	{
		this.activityInventoryDataValidator = activityInventoryDataValidator;
	}

}
