/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import com.bcf.core.model.SaleStatusModel;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


public class SaleStatusEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final SaleStatusModel saleStatus = (SaleStatusModel) currentObject;

		final Date truncatedStartDate = DateUtils.truncate(saleStatus.getStartDate(), Calendar.DATE);
		saleStatus.setStartDate(truncatedStartDate);

		final Date truncateEndDate = DateUtils.truncate(saleStatus.getEndDate(), Calendar.DATE);
		saleStatus.setEndDate(truncateEndDate);

		return super.performSave(widgetInstanceManager, saleStatus);
	}
}
