/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductPriceDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;



public class ExtraProductPriceDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private ExtraProductPriceDataValidator extraProductPriceDataValidator;
	private ModelService modelService;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final ExtraProductPriceDataModel extraProductPriceDataModel = (ExtraProductPriceDataModel) currentObject;
		extraProductPriceDataModel.setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(extraProductPriceDataModel);
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateExtraProductPrice((ExtraProductPriceDataModel) currentObject);
	}

	private List<ValidationInfo> validateExtraProductPrice(final ExtraProductPriceDataModel extraProductPriceDataModel)
	{
		final List<String> invalidAttributeList = extraProductPriceDataValidator.validateExtraProductPriceData(extraProductPriceDataModel);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (invalidAttributeList.isEmpty())
		{
			return invalidValues;
		}
		invalidAttributeList.stream()
				.forEach(
						invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, StringUtils.isNotEmpty(extraProductPriceDataModel.getStatusDescription())?extraProductPriceDataModel.getStatusDescription():BcfbackofficeConstants.MANDATORY)));
		return invalidValues;
	}

	public ExtraProductPriceDataValidator getExtraProductPriceDataValidator()
	{
		return extraProductPriceDataValidator;
	}

	public void setExtraProductPriceDataValidator(
			final ExtraProductPriceDataValidator extraProductPriceDataValidator)
	{
		this.extraProductPriceDataValidator = extraProductPriceDataValidator;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
