/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.core.accommodation.service.AccommodationCancellationFeesService;
import com.bcf.core.accommodation.service.AccommodationCancellationPolicyService;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationPolicyDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class AccommodationCancellationPolicyDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Logger LOG = Logger.getLogger(AccommodationCancellationPolicyDataEditorLogicHandler.class);

	public static final String SPACE = " ";
	public static final String UNDERSCORE = "_";

	private AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService;
	private AccommodationCancellationPolicyService accommodationCancellationPolicyService;
	private AccommodationCancellationFeesService accommodationCancellationFeesService;
	private List<Locale> localeList;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyDataModel = (AccommodationCancellationPolicyDataModel) currentObject;
		accommodationCancellationPolicyDataModel
				.setStartDate(Objects.isNull(accommodationCancellationPolicyDataModel.getStartDate()) ?
						accommodationCancellationPolicyDataModel.getStartDate() :
						DateUtils.truncate(accommodationCancellationPolicyDataModel.getStartDate(), Calendar.DATE));
		accommodationCancellationPolicyDataModel.setEndDate(Objects.isNull(accommodationCancellationPolicyDataModel.getEndDate()) ?
				accommodationCancellationPolicyDataModel.getEndDate() :
				DateUtils.truncate(accommodationCancellationPolicyDataModel.getEndDate(), Calendar.DATE));
		accommodationCancellationPolicyDataModel.setStatus(DataImportProcessStatus.PENDING);
		final int accommodationCancellationPolicyFeesDataModelSize = accommodationCancellationPolicyDataModel
				.getAccommodationCancellationFeesData().size();
		LOG.debug("accommodationCancellationPolicyFeesDataModelSize FeesData size : "
				+ accommodationCancellationPolicyFeesDataModelSize);

		//Get AccommodationCancellationPolicyDataModel from Database
		final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData = getAccommodationCancellationPolicyDataService()
				.getAccommodationCancellationPolicyDataByCode(accommodationCancellationPolicyDataModel.getCode());
		 int accommodationCancellationPolicyFeesDataSize = 0;
		if(CollectionUtils.isNotEmpty(accommodationCancellationPolicyData.getAccommodationCancellationFeesData())){
			accommodationCancellationPolicyFeesDataSize=accommodationCancellationPolicyData
					.getAccommodationCancellationFeesData().size();
		}

		LOG.debug("accommodationCancellationPolicyFeesDataSize FeesData size : " + accommodationCancellationPolicyFeesDataSize);
		if (accommodationCancellationPolicyFeesDataSize > accommodationCancellationPolicyFeesDataModelSize)
		{
			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesData = new ArrayList<>(
					(List) accommodationCancellationPolicyData.getAccommodationCancellationFeesData());
			accommodationCancellationFeesData
					.removeAll(accommodationCancellationPolicyDataModel.getAccommodationCancellationFeesData());
			LOG.debug("accommodationCancellationFeesData size after removeAll : "
					+ accommodationCancellationFeesData.size());

			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesList = new ArrayList<>();
			for (final AccommodationCancellationFeesDataModel accommodationCancellationFees : accommodationCancellationFeesData)
			{
				if (!accommodationCancellationFees.getStatus().equals(DataImportProcessStatus.FAILED))
				{
					accommodationCancellationFeesList.add(accommodationCancellationFees);
				}
			}
			if (!accommodationCancellationFeesList.isEmpty())
			{
				removeFeeAndUpdatePolicy(accommodationCancellationPolicyData, accommodationCancellationFeesList,
						accommodationCancellationPolicyDataModel);
			}
			getModelService().removeAll(accommodationCancellationFeesData);
		}
		return super.performSave(widgetInstanceManager, currentObject);
	}

	private void removeFeeAndUpdatePolicy(final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData,
			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesData,
			final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyDataModel)
	{
		//Get AccommodationCancellationPolicyModel from Database
		final String accommodationCancellationPolicyDataCode = getCodeFromName(
				accommodationCancellationPolicyData.getAccommodationOfferingData().getCode() + UNDERSCORE
						+ accommodationCancellationPolicyData.getAccommodationData().getCode() + UNDERSCORE,
				accommodationCancellationPolicyData.getCode(), StringUtils.EMPTY);
		final AccommodationCancellationPolicyModel accommodationCancellationPolicy = getAccommodationCancellationPolicyService()
				.getAccommodationCancellationPolicy(accommodationCancellationPolicyDataCode,accommodationCancellationPolicyData.getCatalogVersion());
		if (Objects.nonNull(accommodationCancellationPolicy))
		{
			for (final AccommodationCancellationFeesDataModel accommodationCancellationFees : accommodationCancellationFeesData)
			{
				final AccommodationCancellationFeesModel accommodationCancellationFee = getAccommodationCancellationFeesService()
						.getAccommodationCancellationFees(accommodationCancellationPolicy,
								accommodationCancellationFees.getMinDays(),
								accommodationCancellationFees.getMaxDays());
				if (Objects.nonNull(accommodationCancellationFee))
				{
					getModelService().remove(accommodationCancellationFee);
				}
			}
			accommodationCancellationPolicy.setStartDate(Objects.isNull(accommodationCancellationPolicyDataModel.getStartDate()) ?
					accommodationCancellationPolicyDataModel.getStartDate() :
					DateUtils.truncate(accommodationCancellationPolicyDataModel.getStartDate(), Calendar.DATE));
			accommodationCancellationPolicy.setEndDate(Objects.isNull(accommodationCancellationPolicyDataModel.getEndDate()) ?
					accommodationCancellationPolicyDataModel.getEndDate() :
					DateUtils.truncate(accommodationCancellationPolicyDataModel.getEndDate(), Calendar.DATE));
			accommodationCancellationPolicy.setCatalogVersion(accommodationCancellationPolicyDataModel.getCatalogVersion());
			getLocaleList().forEach(locale -> accommodationCancellationPolicy
					.setCancellationPolicyDescription(accommodationCancellationPolicyData.getCancellationPolicyDescription(locale),
							locale));
		}
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return new ArrayList<>();
	}

	/**
	 * Gets the code from name.
	 *
	 * @param prefix the prefix
	 * @param name   the name
	 * @param suffix the suffix
	 * @return the code from name
	 */
	protected String getCodeFromName(final String prefix, final String name, final String suffix)
	{
		return prefix + name.replaceAll(SPACE, UNDERSCORE).toUpperCase() + suffix;
	}

	protected AccommodationCancellationPolicyService getAccommodationCancellationPolicyService()
	{
		return accommodationCancellationPolicyService;
	}

	@Required
	public void setAccommodationCancellationPolicyService(
			final AccommodationCancellationPolicyService accommodationCancellationPolicyService)
	{
		this.accommodationCancellationPolicyService = accommodationCancellationPolicyService;
	}

	protected AccommodationCancellationPolicyDataService getAccommodationCancellationPolicyDataService()
	{
		return accommodationCancellationPolicyDataService;
	}

	@Required
	public void setAccommodationCancellationPolicyDataService(
			final AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService)
	{
		this.accommodationCancellationPolicyDataService = accommodationCancellationPolicyDataService;
	}

	protected AccommodationCancellationFeesService getAccommodationCancellationFeesService()
	{
		return accommodationCancellationFeesService;
	}

	@Required
	public void setAccommodationCancellationFeesService(
			final AccommodationCancellationFeesService accommodationCancellationFeesService)
	{
		this.accommodationCancellationFeesService = accommodationCancellationFeesService;
	}

	protected List<Locale> getLocaleList()
	{
		return localeList;
	}

	@Required
	public void setLocaleList(final List<Locale> localeList)
	{
		this.localeList = localeList;
	}
}
