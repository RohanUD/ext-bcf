/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class BcfVacationLocationEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Logger LOG = Logger.getLogger(BcfVacationLocationEditorLogicHandler.class);

	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		BcfVacationLocationModel vacationLocationModel = (BcfVacationLocationModel) currentObject;

		Set<String> changedAttributes = vacationLocationModel.getItemModelContext().getDirtyAttributes();

		if (changedAttributes.contains(BcfVacationLocationModel.CODE) || changedAttributes.contains(BcfVacationLocationModel.NAME)
				|| changedAttributes.contains(BcfVacationLocationModel.LOCATIONTYPE))
		{
			LocationModel originalLocationModel = bcfTravelLocationService
					.getOrCreateLocationFromVacationLocation(vacationLocationModel);
			vacationLocationModel.setOriginalLocation(originalLocationModel);
		}
		getModelService().save(currentObject);
		return currentObject;
	}


	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateLocationDetails((BcfVacationLocationModel) currentObject);
	}


	protected List<ValidationInfo> validateLocationDetails(
			final BcfVacationLocationModel vacationLocationModel)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (StringUtils.isEmpty(vacationLocationModel.getCode()))
		{
			invalidValues.add(getInvalidDataError(BcfVacationLocationModel.CODE, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(vacationLocationModel.getName()))
		{
			invalidValues.add(getInvalidDataError(BcfVacationLocationModel.NAME, BcfbackofficeConstants.MANDATORY));
		}
		if (Objects.isNull(vacationLocationModel.getLocationType()))
		{
			invalidValues.add(getInvalidDataError(BcfVacationLocationModel.LOCATIONTYPE, BcfbackofficeConstants.MANDATORY));
		}
		return invalidValues;
	}

	public BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
