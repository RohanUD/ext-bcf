/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.core.enums.StockLevelType;
import com.bcf.integration.dataupload.utility.activities.ActivitiesDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityCancellationPolicyDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityInventoryDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityPricesDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityScheduleDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivitiesDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ActivitiesDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final Logger LOG = Logger.getLogger(ActivitiesDataEditorLogicHandler.class);
	private static final String STOCK_LEVEL_TYPE_ERROR = "stockleveltype.error";

	private CatalogVersionService catalogVersionService;
	private ModelService modelService;
	private ActivitiesDataUploadUtility activitiesDataUploadUtility;
	private ActivityPricesDataUploadUtility activityPricesDataUploadUtility;
	private ActivityInventoryDataUploadUtility activityInventoryDataUploadUtility;
	private ActivitiesDataValidator activitiesDataValidator;
	private ProductService productService;
	private ActivityCancellationPolicyDataUploadUtility activityCancellationPolicyDataUploadUtility;
	private ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility;

	@Override
	public void beforeEditorAreaRender(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
	{
		widgetInstanceManager.getModel().setValue("valueChanged", Boolean.TRUE);
		super.beforeEditorAreaRender(widgetInstanceManager, currentObject);
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{

		final ActivitiesDataModel activitiesDataModel = (ActivitiesDataModel) currentObject;
		activitiesDataModel.setStatus(DataImportProcessStatus.PROCESSING);
		final List<ItemModel> items = new ArrayList<>();
		updateActivityProduct(activitiesDataModel, items);
		activitiesDataModel.getActivityPriceDataList()
				.forEach(activityPriceData -> getActivityPricesDataUploadUtility().handleSuccess(activityPriceData));
		activitiesDataModel.getActivityInventoryDataList()
				.forEach(activityInventoryData -> getActivityInventoryDataUploadUtility().handleSuccess(activityInventoryData));
		getActivityCancellationPolicyDataUploadUtility().uploadCancellationPolicy(
				(List<ActivityCancellationPolicyDataModel>) activitiesDataModel.getActivityCancellationPolicyData(), items);
		getActivitiesDataUploadUtility().handleSuccess(activitiesDataModel, items);
		getModelService().saveAll();
		getActivityCancellationPolicyDataUploadUtility().removeInvalidCancellationFees(activitiesDataModel);
		getModelService().save(activitiesDataModel);
		return currentObject;
	}

	private void updateActivityProduct(final ActivitiesDataModel activitiesDataModel, final List<ItemModel> items)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService()
				.getCatalogVersion(BcfbackofficeConstants.PRODUCTCATALOG, BcfbackofficeConstants.CATALOGVERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		if (activitiesDataModel.isDeleted())
		{
			getActivitiesDataUploadUtility().markUnapproved(activitiesDataModel, catalogVersion);
			return;
		}

		final Collection<ActivityPriceDataModel> activityPriceDataModelList = activitiesDataModel.getActivityPriceDataList();
		activityPriceDataModelList.stream()
				.forEach(activityPriceDataModel -> getActivityPricesDataUploadUtility().truncateDates(activityPriceDataModel));
		final Collection<ActivityInventoryDataModel> activityInventoryDataModelList = activitiesDataModel
				.getActivityInventoryDataList();

		final Collection<ActivityCancellationPolicyDataModel> activityCancellationPolicyDataModelList = activitiesDataModel
				.getActivityCancellationPolicyData();
		activityCancellationPolicyDataModelList
				.forEach(activityCancellationPolicyData -> activityCancellationPolicyData.setActivitiesData(activitiesDataModel));

		final Transaction tx = Transaction.current();
		Boolean result = Boolean.FALSE;
		try
		{
			tx.begin();
			getActivitiesDataUploadUtility().uploadActivities(Collections.singletonList(activitiesDataModel),
					catalogVersion, items);
			getModelService().saveAll();
			tx.flushDelayedStore();
			activityPriceDataModelList.stream()
					.forEach(activityPriceDataModel -> {
						activityPriceDataModel.setActivitiesData(activitiesDataModel);
						getActivityPricesDataUploadUtility()
								.uploadActivityPrices(activityPriceDataModel, catalogVersion, items);
					});
			getActivityScheduleDataUploadUtility()
					.uploadActivityScheduleList(activitiesDataModel, activitiesDataModel.getActivityScheduleDataList(), items);
			activityInventoryDataModelList
					.forEach(activityInventoryDataModel -> activityInventoryDataModel.setActivitiesData(activitiesDataModel));
			getActivityInventoryDataUploadUtility().uploadActivityInventoryData((List) activityInventoryDataModelList);
			getActivityInventoryDataUploadUtility().uploadActivityStockLevel(activitiesDataModel);
			getModelService().saveAll();
			LOG.info("Transaction Commit");
			tx.commit();
			result = Boolean.TRUE;
		}
		catch (final ModelSavingException | IllegalArgumentException ex)
		{
			LOG.error("Transaction RollBack : " + ex);
			tx.rollback();
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager,
			final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateActivitiesData((ActivitiesDataModel) currentObject);
	}

	private List<ValidationInfo> validateActivitiesData(final ActivitiesDataModel activitiesDataModel)
	{
		final List<String> invalidAttributeList = getActivitiesDataValidator().validateActivityData(activitiesDataModel);
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (!invalidAttributeList.isEmpty())
		{
			invalidAttributeList.stream().forEach(
					invalidAttribute -> invalidValues.add(getInvalidDataError(invalidAttribute, BcfbackofficeConstants.MANDATORY)));
		}

		if (!activitiesDataModel.getStockLevelType().equals(StockLevelType.STANDARD) && activitiesDataModel
				.getActivityInventoryDataList().stream()
				.anyMatch(activityInventoryDataModel -> CollectionUtils
						.isNotEmpty(activitiesDataModel.getActivityInventoryDataList())))
		{
			invalidValues.add(getInvalidDataError(ActivitiesDataModel.STOCKLEVELTYPE, STOCK_LEVEL_TYPE_ERROR));
		}
		return invalidValues;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	@Override
	protected ModelService getModelService()
	{
		return modelService;
	}

	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ActivitiesDataUploadUtility getActivitiesDataUploadUtility()
	{
		return activitiesDataUploadUtility;
	}

	@Required
	public void setActivitiesDataUploadUtility(
			final ActivitiesDataUploadUtility activitiesDataUploadUtility)
	{
		this.activitiesDataUploadUtility = activitiesDataUploadUtility;
	}

	protected ActivityPricesDataUploadUtility getActivityPricesDataUploadUtility()
	{
		return activityPricesDataUploadUtility;
	}

	@Required
	public void setActivityPricesDataUploadUtility(
			final ActivityPricesDataUploadUtility activityPricesDataUploadUtility)
	{
		this.activityPricesDataUploadUtility = activityPricesDataUploadUtility;
	}

	protected ActivitiesDataValidator getActivitiesDataValidator()
	{
		return activitiesDataValidator;
	}

	@Required
	public void setActivitiesDataValidator(final ActivitiesDataValidator activitiesDataValidator)
	{
		this.activitiesDataValidator = activitiesDataValidator;
	}

	protected ActivityInventoryDataUploadUtility getActivityInventoryDataUploadUtility()
	{
		return activityInventoryDataUploadUtility;
	}

	@Required
	public void setActivityInventoryDataUploadUtility(
			final ActivityInventoryDataUploadUtility activityInventoryDataUploadUtility)
	{
		this.activityInventoryDataUploadUtility = activityInventoryDataUploadUtility;
	}

	public ActivityCancellationPolicyDataUploadUtility getActivityCancellationPolicyDataUploadUtility()
	{
		return activityCancellationPolicyDataUploadUtility;
	}

	public void setActivityCancellationPolicyDataUploadUtility(
			final ActivityCancellationPolicyDataUploadUtility activityCancellationPolicyDataUploadUtility)
	{
		this.activityCancellationPolicyDataUploadUtility = activityCancellationPolicyDataUploadUtility;
	}

	public ActivityScheduleDataUploadUtility getActivityScheduleDataUploadUtility()
	{
		return activityScheduleDataUploadUtility;
	}

	public void setActivityScheduleDataUploadUtility(
			final ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility)
	{
		this.activityScheduleDataUploadUtility = activityScheduleDataUploadUtility;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}
}
