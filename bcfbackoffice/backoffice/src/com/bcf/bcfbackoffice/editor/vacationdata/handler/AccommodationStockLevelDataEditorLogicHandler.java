/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.vacationdata.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsStockLevelDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class AccommodationStockLevelDataEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";
	private static final String INVALID_STOCK = "invalid.stock";

	AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility;

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final AccommodationStockLevelDataModel accommodationStockLevelData = (AccommodationStockLevelDataModel) currentObject;
		accommodationStockLevelData.setStatus(DataImportProcessStatus.PENDING);
		accommodationStockLevelData.setStartDate(Objects.isNull(accommodationStockLevelData.getStartDate()) ?
				accommodationStockLevelData.getStartDate() :
				DateUtils.truncate(accommodationStockLevelData.getStartDate(), Calendar.DATE));
		accommodationStockLevelData.setEndDate(Objects.isNull(accommodationStockLevelData.getEndDate()) ?
				accommodationStockLevelData.getEndDate() :
				DateUtils.truncate(accommodationStockLevelData.getEndDate(), Calendar.DATE));
		getModelService().save(currentObject);
		getModelService().refresh(currentObject);
		accommodationsStockLevelDataUploadUtility.uploadAccommodationStockLevelDatas();
		return currentObject;
	}

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateStockLevelData((AccommodationStockLevelDataModel) currentObject);
	}

	protected List<ValidationInfo> validateStockLevelData(
			final AccommodationStockLevelDataModel accommodationStockLevelDataModel)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		if (Objects.isNull(accommodationStockLevelDataModel.getStartDate()))
		{
			invalidValues.add(getInvalidDataError(AccommodationStockLevelDataModel.STARTDATE, BcfbackofficeConstants.MANDATORY));
		}

		if (Objects.nonNull(accommodationStockLevelDataModel.getEndDate()) && accommodationStockLevelDataModel.getEndDate()
				.before(accommodationStockLevelDataModel.getStartDate()))
		{
			invalidValues.add(getInvalidDataError(AccommodationStockLevelDataModel.ENDDATE, END_DATE_BEFORE_START_DATE));
		}

		if (Objects.isNull(accommodationStockLevelDataModel.getStockLevel())
				|| accommodationStockLevelDataModel.getStockLevel() < 0)
		{
			invalidValues.add(getInvalidDataError(AccommodationStockLevelDataModel.STOCKLEVEL, INVALID_STOCK));
		}


		return invalidValues;
	}

	@Required
	public void setAccommodationsStockLevelDataUploadUtility(
			final AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility)
	{
		this.accommodationsStockLevelDataUploadUtility = accommodationsStockLevelDataUploadUtility;
	}
}
