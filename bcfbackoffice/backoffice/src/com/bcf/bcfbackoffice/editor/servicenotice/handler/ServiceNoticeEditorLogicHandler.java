/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.editor.servicenotice.handler;

import static com.bcf.constants.BcfbackofficeConstants.DATE_IN_PAST_ERROR;
import static com.bcf.constants.BcfbackofficeConstants.EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR;
import static com.bcf.constants.BcfbackofficeConstants.PUBLISH_DATE_CHANGE_NOT_ALLOWED_ERROR;

import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfbackoffice.editor.vacationdata.handler.AbstractDataEditorLogicHandler;
import com.bcf.core.enums.ServiceNoticeStatus;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.workflow.BcfWorkflowService;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class ServiceNoticeEditorLogicHandler extends AbstractDataEditorLogicHandler
{

	@Resource(name = "bcfWorkflowService")
	BcfWorkflowService bcfWorkflowService;

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		return validateServiceNotice((ServiceNoticeModel) currentObject);
	}

	private List<ValidationInfo> validateServiceNotice(final ServiceNoticeModel serviceNotice)
	{
		final List<ValidationInfo> invalidValues = new ArrayList<>();

		Date currentDate = new Date();

		Set<String> modifiedAttributes = serviceNotice.getItemModelContext().getDirtyAttributes();

		if (CollectionUtils.isEmpty(modifiedAttributes))
		{
			return invalidValues;
		}

		if (modifiedAttributes.contains(ServiceNoticeModel.PUBLISHDATE) && !getModelService().isNew(serviceNotice))
		{
			invalidValues
					.add(getInvalidDataError(ServiceNoticeModel.PUBLISHDATE, PUBLISH_DATE_CHANGE_NOT_ALLOWED_ERROR));
		}

		if (modifiedAttributes.contains(ServiceNoticeModel.PUBLISHDATE) && serviceNotice.getPublishDate().before(currentDate))
		{
			invalidValues
					.add(getInvalidDataError(ServiceNoticeModel.PUBLISHDATE, DATE_IN_PAST_ERROR));
		}

		if (modifiedAttributes.contains(ServiceNoticeModel.EXPIRYDATE) && serviceNotice.getExpiryDate().before(currentDate))
		{
			invalidValues
					.add(getInvalidDataError(ServiceNoticeModel.EXPIRYDATE, DATE_IN_PAST_ERROR));
		}
		if (serviceNotice.getExpiryDate().before(serviceNotice.getPublishDate()))
		{
			invalidValues
					.add(getInvalidDataError(ServiceNoticeModel.EXPIRYDATE, EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR));
		}
		return invalidValues;
	}

	public Object performSave(WidgetInstanceManager widgetInstanceManager, Object currentObject) throws ObjectSavingException
	{
		if (currentObject instanceof ServiceNoticeModel)
		{
			ServiceNoticeModel model = (ServiceNoticeModel) currentObject;

			if (getModelService().isModified(model))
			{
				//isNew check is for the clone functionality, no need to check existing workflows as its a new model generated through clone
				if (!getModelService().isNew(model))
				{
					List<WorkflowModel> existingWorkflows = bcfWorkflowService.getActiveWorkflowForServiceNotice(model);

					//terminate existing workflows if active
					if (CollectionUtils.isNotEmpty(existingWorkflows))
					{
						bcfWorkflowService.terminateWorkflow(existingWorkflows);
					}
				}
				//start a new workflow
				model.setStatus(ServiceNoticeStatus.DRAFT);
				model.setPublished(Boolean.FALSE);
				getModelService().save(model);
				bcfWorkflowService.startWorkflowForServiceNotice(model);
			}
		}
		return super.performSave(widgetInstanceManager, currentObject);
	}

}
