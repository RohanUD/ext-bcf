/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.bcfbackoffice.editor.traveladvisory.handler;

import static com.bcf.constants.BcfbackofficeConstants.DATE_IN_PAST_ERROR;
import static com.bcf.constants.BcfbackofficeConstants.EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR;
import static com.bcf.constants.BcfbackofficeConstants.TRAVEL_ADVISORY_DESCRIPTION_DATE_EMPTY;
import static com.bcf.constants.BcfbackofficeConstants.TRAVEL_ADVISORY_EXPIRY_DATE_EMPTY;
import static com.bcf.constants.BcfbackofficeConstants.TRAVEL_ADVISORY_PUBLISH_DATE_EMPTY;
import static com.bcf.constants.BcfbackofficeConstants.TRAVEL_ADVISORY_TITLE_EMPTY;

import de.hybris.platform.workflow.model.WorkflowModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.editor.vacationdata.handler.AbstractDataEditorLogicHandler;
import com.bcf.core.enums.TravelAdvisoryStatus;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.workflow.BcfWorkflowService;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.validation.ValidationContext;
import com.hybris.cockpitng.validation.model.ValidationInfo;


public class TravelAdvisoryEditorLogicHandler extends AbstractDataEditorLogicHandler
{
	private BcfWorkflowService bcfWorkflowService;

	@Override
	public List<ValidationInfo> performValidation(final WidgetInstanceManager widgetInstanceManager, final Object currentObject,
			final ValidationContext validationContext)
	{
		final TravelAdvisoryModel travelAdvisoryModel = (TravelAdvisoryModel) currentObject;
		final List<ValidationInfo> invalidValues = new ArrayList<>();
		if (StringUtils.isEmpty(travelAdvisoryModel.getAdvisoryTitle()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.ADVISORYTITLE, TRAVEL_ADVISORY_TITLE_EMPTY));
		}
		if (Objects.isNull(travelAdvisoryModel.getPublishDateTime()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.PUBLISHDATETIME, TRAVEL_ADVISORY_PUBLISH_DATE_EMPTY));
		}
		if (Objects.isNull(travelAdvisoryModel.getExpiryDateTime()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.EXPIRYDATETIME, TRAVEL_ADVISORY_EXPIRY_DATE_EMPTY));
		}
		if (Objects.nonNull(travelAdvisoryModel.getExpiryDateTime()) && travelAdvisoryModel.getExpiryDateTime().before(new Date()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.EXPIRYDATETIME, DATE_IN_PAST_ERROR));
		}
		if (Objects.nonNull(travelAdvisoryModel.getPublishDateTime()) && Objects.nonNull(travelAdvisoryModel.getExpiryDateTime())
				&& travelAdvisoryModel.getExpiryDateTime().before(travelAdvisoryModel.getPublishDateTime()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.EXPIRYDATETIME, EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR));
		}
		if (Objects.isNull(travelAdvisoryModel.getAdvisoryDescription()))
		{
			invalidValues.add(getInvalidDataError(TravelAdvisoryModel.ADVISORYDESCRIPTION, TRAVEL_ADVISORY_DESCRIPTION_DATE_EMPTY));
		}
		return invalidValues;
	}

	@Override
	public Object performSave(final WidgetInstanceManager widgetInstanceManager, final Object currentObject)
			throws ObjectSavingException
	{
		final boolean performCustomSave =
				currentObject instanceof TravelAdvisoryModel && getModelService().isModified(currentObject);
		if (!performCustomSave)
		{
			return super.performSave(widgetInstanceManager, currentObject);
		}
		final TravelAdvisoryModel model = (TravelAdvisoryModel) currentObject;
		if (!getModelService().isNew(model))
		{
			final List<WorkflowModel> existingWorkflows = bcfWorkflowService.getActiveWorkflowForTravelAdvisory(model);
			if (CollectionUtils.isNotEmpty(existingWorkflows))
			{
				getBcfWorkflowService().terminateWorkflow(existingWorkflows);
			}
		}
		model.setStatus(TravelAdvisoryStatus.DRAFT);
		final Object travelAdvisorySavedItem = super.performSave(widgetInstanceManager, currentObject);
		getBcfWorkflowService().startWorkflowForTravelAdvisory(model);
		return travelAdvisorySavedItem;
	}

	public BcfWorkflowService getBcfWorkflowService()
	{
		return bcfWorkflowService;
	}

	@Required
	public void setBcfWorkflowService(final BcfWorkflowService bcfWorkflowService)
	{
		this.bcfWorkflowService = bcfWorkflowService;
	}
}
