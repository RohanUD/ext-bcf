package com.bcf.bcfbackoffice.util;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;


public class BCFBackofficeUtil
{
	private static final String SPACE = " ";
	private static final String COMMA = ",";

	public static String getRoomRateProductLabel(RoomRateProductModel roomRateProductModel)
	{
		StringBuilder labelBuilder = new StringBuilder(StringUtils.EMPTY);
		CategoryModel categoryModel = roomRateProductModel.getSupercategories().stream().findFirst().get();

		if (Objects.nonNull(categoryModel) && categoryModel instanceof RatePlanModel)
		{
			Collection<AccommodationModel> accommodationModels = ((RatePlanModel) categoryModel).getAccommodation();
			if (CollectionUtils.isNotEmpty(accommodationModels))
			{
				AccommodationModel accommodationModel = accommodationModels.stream().findFirst().get();
				if (Objects.nonNull(accommodationModel))
				{
					Collection<RatePlanConfigModel> ratePlanConfigModels = accommodationModel.getRatePlanConfigs();
					if (CollectionUtils.isNotEmpty(ratePlanConfigModels))
					{
						RatePlanConfigModel ratePlanConfigModel = ratePlanConfigModels.stream().findFirst().get();
						if (Objects.nonNull(ratePlanConfigModel))
						{
							Collection<MarketingRatePlanInfoModel> marketingRatePlanInfoModels = ratePlanConfigModel
									.getMarketingRatePlanInfo();
							if (CollectionUtils.isNotEmpty(marketingRatePlanInfoModels))
							{
								MarketingRatePlanInfoModel marketingRatePlanInfoModel = marketingRatePlanInfoModels.stream()
										.findFirst().get();
								if (Objects.nonNull(marketingRatePlanInfoModel) && Objects
										.nonNull(marketingRatePlanInfoModel.getAccommodationOffering()))
								{
									String hotelName = marketingRatePlanInfoModel.getAccommodationOffering().getCode();
									labelBuilder.append(hotelName);
									labelBuilder.append(COMMA);
									labelBuilder.append(SPACE);
								}
							}
						}
					}
					String room = accommodationModel.getName();
					labelBuilder.append(room);
					labelBuilder.append(COMMA);
				}
			}
		}

		getSuffix(roomRateProductModel, labelBuilder);
		return labelBuilder.toString();
	}

	private static String getSuffix(RoomRateProductModel roomRateProductModel, StringBuilder labelBuilder)
	{
		if (Objects.nonNull(roomRateProductModel.getDateRanges()))
		{
			final DateRangeModel dateRangeModel = roomRateProductModel.getDateRanges().stream().findFirst().get();
			if (Objects.nonNull(dateRangeModel) && Objects.nonNull(dateRangeModel.getStartingDate()) && Objects
					.nonNull(dateRangeModel.getEndingDate()))
			{
				final Date startDate = DateUtils.truncate(dateRangeModel.getStartingDate(), Calendar.DATE);
				final Date endDate = DateUtils.truncate(dateRangeModel.getEndingDate(), Calendar.DATE);
				final String strDateFormat = "ddMMyyyy";
				final DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
				final String startDateStr = dateFormat.format(startDate);
				final String endDateStr = dateFormat.format(endDate);

				String daysCode = StringUtils.EMPTY;

				final List<DayOfWeek> daysOfWeeks = roomRateProductModel.getDaysOfWeek();
				if (CollectionUtils.isNotEmpty(daysOfWeeks))
				{
					daysCode = daysOfWeeks.stream().map(dayOfWeek -> String.valueOf(dayOfWeek.getCode().charAt(0)))
							.collect(Collectors.joining());
				}

				labelBuilder.append(SPACE);
				labelBuilder.append(startDateStr);
				labelBuilder.append(SPACE);
				labelBuilder.append(endDateStr);
				labelBuilder.append(SPACE);
				labelBuilder.append(daysCode);
			}
		}
		return StringUtils.EMPTY;
	}
}
