/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.handler;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.tx.Transaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraGuestOccupancyProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationCancellationPolicyDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsPriceDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsStockLevelDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.ExtraGuestOccupancyProductUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.ExtraProductDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.MinimumNightsStayRulesDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;


public class AccommodationHandlerForWizard extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(
			AccommodationHandlerForWizard.class);
	private static final String ACCOMMODATION_OFFERING_PARTIAL_SAVE = "accommodationofferingdata.partialsave.true";
	private static final String ACCOMMODATION_PARTIAL_SAVE = "accommodationdata.partialsave.true";
	private static final String ACCOMMODATION_RATEPLANDATA_EMPTY = "accommodationdata.rateplandata.empty";
	private static final String RATEPLANDATA_PARTIAL_SAVE = "rateplandata.partialsave.true";
	private static final String EXTRAPRODUCT_PARTIAL_SAVE = "extraproduct.partialsave.true";
	private static final String RATEPLANDATA_PARTIAL_SAVE_ACCOMMDOATION_MSG = "rateplandata.partialsave.accommodationdata.name";
	private static final String ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE = "accommodationcancellationpolicydata.partialsave.true";
	private static final String ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE_RATEPLANDATA_MSG = "accommodationcancellationpolicydata.partialsave.rateplandata.name";
	private static final String ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE_ACCOMMDOATION_MSG = "accommodationcancellationpolicydata.partialsave.accommodationdata.name";
	private static final String NEW_LINE = "\n";

	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;
	private AccommodationsPriceDataUploadUtility accommodationsPriceDataUploadUtility;
	private AccommodationCancellationPolicyDataUploadUtility accommodationCancellationPolicyDataUploadUtility;
	private AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility;
	private ExtraProductDataUploadUtility extraProductDataUploadUtility;
	private SetupSolrIndexerService setupSolrIndexerService;
	private ExtraGuestOccupancyProductUploadUtility extraGuestOccupancyProductUploadUtility;
	private MinimumNightsStayRulesDataUploadUtility minimumNightsStayRulesDataUploadUtility;

	public void uploadData(final AccommodationOfferingDataModel accommodationOfferingDataModel) throws ObjectSavingException
	{
		if (accommodationOfferingDataModel.isDeleted())
		{
			getAccommodationsDataUploadUtility().markDeleted(accommodationOfferingDataModel);
			getModelService().save(accommodationOfferingDataModel);
			return;
		}

		if (accommodationOfferingDataModel.isPartialSave() || !validateForPublish(
				accommodationOfferingDataModel))
		{
			return;
		}

		final List<RatePlanDataModel> ratePlanDataModelList = new ArrayList<>();
		final List<ExtraProductDataModel> extraProductDataModelList = new ArrayList<>();
		final List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDataModelList = new ArrayList<>();
		final List<AccommodationStockLevelDataModel> accommodationStockLevelDataModelList = new ArrayList<>();
		final Map<AccommodationDataModel, List<ExtraGuestOccupancyProductDataModel>> accommodationExtraGuestOccupancyProductDatasMap = new HashMap<>();
		final Map<AccommodationDataModel, List<MinimumNightsStayRulesDataModel>> accommodationMinimumNightsStayRulesDatasMap = new HashMap<>();

		accommodationOfferingDataModel.getAccommodationDatas().stream()
				.filter(accommodationDataModel -> accommodationDataModel.getStatus().equals(DataImportProcessStatus.PENDING))
				.forEach(accommodationDataModel -> {

					accommodationDataModel.getRatePlanDatas().stream()
							.filter(ratePlanDataModel -> ratePlanDataModel.getStatus().equals(DataImportProcessStatus.PENDING))
							.forEach(ratePlanDataModel -> {
								ratePlanDataModel.setAccommodation(accommodationDataModel);
								ratePlanDataModel.setAccommodationOffering(accommodationOfferingDataModel);

								if (CollectionUtils.isNotEmpty(ratePlanDataModel.getAccommodationCancellationPolicyData()))
								{
									setAccommodationCancellationPolicyData(accommodationCancellationPolicyDataModelList,
											accommodationOfferingDataModel, accommodationDataModel, ratePlanDataModel);
								}
								ratePlanDataModelList.add(ratePlanDataModel);
							});
					accommodationDataModel.setAccommodationOffering(accommodationOfferingDataModel);

					setAccommodationStockLevelData(accommodationStockLevelDataModelList, accommodationDataModel);

					accommodationExtraGuestOccupancyProductData(accommodationExtraGuestOccupancyProductDatasMap,
							accommodationDataModel);

					accommodationMinimumNightsStayRulesData(accommodationMinimumNightsStayRulesDatasMap,
							accommodationDataModel);
				});

			setExtraProductData(extraProductDataModelList, accommodationOfferingDataModel);



		final Transaction tx = Transaction.current();
		final List<ItemModel> items = new ArrayList<>();
		boolean result = false;
		try
		{
			getModelService().save(accommodationOfferingDataModel);
			tx.begin();

			final CatalogVersionModel catalogVersion = getCatalogVersionService()
					.getCatalogVersion(BcfbackofficeConstants.PRODUCTCATALOG, BcfbackofficeConstants.CATALOGVERSION);

			getAccommodationsDataUploadUtility()
					.uploadAccommodationsData(Collections.singletonList(accommodationOfferingDataModel), items);
			if (CollectionUtils.isNotEmpty(accommodationStockLevelDataModelList))
			{
				getAccommodationsStockLevelDataUploadUtility()
						.uploadAccommodationStockLevelData(accommodationStockLevelDataModelList);
			}
			getAccommodationsPriceDataUploadUtility().uploadRatePlansData(ratePlanDataModelList, items);
			if (CollectionUtils.isNotEmpty(extraProductDataModelList))
			{
				final List<ItemModel> itemsToSave = new ArrayList<>();
				itemsToSave.addAll(extraProductDataModelList);

				getExtraProductDataUploadUtility().uploadExtraProductData(extraProductDataModelList,catalogVersion, items,itemsToSave, accommodationOfferingDataModel.isPublish());
				getModelService().saveAll(itemsToSave);
			}
			if (CollectionUtils.isNotEmpty(accommodationCancellationPolicyDataModelList))
			{
				getAccommodationCancellationPolicyDataUploadUtility()
						.uploadAccommodationCancellationPolicyData(accommodationCancellationPolicyDataModelList, items);
			}
			if (MapUtils.isNotEmpty(accommodationExtraGuestOccupancyProductDatasMap))
			{
				getExtraGuestOccupancyProductUploadUtility()
						.uploadExtraGuestOccupancy(accommodationExtraGuestOccupancyProductDatasMap, items);
			}
			if (MapUtils.isNotEmpty(accommodationMinimumNightsStayRulesDatasMap))
			{
				minimumNightsStayRulesDataUploadUtility
						.processData(accommodationMinimumNightsStayRulesDatasMap, items);

			}
			LOG.info("Transaction Commit");

			tx.commit();
			result = true;
		}
		catch (final ModelSavingException ex)
		{
			LOG.error("Transaction RollBack : " + ex);

			throw new ObjectSavingException(ex.getMessage(), new Throwable());
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
		if (accommodationOfferingDataModel.isPublish())
		{
			if (CollectionUtils.isEmpty(items))
			{
				// edit case - collectItemsToSync
				collectAccommodationOfferingRelatedItems(accommodationOfferingDataModel, items);
			}
			getAccommodationsDataUploadUtility().synchronizeProductCatalog(items);
			getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCVACATION_INDEX, false);
		}
	}

	private void accommodationExtraGuestOccupancyProductData(
			final Map<AccommodationDataModel, List<ExtraGuestOccupancyProductDataModel>> accommodationExtraGuestOccupancyProductDatasMap,
			final AccommodationDataModel accommodationDataModel)
	{
		if (CollectionUtils.isNotEmpty(accommodationDataModel.getExtraGuestOccupancyProductDatas()))
		{
			accommodationExtraGuestOccupancyProductDatasMap
					.put(accommodationDataModel,
							new ArrayList<>(accommodationDataModel.getExtraGuestOccupancyProductDatas()));
		}
	}

	private void accommodationMinimumNightsStayRulesData(
			final Map<AccommodationDataModel, List<MinimumNightsStayRulesDataModel>> accommodationMinimumNightsStayRulesDatasMap,
			final AccommodationDataModel accommodationDataModel)
	{
		if (CollectionUtils.isNotEmpty(accommodationDataModel.getMinimumNightsStayRulesData()))
		{
			accommodationMinimumNightsStayRulesDatasMap
					.put(accommodationDataModel,
							new ArrayList<>(accommodationDataModel.getMinimumNightsStayRulesData()));
		}
	}

	public boolean validateForPublish(final AccommodationOfferingDataModel accommodationOfferingDataModel)
	{
		final StringJoiner stringJoiner = new StringJoiner(StringUtils.EMPTY);
		if (accommodationOfferingDataModel.isPartialSave())
		{
			stringJoiner.add(getLocalizedString(ACCOMMODATION_OFFERING_PARTIAL_SAVE) + accommodationOfferingDataModel.getCode()
					+ NEW_LINE);
		}

		addExtraProductPartialSaveString(accommodationOfferingDataModel, stringJoiner);
		accommodationOfferingDataModel.getAccommodationDatas().forEach(accommodationDataModel -> {
			if (accommodationDataModel.isPartialSave())
			{
				stringJoiner.add(getLocalizedString(ACCOMMODATION_PARTIAL_SAVE) + accommodationDataModel.getCode() + NEW_LINE);
			}
			if (CollectionUtils.isEmpty(accommodationDataModel.getRatePlanDatas()))
			{
				stringJoiner.add(getLocalizedString(ACCOMMODATION_RATEPLANDATA_EMPTY) + accommodationDataModel.getCode()
						+ NEW_LINE);
				return;
			}
			accommodationDataModel.getRatePlanDatas().forEach(ratePlanDataModel -> {
				if (ratePlanDataModel.isPartialSave())
				{
					stringJoiner
							.add(getLocalizedString(RATEPLANDATA_PARTIAL_SAVE) + ratePlanDataModel.getCode() + getLocalizedString(
									RATEPLANDATA_PARTIAL_SAVE_ACCOMMDOATION_MSG)
									+ accommodationDataModel.getCode() + NEW_LINE);
					return;
				}
				if (CollectionUtils.isNotEmpty(ratePlanDataModel.getAccommodationCancellationPolicyData()))
				{
					ratePlanDataModel.getAccommodationCancellationPolicyData().stream()
							.filter(AccommodationCancellationPolicyDataModel::isPartialSave).forEach(
							accommodationCancellationPolicyDataModel -> stringJoiner
									.add(getLocalizedString(ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE)
											+ accommodationCancellationPolicyDataModel.getCode()
											+ getLocalizedString(ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE_RATEPLANDATA_MSG)
											+ ratePlanDataModel.getCode()
											+ getLocalizedString(ACCOMMODATIONCANCELLATIONDATA_PARTIAL_SAVE_ACCOMMDOATION_MSG)
											+ accommodationDataModel.getCode()
									));
				}
			});
		});
		if (StringUtils.isBlank(stringJoiner.toString()))
		{
			return Boolean.TRUE;
		}
		accommodationOfferingDataModel.setStatus(DataImportProcessStatus.FAILED);
		accommodationOfferingDataModel.setStatusDescription(stringJoiner.toString());
		return Boolean.FALSE;
	}

	private void addExtraProductPartialSaveString(final AccommodationOfferingDataModel accommodationOfferingDataModel,
			final StringJoiner stringJoiner)
	{
		if(CollectionUtils.isNotEmpty(accommodationOfferingDataModel.getExtraProductDatas())){

			accommodationOfferingDataModel.getExtraProductDatas().forEach(extraProductData -> {

				if (extraProductData.isPartialSave())
				{
					stringJoiner.add(getLocalizedString(EXTRAPRODUCT_PARTIAL_SAVE) + extraProductData.getCode() + NEW_LINE);
				}
			});

		}
	}

	protected String getLocalizedString(final String label)
	{
		return Labels.getLabel(label);
	}

	private void setAccommodationCancellationPolicyData(
			final List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDataModelList,
			final AccommodationOfferingDataModel accommodationOfferingDataModel,
			final AccommodationDataModel accommodationDataModel,
			final RatePlanDataModel ratePlanDataModel)
	{
		ratePlanDataModel.getAccommodationCancellationPolicyData().stream()
				.filter(accommodationCancellationPolicyDataModel -> DataImportProcessStatus.PENDING
						.equals(accommodationCancellationPolicyDataModel
								.getStatus()))
				.forEach(accommodationCancellationPolicyDataModel -> {
					accommodationCancellationPolicyDataModel
							.setAccommodationOfferingData(accommodationOfferingDataModel);
					accommodationCancellationPolicyDataModel.setAccommodationData(accommodationDataModel);
					accommodationCancellationPolicyDataModel.setRatePlanData(ratePlanDataModel);

					accommodationCancellationPolicyDataModel.getAccommodationCancellationFeesData().stream()
							.filter(accommodationCancellationFeesDataModel -> accommodationCancellationFeesDataModel
									.getStatus().equals(DataImportProcessStatus.PENDING))
							.forEach(accommodationCancellationFeesDataModel ->
									accommodationCancellationFeesDataModel.setAccommodationCancellationPolicyData(
											accommodationCancellationPolicyDataModel));
					accommodationCancellationPolicyDataModelList.add(accommodationCancellationPolicyDataModel);
				});
	}

	private void setAccommodationStockLevelData(final List<AccommodationStockLevelDataModel> accommodationStockLevelDataModelList,
			final AccommodationDataModel accommodationDataModel)
	{
		if (CollectionUtils.isNotEmpty(accommodationDataModel.getAccommodationStockLevelDataList()))
		{
			accommodationDataModel.getAccommodationStockLevelDataList().stream()
					.filter(accommodationStockLevelDataModel -> accommodationStockLevelDataModel.getStatus()
							.equals(DataImportProcessStatus.PENDING))
					.forEach(accommodationStockLevelDataModel -> {
						accommodationStockLevelDataModel
								.setAccommodationData(accommodationDataModel);
						accommodationStockLevelDataModelList.add(accommodationStockLevelDataModel);
					});
		}
	}

	private void setExtraProductData(
			final List<ExtraProductDataModel> extraProductDataModelList,
			final AccommodationOfferingDataModel accommodationOfferingDataModel)
	{
		if (CollectionUtils.isNotEmpty(accommodationOfferingDataModel.getExtraProductDatas()))
		{
			accommodationOfferingDataModel.getExtraProductDatas().stream()
					.filter(extraProductData -> !extraProductData.getStatus().equals(DataImportProcessStatus.FAILED))
					.forEach(extraProductData -> {
						final DateFormat dateFormat = new SimpleDateFormat(BcfintegrationserviceConstants.DATE_FORMAT);
						extraProductData.setAccommodationOfferingCode(accommodationOfferingDataModel.getCode());
						extraProductDataModelList.add(extraProductData);
					});
		}
	}

	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	protected AccommodationsDataUploadUtility getAccommodationsDataUploadUtility()
	{
		return accommodationsDataUploadUtility;
	}

	@Required
	public void setAccommodationsDataUploadUtility(
			final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}

	protected AccommodationsPriceDataUploadUtility getAccommodationsPriceDataUploadUtility()
	{
		return accommodationsPriceDataUploadUtility;
	}

	@Required
	public void setAccommodationsPriceDataUploadUtility(
			final AccommodationsPriceDataUploadUtility accommodationsPriceDataUploadUtility)
	{
		this.accommodationsPriceDataUploadUtility = accommodationsPriceDataUploadUtility;
	}

	protected AccommodationsStockLevelDataUploadUtility getAccommodationsStockLevelDataUploadUtility()
	{
		return accommodationsStockLevelDataUploadUtility;
	}

	@Required
	public void setAccommodationsStockLevelDataUploadUtility(
			final AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility)
	{
		this.accommodationsStockLevelDataUploadUtility = accommodationsStockLevelDataUploadUtility;
	}

	protected AccommodationCancellationPolicyDataUploadUtility getAccommodationCancellationPolicyDataUploadUtility()
	{
		return accommodationCancellationPolicyDataUploadUtility;
	}

	@Required
	public void setAccommodationCancellationPolicyDataUploadUtility(
			final AccommodationCancellationPolicyDataUploadUtility accommodationCancellationPolicyDataUploadUtility)
	{
		this.accommodationCancellationPolicyDataUploadUtility = accommodationCancellationPolicyDataUploadUtility;
	}

	protected ExtraProductDataUploadUtility getExtraProductDataUploadUtility()
	{
		return extraProductDataUploadUtility;
	}

	@Required
	public void setExtraProductDataUploadUtility(
			final ExtraProductDataUploadUtility extraProductDataUploadUtility)
	{
		this.extraProductDataUploadUtility = extraProductDataUploadUtility;
	}

	protected ExtraGuestOccupancyProductUploadUtility getExtraGuestOccupancyProductUploadUtility()
	{
		return extraGuestOccupancyProductUploadUtility;
	}

	@Required
	public void setExtraGuestOccupancyProductUploadUtility(
			final ExtraGuestOccupancyProductUploadUtility extraGuestOccupancyProductUploadUtility)
	{
		this.extraGuestOccupancyProductUploadUtility = extraGuestOccupancyProductUploadUtility;
	}

	public MinimumNightsStayRulesDataUploadUtility getMinimumNightsStayRulesDataUploadUtility()
	{
		return minimumNightsStayRulesDataUploadUtility;
	}

	public void setMinimumNightsStayRulesDataUploadUtility(
			final MinimumNightsStayRulesDataUploadUtility minimumNightsStayRulesDataUploadUtility)
	{
		this.minimumNightsStayRulesDataUploadUtility = minimumNightsStayRulesDataUploadUtility;
	}
}
