/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import java.util.Date;
import java.util.Objects;


public class StockLevelHandler
{
	public void updateStockLevel(StockLevelModel stockLevelModel){
		if (Objects.nonNull(stockLevelModel.getDate()))
		{
			Date date = stockLevelModel.getDate();
			updateDateWithDefaultTime(date);
			stockLevelModel.setDate(date);
			if (Objects.isNull(stockLevelModel.getReleaseBlockDay()))
			{
				stockLevelModel.setReleaseBlockDay(date);
			}
		}else if(Objects.isNull(stockLevelModel.getDate()) && Objects.nonNull(stockLevelModel.getReleaseBlockDay())){
			updateDateWithDefaultTime(stockLevelModel.getReleaseBlockDay());
		}
	}

	protected void updateDateWithDefaultTime(Date date){
		date.setSeconds(0);
		date.setMinutes(0);
		date.setHours(0);
	}
}
