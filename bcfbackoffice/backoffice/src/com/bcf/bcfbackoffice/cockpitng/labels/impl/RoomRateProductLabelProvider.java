package com.bcf.bcfbackoffice.cockpitng.labels.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanConfigModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.Collection;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.util.BCFBackofficeUtil;
import com.hybris.cockpitng.labels.LabelProvider;
import com.hybris.cockpitng.labels.LabelService;


public class RoomRateProductLabelProvider implements LabelProvider<RoomRateProductModel>
{
	private LabelService labelService;

	public RoomRateProductLabelProvider()
	{
	}

	public String getLabel(RoomRateProductModel roomRateProductModel)
	{
		if (Objects.isNull(roomRateProductModel) || Objects.isNull(roomRateProductModel.getSupercategories()))
		{
			return null;
		}
		else
		{
			String label = BCFBackofficeUtil.getRoomRateProductLabel(roomRateProductModel);
			return label;
		}
	}

	private String getStringValue(Object object)
	{
		if (object == null)
		{
			return null;
		}
		else
		{
			return object instanceof String ? object.toString() : this.labelService.getObjectLabel(object);
		}
	}

	public String getDescription(RoomRateProductModel roomRateProductModel)
	{
		return null;
	}

	public String getIconPath(RoomRateProductModel roomRateProductModel)
	{
		return null;
	}

	@Required
	public void setLabelService(LabelService labelService)
	{
		this.labelService = labelService;
	}
}
