package com.bcf.bcfbackoffice.cockpitng.labels.impl.renderers;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.util.UITools;
import com.hybris.cockpitng.widgets.common.AbstractWidgetComponentRenderer;


public class CustomerNameListViewRenderer extends AbstractWidgetComponentRenderer<Listcell, ListColumn, Object>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomerNameListViewRenderer.class);

	@Override
	public void render(final Listcell parent, final ListColumn configuration, final Object object, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManager)
	{

		String userName = StringUtils.EMPTY;
		if (object instanceof AbstractOrderModel)
		{
			final AbstractOrderModel orderModel = (AbstractOrderModel) object;
			if (Objects.nonNull(orderModel.getUser()))
			{
				userName = orderModel.getUser().getName();
			}
		}
		else
		{
			LOG.warn("Passed object: [{}] is not of supported type", Objects.toString(object));
		}

		this.renderComponents(parent, configuration, object, Boolean.FALSE, userName);
	}

	protected void renderComponents(final Listcell parent, final ListColumn configuration, final Object object,
			final boolean initialised,
			final String userName)
	{
		final String labelText = userName;
		final Label label = new Label(labelText);
		UITools.modifySClass(label, "yw-listview-cell-label", true);
		label.setAttribute("hyperlink-candidate", Boolean.TRUE);
		parent.appendChild(label);
		this.fireComponentRendered(label, parent, configuration, object);
		this.fireComponentRendered(parent, configuration, object);
	}
}
