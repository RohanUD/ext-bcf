/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfbackoffice.widgets.traveladvisory.handler;

import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.widgets.vacationdata.handler.AbstractBcfActionHandler;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.workflow.BcfWorkflowService;
import com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.ItemModificationHistoryService;
import com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.ItemModificationInfo;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class TravelAdvisoryFlowActionHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String TRAVEL_ADVISORY = "newTravelAdvisory";
	private BcfWorkflowService bcfWorkflowService;
	private ItemModificationHistoryService itemModificationHistoryService;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final TravelAdvisoryModel travelAdvisoryModel = adapter.getWidgetInstanceManager().getModel()
				.getValue(TRAVEL_ADVISORY, TravelAdvisoryModel.class);
		final ItemModificationInfo modificationInfo = getItemModificationHistoryService()
				.createModificationInfo(travelAdvisoryModel);
		getModelService().save(travelAdvisoryModel);
		getItemModificationHistoryService().logItemModification(modificationInfo);
		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, travelAdvisoryModel);
		adapter.done();
		bcfWorkflowService.startWorkflowForTravelAdvisory(travelAdvisoryModel);
	}

	public BcfWorkflowService getBcfWorkflowService()
	{
		return bcfWorkflowService;
	}

	@Required
	public void setBcfWorkflowService(final BcfWorkflowService bcfWorkflowService)
	{
		this.bcfWorkflowService = bcfWorkflowService;
	}

	public ItemModificationHistoryService getItemModificationHistoryService()
	{
		return itemModificationHistoryService;
	}

	@Required
	public void setItemModificationHistoryService(final ItemModificationHistoryService itemModificationHistoryService)
	{
		this.itemModificationHistoryService = itemModificationHistoryService;
	}
}
