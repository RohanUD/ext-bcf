/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.service.notice.handler;

import static com.bcf.constants.BcfbackofficeConstants.DATE_IN_PAST_ERROR;
import static com.bcf.constants.BcfbackofficeConstants.EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR;

import de.hybris.platform.servicelayer.user.UserService;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfbackoffice.widgets.vacationdata.handler.AbstractBcfActionHandler;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.subscription.strategy.SetAffectedRouteStrategy;
import com.bcf.core.workflow.BcfWorkflowService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class TriggerServiceNoticeEmailHandler extends AbstractBcfActionHandler implements FlowActionHandler
{

	private static final String SERVICE_NOTICE = "newServiceNotice";
	private static final String SERVICE_NOTICE_DATA_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.ServiceNotice.error.title";
	private static final String ICON = "z-messagebox-icon z-messagebox-exclamation";

	private BcfWorkflowService bcfWorkflowService;
	private UserService userService;
	private Map<String, SetAffectedRouteStrategy> affectedRoutePopulateStrategyMap;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final Date currentDate = new Date();
		final ServiceNoticeModel serviceNotice = adapter.getWidgetInstanceManager().getModel()
				.getValue(SERVICE_NOTICE, ServiceNoticeModel.class);

		setAffectedRoutes(serviceNotice);
		serviceNotice.setAuthor(userService.getCurrentUser());
		setPublishDateIfRequired(serviceNotice);
		setExpiryDateIfRequired(serviceNotice);

		if (!isValidFields(currentDate, serviceNotice))
		{
			return;
		}

		getModelService().save(serviceNotice);

		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, serviceNotice);

		adapter.done();
		bcfWorkflowService.startWorkflowForServiceNotice(serviceNotice);
	}

	private void setAffectedRoutes(final ServiceNoticeModel serviceNotice)
	{
		//setting only unique groups and names, as user can select same group multiple times from backoffice
		if (CollectionUtils.isNotEmpty(serviceNotice.getSubscriptionGroups()))
		{
			serviceNotice
					.setSubscriptionGroups(serviceNotice.getSubscriptionGroups().stream().distinct().collect(Collectors.toSet()));
		}

		if (CollectionUtils.isNotEmpty(serviceNotice.getSubscriptionMasterDetails()))
		{
			serviceNotice.setSubscriptionMasterDetails(
					serviceNotice.getSubscriptionMasterDetails().stream().distinct().collect(Collectors.toSet()));
		}
		if (affectedRoutePopulateStrategyMap.containsKey(serviceNotice.getClass().getTypeName()))
		{
			affectedRoutePopulateStrategyMap.get(serviceNotice.getClass().getTypeName()).setAffectedRoutes(serviceNotice);
		}
	}

	/**
	 * we ned to update publish date only when the SN is "instant published"
	 *
	 * @param serviceNotice
	 */
	private void setPublishDateIfRequired(final ServiceNoticeModel serviceNotice)
	{
		if (isInstantPublish(serviceNotice))
		{
			updatePublishDate(serviceNotice);
		}
	}

	private void setExpiryDateIfRequired(final ServiceNoticeModel serviceNotice)
	{
		if (Objects.isNull(serviceNotice.getExpiryDate()))
		{
			setDefaultExpiryDate(serviceNotice);
		}
	}


	/**
	 * If instant publish than set expiry date as today's date with time 23:00:00 and,
	 * <p>if not instant publish than set expiry date as date of publish with time 23:00:00</p>
	 *
	 * @param serviceNotice
	 */
	private void setDefaultExpiryDate(final ServiceNoticeModel serviceNotice)
	{
		final Calendar c = Calendar.getInstance();
		if (!isInstantPublish(serviceNotice))
		{
			c.setTime(serviceNotice.getPublishDate());
		}
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 00);
		c.set(Calendar.SECOND, 00);

		serviceNotice.setExpiryDate(c.getTime());
	}

	private boolean isValidFields(final Date currentDate, final ServiceNoticeModel serviceNotice)
	{
		if (DateUtils.truncatedCompareTo(serviceNotice.getPublishDate(), currentDate, Calendar.DATE) < 0)
		{
			Messagebox.show("Publish " + Labels.getLabel(DATE_IN_PAST_ERROR), Labels.getLabel(SERVICE_NOTICE_DATA_ERROR_TITLE), 1,
					ICON);

			return false;
		}

		if (DateUtils.truncatedCompareTo(serviceNotice.getExpiryDate(), currentDate, Calendar.DATE) < 0)
		{
			Messagebox.show("Expiry " + Labels.getLabel(DATE_IN_PAST_ERROR), Labels.getLabel(SERVICE_NOTICE_DATA_ERROR_TITLE), 1,
					ICON);

			return false;
		}
		else if (serviceNotice.getExpiryDate().before(serviceNotice.getPublishDate()))
		{
			Messagebox.show(Labels.getLabel(EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR), Labels.getLabel(
					SERVICE_NOTICE_DATA_ERROR_TITLE), 1,
					ICON);

			return false;
		}
		return true;
	}

	private boolean isInstantPublish(final ServiceNoticeModel serviceNotice)
	{
		return serviceNotice.getInstantPublish();
	}

	private void updatePublishDate(final ServiceNoticeModel serviceNotice)
	{
		serviceNotice.setPublishDate(new Date());
	}

	public BcfWorkflowService getBcfWorkflowService()
	{
		return bcfWorkflowService;
	}

	@Required
	public void setBcfWorkflowService(final BcfWorkflowService bcfWorkflowService)
	{
		this.bcfWorkflowService = bcfWorkflowService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Required
	public void setAffectedRoutePopulateStrategyMap(
			final Map<String, SetAffectedRouteStrategy> affectedRoutePopulateStrategyMap)
	{
		this.affectedRoutePopulateStrategyMap = affectedRoutePopulateStrategyMap;
	}
}
