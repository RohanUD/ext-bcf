/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Map;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class MinimumNightsStayRulesDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private final String NEW_MIN_NIGHTS_STAY_RULES_DATA = "newMinimumNightsStayRulesData";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final MinimumNightsStayRulesDataModel minimumNightsStayRulesDataModel = adapter.getWidgetInstanceManager()
				.getModel()
				.getValue(NEW_MIN_NIGHTS_STAY_RULES_DATA, MinimumNightsStayRulesDataModel.class);
		getModelService().save(minimumNightsStayRulesDataModel);
		adapter.done();
	}

}
