/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationFeesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityScheduleDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.activities.ActivitiesDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityCancellationPolicyDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityInventoryDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityPricesDataUploadUtility;
import com.bcf.integration.dataupload.utility.activities.ActivityScheduleDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivitiesDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityCancellationFeesDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityCancellationPolicyDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityInventoryDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityPriceDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityScheduleDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ActivityDataCreationHandler implements FlowActionHandler
{
	private static final Logger LOG = Logger.getLogger(ActivityDataCreationHandler.class);
	private static final String NEW_ACTIVITIES_DATA = "newActivitiesData";
	private static final String ACTIVITY_DATA = "Activity Data, ";

	private CatalogVersionService catalogVersionService;
	private ModelService modelService;
	private ActivitiesDataUploadUtility activitiesDataUploadUtility;
	private ActivityPricesDataUploadUtility activityPricesDataUploadUtility;
	private ActivitiesDataValidator activitiesDataValidator;
	private ActivityPriceDataValidator activityPriceDataValidator;
	private ActivityInventoryDataUploadUtility activityInventoryDataUploadUtility;
	private ActivityInventoryDataValidator activityInventoryDataValidator;
	private ActivityCancellationPolicyDataValidator activityCancellationPolicyDataValidator;
	private ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator;
	private ActivityCancellationPolicyDataUploadUtility activityCancellationPolicyDataUploadUtility;
	private ActivityScheduleDataValidator activityScheduleDataValidator;
	private ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{
		final ActivitiesDataModel activityDataModel = adapter.getWidgetInstanceManager().getModel().getValue(NEW_ACTIVITIES_DATA,
				ActivitiesDataModel.class);
		final Collection<ActivityPriceDataModel> activityPriceDataModelList = activityDataModel.getActivityPriceDataList();
		final Collection<ActivityInventoryDataModel> activityInventoryDataModelList = CollectionUtils
				.isEmpty(activityDataModel.getActivityInventoryDataList()) ?
				Collections.emptyList() :
				(List<ActivityInventoryDataModel>) activityDataModel.getActivityInventoryDataList();
		final List<ActivityCancellationPolicyDataModel> activityCancellationPolicyDataModelList = CollectionUtils
				.isEmpty(activityDataModel.getActivityCancellationPolicyData()) ?
				Collections.emptyList() :
				(List<ActivityCancellationPolicyDataModel>) activityDataModel.getActivityCancellationPolicyData();

		final List<ActivityScheduleDataModel> activityScheduleDataModelList = CollectionUtils
				.isEmpty(activityDataModel.getActivityScheduleDataList()) ?
				Collections.emptyList() :
				(List<ActivityScheduleDataModel>) activityDataModel.getActivityScheduleDataList();

		activityScheduleDataModelList
				.forEach(activityScheduleDataModel -> activityScheduleDataModel.setStatus(DataImportProcessStatus.PENDING));

		activityInventoryDataModelList.stream()
				.forEach(activityInventoryDataModel -> {
					activityInventoryDataModel.setStatus(DataImportProcessStatus.PENDING);
					activityInventoryDataModel.setActivitiesData(activityDataModel);
				});

		if (!validateActivityData(activityDataModel, activityPriceDataModelList, activityInventoryDataModelList,
				activityCancellationPolicyDataModelList, adapter))
		{
			return;
		}

		activityCancellationPolicyDataModelList.stream().forEach(activityCancellationPolicyDataModel -> {
			activityCancellationPolicyDataModel.setStatus(DataImportProcessStatus.PENDING);
			activityCancellationPolicyDataModel.getActivityCancellationFeesData().forEach(cancellationFees -> {
				cancellationFees.setActivityCancellationPolicyData(activityCancellationPolicyDataModel);
				cancellationFees.setStatus(DataImportProcessStatus.PENDING);
			});
		});

		activityPriceDataModelList.stream()
				.forEach(activityPriceDataModel -> getActivityPricesDataUploadUtility().truncateDates(activityPriceDataModel));
		activityDataModel.setStatus(DataImportProcessStatus.PROCESSING);
		activityPriceDataModelList.stream()
				.forEach(activityPriceDataModel -> activityPriceDataModel.setStatus(DataImportProcessStatus.PROCESSING));

		activityInventoryDataModelList.stream()
				.forEach(activityInventoryDataModel -> activityInventoryDataModel.setStatus(DataImportProcessStatus.PROCESSING));

		activityCancellationPolicyDataModelList.stream().forEach(
				activityCancellationPolicyDataModel -> activityCancellationPolicyDataModel
						.setStatus(DataImportProcessStatus.PROCESSING));

		final CatalogVersionModel catalogVersion = getCatalogVersionService()
				.getCatalogVersion(BcfbackofficeConstants.PRODUCTCATALOG, BcfbackofficeConstants.CATALOGVERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final List<ItemModel> items = new ArrayList<>();
		if (!createActivityProduct(activityDataModel, activityPriceDataModelList, activityInventoryDataModelList,
				activityCancellationPolicyDataModelList, catalogVersion,
				adapter, items))
		{
			return;
		}

		activityPriceDataModelList
				.forEach(activityPriceData -> getActivityPricesDataUploadUtility().handleSuccess(activityPriceData));

		activityInventoryDataModelList
				.forEach(activityInventoryData -> getActivityInventoryDataUploadUtility().handleSuccess(activityInventoryData));

		getActivitiesDataUploadUtility().handleSuccess(activityDataModel, items);
		getModelService().saveAll();
		//Need to save it explicitly.
		getModelService().save(activityDataModel);
		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, activityDataModel);

		adapter.done();
	}



	private boolean validateActivityData(final ActivitiesDataModel activityDataModel,
			final Collection<ActivityPriceDataModel> activityPriceDataModelList,
			final Collection<ActivityInventoryDataModel> activityInventoryDataModelList,
			final List<ActivityCancellationPolicyDataModel> activityCancellationPolicyDataModelList,
			final FlowActionHandlerAdapter adapter)
	{
		if (Objects.isNull(activityDataModel) || Objects.isNull(activityPriceDataModelList) || activityPriceDataModelList.isEmpty()
				|| !getActivitiesDataValidator().validateActivityData(activityDataModel).isEmpty())
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					ACTIVITY_DATA + activityDataModel.getStatusDescription());
			return Boolean.FALSE;
		}

		if (CollectionUtils.isNotEmpty(activityDataModel.getActivityScheduleDataList()))
		{
			activityDataModel.getActivityScheduleDataList().forEach(schedule -> schedule.setActivitiesData(activityDataModel));

			final List<ActivityScheduleDataModel> invalidActivityScheduleData = activityDataModel.getActivityScheduleDataList()
					.stream()
					.filter(activityScheduleDataModel -> !getActivityScheduleDataValidator()
							.validateActivityScheduleData(activityScheduleDataModel).isEmpty()).collect(Collectors.toList());

			if (!invalidActivityScheduleData.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + invalidActivityScheduleData.get(0).getStatusDescription());
				return Boolean.FALSE;
			}
		}
		final List<ActivityPriceDataModel> invalidActivityPriceData = activityPriceDataModelList.stream()
				.filter(activityPriceDataModel -> !getActivityPriceDataValidator()
						.validateActivityPriceData(activityPriceDataModel).isEmpty()).collect(Collectors.toList());

		if (!invalidActivityPriceData.isEmpty())
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					ACTIVITY_DATA + invalidActivityPriceData.get(0).getStatusDescription());
			return Boolean.FALSE;
		}

		// setting vendor and activity names for activityInventoryValidation.
		if (Objects.nonNull(activityInventoryDataModelList) && !activityInventoryDataModelList.isEmpty())
		{
			final List<ActivityInventoryDataModel> invalidActivityInventoryData = activityInventoryDataModelList.stream()
					.filter(activityInventoryDataModel -> !getActivityInventoryDataValidator()
							.validateActivityInventoryData(activityInventoryDataModel).isEmpty())
					.collect(Collectors.toList());

			if (!invalidActivityInventoryData.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + invalidActivityInventoryData.get(0).getStatusDescription());
				return Boolean.FALSE;
			}

			final List<ActivityInventoryDataModel> invalidActivityInventoryForStockData = activityInventoryDataModelList.stream()
					.filter(activityInventoryDataModel -> !getActivityInventoryDataValidator()
							.validateInventoryForStockTypeData(activityInventoryDataModel, activityDataModel).isEmpty())
					.collect(Collectors.toList());

			if (!invalidActivityInventoryForStockData.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + invalidActivityInventoryForStockData.get(0).getStatusDescription());
				return Boolean.FALSE;
			}

			final List<ActivityCancellationPolicyDataModel> invalidActivityCancellationPolicyDataList = activityCancellationPolicyDataModelList
					.stream()
					.filter(activityCancellationPolicyDataModel -> !validateActivityCancellationPolicy(
							activityCancellationPolicyDataModel).isEmpty())
					.collect(Collectors.toList());
			if (!invalidActivityCancellationPolicyDataList.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + invalidActivityCancellationPolicyDataList.get(0).getStatusDescription());
				return Boolean.FALSE;
			}
		}

		return Boolean.TRUE;
	}

	private List<String> validateActivityCancellationPolicy(final ActivityCancellationPolicyDataModel cancellationPolicyData)
	{
		final List<String> invalidFieldList = getActivityCancellationPolicyDataValidator()
				.validateCancellationPolicyData(cancellationPolicyData);
		for (final ActivityCancellationFeesDataModel cancellationFeesData : cancellationPolicyData
				.getActivityCancellationFeesData())
		{
			invalidFieldList.addAll(getActivityCancellationFeesDataValidator().validateCancellationFeesData(cancellationFeesData));
		}
		return invalidFieldList;
	}


	private Boolean createActivityProduct(final ActivitiesDataModel activityDataModel,
			final Collection<ActivityPriceDataModel> activityPriceDataModelList,
			final Collection<ActivityInventoryDataModel> activityInventoryDataModelList,
			final List<ActivityCancellationPolicyDataModel> activityCancellationPolicyDataModelList,
			final CatalogVersionModel catalogVersion,
			final FlowActionHandlerAdapter adapter, final List<ItemModel> items)
	{
		final Transaction tx = Transaction.current();
		Boolean result = false;
		try
		{
			tx.begin();

			if (StringUtils.isEmpty(activityDataModel.getCode()))
			{
				getActivitiesDataUploadUtility().setCode(activityDataModel);
			}
			final Boolean status = getActivitiesDataUploadUtility().uploadActivities(Collections.singletonList(activityDataModel),
					catalogVersion, items);
			if (BooleanUtils.isFalse(status))
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + activityDataModel.getStatusDescription());
				return Boolean.FALSE;
			}

			getModelService().saveAll();

			activityPriceDataModelList.forEach(priceData -> priceData.setActivitiesData(activityDataModel));
			final List<ActivityPriceDataModel> invalidActivityPriceData = activityPriceDataModelList.stream()
					.filter(activityPriceDataModel -> !getActivityPricesDataUploadUtility()
							.uploadActivityPrices(activityPriceDataModel, catalogVersion, items))
					.collect(Collectors.toList());

			if (!invalidActivityPriceData.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						ACTIVITY_DATA + invalidActivityPriceData.get(0).getStatusDescription());
				return Boolean.FALSE;
			}
			getModelService().saveAll();
			getModelService().save(activityDataModel);

			if (Objects.nonNull(activityDataModel.getActivityScheduleDataList()))
			{
				getActivityScheduleDataUploadUtility()
						.uploadActivityScheduleList(activityDataModel, activityDataModel.getActivityScheduleDataList(), items);
				getModelService().saveAll();
			}

			if (Objects.nonNull(activityInventoryDataModelList))
			{
				getActivityInventoryDataUploadUtility().uploadActivityInventoryData((List) activityInventoryDataModelList);
				getModelService().saveAll();
			}
			if (Objects.nonNull(activityCancellationPolicyDataModelList))
			{
				activityCancellationPolicyDataModelList.forEach(cancellationPolicy -> {
					cancellationPolicy.getActivityCancellationFeesData()
							.forEach(cancellationFees -> cancellationFees.setActivityCancellationPolicyData(cancellationPolicy));
					getModelService().saveAll(cancellationPolicy.getActivityCancellationFeesData());
				});
				activityCancellationPolicyDataModelList.forEach(cancellationPolicy ->
						cancellationPolicy.setActivitiesData(activityDataModel));
				getModelService().saveAll(activityCancellationPolicyDataModelList);
				getActivityCancellationPolicyDataUploadUtility()
						.uploadCancellationPolicy(activityCancellationPolicyDataModelList, items);
			}
			LOG.info("Transaction Commit");
			tx.commit();
			result = Boolean.TRUE;
			return Boolean.TRUE;
		}
		catch (final ModelSavingException mse)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE, mse);
			LOG.error("Transaction RollBack : " + mse);
			return Boolean.FALSE;
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the activitiesDataUploadUtility
	 */
	protected ActivitiesDataUploadUtility getActivitiesDataUploadUtility()
	{
		return activitiesDataUploadUtility;
	}

	/**
	 * @param activitiesDataUploadUtility the activitiesDataUploadUtility to set
	 */
	@Required
	public void setActivitiesDataUploadUtility(final ActivitiesDataUploadUtility activitiesDataUploadUtility)
	{
		this.activitiesDataUploadUtility = activitiesDataUploadUtility;
	}

	protected ActivityPricesDataUploadUtility getActivityPricesDataUploadUtility()
	{
		return activityPricesDataUploadUtility;
	}

	@Required
	public void setActivityPricesDataUploadUtility(
			final ActivityPricesDataUploadUtility activityPricesDataUploadUtility)
	{
		this.activityPricesDataUploadUtility = activityPricesDataUploadUtility;
	}

	protected ActivitiesDataValidator getActivitiesDataValidator()
	{
		return activitiesDataValidator;
	}

	@Required
	public void setActivitiesDataValidator(final ActivitiesDataValidator activitiesDataValidator)
	{
		this.activitiesDataValidator = activitiesDataValidator;
	}

	protected ActivityPriceDataValidator getActivityPriceDataValidator()
	{
		return activityPriceDataValidator;
	}

	@Required
	public void setActivityPriceDataValidator(
			final ActivityPriceDataValidator activityPriceDataValidator)
	{
		this.activityPriceDataValidator = activityPriceDataValidator;
	}

	protected ActivityInventoryDataUploadUtility getActivityInventoryDataUploadUtility()
	{
		return activityInventoryDataUploadUtility;
	}

	@Required
	public void setActivityInventoryDataUploadUtility(
			final ActivityInventoryDataUploadUtility activityInventoryDataUploadUtility)
	{
		this.activityInventoryDataUploadUtility = activityInventoryDataUploadUtility;
	}

	protected ActivityInventoryDataValidator getActivityInventoryDataValidator()
	{
		return activityInventoryDataValidator;
	}

	@Required
	public void setActivityInventoryDataValidator(
			final ActivityInventoryDataValidator activityInventoryDataValidator)
	{
		this.activityInventoryDataValidator = activityInventoryDataValidator;
	}

	public ActivityCancellationPolicyDataValidator getActivityCancellationPolicyDataValidator()
	{
		return activityCancellationPolicyDataValidator;
	}

	public void setActivityCancellationPolicyDataValidator(
			final ActivityCancellationPolicyDataValidator activityCancellationPolicyDataValidator)
	{
		this.activityCancellationPolicyDataValidator = activityCancellationPolicyDataValidator;
	}

	public ActivityCancellationPolicyDataUploadUtility getActivityCancellationPolicyDataUploadUtility()
	{
		return activityCancellationPolicyDataUploadUtility;
	}

	public void setActivityCancellationPolicyDataUploadUtility(
			final ActivityCancellationPolicyDataUploadUtility activityCancellationPolicyDataUploadUtility)
	{
		this.activityCancellationPolicyDataUploadUtility = activityCancellationPolicyDataUploadUtility;
	}

	public ActivityCancellationFeesDataValidator getActivityCancellationFeesDataValidator()
	{
		return activityCancellationFeesDataValidator;
	}

	public void setActivityCancellationFeesDataValidator(
			final ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator)
	{
		this.activityCancellationFeesDataValidator = activityCancellationFeesDataValidator;
	}

	public ActivityScheduleDataValidator getActivityScheduleDataValidator()
	{
		return activityScheduleDataValidator;
	}

	public void setActivityScheduleDataValidator(
			final ActivityScheduleDataValidator activityScheduleDataValidator)
	{
		this.activityScheduleDataValidator = activityScheduleDataValidator;
	}

	public ActivityScheduleDataUploadUtility getActivityScheduleDataUploadUtility()
	{
		return activityScheduleDataUploadUtility;
	}

	public void setActivityScheduleDataUploadUtility(
			final ActivityScheduleDataUploadUtility activityScheduleDataUploadUtility)
	{
		this.activityScheduleDataUploadUtility = activityScheduleDataUploadUtility;
	}
}
