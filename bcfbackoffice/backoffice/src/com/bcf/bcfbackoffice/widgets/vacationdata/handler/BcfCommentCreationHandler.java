/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import com.bcf.core.model.accommodation.BcfCommentModel;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class BcfCommentCreationHandler implements FlowActionHandler
{
	private static final String BCF_COMMENT_STATUS = "newBcfComment";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final BcfCommentModel bcfComment = adapter.getWidgetInstanceManager().getModel()
				.getValue(BCF_COMMENT_STATUS, BcfCommentModel.class);

		final Date truncatedStartDate = DateUtils.truncate(bcfComment.getStartDate(), Calendar.DATE);
		bcfComment.setStartDate(truncatedStartDate);
		final Date truncatedEndDate = DateUtils.truncate(bcfComment.getEndDate(), Calendar.DATE);
		bcfComment.setEndDate(truncatedEndDate);
		adapter.done();
	}
}
