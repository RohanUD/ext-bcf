/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.handler.StockLevelHandler;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class StockLevelCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private StockLevelHandler stockLevelHandler;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final StockLevelModel stockLevelModel = adapter.getWidgetInstanceManager().getModel()
				.getValue("newStockLevel", StockLevelModel.class);
		getStockLevelHandler().updateStockLevel(stockLevelModel);
		getModelService().save(stockLevelModel);
		adapter.done();
	}

	protected StockLevelHandler getStockLevelHandler()
	{
		return stockLevelHandler;
	}

	@Required
	public void setStockLevelHandler(final StockLevelHandler stockLevelHandler)
	{
		this.stockLevelHandler = stockLevelHandler;
	}
}
