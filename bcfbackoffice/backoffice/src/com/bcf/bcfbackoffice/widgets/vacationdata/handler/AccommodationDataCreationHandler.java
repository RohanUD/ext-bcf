/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.core.enums.StockLevelType;
import com.bcf.integration.dataupload.service.accommodations.AccommodationDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class AccommodationDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String ACCOMMODATION_DATA = "newAccommodationData";
	private static final String UNIQUE_CODE_ERROR = "unique.code.error";
	private static final String ROOM_NAME_ERROR = "room.name.error";
	private static final String STOCK_LEVEL_TYPE_ERROR = "stockleveltype.error";
	private static final String ICON = "z-messagebox-icon z-messagebox-exclamation";
	private static final String ACCOMMODATION_DATA_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.AccommodationData.error.title";
	private AccommodationDataService accommodationDataService;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{
		final AccommodationDataModel accommodationDataModel = adapter.getWidgetInstanceManager().getModel()
				.getValue(ACCOMMODATION_DATA, AccommodationDataModel.class);

		if (Objects.isNull(accommodationDataModel))
		{
			return;
		}
		try
		{
			if (Objects.nonNull(getAccommodationDataService().getAccommodationData(accommodationDataModel.getCode())))
			{
				Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_DATA_ERROR_TITLE), 1, ICON);

				return;
			}

		}
		catch (final AmbiguousIdentifierException ex)
		{
			Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_DATA_ERROR_TITLE), 1, ICON);

			return;
		}
		catch (final ModelNotFoundException ex)
		{
			//This means code is unique.
		}


		if (!StockLevelType.STANDARD.equals(accommodationDataModel.getStockLevelType()) && CollectionUtils
				.isNotEmpty(accommodationDataModel.getAccommodationStockLevelDataList()))
		{
			Messagebox.show(Labels.getLabel(STOCK_LEVEL_TYPE_ERROR), Labels.getLabel(ACCOMMODATION_DATA_ERROR_TITLE), 1, ICON);

			return;
		}

		accommodationDataModel.setStatus(DataImportProcessStatus.PENDING);

		if (CollectionUtils.isNotEmpty(accommodationDataModel.getExtraGuestOccupancyProductDatas()))
		{
			accommodationDataModel.getExtraGuestOccupancyProductDatas().forEach(extraGuestOccupancyProductDataModel -> {
				extraGuestOccupancyProductDataModel.setAccommodationData(accommodationDataModel);
				extraGuestOccupancyProductDataModel
						.setCode(accommodationDataModel.getCode() + "_" + extraGuestOccupancyProductDataModel.getPassengerType()
								.getCode());
				getModelService().save(extraGuestOccupancyProductDataModel);
					}
			);
		}

		if (CollectionUtils.isNotEmpty(accommodationDataModel.getMinimumNightsStayRulesData()))
		{
			accommodationDataModel.getMinimumNightsStayRulesData().forEach(minimumNightsStayRulesData -> {
				minimumNightsStayRulesData.setAccommodationData(accommodationDataModel);
						getModelService().save(minimumNightsStayRulesData);
					}
			);
		}
		if (StringUtils.isBlank(accommodationDataModel.getAccommodationName()))
		{
			Messagebox.show(Labels.getLabel(ROOM_NAME_ERROR), Labels.getLabel(ACCOMMODATION_DATA_ERROR_TITLE), 1, ICON);

			return;
		}
		getModelService().save(accommodationDataModel);
		adapter.done();
	}


	/**
	 * @return the accommodationDataService
	 */
	protected AccommodationDataService getAccommodationDataService()
	{
		return accommodationDataService;
	}


	/**
	 * @param accommodationDataService the accommodationDataService to set
	 */
	@Required
	public void setAccommodationDataService(final AccommodationDataService accommodationDataService)
	{
		this.accommodationDataService = accommodationDataService;
	}
}
