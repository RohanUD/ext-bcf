/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.bcfintegrationservice.utilitydata.strategy.RatePlanCodeGenerateStrategy;
import com.bcf.integration.dataupload.service.accommodations.RatePlanDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;

public class RatePlanDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String RATE_PLAN_DATA_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.RatePlanData.error.title";
	private static final String RATE_PLAN_DATA = "newRatePlanData";
	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";
	private static final String UNIQUE_CODE_ERROR = "unique.code.error";
	private static final String ICON = "z-messagebox-icon z-messagebox-exclamation";
	private static final String UNDERSCORE = "_";

	private RatePlanDataService ratePlanDataService;
	private RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{
		final RatePlanDataModel ratePlanDataModel = adapter.getWidgetInstanceManager().getModel()
				.getValue(RATE_PLAN_DATA, RatePlanDataModel.class);

		if (Objects.isNull(ratePlanDataModel))
		{
			return;
		}

		if (StringUtils.isEmpty(ratePlanDataModel.getCode())
				|| (StringUtils.countMatches(ratePlanDataModel.getCode(), UNDERSCORE) < 3))
		{
			ratePlanDataModel.setCode(getRatePlanCodeGenerateStrategy().getCode(ratePlanDataModel));
		}
		ratePlanDataModel.setStatus(DataImportProcessStatus.PENDING);


		if (Objects.nonNull(ratePlanDataModel.getRatePlanStartDate()) && Objects.nonNull(ratePlanDataModel.getRatePlanEndDate())
				&& ratePlanDataModel.getRatePlanEndDate().before(ratePlanDataModel.getRatePlanStartDate()))
		{
			Messagebox.show(Labels.getLabel(END_DATE_BEFORE_START_DATE), Labels.getLabel(RATE_PLAN_DATA_ERROR_TITLE), 1,
					ICON);

			return;
		}
		try
		{
			if (Objects.nonNull(getRatePlanDataService().getRatePlanData(ratePlanDataModel.getCode())))
			{
				Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(RATE_PLAN_DATA_ERROR_TITLE), 1,
						ICON);

				return;
			}
		}
		catch (final AmbiguousIdentifierException ex)
		{
			Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(RATE_PLAN_DATA_ERROR_TITLE), 1,
					ICON);

			return;
		}
		catch (final ModelNotFoundException ex)
		{
			//This means code is unique.
		}
		ratePlanDataModel.setRatePlanStartDate(Objects.isNull(ratePlanDataModel.getRatePlanStartDate()) ?
				ratePlanDataModel.getRatePlanStartDate() :
				DateUtils.truncate(ratePlanDataModel.getRatePlanStartDate(), Calendar.DATE));
		ratePlanDataModel.setRatePlanEndDate(Objects.isNull(ratePlanDataModel.getRatePlanEndDate()) ?
				ratePlanDataModel.getRatePlanEndDate() :
				DateUtils.truncate(ratePlanDataModel.getRatePlanEndDate(), Calendar.DATE));

		getModelService().save(ratePlanDataModel);
		adapter.done();
	}

	protected RatePlanDataService getRatePlanDataService()
	{
		return ratePlanDataService;
	}

	@Required
	public void setRatePlanDataService(final RatePlanDataService ratePlanDataService)
	{
		this.ratePlanDataService = ratePlanDataService;
	}

	/**
	 * @return the ratePlanCodeGenerateStrategy
	 */
	protected RatePlanCodeGenerateStrategy getRatePlanCodeGenerateStrategy()
	{
		return ratePlanCodeGenerateStrategy;
	}

	/**
	 * @param ratePlanCodeGenerateStrategy
	 *           the ratePlanCodeGenerateStrategy to set
	 */
	@Required
	public void setRatePlanCodeGenerateStrategy(final RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy)
	{
		this.ratePlanCodeGenerateStrategy = ratePlanCodeGenerateStrategy;
	}
}

