/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfbackoffice.SaleStatusDataHelper;
import com.bcf.bcfintegrationservice.model.utility.SaleStatusDataModel;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class SaleStatusDataCreationHandler implements FlowActionHandler
{
	private static final Logger LOG = Logger.getLogger(SaleStatusDataCreationHandler.class);
	private static final String NEW_SALE_STATUS_DATA = "newSaleStatusData";

	ModelService modelService;
	SaleStatusDataHelper saleStatusDataHelper;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final SaleStatusDataModel saleStatusData = adapter.getWidgetInstanceManager().getModel()
				.getValue(NEW_SALE_STATUS_DATA, SaleStatusDataModel.class);
		final Date truncatedStartDate = DateUtils.truncate(saleStatusData.getStartDate(), Calendar.DATE);
		saleStatusData.setStartDate(truncatedStartDate);

		final Date truncateEndDate = DateUtils.truncate(saleStatusData.getEndDate(), Calendar.DATE);
		saleStatusData.setEndDate(truncateEndDate);

		modelService.save(saleStatusData);
		try
		{
			getSaleStatusDataHelper()
					.updateAccommodationAndAccommodationOfferingWithSaleStatusChanges(saleStatusData);
		}
		catch (final Exception ex)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_UPDATE, NotificationEvent.Level.FAILURE,
					ex);
			LOG.error(ex);
		}

		adapter.done();
	}

	public SaleStatusDataHelper getSaleStatusDataHelper()
	{
		return saleStatusDataHelper;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setSaleStatusDataHelper(final SaleStatusDataHelper saleStatusDataHelper)
	{
		this.saleStatusDataHelper = saleStatusDataHelper;
	}
}
