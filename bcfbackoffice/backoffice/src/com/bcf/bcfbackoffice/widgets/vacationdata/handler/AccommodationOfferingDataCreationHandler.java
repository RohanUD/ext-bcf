/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import reactor.util.CollectionUtils;
import reactor.util.StringUtils;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfbackoffice.handler.AccommodationHandlerForWizard;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.integration.dataupload.service.accommodations.AccommodationsOfferingDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class AccommodationOfferingDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String ACCOMMODATION_OFFERING_DATA = "accommodationsOfferingData";
	private static final String UNIQUE_CODE_ERROR = "unique.code.error";
	private static final String ICON = "z-messagebox-icon z-messagebox-exclamation";

	private static final String ACCOMMODATION_OFFERING_OBJECT_NULL = "com.hybris.cockpitng.widgets.configurableflow.create.AccommodationOfferingData.object.null.error.message";
	private static final String ACCOMMODATION_OFFERING_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.AccommodationOfferingData.error.title";
	private static final String ACCOMMODATION_OFFERING_LANGUAGE_ERROR = "com.hybris.cockpitng.widgets.configurableflow.create.AccommodationOfferingData.english.title";
	private static final String ACCOMMODATION_OFFERING_PROPERTYTYPE_ERROR ="com.hybris.cockpitng.widgets.configurableflow.create.AccommodationOfferingData.propertyType.error";

	private AccommodationHandlerForWizard accommodationHandlerForWizard;
	private AccommodationsOfferingDataService accommodationsOfferingDataService;


	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)

	{
		final AccommodationOfferingDataModel accommodationOfferingData = adapter.getWidgetInstanceManager().getModel()
				.getValue(ACCOMMODATION_OFFERING_DATA, AccommodationOfferingDataModel.class);

		if (Objects.isNull(accommodationOfferingData))
		{
			Messagebox
					.show(Labels.getLabel(ACCOMMODATION_OFFERING_OBJECT_NULL), Labels.getLabel(ACCOMMODATION_OFFERING_ERROR_TITLE), 1,
							ICON);
			return;
		}
		if (accommodationOfferingData.getPropertyType()==null)
		{
			Messagebox
					.show(Labels.getLabel(ACCOMMODATION_OFFERING_PROPERTYTYPE_ERROR), Labels.getLabel(ACCOMMODATION_OFFERING_ERROR_TITLE),
							1, ICON);
			return;
		}
		if(Objects.nonNull(accommodationOfferingData.getDealOfTheDayStartDate()))
		{
			accommodationOfferingData.setDealOfTheDayStartDate(DateUtils.truncate(accommodationOfferingData.getDealOfTheDayStartDate(), Calendar.DATE));
		}
		if(Objects.nonNull(accommodationOfferingData.getDealOfTheDayEndDate()))
		{
			accommodationOfferingData.setDealOfTheDayEndDate(DateUtils.truncate(accommodationOfferingData.getDealOfTheDayEndDate(), Calendar.DATE));
		}


		if (accommodationOfferingData.isPartialSave())
		{
			 List<ItemModel> itemsToSave =new ArrayList<>();
			if(!CollectionUtils.isEmpty(accommodationOfferingData.getExtraProductDatas())){

				for(ExtraProductDataModel extraProductDataModel:accommodationOfferingData.getExtraProductDatas()){

					extraProductDataModel.setAccommodationOfferingCode(accommodationOfferingData.getCode());
					itemsToSave.add(extraProductDataModel);
				}
				getModelService().saveAll(itemsToSave);
			}
			getModelService().save(accommodationOfferingData);
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, accommodationOfferingData);

			adapter.done();
			return;
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getPropertyName(Locale.ENGLISH)))
		{
			Messagebox
					.show(Labels.getLabel(ACCOMMODATION_OFFERING_LANGUAGE_ERROR), Labels.getLabel(ACCOMMODATION_OFFERING_ERROR_TITLE),
							1, ICON);
			return;
		}
		try
		{
			if (Objects
					.nonNull(
							getAccommodationsOfferingDataService().getAccommodationsOfferingData(accommodationOfferingData.getCode())))
			{
				Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_OFFERING_ERROR_TITLE), 1,
						ICON);
				return;
			}
		}
		catch (final AmbiguousIdentifierException ex)
		{
			Messagebox.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_OFFERING_ERROR_TITLE), 1,
					ICON);

			return;
		}
		catch (final ModelNotFoundException ex)
		{
			//This means code is unique.
		}

		try
		{
			getAccommodationHandlerForWizard().uploadData(accommodationOfferingData);
		}catch(ObjectSavingException ex){

			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					ex);
			return;
		}
		if (DataImportProcessStatus.FAILED.equals(accommodationOfferingData.getStatus()))
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					ACCOMMODATION_OFFERING_DATA + accommodationOfferingData.getStatusDescription());
			return;
		}
		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, accommodationOfferingData);

		adapter.done();
	}

	protected AccommodationHandlerForWizard getAccommodationHandlerForWizard()
	{
		return accommodationHandlerForWizard;
	}

	@Required
	public void setAccommodationHandlerForWizard(
			final AccommodationHandlerForWizard accommodationHandlerForWizard)
	{
		this.accommodationHandlerForWizard = accommodationHandlerForWizard;
	}

	protected AccommodationsOfferingDataService getAccommodationsOfferingDataService()
	{
		return accommodationsOfferingDataService;
	}

	@Required
	public void setAccommodationsOfferingDataService(
			final AccommodationsOfferingDataService accommodationsOfferingDataService)
	{
		this.accommodationsOfferingDataService = accommodationsOfferingDataService;
	}
}
