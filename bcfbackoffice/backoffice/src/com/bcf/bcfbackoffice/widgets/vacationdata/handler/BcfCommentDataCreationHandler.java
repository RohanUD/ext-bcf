/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import com.bcf.bcfintegrationservice.model.utility.BcfCommentDataModel;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class BcfCommentDataCreationHandler implements FlowActionHandler
{
	private static final String BCF_COMMENT_DATA = "newBcfCommentData";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final BcfCommentDataModel bcfCommentData = adapter.getWidgetInstanceManager().getModel()
				.getValue(BCF_COMMENT_DATA, BcfCommentDataModel.class);
		final Date truncatedStartDate = DateUtils.truncate(bcfCommentData.getStartDate(), Calendar.DATE);
		bcfCommentData.setStartDate(truncatedStartDate);

		final Date truncateEndDate = DateUtils.truncate(bcfCommentData.getEndDate(), Calendar.DATE);
		bcfCommentData.setEndDate(truncateEndDate);

		adapter.done();


	}
}
