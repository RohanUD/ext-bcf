/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Map;
import com.bcf.bcfintegrationservice.model.utility.ExtraGuestOccupancyProductDataModel;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ExtraGuestOccupancyProductDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private final String EXTRA_OCCUPANCY_PRODUCT_DATA = "newExtraGuestOccupancyProductData";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel = adapter.getWidgetInstanceManager()
				.getModel()
				.getValue(EXTRA_OCCUPANCY_PRODUCT_DATA, ExtraGuestOccupancyProductDataModel.class);
		getModelService().save(extraGuestOccupancyProductDataModel);
		adapter.done();
	}

}
