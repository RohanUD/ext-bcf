/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.integration.dataupload.service.activities.ActivityCancellationPolicyDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ActivityCancellationPolicyDataCreationHandler implements FlowActionHandler
{

	private static final String NEW_ACTIVITY_CANCELLATION_POLICY_DATA = "newActivityCancellationPolicyData";

	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";
	private static final String UNIQUE_CODE_ERROR = "unique.code.error";
	private static final String ACTIVITY_CANCELLATION_POLICY_DATA_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.ActivityCancellationPolicyData.error.title";



	private ActivityCancellationPolicyDataService activityCancellationPolicyDataService;



	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ActivityCancellationPolicyDataModel activityCancellationPolicyData = adapter.getWidgetInstanceManager().getModel().getValue(NEW_ACTIVITY_CANCELLATION_POLICY_DATA,
				ActivityCancellationPolicyDataModel.class);

		activityCancellationPolicyData.setStartDate(Objects.isNull(activityCancellationPolicyData.getStartDate()) ?
				activityCancellationPolicyData.getStartDate() :
				DateUtils.truncate(activityCancellationPolicyData.getStartDate(), Calendar.DATE));
		activityCancellationPolicyData.setEndDate(Objects.isNull(activityCancellationPolicyData.getEndDate()) ?
				activityCancellationPolicyData.getEndDate() :
				DateUtils.truncate(activityCancellationPolicyData.getEndDate(), Calendar.DATE));

		if (Objects.isNull(activityCancellationPolicyData))
		{
			return;
		}

		if (Objects.nonNull(activityCancellationPolicyData.getStartDate())&& Objects.nonNull(activityCancellationPolicyData.getEndDate()) && activityCancellationPolicyData
				.getEndDate()
				.before(activityCancellationPolicyData.getStartDate()))
		{

				Messagebox.show(END_DATE_BEFORE_START_DATE);

				return;

		}

		try
		{
			if (Objects.nonNull(getActivityCancellationPolicyDataService()
							.getCancellationPolicyFromCode(activityCancellationPolicyData.getCode())))
			{
				Messagebox
						.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACTIVITY_CANCELLATION_POLICY_DATA_ERROR_TITLE),
								1,
								"z-messagebox-icon z-messagebox-exclamation");

				return;
			}
		}
		catch (final AmbiguousIdentifierException ex)
		{
			Messagebox
					.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACTIVITY_CANCELLATION_POLICY_DATA_ERROR_TITLE), 1,
							"z-messagebox-icon z-messagebox-exclamation");

			return;
		}

		activityCancellationPolicyData.setStatus(DataImportProcessStatus.PENDING);
		adapter.done();

	}

	public ActivityCancellationPolicyDataService getActivityCancellationPolicyDataService()
	{
		return activityCancellationPolicyDataService;
	}

	public void setActivityCancellationPolicyDataService(
			final ActivityCancellationPolicyDataService activityCancellationPolicyDataService)
	{
		this.activityCancellationPolicyDataService = activityCancellationPolicyDataService;
	}
}
