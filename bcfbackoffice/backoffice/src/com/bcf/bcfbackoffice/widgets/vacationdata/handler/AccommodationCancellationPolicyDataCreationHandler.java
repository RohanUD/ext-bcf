/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationPolicyDataService;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class AccommodationCancellationPolicyDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String ACCOMMODATION_CANCELLATION_POLICY_DATA = "newAccommodationCancellationPolicyData";
	private static final String UNIQUE_CODE_ERROR = "unique.code.error";
	private static final String ACCOMMODATION_CANCELLATION_POLICY_DATA_ERROR_TITLE = "com.hybris.cockpitng.widgets.configurableflow.create.AccommodationCancellationPolicyData.error.title";
	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";

	private AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData = adapter.getWidgetInstanceManager()
				.getModel()
				.getValue(ACCOMMODATION_CANCELLATION_POLICY_DATA, AccommodationCancellationPolicyDataModel.class);

		if (Objects.isNull(accommodationCancellationPolicyData))
		{
			return;
		}
		accommodationCancellationPolicyData.setStartDate(Objects.isNull(accommodationCancellationPolicyData.getStartDate()) ?
				accommodationCancellationPolicyData.getStartDate() :
				DateUtils.truncate(accommodationCancellationPolicyData.getStartDate(), Calendar.DATE));
		accommodationCancellationPolicyData.setEndDate(Objects.isNull(accommodationCancellationPolicyData.getEndDate()) ?
				accommodationCancellationPolicyData.getEndDate() :
				DateUtils.truncate(accommodationCancellationPolicyData.getEndDate(), Calendar.DATE));

		if (accommodationCancellationPolicyData.isPartialSave())
		{
			getModelService().save(accommodationCancellationPolicyData);
			adapter.done();
			return;
		}

		if (Objects.nonNull(accommodationCancellationPolicyData.getStartDate()) && Objects
				.nonNull(accommodationCancellationPolicyData.getEndDate()) && accommodationCancellationPolicyData
				.getEndDate()
				.before(accommodationCancellationPolicyData.getStartDate()))
		{
			Messagebox.show(END_DATE_BEFORE_START_DATE);

			return;
		}

		try
		{
			if (Objects.nonNull(getAccommodationCancellationPolicyDataService()
					.getAccommodationCancellationPolicyDataByCode(accommodationCancellationPolicyData.getCode())))
			{
				Messagebox
						.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_CANCELLATION_POLICY_DATA_ERROR_TITLE),
								1,
								"z-messagebox-icon z-messagebox-exclamation");

				return;
			}
		}
		catch (final AmbiguousIdentifierException ex)
		{
			Messagebox
					.show(Labels.getLabel(UNIQUE_CODE_ERROR), Labels.getLabel(ACCOMMODATION_CANCELLATION_POLICY_DATA_ERROR_TITLE), 1,
							"z-messagebox-icon z-messagebox-exclamation");

			return;
		}

		accommodationCancellationPolicyData.setStatus(DataImportProcessStatus.PENDING);
		adapter.done();
	}

	protected AccommodationCancellationPolicyDataService getAccommodationCancellationPolicyDataService()
	{
		return accommodationCancellationPolicyDataService;
	}

	@Required
	public void setAccommodationCancellationPolicyDataService(
			final AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService)
	{
		this.accommodationCancellationPolicyDataService = accommodationCancellationPolicyDataService;
	}
}
