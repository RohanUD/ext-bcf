/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.integration.dataupload.utility.deals.DealsBundleDataUploadUtility;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class DealBundleTemplateCreationHandler implements FlowActionHandler
{
	private static final String NEWDEALBUNDLETEMPLATE = "newDealBundleTemplate";
	private ModelService modelService;
	private PropertySourceFacade propertySourceFacade;
	private DealsBundleDataUploadUtility dealsBundleDataUploadUtility;
	private AbstractUrlResolver<DealBundleTemplateModel> dealBundleTemplateModelUrlResolver;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{
		final DealBundleTemplateModel dealBundleTemplate = adapter.getWidgetInstanceManager().getModel()
				.getValue(NEWDEALBUNDLETEMPLATE, DealBundleTemplateModel.class);
		createAndSetSeoUrlIfEmpty(dealBundleTemplate);
		final String validationMessage;
		if (Objects.isNull(dealBundleTemplate))
		{
			validationMessage = "dealbundletemplate.null.error";
		}
		else
		{
			validationMessage = validateDealBundleTemplate(dealBundleTemplate);
		}

		if (StringUtils.isNotEmpty(validationMessage))
		{
			final String errorMessage = getPropertySourceFacade().getPropertySourceValueMessage(validationMessage, null);
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE, errorMessage);
			return;
		}
		try
		{
			getModelService().save(dealBundleTemplate);
			getDealsBundleDataUploadUtility().indexDeals();
			adapter.done();
		}
		catch (final ModelSavingException mse)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE, mse);
		}
	}

	private void createAndSetSeoUrlIfEmpty(final DealBundleTemplateModel dealBundleTemplateModel)
	{
		if(StringUtils.isEmpty(dealBundleTemplateModel.getSeoUrl())){
			dealBundleTemplateModel.setSeoUrl(getDealBundleTemplateModelUrlResolver().resolve(dealBundleTemplateModel));
		}
	}

	/**
	 * Validate deal bundle template.
	 *
	 * @param dealBundleTemplate the deal bundle template
	 * @return the string
	 */
	protected String validateDealBundleTemplate(final DealBundleTemplateModel dealBundleTemplate)
	{
		String validationMessage = StringUtils.EMPTY;
		if (StringUtils.isEmpty(dealBundleTemplate.getName()))
		{
			validationMessage = "dealbundletemplate.name.empty";
			return validationMessage;
		}
		if (StringUtils.isEmpty(dealBundleTemplate.getStartingDatePattern()))
		{
			validationMessage = "dealbundletemplate.startingdatepattern.empty";
			return validationMessage;
		}
		if (CollectionUtils.isEmpty(dealBundleTemplate.getChildTemplates()) || !dealBundleTemplate.getChildTemplates().stream()
				.allMatch(childTemplate -> childTemplate instanceof DealBundleTemplateModel))
		{
			validationMessage = "dealbundletemplate.childbundles.invalid";
			return validationMessage;
		}
		if (CollectionUtils.isEmpty(dealBundleTemplate.getDealTypes()))
		{
			validationMessage = "dealbundletemplate.dealtypes.empty";
			return validationMessage;
		}
		if (CollectionUtils.isEmpty(dealBundleTemplate.getDateRanges()))
		{
			validationMessage = "dealbundletemplate.dateranges.empty";
			return validationMessage;
		}
		if (CollectionUtils.isEmpty(dealBundleTemplate.getGuestCounts()))
		{
			validationMessage = "dealbundletemplate.guestcounts.empty";
			return validationMessage;
		}
		if (dealBundleTemplate.getLength() <= 0)
		{
			validationMessage = "dealbundletemplate.durationofstay.invalid";
			return validationMessage;
		}
		if (Objects.isNull(dealBundleTemplate.getFromPrice()))
		{
			validationMessage = "dealbundletemplate.fromprice.empty";
			return validationMessage;
		}
		if (Objects.isNull(dealBundleTemplate.getFromPrice()))
		{
			validationMessage = "dealbundletemplate.seourl.empty ";
			return validationMessage;
		}
		return validationMessage;
	}

	/**
	 * Gets the model service.
	 *
	 * @return the model service
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets the model service.
	 *
	 * @param modelService the new model service
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * Gets the property source facade.
	 *
	 * @return the property source facade
	 */
	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	/**
	 * Sets the property source facade.
	 *
	 * @param propertySourceFacade the new property source facade
	 */
	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	/**
	 * Gets the deals bundle data upload utility.
	 *
	 * @return the deals bundle data upload utility
	 */
	protected DealsBundleDataUploadUtility getDealsBundleDataUploadUtility()
	{
		return dealsBundleDataUploadUtility;
	}

	/**
	 * Sets the deals bundle data upload utility.
	 *
	 * @param dealsBundleDataUploadUtility the new deals bundle data upload utility
	 */
	@Required
	public void setDealsBundleDataUploadUtility(
			final DealsBundleDataUploadUtility dealsBundleDataUploadUtility)
	{
		this.dealsBundleDataUploadUtility = dealsBundleDataUploadUtility;
	}

	protected AbstractUrlResolver<DealBundleTemplateModel> getDealBundleTemplateModelUrlResolver()
	{
		return dealBundleTemplateModelUrlResolver;
	}

	@Required
	public void setDealBundleTemplateModelUrlResolver(
			final AbstractUrlResolver<DealBundleTemplateModel> dealBundleTemplateModelUrlResolver)
	{
		this.dealBundleTemplateModelUrlResolver = dealBundleTemplateModelUrlResolver;
	}
}
