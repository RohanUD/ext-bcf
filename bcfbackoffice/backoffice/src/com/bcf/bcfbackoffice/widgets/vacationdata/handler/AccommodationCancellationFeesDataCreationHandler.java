/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Map;
import java.util.Objects;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class AccommodationCancellationFeesDataCreationHandler  extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String ACCOMMODATION_CANCELLATION_FEES_DATA = "newAccommodationCancellationFeesData";
	private static final String INVALID_MIN_DAYS = "invalid.minDays";
	private static final String INVALID_MAX_DAYS = "invalid.maxDays";
	private static final String INVALID_MIN_MAX_DAYS = "invalid.min.max.Days";
	private static final String INVALID_CHANGE_FEE_TYPE = "invalid.change.fee.type";
	private static final String INVALID_CANCELLATION_FEE_TYPE = "invalid.cancellation.fee.type";
	private static final String INVALID_CANCELLATION_FEE = "invalid.cancellation.fee";
	private static final String INVALID_CHANGE_FEE = "invalid.change.fee";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final AccommodationCancellationFeesDataModel accommodationCancellationFeesDataModel = adapter.getWidgetInstanceManager()
				.getModel().getValue(ACCOMMODATION_CANCELLATION_FEES_DATA, AccommodationCancellationFeesDataModel.class);

		if (Objects.isNull(accommodationCancellationFeesDataModel))
		{
			return;
		}
		
		if (Objects.isNull(accommodationCancellationFeesDataModel.getMinDays())
				|| accommodationCancellationFeesDataModel.getMinDays() < 0)
		{
			Messagebox.show(INVALID_MIN_DAYS);
			return;
		}

		if (Objects.isNull(accommodationCancellationFeesDataModel.getMaxDays())
				|| accommodationCancellationFeesDataModel.getMaxDays() < 0)
		{
			Messagebox.show(INVALID_MAX_DAYS);
			return;
		}

		if (accommodationCancellationFeesDataModel.getMaxDays() < accommodationCancellationFeesDataModel.getMinDays())
		{
			Messagebox.show(INVALID_MIN_MAX_DAYS);
			return;
		}

		if (Objects.isNull(accommodationCancellationFeesDataModel.getCancellationFeeType()))
		{
			Messagebox.show(INVALID_CANCELLATION_FEE_TYPE);
			return;
		}

		if (Objects.isNull(accommodationCancellationFeesDataModel.getChangeFeeType()))
		{
			Messagebox.show(INVALID_CHANGE_FEE_TYPE);
			return;
		}

		if (Objects.isNull(accommodationCancellationFeesDataModel.getCancellationFee()))
		{
			Messagebox.show(INVALID_CANCELLATION_FEE);
			return;
		}

		if (Objects.isNull(accommodationCancellationFeesDataModel.getChangeFee()))
		{
			Messagebox.show(INVALID_CHANGE_FEE);
			return;
		}


		accommodationCancellationFeesDataModel.setStatus(DataImportProcessStatus.PENDING);
		getModelService().save(accommodationCancellationFeesDataModel);
		adapter.done();
	}
}
