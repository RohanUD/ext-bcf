/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.constants.BcfbackofficeConstants;
import com.bcf.integration.dataupload.utility.accommodations.ExtraProductDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.ExtraProductPricesDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductDataValidator;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductPriceDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ExtraProductDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final Logger LOG = Logger.getLogger(ExtraProductDataCreationHandler.class);
	private static final String EXTRA_PRODUCT_DATA = "newExtraProductData";
	private static final String EXTRA_PRODUCT_DATA_MSG = "Extra Product Data, ";

	private ExtraProductDataValidator extraProductDataValidator;
	private ExtraProductPriceDataValidator  extraProductPriceDataValidator;
	private ExtraProductDataUploadUtility extraProductDataUploadUtility;
	private ExtraProductPricesDataUploadUtility extraProductPricesDataUploadUtility;
	private CatalogVersionService catalogVersionService;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ExtraProductDataModel extraProductData = adapter.getWidgetInstanceManager().getModel()
				.getValue(EXTRA_PRODUCT_DATA, ExtraProductDataModel.class);

		final Collection<ExtraProductPriceDataModel> extraProductPriceDataModelList = extraProductData.getExtraProductPriceDataList();

		//Validate extra product and price
		if (!validateExtraProductData(extraProductData, extraProductPriceDataModelList,adapter))
		{
			return;
		}

		extraProductPriceDataModelList.stream()
				.forEach(extraProductPriceDataModel -> extraProductPricesDataUploadUtility.truncateDates(extraProductPriceDataModel));
		extraProductData.setStatus(DataImportProcessStatus.PROCESSING);
		extraProductPriceDataModelList.stream()
				.forEach(extraProductPriceDataModel -> extraProductPriceDataModel.setStatus(DataImportProcessStatus.PROCESSING));



		final CatalogVersionModel catalogVersion = getCatalogVersionService()
				.getCatalogVersion(BcfbackofficeConstants.PRODUCTCATALOG, BcfbackofficeConstants.CATALOGVERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final List<ItemModel> items = new ArrayList<>();
		if (!createExtraProduct(extraProductData, extraProductPriceDataModelList, catalogVersion,
				adapter, items))
		{
			return;
		}

		extraProductPriceDataModelList
				.forEach(extraProductPriceDataModel -> extraProductPricesDataUploadUtility.handleSuccess(extraProductPriceDataModel));

		extraProductDataUploadUtility.handleSuccess(extraProductData, items);
		getModelService().saveAll();
		getModelService().save(extraProductData);
		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, extraProductData);
		adapter.done();
	}

	private boolean validateExtraProductData(final ExtraProductDataModel extraProductData,
			final Collection<ExtraProductPriceDataModel> extraProductPriceDataModelList,
			final FlowActionHandlerAdapter adapter)
	{

		if (Objects.isNull(extraProductData) || Objects.isNull(extraProductPriceDataModelList) || extraProductPriceDataModelList.isEmpty()
				|| !getExtraProductDataValidator().validateExtraProductData(extraProductData).isEmpty())
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					EXTRA_PRODUCT_DATA_MSG + extraProductData.getStatusDescription());
			return Boolean.FALSE;

		}
		final List<ExtraProductPriceDataModel> invalidExtraProductPriceData = extraProductPriceDataModelList.stream()
				.filter(extraProductPriceDataModel -> !extraProductPriceDataValidator
						.validateExtraProductPriceData(extraProductPriceDataModel).isEmpty()).collect(Collectors.toList());

		if (!invalidExtraProductPriceData.isEmpty())
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					EXTRA_PRODUCT_DATA_MSG + invalidExtraProductPriceData.get(0).getStatusDescription());
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}


	private Boolean createExtraProduct(final ExtraProductDataModel extraProductDataModel,
			final Collection<ExtraProductPriceDataModel> extraProductPriceDataModelList,
			final CatalogVersionModel catalogVersion,
			final FlowActionHandlerAdapter adapter, final List<ItemModel> items)
	{
		final Transaction tx = Transaction.current();
		Boolean result = false;
		try
		{
			tx.begin();
			final List<ItemModel> itemsToSave = new ArrayList<>();


			for (final ExtraProductPriceDataModel extraProductPriceDataModel : extraProductPriceDataModelList)
			{
				extraProductPriceDataModel.setExtraProductData(extraProductDataModel);
				itemsToSave.add(extraProductPriceDataModel);

			}
			extraProductDataModel.setExtraProductPriceDataList(extraProductPriceDataModelList);
			itemsToSave.add(extraProductDataModel);

			if(extraProductDataModel.isPartialSave()){
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, extraProductDataModel);
				getModelService().saveAll(itemsToSave);
				tx.commit();
				result = Boolean.TRUE;
				return true;
			}
			final Boolean status = extraProductDataUploadUtility.uploadExtraProductData(Collections.singletonList(extraProductDataModel),
					catalogVersion, items,itemsToSave,true);
			if (BooleanUtils.isFalse(status))
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						EXTRA_PRODUCT_DATA_MSG + extraProductDataModel.getStatusDescription());
				return Boolean.FALSE;
			}
			getModelService().saveAll(itemsToSave);

			extraProductPriceDataModelList.forEach(priceData -> priceData.setExtraProductData(extraProductDataModel));

			final List<ExtraProductPriceDataModel> invalidExtraProductPriceData = extraProductPriceDataModelList.stream()
					.filter(extraProductPriceDataModel -> !extraProductPricesDataUploadUtility
							.uploadExtraProductPrices(extraProductPriceDataModel, catalogVersion, items))
					.collect(Collectors.toList());

			if (!invalidExtraProductPriceData.isEmpty())
			{
				NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
						NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
						EXTRA_PRODUCT_DATA_MSG + invalidExtraProductPriceData.get(0).getStatusDescription());
				return Boolean.FALSE;
			}

			getModelService().saveAll();
			getModelService().save(extraProductDataModel);
			LOG.info("Transaction Commit");
			tx.commit();
			result = Boolean.TRUE;
			return Boolean.TRUE;
		}
		catch (final ModelSavingException mse)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(adapter.getWidgetInstanceManager()),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE, mse);
			LOG.error("Transaction RollBack : " + mse);
			return Boolean.FALSE;
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}

	public ExtraProductPricesDataUploadUtility getExtraProductPricesDataUploadUtility()
	{
		return extraProductPricesDataUploadUtility;
	}

	public void setExtraProductPricesDataUploadUtility(
			final ExtraProductPricesDataUploadUtility extraProductPricesDataUploadUtility)
	{
		this.extraProductPricesDataUploadUtility = extraProductPricesDataUploadUtility;
	}

	protected ExtraProductDataValidator getExtraProductDataValidator()
	{
		return extraProductDataValidator;
	}

	@Required
	public void setExtraProductDataValidator(
			final ExtraProductDataValidator extraProductDataValidator)
	{
		this.extraProductDataValidator = extraProductDataValidator;
	}

	public ExtraProductPriceDataValidator getExtraProductPriceDataValidator()
	{
		return extraProductPriceDataValidator;
	}
	@Required
	public void setExtraProductPriceDataValidator(
			final ExtraProductPriceDataValidator extraProductPriceDataValidator)
	{
		this.extraProductPriceDataValidator = extraProductPriceDataValidator;
	}

	public ExtraProductDataUploadUtility getExtraProductDataUploadUtility()
	{
		return extraProductDataUploadUtility;
	}

	public void setExtraProductDataUploadUtility(
			final ExtraProductDataUploadUtility extraProductDataUploadUtility)
	{
		this.extraProductDataUploadUtility = extraProductDataUploadUtility;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
