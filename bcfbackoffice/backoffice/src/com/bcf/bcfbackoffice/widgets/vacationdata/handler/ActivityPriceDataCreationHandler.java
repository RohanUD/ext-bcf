/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ActivityPriceDataCreationHandler implements FlowActionHandler
{
	private static final String NEW_ACTIVITY_PRICE_DATA = "newActivityPriceData";

	private ModelService modelService;

	/**
	 * this method checks if price data is created while creating a new activity data, then remove the activity data from price data as they are getting attached in ActivitiesDataUpload.
	 * Also do not checked for Overlapping ActivityPriceData at this time as we do not have information of activities data at this time which we need in overlapping checking process.
	 * <br>
	 * Overlapping of ActivityPriceData will only be checked in the case where activities data is already created and a new price data is added afterwards
	 *
	 * @param customType
	 * @param adapter
	 * @param map
	 */
	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ActivityPriceDataModel activityPriceDataModel = adapter.getWidgetInstanceManager().getModel()
				.getValue(NEW_ACTIVITY_PRICE_DATA, ActivityPriceDataModel.class);

		activityPriceDataModel.setStatus(DataImportProcessStatus.PENDING);

		if (modelService.isNew(activityPriceDataModel.getActivitiesData()))
		{
			activityPriceDataModel.setActivitiesData(null);
		}
		else
		{
			getModelService().save(activityPriceDataModel);
			getModelService().refresh(activityPriceDataModel);
			deleteOverlappingActivityPriceData(activityPriceDataModel);
		}
		adapter.done();
	}

	private void deleteOverlappingActivityPriceData(final ActivityPriceDataModel activityPriceData)
	{
		String daysOfWeekCode = BCFDateUtils.createDaysOfWeekCode(activityPriceData.getDaysOfWeek());
		Date startDate = activityPriceData.getStartDate();
		Date endDate = activityPriceData.getEndDate();
		String passengerCode = activityPriceData.getPassengerType().getCode();

		ActivitiesDataModel activitiesData = activityPriceData.getActivitiesData();
		Collection<ActivityPriceDataModel> previousActivityPriceDataList = StreamUtil
				.safeStream(activitiesData
						.getActivityPriceDataList()).filter(savedPrice -> savedPrice != activityPriceData).collect(Collectors.toList());

		List<ActivityPriceDataModel> overlappingActivityPriceData = StreamUtil.safeStream(previousActivityPriceDataList)
				.filter(savedPrice -> BCFDateUtils.checkIfDatesOverlapped(startDate, endDate, savedPrice.getStartDate(),
						savedPrice.getEndDate()))
				.filter(savedPrice -> StringUtils.equals(passengerCode, savedPrice.getPassengerType().getCode()))
				.filter(
						savedPrice -> StringUtils.equals(daysOfWeekCode, BCFDateUtils.createDaysOfWeekCode(savedPrice.getDaysOfWeek())))
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(overlappingActivityPriceData))
		{
			List<ActivityPriceDataModel> activityPriceDataWithoutOverlappingList = StreamUtil
					.safeStream(activitiesData.getActivityPriceDataList())
					.filter(savedPrice -> !overlappingActivityPriceData.contains(savedPrice)).collect(
							Collectors.toList());
			activitiesData.setActivityPriceDataList(activityPriceDataWithoutOverlappingList);

			getModelService().removeAll(overlappingActivityPriceData);
			getModelService().save(activitiesData);
		}
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
