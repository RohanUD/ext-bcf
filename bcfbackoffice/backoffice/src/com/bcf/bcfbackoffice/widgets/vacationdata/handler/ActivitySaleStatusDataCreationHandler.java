/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import com.bcf.bcfintegrationservice.model.utility.ActivitySaleStatusDataModel;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ActivitySaleStatusDataCreationHandler implements FlowActionHandler
{
	private static final String NEW_SALE_STATUS_DATA = "newSaleStatusData";

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ActivitySaleStatusDataModel saleStatusData = adapter.getWidgetInstanceManager().getModel()
				.getValue(NEW_SALE_STATUS_DATA, ActivitySaleStatusDataModel.class);
		final Date truncatedStartDate = DateUtils.truncate(saleStatusData.getStartDate(), Calendar.DATE);
		saleStatusData.setStartDate(truncatedStartDate);

		final Date truncateEndDate = DateUtils.truncate(saleStatusData.getEndDate(), Calendar.DATE);
		saleStatusData.setEndDate(truncateEndDate);

		adapter.done();
	}
}
