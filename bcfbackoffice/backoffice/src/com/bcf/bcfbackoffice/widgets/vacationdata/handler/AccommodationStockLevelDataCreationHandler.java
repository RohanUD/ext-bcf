/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsStockLevelDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class AccommodationStockLevelDataCreationHandler extends AbstractBcfActionHandler implements FlowActionHandler
{
	private static final String ACCOMMODATION_STOCK_DATA = "newAccommodationStockLevelData";
	private static final String END_DATE_BEFORE_START_DATE = "enddate.before.startdate.error";
	private static final String INVALID_STOCK = "invalid.stock";

	AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final AccommodationStockLevelDataModel accommodationStockLevelDataModel = adapter.getWidgetInstanceManager().getModel()
				.getValue(ACCOMMODATION_STOCK_DATA, AccommodationStockLevelDataModel.class);

		if (Objects.isNull(accommodationStockLevelDataModel))
		{
			return;
		}

		if (Objects.nonNull(accommodationStockLevelDataModel.getEndDate()) && accommodationStockLevelDataModel.getEndDate()
				.before(accommodationStockLevelDataModel.getStartDate()))
		{
			Messagebox.show(END_DATE_BEFORE_START_DATE);

			return;
		}

		if (Objects.isNull(accommodationStockLevelDataModel.getStockLevel())
				|| accommodationStockLevelDataModel.getStockLevel() < 0)
		{
			Messagebox.show(INVALID_STOCK);
			return;
		}

		accommodationStockLevelDataModel.setStatus(DataImportProcessStatus.PENDING);
		accommodationStockLevelDataModel.setStartDate(Objects.isNull(accommodationStockLevelDataModel.getStartDate()) ?
				accommodationStockLevelDataModel.getStartDate() :
				DateUtils.truncate(accommodationStockLevelDataModel.getStartDate(), Calendar.DATE));
		accommodationStockLevelDataModel.setEndDate(Objects.isNull(accommodationStockLevelDataModel.getEndDate()) ?
				accommodationStockLevelDataModel.getEndDate() :
				DateUtils.truncate(accommodationStockLevelDataModel.getEndDate(), Calendar.DATE));
		getModelService().save(accommodationStockLevelDataModel);
		getModelService().refresh(accommodationStockLevelDataModel);
		accommodationsStockLevelDataUploadUtility.uploadAccommodationStockLevelDatas();
		adapter.done();
	}

	@Required
	public void setAccommodationsStockLevelDataUploadUtility(
			final AccommodationsStockLevelDataUploadUtility accommodationsStockLevelDataUploadUtility)
	{
		this.accommodationsStockLevelDataUploadUtility = accommodationsStockLevelDataUploadUtility;
	}
}
