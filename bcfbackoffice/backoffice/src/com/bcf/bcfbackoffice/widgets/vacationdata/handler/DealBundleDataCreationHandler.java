/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.integration.dataupload.utility.deals.DealsBundleDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEventTypes;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class DealBundleDataCreationHandler implements FlowActionHandler
{
	private static final String NEW_DEALS_BUNDLEDATA = "newDealsBundleData";
	private static final String DEAL_BUNDLE = "Deal Bundle, ";
	private static final String DEAL_BUNDLE_SUCCESS = "Deal bundle created successfully";
	private static final String ENABLE_TRANSACTIONAL_SAVES = "enableTransactionalSaves";

	private ModelService modelService;
	private DealsBundleDataUploadUtility dealsBundleDataUploadUtility;
	private SessionService sessionService;

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{
		final DealsBundleDataModel dealBundleData = adapter.getWidgetInstanceManager().getModel().getValue(NEW_DEALS_BUNDLEDATA,
				DealsBundleDataModel.class);
		if (Objects.isNull(dealBundleData))
		{
			return;
		}
		final boolean status = uploadData(adapter.getWidgetInstanceManager(), dealBundleData);
		if (status)
		{
			adapter.done();
		}
	}

	/**
	 * Upload data.
	 *
	 * @param widgetInstanceManager the widget instance manager
	 * @param dealBundleData        the deal bundle data
	 * @return true, if successful
	 */
	public boolean uploadData(final WidgetInstanceManager widgetInstanceManager, final DealsBundleDataModel dealBundleData)
	{
		final List<ItemModel> items = new ArrayList<>();
		final CatalogVersionModel catalogVersion = dealBundleData.getCatalogVersion();
		final Boolean status = getDealsBundleDataUploadUtility().uploadDealBundleData(dealBundleData, catalogVersion, items);
		if (BooleanUtils.isFalse(status))
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE,
					DEAL_BUNDLE + dealBundleData.getStatusDescription());
			return false;
		}

		try
		{
			getSessionService().setAttribute(ENABLE_TRANSACTIONAL_SAVES, Boolean.TRUE);
			getModelService().saveAll();
			getDealsBundleDataUploadUtility().synchronizeProductCatalog(items);
			getDealsBundleDataUploadUtility().indexDeals();
			dealBundleData.setStatus(DataImportProcessStatus.SUCCESS);
			dealBundleData.setStatusDescription(DEAL_BUNDLE_SUCCESS);
			getModelService().save(dealBundleData);
		}
		catch (final ModelSavingException mse)
		{
			NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
					NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.FAILURE, mse);
			return false;
		}

		NotificationUtils.notifyUser(NotificationUtils.getWidgetNotificationSource(widgetInstanceManager),
				NotificationEventTypes.EVENT_TYPE_OBJECT_CREATION, NotificationEvent.Level.SUCCESS, dealBundleData);
		return true;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the dealsBundleDataUploadUtility
	 */
	protected DealsBundleDataUploadUtility getDealsBundleDataUploadUtility()
	{
		return dealsBundleDataUploadUtility;
	}

	/**
	 * @param dealsBundleDataUploadUtility the dealsBundleDataUploadUtility to set
	 */
	@Required
	public void setDealsBundleDataUploadUtility(final DealsBundleDataUploadUtility dealsBundleDataUploadUtility)
	{
		this.dealsBundleDataUploadUtility = dealsBundleDataUploadUtility;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

}
