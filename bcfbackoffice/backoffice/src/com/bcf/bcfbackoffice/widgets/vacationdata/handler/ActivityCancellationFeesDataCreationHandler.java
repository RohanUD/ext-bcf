/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfbackoffice.widgets.vacationdata.handler;

import java.util.Map;
import java.util.Objects;
import org.zkoss.zul.Messagebox;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationFeesDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandler;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;


public class ActivityCancellationFeesDataCreationHandler implements FlowActionHandler
{

	private static final String NEW_ACTIVITY_CANCELLATION_POLICY_DATA = "newActivityCancellationFeesData";
	private static final String INVALID_MIN_DAYS = "invalid.minDays";
	private static final String INVALID_MAX_DAYS = "invalid.maxDays";
	private static final String INVALID_MIN_MAX_DAYS = "invalid.min.max.Days";
	private static final String INVALID_CHANGE_FEE_TYPE = "invalid.change.fee.type";
	private static final String INVALID_CANCELLATION_FEE_TYPE = "invalid.cancellation.fee.type";
	private static final String INVALID_CANCELLATION_FEE = "invalid.cancellation.fee";
	private static final String INVALID_CHANGE_FEE = "invalid.change.fee";


	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter,
			final Map<String, String> map)
	{
		final ActivityCancellationFeesDataModel activityCancellationFeesDataModel = adapter.getWidgetInstanceManager()
				.getModel().getValue(NEW_ACTIVITY_CANCELLATION_POLICY_DATA, ActivityCancellationFeesDataModel.class);

		if (Objects.isNull(activityCancellationFeesDataModel))
		{
			return;
		}

		if (Objects.isNull(activityCancellationFeesDataModel.getCancellationFeeType()))
		{
			Messagebox.show(INVALID_CANCELLATION_FEE_TYPE);
			return;
		}

		if (Objects.isNull(activityCancellationFeesDataModel.getChangeFeeType()))
		{
			Messagebox.show(INVALID_CHANGE_FEE_TYPE);
			return;
		}

		if (Objects.isNull(activityCancellationFeesDataModel.getCancellationFee()))
		{
			Messagebox.show(INVALID_CANCELLATION_FEE);
			return;
		}

		if (Objects.isNull(activityCancellationFeesDataModel.getChangeFee()))
		{
			Messagebox.show(INVALID_CHANGE_FEE);
			return;
		}


		if (Objects.isNull(activityCancellationFeesDataModel.getMinDays())
				|| activityCancellationFeesDataModel.getMinDays() < 0)
		{
			Messagebox.show(INVALID_MIN_DAYS);
			return;
		}

		if (Objects.isNull(activityCancellationFeesDataModel.getMaxDays())
				|| activityCancellationFeesDataModel.getMaxDays() < 0)
		{
			Messagebox.show(INVALID_MAX_DAYS);
			return;
		}

		if (activityCancellationFeesDataModel.getMaxDays() < activityCancellationFeesDataModel.getMinDays())
		{
			Messagebox.show(INVALID_MIN_MAX_DAYS);
			return;
		}


		activityCancellationFeesDataModel.setStatus(DataImportProcessStatus.PENDING);
		adapter.done();
	}
}
