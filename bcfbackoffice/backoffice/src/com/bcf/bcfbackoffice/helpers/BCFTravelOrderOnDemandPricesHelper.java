/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfbackoffice.helpers;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.integration.commons.OndemandDiscountedOrderEntry;
import de.hybris.platform.travelbackoffice.helpers.TravelOrderEntryInfo;
import de.hybris.platform.travelbackoffice.helpers.TravelOrderInfo;
import de.hybris.platform.travelbackoffice.helpers.TravelOrderOnDemandPricesHelper;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.util.TaxValue;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import com.bcf.bcfbackoffice.util.BCFBackofficeUtil;


public class BCFTravelOrderOnDemandPricesHelper extends TravelOrderOnDemandPricesHelper
{
	@Override
	public TravelOrderInfo estimateOrderInfo(final AbstractOrderModel order)
	{
		final TravelOrderInfo traveOrderInfo = new TravelOrderInfo();
		final Double totalTax = order.getTotalTax();
		final Double totalPrice = order.getTotalPrice();
		final Double totalPriceWithoutTax = totalPrice - totalTax;

		final BigDecimal totalTaxBD = new BigDecimal(totalTax).setScale(2, RoundingMode.HALF_UP);
		final BigDecimal totalPriceBD = new BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP);
		final BigDecimal totalPriceWithoutTaxBD = new BigDecimal(totalPriceWithoutTax).setScale(2, RoundingMode.HALF_UP);

		traveOrderInfo.setTotalTax(totalTaxBD.toString());
		traveOrderInfo.setTotalPrice(totalPriceWithoutTaxBD.toString());
		traveOrderInfo.setTotalWithTax(totalPriceBD.toString());

		final List<OndemandDiscountedOrderEntry> entryList = getOnDemandPromotionService()
				.calculateProportionalDiscountForEntries(order);
		OndemandDiscountedOrderEntry entry;
		for (final Iterator var7 = entryList.iterator(); var7.hasNext(); )
		{
			entry = (OndemandDiscountedOrderEntry) var7.next();
			traveOrderInfo.getTravelOrderEntryList().add(this.createEntryInfo(entry));
		}
		return traveOrderInfo;
	}

	private TravelOrderEntryInfo createEntryInfo(final OndemandDiscountedOrderEntry entry)
	{
		final TravelOrderEntryInfo travelOrderEntryInfo = new TravelOrderEntryInfo();

		String entryDescription = StringUtils.EMPTY;
		final AbstractOrderEntryModel abstractOrderEntryModel = entry.getOrderEntry();
		if (OrderEntryType.ACCOMMODATION.equals(abstractOrderEntryModel.getType()))
		{
			final ProductModel roomRateProuctModel = abstractOrderEntryModel.getProduct();
			if (roomRateProuctModel instanceof RoomRateProductModel)
			{
				entryDescription = BCFBackofficeUtil.getRoomRateProductLabel(((RoomRateProductModel) roomRateProuctModel));
			}
		}
		else
		{
			entryDescription = StringUtils.isNotBlank(entry.getOrderEntry().getProduct().getName()) ?
					entry.getOrderEntry().getProduct().getName() :
					entry.getOrderEntry().getProduct().getCode();
		}
		travelOrderEntryInfo.setEntryDesc(entryDescription);
		travelOrderEntryInfo.setUnitPrice(entry.getDiscountedUnitPrice().toString());
		travelOrderEntryInfo.setEntryPrice(entry.getDiscountedLinePrice().toString());
		final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();
		final BigDecimal unitTaxValue = getOndemandTaxCalculationService()
				.calculatePreciseUnitTax(orderEntry.getTaxValues(), orderEntry.getQuantity().doubleValue(),
						orderEntry.getOrder().getNet());

		String unitTax = "0.00";
		if (OrderEntryType.ACCOMMODATION.equals(orderEntry.getType()) || OrderEntryType.ACTIVITY.equals(orderEntry.getType()))
		{
			if (Objects.nonNull(orderEntry.getTaxValues()) && !orderEntry.getTaxValues().isEmpty())
			{
				final TaxValue gstTaxValue = orderEntry.getTaxValues().stream()
						.filter(taxValue -> "GST".equals(taxValue.getCode())).findFirst().orElse(null);
				if (Objects.nonNull(gstTaxValue))
				{
					final BigDecimal gstTax = BigDecimal.valueOf(gstTaxValue.getValue()).setScale(2, RoundingMode.HALF_UP);
					unitTax = Double.toString(gstTax.doubleValue());
					travelOrderEntryInfo.setUnitTax(unitTax);
				}
			}
		}
		else
		{
			travelOrderEntryInfo.setUnitTax(unitTax);
		}

		final BigDecimal taxValue = unitTaxValue.multiply(BigDecimal.valueOf(orderEntry.getQuantity().doubleValue()))
				.setScale(2, RoundingMode.HALF_UP);
		travelOrderEntryInfo.setEntryTax(Double.toString(taxValue.doubleValue()));
		travelOrderEntryInfo.setEntryTotal(entry.getDiscountedLinePrice().add(taxValue).toString());
		if (entry.getOrderEntry().getTravelOrderEntryInfo() != null)
		{
			final TravelOrderEntryInfoModel travelOrderEntryInfoModel = entry.getOrderEntry().getTravelOrderEntryInfo();
			travelOrderEntryInfo
					.setTransportOfferings(this.populateTransportOfferingNumbers(travelOrderEntryInfoModel.getTransportOfferings()));
			if (travelOrderEntryInfoModel.getTravelRoute() != null)
			{
				travelOrderEntryInfo.setTravelRoute(travelOrderEntryInfoModel.getTravelRoute().getName());
			}

			if (travelOrderEntryInfoModel.getOriginDestinationRefNumber() != null)
			{
				travelOrderEntryInfo.setOriginDestinationRefNumber(
						entry.getOrderEntry().getTravelOrderEntryInfo().getOriginDestinationRefNumber().toString());
			}

			travelOrderEntryInfo.setTravellers(this.populateTravellerNames(travelOrderEntryInfoModel.getTravellers()));
		}

		return travelOrderEntryInfo;
	}

	@Override
	public TravelOrderEntryInfo estimateOrderEntryInfo(final AbstractOrderEntryModel orderEntry)
	{
		final TravelOrderEntryInfo travelOrderEntryInfo = super.estimateOrderEntryInfo(orderEntry);

		String entryDescription = StringUtils.EMPTY;
		final AbstractOrderEntryModel abstractOrderEntryModel = orderEntry;
		if (OrderEntryType.ACCOMMODATION.equals(abstractOrderEntryModel.getType()))
		{
			final ProductModel roomRateProuctModel = abstractOrderEntryModel.getProduct();
			if (roomRateProuctModel instanceof RoomRateProductModel)
			{
				entryDescription = BCFBackofficeUtil.getRoomRateProductLabel(((RoomRateProductModel) roomRateProuctModel));
			}
			travelOrderEntryInfo.setEntryDesc(entryDescription);
			return travelOrderEntryInfo;
		}
		else
		{
			return travelOrderEntryInfo;
		}
	}
}
