/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.widgets.releasecenter;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import com.bcf.core.services.servicenotice.ServiceNoticesService;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;


public class ReleaseCenterWidgetController extends DefaultWidgetController
{
	public static final String SERVICE_NOTICE_COUNT_KEY = "serviceNoticeCount";
	public static final String NEWS_RELEASE_COUNT_KEY = "newsReleaseCount";
	public static final String BUSINESS_OPPORTUNITY_COUNT_KEY = "businessOpportunityCount";
	public static final String ELECTRONIC_NEWS_LETTER_COUNT_KEY = "electronicNewsLetterCount";
	public static final String SERVICE_NOTICE_TEXT_KEY = "bcfbackoffice.releasecenter.servicenotice.text";
	public static final String NEWS_RELEASE_TEXT_KEY = "bcfbackoffice.releasecenter.newsrelease.text";
	public static final String BUSINESS_OPPORTUNITY_TEXT_KEY = "bcfbackoffice.releasecenter.businessopportunity.text";
	public static final String ELECTRONIC_NEWS_LETTER_TEXT_KEY = "bcfbackoffice.releasecenter.electronicnewsletter.text";

	private Listbox releaseCenterInfoList;

	@WireVariable
	private ServiceNoticesService serviceNoticesService;

	@WireVariable
	private ConfigurationService configurationService;

	public void initialize(Component comp)
	{
		super.initialize(comp);
	}

	private void loadServiceNoticesCount()
	{
		ListModelList<String> listModel = new ListModelList();

		Map<String, Long> servicesAndNewsCountMap = serviceNoticesService.getServiceAndNewsCount();

		listModel.add(getNoticeLabel(SERVICE_NOTICE_TEXT_KEY) + getCount(servicesAndNewsCountMap, SERVICE_NOTICE_COUNT_KEY));
		listModel.add(getNoticeLabel(NEWS_RELEASE_TEXT_KEY) + getCount(servicesAndNewsCountMap, NEWS_RELEASE_COUNT_KEY));
		listModel.add(getNoticeLabel(BUSINESS_OPPORTUNITY_TEXT_KEY) + getCount(servicesAndNewsCountMap, BUSINESS_OPPORTUNITY_COUNT_KEY));
		listModel.add(getNoticeLabel(ELECTRONIC_NEWS_LETTER_TEXT_KEY) + getCount(servicesAndNewsCountMap, ELECTRONIC_NEWS_LETTER_COUNT_KEY));

		this.releaseCenterInfoList.setModel(listModel);
	}

	private String getNoticeLabel(final String labelKey)
	{
		return configurationService.getConfiguration().getString(labelKey);
	}

	private long getCount(final Map<String, Long> servicesAndNewsCountMap, final String countTypeKey)
	{
		Long count = servicesAndNewsCountMap.get(countTypeKey);

		return (count != null && count > 0) ?
				count :
				0;
	}

	@ViewEvent(componentID = "releaseCenterInfoBtn", eventName = "onClick")
	public void onSearchButtonClick(Event event)
	{
		loadServiceNoticesCount();
	}

}
