/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.labels.impl;

import java.util.Objects;
import org.zkoss.zhtml.Text;
import org.zkoss.zul.Listcell;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;


public class BcfVacationParentLocationRenderer implements WidgetComponentRenderer<Listcell, ListColumn, Object>
{
	public BcfVacationParentLocationRenderer()
	{
		//empty constructor
	}

	@Override
	public void render(final Listcell listcell, final ListColumn listColumn, final Object data, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManager)
	{
		if (data instanceof BcfVacationLocationModel)
		{
			BcfVacationLocationModel bcfVacationLocationModel = (BcfVacationLocationModel) data;
			String parentLocations = "";
			if (Objects.nonNull(bcfVacationLocationModel.getParentLocation()))
			{
				parentLocations = bcfVacationLocationModel.getParentLocation().getName();
			}
			Text parentLocationText = new Text();
			parentLocationText.setValue(parentLocations);
			listcell.appendChild(parentLocationText);
		}
	}
}
