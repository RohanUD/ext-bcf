/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class BcfbackofficeConstants extends GeneratedBcfbackofficeConstants
{
	public static final String EXTENSIONNAME = "bcfbackoffice";
	public static final String PRODUCTCATALOG = "bcfProductCatalog";
	public static final String CATALOGVERSION = "Staged";
	public static final String MANDATORY = "mandatory.field";
	public static final String DATE_IN_PAST_ERROR = "date.in.past.error";
	public static final String EXPIRY_DATE_BEFORE_PUBLISH_DATE_ERROR = "expiry.date.before.publish.date.error";
	public static final String PUBLISH_DATE_CHANGE_NOT_ALLOWED_ERROR = "publish.date.change.not.allowed.error";

	public static final String TRAVEL_ADVISORY_PUBLISH_DATE_EMPTY = "travel.advisory.publish.date.empty";
	public static final String TRAVEL_ADVISORY_EXPIRY_DATE_EMPTY = "travel.advisory.expiry.date.empty";
	public static final String TRAVEL_ADVISORY_TITLE_EMPTY = "travel.advisory.title.empty";
	public static final String TRAVEL_ADVISORY_DESCRIPTION_DATE_EMPTY = "travel.advisory.description.empty";

	private BcfbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
