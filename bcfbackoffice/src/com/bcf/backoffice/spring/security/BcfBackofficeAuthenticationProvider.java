/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.backoffice.spring.security;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import com.bcf.integration.activedirectory.service.ADUserService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.oauth.security.ADAuthenticationStrategy;
import com.hybris.backoffice.spring.security.BackofficeAuthenticationProvider;


public class BcfBackofficeAuthenticationProvider extends BackofficeAuthenticationProvider
{
	private ADUserService adUserService;
	private ADAuthenticationStrategy adAuthenticationStrategy;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		if (getAdAuthenticationStrategy().isADUserAuthenticationApplicable(authentication.getName()))
		{
			callADAuthenticationService(authentication);
			return createSuccessAuthentication(authentication, super.retrieveUser(authentication.getName()));
		}
		return super.authenticate(authentication);
	}

	private void callADAuthenticationService(final Authentication authentication)
	{
		try
		{
			getAdUserService().authenticateAndUpdateADUser(authentication.getName(), authentication.getCredentials().toString());
		}
		catch (final IntegrationException e)
		{
			throw new BadCredentialsException("Bad credentials", e);
		}
	}

	protected ADUserService getAdUserService()
	{
		return adUserService;
	}

	@Required
	public void setAdUserService(final ADUserService adUserService)
	{
		this.adUserService = adUserService;
	}

	public ADAuthenticationStrategy getAdAuthenticationStrategy()
	{
		return adAuthenticationStrategy;
	}

	@Required
	public void setAdAuthenticationStrategy(final ADAuthenticationStrategy adAuthenticationStrategy)
	{
		this.adAuthenticationStrategy = adAuthenticationStrategy;
	}
}
