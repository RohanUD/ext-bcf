/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.travelb2bfacades.b2b.populator;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BUnitPopulator;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;


/**
 * Populates {@link B2BUnitModel} to {@link B2BUnitData}.
 */
public class BcfB2BUnitPopulator extends B2BUnitPopulator
{

	@Override
	public void populate(final B2BUnitModel source, final B2BUnitData target)
	{
		super.populate(source,target);
		target.setEmail(source.getEmail());
		target.setPhoneNumber(source.getPhoneNumber());
		target.setPhoneType(source.getPhoneType());
	}
}
