/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.travelb2bfacades.checkout;

import de.hybris.platform.b2b.model.B2BUnitModel;
import java.util.List;
import java.util.Optional;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;
import com.bcf.travelb2bfacades.checkout.users.UserB2BUnitData;


public interface BcfTravelb2bCheckoutFacade
{

   List<UserB2BUnitData> getUserB2BUnitsData();

   Optional<B2BUnitModel> getB2BUnitByCode(String code);

   void setB2BCheckoutContextData(String b2bUnitCode);

   B2BCheckoutContextData getB2BCheckoutContextData();

}
