/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.travelb2bfacades.checkout.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.travelb2bfacades.checkout.BcfTravelb2bCheckoutFacade;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;
import com.bcf.travelb2bfacades.checkout.users.UserB2BUnitData;


public class DefaultBcfTravelb2bCheckoutFacade implements BcfTravelb2bCheckoutFacade
{

   @Resource(name = "userService")
   private UserService userService;

   @Resource(name = "sessionService")
   private SessionService sessionService;

	private static final String IS_B2B_CUSTOMER = "isB2bCustomer";

   @Override
   public List<UserB2BUnitData> getUserB2BUnitsData()
   {
      final List<UserB2BUnitData> userB2BUnits = new ArrayList<>();
      final UserModel user = userService.getCurrentUser();
      if (user instanceof CustomerModel)
      {
         //B2C scenario
         final UserB2BUnitData userWithNoB2BUnit = new UserB2BUnitData();
         userWithNoB2BUnit.setB2bUnitCode("");
         userWithNoB2BUnit.setB2bUnitName("");
         userWithNoB2BUnit.setUserCode(user.getUid());
         userWithNoB2BUnit.setSelected(true);
         userWithNoB2BUnit.setDisplayName(user.getDisplayName());
         userWithNoB2BUnit.setB2bUnitModel(null);
         userB2BUnits.add(userWithNoB2BUnit);


         // B2B scenarios
         final Set<B2BUnitModel> b2bUnits = getB2BUnits();
         for (final B2BUnitModel unit : b2bUnits)
         {
            final UserB2BUnitData userB2BUnit = new UserB2BUnitData();
            userB2BUnit.setB2bUnitCode(unit.getUid());
            userB2BUnit.setB2bUnitName(unit.getDisplayName());
            userB2BUnit.setUserCode(user.getUid());
            userB2BUnit.setDisplayName(user.getDisplayName() + " - " + unit.getDisplayName());
            userB2BUnit.setB2bUnitModel(unit);
            userB2BUnits.add(userB2BUnit);
         }
      }

      setSelected(userB2BUnits);

      return userB2BUnits;
   }

   protected void setSelected(final List<UserB2BUnitData> userB2BUnits)
   {
      final B2BCheckoutContextData b2bCheckoutContextData = getB2BCheckoutContextData();

      if (b2bCheckoutContextData != null && b2bCheckoutContextData.getB2bUnit() != null)
      {
         final Optional<UserB2BUnitData> matchingUserB2BUnit = userB2BUnits.stream()
               .filter(userB2BUnit -> userB2BUnit.getB2bUnitCode().equalsIgnoreCase(b2bCheckoutContextData.getB2bUnit().getUid()))
               .findFirst();

         if (matchingUserB2BUnit.isPresent())
         {
            // Remove the previous selection
            userB2BUnits.stream().forEach(userB2BUnit -> userB2BUnit.setSelected(false));

            matchingUserB2BUnit.get().setSelected(true);
         }
      }
   }

   protected Set<B2BUnitModel> getB2BUnits()
   {
      final UserModel user = userService.getCurrentUser();
      if (user instanceof CustomerModel)
      {
         final Set<PrincipalGroupModel> principalGroups = user.getAllGroups();

         if (CollectionUtils.isNotEmpty(principalGroups))
         {
            return principalGroups.stream()
                  .filter(principal -> principal instanceof B2BUnitModel).map(B2BUnitModel.class::cast)
                  .collect(Collectors.toSet());
         }
      }
      return new HashSet<>();
   }

   @Override
   public Optional<B2BUnitModel> getB2BUnitByCode(final String code)
   {
      return getB2BUnits().stream().filter(unit -> unit.getUid().equalsIgnoreCase(code)).findFirst();
   }

   protected Optional<UserB2BUnitData> getUserB2BUnitByCode(final String code)
   {
      return getUserB2BUnitsData().stream().filter(unit -> unit.getB2bUnitCode().equalsIgnoreCase(code)).findFirst();
   }

   @Override
   public synchronized void setB2BCheckoutContextData(final String b2bUnitCode)
	{
		if (StringUtils.isBlank(b2bUnitCode))
		{
			sessionService.removeAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			sessionService.removeAttribute(IS_B2B_CUSTOMER);
			return;
		}


      final B2BCheckoutContextData b2bCheckoutContextData = getB2BCheckoutContextData();
      final Optional<UserB2BUnitData> userUnit = getUserB2BUnitByCode(b2bUnitCode);
      if (userUnit.isPresent())
      {
         if (b2bCheckoutContextData == null)
         {
            final B2BCheckoutContextData b2bCheckoutContextToSet = new B2BCheckoutContextData();
            b2bCheckoutContextToSet.setB2bUnit(userUnit.get().getB2bUnitModel());
            sessionService.setAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA, b2bCheckoutContextToSet);
         }
         else
         {
            // We will also set the B2C customer selection which may be null.
            b2bCheckoutContextData.setB2bUnit(userUnit.get().getB2bUnitModel());
            sessionService.setAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA, b2bCheckoutContextData);
         }
			sessionService.setAttribute(IS_B2B_CUSTOMER, true);
      }
   }

   @Override
   public synchronized B2BCheckoutContextData getB2BCheckoutContextData()
   {
      return (B2BCheckoutContextData) sessionService.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
   }
}
