<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright (c) 2019 British Columbia Ferry Services Inc
  ~ All rights reserved.
  ~
  ~ This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
  ~ You shall not disclose such Confidential Information and shall use it only in accordance with the
  ~ terms of the license agreement you entered into with British Columbia Ferry Services Inc.
  ~
  ~ Last Modified: 02/07/19 14:01
  -->

<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xmlns:util="http://www.springframework.org/schema/util" xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/util
           http://www.springframework.org/schema/util/spring-util.xsd">

	<util:list id="addBundleToCartValidationStrategyList">
		<ref bean="originDestinationRefNumberValidationStrategy" />
		<ref bean="originDestinationValidationStrategy" />
		<ref bean="departureArrivalTimesValidationStrategy" />
		<ref bean="addAdditionalSailingsValidationStrategy" />
	</util:list>

	<util:map id="bookingActionTypeUrlMap" key-type="java.lang.String">
		<entry key="AMEND_PERSONAL_DETAILS"
			   value="/traveller-details?journeyRefNum={journeyReferenceNumber}&amp;amp;odRefNum={originDestinationRefNumber}&amp;amp;orderCode={orderCode}" />
		<entry key="AMEND_PASSENGER_VEHICLE"
			   value="/manage-booking/{orderCode}/{originDestinationRefNumber}/{journeyReferenceNumber}/amend-travellers" />
		<entry key="REPLAN_JOURNEY" value="/manage-booking/replan-journey" />
		<entry key="CANCEL_SAILING" value="/manage-booking/cancel-sailing" />
		<entry key="AMEND_ANCILLARY"
			   value="/manage-booking/{orderCode}/{originDestinationRefNumber}/{journeyReferenceNumber}/amend-ancillary" />
		<entry key="CANCEL_BOOKING" value="/manage-booking/cancel-booking/{orderCode}" />
		<entry key="APPLY_GOODWILL_REFUND" value="/manage-booking/refund/goodwill-refund-selection/{bookingReference}" />

	</util:map>

	<util:map id="accommodationBookingActionTypeUrlMap" key-type="java.lang.String">
		<entry key="AMEND_ACCOMMODATION" value="/manage-booking/amend-accommodation/{journeyRef}/{bookingReference}" />
		<entry key="AMEND_SAILING" value="/manage-booking/amend-sailing/{journeyRef}/{bookingReference}" />
		<entry key="AMEND_ACTIVITY" value="/manage-booking/amend-activity/{journeyRef}/{bookingReference}" />
		<entry key="ADJUST_COST" value="/manage-booking/adjust-cost/{bookingReference}" />
	</util:map>

	<util:map id="globalBookingActionTypeUrlMap" key-type="java.lang.String">
		<entry key="CANCEL_BOOKING" value="/manage-booking/cancel-booking/{bookingReference}" />
		<entry key="AMEND_ALACARTE_BOOKING" value="/manage-booking/amend-alacarte-booking/{bookingReference}" />
		<entry key="AMEND_STANDALONE_ACTIVITY" value="/manage-booking/amend-activity-booking/{bookingReference}" />
		<entry key="CANCEL_STANDALONE_ACTIVITY" value="/manage-booking/cancel-activity-booking/{bookingReference}" />
		<entry key="APPLY_GOODWILL_REFUND" value="/manage-booking/refund/goodwill-refund-selection/{bookingReference}" />
	</util:map>

	<util:map id="accommodationBookingActionStrategyMap"
			  key-type="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption">
		<entry key="CANCEL_BOOKING" value-ref="accommodationBookingLevelBookingActionStrategy" />
		<entry key="AMEND_ACCOMMODATION" value-ref="accommodationBookingLevelBookingActionStrategy" />
		<entry key="AMEND_SAILING" value-ref="accommodationBookingLevelBookingActionStrategy" />
		<entry key="AMEND_ACTIVITY" value-ref="accommodationBookingLevelBookingActionStrategy" />
		<entry key="ADJUST_COST" value-ref="accommodationBookingLevelBookingActionStrategy" />

	</util:map>

	<util:map id="globalBookingActionStrategyMap" key-type="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption">
		<entry key="CANCEL_BOOKING" value-ref="globalBookingLevelBookingActionStrategy" />
		<entry key="APPLY_GOODWILL_REFUND" value-ref="globalBookingLevelBookingActionStrategy" />
		<entry key="AMEND_ALACARTE_BOOKING" value-ref="globalBookingLevelBookingActionStrategy" />
		<entry key="AMEND_STANDALONE_ACTIVITY" value-ref="globalBookingLevelBookingActionStrategy" />
		<entry key="CANCEL_STANDALONE_ACTIVITY" value-ref="globalBookingLevelBookingActionStrategy" />
	</util:map>

	<util:map id="accommodationBookingActionEnabledCalculationStrategiesMap"
			  key-type="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption">
		<entry key="CANCEL_BOOKING">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="accommodationUserGroupTypeRestriction" />
				<ref bean="roomStayCheckInDateRestrictionStrategy" />
				<ref bean="accommodationUserActionTypeRestrictionStrategy" />
				<ref bean="accommodationPaymentTypeRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
			</list>
		</entry>

		<entry key="APPLY_GOODWILL_REFUND">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageAlaCarteJourneyTypeRestriction" />
			</list>
		</entry>

		<entry key="AMEND_ACCOMMODATION">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="accommodationUserGroupTypeRestriction" />
				<ref bean="roomStayCheckInDateRestrictionStrategy" />
				<ref bean="accommodationPaymentTypeRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageJourneyTypeRestriction" />

			</list>
		</entry>
		<entry key="AMEND_SAILING">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="accommodationUserGroupTypeRestriction" />
				<ref bean="roomStayCheckInDateRestrictionStrategy" />
				<ref bean="accommodationPaymentTypeRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageJourneyTypeRestriction" />
			</list>
		</entry>
		<entry key="AMEND_ACTIVITY">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="accommodationUserGroupTypeRestriction" />
				<ref bean="roomStayCheckInDateRestrictionStrategy" />
				<ref bean="accommodationPaymentTypeRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageJourneyTypeRestriction" />
			</list>
		</entry>
		<entry key="ADJUST_COST">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="accommodationUserGroupTypeRestriction" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageJourneyTypeRestriction" />
			</list>
		</entry>
	</util:map>


	<util:map id="globalBookingActionEnabledCalculationStrategiesMap"
			  key-type="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption">
		<entry key="CANCEL_BOOKING">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
			</list>
		</entry>
		<entry key="APPLY_GOODWILL_REFUND">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfPackageAlaCarteJourneyTypeRestriction" />
			</list>
		</entry>
		<entry key="AMEND_ALACARTE_BOOKING">
			<list>
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfChannelTypeRestrictionStrategy" />
				<ref bean="bcfAlaCarteJourneyTypeRestriction" />
			</list>
		</entry>
		<entry key="AMEND_STANDALONE_ACTIVITY">
			<list>
				<ref bean="bcfStandaloneActivityJourneyTypeRestriction" />
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
			</list>
		</entry>
		<entry key="CANCEL_STANDALONE_ACTIVITY">
			<list>
				<ref bean="bcfStandaloneActivityJourneyTypeRestriction" />
				<ref bean="bcfBookingStatusRestrictionStrategy" />
				<ref bean="bcfUserTypeRestriction" />
				<ref bean="bcfMoreThanOneUniqueActivityRestriction" />
			</list>
		</entry>
	</util:map>


	<util:list id="bookingActions">
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.AMEND_PASSENGER_VEHICLE" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.REPLAN_JOURNEY" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.CANCEL_SAILING" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.CANCEL_BOOKING" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.APPLY_GOODWILL_REFUND" />
	</util:list>

	<util:list id="globalBookingActions">
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.CANCEL_BOOKING" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.APPLY_GOODWILL_REFUND" />

	</util:list>

	<util:list id="longRouteBookingActions">
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.AMEND_PERSONAL_DETAILS" />
		<util:constant static-field="de.hybris.platform.commercefacades.travel.enums.ActionTypeOption.AMEND_ANCILLARY" />
	</util:list>

	<util:list id="bookingFeeCodes" value-type="java.lang.String">
		<value>COMPENSATION</value>
		<value>RPR_ADV_GT7D_UH_OS</value>
		<value>RPR_ADV_LT7D_UH_OS</value>
		<value>RPR_MRLT7D_UH_OS</value>
		<value>RPR_MR_GT7D_UH_OS</value>
		<value>RPR_MR_LT1D_UH_OS</value>
		<value>RPR_STD_UH_OS</value>
		<value>CAF_MR_RP_GT7_UH_OS</value>
		<value>CAF_MR_RP_LT1_UH_OS</value>
		<value>CAF_MR_RP_LT7_UH_OS</value>
		<value>CAF_R10_11_BUS</value>
		<value>CAF_R10_11_CV</value>
		<value>CAF_R10_UH_OS</value>
		<value>CAF_R11_UH_OS</value>
		<value>CAF_R9_ALL</value>
		<value>CAF_SVR_GT3H_UH_OS</value>
		<value>CAF_SVR_LT3H_UH_OS</value>
		<value>RCF_MR_UH_OS</value>
		<value>RCF_R10_11_BUS</value>
		<value>RCF_R10_UH_OS</value>
		<value>RCF_R11_UH_OS</value>
		<value>NSF_R10_11_CV</value>
		<value>NSF_R10_UH_OS</value>
	</util:list>

	<util:list id="addToCartValidationStrategyList">
		<ref bean="bundledProductValidationStrategy" />
	</util:list>

	<util:map id="fareSelectionSortingStrategyMap"
			  key-type="de.hybris.platform.commercefacades.travel.enums.FareSelectionDisplayOrder"
			  value-type="de.hybris.platform.travelfacades.fare.sorting.strategies.AbstractSortingStrategy" scope="prototype">
		<entry key="PRICE_DESC" value-ref="priceSortingStrategyDesc" />
		<entry key="PRICE" value-ref="priceSortingStrategy" />
		<entry key="NUMBER_OF_STOPS" value-ref="numberOfStopsSortingStrategy" />
		<entry key="DEPARTURE_TIME" value-ref="departureTimeSortingStrategy" />
		<entry key="ARRIVAL_TIME" value-ref="arrivalTimeSortingStrategy" />
		<entry key="TRAVEL_TIME" value-ref="travelTimeSortingStrategy" />
	</util:map>


	<util:map id="paymentTypeMethodMap" key-type="java.lang.String">
		<entry key="Cash" value="cash" />
		<entry key="GiftCard" value="giftcard" />
		<entry key="CTCCard" value="ctccard" />
		<entry key="cardPresent" value="card" />
		<entry key="cardNotPresent" value="card" />
		<entry key="savedCard" value="card" />
	</util:map>

	<util:map id="paymentMethodStrategy" key-type="java.lang.String">
		<entry key="cash" value-ref="cashPaymentMethodStrategy" />
		<entry key="card" value-ref="cardPaymentMethodStrategy" />
		<entry key="giftcard" value-ref="giftPaymentMethodStrategy" />
		<entry key="ctccard" value-ref="ctcCardPaymentMethodStrategy" />
	</util:map>

	<util:map id="debitAmountHandlersMap" key-type="java.lang.String">
		<entry key="Web" value-ref="webPaymentHandler" />
		<entry key="CallCenter" value-ref="callCenterPaymentHandler" />
		<entry key="TravelCentre" value-ref="travelCentrePaymentHandler" />
		<entry key="TravelCentreFerryOnly" value-ref="travelCentrePaymentHandler" />
	</util:map>

	<!-- Payment Service Configuration -->

	<util:map id="paymentApplicationIdMap" key-type="java.lang.String">
		<entry key="BOOKING_TRANSPORT_ONLY" value-ref="ferryPaymentClientCodeMap" />
		<entry key="BOOKING_PACKAGE" value-ref="vacationPaymentClientCodeMap" />
		<entry key="BOOKING_ACTIVITY_ONLY" value-ref="vacationPaymentClientCodeMap" />
		<entry key="BOOKING_ACCOMMODATION_ONLY" value-ref="vacationPaymentClientCodeMap" />
		<entry key="BOOKING_TRANSPORT_ACCOMMODATION" value-ref="vacationPaymentClientCodeMap" />
		<entry key="BOOKING_ALACARTE" value-ref="vacationPaymentClientCodeMap" />
	</util:map>

	<util:map id="ferryPaymentClientCodeMap" key-type="java.lang.String">
		<entry key="CallCenter"
			   value="#{configurationService.configuration.getProperty('payment.ferries.callcenter.applicationId')}" />
		<entry key="Web" value="#{configurationService.configuration.getProperty('payment.ferries.customer.applicationId')}" />
		<entry key="TravelCentre"
			   value="#{configurationService.configuration.getProperty('payment.ferries.travelcentre.applicationId')}" />
		<entry key="TravelCentreFerryOnly"
			   value="#{configurationService.configuration.getProperty('payment.ferries.travelcentreferryonly.applicationId')}" />
	</util:map>

	<util:map id="vacationPaymentClientCodeMap" key-type="java.lang.String">
		<entry key="CallCenter"
			   value="#{configurationService.configuration.getProperty('payment.vacation.callcenter.applicationId')}" />
		<entry key="Web" value="#{configurationService.configuration.getProperty('payment.vacation.customer.applicationId')}" />
		<entry key="TravelCentre"
			   value="#{configurationService.configuration.getProperty('payment.vacation.travelcentre.applicationId')}" />
	</util:map>

	<util:map id="activityPaymentClientCodeMap" key-type="java.lang.String">
		<entry key="CallCenter"
			   value="#{configurationService.configuration.getProperty('payment.activity.callcenter.applicationId')}" />
		<entry key="Web" value="#{configurationService.configuration.getProperty('payment.activity.customer.applicationId')}" />
		<entry key="TravelCentre"
			   value="#{configurationService.configuration.getProperty('payment.activity.travelcentre.applicationId')}" />
	</util:map>

	<util:map id="paymentClientCodePasswordMap" key-type="java.lang.String">
		<entry key="MYBCF" value="#{configurationService.configuration.getProperty('mybcf.password')}" />
		<entry key="MYBCF_ASM" value="#{configurationService.configuration.getProperty('mybcf.asm.password')}" />
		<entry key="VACATIONS" value="#{configurationService.configuration.getProperty('vacations.password')}" />
		<entry key="VACATIONS_ASM" value="#{configurationService.configuration.getProperty('vacations.asm.password')}" />
	</util:map>

	<util:map id="OrderTypeDeclareMap" key-type="java.lang.String">
		<entry key="ACCOMMODATION" value="hotel" />
		<entry key="ACTIVITY" value="activity" />
		<entry key="TRANSPORT" value="ferry" />
		<entry key="RESERVERD_SAILING" value="reservedSailing" />
		<entry key="OPEN_TICKET" value="openTicket" />
		<entry key="FEE" value="Fee" />
		<entry key="DEPOSIT" value="deposit" />
		<entry key="GOODWILL" value="goodwill" />
		<entry key="CASH" value="Cash" />
		<entry key="GIFTCARD" value="Gift Card" />
	</util:map>
	<util:map id="salesChannelBookingJourneySummaryUrlsMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="TravelCentre-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="TravelCentre-BOOKING_TRANSPORT_ONLY" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_TRANSPORT_ACCOMMODATION" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ACTIVITY_ONLY" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ACCOMMODATION_ONLY" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ALACARTE" value="/fare-selection-review" />
		<entry key="TravelCentreFerryOnly-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="TravelCentreFerryOnly-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ALACARTE" value="/cart/summary" />
		<entry key="Web-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="Web-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="Web-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="Web-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="Web-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="Web-BOOKING_ALACARTE" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="CallCenter-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ALACARTE" value="/cart/summary" />
	</util:map>


	<util:map id="salesChannelAmendBookingJourneySummaryUrlsMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="TravelCentre-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="TravelCentre-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="TravelCentre-BOOKING_TRANSPORT_ACCOMMODATION" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ACTIVITY_ONLY" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ACCOMMODATION_ONLY" value="/alacarte-review" />
		<entry key="TravelCentre-BOOKING_ALACARTE" value="/alacarte-review" />
		<entry key="TravelCentreFerryOnly-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="TravelCentreFerryOnly-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="TravelCentreFerryOnly-BOOKING_ALACARTE" value="/cart/summary" />
		<entry key="Web-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="Web-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="Web-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="Web-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="Web-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="Web-BOOKING_ALACARTE" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_PACKAGE" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_TRANSPORT_ONLY" value="/fare-selection-review" />
		<entry key="CallCenter-BOOKING_TRANSPORT_ACCOMMODATION" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ACTIVITY_ONLY" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ACCOMMODATION_ONLY" value="/cart/summary" />
		<entry key="CallCenter-BOOKING_ALACARTE" value="/cart/summary" />
	</util:map>

	<util:map id="agentGroupRoleMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="bcfvagentgroup" value="BCFV_AGENT" />
		<entry key="bcfvmanagergroup" value="BCFV_MANAGER" />
		<entry key="ccagentgroup" value="CC_AGENT" />
		<entry key="ccmanagergroup" value="CC_MANAGER" />
		<entry key="financestaffgroup" value="FINANCE_STAFF" />
	</util:map>

</beans>
