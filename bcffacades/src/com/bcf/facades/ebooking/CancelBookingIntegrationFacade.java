/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.integrations.cancelbooking.order.response.CancelBookingResponseForOrder;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CancelBookingIntegrationFacade
{
	List<RemoveSailingResponseData> cancelSailingsInCart(List<CartFilterParamsDto> cartFilterParamsDtos,
			boolean isCancelCompleteBooking) throws IntegrationException;

	List<RemoveSailingResponseData> filterCancelBookingList(List<RemoveSailingResponseData> removeSailingResponseDatas,
			boolean isSuccess);

	/**
	 * Cancel bookings.
	 *
	 * @param order the order
	 * @return the list
	 * @throws IntegrationException
	 */
	List<RemoveSailingResponseData> cancelBookings(OrderModel order) throws IntegrationException;

	CancelBookingResponseForOrder cancelMultipleBookings(String cancelBookingRefs)
			throws IntegrationException;

	/**
	 * Cancel bookings.
	 *
	 * @param order         the order
	 * @param journeyRefNum the journey ref num
	 * @param odRefNum      the od ref num
	 * @return the list
	 * @throws IntegrationException the integration exception
	 */
	List<RemoveSailingResponseData> cancelBookings(OrderModel order, int journeyRefNum, int odRefNum) throws IntegrationException;

	List<RemoveSailingResponseData> cancelSailingsInCart(CartModel cartModel, List<AbstractOrderEntryModel> removeOrderEntries)
			throws IntegrationException;
}
