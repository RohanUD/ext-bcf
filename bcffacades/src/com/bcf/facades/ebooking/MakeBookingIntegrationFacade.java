/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import java.util.List;
import java.util.Map;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public interface MakeBookingIntegrationFacade
{
	/**
	 * makes makesBookingRequest for AddBundleToCartRequestData
	 *
	 * @param requestData object of AddBundleToCartRequestData
	 */
	BCFMakeBookingResponse getMakeBookingResponse(AddBundleToCartRequestData requestData) throws IntegrationException;


	/**
	 * makes makeBookingRequest for each of the sailings in the cart Model and Update the cart with the response
	 *
	 * @param cart object of CartModel
	 */
	void updateCartWithMakeBookingResponse(CartModel cartModel) throws IntegrationException, NumberFormatException;

	/**
	 * updates sailings in the cart with the products.
	 *
	 * @param cartModel                    object of CartModel
	 * @param addProductToCartRequestDatas list of AddProductToCartRequestData
	 */
	BCFMakeBookingResponse getMakeBookingResponse(CartModel cartModel,
			List<AddProductToCartRequestData> addProductToCartRequestDatas)
			throws IntegrationException, NumberFormatException;

	/**
	 * creates MakeBookingRequestData
	 *
	 * @param cartModel
	 * @param sailingEntries
	 * @param productCodeEntryMap
	 * @param cachingKeys
	 * @param addProductToCartRequestDatas
	 * @param addBundleToCartRequestData
	 * @return
	 */
	MakeBookingRequestData createMakeBookingRequestData(CartModel cartModel, List<AbstractOrderEntryModel> sailingEntries,
			Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap, List<String> cachingKeys,
			List<AddProductToCartRequestData> addProductToCartRequestDatas, AddBundleToCartRequestData addBundleToCartRequestData);

	/**
	 * returns BCFMakeBookingResponse using MakeBookingRequestData
	 *
	 * @param requestData
	 * @return
	 * @throws IntegrationException
	 */
	BCFMakeBookingResponse getMakeBookingResponse(MakeBookingRequestData requestData) throws IntegrationException;

	public void updateCartWithMakeBookingResponse(final CartModel cartModel,boolean isBookingRefUpdate) throws IntegrationException;

	public void checkAndUpdateCartWithMakeBookingResponseForQuote(final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries, CartModel cartModel) throws IntegrationException;

}
