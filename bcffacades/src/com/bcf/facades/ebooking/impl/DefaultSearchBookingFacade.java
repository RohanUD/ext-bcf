/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;


import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.order.strategies.update.BcfUpdateOrderFromEBookingStrategy;
import com.bcf.core.service.BcfBookingService;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.populators.BookingDetailsResponseHandler;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integration.ebooking.service.SearchBookingService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.integrations.search.booking.response.Itinerary;


public class DefaultSearchBookingFacade implements SearchBookingFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultSearchBookingFacade.class);

	private BcfBookingFacade bookingFacade;
	private ConfigurationService configurationService;
	private SearchBookingService searchBookingService;
	private UserService userService;
	private BookingDetailsResponseHandler bookingDetailsResponseHandler;
	private static final String STRING_TOKENIZER = "-";
	private BcfBookingService bcfBookingService;
	private BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy;


	@Override
	public ReservationDataList searchUpcomingBookings(final int pageNumber) throws IntegrationException
	{
		final ReservationDataList reservationDataList = getCurrentCustomerBookings(pageNumber, Boolean.TRUE, true);
		if (Objects.nonNull(reservationDataList))
		{
			reservationDataList.setFilterCriteria(BcfFacadesConstants.UPCOMING);//NOSONAR
		}
		return reservationDataList;
	}

	@Override
	public ReservationDataList advanceSearchBookings(final int pageNumber,
			final BookingsAdvanceSearchData bookingsAdvanceSearchData)
			throws IntegrationException
	{
		final CustomerModel currentCustomer = (CustomerModel) this.getUserService().getCurrentUser();
		if (!this.getUserService().isAnonymousUser(currentCustomer))
		{
			final SearchBookingResponseDTO response = getSearchBookingService()
					.advanceSearchBookings(currentCustomer, pageNumber, bookingsAdvanceSearchData);
			if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getSearchResult()))
			{
				return getBookingDetailsResponseHandler().createReservationDataList(response);
			}
		}
		return null;
	}

	@Override
	public ReservationDataList searchPastBookings(final int pageNumber) throws IntegrationException
	{
		final ReservationDataList reservationDataList = getCurrentCustomerBookings(pageNumber, Boolean.FALSE, true);
		if (reservationDataList != null)
		{
			reservationDataList.setFilterCriteria(BcfFacadesConstants.PAST);
		}
		return reservationDataList;
	}

	private ReservationDataList getCurrentCustomerBookings(final int currentPageNumber, final Boolean isUpcomingBooking,
			final boolean fetchAll)
			throws IntegrationException
	{
		final CustomerModel currentCustomer = (CustomerModel) this.getUserService().getCurrentUser();
		if (!this.getUserService().isAnonymousUser(currentCustomer))
		{
			final SearchBookingResponseDTO response = getSearchBookingService()
					.searchBookings(currentCustomer, currentPageNumber, isUpcomingBooking, fetchAll);
			if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getSearchResult()))
			{
				return getBookingDetailsResponseHandler().createReservationDataList(response);
			}
		}
		return null;
	}

	@Override
	public ReservationDataList searchBookings(final String yBookingReference, final String eBookingReferences)
			throws IntegrationException
	{
		if (getConfigurationService().getConfiguration().getBoolean(BcfFacadesConstants.SEARCH_FROM_EBOOKING))
		{
			return getBookingsFromEBooking(createBCFBookingRefList(eBookingReferences));
		}
		else
		{
			return getBookingFacade().getBookingsByBookingReferenceAndAmendingOrder(yBookingReference);
		}
	}

	@Override
	public List<String> createBCFBookingRefList(final String bcfBookingReferences)
	{
		if (StringUtils.isNotBlank(bcfBookingReferences))
		{
			return Arrays.asList(bcfBookingReferences.split(STRING_TOKENIZER));
		}
		return Collections.emptyList();
	}

	@Override
	public ReservationDataList getBookingsFromEBooking(final CustomerModel customerModel, final List<String> bcfBookingReferences)
			throws IntegrationException
	{
		if (Objects.nonNull(customerModel) && CollectionUtils.isNotEmpty(bcfBookingReferences))
		{
			final SearchBookingResponseDTO response = getSearchBookingService()
					.searchBookings(customerModel, bcfBookingReferences,
							1);
			if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getSearchResult()))
			{
				final Itinerary itinerary = response.getSearchResult().stream()
						.filter(itineraryData -> itineraryData.getBooking() != null).findFirst().orElse(null);
				if (itinerary != null)
				{
					bcfUpdateOrderFromEBookingStrategy.updateUserSystemIdentifier(itinerary.getBooking(), customerModel);
				}

				return getBookingDetailsResponseHandler().createReservationDataList(response);
			}
		}
		return null;
	}

	@Override
	public ReservationDataList getBookingsFromEBooking(final List<String> bcfBookingReferences) throws IntegrationException
	{
		final CustomerModel currentCustomer = (CustomerModel) this.getUserService().getCurrentUser();
		return this.getBookingsFromEBooking(currentCustomer, bcfBookingReferences);
	}

	@Override
	public ReservationDataList getVacationBookingsFromEBooking(final List<String> bcfBookingReferences,
			final AbstractOrderModel order) throws IntegrationException
	{
		final CustomerModel currentCustomer = (CustomerModel) this.getUserService().getCurrentUser();

		return this.getVacationBookingsFromEBooking(currentCustomer, bcfBookingReferences,order);
	}

	@Override
	public ReservationDataList getVacationBookingsFromEBooking(final CustomerModel currentCustomer,
			final List<String> bcfBookingReferences, final AbstractOrderModel order) throws IntegrationException
	{
		if (CollectionUtils.isNotEmpty(bcfBookingReferences))
		{
			if (Objects.nonNull(currentCustomer))
			{
				final SearchBookingResponseDTO response = updateOrderFromEbookingResponse(currentCustomer, bcfBookingReferences, order);
				return getBookingDetailsResponseHandler().createReservationDataListForVacations(response);
			}
		}
		return null;
	}

	@Override
	public SearchBookingResponseDTO updateOrderFromEbookingResponse(final CustomerModel currentCustomer,
			final List<String> bcfBookingReferences, final AbstractOrderModel order) throws IntegrationException
	{
		CustomerModel searchCustomerModel = currentCustomer;
		if (Objects.isNull(currentCustomer))
		{
			searchCustomerModel = (CustomerModel) this.getUserService().getCurrentUser();
		}

		final SearchBookingResponseDTO response = getSearchBookingService().searchBookings(searchCustomerModel, bcfBookingReferences,1);

		if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getSearchResult()))
		{
			if(order!=null && order instanceof OrderModel)
			{
				try
				{
					getBcfUpdateOrderFromEBookingStrategy().updateVersionedOrder((OrderModel) order, response);
				}catch(final BcfOrderUpdateException | CalculationException ex){
						LOG.error(ex);
				}

				final Itinerary itinerary = response.getSearchResult().stream()
						.filter(itineraryData -> itineraryData.getBooking() != null).findFirst().orElse(null);
				if (itinerary != null)
				{
					bcfUpdateOrderFromEBookingStrategy.updateUserSystemIdentifier(itinerary.getBooking(), order.getUser());
				}

			}
		}
		return response;
	}

	protected SearchBookingService getSearchBookingService()
	{
		return searchBookingService;
	}

	@Required
	public void setSearchBookingService(final SearchBookingService searchBookingService)
	{
		this.searchBookingService = searchBookingService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected BookingDetailsResponseHandler getBookingDetailsResponseHandler()
	{
		return bookingDetailsResponseHandler;
	}

	@Required
	public void setBookingDetailsResponseHandler(final BookingDetailsResponseHandler bookingDetailsResponseHandler)
	{
		this.bookingDetailsResponseHandler = bookingDetailsResponseHandler;
	}

	protected BcfBookingFacade getBookingFacade()
	{
		return bookingFacade;
	}

	@Required
	public void setBookingFacade(final BcfBookingFacade bookingFacade)
	{
		this.bookingFacade = bookingFacade;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BcfUpdateOrderFromEBookingStrategy getBcfUpdateOrderFromEBookingStrategy()
	{
		return bcfUpdateOrderFromEBookingStrategy;
	}

	public void setBcfUpdateOrderFromEBookingStrategy(
			final BcfUpdateOrderFromEBookingStrategy bcfUpdateOrderFromEBookingStrategy)
	{
		this.bcfUpdateOrderFromEBookingStrategy = bcfUpdateOrderFromEBookingStrategy;
	}
}
