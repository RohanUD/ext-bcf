/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.service.ModifyBookingService;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultModifyBookingIntegrationFacade extends AbstractBookingIntegrationFacade
		implements ModifyBookingIntegrationFacade
{
	private static final Logger LOGGER = Logger.getLogger(DefaultModifyBookingIntegrationFacade.class);

	private ModifyBookingService modifyBookingService;
	private BCFTravelCartService bcfTravelCartService;
	private ModelService modelService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;


	@Override
	public BCFModifyBookingResponse getModifyBookingResponseForConvertBookingToOption(final MakeBookingRequestData requestData,
			final String requestType)
	{
		try
		{
			final BCFModifyBookingResponse bcfModifyBookingResponse = getModifyBookingResponse(requestData,
					requestType);
			setBookingReference(bcfModifyBookingResponse);
			return bcfModifyBookingResponse;
		}
		catch (final Exception ex)
		{
			LOGGER.error("Exception calling modify booking - CONVERT_QUOTATION_TO_OPTION", ex);
			getBcfTravelCartService().removeSessionCart();
			return null;
		}
	}


	protected void setBookingReference(final BCFModifyBookingResponse bcfModifyBookingResponse)
	{
		final AbstractOrderModel order = getBcfTravelCartService().getSessionCart();
		final Map<String, List<AbstractOrderEntryModel>> cartEntriesByCachingKey = StreamUtil.safeStream(order.getEntries())
				.filter(entry -> StringUtils.isNotBlank(entry.getCacheKey()))
				.collect(Collectors.groupingBy(entry -> entry.getCacheKey()));
		final Map<String, List<AmountToPayInfoModel>> amountToPayInfoByCachingKey = StreamUtil
				.safeStream(order.getAmountToPayInfos()).collect(Collectors.groupingBy(entry -> entry.getCacheKey()));
		bcfModifyBookingResponse.getConvertQuotationToOptionResponse().getBookingReference().forEach(bookingReferences -> {
			cartEntriesByCachingKey.get(bookingReferences.getCachingKey()).forEach(cartEntry -> {
				cartEntry.setBookingReference(bookingReferences.getBookingReference());
			});
			amountToPayInfoByCachingKey.get(bookingReferences.getCachingKey()).forEach(payInfo -> {
				payInfo.setBookingReference(bookingReferences.getBookingReference());
			});
			getModelService().saveAll();
		});
	}


	@Override
	public BCFModifyBookingResponse getModifyBookingResponse(final MakeBookingRequestData requestData,
			final String requestType) throws IntegrationException
	{
		if (!StringUtils.equals(requestType, BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_REQUEST))
		{
			requestData.setAddProductToCartRequestDatas(createExistingAddProductToCartRequestData(requestData.getCartModel(),
					requestData.getAddBundleToCartRequestData().getSelectedJourneyRefNumber(),
					requestData.getAddBundleToCartRequestData().getSelectedOdRefNumber()));
		}
		return modifyBookingService.getModifyBookingResponse(requestData, requestType);
	}



	@Override
	public BCFModifyBookingResponse getModifyBookingResponse(final CartModel cartModel,
			final List<AddProductToCartRequestData> addProductToCartRequestDatas) throws IntegrationException
	{
		if (Objects.nonNull(cartModel) && CollectionUtils.isNotEmpty(cartModel.getEntries())
				&& CollectionUtils.isNotEmpty(addProductToCartRequestDatas))
		{
			final List<String> cartCachingKeys = getBcfTravelCartService().getOrderCachingKeys(cartModel);
			final Integer journeyKey = addProductToCartRequestDatas.get(0).getJourneyRefNumber();
			final Integer sailingKey = addProductToCartRequestDatas.get(0).getOriginDestinationRefNumber();
			final List<AbstractOrderEntryModel> sailingEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)
							&& entry.getJourneyReferenceNumber() == journeyKey
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == sailingKey)
					.collect(Collectors.toList());
			final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
			return modifyBookingService.getModifyBookingResponse(createMakeBookingRequestData(cartModel, sailingEntries,
					productCodeEntryMap, cartCachingKeys, addProductToCartRequestDatas),
					BcfCoreConstants.MAKE_BOOKING_REQUEST);
		}

		return null;
	}

	@Override
	public BCFModifyBookingResponse getModifyBookingResponse(final AddBundleToCartRequestData requestData,
			final String requestType) throws IntegrationException
	{
		final List<AddProductToCartRequestData> existingProducts = createExistingAddProductToCartRequestData(
				bcfTravelCartService.getSessionCart(), requestData.getSelectedJourneyRefNumber(),
				requestData.getSelectedOdRefNumber());
		final List<String> accessibilityCodes = BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
		if (CollectionUtils.isNotEmpty(existingProducts))
		{
			existingProducts.removeAll(StreamUtil.safeStream(existingProducts)
					.filter(existingProduct -> accessibilityCodes.contains(existingProduct.getProductCode()))
					.collect(Collectors.toList()));
		}
		requestData.setAddProductToCartRequestDatas(existingProducts);
		return modifyBookingService.getModifyBookingResponse(requestData, requestType);
	}

	@Override
	public void updateCartWithMakeBookingResponse(final CartModel cartModel, final boolean isBookingRefUpdate)
			throws IntegrationException
	{
		if (Objects.nonNull(cartModel) && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)
							&& !RoomRateProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

			final List<String> cartCachingKeys = new ArrayList<>();
			for (final List<AbstractOrderEntryModel> value : sailingEntries.values())
			{
				final Map<Integer, List<AbstractOrderEntryModel>> originDestinationEntriesMap = value.stream()
						.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
				for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> entrySet : originDestinationEntriesMap.entrySet())
				{
					final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
					final MakeBookingRequestData makeBookingRequestData = createMakeBookingRequestData(cartModel,
							originDestinationEntriesMap.get(entrySet.getKey()), productCodeEntryMap, cartCachingKeys, null);
					makeBookingRequestData.setBookingRefUpdate(isBookingRefUpdate);
					final BCFModifyBookingResponse response = modifyBookingService
							.getModifyBookingResponse(makeBookingRequestData, BcfCoreConstants.MAKE_BOOKING_REQUEST);
					getBcfTravelCartService().updateCart(makeBookingRequestData, response.getMakeBookingResponse());
				}
			}
			cartModel.setCurrentJourneyRefNum(Collections.max(sailingEntries.keySet()));
		}
	}

	public ModifyBookingService getModifyBookingService()
	{
		return modifyBookingService;
	}

	public void setModifyBookingService(final ModifyBookingService modifyBookingService)
	{
		this.modifyBookingService = modifyBookingService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return super.getBcfTravelCommerceStockService();
	}

	@Override
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		super.setBcfTravelCommerceStockService(bcfTravelCommerceStockService);
	}
}
