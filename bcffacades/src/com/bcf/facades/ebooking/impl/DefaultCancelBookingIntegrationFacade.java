/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;

import static com.bcf.facades.constants.BcfFacadesConstants.FAILURE;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.cancelbooking.common.BookingReference;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.cancelbooking.order.response.CancelBookingResponseForOrder;
import com.bcf.integrations.cancelbooking.response.CancellationResult;
import com.bcf.integrations.cancelbooking.response.Error;
import com.bcf.integrations.common.BookingClientSystem;
import com.bcf.integrations.common.BookingReferences;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCancelBookingIntegrationFacade implements CancelBookingIntegrationFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultCancelBookingIntegrationFacade.class);
	private static final String CANCELBOOKING_SUCCESS_RESULT = "SUCCESS";
	private static final String NON_TRANSPORT_BOOKING_ERROR = "Booking is not Transport Only";
	private CancelBookingService cancelBookingService;
	private BCFTravelCartService bcfTravelCartService;
	private Converter<TravelRouteModel, TravelRouteData> travelRouteConverter;
	private UserService userService;
	private IntegrationUtility integrationUtility;
	private BaseRestService eBookingRestService;
	private BcfBookingFacade bcfTravelBookingFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private BusinessProcessService businessProcessService;
	private ModelService modelService;

	@Override
	public List<RemoveSailingResponseData> cancelSailingsInCart(final List<CartFilterParamsDto> cartFilterParamsDtos,
			final boolean isCancelCompleteBooking) throws IntegrationException
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();

		final List<AbstractOrderEntryModel> removeOrderEntries = filterEntries(cartModel, isCancelCompleteBooking,
				cartFilterParamsDtos);
		final List<RemoveSailingResponseData> removeSailingResponseDatas = cancelSailingsInCart(cartModel, removeOrderEntries);
		getBcfTravelCartService().calculateCart(cartModel);
		return removeSailingResponseDatas;
	}

	@Override
	public List<RemoveSailingResponseData> cancelSailingsInCart(final CartModel cartModel,
			final List<AbstractOrderEntryModel> removeOrderEntries)
			throws IntegrationException
	{
		List<RemoveSailingResponseData> removeSailingResponseDatas = null;
		final CancelBookingResponseForCart cancelBookingResponse = getCancelBookingService().cancelBooking(cartModel,
				removeOrderEntries);
		removeSailingResponseDatas = populateResponse(removeOrderEntries, cancelBookingResponse);
		return removeSailingResponseDatas;
	}

	@Override
	public List<RemoveSailingResponseData> filterCancelBookingList(
			final List<RemoveSailingResponseData> removeSailingResponseDatas, final boolean isSuccess)
	{
		return CollectionUtils.isEmpty(removeSailingResponseDatas) ? Collections.emptyList()
				: removeSailingResponseDatas.stream()
				.filter(removeSailingResponseData -> removeSailingResponseData.isSuccess() == isSuccess)
				.collect(Collectors.toList());
	}



	/**
	 * @return the bcfTravelCartService
	 */
	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	protected List<AbstractOrderEntryModel> filterEntries(final AbstractOrderModel order, final boolean emptyCart,
			final List<CartFilterParamsDto> cartFilterParamsDtos)
	{
		return order.getEntries().stream().filter(entry -> entry.getActive()
				&& entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
				&& ((emptyCart && StringUtils.isNotBlank(entry.getBookingReference())) || (
				CollectionUtils.isNotEmpty(cartFilterParamsDtos)
						&& cartFilterParamsDtos.stream()
						.anyMatch(cartFilterParamsDto -> cartFilterParamsDto.getJourneyRefNo() == entry.getJourneyReferenceNumber()
								&& cartFilterParamsDto.getOdRefNo() == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))))
				.collect(Collectors.toList());
	}

	/**
	 * @return the cancelBookingService
	 */
	protected CancelBookingService getCancelBookingService()
	{
		return cancelBookingService;
	}

	protected List<RemoveSailingResponseData> populateResponse(final List<AbstractOrderEntryModel> removeEntries,
			final CancelBookingResponseForCart cancelBookingResponse)
	{
		final List<RemoveSailingResponseData> removeSailingResponseDatas = new ArrayList<>();
		cancelBookingResponse.getResult().stream()
				.forEach(result -> removeEntries.stream()
						.filter(entry -> StringUtils.equals(entry.getCacheKey(), result.getBookingReference().getCachingKey())
								|| (StringUtils.isNotBlank(entry.getBookingReference()) && entry.getBookingReference()
								.equals(result.getBookingReference().getBookingReference()))
								&& (CollectionUtils.isEmpty(removeSailingResponseDatas) || removeSailingResponseDatas.stream()
								.noneMatch(removeSailingResponseData -> removeSailingResponseData.getJourneyRefNumber() == entry
										.getJourneyReferenceNumber()
										&& removeSailingResponseData.getOriginDestinationRefNumber() == entry
										.getTravelOrderEntryInfo().getOriginDestinationRefNumber())))
						.forEach(entry -> removeSailingResponseDatas.add(createRemoveSailingResponseData(entry, result))));
		return removeSailingResponseDatas;
	}

	@Override
	public List<RemoveSailingResponseData> cancelBookings(final OrderModel order) throws IntegrationException
	{
		final CancelBookingResponseForOrder cancelBookingResponse = getCancelBookingService().cancelBookings(order);
		return populateResponse(order, cancelBookingResponse);
	}

	@Override
	public CancelBookingResponseForOrder cancelMultipleBookings(final String cancelBookingRefs)
			throws IntegrationException
	{
		final List<String> bookingRefs = new ArrayList<>(Arrays.asList(cancelBookingRefs.split(",")));
		final List<String> nonTransportBookings = new ArrayList<>();
		CancelBookingResponseForOrder cancelBookingResponse = new CancelBookingResponseForOrder();
		filterTransportBookings(bookingRefs, nonTransportBookings);
		if (!CollectionUtils.isNotEmpty(bookingRefs))
		{
			updateNonTransportBookingsError(nonTransportBookings, cancelBookingResponse);
			return cancelBookingResponse;
		}

		final CancelBookingRequestForOrder cancelBookingsRequest = createCancelRequest(bookingRefs);
		cancelBookingResponse = (CancelBookingResponseForOrder) geteBookingRestService()
				.sendRestRequest(cancelBookingsRequest,
						CancelBookingResponseForOrder.class, BcfintegrationserviceConstants.EBOOKING_CANCEL_BOOKING_SERVICE_URL,
						HttpMethod.POST);
		updateCancellationResponse(cancelBookingResponse, nonTransportBookings);
		startCancelOrderEmailProcess(cancelBookingRefs);
		return cancelBookingResponse;
	}

	protected void startCancelOrderEmailProcess(final String bookingReferences)
	{
		final OrderProcessModel orderProcessModel = getBusinessProcessService()
				.createProcess("cancel-order-process-" + bookingReferences, "cancel-order-process");
		orderProcessModel.setUser(getUserService().getCurrentUser());
		orderProcessModel.setBookingReferences(bookingReferences);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	@Override
	public List<RemoveSailingResponseData> cancelBookings(final OrderModel order, final int journeyRefNum, final int odRefNum)
			throws IntegrationException
	{
		final List<AbstractOrderEntryModel> removeOrderEntries = filterOrderEntries(order, journeyRefNum, odRefNum);
		final CancelBookingResponseForOrder cancelBookingResponse = getCancelBookingService().cancelBookings(order,
				removeOrderEntries);
		return populateResponse(order, cancelBookingResponse);
	}

	/**
	 * Filter order entries.
	 *
	 * @param order         the order
	 * @param journeyRefNum the journey ref num
	 * @param odRefNum      the od ref num
	 * @return the list
	 */
	protected List<AbstractOrderEntryModel> filterOrderEntries(final OrderModel order, final int journeyRefNum, final int odRefNum)
	{
		return order.getEntries().stream()
				.filter(entry -> entry.getActive() && Objects.equals(OrderEntryType.TRANSPORT, entry.getType())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo()) && entry.getJourneyReferenceNumber() == journeyRefNum
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNum)
				.collect(Collectors.toList());
	}

	/**
	 * Populate response.
	 *
	 * @param orderModel            the order model
	 * @param cancelBookingResponse the cancel booking response
	 * @return the list
	 */
	protected List<RemoveSailingResponseData> populateResponse(final AbstractOrderModel orderModel,
			final CancelBookingResponseForOrder cancelBookingResponse)
	{
		final List<RemoveSailingResponseData> removeSailingResponseDatas = new ArrayList<>();
		cancelBookingResponse.getResult().forEach(result -> {
			final AbstractOrderEntryModel entry = orderModel.getEntries().stream()
					.filter(orderEntry -> Objects.equals(orderEntry.getType(), OrderEntryType.TRANSPORT))
					.findAny().orElse(null);
			if (Objects.nonNull(entry))
			{
				removeSailingResponseDatas.add(createRemoveSailingResponseData(entry, result));
			}
		});
		return removeSailingResponseDatas;
	}

	protected RemoveSailingResponseData createRemoveSailingResponseData(final AbstractOrderEntryModel entry,
			final CancellationResult result)
	{
		final RemoveSailingResponseData removeSailingResponseData = new RemoveSailingResponseData();
		removeSailingResponseData
				.setTravelRoute(getTravelRouteConverter().convert(entry.getTravelOrderEntryInfo().getTravelRoute()));
		removeSailingResponseData.setJourneyRefNumber(entry.getJourneyReferenceNumber());
		removeSailingResponseData.setOriginDestinationRefNumber(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber());
		removeSailingResponseData.setSuccess(StringUtils.equals(CANCELBOOKING_SUCCESS_RESULT, result.getResult()));
		removeSailingResponseData.setBookingReference(entry.getBookingReference());
		if (Objects.nonNull(result.getError()))
		{
			LOG.debug("Booking with cachingKey (" + entry.getCacheKey() + ") was not cancelled by Ebooking. Error code :"
					+ result.getError().getErrorCode() + ". Error Summary :" + result.getError().getErrorSummary());
			removeSailingResponseData.setErrorCode(result.getError().getErrorCode());
			if (Objects.nonNull(result.getError().getErrorSummary()))
			{
				removeSailingResponseData.setErrorSummary(result.getError().getErrorSummary());
			}
		}
		return removeSailingResponseData;
	}


	private void updateCancellationResponse(final CancelBookingResponseForOrder cancelBookingResponse,
			final List<String> nonTransportBookings)
	{
		cancelBookingResponse.getResult().stream().forEach(cancellationResult -> {
			if (Objects.isNull(cancellationResult.getError()))
			{
				final List<OrderEntryModel> orderEntryModels = bcfTravelBookingFacade
						.findOrderEntriesByEBookingCode(cancellationResult.getBookingReference().getBookingReference());
				cancelBookingService.updateCancelOrder(orderEntryModels);
			}
		});
		if (CollectionUtils.isNotEmpty(nonTransportBookings))
		{
			updateNonTransportBookingsError(nonTransportBookings, cancelBookingResponse);

		}
	}

	private void updateNonTransportBookingsError(final List<String> nonTransportBookings,
			final CancelBookingResponseForOrder cancelBookingResponse)
	{
		final List<CancellationResult> cancellationResults = new ArrayList<>();
		final CancellationResult result = new CancellationResult();
		final Error error = new Error();
		result.setError(error);
		result.getError().setErrorSummary(NON_TRANSPORT_BOOKING_ERROR);
		result.setResult(FAILURE);
		final BookingReference bookingReference = new BookingReference();
		result.setBookingReference(bookingReference);
		result.getBookingReference().setBookingReference(String.join(",", nonTransportBookings));
		cancellationResults.add(result);
		cancelBookingResponse.setResult(cancellationResults);
		cancelBookingResponse.getResult().add(result);
	}

	private CancelBookingRequestForOrder createCancelRequest(final List<String> bookingRefs)
	{
		final CancelBookingRequestForOrder cancelBookingsRequest = new CancelBookingRequestForOrder();
		final BookingClientSystem bookingClientSystem = new BookingClientSystem();
		bookingClientSystem
				.setClientCode(getIntegrationUtility().getClientCode(getBcfSalesApplicationResolverService().getCurrentSalesChannel(),
						BookingJourneyType.BOOKING_TRANSPORT_ONLY));

		cancelBookingsRequest.setBookingClientSystem(bookingClientSystem);

		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();

		cancelBookingsRequest.setCustomerInfo(getIntegrationUtility().createCustomerInfo(customerModel));
		cancelBookingsRequest
				.setNotes(BcfintegrationserviceConstants.NOTE_DELIMITER + customerModel.getName() + BcfintegrationserviceConstants.
						NOTE_DELIMITER + BcfintegrationserviceConstants.CANCELLING_BOOKING_NOTE);
		final List<BookingReferences> bookingReferences = new ArrayList<>();


		bookingRefs.stream().forEach(bookingRef -> {
			final BookingReferences bookingReference = new BookingReferences();
			bookingReference.setBookingReference(bookingRef);
			bookingReferences.add(bookingReference);
		});
		cancelBookingsRequest.setBookingReferences(bookingReferences);
		return cancelBookingsRequest;
	}

	private void filterTransportBookings(final List<String> bookingRefs, final List<String> nonTransportBookings)
	{
		bookingRefs.stream().forEach(bookingRef -> {
			final List<OrderEntryModel> orderEntryModels = bcfTravelBookingFacade
					.findOrderEntriesByEBookingCode(bookingRef);
			if (Objects.nonNull(orderEntryModels) && !(orderEntryModels.get(0).getOrder().getBookingJourneyType().getCode()
					== BcfFacadesConstants.BOOKING_TRANSPORT_ONLY))
			{
				nonTransportBookings.add(bookingRef);
			}
		});
		bookingRefs.removeAll(nonTransportBookings);
	}


	/**
	 * @return the travelRouteConverter
	 */
	protected Converter<TravelRouteModel, TravelRouteData> getTravelRouteConverter()
	{
		return travelRouteConverter;
	}

	/**
	 * @param travelRouteConverter the travelRouteConverter to set
	 */
	@Required
	public void setTravelRouteConverter(final Converter<TravelRouteModel, TravelRouteData> travelRouteConverter)
	{
		this.travelRouteConverter = travelRouteConverter;
	}

	/**
	 * @param cancelBookingService the cancelBookingService to set
	 */
	@Required
	public void setCancelBookingService(final CancelBookingService cancelBookingService)
	{
		this.cancelBookingService = cancelBookingService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public IntegrationUtility getIntegrationUtility()
	{
		return integrationUtility;
	}

	@Required
	public void setIntegrationUtility(final IntegrationUtility integrationUtility)
	{
		this.integrationUtility = integrationUtility;
	}

	public BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

	public BcfBookingFacade getBcfTravelBookingFacade()
	{
		return bcfTravelBookingFacade;
	}

	@Required
	public void setBcfTravelBookingFacade(final BcfBookingFacade bcfTravelBookingFacade)
	{
		this.bcfTravelBookingFacade = bcfTravelBookingFacade;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService() {
		return this.modelService;
	}

	@Required
	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}


}
