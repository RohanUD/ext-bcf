/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.service.MakeBookingService;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultMakeBookingIntegrationFacade extends AbstractBookingIntegrationFacade implements MakeBookingIntegrationFacade
{
	private MakeBookingService makeBookingService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public BCFMakeBookingResponse getMakeBookingResponse(final AddBundleToCartRequestData requestData) throws IntegrationException
	{
		final List<AddProductToCartRequestData> existingProducts = createExistingAddProductToCartRequestData(
				bcfTravelCartService.getSessionCart(), requestData.getSelectedJourneyRefNumber(),
				requestData.getSelectedOdRefNumber());
		final List<String> accessibilityCodes = BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
		if (CollectionUtils.isNotEmpty(existingProducts))
		{
			existingProducts.removeAll(StreamUtil.safeStream(existingProducts)
					.filter(existingProduct -> accessibilityCodes.contains(existingProduct.getProductCode()))
					.collect(Collectors.toList()));
		}
		requestData.setAddProductToCartRequestDatas(existingProducts);
		return getMakeBookingService().getMakeBookingResponse(requestData);
	}

	@Override
	public BCFMakeBookingResponse getMakeBookingResponse(final MakeBookingRequestData requestData) throws IntegrationException
	{
		requestData.setAddProductToCartRequestDatas(createExistingAddProductToCartRequestData(requestData.getCartModel(),
				requestData.getAddBundleToCartRequestData().getSelectedJourneyRefNumber(),
				requestData.getAddBundleToCartRequestData().getSelectedOdRefNumber()));

		return getMakeBookingService().getMakeBookingResponse(requestData);
	}

	@Override
	public void updateCartWithMakeBookingResponse(final CartModel cartModel) throws IntegrationException
	{
		if (Objects.nonNull(cartModel) && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)
							&& Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& !RoomRateProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

			final List<String> cartCachingKeys = new ArrayList<>();
			for (final List<AbstractOrderEntryModel> value : sailingEntries.values())
			{
				final Map<Integer, List<AbstractOrderEntryModel>> originDestinationEntriesMap = value.stream()
						.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
				for (final Entry<Integer, List<AbstractOrderEntryModel>> entrySet : originDestinationEntriesMap.entrySet())
				{
					final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
					final MakeBookingRequestData makeBookingRequestData = createMakeBookingRequestData(cartModel,
							originDestinationEntriesMap.get(entrySet.getKey()), productCodeEntryMap, cartCachingKeys, null);
					final BCFMakeBookingResponse response = getMakeBookingService().getMakeBookingResponse(makeBookingRequestData);
					getBcfTravelCartService().updateCart(makeBookingRequestData, response);

					if (OptionBookingUtil.isOptionBooking(cartModel))
					{
						getBcfTravelCartFacade().setAmountToPay(response, cartModel);
					}
				}
			}
			cartModel.setCurrentJourneyRefNum(Collections.max(sailingEntries.keySet()));
		}
	}


	@Override
	public void checkAndUpdateCartWithMakeBookingResponseForQuote(final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries,
			final CartModel cartModel) throws IntegrationException
	{
			final List<String> cartCachingKeys = new ArrayList<>();
			for (final List<AbstractOrderEntryModel> value : sailingEntries.values())
			{
				final Map<Integer, List<AbstractOrderEntryModel>> originDestinationEntriesMap = value.stream()
						.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
				for (final Entry<Integer, List<AbstractOrderEntryModel>> entrySet : originDestinationEntriesMap.entrySet())
				{
					final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
					final MakeBookingRequestData makeBookingRequestData = createMakeBookingRequestData(cartModel,
							originDestinationEntriesMap.get(entrySet.getKey()), productCodeEntryMap, cartCachingKeys, null);
					final BCFMakeBookingResponse response = getMakeBookingService().getMakeBookingResponse(makeBookingRequestData);
					if(response==null || response.getBookingType()==null || response.getItinerary()==null){
						throw new IntegrationException("invalid makebooking response");
					}
					getBcfTravelCartService().updateCart(makeBookingRequestData, response);

					bcfTravelCartFacade.setAmountToPay(response, cartModel);
					bcfTravelCartFacade.setTermsAndConditionsCode(cartModel, response);

				}
			}

	}



	@Override
	public void updateCartWithMakeBookingResponse(final CartModel cartModel, final boolean isBookingRefUpdate)
			throws IntegrationException
	{
		if (Objects.nonNull(cartModel) && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)
							&& !RoomRateProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

			final List<String> cartCachingKeys = new ArrayList<>();
			for (final List<AbstractOrderEntryModel> value : sailingEntries.values())
			{
				final Map<Integer, List<AbstractOrderEntryModel>> originDestinationEntriesMap = value.stream()
						.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
				for (final Entry<Integer, List<AbstractOrderEntryModel>> entrySet : originDestinationEntriesMap.entrySet())
				{
					final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
					final MakeBookingRequestData makeBookingRequestData = createMakeBookingRequestData(cartModel,
							originDestinationEntriesMap.get(entrySet.getKey()), productCodeEntryMap, cartCachingKeys, null);
					makeBookingRequestData.setBookingRefUpdate(isBookingRefUpdate);
					final BCFMakeBookingResponse response = getMakeBookingService().getMakeBookingResponse(makeBookingRequestData);
					getBcfTravelCartService().updateCart(makeBookingRequestData, response);
				}
			}
			cartModel.setCurrentJourneyRefNum(Collections.max(sailingEntries.keySet()));
		}
	}

	@Override
	public BCFMakeBookingResponse getMakeBookingResponse(final CartModel cartModel,
			final List<AddProductToCartRequestData> addProductToCartRequestDatas) throws IntegrationException
	{

		if (Objects.nonNull(cartModel) && CollectionUtils.isNotEmpty(cartModel.getEntries())
				&& CollectionUtils.isNotEmpty(addProductToCartRequestDatas))
		{
			final List<String> cartCachingKeys = getBcfTravelCartService().getOrderCachingKeys(cartModel);
			final Integer journeyKey = addProductToCartRequestDatas.get(0).getJourneyRefNumber();
			final Integer sailingKey = addProductToCartRequestDatas.get(0).getOriginDestinationRefNumber();
			final List<AbstractOrderEntryModel> sailingEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)
							&& entry.getJourneyReferenceNumber() == journeyKey
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == sailingKey)
					.collect(Collectors.toList());
			final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap = new HashMap<>();
			return getMakeBookingService().getMakeBookingResponse(createMakeBookingRequestData(cartModel, sailingEntries,
					productCodeEntryMap, cartCachingKeys, addProductToCartRequestDatas));
		}

		return null;
	}

	@Override
	public MakeBookingRequestData createMakeBookingRequestData(final CartModel cartModel,
			final List<AbstractOrderEntryModel> sailingEntries, final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap,
			final List<String> cachingKeys, final List<AddProductToCartRequestData> addProductToCartRequestDatas,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final MakeBookingRequestData makeBookingRequestData = createMakeBookingRequestData(cartModel, sailingEntries,
				productCodeEntryMap, cachingKeys, addProductToCartRequestDatas);
		makeBookingRequestData.setAddBundleToCartRequestData(addBundleToCartRequestData);
		makeBookingRequestData.setOpenTicket(addBundleToCartRequestData.isOpenTicket());
		return makeBookingRequestData;
	}

	/**
	 * @return the makeBookingService
	 */
	protected MakeBookingService getMakeBookingService()
	{
		return makeBookingService;
	}

	/**
	 * @param makeBookingService the makeBookingService to set
	 */
	@Required
	public void setMakeBookingService(final MakeBookingService makeBookingService)
	{
		this.makeBookingService = makeBookingService;
	}

	/**
	 * @return the bcfTravelCartService
	 */
	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return super.getBcfTravelCommerceStockService();
	}

	@Override
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		super.setBcfTravelCommerceStockService(bcfTravelCommerceStockService);
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}
}
