/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.ebooking.ConfirmBookingIntegrationFacade;
import com.bcf.facades.ebooking.ConfirmBookingServiceSuccessData;
import com.bcf.integration.ebooking.service.BCFConfirmBookingIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultConfirmBookingIntegrationFacade implements ConfirmBookingIntegrationFacade
{

	private BCFConfirmBookingIntegrationService bcfConfirmBookingIntegrationService;

	@Override
	public ConfirmBookingServiceSuccessData makeConfirmBookingRequest(final AbstractOrderModel abstractOrderModel) throws IntegrationException
	{
		return getBcfConfirmBookingIntegrationService().makeConfirmBookingRequest(abstractOrderModel);
	}

	protected BCFConfirmBookingIntegrationService getBcfConfirmBookingIntegrationService()
	{
		return bcfConfirmBookingIntegrationService;
	}

	@Required
	public void setBcfConfirmBookingIntegrationService(
			final BCFConfirmBookingIntegrationService bcfConfirmBookingIntegrationService)
	{
		this.bcfConfirmBookingIntegrationService = bcfConfirmBookingIntegrationService;
	}

}
