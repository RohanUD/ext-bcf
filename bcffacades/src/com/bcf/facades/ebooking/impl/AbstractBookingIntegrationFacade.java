/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.integration.data.MakeBookingRequestData;


public abstract class AbstractBookingIntegrationFacade
{

	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	protected MakeBookingRequestData createMakeBookingRequestData(final CartModel cartModel,
			final List<AbstractOrderEntryModel> sailingEntries, final Map<String, List<AbstractOrderEntryModel>> productCodeEntryMap,
			final List<String> cachingKeys, final List<AddProductToCartRequestData> addProductToCartRequestDatas)
	{
		final MakeBookingRequestData makeBookingRequestData = new MakeBookingRequestData();
		makeBookingRequestData.setCartModel(cartModel);
		makeBookingRequestData.setProductCodeEntryMap(productCodeEntryMap);
		makeBookingRequestData.setSailingEntries(sailingEntries);
		makeBookingRequestData.setCachingKeys(cachingKeys);
		makeBookingRequestData.setAddProductToCartRequestDatas(addProductToCartRequestDatas);
		makeBookingRequestData.setHoldTimeInSeconds(getHoldTimeInSeconds(cartModel));
		if (CollectionUtils.isNotEmpty(sailingEntries))
		{
			makeBookingRequestData.setOpenTicket(sailingEntries.get(0).getTravelOrderEntryInfo().isOpenTicket());
		}
		return makeBookingRequestData;

	}

	private int getHoldTimeInSeconds(final CartModel cartModel)
	{
		if(cartModel.isVacationOptionBooking())
		{
			return OptionBookingUtil.getHoldTimeInSeconds(cartModel);
		}
		return bcfTravelCommerceStockService.getStockHoldTimeInMs(cartModel.getSalesApplication())/1000;

	}

	protected AddProductToCartRequestData  createAddProductToCartRequestData(
			final Integer journeyRefNumber, final Integer originDestinationRefNumber, final long qty, final String productCode)
	{

		final AddProductToCartRequestData addProductToCartRequestData = new AddProductToCartRequestData();
		addProductToCartRequestData.setOriginDestinationRefNumber(originDestinationRefNumber);
		addProductToCartRequestData.setProductCode(productCode);
		addProductToCartRequestData.setQty(qty);
		addProductToCartRequestData.setJourneyRefNumber(journeyRefNumber);
		addProductToCartRequestData.setProductType(AncillaryProductModel._TYPECODE);
		return addProductToCartRequestData;

	}



	protected List<AddProductToCartRequestData>  createExistingAddProductToCartRequestData(final CartModel cartModel,
			final Integer journeyRefNumber, final Integer originDestinationRefNumber)
	{
		List<AddProductToCartRequestData> addProductToCartRequestDatas=null;
		final List<AbstractOrderEntryModel> ancillaryEntries = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE)
						&& entry.getJourneyReferenceNumber() == journeyRefNumber
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationRefNumber)
				.collect(Collectors.toList());

		if(CollectionUtils.isNotEmpty(ancillaryEntries)){

			addProductToCartRequestDatas=new ArrayList<>();

			for (final AbstractOrderEntryModel ancillaryEntry : ancillaryEntries)
			{
				addProductToCartRequestDatas.add(createAddProductToCartRequestData(journeyRefNumber,originDestinationRefNumber,ancillaryEntry.getQuantity(),ancillaryEntry.getProduct().getCode()));
			}

		}

		return addProductToCartRequestDatas;
	}

	protected List<AddProductToCartRequestData> updateAddProductToCartRequestDataQtyWithNewRequest(
			final AddProductToCartRequestData newElement,
			final List<AddProductToCartRequestData> existingElements)
	{
		List<AddProductToCartRequestData> addProductToCartRequestList = null;
		if (CollectionUtils.isNotEmpty(existingElements))
		{
			checkExistingProduct(newElement, existingElements);
			addProductToCartRequestList = existingElements;
		}
		else if (Objects.nonNull(newElement))
		{
			addProductToCartRequestList = Collections.singletonList(newElement);
		}
		return addProductToCartRequestList;
	}

	private void checkExistingProduct(final AddProductToCartRequestData newElement,
			final List<AddProductToCartRequestData> existingElements)
	{
		if (Objects.nonNull(newElement))
		{
			final ListIterator<AddProductToCartRequestData> iterator = existingElements.listIterator();
			while (iterator.hasNext())
			{
				final AddProductToCartRequestData existingElement = iterator.next();
				if (existingElement.getProductCode().equals(newElement.getProductCode()))
				{
					if (newElement.getQty() == 0)
					{
						iterator.remove();
						continue;
					}
					existingElement.setQty(newElement.getQty());
				}
				else
				{
					iterator.add(newElement);
				}
			}
		}
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
