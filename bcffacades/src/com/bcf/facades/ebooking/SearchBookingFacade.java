/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.List;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface SearchBookingFacade
{
	ReservationDataList searchUpcomingBookings(int pageNumber) throws IntegrationException;

	ReservationDataList searchPastBookings(int pageNumber) throws IntegrationException;

	ReservationDataList getBookingsFromEBooking(CustomerModel customerModel, List<String> bcfBookingReferences)
			throws IntegrationException;

	ReservationDataList getBookingsFromEBooking(List<String> bcfBookingReferences) throws IntegrationException;

	ReservationDataList searchBookings(final String yBookingReference, final String bcfBookingReferences)
			throws IntegrationException;

	List<String> createBCFBookingRefList(String bcfBookingReferences);

	ReservationDataList getVacationBookingsFromEBooking(final List<String> bcfBookingReferences,AbstractOrderModel order) throws IntegrationException;

	ReservationDataList getVacationBookingsFromEBooking(CustomerModel currentCustomer, List<String> bcfBookingReferences,AbstractOrderModel order)
			throws IntegrationException;

	ReservationDataList advanceSearchBookings(int pageNumber, BookingsAdvanceSearchData bookingsAdvanceSearchData)
			throws IntegrationException;

	SearchBookingResponseDTO updateOrderFromEbookingResponse(final CustomerModel currentCustomer,
			final List<String> bcfBookingReferences, final AbstractOrderModel order) throws IntegrationException;
}
