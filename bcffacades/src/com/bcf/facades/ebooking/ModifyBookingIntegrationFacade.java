/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ebooking;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.CartModel;
import java.util.List;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public interface ModifyBookingIntegrationFacade
{
	BCFModifyBookingResponse getModifyBookingResponse(MakeBookingRequestData requestData, String requestType)
			throws IntegrationException;

	BCFModifyBookingResponse getModifyBookingResponse(CartModel cartModel,
			List<AddProductToCartRequestData> addProductToCartRequestDatas) throws IntegrationException, NumberFormatException;

	BCFModifyBookingResponse getModifyBookingResponse(AddBundleToCartRequestData requestData, String requestType)
			throws IntegrationException;

	BCFModifyBookingResponse getModifyBookingResponseForConvertBookingToOption(final MakeBookingRequestData requestData,
			final String requestType);

	void updateCartWithMakeBookingResponse(final CartModel cartModel, final boolean isBookingRefUpdate)
			throws IntegrationException;
}
