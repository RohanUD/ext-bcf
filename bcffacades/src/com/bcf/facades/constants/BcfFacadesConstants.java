/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.constants;

import java.util.regex.Pattern;


/**
 * Global class for all BcfFacades constants.
 */
@SuppressWarnings("PMD")
public class BcfFacadesConstants extends GeneratedBcfFacadesConstants
{
	public static final String EXTENSIONNAME = "bcffacades";

	private BcfFacadesConstants()
	{
		//empty
	}

	// implement here constants used by this extension
	public static final String VERIFIED = "VERIFIED";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";
	public static final String AMENITY = "AMENITY";
	public static final String MINIMUM_PRODUCT_AVAILABILITY = "minimumProductAvailability";
	public static final String TRAVELLER_TYPE_VEHICLE = "VEHICLE";
	public static final String SHIPPING_ADDRESS_TYPE = "shipping";
	public static final String BILLING_ADDRESS_TYPE = "billing";
	public static final String BOOKING_TRANSPORT_ONLY = "BOOKING_TRANSPORT_ONLY";
	public static final String BOOKING_ACCOMMODATION_ONLY = "BOOKING_ACCOMMODATION_ONLY";
	public static final String BOOKING_TRANSPORT_ACCOMMODATION = "BOOKING_TRANSPORT_ACCOMMODATION";
	public static final String BOOKING_ALACARTE = "BOOKING_ALACARTE";
	public static final String BOOKING_PACKAGE = "BOOKING_PACKAGE";
	public static final String DATE_FORMAT = "dateFormat";
	public static final String TIME_FORMAT = "timeFormat";
	public static final String DATE_PATTERN_MMM_DD_YYYY = "MMM dd yyyy";
	public static final String DATE_PATTERN_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DATE_PATTERN_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String BCF_SAILING_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm";
	public static final String DEAL_OF_THE_DAY_DATE_PATTERN = "E MMM dd HH:mm:ss Z yyyy";
	public static final String SCHEDULE_SYNC_DATE_FORMAT = "dd/MM/yyyy HH:mm";
	public static final String PRODUCTS_SYNC_DATE_FORMAT = "dd/MM/yyyy hh:mm";
	public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");
	public static final String JOURNEY_REF_NUMBER = "journeyRefNum";

	public static final String MAX_GUEST_QUANTITY = "finder.max.guests";
	public static final String MAX_CHILD_GUEST_QUANTITY = "finder.max.child.guests";
	public static final String TERMINAL_SEARCH_RESULT_PAGE_SIZE = "terminal.search.results.page.size";
	public static final String DEFAULT_PROVINCE = "bcf.province.default";
	public static final String MAX_COMMENTS_LENGTH = "comments.max.length";
	public static final String MAX_PERSONS_ALLOWED = "maxPassengerAllowed";
	public static final String MAX_PERSONS_ALLOWED_PER_PAX_TYPE_PER_ACTIVITY = "maxPassengerAllowedPerActivity";
	public static final String CHANGE_FARE_OR_REPLAN = "changeFareOrReplan";
	public static final String CHANGE_FARE = "changeFare";
	public static final String REPLAN = "changeReplan";
	public static final String PLAN = "Plan";
	public static final String ORIGIN_DESTINATION_REF_NUM = "odRefNum";
	public static final String SELECTED_JOURNEY_REF_NO = "selectedJourneyRefNumber";
	public static final String LONG_ROUTE_TYPE = "longRouteType";
	public static final String CURRENT_CONDITIONS_LANDING_TERMINALS = "currentConditionsLandingPageTerminals";
	public static final String CURRENT_CONDITIONS_DETAILS_VESSEL_FULL_VALUE = "100";
	public static final String CURRENT_CONDITIONS_MAJOR_TERMINALS_LIMIT_VALUE = "8";
	public static final String CURRENT_CONDITIONS_DETAILS_LIMIT_VALUE = "3";
	public static final String CURRENT_CONDITIONS_SOUTHERN_ISLAND_TERMINALS = "currentConditionsSouthernGulfIslandTerminals";
	public static final String CURRENT_CONDITIONS_MAJOR_TERMINALS = "currentConditionsMajorTerminals";
	public static final String CURRENT_CONDITIONS_WEBCAM_TERMINALS = "currentConditionsWebcamTerminals";
	public static final String CURRENT_CONDITIONS_DETAILS_PAGE_REFRESH_TIME = "currentConditionsDetailsPageRefreshTimeInMilliSeconds";
	public static final String CURRENT_CONDITIONS_MAJOR_TERMINALS_NEXT_SAILING_SHOW_COUNT = "currentConditionsMajorTerminalsNextSailingShowCount";
	public static final String ROUTE_FARES_GUIDE_MEDIA_ID = "routeFaresGuideMediaId";
	public static final String ACTIVATE_PROFILE_SUBSCRIPTION_DISPLAY_LIST = "activateProfilePageSubscriptionDisplayList";

	public static final String GET_PERMANENT_TOKEN = "getPermanentToken";
	public static final String PROCESS_PAYMENT = "processPayment";
	public static final String BUNDLE_TYPE_NAME = "bundleType";
	public static final String TRANSPORT_OFFERINGS = "transportOfferings";
	public static final String CANCEL_BOOKING_RESTRICTION_TIMEGAP = "cancelBookingRestrictionTimeGap";
	public static final String QUOTE_LISTING_PAGE_RECORDS = "quoteListingPageRecords";
	public static final String BOOKING_CONFIRMATION_REFERENCE = "bookingConfirmationReference";
	public static final String REFUND_ORDER = "refundOrder";

	public static final String ERROR_PLACE_ORDER = "error_place_order";
	public static final String ERROR_INVALID_DEPARTURE_TIME = "error.invalid.departure.time";
	public static final String ERROR_CACHE_KEY_OR_BOOKING_REF_NULL = "error.cachekey.or.bookingref.null";
	public static final String SEARCH_FROM_EBOOKING = "mybookings.search.from.ebooking.enable";
	public static final String IS_EBOOKING_ORDER = "isEBookingOrder";
	public static final String SALES_CHANNEL = "salesChannel";
	public static final String BOOKING_APPLICATION_ID = "bookingApplicationId";

	public static final String LENGTH = "length";
	public static final String DESTINATION_LOCATION_CODE = "destinationLocationCode";
	public static final String STAR_RATING = "starRating";
	public static final String ORIGIN_LOCATION_CODE = "originLocationCode";
	public static final String ACCOMMODATION_OFFERING_CODE = "accommodationOfferingcode";
	public static final String LOCATION_CODES = "locationCodes";
	public static final String DEPARTURE_DATE = "departureDate";
	public static final String PRODUCT_CODE_PATTERN = "/{productCode:.*}";
	public static final String ACTIVITY_DATE_PATTERN = "MMM dd yyyy HH:mm:ss";
	public static final String ACTIVITY_DATE_FORMAT = "dd/MM/yyyy";

	public static final String ACCESSIBILITIES = "accessibilities";
	public static final String SPECIALSERVICES = "specialServices";
	public static final String BCF_FARE_FINDER_FORM = "bcfFareFinderForm";
	public static final String FARE_CALCULATOR_FORM = "fareCalculatorForm";
	public static final String TRAVEL_FINDER_FORM = "travelFinderForm";
	public static final String ACCOMMODATION_FINDER_FORM = "accommodationFinderForm";
	public static final String DEFAULT_FARE_BASIS_CODE_FOR_PASSENGER = "defaultTransportOfferingFareProductFareBasisCode";
	public static final String DEFAULT_FARE_BASIS_CODE_FOR_VEHICLE = "defaultTransportOfferingVehicleProductFareBasisCode";
	public static final String NEXT_URL = "nextURL";
	public static final String IS_RETURN = "isReturn";
	public static final String GUEST_TYPES_FOR_PERPERSON_PRICE = "guestTypesforPricePerPersonConsideration";
	public static final String TRANSPORT_OFFERING = "TRANSPORT_OFFERING";
	public static final String EBOOKING_SAILINGCODE = "ebookingSailingCode";
	public static final String TRANSPORTOFFERING_DATE_FORMAT = "dd/MM/yyyy";
	public static final String EBOOKING_TRANSFERIDENTIFIER = "transferIdentifier";
	public static final String RESERVABLE = "reservable";
	public static final String SAILINGCODE_PRODUCTAVAILABILITY_MAP = "sailingCodeProductAvailabilityMap";

	public static final String ACCOUNT_ACTIVATION_EXPIRY_TIME = "account.activation.key.expiry.time";
	public static final String PACKAGE_AVAILABILITY_STATUS = "availabilityStatus";
	public static final String MODIFICATION_JOURNEY = "modify";
	public static final int DEFAULT_PAGE_NUMBER = 1;
	public static final int DEFAULT_ADULTS = 0;
	public static final String TRANSPORT_BOOKINGS = "transportBookings";
	public static final String MY_ACCOUNT_BOOKING = "myBookings";
	public static final String UPCOMING = "upcoming";
	public static final String PAST = "past";
	public static final String HAS_ERROR_FLAG = "hasErrorFlag";
	public static final String ERROR_MESSAGE = "errorMsg";
	public static final String FERRY_TRANSPORTRESERVATION_PATH = "/fare-selection-review";
	public static final String HOME_PAGE_PATH = "/";
	public static final String PACKAGE_DEAL_HOTEL_LISTING_HOTEL_DEAL_CODE = "packageDealCode";
	public static final String PACKAGE_DEAL_HOTEL_LISTING_HOTEL_CODES = "hotelCodes";
	public static final String PACKAGE_DEAL_HOTEL_LISTING_ROOM_RATE_PRODUCTS = "roomRateProducts";
	public static final String PACKAGE_DEAL_HOTEL_LISTING_URL = "/vacations/promotions/";
	public static final String PACKAGE_HOTEL_DETAILS_DESTINATION_CODE = "destinationCode";
	public static final String PACKAGE_HOTEL_DETAILS_PAGE_PATH = "/vacations/hotels/{cityName}/{hotelName}/{accommodationOfferingCode}";
	public static final String MAX_SAILING_ALLOWED = "maxSailingAllowed";
	public static final String ORDER_REPORT_FILE_PATH_DATE = "YYYY-MM-DD HH:mm:ss.sssZ";
	public static final String LARGE_ITEMS_PRODUCTS_CODES = "large.items.products.codes";
	public static final String ACCESSIBILITY_REQUEST_PRODUCTS = "accessibilityRequestProducts";
	public static final String PASSENGER_ADULT_LIST = "passengerAdultList";
	public static final String CARD_TYPE = "cardType";
	public static final String ADDRESS_UPDATE = "text.update.profile.address";
	public static final String NAME_UPDATE = "text.update.profile.name";
	public static final String PHONE_UPDATE = "text.update.profile.phone";
	public static final String PASSWORD_UPDATE = "text.update.profile.password";
	public static final String ADULT = "adult";
	public static final String CHILD = "child";
	public static final String ACTIVITY_SEARCH_RESULT_PAGE_SIZE = "activity.search.results.page.size";
	public static final String ALCARTE_SALES_CHANNELS = "alacarteSalesChannels";
	public static final String BREADCRUMBS = "breadcrumbs";
	public static final String IS_MODIFYING_TRANSPORT_BOOKING = "isModifyingTransportBooking";
	public static final String ORIGINAL_BOOKING_REFERENCE = "originalBookingReference";
	public static final String AMENDING_BOOKING_REFERENCES = "amendingBookingReferences";

	public static final String REVIEW_LOCATION_ID = "reviewLocationId";
	public static final String IS_DEAL_OF_THE_DAY = "isDealOfTheDay";
	public static final String DEAL_OF_THE_DAY_STARTDATE = "dealOfTheDayStartDate";
	public static final String DEAL_OF_THE_DAY_ENDDATE = "dealOfTheDayEndDate";
	public static final String BASE_OCCUPANCY = "baseOccupancy";
	public static final String MAX_OCCUPANCY = "maxOccupancy";
	public static final String NO_OF_ADULTS = "numberOfAdults";

	public static final String SAME_DAY_TRANSPORT_BOOKING_THRESHOLD_HOURS = "thresholdHoursForSameDayBooking";
	public static final String SAME_DAY_HOTEL_BOOKING_THRESHOLD_HOURS = "thresholdHoursForSameDayHotelBooking";
	public static final String OVER_9_FEET_WIDE_THRESHOLD_HOURS = "thresholdHoursForOver9FeetWideVehicle";
	public static final String OVER_9_FEET_VEHICLE_CODE = "over9FtWide";
	public static final String ROOM_GUEST_DATAS = "roomGuestDatas";
	public static final String ACTIVITY_DATAS = "activityGuestDatas";
	public static final String DEBIT_CARD = "Debit Card";
	public static final String CREDIT_CARD = "Credit Card";
	public static final String CARD_WTH_PINPAD = "Card Refund with Pin Pad";
	public static final String REMOVE_FORM_AND_REDIRECT_TO_HOME_URL = "/fare-selection/clear-session-form";
	public static final String DECREMENT_SORT = "DESC";
	public static final String DEPARTURE_LOCATION = "departureLocation";
	public static final String ARRIVAL_LOCATION = "arrivalLocation";
	public static final String TRAVEL_ADVISORIES = "traveladvisories";

	public static final String SESSION_CART_PARAMETER_NAME = "cart";
	public static final String ALACARTE_MAX_ROOMS = "alacarte.passenger.type.number.rooms.max.count";


	public interface Activity
	{
		String DESTINATION = "destination";
		String ACTIVITY_CATEGORY_TYPES = "activityCategoryTypes";
		String ACTIVITY_CODE = "code";
		String AGE = "AGE";
		String ANY = "ANY";
		String ACTIVITY_DATE = "activityDate";
	}

	public interface Payment
	{
		String PAYMENT_ACTION = "paymentAction";
		String ACTION_PAY = "actionPay";
		String ACTION_REFUND = "actionRefund";
		String NO_ACTION = "noAction";
		String PAYMENT_TYPE = "paymentType";
		String GIFT_CARD = "GiftCard";
		String DEPOSIT_AMOUNT = "depositAmount";
		String DEPOSIT_PAY_DATE = "depositPayDate";
		String PAYMENT_TYPE_CARD_PRESENT = "cardPresent";
		String PAYMENT_TYPE_SAVED_CARD_PRESENT = "savedCard";
		String PAYMENT_TYPE_CARD_NOT_PRESENT = "cardNotPresent";
		String PAYMENT_METHOD_CARD = "card";
		String BCF_SUPPORTED_CARD_TYPES = "bcfacceptedcardtypes";
		String AVS_STREET_NAME = "avsStreetName";
		String AVS_STREET_NO = "avsStreetNumber";
		String AVS_ZIPCODE = "avsZipCode";
		String TOKEN = "token";
		String TOKEN_TYPE = "token_type";
		String CARD_TYPE = "cardType";
		String CARD_NUMBER = "cardNumber";
		String CTC_CARD_NUMBER = "ctcCardNo";
		String CARD_EXPIRATION = "cardExpiration";
		String CARD_EXPIRATION_MONTH = "cardExpirationMonth";
		String CARD_EXPIRATION_YEAR = "cardExpirationYear";
		String CARD_CVV_NUMBER = "cardCVNumber";
		String PAYMENT_METHOD_PINPAD = "PINPAD";
		String SAVE_CTC_CARD = "saveCTCcard";
		String SAVE_CC_CARD = "saveCCCard";
		String SAVED_CTC_CARD_CODE = "savedCTCcardCode";
		String SAVED_CC_CARD_CODE = "savedCCCardCode";
	}

	public interface PaymentReceipt
	{
		String BUSINESS_ADDRESS_LINE_1 = "payment.receipt.business.address.line1";
		String BUSINESS_ADDRESS_LINE_2 = "payment.receipt.business.address.line2";
		String BUSINESS_ADDRESS_LINE_3 = "payment.receipt.business.address.line3";
		String BUSINESS_CONTACT_NUMBER = "payment.receipt.business.contact.number";
		String BUSINESS_CONTACT_EMAIL = "payment.receipt.business.contact.email";
		String BUSINESS_GST_NUMBER = "payment.receipt.gst.number";
		String LANG_FRENCH = "FRENCH";
		String RETAIN_INFO_EN = "payment.receipt.retain.info.english";
		String RETAIN_INFO_FR = "payment.receipt.retain.info.french";
		String CARD_HOLDER_COPY_EN = "payment.receipt.card.holder.copy.english";
		String CARD_HOLDER_COPY_FR = "payment.receipt.card.holder.copy.french";
		String MERCHANT_COPY_EN = "payment.receipt.merchant.copy.english";
		String MERCHANT_COPY_FR = "payment.receipt.merchant.copy.french";
		String MERCHANT_MESSAGE_EN = "payment.receipt.merchant.message.english";
		String MERCHANT_MESSAGE_FR = "payment.receipt.merchant.message.french";
	}
}
