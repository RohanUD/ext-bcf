/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.user.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;


public class DefaultBcfAddressReversePopulator extends AddressReversePopulator
{

	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		super.populate(addressData, addressModel);
		addressModel.setPhoneType(addressData.getPhoneType());
		if ((Objects.isNull(addressData.getRegion()) || (Objects.nonNull(addressModel.getRegion()) && Objects
				.equals(addressData.getRegion().getIsocode(), "-1"))))
		{
			addressModel.setRegion(null);
		}
	}
}
