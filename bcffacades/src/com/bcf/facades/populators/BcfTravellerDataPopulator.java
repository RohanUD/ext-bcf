/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;


public class BcfTravellerDataPopulator implements Populator<TravellerModel, TravellerData>
{
	private Converter<BCFVehicleInformationModel, VehicleInformationData> vehicleInformationDataConverter;

	@Override
	public void populate(final TravellerModel source, final TravellerData target) throws ConversionException
	{
		if (source.getType().getCode().equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_VEHICLE))
		{
			target.setTravellerInfo(getVehicleInformationDataConverter().convert((BCFVehicleInformationModel) source.getInfo()));
		}
	}

	public Converter<BCFVehicleInformationModel, VehicleInformationData> getVehicleInformationDataConverter()
	{
		return vehicleInformationDataConverter;
	}

	public void setVehicleInformationDataConverter(
			final Converter<BCFVehicleInformationModel, VehicleInformationData> vehicleInformationDataConverter)
	{
		this.vehicleInformationDataConverter = vehicleInformationDataConverter;
	}

}
