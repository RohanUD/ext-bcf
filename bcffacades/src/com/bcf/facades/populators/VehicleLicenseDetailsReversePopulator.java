/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import javax.annotation.Resource;
import com.bcf.core.enums.VehicleCategoryType;
import com.bcf.core.enums.VehicleTypeCode;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;


public class VehicleLicenseDetailsReversePopulator implements Populator<VehicleInformationData, BCFVehicleInformationModel>
{
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Override
	public void populate(final VehicleInformationData source, final BCFVehicleInformationModel target) throws ConversionException
	{
		target.setLicensePlateNumber(source.getLicensePlateNumber());
		target.setLicensePlateCountry(source.getLicensePlateCountry());
		target.setLicensePlateProvince(source.getLicensePlateProvince());
		target.setCarryingLivestock(source.isCarryingLivestock());
		target.setVehicleWithSidecarOrTrailer(source.isVehicleWithSidecarOrTrailer());
		target.setLength(source.getLength());
		target.setHeight(source.getHeight());
		target.setWidth(source.getWidth());
		target.getVehicleType().setVehicleCategoryType(
				enumerationService.getEnumerationValue(VehicleCategoryType.class, source.getVehicleType().getCategory()));
		target.getVehicleType()
				.setType(enumerationService.getEnumerationValue(VehicleTypeCode.class, source.getVehicleType().getCode()));
		target.getVehicleType().setName(source.getVehicleType().getName());
	}

}
