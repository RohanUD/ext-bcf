/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfstorefrontaddon.model.components.BCFOGGraphTagsModel;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.metatags.BCFOGGraphTagData;


public class BCFOGGraphTagsPopulator implements Populator<BCFOGGraphTagsModel, BCFOGGraphTagData>
{
	private Converter<MediaModel, ImageData> imageConverter;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public void populate(final BCFOGGraphTagsModel source, final BCFOGGraphTagData target) throws ConversionException
	{
		target.setTitle(source.getTitle());
		target.setDescription(source.getDescription());
		target.setType(source.getType());
		target.setUrl(source.getUrl());
		target.setSitename(source.getSitename());
		if (source.getImage() != null)
		{
			final String siteUrl = configurationService.getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL);
			final ImageData imageData = getImageConverter().convert(source.getImage());
			final String imageurl = siteUrl + imageData.getUrl();
			target.setImage(imageurl.replace("//", "/"));
		}
	}

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	@Required
	public void setImageConverter(
			final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}
}
