/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.notification.request.order.createorder.TermsAndConditionsData;


public class BcfTermsAndConditionsDataWSPopulator implements
		Populator<FerryTandCModel, TermsAndConditionsData>
{
	@Override
	public void populate(final FerryTandCModel source, final TermsAndConditionsData target)
			throws ConversionException
	{
		target.setCode(source.getCode());
		target.setTitle(source.getTitle());
		target.setCheckInTitle(source.getCheckInTitle());
		target.setCheckInText(source.getCheckInText());
		target.setAdditionalcheckInTitle(source.getAdditionalCheckInTitle());
		target.setAdditionalcheckInText(source.getAdditionalCheckInText());
		if (source.getImage() != null)
		{
			target.setImage(source.getImage().getURL());
		}
	}

}
