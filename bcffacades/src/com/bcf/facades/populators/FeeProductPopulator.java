package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.FeeProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.util.Objects;


public class FeeProductPopulator implements Populator<FeeProductModel, FeeProductData>
{
	@Override
	public void populate(final FeeProductModel source, final FeeProductData target)
	{
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setFlexibilityLevel(Objects.nonNull(source.getFlexibilityLevel()) ? source.getFlexibilityLevel().getCode() : null);
	}
}
