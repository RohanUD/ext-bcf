/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.commercefacades.travel.enums.ServiceNoticeType;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.core.model.ServiceNoticeModel;


public class ServiceNoticePopulator implements Populator<ServiceNoticeModel, ServiceNoticeData>
{

	@Resource(name = "currentConditionsTravelRouteConverter")
	private Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter;

	@Override
	public void populate(final ServiceNoticeModel source, final ServiceNoticeData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(source, "source must not be null");
		ServicesUtil.validateParameterNotNull(target, "target must not be null");

		target.setCode(source.getPk().toString());
		target.setTitle(source.getTitle());
		target.setPublishedDate(source.getPublishDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setBodyContent(source.getBodyContent());

		List<String> subscriptionGroups = source.getSubscriptionGroups().stream().map(SubscriptionGroup::getCode).distinct()
				.collect(Collectors.toList());
		target.setSubscriptionGroups(subscriptionGroups);

		populateAffectedRoutes(source, target);

		target.setNoticeType(ServiceNoticeType.valueOf(source.getItemtype().toUpperCase()));
	}

	private void populateAffectedRoutes(final ServiceNoticeModel source, final ServiceNoticeData target)
	{
		final List<TravelRouteInfo> travelRoutes = Converters
				.convertAll(source.getAffectedRoutes(), currentConditionsTravelRouteConverter);
		target.setTravelRoutes(travelRoutes);
	}
}
