/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TravelAdvisoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import com.bcf.core.model.TravelAdvisoryModel;


public class TravelAdvisoryPopulator implements Populator<TravelAdvisoryModel, TravelAdvisoryData>
{
	@Override
	public void populate(final TravelAdvisoryModel source, final TravelAdvisoryData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(source, "source must not be null");
		ServicesUtil.validateParameterNotNull(target, "target must not be null");

		target.setCode(source.getPk().toString());
		target.setAdvisoryTitle(source.getAdvisoryTitle());
		target.setPublishDateTime(source.getPublishDateTime());
		target.setExpiryDateTime(source.getExpiryDateTime());
		target.setAdvisoryDescription(source.getAdvisoryDescription());
		target.setStatus(source.getStatus());
		target.setDisplayType(source.getDisplayType().getCode());
	}
}
