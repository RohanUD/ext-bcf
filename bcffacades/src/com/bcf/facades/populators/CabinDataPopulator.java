/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.CabinModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.vessel.CabinData;


public class CabinDataPopulator implements Populator<CabinModel, CabinData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	public void populate(final CabinModel source, final CabinData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(source, "source must not be null");
		ServicesUtil.validateParameterNotNull(target, "target must not be null");

		target.setCabinName(source.getCabinName());
		target.setCabinMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getCabinImage()));
		target.setCabinInfo(source.getCabinInfo());
	}

	protected BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
