/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import org.springframework.beans.factory.annotation.Required;


public class BCFTerminalTravelRoutePopulator implements Populator<TravelRouteModel, TravelRouteData>
{
	private Converter<TransportFacilityModel, TransportFacilityData> transportFacilityConverter;

	@Override
	public void populate(final TravelRouteModel source, final TravelRouteData target) throws ConversionException
	{
		target.setCode(source.getCode());
		target.setName(target.getName());
		target.setDestination(transportFacilityConverter.convert(source.getDestination()));
	}

	@Required
	public void setTransportFacilityConverter(
			final Converter<TransportFacilityModel, TransportFacilityData> transportFacilityConverter)
	{
		this.transportFacilityConverter = transportFacilityConverter;
	}
}
