/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.BcfTravelRouteUtil;


public class BcfCurrentConditionsTravelRoutePopulator implements Populator<TravelRouteModel, TravelRouteInfo>
{
	private ConfigurationService configurationService;

	@Override
	public void populate(final TravelRouteModel source, final TravelRouteInfo target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(source, "source must not be null");
		ServicesUtil.validateParameterNotNull(source.getDestination(), "Destination on source must not be null");
		ServicesUtil
				.validateParameterNotNull(source.getDestination().getLocation(), "Location on source's destination must not be null");
		ServicesUtil.validateParameterNotNull(target, "target must not be null");

		target.setRouteCode(source.getCode());
		target.setSourceTerminalCode(source.getOrigin().getCode());
		target.setSourceTerminalName(source.getOrigin().getName());
		target.setSourceRouteName(BcfTravelRouteUtil.translateName(source.getOrigin().getName()));
		target.setSourceLocationName(source.getOrigin().getLocation().getName());
		target.setSourceTerminalPOSLatitude(source.getOrigin().getPointOfService().get(0).getLatitude());
		target.setSourceTerminalPOSLongitude(source.getOrigin().getPointOfService().get(0).getLongitude());
		target.setDestinationTerminalCode(source.getDestination().getCode());
		target.setDestinationTerminalName(source.getDestination().getName());
		target.setDestinationRouteName(BcfTravelRouteUtil.translateName(source.getDestination().getName()));
		target.setDestinationLocationName(source.getDestination().getLocation().getName());
		target.setDestinationTerminalPOSLatitude(source.getDestination().getPointOfService().get(0).getLatitude());
		target.setDestinationTerminalPOSLongitude(source.getDestination().getPointOfService().get(0).getLongitude());
		if(source.getMapKmlFile()!=null)
		{
			target.setMapKmlFileUrl(source.getMapKmlFile().getURL());
		}
		target.setFerryTrackingLink(source.getFerryTrackingLink());
		if (Objects.nonNull(source.getRouteRegion()))
		{
			target.setRouteRegion(source.getRouteRegion().getCode());
			target.setRouteRegionColor(
					getConfigurationService().getConfiguration().getString(source.getRouteRegion().getCode() + ".region.color"));
		}
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
