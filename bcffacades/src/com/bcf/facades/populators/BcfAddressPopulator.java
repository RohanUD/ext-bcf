/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.Collections;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.dao.BcfTravelLocationDao;


public class BcfAddressPopulator extends AddressPopulator
{
	private BcfTravelLocationDao bcfTravelLocationDao;
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		super.populate(source, target);
		final LocationModel locationByName = bcfTravelLocationDao.findLocationByName(source.getTown());
		if (Objects.nonNull(locationByName))
		{
			final LocationModel provinceLocation = getBcfTravelLocationService()
					.getLocationWithLocationType(Collections.singletonList(locationByName), LocationType.PROVINCE);
			if (Objects.nonNull(provinceLocation))
			{
				target.setProvince(provinceLocation.getCode());
			}
		}
	}

	protected BcfTravelLocationDao getBcfTravelLocationDao()
	{
		return bcfTravelLocationDao;
	}

	@Required
	public void setBcfTravelLocationDao(final BcfTravelLocationDao bcfTravelLocationDao)
	{
		this.bcfTravelLocationDao = bcfTravelLocationDao;
	}

	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
