/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.facades.travel.data.BcfLocationData;


public class BCFVacationLocationPopulator implements Populator<BcfVacationLocationModel, BcfLocationData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CommerceCommonI18NService commerceCommonI18NService;
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final BcfVacationLocationModel source, final BcfLocationData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());
		if (source.getPicture() != null)
		{
			target.setImage(this.getImageConverter().convert(source.getPicture()));
		}
		final BcfLocationData bcfLocationData = target;
		final List<BcfLocationData> locationDataList = new ArrayList();
		final LocationModel locationModel = source.getOriginalLocation();

		if (locationModel != null)
		{
			final BcfLocationData locationData = new BcfLocationData();
			locationData.setCode(locationModel.getCode());
			locationData.setName(locationModel.getName());
			locationDataList.add(locationData);
		}
		bcfLocationData.setDescription(source.getDescription());
		if (Objects.nonNull(source.getPicture()))
		{
			bcfLocationData.setDataMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getPicture()));
			bcfLocationData.setPictureAltText(source.getPicture().getAltText());
		}
		if (Objects.nonNull(source.getMainImage()))
		{
			bcfLocationData.setMainImageDataMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getMainImage()));
			bcfLocationData.setMainImageAltText(source.getMainImage().getAltText());
		}
		bcfLocationData.setOverlayText(source.getOverlayText(getCommerceCommonI18NService().getCurrentLocale()));
		bcfLocationData.setLocationType(source.getLocationType().getCode());
		bcfLocationData.setSuperlocation(locationDataList);

		final MediaModel media = source.getNormalImage();
		if (Objects.nonNull(media))
		{
			final ImageData imageData = new ImageData();
			imageData.setUrl(getBcfResponsiveMediaStrategy().getResponsiveJson(media));
			imageData.setAltText(media.getAltText());
			target.setNormalImage(imageData);
		}
		bcfLocationData.setTranslatedName(BcfTravelRouteUtil.translateName(source.getName()));
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return this.imageConverter;
	}

	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}

}

