/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TransportFacilityAmenityData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.travel.TransportFacilityAmenityModel;
import com.bcf.core.services.servicenotice.ServiceNoticesService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.currentconditions.CurrentConditionsIntegrationFacade;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class BcfTerminalTransportFacilityPopulator extends BcfTransportFacilityPopulator
{
	private static final Logger LOG = Logger.getLogger(BcfTerminalTransportFacilityPopulator.class);
	private static final BigDecimal PERCENT = new BigDecimal(100);

	private Converter<TransportFacilityAmenityModel, TransportFacilityAmenityData> transportFacilityAmenitiesConverter;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CommerceCommonI18NService commerceCommonI18NService;
	private ServiceNoticesService serviceNoticesService;
	private Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeConverter;
	private Converter<TravelRouteModel, TravelRouteData> terminalTravelRouteConverter;
	private BCFTravelRouteService bcfTravelRouteService;
	private CurrentConditionsIntegrationFacade currentConditionsIntegrationFacade;

	@Override
	public void populate(final TransportFacilityModel source, final TransportFacilityData target)
			throws ConversionException
	{
		super.populate(source, target);
		final List<TransportFacilityAmenityData> transportFacilityAmenitiesData = StreamUtil.safeStream(source.getAmenities())
				.map(transportFacilityAmenitiesConverter::convert)
				.collect(Collectors.toList());
		target.setAmenities(transportFacilityAmenitiesData);
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		target.setParkingInfo(source.getParkingInfo(currentLocale));
		target.setDescription(source.getDescription(currentLocale));
		target.setAccessibilityDescription(source.getAccessibilityDescription(currentLocale));
		target.setLuggageServiceDescription(source.getLuggageServiceDescription(currentLocale));
		target.setContactInformation(source.getContactInformation(currentLocale));
		if (BooleanUtils.isTrue(source.getCurrentConditionEnabled()))
		{
			try
			{
				final CurrentConditionsResponseData currentConditions = currentConditionsIntegrationFacade
						.getCurrentConditions(source.getCode());
				if (Objects.nonNull(currentConditions) && Objects.nonNull(currentConditions.getParking()) && Objects
						.nonNull(currentConditions.getParking().getParkingFullPercent()))
				{
					final BigDecimal parkingFullPercent = BigDecimal
							.valueOf(Double.parseDouble(currentConditions.getParking().getParkingFullPercent()));
					target.setParkingAvailable(PERCENT.subtract(parkingFullPercent).toString());
				}
			}
			catch (final IntegrationException e)
			{
				LOG.error("Unable to get Parking available for terminal " + source.getCode(), e);
			}
		}
		target.setMap(bcfResponsiveMediaStrategy.getResponsiveJson(source.getMap()));
		final List<ServiceNoticeData> serviceNoticeData = StreamUtil
				.safeStream(serviceNoticesService.getServiceNoticesForAllRoutes())
				.filter(serviceNotice -> isNoticeForCurrentTransportFacility(serviceNotice, source.getCode()))
				.map(serviceNoticeConverter::convert)
				.collect(Collectors.toList());
		target.setServiceNotices(serviceNoticeData);

		final List<TravelRouteData> travelRouteData = StreamUtil.safeStream(bcfTravelRouteService.getTravelRoutesForOrigin(source))
				.filter(travelRouteModel ->
						Objects.nonNull(travelRouteModel.getIndirectRoute()) && Objects
								.equals(travelRouteModel.getIndirectRoute(), Boolean.FALSE) &&
								Objects.nonNull(travelRouteModel.getCurrentConditionEnabled()) && Objects
								.equals(travelRouteModel.getCurrentConditionEnabled(), Boolean.TRUE))
				.map(terminalTravelRouteConverter::convert)
				.collect(Collectors.toList());
		target.setTravelRoutes(travelRouteData);
		if (source.getActive() != null)
		{
			target.setActive(source.getActive());
		}
		if (source.getCurrentConditionEnabled() != null)
		{
			target.setCurrentConditionEnabled(source.getCurrentConditionEnabled());
		}
	}

	private boolean isNoticeForCurrentTransportFacility(final ServiceNoticeModel serviceNoticeModel,
			final String transportFacilityCode)
	{
		return StreamUtil.safeStream(serviceNoticeModel.getAffectedRoutes())
				.filter(affectedRoute -> transportFacilityCode.equals(affectedRoute.getOrigin().getCode()))
				.findAny()
				.isPresent();
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	@Required
	public void setTransportFacilityAmenitiesConverter(
			final Converter<TransportFacilityAmenityModel, TransportFacilityAmenityData> transportFacilityAmenitiesConverter)
	{
		this.transportFacilityAmenitiesConverter = transportFacilityAmenitiesConverter;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	@Required
	public void setServiceNoticesService(final ServiceNoticesService serviceNoticesService)
	{
		this.serviceNoticesService = serviceNoticesService;
	}

	@Required
	public void setServiceNoticeConverter(
			final Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeConverter)
	{
		this.serviceNoticeConverter = serviceNoticeConverter;
	}

	@Required
	public void setTerminalTravelRouteConverter(
			final Converter<TravelRouteModel, TravelRouteData> terminalTravelRouteConverter)
	{
		this.terminalTravelRouteConverter = terminalTravelRouteConverter;
	}

	@Required
	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		this.bcfTravelRouteService = bcfTravelRouteService;
	}

	@Required
	public void setCurrentConditionsIntegrationFacade(
			final CurrentConditionsIntegrationFacade currentConditionsIntegrationFacade)
	{
		this.currentConditionsIntegrationFacade = currentConditionsIntegrationFacade;
	}
}
