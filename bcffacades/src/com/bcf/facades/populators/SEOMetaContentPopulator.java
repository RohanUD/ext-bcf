/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.model.content.SEOMetaContentModel;
import com.bcf.facades.article.SEOMetaContentData;


public class SEOMetaContentPopulator implements Populator<SEOMetaContentModel, SEOMetaContentData>
{

	@Override
	public void populate(final SEOMetaContentModel source, final SEOMetaContentData target) throws ConversionException
	{
		target.setTitle(source.getTitle() != null ? source.getTitle() : StringUtils.EMPTY);
		if (CollectionUtils.isNotEmpty(source.getKeywords()))
		{
			target.setKeywords(
					source.getKeywords().stream().filter(keyWord -> StringUtils.isNotEmpty(keyWord)).collect(Collectors.joining(",")));
		}
		target.setDescription(source.getDescription());
		target.setType(source.getType());
	}

}
