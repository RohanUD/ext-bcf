/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.populators.LocationPopulator;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.facades.travel.data.BcfLocationData;


public class BCFLocationPopulator extends LocationPopulator
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final LocationModel source, final LocationData target) throws ConversionException
	{
		super.populate(source, target);
		if (target instanceof BcfLocationData && CollectionUtils.isNotEmpty(source.getSuperlocations()))
		{
			final BcfLocationData bcfLocationData = (BcfLocationData) target;
			final List<BcfLocationData> locationDataList = new ArrayList();
			for (final LocationModel locationModel : source.getSuperlocations())
			{
				final BcfLocationData locationData = new BcfLocationData();
				locationData.setCode(locationModel.getCode());
				locationData.setName(locationModel.getName());
				locationDataList.add(locationData);
			}
			bcfLocationData.setDescription(source.getDescription());
			if (Objects.nonNull(source.getPicture()))
			{
				bcfLocationData.setDataMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getPicture()));
				bcfLocationData.setPictureAltText(source.getPicture().getAltText());
			}
			bcfLocationData.setLocationType(source.getLocationType().getCode());
			bcfLocationData.setSuperlocation(locationDataList);
			bcfLocationData.setTranslatedName(BcfTravelRouteUtil.translateName(source.getName()));

		}
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}

