/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TransportFacilityAmenityData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.travel.TransportFacilityAmenityModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


public class BCFTransportFacilityAmenitiesPopulator
		implements Populator<TransportFacilityAmenityModel, TransportFacilityAmenityData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final TransportFacilityAmenityModel source, final TransportFacilityAmenityData target)
	{
		target.setDescription(source.getDescription(getCommerceCommonI18NService().getCurrentLocale()));
		target.setImage(getBcfResponsiveMediaStrategy()
				.getResponsiveJson(source.getImage(getCommerceCommonI18NService().getCurrentLocale())));
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
