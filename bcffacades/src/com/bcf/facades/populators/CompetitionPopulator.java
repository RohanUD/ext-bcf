/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.CompetitionData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.ServiceNoticeModel;


public class CompetitionPopulator implements Populator<CompetitionModel, CompetitionData>
{
	private Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final CompetitionModel source, final CompetitionData target) throws ConversionException
	{
		getServiceNoticePopulator().populate(source, target);
		target.setName(source.getName());
		target.setMainContact(source.getMainContact(getCommerceCommonI18NService().getCurrentLocale()));
		target.setSecondaryContact(source.getSecondaryContact(getCommerceCommonI18NService().getCurrentLocale()));
	}

	public Populator<ServiceNoticeModel, ServiceNoticeData> getServiceNoticePopulator()
	{
		return serviceNoticePopulator;
	}

	@Required
	public void setServiceNoticePopulator(final Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator)
	{
		this.serviceNoticePopulator = serviceNoticePopulator;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
