/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.receipt.ReceiptData;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;


public class FailureReceiptPopulator implements Populator<TransactionDetails, ReceiptData>
{
	private static final Logger LOG = Logger.getLogger(FailureReceiptPopulator.class);

	private ConfigurationService configurationService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final TransactionDetails transactionDetails, final ReceiptData receiptData)
			throws ConversionException
	{
		if (Objects.nonNull(transactionDetails))
		{
			receiptData.setCartOrOrderId(transactionDetails.getCartCode());
			receiptData.setBusinessAddressLine1(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_1));
			receiptData.setBusinessAddressLine2(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_2));
			receiptData.setBusinessAddressLine3(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_3));
			receiptData.setBusinessContactNumber(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_CONTACT_NUMBER));
			receiptData.setBusinessContactEmail(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_CONTACT_EMAIL));
			receiptData.setBusinessGSTNumber(
					getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_GST_NUMBER));
			receiptData.setTransactionAmount(getTravelCommercePriceFacade()
					.createPriceData(transactionDetails.getTransactionAmountInCents() / 100d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			if (StringUtils
					.equalsIgnoreCase(BcfFacadesConstants.PaymentReceipt.LANG_FRENCH, transactionDetails.getCardHolderLanguage()))
			{
				receiptData.setReceiptRetainInfo(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.RETAIN_INFO_FR));
				receiptData.setReceiptCardHolderCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.CARD_HOLDER_COPY_FR));
				receiptData.setReceiptMerchantCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_COPY_FR));
				receiptData.setReceiptMerchantMessage(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_MESSAGE_FR));
			}
			else
			{
				receiptData.setReceiptRetainInfo(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.RETAIN_INFO_EN));
				receiptData.setReceiptCardHolderCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.CARD_HOLDER_COPY_EN));
				receiptData.setReceiptMerchantCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_COPY_EN));
				receiptData.setReceiptMerchantMessage(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_MESSAGE_EN));
			}
			receiptData.setTransactionType(transactionDetails.getTransactionType());
			if (StringUtils.isNotBlank(transactionDetails.getTransactionTimeStamp()))
			{
				final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BcfintegrationserviceConstants.TRANS_TIMESTAMP);
				try
				{
					receiptData.setTransactionTimestamp(simpleDateFormat.parse(transactionDetails.getTransactionTimeStamp()));
				}
				catch (final ParseException e)
				{
					LOG.error("Unable to parse date", e);
				}
			}
			receiptData.setCardType(transactionDetails.getCardType());
			receiptData.setCardNumberToPrint(transactionDetails.getCardNumberToPrint());
			receiptData.setApprovalNumber(transactionDetails.getApprovalNumber());
			receiptData.setServiceProviderPaymentTerminalId(transactionDetails.getServiceProviderPaymentTerminalId());
			receiptData.setTransactionRefNumber(transactionDetails.getTransactionRefNumber());
			receiptData.setCardEntryMethod(transactionDetails.getCardEntryMethod());
			receiptData.setEmvApplicationPreferredName(transactionDetails.getEmvApplicationLabel());
			receiptData.setEmvAid(transactionDetails.getEmvAid());
			receiptData.setEmvTvr(transactionDetails.getEmvTvr());
			receiptData.setEmvTsi(transactionDetails.getEmvTsi());
			receiptData.setEmvPinEntryMessage(transactionDetails.getEmvPinEntryMessage());
			receiptData.setApprovalOrDeclineMessage(transactionDetails.getApprovalOrDeclineMessage());
			receiptData.setSignatureRequired(transactionDetails.isSignatureRequiredFlag());
			receiptData.setDebitAccountType(transactionDetails.getDebitAccountType());
		}
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
