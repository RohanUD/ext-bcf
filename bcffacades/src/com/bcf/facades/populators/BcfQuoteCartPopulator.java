/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.cart.QuoteAccommodationData;
import com.bcf.facades.cart.QuoteActivityData;
import com.bcf.facades.cart.QuoteCartData;
import com.bcf.facades.cart.QuoteSailingData;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public class BcfQuoteCartPopulator implements Populator<CartModel, QuoteCartData>
{

	private BCFGlobalReservationFacade globalReservationFacade;

	@Override
	public void populate(final CartModel source, final QuoteCartData target)
	{
		target.setCode(source.getCode());

		BcfGlobalReservationData bcfGlobalReservationData = null;
		final BcfGlobalReservationData bcfFerryReservationData = globalReservationFacade
				.getbcfGlobalReservationDataListForFerry(source);

		if (!BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(source.getBookingJourneyType()))
		{
			bcfGlobalReservationData = globalReservationFacade.getbcfGlobalReservationDataList(source);
			if (bcfFerryReservationData != null)
			{
				bcfGlobalReservationData.setTransportReservations(bcfFerryReservationData.getTransportReservations());
			}

		}
		else
		{
			bcfGlobalReservationData = bcfFerryReservationData;
		}

		final List<QuoteAccommodationData> quoteAccommodationDatas = new ArrayList<>();
		final List<QuoteActivityData> quoteActivityDatas = new ArrayList<>();
		final List<QuoteSailingData> quoteSailingDatas = new ArrayList<>();

		if (bcfGlobalReservationData.getPackageReservations() != null && CollectionUtils
				.isNotEmpty(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()))
		{

			for (final GlobalTravelReservationData globalTravelReservationData : bcfGlobalReservationData.getPackageReservations()
					.getPackageReservationDatas())
			{
				if (globalTravelReservationData.getAccommodationReservationData() != null)
				{
					quoteAccommodationDatas
							.add(createQuoteAccommodationData(globalTravelReservationData.getAccommodationReservationData()));

				}

				if (globalTravelReservationData.getActivityReservations() != null && CollectionUtils
						.isNotEmpty(globalTravelReservationData.getActivityReservations().getActivityReservationDatas()))
				{
					for (final ActivityReservationData activityReservationData : globalTravelReservationData.getActivityReservations()
							.getActivityReservationDatas())
					{
						quoteActivityDatas.add(createQuoteActivityData(activityReservationData));
					}

				}

				if (globalTravelReservationData.getReservationData() != null && CollectionUtils
						.isNotEmpty(globalTravelReservationData.getReservationData().getReservationItems()))
				{



					quoteSailingDatas.add(createQuoteSailingData(globalTravelReservationData.getReservationData()));


				}

			}



		}
		else
		{

			if (bcfGlobalReservationData.getAccommodationReservations() != null && CollectionUtils
					.isNotEmpty(bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()))
			{

				for (final AccommodationReservationData accommodationReservationData : bcfGlobalReservationData
						.getAccommodationReservations()
						.getAccommodationReservations())
				{
					quoteAccommodationDatas.add(createQuoteAccommodationData(accommodationReservationData));
				}

			}

			if (bcfGlobalReservationData.getActivityReservations() != null && CollectionUtils
					.isNotEmpty(bcfGlobalReservationData.getActivityReservations().getActivityReservationDatas()))
			{
				for (final ActivityReservationData activityReservationData : bcfGlobalReservationData.getActivityReservations()
						.getActivityReservationDatas())
				{
					quoteActivityDatas.add(createQuoteActivityData(activityReservationData));
				}

			}

			if (bcfGlobalReservationData.getTransportReservations() != null && CollectionUtils
					.isNotEmpty(bcfGlobalReservationData.getTransportReservations().getReservationDatas()))
			{

				for (final ReservationData reservationData : bcfGlobalReservationData.getTransportReservations()
						.getReservationDatas())
				{


					quoteSailingDatas.add(createQuoteSailingData(reservationData));



				}

			}



		}

		target.setQuoteAccommodationDatas(quoteAccommodationDatas);
		target.setQuoteActivityDatas(quoteActivityDatas);
		target.setQuoteSailingDatas(quoteSailingDatas);

		final AbstractOrderEntryModel transportEntry = source.getEntries().stream()
				.filter(entry -> entry.getType().equals(OrderEntryType.TRANSPORT)).findFirst().orElse(null);

		if(transportEntry!=null)
		{
			target.setDeparturedate(
					transportEntry
							.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get().getDepartureTime());
			final List<LocationModel> destinationLocations = transportEntry.getTravelOrderEntryInfo()
					.getTransportOfferings()
					.stream().findFirst().get().getDestinationLocations();
			if (CollectionUtils.isNotEmpty(destinationLocations))
			{
				target.setDestination(destinationLocations.stream().findFirst().get().getName());
			}
		}
		target.setModifiedTime(source.getModifiedtime());
	}

	private QuoteAccommodationData createQuoteAccommodationData(final AccommodationReservationData accommodationReservationData)
	{

		final QuoteAccommodationData quoteAccommodationData = new QuoteAccommodationData();
		quoteAccommodationData.setName(
				accommodationReservationData.getAccommodationReference()
						.getAccommodationOfferingName());
		if (CollectionUtils.isNotEmpty(accommodationReservationData.getRoomStays()))
		{
			quoteAccommodationData.setCheckInDate(
					accommodationReservationData.getRoomStays().get(0).getCheckInDate());
			quoteAccommodationData.setCheckOutDate(
					accommodationReservationData.getRoomStays().get(0).getCheckOutDate());
		}

		return quoteAccommodationData;

	}

	private QuoteActivityData createQuoteActivityData(final ActivityReservationData activityReservationData)
	{

		final QuoteActivityData quoteActivityData = new QuoteActivityData();

		quoteActivityData.setName(
				activityReservationData.getActivityDetails().getName());
		quoteActivityData.setActivityDate(activityReservationData.getActivityDate());
		quoteActivityData.setActivityTime(activityReservationData.getActivityTime());

		return quoteActivityData;

	}

	private QuoteSailingData createQuoteSailingData(final ReservationData reservationData)
	{


		final QuoteSailingData quoteSailingData = new QuoteSailingData();
		for (final ReservationItemData reservationItemData : reservationData.getReservationItems())
		{

			if (reservationItemData.getOriginDestinationRefNumber() == 0)
			{

				quoteSailingData.setRoute(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0).getTransportOfferings()
								.get(0).getSector().getOrigin().getLocation().getName() + " " + reservationItemData
								.getReservationItinerary()
								.getOriginDestinationOptions().get(0).getTransportOfferings()
								.get(0).getSector().getOrigin().getName() + "-" + reservationItemData.getReservationItinerary()
								.getOriginDestinationOptions().get(0).getTransportOfferings()
								.get(0).getSector().getDestination().getLocation().getName() + " " + reservationItemData
								.getReservationItinerary()
								.getOriginDestinationOptions().get(0).getTransportOfferings()
								.get(0).getSector().getDestination().getName());

				quoteSailingData.setDepartureVehicle(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getTransportVehicle().getVehicleInfo().getName());
				quoteSailingData.setDepartureVehicleCode(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getTransportVehicle().getVehicleInfo().getCode());

				quoteSailingData.setDepartureDate(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getDepartureTime());
			}
			else
			{
				quoteSailingData.setReturnVehicle(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getTransportVehicle().getVehicleInfo().getName());
				quoteSailingData.setReturnVehicleCode(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getTransportVehicle().getVehicleInfo().getCode());
				quoteSailingData.setReturnDate(
						reservationItemData.getReservationItinerary().getOriginDestinationOptions().get(0)
								.getTransportOfferings().get(0).getDepartureTime());

			}






		}

		return quoteSailingData;

	}



	public BCFGlobalReservationFacade getGlobalReservationFacade()
	{
		return globalReservationFacade;
	}

	public void setGlobalReservationFacade(final BCFGlobalReservationFacade globalReservationFacade)
	{
		this.globalReservationFacade = globalReservationFacade;
	}
}
