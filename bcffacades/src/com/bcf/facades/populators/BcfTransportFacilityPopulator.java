/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelfacades.populators.TransportFacilityPopulator;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.TransportFacilityService;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.util.BcfTravelRouteUtil;


public class BcfTransportFacilityPopulator extends TransportFacilityPopulator
{
	BCFTravelCommercePriceService bcfTravelCommercePriceService;
	private Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator;

	@Override
	public void populate(final TransportFacilityModel source, final TransportFacilityData target)
			throws ConversionException
	{
		super.populate(source, target);

		target.setMarginRate(getBcfTravelCommercePriceService().getMarginRate(source));
		if (Objects.nonNull(source.getStandardVehicleLength()))
		{
			target.setStandardVehicleLength(source.getStandardVehicleLength());
		}
		if (Objects.nonNull(source.getOver5500VehicleLength()))
		{
			target.setOver5500VehicleLength(source.getOver5500VehicleLength());
		}
		if (Objects.nonNull(source.getStandardVehicleHeight()))
		{
			target.setStandardVehicleHeight(source.getStandardVehicleHeight());
		}
		if (Objects.nonNull(source.getOver5500VehicleHeight()))
		{
			target.setOver5500VehicleHeight(source.getOver5500VehicleHeight());
		}
		if (Objects.nonNull(source.getVehicleWidth()))
		{
			target.setVehicleWidth(source.getVehicleWidth());
		}
		if(Objects.nonNull(source.getName())){
			target.setRouteName(BcfTravelRouteUtil.translateName(source.getName()));
		}
		if (CollectionUtils.isNotEmpty(source.getPointOfService()))
		{
			final PointOfServiceData pointOfServiceData = new PointOfServiceData();
			pointOfServicePopulator.populate(source.getPointOfService().iterator().next(), pointOfServiceData);
			target.setPointOfServiceData(pointOfServiceData);
		}

		target.setGoogleDirections(source.getGoogleDirections());
		target.setTransportFacilityServices(source.getServices().stream().map(TransportFacilityService::getCode)
				.collect(Collectors.toList()));
		target.setParkingInfo(source.getParkingInfo());
		if (source.getCurrentConditionEnabled() != null)
		{
			target.setCurrentConditionEnabled(source.getCurrentConditionEnabled());
		}
	}

	/**
	 * @return the bcfTravelCommercePriceService
	 */
	protected BCFTravelCommercePriceService getBcfTravelCommercePriceService()
	{
		return bcfTravelCommercePriceService;
	}

	/**
	 * @param bcfTravelCommercePriceService the bcfTravelCommercePriceService to set
	 */
	@Required
	public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
	{
		this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
	}

	@Required
	public void setPointOfServicePopulator(
			final Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator)
	{
		this.pointOfServicePopulator = pointOfServicePopulator;
	}
}
