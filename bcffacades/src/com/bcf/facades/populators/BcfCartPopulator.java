/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.model.order.CartModel;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.util.OptionBookingUtil;


public class BcfCartPopulator extends CartPopulator
{
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		super.populate(source, target);
		target.setModifiedtime(source.getModifiedtime());
		target.setCreationTime(source.getCreationtime());
		target.setCartStatus(source.getStatus());

		if (OptionBookingUtil.isOptionBooking(source))
		{
			target.setExpirationTime(source.getOptionBookingExpiryDate());
			target.setOptionBookingCreationDate(source.getOptionBookingCreationDate());
			target.setOptionBookingExpiryDate(source.getOptionBookingExpiryDate());
			target.setOptionBookingReleaseDate(source.getOptionBookingReleaseDate());
			target.setOptionBooking(source.isVacationOptionBooking());
			addCreatedBy(source, target);
			addDepartureDate(source, target);
		}
	}

	private void addCreatedBy(final CartModel source, final CartData target)
	{
		if (Objects.nonNull(source.getAgent()))
		{
			final PrincipalData createdBy = new PrincipalData();
			if (StringUtils.isNotEmpty(source.getAgent().getName()))
			{
				createdBy.setName(source.getAgent().getName());
			}

			if (StringUtils.isNotEmpty(source.getAgent().getUid()))
			{
				createdBy.setUid(source.getAgent().getUid());

			}
			target.setCreatedBy(createdBy);
		}
	}

	private void addDepartureDate(final CartModel source, final CartData target)
	{
		target.setDepartureDate(OptionBookingUtil.getDepartureDate(source));
	}
}
