/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.NewsData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;


public class NewsPopulator implements Populator<NewsModel, NewsData>
{
	private Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator;

	@Override
	public void populate(final NewsModel source, final NewsData target) throws ConversionException
	{
		getServiceNoticePopulator().populate(source, target);
		target.setPageUrl(source.getPageUrl());
	}

	public Populator<ServiceNoticeModel, ServiceNoticeData> getServiceNoticePopulator()
	{
		return serviceNoticePopulator;
	}

	@Required
	public void setServiceNoticePopulator(final Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator)
	{
		this.serviceNoticePopulator = serviceNoticePopulator;
	}
}
