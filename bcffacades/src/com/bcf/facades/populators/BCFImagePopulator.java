/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


/**
 * Image Populator. Only populates custom properties. Use bcfImageConverter to convert Image data.
 * Converter implementation for {@link de.hybris.platform.core.model.media.MediaModel} as source and {@link de.hybris.platform.commercefacades.product.data.ImageData} as target type.
 */
public class BCFImagePopulator implements Populator<MediaModel, ImageData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	public void populate(final MediaModel source, final ImageData target)
	{
		target.setCaption(source.getCaption());
		target.setLocationTag(source.getLocationTag());
		target.setFeaturedText(source.getFeaturedText());
		target.setIconText(source.getIconText());
		target.setPromoText(source.getPromoText());
		target.setConversionGroup(source.getConversionGroup());
		target.setDataMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source));
		target.setAltText(source.getAltText());
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
