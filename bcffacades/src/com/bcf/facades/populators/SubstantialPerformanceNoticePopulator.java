/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.SubstantialPerformanceNoticeData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;


public class SubstantialPerformanceNoticePopulator
		implements Populator<SubstantialPerformanceNoticeModel, SubstantialPerformanceNoticeData>
{
	private Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final SubstantialPerformanceNoticeModel source, final SubstantialPerformanceNoticeData target)
			throws ConversionException
	{
		getServiceNoticePopulator().populate(source, target);
		target.setProductOwner(source.getProductOwner());
		target.setProductOwnerAddress(source.getProductOwnerAddress(getCommerceCommonI18NService().getCurrentLocale()));
		target.setContractor(source.getContractor());
		target.setConsultant(source.getConsultant());
		target.setSubstantialDate(source.getSubstantialDate());
		target.setAdditionalNote(source.getAdditionalNote(getCommerceCommonI18NService().getCurrentLocale()));
		target.setPrimaryContact(source.getPrimaryContact(getCommerceCommonI18NService().getCurrentLocale()));
	}

	public Populator<ServiceNoticeModel, ServiceNoticeData> getServiceNoticePopulator()
	{
		return serviceNoticePopulator;
	}

	@Required
	public void setServiceNoticePopulator(final Populator<ServiceNoticeModel, ServiceNoticeData> serviceNoticePopulator)
	{
		this.serviceNoticePopulator = serviceNoticePopulator;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
