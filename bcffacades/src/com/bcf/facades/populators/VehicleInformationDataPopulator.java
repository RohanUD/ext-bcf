/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.facades.ferry.VehicleTypeData;


public class VehicleInformationDataPopulator implements Populator<BCFVehicleInformationModel, VehicleInformationData>
{
	@Override
	public void populate(final BCFVehicleInformationModel source, final VehicleInformationData target) throws ConversionException
	{
		if (Objects.isNull(source.getVehicleType()))
		{
			return;
		}
		final VehicleTypeData vehicledata = new VehicleTypeData();
		vehicledata.setCode(source.getVehicleType().getType().getCode());
		vehicledata.setName(source.getVehicleType().getName());
		vehicledata.setCategory(source.getVehicleType().getVehicleCategoryType().getCode());
		target.setLength(Objects.nonNull(source.getLength()) ? source.getLength() : 0d);
		target.setHeight(Objects.nonNull(source.getHeight()) ? source.getHeight() : 0d);
		target.setWidth(Objects.nonNull(source.getWidth()) ? source.getWidth() : 0d);
		target.setWeight(Objects.nonNull(source.getWeight()) ? source.getWeight() : 0d);
		target.setCarryingLivestock(source.getCarryingLivestock());
		target.setVehicleWithSidecarOrTrailer(source.getVehicleWithSidecarOrTrailer());
		target.setVehicleType(vehicledata);
		target.setLicensePlateCountry(source.getLicensePlateCountry());
		target.setLicensePlateNumber(source.getLicensePlateNumber());
		target.setLicensePlateProvince(source.getLicensePlateProvince());
		target.setNumberOfAxis(source.getNumberOfAxis()!=null?source.getNumberOfAxis():0);

	}
}
