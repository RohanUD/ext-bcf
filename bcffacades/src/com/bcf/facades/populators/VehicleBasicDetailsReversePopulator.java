/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.vehicletype.VehicleTypeService;


public class VehicleBasicDetailsReversePopulator implements Populator<VehicleInformationData, BCFVehicleInformationModel>
{

	private VehicleTypeService vehicleTypeService;

	@Override
	public void populate(final VehicleInformationData source, final BCFVehicleInformationModel target) throws ConversionException
	{
		target.setLength(source.getLength());
		target.setHeight(source.getHeight());
		target.setWidth(source.getWidth());
		target.setCarryingLivestock(source.isCarryingLivestock());
		target.setVehicleWithSidecarOrTrailer(source.isVehicleWithSidecarOrTrailer());
		target.setVehicleType(getVehicleTypeService().getVehicleForCode(source.getVehicleType().getCode()));
	}

	protected VehicleTypeService getVehicleTypeService()
	{
		return vehicleTypeService;
	}

	@Required
	public void setVehicleTypeService(final VehicleTypeService vehicleTypeService)
	{
		this.vehicleTypeService = vehicleTypeService;
	}

}
