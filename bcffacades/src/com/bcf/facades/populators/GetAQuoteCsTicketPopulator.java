/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.change.booking.CsTicketData;


/**
 * Cs Ticket Data for Get A Quote implementation.
 */
public class GetAQuoteCsTicketPopulator implements Populator<CsTicketModel, CsTicketData>
{
	private TicketService ticketService;
	private TypeService typeService;
	private AssistedServiceService assistedServiceService;

	@Override
	public void populate(final CsTicketModel csTicketModel, final CsTicketData csTicketData) throws ConversionException
	{
		if (csTicketModel != null)
		{
			final CsCustomerEventModel csCustomerEvent = (CsCustomerEventModel) (ticketService.getEventsForTicket(csTicketModel)
					.get(0));
			csTicketData.setCode(csTicketModel.getTicketID());
			csTicketData.setChangeDescription(csCustomerEvent.getText());
			csTicketData.setCustomerContactNumber(csCustomerEvent.getContactNumber());
			csTicketData.setCustomerEmail(csCustomerEvent.getEmail());
			csTicketData.setCustomerName(csCustomerEvent.getName());
			csTicketData.setTicketStatusCode(csTicketModel.getState().getCode());
			csTicketData.setTicketStatus(getTypeService().getEnumerationValue(csTicketModel.getState()).getName());
			csTicketData.setCreationTime(csTicketModel.getCreationtime());
			csTicketData.setDetails(csCustomerEvent.getText());
			if (csTicketModel.getAssignedAgent() != null)
			{
				csTicketData.setAssignee(csTicketModel.getAssignedAgent());
			}
			if (assistedServiceService.getAsmSession() != null && csTicketModel.getAssignedAgent() != null)
			{
				csTicketData.setIsClosed(
						assistedServiceService.getAsmSession().getAgent().getName().equals(csTicketModel.getAssignedAgent().getName()));
			}
			else
			{
				csTicketData.setIsClosed(false);
			}
		}
	}

	public TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	public TicketService getTicketService()
	{
		return ticketService;
	}

	public void setTicketService(final TicketService ticketService)
	{
		this.ticketService = ticketService;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}
}
