/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;


public class BcfPassengerDetailsReversePopulator implements Populator<PassengerInformationData, PassengerInformationModel>

{
	public void populate(PassengerInformationData source, PassengerInformationModel target) throws ConversionException
	{
		target.setPhoneNumber(source.getPhoneNumber());
	}


}

