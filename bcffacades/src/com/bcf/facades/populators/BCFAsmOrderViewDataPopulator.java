/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.facades.order.AsmActivitiesDataView;
import com.bcf.facades.order.AsmEntryPassengerDataView;
import com.bcf.facades.order.AsmEntryProductDataView;
import com.bcf.facades.order.AsmGoodWillDataView;
import com.bcf.facades.order.AsmHotelRoomDataView;
import com.bcf.facades.order.AsmOrderModificationsDataView;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.order.AsmRoomGuestDataView;
import com.bcf.facades.order.AsmTransactionsDataView;
import com.bcf.facades.order.AsmTransportEntryDataView;
import com.bcf.facades.order.AsmVacationPacakageAppliedPromotionsDataView;
import com.bcf.facades.order.BcfOrderFacade;


public class BCFAsmOrderViewDataPopulator implements Populator<AbstractOrderModel, AsmOrderViewData>
{
	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	@Resource(name = "changeAndCancellationFeeCalculationHelper")
	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AsmOrderViewData asmOrderViewData)
			throws ConversionException
	{
		final List<AbstractOrderEntryModel> entryModels = abstractOrderModel.getEntries();

		final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByBookingRef = entryModels.stream()
				.filter(entry -> BooleanUtils.isTrue(entry.getActive()))
				.filter(entry -> StringUtils.isNotBlank(entry.getBookingReference()))
				.collect(Collectors.groupingBy(entry -> entry.getBookingReference()));

		final Map<String, Map<OrderEntryType, List<AbstractOrderEntryModel>>> orderEntriesGroupByBookingRefEntryType = new HashMap<>();

		orderEntriesGroupByBookingRef.entrySet().forEach(entry -> {
			orderEntriesGroupByBookingRefEntryType
					.put(entry.getKey(), entry.getValue().stream().collect(Collectors.groupingBy(orderEntry -> orderEntry.getType())));
		});

		asmOrderViewData.setBookingDate(abstractOrderModel.getCreationtime());

		setSalesApplication(abstractOrderModel, asmOrderViewData);

		asmOrderViewData
				.setCreatedByAgent(abstractOrderModel.getAgent() != null ?
						abstractOrderModel.getAgent().getName() :
						SalesApplication.WEB.getCode());

		if (CollectionUtils.isNotEmpty(abstractOrderModel.getDiscounts()))
		{
			final Double packageDiscounts =
					abstractOrderModel.getDiscounts().stream().mapToDouble(discount -> discount.getValue()).sum();
			asmOrderViewData.setPackageDiscountsBeforeTax(packageDiscounts);
		}

		final List<AsmGoodWillDataView> asmGoodWillDataViewList = getGoodWillData(abstractOrderModel);
		final Double goodwillBeforeTax = asmGoodWillDataViewList.stream()
				.mapToDouble(asmGoodWillDataView -> asmGoodWillDataView.getGoodwillTransactionAmount())
				.sum();
		asmOrderViewData.setGoodwillBeforeTax(goodwillBeforeTax);

		setTravelDate(abstractOrderModel, asmOrderViewData);

		if (CollectionUtils.isNotEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			asmOrderViewData.setVacationsDepositPaidDate(getDepositPaidDate(abstractOrderModel));
		}
		asmOrderViewData.setVacationsBalanceDueDate(abstractOrderModel.getDepositPayDate());


		if (isVacationOrder(abstractOrderModel))
		{
			asmOrderViewData.setVacationOrder(Boolean.TRUE);
			asmOrderViewData.setVacationsTravellerName(getTravellerName(entryModels));
			final Double vacationCostToBCFV =
					entryModels.stream().filter(entry -> Objects.nonNull(entry.getCostToBCF()))
							.mapToDouble(entry -> entry.getCostToBCF()).sum();
			final Double vacationMargin = entryModels.stream().filter(entry -> Objects.nonNull(entry.getMargin()))
					.mapToDouble(entry -> entry.getMargin()).sum();
			asmOrderViewData.setVacationsTotalCostToBCF(vacationCostToBCFV);
			asmOrderViewData.setVacationsTotalMargin(vacationMargin);

			final List<AsmVacationPacakageAppliedPromotionsDataView> appliedPromotionsDataViews = getOrderPromotionsData(
					abstractOrderModel);
			asmOrderViewData.setVacationPacakageAppliedPromotions(appliedPromotionsDataViews);
		}

		if (CollectionUtils.isNotEmpty(abstractOrderModel.getComments()))
		{
			final CommentModel commentModel = abstractOrderModel.getComments().stream().findFirst().get();
			asmOrderViewData.setVacationsCommentText(commentModel.getText());
			asmOrderViewData.setVacationsCommentDate(commentModel.getCreationtime());
			if (Objects.nonNull(commentModel.getAuthor()))
			{
				asmOrderViewData.setVacationsCommentAgent(commentModel.getAuthor().getName());
			}
		}

		populateEntryProductDetails(asmOrderViewData, orderEntriesGroupByBookingRef);

		populateEntryTransportDetails(asmOrderViewData, orderEntriesGroupByBookingRefEntryType);

		populateEntryPassengerDetails(asmOrderViewData, orderEntriesGroupByBookingRefEntryType);

		populateEntryHotelRoomDetails(asmOrderViewData, entryModels);

		populateEntryActivityDetails(asmOrderViewData, entryModels);

		if (CollectionUtils.isNotEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			populatePaymentTransactions(asmOrderViewData, abstractOrderModel.getPaymentTransactions());
		}

		if (abstractOrderModel instanceof OrderModel)
		{
			populateOrderHistoryData(asmOrderViewData, ((OrderModel) abstractOrderModel));
		}
	}

	private void setTravelDate(final AbstractOrderModel abstractOrderModel, final AsmOrderViewData asmOrderViewData)
	{
		Date travelDate = null;
		if (abstractOrderModel instanceof CartModel)
		{
			travelDate = changeAndCancellationFeeCalculationHelper.getFirstTravelDate(abstractOrderModel.getEntries(), null);
		}
		else if (abstractOrderModel instanceof OrderModel)
		{
			travelDate = ((OrderModel) abstractOrderModel).getVacationDepartureDate();
		}
		else
		{
			travelDate = abstractOrderModel.getCreationtime();
		}

		asmOrderViewData.setVacationsDepartureDate(travelDate);
	}

	private void setSalesApplication(final AbstractOrderModel abstractOrderModel, final AsmOrderViewData asmOrderViewData)
	{
		SalesApplication salesApplication = null;
		if (abstractOrderModel instanceof CartModel)
		{
			salesApplication = ((CartModel) abstractOrderModel).getSalesApplication();
		}
		else if (abstractOrderModel instanceof OrderModel)
		{
			salesApplication = ((OrderModel) abstractOrderModel).getSalesApplication();
		}
		else
		{
			salesApplication = SalesApplication.WEB;
		}

		asmOrderViewData.setSalesChannel(salesApplication != null ? salesApplication.getCode() :
				StringUtils.EMPTY);
	}

	private void populateOrderHistoryData(final AsmOrderViewData asmOrderViewData,
			final OrderModel orderModel)
	{
		final List<AsmOrderModificationsDataView> orderModificationsList = new ArrayList<>();
		populateVersionedOrder(orderModificationsList, orderModel);
		asmOrderViewData.setOrderModificationsData(orderModificationsList);
	}

	private void populateVersionedOrder(final List<AsmOrderModificationsDataView> orderModificationsList,
			final OrderModel mainOrderModel)
	{
		final List<OrderModel> orderModels = bcfOrderFacade.getOrdersByCode(mainOrderModel.getCode());
		if (CollectionUtils.isNotEmpty(orderModels))
		{
			Double orderTotal = 0.0;
			Double orderTotalDifference = 0.0;
			Double orderTotalCostToBCF = 0.0;
			Double orderTotalCostToBCFDifference = 0.0;

			for (final OrderModel orderModel : orderModels)
			{
				final AsmOrderModificationsDataView asmOrderModificationsDataView = new AsmOrderModificationsDataView();
				asmOrderModificationsDataView.setOrderModificationDate(orderModel.getModifiedtime());
				if (Objects.nonNull(orderModel.getAgent()))
				{
					asmOrderModificationsDataView.setModifiedByAgent(orderModel.getAgent().getName());
				}

				if (orderTotal == 0)
				{
					orderTotal = orderModel.getTotalPrice();
				}
				else
				{
					final Double newOrderTotal = orderModel.getTotalPrice();
					orderTotalDifference = newOrderTotal - orderTotal;
					orderTotal = newOrderTotal;
				}

				if (orderTotalCostToBCF == 0)
				{
					orderTotalCostToBCF = orderModel.getEntries().stream().filter(entry -> Objects.nonNull(entry.getCostToBCF()))
							.mapToDouble(entry -> entry.getCostToBCF()).sum();
				}
				else
				{
					final Double newOrderTotalCostToBCF = orderModel.getEntries().stream()
							.filter(entry -> Objects.nonNull(entry.getCostToBCF()))
							.mapToDouble(entry -> entry.getCostToBCF()).sum();
					orderTotalCostToBCFDifference = newOrderTotalCostToBCF - orderTotalCostToBCF;
					orderTotalCostToBCF = newOrderTotalCostToBCF;
				}

				asmOrderModificationsDataView.setOrderTotalDifference(orderTotalDifference);
				asmOrderModificationsDataView.setOrderTotalCostToBCFDifference(orderTotalCostToBCFDifference);
				asmOrderModificationsDataView.setOrderVersionId(orderModel.getVersionID());
				orderModificationsList.add(asmOrderModificationsDataView);
			}
		}
	}

	private void populatePaymentTransactions(final AsmOrderViewData asmOrderViewData,
			final List<PaymentTransactionModel> paymentTransactions)
	{
		final List<AsmTransactionsDataView> transactions = new ArrayList<>();

		paymentTransactions.forEach(transactionModel ->
		{
			final AsmTransactionsDataView asmTransactionsDataView = new AsmTransactionsDataView();
			asmTransactionsDataView.setPaymentTimeStamp(transactionModel.getCreationtime());
			final List<PaymentTransactionEntryModel> transactionEntryModels = transactionModel.getEntries();
			if (CollectionUtils.isNotEmpty(transactionEntryModels))
			{
				transactionEntryModels.forEach(transactionEntryModel -> {
					asmTransactionsDataView.setPaymentAgent(Objects.nonNull(transactionEntryModel.getAgent()) ?
							transactionEntryModel.getAgent().getName() :
							StringUtils.EMPTY);
					asmTransactionsDataView.setTransactionAmount(
							Objects.nonNull(transactionEntryModel.getAmount()) ? transactionEntryModel.getAmount().doubleValue() : 0.0);
					setPaymentNumber(transactionModel, asmTransactionsDataView, transactionEntryModel);
				});
			}
			transactions.add(asmTransactionsDataView);
		});
		asmOrderViewData.setTransactions(transactions);
	}

	private void setPaymentNumber(final PaymentTransactionModel transactionModel,
			final AsmTransactionsDataView asmTransactionsDataView, final PaymentTransactionEntryModel transactionEntryModel)
	{
		final BCFPaymentMethodType bcfPaymentMethodType = transactionEntryModel.getTransactionType();
		if (Objects.nonNull(bcfPaymentMethodType))
		{
			asmTransactionsDataView.setPaymentMethod(bcfPaymentMethodType.getCode());
			if (BCFPaymentMethodType.CARD.equals(bcfPaymentMethodType))
			{
				final CreditCardPaymentInfoModel paymentInfo = ((CreditCardPaymentInfoModel) transactionModel
						.getInfo());
				asmTransactionsDataView.setPaymentNumberText(paymentInfo.getNumber());
			}

			if (BCFPaymentMethodType.CASH.equals(bcfPaymentMethodType))
			{
				final CashPaymentInfoModel paymentInfo = ((CashPaymentInfoModel) transactionModel.getInfo());
				asmTransactionsDataView.setPaymentNumberText(paymentInfo.getReceiptNumber());
			}

			if (BCFPaymentMethodType.GIFTCARD.equals(bcfPaymentMethodType))
			{
				final GiftCardPaymentInfoModel paymentInfo = ((GiftCardPaymentInfoModel) transactionModel.getInfo());
				asmTransactionsDataView.setPaymentNumberText(paymentInfo.getGiftCardNumber());
			}
		}
	}

	private void populateEntryActivityDetails(final AsmOrderViewData asmOrderViewData,
			final List<AbstractOrderEntryModel> entryModels)
	{
		final List<AbstractOrderEntryModel> activitiesEntries = entryModels.stream()
				.filter(entry -> OrderEntryType.ACTIVITY.equals(entry.getType())).collect(Collectors.toList());

		final List<AsmActivitiesDataView> activitiesDataViews = new ArrayList<>();
		activitiesEntries.forEach(activityEntry ->
		{
			final AsmActivitiesDataView asmActivitiesDataView = new AsmActivitiesDataView();
			final ActivityProductModel activityProduct = (ActivityProductModel) activityEntry.getProduct();
			if (CollectionUtils.isNotEmpty(activityProduct.getVendors()))
			{
				asmActivitiesDataView.setActivityName(activityProduct.getName());
				asmActivitiesDataView.setActivityInventoryType(activityProduct.getStockLevelType().getCode());
				populateActivityVendorAgentDetails(activityEntry, asmActivitiesDataView, activityProduct);
			}
			activitiesDataViews.add(asmActivitiesDataView);
		});

		asmOrderViewData.setActivities(activitiesDataViews);
	}

	private void populateActivityVendorAgentDetails(final AbstractOrderEntryModel activityEntry,
			final AsmActivitiesDataView asmActivitiesDataView, final ActivityProductModel activityProduct)
	{
		final Optional<VendorModel> refObj = activityProduct.getVendors().stream().findFirst();
		if (refObj.isPresent())
		{
			final VendorModel supplier = refObj.get();
			asmActivitiesDataView.setSupplierName(supplier.getName());
			final AddressModel supplierAddress = supplier.getAddress();
			if (Objects.nonNull(supplierAddress))
			{
				asmActivitiesDataView.setSupplierPhone(supplierAddress.getPhone1());
			}
			asmActivitiesDataView.setSupplierEmail(activityProduct.getActivityProviderEmail());
			asmActivitiesDataView.setSupplierName(supplier.getName());
			if (Objects.nonNull(activityEntry.getComments()))
			{
				final Optional<CommentModel> commentModel = activityEntry.getComments().stream().findFirst();
				if (commentModel.isPresent())
				{
					final CommentModel model = commentModel.get();
					asmActivitiesDataView.setAgentComment(model.getCode());
					asmActivitiesDataView.setAgentCommentDate(model.getCreationtime());
				}
			}

			asmActivitiesDataView.setAgentLogin(Objects.nonNull(activityEntry.getOrder().getAgent()) ?
					activityEntry.getOrder().getAgent().getName() :
					StringUtils.EMPTY);
		}
	}

	private void populateEntryHotelRoomDetails(final AsmOrderViewData asmOrderViewData,
			final List<AbstractOrderEntryModel> entryModels)
	{
		final List<AbstractOrderEntryModel> accommodationEntries = entryModels.stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType())).collect(Collectors.toList());

		final List<AsmHotelRoomDataView> bookingReferenceHotelRoomDetails = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{
			accommodationEntries.forEach(accommodationEntry ->
			{
				final AsmHotelRoomDataView asmHotelRoomDataView = new AsmHotelRoomDataView();

				final AbstractOrderEntryGroupModel entryGroupModel = accommodationEntry.getEntryGroup();
				if (Objects.nonNull(entryGroupModel) && (entryGroupModel instanceof DealOrderEntryGroupModel))
				{
					final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) entryGroupModel;
					final Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = dealOrderEntryGroupModel
							.getAccommodationEntryGroups();
					populateAccommodationEntryDetails(bookingReferenceHotelRoomDetails, accommodationEntry, asmHotelRoomDataView,
							accommodationOrderEntryGroupModels);
				}
			});
			asmOrderViewData.setBookingReferenceHotelRoomDetails(bookingReferenceHotelRoomDetails);
		}
	}

	private void populateAccommodationEntryDetails(final List<AsmHotelRoomDataView> bookingReferenceHotelRoomDetails,
			final AbstractOrderEntryModel accommodationEntry, final AsmHotelRoomDataView asmHotelRoomDataView,
			final Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels)
	{
		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = accommodationOrderEntryGroupModels
					.stream()
					.findFirst().get();
			if (Objects.nonNull(accommodationOrderEntryGroup.getAccommodationOffering()))
			{
				final String hotelName = accommodationOrderEntryGroup.getAccommodationOffering().getName();
				asmHotelRoomDataView.setHotelName(hotelName);

				populateAccommodationVendorDetails(asmHotelRoomDataView, accommodationOrderEntryGroup);
			}
			populateAccommodationRoomGuest(asmHotelRoomDataView, accommodationOrderEntryGroup);

			asmHotelRoomDataView.setPricePerNight(accommodationEntry.getBasePrice());

			populateAccommodationAgentComment(accommodationEntry, asmHotelRoomDataView);

			asmHotelRoomDataView.setAgentLogin(Objects.nonNull(accommodationEntry.getOrder().getAgent()) ?
					accommodationEntry.getOrder().getAgent().getName() :
					StringUtils.EMPTY);

			bookingReferenceHotelRoomDetails.add(asmHotelRoomDataView);
		}
	}

	private void populateAccommodationAgentComment(final AbstractOrderEntryModel accommodationEntry,
			final AsmHotelRoomDataView asmHotelRoomDataView)
	{
		if (Objects.nonNull(accommodationEntry.getComments()))
		{
			final Optional<CommentModel> commentModel = accommodationEntry.getComments().stream().findFirst();
			if (commentModel.isPresent())
			{
				final CommentModel model = commentModel.get();
				asmHotelRoomDataView.setAgentComment(model.getCode());
				asmHotelRoomDataView.setAgentCommentDate(model.getCreationtime());
			}
		}
	}

	private void populateAccommodationRoomGuest(final AsmHotelRoomDataView asmHotelRoomDataView,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup)
	{
		if (Objects.nonNull(accommodationOrderEntryGroup.getAccommodation()))
		{
			final AccommodationModel accommodationModel = accommodationOrderEntryGroup.getAccommodation();
			if (Objects.nonNull(accommodationModel.getRoomType()))
			{
				asmHotelRoomDataView.setRoomType(accommodationModel.getRoomType().getCode());
			}

			final List<GuestCountModel> guestCounts = accommodationOrderEntryGroup.getGuestCounts();
			if (CollectionUtils.isNotEmpty(guestCounts))
			{
				final ArrayList<AsmRoomGuestDataView> guestDataViews = new ArrayList<>();
				guestCounts.forEach(guestCount -> {
					final AsmRoomGuestDataView asmRoomGuestDataView = new AsmRoomGuestDataView();
					asmRoomGuestDataView.setGuestType(guestCount.getCode());
					asmRoomGuestDataView.setConfiguredRoomCost(0.0);
					guestDataViews.add(asmRoomGuestDataView);
				});
				asmHotelRoomDataView.setGuests(guestDataViews);
			}
		}
	}

	private void populateAccommodationVendorDetails(final AsmHotelRoomDataView asmHotelRoomDataView,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup)
	{
		final VendorModel vendor = accommodationOrderEntryGroup.getAccommodationOffering().getVendor();
		if (Objects.nonNull(vendor))
		{
			asmHotelRoomDataView.setSupplierName(vendor.getName());
			if (Objects.nonNull(vendor.getAddress()))
			{
				asmHotelRoomDataView.setSupplierPhone(vendor.getAddress().getPhone1());
			}
			if (CollectionUtils.isNotEmpty(vendor.getEmails()))
			{
				final Optional<String> email = vendor.getEmails().stream().findFirst();
				if (email.isPresent())
				{
					final String vendorEmail = email.get();
					asmHotelRoomDataView.setSupplierEmail(vendorEmail);
				}
			}
		}
	}

	private void populateEntryPassengerDetails(final AsmOrderViewData asmOrderViewData,
			final Map<String, Map<OrderEntryType, List<AbstractOrderEntryModel>>> orderEntriesGroupByBookingRefEntryType)
	{
		final Map<String, List<AsmEntryPassengerDataView>> bookingReferencePassengerDetails = new HashMap<>();
		orderEntriesGroupByBookingRefEntryType.entrySet().forEach(entry -> {
			final List<AbstractOrderEntryModel> transportEntries = entry.getValue().get(OrderEntryType.TRANSPORT);
			if (CollectionUtils.isNotEmpty(transportEntries))
			{
				final List<AsmEntryPassengerDataView> asmEntryPassengerDataViewList = new ArrayList<>();
				populateTravellerDetails(transportEntries, asmEntryPassengerDataViewList);
				bookingReferencePassengerDetails.put(entry.getKey(), asmEntryPassengerDataViewList);
			}
		});
		asmOrderViewData.setBookingReferencePassengerDetails(bookingReferencePassengerDetails);
	}

	private void populateTravellerDetails(final List<AbstractOrderEntryModel> transportEntries,
			final List<AsmEntryPassengerDataView> asmEntryPassengerDataViewList)
	{
		for (final AbstractOrderEntryModel entryModel : transportEntries)
		{
			if (Objects.nonNull(entryModel.getTravelOrderEntryInfo()) && CollectionUtils
					.isNotEmpty(entryModel.getTravelOrderEntryInfo().getTravellers()))
			{
				populatePassengerDetails(asmEntryPassengerDataViewList, entryModel);
			}
		}
	}

	private void populatePassengerDetails(final List<AsmEntryPassengerDataView> asmEntryPassengerDataViewList,
			final AbstractOrderEntryModel entryModel)
	{
		final Optional<TravellerModel> travellerModel = entryModel.getTravelOrderEntryInfo().getTravellers().stream()
				.findFirst();
		if (travellerModel.isPresent())
		{
			final TravellerModel existingTravellerModel = travellerModel.get();
			if (existingTravellerModel.getInfo() instanceof PassengerInformationModel)
			{
				final PassengerInformationModel passengerInformationModel = (PassengerInformationModel) existingTravellerModel
						.getInfo();
				final AsmEntryPassengerDataView asmEntryPassengerDataView = new AsmEntryPassengerDataView();
				if (Objects.nonNull(passengerInformationModel.getPassengerType()) && Objects
						.nonNull(passengerInformationModel.getPassengerType().getCode()))
				{
					asmEntryPassengerDataView.setPassengerType(passengerInformationModel.getPassengerType().getCode());
				}
				asmEntryPassengerDataView.setPassengerName(passengerInformationModel.getFirstName());
				asmEntryPassengerDataView.setPassengerGenderCode(passengerInformationModel.getGender());
				asmEntryPassengerDataView.setPassengerEmail(passengerInformationModel.getEmail());
				asmEntryPassengerDataView.setPassengerPhoneNo(passengerInformationModel.getPhoneNumber());
				asmEntryPassengerDataView.setPassengerCostToBCF(entryModel.getCostToBCF());
				asmEntryPassengerDataViewList.add(asmEntryPassengerDataView);
			}
		}
	}

	private void populateEntryTransportDetails(final AsmOrderViewData asmOrderViewData,
			final Map<String, Map<OrderEntryType, List<AbstractOrderEntryModel>>> orderEntriesGroupByBookingRefEntryType)
	{
		final Map<String, AsmTransportEntryDataView> bookingReferenceTransportDetails = new HashMap<>();
		orderEntriesGroupByBookingRefEntryType.entrySet().forEach(entry -> {
			final List<AbstractOrderEntryModel> transportEntries = entry.getValue().get(OrderEntryType.TRANSPORT);
			if (CollectionUtils.isNotEmpty(transportEntries))
			{
				populateTransportEntry(bookingReferenceTransportDetails, entry, transportEntries);
			}
		});
		asmOrderViewData.setBookingReferenceSailingDetails(bookingReferenceTransportDetails);
	}

	private void populateTransportEntry(final Map<String, AsmTransportEntryDataView> bookingReferenceTransportDetails,
			final Map.Entry<String, Map<OrderEntryType, List<AbstractOrderEntryModel>>> entry,
			final List<AbstractOrderEntryModel> transportEntries)
	{
		final Optional<AbstractOrderEntryModel> entryModel = transportEntries.stream()
				.filter(transportEntry -> Objects.nonNull(transportEntry.getTravelOrderEntryInfo()))
				.filter(transportEntry -> CollectionUtils
						.isNotEmpty(transportEntry.getTravelOrderEntryInfo().getTravellers()))
				.filter(
						transportEntry -> (transportEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get()
								.getInfo() instanceof BCFVehicleInformationModel))
				.findFirst();
		if (entryModel.isPresent())
		{
			final AbstractOrderEntryModel transportEntry = entryModel.get();
			final AsmTransportEntryDataView asmTransportEntryDataView = new AsmTransportEntryDataView();
			asmTransportEntryDataView.setEBookingReference(transportEntry.getBookingReference());
			populateVehicleDetails(transportEntry, asmTransportEntryDataView);
			bookingReferenceTransportDetails.put(entry.getKey(), asmTransportEntryDataView);
		}
	}

	private void populateVehicleDetails(final AbstractOrderEntryModel transportEntry,
			final AsmTransportEntryDataView asmTransportEntryDataView)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfoModel = transportEntry.getTravelOrderEntryInfo();
		if (Objects.nonNull(travelOrderEntryInfoModel))
		{
			asmTransportEntryDataView.setOpenTicket(travelOrderEntryInfoModel.isOpenTicket());

			final BCFVehicleInformationModel vehicleInformationModel = (BCFVehicleInformationModel) travelOrderEntryInfoModel
					.getTravellers().stream().findFirst().get().getInfo();
			asmTransportEntryDataView.setDangerousGoods(vehicleInformationModel.getCarryingDangerousGoods());
			asmTransportEntryDataView.setLiveStock(vehicleInformationModel.getCarryingLivestock());
			asmTransportEntryDataView
					.setSpecialInstructions(Objects.nonNull(travelOrderEntryInfoModel.getSpecialRequestDetail()) ?
							travelOrderEntryInfoModel.getSpecialRequestDetail().getDescription() :
							StringUtils.EMPTY);
			asmTransportEntryDataView.setVehicleLicensePlateNumber(vehicleInformationModel.getLicensePlateNumber());
			asmTransportEntryDataView.setVehicleLicensePlateCountry(vehicleInformationModel.getLicensePlateCountry());
			asmTransportEntryDataView.setVehicleLicensePlateState(vehicleInformationModel.getLicensePlateProvince());
			asmTransportEntryDataView.setVehicleCostToBCF(transportEntry.getCostToBCF());
		}
	}

	private void populateEntryProductDetails(final AsmOrderViewData asmOrderViewData,
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesGroupByBookingRef)
	{
		final Map<String, List<AsmEntryProductDataView>> bookingReferenceDetails = new HashMap<>();
		orderEntriesGroupByBookingRef.entrySet().forEach(entry -> {

			final List<AsmEntryProductDataView> entryProducts = populateProductDetails(entry);

			bookingReferenceDetails.put(entry.getKey(), entryProducts);
		});

		asmOrderViewData.setBookingReferenceProductDetails(bookingReferenceDetails);
	}

	private List<AsmEntryProductDataView> populateProductDetails(final Map.Entry<String, List<AbstractOrderEntryModel>> entry)
	{
		final List<AbstractOrderEntryModel> entries = entry.getValue();
		final List<AsmEntryProductDataView> entryProducts = new ArrayList<>();
		for (final AbstractOrderEntryModel entryModel : entries)
		{
			final AsmEntryProductDataView asmEntryProductDataView = new AsmEntryProductDataView();
			final ProductModel productModel = entryModel.getProduct();
			asmEntryProductDataView.setProductCode(productModel.getCode());
			asmEntryProductDataView.setProductCostToBCFV(entryModel.getCostToBCF());
			asmEntryProductDataView.setBcfvMarginRate(entryModel.getMargin());

			final List<DiscountValue> discountValues = entryModel.getDiscountValues();
			if (CollectionUtils.isNotEmpty(discountValues))
			{
				final Double productPromotionDiscounts =
						discountValues.stream().mapToDouble(discount -> discount.getValue()).sum();
				asmEntryProductDataView.setProductPromotionDiscountAmount(productPromotionDiscounts);
			}

			asmEntryProductDataView.setProductPriceToCustomerAmount(entryModel.getTotalPrice());

			if (Objects.nonNull(entryModel.getTaxValues()) && !entryModel.getTaxValues().isEmpty())
			{
				final TaxValue gstTaxValue = entryModel.getTaxValues().stream()
						.filter(taxValue -> BcfCoreConstants.GST.equals(taxValue.getCode())).findFirst().orElse(null);
				if (Objects.nonNull(gstTaxValue))
				{
					asmEntryProductDataView.setProductGSTToCustomerAmount(gstTaxValue.getValue());
				}
			}
			entryProducts.add(asmEntryProductDataView);
		}
		return entryProducts;
	}

	private List<AsmVacationPacakageAppliedPromotionsDataView> getOrderPromotionsData(final AbstractOrderModel abstractOrderModel)
	{
		final Set<PromotionResultModel> allPromotions = abstractOrderModel.getAllPromotionResults();
		final List<AsmVacationPacakageAppliedPromotionsDataView> appliedPromotionsDataViews = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(allPromotions))
		{
			final List<PromotionResultModel> appliedOrderPromotions = allPromotions.stream()
					.filter(promotionResultModel -> CollectionUtils.isNotEmpty(promotionResultModel.getConsumedEntries()))
					.collect(Collectors.toList());

			for (final PromotionResultModel promotionResultModel : appliedOrderPromotions)
			{
				final AsmVacationPacakageAppliedPromotionsDataView appliedPromotionsDataView = new AsmVacationPacakageAppliedPromotionsDataView();
				appliedPromotionsDataView.setPromotionsName(promotionResultModel.getPromotion().getName());
				final Double promotionAmount = promotionResultModel.getConsumedEntries().stream()
						.mapToDouble(PromotionOrderEntryConsumedModel::getAdjustedUnitPrice).sum();
				appliedPromotionsDataView.setDiscountsAmount(promotionAmount);
				appliedPromotionsDataViews.add(appliedPromotionsDataView);
			}
		}
		return appliedPromotionsDataViews;
	}

	private Boolean isVacationOrder(final AbstractOrderModel abstractOrderModel)
	{
		final BookingJourneyType orderBookingJourneyType = abstractOrderModel.getBookingJourneyType();
		if (BookingJourneyType.BOOKING_ALACARTE.equals(orderBookingJourneyType) || BookingJourneyType.BOOKING_PACKAGE
				.equals(orderBookingJourneyType) || BookingJourneyType.BOOKING_ACTIVITY_ONLY.equals(orderBookingJourneyType))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private String getTravellerName(final List<AbstractOrderEntryModel> entryModels)
	{
		final Optional<AbstractOrderEntryModel> optionalOrderEntryModel = entryModels.stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType())).findFirst();

		if (optionalOrderEntryModel.isPresent())
		{
			final AbstractOrderEntryModel orderEntryModel = optionalOrderEntryModel.get();

			getEntryTravellerName(orderEntryModel);
		}
		return StringUtils.EMPTY;
	}

	private String getEntryTravellerName(final AbstractOrderEntryModel orderEntryModel)
	{
		if (Objects.nonNull(orderEntryModel) && Objects.nonNull(orderEntryModel.getEntryGroup()) && (orderEntryModel
				.getEntryGroup() instanceof DealOrderEntryGroupModel))
		{
			final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) orderEntryModel
					.getEntryGroup();

			final Optional<AccommodationOrderEntryGroupModel> refObj = dealOrderEntryGroupModel
					.getAccommodationEntryGroups().stream().findFirst();
			if (refObj.isPresent())
			{
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = refObj.get();
				return accommodationOrderEntryGroupModel.getContactName();
			}
		}
		return StringUtils.EMPTY;
	}

	private List<AsmGoodWillDataView> getGoodWillData(final AbstractOrderModel abstractOrderModel)
	{
		final List<AsmGoodWillDataView> goodWillDataList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getGoodwillRefundOrderInfos()))
		{
			for (final GoodWillRefundOrderInfoModel goodwillRefundOrderInfo : abstractOrderModel.getGoodwillRefundOrderInfos())
			{
				final AsmGoodWillDataView goodWillData = new AsmGoodWillDataView();
				goodWillData.setGoodwillCode(goodwillRefundOrderInfo.getReasonCode());
				goodWillData.setGoodwillTransactionAmount(goodwillRefundOrderInfo.getAmount());
				goodWillData.setGoodwillTxDate(goodwillRefundOrderInfo.getCreationtime());
				if (Objects.nonNull(goodwillRefundOrderInfo.getAgent()))
				{
					goodWillData.setGoodwillAgentName(goodwillRefundOrderInfo.getAgent().getName());
				}
				goodWillDataList.add(goodWillData);
			}
		}
		return goodWillDataList;
	}

	private Date getDepositPaidDate(final AbstractOrderModel abstractOrderModel)
	{
		for (final PaymentTransactionModel paymentTransactionModel : abstractOrderModel.getPaymentTransactions())
		{
			if (!CollectionUtils.isEmpty(paymentTransactionModel.getEntries()))
			{
				final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransactionModel.getEntries()
						.stream()
						.filter(paymentTransactionEntry -> paymentTransactionEntry.isDepositPayment()).collect(
								Collectors.toList());
				if (CollectionUtils.isNotEmpty(paymentTransactionEntries))
				{
					return paymentTransactionEntries.get(0).getCreationtime();
				}
			}
		}
		return null;
	}
}
