/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.CRMSubscriptionMasterDetailsData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;


public class BcfSubscriptionMasterDetailsPopulator
		implements Populator<CRMSubscriptionMasterDetailModel, CRMSubscriptionMasterDetailsData>
{
	private Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter;

	@Override
	public void populate(final CRMSubscriptionMasterDetailModel source,
			final CRMSubscriptionMasterDetailsData target) throws ConversionException
	{
		target.setCode(source.getSubscriptionCode());
		target.setName(source.getName());
		target.setGroup(source.getSubscriptionGroup().getCode());
		target.setType(source.getSubscriptionType().getCode());
		if (CollectionUtils.isNotEmpty(source.getRoutes()))
		{
			target.setRoutes(currentConditionsTravelRouteConverter.convertAll(source.getRoutes()));
		}
	}

	@Required
	public void setCurrentConditionsTravelRouteConverter(
			final Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter)
	{
		this.currentConditionsTravelRouteConverter = currentConditionsTravelRouteConverter;
	}
}
