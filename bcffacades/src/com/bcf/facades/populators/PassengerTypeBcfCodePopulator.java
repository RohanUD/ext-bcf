/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;


public class PassengerTypeBcfCodePopulator implements Populator<PassengerTypeModel, PassengerTypeData>
{
	@Override
	public void populate(final PassengerTypeModel source, final PassengerTypeData target) throws ConversionException
	{
		target.setBcfCode(source.getEBookingCode());
		if (source.getInfoRequired() != null)
		{
			target.setInfoRequired(source.getInfoRequired());
		}
	}
}
