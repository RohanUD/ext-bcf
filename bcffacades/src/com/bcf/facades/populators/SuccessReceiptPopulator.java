/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.receipt.ReceiptData;


public class SuccessReceiptPopulator implements Populator<PaymentTransactionEntryModel, ReceiptData>
{
	private ConfigurationService configurationService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final PaymentTransactionEntryModel txnEntry, final ReceiptData receiptData)
			throws ConversionException
	{
		if (Objects.nonNull(txnEntry))
		{
			receiptData.setBusinessAddressLine1(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_1));
			receiptData.setBusinessAddressLine2(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_2));
			receiptData.setBusinessAddressLine3(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_ADDRESS_LINE_3));
			receiptData.setBusinessContactNumber(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_CONTACT_NUMBER));
			receiptData.setBusinessContactEmail(getConfigurationService().getConfiguration()
					.getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_CONTACT_EMAIL));
			receiptData.setBusinessGSTNumber(
					getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.BUSINESS_GST_NUMBER));
			receiptData.setTransactionAmount(getTravelCommercePriceFacade()
					.createPriceData(txnEntry.getAmount().doubleValue(),
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			if (StringUtils.equalsIgnoreCase(BcfFacadesConstants.PaymentReceipt.LANG_FRENCH, txnEntry.getCardHolderLanguage()))
			{
				receiptData.setReceiptRetainInfo(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.RETAIN_INFO_FR));
				receiptData.setReceiptCardHolderCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.CARD_HOLDER_COPY_FR));
				receiptData.setReceiptMerchantCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_COPY_FR));
				receiptData.setReceiptMerchantMessage(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_MESSAGE_FR));
			}
			else
			{
				receiptData.setReceiptRetainInfo(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.RETAIN_INFO_EN));
				receiptData.setReceiptCardHolderCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.CARD_HOLDER_COPY_EN));
				receiptData.setReceiptMerchantCopy(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_COPY_EN));
				receiptData.setReceiptMerchantMessage(
						getConfigurationService().getConfiguration().getString(BcfFacadesConstants.PaymentReceipt.MERCHANT_MESSAGE_EN));
			}
			receiptData.setTransactionType(txnEntry.getTransactionType() != null ? txnEntry.getTransactionType().getCode() : "");
			receiptData.setTransactionTimestamp(txnEntry.getTime());
			receiptData.setCardType(txnEntry.getCardType());
			receiptData.setCardNumberToPrint(txnEntry.getCardNumberToPrint());
			receiptData.setApprovalNumber(txnEntry.getApprovalNumber());
			receiptData.setServiceProviderPaymentTerminalId(txnEntry.getServiceProviderPaymentTerminalId());
			receiptData.setTransactionRefNumber(txnEntry.getTransactionRefNumber());
			receiptData.setCardEntryMethod(txnEntry.getCardEntryMethod());
			receiptData.setEmvApplicationPreferredName(txnEntry.getEmvApplicationLabel());
			receiptData.setEmvAid(txnEntry.getEmvAid());
			receiptData.setEmvTvr(txnEntry.getEmvTvr());
			receiptData.setEmvTsi(txnEntry.getEmvTsi());
			receiptData.setEmvPinEntryMessage(txnEntry.getEmvPinEntryMessage());
			receiptData.setApprovalOrDeclineMessage(txnEntry.getApprovalOrDeclineMessage());
			receiptData.setSignatureRequired(txnEntry.isSignatureRequiredFlag());
			receiptData.setDebitAccountType(txnEntry.getDebitAccountType());
		}
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
