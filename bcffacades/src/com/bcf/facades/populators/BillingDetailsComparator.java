/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import java.util.Comparator;
import java.util.Date;
import javax.annotation.Nonnull;


/**
 * Comparator for sorting BillingTime based on the sequence.
 */
public class BillingDetailsComparator implements Comparator<ReservationData>
{
	@Override
	public int compare(@Nonnull final ReservationData reservationData, @Nonnull final ReservationData reservationData1)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("reservationData", reservationData);
		ServicesUtil.validateParameterNotNullStandardMessage("reservationData1", reservationData1);


		final Date departureTime = reservationData.getReservationItems().get(0).getReservationItinerary()
				.getOriginDestinationOptions().get(0).getTransportOfferings().get(0).getDepartureTime();

		final Date departureTime1 = reservationData1.getReservationItems().get(0).getReservationItinerary()
				.getOriginDestinationOptions().get(0).getTransportOfferings().get(0).getDepartureTime();

		return departureTime.compareTo(departureTime1);
	}
}
