/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.vacation.TopAttractionModel;
import com.bcf.facades.vacations.TopAttractionData;


public class TopAttractionPopulator implements Populator<TopAttractionModel, TopAttractionData>
{
	private CommerceCommonI18NService commerceCommonI18NService;
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final TopAttractionModel source, final TopAttractionData target) throws ConversionException
	{
		target.setCode(source.getCode());
		target.setTitle(source.getTitle(getCommerceCommonI18NService().getCurrentLocale()));
		target.setDescription(source.getDescription(getCommerceCommonI18NService().getCurrentLocale()));
		target.setImages(getImageConverter().convertAll(source.getImages()));
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}
}
