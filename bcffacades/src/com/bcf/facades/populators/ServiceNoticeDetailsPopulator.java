/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import com.bcf.core.model.ServiceNoticeModel;


public class ServiceNoticeDetailsPopulator implements Populator<ServiceNoticeModel, ServiceNoticeData>
{
	@Override
	public void populate(final ServiceNoticeModel source, final ServiceNoticeData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(source, "source must not be null");
		ServicesUtil.validateParameterNotNull(target, "target must not be null");

		target.setTitle(source.getTitle());
		target.setPublishedDate(source.getPublishDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setBodyContent(source.getBodyContent());
	}
}
