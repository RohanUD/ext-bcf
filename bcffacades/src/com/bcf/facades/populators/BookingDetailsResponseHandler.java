/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.FeeData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.OriginDestinationOptionData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerFareData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TransportVehicleData;
import de.hybris.platform.commercefacades.travel.TransportVehicleInfoData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelSectorData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationPricingInfoData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.util.TransportOfferingUtils;
import de.hybris.platform.travelservices.dao.TransportFacilityDao;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BCFPassengerTypeDao;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.order.dao.SailingReturnPairMappingDao;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.core.vehicletype.dao.VehicleTypeDao;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.bcffacades.BcfTermsAndConditionsFacade;
import com.bcf.facades.booking.action.strategies.impl.BCFOriginDestinationLevelActionStrategy;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.common.data.FareDetail;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.common.data.Tax;
import com.bcf.integration.common.data.VehicleInfo;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.search.booking.response.Itinerary;


public class BookingDetailsResponseHandler
{
	private static final Logger LOG = Logger.getLogger(BookingDetailsResponseHandler.class);

	private static final String SEARCH_PERSONAL_BOOKING_PAGE_SIZE = "search.personal.bookings.display.page.size";
	private static final String SEARCH_BUSINESS_BOOKING_PAGE_SIZE = "search.business.bookings.display.page.size";
	private static final String BOOKING_STATUS_CANCELLED = "Cancelled";
	private static final String BOOKING_STATUS_BOOKED = "Booked";
	private static final String BOOKING_STATUS_MODIFIED = "Modified";
	private static final String BOOKING_STATUS_OPTION = "Option";
	private static final String BOOKING_STATUS_CONFIRMED = "Confirmed";
	private static final String BOOKING_STATUS_STANDBY = "StandBy";
	private static final String TEXT_TAX_PST = "text.payment.reservation.transport.fare.breakup.tax.pst";
	private static final String TEXT_TAX_GST = "text.payment.reservation.transport.fare.breakup.tax.gst";

	private TransportFacilityDao transportFacilityDao;
	private BCFOriginDestinationLevelActionStrategy bookingActionStrategy;
	private PriceDataFactory priceDataFactory;
	private CommonI18NService commonI18NService;
	private BCFPassengerTypeDao bcfPassengerTypeDao;
	private VehicleTypeDao vehicleTypeDao;
	private SailingReturnPairMappingDao sailingReturnPairMappingDao;
	private BcfTermsAndConditionsFacade bcfTermsAndConditionsFacade;
	private ConfigurationService configurationService;
	private BCFTravelRouteFacade travelRouteFacade;
	private BcfTransportVehicleService bcfTransportVehicleService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private AncillaryProductFacade ancillaryProductFacade;
	private BcfOrderService orderService;
	private BcfProductFacade bcfProductFacade;
	private UserService userService;

	public ReservationDataList createReservationDataList(final SearchBookingResponseDTO response)
	{

		final Map<String, Itinerary> itineraryByBookingReferenceMap = StreamUtil.safeStream(response.getSearchResult())
				.collect(Collectors.toMap(itinerary -> itinerary.getBooking().getBookingReference().getBookingReference(),
						itinerary -> itinerary, (s1, s2) -> s1));
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<String> oneWayReferences = new ArrayList<>();
		final List<String> outboundReferences = new ArrayList<>();
		final List<String> inboundReferences = new ArrayList<>();
		itineraryByBookingReferenceMap.entrySet().stream().forEach(entry -> {

			final AbstractOrderModel bookingForItinerary = getBookingForItinerary(entry.getValue());

			if (!isVacationOrAlacarteBooking(bookingForItinerary) && !OptionBookingUtil.isOptionBooking(bookingForItinerary))
			{
				final String inboundReference = getInboundReference(bookingForItinerary, entry.getValue());
				if (StringUtils.isNotBlank(inboundReference) && Objects.nonNull(itineraryByBookingReferenceMap.get(inboundReference)))
				{
					outboundReferences.add(entry.getKey());
					inboundReferences.add(inboundReference);
				}
				oneWayReferences.add(entry.getKey());
			}
		});
		oneWayReferences.removeAll(outboundReferences);
		oneWayReferences.removeAll(inboundReferences);
		final List<ReservationData> reservationDatas = new ArrayList<>();
		IntStream.range(0, outboundReferences.size()).forEach(index -> {
			final Itinerary outbound = itineraryByBookingReferenceMap.get(outboundReferences.get(index));
			final Itinerary inbound = itineraryByBookingReferenceMap.get(inboundReferences.get(index));
			reservationDatas.add(createReservationData(outbound, inbound));
		});
		IntStream.range(0, oneWayReferences.size()).forEach(index -> {
			final Itinerary singleJourney = itineraryByBookingReferenceMap.get(oneWayReferences.get(index));
			reservationDatas.add(createReservationData(singleJourney, null));
		});

		Collections.sort(reservationDatas, new BillingDetailsComparator());

		reservationDataList.setReservationDatas(reservationDatas);
		updatePaginationData(reservationDataList);
		return reservationDataList;
	}

	public ReservationDataList createReservationDataListForVacations(final SearchBookingResponseDTO response)
	{
		final Map<String, Itinerary> itineraryByBookingReferenceMap = StreamUtil.safeStream(response.getSearchResult())
				.collect(Collectors.toMap(itinerary -> itinerary.getBooking().getBookingReference().getBookingReference(),
						itinerary -> itinerary, (s1, s2) -> s1));
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<String> oneWayReferences = new ArrayList<>();
		final List<String> outboundReferences = new ArrayList<>();
		final List<String> inboundReferences = new ArrayList<>();
		itineraryByBookingReferenceMap.entrySet().stream().forEach(entry -> {

			final AbstractOrderModel bookingForItinerary = getBookingForItinerary(entry.getValue());


			final String inboundReference = getInboundReference(bookingForItinerary, entry.getValue());
			if (StringUtils.isNotBlank(inboundReference) && Objects.nonNull(itineraryByBookingReferenceMap.get(inboundReference)))
			{
				outboundReferences.add(entry.getKey());
				inboundReferences.add(inboundReference);
			}
			oneWayReferences.add(entry.getKey());

		});
		oneWayReferences.removeAll(outboundReferences);
		oneWayReferences.removeAll(inboundReferences);
		final List<ReservationData> reservationDatas = new ArrayList<>();
		IntStream.range(0, outboundReferences.size()).forEach(index -> {
			final Itinerary outbound = itineraryByBookingReferenceMap.get(outboundReferences.get(index));
			final Itinerary inbound = itineraryByBookingReferenceMap.get(inboundReferences.get(index));
			reservationDatas.add(createReservationData(outbound, inbound));
		});
		IntStream.range(0, oneWayReferences.size()).forEach(index -> {
			final Itinerary singleJourney = itineraryByBookingReferenceMap.get(oneWayReferences.get(index));
			reservationDatas.add(createReservationData(singleJourney, null));
		});

		Collections.sort(reservationDatas, new BillingDetailsComparator());

		reservationDataList.setReservationDatas(reservationDatas);
		updatePaginationData(reservationDataList);
		return reservationDataList;
	}

	private boolean isVacationOrAlacarteBooking(final AbstractOrderModel order)
	{
		if (Objects.nonNull(order))
		{
			return BookingJourneyType.BOOKING_PACKAGE.equals(order.getBookingJourneyType()) || BookingJourneyType.BOOKING_ALACARTE
					.equals(order.getBookingJourneyType());
		}
		return false;
	}

	private void updatePaginationData(final ReservationDataList reservationDataList)
	{
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			reservationDataList.setPageSize(
					Integer.parseInt(getConfigurationService().getConfiguration().getString(SEARCH_BUSINESS_BOOKING_PAGE_SIZE)));
		}
		else
		{
			reservationDataList.setPageSize(
					Integer.parseInt(getConfigurationService().getConfiguration().getString(SEARCH_PERSONAL_BOOKING_PAGE_SIZE)));
		}
	}

	private AbstractOrderModel getBookingForItinerary(final Itinerary itinerary)
	{
		AbstractOrderModel order = null;
		final String bookingReference = itinerary.getBooking().getBookingReference().getBookingReference();
		if (StringUtils.isNotBlank(bookingReference))
		{
			order = getOrderService().getOrderByEBookingCode(bookingReference);
		}
		return order;
	}

	private String getInboundReference(final AbstractOrderModel order, final Itinerary itinerary)
	{
		if (Objects.nonNull(order))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupedByJourney = StreamUtil.safeStream(order.getEntries())
					.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& Objects.nonNull(entry.getTravelOrderEntryInfo()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> entriesByJourney : entriesGroupedByJourney.entrySet())
			{
				if (StreamUtil.safeStream(entriesByJourney.getValue()).anyMatch(
						entry -> StringUtils.equals(entry.getBookingReference(),
								itinerary.getBooking().getBookingReference().getBookingReference())
								&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0))
				{
					final Optional<AbstractOrderEntryModel> optionalEntry = StreamUtil.safeStream(entriesByJourney.getValue())
							.filter(
									entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 1)
							.findFirst();
					if (optionalEntry.isPresent())
					{
						return optionalEntry.get().getBookingReference();
					}
				}
			}
		}
		return null;
	}

	private ReservationData createReservationData(final Itinerary outBoundItinerary, final Itinerary inBoundItinerary)
	{
		final ReservationData reservationData = new ReservationData();

		final List<ReservationItemData> reservationItemDataList = new ArrayList<>();
		reservationData.setCode(outBoundItinerary.getBooking().getBookingReference().getExternalBookingReference());
		reservationData.setBookingStatusCode("ACTIVE");//  - need to change
		reservationData.setBookingStatusName("Active");//  - need to change
		populateReservationItemDataList(outBoundItinerary, reservationItemDataList, 0);
		if (Objects.nonNull(inBoundItinerary))
		{
			populateReservationItemDataList(inBoundItinerary, reservationItemDataList, 1);
		}
		reservationData.setReservationItems(reservationItemDataList);
		return reservationData;
	}

	private void populateReservationItemDataList(final Itinerary itinerary,
			final List<ReservationItemData> reservationItemDataList, final int odNum)
	{
		final ReservationItemData reservationItem = new ReservationItemData();
		reservationItem.setReservationItinerary(createReservationItinerary(itinerary.getSailing().getLine(), odNum));
		reservationItem.setBcfBookingReference(itinerary.getBooking().getBookingReference().getBookingReference());
		reservationItem.setExternalBookingReference(itinerary.getBooking().getBookingReference().getExternalBookingReference());
		reservationItem.setReservationPricingInfo(createReservationPricingInfo(itinerary));
		if (StringUtils.equalsAnyIgnoreCase(BOOKING_STATUS_CANCELLED, itinerary.getBooking().getBookingStatus()))
		{
			reservationItem.setCancelAllowed(false);
			reservationItem.setModifyAllowed(false);
			reservationItem.setWaitlisted(false);
		}
		else
		{
			reservationItem.setCancelAllowed(itinerary.getBooking().getIsCancelAllowed());
			reservationItem.setModifyAllowed(itinerary.getBooking().getIsModifyAllowed());
			if (Objects.isNull(itinerary.getBooking().getIsWaitListed()))
			{
				reservationItem.setWaitlisted(false);
			}
			else
			{
				reservationItem.setWaitlisted(itinerary.getBooking().getIsWaitListed());
			}
		}
		reservationItem.setTermsAndConditionsCode(itinerary.getBooking().getTermsAndConditionsCode());
		reservationItem.setTermsAndConditionsData(getBcfTermsAndConditionsFacade()
				.getTermsAndConditionsForCode(itinerary.getBooking().getTermsAndConditionsCode()));
		updateBookingStatus(reservationItem, itinerary.getBooking().getBookingStatus());
		reservationItem.setUpcoming(new Date().before(TravelDateUtils
				.getDate(itinerary.getSailing().getLine().get(0).getLineInfo().getDepartureDateTime(),
						BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN)));
		reservationItem.setBookerName(getBookerName(itinerary));
		reservationItem.setRouteReference(itinerary.getBooking().getRouteReference());
		reservationItem.setPassengerCount((int) StreamUtil.safeStream(reservationItem.getReservationItinerary().getTravellers())
				.filter(travellerData -> StringUtils
						.equalsIgnoreCase(TravellerType.PASSENGER.getCode(), travellerData.getTravellerType())).count());
		final Optional<TravellerData> vehicle = StreamUtil.safeStream(reservationItem.getReservationItinerary().getTravellers())
				.filter(travellerData -> StringUtils
						.equalsIgnoreCase(TravellerType.VEHICLE.getCode(), travellerData.getTravellerType())).findFirst();
		if (vehicle.isPresent())
		{
			final int vehicleLength = (int) Math
					.round(((VehicleInformationData) vehicle.get().getTravellerInfo()).getLength() / 100d);
			if (vehicleLength > 0)
			{
				reservationItem.setVehicleLength(String.format("%s%s", vehicleLength, "'"));
			}
			reservationItem.setVehicleType(((VehicleInformationData) vehicle.get().getTravellerInfo()).getVehicleType().getCode());
		}
		reservationItemDataList.add(reservationItem);
	}

	private void updateBookingStatus(final ReservationItemData reservationItem, final String bookingStatus)
	{
		if (StringUtils.equalsAnyIgnoreCase(BOOKING_STATUS_CANCELLED, bookingStatus))
		{
			reservationItem.setBookingStatus(BOOKING_STATUS_CANCELLED);
		}
		else if (StringUtils.equalsAnyIgnoreCase(BOOKING_STATUS_BOOKED, bookingStatus) || StringUtils
				.equalsAnyIgnoreCase(BOOKING_STATUS_MODIFIED, bookingStatus))
		{
			if (reservationItem.isWaitlisted())
			{
				reservationItem.setBookingStatus(BOOKING_STATUS_STANDBY);
			}
			else
			{
				reservationItem.setBookingStatus(BOOKING_STATUS_CONFIRMED);
			}
		}
		else if (StringUtils.equalsAnyIgnoreCase(BOOKING_STATUS_OPTION, bookingStatus))
		{
			reservationItem.setBookingStatus(BOOKING_STATUS_STANDBY);
		}
	}

	private ReservationPricingInfoData createReservationPricingInfo(final Itinerary itinerary)
	{
		final ReservationPricingInfoData reservationPricingInfoData = new ReservationPricingInfoData();
		final String currencyIsoCode = getCommonI18NService().getCurrentCurrency().getIsocode();
		final TotalFareData totalFareData = new TotalFareData();
		final PriceData totalPrice;
		final PriceData paidPrice;
		final PriceData dueAtTerminalPrice;
		double totalFareInCents = 0;
		double paidFareInCents = 0;
		double dueAtTerminalFareInCents = 0;
		if (Objects.nonNull(itinerary.getBooking().getGrossAmountInCents()))
		{
			totalFareInCents = itinerary.getBooking().getGrossAmountInCents().longValue();
		}
		if (Objects.nonNull(itinerary.getBooking().getPaidFaresInCents()))
		{
			paidFareInCents = itinerary.getBooking().getPaidFaresInCents().longValue();
		}
		if (totalFareInCents > paidFareInCents && !StringUtils
				.equalsAnyIgnoreCase(BOOKING_STATUS_CANCELLED, itinerary.getBooking().getBookingStatus()))
		{
			dueAtTerminalFareInCents = totalFareInCents - paidFareInCents;
		}
		totalPrice = getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(totalFareInCents).divide(BigDecimal.valueOf(100)),
						getCommonI18NService().getCurrentCurrency());
		paidPrice = getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(paidFareInCents).divide(BigDecimal.valueOf(100)),
						getCommonI18NService().getCurrentCurrency());
		dueAtTerminalPrice = getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(dueAtTerminalFareInCents).divide(BigDecimal.valueOf(100)),
						getCommonI18NService().getCurrentCurrency());
		totalFareData.setTaxes(createTaxes(itinerary, currencyIsoCode));
		totalFareData.setFees(createFees(itinerary, currencyIsoCode));
		totalFareData.setTotalPrice(totalPrice);
		reservationPricingInfoData.setTotalFare(totalFareData);
		reservationPricingInfoData.setPaidFare(paidPrice);
		reservationPricingInfoData.setDueAtTerminal(dueAtTerminalPrice);
		reservationPricingInfoData.setItineraryPricingInfo(createItineraryPricingInfo(itinerary));
		return reservationPricingInfoData;
	}

	private List<TaxData> createTaxes(final Itinerary itinerary, final String currencyIsoCode)
	{
		final List<Tax> taxList = StreamUtil.safeStream(itinerary.getSailing().getLine()).map(SailingLine::getSailingPrices)
				.flatMap(sailingPrices -> StreamUtil.safeStream(sailingPrices)).map(SailingPrices::getProductFares)
				.flatMap(productFares -> StreamUtil.safeStream(productFares)).map(ProductFare::getFareDetail)
				.flatMap(fareDetails -> StreamUtil.safeStream(fareDetails)).map(FareDetail::getTaxes)
				.flatMap(taxes -> StreamUtil.safeStream(taxes))
				.collect(Collectors.toList());
		final List<Tax> gstTaxList = taxList.stream().filter(tax -> StringUtils.equals(BcfCoreConstants.GST, tax.getTaxCode()))
				.collect(Collectors.toList());
		final List<Tax> pstTaxList = taxList.stream().filter(tax -> StringUtils.equals(BcfCoreConstants.PST, tax.getTaxCode()))
				.collect(Collectors.toList());
		final List<TaxData> taxDataList = new ArrayList<>();
		taxDataList.add(createGstTotal(gstTaxList, currencyIsoCode));
		taxDataList.add(createPstTotal(pstTaxList, currencyIsoCode));
		return taxDataList;
	}

	private TaxData createGstTotal(final List<Tax> gstTaxList, final String currencyIsoCode)
	{
		final TaxData totalGST = new TaxData();
		totalGST.setPrice(getTravelCommercePriceFacade()
				.createPriceData(gstTaxList.stream().mapToLong(Tax::getAmountInCents).sum() / 100d, currencyIsoCode));
		totalGST.setCode(TEXT_TAX_GST);
		return totalGST;
	}

	private TaxData createPstTotal(final List<Tax> pstTaxList, final String currencyIsoCode)
	{
		final TaxData totalPST = new TaxData();
		totalPST.setPrice(getTravelCommercePriceFacade()
				.createPriceData(pstTaxList.stream().mapToLong(Tax::getAmountInCents).sum() / 100d, currencyIsoCode));
		totalPST.setCode(TEXT_TAX_PST);
		return totalPST;
	}

	private List<FeeData> createFees(final Itinerary itinerary, final String currencyIsoCode)
	{
		final List<FeeData> feeDataList = new ArrayList<>();
		final Map<String, List<FareDetail>> fareDetailsMap =
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getOtherCharges())
						.map(ProductFare::getFareDetail)
						.flatMap(fareDetails -> StreamUtil.safeStream(fareDetails)).collect(Collectors.toList()))
						.collect(Collectors.groupingBy(FareDetail::getFareCode));
		StreamUtil.safeStream(fareDetailsMap.entrySet()).forEach(stringListEntry -> {
			final ProductData feeProduct = getBcfProductFacade().getProductForCodeAndOptions(stringListEntry.getKey(), null);
			if (Objects.nonNull(feeProduct))
			{
				final FeeData feeData = new FeeData();
				feeData.setPrice(getTravelCommercePriceFacade()
						.createPriceData(
								StreamUtil.safeStream(stringListEntry.getValue()).mapToLong(FareDetail::getGrossAmountInCents).sum()
										/ 100d, currencyIsoCode));
				feeData.setName(feeProduct.getName());
				feeData.setQuantity(stringListEntry.getValue().size());
				feeDataList.add(feeData);
			}
		});
		return feeDataList;
	}

	private ItineraryPricingInfoData createItineraryPricingInfo(final Itinerary itinerary)
	{
		final ItineraryPricingInfoData itineraryPricingInfoData = new ItineraryPricingInfoData();

		itineraryPricingInfoData.setPtcFareBreakdownDatas(createPtcFareBreakdownList(itinerary));
		itineraryPricingInfoData.setVehicleFareBreakdownDatas(createVehicleFareBreakdownList(itinerary));
		itineraryPricingInfoData.setLargeItemsBreakdownDataList(createLargeItemsBreakdownList(itinerary));
		itineraryPricingInfoData.setBundleType(
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get().getSailingPrices())
						.findFirst().get().getTariffType());
		return itineraryPricingInfoData;
	}

	private List<LargeItemData> createLargeItemsBreakdownList(final Itinerary itinerary)
	{
		final Map<String, String> ancillaryProductNamesMap = getAncillaryProductNamesMap();
		final List<LargeItemData> largeItems = new ArrayList<>();
		StreamUtil.safeStream(
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get().getSailingPrices())
						.findFirst().get().getProductFares())
				.filter(productFare -> StringUtils
						.equals(BcfCoreConstants.CATEGORY_STOWAGE, productFare.getProduct().getProductCategory()))
				.forEach(productFare -> {
					final LargeItemData largeItemData = new LargeItemData();
					largeItemData.setCode(productFare.getProduct().getProductId());
					largeItemData.setName(ancillaryProductNamesMap.get(productFare.getProduct().getProductId()));
					largeItemData.setQuantity(1);
					largeItemData.setPrice(getPriceDataFactory().create(PriceDataType.BUY,
							new BigDecimal(productFare.getFareDetail().get(0).getGrossAmountInCents()).divide(BigDecimal.valueOf(100)),
							getCommonI18NService().getCurrentCurrency()));
					largeItems.add(largeItemData);
				});
		final List<LargeItemData> largeItemDataList = new ArrayList<>();
		StreamUtil.safeStream(largeItems).collect(Collectors.groupingBy(largeItemData -> largeItemData.getCode())).entrySet()
				.forEach(entry -> {
					final LargeItemData largeItemData = entry.getValue().get(0);
					largeItemData.setQuantity(entry.getValue().size());
					final double totalPrice = StreamUtil.safeStream(entry.getValue())
							.mapToDouble(value -> value.getPrice().getValue().doubleValue()).sum();
					largeItemData.setPrice(getPriceDataFactory()
							.create(PriceDataType.BUY, BigDecimal.valueOf(totalPrice), getCommonI18NService().getCurrentCurrency()));
					largeItemDataList.add(largeItemData);
				});
		return largeItemDataList;
	}

	private List<VehicleFareBreakdownData> createVehicleFareBreakdownList(final Itinerary itinerary)
	{
		final List<VehicleFareBreakdownData> vehicleFareBreakdownDatas = new ArrayList<>();
		StreamUtil.safeStream(
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get().getSailingPrices())
						.findFirst().get().getProductFares())
				.filter(productFare -> TravellerType.VEHICLE.getCode().equals(productFare.getProduct().getProductCategory()))
				.forEach(productFare -> vehicleFareBreakdownDatas.add(createVehicleFareBreakdownData(productFare)));
		return vehicleFareBreakdownDatas;
	}

	private VehicleFareBreakdownData createVehicleFareBreakdownData(final ProductFare productFare)
	{
		final VehicleFareBreakdownData vehicleFareBreakdown = new VehicleFareBreakdownData();
		vehicleFareBreakdown.setVehicleTypeQuantity(createVehicleTypeQuantity(productFare));
		vehicleFareBreakdown.setVehicleFare(createProductFare(Collections.singletonList(productFare)));
		return vehicleFareBreakdown;
	}

	private VehicleTypeQuantityData createVehicleTypeQuantity(final ProductFare productFare)
	{
		final VehicleTypeModel vehicle = getVehicleTypeDao().getVehicleByCode(productFare.getProduct().getProductId());
		if (Objects.nonNull(vehicle))
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			vehicleTypeQuantityData.setQty((int) productFare.getProduct().getProductNumber());
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			vehicleTypeData.setName(vehicle.getName());
			vehicleTypeQuantityData.setVehicleType(vehicleTypeData);
			return vehicleTypeQuantityData;
		}
		LOG.error(String.format("Vehicle with code [%s] not present in hybris", productFare.getProduct().getProductId()));
		return null;
	}

	private List<PTCFareBreakdownData> createPtcFareBreakdownList(final Itinerary itinerary)
	{
		final List<PTCFareBreakdownData> ptcFareBreakdownDatas = new ArrayList<>();
		final Map<String, List<ProductFare>> paxGroupedById = StreamUtil
				.safeStream(StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get()
						.getSailingPrices()).findFirst().get().getProductFares())
				.filter(productFare -> TravellerType.PASSENGER.getCode().equals(productFare.getProduct().getProductCategory()))
				.collect(Collectors.groupingBy(productFare -> productFare.getProduct().getProductId()));
		paxGroupedById.entrySet().stream().forEach(stringListEntry ->
				ptcFareBreakdownDatas.add(createPTCFareBreakdown(stringListEntry.getValue()))
		);
		return ptcFareBreakdownDatas;
	}

	private PTCFareBreakdownData createPTCFareBreakdown(final List<ProductFare> productFares)
	{
		final PTCFareBreakdownData ptcFareBreakdownData = new PTCFareBreakdownData();
		ptcFareBreakdownData.setPassengerFare(createProductFare(productFares));
		ptcFareBreakdownData.setPassengerTypeQuantity(createPassengerTypeQuantity(productFares));

		return ptcFareBreakdownData;
	}

	private PassengerFareData createProductFare(final List<ProductFare> productFares)
	{
		final PassengerFareData passengerFareData = new PassengerFareData();
		final long paxFaresInCents = productFares.stream()
				.mapToLong(productFare -> productFare.getFareDetail().stream().findFirst().get().getGrossAmountInCents()).sum();
		final PriceData baseFare = getPriceDataFactory().create(PriceDataType.BUY,
				new BigDecimal(paxFaresInCents).divide(BigDecimal.valueOf(100)),
				getCommonI18NService().getCurrentCurrency());
		passengerFareData.setBaseFare(baseFare);
		return passengerFareData;
	}

	private PassengerTypeQuantityData createPassengerTypeQuantity(final List<ProductFare> productFares)
	{
		final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
		final int quantity = productFares.stream().mapToInt(productFare -> (int) productFare.getProduct().getProductNumber()).sum();
		passengerTypeQuantityData.setQuantity(quantity);
		passengerTypeQuantityData.setPassengerType(createPassengerType(productFares.stream().findFirst().get()));
		return passengerTypeQuantityData;
	}

	private PassengerTypeData createPassengerType(final ProductFare productFare)
	{
		final PassengerTypeData passengerTypeData = new PassengerTypeData();
		passengerTypeData.setBcfCode(productFare.getProduct().getProductId());
		passengerTypeData.setName(
				getBcfPassengerTypeDao().findPassengerModelForEBookingCode(productFare.getProduct().getProductId()).getName());
		return passengerTypeData;
	}

	private ItineraryData createReservationItinerary(final List<SailingLine> sectorList, final int odRef)
	{
		final ItineraryData itineraryData = new ItineraryData();
		itineraryData.setRoute(createRoute(sectorList));
		itineraryData.setOriginDestinationOptions(createOriginDestinationOptions(sectorList, odRef));
		itineraryData.setTravellers(createTravellers(sectorList));
		Map<String, Integer> durationMap = null;
		final boolean anyOfferingWithNullCode = StreamUtil.safeStream(itineraryData.getOriginDestinationOptions()).findFirst().get()
				.getTransportOfferings().stream()
				.anyMatch(transportOfferingData -> StringUtils.isBlank(transportOfferingData.getCode()));
		if (!anyOfferingWithNullCode)
		{
			durationMap = TransportOfferingUtils.calculateJourneyDuration(
					itineraryData.getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings());
		}
		if (Objects.isNull(durationMap))
		{
			final int numberOfTransportOfferings = CollectionUtils
					.size(itineraryData.getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings());
			final Date arrivalTime = itineraryData.getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings()
					.get(numberOfTransportOfferings - 1).getArrivalTime();
			final Date departureTime = itineraryData.getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings()
					.get(0).getDepartureTime();
			durationMap = TransportOfferingUtils.getDurationMap(arrivalTime.getTime() - departureTime.getTime());
		}
		itineraryData.setDuration(durationMap);
		return itineraryData;
	}

	private List<OriginDestinationOptionData> createOriginDestinationOptions(final List<SailingLine> sectorList, final int odRef)
	{
		final List<OriginDestinationOptionData> originDestinationOptionList = new ArrayList<>();
		final OriginDestinationOptionData originDestinationOption = new OriginDestinationOptionData();
		originDestinationOption.setTransportOfferings(createTransportOfferingList(sectorList));
		originDestinationOption.setOriginDestinationRefNumber(odRef);
		originDestinationOptionList.add(originDestinationOption);
		return originDestinationOptionList;
	}

	private List<TravellerData> createTravellers(final List<SailingLine> sailingLines)
	{
		final List<TravellerData> travellers = new ArrayList<>();
		StreamUtil.safeStream(sailingLines).forEach(sailingLine ->
				StreamUtil.safeStream(sailingLine.getSailingPrices()).forEach(sailingPrices ->
						StreamUtil.safeStream(sailingPrices.getProductFares()).forEach(productFare ->
								populateTraveller(travellers, productFare))));
		return travellers;
	}

	private void populateTraveller(final List<TravellerData> travellers, final ProductFare productFare)
	{
		if (StringUtils.equals(TravellerType.PASSENGER.getCode(), productFare.getProduct().getProductCategory()))
		{
			StreamUtil.safeStream(productFare.getProduct().getPassengerInfo()).forEach(customerInfo -> {
				if (Objects.nonNull(customerInfo))
				{
					final PassengerInformationData travellerInfo = new PassengerInformationData();
					travellerInfo.setFirstName(customerInfo.getFirstName());
					travellerInfo.setSurname(customerInfo.getLastName());
					final TravellerData travellerData = new TravellerData();
					travellerData.setTravellerType(productFare.getProduct().getProductCategory());
					travellerData.setTravellerInfo(travellerInfo);
					travellers.add(travellerData);
				}
			});
		}
		else if (StringUtils.equals(TravellerType.VEHICLE.getCode(), productFare.getProduct().getProductCategory()) && Objects
				.nonNull(productFare.getProduct().getVehicleInfo()))
		{
			final VehicleInfo vehicleInfo = productFare.getProduct().getVehicleInfo();
			final VehicleInformationData travellerInfo = new VehicleInformationData();
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			vehicleTypeData.setCode(productFare.getProduct().getProductId());
			travellerInfo.setVehicleType(vehicleTypeData);
			travellerInfo.setHeight(Objects.nonNull(vehicleInfo.getVehicleHeight()) ? vehicleInfo.getVehicleHeight() : 0d);
			travellerInfo.setWeight(Objects.nonNull(vehicleInfo.getVehicleWeight()) ? vehicleInfo.getVehicleWeight() : 0d);
			travellerInfo.setWidth(Objects.nonNull(vehicleInfo.getVehicleWidth()) ? vehicleInfo.getVehicleWidth() : 0d);
			travellerInfo.setLength(Objects.nonNull(vehicleInfo.getVehicleLength()) ? vehicleInfo.getVehicleLength() : 0d);
			travellerInfo.setLicensePlateNumber(vehicleInfo.getVehicleLicense());
			travellerInfo.setLicensePlateProvince(vehicleInfo.getVehicleProvince());
			travellerInfo.setLicensePlateCountry(vehicleInfo.getVehicleNationality());
			final TravellerData travellerData = new TravellerData();
			travellerData.setTravellerType(productFare.getProduct().getProductCategory());
			travellerData.setTravellerInfo(travellerInfo);
			travellers.add(travellerData);
		}
	}

	private List<TransportOfferingData> createTransportOfferingList(final List<SailingLine> sectorList)
	{
		final List<TransportOfferingData> transportOfferingList = new ArrayList<>();
		for (final SailingLine sailing : sectorList)
		{
			final TransportOfferingData transportOffering = new TransportOfferingData();
			transportOffering.setTransportVehicle(createTransportVehicle(sailing));
			transportOffering.setDepartureTime(TravelDateUtils
					.getDate(sailing.getLineInfo().getDepartureDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			transportOffering.setArrivalTime(TravelDateUtils
					.getDate(sailing.getLineInfo().getArrivalDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			transportOffering.setDepartureTimeZoneId(ZoneId.of("America/Vancouver"));// Need to check and change
			transportOffering.setArrivalTimeZoneId(ZoneId.of("America/Vancouver"));// Need to check and change
			transportOffering.setSector(createSector(sailing));

			transportOfferingList.add(transportOffering);
		}
		return transportOfferingList;
	}

	private TravelSectorData createSector(final SailingLine sailing)
	{
		final TravelSectorData travelSectorData = new TravelSectorData();
		travelSectorData.setOrigin(createTransportFacility(sailing.getLineInfo().getDeparturePortCode()));
		travelSectorData.setDestination(createTransportFacility(sailing.getLineInfo().getArrivalPortCode()));

		return travelSectorData;
	}

	private TransportVehicleData createTransportVehicle(final SailingLine sailing)
	{
		final TransportVehicleData transportVehicleData = new TransportVehicleData();
		transportVehicleData.setVehicleInfo(createVehicleInfo(sailing));
		return transportVehicleData;
	}

	private TransportVehicleInfoData createVehicleInfo(final SailingLine sailing)
	{
		final TransportVehicleInfoData transportVehicleInfo = new TransportVehicleInfoData();
		final TransportVehicleModel vehicle = getBcfTransportVehicleService()
				.getTransportVehicle(sailing.getLineInfo().getVesselCode());
		if (Objects.nonNull(vehicle) && Objects.nonNull(vehicle.getTransportVehicleInfo()))
		{
			transportVehicleInfo.setName(vehicle.getTransportVehicleInfo().getName());
			transportVehicleInfo.setCode(vehicle.getTransportVehicleInfo().getCode());
		}
		return transportVehicleInfo;
	}

	// Origin & Destination for one end to end leg/sailing
	private TravelRouteData createRoute(final List<SailingLine> sectorList)
	{
		final TravelRouteData travelRoute = new TravelRouteData();
		travelRoute.setOrigin(createTransportFacility(sectorList.get(0).getLineInfo().getDeparturePortCode()));
		travelRoute
				.setDestination(createTransportFacility(sectorList.get(sectorList.size() - 1).getLineInfo().getArrivalPortCode()));
		final RouteType routeType = getTravelRouteFacade()
				.getRouteType(travelRoute.getOrigin().getCode(), travelRoute.getDestination().getCode());
		if (Objects.isNull(routeType))
		{
			LOG.warn(String.format("no route present in system from [%s] to [%s]", travelRoute.getOrigin().getCode(),
					travelRoute.getDestination().getCode()));
		}
		else
		{
			travelRoute.setRouteType(routeType.getCode());
		}
		return travelRoute;
	}

	private TransportFacilityData createTransportFacility(final String portCode)
	{
		final TransportFacilityData transportFacility = new TransportFacilityData();
		final LocationData locationData = new LocationData();
		final TransportFacilityModel transportFacilityModel = getTransportFacilityDao().findTransportFacility(portCode);

		locationData.setName(transportFacilityModel.getLocation().getName());

		transportFacility.setCode(portCode);
		transportFacility.setName(transportFacilityModel.getName());
		transportFacility.setLocation(locationData);
		return transportFacility;
	}

	private Map<String, String> getAncillaryProductNamesMap()
	{
		final Map<String, String> ancillaryProductNamesMap = new HashMap<>();
		StreamUtil.safeStream(getAncillaryProductFacade().getAncillaryProductsForCodes(BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES))))
				.forEach(productData ->
						ancillaryProductNamesMap.put(productData.getCode(), productData.getName()));
		return ancillaryProductNamesMap;
	}

	private String getBookerName(final Itinerary itinerary)
	{
		if (Objects.nonNull(itinerary.getBooking().getBookingHolderInfo()))
		{
			return String.format("%s %s", itinerary.getBooking().getBookingHolderInfo().getFirstName(),
					itinerary.getBooking().getBookingHolderInfo().getLastName());
		}
		return null;
	}

	protected TransportFacilityDao getTransportFacilityDao()
	{
		return transportFacilityDao;
	}

	@Required
	public void setTransportFacilityDao(final TransportFacilityDao transportFacilityDao)
	{
		this.transportFacilityDao = transportFacilityDao;
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected BCFPassengerTypeDao getBcfPassengerTypeDao()
	{
		return bcfPassengerTypeDao;
	}

	@Required
	public void setBcfPassengerTypeDao(final BCFPassengerTypeDao bcfPassengerTypeDao)
	{
		this.bcfPassengerTypeDao = bcfPassengerTypeDao;
	}

	protected VehicleTypeDao getVehicleTypeDao()
	{
		return vehicleTypeDao;
	}

	@Required
	public void setVehicleTypeDao(final VehicleTypeDao vehicleTypeDao)
	{
		this.vehicleTypeDao = vehicleTypeDao;
	}

	protected SailingReturnPairMappingDao getSailingReturnPairMappingDao()
	{
		return sailingReturnPairMappingDao;
	}

	@Required
	public void setSailingReturnPairMappingDao(final SailingReturnPairMappingDao sailingReturnPairMappingDao)
	{
		this.sailingReturnPairMappingDao = sailingReturnPairMappingDao;
	}

	protected BCFOriginDestinationLevelActionStrategy getBookingActionStrategy()
	{
		return bookingActionStrategy;
	}

	@Required
	public void setBookingActionStrategy(final BCFOriginDestinationLevelActionStrategy bookingActionStrategy)
	{
		this.bookingActionStrategy = bookingActionStrategy;
	}

	protected BcfTermsAndConditionsFacade getBcfTermsAndConditionsFacade()
	{
		return bcfTermsAndConditionsFacade;
	}

	@Required
	public void setBcfTermsAndConditionsFacade(final BcfTermsAndConditionsFacade bcfTermsAndConditionsFacade)
	{
		this.bcfTermsAndConditionsFacade = bcfTermsAndConditionsFacade;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected BCFTravelRouteFacade getTravelRouteFacade()
	{
		return travelRouteFacade;
	}

	@Required
	public void setTravelRouteFacade(final BCFTravelRouteFacade travelRouteFacade)
	{
		this.travelRouteFacade = travelRouteFacade;
	}

	protected BcfTransportVehicleService getBcfTransportVehicleService()
	{
		return bcfTransportVehicleService;
	}

	@Required
	public void setBcfTransportVehicleService(final BcfTransportVehicleService bcfTransportVehicleService)
	{
		this.bcfTransportVehicleService = bcfTransportVehicleService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected AncillaryProductFacade getAncillaryProductFacade()
	{
		return ancillaryProductFacade;
	}

	@Required
	public void setAncillaryProductFacade(final AncillaryProductFacade ancillaryProductFacade)
	{
		this.ancillaryProductFacade = ancillaryProductFacade;
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
