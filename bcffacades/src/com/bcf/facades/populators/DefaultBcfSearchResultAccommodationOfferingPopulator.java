/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import static com.bcf.facades.constants.BcfFacadesConstants.DEAL_OF_THE_DAY_DATE_PATTERN;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.travelfacades.search.converters.populator.SearchResultAccommodationOfferingPopulator;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.services.RatePlanService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.facades.constants.BcfFacadesConstants;



public class DefaultBcfSearchResultAccommodationOfferingPopulator extends SearchResultAccommodationOfferingPopulator
{
	private RatePlanService ratePlanService;

	@Override
	public void populate(final SearchResultValueData source, final AccommodationOfferingDayRateData target)
	{
		super.populate(source, target);
		target.setReviewLocationId(getValue(source, BcfFacadesConstants.REVIEW_LOCATION_ID));
		target.setMaxOccupancy(this.<Integer>getValue(source, BcfFacadesConstants.MAX_OCCUPANCY));
		target.setAdultsCount(this.<Integer>getValue(source, BcfFacadesConstants.NO_OF_ADULTS));

		final Boolean isDealOfTheDay = this.<Boolean>getValue(source, BcfFacadesConstants.IS_DEAL_OF_THE_DAY);
		if (BooleanUtils.isTrue(isDealOfTheDay))
		{
			target.setIsDealOfTheDay(getValue(source, BcfFacadesConstants.IS_DEAL_OF_THE_DAY));
			if (target.isIsDealOfTheDay())
			{
				target.setDealOfTheDayStartDate(TravelDateUtils
						.getDate(getValue(source, BcfFacadesConstants.DEAL_OF_THE_DAY_STARTDATE),
								DEAL_OF_THE_DAY_DATE_PATTERN));
				target.setDealOfTheDayEndDate(TravelDateUtils
						.getDate(getValue(source, BcfFacadesConstants.DEAL_OF_THE_DAY_ENDDATE), DEAL_OF_THE_DAY_DATE_PATTERN));
			}
		}

		final List<String> ratePlanConfigs = target.getRatePlanConfigs();
		target.setRatePlanConfigs(getValidRatePlanConfigs(target.getDateOfStay(), ratePlanConfigs));
	}

	/**
	 * get valid rate plan configs against a date
	 *
	 * @param dateOfStay
	 * @param ratePlanConfigs
	 * @return
	 */
	protected List<String> getValidRatePlanConfigs(final Date dateOfStay, final List<String> ratePlanConfigs)
	{
		final List<String> validRatePlanConfigs = new ArrayList<>();
		for (final String ratePlanConfig : ratePlanConfigs)
		{
			final String ratePlanCode = ratePlanConfig.split("\\|", 3)[0];
			final RatePlanModel ratePlan = getRatePlanService().getRatePlanForCode(ratePlanCode);
			if (BcfRatePlanUtil.isValidRatePlan(dateOfStay, ratePlan))
			{
				validRatePlanConfigs.add(ratePlanConfig);
			}
		}
		return validRatePlanConfigs;
	}

	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}
}
