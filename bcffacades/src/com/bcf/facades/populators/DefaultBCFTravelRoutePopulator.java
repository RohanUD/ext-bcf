/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.populators;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.populators.TravelRoutePopulator;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.StreamUtil;


public class DefaultBCFTravelRoutePopulator extends TravelRoutePopulator
{
	private BCFTravelRouteService bcfTravelRouteService;

	@Override
	public void populate(final TravelRouteModel source, final TravelRouteData target) throws ConversionException
	{
		super.populate(source, target);
		if (Objects.nonNull(source.getRouteType()))
		{
			target.setRouteType(source.getRouteType().getCode());
		}
		target.setAllowMotorcycles(Objects.nonNull(source.getAllowMotorcycles()) ? source.getAllowMotorcycles() : false);
		target.setWalkOn(Objects.nonNull(source.getWalkOn()) ? source.getWalkOn() : false);
		target.setAllowsWalkOnOptions(Objects.nonNull(source.getAllowsWalkOnOptions()) ? source.getAllowsWalkOnOptions() : false);
		target.setFerryTrackingLink(source.getFerryTrackingLink());
		target.setReservable(source.isReservable());
		target.setRoute(source.getNumber());
		target.setAllowOpenTicket(source.isAllowOpenTicket());
		target.setFullyPrepaid(source.isFullyPrepaid());
		target.setGeographicalAreaName(getGeographicalAreaName(source.getOrigin().getLocation()));

		if (source.getMapKmlFile() != null)
		{
			target.setMapKmlFileUrl(source.getMapKmlFile().getURL());
		}
		target.setEnableRevenueManagement(source.isEnableRevenueManagement());

		getWaitListLinkByDirectRoute(source, target);
	}

	private String getGeographicalAreaName(final LocationModel locationModel)
	{
		if (LocationType.GEOGRAPHICAL_AREA.equals(locationModel.getLocationType()))
		{
			return locationModel.getName();
		}
		if (CollectionUtils.isEmpty(locationModel.getSuperlocations()))
		{
			return StringUtils.EMPTY;
		}
		return getGeographicalAreaName(locationModel.getSuperlocations().get(0));
	}

	private void getWaitListLinkByDirectRoute(final TravelRouteModel source, final TravelRouteData target)
	{
		final Optional<TravelRouteModel> optionalTravelRouteModel = StreamUtil
				.safeStream(getBcfTravelRouteService()
						.getDirectTravelRoutes(source.getOrigin().getCode(), source.getDestination().getCode()))
				.findAny();
		if (optionalTravelRouteModel.isPresent())
		{
			target.setWaitListLink(optionalTravelRouteModel.get().getWaitListLink());
			target.setTravelAgentWaitListLink(optionalTravelRouteModel.get().getTravelAgentWaitListLink());
		}
	}

	public BCFTravelRouteService getBcfTravelRouteService()
	{
		return bcfTravelRouteService;
	}

	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		this.bcfTravelRouteService = bcfTravelRouteService;
	}
}
