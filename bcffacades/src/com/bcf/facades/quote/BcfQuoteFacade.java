/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.quote;

import de.hybris.platform.commercefacades.order.QuoteFacade;
import de.hybris.platform.order.exceptions.CalculationException;
import java.util.Date;
import java.util.List;
import com.bcf.facades.cart.QuoteCartData;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public interface BcfQuoteFacade extends QuoteFacade
{
	void createQuote(final String emailAddress);

	void cancelQuotes(List<String> codes);

	BcfGlobalReservationData retrieveCart(final String cartCode) throws CalculationException;

	List<QuoteCartData> getPagedQuote();

	public boolean isQuoteExpired(final Date quoteExpiryDate);

	public Date getQuoteExpiryDate(final String cart);

	public boolean reserveAccommodationAndActivityStocksForQuote(final String cartCode);

	public boolean reserveSailingStocksForQuote(final String cartCode) throws CalculationException;

	public void removeSailingEntries(final String  cartCode) throws CalculationException;

	public void attachCartToSession(final String cartCode);

	public boolean isQuote();
}
