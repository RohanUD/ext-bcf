/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.quote.impl;

import de.hybris.platform.commercefacades.order.impl.DefaultQuoteFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.SaveQuoteProcessModel;
import com.bcf.core.order.strategies.ordercloning.BcfCloneOrderStrategy;
import com.bcf.core.quote.service.BcfQuoteService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.cart.QuoteCartData;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.quote.BcfQuoteFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.stock.preprocessor.BcfStockPreprocessor;
import com.bcf.integration.ebooking.service.MakeBookingService;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfQuoteFacade extends DefaultQuoteFacade implements BcfQuoteFacade
{
	private static final String SESSION_CART_PARAMETER_NAME = "cart";

	private UserFacade userFacade;
	private CalculationService calculationService;
	private BCFGlobalReservationFacade globalReservationFacade;
	private BcfQuoteService quoteService;
	private BusinessProcessService businessProcessService;
	private Converter<CartModel, QuoteCartData> bcfQuoteCartConverter;
	private BCFTravelCartService bcfTravelCartService;
	private SessionService sessionService;
	private BaseSiteService baseSiteService;
	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfStockPreprocessor bcfStockPreprocessor;
	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;
	private MakeBookingService makeBookingService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfTransportOfferingService bcftransportOfferingService;
	private BCFTravelCartService cartService;
	private BcfCloneOrderStrategy bcfCloneOrderStrategy;
	private KeyGenerator orderCodeGenerator;

	private static final Logger LOG = Logger.getLogger(DefaultBcfQuoteFacade.class);

	@Override
	public void createQuote(final String emailAddress)
	{
		final CartModel cart = getCartService().getSessionCart();

		CartModel quoteCart = (CartModel) getBcfCloneOrderStrategy().cloneCart(cart);
		quoteCart.setQuote(true);
		quoteCart.setCode(getOrderCodeGenerator().generate().toString());
		quoteCart.setQuoteExpiryDate(getQuoteExpirationDate(cart));
		quoteCart.setStockHoldTime(0l);
		if (getUserFacade().isAnonymousUser() && !StringUtils.isEmpty(emailAddress))
		{
			quoteCart.setEmailAddress(emailAddress);
		}
		getModelService().save(quoteCart);
		LOG.info(String.format("Quote [%s] created from cart [%s]", quoteCart.getCode(), cart.getCode()));
		startSaveQuoteProcess(quoteCart);
	}

	private Date getQuoteExpirationDate(final CartModel cart)
	{
		final Date firstTravelDate = getFirstTravelDate(cart.getEntries());

		final int defaultQuotationExpiryDays =
				Integer.parseInt(bcfConfigurablePropertiesService.getBcfPropertyValue("defaultQuotationExpirationDays"));
		final Calendar cal = Calendar.getInstance();

		cal.add(Calendar.DAY_OF_MONTH, defaultQuotationExpiryDays);

		if (firstTravelDate == null)
		{
			return cal.getTime();
		}
		return Stream.of(firstTravelDate, cal.getTime())
				.min(Date::compareTo).get();

	}

	@Override
	public boolean isQuoteExpired(final Date quoteExpiryDate)
	{

		if (quoteExpiryDate.compareTo(new Date()) < 0)
		{
			return true;
		}
		return false;
	}


	@Override
	public Date getQuoteExpiryDate(final String cart)
	{

		final CartModel cartModel = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cart, baseSiteService.getCurrentBaseSite());

		return cartModel.getQuoteExpiryDate();
	}

	private void startSaveQuoteProcess(final CartModel cart)
	{
		final SaveQuoteProcessModel saveQuoteProcessModel = getBusinessProcessService()
				.createProcess("saveQuoteProcess-" + cart.getCode() + System.currentTimeMillis(), "saveQuoteProcess");
		final CustomerModel customerModel = (CustomerModel) cart.getUser();

		//this is required in case the user is anonymous
		if (StringUtils.isNotBlank(cart.getEmailAddress()))
		{
			saveQuoteProcessModel.setEmailAddress(cart.getEmailAddress());
		}
		executeBusinessProcess(saveQuoteProcessModel, cart, customerModel);
	}

	private void executeBusinessProcess(final SaveQuoteProcessModel saveQuoteProcessModel, final CartModel cart,
			final CustomerModel customerModel)
	{
		saveQuoteProcessModel.setQuoteCode(cart.getCode());
		saveQuoteProcessModel.setCustomer(customerModel);
		saveQuoteProcessModel.setQuoteExpiryDate(
				TravelDateUtils.convertDateToStringDate(cart.getQuoteExpiryDate(), BcfCoreConstants.ADDTOCART_DATE_PATTERN));
		saveQuoteProcessModel.setGuestBookingIdentifier(cart.getGuid());
		getModelService().save(saveQuoteProcessModel);
		getBusinessProcessService().startProcess(saveQuoteProcessModel);
	}

	@Override
	public List<QuoteCartData> getPagedQuote()
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		final List<CartModel> quoteModelSearchData = getQuoteService()
				.getQuoteList(currentCustomer, currentBaseStore, true);
		final List<CartModel> quotesModel = new ArrayList<>();
		quoteModelSearchData.stream().forEach(cart -> {

			final Date firstTravelDate = getFirstTravelDate(cart.getEntries());

			if (firstTravelDate.after(new Date()))
			{
				quotesModel.add(cart);
			}
		});

		final List<QuoteCartData> quoteCartDataList = Converters.convertAll(quotesModel, getBcfQuoteCartConverter());
		return quoteCartDataList.stream().sorted(Comparator.comparing(QuoteCartData::getModifiedTime).reversed())
				.collect(Collectors.toList());
	}


	private Date getFirstTravelDate(final List<AbstractOrderEntryModel> entries)
	{

		final List<AbstractOrderEntryModel> transportEntries = entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.TRANSPORT
						.equals(entry.getType()) && entry.getTravelOrderEntryInfo() != null && !entry.getTravelOrderEntryInfo()
						.getTransportOfferings().iterator().next().getDefault()).collect(
				Collectors.toList());

		final Date firstTransportDate = StreamUtil.safeStream(transportEntries)
				.map(entry -> entry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDepartureTime())
				.min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> accommodationEntries = entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.ACCOMMODATION
						.equals(entry.getType())).collect(
				Collectors.toList());

		final Date firstAccommodationDate = changeAndCancellationFeeCalculationHelper
				.getAccommodationOrderEntryGroup(accommodationEntries).stream()
				.map(entryGroup -> entryGroup.getStartingDate()).min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> activityEntries = entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.ACTIVITY
						.equals(entry.getType())).collect(
				Collectors.toList());

		final Date firstActivityDate = StreamUtil.safeStream(activityEntries)
				.map(entry -> entry.getActivityOrderEntryInfo().getActivityDate()).min(Date::compareTo).orElse(null);


		return Stream.of(firstTransportDate, firstAccommodationDate, firstActivityDate)
				.filter(Objects::nonNull).min(Date::compareTo).orElse(null);

	}

	@Override
	public void cancelQuotes(final List<String> codes)
	{
		StreamUtil.safeStream(codes).forEach(code -> {
			final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
			final CartModel cart = getCommerceCartService().getCartForCodeAndUser(code, currentCustomer);
			getBcfTravelCartService().removeCart(cart);
		});
	}

	@Override
	public BcfGlobalReservationData retrieveCart(final String cartCode) throws CalculationException
	{
		final CartModel cart = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());
		if (Objects.isNull(cart))
		{
			return null;
		}
		getCalculationService().recalculate(cart);
		getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, cart);
		if (cart.getBookingJourneyType() != null)
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, cart.getBookingJourneyType().getCode());
		}
		final BcfGlobalReservationData bcfGlobalReservationData = getGlobalReservationFacade()
				.getbcfGlobalReservationDataList(cart);

		final BcfGlobalReservationData ferryBcfGlobalReservationData = getGlobalReservationFacade()
				.getbcfGlobalReservationDataListForFerry(cart);
		if (ferryBcfGlobalReservationData != null && bcfGlobalReservationData != null)
		{
			bcfGlobalReservationData.setTransportReservations(ferryBcfGlobalReservationData.getTransportReservations());

		}

		return bcfGlobalReservationData;
	}


	@Override
	public void attachCartToSession(final String cartCode)
	{
		final CartModel cart = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());

		if (Objects.isNull(cart))
		{
			LOG.error(String.format(
					"No quote [%s] found to convert to cart during quote-to-cart conversion.",
					cart.getCode()));
			return;
		}
		cart.setQuote(false);
		cart.setQuoteExpirationDate(null);
		cart.setStockHoldTime(bcfTravelCommerceStockService.getStockHoldExpiryTimeInMs(cart.getSalesApplication()));
		getModelService().save(cart);
		LOG.info(String.format("Quote [%s] converted to cart.", cart.getCode()));
		try
		{
			getCalculationService().recalculate(cart);
		}
		catch (final CalculationException ex)
		{
			LOG.error(String.format(
					"Error occurred while recalculation of cart [%s] during quote-to-cart conversion. Removing cart from session.",
					cart.getCode()));
			cartService.removeSessionCart();
			return;
		}

		getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, cart);
		if (cart.getBookingJourneyType() != null)
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, cart.getBookingJourneyType().getCode());
		}
	}

	@Override
	public boolean reserveAccommodationAndActivityStocksForQuote(final String cartCode)

	{
		final CartModel cart = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());
		if (Objects.isNull(cart))
		{
			return false;
		}

		boolean executed = false;

		if (CollectionUtils.isNotEmpty(cart.getEntries()))
		{
			final boolean accommodationOrActivitiesPresent = cart.getEntries().stream()
					.anyMatch(entry -> entry.getActive() && (OrderEntryType.ACCOMMODATION.equals(entry.getType())
							|| OrderEntryType.ACTIVITY.equals(entry.getType())));

			if (!accommodationOrActivitiesPresent)
			{
				return true;
			}


			final Transaction currentTransaction = Transaction.current();
			try
			{
				currentTransaction.begin();
				final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
				bcfStockPreprocessor.validateAndReserve(decisionData, cart);
				executed = true;
			}
			catch (final CartValidationException | InsufficientStockLevelException | OrderProcessingException ex)
			{
				LOG.error("Error while reserving stock for quote:" + cartCode, ex);
			}

			finally
			{
				if (executed)
				{
					currentTransaction.commit();
				}
				else
				{
					currentTransaction.rollback();
				}
			}
		}

		if (executed)
		{
			return true;
		}
		else
		{
			cartService.removeSessionCart();
			return false;
		}
	}


	@Override
	public boolean reserveSailingStocksForQuote(final String cartCode) throws CalculationException
	{
		final CartModel cartModel = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());
		if (Objects.isNull(cartModel))
		{
			return false;
		}

		if (CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			return true;
		}

		final Map<Integer, List<AbstractOrderEntryModel>> sailingEntries = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
						&& entry.getTravelOrderEntryInfo() != null
				)
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		if (MapUtils.isEmpty(sailingEntries))
		{
			return true;
		}
		try
		{
			makeBookingIntegrationFacade.checkAndUpdateCartWithMakeBookingResponseForQuote(sailingEntries, cartModel);

		}
		catch (final IntegrationException ex)
		{
			LOG.error("integration error while calling make booking service for quote ", ex);
			if (BookingJourneyType.BOOKING_ALACARTE.equals(cartModel.getBookingJourneyType()))
			{
				removeSailingEntries(cartCode);
			}
			return false;
		}

		return true;
	}

	@Override
	public void removeSailingEntries(final String cartCode) throws CalculationException
	{
		final CartModel cartModel = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());

		final Set<AbstractOrderEntryModel> entries = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
				)
				.collect(Collectors.toSet());
		bcfTravelCartService.removeEntriesForTransport(entries);
		getModelService().refresh(cartModel);
		getCalculationService().recalculate(cartModel);
	}


	@Override
	public boolean isQuote()
	{
		if (cartService.hasSessionCart())
		{
			final CartModel cart = cartService.getSessionCart();

			if (cart.isQuote())
			{
				return true;
			}
		}
		return false;
	}


	protected UserFacade getUserFacade()
	{
		return userFacade;
	}

	@Required
	public void setUserFacade(final UserFacade userFacade)
	{
		this.userFacade = userFacade;
	}

	protected CalculationService getCalculationService()
	{
		return calculationService;
	}

	@Required
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	protected BCFGlobalReservationFacade getGlobalReservationFacade()
	{
		return globalReservationFacade;
	}

	@Required
	public void setGlobalReservationFacade(final BCFGlobalReservationFacade globalReservationFacade)
	{
		this.globalReservationFacade = globalReservationFacade;
	}

	@Override
	protected BcfQuoteService getQuoteService()
	{
		return quoteService;
	}

	@Required
	public void setQuoteService(final BcfQuoteService quoteService)
	{
		this.quoteService = quoteService;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected Converter<CartModel, QuoteCartData> getBcfQuoteCartConverter()
	{
		return bcfQuoteCartConverter;
	}

	@Required
	public void setBcfQuoteCartConverter(
			final Converter<CartModel, QuoteCartData> bcfQuoteCartConverter)
	{
		this.bcfQuoteCartConverter = bcfQuoteCartConverter;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public ChangeAndCancellationFeeCalculationHelper getChangeAndCancellationFeeCalculationHelper()
	{
		return changeAndCancellationFeeCalculationHelper;
	}

	public void setChangeAndCancellationFeeCalculationHelper(
			final ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper)
	{
		this.changeAndCancellationFeeCalculationHelper = changeAndCancellationFeeCalculationHelper;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfStockPreprocessor getBcfStockPreprocessor()
	{
		return bcfStockPreprocessor;
	}

	public void setBcfStockPreprocessor(final BcfStockPreprocessor bcfStockPreprocessor)
	{
		this.bcfStockPreprocessor = bcfStockPreprocessor;
	}

	public MakeBookingIntegrationFacade getMakeBookingIntegrationFacade()
	{
		return makeBookingIntegrationFacade;
	}

	public void setMakeBookingIntegrationFacade(final MakeBookingIntegrationFacade makeBookingIntegrationFacade)
	{
		this.makeBookingIntegrationFacade = makeBookingIntegrationFacade;
	}

	public MakeBookingService getMakeBookingService()
	{
		return makeBookingService;
	}

	public void setMakeBookingService(final MakeBookingService makeBookingService)
	{
		this.makeBookingService = makeBookingService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BcfTransportOfferingService getBcftransportOfferingService()
	{
		return bcftransportOfferingService;
	}

	public void setBcftransportOfferingService(final BcfTransportOfferingService bcftransportOfferingService)
	{
		this.bcftransportOfferingService = bcftransportOfferingService;
	}

	@Override
	public BCFTravelCartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final BCFTravelCartService cartService)
	{
		this.cartService = cartService;
	}

	public BcfCloneOrderStrategy getBcfCloneOrderStrategy()
	{
		return bcfCloneOrderStrategy;
	}

	@Required
	public void setBcfCloneOrderStrategy(final BcfCloneOrderStrategy bcfCloneOrderStrategy)
	{
		this.bcfCloneOrderStrategy = bcfCloneOrderStrategy;
	}

	public KeyGenerator getOrderCodeGenerator()
	{
		return orderCodeGenerator;
	}

	@Required
	public void setOrderCodeGenerator(final KeyGenerator orderCodeGenerator)
	{
		this.orderCodeGenerator = orderCodeGenerator;
	}
}
