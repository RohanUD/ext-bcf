/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.helper;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import de.hybris.platform.travelservices.enums.TransportVehicleType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.shipinfo.service.BcfShipInfoService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.travelsector.service.TravelSectorService;


public class TravelDataHelper
{
	private ModelService modelService;
	private BcfTransportFacilityService transportFacilityService;
	private BCFTravelRouteService travelRouteService;
	private PointOfServiceService pointOfServiceService;
	private BcfTravelLocationService travelLocationService;
	private BcfTransportVehicleService transportVehicleService;
	private TravelSectorService travelSectorService;
	private BcfShipInfoService bcfShipInfoService;

	public TransportFacilityModel getOrCreateTransportFacility(final String code)
	{
		TransportFacilityModel transportFacilityModel = getTransportFacilityService().getTransportFacilityForCode(code);

		if (Objects.isNull(transportFacilityModel))
		{
			transportFacilityModel = getModelService().create(TransportFacilityModel.class);
			transportFacilityModel.setCode(code);
			getModelService().save(transportFacilityModel);
		}
		return transportFacilityModel;
	}

	public TravelSectorModel getOrCreateTravelSector(final String sectorCode)
	{
		TravelSectorModel travelSectorModel;
		try
		{
			travelSectorModel = getTravelSectorService().getTravelSectorForCode(sectorCode);
			if (Objects.isNull(travelSectorModel))
			{
				travelSectorModel = getModelService().create(TravelSectorModel.class);
				travelSectorModel.setCode(sectorCode);
				getModelService().save(travelSectorModel);
			}
		}
		catch (final ModelNotFoundException mEx)
		{
			travelSectorModel = getModelService().create(TravelSectorModel.class);
			travelSectorModel.setCode(sectorCode);
			getModelService().save(travelSectorModel);
		}
		return travelSectorModel;
	}

	public TravelSectorModel getOrCreateTravelSector(final String sectorCode, final String origin, final String destination)
	{
		TravelSectorModel travelSectorModel = getTravelSectorService().getTravelSectorForCode(sectorCode);
		if (Objects.isNull(travelSectorModel))
		{
			travelSectorModel = getModelService().create(TravelSectorModel.class);
		}
		travelSectorModel.setCode(sectorCode);
		travelSectorModel.setOrigin(getTransportFacilityService().getTransportFacilityForCode(origin));
		travelSectorModel.setDestination(getTransportFacilityService().getTransportFacilityForCode(destination));
		getModelService().save(travelSectorModel);
		return travelSectorModel;
	}

	public TransportVehicleModel getOrCreateTransportVehicle(final String code)
	{
		TransportVehicleModel transportVehicleModel;
		transportVehicleModel = getTransportVehicleService().getTransportVehicle(code);
		if (Objects.isNull(transportVehicleModel))
		{
			transportVehicleModel = getModelService().create(TransportVehicleModel.class);
			transportVehicleModel.setCode(code);
			transportVehicleModel.setType(TransportVehicleType.SHIP);
			transportVehicleModel.setActive(Boolean.TRUE);
			transportVehicleModel.setTransportVehicleInfo(getOrCreateTransportVehicleInfo(code));
			getModelService().save(transportVehicleModel);
		}
		return transportVehicleModel;
	}

	private TransportVehicleInfoModel getOrCreateTransportVehicleInfo(final String code)
	{
		ShipInfoModel shipInfo = getBcfShipInfoService().getShipInfoForCode(code);
		if (Objects.isNull(shipInfo))
		{
			shipInfo = getModelService().create(ShipInfoModel.class);
			shipInfo.setCode(code);
			shipInfo.setName(code);
			getModelService().save(shipInfo);
		}
		return shipInfo;
	}

	public TravelRouteModel getOrCreateTravelRoute(final String code)
	{
		TravelRouteModel travelRouteModel;
		try
		{
			travelRouteModel = getTravelRouteService().getTravelRoute(code);
			if (Objects.nonNull(travelRouteModel))
			{
				travelRouteModel = modelService.create(TravelRouteModel.class);
				travelRouteModel.setCode(code);
			}
		}
		catch (final ModelNotFoundException mEx)
		{
			travelRouteModel = modelService.create(TravelRouteModel.class);
			travelRouteModel.setCode(code);
		}
		return travelRouteModel;
	}

	public LocationModel getOrCreateLocation(final String code)
	{
		LocationModel locationModel = getTravelLocationService().getLocation(code);
		if (Objects.isNull(locationModel))
		{
			locationModel = modelService.create(LocationModel.class);
			locationModel.setCode(code);
		}
		return locationModel;

	}

	public PointOfServiceModel getOrCreatePointOfService(final String name)
	{
		PointOfServiceModel pointOfServiceModel = getPointOfServiceService().getPointOfServiceForName(name);
		if (Objects.isNull(pointOfServiceModel))
		{
			pointOfServiceModel = modelService.create(PointOfServiceModel.class);
			pointOfServiceModel.setName(name);
			pointOfServiceModel.setType(PointOfServiceTypeEnum.STORE);
		}

		return pointOfServiceModel;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public void deleteTravelRouteForCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be empty");
		final TravelRouteModel travelRouteModel = getTravelRouteService().getTravelRoute(code);
		if (Objects.isNull(travelRouteModel))
		{
			throw new UnknownIdentifierException("No travel route found for the given code " + code);
		}
		travelRouteModel.setActive(Boolean.FALSE);
		getModelService().save(travelRouteModel);
	}

	public void deleteTerminalForCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be empty");
		final TransportFacilityModel transportFacilityForCode = getTransportFacilityService().getTransportFacilityForCode(code);
		if (Objects.isNull(transportFacilityForCode))
		{
			throw new UnknownIdentifierException("No terminal found for the given code " + code);
		}
		transportFacilityForCode.setActive(Boolean.FALSE);
		getModelService().save(transportFacilityForCode);
	}

	protected BcfTransportFacilityService getTransportFacilityService()
	{
		return transportFacilityService;
	}

	@Required
	public void setTransportFacilityService(final BcfTransportFacilityService transportFacilityService)
	{
		this.transportFacilityService = transportFacilityService;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected PointOfServiceService getPointOfServiceService()
	{
		return pointOfServiceService;
	}

	@Required
	public void setPointOfServiceService(final PointOfServiceService pointOfServiceService)
	{
		this.pointOfServiceService = pointOfServiceService;
	}

	protected BcfTravelLocationService getTravelLocationService()
	{
		return travelLocationService;
	}

	@Required
	public void setTravelLocationService(final BcfTravelLocationService travelLocationService)
	{
		this.travelLocationService = travelLocationService;
	}

	protected BcfTransportVehicleService getTransportVehicleService()
	{
		return transportVehicleService;
	}

	@Required
	public void setTransportVehicleService(final BcfTransportVehicleService transportVehicleService)
	{
		this.transportVehicleService = transportVehicleService;
	}

	protected TravelSectorService getTravelSectorService()
	{
		return travelSectorService;
	}

	@Required
	public void setTravelSectorService(final TravelSectorService travelSectorService)
	{
		this.travelSectorService = travelSectorService;
	}

	protected BcfShipInfoService getBcfShipInfoService()
	{
		return bcfShipInfoService;
	}

	@Required
	public void setBcfShipInfoService(final BcfShipInfoService bcfShipInfoService)
	{
		this.bcfShipInfoService = bcfShipInfoService;
	}
}
