/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.schedules.impl;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.VendorService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteRegion;
import com.bcf.core.model.schedules.SeasonalScheduleModel;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.schedules.ScheduleDate;
import com.bcf.facades.schedules.SeasonalScheduleData;
import com.bcf.facades.webservices.populators.SchedulePopulator;
import com.bcf.facades.webservices.populators.ScheduleReversePopulator;
import com.bcf.facades.webservices.schedules.BcfSchedulesFacade;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.schedule.SeasonalSchedulesData;
import com.bcf.integration.schedule.service.SchedulesIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.webservices.travel.data.ScheduleData;
import com.bcf.webservices.travel.data.SchedulesData;


public class DefaultBcfSchedulesFacade implements BcfSchedulesFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfSchedulesFacade.class);

	private ScheduleReversePopulator scheduleReversePopulator;
	private SchedulePopulator schedulePopulator;
	private FlexibleSearchService flexibleSearchService;
	private ModelService modelService;
	private VendorService vendorService;
	private BcfTransportOfferingService bcftransportOfferingService;
	private SchedulesIntegrationService schedulesIntegrationService;
	private BCFTravelRouteService travelRouteService;
	private Converter<SeasonalScheduleModel, ScheduleDate> schedulesDateConverter;
	private EnumerationService enumerationService;
	private BcfTravelLocationFacade bcfTravelLocationFacade;
	private ConfigurationService configurationService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "defaultBcfTransportFacilityService")
	private BcfTransportFacilityService bcfTransportFacilityService;

	@Resource(name = "defaultBcfTravelRouteService")
	private BCFTravelRouteService bcfTravelRouteService;

	@Resource(name = "currentConditionsTravelRouteConverter")
	private Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter;

	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

   @Resource(name = "i18nService") private I18NService i18nService;

	private SetupSolrIndexerService setupSolrIndexerService;

	@Override
	public void createOrUpdateSchedules(final SchedulesData schedulesData) throws Exception
	{
		try
		{
			Transaction.current().execute(
					new TransactionBody()
					{
						@Override
						public Object execute()
						{
							schedulesData.getSchedules().stream().forEach(scheduleData -> {
								processScheduleData(scheduleData);
							});
							return null;
						}
					}
			);
		}

		catch (final Exception e)
		{
			LOG.error("exception happened while processing schedule synch process", e);
			throw e;
		}
		getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCFERRIES_INDEX, false);
	}

   private void processScheduleData(final ScheduleData scheduleData) {
      String sectorCode = scheduleData.getTravelSector();
      sectorCode = sectorCode.replaceAll("-", "");

      final String transportOfferingCode = scheduleData.getSailingCode() + sectorCode;

      TransportOfferingModel transportOfferingModel = getModelService().create(TransportOfferingModel.class);
      transportOfferingModel.setCode(transportOfferingCode);

      final List<TransportOfferingModel> transportOfferingModels = flexibleSearchService.getModelsByExample(transportOfferingModel);

      final List<ItemModel> itemsToSave = new ArrayList<ItemModel>();

      if (CollectionUtils.isNotEmpty(transportOfferingModels)) {
         transportOfferingModel = transportOfferingModels.get(0);

         if (Objects.nonNull(transportOfferingModel)) {

            final List<AbstractOrderEntryModel> modifiedEntries =
                  getBcftransportOfferingService().getModifiedOrderEntriesForOfferingCode(transportOfferingModel.getCode());

            final Boolean scheduleVehicleChanged = hasVehicleChanged(scheduleData, transportOfferingModel);
            if (org.apache.commons.lang.BooleanUtils.isTrue(scheduleVehicleChanged)) {
               for (final AbstractOrderEntryModel orderEntryModel : modifiedEntries) {
                  orderEntryModel.setVesselModified(Boolean.TRUE);
                  itemsToSave.add(orderEntryModel);
               }
            }

            final Boolean scheduleChanged = hasScheduleChanged(scheduleData, transportOfferingModel);
            if (org.apache.commons.lang.BooleanUtils.isTrue(scheduleChanged)) {
               for (final AbstractOrderEntryModel orderEntryModel : modifiedEntries) {
                  orderEntryModel.setScheduleModified(Boolean.TRUE);
                  itemsToSave.add(orderEntryModel);
               }
            }
         }
      } else {
         transportOfferingModel = createTransportOffering(transportOfferingCode);
         itemsToSave.add(transportOfferingModel);
      }

      scheduleReversePopulator.populate(scheduleData, transportOfferingModel);
      modelService.saveAll(itemsToSave);
      modelService.refresh(transportOfferingModel);
   }

   private Boolean hasVehicleChanged(final ScheduleData scheduleData, final TransportOfferingModel transportOfferingModel) {
      final String scheduleTransportVehicle = scheduleData.getTransportVehicle();
      final TransportVehicleModel transportVehicleModel = transportOfferingModel.getTransportVehicle();
      if (Objects.nonNull(transportVehicleModel) && Objects.nonNull(scheduleTransportVehicle) && scheduleTransportVehicle
            .equals(transportVehicleModel.getCode())) {
         return Boolean.FALSE;
      }
      return Boolean.TRUE;
   }

   private Boolean hasScheduleChanged(final ScheduleData scheduleData, final TransportOfferingModel transportOfferingModel) {
      final Date existingArriavalTime = transportOfferingModel.getArrivalTime();
      final Date existingDepartureTime = transportOfferingModel.getDepartureTime();

      final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BcfFacadesConstants.SCHEDULE_SYNC_DATE_FORMAT, i18nService.getCurrentLocale());
      try {
         final Date scheduleArriavalTime = simpleDateFormat.parse(scheduleData.getDepartureTime());
         final Date scheduleDepartureTime = simpleDateFormat.parse(scheduleData.getArrivalTime());

         if (Objects.nonNull(existingArriavalTime) && Objects.nonNull(scheduleArriavalTime) && Objects.nonNull(existingDepartureTime) && Objects
               .nonNull(scheduleDepartureTime) && (existingArriavalTime.compareTo(scheduleArriavalTime) == 0) && (
               existingDepartureTime.compareTo(scheduleDepartureTime) == 0)) {
            return Boolean.FALSE;
         }
         return Boolean.TRUE;
      } catch (final ParseException e) {
         LOG.error("error parsing the arrival / departure date from schedule data|");
      }
      return Boolean.TRUE;
   }

	@Override
	public ImageData getSchedulesLandingMap(final MediaModel mediaModel)
	{
		return imageConverter.convert(mediaModel);
	}

	@Override
	public Map<String, List<TravelRouteInfo>> getTravelRouteInfo()
	{
		final Map<String, List<TravelRouteInfo>> travelRoutesByRegion = new HashMap<>();
		final List<TravelRouteInfo> travelRoutes = new ArrayList<TravelRouteInfo>();
		// Get configured terminals and collect all travel routes for all those terminals
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_LANDING_TERMINALS);

		if (Objects.isNull(sourceTerminals))
		{
			LOG.error("Source Terminals are not found.");
			return travelRoutesByRegion;
		}

		final String[] terminals = sourceTerminals.split(",");
		final List<TransportFacilityModel> transportFacilities = bcfTransportFacilityService
				.getTransportFacilityForTerminalCodes(Arrays.asList(terminals));

		transportFacilities.stream().forEach(transportFacility -> {
			final List<TravelRouteModel> travelRouteModels = bcfTravelRouteService.getTravelRoutesForOrigin(transportFacility);
			travelRoutes.addAll(Converters.convertAll(travelRouteModels, currentConditionsTravelRouteConverter));
		});

		final List<String> routeRegions = travelRoutes.stream().map(TravelRouteInfo::getRouteRegion)
				.distinct().collect(Collectors.toList());

		routeRegions.stream().forEach(routeRegion -> {
			if (Objects.nonNull(routeRegion))
			{
				travelRoutesByRegion.put(routeRegion, travelRoutes.stream().filter(travelRout ->
						routeRegion.equals(travelRout.getRouteRegion())).collect(Collectors.toList()));
			}
		});

		return travelRoutesByRegion;
	}

	@Override
	public void deleteSchedules(final String sailingCode)
	{
		LOG.debug("Deleting travel sector " + sailingCode);
		final List<TransportOfferingModel> transportOfferings = getBcftransportOfferingService()
				.getTransportOfferingsForSailingCode(sailingCode);
		if (CollectionUtils.isEmpty(transportOfferings))
		{
			throw new UnknownIdentifierException("No transport offerings found for the given code " + sailingCode);
		}

		transportOfferings.stream().forEach(transportOfferingModel -> {
			transportOfferingModel.setActive(Boolean.FALSE);
			getModelService().save(transportOfferingModel);
		});

		//Perform Update indexing
		getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCFERRIES_INDEX, false);
	}

	@Override
	public SeasonalScheduleData getSeasonalSchedules(final Map<String, Object> params) throws IntegrationException
	{

		final SeasonalScheduleData scheduleData = new SeasonalScheduleData();

		final TransportFacilityModel depTransportFacility = bcfTransportFacilityService
				.getTransportFacilityForCode(String.valueOf(params.get(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_PORT)));
		scheduleData.setDeparturePort(depTransportFacility.getCode());
		scheduleData.setDepartureTerminal(depTransportFacility.getName());
		scheduleData.setDepartureLocation(depTransportFacility.getLocation().getName());

		final TransportFacilityModel arrTransportFacility = bcfTransportFacilityService
				.getTransportFacilityForCode(String.valueOf(params.get(BcfCoreConstants.SEASONAL_SCHEDULE_ARRIVAL_PORT)));
		scheduleData.setArrivalPort(arrTransportFacility.getCode());
		scheduleData.setArrivalTerminal(arrTransportFacility.getName());
		scheduleData.setArrivalLocation(arrTransportFacility.getLocation().getName());

		final List<ScheduleDate> scheduleDates = getDateScheduleForRoute(
				String.valueOf(params.get(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_PORT)),
				String.valueOf(params.get(BcfCoreConstants.SEASONAL_SCHEDULE_ARRIVAL_PORT)));
		scheduleData.setScheduleDates(scheduleDates);

		if (!params.containsKey(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_FROM) || !params
				.containsKey(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_TO))
		{
			final Date todaysDate = new Date();
			if (CollectionUtils.isNotEmpty(scheduleDates))
			{
				updateScheduleDateParams(params, scheduleDates, todaysDate);

			}
			else
			{
				params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_FROM,
						DateFormatUtils.format(todaysDate, BcfCoreConstants.SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD));

				params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_TO,
						DateFormatUtils
								.format(DateUtils.addDays(todaysDate, 1), BcfCoreConstants.SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD));
			}

		}

		final SeasonalSchedulesData seasonalSchedules = getSchedulesIntegrationService().getSeasonalSchedules(params);
		scheduleData.setSchedulesData(seasonalSchedules);

		this.processSchedules(scheduleData);

		return scheduleData;
	}
	@Override
	public  Map<String, List<TravelRouteInfo>> getTravelRouteInfoWithRouteRegion(
			final List<RouteRegion> routeRegionList)
	{
		final Map<String, List<TravelRouteInfo>> travelRoutesByRegion = new HashMap<>();

		routeRegionList.stream().forEach(routeRegion -> {
			final List<TravelRouteModel> travelRouteList = bcfTravelRouteService.getTravelRoutesByRegions(routeRegion);
			final List<TravelRouteModel> filteredTravelRouteList = travelRouteList.stream()
					.filter(travelRouteData -> Objects.nonNull(travelRouteData.getIndirectRoute()) && (Boolean.FALSE).equals(travelRouteData
							.getIndirectRoute())).collect(Collectors.toList());
			final List<TravelRouteInfo> travelRoutes = new ArrayList<>();
			travelRoutes.addAll(Converters.convertAll(filteredTravelRouteList, currentConditionsTravelRouteConverter));
			travelRoutesByRegion.put(routeRegion.getCode(),travelRoutes);
		});
		return travelRoutesByRegion;
	}

	private void updateScheduleDateParams(final Map<String, Object> params, final List<ScheduleDate> scheduleDates,
			final Date todaysDate)
	{
		ScheduleDate sd = null;
		for (final ScheduleDate scheduleDate : scheduleDates)
		{
			if (scheduleDate.getTo().after(todaysDate) && (Objects.isNull(sd) || scheduleDate.getTo().before(sd.getTo())))//NOSONAR
			{
				sd = scheduleDate;
			}
		}
		if(sd!=null)
		{
			params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_FROM,
					DateFormatUtils.format(sd.getFrom(), BcfCoreConstants.SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD));
			params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_TO,
					DateFormatUtils.format(sd.getTo(), BcfCoreConstants.SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD));
		}
	}

	private void processSchedules(final SeasonalScheduleData seasonalScheduleData)
	{
		final SeasonalSchedulesData seasonalSchedulesData = seasonalScheduleData.getSchedulesData();
		if (CollectionUtils.isEmpty(seasonalSchedulesData.getSchedules()))
		{
			return;
		}

		for (final com.bcf.integration.schedule.SchedulesData schedulesData : seasonalSchedulesData.getSchedules())
		{
			processScheduleTimeStamp(schedulesData);
		}
	}

	private void processScheduleTimeStamp(final com.bcf.integration.schedule.SchedulesData schedulesData)
	{
		try
		{
			final DateTimeFormatter hHmm = DateTimeFormatter.ofPattern("HHmm");
			final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("h:mm a");

			schedulesData.setDepartureTimeDisplay(dateTimeFormatter.format(LocalTime.parse(schedulesData.getDepartureTime(), hHmm)));
			schedulesData.setArrivalTimeDisplay(dateTimeFormatter.format(LocalTime.parse(schedulesData.getArrivalTime(), hHmm)));

			final String scheduleDays = schedulesData.getScheduleDays();
			if (StringUtils.isNotEmpty(scheduleDays))
			{
				final LocalDate ldt = LocalDate.now();
				final DateTimeFormatter dayDateFormatter = DateTimeFormatter.ofPattern("EEE MMM dd");
				final String substring = scheduleDays.substring(scheduleDays.indexOf(':') + 1);
				final String[] dates = substring.split(",");
				final List<String> sailingDatesList = new ArrayList<>();
				for (final String d : dates)
				{

					String st = d.trim().toLowerCase();
					final String charToCapitalise = st.substring(st.length() - 3, st.length() - 2);
					st = st.replace(charToCapitalise, charToCapitalise.toUpperCase());

					final LocalDate ld = LocalDate.parse(ldt.getYear() + st, DateTimeFormatter.ofPattern("yyyydd MMM"));
					sailingDatesList.add(dayDateFormatter.format(ld));
				}
				schedulesData.setSailingDatesList(sailingDatesList);
			}
		}
		catch (final DateTimeParseException dtEx)
		{
			LOG.error(dtEx.getMessage(), dtEx);
		}
	}

	private List<ScheduleDate> getDateScheduleForRoute(final String origin, final String destination)
	{
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(origin, destination);

		final List<TravelRouteModel> travelRouteModels = travelRoutes.stream().filter(travelRouteModel ->
				Objects.nonNull(travelRouteModel.getIndirectRoute()) && BooleanUtils.isFalse(travelRouteModel.getIndirectRoute())
		).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(travelRouteModels))
		{
			return this.getTravelScheduleForRoute(travelRouteModels.get(0));
		}
		LOG.error("Seasonal Schedules dates are not configured for the route " + origin + "-" + destination);
		return Collections.emptyList();
	}


	private List<ScheduleDate> getTravelScheduleForRoute(final TravelRouteModel travelRouteModel)
	{
		final Date today = new Date();
		final List<SeasonalScheduleModel> seasonalScheduleModels = travelRouteModel.getSeasonalSchedules().stream()
				.filter(seasonalScheduleModel ->
						seasonalScheduleModel.getTo().after(today)
				).collect(Collectors.toList());

		return Converters.convertAll(seasonalScheduleModels, getSchedulesDateConverter());
	}

	@Override
	public Map<String, String> getSailingDurations(final SeasonalSchedulesData seasonalSchedulesData)
	{
		final List<com.bcf.integration.schedule.SchedulesData> schedules = seasonalSchedulesData.getSchedules();
		final Map<String, String> minMaxDurations = new HashMap<>(2);

		if (reactor.util.CollectionUtils.isEmpty(schedules))
		{
			LOG.error("scheduledRoutesData must not be empty");
			return minMaxDurations;
		}

		long min = 0, max = 0;
		for (final com.bcf.integration.schedule.SchedulesData schedulesData : schedules)
		{
			try
			{
				final DateTimeFormatter df = DateTimeFormatter
						.ofPattern(BcfCoreConstants.SEASONAL_SCHEDULE_DATE_FORMAT_WITH_HOURS_MIN);
				final LocalDateTime depLdt = LocalDateTime
						.parse(seasonalSchedulesData.getDateFrom() + schedulesData.getDepartureTime(), df);

				final int dtNum = Integer.parseInt(schedulesData.getDepartureTime());
				final int atNum = Integer.parseInt(schedulesData.getArrivalTime());
				LocalDateTime arrLdt;
				if (atNum > dtNum)
				{
					arrLdt = LocalDateTime.parse(seasonalSchedulesData.getDateFrom() + schedulesData.getArrivalTime(), df);
				}
				else
				{
					arrLdt = LocalDateTime.parse(seasonalSchedulesData.getDateFrom() + schedulesData.getArrivalTime(), df);
					arrLdt = arrLdt.plusDays(1);
				}

				final long diff = ChronoUnit.MINUTES.between(depLdt, arrLdt);
				if (diff < min || min == 0)
				{
					min = diff;
				}
				if (max == 0 || diff > max)
				{
					max = diff;
				}
			}
			catch (final DateTimeParseException dtEx)
			{
				LOG.error(dtEx.getMessage(), dtEx);
			}
		}

		minMaxDurations.put("minDuration", BCFDateUtils.convertIntoHoursAndMin(min));
		minMaxDurations.put("maxDuration", BCFDateUtils.convertIntoHoursAndMin(max));

		return minMaxDurations;
	}

	@Override
	public Map<String, List<TravelRouteData>> getAllSchedules()
	{
		final Map<String, List<TravelRouteData>> routesPerRegions = new HashMap<>();
		final List<RouteRegion> routeRegionList = enumerationService.getEnumerationValues(RouteRegion._TYPECODE);
		routeRegionList.stream().forEach(routeRegion -> {
			routesPerRegions.put(getConfigurationService().getConfiguration().getString(routeRegion.getCode() + ".region.color"),
					bcfTravelLocationFacade.getTravelRoutesByRegions(routeRegion.getCode()).stream()
							.filter(travelRouteData -> Objects.nonNull(travelRouteData.isIndirectRoute()) && (Boolean.FALSE).equals(travelRouteData
									.isIndirectRoute())).collect(Collectors.toList()));
		});
		return routesPerRegions;
	}


	@Override
	public SchedulesData getTravelRouteSchedulesForTransferSailingCode(final String sailingCode)
	{
		final List<TransportOfferingModel> transportOfferings = getBcftransportOfferingService()
				.getTransportOfferingsForTransferIdentifier(sailingCode);
		if (CollectionUtils.isEmpty(transportOfferings))
		{
			throw new UnknownIdentifierException("No transport offerings found for the given code " + sailingCode);
		}

		final SchedulesData schedulesData = new SchedulesData();
		final List<ScheduleData> schedules = new ArrayList<>();
		transportOfferings.stream().forEach(transportOfferingModel -> {
			final ScheduleData scheduleData = new ScheduleData();
			getSchedulePopulator().populate(transportOfferingModel, scheduleData);
			schedules.add(scheduleData);
		});
		schedulesData.setSchedules(schedules);
		return schedulesData;
	}

	private TransportOfferingModel createTransportOffering(final String code)
	{
		final TransportOfferingModel transportOfferingModel = getModelService().create(TransportOfferingModel.class);
		transportOfferingModel.setCode(code);
		transportOfferingModel.setVendor(vendorService.getVendor(BcfCoreConstants.VENDOR_BCFERRIES));
		return transportOfferingModel;
	}

	protected VendorService getVendorService()
	{
		return vendorService;
	}

	public void setVendorService(final VendorService vendorService)
	{
		this.vendorService = vendorService;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ScheduleReversePopulator getScheduleReversePopulator()
	{
		return scheduleReversePopulator;
	}

	public void setScheduleReversePopulator(final ScheduleReversePopulator scheduleReversePopulator)
	{
		this.scheduleReversePopulator = scheduleReversePopulator;
	}

	protected SchedulePopulator getSchedulePopulator()
	{
		return schedulePopulator;
	}

	@Required
	public void setSchedulePopulator(final SchedulePopulator schedulePopulator)
	{
		this.schedulePopulator = schedulePopulator;
	}

	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	protected BcfTransportOfferingService getBcftransportOfferingService()
	{
		return bcftransportOfferingService;
	}

	@Required
	public void setBcftransportOfferingService(final BcfTransportOfferingService bcftransportOfferingService)
	{
		this.bcftransportOfferingService = bcftransportOfferingService;
	}

	protected SchedulesIntegrationService getSchedulesIntegrationService()
	{
		return schedulesIntegrationService;
	}

	@Required
	public void setSchedulesIntegrationService(final SchedulesIntegrationService schedulesIntegrationService)
	{
		this.schedulesIntegrationService = schedulesIntegrationService;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected Converter<SeasonalScheduleModel, ScheduleDate> getSchedulesDateConverter()
	{
		return schedulesDateConverter;
	}

	@Required
	public void setSchedulesDateConverter(
			final Converter<SeasonalScheduleModel, ScheduleDate> schedulesDateConverter)
	{
		this.schedulesDateConverter = schedulesDateConverter;
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	public BcfTravelLocationFacade getBcfTravelLocationFacade()
	{
		return bcfTravelLocationFacade;
	}

	@Required
	public void setBcfTravelLocationFacade(final BcfTravelLocationFacade bcfTravelLocationFacade)
	{
		this.bcfTravelLocationFacade = bcfTravelLocationFacade;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
