/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.schedules;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.core.model.media.MediaModel;
import java.util.List;
import java.util.Map;
import com.bcf.core.enums.RouteRegion;
import com.bcf.facades.schedules.SeasonalScheduleData;
import com.bcf.integration.schedule.SeasonalSchedulesData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.webservices.travel.data.SchedulesData;


public interface BcfSchedulesFacade
{
	void createOrUpdateSchedules(SchedulesData schedulesData) throws Exception;

	ImageData getSchedulesLandingMap(final MediaModel mediaModel) throws CMSItemNotFoundException;

	Map<String, List<TravelRouteInfo>> getTravelRouteInfo();

	void deleteSchedules(String sailingCode);

	SchedulesData getTravelRouteSchedulesForTransferSailingCode(String transferIdentifier);

	SeasonalScheduleData getSeasonalSchedules(final Map<String, Object> params)
			throws IntegrationException;

	Map<String, List<TravelRouteInfo>> getTravelRouteInfoWithRouteRegion(
			final List<RouteRegion> routeRegionList);

	Map<String, String> getSailingDurations(SeasonalSchedulesData schedulesData);

	Map<String, List<TravelRouteData>> getAllSchedules();
}
