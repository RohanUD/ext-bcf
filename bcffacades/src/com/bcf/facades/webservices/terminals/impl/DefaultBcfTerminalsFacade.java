/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.terminals.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import com.bcf.facades.webservices.helper.TravelDataHelper;
import com.bcf.facades.webservices.populators.TerminalReversePopulator;
import com.bcf.facades.webservices.terminals.BcfTerminalsFacade;
import com.bcf.webservices.travel.data.TerminalsData;


public class DefaultBcfTerminalsFacade implements BcfTerminalsFacade
{
	private ModelService modelService;
	private TravelDataHelper travelDataHelper;
	private TerminalReversePopulator terminalReversePopulator;

	@Override
	public void createOrUpdateTerminals(final TerminalsData terminalsData) throws Exception
	{
		Transaction.current().execute(new TransactionBody()
			{
				@Override
				public Object execute()
				{
					terminalsData.getTerminals().stream().forEach(terminalInfo -> {
						final TransportFacilityModel transportFacilityModel = getTravelDataHelper().getOrCreateTransportFacility(terminalInfo.getCode());
						getTerminalReversePopulator().populate(terminalInfo, transportFacilityModel);
						getModelService().saveAll();
					});
					return null;
				}
			}
		);
	}

	@Override
	public void deleteTerminal(final String code)
	{
		getTravelDataHelper().deleteTerminalForCode(code);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public TerminalReversePopulator getTerminalReversePopulator()
	{
		return terminalReversePopulator;
	}

	public void setTerminalReversePopulator(final TerminalReversePopulator terminalReversePopulator)
	{
		this.terminalReversePopulator = terminalReversePopulator;
	}

	public TravelDataHelper getTravelDataHelper()
	{
		return travelDataHelper;
	}

	public void setTravelDataHelper(final TravelDataHelper travelDataHelper)
	{
		this.travelDataHelper = travelDataHelper;
	}

}
