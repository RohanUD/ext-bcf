/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelservices.enums.TransportOfferingStatus;
import de.hybris.platform.travelservices.enums.TransportOfferingType;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.BcfTravelProviderService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.webservices.helper.TravelDataHelper;
import com.bcf.webservices.travel.data.ScheduleData;



public class ScheduleReversePopulator implements Populator<ScheduleData, TransportOfferingModel>
{
	private static final Logger LOG = Logger.getLogger(ScheduleReversePopulator.class);

	private I18NService i18nService;

	private TravelDataHelper travelDataHelper;
	private BcfTravelProviderService bcfTravelProviderService;

	public I18NService getI18nService()
	{
		return i18nService;
	}

	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

	@Override
	public void populate(final ScheduleData source, final TransportOfferingModel target)
	{

		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BcfFacadesConstants.SCHEDULE_SYNC_DATE_FORMAT,
				i18nService.getCurrentLocale());
		simpleDateFormat.setLenient(false);

		target.setEbookingSailingCode(source.getSailingCode());

		target.setTravelSector(getTravelDataHelper()
				.getOrCreateTravelSector(source.getTravelSector(), source.getOriginTerminal(), source.getDestinationTerminal()));
		try
		{
			target.setDepartureTime(simpleDateFormat.parse(source.getDepartureTime()));
		}
		catch (final ParseException e)
		{
			LOG.error("error parsing the departure date for transport offering: " + target.getCode(), e);
		}

		try
		{
			target.setArrivalTime(simpleDateFormat.parse(source.getArrivalTime()));
		}
		catch (final ParseException e)
		{
			LOG.error("error parsing the arrival date for transport offering: " + target.getCode(), e);
		}
		target.setNumber(source.getNumber());
		target.setOriginTransportFacility(getTravelDataHelper().getOrCreateTransportFacility(source.getOriginTerminal()));
		target.setDestinationTransportFacility(getTravelDataHelper().getOrCreateTransportFacility(source.getDestinationTerminal()));
		target.setTransportVehicle(getTravelDataHelper().getOrCreateTransportVehicle(source.getTransportVehicle()));
		if(source.getStatus() != null)
		{
			target.setStatus(TransportOfferingStatus.valueOf(source.getStatus().replaceAll(" ","_")));
		}
		target.setTransferIdentifier(source.getTransferIdentifier());
		target.setActive(Boolean.TRUE);
		target.setType(TransportOfferingType.CROSSING);
		target.setReservable(Boolean.TRUE);
		target.setTravelProvider(getBcfTravelProviderService().getDefaultProviderModel());
	}


	public TravelDataHelper getTravelDataHelper()
	{
		return travelDataHelper;
	}

	public void setTravelDataHelper(final TravelDataHelper travelDataHelper)
	{
		this.travelDataHelper = travelDataHelper;
	}

	protected BcfTravelProviderService getBcfTravelProviderService()
	{
		return bcfTravelProviderService;
	}

	@Required
	public void setBcfTravelProviderService(final BcfTravelProviderService bcfTravelProviderService)
	{
		this.bcfTravelProviderService = bcfTravelProviderService;
	}
}
