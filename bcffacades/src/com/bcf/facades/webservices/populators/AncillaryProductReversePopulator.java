/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.webservices.travel.data.ProductData;



public class AncillaryProductReversePopulator implements Populator<ProductData, ProductModel>
{
	private BcfTransportVehicleService bcfTransportVehicleService;
	private ModelService modelService;
	private CatalogVersionService catalogVersionService;

	@Override
	public void populate(final ProductData source, final ProductModel target)
	{
		target.setCode(source.getCode());
		target.setName(source.getDescription());
		target.setDescription(source.getDescription());
		target.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG, BcfCoreConstants.STAGED_CATALOG_VERSION);
		target.setCatalogVersion(catalogVersion);

		if (CollectionUtils.isNotEmpty(source.getVessels()))
		{
			target.setVehicles(getVehicles(source.getVessels()));
		}

		if (StringUtils.isNotEmpty(source.getStartDate()))
		{
			target.setOnlineDate(TravelDateUtils.getDate(source.getStartDate(), BcfFacadesConstants.PRODUCTS_SYNC_DATE_FORMAT));
		}

		if (StringUtils.isNotEmpty(source.getEndDate()))
		{
			target.setOfflineDate(TravelDateUtils.getDate(source.getEndDate(), BcfFacadesConstants.PRODUCTS_SYNC_DATE_FORMAT));
		}
	}

	private List<TransportVehicleModel> getVehicles(final List<String> vessels)
	{
		final List<TransportVehicleModel> vehicles = new ArrayList<>();
		vessels.stream().forEach(s -> {
			final TransportVehicleModel transportVehicle = getBcfTransportVehicleService().getTransportVehicle(s);
			if (Objects.isNull(transportVehicle))
			{
				throw new UnknownIdentifierException("Vessel with code '" + s + "' not found");
			}
			vehicles.add(transportVehicle);
		});
		return vehicles;
	}

	protected BcfTransportVehicleService getBcfTransportVehicleService()
	{
		return bcfTransportVehicleService;
	}

	@Required
	public void setBcfTransportVehicleService(final BcfTransportVehicleService bcfTransportVehicleService)
	{
		this.bcfTransportVehicleService = bcfTransportVehicleService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
