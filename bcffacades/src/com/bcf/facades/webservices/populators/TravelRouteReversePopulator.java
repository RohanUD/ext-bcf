/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.ArrayList;
import java.util.List;
import com.bcf.facades.webservices.helper.TravelDataHelper;
import com.bcf.webservices.travel.data.RouteInfo;


public class TravelRouteReversePopulator implements Populator<RouteInfo, TravelRouteModel>
{
	private TravelDataHelper travelDataHelper;


	@Override
	public void populate(final RouteInfo source, final TravelRouteModel target) throws ConversionException
	{
		List<TravelSectorModel> travelSectorModels = new ArrayList<>();

		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setOrigin(getTravelDataHelper().getOrCreateTransportFacility(source.getOrigin()));
		target.setActive(Boolean.TRUE);
		target.setDestination(getTravelDataHelper().getOrCreateTransportFacility(source.getDestination()));
		source.getSectors().forEach(sector->{
			travelSectorModels.add(getTravelDataHelper().getOrCreateTravelSector(sector));
		});
		target.setTravelSector(travelSectorModels);
	}

	public TravelDataHelper getTravelDataHelper()
	{
		return travelDataHelper;
	}

	public void setTravelDataHelper(final TravelDataHelper travelDataHelper)
	{
		this.travelDataHelper = travelDataHelper;
	}
}
