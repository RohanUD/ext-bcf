/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.facility.ShipFacilityModel;
import com.bcf.webservices.travel.data.FacilityData;
import com.bcf.webservices.travel.data.ShipInfoData;



public class ShipInfoReversePopulator implements Populator<ShipInfoData, ShipInfoModel>
{
	private ModelService modelService;

	@Override
	public void populate(final ShipInfoData source, final ShipInfoModel target)
	{
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setActive(Boolean.TRUE);
		target.setShipFacilities(populateFacilities(source.getFacilities()));
		target.setOverallLength(source.getOverallLength());
		target.setMaximumDisplacement(source.getMaximumDisplacement());
		target.setCarCapacity(source.getVehicleCapacity());
		target.setTotalTravellerCapacity(source.getTotalTravellerCapacity());
		target.setMaximumSpeed(source.getMaximumSpeed());
		target.setPower(source.getPower());
		target.setLengthThreshold(source.getLengthThreshold());
		target.setWidthThreshold(source.getWidthThreshold());
		target.setHeightThreshold(source.getHeightThreshold());
	}

	private Collection<ShipFacilityModel> populateFacilities(final List<FacilityData> facilities)
	{
		if (CollectionUtils.isEmpty(facilities))
		{
			return Collections.emptyList();
		}

		final List<ShipFacilityModel> facilityModels = new ArrayList<>();
		facilities.stream().forEach(facilityData -> {
			final ShipFacilityModel shipFacility = getModelService().create(ShipFacilityModel.class);
			shipFacility.setCode(facilityData.getCode());
			shipFacility.setShortDescription(facilityData.getName());
			getModelService().save(shipFacility);
		});
		return facilityModels;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
