/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import com.bcf.webservices.travel.data.PassengerType;

public class PassengerTypeReversePopulator implements Populator<PassengerType, PassengerTypeModel> {

    @Override
    public void populate(final PassengerType source, final PassengerTypeModel target) {
        target.setCode(source.getCode());
        target.setMinAge(source.getMinAge());
        target.setMaxAge(source.getMaxAge());
        target.setActive(source.isActive());
    }
}
