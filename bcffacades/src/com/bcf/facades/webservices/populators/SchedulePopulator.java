/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Objects;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.webservices.travel.data.ScheduleData;

public class SchedulePopulator implements Populator<TransportOfferingModel, ScheduleData>
{

	@Override
	public void populate( final TransportOfferingModel source, final ScheduleData target)
	{
		target.setSailingCode(source.getEbookingSailingCode());
		target.setTravelSector(source.getTravelSector().getCode());
		target.setDepartureTime(TravelDateUtils.convertDateToStringDate(source.getDepartureTime(), BcfFacadesConstants.SCHEDULE_SYNC_DATE_FORMAT));
		target.setArrivalTime(TravelDateUtils.convertDateToStringDate(source.getArrivalTime(), BcfFacadesConstants.SCHEDULE_SYNC_DATE_FORMAT));

		if(Objects.nonNull(source.getOriginTransportFacility()))
		{
			target.setOriginTerminal(source.getOriginTransportFacility().getCode());
		}
		if(Objects.nonNull(source.getDestinationTransportFacility()))
		{
			target.setDestinationTerminal(source.getDestinationTransportFacility().getCode());
		}

		if(Objects.nonNull(source.getTransportVehicle()))
		{
			target.setTransportVehicle(source.getTransportVehicle().getCode());
		}
		target.setStatus(source.getStatus().getCode());
		target.setTransferIdentifier(source.getTransferIdentifier());
	}
}
