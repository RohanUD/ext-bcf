/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.webservices.helper.TravelDataHelper;
import com.bcf.webservices.travel.data.TerminalInfo;

public class TerminalReversePopulator implements Populator<TerminalInfo, TransportFacilityModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(TerminalReversePopulator.class);

	private TravelDataHelper travelDataHelper;
	private ModelService modelService;
	private ConfigurationService configurationService;
	private static final String EN = "en";

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final TerminalInfo source, final TransportFacilityModel target)
	{
		final PointOfServiceModel pointOfServiceModel = getTravelDataHelper().getOrCreatePointOfService(source.getCode());
		try
		{
			AddressModel addressModel = pointOfServiceModel.getAddress();
			if (Objects.isNull(addressModel))
			{
				addressModel = modelService.create(AddressModel.class);
				addressModel.setOwner(pointOfServiceModel);
			}

			addressModel.setStreetname(source.getAddressLine1());
			addressModel.setTown(source.getCity());
			addressModel.setPostalcode(source.getPostalCode());
			final CountryModel countryForIsoCode = commonI18NService.getCountry(source.getCountry());
			addressModel.setCountry(countryForIsoCode);
			pointOfServiceModel.setAddress(addressModel);
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error(
					"Country not found for the code " + source.getCountry() + ", while data sync for Terminal " + source.getCode());
		}

		final LocationModel country = getTravelDataHelper().getOrCreateLocation(source.getCountry());
		country.setLocationType(LocationType.COUNTRY);

		final LocationModel province = getTravelDataHelper()
				.getOrCreateLocation(getConfigurationService().getConfiguration().getString(BcfFacadesConstants.DEFAULT_PROVINCE));
		province.setSuperlocations(Arrays.asList(country));
		province.setLocationType(LocationType.PROVINCE);

		final LocationModel geographicArea = getTravelDataHelper().getOrCreateLocation(source.getGeographicAreaCode());
		geographicArea.setName(source.getGeographicAreaName(),new Locale(EN));
		geographicArea.setSuperlocations(Arrays.asList(province));
		geographicArea.setLocationType(LocationType.GEOGRAPHICAL_AREA);

		final LocationModel city = getTravelDataHelper().getOrCreateLocation(source.getCity());
		city.setSuperlocations(Arrays.asList(geographicArea));
		city.setLocationType(LocationType.CITY);

		target.setCode(source.getCode());
		target.setName(source.getName(),new Locale(EN));
		target.setDescription(source.getDescription());
		target.setPointOfService(Arrays.asList(pointOfServiceModel));
		target.setLocation(city);
		target.setActive(Boolean.TRUE);
	}

	public TravelDataHelper getTravelDataHelper()
	{
		return travelDataHelper;
	}

	public void setTravelDataHelper(final TravelDataHelper travelDataHelper)
	{
		this.travelDataHelper = travelDataHelper;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
