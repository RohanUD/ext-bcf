/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.webservices.travelroutes.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import com.bcf.facades.webservices.helper.TravelDataHelper;
import com.bcf.facades.webservices.populators.TravelRouteReversePopulator;
import com.bcf.facades.webservices.travelroutes.BcfTravelRoutesFacade;
import com.bcf.webservices.travel.data.RoutesData;


public class DefaultBcfTravelRoutesFacade implements BcfTravelRoutesFacade
{
	private ModelService modelService;
	TravelRouteReversePopulator travelRouteReversePopulator;
	private TravelDataHelper travelDataHelper;


	@Override
	public void createOrUpdateRoutes(final RoutesData routesData) throws Exception
	{
		Transaction.current().execute(new TransactionBody()
			{
				public Object execute()
				{
					routesData.getRoutes().stream().forEach(routeData -> {
						TravelRouteModel travelRouteModel =getTravelDataHelper().getOrCreateTravelRoute(routeData.getCode());
						getTravelRouteReversePopulator().populate(routeData, travelRouteModel);
						getModelService().save(travelRouteModel);
					});
					return null;
				}
			}
		);
	}

	@Override
	public void deleteTravelRoute(final String code)
	{
		getTravelDataHelper().deleteTravelRouteForCode(code);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public TravelRouteReversePopulator getTravelRouteReversePopulator()
	{
		return travelRouteReversePopulator;
	}

	public void setTravelRouteReversePopulator(
			final TravelRouteReversePopulator travelRouteReversePopulator)
	{
		this.travelRouteReversePopulator = travelRouteReversePopulator;
	}

	public TravelDataHelper getTravelDataHelper()
	{
		return travelDataHelper;
	}

	public void setTravelDataHelper(final TravelDataHelper travelDataHelper)
	{
		this.travelDataHelper = travelDataHelper;
	}
}
