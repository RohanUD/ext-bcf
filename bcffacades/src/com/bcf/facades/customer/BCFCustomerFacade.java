/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.customer;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AccountTypeData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.travelfacades.facades.customer.TravelCustomerFacade;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.facades.payment.PaymentCardsData;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BCFCustomerFacade extends TravelCustomerFacade
{
	void createGuestUserForAnonymousCheckout(String email, String name, String firstName, String lastName, String phoneNo,
			String phoneType, final String country) throws DuplicateUidException;

	void updateCustomerProfile(CustomerData customerData) throws IntegrationException, DuplicateUidException;

	void updateCustomerPassword(final String key, final String newPassword) throws TokenInvalidatedException, IntegrationException;

	void resendVerificationEmail(String email);

	void updateEmail(String newEmail) throws DuplicateUidException, IntegrationException, UnsupportedEncodingException;

	void forgotPassword(final String uid) throws IntegrationException;

	void doRegister(final RegisterData registerData) throws DuplicateUidException, IntegrationException;

	void activateProfile(RegisterData registerData) throws IntegrationException;

	List<AccountTypeData> getCustomerAccountTypes();

	String checkAndgetCustomerIdForAccountValidationToken(String accountValidationKey);

	boolean checkEmailVerified(String email);

	boolean validateActivationToken(String accountActivationKey, String email);

	boolean upgradeAccount(RegisterData populateRegisterData);

	boolean changePassword(final String newPassword) throws IntegrationException;

	boolean isUpdateEmailRequested(final String customerId);

	void verifyEmailChange(String customerId, String accountValidationKey) throws IntegrationException;

	PaymentCardsData getPaymentCards() throws IntegrationException;

	PaymentCardsData getBusinessPaymentCards() throws IntegrationException;

	void updatePaymentCard(CCPaymentInfoData populatepaymentProfileInfo) throws IntegrationException;

	void updateAccountPaymentCard(CTCTCCardData ctcTcCardData) throws IntegrationException;

	void removePaymentCard(final String cardType, String paymentInfoId) throws IntegrationException;

	PaymentInfoModel addPaymentCard(Map<String, String> resultMap) throws IntegrationException;

	TransactionDetails getCTCCardInfo(final Map<String, String> resultMap) throws IntegrationException;
}
