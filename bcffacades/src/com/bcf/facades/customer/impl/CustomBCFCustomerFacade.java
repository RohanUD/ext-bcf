/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.customer.impl;

import static com.bcf.facades.constants.BcfFacadesConstants.ADDRESS_UPDATE;
import static com.bcf.facades.constants.BcfFacadesConstants.NAME_UPDATE;
import static com.bcf.facades.constants.BcfFacadesConstants.PASSWORD_UPDATE;
import static com.bcf.facades.constants.BcfFacadesConstants.PHONE_UPDATE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AccountTypeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.travelfacades.facades.customer.impl.DefaultTravelCustomerFacade;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.customer.service.CustomerUpdateService;
import com.bcf.core.enums.AccountStatusType;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.model.UpdateProfileProcessModel;
import com.bcf.core.service.BcfCustomerService;
import com.bcf.core.services.strategies.BcfCreditCardPaymentInfoCreateStrategy;
import com.bcf.core.services.strategies.BcfCtcTcCardPaymentInfoCreateStrategy;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.integration.integration.payment.CRMPaymentMethodCreationHandler;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.facades.payment.PaymentCardsData;
import com.bcf.facades.payment.handler.impl.MyAccountPaymentHandler;
import com.bcf.facades.strategies.impl.ASMCreateCustomerStrategy;
import com.bcf.facades.strategies.impl.WebCreateCustomerStrategy;
import com.bcf.integration.activedirectory.service.BCFCustomerLDAPService;
import com.bcf.integration.crm.service.CRMPaymentCardsService;
import com.bcf.integration.crm.service.CRMSearchCustomerService;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CRMAddOrUpdatePaymentCardsResponseDTO;
import com.bcf.integration.data.CRMCustomerEmailUpdateResponseDTO;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.CRMGetPaymentCardsResponseDTO;
import com.bcf.integration.data.CRMRemovePaymentCardResponseDTO;
import com.bcf.integration.data.ChangeEmailResponseDTO;
import com.bcf.integration.data.PasswordChangeResponseDTO;
import com.bcf.integration.data.PasswordResetResponseDTO;
import com.bcf.integration.data.PaymentProfileInfo;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integration.data.VerifyCustomerResponseDTO;
import com.bcf.integration.data.VerifyPasswordResetResponseDTO;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class CustomBCFCustomerFacade extends DefaultTravelCustomerFacade implements BCFCustomerFacade
{
	private static final Logger LOG = Logger.getLogger(CustomBCFCustomerFacade.class);

	private static final String SUCCESS = "SUCCESS";
	private static final String UID_NOT_EMPTY = "The field [uid] cannot be empty";
	public static final String FOR_THE_CURRENT_USER = ", for the current user ";

	@Resource(name = "customerLDAPService")
	private BCFCustomerLDAPService customerLDAPService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "crmUpdateCustomerService")
	private CRMUpdateCustomerService crmUpdateCustomerService;

	@Resource(name = "customerRegisterReversePopulator")
	private Populator<RegisterData, CustomerModel> customerRegisterReversePopulator;

	@Resource(name = "addressReversePopulator")
	private Populator<AddressData, AddressModel> addressReversePopulator;

	@Resource(name = "crmSearchCustomerService")
	private CRMSearchCustomerService crmSearchCustomerService;

	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "integrationUtils")
	private IntegrationUtility integrationUtils;

	@Resource(name = "creditCardPaymentInfoConverter")
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

	@Resource(name = "ctcTcCardConverter")
	private Converter<CTCTCCardPaymentInfoModel, CTCTCCardData> ctcTcCardPaymentInfoConverter;

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BusinessProcessService businessProcessService;
	private CMSSiteService cmsSiteService;
	private BaseStoreService baseStoreService;
	private BcfCustomerService bcfCustomerService;
	private AssistedServiceService assistedServiceService;
	private ASMCreateCustomerStrategy asmCreateCustomerStrategy;
	private WebCreateCustomerStrategy webCreateCustomerStrategy;
	private CustomerUpdateService customerUpdateService;
	private CRMPaymentCardsService crmPaymentCardsService;
	private Populator<CCPaymentInfoData, PaymentInfoModel> cardPaymentInfoReverseConverter;
	private Populator<CTCTCCardData, CTCTCCardPaymentInfoModel> crmCtcTcPaymentInfoReversePopulator;
	private MyAccountPaymentHandler myAccountPaymentHandler;
	private BcfCreditCardPaymentInfoCreateStrategy bcfCreditCardPaymentInfoCreateStrategy;
	private BcfCtcTcCardPaymentInfoCreateStrategy bcfCtcTcCardPaymentInfoCreateStrategy;
	private BcfUserFacade bcfUserFacade;
	private Map<String, CRMPaymentMethodCreationHandler> crmPaymentMethodCreationHandlerMap;

	@Override
	public void doRegister(final RegisterData registerData) throws DuplicateUidException, IntegrationException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		if (Objects.nonNull(assistedServiceService.getAsmSession()))
		{
			getAsmCreateCustomerStrategy().createCustomer(registerData);
		}
		else
		{
			getWebCreateCustomerStrategy().createCustomer(registerData);
		}
	}

	@Override
	public void forgottenPassword(final String uid)
	{
		Assert.hasText(uid, UID_NOT_EMPTY);

		try
		{
			processForgotPassword(uid);
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	private void processForgotPassword(final String uid) throws IntegrationException
	{
		final PasswordResetResponseDTO response = getCustomerLDAPService().resetPassword(uid);
		if (Objects.isNull(response))
		{
			LOG.error("Response for forgot password is empty for the UID " + uid);
			return;
		}

		CustomerModel customerModel = null;
		try
		{
			customerModel = getUserService().getUserForUID(uid.toLowerCase(), CustomerModel.class);
			customerModel.setPasswordRecoveryKey(response.getPasswordRecoveryKey());
			getCustomerAccountService().forgottenPassword(customerModel);
		}
		catch (final UnknownIdentifierException unEx)
		{
			LOG.error("Customer not found for the given ID " + uid);
		}

		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final ForgottenPasswordProcessModel forgottenPasswordProcessModel = getBusinessProcessService()
				.createProcess("forgottenPassword-" + uid + "-" + System.currentTimeMillis(), "forgottenPasswordEmailProcess");
		forgottenPasswordProcessModel.setToken(response.getPasswordRecoveryKey());
		executeBusinessProcess(forgottenPasswordProcessModel, customerModel, siteModel);
	}

	@Override
	public void resendVerificationEmail(final String uid)
	{
		Assert.hasText(uid, UID_NOT_EMPTY);
		final CustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(), CustomerModel.class);
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = getBusinessProcessService()
				.createProcess(
						"customerRegistrationEmailProcess-" + customerModel.getUid() + "-" + System.currentTimeMillis(),
						"customerRegistrationEmailProcess");
		executeBusinessProcess(storeFrontCustomerProcessModel, customerModel, siteModel);
	}


	private void executeBusinessProcess(final StoreFrontCustomerProcessModel businessProcessModel,
			final CustomerModel customerModel, final CMSSiteModel siteModel)
	{
		businessProcessModel.setSite(siteModel);
		businessProcessModel.setCustomer(customerModel);
		businessProcessModel.setLanguage(getCommonI18NService().getCurrentLanguage());
		businessProcessModel.setCurrency(getCommonI18NService().getCurrentCurrency());
		businessProcessModel.setStore(getBaseStoreService().getCurrentBaseStore());
		getModelService().save(businessProcessModel);
		getBusinessProcessService().startProcess(businessProcessModel);
	}

	@Override
	public void updateCustomerPassword(final String key, final String newPassword)
			throws TokenInvalidatedException, IntegrationException
	{
		Assert.hasText(key, "The field [key] cannot be empty");
		Assert.hasText(newPassword, "The field [newPassword] cannot be empty");
		final VerifyPasswordResetResponseDTO response = getCustomerLDAPService().verifyPasswordReset(key, newPassword);
		if (response == null)
		{
			LOG.error("Response received from verifyPasswordAtLDAP() service is null");
			throw new IllegalArgumentException();
		}
		ldapUpdatePassword(key, newPassword);
	}

	@Override
	public void updateEmail(final String newEmail) throws DuplicateUidException, IntegrationException, UnsupportedEncodingException
	{
		final CustomerModel currentUser = (CustomerModel) getCurrentUser();
		UserModel userForUID = null;

		// Check if the user already exists in Hybris
		try
		{
			userForUID = getUserService().getUserForUID(newEmail);
			if (Objects.nonNull(userForUID))
			{
				throw new DuplicateUidException(
						"Customer already exists in Hybris with the new email ID provided " + newEmail + FOR_THE_CURRENT_USER
								+ currentUser.getUid());
			}
		}
		catch (final UnknownIdentifierException ukEx)
		{
			//No user found with the new email in Hybris, next we check with CRM
			LOG.info("Change email request - No user found for the new email " + newEmail + FOR_THE_CURRENT_USER + currentUser
					.getUid());
		}

		// Check if the user already exists in CRM
		try
		{
			boolean guestind = false;

			if (userForUID != null && userForUID instanceof CustomerModel)
			{
				guestind = ((CustomerModel) userForUID).isConsumer();
			}

			final CRMGetCustomerResponseDTO customer = crmSearchCustomerService.getCustomer(newEmail, guestind);
			if (Objects.nonNull(customer) && Objects.nonNull(customer.getBCF_Customer()))
			{
				throw new DuplicateUidException(
						"Customer already exists in CRM with the new email ID provided " + newEmail + FOR_THE_CURRENT_USER
								+ currentUser.getUid());
			}
		}
		catch (final IntegrationException intEx)
		{
			//No user found with the new email in CRM, next we request LDAP to change email
			LOG.info("Change email request - No user found for the new email " + newEmail + FOR_THE_CURRENT_USER + currentUser
					.getUid());
		}

		// Req LDAP to change email
		final ChangeEmailResponseDTO changeEmailResponseDTO = customerLDAPService.changeEmail(getCurrentUser().getUid(), newEmail);

		currentUser.setAccountStatus(AccountStatusType.UPDATE_EMAIL);
		currentUser.setRequestedUpdateEmail(newEmail);
		currentUser.setAccountValidationKey(changeEmailResponseDTO.getAccountValidationKey());
		final Date expDate = DateUtils.addHours(new Date(), Integer.parseInt(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCOUNT_ACTIVATION_EXPIRY_TIME)));
		currentUser.setAccountActivationKeyExpiryTime(expDate);
		getModelService().save(currentUser);
		sendEmailProcess(currentUser);
	}

	@Override
	public void forgotPassword(final String uid) throws IntegrationException
	{
		processForgotPassword(uid);
	}

	@Override
	public void activateProfile(final RegisterData registerData) throws IntegrationException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel customerModel = getUserService()
				.getUserForUID(registerData.getLogin().toLowerCase(), CustomerModel.class);

		customerRegisterReversePopulator.populate(registerData, customerModel);
		updateOrCreateAddress(registerData, customerModel);

		try
		{
			this.updateCRMCustomer(customerModel);
			getModelService().save(customerModel);
		}
		catch (final IntegrationException intEx)
		{
			LOG.error("Exception while updating the Customer" + customerModel.getUid() + ", with CRM", intEx);
			// Revert the clear the account type, so that the profile activation marked as not completed.
			customerModel.setAccountType(null);
			getModelService().save(customerModel);

			// Throw it again to catch it by the controller to redirect to the page with error message.
			throw intEx;
		}
	}

	//Create/Update customer at CRM
	private void updateCRMCustomer(final CustomerModel customerModel) throws IntegrationException
	{
		final UpdateCRMCustomerResponseDTO response = crmUpdateCustomerService.updateCustomer(customerModel);
		getCustomerUpdateService().updateSystemIdentifiers(customerModel, response.getBCF_Customer().getSystemIdentifier());
		getModelService().save(customerModel);
	}

	private void updateOrCreateAddress(final RegisterData registerData, final CustomerModel customerModel)
	{
		if (AccountType.FULL_ACCOUNT.getCode().equals(registerData.getAccountType()))
		{
			AddressModel defaultAddress = customerModel.getAddresses().stream().filter(AddressModel::getPrimaryInd).findFirst()
					.orElse(null);
			boolean defaultAddressFound = true;
			if (Objects.isNull(defaultAddress))
			{
				defaultAddress = getModelService().create(AddressModel.class);
				defaultAddress.setOwner(customerModel);
				defaultAddressFound = false;
			}

			defaultAddress.setPrimaryInd(Boolean.TRUE);
			defaultAddress.setShippingAddress(Boolean.TRUE);

			final CountryModel countryModel = getCommonI18NService().getCountry(registerData.getCountry());
			defaultAddress.setLine1(registerData.getAddressLine1());
			defaultAddress.setLine2(registerData.getAddressLine2());
			defaultAddress.setTown(registerData.getCity());

			defaultAddress.setCountry(countryModel);
			defaultAddress.setPrimaryInd(Boolean.TRUE);
			defaultAddress.setPostalcode(registerData.getZipCode());

			if (StringUtils.isNotEmpty(registerData.getProvince()) && !StringUtils.equals(registerData.getProvince(), "-1"))
			{
				final RegionModel regionModel = getCommonI18NService().getRegion(
						getCommonI18NService().getCountry(registerData.getCountry()), registerData.getProvince());
				defaultAddress.setRegion(regionModel);
			}
			else
			{
				defaultAddress.setRegion(null);
			}

			getModelService().saveAll(customerModel, defaultAddress);
			if (!defaultAddressFound)
			{
				getCustomerAccountService().setDefaultAddressEntry(customerModel, defaultAddress);
			}
		}
		else
		{
			if (CollectionUtils.isEmpty(customerModel.getAddresses()))
			{
				return;
			}
			customerModel.getAddresses().stream().forEach(addressModel -> {
				if (Objects.nonNull(addressModel.getPrimaryInd()) && addressModel.getPrimaryInd().equals(Boolean.TRUE))
				{
					getModelService().remove(addressModel);
				}
			});
		}
	}

	@Override
	public List<AccountTypeData> getCustomerAccountTypes()
	{
		final List<AccountTypeData> accountTypes = new ArrayList<>();
		final List<AccountType> enumerationValues = enumerationService.getEnumerationValues(AccountType.class);
		enumerationValues.stream().forEach(accountType -> {
			final AccountTypeData accountTypeData = new AccountTypeData();
			accountTypeData.setCode(accountType.getCode());
			accountTypeData.setName(enumerationService.getEnumerationName(accountType));
			accountTypes.add(accountTypeData);
		});
		return accountTypes;
	}

	private void ldapUpdatePassword(final String key, final String newPassword) throws TokenInvalidatedException
	{
		Assert.hasText(key, "The field [key] cannot be empty");
		Assert.hasText(newPassword, "The field [newPassword] cannot be empty");



		final CustomerModel customer = getBcfCustomerService().getCustomerForPasswordRecoveryKey(key);
		if (customer == null)
		{
			throw new IllegalArgumentException("user for Key not found");
		}
		final Date date = customer.getPasswordRecoveryKeyExpiryTime();
		if (!date.after(new Date()))
		{
			throw new IllegalArgumentException("password link has expired");
		}
		if (!key.equals(customer.getPasswordRecoveryKey()))
		{
			throw new TokenInvalidatedException();
		}
		customer.setLoginDisabled(false);
		getModelService().save(customer);
	}

	@Override
	public String checkAndgetCustomerIdForAccountValidationToken(final String accountValidationKey)
	{
		final CustomerModel customerModel = getBcfCustomerService()
				.getCustomerAccountValidationKey(accountValidationKey);
		return Objects.nonNull(customerModel) ? customerModel.getUid() : null;
	}

	@Override
	public boolean checkEmailVerified(final String email)
	{
		final CustomerModel customer = getCustomerForUid(email);
		return (Objects.nonNull(customer) && Objects.nonNull(customer.getAccountStatus())) && AccountStatusType.VERIFIED //NOSONAR
				.equals(customer.getAccountStatus());
	}

	@Override
	public boolean validateActivationToken(final String accountActivationKey, final String email)
	{
		final CustomerModel customer = getCustomerForUid(email);

		if (Objects.isNull(customer) || !(AccountStatusType.PENDING.equals(customer.getAccountStatus()) || //NOSONAR
				AccountStatusType.UPDATE_EMAIL.equals(customer.getAccountStatus())) ||
				!StringUtils.equals(customer.getAccountValidationKey(), accountActivationKey)
				|| new Date().compareTo(customer.getAccountActivationKeyExpiryTime()) > 0)
		{
			LOG.debug("Account not verified, User not found for UID:" + email);
			return false;
		}
		return true;
	}

	@Override
	public boolean upgradeAccount(final RegisterData registerData)
	{
		// Call integration service
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		customerModel.setAccountType(AccountType.FULL_ACCOUNT);
		customerModel.setConsumer(Boolean.FALSE);
		customerModel.setPhoneNo(registerData.getPhoneNo());
		AddressModel defaultAddress = this.getPrimaryAddress(customerModel);
		if (Objects.isNull(defaultAddress))
		{
			defaultAddress = getModelService().create(AddressModel.class);
			defaultAddress.setOwner(customerModel);
			defaultAddress.setPrimaryInd(Boolean.TRUE);
			defaultAddress.setShippingAddress(Boolean.TRUE);
			customerModel.setDefaultShipmentAddress(defaultAddress);
			final List<AddressModel> addressModels = new ArrayList<AddressModel>(customerModel.getAddresses());
			addressModels.add(defaultAddress);
			customerModel.setAddresses(addressModels);
		}

		defaultAddress.setTown(registerData.getCity());
		defaultAddress.setPostalcode(registerData.getZipCode());

		final CountryModel countryModel = getCommonI18NService().getCountry(registerData.getCountry());
		defaultAddress.setCountry(countryModel);

		if (StringUtils.isNotEmpty(registerData.getProvince()) && !StringUtils.equals(registerData.getProvince(), "-1"))
		{
			final RegionModel regionModel = getCommonI18NService().getRegion(countryModel, registerData.getProvince());
			defaultAddress.setRegion(regionModel);
		}
		else
		{
			defaultAddress.setRegion(null);
		}

		try
		{
			crmUpdateCustomerService.updateCustomer(customerModel);
			getModelService().saveAll(defaultAddress, customerModel);
		}

		catch (final IntegrationException intEx)
		{
			LOG.error(intEx.getMessage(), intEx);
			return false;
		}
		return true;
	}

	private AddressModel getPrimaryAddress(final CustomerModel customerModel)
	{
		if (Objects.isNull(customerModel) | CollectionUtils.isEmpty(customerModel.getAddresses()))
		{
			return null;
		}

		return customerModel.getAddresses().stream().filter(AddressModel::getPrimaryInd).findFirst().orElse(null);
	}

	@Override
	public boolean changePassword(final String newPassword) throws IntegrationException
	{
		final PasswordChangeResponseDTO passwordChangeResponseDTO = getCustomerLDAPService()
				.changePassword(getUserService().getCurrentUser().getUid(), newPassword);
		if (Objects.nonNull(passwordChangeResponseDTO) && SUCCESS.equals(passwordChangeResponseDTO.getStatus()))
		{
			final CustomerModel customerModel = getCurrentSessionCustomer();
			final List<String> updateProfileDataList = new ArrayList<>();
			final String passwordUpdateLabel = getPropertySourceFacade().getPropertySourceValueMessage(PASSWORD_UPDATE, null);
			updateProfileDataList.add(passwordUpdateLabel);
			startBusinessProcess(customerModel, updateProfileDataList);
			return true;
		}
		return false;
	}

	@Override
	public boolean isUpdateEmailRequested(final String customerId)
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getUserForUID(customerId);
		return StringUtils.isNotEmpty(currentUser.getRequestedUpdateEmail());
	}

	@Override
	public void verifyEmailChange(final String customerId, final String accountValidationKey) throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getUserForUID(customerId);
		final String requestedUpdateEmail = currentUser.getRequestedUpdateEmail();
		final VerifyCustomerResponseDTO customerResponseDTO = getCustomerLDAPService()
				.verifyEmailChangeAtLDAP(accountValidationKey, customerId, requestedUpdateEmail);

		if (!StringUtils.equals(BcfFacadesConstants.VERIFIED, customerResponseDTO.getAccountStatus()))
		{
			throw new IntegrationException(
					"Verify email change at LDAP hasn't been successful for the customer id: " + customerId + ", token: "
							+ accountValidationKey);
		}

		// Update CRM with email change
		final CRMCustomerEmailUpdateResponseDTO crmCustomerEmailUpdateResponseDTO = crmUpdateCustomerService
				.updateEmail(currentUser, requestedUpdateEmail);
		if (!StringUtils.equals(BcfFacadesConstants.SUCCESS, crmCustomerEmailUpdateResponseDTO.getResult()))
		{
			throw new IntegrationException(
					"Update email change at CRM hasn't been successful for the customer id: " + customerId + ", token: "
							+ accountValidationKey);
		}

		currentUser.setUid(requestedUpdateEmail);
		currentUser.setRequestedUpdateEmail(null);
		getModelService().save(currentUser);
	}

	@Override
	public PaymentCardsData getPaymentCards() throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final CRMGetPaymentCardsResponseDTO paymentCardsResData = getCrmPaymentCardsService().getPaymentsCards(currentUser);
		this.createOrRemovePaymentInfos(currentUser, paymentCardsResData);
		final PaymentCardsData paymentCardsData = new PaymentCardsData();
		final List<CCPaymentInfoData> ccPaymentInfos = getUserFacade().getCCPaymentInfos(true);
		paymentCardsData.setSavedCards(ccPaymentInfos);
		final List<CTCTCCardData> ctcTcCards = getBcfUserFacade().getCtcTcCardsPaymentInfos();
		paymentCardsData.setSavedCtcTcCards(ctcTcCards);
		return paymentCardsData;
	}

	@Override
	public PaymentCardsData getBusinessPaymentCards() throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final PaymentCardsData paymentCardsData = new PaymentCardsData();
		final CRMGetPaymentCardsResponseDTO paymentCardsResData = getCrmPaymentCardsService().getPaymentsCards(currentUser);
		this.createOrRemovePaymentInfos(currentUser, paymentCardsResData);
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			final List<PaymentInfoModel> savedPaymentInfoModels = StreamUtil.safeStream(currentUser.getPaymentInfos()).filter(
					paymentInfo -> Objects.nonNull(paymentInfo.getB2bUnit()) && paymentInfo.isSaved() && StringUtils
							.equalsIgnoreCase(b2bCheckoutContextData.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM),
									paymentInfo.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM)))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(savedPaymentInfoModels))
			{
				final List<CreditCardPaymentInfoModel> creditCards = new ArrayList<>();
				final List<CTCTCCardPaymentInfoModel> internalCards = new ArrayList<>();
				StreamUtil.safeStream(savedPaymentInfoModels).forEach(paymentInfoModel -> {
					if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
					{
						creditCards.add(CreditCardPaymentInfoModel.class.cast(paymentInfoModel));
					}
					if (paymentInfoModel instanceof CTCTCCardPaymentInfoModel)
					{
						internalCards.add(CTCTCCardPaymentInfoModel.class.cast(paymentInfoModel));
					}
				});
				if (CollectionUtils.isNotEmpty(creditCards))
				{
					final List<CCPaymentInfoData> ccPaymentInfos = Converters.convertAll(creditCards, creditCardPaymentInfoConverter);
					paymentCardsData.setSavedCards(ccPaymentInfos);
				}
				if (CollectionUtils.isNotEmpty(internalCards))
				{
					final List<CTCTCCardData> ctcTcCards = Converters.convertAll(internalCards, ctcTcCardPaymentInfoConverter);
					paymentCardsData.setSavedCtcTcCards(ctcTcCards);
				}
			}
		}
		return paymentCardsData;
	}

	@Override
	public void updateAccountPaymentCard(final CTCTCCardData ctcTcCardData) throws IntegrationException
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = ((BcfCustomerAccountService) getCustomerAccountService())
				.getCtcTcCardPaymentInfoForCode(customerModel, ctcTcCardData.getCode());

		getCrmCtcTcPaymentInfoReversePopulator().populate(ctcTcCardData, ctcTcCardPaymentInfoModel);
		final CRMAddOrUpdatePaymentCardsResponseDTO crmAddOrUpdatePaymentCardsResponseDTO = getCrmPaymentCardsService()
				.updateAccountPaymentCard(ctcTcCardPaymentInfoModel);
		if (Objects.nonNull(crmAddOrUpdatePaymentCardsResponseDTO) && Objects
				.nonNull(crmAddOrUpdatePaymentCardsResponseDTO.getPaymentProfileInfo()))
		{
			getModelService().saveAll(ctcTcCardPaymentInfoModel.getBillingAddress(), ctcTcCardPaymentInfoModel);
		}
	}

	@Override
	public void updatePaymentCard(final CCPaymentInfoData ccPaymentInfoData) throws IntegrationException
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		final CreditCardPaymentInfoModel cardPaymentInfoModel = getCustomerAccountService()
				.getCreditCardPaymentInfoForCode(customerModel, ccPaymentInfoData.getId());
		if (Objects.isNull(cardPaymentInfoModel.getBillingAddress()))
		{
			final AddressModel addressModel = getModelService().create(AddressModel.class);
			addressModel.setOwner(customerModel);
			cardPaymentInfoModel.setBillingAddress(addressModel);
		}
		getCardPaymentInfoReverseConverter().populate(ccPaymentInfoData, cardPaymentInfoModel);

		final CRMAddOrUpdatePaymentCardsResponseDTO crmAddOrUpdatePaymentCardsResponseDTO = getCrmPaymentCardsService()
				.updatePaymentCards(cardPaymentInfoModel);
		if (Objects.nonNull(crmAddOrUpdatePaymentCardsResponseDTO) && Objects
				.nonNull(crmAddOrUpdatePaymentCardsResponseDTO.getPaymentProfileInfo()))
		{
			getModelService().saveAll(cardPaymentInfoModel.getBillingAddress(), cardPaymentInfoModel);
		}
	}


	@Override
	public void removePaymentCard(final String cardType, final String paymentInfoId) throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final PaymentInfoModel paymentInfoModel = getPaymentCardByType(currentUser, cardType, paymentInfoId);
		if (Objects.isNull(paymentInfoModel))
		{
			throw new ModelNotFoundException(
					"Could not find the payment method with id " + paymentInfoId + ", for the customer " + currentUser.getUid());
		}

		final CRMRemovePaymentCardResponseDTO crmRemovePaymentCardResponseDTO = getCrmPaymentCardsService()
				.removePaymentCard(currentUser, paymentInfoModel.getCode());
		if (Objects.nonNull(crmRemovePaymentCardResponseDTO) && Objects
				.nonNull(crmRemovePaymentCardResponseDTO.getPaymentProfileInfo()) && Objects
				.equals(crmRemovePaymentCardResponseDTO.getPaymentProfileInfo().getNumber(), paymentInfoModel.getCode()))
		{
			if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
			{
				getCustomerAccountService().deleteCCPaymentInfo(currentUser, (CreditCardPaymentInfoModel) paymentInfoModel);
			}
			else
			{
				((BcfCustomerAccountService) getCustomerAccountService())
						.deleteCtcTcCardPaymentInfo(currentUser, (CTCTCCardPaymentInfoModel) paymentInfoModel);
			}
		}
	}

	private PaymentInfoModel getPaymentCardByType(final CustomerModel currentUser,
			final String cardType, final String paymentInfoId)
	{

		if (Objects.nonNull(cardType) && Objects.equals(cardType, BcfCoreConstants.CTC_TC_CARD))
		{
			return ((BcfCustomerAccountService) getCustomerAccountService())
					.getCtcTcCardPaymentInfoForCode(currentUser, paymentInfoId);
		}
		return getCustomerAccountService().getCreditCardPaymentInfoForCode(currentUser, paymentInfoId);

	}

	@Override
	public PaymentInfoModel addPaymentCard(final Map<String, String> resultMap) throws IntegrationException
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();

		// CTC-TC Cards Validation and Updating with CRM
		if (StringUtils.isNotBlank(resultMap.get("cardType")) && resultMap.get("cardType").equals(BcfCoreConstants.CTC_TC_CARD))
		{
			final TransactionDetails transactionDetails = this.getCTCCardInfo(resultMap);
			if (!transactionDetails.getIsSuccessful())
			{
				throw new IntegrationException(
						"Failed to get the permanent token, decision is " + transactionDetails.getBcfResponseDescription()
								+ ", for customer " + customerModel.getUid());
			}
			return addAccountCardToCrm(customerModel, resultMap);
		}

		// Credit Cards Validation and Updating with CRM
		resultMap.put(BcfFacadesConstants.Payment.SAVE_CC_CARD, "true");
		final TransactionDetails transactionDetails = getMyAccountPaymentHandler()
				.handlePayment(resultMap, 0);
		if (!transactionDetails.getIsSuccessful())
		{
			throw new IntegrationException(
					"Failed to get the permanent token, decision is " + transactionDetails.getBcfResponseDescription()
							+ ", for customer " + customerModel.getUid());
		}

		return addCreditCardToCrm(customerModel, resultMap, transactionDetails);
	}

	private PaymentInfoModel addAccountCardToCrm(final CustomerModel customerModel, final Map<String, String> resultMap)
			throws IntegrationException
	{
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = getBcfCtcTcCardPaymentInfoCreateStrategy()
				.saveAccountPaymentDetails(customerModel, resultMap);
		getCrmPaymentCardsService().updateAccountPaymentCard(ctcTcCardPaymentInfoModel);
		getModelService().save(ctcTcCardPaymentInfoModel);
		return ctcTcCardPaymentInfoModel;
	}

	private PaymentInfoModel addCreditCardToCrm(final CustomerModel customerModel,
			final Map<String, String> resultMap,
			final TransactionDetails transactionDetails) throws IntegrationException
	{
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = bcfCreditCardPaymentInfoCreateStrategy
				.savePaymentDetails(customerModel, resultMap);
		creditCardPaymentInfoModel.setNumber(transactionDetails.getCardNumberToPrint());
		creditCardPaymentInfoModel.setToken(transactionDetails.getTokenValue());
		creditCardPaymentInfoModel.setTokenType(transactionDetails.getTokenType());
		creditCardPaymentInfoModel.setSaved(true);

		final BcfUserService bcfUserService = (BcfUserService) getUserService();
		if (bcfUserService.isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			creditCardPaymentInfoModel.setB2bUnit(b2bCheckoutContextData.getB2bUnit());
		}

		getCrmPaymentCardsService().updatePaymentCards(creditCardPaymentInfoModel);
		getModelService().saveAll(creditCardPaymentInfoModel.getBillingAddress(), creditCardPaymentInfoModel);
		return creditCardPaymentInfoModel;
	}

	private void createOrRemovePaymentInfos(final CustomerModel currentUser,
			final CRMGetPaymentCardsResponseDTO paymentCardsResData)
	{
		final Map<String, PaymentInfoModel> savedPaymentInfoMap = getSavedPaymentInfoMap(currentUser);
		final List<PaymentInfoModel> itemsToSave = new ArrayList<>();
		if (Objects.nonNull(paymentCardsResData) && CollectionUtils.isNotEmpty(paymentCardsResData.getPaymentProfileInfo()))
		{
			final Map<String, PaymentProfileInfo> paymentProfileInfoMap = getPaymentProfileInfoMap(paymentCardsResData);
			final List<PaymentProfileInfo> infosToCreate = getInfosToCreate(savedPaymentInfoMap, paymentProfileInfoMap);
			if (CollectionUtils.isNotEmpty(infosToCreate))
			{
				LOG.debug(String.format("Creating payment infos not present in hybris for user [%s]", currentUser.getUid()));
				StreamUtil.safeStream(infosToCreate).forEach(paymentProfileInfo -> {
					try
					{
						createPaymentInfo(currentUser, paymentCardsResData, paymentProfileInfo);
					}
					catch (final ModelSavingException msEx)
					{
						LOG.error(msEx.getMessage(), msEx);
						LOG.error("Card creation exception occurred for the card details from CRM Card holder name" + paymentProfileInfo
								.getCreditCardHolderName() + ", card number " + paymentProfileInfo.getNumber() + ", for the customer "
								+ currentUser.getUid());
					}
				});
			}
			final List<PaymentInfoModel> infosToDisable = getInfosToDisable(savedPaymentInfoMap, paymentProfileInfoMap);
			if (CollectionUtils.isNotEmpty(infosToDisable))
			{
				LOG.debug(String.format("Disabling payment infos not present in CRM for user [%s]", currentUser.getUid()));
				StreamUtil.safeStream(infosToDisable).forEach(paymentInfoModel -> paymentInfoModel.setSaved(false));
				itemsToSave.addAll(infosToDisable);
			}
		}
		else
		{
			LOG.debug(
					String.format("Disabling all infos for user/account [%s] as they are not present in CRM", currentUser.getUid()));
			StreamUtil.safeStream(savedPaymentInfoMap.values()).forEach(paymentInfoModel -> paymentInfoModel.setSaved(false));
			itemsToSave.addAll(savedPaymentInfoMap.values());
		}
		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
	}

	private Map<String, PaymentInfoModel> getSavedPaymentInfoMap(final CustomerModel currentUser)
	{
		final List<PaymentInfoModel> savedPaymentInfoModels;
		final List<PaymentInfoModel> itemsToSave = new ArrayList<>();
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			savedPaymentInfoModels = StreamUtil.safeStream(currentUser.getPaymentInfos()).filter(
					paymentInfo -> Objects.nonNull(paymentInfo.getB2bUnit()) && paymentInfo.isSaved() && StringUtils
							.equalsIgnoreCase(b2bCheckoutContextData.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM),
									paymentInfo.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM)))
					.collect(Collectors.toList());
			LOG.debug(String.format("Found [%s] payment infos in hybris for business user [%s]", savedPaymentInfoModels.size(),
					currentUser.getUid()));
		}
		else
		{
			savedPaymentInfoModels = StreamUtil.safeStream(currentUser.getPaymentInfos())
					.filter(paymentInfo -> Objects.isNull(paymentInfo.getB2bUnit()) && paymentInfo.isSaved())
					.collect(Collectors.toList());
			LOG.debug(String.format("Found [%s] payment infos in hybris for personal user [%s]", savedPaymentInfoModels.size(),
					currentUser.getUid()));

		}
		final Map<String, PaymentInfoModel> savedPaymentInfoMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(savedPaymentInfoModels))
		{
			//assuming there are no duplicate saved payment info, if found duplicate, disable it
			StreamUtil.safeStream(savedPaymentInfoModels).forEach(paymentInfo -> {
				if (Objects.nonNull(savedPaymentInfoMap.get(paymentInfo.getCode())))
				{
					savedPaymentInfoMap.put(paymentInfo.getCode(), paymentInfo);
				}
				else
				{
					paymentInfo.setSaved(false);
					itemsToSave.add(paymentInfo);
				}
			});
		}
		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
		LOG.debug(
				String.format("Total [%s] payment infos found for user [%s]", savedPaymentInfoModels.size(), currentUser.getUid()));
		return savedPaymentInfoMap;
	}

	private Map<String, PaymentProfileInfo> getPaymentProfileInfoMap(final CRMGetPaymentCardsResponseDTO paymentCardsResData)
	{
		final Map<String, PaymentProfileInfo> paymentProfileInfoMap = new HashMap<>();
		//assuming there are no duplicate payment profile coming from CRM
		paymentCardsResData.getPaymentProfileInfo().forEach(payInfo -> {
			if (StringUtils.isNotEmpty(payInfo.getNumber()))
			{
				paymentProfileInfoMap.put(payInfo.getNumber(), payInfo);
			}
		});
		return paymentProfileInfoMap;
	}

	private List<PaymentProfileInfo> getInfosToCreate(final Map<String, PaymentInfoModel> savedPaymentInfoMap,
			final Map<String, PaymentProfileInfo> paymentProfileInfoMap)
	{
		final List<PaymentProfileInfo> infosToCreate = new ArrayList<>();
		StreamUtil.safeStream(CollectionUtils.removeAll(paymentProfileInfoMap.keySet(), savedPaymentInfoMap.keySet()))
				.forEach(s -> infosToCreate.add(paymentProfileInfoMap.get(s)));
		return infosToCreate;
	}

	private List<PaymentInfoModel> getInfosToDisable(final Map<String, PaymentInfoModel> savedPaymentInfoMap,
			final Map<String, PaymentProfileInfo> paymentProfileInfoMap)
	{
		final List<PaymentInfoModel> infosToDisable = new ArrayList<>();
		StreamUtil.safeStream(CollectionUtils.removeAll(savedPaymentInfoMap.keySet(), paymentProfileInfoMap.keySet()))
				.forEach(s -> infosToDisable.add(savedPaymentInfoMap.get(s)));
		return infosToDisable;
	}

	private void createPaymentInfo(final CustomerModel currentUser,
			final CRMGetPaymentCardsResponseDTO paymentCardsResData,
			final PaymentProfileInfo paymentProfileInfo)
	{
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			final String unitId = integrationUtils.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM,
					b2bCheckoutContextData.getB2bUnit().getSystemIdentifiers());
			if (!StringUtils.equalsIgnoreCase(unitId, paymentCardsResData.getCustomerId()))
			{
				LOG.error("Units mismatch with logged in user and the card data response. The Logged in user unit ID is " + unitId
						+ ", and the Units ID in the service response is " + paymentCardsResData.getContactId());
				return;
			}
		}
		final PaymentInfoModel paymentInfoModel = getCrmPaymentMethodCreationHandlerMap().get(paymentProfileInfo.getType())
				.createPaymentMethod(currentUser, paymentProfileInfo);
		new ArrayList<>(currentUser.getPaymentInfos()).add(paymentInfoModel);
		getModelService().saveAll(currentUser, paymentInfoModel);
	}

	@Override
	public void updateCustomerProfile(final CustomerData customerData) throws IntegrationException, DuplicateUidException
	{
		final CustomerModel customerModel = getCurrentSessionCustomer();
		final List<String> updateProfileDataList = populateUpdateProfileDataList(customerModel, customerData);
		if (!updateProfileDataList.isEmpty())
		{
			customerModel.setFirstName(customerData.getFirstName());
			customerModel.setLastName(customerData.getLastName());
			customerModel.setPhoneNo(customerData.getContactNumber());
			final AddressModel addressModel;
			if (Objects.nonNull(customerModel.getDefaultShipmentAddress()))
			{
				addressModel = customerModel.getDefaultShipmentAddress();
			}
			else
			{
				addressModel = getModelService().create(AddressModel.class);
				addressModel.setOwner(customerModel);
				addressModel.setPrimaryInd(Boolean.TRUE);
				addressModel.setShippingAddress(Boolean.TRUE);
			}
			addressReversePopulator.populate(customerData.getDefaultAddress(), addressModel);
			customerModel.setDefaultShipmentAddress(addressModel);

			crmUpdateCustomerService.updateCustomer(customerModel);
			final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
			getCustomerAccountService().updateProfile(customerModel, customerData.getTitleCode(), name, customerModel.getUid());
			getModelService().saveAll(addressModel, customerModel);
			startBusinessProcess(customerModel, updateProfileDataList);
		}
		else
		{
			LOG.debug("Profile data has not been changed for customer " + customerData.getEmail());
		}

	}

	private List<String> populateUpdateProfileDataList(final CustomerModel customerModel, final CustomerData customerData)
	{
		final List<String> updateProfileDataList = new ArrayList<>();
		final AddressModel defaultAddress = customerModel.getDefaultShipmentAddress();
		final AddressData defaultAddressData = customerData.getDefaultAddress();

		final String addressUpdateLabel = getPropertySourceFacade().getPropertySourceValueMessage(ADDRESS_UPDATE, null);
		final String nameUpdateLabel = getPropertySourceFacade().getPropertySourceValueMessage(NAME_UPDATE, null);
		final String phoneUpdateLabel = getPropertySourceFacade().getPropertySourceValueMessage(PHONE_UPDATE, null);

		if (!StringUtils.equals(customerModel.getFirstName(), customerData.getFirstName()) || !StringUtils
				.equals(customerModel.getLastName(), customerData.getLastName()))
		{
			updateProfileDataList.add(nameUpdateLabel);
		}
		if (!StringUtils.equals(customerModel.getPhoneNo(), customerData.getContactNumber()))
		{
			updateProfileDataList.add(phoneUpdateLabel);
		}
		if (Objects.isNull(defaultAddress) || !StringUtils.equals(defaultAddress.getLine1(), defaultAddressData.getLine1())
				|| !StringUtils.equals(defaultAddress.getLine2(), defaultAddressData.getLine2()) || !StringUtils
				.equals(defaultAddress.getTown(), defaultAddressData.getTown())
				|| !StringUtils.equals(defaultAddress.getPostalcode(), defaultAddressData.getPostalCode()))
		{
			updateProfileDataList.add(addressUpdateLabel);
		}
		if (Objects.nonNull(defaultAddress) && Objects.nonNull(defaultAddressData))
		{
			final RegionModel region = defaultAddress.getRegion();
			final RegionData regionData = defaultAddressData.getRegion();
			if ((Objects.isNull(region) && Objects.nonNull(regionData)
					|| Objects.isNull(regionData) && Objects.nonNull(region)
					|| (Objects.nonNull(regionData) && Objects.nonNull(region) && !StringUtils
					.equals(region.getIsocode(), regionData.getIsocode()))) && !updateProfileDataList.contains(addressUpdateLabel))
			{
				updateProfileDataList.add(addressUpdateLabel);
			}

		}
		return updateProfileDataList;
	}


	private void startBusinessProcess(final CustomerModel customerModel, final List<String> updateProfileDataList)
	{
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final UpdateProfileProcessModel updateProfileProcessModel = getBusinessProcessService()
				.createProcess(
						"customerUpdateProfileEmail-" + customerModel.getUid() + "-" + System.currentTimeMillis(),
						"customerUpdateProfileEmailProcess");
		updateProfileProcessModel.setSite(siteModel);
		updateProfileProcessModel.setCustomer(customerModel);
		updateProfileProcessModel.setLanguage(getCommonI18NService().getCurrentLanguage());
		updateProfileProcessModel.setCurrency(getCommonI18NService().getCurrentCurrency());
		updateProfileProcessModel.setStore(getBaseStoreService().getCurrentBaseStore());
		updateProfileProcessModel.setUpdateProfileDataList(Sets.newHashSet(updateProfileDataList));
		getModelService().save(updateProfileProcessModel);
		getBusinessProcessService().startProcess(updateProfileProcessModel);
	}

	@Override
	public void createGuestUserForAnonymousCheckout(final String email, final String name, final String firstName,
			final String lastName, final String phoneNo, final String phoneType, final String country) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("email", email);
		final CustomerModel guestCustomer = getModelService().create(CustomerModel.class);
		final String guid = generateGUID();

		//takes care of localizing the name based on the site language
		guestCustomer.setUid(guid + "|" + email);
		guestCustomer.setName(name);
		guestCustomer.setFirstName(firstName);
		guestCustomer.setLastName(lastName);
		guestCustomer.setPhoneType(phoneType);
		guestCustomer.setPhoneNo(phoneNo);
		guestCustomer.setType(CustomerType.valueOf(CustomerType.GUEST.getCode()));
		guestCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		guestCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		guestCustomer.setCountry(country);
		getCustomerAccountService().registerGuestForAnonymousCheckout(guestCustomer, guid);
		updateCartWithGuestForAnonymousCheckout(getCustomerConverter().convert(guestCustomer));
	}

	private CustomerModel getCustomerForUid(final String uid)
	{
		CustomerModel customer = null;
		try
		{
			customer = getUserService().getUserForUID(uid, CustomerModel.class);
		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}

		return customer;
	}

	private void sendEmailProcess(final CustomerModel customerModel)
	{
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = getBusinessProcessService()
				.createProcess(
						"customerChangeEmailProcess-" + customerModel.getRequestedUpdateEmail() + "-" + System.currentTimeMillis(),
						"customerEmailChangeProcess");
		executeBusinessProcess(storeFrontCustomerProcessModel, customerModel, siteModel);
	}

	@Override
	public TransactionDetails getCTCCardInfo(final Map<String, String> resultMap) throws IntegrationException
	{
		//pass details in resultMap
		final TransactionDetails transactionDetails = getMyAccountPaymentHandler()
				.handlePayment(resultMap, 0);
		if (!transactionDetails.getIsSuccessful())
		{
			throw new IntegrationException(
					"Failed to get cardInfo " + transactionDetails.getBcfResponseDescription()
							+ ", for card number " + resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER));
		}
		return transactionDetails;
	}

	protected BCFCustomerLDAPService getCustomerLDAPService()
	{
		return customerLDAPService;
	}

	public void setCustomerLDAPService(final BCFCustomerLDAPService customerLDAPService)
	{
		this.customerLDAPService = customerLDAPService;
	}


	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public BcfCustomerService getBcfCustomerService()
	{
		return bcfCustomerService;
	}

	@Required
	public void setBcfCustomerService(final BcfCustomerService bcfCustomerService)
	{
		this.bcfCustomerService = bcfCustomerService;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}

	public ASMCreateCustomerStrategy getAsmCreateCustomerStrategy()
	{
		return asmCreateCustomerStrategy;
	}

	@Required
	public void setAsmCreateCustomerStrategy(final ASMCreateCustomerStrategy asmCreateCustomerStrategy)
	{
		this.asmCreateCustomerStrategy = asmCreateCustomerStrategy;
	}

	public WebCreateCustomerStrategy getWebCreateCustomerStrategy()
	{
		return webCreateCustomerStrategy;
	}

	@Required
	public void setWebCreateCustomerStrategy(final WebCreateCustomerStrategy webCreateCustomerStrategy)
	{
		this.webCreateCustomerStrategy = webCreateCustomerStrategy;
	}

	protected CustomerUpdateService getCustomerUpdateService()
	{
		return customerUpdateService;
	}

	@Resource
	public void setCustomerUpdateService(final CustomerUpdateService customerUpdateService)
	{
		this.customerUpdateService = customerUpdateService;
	}

	protected CRMPaymentCardsService getCrmPaymentCardsService()
	{
		return crmPaymentCardsService;
	}

	@Required
	public void setCrmPaymentCardsService(final CRMPaymentCardsService crmPaymentCardsService)
	{
		this.crmPaymentCardsService = crmPaymentCardsService;
	}

	protected Populator<CTCTCCardData, CTCTCCardPaymentInfoModel> getCrmCtcTcPaymentInfoReversePopulator()
	{
		return crmCtcTcPaymentInfoReversePopulator;
	}

	@Resource
	public void setCrmCtcTcPaymentInfoReversePopulator(
			final Populator<CTCTCCardData, CTCTCCardPaymentInfoModel> crmCtcTcPaymentInfoReversePopulator)
	{
		this.crmCtcTcPaymentInfoReversePopulator = crmCtcTcPaymentInfoReversePopulator;
	}

	protected Populator<CCPaymentInfoData, PaymentInfoModel> getCardPaymentInfoReverseConverter()
	{
		return cardPaymentInfoReverseConverter;
	}

	@Required
	public void setCardPaymentInfoReverseConverter(
			final Populator<CCPaymentInfoData, PaymentInfoModel> cardPaymentInfoReverseConverter)
	{
		this.cardPaymentInfoReverseConverter = cardPaymentInfoReverseConverter;
	}

	protected MyAccountPaymentHandler getMyAccountPaymentHandler()
	{
		return myAccountPaymentHandler;
	}

	@Required
	public void setMyAccountPaymentHandler(final MyAccountPaymentHandler myAccountPaymentHandler)
	{
		this.myAccountPaymentHandler = myAccountPaymentHandler;
	}

	protected BcfCreditCardPaymentInfoCreateStrategy getBcfCreditCardPaymentInfoCreateStrategy()
	{
		return bcfCreditCardPaymentInfoCreateStrategy;
	}

	@Required
	public void setBcfCreditCardPaymentInfoCreateStrategy(
			final BcfCreditCardPaymentInfoCreateStrategy bcfCreditCardPaymentInfoCreateStrategy)
	{
		this.bcfCreditCardPaymentInfoCreateStrategy = bcfCreditCardPaymentInfoCreateStrategy;
	}

	protected BcfCtcTcCardPaymentInfoCreateStrategy getBcfCtcTcCardPaymentInfoCreateStrategy()
	{
		return bcfCtcTcCardPaymentInfoCreateStrategy;
	}

	@Required
	public void setBcfCtcTcCardPaymentInfoCreateStrategy(
			final BcfCtcTcCardPaymentInfoCreateStrategy bcfCtcTcCardPaymentInfoCreateStrategy)
	{
		this.bcfCtcTcCardPaymentInfoCreateStrategy = bcfCtcTcCardPaymentInfoCreateStrategy;
	}

	protected BcfUserFacade getBcfUserFacade()
	{
		return bcfUserFacade;
	}

	@Required
	public void setBcfUserFacade(final BcfUserFacade bcfUserFacade)
	{
		this.bcfUserFacade = bcfUserFacade;
	}

	protected Map<String, CRMPaymentMethodCreationHandler> getCrmPaymentMethodCreationHandlerMap()
	{
		return crmPaymentMethodCreationHandlerMap;
	}

	@Required
	public void setCrmPaymentMethodCreationHandlerMap(
			final Map<String, CRMPaymentMethodCreationHandler> crmPaymentMethodCreationHandlerMap)
	{
		this.crmPaymentMethodCreationHandlerMap = crmPaymentMethodCreationHandlerMap;
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}

