/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.customer.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.model.BusinessOpportunitiesModel;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.facades.customer.data.BusinessOpportunitiesData;
import com.bcf.facades.customer.data.CustomerSubscriptionData;


public class BCFCustomerPopulator extends CustomerPopulator
{
	private CustomerAccountService customerAccountService;
	private Converter<AddressModel, AddressData> addressConverter;

	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		super.populate(source, target);

		final AddressModel defaultAddress = getCustomerAccountService().getDefaultAddress(source);
		if (null != defaultAddress)
		{
			final AddressData customerAddress = addressConverter.convert(defaultAddress);
			target.setDefaultAddress(customerAddress);
		}

		target.setAccountValidationKey(source.getAccountValidationKey());
		target.setRegistrationSource(source.getRegistrationSource());
		if (Objects.nonNull(source.getAccountStatus()))
		{
			target.setAccountStatus(source.getAccountStatus().getCode());
		}
		target.setContactNumber(source.getPhoneNo());
		target.setCountry(source.getCountry());
		if (Objects.nonNull(source.getAccountType()))
		{
			target.setAccountType(source.getAccountType().getCode());
		}
		populateSubscriptionData(source, target);
	}

	private void populateSubscriptionData(final CustomerModel source, final CustomerData target)
	{
		final List<CustomerSubscriptionData> dataList = new ArrayList<CustomerSubscriptionData>();
		for (final CustomerSubscriptionModel subscription : source.getSubscriptionList())
		{
			final CustomerSubscriptionData subscriptionData = new CustomerSubscriptionData();
			final CRMSubscriptionMasterDetailModel subscriptionMasterData = subscription.getSubscriptionMasterData();
			if (Objects.nonNull(subscriptionMasterData))
			{
				subscriptionData.setName(subscriptionMasterData.getName());
				subscriptionData.setCode(subscriptionMasterData.getSubscriptionCode());
				subscriptionData.setType(subscriptionMasterData.getSubscriptionType().getCode());
				if (Objects.nonNull(subscriptionMasterData.getSubscriptionGroup()))
				{
					subscriptionData.setNoticeGroup(subscriptionMasterData.getSubscriptionGroup().getCode());
				}

				if (subscriptionMasterData.getSubscriptionType().equals(CustomerSubscriptionType.BUSINESS_OPPORTUNITY))
				{
					subscriptionData.setBusinessOpportunities(populateBusinessOpportunities(subscription));
				}
			}
			subscriptionData.setOptIn(subscription.getIsSubscribed());
			subscriptionData.setOptInDateTime(subscription.getSubscribedDate());
			subscriptionData.setOptOutDateTime(subscription.getUnSubscribedDate());
			dataList.add(subscriptionData);
		}
		target.setSubscriptionList(dataList);
	}

	private BusinessOpportunitiesData populateBusinessOpportunities(final CustomerSubscriptionModel subscription)
	{
		final BusinessOpportunitiesData businessOpportunitiesData = new BusinessOpportunitiesData();
		final BusinessOpportunitiesModel businessOpportunities = subscription.getBusinessOpportunities();
		if (Objects.nonNull(businessOpportunities))
		{
			businessOpportunitiesData.setCompanyName(businessOpportunities.getCompanyName());
			businessOpportunitiesData.setJobTitle(businessOpportunities.getJobTitle());
			final List<String> serviceSupplies = businessOpportunities.getServiceSupplies().stream()
					.map(service_supplies -> service_supplies.getCode()).collect(
							Collectors.toList());
			businessOpportunitiesData.setServiceSupplies(serviceSupplies);
		}
		return businessOpportunitiesData;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(
			final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}
}
