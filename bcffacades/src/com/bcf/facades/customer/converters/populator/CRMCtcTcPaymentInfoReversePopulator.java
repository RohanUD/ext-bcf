/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.facades.customer.converters.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.facades.payment.CTCTCCardData;

/**
 *
 */
public class CRMCtcTcPaymentInfoReversePopulator implements Populator<CTCTCCardData, CTCTCCardPaymentInfoModel>
{

	private ModelService modelService;
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final CTCTCCardData source, final CTCTCCardPaymentInfoModel target)
	{

		if (Objects.nonNull(source.getBillingAddress()))
		{
			if (Objects.isNull(target.getBillingAddress()))
			{
				final AddressModel addressModel = getModelService().create(AddressModel.class);
				addressModel.setBillingAddress(Boolean.TRUE);
				addressModel.setOwner(target.getUser());
				target.setBillingAddress(addressModel);
			}
			populateBillingAddress(source.getBillingAddress(), target.getBillingAddress());
		}
	}

	private void populateBillingAddress(final AddressData address, final AddressModel addressModel)
	{
		addressModel.setLine1(address.getLine1());
		addressModel.setLine2(address.getLine2());
		addressModel.setTown(address.getTown());
		addressModel.setPostalcode(address.getPostalCode());
		if (Objects.nonNull(address.getCountry()) && Objects.nonNull(address.getCountry().getIsocode()))
		{
			final CountryModel countryModel = getCommonI18NService().getCountry(address.getCountry().getIsocode());
			addressModel.setCountry(countryModel);
			 final RegionModel regionModel;
			if (Objects.nonNull(address.getRegion()) && Objects.nonNull(address.getRegion().getIsocode()))
			{
				final String regionCode = address.getRegion().getIsocode();
				if(regionCode.contains("-")) {
					regionModel = getCommonI18NService().getRegion(countryModel, address.getRegion().getIsocode());
				} else {
					regionModel = getCommonI18NService()
							.getRegion(countryModel, countryModel.getIsocode() + "-" + address.getRegion().getIsocode());
				}

				addressModel.setRegion(regionModel);
			}
		}
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
