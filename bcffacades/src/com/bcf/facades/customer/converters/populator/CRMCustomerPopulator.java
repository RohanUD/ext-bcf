/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.customer.converters.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.integration.data.BCF_Customer;
import com.bcf.integration.data.EmailAddress;


public class CRMCustomerPopulator implements Populator<BCF_Customer, CustomerData>
{
	private CustomerNameStrategy customerNameStrategy;

	@Override
	public void populate(final BCF_Customer source, final CustomerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final String firstName = source.getPersonName().getGivenName();
		final String lastName = source.getPersonName().getSurname();
		final String email = source.getEmailAddress().stream().filter(EmailAddress::isPrimaryInd).findFirst().get()
				.getEmailAddress();

		target.setFirstName(firstName);
		target.setLastName(lastName);
		target.setName(getCustomerNameStrategy().getName(firstName, lastName));
		target.setEmail(email);
		target.setUid(email);
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}
}
