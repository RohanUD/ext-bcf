/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.customer.converters.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import com.bcf.integration.data.Address;
import com.bcf.integration.data.AssociatedAccount;
import com.bcf.integration.data.BCF_Customer;


public class CRMsearchCustomerPopulator implements Populator<BCF_Customer, CustomerData>
{

	private static final Logger LOG = Logger.getLogger(CRMsearchCustomerPopulator.class);

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final BCF_Customer source, final CustomerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		try
		{
			target.setFirstName(source.getPersonName().getGivenName());
			target.setLastName(source.getPersonName().getSurname());
			target.setGuestInd(Objects.isNull(source.getGuestInd()) ? Boolean.TRUE : source.getGuestInd());

			if (CollectionUtils.isNotEmpty(source.getEmailAddress()))
			{
				final String emailAddress = source.getEmailAddress().get(0).getEmailAddress();
				target.setEmail(emailAddress);
				target.setUid(emailAddress);
			}

			if (CollectionUtils.isNotEmpty(source.getPhoneNumber()))
			{
				final String phoneNumber = source.getPhoneNumber().get(0).getPhoneNumber();
				target.setMobileNumber(phoneNumber);
			}

			if (CollectionUtils.isNotEmpty(source.getAddress()))
			{
				final Address address = source.getAddress().stream().filter(Address::isPrimaryInd).findFirst().orElse(null);
				if (Objects.nonNull(address))
				{
					final AddressData addressData = new AddressData();
					addressData.setLine1(address.getAddressLine1());
					addressData.setLine2(address.getAddressLine2());
					addressData.setTown(address.getCity());
					if (Objects.nonNull(address.getCountryCode()))
					{
						final CountryData countryData = new CountryData();
						countryData.setIsocode(address.getCountryCode());
						countryData.setName(commonI18NService.getCountry(address.getCountryCode()).getName());
						addressData.setCountry(countryData);
					}

					if (Objects.nonNull(address.getProvinceStateCode()))
					{
						final RegionData regionData = new RegionData();
						regionData.setIsocode(address.getProvinceStateCode());
						final RegionModel region = commonI18NService
								.getRegion(commonI18NService.getCountry(address.getCountryCode()),
										address.getCountryCode() + "-" + address.getProvinceStateCode());
						regionData.setName(region.getName());
						addressData.setRegion(regionData);
					}
					target.setDefaultAddress(addressData);
				}
			}

			if (CollectionUtils.isNotEmpty(source.getAssociatedAccount()))
			{
				final StringBuilder companyName = new StringBuilder();
				for (final AssociatedAccount associatedAccount : source.getAssociatedAccount())
				{
					companyName.append(associatedAccount.getAccountName().getOrganizationName());
					companyName.append(",");

				}
				companyName.deleteCharAt(companyName.length() - 1);
				target.setCompanyName(companyName.toString());
			}
		}
		catch (final Exception e)
		{
			LOG.error(String.format("Exception occurred while populating Customer details for [%s] - error [%s]",
					CollectionUtils.isNotEmpty(source.getEmailAddress()) ?
							source.getEmailAddress().get(0).getEmailAddress() :
							"", e.getMessage()));

			if (LOG.isDebugEnabled())
			{
				LOG.error(e);
			}
		}
	}
}
