/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.offer.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.travelfacades.facades.impl.DefaultOffersFacade;
import java.util.ArrayList;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.ancillary.search.manager.AncillarySearchResponseDataListPipelineManager;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.offer.BCFOffersFacade;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;
import com.bcf.facades.reservation.data.ReservationDataList;


public class DefaultBCFOffersFacade extends DefaultOffersFacade implements BCFOffersFacade
{
	private BCFReservationFacade reservationFacade;
	private AncillarySearchResponseDataListPipelineManager ancillarySearchResponseDataListPipelineManager;

	@Override
	public OfferRequestDataList getOffersRequests()
	{
		final OfferRequestDataList offerRequests = new OfferRequestDataList();
		offerRequests.setOfferRequestDatas(new ArrayList<>());

		final ReservationDataList reservationDatalist = getReservationFacade().getCurrentReservations();

		for (final ReservationData reservationData : reservationDatalist.getReservationDatas())
		{
			offerRequests.getOfferRequestDatas().add(getAncillarySearchRequestPipelineManager().executePipeline(reservationData));
		}
		return offerRequests;
	}

	@Override
	public OfferResponseDataList getOffers(final OfferRequestDataList offerRequestDataList)
	{
		if (Objects.isNull(offerRequestDataList))
		{
			return null;
		}

		return getAncillarySearchResponseDataListPipelineManager().executePipeline(offerRequestDataList);
	}

	@Override
	public BCFReservationFacade getReservationFacade()
	{
		return reservationFacade;
	}

	@Required
	public void setReservationFacade(final BCFReservationFacade reservationFacade)
	{
		this.reservationFacade = reservationFacade;
	}

	public AncillarySearchResponseDataListPipelineManager getAncillarySearchResponseDataListPipelineManager()
	{
		return ancillarySearchResponseDataListPipelineManager;
	}

	public void setAncillarySearchResponseDataListPipelineManager(
			final AncillarySearchResponseDataListPipelineManager ancillarySearchResponseDataListPipelineManager)
	{
		this.ancillarySearchResponseDataListPipelineManager = ancillarySearchResponseDataListPipelineManager;
	}

}
