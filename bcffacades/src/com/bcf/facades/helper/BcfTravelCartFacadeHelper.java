/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.helper;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.order.CommerceBundleCartModificationException;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfTravelCartFacadeHelper
{
	void addFeesProductToCart(BCFMakeBookingResponse response, int journeyReferenceNumber, int originDestinationNum)
			throws CommerceCartModificationException;

	void addProductToCartEntry(String changeFeesProductCode, CartModel cartModel, CartModificationData cartModificationData,
			int originDestinationNum, int journeyReferenceNumber);

	void updateOpenTicketStatus(List<CartModificationData> cartModificationDataList);

	/**
	 * Sets the traveller max fare price map to session
	 *
	 * @param fareSelectionData the Fare Selection Data
	 */
	void setMaxFarePriceMapToSession(FareSelectionData fareSelectionData);

	/**
	 * add reservation fees to cart
	 *
	 * @param journeyReferenceNumber
	 * @param originDestinationNum
	 * @param otherChargeDatas
	 * @return
	 * @throws CommerceCartModificationException
	 */
	List<CartModificationData> addReservationFeeToCart(int journeyReferenceNumber, int originDestinationNum,
			List<OtherChargeData> otherChargeDatas) throws CommerceCartModificationException;

	boolean isAlacateFlow();

	void setCurrentJourneyRefNum();

	void calculateChangeFees();

	VacationPolicyTAndCData getVacationPolicyTermsAndConditions(String bookingReference);

	List<BCFNonRefundableCostTAndCData>  getBCFNonRefundableCostTermsAndConditions(String bookingReference);

	Pair<Double,List<RefundTransactionInfoData>> getRefundTransactionInfo(String bookingReference);

	List<RefundTransactionInfoData> getGoodWillRefundTransactionInfo(String bookingReference);

	List<Integer> getLongRouteJourneyRefNumber();

	void updateAvailabilityStatus();

	void setCartBookingType(BookingJourneyType bookingJourneyType);

	 int getHoldTimeInSeconds( SalesApplication salesApplication);

	public Pair<String, Double> getSubmitBookingUrlAndIsRefund(final PriceData totalPay);

	String getPaymentCancelUrl();

	public boolean isCartGuestIdentifierMatching(final String cartCode, final String guestBookingIdentifier);

	public boolean checkJourneyType( final List<BookingJourneyType> bookingJourneyTypes);

	List<String> getPromotionMessages(AbstractOrderModel abstractOrder, Collection<AbstractOrderEntryModel> entries);

	public void updateVehicleCode(final String travelRouteCode,
			final AddBundleToCartRequestData addBundleToCartRequestData);

	public void setFerryOptionBooking(final AddBundleToCartRequestData addBundleToCartRequestData);

	public AddBundleToCartRequestData getAddBundleToCartRequestData(final int originRefNumber);

	public String getErrorMessage(final String errorPrefix, final String errorCode, final String defaultMessage);

	public String addBundleToCart(final AddBundleToCartRequestData addBundleToCartRequestData, final boolean isDynamicPackage,
			boolean recalcuate) throws CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException;

	public void manageOriginDestTravellerSessionMap(final Integer originDestinationRefNumber);

	boolean isOnRequestAmendment();

	public boolean isCardPayment();

	public void updateAmountPaidForOnRequestAmendment(AbstractOrderModel order);

	public void removePreviousOnRequestTransaction();

	boolean isQuoteCart(CartModel cart);

	double getGSTTaxForEntry(final AbstractOrderEntryModel abstractOrderEntry);
}
