/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.helper;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public interface BcfAccommodationFacadeHelper
{
	Pair<AvailabilityStatus,Integer> getAccommodationAvailability(final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate, Date endDate, final AccommodationModel accommodation,
			final RatePlanModel validRatePlan);

	SaleStatusType getSaleStatus(Collection<SaleStatusModel> saleStatuses , Date startDate, Date endDate);

	double previousAccommodationAndTransportPaid(CartModel cart);

	AvailabilityStatus getStockAvailability(final AccommodationOfferingModel accommodationOfferingModel,
			final AccommodationModel accommodation, final StayDateRangeData stayDateRange, final int numberOfRooms);

	LinkedHashMap<PropertyData, List<AccommodationReservationData>> sortAccommodationReservation(
			BcfGlobalReservationData bcfGlobalReservationData);

	List<AccommodationReservationData> sortAccommodationReservations(
			List<AccommodationReservationData> accommodationReservations);

	boolean checkifExtraProductEligible(AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			AccommodationModel accommodationModel, List<RoomStayCandidateData> roomStayCandidates);

	Map<Integer,String> buildAccommodationOfferingPageUrlParameters(
			List<AccommodationReservationData> accommodationReservationDatas);

	PassengerTypeQuantityData getChildPassengerTypeQuantity();

}
