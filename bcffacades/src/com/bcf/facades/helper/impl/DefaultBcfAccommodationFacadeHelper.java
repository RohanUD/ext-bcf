/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.helper.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.util.TaxValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.core.service.BcfSaleStatusService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class DefaultBcfAccommodationFacadeHelper implements BcfAccommodationFacadeHelper
{

	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfProductFacade bcfProductFacade;
	private BcfSaleStatusService saleStatusService;
	private SessionService sessionService;
	private CommonI18NService commerceI18NService;
	private PriceRowService priceRowService;
	private BCFPassengerTypeFacade passengerTypeFacade;

	private static final String EQUALS = "=";
	private static final String HYPHEN = "-";
	private static final String AMPERSAND = "&";
	private static final String COMMA = ",";
	private static final String ROOM_QUERY_STRING_INDICATOR = "r";
	private static final String DESTINATION_LOCATION = "destinationLocation";
	public static final String MODIFY = "modify";
	public static final String MODIFY_ROOM_REF = "modifyRoomRef";
	private static final String PASSENGER_TYPE_CODE_CHILD = "child";

	@Override
	public Pair<AvailabilityStatus, Integer> getAccommodationAvailability(
			final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate, final Date endDate, final AccommodationModel accommodation,
			final RatePlanModel validRatePlan)
	{
		//proceed only if  accommodation offering is open
		if (isAccommodationOfferingWithStatusStopSaleExists(accommodationOfferingModel, startDate, endDate)
				|| isAccommodationOfferingWithStatusClosedExists(
				accommodationOfferingModel, startDate, endDate))
		{
			return Pair.of(AvailabilityStatus.SOLD_OUT, 0);
		}

		//if accommodation offering is set to not bookable online -> send the same status for accommodations
		final SaleStatusModel nonBookableStatus = accommodationOfferingModel.getSaleStatuses().stream()
				.filter(s -> SaleStatusType.NOT_BOOKABLE_ONLINE.equals(s.getSaleStatusType()) && BCFDateUtils
						.checkIfDatesOverlapped(startDate, endDate, s.getStartDate(), s.getEndDate())).findFirst().orElse(null);

		if (Objects.nonNull(nonBookableStatus))
		{
			return Pair.of(AvailabilityStatus.NOT_BOOKABLE_ONLINE, 100);
		}

		if (isAccommodationsWithStatusStopSaleExists(accommodation, startDate, endDate) || isAccommodationWithStatusClosedExists(
				accommodation, startDate, endDate))
		{
			return Pair.of(AvailabilityStatus.SOLD_OUT, 0);
		}

		final List<StockData> stockLevels = new ArrayList<>();
		if (validRatePlan != null)
		{
			for (Date date = startDate; !TravelDateUtils
					.isSameDate(date, endDate); date = DateUtils
					.addDays(date, 1))
			{
				if (BcfRatePlanUtil.isValidRatePlan(date, validRatePlan))
				{
					stockLevels.add(getStockAvailability(accommodationOfferingModel, accommodation, date));
				}
			}
		}
		return getAvailability(accommodation, startDate, endDate, stockLevels);
	}


	@Override
	public SaleStatusType getSaleStatus(final Collection<SaleStatusModel> saleStatuses, final Date startDate, final Date endDate)
	{
		if (org.apache.commons.collections4.CollectionUtils.isEmpty(saleStatuses))
		{
			return SaleStatusType.OPEN;
		}

		for (Date date = startDate; !TravelDateUtils.isSameDate(date, endDate); date = DateUtils.addDays(date,
				1))
		{
			for (final SaleStatusModel saleStatus : saleStatuses)
			{
				if (BCFDateUtils.isBetweenDatesInclusive(date, saleStatus.getStartDate(), saleStatus.getEndDate()))
				{
					return saleStatus.getSaleStatusType();
				}
			}
		}
		return SaleStatusType.OPEN;
	}


	protected StockData getStockAvailability(final AccommodationOfferingModel accommodationOfferingModel,
			final AccommodationModel accommodation, final Date date)
	{
		final Collection<WarehouseModel> warehouses = Collections.singletonList(accommodationOfferingModel);
		return getBcfProductFacade().getStockData(accommodation, date, warehouses);
	}

	protected Pair<AvailabilityStatus, Integer> getAvailability(final AccommodationModel accommodation,
			final Date startDate, final Date endDate, final List<StockData> stockLevels)
	{
		Integer availableQuantity = 0;
		boolean onRequestStock = false;

		if (CollectionUtils.isNotEmpty(stockLevels))
		{
			availableQuantity = stockLevels.stream().filter(stockLevel -> stockLevel.getStockLevel() != null)
					.map(stockLevel -> stockLevel.getStockLevel()).mapToInt(Long::intValue).min().orElse(0);
			onRequestStock = stockLevels.stream().filter(stockLevel -> stockLevel.getStockLevelType() != null)
					.anyMatch(stockLevel -> stockLevel.getStockLevelType().equals(StockLevelType.ONREQUEST));
		}
		final StockLevelType stockLevelType = getBcfTravelCommerceStockService().getStockLevelType(accommodation);
		AvailabilityStatus availableStatus = getAvailabilityStatus(availableQuantity, onRequestStock, stockLevelType);

		if (saleStatusService.toConsiderNonBookableOnlineSaleStatus())
		{
			availableStatus = getAvailableStatusIfNotBookableOnline(accommodation, availableStatus, startDate, endDate);
		}

		return Pair.of(availableStatus, availableQuantity);
	}

	private AvailabilityStatus getAvailabilityStatus(final Integer availableQuantity, final boolean onRequestStock,
			final StockLevelType stockLevelType)
	{
		if (Objects.equals(StockLevelType.ONREQUEST, stockLevelType) || onRequestStock)
		{
			return AvailabilityStatus.ON_REQUEST;
		}
		if (availableQuantity > 0)
		{
			return AvailabilityStatus.AVAILABLE;
		}

		return AvailabilityStatus.SOLD_OUT;
	}

	@Override
	public AvailabilityStatus getStockAvailability(final AccommodationOfferingModel accommodationOfferingModel,
			final AccommodationModel accommodation, final StayDateRangeData stayDateRange, final int numberOfRooms)
	{
		final Collection<WarehouseModel> warehouses = Collections.singletonList(accommodationOfferingModel);
		final Date endingDate = stayDateRange.getEndTime();
		for (Date date = stayDateRange.getStartTime(); !TravelDateUtils.isSameDate(date, endingDate); date = DateUtils
				.addDays(date,
						1))
		{
			final StockData stockData = getBcfProductFacade().getStockData(accommodation, date, warehouses);
			if (Objects.equals(StockLevelType.STANDARD, stockData.getStockLevelType()))
			{
				final Long stockValue = stockData.getStockLevel();
				if (Objects.isNull(stockValue) || (stockValue < numberOfRooms))
				{
					return AvailabilityStatus.SOLD_OUT;
				}
			}
			else
			{
				return AvailabilityStatus.ON_REQUEST;
			}
		}
		return AvailabilityStatus.AVAILABLE;
	}

	private boolean isAccommodationsWithStatusStopSaleExists(final AccommodationModel accommodationModel, final Date startDate,
			final Date endDate)
	{
		return isAccommodationsInDateRangeWithSaleStatus(
				accommodationModel, startDate, endDate, SaleStatusType.STOPSALE, false);
	}

	private boolean isAccommodationWithStatusClosedExists(final AccommodationModel accommodationModel, final Date startDate,
			final Date endDate)
	{
		return isAccommodationsInDateRangeWithSaleStatus(
				accommodationModel, startDate, endDate, SaleStatusType.CLOSED, false);
	}

	private boolean isAccommodationsInDateRangeWithSaleStatus(
			final AccommodationModel accommodationModel,
			final Date startDate, final Date endDate, final SaleStatusType saleStatusType,
			final boolean toConsiderNonBookableOnlineSaleStatus)
	{
		final SaleStatusType saleStatusTypeInRange = getSaleStatus(accommodationModel.getSaleStatuses(), startDate,
				endDate);

		//if Not to consider Not bookable status, such as in case of ASM log in then consider it as Open status
		if (SaleStatusType.OPEN.equals(saleStatusType) && SaleStatusType.NOT_BOOKABLE_ONLINE
				.equals(saleStatusTypeInRange) && !toConsiderNonBookableOnlineSaleStatus)
		{
			return true;
		}
		else
		{
			return Objects
					.equals(saleStatusType,
							getSaleStatus(accommodationModel.getSaleStatuses(), startDate,
									endDate));
		}
	}

	private boolean isAccommodationOfferingWithStatusStopSaleExists(final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate,
			final Date endDate)
	{
		return isAccommodationOfferingInDateRangeWithSaleStatus(
				accommodationOfferingModel, startDate, endDate, SaleStatusType.STOPSALE, false);
	}

	private boolean isAccommodationOfferingWithStatusClosedExists(final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate,
			final Date endDate)
	{
		return isAccommodationOfferingInDateRangeWithSaleStatus(
				accommodationOfferingModel, startDate, endDate, SaleStatusType.CLOSED, false);
	}

	private boolean isAccommodationOfferingInDateRangeWithSaleStatus(
			final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate, final Date endDate, final SaleStatusType saleStatusType,
			final boolean toConsiderNonBookableOnlineSaleStatus)
	{
		final SaleStatusType saleStatusTypeInRange = getSaleStatus(accommodationOfferingModel.getSaleStatuses(), startDate,
				endDate);

		//if Not to consider Not bookable status, such as in case of ASM log in then consider it as Open status
		if (SaleStatusType.OPEN.equals(saleStatusType) && SaleStatusType.NOT_BOOKABLE_ONLINE
				.equals(saleStatusTypeInRange) && !toConsiderNonBookableOnlineSaleStatus)
		{
			return true;
		}
		else
		{
			return Objects
					.equals(saleStatusType,
							getSaleStatus(accommodationOfferingModel.getSaleStatuses(), startDate,
									endDate));
		}
	}

	private AvailabilityStatus getAvailableStatusIfNotBookableOnline(final AccommodationModel accommodation,
			final AvailabilityStatus availableStatus, final Date startDate, final Date endDate)
	{
		final SaleStatusModel nonBookableStatus = accommodation.getSaleStatuses().stream()
				.filter(s -> SaleStatusType.NOT_BOOKABLE_ONLINE.equals(s.getSaleStatusType()) && BCFDateUtils
						.checkIfDatesOverlapped(startDate, endDate, s.getStartDate(), s.getEndDate())).findFirst().orElse(null);

		if (Objects.nonNull(nonBookableStatus))
		{
			return AvailabilityStatus.NOT_BOOKABLE_ONLINE;
		}
		return availableStatus;

	}

	@Override
	public double previousAccommodationAndTransportPaid(final CartModel cart)
	{

		final Integer journeyRefNo = getSessionService().getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		final Integer finalJourneyRefNo = Objects.isNull(journeyRefNo) ? 0 : journeyRefNo;

		final List<AbstractOrderEntryModel> filteredEntries = cart.getEntries().stream()
				.filter(
						entry -> entry.getActive() && (OrderEntryType.ACCOMMODATION.equals(entry.getType()) || OrderEntryType.TRANSPORT
								.equals(entry.getType()))
								&& entry.getJourneyReferenceNumber() == finalJourneyRefNo)
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(filteredEntries))
		{
			return filteredEntries.stream().mapToDouble(entry -> getPriceWithOutGstTax(entry)).sum();
		}
		return 0d;
	}

	private double getPriceWithOutGstTax(final AbstractOrderEntryModel entry)
	{

		double totalPrice = entry.getTotalPrice();
		final Collection<TaxValue> taxValues = entry.getTaxValues();
		if (CollectionUtils.isNotEmpty(taxValues))
		{
			final TaxValue taxValue = taxValues.stream().filter(tax -> tax.getCode().equals(BcfCoreConstants.GST)).findAny()
					.orElse(null);
			if (taxValue != null)
			{
				totalPrice = totalPrice - commerceI18NService
						.roundCurrency(taxValue.isAbsolute() ? taxValue.getValue() : taxValue.getAppliedValue(),
								entry.getOrder().getCurrency().getDigits().intValue());
			}

		}

		return PrecisionUtil.round(totalPrice);

	}

	@Override
	public LinkedHashMap<PropertyData, List<AccommodationReservationData>> sortAccommodationReservation(
			final BcfGlobalReservationData bcfGlobalReservationData)
	{

		bcfGlobalReservationData.getAccommodationReservations().setAccommodationReservations(
				sortAccommodationReservations(bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()
				));

		//then sort map based on accommodation reservation data
		final LinkedHashMap<PropertyData, List<AccommodationReservationData>> accommodationMap = new LinkedHashMap<>();
		bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations().stream()
				.forEach(accommodationReservationData -> {
					final LinkedList<AccommodationReservationData> res = new LinkedList<>();
					if (!reactor.util.CollectionUtils.isEmpty(accommodationMap.get(accommodationReservationData)))
					{
						res.addAll(accommodationMap.get(accommodationReservationData));
					}
					res.add(accommodationReservationData);
					accommodationMap.put(accommodationReservationData.getAccommodationReference(), res);
				});

		return accommodationMap;
	}

	@Override
	public Map<Integer, String> buildAccommodationOfferingPageUrlParameters(
			final List<AccommodationReservationData> accommodationReservationDatas)
	{
		final Map<Integer, String> urlParametersMap = new HashMap<>();

		for (final AccommodationReservationData accommodationReservationData : accommodationReservationDatas)
		{
			for (final ReservedRoomStayData reservedRoomStayData : accommodationReservationData.getRoomStays())
			{
				final StringBuilder urlParameters = new StringBuilder();

				urlParameters.append(DESTINATION_LOCATION);
				urlParameters.append(EQUALS);
				urlParameters.append(reservedRoomStayData.getLocationCode());
				urlParameters.append(AMPERSAND);

				urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
				urlParameters.append(EQUALS);
				urlParameters.append(TravelDateUtils.convertDateToStringDate(reservedRoomStayData.getCheckInDate(),
						BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
				urlParameters.append(AMPERSAND);

				urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE);
				urlParameters.append(EQUALS);
				urlParameters.append(TravelDateUtils.convertDateToStringDate(reservedRoomStayData.getCheckOutDate(),
						BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
				urlParameters.append(AMPERSAND);

				urlParameters.append(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS);
				urlParameters.append(EQUALS);
				urlParameters.append(1);


				urlParameters.append(AMPERSAND);
				urlParameters.append(ROOM_QUERY_STRING_INDICATOR);
				urlParameters.append(0);
				urlParameters.append(EQUALS);
				final StringJoiner joiner = new StringJoiner(COMMA);
				reservedRoomStayData.getGuestCounts().forEach(passenger -> {
					final StringBuilder ages = new StringBuilder();
					if (CollectionUtils.isNotEmpty(passenger.getChildAges()))
					{
						for (final int age : passenger.getChildAges())
						{
							ages.append(HYPHEN + age);
						}
					}
					joiner.add(passenger.getQuantity() + HYPHEN + passenger.getPassengerType().getCode() + ages.toString());
				});

				urlParameters.append(joiner.toString());

				urlParameters.append(AMPERSAND);
				urlParameters.append(MODIFY);
				urlParameters.append(EQUALS);
				urlParameters.append("true");
				urlParameters.append(AMPERSAND);
				urlParameters.append(MODIFY_ROOM_REF);
				urlParameters.append(EQUALS);
				urlParameters.append(reservedRoomStayData.getRoomStayRefNumber());


				urlParametersMap.put(reservedRoomStayData.getRoomStayRefNumber(),urlParameters.toString());

			}
		}
			return urlParametersMap;
	}

	@Override
	public List<AccommodationReservationData> sortAccommodationReservations(
			final List<AccommodationReservationData> accommodationReservations)
	{
		final Comparator<Object> comparePropertyData = Comparator
				.comparing(reservedRoomStayData -> ((ReservedRoomStayData) reservedRoomStayData).getCheckInDate())
				.thenComparing(reservedRoomStayData -> ((ReservedRoomStayData) reservedRoomStayData).getRoomStayRefNumber());


		//first sort roomstays based on check-in date and room ref
		accommodationReservations
				.forEach(accommodationReservation -> {
					accommodationReservation.setRoomStays(
							accommodationReservation.getRoomStays().stream().sorted(comparePropertyData)
									.collect(Collectors.toList()));
				});
		//then sort accommodation reservation data based on room stay
		return accommodationReservations.stream().sorted(
				Comparator.comparing(asd -> asd.getRoomStays().get(0).getCheckInDate())).collect(Collectors.toList());
	}

	@Override
	public boolean checkifExtraProductEligible(
			final AccommodationOfferingDayRateData accommodationOfferingDayRateData, final AccommodationModel accommodationModel,
			final List<RoomStayCandidateData> roomStayCandidates)
	{

		for (final RoomStayCandidateData roomStayCandidateData : roomStayCandidates)
		{
			final Pair<Integer, Integer> extraGuestOccupancies = DefaultBcfTravelCartFacadeHelper
					.getExtraGuestOccupancies(roomStayCandidateData.getPassengerTypeQuantityList(), accommodationModel.getBaseOccupancyCount());

			if (extraGuestOccupancies.getLeft() > 0)
			{

				final Collection<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodationModel
						.getExtraGuestProducts().stream()
						.filter(Objects::nonNull).distinct()
						.filter(extraGuestProduct -> priceRowService
								.getPriceRow(extraGuestProduct, BcfFacadesConstants.ADULT,
										accommodationOfferingDayRateData.getDateOfStay()) != null)
						.collect(
								Collectors.toList());

				if (CollectionUtils.isEmpty(extraGuestProducts))
				{
					return false;
				}

			}

			if (extraGuestOccupancies.getRight() > 0)
			{
				final Collection<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodationModel
						.getExtraGuestProducts().stream()
						.filter(Objects::nonNull).distinct()
						.filter(extraGuestProduct -> priceRowService
								.getPriceRow(extraGuestProduct, BcfFacadesConstants.CHILD,
										accommodationOfferingDayRateData.getDateOfStay()) != null)
						.collect(
								Collectors.toList());

				if (CollectionUtils.isEmpty(extraGuestProducts))
				{
					return false;
				}

			}
		}
		return true;
	}

	@Override
	public PassengerTypeQuantityData getChildPassengerTypeQuantity()
	{
		final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
		final List<String> codes = Arrays.asList(
				PASSENGER_TYPE_CODE_CHILD);

		final PassengerTypeData sortedPassengerType = passengerTypeFacade.getPassengerTypesForCodes(codes).get(0);
		passengerTypeQuantityData.setPassengerType(sortedPassengerType);
		passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);

		return passengerTypeQuantityData;
	}



	public BcfSaleStatusService getSaleStatusService()
	{
		return saleStatusService;
	}

	public void setSaleStatusService(final BcfSaleStatusService saleStatusService)
	{
		this.saleStatusService = saleStatusService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}

	public PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}

	public BCFPassengerTypeFacade getPassengerTypeFacade()
	{
		return passengerTypeFacade;
	}

	public void setPassengerTypeFacade(final BCFPassengerTypeFacade passengerTypeFacade)
	{
		this.passengerTypeFacade = passengerTypeFacade;
	}
}
