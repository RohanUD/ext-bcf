/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.helper.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.helper.BcfCalenderViewHelper;
import com.bcf.facades.sailing.list.CalenderPriceData;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.Sailing;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.listSailingResponse.data.BcfListSailingsResponseData;


public class DefaultBcfCalenderViewHelper implements BcfCalenderViewHelper
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfCalenderViewHelper.class);

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void updateCalenderDataWithMinPrice(final FareSearchRequestData fareSearchRequestData,
			final List<BcfListSailingsResponseData> sailingsList, final FareSelectionData fareSelectionData,
			final boolean addMarginPrice)
	{
		Double marginRate = 0.0d;
		if (CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
		{
			marginRate = fareSelectionData.getPricedItineraries().get(0).getItinerary().getRoute().getDestination().getMarginRate();
		}

		if (isCalenderViewEnable(fareSearchRequestData.getRouteType()))
		{
			final Date requestedDate = fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get()
					.getDepartureTime();
			Date fromDate = getAdjustedDate(requestedDate, -getDaysBefore(fareSearchRequestData.getRouteType()));
			final Date toDate = getAdjustedDate(requestedDate, getDaysAfter(fareSearchRequestData.getRouteType()));
			final List<CalenderPriceData> calenderPriceDataList = new ArrayList<>();
			while (fromDate.before(toDate) || isSameDay(fromDate, toDate))
			{
				if ((!fromDate.before(new Date()) || isSameDay(fromDate, new Date())) && isValidDateForOtherLeg(fareSearchRequestData,
						fromDate))
				{
					populateCalanderPriceData(sailingsList, requestedDate, fromDate, calenderPriceDataList, marginRate,
							addMarginPrice);
				}
				fromDate = getAdjustedDate(fromDate, 1);
			}
			fareSelectionData.setCalenderSailingRange(calenderPriceDataList);
		}
	}

	private void populateCalanderPriceData(final List<BcfListSailingsResponseData> sailingsList, final Date requestedDate,
			final Date fromDate, final List<CalenderPriceData> calenderPriceDataList, final Double marginRate,
			final boolean addMarginPrice)
	{
		final CalenderPriceData calenderPriceData = new CalenderPriceData();
		calenderPriceData.setPricingDate(fromDate);
		if (fromDate.equals(requestedDate))
		{
			calenderPriceData.setRequestedDate(true);
		}
		final PriceData minimumFare = getMinimumPriceForDate(fromDate, sailingsList, marginRate, addMarginPrice);
		calenderPriceData.setMinPrice(minimumFare);
		if (minimumFare.getValue().compareTo(BigDecimal.ZERO) > 0)
		{
			calenderPriceDataList.add(calenderPriceData);
		}
	}

	private boolean isCalenderViewEnable(final String routeType)
	{
		final int daysBefore = getDaysBefore(routeType);
		final int daysAfter = getDaysAfter(routeType);
		return daysBefore > 0 || daysAfter > 0;
	}

	private int getDaysBefore(final String routeType)
	{
		final int daysBefore;
		if (StringUtils.equalsIgnoreCase(RouteType.LONG.getCode(), routeType))
		{
			daysBefore = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_BEFORE_NR));
		}
		else
		{
			daysBefore = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_BEFORE_SR));
		}
		return daysBefore;
	}

	private int getDaysAfter(final String routeType)
	{
		final int daysAfter;
		if (StringUtils.equalsIgnoreCase(RouteType.LONG.getCode(), routeType))
		{
			daysAfter = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_AFTER_NR));
		}
		else
		{
			daysAfter = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_AFTER_SR));
		}
		return daysAfter;
	}

	private boolean isValidDateForOtherLeg(final FareSearchRequestData fareSearchRequestData, final Date fromDate)
	{
		final OriginDestinationInfoData originDestinationInfoData = fareSearchRequestData.getOriginDestinationInfo().stream()
				.findFirst().get();
		if (Objects.equals(TripType.RETURN, fareSearchRequestData.getTripType()))
		{
			return (originDestinationInfoData.getReferenceNumber()
					== BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER && !fromDate.after(originDestinationInfoData.getArrivalTime())) || (
					originDestinationInfoData.getReferenceNumber() == BcfCoreConstants.INBOUND_REFERENCE_NUMBER && !fromDate
							.before(originDestinationInfoData.getArrivalTime()));
		}
		return true;
	}

	private PriceData getMinimumPriceForDate(final Date date, final List<BcfListSailingsResponseData> sailingsList,
			final Double marginRate, final boolean addMarginPrice)
	{
		final SimpleDateFormat formatter = new SimpleDateFormat(BcfintegrationcoreConstants.BCF_SAILING_DATE_TIME_PATTERN);
		List<Sailing> sailings = new ArrayList<>();
		if (sailingsList.stream().findFirst().isPresent())
		{
			sailings = sailingsList.stream().findFirst().get().getSailing();
		}
		final List<Sailing> filteredSailings = new ArrayList<>();
		for (final Sailing bcfSailingData : sailings)
		{
			try
			{
				if (isSameDay(
						formatter.parse(bcfSailingData.getLine().stream().findFirst().get().getLineInfo().getDepartureDateTime()),
						date))
				{
					filteredSailings.add(bcfSailingData);
				}
			}
			catch (final ParseException e)
			{
				LOG.error(String.format("Unable to parse date [%s] with error [%s]",
						bcfSailingData.getLine().stream().findFirst().get().getLineInfo().getDepartureDateTime(), e.getMessage()));
			}
		}
		final List<Double> priceList = new ArrayList<>();

		final List<Sailing> sailingsWithoutTransfers = filteredSailings.stream()
				.filter(sailing -> StringUtils.isBlank(sailing.getTransferSailingIdentifier())).collect(Collectors.toList());

		final List<Sailing> sailingsWithTransfers = filteredSailings.stream()
				.filter(sailing -> StringUtils.isNotBlank(sailing.getTransferSailingIdentifier())).collect(Collectors.toList());

		final Map<String, List<Sailing>> transferSailingsMap = sailingsWithTransfers.stream()
				.collect(Collectors.groupingBy(Sailing::getTransferSailingIdentifier));

		if (MapUtils.isNotEmpty(transferSailingsMap))
		{
			transferSailingsMap.forEach((t, transferSailings) -> {
						final Map<String, Double> sailingPriceMap = new HashMap<>();
						transferSailings.stream().forEach(sailing -> {
							for (final SailingLine sailingLine : sailing.getLine())
							{
								sailingLine.getSailingPrices().stream().forEach(sailingPrice -> {
									final double priceForTariff = getTotalSalePrice(sailingPrice, sailing.getOtherCharges(), addMarginPrice,
											marginRate);
									sailingPriceMap.put(sailingPrice.getTariffType(), sailingPriceMap.get(sailingPrice.getTariffType()) == null ?
											priceForTariff : sailingPriceMap.get(sailingPrice.getTariffType()) + priceForTariff);
								});
							}
						});
						if (MapUtils.isNotEmpty(sailingPriceMap))
						{
							sailingPriceMap.forEach((s, aDouble) -> {
								priceList.add(aDouble);
							});
						}
					}
			);
		}

		sailingsWithoutTransfers.stream().forEach(sailing -> {
			for (final SailingLine sailingLine : sailing.getLine())
			{
				sailingLine.getSailingPrices().stream().forEach(sailingPrice -> {
					priceList.add(getTotalSalePrice(sailingPrice, sailing.getOtherCharges(), addMarginPrice, marginRate));
				});
			}
		});

		if (CollectionUtils.isEmpty(priceList))
		{
			return getTravelCommercePriceFacade().createPriceData(0,
					getCommerceCommonI18NService().getCurrentCurrency().getIsocode());
		}
		else if (priceList.stream().reduce(Double::min).isPresent())
		{
			return getTravelCommercePriceFacade().createPriceData(priceList.stream().reduce(Double::min).get(),
					getCommerceCommonI18NService().getCurrentCurrency().getIsocode());
		}
		return getTravelCommercePriceFacade().createPriceData(priceList.stream().findFirst().get(),
				getCommerceCommonI18NService().getCurrentCurrency().getIsocode());
	}

	double getTotalSalePrice(final SailingPrices sailingPrice, final List<ProductFare> otherCharges, final boolean addMarginRate,
			final Double marginRate)
	{

		double fareTotal = 0.0d;
		if (CollectionUtils.isNotEmpty(sailingPrice.getProductFares()))
		{
			for (final ProductFare productFare : sailingPrice.getProductFares())
			{
				double productFareAmount = productFare.getFareDetail().get(0).getGrossAmountInCents() / 100.0;
				if (addMarginRate && marginRate != null)
				{
					productFareAmount = addMarginRate(productFareAmount, marginRate);
				}
				fareTotal = fareTotal + productFareAmount;
			}

			if (CollectionUtils.isNotEmpty(otherCharges))
			{

				for (final ProductFare otherCharge : otherCharges)
				{

					double productFareAmount = otherCharge.getFareDetail().get(0).getGrossAmountInCents() / 100.0;
					if (addMarginRate && marginRate != null)
					{
						productFareAmount = addMarginRate(productFareAmount, marginRate);
					}
					fareTotal = fareTotal + productFareAmount;
				}


			}

		}


		return fareTotal;
	}

	double addMarginRate(double productFareAmount, final double marginRate)
	{

		productFareAmount = PrecisionUtil.round(productFareAmount);
		final double marginVal = productFareAmount * marginRate / 100;
		return PrecisionUtil.round(productFareAmount + marginVal);
	}



	private Date getAdjustedDate(final Date date, final int diff)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, diff);
		return cal.getTime();
	}

	private boolean isSameDay(final Date requestedDepartureTime, final Date sailingDepartureTime)
	{
		return DateTimeComparator.getDateOnlyInstance().compare(requestedDepartureTime, sailingDepartureTime) == 0;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
