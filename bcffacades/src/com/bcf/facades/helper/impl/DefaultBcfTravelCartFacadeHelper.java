/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.helper.impl;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.order.CommerceBundleCartModificationException;
import de.hybris.platform.util.TaxValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.service.BcfVacationTermsAndConditionsService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.services.vacation.strategies.CancellationFeeStrategy;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.com.bcf.facades.cart.BcfAddToCartHandlerFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.strategies.impl.PackageBookingCancellationStrategy;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfTravelCartFacadeHelper implements BcfTravelCartFacadeHelper{
	public static final String CASH = "Cash";

	private static final Logger LOG = Logger.getLogger(DefaultBcfTravelCartFacadeHelper.class);
	public static final String GIFT_CARD = "Gift Card";
	public static final String CARD_REFUND_WITH_PIN_PAD = "Card Refund with Pin Pad";
	private static final String NO_ITEMS_ADDED = "basket.information.quantity.noItemsAdded.";
	private static final String REDUCED_NUMBER_OF_ITEMS_ADDED = "basket.information.quantity.reducedNumberOfItemsAdded.";
	private static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";
	private static final String MAKE_BOOKING_ERROR = "makebooking.errorcode.";
	protected static final long PRODUCT_QUANTITY = 1;

	private BCFTravelCartService bcfTravelCartService;
	private ModelService modelService;
	private List<String> bookingFeeCodes;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private CartService cartService;
	private ProductService productService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private SessionService sessionService;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
	private BcfBookingService bcfBookingService;
	private AssistedServiceFacade assistedServiceFacade;
	private NotificationUtils notificationUtils;
	private PackageBookingCancellationStrategy packageBookingCancellationStrategy;
	private CancellationFeeStrategy cancellationFeeStrategy;
	private BcfVacationTermsAndConditionsService bcfVacationTermsAndConditionsService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BaseSiteService baseSiteService;
	private CommerceCartService commerceCartService;
	private BcfDealCartFacade dealCartFacade;
	private BCFTravelRouteFacade travelRouteFacade;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private BcfAddToCartHandlerFacade bcfAddToCartHandlerFacade;
	private BcfCartTimerFacade bcfCartTimerFacade;
	private PropertySourceFacade propertySourceFacade;
	private BcfCalculationService bcfCalculationService;
	private CommonI18NService commerceI18NService;

	@Override
	public void addFeesProductToCart(final BCFMakeBookingResponse response, final int journeyReferenceNumber,
			final int originDestinationNum) throws CommerceCartModificationException
	{
		if (Objects.isNull(response))
		{
			return;
		}
		final List<String> feesProductCodes = new ArrayList<>();
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final boolean useCacheKey = bcfTravelCartService.useCacheKey(cartModel);
		final Map<String, List<AbstractOrderEntryModel>> productEntries = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getAmendStatus().equals(AmendStatus.NEW))
				.collect(Collectors.groupingBy(entry -> entry.getProduct().getCode()));
		StreamUtil.safeStream(response.getItinerary().getSailing().getOtherCharges())
				.filter(productFare -> productFare.getProduct().getProductCategory().equals(BcfCoreConstants.CATEGORY_OTHER))
				.forEach(productFare -> {
					if (!productEntries.containsKey(productFare.getProduct().getProductId())
							&& getBookingFeeCodes().contains(productFare.getProduct().getProductId()))
					{
						feesProductCodes.add(productFare.getProduct().getProductId());

					}
					else if (Objects.nonNull(productEntries.get(productFare.getProduct().getProductId())))
					{
						final Optional<AbstractOrderEntryModel> bookingFeesEntry = getBookingFeeEntry(response, useCacheKey,
								productEntries, productFare);

						if (bookingFeesEntry.isPresent())
						{
							updateBookingFeeEntry(response, productFare, bookingFeesEntry);
						}
						else if (!bookingFeesEntry.isPresent() && getBookingFeeCodes()
								.contains(productFare.getProduct().getProductId()))
						{
							feesProductCodes.add(productFare.getProduct().getProductId());
						}
					}
				});
		if (!Collections.isEmpty(feesProductCodes))
		{
			final Pair<String, String> bookingRefAndCacheKey = getBookingReferenceAndCachekey(journeyReferenceNumber,
					originDestinationNum, cartModel);

			final TravelOrderEntryInfoModel travelOrderEntryInfoModel = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& entry.getJourneyReferenceNumber() == journeyReferenceNumber
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationNum)
					.findAny().get().getTravelOrderEntryInfo();
			for (final String feesProductCode : feesProductCodes)
			{
				addFeeProductToCart(journeyReferenceNumber, originDestinationNum, cartModel, bookingRefAndCacheKey,
						travelOrderEntryInfoModel, feesProductCode);
			}
			recalculateCart();
			getModelService().refresh(cartModel);
		}
	}


	private void addFeeProductToCart(final int journeyReferenceNumber, final int originDestinationNum,
			final CartModel cartModel, final Pair<String, String> bookingRefAndCacheKey,
			final TravelOrderEntryInfoModel travelOrderEntryInfoModel,
			final String feesProductCode) throws CommerceCartModificationException
	{
		final CartModificationData cartModificationData = bcfTravelCartFacade.addToCart(feesProductCode, 1);
		bcfTravelCartFacade.setOrderEntryType(OrderEntryType.TRANSPORT, cartModificationData.getEntry().getEntryNumber());
		setBookingReferenceAndCacheKey(cartModificationData, cartModel, bookingRefAndCacheKey);

		addProductToCartEntry(feesProductCode, cartModel, cartModificationData, originDestinationNum, journeyReferenceNumber);
		final AbstractOrderEntryModel createdEntry = cartModel.getEntries().stream()
				.filter(entryModel -> entryModel.getEntryNumber() == cartModificationData.getEntry().getEntryNumber())
				.findFirst().get();
		createdEntry.setTravelOrderEntryInfo(travelOrderEntryInfoModel);
		getModelService().save(createdEntry);
	}

	private Pair<String, String> getBookingReferenceAndCachekey(final int journeyReferenceNumber, final int originDestinationNum,
			final CartModel cartModel)
	{
		final AbstractOrderEntryModel abstractOrderEntryModel = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
						&& entry.getJourneyReferenceNumber() == journeyReferenceNumber
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationNum)
				.findAny().get();

		return Pair.of(abstractOrderEntryModel.getBookingReference(), abstractOrderEntryModel.getCacheKey());
	}


	private Optional<AbstractOrderEntryModel> getBookingFeeEntry(final BCFMakeBookingResponse response, final boolean useCacheKey,
			final Map<String, List<AbstractOrderEntryModel>> productEntries, final ProductFare productFare)
	{
		return productEntries
				.get(productFare.getProduct().getProductId())
				.stream()
				.filter(entry -> entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE) &&
						(useCacheKey && response.getItinerary().getBooking().getBookingReference().getCachingKey().equals(entry
								.getCacheKey())
								|| (
								!useCacheKey && response.getItinerary().getBooking()
										.getBookingReference().equals(entry
												.getBookingReference()
										))))
				.findAny();
	}

	private void updateBookingFeeEntry(final BCFMakeBookingResponse response, final ProductFare productFare,
			final Optional<AbstractOrderEntryModel> bookingFeesEntry)
	{
		if (StringUtils.isNotEmpty(response.getItinerary().getBooking().getBookingReference().getCachingKey()))
		{
			bookingFeesEntry.get().setCacheKey(response.getItinerary().getBooking().getBookingReference().getCachingKey());
		}
		if (StringUtils.isNotEmpty(response.getItinerary().getBooking().getBookingReference().getBookingReference()))
		{
			bookingFeesEntry.get()
					.setBookingReference(
							response.getItinerary().getBooking().getBookingReference().getBookingReference());
		}
		bookingFeesEntry.get().setBasePrice(
				productFare.getFareDetail().stream().findFirst().get().getNetAmountInCents() / (100.0
						* productFare
						.getProduct()
						.getProductNumber()));
		bookingFeesEntry.get().setCalculated(Boolean.FALSE);
		getModelService().save(bookingFeesEntry.get());
	}

	private void recalculateCart()
	{
		if (getCartService().hasSessionCart())
		{
			bcfTravelCartFacade.recalculateCart();
		}
	}

	@Override
	public void addProductToCartEntry(final String feesProductCode, final CartModel cartModel,
			final CartModificationData cartModificationData, final int originDestinationNum, final int journeyReferenceNumber)
	{
		final Map<String, Object> propertiesMap = new HashMap<>();
		propertiesMap.put(AbstractOrderEntryModel.ACTIVE, Boolean.TRUE);
		propertiesMap.put(AbstractOrderEntryModel.AMENDSTATUS, AmendStatus.NEW);
		propertiesMap.put(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER, journeyReferenceNumber);
		propertiesMap.put(TravelOrderEntryInfoModel.ORIGINDESTINATIONREFNUMBER, originDestinationNum);
		final ProductModel product = getProductService().getProductForCode(feesProductCode);
		bcfTravelCommerceCartService.addPropertiesToCartEntry(cartModel, cartModificationData.getEntry().getEntryNumber(),
				product, propertiesMap);
	}

	private void setBookingReferenceAndCacheKey(final CartModificationData cartModificationData, final CartModel cartModel,
			final Pair<String, String> bookingRefAndCacheKey)
	{
		final Optional<AbstractOrderEntryModel> orderEntryModel = CollectionUtils.isNotEmpty(cartModel.getEntries())
				? cartModel.getEntries().stream().filter(entry ->
				entry.getEntryNumber().equals(cartModificationData.getEntry().getEntryNumber())
		).findFirst()
				: null;
		if (Objects.nonNull(orderEntryModel) && orderEntryModel.isPresent())
		{
			orderEntryModel.get().setBookingReference(bookingRefAndCacheKey.getLeft());
			orderEntryModel.get().setCacheKey(bookingRefAndCacheKey.getRight());
		}
	}

	@Override
	public void updateOpenTicketStatus(final List<CartModificationData> cartModificationDataList)
	{
		final List<Integer> entryNumbers = cartModificationDataList.stream()
				.map(cartModificationData -> cartModificationData.getEntry().getEntryNumber()).collect(
						Collectors.toList());
		final List<ItemModel> itemsToSave = new ArrayList<>();
		entryNumbers.forEach(entryNumber -> {
			final TravelOrderEntryInfoModel travelOrderEntryInfoModel = bcfTravelCartService
					.getEntryForNumber(bcfTravelCartService.getSessionCart(), entryNumber).getTravelOrderEntryInfo();
			if (travelOrderEntryInfoModel != null)
			{
				travelOrderEntryInfoModel.setOpenTicket(true);
				itemsToSave.add(travelOrderEntryInfoModel);
			}
		});
		modelService.saveAll(itemsToSave);
	}

	public static Pair<Integer, Integer> getExtraGuestOccupancies(final List<PassengerTypeQuantityData> passengerTypes,
			final int baseOccupancy)
	{
		final int totalAdultCount = passengerTypes.stream()
				.filter(passengerType -> passengerType.getPassengerType().getCode().equalsIgnoreCase(BcfFacadesConstants.ADULT))
				.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum).get();
		final int totalChildCount = passengerTypes.stream()
				.filter(passengerType -> passengerType.getPassengerType().getCode().equalsIgnoreCase(BcfFacadesConstants.CHILD)).filter(
			Objects::nonNull)
				.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).filter(
						Objects::nonNull).reduce(Integer::sum).orElse(0);
		final int extraProductAdultsNeeded = totalAdultCount > baseOccupancy ? (totalAdultCount - baseOccupancy) : 0;
		final int extraProductChildsNeeded =
				totalChildCount - ((totalAdultCount < baseOccupancy) ? baseOccupancy - totalAdultCount : 0);
		return Pair.of(extraProductAdultsNeeded, extraProductChildsNeeded);
	}

	@Override
	public void setMaxFarePriceMapToSession(final FareSelectionData fareSelectionData)
	{
		final Map<Integer, Map<String, BigDecimal>> originDestTravellerFareMap = new HashMap<>();
		final Map<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMap = fareSelectionData.getPricedItineraries()
				.stream().collect(Collectors.groupingBy(PricedItineraryData::getOriginDestinationRefNumber));
		for (final Map.Entry<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMapEntry : originDestRefNoPricedItinerariesMap
				.entrySet())
		{
			final int originDestinationRefNumber = originDestRefNoPricedItinerariesMapEntry.getKey();
			final List<PricedItineraryData> pricedItineraryDatas = originDestRefNoPricedItinerariesMapEntry.getValue();
			final PricedItineraryData pricedItineraryData = pricedItineraryDatas.stream().findFirst().get();
			final ItineraryPricingInfoData itineraryPricingInfo = pricedItineraryData.getItineraryPricingInfos().stream().findFirst()
					.get();
			originDestTravellerFareMap
					.put(originDestinationRefNumber, getTravellerFarePriceMap(originDestinationRefNumber, itineraryPricingInfo));
		}

		getSessionService().setAttribute(BcfCoreConstants.ORIGINDEST_TRAVELLER_FARE_MAP, originDestTravellerFareMap);
	}

	/**
	 * gets traveller fare price map
	 *
	 * @param originDestinationRefNumber the origin destination ref number
	 * @param itineraryPricingInfo       the itineraryPricingInfo
	 */
	protected Map<String, BigDecimal> getTravellerFarePriceMap(final int originDestinationRefNumber,
			final ItineraryPricingInfoData itineraryPricingInfo)
	{
		final Map<String, BigDecimal> travellerFareMap = new HashMap<>();
		//populate travellerFareMap with passenger fare price
		populateMapWithPassengerFarePrice(travellerFareMap, itineraryPricingInfo);
		//populate travellerFareMap with vehicle fare price
		populateMapWithVehicleFarePrice(travellerFareMap, itineraryPricingInfo);
		//populate travellerFareMap with other charges price
		populateMapWithOtherChargesPrice(travellerFareMap, itineraryPricingInfo);

		return travellerFareMap;
	}

	/**
	 * populate travellerFareMap with passenger fare price
	 *
	 * @param travellerFareMap
	 * @param itineraryPricingInfo
	 */
	protected void populateMapWithPassengerFarePrice(final Map<String, BigDecimal> travellerFareMap,
			final ItineraryPricingInfoData itineraryPricingInfo)
	{
		final Map<String, List<PTCFareBreakdownData>> passengerPtcFareBreakdownDataMap = itineraryPricingInfo
				.getPtcFareBreakdownDatas()
				.stream()
				.filter(
						ptcFareBreakdownData -> ((ptcFareBreakdownData.getPassengerTypeQuantity().getQuantity() > 0) && CollectionUtils
								.isNotEmpty(ptcFareBreakdownData.getFareInfos()))).collect(Collectors
						.groupingBy(
								ptcFareBreakdownData -> ptcFareBreakdownData.getPassengerTypeQuantity().getPassengerType().getCode()));

		for (final Map.Entry<String, List<PTCFareBreakdownData>> passengerPtcFareBreakdownDataMapEntry : passengerPtcFareBreakdownDataMap
				.entrySet())
		{
			final String passengerCode = passengerPtcFareBreakdownDataMapEntry.getKey();
			final PTCFareBreakdownData passengerPtcFareBreakdownData = passengerPtcFareBreakdownDataMapEntry.getValue().stream()
					.findFirst().get();
			final FareInfoData fareInfoData = passengerPtcFareBreakdownData.getFareInfos().stream()
					.filter(fareInfo -> CollectionUtils.isNotEmpty(fareInfo.getFareDetails())).findFirst().get();
			final FareDetailsData fareDetailsData = fareInfoData.getFareDetails().stream().filter(
					fareDetails -> (Objects.nonNull(fareDetails.getFareProduct()) && Objects
							.nonNull(fareDetails.getFareProduct().getPrice()))).findFirst().get();

			final BigDecimal fareProductPriceVal = fareDetailsData.getFareProduct().getPrice().getValue();
			travellerFareMap.put(passengerCode, fareProductPriceVal);
			LOG.info("Fare Product Price for " + passengerCode + " : " + fareProductPriceVal);
		}
	}

	/**
	 * populate travellerFareMap with vehicle fare price
	 *
	 * @param travellerFareMap
	 * @param itineraryPricingInfo
	 */
	protected void populateMapWithVehicleFarePrice(final Map<String, BigDecimal> travellerFareMap,
			final ItineraryPricingInfoData itineraryPricingInfo)
	{
		final Map<String, List<VehicleFareBreakdownData>> vehiclePtcFareBreakdownDataMap = itineraryPricingInfo
				.getVehicleFareBreakdownDatas()
				.stream()
				.filter(vehicleFareBreakdownData -> ((vehicleFareBreakdownData.getVehicleTypeQuantity().getQty() > 0) && Objects
						.nonNull(vehicleFareBreakdownData.getFareInfos()))).collect(Collectors
						.groupingBy(
								vehicleFareBreakdownData -> vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType()
										.getCode()));

		for (final Map.Entry<String, List<VehicleFareBreakdownData>> vehiclePtcFareBreakdownDataMapEntry : vehiclePtcFareBreakdownDataMap
				.entrySet())
		{
			final String vehicleCode = vehiclePtcFareBreakdownDataMapEntry.getKey();
			final VehicleFareBreakdownData vehicleFareBreakdownData = vehiclePtcFareBreakdownDataMapEntry.getValue().stream()
					.findFirst().get();
			final FareInfoData fareInfoData = vehicleFareBreakdownData.getFareInfos();
			final FareDetailsData fareDetailsData = fareInfoData.getFareDetails().stream().filter(
					fareDetails -> (Objects.nonNull(fareDetails.getFareProduct()) && Objects
							.nonNull(fareDetails.getFareProduct().getPrice()))).findFirst().get();

			final BigDecimal fareProductPriceVal = fareDetailsData.getFareProduct().getPrice().getValue();
			travellerFareMap.put(vehicleCode, fareProductPriceVal);
			LOG.info("Fare Product Price for " + vehicleCode + " : " + fareProductPriceVal);
		}
	}

	/**
	 * populate travellerFareMap with other charges price
	 *
	 * @param travellerFareMap
	 * @param itineraryPricingInfo
	 */
	protected void populateMapWithOtherChargesPrice(final Map<String, BigDecimal> travellerFareMap,
			final ItineraryPricingInfoData itineraryPricingInfo)
	{
		if (CollectionUtils.isNotEmpty(itineraryPricingInfo.getOtherCharges()))
		{
			for (final OtherChargeData otherChargeData : itineraryPricingInfo.getOtherCharges())
			{
				final BigDecimal otherChargePriceVal = otherChargeData.getPrice().getValue();
				travellerFareMap.put(otherChargeData.getCode(), otherChargePriceVal);
				LOG.info("Product Price for " + otherChargeData.getCode() + " : " + otherChargePriceVal);
			}
		}
	}

	@Override
	public List<CartModificationData> addReservationFeeToCart(final int journeyReferenceNumber, final int originDestinationNum,
			final List<OtherChargeData> otherChargeDatas) throws CommerceCartModificationException
	{
		final List<CartModificationData> cartModificationDatas = new ArrayList<>();
		final AbstractOrderModel cartModel = getBcfTravelCartService().getOrCreateSessionCart();
		for (final OtherChargeData otherChargeData : otherChargeDatas)
		{
			final String reservationFeeProductCode = otherChargeData.getCode();
			if (!getBookingFeeCodes().contains(reservationFeeProductCode))
			{
				continue;
			}

			final int quantity = otherChargeData.getQuantity();
			final List<AbstractOrderEntryModel> relevantCartEntries = cartModel.getEntries().stream()
					.filter(cartEntry -> (cartEntry.getJourneyReferenceNumber() == journeyReferenceNumber) && (
							cartEntry.getTravelOrderEntryInfo() != null &&
									cartEntry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationNum))
					.collect(Collectors.toList());
			final boolean isReservationFeeExists = relevantCartEntries.stream()
					.anyMatch(cartEntry -> StringUtils.equals(reservationFeeProductCode, cartEntry.getProduct().getCode()));
			if (isReservationFeeExists)
			{
				continue;
			}
			final AbstractOrderEntryModel fareProductEntry = relevantCartEntries.stream()
					.filter(cartEntry -> cartEntry.getActive() && StringUtils
							.equals(FareProductModel._TYPECODE, cartEntry.getProduct().getItemtype()))
					.findAny().orElse(null);
			if (Objects.isNull(fareProductEntry))
			{
				continue;
			}
			final TravelOrderEntryInfoModel travelOrderEntryInfo = fareProductEntry.getTravelOrderEntryInfo();
			final CartModificationData cartModificationData =bcfTravelCartFacade
					.addToCart(reservationFeeProductCode, quantity);
			cartModificationDatas.add(cartModificationData);

			final AbstractOrderEntryModel reservationFeeCreatedCartEntry = cartModel.getEntries().stream()
					.filter(entryModel -> entryModel.getEntryNumber() == cartModificationData.getEntry().getEntryNumber())
					.findFirst().get();
			reservationFeeCreatedCartEntry.setJourneyReferenceNumber(journeyReferenceNumber);
			reservationFeeCreatedCartEntry.setQuantity(Long.valueOf(quantity));
			reservationFeeCreatedCartEntry.setActive(true);
			reservationFeeCreatedCartEntry.setAmendStatus(AmendStatus.NEW);
			reservationFeeCreatedCartEntry.setTravelOrderEntryInfo(travelOrderEntryInfo);
			final double price = getBcfTravelCommerceCartService()
					.getPriceFromListSailingSessionMap(travelOrderEntryInfo.getOriginDestinationRefNumber(),
							reservationFeeProductCode);
			reservationFeeCreatedCartEntry.setBasePrice(price);
			reservationFeeCreatedCartEntry.setMarginCalculated(true);
			getModelService().save(reservationFeeCreatedCartEntry);
		}
		return cartModificationDatas;
	}

	@Override
	public boolean isAlacateFlow()
	{

		return bcfTravelCartService.isAlacateFlow();
	}

	@Override
	public boolean isOnRequestAmendment()
	{
		final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();

		if (getBcfTravelCartFacade().isAmendmentCart() && BcfCoreConstants.PACKAGE_ON_REQUEST
				.equals(abstractOrderModel.getAvailabilityStatus()))
		{

			return true;
		}

		return false;
	}


	@Override
	public boolean isCardPayment()
	{
		return bcfTravelCartService.isCardPayment(bcfTravelCartService.getSessionCart());
	}

	@Override
	public void updateAmountPaidForOnRequestAmendment(final AbstractOrderModel abstractOrderModel)
	{
		abstractOrderModel.setAmountPaid(Double.sum(abstractOrderModel.getAmountPaid(),
				abstractOrderModel.getAmountToPay() != null ? abstractOrderModel.getAmountToPay() : 0d));
		abstractOrderModel.setAmountToPay(0d);
		modelService.save(abstractOrderModel);
	}


	@Override
	public void setCurrentJourneyRefNum()
	{
		final CartModel sessionAmendedCart = bcfTravelCartService.getSessionCart();
		final OptionalInt maxJourneyRefNumber = sessionAmendedCart.getEntries().stream()
				.mapToInt(entry -> entry.getJourneyReferenceNumber()).max();
		sessionAmendedCart.setCurrentJourneyRefNum(maxJourneyRefNumber.getAsInt());
		modelService.save(sessionAmendedCart);
	}

	@Override
	public void calculateChangeFees()
	{

		bcfChangeFeeCalculationService.calculateChangeFees(bcfTravelCartService.getSessionCart());

	}


	@Override
	public VacationPolicyTAndCData getVacationPolicyTermsAndConditions(final String bookingReference)
	{
		return bcfVacationTermsAndConditionsService.getVacationPolicyTermsAndConditions(bookingReference);
	}


	@Override
	public List<BCFNonRefundableCostTAndCData> getBCFNonRefundableCostTermsAndConditions(final String bookingReference)
	{
		final List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTermsAndConditions = bcfVacationTermsAndConditionsService
				.getBCFNonRefundableCostTermsAndConditions(bookingReference);
		return CollectionUtils.isNotEmpty(bcfNonRefundableCostTermsAndConditions) ? bcfNonRefundableCostTermsAndConditions : null;
	}



	@Override
	public Pair<Double, List<RefundTransactionInfoData>> getRefundTransactionInfo(final String bookingReference)
	{

		AbstractOrderModel abstractOrderModel = null;
		Double amountToPay = null;
		if (StringUtils.isEmpty(bookingReference))
		{
			abstractOrderModel = bcfTravelCartService.getSessionCart();
			amountToPay = abstractOrderModel.getAmountToPay();
		}
		else
		{

			abstractOrderModel = bcfBookingService.getOrder(bookingReference);
			final BigDecimal totalPaid = bcfBookingService.calculateTotalToRefund((OrderModel) abstractOrderModel);
			final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = packageBookingCancellationStrategy
					.adjustCancellationFee(totalPaid, cancellationFeeStrategy.calculateCancellationFee(abstractOrderModel));
			final BigDecimal totalCancelFee = packageBookingCancellationStrategy
					.getTotalCancellationFee(bcfChangeCancellationVacationFeeData);
			final PriceData totalToRefund = packageBookingCancellationStrategy
					.getRefundTotal(bcfBookingService.calculateTotalToRefund((OrderModel) abstractOrderModel),
							abstractOrderModel.getCurrency().getDigits(), abstractOrderModel.getCurrency().getIsocode(),
							totalCancelFee);
			amountToPay = -totalToRefund.getValue().doubleValue();
		}

		if (amountToPay != null && (amountToPay < 0))
		{

			amountToPay = Math.abs(amountToPay);
			final List<PaymentTransactionModel> paymentTransactionModels = abstractOrderModel.getPaymentTransactions();
			if (CollectionUtils.isNotEmpty(paymentTransactionModels))
			{

				final List<RefundTransactionInfoData> refundTransactionInfoDatas = populateRefundTransactionInfoData(
						abstractOrderModel,
						amountToPay, paymentTransactionModels);
				return Pair.of(amountToPay, refundTransactionInfoDatas);

			}

		}

		return null;

	}


	@Override
	public List<RefundTransactionInfoData> getGoodWillRefundTransactionInfo(final String bookingReference)
	{

		final AbstractOrderModel abstractOrderModel = bcfBookingService.getOrder(bookingReference);

		final List<PaymentTransactionModel> paymentTransactionModels = abstractOrderModel.getPaymentTransactions();
		if (CollectionUtils.isNotEmpty(paymentTransactionModels))
		{
			final List<RefundTransactionInfoData> refundTransactionInfoDatas = populateGoodWillRefundTransactionInfoData(
					paymentTransactionModels, abstractOrderModel.getCurrency());
			return refundTransactionInfoDatas;
		}

		return null;

	}


	private List<RefundTransactionInfoData> populateGoodWillRefundTransactionInfoData(
			final List<PaymentTransactionModel> paymentTransactionModels, final CurrencyModel currency)
	{
		final List<RefundTransactionInfoData> refundTransactionInfoDatas = new ArrayList<>();
		for (final PaymentTransactionModel transaction : paymentTransactionModels)
		{

			final double totalPayedAmount = transaction.getEntries().stream().filter(
					entry -> (PaymentTransactionType.PURCHASE.equals(entry.getType()) && bcfBookingService
							.isPaymentTransactionEntrySuccess(entry)) || (!PaymentTransactionType.REFUND
							.equals(entry.getType()))).mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

			if(totalPayedAmount>0)
			{
				final double totalRefundedAmount = transaction.getEntries().stream().filter(
						entry -> PaymentTransactionType.REFUND.equals(entry.getType()) && bcfBookingService
								.isPaymentTransactionEntrySuccess(entry)).mapToDouble(entry -> entry.getAmount().doubleValue())
						.sum();

				final boolean debitCard = transaction.getEntries().stream()
						.anyMatch(entry -> StringUtils.isNotBlank(entry.getDebitAccountType()));

				updateGoodWillRefundTransactionInfoData(refundTransactionInfoDatas, transaction,totalPayedAmount,totalRefundedAmount,
						debitCard, currency);
			}


		}
		final List<RefundTransactionInfoData> refundTransactionInfos = refundTransactionInfoDatas.stream()
				.filter(refundTransactionInfo -> refundTransactionInfo.isCardPayment()).collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(refundTransactionInfos))
		{
			final double maxAmountRefundable = refundTransactionInfoDatas.stream()
					.mapToDouble(refundTransactionInfo -> refundTransactionInfo.getMaxAmount()).sum();
			final RefundTransactionInfoData refundTransactionInfoData = new RefundTransactionInfoData();
			refundTransactionInfoData.setMaxAmount(maxAmountRefundable);
			refundTransactionInfoData.setMaxFormattedAmount(
					notificationUtils.formatPrice(maxAmountRefundable, currency));
			refundTransactionInfoData.setCode(BcfCoreConstants.CARD_REFUND_WITH_PIN_PAD);
			refundTransactionInfoData.setRefundType(CARD_REFUND_WITH_PIN_PAD);
			refundTransactionInfoDatas.add(refundTransactionInfoData);

		}
		return refundTransactionInfoDatas;
	}


	private List<RefundTransactionInfoData> populateRefundTransactionInfoData(final AbstractOrderModel abstractOrderModel,
			final Double amountToPay, final List<PaymentTransactionModel> paymentTransactionModels)
	{
		final List<RefundTransactionInfoData> refundTransactionInfoDatas = new ArrayList<>();
		for (final PaymentTransactionModel transaction : paymentTransactionModels)
		{

			final double totalPayedAmount = transaction.getEntries().stream().filter(
					entry -> (PaymentTransactionType.PURCHASE.equals(entry.getType()) && bcfBookingService
							.isPaymentTransactionEntrySuccess(entry)) || (!PaymentTransactionType.REFUND
							.equals(entry.getType()))).mapToDouble(entry -> entry.getAmount().doubleValue()).sum();

			if(totalPayedAmount>0)
			{

				final double totalRefundedAmount = transaction.getEntries().stream().filter(
						entry -> PaymentTransactionType.REFUND.equals(entry.getType()) && bcfBookingService
								.isPaymentTransactionEntrySuccess(entry)).mapToDouble(entry -> entry.getAmount().doubleValue())
						.sum();

				final boolean debitCard = transaction.getEntries().stream()
						.anyMatch(entry -> StringUtils.isNotBlank(entry.getDebitAccountType()));

				updateRefundTransactionInfoData(abstractOrderModel, amountToPay, refundTransactionInfoDatas, transaction,
						totalPayedAmount,
						totalRefundedAmount, debitCard);
			}
		}
		final List<RefundTransactionInfoData> refundTransactionInfos = refundTransactionInfoDatas.stream()
				.filter(refundTransactionInfo -> refundTransactionInfo.isCardPayment()).collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(refundTransactionInfos))
		{
			final RefundTransactionInfoData refundTransactionInfoData = new RefundTransactionInfoData();
			double maxAmountRefundable = refundTransactionInfoDatas.stream()
					.mapToDouble(refundTransactionInfo -> refundTransactionInfo.getMaxAmount()).sum();
			if (maxAmountRefundable > amountToPay)
			{
				maxAmountRefundable = amountToPay;
			}
			refundTransactionInfoData.setMaxAmount(maxAmountRefundable);
			refundTransactionInfoData.setMaxFormattedAmount(
					notificationUtils.formatPrice(maxAmountRefundable, abstractOrderModel.getCurrency()));
			refundTransactionInfoData.setCode(BcfCoreConstants.CARD_REFUND_WITH_PIN_PAD);
			refundTransactionInfoData.setRefundType(CARD_REFUND_WITH_PIN_PAD);
			refundTransactionInfoDatas.add(refundTransactionInfoData);

		}
		return refundTransactionInfoDatas;
	}

	private void updateGoodWillRefundTransactionInfoData(final List<RefundTransactionInfoData> refundTransactionInfoDatas,
			final PaymentTransactionModel transaction,
			final double totalPayedAmount, final double totalRefundedAmount, final boolean debitCard, final CurrencyModel currency)
	{
		final RefundTransactionInfoData refundTransactionInfoData = new RefundTransactionInfoData();
			refundTransactionInfoData.setCode(transaction.getCode());
		final PaymentInfoModel paymentInfo = transaction.getInfo();
			if (paymentInfo instanceof CashPaymentInfoModel)
			{
				refundTransactionInfoData.setRefundType(CASH);

			}
			else if (paymentInfo instanceof CreditCardPaymentInfoModel)
			{

				double maxAmount = totalPayedAmount - totalRefundedAmount;

				maxAmount=maxAmount>=0?maxAmount:0;
				refundTransactionInfoData.setMaxAmount(maxAmount);
				refundTransactionInfoData.setMaxFormattedAmount(
						notificationUtils.formatPrice(maxAmount, currency));
				if (debitCard)
				{
					refundTransactionInfoData.setRefundType(BcfFacadesConstants.DEBIT_CARD);
				}
				else
				{
					refundTransactionInfoData.setRefundType(BcfFacadesConstants.CREDIT_CARD);
				}
				refundTransactionInfoData.setCardPayment(true);
			}
			else
			{
				refundTransactionInfoData.setRefundType(GIFT_CARD);
			}

			refundTransactionInfoDatas.add(refundTransactionInfoData);

	}


	private void updateRefundTransactionInfoData(final AbstractOrderModel abstractOrderModel, final Double amountToPay,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas, final PaymentTransactionModel transaction,
			final double totalPayedAmount, final double totalRefundedAmount, final boolean debitCard)
	{
		if (BigDecimal.valueOf(totalPayedAmount).compareTo(BigDecimal.valueOf(totalRefundedAmount)) > 0)
		{
			final RefundTransactionInfoData refundTransactionInfoData = new RefundTransactionInfoData();
			refundTransactionInfoData.setCode(transaction.getCode());
			final double maxAmount = totalPayedAmount - totalRefundedAmount;
			double maxAmountRefundable = maxAmount > totalPayedAmount ? totalPayedAmount : maxAmount;
			if (maxAmountRefundable > amountToPay)
			{
				maxAmountRefundable = amountToPay;
			}
			refundTransactionInfoData.setMaxAmount(maxAmountRefundable);
			refundTransactionInfoData.setMaxFormattedAmount(
					notificationUtils.formatPrice(maxAmountRefundable, abstractOrderModel.getCurrency()));
			final PaymentInfoModel paymentInfo = transaction.getInfo();
			if (paymentInfo instanceof CashPaymentInfoModel)
			{
				refundTransactionInfoData.setRefundType(CASH);

			}
			else if (paymentInfo instanceof CreditCardPaymentInfoModel)
			{
				if (debitCard)
				{
					refundTransactionInfoData.setRefundType(BcfFacadesConstants.DEBIT_CARD);
				}
				else
				{
					refundTransactionInfoData.setRefundType(BcfFacadesConstants.CREDIT_CARD);
				}
				refundTransactionInfoData.setCardPayment(true);
			}
			else
			{
				refundTransactionInfoData.setRefundType(GIFT_CARD);
			}

			refundTransactionInfoDatas.add(refundTransactionInfoData);
		}
	}

	@Override
	public List<Integer> getLongRouteJourneyRefNumber(){

		if(bcfTravelCartService.hasSessionCart()){

			final CartModel cart = bcfTravelCartService.getSessionCart();
			return cart.getEntries().stream().filter(entry-> Objects.nonNull(entry.getTravelOrderEntryInfo())
					&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute())
					&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType())
					&& Objects.nonNull(entry.getActive()) && entry.getActive()
					&& RouteType.LONG.equals(entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType())).filter(Objects::nonNull).map(entry->entry.getJourneyReferenceNumber()).distinct().collect(Collectors.toList());


		}
		return null;
	}

	@Override
	public void updateAvailabilityStatus(){

		final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();

		if(!bcfTravelCartFacade.isOnRequestBookingPresent(abstractOrderModel))
		{
			abstractOrderModel.setAvailabilityStatus("");
		}
		else
		{

			final String availabilityStatus = bcfTravelCartFacade
					.getStockLevelType(abstractOrderModel.getEntries().stream().filter(entry -> entry.getActive()).collect(
							Collectors.toList()));
			abstractOrderModel.setAvailabilityStatus(availabilityStatus);

			if (!BcfCoreConstants.PACKAGE_ON_REQUEST
					.equals(availabilityStatus))
			{

				//for on request order changing to normal order, dont consider the previous amount paid
				if (bcfTravelCartService.isOriginalOrderOnRequest(abstractOrderModel))
				{
					abstractOrderModel.setAmountToPay(PrecisionUtil.round(BigDecimal.valueOf(bcfCalculationService
							.getTotalPrice(abstractOrderModel)).doubleValue()));
					abstractOrderModel.setAmountPaid(0d);
				}
			}
		}

		modelService.save(abstractOrderModel);
	}

	@Override
	public void removePreviousOnRequestTransaction()
	{
		final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();

		if (!BcfCoreConstants.PACKAGE_ON_REQUEST
				.equals(abstractOrderModel.getAvailabilityStatus()) && bcfTravelCartService
				.isOriginalOrderOnRequest(abstractOrderModel))
		{
			final List<PaymentTransactionModel> paymentTransactionModels = abstractOrderModel.getPaymentTransactions().stream()
					.filter(transaction -> (transaction.getInfo() instanceof CreditCardPaymentInfoModel || transaction
							.getInfo() instanceof CTCTCCardPaymentInfoModel) && CollectionUtils.isEmpty(transaction.getEntries()))
					.collect(
							Collectors.toList());

			if (CollectionUtils.isNotEmpty(paymentTransactionModels))
			{
				modelService.removeAll(paymentTransactionModels);
				modelService.refresh(abstractOrderModel);
			}
		}
	}
	@Override
	public void setCartBookingType(final BookingJourneyType bookingJourneyType)
	{
		final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();
		abstractOrderModel.setBookingJourneyType(bookingJourneyType);
		modelService.save(abstractOrderModel);
	}

	@Override
	public Pair<String, Double> getSubmitBookingUrlAndIsRefund(final PriceData totalPay)
	{
		boolean refund = false;
		if (totalPay.getValue().doubleValue() < 0d)
		{
			refund = true;
		}

		//for on request amendment and card payment , show confirm booking page
		if (isOnRequestAmendment() && isCardPayment())
		{
			return Pair.of("/checkout/multi/payment-method/confirm-booking", totalPay.getValue().doubleValue());
		}

		if (refund)
		{
			return Pair.of("/checkout/multi/payment-method/confirm-refund", totalPay.getValue().doubleValue());
		}
		else
		{
			return Pair.of("/checkout/multi/payment-method/confirm-booking", totalPay.getValue().doubleValue());
		}
	}

	@Override
	public String getPaymentCancelUrl()
	{
		if (isAlacateFlow())
		{
			return "/alacarte-review";
		}
		else
		{
			return "/cart/summary";
		}
	}


	@Override
	public boolean isCartGuestIdentifierMatching(final String cartCode, final String guestBookingIdentifier)
	{

		final CartModel cart = ((BCFTravelCommerceCartService) getCommerceCartService())
				.getCartForCodeAndSite(cartCode, baseSiteService.getCurrentBaseSite());
		if (Objects.isNull(cart))
		{
			return false;
		}
		if (guestBookingIdentifier.equals(cart.getGuid()))
		{
			return true;
		}

		return false;
	}

	@Override
	public int getHoldTimeInSeconds(final SalesApplication salesApplication)
	{
		final CartModel cartModel = bcfTravelCartService.getSessionCart();
		if(OptionBookingUtil.isOptionBooking(cartModel))
		{
			return OptionBookingUtil.getHoldTimeInSeconds(cartModel);
		}
		return bcfTravelCommerceStockService.getStockHoldTimeInMs(salesApplication)/1000;
	}

	@Override
	public boolean checkJourneyType( final List<BookingJourneyType> bookingJourneyTypes)
	{

		final CartModel cartModel = bcfTravelCartService.getSessionCart();
		if (Objects.isNull(cartModel) ||cartModel.getBookingJourneyType()==null )
		{
			return Boolean.FALSE;
		}
		return bookingJourneyTypes.contains(cartModel.getBookingJourneyType());
	}

	@Override
	public List<String> getPromotionMessages(final AbstractOrderModel abstractOrderModel,
			final Collection<AbstractOrderEntryModel> entries)
	{
		final Set<PromotionResultModel> promotionResults = abstractOrderModel.getAllPromotionResults();

		if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(promotionResults))
		{
			final List<PromotionOrderEntryConsumedModel> consumedEntries = promotionResults.stream()
					.filter(promotionResult -> CollectionUtils.isNotEmpty(promotionResult.getConsumedEntries()))
					.flatMap(promotionResult -> promotionResult.getConsumedEntries().stream())
					.filter(consumedEntry -> entries.contains(consumedEntry.getOrderEntry())).collect(
							Collectors.toList());

			if (CollectionUtils.isNotEmpty(consumedEntries))
			{

				return consumedEntries.stream().map(consumedEntry -> consumedEntry.getPromotionResult()).distinct()
						.map(promotionResult -> promotionResult.getMessageFired()).collect(
								Collectors.toList());
			}

		}

		return java.util.Collections.emptyList();

	}

	@Override
	public AddBundleToCartRequestData getAddBundleToCartRequestData(final int originRefNumber)
	{
		final Object fareResultData = sessionService.getAttribute("fareSelectionData");

		if (fareResultData != null)
		{

			final FareSelectionData fareSelectionData = (FareSelectionData) fareResultData;


			if (CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
			{
				final List<PricedItineraryData> pricedItineraryDatas = fareSelectionData.getPricedItineraries().stream()
						.filter(pricedItineraryData -> pricedItineraryData.getOriginDestinationRefNumber() == originRefNumber)
						.collect(Collectors.toList());

				for (final PricedItineraryData pricedItineraryData : pricedItineraryDatas)
				{

					final ItineraryData itineraryData = pricedItineraryData.getItinerary();

					final List<ItineraryPricingInfoData> itineraryPricingInfoDatas = pricedItineraryData.getItineraryPricingInfos();

					if (CollectionUtils.isNotEmpty(itineraryPricingInfoDatas))
					{
						final ItineraryPricingInfoData itineraryPricingInfoData = itineraryPricingInfoDatas.stream()
								.filter(itineraryPricingInfo -> itineraryPricingInfo.isSelected()).findAny().orElse(null);
						if (itineraryPricingInfoData == null)
						{

							continue;
						}
						final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
						addBundleToCartRequestData.setTripType(TripType.RETURN);

						if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getPtcFareBreakdownDatas()))
						{

							addBundleToCartRequestData.setPassengerTypes(itineraryPricingInfoData.getPtcFareBreakdownDatas().stream()
									.map(ptcFareBreakdownData -> ptcFareBreakdownData.getPassengerTypeQuantity()).collect(
											Collectors.toList()));


						}

						if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()))
						{

							final List<VehicleTypeQuantityData> vehicleTypes = itineraryPricingInfoData.getVehicleFareBreakdownDatas()
									.stream()
									.map(vehicleFareBreakdownData -> vehicleFareBreakdownData.getVehicleTypeQuantity()).collect(
											Collectors.toList());

							StreamUtil.safeStream(vehicleTypes).forEach(vehicleTypeQuantityData -> {
								if (vehicleTypeQuantityData.isCarryingLivestock() && (BcfCoreConstants.UNDER_HEIGHT
										.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode()) || BcfCoreConstants.OVERSIZE
										.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode())))
								{
									vehicleTypeQuantityData.getVehicleType().setCode(
											vehicleTypeQuantityData.getVehicleType().getCode()
													+ BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX);
								}
							});


							addBundleToCartRequestData.setVehicleTypes(vehicleTypes);


						}

						addBundleToCartRequestData.setReturnWithDifferentVehicle(false);

						if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getOtherCharges()))
						{

							addBundleToCartRequestData.setOtherChargeDatas(itineraryPricingInfoData.getOtherCharges());

						}

						addBundleToCartRequestData
								.setBundleType(itineraryPricingInfoData
										.getBundleType());



						addBundleToCartRequestData.setTransferSailingIdentifier(pricedItineraryData.getTransferSailingIdentifier());

						final String travelRouteCode = itineraryData.getRoute().getCode();

						final Integer originDestinationRefNumber = pricedItineraryData.getOriginDestinationRefNumber();

						Integer journeyRefNumber = sessionService.getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
						journeyRefNumber = Objects.isNull(journeyRefNumber) ? 0 : journeyRefNumber;

						if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getBundleTemplates()))
						{
							final List<AddBundleToCartData> addBundleToCartDatas = new ArrayList<>();

							for (final TravelBundleTemplateData travelBundleTemplateData : itineraryPricingInfoData.getBundleTemplates())
							{

								final BcfAddBundleToCartData addBundleToCart = new BcfAddBundleToCartData();
								dealCartFacade
										.populateAddBundleToCart(travelBundleTemplateData, addBundleToCart, travelRouteCode,
												true);
								addBundleToCart.setBundleTemplateId(travelBundleTemplateData.getFareProductBundleTemplateId());
								addBundleToCart.setOriginDestinationRefNumber(originDestinationRefNumber);
								addBundleToCart.setTravelRouteCode(travelRouteCode);
								addBundleToCart.setJourneyRefNumber(journeyRefNumber);
								addBundleToCartDatas.add(addBundleToCart);
							}

							addBundleToCartRequestData.setAddBundleToCartData(addBundleToCartDatas);


						}
						addBundleToCartRequestData.setSalesApplication(bcfSalesApplicationResolverService.getCurrentSalesChannel());
						if (bcfTravelCartFacade.isAmendmentCart())
						{
							addBundleToCartRequestData.setAddCachingKeys(true);
							addBundleToCartRequestData.setAmendmentCart(true);
						}
						else
						{
							addBundleToCartRequestData.setAddCachingKeys(false);
						}
						addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.PLAN);
						addBundleToCartRequestData.setSelectedJourneyRefNumber(journeyRefNumber);
						addBundleToCartRequestData.setSelectedOdRefNumber(
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber());

						addBundleToCartRequestData.setHoldTimeInSeconds(
								getHoldTimeInSeconds(addBundleToCartRequestData.getSalesApplication()));
						updateVehicleCode(travelRouteCode, addBundleToCartRequestData);
						setFerryOptionBooking(addBundleToCartRequestData);
						return addBundleToCartRequestData;

					}




				}

			}
		}
		return null;

	}

	@Override
	public void updateVehicleCode(final String travelRouteCode,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final List<String> commercialVehicleCodes = BCFPropertiesUtils
				.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getVehicleTypes()))
		{
			addBundleToCartRequestData.getVehicleTypes().stream().forEach(vehicleTypeQuantityData -> {
				if (Objects.nonNull(vehicleTypeQuantityData.getVehicleType()) && commercialVehicleCodes
						.contains(vehicleTypeQuantityData.getVehicleType().getCode())
						&& vehicleTypeQuantityData.getWidth() > BcfCoreConstants.OVER5500KG_MIN_WIDTH)
				{
					final String vehicleCode;
					final TravelRouteData routeData = travelRouteFacade.getTravelRoute(travelRouteCode);
					if (Objects.nonNull(routeData))
					{
						if (RouteType.LONG.getCode().equalsIgnoreCase(routeData.getRouteType()))
						{
							vehicleCode =
									BcfCoreConstants.C_TYPE_VEHICLE_PREFIX + (((int) Math.ceil(vehicleTypeQuantityData.getWidth())) - 1);
						}
						else
						{
							vehicleCode =
									BcfCoreConstants.S_TYPE_VEHICLE_PREFIX + (((int) Math.ceil(vehicleTypeQuantityData.getWidth())) - 1);
						}
						vehicleTypeQuantityData.getVehicleType().setCode(vehicleCode);
					}
				}
			});
		}
	}

	@Override
	public void setFerryOptionBooking(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getVehicleTypes()))
		{
			final List<String> standardVehicleCodes = BCFPropertiesUtils
					.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfCoreConstants.STANDARD_VEHICLE_CODES));
			final List<String> commercialVehicleCodes = BCFPropertiesUtils
					.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
			final TravelRouteData routeData = travelRouteFacade
					.getTravelRoute(addBundleToCartRequestData.getAddBundleToCartData().get(0).getTravelRouteCode());
			final TransportFacilityData originData = routeData.getOrigin();
			final TransportFacilityData destinationData = routeData.getDestination();
			final double length = addBundleToCartRequestData.getVehicleTypes().get(0).getLength();
			final double height = addBundleToCartRequestData.getVehicleTypes().get(0).getHeight();
			final double width = addBundleToCartRequestData.getVehicleTypes().get(0).getWidth();
			if (standardVehicleCodes.contains(addBundleToCartRequestData.getVehicleTypes().get(0).getVehicleType().getCode()))
			{
				final int standardLengthLimit = Math.min((Objects.isNull(originData.getStandardVehicleLength()) ?
								0 : originData.getStandardVehicleLength()),
						(Objects.isNull(destinationData.getStandardVehicleLength()) ? 0 : destinationData.getStandardVehicleLength()));
				final int standardHeightLimit = Math.min((Objects.isNull(originData.getStandardVehicleHeight()) ?
								0 : originData.getStandardVehicleHeight()),
						(Objects.isNull(destinationData.getStandardVehicleHeight()) ? 0 : destinationData.getStandardVehicleHeight()));
				if (length > standardLengthLimit || height > standardHeightLimit)
				{
					addBundleToCartRequestData.setFerryOptionBooking(true);
				}
			}
			else if (commercialVehicleCodes.contains(addBundleToCartRequestData.getVehicleTypes().get(0).getVehicleType().getCode()))
			{
				final int commercialLengthLimit = Math.min((Objects.isNull(originData.getOver5500VehicleLength()) ?
								0 : originData.getOver5500VehicleLength()),
						(Objects.isNull(destinationData.getOver5500VehicleLength()) ? 0 : destinationData.getOver5500VehicleLength()));
				final int commercialHeightLimit = Math.min((Objects.isNull(originData.getOver5500VehicleHeight()) ?
								0 : originData.getOver5500VehicleHeight()),
						(Objects.isNull(destinationData.getOver5500VehicleHeight()) ? 0 : destinationData.getOver5500VehicleHeight()));
				if (length > commercialLengthLimit || height > commercialHeightLimit || width > BcfCoreConstants.OVER5500KG_MIN_WIDTH)
				{
					addBundleToCartRequestData.setFerryOptionBooking(true);
				}
			}
		}
	}

	/**
	 * Performs the addBundleToCart for the given addBundleToCartRequestData.
	 *
	 * @param addBundleToCartRequestData as the addBundleToCartRequestData
	 * @return a string representing the error message if the addToCartBundle was unsuccessful, an empty string otherwise
	 */
	@Override
	public String addBundleToCart(final AddBundleToCartRequestData addBundleToCartRequestData, final boolean isDynamicPackage,
			final boolean recalculate) throws CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException
	{

			List<CartModificationData> cartModificationDataList = null;
			if (isDynamicPackage)
			{
				cartModificationDataList = bcfTravelCartFacade.addTransportBundleToCart(addBundleToCartRequestData, recalculate);
			}
			else
			{
				if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getAccessibilityDatas()) && CollectionUtils
						.isNotEmpty(addBundleToCartRequestData.getPassengerUidsToBeRemoved()))
				{

					addBundleToCartRequestData.setAccessibilityDatas(addBundleToCartRequestData.getAccessibilityDatas().stream()
							.filter(accessibilityData -> !addBundleToCartRequestData.getPassengerUidsToBeRemoved()
									.contains(accessibilityData.getPassengerUid())).collect(
									Collectors.toList()));
				}

				final AddBundleToCartResponseData addBundleToCartResponseData = bcfAddToCartHandlerFacade
						.performAddToCart(addBundleToCartRequestData);
				if (addBundleToCartResponseData.isCancellationFail())
				{
					return addBundleToCartResponseData.getRemoveSailingResponseDatas().get(0).getErrorSummary();
				}
				cartModificationDataList = addBundleToCartResponseData.getCartModificationDataList();
			}

			for (final CartModificationData cartModification : cartModificationDataList)
			{
				if (cartModification.getQuantityAdded() == 0L)
				{
					return NO_ITEMS_ADDED + cartModification.getStatusCode();
				}
				else if (cartModification.getQuantityAdded() < PRODUCT_QUANTITY)
				{
					return REDUCED_NUMBER_OF_ITEMS_ADDED + cartModification.getStatusCode();
				}
			}
			final int journeyRefNum = ((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
					.getJourneyRefNumber();
			bcfTravelCartFacade.evaluateCartForJourneyRefNumber(journeyRefNum, addBundleToCartRequestData.getSelectedOdRefNumber());
			if (!isDynamicPackage)
			{
				bcfCartTimerFacade.setStockHoldTimeAndAllocationStatus(
						cartModificationDataList.stream().map(cartModificationData -> cartModificationData.getEntry().getEntryNumber())
								.collect(
										Collectors.toList()));
			}
			calculateChangeFees();

		return org.apache.commons.lang3.StringUtils.EMPTY;
	}

	@Override
	public void manageOriginDestTravellerSessionMap(final Integer originDestinationRefNumber)
	{
		final Map<Integer, Map<String, BigDecimal>> unmodifiedOrigDestTravellerFareMap = sessionService
				.getAttribute(BcfCoreConstants.ORIGINDEST_TRAVELLER_FARE_MAP);
		if (MapUtils.isNotEmpty(unmodifiedOrigDestTravellerFareMap))
		{
			final Map<Integer, Map<String, BigDecimal>> originDestTravellerFareMap = new HashMap<>(
					unmodifiedOrigDestTravellerFareMap);
			originDestTravellerFareMap.remove(originDestinationRefNumber);
			if (MapUtils.isEmpty(originDestTravellerFareMap))
			{
				sessionService.removeAttribute(BcfCoreConstants.ORIGINDEST_TRAVELLER_FARE_MAP);
			}
			else
			{
				sessionService.setAttribute(BcfCoreConstants.ORIGINDEST_TRAVELLER_FARE_MAP, originDestTravellerFareMap);
			}
		}
	}

	@Override
	public boolean isQuoteCart(final CartModel cart)
	{
		return cart.isQuote();
	}

	@Override
	public double getGSTTaxForEntry(final AbstractOrderEntryModel abstractOrderEntry)
	{
		if (Objects.isNull(abstractOrderEntry))
		{
			return 0.0d;
		}
		final Collection<TaxValue> taxValues = abstractOrderEntry.getTaxValues();
		if (CollectionUtils.isNotEmpty(taxValues))
		{
			final TaxValue taxValue = taxValues.stream().filter(tax -> BcfCoreConstants.GST.equals(tax.getCode())).findAny()
					.orElse(null);
			if (taxValue != null)
			{
				return getCommerceI18NService()
						.roundCurrency(taxValue.isAbsolute() ? taxValue.getValue() : taxValue.getAppliedValue(),
								abstractOrderEntry.getOrder().getCurrency().getDigits().intValue());
			}
		}
		return 0.0d;
	}

	@Override
	public String getErrorMessage(final String errorPrefix, final String errorCode, final String defaultMessage)
	{
		final String errorMessage = propertySourceFacade.getPropertySourceValue(errorPrefix + errorCode);
		if (errorMessage != null)
		{
			return errorMessage;
		}
		return defaultMessage;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public List<String> getBookingFeeCodes()
	{
		return bookingFeeCodes;
	}

	public void setBookingFeeCodes(final List<String> bookingFeeCodes)
	{
		this.bookingFeeCodes = bookingFeeCodes;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(
			final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}

	public PackageBookingCancellationStrategy getPackageBookingCancellationStrategy()
	{
		return packageBookingCancellationStrategy;
	}

	public void setPackageBookingCancellationStrategy(
			final PackageBookingCancellationStrategy packageBookingCancellationStrategy)
	{
		this.packageBookingCancellationStrategy = packageBookingCancellationStrategy;
	}

	public CancellationFeeStrategy getCancellationFeeStrategy()
	{
		return cancellationFeeStrategy;
	}

	public void setCancellationFeeStrategy(final CancellationFeeStrategy cancellationFeeStrategy)
	{
		this.cancellationFeeStrategy = cancellationFeeStrategy;
	}

	public BcfVacationTermsAndConditionsService getBcfVacationTermsAndConditionsService()
	{
		return bcfVacationTermsAndConditionsService;
	}

	@Required
	public void setBcfVacationTermsAndConditionsService(
			final BcfVacationTermsAndConditionsService bcfVacationTermsAndConditionsService)
	{
		this.bcfVacationTermsAndConditionsService = bcfVacationTermsAndConditionsService;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}


	public BcfDealCartFacade getDealCartFacade()
	{
		return dealCartFacade;
	}

	public void setDealCartFacade(final BcfDealCartFacade dealCartFacade)
	{
		this.dealCartFacade = dealCartFacade;
	}

	public BCFTravelRouteFacade getTravelRouteFacade()
	{
		return travelRouteFacade;
	}

	public void setTravelRouteFacade(final BCFTravelRouteFacade travelRouteFacade)
	{
		this.travelRouteFacade = travelRouteFacade;
	}

	public BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	public BcfCartTimerFacade getBcfCartTimerFacade()
	{
		return bcfCartTimerFacade;
	}

	public void setBcfCartTimerFacade(final BcfCartTimerFacade bcfCartTimerFacade)
	{
		this.bcfCartTimerFacade = bcfCartTimerFacade;
	}

	public BcfAddToCartHandlerFacade getBcfAddToCartHandlerFacade()
	{
		return bcfAddToCartHandlerFacade;
	}

	public void setBcfAddToCartHandlerFacade(final BcfAddToCartHandlerFacade bcfAddToCartHandlerFacade)
	{
		this.bcfAddToCartHandlerFacade = bcfAddToCartHandlerFacade;
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	@Required
	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}
}
