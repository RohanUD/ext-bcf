/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.seo.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import org.apache.log4j.Logger;
import com.bcf.core.model.content.SEOMetaContentModel;
import com.bcf.core.seo.service.SEOMetaContentService;
import com.bcf.facades.article.SEOMetaContentData;
import com.bcf.facades.seo.SEOMetaContentFacade;


public class DefaultSEOMetaContentFacade implements SEOMetaContentFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultSEOMetaContentFacade.class);

	private SEOMetaContentService metaContentService;
	private Converter<SEOMetaContentModel, SEOMetaContentData> seoMetaContentConverter;

	@Override
	public SEOMetaContentData getMetaContent(final String uid, final List<CatalogVersionModel> activeContentCatalogs)
	{

		final SEOMetaContentModel metaContent = getMetaContentService().getMetaContent(uid, activeContentCatalogs);
		if (metaContent == null)
		{
			LOG.info("No SEO content found for uid: " + uid);
			return null;
		}

		return getSeoMetaContentConverter().convert(metaContent);
	}

	public SEOMetaContentService getMetaContentService()
	{
		return metaContentService;
	}

	public void setMetaContentService(final SEOMetaContentService metaContentService)
	{
		this.metaContentService = metaContentService;
	}

	public Converter<SEOMetaContentModel, SEOMetaContentData> getSeoMetaContentConverter()
	{
		return seoMetaContentConverter;
	}

	public void setSeoMetaContentConverter(final Converter<SEOMetaContentModel, SEOMetaContentData> seoMetaContentConverter)
	{
		this.seoMetaContentConverter = seoMetaContentConverter;
	}



}
