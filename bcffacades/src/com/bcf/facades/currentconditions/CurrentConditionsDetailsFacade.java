/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.currentconditions;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.CurrentConditionsData;
import de.hybris.platform.commercefacades.travel.NewsData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeByRouteData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import java.util.List;
import java.util.Map;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CurrentConditionsDetailsFacade
{

	CurrentConditionsData getCurrentConditionsForRoute(String terminalCode, String route)
			throws IntegrationException;

	Map<String, List<TravelRouteInfo>> getCurrentConditionsByTerminals();

	ServiceNoticeData getServiceNoticesForRoute(String terminalCode, final String routeCode);

	SearchPageData<ServiceNoticeByRouteData> getPaginatedServiceNotices(int currentPage, int pageSize);

	List<ServiceNoticeData> getServiceNoticesForSourceAndDestination(final String sourceCode, final String originCode);

	Map<String, List<TravelRouteInfo>> getFerryTrackingLinksByRoutes();

	Map<String, List<TravelRouteInfo>> getFerryTrackingLinksForTerminals(String terminalCode);

	CurrentConditionsData getDeparturesByTerminals(final String terminal, final String terminalsForPage)
			throws IntegrationException;

	CurrentConditionsData getDeparturesByTerminals(final String terminalsForPage) throws IntegrationException;

	CurrentConditionsData getDepartingTerminalsForCurrentConditions();

	ImageData getCurrentConditionsLandingMap() throws CMSItemNotFoundException;

	int getPageRefreshTime();

	CurrentConditionsData getWebcamsTerminalsForCurrentConditions();

	String getRouteFaresGuideUrl();

	SearchPageData<NewsData> getPaginatedNewsReleases(final int currentPage, final int pageSize);

	ServiceNoticeData getNewsReleaseForCode(String code);

	List<String> getSouternGulfIslandTerminals();

	boolean displayTerminalsSchedule(CurrentConditionsData currentConditionsData) throws IntegrationException;

	boolean isTodaySailingFull(CurrentConditionsData currentConditionsData);

	boolean noMoreSailingsForToday(CurrentConditionsData currentConditionsData);

	String nextSailingsForToday(CurrentConditionsData currentConditionsData,
			ScheduleArrivalDepartureData[] currentConditionsNextSailingsResponseData);

	CurrentConditionsData getTodayDeparturesByTerminals(String terminalsForPage) throws IntegrationException;

	CurrentConditionsData getTodayDeparturesByTerminals(String terminal, String terminalsForPage) throws IntegrationException;
}
