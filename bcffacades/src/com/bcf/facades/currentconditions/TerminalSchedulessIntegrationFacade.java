package com.bcf.facades.currentconditions;

import java.util.List;
import java.util.Map;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface TerminalSchedulessIntegrationFacade
{
	ScheduleArrivalDepartureData[] getCurrentConditionsNextSailingsForRoute(String terminalCode, String route,
			String scheduledDate, String limit) throws IntegrationException;

	Map<String, Map<String, List<TerminalSchedulesRouteData>>> getScheduleForTerminals(final List<String> terminalCode,
			final String scheuledDate, final String limit) throws IntegrationException;
}
