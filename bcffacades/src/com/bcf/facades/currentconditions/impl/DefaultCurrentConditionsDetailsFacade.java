/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.currentconditions.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.CurrentConditionsData;
import de.hybris.platform.commercefacades.travel.NewsData;
import de.hybris.platform.commercefacades.travel.RouteInfoData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeByRouteData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.commercefacades.travel.VesselInfoData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcommonsaddon.model.components.CurrentConditionsLandingComponentModel;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteRegion;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.services.servicenotice.ServiceNoticesService;
import com.bcf.core.shipinfo.service.BcfShipInfoService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.facades.currentconditions.CurrentConditionsIntegrationFacade;
import com.bcf.facades.currentconditions.TerminalSchedulessIntegrationFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integration.currentconditionsResponse.data.RouteArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCurrentConditionsDetailsFacade implements CurrentConditionsDetailsFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultCurrentConditionsDetailsFacade.class);

	private static final String COMPONENT_UID = "currentConditionsLandingComponent";
	private static final int PAGE_REFRESH_DEFAULT_VALUE = 120;
	private CurrentConditionsIntegrationFacade currentConditionsIntegrationFacade;
	private TerminalSchedulessIntegrationFacade terminalSchedulesIntegrationFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "defaultBcfTransportFacilityService")
	private BcfTransportFacilityService bcfTransportFacilityService;

	@Resource(name = "bcfTransportVehicleService")
	private BcfTransportVehicleService bcfTransportVehicleService;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

	@Resource(name = "serviceNoticesService")
	private ServiceNoticesService serviceNoticesService;

	@Resource(name = "serviceNoticeConverter")
	private Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeConverter;

	@Resource(name = "newsConverter")
	private Converter<NewsModel, NewsData> newsConverter;

	@Resource(name = "serviceNoticeDetailsConverter")
	private Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeDetailsConverter;

	@Resource(name = "currentConditionsTravelRouteConverter")
	private Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	private BCFTravelRouteService travelRouteService;
	private BcfShipInfoService shipInfoService;

	@Override
	public CurrentConditionsData getCurrentConditionsForRoute(final String terminalCode, final String route)
			throws IntegrationException
	{
		final CurrentConditionsResponseData currentConditionsForRoute = getCurrentConditionsIntegrationFacade()
				.getCurrentConditionsForRoute(terminalCode, route);
		final CurrentConditionsResponseData[] currentConditionsResponseData = new CurrentConditionsResponseData[] {
				currentConditionsForRoute };
		return this.populateTerminalsInfo(currentConditionsResponseData);
	}

	@Override
	public boolean displayTerminalsSchedule(final CurrentConditionsData currentConditionsData) throws IntegrationException
	{
		final AtomicInteger counter = new AtomicInteger(0);

		for (final CurrentConditionsResponseData response : currentConditionsData.getCurrentConditionsResponseData())
		{
			for (final Map.Entry<String, List<RouteArrivalDepartureData>> entry : response.getArrivalDepartures().entrySet())
			{
				for (final RouteArrivalDepartureData val : entry.getValue())
				{
					final RouteArrivalDepartureData routeArrivalDepartureData = entry.getValue().stream()
							.max(Comparator.comparing(RouteArrivalDepartureData::getScheduledDeparture)).get();
					if (StringUtils.isNotEmpty(val.getScheduledDeparture()) && (
							getCurrentDate().before(getParsedDate(val.getScheduledDeparture()))
									&& !(BcfFacadesConstants.CURRENT_CONDITIONS_DETAILS_VESSEL_FULL_VALUE)
									.equals(val.getVesselFullPercent())))
					{
						counter.getAndIncrement();
					}
				}
			}
		}
		return counter.intValue() == 0 ? true : false;
	}

	private Date getParsedDate(final String date)
	{
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				BcfCoreConstants.CURRENT_CONDITIONS_SAILING_DURATION_DATE_FORMAT);
		try
		{
			return simpleDateFormat.parse(date);
		}
		catch (final ParseException e)
		{
			LOG.error("Unable to parse String data to Date object:", e);
		}
		return null;
	}

	@Override
	public String nextSailingsForToday(final CurrentConditionsData currentConditionsData,
			final ScheduleArrivalDepartureData[] currentConditionsNextSailingsResponseData)
	{
		for (final CurrentConditionsResponseData response : currentConditionsData.getCurrentConditionsResponseData())
		{
			for (final Map.Entry<String, List<RouteArrivalDepartureData>> entry : response.getArrivalDepartures().entrySet())
			{
				for (final RouteArrivalDepartureData val : entry.getValue())
				{
					if ((StringUtils.isNotEmpty(val.getScheduledDeparture())) && (
							getCurrentDate().before(getParsedDate(val.getScheduledDeparture())) && (!
									(BcfFacadesConstants.CURRENT_CONDITIONS_DETAILS_VESSEL_FULL_VALUE)
											.equals(val.getVesselFullPercent())
									|| (val.getStatus().equals('S') && ("Y").equals(val
									.getWebDisplaySailingFlag())))))
					{
						return val.getScheduledDeparture();
					}
				}
			}
		}
		if (ArrayUtils.isNotEmpty(currentConditionsNextSailingsResponseData))
		{
			return Arrays.asList(currentConditionsNextSailingsResponseData).get(0).getDeparture() + "N";
		}
		return null;
	}

	@Override
	public boolean noMoreSailingsForToday(final CurrentConditionsData currentConditionsData)
	{
		final AtomicInteger isSailing = new AtomicInteger(0);
		for (final CurrentConditionsResponseData response : currentConditionsData.getCurrentConditionsResponseData())
		{
			for (final Map.Entry<String, List<RouteArrivalDepartureData>> entry : response.getArrivalDepartures().entrySet())
			{
				if (CollectionUtils.isEmpty(entry.getValue())){
					return true;
				}

				final RouteArrivalDepartureData routeArrivalDepartureData = entry.getValue().stream()
						.max(Comparator.comparing(RouteArrivalDepartureData::getScheduledDeparture)).get();
				if (!getCurrentDate().after(getParsedDate(routeArrivalDepartureData.getScheduledDeparture())))
				{
					isSailing.getAndIncrement();
				}
			}
		}
		return isSailing.get() == 0 ? true : false;
	}

	@Override
	public boolean isTodaySailingFull(final CurrentConditionsData currentConditionsData)
	{
		final AtomicInteger isSailing = new AtomicInteger(0);
		for (final CurrentConditionsResponseData response : currentConditionsData.getCurrentConditionsResponseData())
		{
			for (final Map.Entry<String, List<RouteArrivalDepartureData>> entry : response.getArrivalDepartures().entrySet())
			{
				for (final RouteArrivalDepartureData val : entry.getValue())
				{
					final RouteArrivalDepartureData routeArrivalDepartureData = entry.getValue().stream()
							.max(Comparator.comparing(RouteArrivalDepartureData::getScheduledDeparture)).get();

					if ((StringUtils.isNotEmpty(val.getScheduledDeparture())) && (
							getCurrentDate().before(getParsedDate(val.getScheduledDeparture())) && (!
									(BcfFacadesConstants.CURRENT_CONDITIONS_DETAILS_VESSEL_FULL_VALUE)
											.equals(val.getVesselFullPercent())
									|| (val.getStatus().equals('S') && ("Y").equals(val
									.getWebDisplaySailingFlag()))) || getCurrentDate()
									.after(getParsedDate(routeArrivalDepartureData.getScheduledDeparture()))))
					{
						isSailing.getAndIncrement();
					}
				}
			}
		}
		return isSailing.get() == 0 ? true : false;
	}

	private Date getCurrentDate()
	{
		Date now = new Date();

		final DateFormat timeFormat = new SimpleDateFormat(BcfCoreConstants.CURRENT_CONDITIONS_SAILING_DURATION_DATE_FORMAT);
		timeFormat.setTimeZone(TimeZone.getTimeZone("America/Vancouver"));
		final String estTime = timeFormat.format(new Date());
		try
		{
			now = new SimpleDateFormat(BcfCoreConstants.CURRENT_CONDITIONS_SAILING_DURATION_DATE_FORMAT, Locale.ENGLISH)
					.parse(estTime);
		}
		catch (final ParseException e)
		{
			LOG.error("Unable to parse String data to Date object:", e);
		}
		return now;
	}

	@Override
	public Map<String, List<TravelRouteInfo>> getCurrentConditionsByTerminals()
	{
		final Map<String, List<TravelRouteInfo>> currentConditionsRouteData = new LinkedHashMap<>();

		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_LANDING_TERMINALS);
		if (Objects.nonNull(sourceTerminals))
		{
			final String[] terminals = sourceTerminals.split(",");
			for (final String terminal : terminals)
			{
				final TransportFacilityModel transportFacilityModel = bcfTransportFacilityService
						.getTransportFacilityForCode(terminal);
				currentConditionsRouteData
						.put(terminal, bcfTravelRouteFacade.getTravelRoutesFromTerminal(transportFacilityModel));
			}
		}

		return currentConditionsRouteData;
	}

	@Override
	public SearchPageData<ServiceNoticeByRouteData> getPaginatedServiceNotices(final int currentPage, final int pageSize)
	{
		final SearchPageData<ServiceNoticeByRouteData> searchPageData = new SearchPageData<>();


		final SearchPageData<ServiceNoticeModel> paginatedSearchResult = serviceNoticesService
				.getPaginatedServiceNoticesForAllRoutes(createPageableData(currentPage, pageSize));
		final List<ServiceNoticeModel> serviceNotices = paginatedSearchResult.getResults();
		final List<ServiceNoticeData> serviceNoticeDataList = Converters.convertAll(serviceNotices, serviceNoticeConverter);

		final Map<String, List<ServiceNoticeData>> serviceNoticesByRouteRegion = new HashMap<>();


		final List<RouteRegion> allSubscriptionGroups = enumerationService.getEnumerationValues(RouteRegion._TYPECODE);
		allSubscriptionGroups.stream().forEach(group -> serviceNoticesByRouteRegion.put(group.getCode(), Collections.emptyList()));

		serviceNoticeDataList.forEach(notice -> {
			notice.getTravelRoutes().forEach(route -> {
				List<ServiceNoticeData> noticeDatas = null;
				final List<ServiceNoticeData> alreadyNoticeDatas = serviceNoticesByRouteRegion.get(route.getRouteRegion());
				if (CollectionUtils.isEmpty(alreadyNoticeDatas))
				{
					noticeDatas = new ArrayList<>();
				}
				else
				{
					noticeDatas = alreadyNoticeDatas;
				}
				if (alreadyNoticeDatas == null || !alreadyNoticeDatas.contains(notice))
				{
					noticeDatas.add(notice);
					serviceNoticesByRouteRegion.put(route.getRouteRegion(), noticeDatas);
				}
			});
		});


		final ServiceNoticeByRouteData serviceNoticeByRouteData = new ServiceNoticeByRouteData();
		serviceNoticeByRouteData.setServiceNoticesByRouteRegion(serviceNoticesByRouteRegion);
		searchPageData.setResults(Collections.singletonList(serviceNoticeByRouteData));
		searchPageData.setPagination(paginatedSearchResult.getPagination());
		return searchPageData;
	}

	@Override
	public ServiceNoticeData getServiceNoticesForRoute(final String serviceNoticeCode, final String routeCode)
	{
		final ServiceNoticeModel serviceNotice = serviceNoticesService.getServiceNoticeForCode(serviceNoticeCode);
		final TravelRouteInfo conditionsTravelRouteData = currentConditionsTravelRouteConverter
				.convert(travelRouteService.getTravelRoute(routeCode));
		final ServiceNoticeData serviceNoticeData = serviceNoticeDetailsConverter.convert(serviceNotice);
		serviceNoticeData.setTravelRoutes(Arrays.asList(conditionsTravelRouteData));
		return serviceNoticeData;
	}

	@Override
	public List<ServiceNoticeData> getServiceNoticesForSourceAndDestination(final String sourceCode, final String originCode)
	{
		final List<ServiceNoticeModel> serviceNoticesForRouts = serviceNoticesService
				.getServiceNoticesForRoute(sourceCode, originCode);

		final List<ServiceNoticeData> serviceNoticeData = Converters
				.convertAll(serviceNoticesForRouts, serviceNoticeConverter);

		return serviceNoticeData;
	}

	@Override
	public SearchPageData<NewsData> getPaginatedNewsReleases(final int currentPage, final int pageSize)
	{
		final SearchPageData<NewsData> searchPageData = new SearchPageData<>();

		final SearchPageData<NewsModel> paginatedSearchResult = serviceNoticesService
				.getPaginatedAllNewsRelease(createPageableData(currentPage, pageSize));
		final List<NewsModel> newsReleases = paginatedSearchResult.getResults();
		final List<NewsData> newsReleaseDataList = Converters.convertAll(newsReleases, newsConverter);
		searchPageData.setResults(newsReleaseDataList);
		searchPageData.setPagination(paginatedSearchResult.getPagination());
		return searchPageData;
	}

	@Override
	public ServiceNoticeData getNewsReleaseForCode(final String code)
	{
		final ServiceNoticeModel serviceNotice = serviceNoticesService.getServiceNoticeForCode(code);
		return serviceNoticeDetailsConverter.convert(serviceNotice);
	}

	@Override
	public List<String> getSouternGulfIslandTerminals()
	{
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_SOUTHERN_ISLAND_TERMINALS);
		if (StringUtils.isEmpty(sourceTerminals))
		{
			return Collections.emptyList();
		}
		return Arrays.asList(sourceTerminals.split(","));
	}

	private PageableData createPageableData(final int currentPage, final int pageSize)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(currentPage);
		pageableData.setPageSize(pageSize);
		return pageableData;
	}

	@Override
	public Map<String, List<TravelRouteInfo>> getFerryTrackingLinksByRoutes()
	{
		final List<TravelRouteModel> travelRoutes = travelRouteService
				.getTravelRoutesForCurrentConditionsFerryTracking();
		final List<TravelRouteInfo> travelRouteInfos = Converters.convertAll(travelRoutes, currentConditionsTravelRouteConverter);

		return populateTravelRoutes(travelRouteInfos);
	}

	@Override
	public CurrentConditionsData getTodayDeparturesByTerminals(final String terminalsForPage) throws IntegrationException
	{
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(terminalsForPage);
		if (Objects.nonNull(sourceTerminals))
		{
			final CurrentConditionsResponseData[] currentConditionsForTerminals = currentConditionsIntegrationFacade
					.getCurrentConditionsForTerminals(Arrays.asList(sourceTerminals.split(",")));
			return this.populateTerminalsInfo(currentConditionsForTerminals);
		}
		return null;
	}

	@Override
	public CurrentConditionsData getTodayDeparturesByTerminals(final String terminal, final String terminalsForPage)
			throws IntegrationException
	{
		if (StringUtils.isEmpty(terminal))
		{
			return this.getTodayDeparturesByTerminals(terminalsForPage);
		}


		final CurrentConditionsResponseData[] currentConditionsForTerminals = currentConditionsIntegrationFacade
				.getCurrentConditionsForTerminals(Arrays.asList(terminal));
		return this.populateTerminalsInfo(currentConditionsForTerminals);
	}

	private Map<String, List<TravelRouteInfo>> populateTravelRoutes(final List<TravelRouteInfo> travelRoutes)
	{
		final Map<String, List<TravelRouteInfo>> currentConditionsRouteData = new LinkedHashMap<>();

		final List<String> routeRegions = new ArrayList<>();
		for (final TravelRouteInfo travelRouteInfo : travelRoutes)
		{
			final String routeRegion = travelRouteInfo.getRouteRegion();
			if (Objects.isNull(routeRegion))
			{
				LOG.error("Travel Route is missing the route region for the route " + travelRouteInfo.getRouteCode()
						+ ", destination terminal " +
						travelRouteInfo.getDestinationTerminalCode() + ", source terminal " + travelRouteInfo.getSourceTerminalCode());
			}
			else
			{
				if (!routeRegions.contains(routeRegion))
				{
					routeRegions.add(routeRegion);
				}
			}
		}

		routeRegions.stream().forEach(routeRegion -> {
			final String enumerationName = enumerationService.getEnumerationName(RouteRegion.valueOf(routeRegion));
			currentConditionsRouteData.put(enumerationName, travelRoutes.stream().filter(travelRout ->
					Objects.nonNull(travelRout.getRouteRegion()) && travelRout.getRouteRegion().equals(routeRegion))
					.collect(Collectors.toList()));
		});

		return currentConditionsRouteData;
	}

	@Override
	public Map<String, List<TravelRouteInfo>> getFerryTrackingLinksForTerminals(final String terminalCode)
	{
		final List<TravelRouteInfo> travelRoutes = new ArrayList<TravelRouteInfo>();

		if (StringUtils.isNotEmpty(terminalCode))
		{
			final TransportFacilityModel transportFacilityModel = bcfTransportFacilityService
					.getTransportFacilityForCode(terminalCode);
			travelRoutes.addAll(bcfTravelRouteFacade.getTravelRoutesFromTerminal(transportFacilityModel));
		}
		else
		{
			// Get configured terminals and collect all travel routes for all those terminals
			final String sourceTerminals = bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_LANDING_TERMINALS);
			if (Objects.nonNull(sourceTerminals))
			{
				final String[] terminals = sourceTerminals.split(",");
				final List<TransportFacilityModel> transportFacilities = bcfTransportFacilityService
						.getTransportFacilityForTerminalCodes(Arrays.asList(terminals));
				transportFacilities.stream().forEach(transportFacilityModel -> {
					travelRoutes.addAll(bcfTravelRouteFacade.getTravelRoutesFromTerminal(transportFacilityModel));
				});
			}
		}

		return this.populateTravelRoutes(travelRoutes);
	}

	@Override
	public CurrentConditionsData getDeparturesByTerminals(final String terminalsForPage) throws IntegrationException
	{
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(terminalsForPage);
		if (Objects.nonNull(sourceTerminals))
		{
			final CurrentConditionsResponseData[] currentConditionsForTerminals = currentConditionsIntegrationFacade
					.getCurrentConditionsForTerminals(Arrays.asList(sourceTerminals.split(",")));
			return getTomorrowSailings(currentConditionsForTerminals);
		}
		return null;
	}

	@Override
	public CurrentConditionsData getDeparturesByTerminals(final String terminal, final String terminalsForPage)
			throws IntegrationException
	{
		if (StringUtils.isEmpty(terminal))
		{
			return this.getDeparturesByTerminals(terminalsForPage);
		}

		final CurrentConditionsResponseData[] currentConditionsForTerminals = currentConditionsIntegrationFacade
				.getCurrentConditionsForTerminals(Arrays.asList(terminal));
		final List<String> terminals = new ArrayList<>();

		return getTomorrowSailings(currentConditionsForTerminals);
	}

	private CurrentConditionsData getTomorrowSailings(final CurrentConditionsResponseData[] currentConditionsForTerminals)
			throws IntegrationException
	{
		final List<String> terminals = new ArrayList<>();

		Arrays.asList(currentConditionsForTerminals).stream()
				.forEach(term -> term.getArrivalDepartures().entrySet().stream().forEach(entry -> {
					final int entrySize = (int) entry.getValue().stream()
							.filter(e -> getParsedDate(e.getScheduledDeparture()).after(getCurrentDate())).count();
					if (entrySize < Integer.valueOf(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_LIMIT_VALUE))
					{
						entry.getValue().stream().forEach(val -> {
							terminals.add(val.getDept());
						});
					}
				}));

		final List<String> distinctTerminals = terminals.stream().distinct().collect(Collectors.toList());
		Map<String, Map<String, List<TerminalSchedulesRouteData>>> scheduleForTerminals = null;

		if (distinctTerminals != null)
		{
			scheduleForTerminals = getTerminalSchedulesIntegrationFacade()
					.getScheduleForTerminals(distinctTerminals, LocalDate.now().plusDays(1L).toString(),
							BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_LIMIT_VALUE);
		}

		return this.populateTerminalsInfos(currentConditionsForTerminals, scheduleForTerminals);
	}

	@Override
	public CurrentConditionsData getWebcamsTerminalsForCurrentConditions()
	{
		final CurrentConditionsData currentConditionsData = new CurrentConditionsData();
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_WEBCAM_TERMINALS);
		return getCurrentConditionsDataForTerminals(currentConditionsData, sourceTerminals);
	}

	@Override
	public String getRouteFaresGuideUrl()
	{
		final String routeFaresGuideMediaId = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.ROUTE_FARES_GUIDE_MEDIA_ID);
		if (Objects.isNull(routeFaresGuideMediaId))
		{
			LOG.error("Route fares guide medial id is not set in configurable properties");
			return null;
		}

		try
		{
			final MediaModel media = mediaService
					.getMedia(catalogVersionService.getCatalogVersion(BcfCoreConstants.TRANSACTION_CATALOG_ID, "Online"),
							routeFaresGuideMediaId);
			if (Objects.isNull(media))
			{
				LOG.error("Route fares media not found for the code " + routeFaresGuideMediaId);
				return null;
			}

			final ImageData imageData = imageConverter.convert(media);
			return imageData.getUrl();
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.error(
					"No route fares guid has been found with the media id " + routeFaresGuideMediaId + " in online catalog version "
							+ BcfCoreConstants.TRANSACTION_CATALOG_ID);
		}

		return null;
	}

	@Override
	public CurrentConditionsData getDepartingTerminalsForCurrentConditions()
	{
		final CurrentConditionsData currentConditionsData = new CurrentConditionsData();
		final String sourceTerminals = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS);
		return getCurrentConditionsDataForTerminals(currentConditionsData, sourceTerminals);
	}

	protected CurrentConditionsData getCurrentConditionsDataForTerminals(final CurrentConditionsData currentConditionsData,
			final String sourceTerminals)
	{
		if (Objects.nonNull(sourceTerminals))
		{

			final Map<String, RouteInfoData> terminalNames = new LinkedHashMap<>();

			final String[] departingTerminals = sourceTerminals.split(",");
			for (final String departingTerminal : departingTerminals)
			{
				this.getTerminalAndLocationNamesForCode(terminalNames, departingTerminal);
			}

			currentConditionsData.setTerminals(terminalNames);
		}
		return currentConditionsData;
	}

	@Override
	public ImageData getCurrentConditionsLandingMap() throws CMSItemNotFoundException
	{
		final CurrentConditionsLandingComponentModel component = cmsComponentService
				.getAbstractCMSComponent(COMPONENT_UID);
		return imageConverter.convert(component.getMap());
	}

	@Override
	public int getPageRefreshTime()
	{
		final String pageRefreshTimeInMillis = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_DETAILS_PAGE_REFRESH_TIME);
		if (StringUtils.isNotEmpty(pageRefreshTimeInMillis))
		{
			try
			{
				return Integer.parseInt(pageRefreshTimeInMillis);
			}
			catch (final NumberFormatException nfEx)
			{
				LOG.error(nfEx.getMessage(), nfEx);
			}
		}
		return PAGE_REFRESH_DEFAULT_VALUE;
	}

	private CurrentConditionsData populateTerminalsInfos(final CurrentConditionsResponseData[] currentConditionsForTerminals,
			final Map<String, Map<String, List<TerminalSchedulesRouteData>>> schedulesForTerminals)
	{
		final CurrentConditionsData currentConditionsData = new CurrentConditionsData();
		final Map<String, RouteInfoData> terminals = new HashMap<>();
		final Map<String, VesselInfoData> vessels = new HashMap<>();
		final Map<String, List<RouteArrivalDepartureData>> scheduleArrivalDepartureData = new HashMap<>();
		final List<CurrentConditionsResponseData> currentConditionsResponse = Arrays.asList(currentConditionsForTerminals);
		currentConditionsResponse.stream()
				.forEach(term -> term.getArrivalDepartures().entrySet().stream().forEach(entry -> {
					final List<RouteArrivalDepartureData> entryValue = entry.getValue();
					final List<RouteArrivalDepartureData> routeArrivalsDepartures = new ArrayList<>();
					schedulesForTerminals.entrySet().stream().forEach(ent -> {

						ent.getValue().entrySet().stream().forEach(value -> {
							if (StringUtils.equalsIgnoreCase((("route" + value.getKey())), entry.getKey()) && CollectionUtils
									.isNotEmpty(entry.getValue()) && StringUtils
									.equalsIgnoreCase(entry.getValue().get(0).getDept(), ent.getKey()))
							{
								final List routeData = value.getValue();
								final int entrySize = (int) entry.getValue().stream()
										.filter(e -> getParsedDate(e.getScheduledDeparture()).after(getCurrentDate())).count();
								final int routeSize = routeData.size();
								final int size =
										Integer.valueOf(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_LIMIT_VALUE) - entrySize
												>= routeSize ?
												routeSize :
												Integer.valueOf(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_LIMIT_VALUE)
														- entrySize;
								for (int i = 0; i < size; i++)
								{
									final RouteArrivalDepartureData routeArrivalsDeparture = new RouteArrivalDepartureData();
									routeArrivalsDeparture
											.setScheduledDeparture(Objects.nonNull(((LinkedHashMap) routeData.get(i)).get("departure")) ?
													((LinkedHashMap) routeData.get(i)).get("departure").toString() :
													"");
									routeArrivalsDeparture.setVesselFullPercent(
											Objects.nonNull(((LinkedHashMap) routeData.get(i)).get("vesselFullPercent")) ?
													((LinkedHashMap) routeData.get(i)).get("vesselFullPercent").toString() :
													"");
									routeArrivalsDeparture
											.setDest(Objects.nonNull(((LinkedHashMap) routeData.get(i)).get("dest1")) ?
													((LinkedHashMap) routeData.get(i)).get("dest1").toString() :
													"");
									routeArrivalsDeparture.setDept((ent.getKey().toUpperCase()));
									routeArrivalsDeparture
											.setStatus(Objects.nonNull(((LinkedHashMap) routeData.get(i)).get("status")) ?
													((LinkedHashMap) routeData.get(i)).get("status").toString() :
													"");
									routeArrivalsDeparture.setRoute(((LinkedHashMap) routeData.get(i)).get("route").toString());
									routeArrivalsDepartures.add(routeArrivalsDeparture);
								}
							}
						});
					});
					entry.getValue().addAll(routeArrivalsDepartures);
				}));

		currentConditionsData.setCurrentConditionsResponseData(currentConditionsResponse);
		currentConditionsData.setTerminals(terminals);
		currentConditionsData.setVessels(vessels);

		for (
				final CurrentConditionsResponseData currentConditionsResponseData : currentConditionsForTerminals)

		{
			currentConditionsResponseData.getArrivalDepartures().forEach((key, val) -> {
				if (Objects.nonNull(val))
				{
					val.stream().forEach(routeArrivalDepartureData -> {
						getTerminalAndLocationNamesForCode(terminals, routeArrivalDepartureData.getDept());
						getTerminalAndLocationNamesForCode(terminals, routeArrivalDepartureData.getDest());
						getVesselNameForCode(vessels, routeArrivalDepartureData.getVessel());
					});
				}
			});
		}
		return currentConditionsData;
	}

	private CurrentConditionsData populateTerminalsInfo(final CurrentConditionsResponseData[] currentConditionsForTerminals)
	{
		final CurrentConditionsData currentConditionsData = new CurrentConditionsData();
		final Map<String, RouteInfoData> terminals = new HashMap<>();
		final Map<String, VesselInfoData> vessels = new HashMap<>();

		currentConditionsData.setCurrentConditionsResponseData(Arrays.asList(currentConditionsForTerminals));
		currentConditionsData.setTerminals(terminals);
		currentConditionsData.setVessels(vessels);

		for (final CurrentConditionsResponseData currentConditionsResponseData : currentConditionsForTerminals)
		{
			currentConditionsResponseData.getArrivalDepartures().forEach((key, val) -> {
				if (Objects.nonNull(val))
				{
					val.stream().forEach(routeArrivalDepartureData -> {
						getTerminalAndLocationNamesForCode(terminals, routeArrivalDepartureData.getDept());
						getTerminalAndLocationNamesForCode(terminals, routeArrivalDepartureData.getDest());
						getVesselNameForCode(vessels, routeArrivalDepartureData.getVessel());
					});
				}
			});
		}
		return currentConditionsData;
	}

	private void getVesselNameForCode(final Map<String, VesselInfoData> vesselNames, final String vesselCode)
	{
		if (StringUtils.isNotEmpty(vesselCode) && !vesselNames.containsKey(vesselCode))
		{
			final ShipInfoModel shipInfo = getShipInfoService().getShipInfoByOSSVesselCode(vesselCode);
			final VesselInfoData vesselInfoData = new VesselInfoData();
			if (Objects.isNull(shipInfo))
			{
				LOG.error("Ship Info is not found for the code " + vesselCode);
				vesselInfoData.setCode(vesselCode);
				vesselInfoData.setShipInfoCode(vesselCode);
				vesselInfoData.setName(vesselCode);
				vesselInfoData.setTranslatedName(vesselCode);
			}
			if (Objects.nonNull(shipInfo))
			{
				vesselInfoData.setCode(vesselCode);
				vesselInfoData.setShipInfoCode(shipInfo.getCode());
				vesselInfoData.setName(shipInfo.getName());
				vesselInfoData.setTranslatedName(BcfTravelRouteUtil.translateName(shipInfo.getName()));
			}

			vesselNames.put(vesselCode, vesselInfoData);
		}
	}

	private void getTerminalAndLocationNamesForCode(final Map<String, RouteInfoData> terminalNames,
			final String terminalCode)
	{
		if (StringUtils.isNotEmpty(terminalCode) && !terminalNames.containsKey(terminalCode))
		{
			final TransportFacilityModel transportFacility = bcfTransportFacilityService
					.getTransportFacilityForCode(terminalCode);
			final RouteInfoData routeInfoData = new RouteInfoData();
			routeInfoData.setCode(transportFacility.getCode());
			routeInfoData.setName(transportFacility.getName());
			routeInfoData.setTranslatedName(BcfTravelRouteUtil.translateName(transportFacility.getName()));
			terminalNames.put(terminalCode, routeInfoData);
		}
	}

	private CurrentConditionsIntegrationFacade getCurrentConditionsIntegrationFacade()
	{
		return currentConditionsIntegrationFacade;
	}

	@Required
	public void setCurrentConditionsIntegrationFacade(final CurrentConditionsIntegrationFacade currentConditionsIntegrationFacade)
	{
		this.currentConditionsIntegrationFacade = currentConditionsIntegrationFacade;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	public BcfShipInfoService getShipInfoService()
	{
		return shipInfoService;
	}

	@Required
	public void setShipInfoService(final BcfShipInfoService shipInfoService)
	{
		this.shipInfoService = shipInfoService;
	}

	public TerminalSchedulessIntegrationFacade getTerminalSchedulesIntegrationFacade()
	{
		return terminalSchedulesIntegrationFacade;
	}

	@Required
	public void setTerminalSchedulesIntegrationFacade(
			final TerminalSchedulessIntegrationFacade terminalSchedulesIntegrationFacade)
	{
		this.terminalSchedulesIntegrationFacade = terminalSchedulesIntegrationFacade;
	}
}
