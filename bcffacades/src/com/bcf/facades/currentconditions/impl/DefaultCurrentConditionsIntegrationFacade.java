/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.currentconditions.impl;

import java.util.List;
import com.bcf.facades.currentconditions.CurrentConditionsIntegrationFacade;
import com.bcf.integration.currentconditions.service.CurrentConditionsIntegrationService;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCurrentConditionsIntegrationFacade implements CurrentConditionsIntegrationFacade
{

	private CurrentConditionsIntegrationService currentConditionsIntegrationService;

	@Override
	public CurrentConditionsResponseData getCurrentConditions(final String terminalCode) throws IntegrationException
	{
		return getCurrentConditionsIntegrationService().getCurrentConditionsForTerminal(terminalCode);
	}

	@Override
	public CurrentConditionsResponseData getCurrentConditionsForRoute(final String terminalCode, final String route)
			throws IntegrationException
	{
		return getCurrentConditionsIntegrationService().getCurrentConditionsForRoute(terminalCode, route);
	}

	@Override
	public CurrentConditionsResponseData[] getCurrentConditionsForTerminals(final List<String> terminals)
			throws IntegrationException
	{
		return getCurrentConditionsIntegrationService().getCurrentConditionsForTerminals(terminals);
	}

	public CurrentConditionsIntegrationService getCurrentConditionsIntegrationService()
	{
		return currentConditionsIntegrationService;
	}

	public void setCurrentConditionsIntegrationService(
			final CurrentConditionsIntegrationService currentConditionsIntegrationService)
	{
		this.currentConditionsIntegrationService = currentConditionsIntegrationService;
	}
}
