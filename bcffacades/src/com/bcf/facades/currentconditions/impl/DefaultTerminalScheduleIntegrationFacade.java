package com.bcf.facades.currentconditions.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.currentconditions.TerminalSchedulessIntegrationFacade;
import com.bcf.integration.currentconditions.service.TerminalSchedulesIntegrationService;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultTerminalScheduleIntegrationFacade implements TerminalSchedulessIntegrationFacade
{

	private TerminalSchedulesIntegrationService terminalSchedulesIntegrationService;

	@Override
	public ScheduleArrivalDepartureData[] getCurrentConditionsNextSailingsForRoute(final String terminalCode,
			final String route,
			final String scheduledDate, final String limit)
			throws IntegrationException
	{
		return getTerminalSchedulesIntegrationService()
				.getCurrentConditionsNextSailingsForRoute(terminalCode, route, scheduledDate, limit);
	}

	@Override
	public Map<String, Map<String, List<TerminalSchedulesRouteData>>> getScheduleForTerminals(final List<String> terminalCode,
			final String scheuledDate, final String limit) throws IntegrationException
	{
		return getTerminalSchedulesIntegrationService().getScheduleForTerminals(terminalCode, scheuledDate, limit);
	}

	public TerminalSchedulesIntegrationService getTerminalSchedulesIntegrationService()
	{
		return terminalSchedulesIntegrationService;
	}

	@Required
	public void setTerminalSchedulesIntegrationService(
			final TerminalSchedulesIntegrationService terminalSchedulesIntegrationService)
	{
		this.terminalSchedulesIntegrationService = terminalSchedulesIntegrationService;
	}
}
