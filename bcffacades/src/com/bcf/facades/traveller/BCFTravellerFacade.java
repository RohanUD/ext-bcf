/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.traveller;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.PassengerRequestData;
import com.bcf.facades.cart.VehicleRequestData;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BCFTravellerFacade extends TravellerFacade
{

	/**
	 * Method adds a new traveller to the user's list of saved traveller.
	 *
	 * @return
	 */
	TravellerData addCustomerSavedTravellers(TravellerData travellerData);


	/**
	 * Creates a new instance of TravellerData with relevant details populated
	 *
	 * @param travellerType      the traveller type
	 * @param passengerType      the passenger type
	 * @param travellerCode      the traveller code
	 * @param passengerNumber    the passenger number
	 * @param travellerUidPrefix the traveller uid prefix
	 * @return newly created instance of Traveller
	 */
	TravellerData createVehicleTraveller(String travellerType, VehicleTypeQuantityData vehicleData, String travellerCode,
			int passengerNumber, String travellerUidPrefix);

	/**
	 * Creates a new instance of TravellerData with relevant details populated
	 *
	 * @param travellerType      the traveller type
	 * @param passengerType      the passenger type
	 * @param travellerCode      the traveller code
	 * @param passengerNumber    the passenger number
	 * @param travellerUidPrefix the traveller uid prefix
	 * @param cartOrOrderCode    the cartOrOrderCode
	 * @return newly created instance of Traveller
	 */
	TravellerData createVehicleTraveller(String travellerType, VehicleTypeQuantityData vehicleData, String travellerCode,
			int passengerNumber, String travellerUidPrefix, String cartOrOrderCode);


	/**
	 * returns travellers grouped by journey reference number
	 */
	BCFTravellersData getTravellersGroupedByJourney(CartFilterParamsDto cartFilterParamsDto);

	/**
	 * sets services that are saved for travellers for a journey ref number. This method is specifically used for
	 * Ancillary page
	 *
	 * @param journey                                journey ref number
	 * @param travellersPerJourney                   travellers for this journey ref number
	 * @param selectedServicesForJourneyAndTraveller list where the special services will be saved
	 */
	void setTravellerPerOriginDestination(final Integer journey, final List<TravellerData> travellersPerJourney,
			final List<String> selectedServicesForJourneyAndTraveller);

	AddBundleToCartRequestData getRequestData(
			final BCFTravellersData bcfTravellersData, final int journeyRefNo, final int odRefNo);

	void updateTravellerInformation(
			final AddBundleToCartRequestData outboundAddBundleToCartRequestData, final List<TravellerData> travellerDataList) throws
			IntegrationException;

	TravellerData createTravellerWithSpecialServiceRequest(
			String travellerType, String passengerType, String travellerCode, int passengerNumber,
			String travellerUidPrefix, String cartOrOrderCode, List<SpecialServicesData> specialServicesDataList);

	void updateTravellerWithSpecialServiceRequest(TravellerData travellerData,
			final List<SpecialServicesData> specialServicesDataList);

	public BCFTravellersData getPassengersByTypeGroupedByJourney(Integer journeyRefNum, String eBookingRef,
			List<String> routes, Set<String> passengerTypes, List<Pair<String, String>> passengerTypeAndUidsToBeRemoved);

	public Map<String, Long> getPassengersTypeAndQuantityFromCart(Integer journeyRefNum, String eBookingRef);

	public Map<String, Long> getAccessibilityPassengersTypeAndQuantityFromCart(Integer journeyRefNum, String eBookingRef);

	public List<TravellerData> getTravellersForCartEntries(final int journeyRefNo, final int odRefNumber);

	List<PassengerRequestData> getPassengerTraveller(List<TravellerData> travellerData,
			List<PassengerTypeData> passengerTypeDataList);

	VehicleRequestData getVehicleTraveller(TravellerData traveller, List<VehicleTypeData> vehicleTypeDataList);

	void populateQuantityData(List<PassengerTypeData> passengerTypeDataList, List<VehicleTypeData> vehicleTypeDataList,
			List<PassengerTypeQuantityData> passengerTypeQuantityDataList,
			List<VehicleTypeQuantityData> vehicleTypeQuantityDataList);

	public void getUserSelectedSpecialServiceData(final BCFTravellersData bcfTravellersData,
			final List<AccessibilityRequestData> accessibilityRequestDataList,final List<AccessibilityRequestData> inboundAccessibilityRequestDataList);
}
