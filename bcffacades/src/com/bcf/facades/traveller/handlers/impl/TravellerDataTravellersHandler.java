/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.traveller.handlers.impl;

import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.SpecialRequestDetailData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.travel.data.BCFTravellerDataPerJourney;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.handlers.TravellerDataHandler;


public class TravellerDataTravellersHandler implements TravellerDataHandler
{

	private BCFTravellerService travellerService;
	private Converter<TravellerModel, TravellerData> travellerDataConverter;

	@Override
	public void handle(final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final BCFTravellersData travellersData)
	{
		final Iterator<BCFTravellerDataPerJourney> itr = travellersData.getTravellerDataPerJourney().iterator();
		while (itr.hasNext())
		{
			final BCFTravellerDataPerJourney travellerDataPerJourney = itr.next();
			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(travellerDataPerJourney.getJourneyReferenceNumber());
			final List<AbstractOrderEntryModel> outboundEntries = entries.stream().filter(
					abstractOrderEntryModel -> Objects.nonNull(abstractOrderEntryModel.getTravelOrderEntryInfo()) && (
							abstractOrderEntryModel.getTravelOrderEntryInfo().getOriginDestinationRefNumber()
									== BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER)).collect(Collectors.toList());
			final List<TravellerData> outBoundTravellerData = Converters
					.convertAll(getTravellerService().getTravellers(outboundEntries),
							getTravellerDataConverter());
			travellerDataPerJourney.setOutboundTravelDate(getTravelDate(outboundEntries));
			final List<AbstractOrderEntryModel> ancillaryEntries = outboundEntries.stream().filter(
					entry -> entry.getTravelOrderEntryInfo() != null && entry.getProduct().getItemtype()
							.equals(AncillaryProductModel._TYPECODE)).collect(
					Collectors.toList());
			if (CollectionUtils.isNotEmpty(ancillaryEntries))
			{

				for (final AbstractOrderEntryModel ancillaryEntry : ancillaryEntries)
				{
					TravellerData travellerData = null;
					if (CollectionUtils.isNotEmpty(ancillaryEntry.getTravelOrderEntryInfo().getTravellers()))
					{
						travellerData = outBoundTravellerData.stream().filter(outBoundTraveller -> outBoundTraveller.getUid()
								.equalsIgnoreCase(ancillaryEntry.getTravelOrderEntryInfo().getTravellers().iterator().next().getUid()))
								.findAny().orElse(null);
					}
					if (travellerData != null)
					{
						SpecialRequestDetailData specialRequestDetailData = travellerData.getSpecialRequestDetail();
						if (specialRequestDetailData == null)
						{
							specialRequestDetailData = new SpecialRequestDetailData();
						}
						final List<SpecialServiceRequestData> specialRequestDetailDatas = new ArrayList();
						if (CollectionUtils.isNotEmpty(specialRequestDetailData.getSpecialServiceRequests()))
						{

							specialRequestDetailDatas.addAll(specialRequestDetailData.getSpecialServiceRequests());
						}
						final SpecialServiceRequestData specialServiceRequestData = new SpecialServiceRequestData();
						specialServiceRequestData.setName(ancillaryEntry.getProduct().getName());
						specialServiceRequestData.setCode(ancillaryEntry.getProduct().getCode());
						specialRequestDetailDatas.add(specialServiceRequestData);
						specialRequestDetailData.setSpecialServiceRequests(specialRequestDetailDatas);
					}
				}
			}

			final List<AbstractOrderEntryModel> inboundEntries = entries.stream().filter(
					abstractOrderEntryModel -> Objects.nonNull(abstractOrderEntryModel.getTravelOrderEntryInfo()) && (
							abstractOrderEntryModel.getTravelOrderEntryInfo().getOriginDestinationRefNumber()
									== BcfCoreConstants.INBOUND_REFERENCE_NUMBER)).collect(Collectors.toList());
			travellerDataPerJourney.setInboundTravelDate(getTravelDate(inboundEntries));
			final List<TravellerData> inBoundTravellerData = Converters
					.convertAll(getTravellerService().getTravellers(inboundEntries),
							getTravellerDataConverter());
			travellerDataPerJourney.setOutboundTravellerData(outBoundTravellerData);
			travellerDataPerJourney.setInboundTravellerData(inBoundTravellerData);
			if (hasVehicleTraveller(outBoundTravellerData))
			{
				travellerDataPerJourney.setHasVehicleOutbound(true);
			}
			if (hasVehicleTraveller(inBoundTravellerData))
			{
				travellerDataPerJourney.setHasVehicleInbound(true);
			}
			travellerDataPerJourney
					.setIsReturnWithDifferentVehicle(isReturnWithDifferentVehicle(inboundEntries));
			travellerDataPerJourney
					.setIsReturnWithDifferentPassenger(isReturnWithDifferentPassenger(inboundEntries));
		}
	}

	private boolean hasVehicleTraveller(final List<TravellerData> travellerDataList)
	{
		if (travellerDataList.stream().anyMatch(travellerData -> travellerData.getTravellerType().equalsIgnoreCase("VEHICLE")))
		{
			swapDriverPassengerToTop(travellerDataList);
			return true;
		}
		return false;
	}

	private boolean isReturnWithDifferentVehicle(final List<AbstractOrderEntryModel> inboundEntries)
	{
		if (CollectionUtils.isNotEmpty(inboundEntries))
		{
			final AbstractOrderEntryModel vehicleEntry = getAnyEntryForType(inboundEntries, TravellerType.VEHICLE);
			if (Objects.nonNull(vehicleEntry))
			{
				return vehicleEntry.getTravelOrderEntryInfo().isReturnWithDifferentVehicle();
			}
		}
		return false;
	}

	private boolean isReturnWithDifferentPassenger(final List<AbstractOrderEntryModel> inboundEntries)
	{
		if (CollectionUtils.isNotEmpty(inboundEntries))
		{
			final AbstractOrderEntryModel vehicleEntry = getAnyEntryForType(inboundEntries, TravellerType.PASSENGER);
			if (Objects.nonNull(vehicleEntry))
			{
				return vehicleEntry.getTravelOrderEntryInfo().isReturnWithDifferentPassenger();
			}
		}
		return false;
	}


	private void swapDriverPassengerToTop(final List<TravellerData> travellerDataList)
	{
		int indexOfDriverPassenger;
		for (indexOfDriverPassenger = 0; indexOfDriverPassenger < travellerDataList.size(); indexOfDriverPassenger++)
		{
			if (travellerDataList.get(indexOfDriverPassenger).getTravellerType().equalsIgnoreCase("PASSENGER")
					&&
					((PassengerInformationData) travellerDataList.get(indexOfDriverPassenger).getTravellerInfo()).getPassengerType()
							.getMinAge() >= 12)
			{
				travellerDataList.get(indexOfDriverPassenger);
				Collections.swap(travellerDataList, 0, indexOfDriverPassenger);
				break;
			}
		}
	}

	private Date getTravelDate(final List<AbstractOrderEntryModel> entries)
	{
		if (CollectionUtils.isNotEmpty(entries))
		{
			final Optional<AbstractOrderEntryModel> optionalEntryModel = StreamUtil.safeStream(entries)
					.filter(entry -> entry.getActive() && Objects.nonNull(entry.getTravelOrderEntryInfo())).findFirst();
			if (optionalEntryModel.isPresent() && CollectionUtils
					.isNotEmpty(optionalEntryModel.get().getTravelOrderEntryInfo().getTransportOfferings()))
			{
				final List<TransportOfferingModel> transportOfferings = optionalEntryModel.get().getTravelOrderEntryInfo()
						.getTransportOfferings().stream().collect(Collectors.toList());
				Collections.sort(transportOfferings, Comparator.comparing(TransportOfferingModel::getDepartureTime));
				return transportOfferings.stream().findFirst().get().getDepartureTime();
			}
		}
		return null;
	}

	private AbstractOrderEntryModel getAnyEntryForType(final List<AbstractOrderEntryModel> transportEntries,
			final TravellerType travellerType)
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
		StreamUtil.safeStream(transportEntries)
				.forEach(entry -> entry.getTravelOrderEntryInfo().getTravellers().stream().forEach(travellerModel -> {
					if (travellerModel.getType().equals(travellerType))
					{
						orderEntries.add(entry);
					}
				}));
		return CollectionUtils.isNotEmpty(orderEntries) ? orderEntries.get(0) : null;
	}

	protected BCFTravellerService getTravellerService()
	{
		return travellerService;
	}

	@Required
	public void setTravellerService(final BCFTravellerService travellerService)
	{
		this.travellerService = travellerService;
	}

	protected Converter<TravellerModel, TravellerData> getTravellerDataConverter()
	{
		return travellerDataConverter;
	}

	@Required
	public void setTravellerDataConverter(final Converter<TravellerModel, TravellerData> travellerDataConverter)
	{
		this.travellerDataConverter = travellerDataConverter;
	}

}
