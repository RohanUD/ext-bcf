/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.traveller.handlers.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.facades.travel.data.BCFTravellerDataPerJourney;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.handlers.TravellerDataHandler;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


public class TravellerDataInitHandler implements TravellerDataHandler
{

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Override
	public void handle(final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final BCFTravellersData travellerData)
	{
		final List<BCFTravellerDataPerJourney> travellerDataPerJourneyList = new ArrayList<>();
		cartEntriesByJourneyRefNumber.keySet().forEach(key -> {
			final BCFTravellerDataPerJourney travellerDataPerJourney = new BCFTravellerDataPerJourney();
			travellerDataPerJourney.setJourneyReferenceNumber(key);
			travellerDataPerJourney.setTravelRoute(bcfTravelRouteFacade.getTravelRoute(cartEntriesByJourneyRefNumber.get(key)));
			travellerDataPerJourneyList.add(travellerDataPerJourney);
		});
		travellerData.setTravellerDataPerJourney(travellerDataPerJourneyList);
	}

}
