/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.traveller.manager;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.travel.data.BCFTravellersData;


public interface TravellerDataPipelineManager
{
	/**
	 * returns BCFTravellersData which is nothing but a wrapper object, grouping travelers and other required information
	 * by journey reference number
	 */
	BCFTravellersData executePipeline(AbstractOrderModel abstractOrderModel, List<String> routes);

	BCFTravellersData executePipeline(AbstractOrderModel abstractOrderModel, CartFilterParamsDto cartFilterParamsDto);

	public BCFTravellersData executePipeline(final AbstractOrderModel abstractOrderModel,
			Integer journeyRefNum, String eBookingRef, List<String> routes, Set<String> passengerTypes,
			List<Pair<String, String>> passengerTypeAndUidsToBeRemoved);
}
