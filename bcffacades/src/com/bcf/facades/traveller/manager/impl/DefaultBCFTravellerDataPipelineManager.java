/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.traveller.manager.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.handlers.TravellerDataHandler;
import com.bcf.facades.traveller.manager.TravellerDataPipelineManager;

public class DefaultBCFTravellerDataPipelineManager implements TravellerDataPipelineManager
{

	private List<TravellerDataHandler> handlers;
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public BCFTravellersData executePipeline(final AbstractOrderModel abstractOrderModel, final List<String> routes)
	{
		final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries();

		return getTravellersDataForCartEntries(entries, routes);
	}

	@Override
	public BCFTravellersData executePipeline(final AbstractOrderModel abstractOrderModel,
			final CartFilterParamsDto cartFilterParamsDto)
	{
		List<AbstractOrderEntryModel> entries;
		entries = abstractOrderModel.getEntries();

		if (Objects.nonNull(cartFilterParamsDto.getJourneyRefNo()) && Objects.nonNull(cartFilterParamsDto.getOdRefNo()))
		{
			entries = entries.stream()
					.filter(entry -> entry.getActive() && entry.getJourneyReferenceNumber() == cartFilterParamsDto.getJourneyRefNo())
					.filter(
							entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartFilterParamsDto.getOdRefNo())
					.collect(Collectors.toList());
		}
		return getTravellersDataForCartEntries(entries, cartFilterParamsDto.getRoutes());
	}

	@Override
	public BCFTravellersData executePipeline(final AbstractOrderModel abstractOrderModel,
			final Integer journeyRefNum, final String eBookingRef, final List<String> routes, final Set<String> passengerTypes,
			final List<Pair<String, String>> passengerTypeAndUidsToBeRemoved)
	{
		List<AbstractOrderEntryModel> entries;
		entries = abstractOrderModel.getEntries();

		final List<String> uidsToBeRemoved = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(passengerTypeAndUidsToBeRemoved))
		{
			uidsToBeRemoved.addAll(passengerTypeAndUidsToBeRemoved.stream()
					.map(passengerTypeAndUidToBeRemoved -> passengerTypeAndUidToBeRemoved.getRight()).collect(
							Collectors.toList()));
		}
		entries = entries.stream()
				.filter(
						entry -> entry.getActive() && entry.getJourneyReferenceNumber() == journeyRefNum && eBookingRef
								.equals(entry.getBookingReference())
								&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getType().
								equals(TravellerType.PASSENGER) && passengerTypes.contains(
								((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator().next()
										.getInfo()).getPassengerType().getCode()) && !uidsToBeRemoved
								.contains((entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getUid())))
				.collect(Collectors.toList());

		return getTravellersDataForCartEntries(entries, routes);
	}





	private BCFTravellersData getTravellersDataForCartEntries(final List<AbstractOrderEntryModel> entries,
			final List<String> routes)
	{
		final BCFTravellersData travellersData = new BCFTravellersData();
		final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber;

			if (CollectionUtils.isNotEmpty(routes))
			{
				cartEntriesByJourneyRefNumber =
						entries.stream()
								.filter(
										entry -> entry.getActive() && (entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE) || entry.getProduct().getItemtype().equals(
												AncillaryProductModel._TYPECODE))
												&& routes
										.contains(entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType().getCode())
										&& Objects.nonNull(entry.getActive()) && entry.getActive())
								.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			}
			else
			{
				cartEntriesByJourneyRefNumber = entries.stream()
						.filter(entry -> entry.getActive() && (entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE) || entry.getProduct().getItemtype().equals(
								AncillaryProductModel._TYPECODE)))
						.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			}


		for (final TravellerDataHandler handler : handlers)
		{
			handler.handle(cartEntriesByJourneyRefNumber, travellersData);
		}
		return travellersData;
	}

	protected List<TravellerDataHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<TravellerDataHandler> handlers)
	{
		this.handlers = handlers;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}
}
