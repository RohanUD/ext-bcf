/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades;

import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.ScheduledRouteData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravelSectorData;
import de.hybris.platform.travelfacades.facades.TransportOfferingFacade;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import com.bcf.facades.date.CalendarData;
import com.bcf.facades.schedules.DailySchedulesData;
import com.bcf.facades.schedules.search.request.data.ScheduleSearchCriteriaData;
import com.bcf.integration.listSailingResponse.data.BcfListSailingsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfTransportOfferingFacade extends TransportOfferingFacade
{
	TransportOfferingData getDefaultTransportOfferingForRoute(String travelRouteCode);

	Queue<List<TransportOfferingData>> buildTOConnections(
			final OriginDestinationInfoData originDestinationInfo, final List<TravelSectorData> travelSectors,
			final ScheduleSearchCriteriaData scheduleSearchCriteriaData);

	List<ScheduledRouteData> getScheduledRoutesFromSolr(final ScheduleSearchCriteriaData scheduleSearchCriteriaData);

	DailySchedulesData searchTransportOfferings(final ScheduleSearchCriteriaData scheduleSearchCriteriaData)
			throws IntegrationException;

	List<TransportOfferingData> fetchTransportOfferings(OriginDestinationInfoData originDestinationInfo, String startDate, String endDate);

	List<CalendarData> getDepartureDates(OriginDestinationInfoData originDestinationInfoData, String startDate, String endDate);

	List<ScheduledRouteData> getSheduledRoutesFromResponse(final List<BcfListSailingsResponseData> sailingsList,
			final OriginDestinationInfoData originDestinationInfoData, List<String> sailingCodesInResponse);

	List<ScheduledRouteData> getsheduledRoutesForTransferSailingFromResponse(
			final List<BcfListSailingsResponseData> sailingsList,
			final OriginDestinationInfoData originDestinationInfoData, final List<String> transferIdentifierInResponse);

	Map<String, String> getSailingDurations(List<ScheduledRouteData> scheduledRouteData);

	List<TransportOfferingData> getDefaultTransportOfferingsForTravelSector(final String travelSectorCode);

}
