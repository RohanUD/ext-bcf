/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.schedules.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.schedules.SeasonalScheduleModel;
import com.bcf.facades.schedules.ScheduleDate;


public class SeasonalSchedulePopulator implements Populator<SeasonalScheduleModel, ScheduleDate>
{
	@Override
	public void populate(final SeasonalScheduleModel source, final ScheduleDate target)
			throws ConversionException
	{
		target.setFrom(source.getFrom());
		target.setTo(source.getTo());
	}
}
