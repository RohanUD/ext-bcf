/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.cart.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.add.to.cart.manager.AddToCartPipelineManager;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.com.bcf.facades.cart.BcfAddToCartHandlerFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfAddToCartHandlerFacade implements BcfAddToCartHandlerFacade
{
	private AddToCartPipelineManager addToCartPipelineManager;
	private AddToCartPipelineManager updateCartPipelineManager;
	private AddToCartPipelineManager updateAmendmendCartPipelineManager;
	private BcfTravelCartFacade bcfCartFacade;
	private SessionService sessionService;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Override
	public AddBundleToCartResponseData performAddToCart(final AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException, IntegrationException//NOSONAR
	{
		if (getBcfCartFacade().getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
				addBundleToCartRequestData.getSelectedOdRefNumber()).size() > 0)
		{
			if (bcfCartFacade.isAmendmentCart() )
			{
				return getUpdateAmendmendCartPipelineManager().executePipeline(addBundleToCartRequestData);
			}
			else
			{
				return getUpdateCartPipelineManager().executePipeline(addBundleToCartRequestData);
			}
		}
		else
		{
			return getAddToCartPipelineManager().executePipeline(addBundleToCartRequestData);
		}
	}

	@Override
	public List<CartModificationData> performAddDealToCart(final AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException, IntegrationException//NOSONAR
	{
		if (addBundleToCartRequestData.isUseEbookingIntegration())
		{
			getSessionService().setAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY, Boolean.TRUE);
			final List<CartModificationData> cartModificationDataList = getAddToCartPipelineManager()
					.executePipeline(addBundleToCartRequestData).getCartModificationDataList();
			getSessionService().removeAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY);
			return cartModificationDataList;
		}
		return getBcfCartFacade().addTransportBundleToCart(addBundleToCartRequestData, true);
	}

	protected AddToCartPipelineManager getAddToCartPipelineManager()
	{
		return addToCartPipelineManager;
	}

	@Required
	public void setAddToCartPipelineManager(final AddToCartPipelineManager addToCartPipelineManager)
	{
		this.addToCartPipelineManager = addToCartPipelineManager;
	}

	protected AddToCartPipelineManager getUpdateCartPipelineManager()
	{
		return updateCartPipelineManager;
	}

	@Required
	public void setUpdateCartPipelineManager(final AddToCartPipelineManager updateCartPipelineManager)
	{
		this.updateCartPipelineManager = updateCartPipelineManager;
	}

	protected AddToCartPipelineManager getUpdateAmendmendCartPipelineManager()
	{
		return updateAmendmendCartPipelineManager;
	}

	@Required
	public void setUpdateAmendmendCartPipelineManager(final AddToCartPipelineManager updateAmendmendCartPipelineManager)
	{
		this.updateAmendmendCartPipelineManager = updateAmendmendCartPipelineManager;
	}

	protected BcfTravelCartFacade getBcfCartFacade()
	{
		return bcfCartFacade;
	}

	@Required
	public void setBcfCartFacade(final BcfTravelCartFacade bcfCartFacade)
	{
		this.bcfCartFacade = bcfCartFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
