/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.specialassistance;

import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import java.util.List;


/**
 * Interface for handling Special Service Requests
 */
public interface BCFSpecialAssistanceFacade
{
	/**
	 * gets all Special Service Service Requests
	 *
	 * @return list of type {@link SpecialServiceRequestData}
	 */
	List<SpecialServiceRequestData> getAllSpecialServiceRequest();

	/**
	 * adds the selected special assistance request to a particular traveller
	 *
	 * @param originDestinationRefNo
	 * @param travellerUid
	 * @param specialServiceCode
	 * @return
	 */
	String addSpecialRequestToCart(final int originDestinationRefNo, final String travellerUid,
			final String specialServiceCode);

	/**
	 * removes the selected special assistance request from particular traveller
	 *
	 * @param originDestinationRefNo
	 * @param travellerUid
	 * @param specialServiceCode
	 * @return
	 */
	String removeSpecialRequestFromCart(final int originDestinationRefNo, final String travellerUid,
			final String specialServiceCode);
}
