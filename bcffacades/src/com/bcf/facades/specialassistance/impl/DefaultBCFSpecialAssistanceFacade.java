/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.specialassistance.impl;

import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.user.SpecialRequestDetailModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.specialassistance.service.BCFSpecialServiceRequestService;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;


public class DefaultBCFSpecialAssistanceFacade implements BCFSpecialAssistanceFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBCFSpecialAssistanceFacade.class);

	@Resource(name = "specialServiceRequestService")
	private BCFSpecialServiceRequestService specialServiceRequestService;

	private Converter<SpecialServiceRequestModel, SpecialServiceRequestData> specialServiceRequestConverter;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public List<SpecialServiceRequestData> getAllSpecialServiceRequest()
	{
		final List<SpecialServiceRequestModel> specialServiceRequestModels = getSpecialServiceRequestService()
				.getAllSpecialServiceRequest();
		return getSpecialServiceRequestConverter().convertAll(specialServiceRequestModels);
	}


	@Override
	public String addSpecialRequestToCart(final int journeyRefNumber, final String travellerUid, final String specialServiceCode)
	{
		final CartModel sessionCart = cartService.getSessionCart();

		try
		{
			sessionCart.getEntries().stream()
					.filter(aoem -> aoem.getJourneyReferenceNumber() == journeyRefNumber && aoem.getTravelOrderEntryInfo() != null)
					.forEach(aoem -> {

						aoem.getTravelOrderEntryInfo().getTravellers().forEach(traveller -> {
							if (travellerUid.equals(traveller.getUid()))
							{
								SpecialRequestDetailModel specialRequestDetailModel = traveller.getSpecialRequestDetail();

								if (specialRequestDetailModel == null
										|| CollectionUtils.isEmpty(specialRequestDetailModel.getSpecialServiceRequest()))
								{
									specialRequestDetailModel = new SpecialRequestDetailModel();
									final SpecialServiceRequestModel specialServiceRequestModel = specialServiceRequestService
											.getSpecialServiceRequest(specialServiceCode);

									final List<SpecialServiceRequestModel> serviceRequests = new ArrayList<>();
									serviceRequests.add(specialServiceRequestModel);
									specialRequestDetailModel.setSpecialServiceRequest(serviceRequests);
									traveller.setSpecialRequestDetail(specialRequestDetailModel);
									modelService.saveAll();
								}
								else if (!isSpecialRequestExists(specialRequestDetailModel.getSpecialServiceRequest(),
										specialServiceCode))
								{
									final SpecialServiceRequestModel specialServiceRequestModel = specialServiceRequestService
											.getSpecialServiceRequest(specialServiceCode);

									final List<SpecialServiceRequestModel> serviceRequests = new ArrayList<>();
									serviceRequests.addAll(specialRequestDetailModel.getSpecialServiceRequest());
									serviceRequests.add(specialServiceRequestModel);
									specialRequestDetailModel.setSpecialServiceRequest(serviceRequests);
									traveller.setSpecialRequestDetail(specialRequestDetailModel);
									modelService.saveAll();
								}
							}
						});
					});
			return "success";
		}
		catch (final Exception e)
		{
			LOG.error("Error in updating service request -" + specialServiceCode + ", for traveller " + travellerUid, e);
			return "failure";
		}
	}

	@Override
	public String removeSpecialRequestFromCart(final int journeyRefNumber, final String travellerUid,
			final String specialServiceCode)
	{
		final CartModel sessionCart = cartService.getSessionCart();

		try
		{
			sessionCart.getEntries().stream()
					.filter(aoem -> aoem.getJourneyReferenceNumber() == journeyRefNumber && aoem.getTravelOrderEntryInfo() != null)
					.forEach(aoem -> {

						aoem.getTravelOrderEntryInfo().getTravellers().forEach(traveller -> {
							if (travellerUid.equals(traveller.getUid()))
							{
								final SpecialRequestDetailModel specialRequestDetailModel = traveller.getSpecialRequestDetail();

								if (CollectionUtils.isNotEmpty(specialRequestDetailModel.getSpecialServiceRequest())
										&& isSpecialRequestExists(specialRequestDetailModel.getSpecialServiceRequest(), specialServiceCode))
								{
									final List<SpecialServiceRequestModel> serviceRequests = specialRequestDetailModel
											.getSpecialServiceRequest().stream().filter(ssr -> !specialServiceCode.equals(ssr.getCode()))
											.collect(Collectors.toList());

									specialRequestDetailModel.setSpecialServiceRequest(serviceRequests);
									traveller.setSpecialRequestDetail(specialRequestDetailModel);
									modelService.saveAll();
								}
							}
						});
					});
			return "success";
		}
		catch (final Exception e)
		{
			LOG.error("Error in updating service request -" + specialServiceCode + ", for traveller " + travellerUid, e);
			return "failure";
		}
	}

	private boolean isSpecialRequestExists(final List<SpecialServiceRequestModel> specialServiceRequest,
			final String specialServiceCode)
	{
		boolean exists = false;

		if (StringUtils.isBlank(specialServiceCode))
		{
			return exists;
		}

		for (final SpecialServiceRequestModel request : specialServiceRequest)
		{
			if (specialServiceCode.equals(request.getCode()))
			{
				exists = true;
				break;
			}
		}
		return exists;
	}

	public BCFSpecialServiceRequestService getSpecialServiceRequestService()
	{
		return specialServiceRequestService;
	}

	@Required
	public void setSpecialServiceRequestService(final BCFSpecialServiceRequestService specialServiceRequestService)
	{
		this.specialServiceRequestService = specialServiceRequestService;
	}

	public Converter<SpecialServiceRequestModel, SpecialServiceRequestData> getSpecialServiceRequestConverter()
	{
		return specialServiceRequestConverter;
	}

	@Required
	public void setSpecialServiceRequestConverter(
			final Converter<SpecialServiceRequestModel, SpecialServiceRequestData> specialServiceRequestConverter)
	{
		this.specialServiceRequestConverter = specialServiceRequestConverter;
	}


}
