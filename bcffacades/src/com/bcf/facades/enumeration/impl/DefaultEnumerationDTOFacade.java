/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.enumeration.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.DTO.EnumData;
import com.bcf.facades.enumeration.EnumerationDTOFacade;


public class DefaultEnumerationDTOFacade implements EnumerationDTOFacade
{
	private EnumerationService enumerationService;

	private Converter<HybrisEnumValue, EnumData> enumDTOConverter;

	@Override
	public List<EnumData> getEnumData(final String enumClassName)
	{
		final List<HybrisEnumValue> enumerationValueModels = enumerationService.getEnumerationValues(enumClassName);

		return Converters.convertAll(enumerationValueModels, getEnumDTOConverter());
	}

	protected Converter<HybrisEnumValue, EnumData> getEnumDTOConverter()
	{
		return enumDTOConverter;
	}

	@Required
	public void setEnumDTOConverter(
			final Converter<HybrisEnumValue, EnumData> enumDTOConverter)
	{
		this.enumDTOConverter = enumDTOConverter;
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
