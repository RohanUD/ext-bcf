/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelrulesengine.conditions;

import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrNotCondition;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.apache.solr.common.util.Pair;


public class RuleHotelDateCartConditionTranslator implements RuleConditionTranslator
{

   private static final String OPERATOR_PARAMETER = "operator";
   private static final String DATE_PARAMETER = "date";
   private static final String ALL_DAY_BOOLEAN_PARAMETER = "allDayBoolean";
   public static final String HOTEL_JOURNEY_START_DATE = "hotelJourney.startDate";
   public static final String HOTEL_JOURNEY_END_DATE ="hotelJourney.endDate";
   public static final int PADDED_YEARS_FUTURE = 50;
   public static final int PADDED_YEARS_PAST = 20;
   public static final int SINGLE_DAY = 1;

   public RuleHotelDateCartConditionTranslator()
   {
      //empty constructor
   }

   protected Date addYears(Date selectedDate, int years)
   {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(selectedDate);
      calendar.add(Calendar.YEAR, years);
      return calendar.getTime();
   }

   protected Date addDays(Date selectedDate, int days)
   {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(selectedDate);
      calendar.add(Calendar.DATE, days);
      return calendar.getTime();
   }

   protected Date setTime(Date selectedDate, int hours, int minutes, int seconds)
   {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(selectedDate);
      calendar.set(Calendar.HOUR, hours);
      calendar.set(Calendar.MINUTE, minutes);
      calendar.set(Calendar.SECOND, seconds);
      return calendar.getTime();
   }


   public Pair<Date, Date> getDatePair(RuleIrAttributeOperator selectedOperator, Date selectedDate, Boolean selectedAllDay)
   {

      Date blockWindowStartDate = null;
      Date blockWindowEndDate = null;

      if (selectedOperator.equals(RuleIrAttributeOperator.EQUAL))
      {
         // Star and End dates are the same.

         blockWindowStartDate = selectedDate;
         blockWindowEndDate = selectedDate;
      }
      else if (selectedOperator.equals(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL))
      {
         // Start date will be same
         // End Date + 50 years

         blockWindowStartDate = selectedDate;
         blockWindowEndDate = addYears(selectedDate, PADDED_YEARS_FUTURE);
      }
      else if (selectedOperator.equals(RuleIrAttributeOperator.GREATER_THAN))
      {
         // Start date will date provided + 1
         // End Date + 50 years

         blockWindowStartDate = addDays(selectedDate, SINGLE_DAY);
         blockWindowEndDate = addYears(selectedDate, PADDED_YEARS_FUTURE);
      }
      else if (selectedOperator.equals(RuleIrAttributeOperator.LESS_THAN_OR_EQUAL))
      {
         // Start date will date provided - 50 years
         // End Date will be the start date

         blockWindowStartDate = addYears(selectedDate, -PADDED_YEARS_PAST);
         blockWindowEndDate = selectedDate;
      }
      else if (selectedOperator.equals(RuleIrAttributeOperator.LESS_THAN))
      {
         // Start date will date provided - 50 years
         // End Date will be the start date - 1

         blockWindowStartDate = addYears(selectedDate, -PADDED_YEARS_PAST);
         blockWindowEndDate = addDays(selectedDate, -SINGLE_DAY);
      }

      if (selectedAllDay)
      {
         // Start the date with 00:00
         // End the date with 23:59
         blockWindowStartDate = setTime(blockWindowStartDate, 0, 0, 0);
         blockWindowEndDate = setTime(blockWindowEndDate, 23, 59, 59);
      }

      Pair<Date, Date> pair = new Pair(blockWindowStartDate, blockWindowEndDate);

      return pair;

   }

   public RuleIrCondition translate(
         RuleCompilerContext context, RuleConditionData condition, RuleConditionDefinitionData ruleConditionDefinitionData)
   {
      // Sudo logic
      // check (checkinDate >= blockWindowStart && checkinDate <= blockWindowEnd)
      // else check (checkinDate < blockWindowStart && checkOut > blockWindowStart)

      String cartRAOVariable = context.generateVariable(CartRAO.class);

      RuleParameterData operatorParameter = condition.getParameters().get(OPERATOR_PARAMETER);
      RuleParameterData operatorDate = condition.getParameters().get(DATE_PARAMETER);
      RuleParameterData allDayBoolean = condition.getParameters().get(ALL_DAY_BOOLEAN_PARAMETER);

      AmountOperator operator = operatorParameter.getValue();
      Date date = operatorDate.getValue();
      Boolean allDay = (Boolean) (allDayBoolean.getValue() == null ? Boolean.FALSE : allDayBoolean.getValue());

      Pair<Date, Date> blockWindowDatePair = getDatePair(RuleIrAttributeOperator.valueOf(operator.name()), date, allDay);

      final Date blockWindowStartDate = blockWindowDatePair.first();
      final Date blockWindowEndDate = blockWindowDatePair.second();

      RuleIrNotCondition irCartLeftBlockWindowCondition = new RuleIrNotCondition();
      irCartLeftBlockWindowCondition.setChildren(new ArrayList());

      RuleIrNotCondition irCartRightBlockWindowCondition = new RuleIrNotCondition();
      irCartRightBlockWindowCondition.setChildren(new ArrayList());

      RuleIrGroupCondition irGroupCartMainBlockWindowCondition = new RuleIrGroupCondition();
      irGroupCartMainBlockWindowCondition.setOperator(RuleIrGroupOperator.AND);
      irGroupCartMainBlockWindowCondition.setChildren(new ArrayList());

      RuleIrGroupCondition irGroupCartLeftBlockWindowCondition = new RuleIrGroupCondition();
      irGroupCartLeftBlockWindowCondition.setOperator(RuleIrGroupOperator.AND);
      irGroupCartLeftBlockWindowCondition.setChildren(new ArrayList());

      RuleIrAttributeCondition hotelJourneyStartDateLeftBlockWindowCondition = new RuleIrAttributeCondition();
      hotelJourneyStartDateLeftBlockWindowCondition.setVariable(cartRAOVariable);
      hotelJourneyStartDateLeftBlockWindowCondition.setAttribute(HOTEL_JOURNEY_START_DATE);
      hotelJourneyStartDateLeftBlockWindowCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL);
      hotelJourneyStartDateLeftBlockWindowCondition.setValue(blockWindowStartDate);

      RuleIrAttributeCondition hotelJourneyEndDateLeftBlockWindowCondition = new RuleIrAttributeCondition();
      hotelJourneyEndDateLeftBlockWindowCondition.setVariable(cartRAOVariable);
      hotelJourneyEndDateLeftBlockWindowCondition.setAttribute(HOTEL_JOURNEY_START_DATE);
      hotelJourneyEndDateLeftBlockWindowCondition.setOperator(RuleIrAttributeOperator.LESS_THAN_OR_EQUAL);
      hotelJourneyEndDateLeftBlockWindowCondition.setValue(blockWindowEndDate);

      irGroupCartLeftBlockWindowCondition.getChildren().add(hotelJourneyStartDateLeftBlockWindowCondition);
      irGroupCartLeftBlockWindowCondition.getChildren().add(hotelJourneyEndDateLeftBlockWindowCondition);

      irCartLeftBlockWindowCondition.getChildren().add(irGroupCartLeftBlockWindowCondition);


      RuleIrGroupCondition irGroupCartRightBlockWindowCondition = new RuleIrGroupCondition();
      irGroupCartRightBlockWindowCondition.setOperator(RuleIrGroupOperator.AND);
      irGroupCartRightBlockWindowCondition.setChildren(new ArrayList());

      RuleIrAttributeCondition hotelJourneyStartDateRightBlockWindowCondition = new RuleIrAttributeCondition();
      hotelJourneyStartDateRightBlockWindowCondition.setVariable(cartRAOVariable);
      hotelJourneyStartDateRightBlockWindowCondition.setAttribute(HOTEL_JOURNEY_START_DATE);
      hotelJourneyStartDateRightBlockWindowCondition.setOperator(RuleIrAttributeOperator.LESS_THAN);
      hotelJourneyStartDateRightBlockWindowCondition.setValue(blockWindowStartDate);

      RuleIrAttributeCondition hotelJourneyEndDateRightBlockWindowCondition = new RuleIrAttributeCondition();
      hotelJourneyEndDateRightBlockWindowCondition.setVariable(cartRAOVariable);
      hotelJourneyEndDateRightBlockWindowCondition.setAttribute(HOTEL_JOURNEY_END_DATE);
      hotelJourneyEndDateRightBlockWindowCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN);
      hotelJourneyEndDateRightBlockWindowCondition.setValue(blockWindowStartDate);

      irGroupCartRightBlockWindowCondition.getChildren().add(hotelJourneyStartDateRightBlockWindowCondition);
      irGroupCartRightBlockWindowCondition.getChildren().add(hotelJourneyEndDateRightBlockWindowCondition);

      irCartRightBlockWindowCondition.getChildren().add(irGroupCartRightBlockWindowCondition);

      irGroupCartMainBlockWindowCondition.getChildren().add(irCartLeftBlockWindowCondition);
      irGroupCartMainBlockWindowCondition.getChildren().add(irCartRightBlockWindowCondition);

      return irGroupCartMainBlockWindowCondition;

   }

}

