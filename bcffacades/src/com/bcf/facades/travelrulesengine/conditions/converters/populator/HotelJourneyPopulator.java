/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelrulesengine.conditions.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.ruleengineservices.rao.HotelJourney;
import com.google.common.base.Preconditions;


public class HotelJourneyPopulator implements Populator<AbstractOrderModel, CartRAO>
{

   public void populate(AbstractOrderModel cartModel, CartRAO cartRao)
   {
      Preconditions.checkNotNull(cartModel, "Cart model is not expected to be NULL here");
      Preconditions.checkNotNull(cartRao, "Cart RAO is not expected to be NULL here");

      final List<Date> startDates = new ArrayList<>();
      final List<Date> endDates = new ArrayList<>();

      if (CollectionUtils.isNotEmpty(cartModel.getEntries()))
      {
         final Set<DealOrderEntryGroupModel> dealGroups = cartModel.getEntries().stream()
               .filter(entry -> entry.getProduct() instanceof RoomRateProductModel)
               .filter(entry -> (entry.getEntryGroup() instanceof DealOrderEntryGroupModel))
               .map(entry -> (DealOrderEntryGroupModel) entry.getEntryGroup()).collect(Collectors.toSet());

         final Set<AccommodationOrderEntryGroupModel> accommodations = dealGroups.stream()
               .flatMap(dealGroup -> dealGroup.getAccommodationEntryGroups().stream()).collect(Collectors.toSet());

         for (AccommodationOrderEntryGroupModel accommodation : accommodations)
         {
            startDates.add(accommodation.getStartingDate());
            endDates.add(accommodation.getEndingDate());
         }

         if (CollectionUtils.isNotEmpty(startDates) && CollectionUtils.isNotEmpty(endDates))
         {
            Collections.sort(startDates);
            Collections.sort(endDates);

            final HotelJourney hotelJourney = new HotelJourney();
            hotelJourney.setStartDate(startDates.get(0));
            hotelJourney.setEndDate(endDates.get(endDates.size() - 1));
            cartRao.setHotelJourney(hotelJourney);
         } else {
            final HotelJourney hotelJourney = new HotelJourney();
            hotelJourney.setStartDate(new Date());
            hotelJourney.setEndDate(new Date());
            cartRao.setHotelJourney(hotelJourney);
         }
      }
   }
}
