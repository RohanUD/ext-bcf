/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelrulesengine.compiler;

import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleConditionsGenerator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import java.util.Collection;
import java.util.Collections;


public class BcftDroolsRuleConditionsGenerator extends DefaultDroolsRuleConditionsGenerator
{

   /**
    * Extending this class was to resolve the Hybris bug of excluding the conditions when
    * OR group used, with multiple conditions. We are extending this to make the callers
    * condition true, so that it will always add the conditions.
    *
    * We need to find any other issues this may bring but required extensive testing, around
    * Groups in promotions.
    *
    * @param conditions
    * @return
    */
   @Override
   protected Collection<RuleIrCondition> filterOutNonGroupConditions(Collection<RuleIrCondition> conditions) {
      return Collections.emptyList();
   }

}
