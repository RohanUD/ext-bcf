/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */
package com.bcf.facades.travelrulesengine.actions;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleActionContext;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.FreeProductRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryConsumedRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.impl.RuleFreeGiftRAOAction;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections4.MapUtils;


public class BcfRuleFreeGiftRAOAction extends RuleFreeGiftRAOAction
{

	@Override
	public boolean performActionInternal(RuleActionContext context)
	{
		String productCode = (String) context.getParameter("product", String.class);
		Integer quantity = (Integer) context.getParameter("quantity", Integer.class);
		return this.performAction(context, productCode, quantity);
	}



	protected boolean performAction(final RuleActionContext context, final String productCode, final Integer quantity)
	{
		final CartRAO cartRao = context.getCartRao();
		final ProductModel product = this.findProduct(productCode, context);
		if (Objects.nonNull(product))
		{
			final FreeProductRAO freeProductRAO = this.getRuleEngineCalculationService()
					.addFreeProductsToCart(cartRao, product, quantity);


			final int availableFreeProducts = quantity - this.getRuleEngineCalculationService()
					.getConsumedQuantityForOrderEntry(freeProductRAO.getAddedOrderEntry());
			if (availableFreeProducts > 0)
			{
				this.setRAOMetaData(context, new AbstractRuleActionRAO[] { freeProductRAO });
				final RuleEngineResultRAO result = context.getRuleEngineResultRao();
				if (context instanceof DefaultDroolsRuleActionContext)
				{
					final DefaultDroolsRuleActionContext defaultDroolsRuleActionContext = (DefaultDroolsRuleActionContext) context;
					if (MapUtils.isNotEmpty(defaultDroolsRuleActionContext.getVariables())
							&&
							defaultDroolsRuleActionContext.getVariables().get("de.hybris.platform.ruleengineservices.rao.OrderEntryRAO")
									!= null)
					{
						final OrderEntryRAO orderEntryRAO = (OrderEntryRAO) ((Collection) defaultDroolsRuleActionContext
								.getVariables()
								.get("de.hybris.platform.ruleengineservices.rao.OrderEntryRAO")).iterator().next();
						final OrderEntryConsumedRAO orderEntryConsumedRAO = new OrderEntryConsumedRAO();
						orderEntryConsumedRAO.setOrderEntry(orderEntryRAO);
						orderEntryConsumedRAO.setFiredRuleCode(freeProductRAO.getFiredRuleCode());
						orderEntryConsumedRAO.setAdjustedUnitPrice(BigDecimal.ZERO);
						orderEntryConsumedRAO.setQuantity(1);
						freeProductRAO.setConsumedEntries(Stream.of(orderEntryConsumedRAO).collect(Collectors.toSet()));
					}
				}
				result.getActions().add(freeProductRAO);
				context.scheduleForUpdate(new Object[] { cartRao, result });
				context.insertFacts(new Object[] { freeProductRAO, freeProductRAO.getAddedOrderEntry() });
				return processOrderEntry(context, freeProductRAO.getAddedOrderEntry(), BigDecimal
						.valueOf(100.0D));
			}
		}

		return false;
	}

	protected boolean processOrderEntry(final RuleActionContext context, final OrderEntryRAO orderEntryRao, final BigDecimal value)
	{
		boolean isPerformed = false;
		final int consumableQuantity = context.isStackable() ?
				this.getConsumableQuantityStackable(orderEntryRao) :
				this.getConsumableQuantity(orderEntryRao);
		if (consumableQuantity > 0)
		{
			final DiscountRAO discount = context.isStackable() ?
					this.getRuleEngineCalculationService().addOrderEntryLevelDiscountStackable(orderEntryRao, false, value) :
					this.getRuleEngineCalculationService().addOrderEntryLevelDiscount(orderEntryRao, false, value);
			this.setRAOMetaData(context, new AbstractRuleActionRAO[] { discount });
			this.consumeOrderEntry(orderEntryRao, consumableQuantity, this.adjustUnitPrice(orderEntryRao, consumableQuantity),
					discount);
			final RuleEngineResultRAO result = (RuleEngineResultRAO) context.getValue(RuleEngineResultRAO.class);
			result.getActions().add(discount);
			context.scheduleForUpdate(new Object[] { orderEntryRao, orderEntryRao.getOrder(), result });
			context.insertFacts(new Object[] { discount });
			context.insertFacts(discount.getConsumedEntries());
			isPerformed = true;
		}

		return isPerformed;
	}
}

