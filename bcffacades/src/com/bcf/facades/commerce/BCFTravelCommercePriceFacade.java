/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.commerce;

import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.Date;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public interface BCFTravelCommercePriceFacade extends TravelCommercePriceFacade
{

	/**
	 * Gets the price info for ancillary.
	 *
	 * @param originDestinationRefNumber
	 *           the origin destination ref number
	 * @param productCode
	 *           the product code
	 * @param transportOfferingCode
	 *           the transport offering code
	 * @param routeCode
	 *           the route code
	 * @param offerGroupCode
	 *           the offer group code
	 * @return the price info for ancillary
	 */
	PriceInformation getPriceInfoForAncillary(int originDestinationRefNumber, String productCode, String transportOfferingCode,
			String routeCode, String offerGroupCode);

	/**
	 * Calculate rate data.
	 *
	 * @param roomRateProduct
	 *           the room rate product
	 * @param quantity
	 *           the quantity
	 * @param ratePlan
	 *           the rate plan
	 * @param location
	 *           the location
	 * @return the rate data
	 */
	RateData calculateRateData(RoomRateProductModel roomRateProduct, long quantity, RatePlanModel ratePlan,
			LocationModel location);

	/**
	 * Calculate rate data.
	 *
	 * @param extraGuestOccupancyProduct
	 *           the extra guest occupancy product
	 * @param quantity
	 *           the quantity
	 * @param passengerType
	 *           the passenger type
	 * @param ratePlan
	 *           the rate plan
	 * @param location
	 *           the location
	 * @return the rate data
	 */
	RateData calculateRateData(ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, Long quantity,
			String passengerType, RatePlanModel ratePlan, LocationModel location,  Date stayDate);
	/**
	 * Calculate rate data.
	 *
	 * @param activityProduct
	 *           the activity product
	 * @param quantity
	 *           the quantity
	 * @param passengerType
	 *           the passenger type
	 * @param commencementDate
	 *           the commencement date
	 * @param location
	 *           the location
	 * @return the rate data
	 */
	RateData calculateRateData(ActivityProductModel activityProduct, Long quantity, PassengerTypeModel passengerType,
			Date commencementDate, LocationModel location);
}
