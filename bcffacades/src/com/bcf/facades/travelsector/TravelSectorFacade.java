/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelsector;

import de.hybris.platform.commercefacades.travel.TravelSectorData;
import java.util.List;


public interface TravelSectorFacade
{

	/**
	 * Method returns a list of Travel Sectors
	 *
	 * @return List<TravelSectorData>
	 */
	List<TravelSectorData> getTravelSectors();

}
