/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelsector.impl;

import de.hybris.platform.commercefacades.travel.TravelSectorData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.travelsector.service.TravelSectorService;
import com.bcf.facades.travelsector.TravelSectorFacade;


public class DefaultTravelSectorFacade implements TravelSectorFacade
{

	private TravelSectorService travelSectorService;
	private Converter<TravelSectorModel, TravelSectorData> travelSectorConverter;

	@Override
	public List<TravelSectorData> getTravelSectors()
	{

		final List<TravelSectorModel> travelSectors = travelSectorService.getTravelSectors();
		if (CollectionUtils.isNotEmpty(travelSectors))
		{
			return Converters.convertAll(travelSectors, travelSectorConverter);
		}
		return Collections.emptyList();
	}


	protected TravelSectorService getTravelSectorService()
	{
		return travelSectorService;
	}

	@Required
	public void setTravelSectorService(final TravelSectorService travelSectorService)
	{
		this.travelSectorService = travelSectorService;
	}

	protected Converter<TravelSectorModel, TravelSectorData> getTravelSectorConverter()
	{
		return travelSectorConverter;
	}

	@Required
	public void setTravelSectorConverter(final Converter<TravelSectorModel, TravelSectorData> travelSectorConverter)
	{
		this.travelSectorConverter = travelSectorConverter;
	}

}
