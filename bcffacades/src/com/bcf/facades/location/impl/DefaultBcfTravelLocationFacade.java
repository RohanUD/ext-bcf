/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.location.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.impl.DefaultTravelLocationFacade;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.enums.RouteRegion;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.model.vacation.TopAttractionModel;
import com.bcf.core.service.ActivityProductService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.populators.BCFLocationPopulator;
import com.bcf.facades.populators.BCFVacationLocationPopulator;
import com.bcf.facades.populators.TopAttractionPopulator;
import com.bcf.facades.travel.data.BCFLocationRoutesData;
import com.bcf.facades.travel.data.BcfLocationData;
import com.bcf.facades.travel.data.BcfLocationPaginationData;
import com.bcf.facades.travel.data.BcfTerminalLocationData;
import com.bcf.facades.travel.data.BcfTerminalLocationInfoData;
import com.bcf.facades.vacations.TopAttractionData;
import com.bcf.request.data.reports.inventory.VacationLevelData;


public class DefaultBcfTravelLocationFacade extends DefaultTravelLocationFacade implements BcfTravelLocationFacade
{
	private static final String PRODUCTCATALOG = "bcfProductCatalog";
	private static final String CATALOGVERSION = CatalogManager.ONLINE_VERSION;
	private static final Logger LOG = Logger.getLogger(DefaultBcfTravelLocationFacade.class);

	private BcfTravelLocationService bcfTravelLocationService;
	private Converter<LocationModel, BCFLocationRoutesData> bcfLocationRoutesConverter;
	private BCFLocationPopulator bcfLocationPopulator;
	private BCFVacationLocationPopulator bcfVacationLocationPopulator;
	private TopAttractionPopulator topAttractionPopulator;
	private BCFTravelRouteService bcfTravelRouteService;
	private Converter<TravelRouteModel, TravelRouteData> travelRouteConverter;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;
	private ActivityProductService activityProductService;
	private Converter<BcfVacationLocationModel, BcfLocationData> bcfVacationLocationConverter;
	private CatalogVersionService catalogVersionService;
	private Converter<ActivityProductModel, ActivityDetailsData> activityProductDetailConverter;
	private AssistedServiceService assistedServiceService;

	@Override
	public List<BcfTerminalLocationInfoData> getBcfVacationLocations()
	{
		final List<BcfTerminalLocationInfoData> terminalLocationInfoDataList;

		if (assistedServiceService.getAsmSession() != null && assistedServiceService.getAsmSession().getAgent() != null)
		{
			terminalLocationInfoDataList = StreamUtil.safeStream(getBcfTravelLocationService().getAllBcfVacationLocations())
					.filter(loc -> BooleanUtils.isTrue(loc.getAsmDisplay()) && null != loc.getParentLocation() && BooleanUtils
							.isTrue(loc.getParentLocation().getAsmDisplay()))
					.map(bcfVacationLocation -> getBcfTerminalLocationInfo(Collections.singletonList(bcfVacationLocation)))
					.filter(Objects::nonNull).collect(Collectors.toList());
		}
		else
		{
			terminalLocationInfoDataList = StreamUtil.safeStream(getBcfTravelLocationService().getAllBcfVacationLocations())
					.filter(loc -> BooleanUtils.isTrue(loc.getStorefrontDisplay()) && null != loc.getParentLocation() && BooleanUtils
							.isTrue(loc.getParentLocation().getStorefrontDisplay()))
					.map(bcfVacationLocation -> getBcfTerminalLocationInfo(Collections.singletonList(bcfVacationLocation)))
					.filter(Objects::nonNull).collect(
							Collectors.toList());
		}

		return terminalLocationInfoDataList;
	}

	/**
	 * Gets the bcf vacation locations.
	 *
	 * @return the bcf vacation locations
	 */
	@Override
	public Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> getGeoAreaVacationLocationsMap()
	{
		final List<BcfTerminalLocationInfoData> bcfVacationLocations = getBcfVacationLocations();

		final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> geoAreaVacationLocationsMap = new HashMap<>();
		for (final BcfTerminalLocationInfoData bcfVacationLocation : bcfVacationLocations)
		{
			final BcfTerminalLocationData cityData = bcfVacationLocation.getCity();
			final BcfTerminalLocationData geoAreaData = bcfVacationLocation.getGeographicalArea();
			if (geoAreaData != null)
			{
				final Optional<BcfTerminalLocationData> optionalGeoAreaData = geoAreaVacationLocationsMap.keySet().stream()
						.filter(geoArea -> StringUtils.equals(geoAreaData.getCode(), geoArea.getCode())).findFirst();
				final BcfTerminalLocationData uniqueGeoAreaData = optionalGeoAreaData.isPresent() ? optionalGeoAreaData.get()
						: geoAreaData;

				final List<BcfTerminalLocationData> cityDatas = Objects.nonNull(geoAreaVacationLocationsMap.get(uniqueGeoAreaData))
						? geoAreaVacationLocationsMap.get(uniqueGeoAreaData)
						: new ArrayList<>();
				cityDatas.add(cityData);
				geoAreaVacationLocationsMap.put(uniqueGeoAreaData, cityDatas);
			}

		}

		return getSortedVacationLocationMap(geoAreaVacationLocationsMap);
	}

	/**
	 * sorts the map entries based on 2 parameters<br>
	 * 1) if the sortOrder is present than sort based on that first.<br>
	 * 2) for all other values in an map entry where sortOrder is not present - sort alphabetically by location name <br>
	 *
	 * @param geoAreaVacationLocationsMap
	 */
	public Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> getSortedVacationLocationMap(
			final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> geoAreaVacationLocationsMap)
	{
		final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> sortedGeoAreaVacationLocationsMap = new LinkedHashMap<>();

		final List<BcfTerminalLocationData> sortedKeySet = new ArrayList<>();

		//sort geographical area based on sort order
		sortedKeySet.addAll(geoAreaVacationLocationsMap.keySet().stream().filter(l -> Objects.nonNull(l.getSortOrder()))
				.sorted(Comparator.comparingInt(BcfTerminalLocationData::getSortOrder)).collect(
						Collectors.toList()));

		//sort geographical area based on names where sort order is not provided
		sortedKeySet.addAll(geoAreaVacationLocationsMap.keySet().stream().filter(l -> Objects.isNull(l.getSortOrder()))
				.sorted(Comparator.comparing(BcfTerminalLocationData::getName)).collect(
						Collectors.toList()));

		sortedKeySet.forEach(geoAreaVacationLocation -> {
			final List<BcfTerminalLocationData> sortedCityDatas = new ArrayList<>();
			final List<BcfTerminalLocationData> unsortedCityDatas = geoAreaVacationLocationsMap.get(geoAreaVacationLocation);

			//sort based on sortOrder field where sortOrder is provided
			sortedCityDatas.addAll(unsortedCityDatas.stream().filter(l -> Objects.nonNull(l.getSortOrder()))
					.sorted(Comparator.comparingInt(BcfTerminalLocationData::getSortOrder)).collect(
							Collectors.toList()));

			//sort alphabetically for rest of the locations where sortOrder is not provided
			sortedCityDatas.addAll(unsortedCityDatas.stream().filter(l -> Objects.isNull(l.getSortOrder()))
					.sorted(Comparator.comparing(BcfTerminalLocationData::getName)).collect(
							Collectors.toList()));

			sortedGeoAreaVacationLocationsMap.put(geoAreaVacationLocation, sortedCityDatas);
		});
		return sortedGeoAreaVacationLocationsMap;
	}

	@Override
	public List<TravelRouteData> getTravelRoutesByRegions(final String region)
	{
		final RouteRegion routeRegion = RouteRegion.valueOf(RouteRegion.valueOf(region).toString());
		final List<TravelRouteModel> travelRoutelist = bcfTravelRouteService.getTravelRoutesByRegions(routeRegion);
		final List<TravelRouteModel> filtertravelRoutelist = travelRoutelist.stream()
				.filter(
						travelRouteData -> Objects.nonNull(travelRouteData.getIndirectRoute()) && (Boolean.FALSE).equals(travelRouteData
								.getIndirectRoute())).collect(Collectors.toList());
		final List<TravelRouteData> travelRouteDatalist = new ArrayList<>();
		filtertravelRoutelist.stream().forEach(travelRoute -> {
			travelRouteDatalist.add(getTravelRouteConverter().convert(travelRoute));
		});

		return travelRouteDatalist;
	}

	@Override
	public Map<String, Set<String>> createMapInfoWindowRouteMapping(final List<TravelRouteData> travelRoutelist)
	{
		final Map<String, Set<String>> originToDestinationMapping = new HashMap<>();
		travelRoutelist.stream().forEach(travelRouteData -> {
			if (travelRouteData.getMapKmlFileUrl() != null)
			{
				updateInfoWindowRouteMapping(travelRouteData.getOrigin().getName(),
						travelRouteData.getOrigin().getCode(), travelRouteData.getDestination().getName(),
						travelRouteData.getDestination().getCode(), originToDestinationMapping);
				// To fetch return journey travel route details
				updateInfoWindowRouteMapping(travelRouteData.getDestination().getName(),
						travelRouteData.getDestination().getCode(), travelRouteData.getOrigin().getName(),
						travelRouteData.getOrigin().getCode(), originToDestinationMapping);
			}
		});
		return originToDestinationMapping;
	}

	private void updateInfoWindowRouteMapping(final String fromTerminalName, final String fromTerminalCode,
			final String toTerminalName, final String toTerminalCode, final Map<String, Set<String>> originToDestinationMapping)
	{
		if (originToDestinationMapping.containsKey(fromTerminalName + "|" + fromTerminalCode))
		{
			originToDestinationMapping.get(fromTerminalName + "|" + fromTerminalCode).add(toTerminalName + "|" + toTerminalCode);
		}
		else
		{
			originToDestinationMapping.put(fromTerminalName + "|" + fromTerminalCode,
					new HashSet<String>(Arrays.asList(toTerminalName + "|" + toTerminalCode)));
		}
	}

	@Override
	public List<BcfLocationData> getVacationLocationsByType(final String locationCode)
	{
		final List<BcfVacationLocationModel> bcfLocations = getBcfTravelLocationService().getVacationLocationsByType(locationCode);

		return Converters.convertAll(bcfLocations, getBcfVacationLocationConverter());
	}

	@Override
	public BcfLocationPaginationData getPaginatedLocationsByType(final String locationType, final int pageSize,
			final int currentPage)
	{
		final SearchPageData<BcfVacationLocationModel> searchPageData = getBcfTravelLocationService()
				.getPaginatedLocationsByType(locationType, pageSize, currentPage);
		final BcfLocationPaginationData bcfLocationPaginationData = new BcfLocationPaginationData();
		bcfLocationPaginationData.setResults(
				Converters.convertAll(searchPageData.getResults(), getBcfVacationLocationConverter()));
		bcfLocationPaginationData.setPaginationData(searchPageData.getPagination());
		return bcfLocationPaginationData;
	}

	@Override
	public BcfLocationPaginationData getPaginatedCitiesByRegion(final String regionCode, final int pageSize,
			final int currentPage)
	{
		final SearchPageData<BcfVacationLocationModel> searchPageData = getBcfTravelLocationService()
				.getPaginatedCitiesByRegion(regionCode, pageSize, currentPage);
		final BcfLocationPaginationData bcfLocationPaginationData = new BcfLocationPaginationData();
		bcfLocationPaginationData.setResults(
				Converters.convertAll(searchPageData.getResults(), getBcfVacationLocationConverter()));
		bcfLocationPaginationData.setPaginationData(searchPageData.getPagination());
		return bcfLocationPaginationData;
	}

	@Override
	public BcfTerminalLocationInfoData getBcfTerminalLocationInfo(final List<BcfVacationLocationModel> locations)
	{
		BcfTerminalLocationInfoData bcfTerminalLocationInfoData = null;

		// population of city
		final BcfVacationLocationModel cityLocation = getBcfTravelLocationService()
				.getBcfVacationLocationWithLocationType(locations, LocationType.CITY);
		if (Objects.nonNull(cityLocation))
		{
			bcfTerminalLocationInfoData = new BcfTerminalLocationInfoData();

			final BcfTerminalLocationData cityLocationData = new BcfTerminalLocationData();
			cityLocationData.setName(cityLocation.getName());
			cityLocationData.setCode(cityLocation.getCode());
			cityLocationData.setSortOrder(cityLocation.getSortorder());
			bcfTerminalLocationInfoData.setCity(cityLocationData);

			// population of geoGraphicalArea
			final BcfVacationLocationModel geoGraphicalAreaLocation = getBcfTravelLocationService()
					.getBcfVacationLocationWithLocationType(locations,
							LocationType.GEOGRAPHICAL_AREA);
			if (Objects.nonNull(geoGraphicalAreaLocation))
			{
				final BcfTerminalLocationData geoGraphicalLocationData = new BcfTerminalLocationData();
				geoGraphicalLocationData.setName(geoGraphicalAreaLocation.getName());
				geoGraphicalLocationData.setCode(geoGraphicalAreaLocation.getCode());
				geoGraphicalLocationData.setSortOrder(geoGraphicalAreaLocation.getSortorder());
				bcfTerminalLocationInfoData.setGeographicalArea(geoGraphicalLocationData);
			}
		}
		return bcfTerminalLocationInfoData;
	}

	@Override
	public BcfTerminalLocationInfoData getTerminalLocationInfo(final List<LocationModel> locations)
	{
		final BcfTerminalLocationInfoData bcfTerminalLocationInfoData = new BcfTerminalLocationInfoData();

		// population of city
		final LocationModel cityLocation = getBcfTravelLocationService().getLocationWithLocationType(locations, LocationType.CITY);
		if (Objects.nonNull(cityLocation))
		{
			final BcfTerminalLocationData cityLocationData = new BcfTerminalLocationData();
			cityLocationData.setName(escapeHtml(cityLocation.getName()));
			cityLocationData.setCode(cityLocation.getCode());
			cityLocationData.setSortOrder(cityLocation.getSortorder());
			bcfTerminalLocationInfoData.setCity(cityLocationData);
		}

		// population of geoGraphicalArea
		final LocationModel geoGraphicalAreaLocation = getBcfTravelLocationService().getLocationWithLocationType(locations,
				LocationType.GEOGRAPHICAL_AREA);
		if (Objects.nonNull(geoGraphicalAreaLocation))
		{
			final BcfTerminalLocationData geoGraphicalLocationData = new BcfTerminalLocationData();
			geoGraphicalLocationData.setName(escapeHtml(geoGraphicalAreaLocation.getName()));
			geoGraphicalLocationData.setCode(geoGraphicalAreaLocation.getCode());
			geoGraphicalLocationData.setSortOrder(geoGraphicalAreaLocation.getSortorder());
			bcfTerminalLocationInfoData.setGeographicalArea(geoGraphicalLocationData);
		}
		return bcfTerminalLocationInfoData;
	}

	@Override
	public BcfLocationData getLocation(final String locationCode)
	{
		final BcfVacationLocationModel bcfVacationLocation = getBcfTravelLocationService()
				.findBcfVacationLocationByCode(locationCode);

		final List<BcfLocationData> featuredLocations = StreamUtil.safeStream(bcfVacationLocation.getFeaturedLocations())
				.map(this::getBcfVacationLocationData)
				.collect(Collectors.toList());
		final List<TopAttractionData> topAttractions = getTopAttractionsForLocation(bcfVacationLocation);
		final BcfLocationData locationData = getBcfVacationLocationData(bcfVacationLocation);
		locationData.setTopAttractions(topAttractions);
		locationData.setFeaturedLocations(featuredLocations);
		return locationData;

	}

	@Override
	public List<BcfLocationData> getSubLocations(final LocationModel locationModel)
	{
		return StreamUtil.safeStream(getBcfTravelLocationService().getSubLocations(locationModel)).map(this::getLocationData)
				.collect(Collectors.toList());
	}

	@Override
	public List<BcfLocationData> getBcfVacationSubLocations(final BcfVacationLocationModel locationModel)
	{
		return StreamUtil.safeStream(getBcfTravelLocationService().getBcfVacationSubLocations(locationModel))
				.map(this::getBcfVacationLocationData)
				.collect(Collectors.toList());
	}

	@Override
	public List<TopAttractionData> getTopAttractionsForLocation(final BcfVacationLocationModel locationModel)
	{
		return StreamUtil.safeStream(locationModel.getTopAttractions())
				.filter(Objects::nonNull)
				.map(this::getTopAttractionData)
				.collect(Collectors.toList());
	}

	private TopAttractionData getTopAttractionData(final TopAttractionModel topAttractionModel)
	{
		final TopAttractionData topAttractionData = new TopAttractionData();
		topAttractionPopulator.populate(topAttractionModel, topAttractionData);
		return topAttractionData;
	}

	private BcfLocationData getLocationData(final LocationModel locationModel)
	{
		final BcfLocationData bcfLocationData = new BcfLocationData();
		bcfLocationPopulator.populate(locationModel, bcfLocationData);
		return bcfLocationData;
	}

	@Override
	public List<VacationLevelData> getAccommodationLevelDatas()
	{
		final List<AccommodationOfferingModel> accommodationOfferings = getBcfAccommodationOfferingService()
				.getAccommodationOfferings();

		return populateData(accommodationOfferings);
	}

	private List<VacationLevelData> populateData(final List<AccommodationOfferingModel> accommodationOfferings)
	{
		final List<VacationLevelData> vacationLevelDataList = new ArrayList<VacationLevelData>();
		final List<BcfVacationLocationModel> bcfVacationLocationModelList = getBcfTravelLocationService()
				.getVacationLocationsByType(LocationType.GEOGRAPHICAL_AREA.getCode());

		for (final BcfVacationLocationModel bcfVacationLocationModel : bcfVacationLocationModelList)
		{
			final VacationLevelData geoAreaAccommodationLevelData = new VacationLevelData();
			geoAreaAccommodationLevelData.setCode(bcfVacationLocationModel.getCode());
			geoAreaAccommodationLevelData.setName(escapeHtml(bcfVacationLocationModel.getName()));

			final ArrayList<VacationLevelData> bcfCityVacationLocationlList = new ArrayList<VacationLevelData>();
			final LocationModel originalGeoLocationModel = bcfVacationLocationModel.getOriginalLocation();
			if (Objects.nonNull(originalGeoLocationModel))
			{
				for (final LocationModel citylocationModel : originalGeoLocationModel.getLocations())
				{
					if (Objects.nonNull(citylocationModel))
					{
						final BcfVacationLocationModel bcfCityVacationLocationModel = getBcfTravelLocationService()
								.getBcfVacationLocationByCode(citylocationModel.getCode());

						if (Objects.nonNull(bcfCityVacationLocationModel))
						{
							final VacationLevelData cityLevelData = new VacationLevelData();
							cityLevelData.setCode(bcfCityVacationLocationModel.getCode());
							cityLevelData.setName(escapeHtml(bcfCityVacationLocationModel.getName()));

							final ArrayList<VacationLevelData> accomodationOfferingByCityLocationlList = new ArrayList<VacationLevelData>();
							final List<LocationModel> propertyLocations = citylocationModel.getLocations();
							for (final LocationModel propertyLocationModel : propertyLocations)
							{
								if (Objects.nonNull(propertyLocationModel))
								{
									final Optional<AccommodationOfferingModel> locationAccommodationOfferingOptionalModel = accommodationOfferings
											.stream().filter(Objects::nonNull)
											.filter(accommodationOfferingModel -> Objects.nonNull(accommodationOfferingModel.getLocation()))
											.filter(accommodationOfferingModel -> propertyLocationModel
													.equals(accommodationOfferingModel.getLocation())).findFirst();

									if (locationAccommodationOfferingOptionalModel.isPresent())
									{
										final AccommodationOfferingModel accommodationOfferingModel = locationAccommodationOfferingOptionalModel
												.get();
										final VacationLevelData accommodationOfferingLevelData = new VacationLevelData();
										accommodationOfferingLevelData.setCode(escapeHtml(accommodationOfferingModel.getCode()));
										accommodationOfferingLevelData.setName(escapeHtml(accommodationOfferingModel.getName()));

										final List<VacationLevelData> accommodationLevelDatas = getAccommodationLevelDatas(
												accommodationOfferingModel.getAccommodations());

										if (CollectionUtils.isNotEmpty(accommodationLevelDatas))
										{
											accommodationOfferingLevelData.setChildLevelDatas(accommodationLevelDatas);
											accomodationOfferingByCityLocationlList.add(accommodationOfferingLevelData);
										}
									}
								}
							}

							if (CollectionUtils.isNotEmpty(accomodationOfferingByCityLocationlList))
							{
								cityLevelData.setChildLevelDatas(accomodationOfferingByCityLocationlList);
								bcfCityVacationLocationlList.add(cityLevelData);
							}
						}
					}
				}
			}

			if (CollectionUtils.isNotEmpty(bcfCityVacationLocationlList))
			{
				geoAreaAccommodationLevelData.setChildLevelDatas(bcfCityVacationLocationlList);
				vacationLevelDataList.add(geoAreaAccommodationLevelData);
			}
		}

		return vacationLevelDataList;
	}

	protected List<VacationLevelData> getAccommodationLevelDatas(final List<String> accommodationCodes)
	{
		final List<VacationLevelData> accommodationLevelDatas = new ArrayList<>();
		final List<AccommodationModel> accommodationModels = accommodationCodes.stream()
				.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
				.collect(Collectors.toList());
		for (final AccommodationModel accommodationModel : accommodationModels)
		{
			final VacationLevelData accommodationLevelData = new VacationLevelData();
			accommodationLevelData.setCode(escapeHtml(accommodationModel.getCode()));
			accommodationLevelData.setName(escapeHtml(accommodationModel.getName()));

			accommodationLevelDatas.add(accommodationLevelData);
		}
		return accommodationLevelDatas;
	}

	@Override
	public List<VacationLevelData> getActivityLevelDatas()
	{
		final List<ActivityProductModel> activityProducts = getActivityProductService().findAllActivityProducts();
		if (CollectionUtils.isEmpty(activityProducts))
		{
			return Collections.emptyList();
		}

		return populateActivityLevelData(activityProducts);
	}

	private List<VacationLevelData> populateActivityLevelData(final List<ActivityProductModel> activityProducts)
	{
		final List<VacationLevelData> vacationLevelDataList = new ArrayList<>();
		final List<BcfVacationLocationModel> bcfVacationLocationModelList = getBcfTravelLocationService()
				.getVacationLocationsByType(LocationType.GEOGRAPHICAL_AREA.getCode());

		for (final BcfVacationLocationModel bcfVacationLocationModel : bcfVacationLocationModelList)
		{
			final VacationLevelData geoAreaLocationLevelData = new VacationLevelData();
			geoAreaLocationLevelData.setCode(bcfVacationLocationModel.getCode());
			geoAreaLocationLevelData.setName(escapeHtml(bcfVacationLocationModel.getName()));

			final ArrayList<VacationLevelData> bcfCityLevelLocationDatalList = new ArrayList<>();
			final LocationModel originalGeoLocationModel = bcfVacationLocationModel.getOriginalLocation();
			if (Objects.nonNull(originalGeoLocationModel))
			{
				for (final LocationModel cityLocationModel : originalGeoLocationModel.getLocations())
				{
					if (Objects.nonNull(cityLocationModel))
					{
						final BcfVacationLocationModel bcfCityVacationLocationModel = getBcfTravelLocationService()
								.getBcfVacationLocationByCode(cityLocationModel.getCode());

						if (Objects.nonNull(bcfCityVacationLocationModel))
						{
							final VacationLevelData cityLevelData = new VacationLevelData();
							cityLevelData.setCode(bcfCityVacationLocationModel.getCode());
							cityLevelData.setName(escapeHtml(bcfCityVacationLocationModel.getName()));

							final ArrayList<VacationLevelData> activityLevelDataLocationList = new ArrayList<>();

							final Set<ActivityProductModel> matchedActivitiesByLocation = activityProducts
									.stream().filter(Objects::nonNull)
									.filter(activityProductModel -> Objects.nonNull(activityProductModel.getDestination()))
									.filter(activityProductModel -> cityLocationModel
											.equals(activityProductModel.getDestination())).collect(Collectors.toSet());

							for (final ActivityProductModel matchedActivityByLocation : matchedActivitiesByLocation)
							{
								final VacationLevelData activityLevelData = new VacationLevelData();
								activityLevelData.setCode(escapeHtml(matchedActivityByLocation.getCode()));
								activityLevelData.setName(escapeHtml(matchedActivityByLocation.getName()));
								activityLevelDataLocationList.add(activityLevelData);
							}

							if (CollectionUtils.isNotEmpty(activityLevelDataLocationList))
							{
								cityLevelData.setChildLevelDatas(activityLevelDataLocationList);
								bcfCityLevelLocationDatalList.add(cityLevelData);
							}
						}
					}
				}
			}
			if (CollectionUtils.isNotEmpty(bcfCityLevelLocationDatalList))
			{
				geoAreaLocationLevelData.setChildLevelDatas(bcfCityLevelLocationDatalList);
				vacationLevelDataList.add(geoAreaLocationLevelData);
			}
		}
		return vacationLevelDataList;
	}

	@Override
	public List<ActivityDetailsData> getActivityProductsByDestination(final String destinationCode)
	{
		final LocationModel location = getBcfTravelLocationService().getLocation(destinationCode);
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);
		if (StringUtils.equalsIgnoreCase("GEOGRAPHICAL_AREA", location.getLocationType().getCode()))
		{
			final List<BcfLocationData> locations = getSubLocations(location);
			final List<String> cities = StreamUtil.safeStream(locations)
					.filter(loc -> StringUtils.equalsIgnoreCase("City", loc.getLocationType()))
					.map(BcfLocationData::getCode).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(cities))
			{
				final List<ActivityProductModel> activityProducts = getActivityProductService()
						.getActivityProductsByDestination(cities, catalogVersion);
				return Converters.convertAll(activityProducts, getActivityProductDetailConverter());
			}

		}
		else
		{
			final List<ActivityProductModel> activityProducts = getActivityProductService()
					.getActivityProductsByDestination(Collections.singletonList(destinationCode), catalogVersion);
			return Converters.convertAll(activityProducts, getActivityProductDetailConverter());
		}
		return null;
	}

	@Override
	public Map<String, Set<String>> createMappingForReturnJourneys(
			final Map<String, List<TravelRouteInfo>> currentConditionsByTerminals)
	{
		final Map<String, Set<String>> originToDestinationMapping = new HashMap<>();
		currentConditionsByTerminals.values().stream().forEach(travelRouteInfoList -> {
			travelRouteInfoList.stream().forEach(travelRouteInfo -> {
				if (travelRouteInfo.getMapKmlFileUrl() != null)
				{
					updateInfoWindowRouteMapping(travelRouteInfo.getSourceTerminalName(), travelRouteInfo.getSourceTerminalCode(),
							travelRouteInfo.getDestinationTerminalName(), travelRouteInfo.getDestinationTerminalCode(),
							originToDestinationMapping);
					// To fetch return journey travel route
					updateInfoWindowRouteMapping(travelRouteInfo.getDestinationTerminalName(),
							travelRouteInfo.getDestinationTerminalCode(),
							travelRouteInfo.getSourceTerminalName(), travelRouteInfo.getSourceTerminalCode(),
							originToDestinationMapping);
				}
			});

		});

		return originToDestinationMapping;
	}

	private BcfLocationData getBcfVacationLocationData(final BcfVacationLocationModel locationModel)
	{
		final BcfLocationData bcfLocationData = new BcfLocationData();
		bcfVacationLocationPopulator.populate(locationModel, bcfLocationData);
		return bcfLocationData;
	}

	protected String escapeHtml(final String name)
	{
		return StringUtils.contains(name, "'") ? name.replaceAll("'", "&quot;") : name;
	}

	/**
	 * @return the bcfTravelLocationService
	 */
	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	/**
	 * @param bcfTravelLocationService the bcfTravelLocationService to set
	 */
	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}

	protected Converter<LocationModel, BCFLocationRoutesData> getBcfLocationRoutesConverter()
	{
		return bcfLocationRoutesConverter;
	}

	@Required
	public void setBcfLocationRoutesConverter(final Converter<LocationModel, BCFLocationRoutesData> bcfLocationRoutesConverter)
	{
		this.bcfLocationRoutesConverter = bcfLocationRoutesConverter;
	}

	protected BCFLocationPopulator getBcfLocationPopulator()
	{
		return bcfLocationPopulator;
	}

	@Required
	public void setBcfLocationPopulator(final BCFLocationPopulator bcfLocationPopulator)
	{
		this.bcfLocationPopulator = bcfLocationPopulator;
	}

	protected TopAttractionPopulator getTopAttractionPopulator()
	{
		return topAttractionPopulator;
	}

	@Required
	public void setTopAttractionPopulator(final TopAttractionPopulator topAttractionPopulator)
	{
		this.topAttractionPopulator = topAttractionPopulator;
	}

	protected BCFTravelRouteService getBcfTravelRouteService()
	{
		return bcfTravelRouteService;
	}

	@Required
	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		this.bcfTravelRouteService = bcfTravelRouteService;
	}

	protected Converter<TravelRouteModel, TravelRouteData> getTravelRouteConverter()
	{
		return travelRouteConverter;
	}

	@Required
	public void setTravelRouteConverter(final Converter<TravelRouteModel, TravelRouteData> travelRouteConverter)
	{
		this.travelRouteConverter = travelRouteConverter;
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected ActivityProductService getActivityProductService()
	{
		return activityProductService;
	}

	@Required
	public void setActivityProductService(final ActivityProductService activityProductService)
	{
		this.activityProductService = activityProductService;
	}

	public Converter<BcfVacationLocationModel, BcfLocationData> getBcfVacationLocationConverter()
	{
		return bcfVacationLocationConverter;
	}

	@Required
	public void setBcfVacationLocationConverter(
			final Converter<BcfVacationLocationModel, BcfLocationData> bcfVacationLocationConverter)
	{
		this.bcfVacationLocationConverter = bcfVacationLocationConverter;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	public Converter<ActivityProductModel, ActivityDetailsData> getActivityProductDetailConverter()
	{
		return activityProductDetailConverter;
	}

	@Required
	public void setActivityProductDetailConverter(
			final Converter<ActivityProductModel, ActivityDetailsData> activityProductDetailConverter)
	{
		this.activityProductDetailConverter = activityProductDetailConverter;
	}

	public BCFVacationLocationPopulator getBcfVacationLocationPopulator()
	{
		return bcfVacationLocationPopulator;
	}

	@Required
	public void setBcfVacationLocationPopulator(final BCFVacationLocationPopulator bcfVacationLocationPopulator)
	{
		this.bcfVacationLocationPopulator = bcfVacationLocationPopulator;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}
}
