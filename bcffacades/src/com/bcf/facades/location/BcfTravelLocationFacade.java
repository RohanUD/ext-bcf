/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.location;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.travel.data.BcfLocationData;
import com.bcf.facades.travel.data.BcfLocationPaginationData;
import com.bcf.facades.travel.data.BcfTerminalLocationData;
import com.bcf.facades.travel.data.BcfTerminalLocationInfoData;
import com.bcf.facades.vacations.TopAttractionData;
import com.bcf.request.data.reports.inventory.VacationLevelData;


public interface BcfTravelLocationFacade
{

	/**
	 * Gets the bcf vacation locations.
	 *
	 * @return the bcf vacation locations
	 */
	List<BcfTerminalLocationInfoData> getBcfVacationLocations();

	Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> getGeoAreaVacationLocationsMap();

	Map<String, Set<String>> createMapInfoWindowRouteMapping(List<TravelRouteData> travelRoutelist);

	List<BcfLocationData> getVacationLocationsByType(String locationCode);

	/**
	 * Gets the terminal location info.
	 *
	 * @param locations the locations
	 * @return the terminal location info
	 */
	BcfTerminalLocationInfoData getTerminalLocationInfo(List<LocationModel> locations);

	BcfTerminalLocationInfoData getBcfTerminalLocationInfo(List<BcfVacationLocationModel> locations);

	List<BcfLocationData> getSubLocations(LocationModel locationModel);

	List<BcfLocationData> getBcfVacationSubLocations(BcfVacationLocationModel locationModel);

	List<TopAttractionData> getTopAttractionsForLocation(BcfVacationLocationModel locationModel);

	BcfLocationData getLocation(String locationCode);

	List<TravelRouteData> getTravelRoutesByRegions(String region);

	List<VacationLevelData> getAccommodationLevelDatas();

	List<VacationLevelData> getActivityLevelDatas();

	List<ActivityDetailsData> getActivityProductsByDestination(String destinationCode);

	Map<String, Set<String>> createMappingForReturnJourneys(Map<String, List<TravelRouteInfo>> currentConditionsByTerminals);

	BcfLocationPaginationData getPaginatedLocationsByType(String locationType, int pageSize, int currentPage);

	BcfLocationPaginationData getPaginatedCitiesByRegion(String regionCode, int pageSize, int currentPage);

	Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> getSortedVacationLocationMap(
			final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> geoAreaVacationLocationsMap);
}
