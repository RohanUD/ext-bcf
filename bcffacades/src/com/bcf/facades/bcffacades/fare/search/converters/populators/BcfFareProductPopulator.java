/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.fare.search.converters.populators;

import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.fare.search.converters.populator.FareProductPopulator;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;


public class BcfFareProductPopulator extends FareProductPopulator
{
	@Override
	public void populate(FareProductModel fareProductModel, FareProductData fareProductData) throws ConversionException
	{
		super.populate(fareProductModel, fareProductData);
		fareProductData.setFareProductType(Objects.nonNull(fareProductModel.getFareProductType()) ? fareProductModel.getFareProductType().getCode() : StringUtils.EMPTY);
	}
}
