/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.fare.search.converters.populators;

import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.fare.search.converters.populator.TravelBundleTemplatePopulator;


public class BcfTravelBundleTemplatePopulator<SOURCE extends BundleTemplateModel, TARGET extends BundleTemplateData>
		extends TravelBundleTemplatePopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		super.populate(source, target);
		if (target instanceof TravelBundleTemplateData)
		{
			final TravelBundleTemplateData travelBundleTemplateData = (TravelBundleTemplateData) target;
			travelBundleTemplateData.setDescription(source.getDescription());
		}
	}
}
