/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import java.util.List;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.facades.order.refund.GoodWillRefundData;
import com.bcf.integration.payment.exceptions.BcfRefundException;


public interface BCFGoodWillRefundFacade
{
	List<GoodWillRefundData> getAllRefundReasonCodes();

	public void processGoodWillRefund(final String bookingReference, final String reasonCode, final String comment,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas) throws BcfRefundException, Exception;
}
