/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.GoodWillRefundModel;
import com.bcf.facades.order.refund.GoodWillRefundData;


public class BcfGoodWillRefundPopulator implements Populator<GoodWillRefundModel, GoodWillRefundData>
{
	@Override
	public void populate(final GoodWillRefundModel goodWillRefundModel, final GoodWillRefundData goodWillRefundData) throws ConversionException
	{

		goodWillRefundData.setCode(goodWillRefundModel.getReasonCode());
		goodWillRefundData.setName(goodWillRefundModel.getName());
		goodWillRefundData.setThresholdValue(goodWillRefundModel.getThresholdValue());
	}
}
