/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;
import java.util.Objects;
import com.bcf.facades.change.booking.CsTicketData;


public class CsTicketPopulator implements Populator<CsTicketModel, CsTicketData>
{
	private TicketService ticketService;

	private TypeService typeService;


	public TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	public TicketService getTicketService()
	{
		return ticketService;
	}

	public void setTicketService(final TicketService ticketService)
	{
		this.ticketService = ticketService;
	}

	@Override
	public void populate(final CsTicketModel csTicketModel, final CsTicketData csTicketData) throws ConversionException
	{
		if (csTicketModel != null)
		{
			final CsCustomerEventModel csCustomerEvent = (CsCustomerEventModel) (ticketService.getEventsForTicket(csTicketModel)
					.get(0));
			csTicketData.setCode(csTicketModel.getTicketID());
			if (Objects.nonNull(csTicketModel.getOrder()))
			{
				csTicketData.setBookingReference(csTicketModel.getOrder().getCode());
			}
			csTicketData.setChangeDescription(csCustomerEvent.getText());
			csTicketData.setCustomerContactNumber(csCustomerEvent.getContactNumber());
			csTicketData.setCustomerEmail(csCustomerEvent.getEmail());
			csTicketData.setCustomerName(csCustomerEvent.getName());
			csTicketData.setTicketStatusCode(csTicketModel.getState().getCode());
			csTicketData.setTicketStatus(getTypeService().getEnumerationValue(csTicketModel.getState()).getName());


		}
	}
}
