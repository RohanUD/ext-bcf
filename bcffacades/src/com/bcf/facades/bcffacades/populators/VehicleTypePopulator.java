/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.facades.ferry.VehicleTypeData;


public class VehicleTypePopulator implements Populator<VehicleTypeModel, VehicleTypeData>
{
	@Override
	public void populate(final VehicleTypeModel source, final VehicleTypeData target) throws ConversionException
	{
		if (source != null)
		{
			target.setCode(source.getType().getCode());
			target.setName(source.getName());
			if (Objects.nonNull(source.getVehicleCategoryType()))
			{
				target.setCategory(source.getVehicleCategoryType().getCode().toLowerCase());
			}
			target.setDescription(source.getDescription());
		}
	}
}
