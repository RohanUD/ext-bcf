/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.Objects;


public class BcfRouteInfoPopulator implements Populator<TravelRouteModel, TravelRouteData>
{
	@Override
	public void populate(final TravelRouteModel travelRouteModel, final TravelRouteData travelRouteData) throws ConversionException
	{
		travelRouteData.setLimitedAvailability(Objects.nonNull(travelRouteModel.getLimitedAvailability()) ? travelRouteModel.getLimitedAvailability() : Boolean.FALSE);
		travelRouteData.setIndirectRoute(Objects.nonNull(travelRouteModel.getIndirectRoute()) ? travelRouteModel.getIndirectRoute() : Boolean.FALSE);
		travelRouteData.setCurrentConditionsEnabled(travelRouteModel.getCurrentConditionEnabled());
	}
}
