/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.cart.AsmCommentsData;


public class BcfAsmCommentsPopulator implements Populator<CommentModel, AsmCommentsData>
{
	@Override
	public void populate(final CommentModel asmCommentModel, final AsmCommentsData asmCommentsData) throws ConversionException
	{
		asmCommentsData.setComment(asmCommentModel.getText());
		asmCommentsData.setAgent(asmCommentModel.getAuthor() != null ? asmCommentModel.getAuthor().getUid() : "");
		asmCommentsData.setDate(
				TravelDateUtils.convertDateToStringDate(asmCommentModel.getCreationtime(), BcfCoreConstants.ADDTOCART_DATE_PATTERN));


	}
}
