/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;
import com.bcf.core.enums.ActivityCategoryType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.facades.product.data.ActivityProductData;


public class ActivityProductPopulator implements Populator<ActivityProductModel, ActivityProductData>
{

	private PriceService priceService;
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(final ActivityProductModel source, final ActivityProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		if (CollectionUtils.isNotEmpty(source.getActivityCategoryTypes()))
		{
			target.setActivityTypes(
					source.getActivityCategoryTypes().stream().map(ActivityCategoryType::getCode).collect(Collectors.toList()));
		}

		if (source.getTermsAndConditions() != null)
		{
			target.setTermsAndCondition(source.getTermsAndConditions());
		}
		if (source.getDestination() != null)
		{
			target.setDestination(source.getDestination().getName());
		}
		else if (!CollectionUtils.isEmpty(source.getTravelRouteList()))
		{
			target.setDestination(source.getTravelRouteList().iterator().next().getDestination().getName());
		}

		final List<PriceInformation> pricesInfos = getPriceService().getPriceInformationsForProduct(source);
		if (pricesInfos != null && !pricesInfos.isEmpty())
		{
			final PriceDataType priceType = PriceDataType.BUY;
			final PriceData priceData = createPriceData(priceType, pricesInfos.get(0));
			target.setPrice(priceData);
		}

	}

	protected PriceData createPriceData(final PriceDataType priceType, final PriceInformation priceInfo)
	{
		return getPriceDataFactory().create(priceType, BigDecimal.valueOf(priceInfo.getPriceValue().getValue()),
				priceInfo.getPriceValue().getCurrencyIso());
	}

	public PriceService getPriceService()
	{
		return priceService;
	}

	public void setPriceService(final PriceService priceService)
	{
		this.priceService = priceService;
	}

	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}
}
