/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.ActivityCategoryType;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.product.ActivityScheduleModel;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.travel.data.BcfLocationData;


public class ActivityProductDetailPopulator implements Populator<ActivityProductModel, ActivityDetailsData>
{
	private static final String BCF_BASE_PASSENGERS = "bcfBasePassengers";
	private static final String COMMA = ",";

	private BCFTravelCommercePriceFacade travelCommercePriceFacade;
	private BCFPassengerTypeService passengerTypeService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private Converter<ActivityScheduleModel, ActivityScheduleData> activityScheduleConverter;
	private Converter<LocationModel, BcfLocationData> locationConverter;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private Converter<PassengerTypeModel, PassengerTypeData> passengerTypeConverter;

	@Override
	public void populate(final ActivityProductModel source, final ActivityDetailsData target) throws ConversionException
	{
		if (Objects.isNull(source))
		{
			return;
		}
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setTermsAndConditions(source.getTermsAndConditions());
		if (Objects.nonNull(source.getPicture()))
		{
			target.setDataMedia(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getPicture()));
			target.setDataMediaAltText(source.getPicture().getAltText());
		}
		target.setPromoText(source.getPromoText());
		final PassengerTypeModel passengerType = getPassengerTypeService()
				.getPassengerType(TravelfacadesConstants.PASSENGER_TYPE_CODE_ADULT);
		target.setPrice(
				getTravelCommercePriceFacade().calculateRateData(source, 1L, passengerType, new Date(), source.getDestination()));

		final Collection<MediaModel> galleryImages = source.getNormal();
		if (galleryImages != null)
		{
			final List<ImageData> imageDatas = new ArrayList<>();

			for (final MediaModel image : galleryImages)
			{
				final ImageData imageData = new ImageData();
				imageData.setImageType(ImageDataType.PRIMARY);
				imageData.setUrl(bcfResponsiveMediaStrategy.getResponsiveJson(image));
				imageData.setAltText(image.getAltText());
				imageData.setCaption(image.getCaption());
				imageDatas.add(imageData);

			}
			target.setImages(imageDatas);
		}

		target.setActivitySchedules(getActivityScheduleConverter().convertAll(source.getActivityScheduleList()));
		target.setPassengerTypes(getPassengerTypes(source.getSpecializedPassengerTypes()));
		final List<String> activityCategoryTypes = source.getActivityCategoryTypes().stream().map(ActivityCategoryType::getCode)
				.collect(Collectors.toList());
		target.setActivityCategoryTypes(activityCategoryTypes);
		target.setDestination(getLocationConverter().convert(source.getDestination()));
		target.setBlockTypeStatus(source.getStockLevelType().getCode());
	}

	protected List<PassengerTypeData> getPassengerTypes(final Collection<SpecializedPassengerTypeModel> specializedPassengerTypes)
	{
		final List<PassengerTypeModel> passengerTypes;
		if (CollectionUtils.isEmpty(specializedPassengerTypes))
		{
			final String configuredBasePassengers = getBcfConfigurablePropertiesService().getBcfPropertyValue(BCF_BASE_PASSENGERS);
			final List<String> configuredPassengerTypes = Arrays.asList(StringUtils.split(configuredBasePassengers, COMMA));
			passengerTypes = configuredPassengerTypes.stream()
					.map(configuredPassengerType -> getPassengerTypeService().getPassengerType(configuredPassengerType))
					.map(passengerType -> (passengerType instanceof SpecializedPassengerTypeModel)
							? populateSpecializedPassengerType((SpecializedPassengerTypeModel) passengerType)
							: passengerType)
					.collect(Collectors.toList());
		}
		else
		{
			passengerTypes = specializedPassengerTypes.stream().map(pax -> populateSpecializedPassengerType(pax))
					.collect(Collectors.toList());

		}

		List<PassengerTypeData> passengerTypesData = getPassengerTypeConverter().convertAll(passengerTypes);
		passengerTypesData = passengerTypesData.stream().sorted(Comparator.comparingInt(PassengerTypeData::getMinAge).reversed())
				.collect(Collectors.toList());
		return passengerTypesData;
	}

	/**
	 * Sets the basic properties from basePassengerType and age range from the specializedPassengerType
	 *
	 * @param specializedPassengerType
	 * @return
	 */
	PassengerTypeModel populateSpecializedPassengerType(final SpecializedPassengerTypeModel specializedPassengerType)
	{
		final PassengerTypeModel passengerTypeModel = specializedPassengerType.getBasePassengerType();
		passengerTypeModel.setName(passengerTypeService.getAgeRangeForPassengerType(specializedPassengerType));
		passengerTypeModel.setMinAge(specializedPassengerType.getMinAge());
		passengerTypeModel.setMaxAge(specializedPassengerType.getMaxAge());
		return passengerTypeModel;
	}

	protected BCFTravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final BCFTravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	protected Converter<ActivityScheduleModel, ActivityScheduleData> getActivityScheduleConverter()
	{
		return activityScheduleConverter;
	}

	@Required
	public void setActivityScheduleConverter(
			final Converter<ActivityScheduleModel, ActivityScheduleData> activityScheduleConverter)
	{
		this.activityScheduleConverter = activityScheduleConverter;
	}

	protected Converter<LocationModel, BcfLocationData> getLocationConverter()
	{
		return locationConverter;
	}

	@Required
	public void setLocationConverter(final Converter<LocationModel, BcfLocationData> locationConverter)
	{
		this.locationConverter = locationConverter;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected Converter<PassengerTypeModel, PassengerTypeData> getPassengerTypeConverter()
	{
		return passengerTypeConverter;
	}

	@Required
	public void setPassengerTypeConverter(final Converter<PassengerTypeModel, PassengerTypeData> passengerTypeConverter)
	{
		this.passengerTypeConverter = passengerTypeConverter;
	}
}
