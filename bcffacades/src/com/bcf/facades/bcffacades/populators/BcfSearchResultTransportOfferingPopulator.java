/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TransportVehicleData;
import de.hybris.platform.commercefacades.travel.TransportVehicleInfoData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.travelfacades.search.converters.populator.SearchResultTransportOfferingPopulator;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.shipinfo.service.BcfShipInfoService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


public class BcfSearchResultTransportOfferingPopulator extends SearchResultTransportOfferingPopulator
{
	private BcfShipInfoService shipInfoService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	public void populate(final SearchResultValueData source, final TransportOfferingData target)
	{
		super.populate(source, target);
		final String transferIdentifier = getValue(source, TransportOfferingModel.TRANSFERIDENTIFIER);
		target.setTransferIdentifier(StringUtils.isNoneBlank(transferIdentifier) ? transferIdentifier : StringUtils.EMPTY);
		final String sailingCode = getValue(source, TransportOfferingModel.SAILINGCODE);
		target.setSailingCode(StringUtils.isNoneBlank(sailingCode) ? sailingCode : StringUtils.EMPTY);
		final String eBookingSailingCode = getValue(source, TransportOfferingModel.EBOOKINGSAILINGCODE);
		target.setEbookingSailingCode(StringUtils.isNoneBlank(eBookingSailingCode) ? eBookingSailingCode : StringUtils.EMPTY);
		final TransportVehicleData vehicle = new TransportVehicleData();
		final TransportVehicleInfoData vehicleInfo = new TransportVehicleInfoData();
		vehicleInfo.setName((String) this.getValue(source, "vehicleInformationName"));
		final String vehicleInformationCode = (String) this.getValue(source, "vehicleInformationCode");
		vehicleInfo.setCode(vehicleInformationCode);
		final ShipInfoModel shipInfo = getShipInfoService().getShipInfoForCode(vehicleInformationCode);
		if(Objects.nonNull(shipInfo.getShipIcon()))
		{
			vehicleInfo.setShipIcon(getBcfResponsiveMediaStrategy().getResponsiveJson(shipInfo.getShipIcon()));
			vehicleInfo.setShipIconAltText(shipInfo.getShipIcon().getAltText());
		}
		vehicle.setVehicleInfo(vehicleInfo);
		target.setTransportVehicle(vehicle);
	}

	public BcfShipInfoService getShipInfoService()
	{
		return shipInfoService;
	}

	public void setShipInfoService(final BcfShipInfoService shipInfoService)
	{
		this.shipInfoService = shipInfoService;
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
