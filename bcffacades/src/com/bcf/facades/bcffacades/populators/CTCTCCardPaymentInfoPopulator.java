/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.facades.payment.CTCTCCardData;


public class CTCTCCardPaymentInfoPopulator implements Populator<CTCTCCardPaymentInfoModel, CTCTCCardData>
{
	private Converter<AddressModel, AddressData> addressConverter;

	@Override
	public void populate(final CTCTCCardPaymentInfoModel source, final CTCTCCardData target)
			throws ConversionException
	{
		target.setCardType(BcfCoreConstants.BCF_CTC_TC_CARD_TYPE);
		final String cardNumber = source.getCardNumber();
		target.setCardNumber(cardNumber);
		target.setDisplayCardNumber(cardNumber.substring(cardNumber.length()-4));
		target.setCode(source.getPk().toString());
		if(Objects.nonNull(source.getBillingAddress())) {
			target.setBillingAddress(addressConverter.convert(source.getBillingAddress()));
		}
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(
			final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}
}
