/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.enums.ActivityCategoryType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.facades.activityProduct.ActivityDetailsData;



public class ActivityDetailsPopulator implements Populator<ActivityProductModel, ActivityDetailsData>
{
	@Override
	public void populate(final ActivityProductModel source, final ActivityDetailsData target) throws ConversionException
	{
		if (source != null)
		{
			target.setCode(source.getCode());
			target.setName(source.getName());
			if (CollectionUtils.isNotEmpty(source.getActivityCategoryTypes()))
			{
				target.setActivityCategoryTypes(
						source.getActivityCategoryTypes().stream().map(ActivityCategoryType::getCode).collect(Collectors.toList()));
			}

		}
	}
}
