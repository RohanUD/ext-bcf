/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.populators;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelfacades.populators.TravellerDataPopulator;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

public class BCFPassengerTravellerDataPopulator extends TravellerDataPopulator
{
	private ModelService modelService;

	private Converter<TravelRouteModel, TravelRouteData> travelRouteConverter;


	@Override
	public void populate(final TravellerModel source, final TravellerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		getModelService().refresh(source);

		target.setLabel(source.getLabel());
		target.setUid(source.getUid());
		if (source.getBooker() == null)
		{
			target.setBooker(false);
		}
		else
		{
			target.setBooker(source.getBooker().booleanValue());
		}

		if (source.getType() != null && source.getType().getCode() != null)
		{
			target.setTravellerType(source.getType().getCode());
			if (source.getType().getCode().equalsIgnoreCase("PASSENGER"))
			{
				target.setTravellerInfo(this.getPassengerInformationDataConverter()
						.convert((PassengerInformationModel) source.getInfo()));
			}
		}

		if (source.getSpecialRequestDetail() != null)
		{
			target.setSpecialRequestDetail(
					this.getSpecialRequestDetailsConverter().convert(source.getSpecialRequestDetail()));
		}

		if (source.getSavedTravellerUid() != null)
		{
			target.setSavedTravellerUid(source.getSavedTravellerUid());
		}

		if (source.getVersionID() != null)
		{
			target.setVersionID(source.getVersionID());
		}

		if (source.getType().getCode().equalsIgnoreCase("PASSENGER")
				&& StringUtils.isNotBlank(((PassengerInformationModel) source.getInfo()).getFirstName())
				&& Objects.nonNull(source.getSimpleUID()))
		{
			target.setSimpleUID(source.getSimpleUID());
		}

		if (CollectionUtils.isNotEmpty(source.getTravelOrderEntryInfo()))
		{
			target.setTravelRoute(
					getTravelRouteConverter().convert(source.getTravelOrderEntryInfo().stream().findFirst().get().getTravelRoute()));
		}
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected Converter<TravelRouteModel, TravelRouteData> getTravelRouteConverter()
	{
		return travelRouteConverter;
	}

	@Required
	public void setTravelRouteConverter(final Converter<TravelRouteModel, TravelRouteData> travelRouteConverter)
	{
		this.travelRouteConverter = travelRouteConverter;
	}
}
