/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import java.util.List;
import java.util.Map;
import com.bcf.facades.reservation.data.ReservationDataList;


public interface BCFReservationFacade extends ReservationFacade
{

	ReservationDataList getCurrentReservations();

	ReservationDataList getCurrentReservationsByTravelRoute(String travelRoute);

	ReservationDataList getReservationDataList(AbstractOrderModel abstractOrderModel);

	ReservationDataList getReservationDataListByTravelRoute(AbstractOrderModel abstractOrderModel, String travelRoute);

	PriceData getExtrasSummary(final ReservationDataList reservationDataList);

	Map<Integer, List<PTCFareBreakdownData>> getPTCBreakdownSummary(ReservationDataList reservationDataList);

	ReservationItemData getReservationItemData(ReservationDataList reservationDataList, int journeyRefNumber, int odRefNumber);

	/**
	 * Checks if is first TransportOffering meets departure time restriction gap.
	 *
	 * @param reservationData  the reservation data
	 * @param thresholdTimeGap
	 * @return true, if is first TO meets departure time restriction gap
	 */
	boolean isFirstTOMeetsDepartureTimeRestrictionGap(ReservationData reservationData, final int thresholdTimeGap);

	/**
	 * Checks if is first TransportOffering meets departure time restriction gap.
	 *
	 * @param transportOfferings the transport offerings
	 * @param thresholdTimeGap
	 * @return true, if is first TO meets departure time restriction gap
	 */
	boolean isFirstTOMeetsDepartureTimeRestrictionGap(List<TransportOfferingData> transportOfferings, final int thresholdTimeGap);

	/**
	 * Gets the number of journeys.
	 *
	 * @param orderCode the order code
	 * @return the number of journeys
	 */
	int getNumberOfJourneys(String orderCode);

	Map<String, List<TravellerData>> getTravellerDataMap(final ReservationData reservationData);
}
