/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import java.util.Map;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.EndOfDayReportData;


public interface BcfEndOfDayReportFacade
{
	EndOfDayReportData getEndOfDayReportData(AssistedServiceSession asm, String agentToDeclare,
			Map<String, Object> agentDeclareDataMap);

	void submitEndOfDayReport(EndOfDayReportData endOfDayReportData) throws IntegrationException;
}
