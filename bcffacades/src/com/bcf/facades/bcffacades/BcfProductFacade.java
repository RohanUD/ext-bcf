/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.FeeProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import com.bcf.core.enums.FareProductType;
import com.bcf.data.AddActivityToCartData;


public interface BcfProductFacade extends ProductFacade
{
	ProductData getActivityProduct(String code, String activityDate) throws UnknownIdentifierException,
			ParseException;

	StockData getStockData(String code, String activityDate) throws ParseException;

	StockData getStockData(ProductModel product, Date date, Collection<WarehouseModel> warehouses);

	List<ProductData> getProductsForCategory(String categoryCodes);

	String getAmenityTypeForCode(String code);

	FareProductData getFareProductForFareBasisCode(String fareBasisCode, FareProductType fareProductType);

	Integer getStockForDate(ProductModel product, Date date, Collection<WarehouseModel> warehouses);

	StockData getStockData(AddActivityToCartData addActivityToCartData) throws ParseException;

	StockData getStockData(ProductModel product, Date date, int requiredStockLevel, Collection<WarehouseModel> warehouses);

	StockData getStockData(ProductModel product, Date date, int requiredStockLevel, StockLevelModel stockLevelModel);

	ProductData getAccessibilityProduct(String code);

	Integer getStockForDate(final ProductModel product, final Date date, final int requiredStockLevel,
			final Collection<WarehouseModel> warehouses);

	List<FeeProductData> getFeeProducts();
}
