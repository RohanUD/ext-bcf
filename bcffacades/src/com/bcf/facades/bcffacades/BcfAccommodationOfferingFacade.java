/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationOfferingFacade;
import java.util.List;


public interface BcfAccommodationOfferingFacade extends AccommodationOfferingFacade
{
	List<String> getAccommodationsLinkedToAccommodationOffering(String accommodationOfferingCode);

	List<PropertyData> getPackageData(String[] hotelCodes, String packageDealCode, String roomRateProductsParam);
}
