/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import java.util.List;
import com.bcf.webservices.travel.data.PassengerType;


public interface BCFPassengerTypeFacade extends PassengerTypeFacade
{
	List<TitleData> getAllowedTitles(String passengerTypeCode);

	String getPassengerCodeForEbookingCode(String eBookingCode);

	List<PassengerTypeData> getPassengerTypesForCodes(final List<String> codes);

	void createOrUpdatePassengerTypes(List<PassengerType> passengerTypes) throws Exception;

	void deletePassengerTypes(String code);

	Integer getMinAgeByPassengerTypeCode(String code);
}
