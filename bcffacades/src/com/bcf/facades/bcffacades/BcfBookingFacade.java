/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.facades.managebooking.data.AdjustCostData;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfBookingFacade extends BookingFacade
{

	ReservationDataList getBookingsByBookingReferenceAndAmendingOrder(String bookingReference);

	PriceData getAmountToPay(final String bookingReference);

	/**
	 * Cancel transport bookings.
	 *
	 * @param orderCode     the order code
	 * @param journeyRefNum the journey ref num
	 * @param odRefNum      the od ref num
	 * @return the cancel booking response data
	 */
	CancelBookingResponseData cancelTransportBookings(String orderCode, int journeyRefNum, int odRefNum);

	/**
	 * Gets the refund total.
	 *
	 * @param orderCode     the order code
	 * @param journeyRefNum the journey ref num
	 * @param odRefNum      the od ref num
	 * @return the refund total
	 */
	PriceData getRefundTotal(String orderCode, int journeyRefNum, int odRefNum);

	/**
	 * Creates the amendable cart.
	 *
	 * @param orderCode the order code
	 * @param guid      the guid
	 * @throws IntegrationException    the integration exception
	 * @throws BcfOrderUpdateException
	 * @throws CalculationException
	 */

	void createAmendableCart(String orderCode, String guid, boolean searchEBooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException;

	CartModel createAndSetAmendableCartForEBookingRef(String eBookingReference, boolean searchEBooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException;

	/**
	 * Gets the total amount payable at terminal.
	 *
	 * @param bookingReference the booking reference code of order
	 * @return total mount payable at terminal
	 */
	Double getPayAtTerminal(String bookingReference);

	OrderModel getOrderFromBookingReferenceCode(String bookingReferenceCode);

	PriceData getAmountPaid(String bookingReference);

	void shareItinerary(OrderModel order, Set<String> emails);

	String getBookingJourneyType(final String orderCode);

	/**
	 * calculates order total, if order is net then it adds total tax as well to the order total and return the same
	 *
	 * @param bookingReference
	 * @return
	 */
	BigDecimal getOrderTotal(String bookingReference);

	/**
	 * simply returns amount to pay for an order
	 *
	 * @param bookingReference
	 * @return
	 */
	PriceData getAmountToBePaidForOrder(final String bookingReference);

	void poulateCancelBookingResponseData(final String orderCode,
			final CancelBookingResponseData cancelBookingResponseData, final Exception ex);

	String getOrderCodeForEBookingCode(String eBookingCode);

	String getGuidForEBookingCode(String eBookingCode);

	List<OrderEntryModel> findOrderEntriesByEBookingCode(String eBookingCode);

	OrderModel getGuestOrderForGUID(String guid);

	String getGuestIdentifierForOrderCode(String bookingReference);

	boolean checkBookingJourneyType(final String orderCode, final List<BookingJourneyType> bookingJourneyTypes);

	boolean adjustCostForBooking(final List<AdjustCostData> adjustAccommodationCosts, List<AdjustCostData> adjustActivityCosts,
			final String bookingReferenceCode)
			throws IntegrationException;

	void getAdjustedCostForBooking(final List<AdjustCostData> adjustAccommodationCosts, List<AdjustCostData> adjustActivityCosts,
			final String bookingReferenceCode);

	boolean updateActivityOrderEntryInfo(List<ActivityGuestData> activityGuestDatas);

	void prepareAndSaveLeadPassengerDetails(final Map<String, String> resultMap);

	void removeAmendableCart();

	Boolean updateAccommodationOrderEntryGroup(List<RoomGuestData> roomGuestDatas);

	void updateActivityOrderEntryInfoFromAccomodationLeadPaxDetails(final String firstName, final String lastName);

	public boolean isTransportBookingJourney(final String orderCode);

	String getPackageAvailabilityStatus(final String bookingReference);

	AsmOrderViewData getASMOrderData(final AbstractOrderModel abstractOrderModel);
}
