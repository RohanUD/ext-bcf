/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.hotelinfo.impl;

import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.bcffacades.hotelinfo.BcfHotelInfoFacade;


public class DefaultBcfHotelInfoFacade implements BcfHotelInfoFacade
{
	private AbstractPopulatingConverter<AccommodationOfferingModel, PropertyData> accommodationOfferingConverter;
	private AccommodationOfferingService accommodationOfferingService;


	@Override
	public PropertyData hotelDetails(String accommodationOfferingCode) {
		AccommodationOfferingModel accommodationOfferingModel = getAccommodationOfferingService().getAccommodationOffering(accommodationOfferingCode);
		return this.getAccommodationOfferingConverter().convert(accommodationOfferingModel);
	}

	protected AbstractPopulatingConverter<AccommodationOfferingModel, PropertyData> getAccommodationOfferingConverter()
	{
		return accommodationOfferingConverter;
	}

	@Required
	public void setAccommodationOfferingConverter(
			final AbstractPopulatingConverter<AccommodationOfferingModel, PropertyData> accommodationOfferingConverter)
	{
		this.accommodationOfferingConverter = accommodationOfferingConverter;
	}

	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}
}
