/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.travelfacades.facades.accommodation.impl.DefaultAccommodationSuggestionFacade;
import java.util.List;


public class DefaultBcfAccommodationSuggestionFacade extends DefaultAccommodationSuggestionFacade
{
	@Override
	public List<GlobalSuggestionData> getLocationSuggestions(final String text)
	{
		final int pageSize = this.getConfigurationService().getConfiguration().getInt("accommodationfinder.suggestions.max.result");
		final int maxLocationNumber = this.getConfigurationService().getConfiguration()
				.getInt("accommodationfinder.suggestions.max.location");
		return this.getSuggestions(text, Math.min(maxLocationNumber, pageSize), "SUGGESTIONS_ACCOMMODATION_LOCATION");
	}
}
