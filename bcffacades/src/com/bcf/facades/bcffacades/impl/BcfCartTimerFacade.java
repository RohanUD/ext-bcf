/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.bcffacades.CartTimerFacade;
import com.bcf.facades.cart.timer.CartTimerData;


public class BcfCartTimerFacade implements CartTimerFacade
{

	private SessionService sessionService;
	private BCFTravelCartService cartService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private PropertySourceFacade propertySourceFacade;
	private ModelService modelService;

	private static final String ABOUT_TO_EXPIRE_MESSAGE = "text.cart.timer.about.to.expire.items";
	private static final String TIMER_START_MESSAGE = "text.cart.timer.hold.items";


	@Override
	public CartTimerData getCartTimerDetails()
	{
		final boolean hasSessionCart = cartService.hasSessionCart();
		if (hasSessionCart)
		{
			final CartModel cart = cartService.getSessionCart();

			if (!cartService.isCartValidWithMinimumRequirements(cart) || cart.isQuote() || OptionBookingUtil.isOptionBooking(cart))
			{
				return null;
			}

			final long stockHoldTime = cart.getStockHoldTime();

			if (stockHoldTime > 0)
			{
				final Date now = new Date();

				if (now.getTime() >= stockHoldTime)
				{
					final CartTimerData cartTimerData = new CartTimerData();

					cartTimerData.setShowExpired(true);
					cartTimerData.setSearchAgainUrl("/");
					cartService.removeSessionCart();
					return cartTimerData;
				}
				else if (now.getTime() >= (stockHoldTime - bcfTravelCommerceStockService
						.getStockAboutToExpireTimeInMs(cart.getSalesApplication()))
						&& !cart.isAboutToExpireDisplayed())
				{
					final CartTimerData cartTimerData = new CartTimerData();
					cart.setAboutToExpireDisplayed(true);
					modelService.save(cart);
					cartTimerData.setShowAboutToExpire(true);
					final Object[] messageParams =
							{ getStockAboutToExpireTime(cart.getSalesApplication()) };

					final String message = getPropertySourceFacade()
							.getPropertySourceValueMessage(ABOUT_TO_EXPIRE_MESSAGE, messageParams);

					cartTimerData.setAboutToExpireTimeMessage(message);
					return cartTimerData;
				}

			}
		}

		return null;
	}

	@Override
	public CartTimerData getCartStartTimerDetails()
	{

		final boolean hasSessionCart = cartService.hasSessionCart();
		if (hasSessionCart)
		{
			final CartModel cart = cartService.getSessionCart();

			if (OptionBookingUtil.isOptionBooking(cart))
			{
				return null;
			}

			final long stockHoldTime = cart.getStockHoldTime();

			if (stockHoldTime > 0)
			{
				final Date now = new Date();

				if (stockHoldTime > now.getTime() && !cart.isTimerDisplayed())
				{
					final CartTimerData cartTimerData = new CartTimerData();
					cart.setTimerDisplayed(true);
					modelService.save(cart);
					cartTimerData.setShowTimerStart(true);
					final Object[] messageParams =
							{ getStockHoldTime(cart.getSalesApplication()) };
					final String message = getPropertySourceFacade().getPropertySourceValueMessage(TIMER_START_MESSAGE, messageParams);
					cartTimerData.setHoldTimeMessage(message);
					return cartTimerData;
				}
			}

		}
		return null;
	}


	@Override
	public void setStockHoldTimeAndAllocationStatus(final List<Integer> entryNumbers)
	{

		if (CollectionUtils.isNotEmpty(entryNumbers))
		{
			final CartModel cart = cartService.getSessionCart();
			if (bcfTravelCommerceStockService.getEarlyStockAllocation(cart.getSalesApplication()))
			{
				final List<ItemModel> items = new ArrayList<>();

				cart.getEntries().stream().filter(entry -> entryNumbers.contains(entry.getEntryNumber())).forEach(entry -> {
					entry.setStockAllocated(true);
					items.add(entry);
				});

				if (cart.getStockHoldTime() == 0l)
				{
					cart.setStockHoldTime(bcfTravelCommerceStockService.getStockHoldExpiryTimeInMs(cart.getSalesApplication()));
					items.add(cart);
				}
				modelService.saveAll(items);
			}
		}

	}




	private int getStockHoldTime(final SalesApplication channel)
	{
		return bcfTravelCommerceStockService.getStockHoldTimeInMs(channel) / 60000;
	}

	private int getStockAboutToExpireTime(final SalesApplication channel)
	{

		return bcfTravelCommerceStockService.getStockAboutToExpireTimeInMs(channel) / 60000;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BCFTravelCartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final BCFTravelCartService cartService)
	{
		this.cartService = cartService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
