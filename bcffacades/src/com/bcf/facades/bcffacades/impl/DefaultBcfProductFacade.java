/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.FeeProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.BcfProductService;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.product.data.ActivityProductData;
import com.bcf.facades.vacations.GuestData;


public class DefaultBcfProductFacade extends DefaultProductFacade implements BcfProductFacade
{
	private Converter<ActivityProductModel, ActivityProductData> activityProductConverter;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private WarehouseService warehouseService;
	private CategoryService categoryService;
	private Converter<ProductModel, ProductData> ancillaryProductConverter;
	private Converter<FareProductModel, FareProductData> fareProductConverter;
	private BcfProductService bcfProductService;
	private Converter<FeeProductModel, FeeProductData> feeProductConverter;

	@Override
	public ProductData getActivityProduct(final String code, final String activityDate) throws ParseException
	{
		final ProductModel productModel = getProductService().getProductForCode(code);
		final ProductData productData;
		final StockData stock = getStockData(code, activityDate);
		productData = getActivityProductConverter().convert((ActivityProductModel) productModel);
		productData.setStock(stock);
		return productData;
	}

	@Override
	public ProductData getAccessibilityProduct(final String code)
	{
		final ProductModel productModel = getProductService().getProductForCode(code);
		if (Objects.nonNull(productModel) && productModel instanceof AncillaryProductModel)
		{
			return getAncillaryProductConverter().convert(productModel);
		}
		return null;
	}

	@Override
	public String getAmenityTypeForCode(final String code)
	{
		final ProductModel productModel = getProductService().getProductForCode(code);
		if (productModel instanceof AncillaryProductModel)
		{
			return AncillaryProductModel.class.cast(productModel).getType().getCode();
		}
		return null;
	}

	@Override
	public StockData getStockData(final String code, final String activityStringDate) throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);
		Date activityDate = sdf.parse(sdf.format(new Date()));
		if (StringUtils.isNotEmpty(activityStringDate))
		{
			activityDate = sdf.parse(activityStringDate);
		}
		final ProductModel product = getProductService().getProductForCode(code);
		return getStockData(product, activityDate,
				Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
	}

	@Override
	public StockData getStockData(final AddActivityToCartData addActivityToCartData) throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);
		Date activityDate = sdf.parse(sdf.format(new Date()));
		if (StringUtils.isNotEmpty(addActivityToCartData.getActivityDate()))
		{
			activityDate = sdf.parse(addActivityToCartData.getActivityDate());
		}
		final ProductModel product = getProductService().getProductForCode(addActivityToCartData.getActivityCode());
		final int requiredStockLevel = addActivityToCartData.getGuestData().stream().mapToInt(GuestData::getQuantity).sum();
		return getStockData(product, activityDate, requiredStockLevel,
				Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
	}

	/**
	 * For {@link StockLevelType#STANDARD} fetched stockQuantity if is -ve or 0 or less than requiredStockLevel
	 * causes conversion of stock level type to {@link StockLevelType#ONREQUEST}
	 * <p>
	 * and for {@link StockLevelType#ONREQUEST} or {@link StockLevelType#FREE}, returns as is.
	 */
	@Override
	public StockData getStockData(final ProductModel product, final Date date, final int requiredStockLevel,
			final Collection<WarehouseModel> warehouses) //NOSONAR
	{
		final StockData stockData = new StockData();
		StockLevelType stockLevelType = getBcfTravelCommerceStockService().getStockLevelType(product);
		Long stockQuantity = null;
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final Date stockCheckDate = date != null ? date : new Date();
			final StockLevelModel stockLevelModel = getBcfTravelCommerceStockService()
					.getStockLevelModelForDate(product, stockCheckDate, warehouses);
			if (Objects.nonNull(stockLevelModel))
			{
				stockLevelType = stockLevelModel.getStockLevelType();
				if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
				{
					stockQuantity = Long.valueOf(stockLevelModel.getAvailable() - stockLevelModel.getReserved());
					if (product.getItemtype().equals(ActivityProductModel._TYPECODE) && stockQuantity < requiredStockLevel)
					{
						stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
						getModelService().save(stockLevelModel);

						stockLevelType = StockLevelType.ONREQUEST;
						stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
					}
				}
				else
				{
					stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
				}
			}
			else
			{
				return null;
			}
		}
		else
		{
			stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
		}
		stockData.setStockLevelType(stockLevelType);
		stockData.setStockLevel(stockQuantity);
		return stockData;
	}

	/**
	 * For {@link StockLevelType#STANDARD} fetched stockQuantity if is -ve or 0 or less than requiredStockLevel
	 * causes conversion of stock level type to {@link StockLevelType#ONREQUEST}
	 * <p>
	 * and for {@link StockLevelType#ONREQUEST} or {@link StockLevelType#FREE}, returns as is.
	 */
	@Override
	public StockData getStockData(final ProductModel product, final Date date, final int requiredStockLevel,
			final StockLevelModel stockLevelModel)
	{
		final StockData stockData = new StockData();
		StockLevelType stockLevelType = getBcfTravelCommerceStockService().getStockLevelType(product);
		Long stockQuantity = null;
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			if (Objects.nonNull(stockLevelModel))
			{
				stockLevelType = stockLevelModel.getStockLevelType();
				if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
				{
					stockQuantity = Long.valueOf(stockLevelModel.getAvailable() - stockLevelModel.getReserved());
					if (product.getItemtype().equals(ActivityProductModel._TYPECODE) && stockQuantity < requiredStockLevel)
					{
						stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
						getModelService().save(stockLevelModel);

						stockLevelType = StockLevelType.ONREQUEST;
						stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
					}
				}
				else
				{
					stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
				}
			}
			else
			{
				return null;
			}
		}
		else
		{
			stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
		}
		stockData.setStockLevelType(stockLevelType);
		stockData.setStockLevel(stockQuantity);
		return stockData;
	}

	@Override
	public StockData getStockData(final ProductModel product, final Date date, final Collection<WarehouseModel> warehouses)
	{
		final StockData stockData = new StockData();
		StockLevelType stockLevelType = getBcfTravelCommerceStockService().getStockLevelType(product);
		Long stockQuantity = null;
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final Date stockCheckDate = date != null ? date : new Date();
			final StockLevelModel stockLevelModel = getBcfTravelCommerceStockService()
					.getStockLevelModelForDate(product, stockCheckDate, warehouses);
			if (Objects.nonNull(stockLevelModel))
			{
				stockLevelType = stockLevelModel.getStockLevelType();
				if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
				{
					stockQuantity = Long.valueOf(stockLevelModel.getAvailable() - stockLevelModel.getReserved());
					if (stockQuantity <= 0 && product.getItemtype().equals(AccommodationModel._TYPECODE))
					{
						stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
						getModelService().save(stockLevelModel);

						stockLevelType = StockLevelType.ONREQUEST;
						stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
					}
				}
				else
				{
					stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
				}
			}
		}
		else
		{
			stockQuantity = Long.valueOf(getBcfTravelCommerceStockService().getDefaultStockValue());
		}
		stockData.setStockLevelType(stockLevelType);
		stockData.setStockLevel(stockQuantity);
		return stockData;
	}

	@Override
	public Integer getStockForDate(final ProductModel product, final Date date, final int requiredStockLevel,
			final Collection<WarehouseModel> warehouses)
	{
		final Integer stockForDate = getBcfTravelCommerceStockService().getStockForDate(product, date, warehouses);
		if (Objects.isNull(stockForDate))
		{
			return 0;
		}

		if (stockForDate < requiredStockLevel && product.getItemtype().equals(AccommodationModel._TYPECODE))
		{
			final StockLevelModel stockLevelModel = getBcfTravelCommerceStockService()
					.getStockLevelModelForDate(product, date, warehouses);
			if (Objects.equals(StockLevelType.STANDARD, stockLevelModel.getStockLevelType()))
			{
				return getBcfTravelCommerceStockService().getDefaultStockValue();
			}
		}
		return stockForDate;
	}


	@Override
	public Integer getStockForDate(final ProductModel product, final Date date, final Collection<WarehouseModel> warehouses)
	{
		final Integer stockForDate = getBcfTravelCommerceStockService().getStockForDate(product, date, warehouses);
		if (Objects.isNull(stockForDate))
		{
			return 0;
		}

		if (stockForDate <= 0 && product.getItemtype().equals(AccommodationModel._TYPECODE))
		{
			final StockLevelModel stockLevelModel = getBcfTravelCommerceStockService()
					.getStockLevelModelForDate(product, date, warehouses);
			if (Objects.equals(StockLevelType.STANDARD, stockLevelModel.getStockLevelType()))
			{
				stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
				getModelService().save(stockLevelModel);
			}
		}
		return stockForDate;
	}

	@Override
	public List<ProductData> getProductsForCategory(final String categoryCode)
	{
		final CategoryModel categoryModel = getCategoryService().getCategoryForCode(categoryCode);
		final List<ProductModel> products = getProductService().getProductsForCategory(categoryModel);
		return getAncillaryProductConverter().convertAll(products);
	}

	@Override
	public FareProductData getFareProductForFareBasisCode(final String fareBasisCode, final FareProductType fareProductType)
	{
		return getFareProductConverter()
				.convert(getBcfProductService().getFareProductForFareBasisCode(fareBasisCode, fareProductType));
	}

	@Override
	public List<FeeProductData> getFeeProducts()
	{
		final List<FeeProductModel> products = getBcfProductService().getAllFeeProducts();
		return getFeeProductConverter().convertAll(products);
	}

	public Converter<ActivityProductModel, ActivityProductData> getActivityProductConverter()
	{
		return activityProductConverter;
	}

	public void setActivityProductConverter(final Converter<ActivityProductModel, ActivityProductData> activityProductConverter)
	{
		this.activityProductConverter = activityProductConverter;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	protected Converter<ProductModel, ProductData> getAncillaryProductConverter()
	{
		return ancillaryProductConverter;
	}

	@Required
	public void setAncillaryProductConverter(final Converter<ProductModel, ProductData> ancillaryProductConverter)
	{
		this.ancillaryProductConverter = ancillaryProductConverter;
	}

	protected Converter<FareProductModel, FareProductData> getFareProductConverter()
	{
		return fareProductConverter;
	}

	@Required
	public void setFareProductConverter(
			final Converter<FareProductModel, FareProductData> fareProductConverter)
	{
		this.fareProductConverter = fareProductConverter;
	}

	protected BcfProductService getBcfProductService()
	{
		return bcfProductService;
	}

	@Required
	public void setBcfProductService(final BcfProductService bcfProductService)
	{
		this.bcfProductService = bcfProductService;
	}

	protected Converter<FeeProductModel, FeeProductData> getFeeProductConverter()
	{
		return feeProductConverter;
	}

	@Required
	public void setFeeProductConverter(
			final Converter<FeeProductModel, FeeProductData> feeProductConverter)
	{
		this.feeProductConverter = feeProductConverter;
	}
}
