/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import com.bcf.core.service.BcfEndOfDayReportService;
import com.bcf.facades.bcffacades.BcfEndOfDayReportFacade;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.EndOfDayReportData;


public class DefaultBcfEndOfDayReportFacade implements BcfEndOfDayReportFacade
{
	@Value("${order.report.csv.headers}")
	private String[] csvHeaders;

	@Value("${order.report.csv.filePath}")
	private String filePath;

	private BcfEndOfDayReportService bcfEndOfDayReportService;

	@Resource(name = "userService")
	private UserService userService;

	private CommonI18NService commonI18NService;

	public EndOfDayReportData getEndOfDayReportData(AssistedServiceSession asm, String agentToDeclare,
			Map<String, Object> agentDeclareDataMap)
	{
		UserModel agent = asm.getAgent();
		EmployeeModel employee = (EmployeeModel) asm.getAgent();

		if (StringUtils.isNotBlank(agentToDeclare))
		{
			employee = (EmployeeModel) userService.getUserForUID(agentToDeclare);
		}

		final CurrencyModel currentCurrency = getCommonI18NService().getCurrentCurrency();

		return getbcfOrderReportService().getEndOfDayReportDataForAgent(employee, agentDeclareDataMap, currentCurrency.getDigits());
	}

	public BcfEndOfDayReportService getbcfOrderReportService()
	{
		return bcfEndOfDayReportService;
	}

	public void setBcfEndOfDayReportService(final BcfEndOfDayReportService bcfEndOfDayReportService)
	{
		this.bcfEndOfDayReportService = bcfEndOfDayReportService;
	}

	public void submitEndOfDayReport(EndOfDayReportData endOfDayReportData) throws IntegrationException
	{
		getbcfOrderReportService().submitEndOfDayReport(endOfDayReportData);
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
