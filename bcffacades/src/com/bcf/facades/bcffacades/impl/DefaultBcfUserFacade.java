/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class DefaultBcfUserFacade extends DefaultUserFacade implements BcfUserFacade
{
	private SessionService sessionService;
	private Converter<CTCTCCardPaymentInfoModel, CTCTCCardData> ctcTcCardPaymentInfoConverter;

	@Override
	public List<AddressData> getAddressBook()
	{
		// Get the current customer's addresses
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final Collection<AddressModel> addresses = getCustomerAccountService().getAddressBookEntries(currentUser);

		if (CollectionUtils.isNotEmpty(addresses))
		{
			final List<AddressData> result = new ArrayList<AddressData>();
			final AddressData defaultAddress = getDefaultAddress();

			for (final AddressModel address : addresses)
			{
				// Filter out invalid addresses for the site
				if (address.getCountry() != null)
				{
					final AddressData addressData = getAddressConverter().convert(address);

					if (defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressData.getId()))
					{
						addressData.setDefaultAddress(true);
						result.add(0, addressData);
					}
					else
					{
						result.add(addressData);
					}
				}
			}
			return result;
		}
		return Collections.emptyList();
	}

	@Override
	public void editAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		addressModel.setRegion(null);
		getAddressReversePopulator().populate(addressData, addressModel);
		getCustomerAccountService().saveAddressEntry(currentCustomer, addressModel);
		if (addressData.isDefaultAddress())
		{
			getCustomerAccountService().setDefaultAddressEntry(currentCustomer, addressModel);
		}
		else if (addressModel.equals(currentCustomer.getDefaultShipmentAddress()))
		{
			getCustomerAccountService().clearDefaultAddressEntry(currentCustomer);
		}
	}

	@Override
	public List<CTCTCCardData> getCtcTcCardsPaymentInfos()
	{
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			final List<PaymentInfoModel> savedPaymentInfoModels = StreamUtil.safeStream(currentUser.getPaymentInfos()).filter(
					paymentInfo -> Objects.nonNull(paymentInfo.getB2bUnit()) && paymentInfo.isSaved() && StringUtils
							.equalsIgnoreCase(b2bCheckoutContextData.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM),
									paymentInfo.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM)))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(savedPaymentInfoModels))
			{
				final List<CTCTCCardPaymentInfoModel> internalCards = new ArrayList<>();
				StreamUtil.safeStream(savedPaymentInfoModels).forEach(paymentInfoModel -> {
					if (paymentInfoModel instanceof CTCTCCardPaymentInfoModel)
					{
						internalCards.add(CTCTCCardPaymentInfoModel.class.cast(paymentInfoModel));
					}
				});
				if (CollectionUtils.isNotEmpty(internalCards))
				{
					return Converters.convertAll(internalCards, getCtcTcCardPaymentInfoConverter());
				}
			}
		}
		return Collections.emptyList();
	}

	@Override
	public List<CCPaymentInfoData> getCCPaymentInfos(final boolean saved)
	{
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final List<CCPaymentInfoData> ccPaymentInfos = new ArrayList<>();
		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			final List<PaymentInfoModel> savedPaymentInfoModels = StreamUtil.safeStream(currentCustomer.getPaymentInfos()).filter(
					paymentInfo -> Objects.nonNull(paymentInfo.getB2bUnit()) && paymentInfo.isSaved() && StringUtils
							.equalsIgnoreCase(b2bCheckoutContextData.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM),
									paymentInfo.getB2bUnit().getSystemIdentifiers().get(RegistrationSource.CRM)))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(savedPaymentInfoModels))
			{
				final List<CreditCardPaymentInfoModel> creditCards = new ArrayList<>();
				StreamUtil.safeStream(savedPaymentInfoModels).forEach(paymentInfoModel -> {
					if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
					{
						creditCards.add(CreditCardPaymentInfoModel.class.cast(paymentInfoModel));
					}
				});
				if (CollectionUtils.isNotEmpty(creditCards))
				{
					return Converters.convertAll(creditCards, getCreditCardPaymentInfoConverter());
				}
			}
		}
		else
		{
			final List<CreditCardPaymentInfoModel> creditCards = getCustomerAccountService()
					.getCreditCardPaymentInfos(currentCustomer,
							saved);
			final PaymentInfoModel defaultPaymentInfoModel = currentCustomer.getDefaultPaymentInfo();
			for (final CreditCardPaymentInfoModel ccPaymentInfoModel : creditCards)
			{
				final CCPaymentInfoData paymentInfoData = getCreditCardPaymentInfoConverter().convert(ccPaymentInfoModel);
				if (ccPaymentInfoModel.equals(defaultPaymentInfoModel))
				{
					paymentInfoData.setDefaultPaymentInfo(true);
					ccPaymentInfos.add(0, paymentInfoData);
				}
				else
				{
					ccPaymentInfos.add(paymentInfoData);
				}
			}
		}
		return ccPaymentInfos;
	}

	@Override
	public CTCTCCardData getCtcTcCardsPaymentInfoForCode(final String number)
	{
		if (!((BcfUserService) getUserService()).isB2BCustomer())
		{
			return null;
		}

		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = ((BcfCustomerAccountService) getCustomerAccountService())
				.getCtcTcCardPaymentInfoForCode(customerModel, number);
		return getCtcTcCardPaymentInfoConverter().convert(ctcTcCardPaymentInfoModel);
	}

	@Override
	public Map<String, String> getUserRoles()
	{
		return ((BcfUserService) getUserService()).findUserRoles();
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected Converter<CTCTCCardPaymentInfoModel, CTCTCCardData> getCtcTcCardPaymentInfoConverter()
	{
		return ctcTcCardPaymentInfoConverter;
	}

	@Required
	public void setCtcTcCardPaymentInfoConverter(
			final Converter<CTCTCCardPaymentInfoModel, CTCTCCardData> ctcTcCardPaymentInfoConverter)
	{
		this.ctcTcCardPaymentInfoConverter = ctcTcCardPaymentInfoConverter;
	}

}
