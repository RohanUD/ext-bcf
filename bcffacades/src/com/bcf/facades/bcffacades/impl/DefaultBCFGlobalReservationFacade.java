/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.data.ActivityReservationDataList;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.global.manager.BcfGlobalReservationPipelineManager;


public class DefaultBCFGlobalReservationFacade implements BCFGlobalReservationFacade
{
	private Map<String, BcfGlobalReservationPipelineManager> bcfGlobalReservationDataListPipelineManagerMap;
	private CartService cartService;

	@Override
	public BcfGlobalReservationData getCurrentGlobalReservationData()
	{
		final BcfGlobalReservationData bcfGlobalReservationData = getCartService().hasSessionCart() ?
				getBcfGlobalReservationDataListPipelineManagerMap().get("Cart")
						.executePipeline(getCartService().getSessionCart(), Arrays
								.asList(OrderEntryType.ACCOMMODATION, OrderEntryType.ACTIVITY, OrderEntryType.TRANSPORT,
										OrderEntryType.DEAL)) :
				null;

		clubActivitiesWithSamePaxAndDate(bcfGlobalReservationData);
		return bcfGlobalReservationData;
	}

	@Override
	public BcfGlobalReservationData getGlobalReservationForEntryType(final List<OrderEntryType> orderEntryTypes)
	{
		return getCartService().hasSessionCart() ?
				getBcfGlobalReservationDataListPipelineManagerMap().get("Cart")
						.executePipeline(getCartService().getSessionCart(), orderEntryTypes) :
				null;
	}

	@Override
	public TotalFareData getGlobalReservationForTotalPricewithTax(final List<OrderEntryType> orderEntryTypes)
	{
		return  getCartService().hasSessionCart() ?
				getBcfGlobalReservationDataListPipelineManagerMap().get("Cart")
						.executePipeline(getCartService().getSessionCart(), orderEntryTypes).getTotalFare()
				:null;
	}

	@Override
	public BcfGlobalReservationData getbcfGlobalReservationDataListForFerry(final AbstractOrderModel abstractOrderModel)
	{
		if (Objects.isNull(abstractOrderModel))
		{
			return null;
		}
		final BcfGlobalReservationData bcfGlobalReservationData = getBcfGlobalReservationDataListPipelineManagerMap()
				.get(abstractOrderModel.getItemtype())
				.executePipeline(abstractOrderModel, Arrays
						.asList(OrderEntryType.TRANSPORT));
		return bcfGlobalReservationData;
	}

	@Override
	public BcfGlobalReservationData getbcfGlobalReservationDataList(final AbstractOrderModel abstractOrderModel)
	{
		if (Objects.isNull(abstractOrderModel))
		{
			return null;
		}
		final BcfGlobalReservationData bcfGlobalReservationData =  getBcfGlobalReservationDataListPipelineManagerMap().get(abstractOrderModel.getItemtype())
				.executePipeline(abstractOrderModel, Arrays
						.asList(OrderEntryType.ACCOMMODATION, OrderEntryType.ACTIVITY,
								OrderEntryType.DEAL));
		clubActivitiesWithSamePaxAndDate(bcfGlobalReservationData);
		if (abstractOrderModel.getDepositPayDate() != null)
		{
			bcfGlobalReservationData.setDepositPayDate(TravelDateUtils.convertDateToStringDate(
					abstractOrderModel.getDepositPayDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY));
		}
		return bcfGlobalReservationData;
	}

	private void clubActivitiesWithSamePaxAndDate(final BcfGlobalReservationData bcfGlobalReservationData)
	{
		if(Objects.isNull(bcfGlobalReservationData))
		{
			return;
		}

		if(Objects.nonNull(bcfGlobalReservationData.getActivityReservations()))
		{
			ActivityReservationDataList activityReservationList = bcfGlobalReservationData.getActivityReservations();
			bcfGlobalReservationData.getActivityReservations().setClubbedActivityReservationDatasByActivity(groupyActivity(activityReservationList));
		}
		else if(Objects.nonNull(bcfGlobalReservationData.getPackageReservations()))
		{
			bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas().stream().forEach(packageReservationData->
			{
				ActivityReservationDataList activityReservationList = packageReservationData.getActivityReservations();
				if(Objects.nonNull(activityReservationList))
				{
					packageReservationData.getActivityReservations()
							.setClubbedActivityReservationDatasByActivity(groupyActivity(activityReservationList));
				}
			});
		}
	}

	private Map<String, List<ActivityReservationData>> groupyActivity(final ActivityReservationDataList activityReservationList)
	{
		return activityReservationList.getActivityReservationDatas().stream().collect(Collectors.groupingBy(a->getUniqueCode(a)));
	}

	private String getUniqueCode(final ActivityReservationData a)
	{
		String dateStr = StringUtils.replace(a.getActivityDate()," ","");
		return String.join("_",a.getActivityDetails().getCode(),dateStr,a.getActivityTime());
	}

	public Map<String, BcfGlobalReservationPipelineManager> getBcfGlobalReservationDataListPipelineManagerMap()
	{
		return bcfGlobalReservationDataListPipelineManagerMap;
	}

	public void setBcfGlobalReservationDataListPipelineManagerMap(
			final Map<String, BcfGlobalReservationPipelineManager> bcfGlobalReservationDataListPipelineManagerMap)
	{
		this.bcfGlobalReservationDataListPipelineManagerMap = bcfGlobalReservationDataListPipelineManagerMap;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
