/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;


public class DefaultBcfSalesApplicationResolverFacade implements BcfSalesApplicationResolverFacade
{
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Override
	public String getCurrentSalesChannel()
	{
		return getBcfSalesApplicationResolverService().getCurrentSalesChannel().getCode();
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}
}
