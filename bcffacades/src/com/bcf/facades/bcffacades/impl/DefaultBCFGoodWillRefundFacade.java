/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.services.BookingService;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.GoodWillRefundModel;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BCFGoodWillRefundService;
import com.bcf.facades.bcffacades.BCFGoodWillRefundFacade;
import com.bcf.facades.order.refund.GoodWillRefundData;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.payment.exceptions.BcfRefundException;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.financial.FinancialData;


public class DefaultBCFGoodWillRefundFacade implements BCFGoodWillRefundFacade
{

	private BCFGoodWillRefundService bcfGoodWillRefundService;

	private static final Logger LOG = Logger.getLogger(DefaultBCFGoodWillRefundFacade.class);

	private static final String FINANCIAL_DATA_NOTIFICATION_URL = "financialDataServiceUrl";

	@Resource(name = "createOrderFinancialDataPipelineManager")
	private OrderFinancialDataPipelineManager orderFinancialDataPipelineManager;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "bcfBookingService")
	private BookingService bookingService;

	@Resource(name = "notificationEngineRestService")
	private NotificationEngineRestService notificationEngineRestService;

	private Converter<GoodWillRefundModel, GoodWillRefundData> bcfGoodWillRefundConverter;

	public Converter<GoodWillRefundModel, GoodWillRefundData> getBcfGoodWillRefundConverter()
	{
		return bcfGoodWillRefundConverter;
	}

	public void setBcfGoodWillRefundConverter(
			final Converter<GoodWillRefundModel, GoodWillRefundData> bcfGoodWillRefundConverter)
	{
		this.bcfGoodWillRefundConverter = bcfGoodWillRefundConverter;
	}

	public BCFGoodWillRefundService getBcfGoodWillRefundService()
	{
		return bcfGoodWillRefundService;
	}

	public void setBcfGoodWillRefundService(final BCFGoodWillRefundService bcfGoodWillRefundService)
	{
		this.bcfGoodWillRefundService = bcfGoodWillRefundService;
	}

	@Override
	public List<GoodWillRefundData> getAllRefundReasonCodes()
	{
		return bcfGoodWillRefundConverter.convertAll(bcfGoodWillRefundService.getAllGoodWillRefunds());

	}

	@Override
	public void processGoodWillRefund(final String bookingReference, final String reasonCode, final String comment,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas) throws BcfRefundException, Exception
	{
		bcfGoodWillRefundService.processGoodWillRefund(bookingReference, reasonCode, comment, refundTransactionInfoDatas);

		final OrderModel orderModel = bookingService.getOrderModelFromStore(bookingReference);
		try
		{
			final FinancialData financialData = orderFinancialDataPipelineManager.executePipeline(orderModel);
			notificationEngineRestService.sendRestRequest(financialData, FinancialData.class, HttpMethod.POST,
					FINANCIAL_DATA_NOTIFICATION_URL);
			orderModel.setCreateOrderFinancialDataSent(true);
			modelService.save(orderModel);

		}
		catch (final IntegrationException ex)
		{
			LOG.error("Failed to sent financial information for order: " + orderModel.getCode(), ex);
		}
	}
}
