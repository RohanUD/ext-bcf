/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.PositionData;
import de.hybris.platform.commercefacades.accommodation.search.RadiusData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.ruleengineservices.rule.dao.RuleDao;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationOfferingFacade;
import de.hybris.platform.travelfacades.facades.accommodation.impl.DefaultAccommodationOfferingFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.search.facetdata.AccommodationOfferingSearchPageData;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfTravelLocationDao;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.core.service.BcfSaleStatusService;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BcfAccommodationOfferingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.utils.BcfFacadeUtil;
import com.bcf.policies.RoundingPolicyService;
import com.bcf.travelservices.model.warehouse.AccommodationOfferingFromPriceModel;


public class DefaultBcfAccommodationOfferingFacade extends DefaultAccommodationOfferingFacade implements BcfAccommodationOfferingFacade
{

   @Resource(name = "accommodationOfferingFacade")
   private AccommodationOfferingFacade accommodationOfferingFacade;

   @Resource(name = "ruleDao")
   private RuleDao ruleDao;

   @Resource(name = "bcfTravelLocationDao")
   private BcfTravelLocationDao bcfTravelLocationDao;

   @Resource(name = "commerceCommonI18NService")
   private CommerceCommonI18NService commerceCommonI18NService;

   @Resource(name = "ceilingRoundingPolicyService")
   private RoundingPolicyService ceilingRoundingPolicyService;

	@Resource(name = "saleStatusService")
	BcfSaleStatusService saleStatusService;

   private BcfTravelLocationService bcfTravelLocationService;

   @Override
   public List<String> getAccommodationsLinkedToAccommodationOffering(final String accommodationOfferingCode)
   {
      return getAccommodationOfferingService().getAccommodationOffering(accommodationOfferingCode).getAccommodations();
   }

   @Override
   public AccommodationOfferingSearchPageData<SearchStateData, AccommodationOfferingDayRateData> searchAccommodationOfferingDayRates(
         final AccommodationSearchRequestData accommodationRequestData, final RoomStayCandidateData roomStayCandidateData)
   {
      final SearchData searchData = new SearchData();

      final Map<String, String> filterTerms = new HashMap<>();


      final PositionData position = accommodationRequestData.getCriterion().getPosition();
      final RadiusData radius = accommodationRequestData.getCriterion().getRadius();

      if (position != null && radius != null)
      {
         searchData.setSearchType(TravelservicesConstants.SOLR_SEARCH_TYPE_SPATIAL);

         filterTerms.put(TravelservicesConstants.SOLR_FIELD_POSITION,
               position.getLatitude() + TravelservicesConstants.LOCATION_TYPE_COORDINATES_SEPARATOR + position.getLongitude());
         filterTerms.put(TravelservicesConstants.SOLR_FIELD_RADIUS, Double.toString(radius.getValue()));
      }
      else if (accommodationRequestData.getCriterion().getAccommodationReference() != null && StringUtils
            .isNotEmpty(accommodationRequestData.getCriterion().getAccommodationReference().getAccommodationOfferingCode()))
      {
         searchData.setSearchType(TravelservicesConstants.SOLR_SEARCH_TYPE_ACCOMMODATION);
         filterTerms.put(TravelservicesConstants.SOLR_FIELD_PROPERTY_CODE,
               accommodationRequestData.getCriterion().getAccommodationReference().getAccommodationOfferingCode());
         //else if it consist of final accommodate offering code final in criterian  then final set filter terms with code.
      }
      else
      {
         searchData.setSearchType(TravelservicesConstants.SOLR_SEARCH_TYPE_ACCOMMODATION);
         filterTerms.put(TravelfacadesConstants.SOLR_FIELD_LOCATION_CODES,
               accommodationRequestData.getCriterion().getAddress().getLine1());
      }

      final String propertyFilterText = accommodationRequestData.getCriterion().getPropertyFilterText();
      if (StringUtils.isNotBlank(propertyFilterText))
      {
         searchData.setFreeTextSearch(propertyFilterText);
      }

      filterTerms.put(TravelfacadesConstants.SOLR_FIELD_DATE_OF_STAY,
            getDateOfStayFormatted(accommodationRequestData.getCriterion().getStayDateRange()));

      final Optional<Integer> maxCount = roomStayCandidateData.getPassengerTypeQuantityList().stream()
            .map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum);

      final Optional<Integer> adultMaxCount = roomStayCandidateData.getPassengerTypeQuantityList().stream()
            .filter(passengerTypeQuantity -> passengerTypeQuantity.getPassengerType().getCode().equals(
            BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT)).map(passengerTypeQuantity->passengerTypeQuantity.getQuantity()).reduce(Integer::sum);

      filterTerms.put(BcfCoreConstants.SOLR_FIELD_MAX_COUNT,
            String.format("[%s TO *]", maxCount.get().toString()) );

      filterTerms.put(BcfCoreConstants.SOLR_FIELD_MAX_ADULT_COUNT,
            String.format("[%s TO *]", adultMaxCount.get().toString()) );

      filterTerms.put(BcfCoreConstants.SOLR_FIELD_MINIMUM_LENGTH_OF_STAY,
            String.format("[* TO %s]", getStayLength(accommodationRequestData.getCriterion().getStayDateRange())));


      searchData.setFilterTerms(filterTerms);
      final String query = StringUtils.isNotEmpty(accommodationRequestData.getCriterion().getQuery())
            ? accommodationRequestData.getCriterion().getQuery()
            : StringUtils.EMPTY;
      searchData.setQuery(query);

      final AccommodationOfferingSearchPageData<SearchStateData, AccommodationOfferingDayRateData> accommodationOfferingSearchPageData;
      if (StringUtils.isNotBlank(accommodationRequestData.getCriterion().getSort()))
      {
         final PageableData pageableData = new PageableData();
         pageableData.setSort(accommodationRequestData.getCriterion().getSort());
         searchData.setSort(accommodationRequestData.getCriterion().getSort());
         accommodationOfferingSearchPageData = getAccommodationOfferingSearchFacade().accommodationOfferingSearch(searchData,
               pageableData);
      }
      else
      {
         accommodationOfferingSearchPageData = getAccommodationOfferingSearchFacade().accommodationOfferingSearch(searchData);
      }

      return accommodationOfferingSearchPageData;
   }

   /**
    * Gets the stay length.
    *
    * @param stayDateRange the stay date range
    * @return the stay length
    */
   protected String getStayLength(final StayDateRangeData stayDateRange)
   {
      final Date startDate = stayDateRange.getStartTime();
      final Date endDate = stayDateRange.getEndTime();
      final long numberOfDays = TravelDateUtils.getDaysBetweenDates(startDate, endDate);

      return String.valueOf(numberOfDays);
   }

   @Override
   public List<PropertyData> getPackageData(final String[] hotelCodes, final String packageDealCode,
         final String roomRateProductsParam)
   {
      final List<PropertyData> packageData = new ArrayList<>();
      final PromotionSourceRuleModel promotionSourceRule = ruleDao.findRuleByCode(packageDealCode);

      for (final String hotelCode : hotelCodes)
      {
         final PropertyData property = accommodationOfferingFacade.getPropertyData(hotelCode);

         packageData.add(getPackageData(property, promotionSourceRule, hotelCode));
      }

      populateNextUrl(packageData, packageDealCode, roomRateProductsParam);

      return packageData;
   }


   protected PackageData getPackageData(final PropertyData property, final PromotionSourceRuleModel promotionSourceRule,
         final String hotelCode)
   {
      final PackageData packageData = new PackageData();
      packageData.setFareSelectionData(new FareSelectionData());
      packageData.setTotalPackagePrice(getPrice(promotionSourceRule, hotelCode));

      packageData.setFiltered(false);
      packageData.setPerPersonPackagePrice(new PriceData());
      packageData.setAddress(property.getAddress());

      final LocationModel locationModel = getBcfTravelLocationService()
            .getLocationWithLocationType(
                  Collections.singletonList(getAccommodationOfferingService().getAccommodationOffering(hotelCode).getLocation()),
                  LocationType.CITY);

      final LocationData location = new LocationData();
      location.setCode(locationModel.getCode());
      packageData.setLocation(location);
      packageData.setAmenities(property.getAmenities());
      packageData.setAwards(property.getAwards());
      packageData.setRelativePosition(property.getRelativePosition());
      packageData.setPosition(property.getPosition());
      packageData.setRateRange(property.getRateRange());
      packageData.setBrandCode(property.getBrandCode());
      packageData.setChainCode(property.getChainCode());
      packageData.setChainName(property.getChainName());
      packageData.setAccommodationOfferingCode(property.getAccommodationOfferingCode());
      packageData.setAccommodationOfferingName(property.getAccommodationOfferingName());
      packageData.setImages(property.getImages());
      packageData.setPromoted(property.getPromoted());
      packageData.setDescription(property.getDescription());
      packageData.setPropertyInformation(promotionSourceRule.getPromoText());
      packageData.setRatePlanConfigs(property.getRatePlanConfigs());
      packageData.setPropertyAccomodationCodes(property.getPropertyAccomodationCodes());
      packageData.setCustomerReviewData(property.getCustomerReviewData());
      packageData.setReviewLocationId(property.getReviewLocationId());
      packageData.setPropertyCategoryTypes(property.getPropertyCategoryTypes());
      packageData.setIsDealOfTheDay(property.isIsDealOfTheDay());
      packageData.setTermsAndConditions(property.getTermsAndConditions());
      packageData.setBcfComments(property.getBcfComments());
		setPackageAvailabilityStatus(packageData, property, promotionSourceRule);

      return packageData;
   }

	private void setPackageAvailabilityStatus(final PackageData packageData, final PropertyData property,
			final PromotionSourceRuleModel promotionSourceRule)
	{
      final Collection<SaleStatusModel> saleStatuses = promotionSourceRule.getSaleStatuses();
      final Date today = new Date();
      final SaleStatusType saleStatusType = saleStatusService.getSaleStatusMatchedWithDate(saleStatuses, today);

		AvailabilityStatus packageAvailabilityStatus = AvailabilityStatus.AVAILABLE;

		if (StringUtils.isNotBlank(property.getAvailabilityStatus()))
		{
			packageAvailabilityStatus = AvailabilityStatus.valueOf(property.getAvailabilityStatus());
		}

		//override status if set to NOT_BOOKABLE in promotionSourceRule
		if (saleStatusService.toConsiderNonBookableOnlineSaleStatus() && Objects
				.equals(SaleStatusType.NOT_BOOKABLE_ONLINE, saleStatusType))
		{
			packageAvailabilityStatus = AvailabilityStatus.NOT_BOOKABLE_ONLINE;
		}
		packageData.setAvailabilityStatus(packageAvailabilityStatus.toString());
	}

   protected PriceData getPrice(final PromotionSourceRuleModel promotionSourceRule, final String hotelCode)
   {
      final PriceData price = new PriceData();
      price.setCurrencyIso(commerceCommonI18NService.getCurrentCurrency().getIsocode());
      final Optional<AccommodationOfferingFromPriceModel> accommodationOfferingFromPriceOptional = promotionSourceRule
            .getFromPrices().stream()
            .filter(accommodationOfferingFromPrice -> (accommodationOfferingFromPrice != null
                  && accommodationOfferingFromPrice.getAccommodationOffering() != null)).filter(
                  accommodationOfferingFromPrice -> hotelCode
                        .equalsIgnoreCase(accommodationOfferingFromPrice.getAccommodationOffering().getCode())).findFirst();
      if (accommodationOfferingFromPriceOptional.isPresent())
      {
         price.setValue(ceilingRoundingPolicyService.getRoundedValue(accommodationOfferingFromPriceOptional.get().getFromPrice(),
               BcfCoreConstants.PERCENTAGE_SCALE_FOR_CALCULATIONS));
         price.setFormattedValue(commerceCommonI18NService.getCurrentCurrency().getSymbol() + price.getValue());
      }

      return price;

   }

   protected void populateNextUrl(final List<PropertyData> packages, final String packageDealCode, final String roomRateProducts)
   {
      final Map<String, String> params = new HashMap<>();
      params.put(BcfFacadesConstants.PACKAGE_DEAL_HOTEL_LISTING_ROOM_RATE_PRODUCTS, roomRateProducts);
      params.put(BcfFacadesConstants.PACKAGE_DEAL_HOTEL_LISTING_HOTEL_DEAL_CODE, packageDealCode);

      for (final PropertyData packageData : packages)
      {
         if (packageData instanceof PackageData)
         {
            final PackageData pkgData = (PackageData) packageData;
            params.put(BcfFacadesConstants.PACKAGE_HOTEL_DETAILS_DESTINATION_CODE, packageData.getLocation().getCode());

            String cityName = packageData.getLocation().getCode();
            if (Objects.nonNull(packageData.getAddress()))
            {
               cityName = packageData.getAddress().getTown();
            }
            final String[] pathParams = { cityName.replace(" ", "-"),
                  packageData.getAccommodationOfferingName().replace(" ", "-"),
                  packageData.getAccommodationOfferingCode() };
            final String packageDealHotelDetailsUrl = BcfFacadeUtil
                  .getRelativeSiteUrl(BcfFacadesConstants.PACKAGE_HOTEL_DETAILS_PAGE_PATH, params, pathParams);

            pkgData.setNextUrl(packageDealHotelDetailsUrl);
         }
      }

   }

   public BcfTravelLocationService getBcfTravelLocationService()
   {
      return bcfTravelLocationService;
   }

   public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
   {
      this.bcfTravelLocationService = bcfTravelLocationService;
   }
}
