/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerFareData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commercefacades.travel.reservation.data.OfferBreakdownData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.impl.DefaultReservationFacade;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.manager.BcfPackageReservationDataListPipelineManager;
import com.bcf.facades.reservation.manager.ReservationDataListPipelineManager;


public class DefaultBCFReservationFacade extends DefaultReservationFacade implements BCFReservationFacade
{

	private TravelCommercePriceFacade travelCommercePriceFacade;
	private HashMap<String, ReservationDataListPipelineManager> reservationDataListPipelineManagerMap;
	private BcfPackageReservationDataListPipelineManager bcfPackageReservationDataListPipelineManager;
	private TimeService timeService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public ReservationDataList getCurrentReservations()
	{
		return getCartService().hasSessionCart() ? getReservationDataList(getCartService().getSessionCart()) : null;
	}

	@Override
	public ReservationDataList getReservationDataList(final AbstractOrderModel abstractOrderModel)
	{
		if (Objects.isNull(abstractOrderModel))
		{
			return null;
		}
		return getReservationDataListPipelineManagerMap().get(abstractOrderModel.getItemtype())
				.executePipeline(abstractOrderModel);
	}

	@Override
	public ReservationDataList getCurrentReservationsByTravelRoute(final String travelRoute)
	{
		return getCartService().hasSessionCart() ?
				getReservationDataListByTravelRoute(getCartService().getSessionCart(), travelRoute) :
				null;
	}

	@Override
	public ReservationDataList getReservationDataListByTravelRoute(final AbstractOrderModel abstractOrderModel,
			final String travelRoute)
	{
		if (Objects.isNull(abstractOrderModel) && Objects.isNull(travelRoute))
		{
			return null;
		}
		return getBcfPackageReservationDataListPipelineManager().executePipeline(abstractOrderModel, travelRoute);
	}

	@Override
	public PriceData getExtrasSummary(final ReservationDataList reservationDataList)
	{
		if (Objects.isNull(reservationDataList) || CollectionUtils.isEmpty(reservationDataList.getReservationDatas()))
		{
			return new PriceData();
		}
		BigDecimal totalExtras = BigDecimal.ZERO;
		final List<OfferBreakdownData> offerBreakdowns = new ArrayList<>();
		reservationDataList.getReservationDatas()
				.forEach(reservationData -> reservationData.getReservationItems().forEach(reservationItem -> {
					if (CollectionUtils.isNotEmpty(reservationItem.getReservationPricingInfo().getOfferBreakdowns()))
					{
						offerBreakdowns.addAll(reservationItem.getReservationPricingInfo().getOfferBreakdowns());
					}
				}));
		final List<OfferBreakdownData> reservationOfferBreakdowns = reservationDataList.getReservationDatas().stream()
				.filter(reservationData -> CollectionUtils.isNotEmpty(reservationData.getOfferBreakdowns()))
				.flatMap(reservationData -> reservationData.getOfferBreakdowns().stream()).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(reservationOfferBreakdowns))
		{
			offerBreakdowns.addAll(reservationOfferBreakdowns);
		}

		if (CollectionUtils.isNotEmpty(offerBreakdowns))
		{
			final List<OfferBreakdownData> extras = offerBreakdowns.stream()
					.filter(offer -> !offer.getIncluded()
							|| (offer.getIncluded() && !offer.getTotalFare().getTotalPrice().getValue().equals(BigDecimal.ZERO)))
					.collect(Collectors.toList());

			for (final OfferBreakdownData offer : extras)
			{
				totalExtras = totalExtras.add(offer.getTotalFare().getTotalPrice().getValue());
			}
		}

		return getTravelCommercePriceFacade().createPriceData(totalExtras.doubleValue());
	}

	@Override
	public Map<Integer, List<PTCFareBreakdownData>> getPTCBreakdownSummary(final ReservationDataList reservationDataList)
	{
		if (reservationDataList == null)
		{
			return Collections.emptyMap();
		}

		final Map<Integer, List<PTCFareBreakdownData>> ptcBreakdownsMap = new HashMap<>();

		for (final ReservationData reservationData : reservationDataList.getReservationDatas())
		{
			final List<PTCFareBreakdownData> ptcBreakdowns = new ArrayList<>();
			reservationData.getReservationItems().forEach(item -> {
				final List<PTCFareBreakdownData> ptcFareBreakdownDatas = item.getReservationPricingInfo().getItineraryPricingInfo()
						.getPtcFareBreakdownDatas();
				if (CollectionUtils.isEmpty(ptcBreakdowns))
				{
					createPTCBreakdownSummary(ptcBreakdowns, ptcFareBreakdownDatas);
				}
				else
				{
					updatePTCBreakdowns(ptcBreakdowns, ptcFareBreakdownDatas);
				}
			});
			ptcBreakdownsMap.put(reservationData.getJourneyReferenceNumber(), ptcBreakdowns);
		}

		return ptcBreakdownsMap;
	}

	protected void createPTCBreakdownSummary(final List<PTCFareBreakdownData> ptcBreakdownsSummary,
			final List<PTCFareBreakdownData> ptcFareBreakdownDatas)
	{
		ptcFareBreakdownDatas.forEach(ptc -> {
			final PTCFareBreakdownData breakdown = new PTCFareBreakdownData();
			breakdown.setPassengerTypeQuantity(ptc.getPassengerTypeQuantity());
			final PassengerFareData fare = new PassengerFareData();
			fare.setBaseFare(
					getTravelCommercePriceFacade().createPriceData(ptc.getPassengerFare().getBaseFare().getValue().doubleValue()));
			fare.setTotalFare(
					getTravelCommercePriceFacade().createPriceData(ptc.getPassengerFare().getTotalFare().getValue().doubleValue()));
			breakdown.setPassengerFare(fare);
			ptcBreakdownsSummary.add(breakdown);
		});

	}

	protected void updatePTCBreakdowns(final List<PTCFareBreakdownData> ptcBreakdowns,
			final List<PTCFareBreakdownData> newPtcFareBreakdowns)
	{
		ptcBreakdowns.forEach(ptcBreakdown -> {
			final List<PTCFareBreakdownData> matchingBreakdown = newPtcFareBreakdowns.stream()
					.filter(ptc -> ptc.getPassengerTypeQuantity().getPassengerType().getCode()
							.equals(ptcBreakdown.getPassengerTypeQuantity().getPassengerType().getCode()))
					.collect(Collectors.toList());
			final PassengerFareData fare = ptcBreakdown.getPassengerFare();
			final BigDecimal updatedBasePrice = fare.getBaseFare().getValue()
					.add(matchingBreakdown.get(0).getPassengerFare().getBaseFare().getValue());
			final BigDecimal updatedTotalPrice = fare.getTotalFare().getValue()
					.add(matchingBreakdown.get(0).getPassengerFare().getTotalFare().getValue());
			fare.setBaseFare(getTravelCommercePriceFacade().createPriceData(updatedBasePrice.doubleValue()));
			fare.setTotalFare(getTravelCommercePriceFacade().createPriceData(updatedTotalPrice.doubleValue()));
		});

	}

	@Override
	public ReservationItemData getReservationItemData(final ReservationDataList reservationDataList, final int journeyRefNumber,
			final int odRefNumber)
	{
		final List<ReservationData> reservationDatasByJourneyRefNumber = reservationDataList.getReservationDatas().stream()
				.filter(reservationData -> journeyRefNumber == reservationData.getJourneyReferenceNumber())
				.collect(Collectors.toList());

		for (final ReservationData reservationData : reservationDatasByJourneyRefNumber)
		{
			for (final ReservationItemData reservationItemData : reservationData.getReservationItems())
			{
				if (odRefNumber == reservationItemData.getOriginDestinationRefNumber())
				{
					return reservationItemData;
				}
			}
		}
		return null;
	}

	@Override
	public boolean isFirstTOMeetsDepartureTimeRestrictionGap(final ReservationData reservationData, final int thresholdTimeGap)
	{
		boolean enabled = false;
		for (final ReservationItemData reservationItem : reservationData.getReservationItems())
		{
			final List<TransportOfferingData> transportOfferings = reservationItem.getReservationItinerary()
					.getOriginDestinationOptions().stream().flatMap(odOption -> odOption.getTransportOfferings().stream())
					.collect(Collectors.toList());

			enabled = isFirstTOMeetsDepartureTimeRestrictionGap(transportOfferings, thresholdTimeGap);
		}
		return enabled;
	}

	@Override
	public boolean isFirstTOMeetsDepartureTimeRestrictionGap(final List<TransportOfferingData> transportOfferings,
			final int thresholdTimeGap)
	{
		boolean enabled = false;
		final Optional<TransportOfferingData> firstTransportOffering = transportOfferings.stream()
				.sorted(Comparator
						.comparing(to -> TravelDateUtils.getUtcZonedDateTime(to.getDepartureTime(), to.getDepartureTimeZoneId())))
				.findFirst();
		if (firstTransportOffering.isPresent())
		{
			enabled = TravelDateUtils.isAfter(firstTransportOffering.get().getDepartureTime(),
					firstTransportOffering.get().getDepartureTimeZoneId(),
					DateUtils.addMinutes(getTimeService().getCurrentTime(), thresholdTimeGap),
					ZoneId.systemDefault());
		}
		return enabled;
	}

	@Override
	public int getNumberOfJourneys(final String orderCode)
	{
		final OrderModel order = getBookingService().getOrder(orderCode);
		final Map<Integer, Set<Integer>> journeyOriginDestinationRefNumberMap = new HashMap<>();

		final List<AbstractOrderEntryModel> orderEntries = order.getEntries().stream().filter(entry -> entry.getActive()
				&& Objects.equals(OrderEntryType.TRANSPORT, entry.getType()) && Objects.nonNull(entry.getTravelOrderEntryInfo()))
				.collect(Collectors.toList());

		final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupByJourney = orderEntries.stream()
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		entriesGroupByJourney.forEach((journeyRefNo, journeyEntries) -> {
			final Set<Integer> distinctOriginDestRefNo = journeyEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
					.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).collect(Collectors.toSet());
			journeyOriginDestinationRefNumberMap.put(journeyRefNo, distinctOriginDestRefNo);
		});

		return journeyOriginDestinationRefNumberMap.values().stream().mapToInt(Collection::size).sum();
	}

	@Override
	public Map<String, List<TravellerData>> getTravellerDataMap(final ReservationData reservationData)
	{
		 boolean reservable=false;
		if(CollectionUtils.isNotEmpty(reservationData.getReservationItems())){
			reservable = reservationData.getReservationItems().stream().findFirst().get().getReservationItinerary()
					.getRoute().isReservable();
		}


		if (!reservable)
		{
			Map<String, List<TravellerData>> travellerDataMap = new HashMap<>();
			for (final ReservationItemData reservationItem : reservationData.getReservationItems())
			{
				travellerDataMap = reservationItem.getReservationItinerary()
						.getTravellers().stream()
						.filter(travellerData -> travellerData.getTravellerType().equals(TravellerType.PASSENGER.getCode()))
						.collect(Collectors
								.groupingBy(travellerData -> ((PassengerInformationData) travellerData.getTravellerInfo())
										.getPassengerType().getName()));

				travellerDataMap.putAll(reservationItem.getReservationItinerary().getTravellers().stream()
						.filter(travellerData -> travellerData.getTravellerType().equals(TravellerType.VEHICLE.getCode()))
						.collect(Collectors
								.groupingBy(travellerData -> ((VehicleInformationData) travellerData.getTravellerInfo())
										.getVehicleType().getCategory())));
			}
			return travellerDataMap;
		}
		return null;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected HashMap<String, ReservationDataListPipelineManager> getReservationDataListPipelineManagerMap()
	{
		return reservationDataListPipelineManagerMap;
	}

	@Required
	public void setReservationDataListPipelineManagerMap(
			final HashMap<String, ReservationDataListPipelineManager> reservationDataListPipelineManagerMap)
	{
		this.reservationDataListPipelineManagerMap = reservationDataListPipelineManagerMap;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * @return the bcfConfigurablePropertiesService
	 */
	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	/**
	 * @param bcfConfigurablePropertiesService the bcfConfigurablePropertiesService to set
	 */
	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BcfPackageReservationDataListPipelineManager getBcfPackageReservationDataListPipelineManager()
	{
		return bcfPackageReservationDataListPipelineManager;
	}

	@Required
	public void setBcfPackageReservationDataListPipelineManager(
			final BcfPackageReservationDataListPipelineManager bcfPackageReservationDataListPipelineManager)
	{
		this.bcfPackageReservationDataListPipelineManager = bcfPackageReservationDataListPipelineManager;
	}
}
