/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelfacades.facades.impl.DefaultPassengerTypeFacade;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.webservices.populators.PassengerTypeReversePopulator;
import com.bcf.webservices.travel.data.PassengerType;


public class DefaultBCFPassengerTypeFacade extends DefaultPassengerTypeFacade implements BCFPassengerTypeFacade
{
	private Converter<TitleModel, TitleData> titleConverter;
	private PassengerTypeReversePopulator passengerTypeReversePopulator;
	private ModelService modelService;

	@Override
	public List<TitleData> getAllowedTitles(final String passengerTypeCode)
	{
		final List<TitleData> titleDataList = new ArrayList<>();
		for (final TitleModel title : getPassengerTypeService().getPassengerType(passengerTypeCode).getAllowedTitles())
		{
			titleDataList.add(getTitleConverter().convert(title));
		}
		return titleDataList;
	}

	@Override
	public String getPassengerCodeForEbookingCode(final String eBookingCode)
	{
		final PassengerTypeModel passenger = ((BCFPassengerTypeService) getPassengerTypeService())
				.getPassengerForEBookingCode(eBookingCode);
		if (Objects.nonNull(passenger))
		{
			return passenger.getCode();
		}
		return null;
	}

	@Override
	public List<PassengerTypeData> getPassengerTypes()
	{
		final List<PassengerTypeModel> ptModels = getPassengerTypeService().getPassengerTypes();
		return Converters.convertAll(ptModels, this.getPassengerTypeConverter());
	}

	@Override
	public List<PassengerTypeData> getPassengerTypesForCodes(final List<String> codes)
	{
		final List<PassengerTypeModel> ptModels = ((BCFPassengerTypeService) getPassengerTypeService())
				.getPassengerTypesForCodes(codes);
		return Converters.convertAll(ptModels, this.getPassengerTypeConverter());
	}

	@Override
	public void createOrUpdatePassengerTypes(final List<PassengerType> passengerTypes) {
		passengerTypes.stream().forEach(passengerType -> {
			final List<PassengerTypeModel> ptModels = ((BCFPassengerTypeService) getPassengerTypeService())
					.getPassengerTypesForCodes(Arrays.asList(passengerType.getCode()));

			PassengerTypeModel passengerTypeModel = null;
			if (CollectionUtils.isNotEmpty(ptModels)) {
				passengerTypeModel = ptModels.iterator().next();
			} else {
				passengerTypeModel = getModelService().create(PassengerTypeModel.class);
			}
			passengerTypeReversePopulator.populate(passengerType, passengerTypeModel);
			getModelService().save(passengerTypeModel);
		});


	}

	@Override
	public void deletePassengerTypes(final String code)
	{
		((BCFPassengerTypeService) getPassengerTypeService()).deletePassengerTypeForCode(code);
	}

	@Override
	public Integer getMinAgeByPassengerTypeCode(final String code)
	{
		return ((BCFPassengerTypeService) getPassengerTypeService()).getMinAgeByPassengerTypeCode(code);
	}

	protected Converter<TitleModel, TitleData> getTitleConverter()
	{
		return titleConverter;
	}

	@Required
	public void setTitleConverter(final Converter<TitleModel, TitleData> titleConverter)
	{
		this.titleConverter = titleConverter;
	}

	protected ModelService getModelService() {
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}

	protected PassengerTypeReversePopulator getPassengerTypeReversePopulator() {
		return passengerTypeReversePopulator;
	}

	@Required
	public void setPassengerTypeReversePopulator(final PassengerTypeReversePopulator passengerTypeReversePopulator) {
		this.passengerTypeReversePopulator = passengerTypeReversePopulator;
	}

}
