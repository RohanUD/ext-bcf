/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfTermsAndConditionsService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.bcffacades.BcfTermsAndConditionsFacade;
import com.bcf.facades.booking.confirmation.TermsAndConditionsData;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.data.ActivityReservationDataList;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public class DefaultBcfTermsAndConditionsFacade implements BcfTermsAndConditionsFacade
{
	private BcfTermsAndConditionsService bcfTermsAndConditionsService;
	private BcfBookingService bcfBookingService;
	private Converter<FerryTandCModel, TermsAndConditionsData> bcfTermsAndConditionsDataConverter;

	@Override
	public List<TermsAndConditionsData> getTermsAndConditionsForBooking(final String bookingReference)
	{
		final Set<String> termsAndConditionCodes = new HashSet<>();
		StreamUtil.safeStream(getBcfBookingService().getOrder(bookingReference).getAmountToPayInfos())
				.forEach(amountToPayInfoModel -> {
					if (StringUtils.isNotBlank((amountToPayInfoModel.getTermsAndConditionsCode())))
					{
						termsAndConditionCodes.add(amountToPayInfoModel.getTermsAndConditionsCode());
					}
				});
		return getBcfTermsAndConditionsDataConverter()
				.convertAll(getBcfTermsAndConditionsService().getTermsAndConditionsModels(new ArrayList(termsAndConditionCodes)));
	}

	@Override
	public TermsAndConditionsData getTermsAndConditionsForCode(final String termsAndConditionsCode)
	{
		return getBcfTermsAndConditionsDataConverter().convert(getBcfTermsAndConditionsService()
				.getTermsAndConditionsModel(termsAndConditionsCode));
	}

	protected BcfTermsAndConditionsService getBcfTermsAndConditionsService()
	{
		return bcfTermsAndConditionsService;
	}

	/**
	 * Gets the property terms condition map.
	 *
	 * @param bcfGlobalReservationData the bcf global reservation data
	 * @return the property terms condition map
	 */
	@Override
	public Map<String, String> getPropertyTermsConditionMap(final BcfGlobalReservationData bcfGlobalReservationData)
	{
		final Map<String, String> propertyTermsConditionMap = new HashMap<>();
		if (Objects.nonNull(bcfGlobalReservationData.getAccommodationReservations()))
		{
			propertyTermsConditionMap.putAll(getPropertyTermsConditionMap(
					bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()));
		}
		if (Objects.nonNull(bcfGlobalReservationData.getPackageReservations())
				&& CollectionUtils.isNotEmpty(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()))
		{
			final List<AccommodationReservationData> accommodationReservations = bcfGlobalReservationData.getPackageReservations()
					.getPackageReservationDatas().stream().map(GlobalTravelReservationData::getAccommodationReservationData)
					.collect(Collectors.toList());
			propertyTermsConditionMap.putAll(getPropertyTermsConditionMap(accommodationReservations));
		}
		return propertyTermsConditionMap;
	}

	/**
	 * Gets the property terms condition map.
	 *
	 * @param accommodationReservations the accommodation reservations
	 * @return the property terms condition map
	 */
	protected Map<String, String> getPropertyTermsConditionMap(final List<AccommodationReservationData> accommodationReservations)
	{
		if (CollectionUtils.isEmpty(accommodationReservations))
		{
			return null;
		}

		final Map<String, String> propertyTermsConditionMap = new HashMap<>();
		for (final AccommodationReservationData accommodationReservation : accommodationReservations)
		{
			final PropertyData propertyData = accommodationReservation.getAccommodationReference();
			propertyTermsConditionMap.putIfAbsent(propertyData.getAccommodationOfferingName(), propertyData.getTermsAndConditions());
		}

		return propertyTermsConditionMap;
	}

	@Override
	public Map<String, String> getActivityTermsConditionMap(final BcfGlobalReservationData bcfGlobalReservationData)
	{
		final Map<String, String> activityTermsConditionMap = new HashMap<>();
		// for activities only booking
		if (Objects.nonNull(bcfGlobalReservationData.getActivityReservations()))
		{
			activityTermsConditionMap.putAll(
					getActivityTermsConditionMap(bcfGlobalReservationData.getActivityReservations().getActivityReservationDatas()));
		}
		// for activities within a package booking
		if (Objects.nonNull(bcfGlobalReservationData.getPackageReservations()) && CollectionUtils
				.isNotEmpty(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()))
		{
			final List<ActivityReservationDataList> activityReservationsLists = bcfGlobalReservationData.getPackageReservations()
					.getPackageReservationDatas().stream().map(GlobalTravelReservationData::getActivityReservations)
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(activityReservationsLists))
			{
				final List<ActivityReservationData> activityReservations = activityReservationsLists.stream()
						.flatMap(activityReservationDataList -> activityReservationDataList.getActivityReservationDatas().stream())
						.collect(Collectors.toList());
				activityTermsConditionMap.putAll(getActivityTermsConditionMap(activityReservations));
			}
		}
		return activityTermsConditionMap;
	}

	protected Map<String, String> getActivityTermsConditionMap(final List<ActivityReservationData> activityReservations)
	{
		if (CollectionUtils.isEmpty(activityReservations))
		{
			return null;
		}
		final Map<String, String> activityTermsConditionMap = new HashMap<>();
		for (final ActivityReservationData activityReservationData : activityReservations)
		{
			final ActivityDetailsData activityDetailsData = activityReservationData.getActivityDetails();
			activityTermsConditionMap.putIfAbsent(activityDetailsData.getName(), activityDetailsData.getTermsAndConditions());
		}
		return activityTermsConditionMap;
	}

	@Required
	public void setBcfTermsAndConditionsService(final BcfTermsAndConditionsService bcfTermsAndConditionsService)
	{
		this.bcfTermsAndConditionsService = bcfTermsAndConditionsService;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected Converter<FerryTandCModel, TermsAndConditionsData> getBcfTermsAndConditionsDataConverter()
	{
		return bcfTermsAndConditionsDataConverter;
	}

	@Required
	public void setBcfTermsAndConditionsDataConverter(
			final Converter<FerryTandCModel, TermsAndConditionsData> bcfTermsAndConditionsDataConverter)
	{
		this.bcfTermsAndConditionsDataConverter = bcfTermsAndConditionsDataConverter;
	}
}
