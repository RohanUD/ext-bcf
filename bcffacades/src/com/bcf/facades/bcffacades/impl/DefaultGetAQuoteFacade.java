/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.model.CsTicketModel;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.GetAQuoteService;
import com.bcf.facades.bcffacades.GetAQuoteFacade;
import com.bcf.facades.change.booking.CsTicketData;
import com.bcf.facades.getaquote.data.CsTicketListAndCountData;
import com.bcf.facades.getaquote.data.GetAQuoteData;


public class DefaultGetAQuoteFacade implements GetAQuoteFacade
{
	private static final String POSTAL_CODE = "Postal Code";
	private static final String CHECKIN_DATE_TIME = "CheckInDateTime";
	private static final String PRODUCT_NAME = "ProductName";
	private static final String MORE_INFO = "MoreInfo";
	private static final String COMMENTS = "Comments";
	private static final String PASSENGER_TYPE = "PassengerType";
	private static final String COMMA = ",";
	private static final String HYPHEN = "-";
	private static final String COLON = ":";
	private static final String SPACE = " ";

	private GetAQuoteService getAQuoteService;

	private Converter<CsTicketModel, CsTicketData> getAQuoteCsTicketConverter;

	@Override
	public void createTicketForGetQuote(final GetAQuoteData getAQuoteData,
			final String title)
	{
		final AtomicInteger counter = new AtomicInteger(1);
		final StringBuilder travellerInformation = new StringBuilder();
		getAQuoteData.getRoomStayCandidates().stream().forEach(traveller -> {
			travellerInformation.append("Room").append(SPACE).append(counter).append(COLON).append(SPACE);
			traveller.getPassengerTypeQuantityList().stream().forEach(passenger -> {
				if (passenger.getQuantity() != 0)
				{
					travellerInformation.append(PASSENGER_TYPE).append(COLON).append(SPACE)
							.append(passenger.getPassengerType().getCode()).append(HYPHEN).append(passenger.getQuantity())
							.append(COMMA);
				}
			});
			travellerInformation.append(System.lineSeparator());
			counter.getAndIncrement();
		});

		travellerInformation.append(CHECKIN_DATE_TIME).append(COLON).append(SPACE)
				.append(getAQuoteData.getCheckInDateTime())
				.append(COMMA).append(System.lineSeparator()).append(MORE_INFO).append(COLON)
				.append(SPACE).append(getAQuoteData.isMoreInfo() ? "Yes" : "No")
				.append(COMMA).append(System.lineSeparator()).append(COMMENTS).append(COLON)
				.append(SPACE).append(getAQuoteData.getComments())
				.append(COMMA).append(System.lineSeparator()).append(PRODUCT_NAME).append(COLON)
				.append(SPACE).append(getAQuoteData.getProductName())
				.append(COMMA).append(System.lineSeparator()).append(POSTAL_CODE).append(COLON)
				.append(SPACE).append(getAQuoteData.getPostalCode());

		getGetAQuoteService()
				.createTicketForQuote(travellerInformation, getAQuoteData.getFirstName(), getAQuoteData.getLastName(),
						getAQuoteData.getEmail(), getAQuoteData.getPhoneNumber(), title);
	}

	@Override
	public CsTicketListAndCountData getOpenTickets(final int startIndex, final int pageNumber,
			final int recordsPerPage, final String category)
	{
		final List<String> statuses = Arrays
				.asList(CsTicketState.NEW.getCode(), CsTicketState.OPEN.getCode(), CsTicketState.INPROGRESS.getCode());
		final CsTicketListAndCountData csTicketswithCount = getGetAQuoteService()
				.getOpenTickets(startIndex, pageNumber, recordsPerPage, statuses, category);
		csTicketswithCount
				.setCsTickets(Converters.convertAll(csTicketswithCount.getCsTicketList(), getGetAQuoteCsTicketConverter()));

		return csTicketswithCount;
	}

	@Override
	public void changeTicketStatusForQuote(final String status, final String ticketId)
	{
		getGetAQuoteService().changeTicketStatusForQuote(status, ticketId);
	}

	public GetAQuoteService getGetAQuoteService()
	{
		return getAQuoteService;
	}

	@Required
	public void setGetAQuoteService(final GetAQuoteService getAQuoteService)
	{
		this.getAQuoteService = getAQuoteService;
	}

	public Converter<CsTicketModel, CsTicketData> getGetAQuoteCsTicketConverter()
	{
		return getAQuoteCsTicketConverter;
	}

	@Required
	public void setGetAQuoteCsTicketConverter(
			final Converter<CsTicketModel, CsTicketData> getAQuoteCsTicketConverter)
	{
		this.getAQuoteCsTicketConverter = getAQuoteCsTicketConverter;
	}
}
