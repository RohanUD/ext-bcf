/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.travelfacades.facades.impl.DefaultBookingFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.OrderUserAccountMappingModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.amend.order.manager.AmendableCartPipelineManager;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.managebooking.data.AdjustCostData;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.order.cancel.enums.CancelStatus;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.model.ShareItineraryProcessModel;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integration.ebooking.service.SearchBookingService;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.booking.response.PaymentRecord;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.order.financial.FinancialData;


public class DefaultBcfBookingFacade extends DefaultBookingFacade implements BcfBookingFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfBookingFacade.class);

	private static final String MANAGE_MY_BOOKING_BOOKING_REFERENCE = "manage_my_booking_booking_reference";
	private static final String MANAGE_MY_BOOKING_AUTHENTICATION = "manage_my_booking_authentication";
	private static final String CANCEL_ORDER_EXCEPTION = "text.page.managemybooking.cancel.order.exception";
	private static final String BCF_BOOKING_REFERENCES = "bcfBookingReferences";
	private static final String SEARCH_FROM_EBOOKING = "mybookings.search.from.ebooking.enable";
	private static final String ACTIVITY_LEAD_PAX_START = "activityLeadPaxInfo[";
	private static final String ASM_SESSION_PARAMETER = "ASM";
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;
	private BcfBookingService bcfBookingService;
	private SearchBookingService searchBookingService;
	private AmendableCartPipelineManager amendableCartPipelineManager;
	private SearchBookingFacade searchBookingFacade;
	private ConfigurationService configurationService;
	private CreateOrderNotificationPipelineManager createOrderNotificationPipelineManager;
	private NotificationEngineRestService notificationEngineRestService;
	private CommonI18NService commerceI18NService;
	private CalculationService calculationService;
	private BcfRefundIntegrationService bcfRefundIntegrationService;
	private BcfCheckoutService bcfCheckoutService;
	private BcfOrderService bcfOrderService;
	private BCFTravelCartService bcfTravelCartService;
	private OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager;

	@Resource(name = "asmOrderViewDataConverter")
	private Converter<AbstractOrderModel, AsmOrderViewData> asmOrderViewDataConverter;

	@Autowired
	private BusinessProcessService businessProcessService;



	@Override
	public boolean checkBookingJourneyType(final String orderCode, final List<BookingJourneyType> bookingJourneyTypes)
	{
		if (StringUtils.isBlank(orderCode))
		{
			return Boolean.FALSE;
		}
		final OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(orderCode);
		if (Objects.isNull(orderModel))
		{
			return Boolean.FALSE;
		}
		return bookingJourneyTypes.contains(orderModel.getBookingJourneyType());
	}

	@Override
	public String getBookingJourneyType(final String orderCode)
	{
		final OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(orderCode);
		return orderModel.getBookingJourneyType().getCode();
	}


	@Override
	public boolean isTransportBookingJourney(final String orderCode)
	{
		if (StringUtils.isBlank(orderCode))
		{
			return Boolean.TRUE;
		}
		final OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(orderCode);
		if (Objects.isNull(orderModel))
		{
			return Boolean.TRUE;
		}

		return BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(orderModel.getBookingJourneyType());

	}



	@Override
	public ReservationDataList getBookingsByBookingReferenceAndAmendingOrder(final String yBookingReference)
	{
		OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(yBookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			orderModel = getBookingService().getOrderModelByOriginalOrderCode(yBookingReference);
		}
		return convertOrderModelToReservationDataList(orderModel);
	}

	protected ReservationDataList convertOrderModelToReservationDataList(final OrderModel orderModel)
	{
		return ((BCFReservationFacade) getReservationFacade()).getReservationDataList(orderModel);
	}




	@Override
	public CancelBookingResponseData cancelTransportBookings(final String orderCode, final int journeyRefNum, final int odRefNum)
	{
		final CancelBookingResponseData cancelBookingResponseData = new CancelBookingResponseData();
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		try
		{
			final OrderModel order = getCustomerAccountService().getOrderForCode(orderCode, currentBaseStore);
			final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
					.cancelBookings(order, journeyRefNum, odRefNum);
			doSailingCancellation(order, cancelBookingResponseData, removeSailingResponseDatas);
		}
		catch (final ModelNotFoundException | IntegrationException ex)
		{
			poulateCancelBookingResponseData(orderCode, cancelBookingResponseData, ex);
		}
		return cancelBookingResponseData;
	}


	/**
	 * Do sailing cancellation.
	 *
	 * @param order                      the order
	 * @param cancelBookingResponseData  the cancel booking response data
	 * @param removeSailingResponseDatas the remove sailing response datas
	 */
	protected void doSailingCancellation(final OrderModel order, final CancelBookingResponseData cancelBookingResponseData,
			final List<RemoveSailingResponseData> removeSailingResponseDatas)
	{
		final List<RemoveSailingResponseData> failureCancelEBookings = getCancelBookingIntegrationFacade()
				.filterCancelBookingList(removeSailingResponseDatas, false);
		cancelBookingResponseData.setFailureCancelEBookings(failureCancelEBookings);

		final List<RemoveSailingResponseData> successCancelEBookings = getCancelBookingIntegrationFacade()
				.filterCancelBookingList(removeSailingResponseDatas, true);
		if (CollectionUtils.isEmpty(successCancelEBookings))
		{
			cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_FAIL);
			return;
		}

		final boolean cancelPartialTransportOrder = cancelPartialTransportOrder(order, successCancelEBookings);
		cancelBookingResponseData
				.setCancelStatus(cancelPartialTransportOrder ? CancelStatus.COMPLETE_SUCCESS : CancelStatus.COMPLETE_FAIL);
	}

	/**
	 * Poulate cancel booking response data.
	 *
	 * @param orderCode                 the order code
	 * @param cancelBookingResponseData the cancel booking response data
	 * @param ex                        the ex
	 */
	@Override
	public void poulateCancelBookingResponseData(final String orderCode,
			final CancelBookingResponseData cancelBookingResponseData, final Exception ex)
	{
		LOG.error("An " + ex.getMessage() + " error occurred while doing the order cancellation for " + orderCode + ".", ex);
		cancelBookingResponseData.setException(CANCEL_ORDER_EXCEPTION);
		cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_FAIL);
	}

	/**
	 * Cancel partial transport order.
	 *
	 * @param order                  the order code
	 * @param successCancelEBookings the success cancel E bookings
	 * @return true, if successful
	 */
	protected boolean cancelPartialTransportOrder(final OrderModel order,
			final List<RemoveSailingResponseData> successCancelEBookings)
	{
		final Set<AbstractOrderEntryModel> filteredOrderEntriesToRemove = getFilteredOrderEntries(order, successCancelEBookings);
		return getBcfBookingService().cancelPartialOrder(order, filteredOrderEntriesToRemove);
	}

	/**
	 * Gets the filtered order entries.
	 *
	 * @param order                  the order
	 * @param successCancelEBookings the success cancel E bookings
	 * @return the filtered order entries
	 */
	protected Set<AbstractOrderEntryModel> getFilteredOrderEntries(final AbstractOrderModel order,
			final List<RemoveSailingResponseData> successCancelEBookings)
	{
		final Set<AbstractOrderEntryModel> orderEntries = new HashSet<>();
		successCancelEBookings.forEach(successCancelEBooking -> {
			final List<AbstractOrderEntryModel> matchedOrderEntries = order.getEntries().stream()
					.filter(orderEntry -> isMatchedOrderEntry(orderEntry, successCancelEBooking.getBookingReference()))
					.collect(Collectors.toList());
			orderEntries.addAll(matchedOrderEntries);
		});
		return orderEntries;
	}

	/**
	 * Checks if is matched order entry.
	 *
	 * @param orderEntry       the order entry
	 * @param bookingReference the booking reference
	 * @return true, if is matched order entry
	 */
	protected boolean isMatchedOrderEntry(final AbstractOrderEntryModel orderEntry, final String bookingReference)
	{
		return orderEntry.getActive() && Objects.equals(orderEntry.getType(), OrderEntryType.TRANSPORT)
				&& StringUtils.equals(orderEntry.getBookingReference(), bookingReference);
	}

	@Override
	public PriceData getAmountToPay(final String bookingReference)
	{
		OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			orderModel = getBookingService().getOrderModelByOriginalOrderCode(bookingReference);
		}
		return getTravelCommercePriceFacade()
				.createPriceData(((Objects.nonNull(orderModel.getAmountToPay()) ? orderModel.getAmountToPay() : 0d)),
						orderModel.getCurrency().getIsocode());
	}

	@Override
	public Double getPayAtTerminal(final String bookingReference)
	{
		OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			orderModel = getBookingService().getOrderModelByOriginalOrderCode(bookingReference);
		}
		return Objects.nonNull(orderModel.getPayAtTerminal()) ? orderModel.getPayAtTerminal() : 0d;
	}

	@Override
	public PriceData getAmountPaid(final String bookingReference)
	{
		OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			orderModel = getBookingService().getOrderModelByOriginalOrderCode(bookingReference);
		}
		return getTravelCommercePriceFacade()
				.createPriceData(((Objects.nonNull(orderModel.getAmountToPayInfos()) ?
								orderModel.getAmountToPayInfos().stream().mapToDouble(AmountToPayInfoModel::getPreviouslyPaid).sum() :
								0d) / 100.0),
						orderModel.getCurrency().getIsocode());

	}

	@Override
	public void shareItinerary(final OrderModel order, final Set<String> emails)
	{
		final ShareItineraryProcessModel shareItineraryProcess = getBusinessProcessService()
				.createProcess("shareItineraryNotificationProcess-" + order.getCode() + "-" + System.currentTimeMillis(),
						"shareItineraryNotificationProcess");
		shareItineraryProcess.setOrder(order);
		shareItineraryProcess.setShareItineraryEmails(emails);
		getModelService().save(shareItineraryProcess);
		getBusinessProcessService().startProcess(shareItineraryProcess);
	}

	/**
	 * calculates order total, if order is net then it adds total tax as well to the order total and returns the same
	 *
	 * @param bookingReference
	 * @return
	 */
	@Override
	public BigDecimal getOrderTotal(final String bookingReference)
	{
		OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			orderModel = getBookingService().getOrderModelByOriginalOrderCode(bookingReference);
		}

		final BigDecimal totalPrice = BigDecimal.valueOf(orderModel.getTotalPrice());
		return totalPrice;
	}

	/**
	 * simply returns amount to pay for an order. This is being used for getting amount to pay for pay now functionality
	 * And if an order status is amendment in progress then we return a zero value as amount to be paid
	 * and that is being handled at front end
	 *
	 * @param bookingReference
	 * @return
	 */
	@Override
	public PriceData getAmountToBePaidForOrder(final String bookingReference)
	{
		final OrderModel orderModel = getBookingService().getOrderModelFromStore(bookingReference);
		if (OrderStatus.AMENDMENTINPROGRESS.equals(orderModel.getStatus()))
		{
			return getTravelCommercePriceFacade().createPriceData(0d);
		}

		return getTravelCommercePriceFacade().createPriceData(orderModel.getAmountToPay());
	}

	@Override
	public boolean validateUserForBooking(final String bookingReference)
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(bookingReference);

		if (orderModel == null)
		{
			return Boolean.FALSE;
		}

		if (isUserOrderOwner(bookingReference) || isCurrentUserAndOrderLinked(currentCustomer, orderModel.getCode())
				|| ((BcfUserService) getUserService()).isB2BCustomer())
		{
			return Boolean.TRUE;
		}

		final Boolean mmbAuthentication = getSessionService().getAttribute(MANAGE_MY_BOOKING_AUTHENTICATION);

		return mmbAuthentication != null && mmbAuthentication && StringUtils
				.equalsIgnoreCase(getSessionService().getAttribute(MANAGE_MY_BOOKING_BOOKING_REFERENCE), bookingReference);
	}

	@Override
	public PriceData getRefundTotal(final String orderCode, final int journeyRefNum, final int odRefNum)
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		final OrderModel order = getCustomerAccountService().getOrderForCode(orderCode, currentBaseStore);
		final BigDecimal refundTotal = getBcfBookingService().getOrderTotalPriceByJourney(order, journeyRefNum, odRefNum);
		return getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, refundTotal, order.getCurrency().getIsocode());
	}


	@Override
	public void createAmendableCart(final String orderCode, final String guid, final boolean searchEbooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException
	{
		final OrderModel orderModel = getBcfBookingService().getOrder(orderCode);
		final List<String> bookingReferencesList;
		CartModel cartModel = null;
		if (Objects.isNull(orderModel))
		{
			final String bcfBookingReferences = getSessionService().getAttribute(BCF_BOOKING_REFERENCES);
			bookingReferencesList = searchBookingFacade.createBCFBookingRefList(bcfBookingReferences);
			//cartModel=create new cart from ebooking
			final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
			final SearchBookingResponseDTO searchBookingResponse = getSearchBookingResponse(customerModel, bookingReferencesList,
					0);
			bcfTravelCartService.removeSessionCart();
			cartModel = getAmendableCartPipelineManager().executePipeline(searchBookingResponse);
		}
		else
		{
			if (searchEbooking)
			{
				//update order from ebooking
				final CustomerModel customerModel = (CustomerModel) orderModel.getUser();
				final Boolean searchFromEBooking = getConfigurationService().getConfiguration().getBoolean(SEARCH_FROM_EBOOKING);
				if (searchFromEBooking)
				{
					bookingReferencesList = orderModel.getEntries().stream()
							.filter(orderEntry -> orderEntry.getActive() && StringUtils.isNotEmpty(orderEntry.getBookingReference()))
							.map(AbstractOrderEntryModel::getBookingReference).distinct().collect(Collectors.toList());

					if (CollectionUtils.isNotEmpty(bookingReferencesList))
					{
						final SearchBookingResponseDTO searchBookingResponseDTO = getSearchBookingResponse(customerModel,
								bookingReferencesList, 0);
						//do order update
						getBcfBookingService().updateOrderFromEBooking(orderModel, searchBookingResponseDTO);
					}
				}
			}
			cartModel = getTravelCartService().createCartFromOrder(orderCode, guid);
		}

		getTravelCartService().setSessionCart(cartModel);
	}

	@Override
	public CartModel createAndSetAmendableCartForEBookingRef(final String eBookingReference, final boolean searchEBooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException
	{
		OrderModel orderModel = null;
		CartModel cartModel = null;
		final String orderCode = getOrderCodeForEBookingCode(eBookingReference);
		if (StringUtils.isNotBlank(orderCode))
		{
			orderModel = getBcfBookingService().getOrder(orderCode);
		}
		if (Objects.nonNull(orderModel))
		{
			long previouslyPaidFareInCents = 0;
			LOG.info(String.format("Order with eBooking reference [%s] is present in hybris with order code [%s]", eBookingReference,
					orderCode));
			if (searchEBooking)
			{
				final CustomerModel customer = (CustomerModel) orderModel.getUser();//NOSONAR

				if (StringUtils.isNotBlank(eBookingReference))
				{
					final SearchBookingResponseDTO searchBookingResponseDTO = getSearchBookingResponse(customer,
							Arrays.asList(eBookingReference), BcfFacadesConstants.DEFAULT_PAGE_NUMBER);
					LOG.info(String.format("Updating hybris order [%s] with search booking response", orderCode));
					getBcfBookingService().updateOrderFromEBooking(orderModel, searchBookingResponseDTO);
					final Itinerary itineraryForBooking = StreamUtil.safeStream(searchBookingResponseDTO.getSearchResult())
							.filter(itinerary -> StringUtils.equalsIgnoreCase(eBookingReference,
									itinerary.getBooking().getBookingReference().getBookingReference())).findFirst().get();
					previouslyPaidFareInCents = CollectionUtils.isNotEmpty(itineraryForBooking.getPaymentRecords()) ?
							itineraryForBooking.getPaymentRecords().stream().mapToInt(PaymentRecord::getAmountPaidInCents).sum() : 0;
				}
			}
			cartModel = getTravelCartService().createCartFromOrder(orderCode, orderModel.getUser().getUid());//NOSONAR
			cartModel.setAmountPaid(previouslyPaidFareInCents > 0 ? previouslyPaidFareInCents / 100d : 0d);
			cartModel.setAmountToPayInfos(null);
			getModelService().save(cartModel);
		}
		else if (searchEBooking)
		{
			final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
			final List<String> eBookingReferenceList = searchBookingFacade.createBCFBookingRefList(eBookingReference);
			final SearchBookingResponseDTO searchBookingResponse = getSearchBookingService()
					.searchBookings(customer, eBookingReferenceList, BcfFacadesConstants.DEFAULT_PAGE_NUMBER);
			LOG.info(String.format(
					"Order with eBooking reference [%s] is not present in hybris, creating cart from search booking response",
					eBookingReference));
			cartModel = getAmendableCartPipelineManager().executePipeline(searchBookingResponse);
			cartModel.setAmountToPayInfos(null);
			getModelService().save(cartModel);
		}
		removeAmendableCart();
		getTravelCartService().setSessionCart(cartModel);
		return cartModel;
	}

	@Override
	public void removeAmendableCart()
	{
		final CartModel sessionAmendedCart = bcfTravelCartService.getExistingSessionAmendedCart();
		if (Objects.nonNull(sessionAmendedCart))
		{
			getTravelCartService().removeSessionCart();
		}
	}

	@Override
	public String getOrderCodeForEBookingCode(final String eBookingCode)
	{
		final OrderModel order = (OrderModel) getBcfOrderService().getOrderByEBookingCode(eBookingCode);
		if (Objects.nonNull(order))
		{
			return order.getCode();
		}
		return null;
	}

	@Override
	public String getGuidForEBookingCode(final String eBookingCode)
	{
		final OrderModel order = (OrderModel) getBcfOrderService().getOrderByEBookingCode(eBookingCode);
		if (Objects.nonNull(order))
		{
			return order.getGuid();
		}
		return null;
	}

	@Override
	public List<OrderEntryModel> findOrderEntriesByEBookingCode(final String eBookingCode)
	{
		return getBcfOrderService().findOrderEntriesByEBookingCode(eBookingCode);
	}

	@Override
	public OrderModel getGuestOrderForGUID(final String guid)
	{
		return ((BcfCustomerAccountService) getCustomerAccountService())
				.getGuestOrderForGUID(guid);
	}

	@Override
	public String getGuestIdentifierForOrderCode(final String orderCode)
	{
		final OrderModel order = getCustomerAccountService()
				.getOrderForCode(orderCode, getBaseStoreService().getCurrentBaseStore());
		if (Objects.nonNull(order))
		{
			final CustomerModel customer = (CustomerModel) order.getUser();
			if (Objects.equals(customer.getType(), CustomerType.GUEST))
			{
				return order.getGuid();
			}
		}
		return null;
	}


	@Override
	public Boolean updateAccommodationOrderEntryGroup(final List<RoomGuestData> roomGuestDatas)
	{
		if (CollectionUtils.isNotEmpty(roomGuestDatas))
		{

			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = bcfBookingService
					.getAccommodationOrderEntryGroups(getTravelCartService().getSessionCart());
			final List<ItemModel> itemsToSave = new ArrayList<>();
			for (final RoomGuestData roomGuestData : roomGuestDatas)
			{


				for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup : accommodationOrderEntryGroups)
				{
					if (TravelDateUtils.convertDateToStringDate(accommodationOrderEntryGroup.getStartingDate(),
							BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY).equals(roomGuestData.getCheckInDate())
							&& accommodationOrderEntryGroup.getRoomStayRefNumber() == roomGuestData.getRoomRefNumber()
							&& accommodationOrderEntryGroup.getAccommodation().getCode().equals(roomGuestData.getAccommodationCode()))
					{
						accommodationOrderEntryGroup.setFirstName(roomGuestData.getFirstName());
						accommodationOrderEntryGroup.setLastName(roomGuestData.getLastName());
						itemsToSave.add(accommodationOrderEntryGroup);
					}
				}
			}

			if (CollectionUtils.isNotEmpty(itemsToSave))
			{
				getModelService().saveAll(itemsToSave);
				return Boolean.TRUE;
			}
		}

		return Boolean.FALSE;
	}


	private void updateAccommodationOrderEntryGroup(final String firstName, final String lastName)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = bcfBookingService
				.getAccommodationOrderEntryGroups(getTravelCartService().getSessionCart());
		final List<ItemModel> itemsToSave = new ArrayList<>();

		for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup : accommodationOrderEntryGroups)
		{
			accommodationOrderEntryGroup.setFirstName(firstName);
			accommodationOrderEntryGroup.setLastName(lastName);
			itemsToSave.add(accommodationOrderEntryGroup);

		}
		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}
	}

	@Override
	public void prepareAndSaveLeadPassengerDetails(final Map<String, String> resultMap)
	{
		final String sessionBookingJourney = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isNotEmpty(sessionBookingJourney))
		{
			if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode())
					&& StringUtils.isNotBlank(resultMap.get("activityCount")))
			{
				final int count = Integer.valueOf(resultMap.get("activityCount"));
				final List<ActivityGuestData> activityGuestDatas = new ArrayList<>();
				for (int i = 0; i < count; i++)
				{
					final ActivityGuestData activityGuestData = new ActivityGuestData();
					activityGuestData.setActivityCode(resultMap.get(ACTIVITY_LEAD_PAX_START + i + "].activityCode"));
					activityGuestData.setFirstName(resultMap.get(ACTIVITY_LEAD_PAX_START + i + "].firstName"));
					activityGuestData.setLastName(resultMap.get(ACTIVITY_LEAD_PAX_START + i + "].lastName"));
					activityGuestDatas.add(activityGuestData);
				}
				updateActivityOrderEntryInfo(activityGuestDatas);
			}
			else if (!StringUtils.equalsIgnoreCase(sessionBookingJourney, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode()))
			{

				if (StringUtils.isBlank(resultMap.get("roomCount")))
				{
					resultMap.put("roomGuestData[0].firstName", resultMap.get("firstName"));
					resultMap.put("roomGuestData[0].lastName", resultMap.get("lastName"));

					updateAccommodationOrderEntryGroup(resultMap.get("roomGuestData[0].firstName"),
							resultMap.get("roomGuestData[0].lastName"));
				}
				else if (StringUtils.isNotBlank(resultMap.get("roomCount")))
				{

					final int count = Integer.valueOf(resultMap.get("roomCount"));

					final List<RoomGuestData> roomGuestDatas = new ArrayList<>();
					for (int i = 0; i < count; i++)
					{
						final RoomGuestData roomGuestData = new RoomGuestData();
						roomGuestDatas.add(roomGuestData);
						roomGuestData.setFirstName(resultMap.get("roomGuestData[" + i + "].firstName"));
						roomGuestData.setLastName(resultMap.get("roomGuestData[" + i + "].lastName"));
						roomGuestData.setAccommodationCode(resultMap.get("roomGuestData[" + i + "].accommodationCode"));
						roomGuestData.setRoomRefNumber(Integer.valueOf(resultMap.get("roomGuestData[" + i + "].roomRefNumber")));
						roomGuestData.setCheckInDate(resultMap.get("roomGuestData[" + i + "].checkInDate"));

					}

					updateAccommodationOrderEntryGroup(roomGuestDatas);
				}



				//if activity is present in the booking then update the lead pax in activityorderentryInfo from room 1
				updateActivityOrderEntryInfoFromAccomodationLeadPaxDetails(resultMap.get("roomGuestData[0].firstName"),
						resultMap.get("roomGuestData[0].lastName"));
			}
		}
	}

	/**
	 * This is the case in Package journey
	 * <br>
	 * this method will update lead pax details in ActivityOrderEntryInfo from the lead pax details from Room 1
	 *
	 * @param firstName
	 * @param lastName
	 */
	@Override
	public void updateActivityOrderEntryInfoFromAccomodationLeadPaxDetails(final String firstName, final String lastName)
	{
		final AbstractOrderModel cart = getTravelCartService().getSessionCart();
		if (!bcfTravelCartService.isCartValidWithMinimumRequirements(cart))
		{
			return;
		}
		final Map<ProductModel, List<AbstractOrderEntryModel>> activityOrderEntryModels = cart.getEntries().stream()
				.filter(entry -> OrderEntryType.ACTIVITY.equals(entry.getType()))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getProduct));

		if (MapUtils.isNotEmpty(activityOrderEntryModels))
		{
			final List<ItemModel> itemsToSave = new ArrayList<>();
			try
			{
				activityOrderEntryModels.keySet().stream()
						.forEach(activityProduct -> {
							final List<AbstractOrderEntryModel> orderEntriesForActivityProduct = activityOrderEntryModels
									.get(activityProduct);

							orderEntriesForActivityProduct.stream().forEach(activityOrderEntry -> {
								final ActivityOrderEntryInfoModel activityOrderEntryInfoModel = activityOrderEntry
										.getActivityOrderEntryInfo();
								activityOrderEntryInfoModel.setLeadPaxFirstName(firstName);
								activityOrderEntryInfoModel.setLeadPaxLastName(lastName);
								itemsToSave.add(activityOrderEntryInfoModel);
							});
						});
			}
			finally
			{
				if (CollectionUtils.isNotEmpty(itemsToSave))
				{
					getModelService().saveAll(itemsToSave);
				}
			}
		}
	}


	@Override
	public boolean updateActivityOrderEntryInfo(final List<ActivityGuestData> activityGuestDatas)
	{
		final AbstractOrderModel cart = getTravelCartService().getSessionCart();
		if (!bcfTravelCartService.isCartValidWithMinimumRequirements(cart))
		{
			return false;
		}

		final List<ItemModel> itemsToSave = new ArrayList<>();
		try
		{
			final List<AbstractOrderEntryModel> activityOrderEntryModels = cart.getEntries().stream()
					.filter(entry -> OrderEntryType.ACTIVITY.equals(entry.getType())).collect(Collectors.toList());

			for (final ActivityGuestData activityGuestData : activityGuestDatas)
			{
				for (final AbstractOrderEntryModel activityOrderEntry : activityOrderEntryModels)
				{
					if (activityOrderEntry.getProduct().getCode().equals(activityGuestData.getActivityCode()))
					{
						itemsToSave.add(addLeadPaxDetails(activityOrderEntry, activityGuestData.getFirstName(),
								activityGuestData.getLastName()));
					}
				}
			}
		}
		finally
		{
			if (CollectionUtils.isNotEmpty(itemsToSave))
			{
				getModelService().saveAll(itemsToSave);
			}
		}
		return true;
	}

	private ActivityOrderEntryInfoModel addLeadPaxDetails(final AbstractOrderEntryModel activityOrderEntry,
			final String firstName, final String lastName)
	{
		final ActivityOrderEntryInfoModel orderEntryInfo = activityOrderEntry.getActivityOrderEntryInfo();
		orderEntryInfo.setLeadPaxFirstName(firstName);
		orderEntryInfo.setLeadPaxLastName(lastName);
		return orderEntryInfo;
	}


	//THIS METHOD IS TEMPORARY AND WILL BE REMOVED
	@Override
	public OrderModel getOrderFromBookingReferenceCode(final String bookingReferenceCode)
	{
		return getBcfBookingService().getOrder(bookingReferenceCode);
	}

	/**
	 * Gets the search booking response.
	 *
	 * @param customerModel         the customer model
	 * @param bookingReferencesList the booking references list
	 * @param currentPageNumber     the current page number
	 * @return the search booking response
	 * @throws IntegrationException the integration exception
	 */
	protected SearchBookingResponseDTO getSearchBookingResponse(final CustomerModel customerModel,
			final List<String> bookingReferencesList, final int currentPageNumber) throws IntegrationException
	{
		return getSearchBookingService().searchBookings(customerModel, bookingReferencesList, currentPageNumber);
	}

	/**
	 * Checks if is current user and order linked.
	 *
	 * @param currentCustomer the current customer
	 * @param orderCode       the order code
	 * @return true, if is current user and order linked
	 */
	protected boolean isCurrentUserAndOrderLinked(final CustomerModel currentCustomer, final String orderCode)
	{
		final OrderUserAccountMappingModel customerOrderLink = getCustomerAccountService().getOrderUserMapping(orderCode,
				currentCustomer);
		return Objects.nonNull(customerOrderLink);
	}

	@Override
	public boolean adjustCostForBooking(final List<AdjustCostData> adjustAccommodationCosts,
			final List<AdjustCostData> adjustActivityCosts, final String bookingReferenceCode) throws IntegrationException
	{

		final OrderModel order = bcfBookingService.getOrder(bookingReferenceCode);
		final List<ItemModel> itemsToSave = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(adjustAccommodationCosts))
		{
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = bcfBookingService
					.getAccommodationOrderEntryGroups(order);
			for (final AdjustCostData adjustCostData : adjustAccommodationCosts)
			{
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = StreamUtil
						.safeStream(accommodationOrderEntryGroupModels).filter(
								accommodationOrderEntry -> accommodationOrderEntry.getRoomStayRefNumber() == adjustCostData
										.getRefNumber()).findAny().orElse(null);

				populateItems(itemsToSave, adjustCostData, accommodationOrderEntryGroupModel);
			}
		}

		if (CollectionUtils.isNotEmpty(adjustActivityCosts))
		{
			for (final AdjustCostData adjustCostData : adjustActivityCosts)
			{
				final AbstractOrderEntryModel activityEntry = order.getEntries().stream()
						.filter(entry -> adjustCostData.getRefNumber() == entry.getEntryNumber()).findAny().orElse(null);
				if (activityEntry != null)
				{
					setAdjustmentAgent(activityEntry);
					activityEntry.setAdjustedCost(Double.valueOf(adjustCostData.getCost()));
					itemsToSave.add(activityEntry);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(itemsToSave))
		{
			getModelService().saveAll(itemsToSave);
		}

		final FinancialData financialData = getCreateOrderFinancialDataPipelineManager().executePipeline(order);
		getNotificationEngineRestService()
				.sendRestRequest(financialData, FinancialData.class, HttpMethod.POST,
						BcfintegrationserviceConstants.FINANCIAL_DATA_NOTIFICATION_URL);
		return true;
	}

	private void setAdjustmentAgent(final AbstractOrderEntryModel activityEntry)
	{
		final AssistedServiceSession asmSession = getSessionService().getAttribute(ASM_SESSION_PARAMETER);
		if (Objects.nonNull(asmSession) && Objects.nonNull(asmSession.getAgent()))
		{
			final EmployeeModel asmAgent = (EmployeeModel) asmSession.getAgent();
			if (asmAgent != null)
			{
				activityEntry.setCostAdjustmentAgent(asmAgent);
			}
		}
	}

	private void populateItems(final List<ItemModel> itemsToSave, final AdjustCostData adjustCostData,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{
		if (accommodationOrderEntryGroupModel != null)
		{
			if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getEntries()))
			{
				for (final AbstractOrderEntryModel entry : accommodationOrderEntryGroupModel.getEntries())
				{
					setAdjustmentAgent(entry);
					entry.setAdjustedCost(Double.valueOf(adjustCostData.getCost()));
					itemsToSave.add(entry);
				}

			}
			else if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getDealAccommodationEntries()))
			{

				for (final AbstractOrderEntryModel entry : accommodationOrderEntryGroupModel.getDealAccommodationEntries())
				{
					setAdjustmentAgent(entry);
					entry.setAdjustedCost(Double.valueOf(adjustCostData.getCost()));
					itemsToSave.add(entry);
				}
			}
		}
	}


	@Override
	public void getAdjustedCostForBooking(final List<AdjustCostData> adjustAccommodationCosts,
			final List<AdjustCostData> adjustActivityCosts, final String bookingReferenceCode)
	{
		final OrderModel order = bcfBookingService.getOrder(bookingReferenceCode);

		if (CollectionUtils.isNotEmpty(adjustAccommodationCosts))
		{
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = bcfBookingService
					.getAccommodationOrderEntryGroups(order);

			for (final AdjustCostData adjustCostData : adjustAccommodationCosts)
			{
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = StreamUtil
						.safeStream(accommodationOrderEntryGroupModels).filter(
								accommodationOrderEntry -> accommodationOrderEntry.getRoomStayRefNumber() == adjustCostData
										.getRefNumber()).findAny().orElse(null);
				populateAdjustCost(adjustCostData, accommodationOrderEntryGroupModel);
			}
		}

		if (CollectionUtils.isNotEmpty(adjustActivityCosts))
		{
			for (final AdjustCostData adjustCostData : adjustActivityCosts)
			{
				final AbstractOrderEntryModel activityEntry = order.getEntries().stream()
						.filter(entry -> adjustCostData.getRefNumber() == entry.getEntryNumber()).findAny().orElse(null);
				if (activityEntry != null && activityEntry.getAdjustedCost() != null)
				{
					adjustCostData.setCost(String.valueOf(activityEntry.getAdjustedCost()));
				}

			}
		}
	}

	private void populateAdjustCost(final AdjustCostData adjustCostData,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{
		if (accommodationOrderEntryGroupModel != null)
		{
			if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getEntries()))
			{
				if (accommodationOrderEntryGroupModel.getEntries().get(0).getAdjustedCost() != null)
				{
					adjustCostData.setCost(String.valueOf(accommodationOrderEntryGroupModel.getEntries().get(0).getAdjustedCost()));
				}


			}
			else if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getDealAccommodationEntries()))
			{
				final AbstractOrderEntryModel entry = accommodationOrderEntryGroupModel.getDealAccommodationEntries().iterator()
						.next();
				if (entry.getAdjustedCost() != null)
				{
					adjustCostData.setCost(String.valueOf(entry.getAdjustedCost()));
				}
			}
		}
	}

	@Override
	public String getPackageAvailabilityStatus(final String bookingReference)
	{
		final OrderModel orderModel = bcfBookingService.getOrderModelFromStore(bookingReference);
		return orderModel.getAvailabilityStatus();
	}

	/**
	 * @return the cancelBookingIntegrationFacade
	 */
	protected CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	/**
	 * @param cancelBookingIntegrationFacade the cancelBookingIntegrationFacade to set
	 */
	@Required
	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

	/**
	 * @return the bcfBookingService
	 */
	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	/**
	 * @param bcfBookingService the bcfBookingService to set
	 */
	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	/**
	 * @return the searchBookingService
	 */
	protected SearchBookingService getSearchBookingService()
	{
		return searchBookingService;
	}

	/**
	 * @param searchBookingService the searchBookingService to set
	 */
	@Required
	public void setSearchBookingService(final SearchBookingService searchBookingService)
	{
		this.searchBookingService = searchBookingService;
	}

	protected AmendableCartPipelineManager getAmendableCartPipelineManager()
	{
		return amendableCartPipelineManager;
	}

	@Required
	public void setAmendableCartPipelineManager(final AmendableCartPipelineManager amendableCartPipelineManager)
	{
		this.amendableCartPipelineManager = amendableCartPipelineManager;
	}

	/**
	 * @return the searchBookingFacade
	 */
	protected SearchBookingFacade getSearchBookingFacade()
	{
		return searchBookingFacade;
	}

	/**
	 * @param searchBookingFacade the searchBookingFacade to set
	 */
	@Required
	public void setSearchBookingFacade(final SearchBookingFacade searchBookingFacade)
	{
		this.searchBookingFacade = searchBookingFacade;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public CreateOrderNotificationPipelineManager getCreateOrderNotificationPipelineManager()
	{
		return createOrderNotificationPipelineManager;
	}

	public void setCreateOrderNotificationPipelineManager(
			final CreateOrderNotificationPipelineManager createOrderNotificationPipelineManager)
	{
		this.createOrderNotificationPipelineManager = createOrderNotificationPipelineManager;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}

	public BcfRefundIntegrationService getBcfRefundIntegrationService()
	{
		return bcfRefundIntegrationService;
	}

	public void setBcfRefundIntegrationService(final BcfRefundIntegrationService bcfRefundIntegrationService)
	{
		this.bcfRefundIntegrationService = bcfRefundIntegrationService;
	}

	public BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	public BcfOrderService getBcfOrderService()
	{
		return bcfOrderService;
	}

	public void setBcfOrderService(final BcfOrderService bcfOrderService)
	{
		this.bcfOrderService = bcfOrderService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected OrderFinancialDataPipelineManager getCreateOrderFinancialDataPipelineManager()
	{
		return createOrderFinancialDataPipelineManager;
	}

	@Required
	public void setCreateOrderFinancialDataPipelineManager(
			final OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager)
	{
		this.createOrderFinancialDataPipelineManager = createOrderFinancialDataPipelineManager;
	}

	@Override
	public AsmOrderViewData getASMOrderData(final AbstractOrderModel orderModel)
	{
		final AsmOrderViewData orderData = asmOrderViewDataConverter.convert(orderModel);
		return orderData;
	}
}
