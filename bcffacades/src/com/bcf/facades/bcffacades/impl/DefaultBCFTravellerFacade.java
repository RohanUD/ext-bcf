/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.SpecialRequestDetailData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.impl.DefaultTravellerFacade;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.services.impl.DefaultBcfTravellerService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.PassengerRequestData;
import com.bcf.facades.cart.VehicleRequestData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.travel.data.BCFTravellerDataPerJourney;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.facades.traveller.manager.TravellerDataPipelineManager;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBCFTravellerFacade extends DefaultTravellerFacade implements BCFTravellerFacade
{
	private Converter<VehicleInformationData, BCFVehicleInformationModel> vehicleInformationReverseConverter;

	private TravellerDataPipelineManager travellerDataPipelineManager;

	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;

	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;

	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public TravellerData createVehicleTraveller(final String travellerType, final VehicleTypeQuantityData vehicleData,
			final String travellerCode, final int passengerNumber, final String travellerUidPrefix)
	{
		return createVehicleTraveller(travellerType, vehicleData, travellerCode, passengerNumber, travellerUidPrefix, null);
	}

	@Override
	public TravellerData createTravellerWithSpecialServiceRequest(
			final String travellerType, final String passengerType, final String travellerCode, final int passengerNumber,
			final String travellerUidPrefix, final String cartOrOrderCode, final List<SpecialServicesData> specialServicesDataList)
	{
		final TravellerModel travellerModel = ((BCFTravellerService) getTravellerService())
				.createTravellerWithSpecialServiceRequest(travellerType, passengerType, travellerCode, passengerNumber,
						travellerUidPrefix,
						cartOrOrderCode, specialServicesDataList);
		return getTravellerDataConverter().convert(travellerModel);
	}


	@Override
	public void updateTravellerWithSpecialServiceRequest(final TravellerData travellerData,
			final List<SpecialServicesData> specialServicesDataList)
	{
		final TravellerModel travellerModel = this.getTravellerService().getTravellerFromCurrentCart(travellerData.getLabel());
		if (travellerModel != null)
		{
			((BCFTravellerService) getTravellerService())
					.updateTravellerWithSpecialServiceRequest(travellerModel, specialServicesDataList);
		}
	}


	@Override
	public TravellerData createVehicleTraveller(final String travellerType, final VehicleTypeQuantityData vehicleData,
			final String travellerCode, final int passengerNumber, final String travellerUidPrefix, final String cartOrOrderCode)
	{
		final TravellerModel travellerModel = ((BCFTravellerService) getTravellerService()).createVehicleInformation(travellerType,
				vehicleData, travellerCode, passengerNumber, travellerUidPrefix, cartOrOrderCode);
		return getTravellerDataConverter().convert(travellerModel);
	}

	@Override
	public Map<String, Map<String, String>> populateTravellersNamesMap(final List<TravellerData> travellerDatas)
	{
		final Map<String, Map<String, String>> travellerPassengerTypeMap = new HashMap<>();

		for (final PassengerTypeData passengerTypeData : getPassengerTypeFacade().getPassengerTypes())
		{
			final Map<String, String> travellerNameMap = new HashMap<>();

			final int counter = 1;
			for (final TravellerData travellerData : travellerDatas)
			{
				if (travellerData.getTravellerInfo() instanceof PassengerInformationData)
				{
					if (!passengerTypeData.getCode().equalsIgnoreCase(
							((PassengerInformationData) travellerData.getTravellerInfo()).getPassengerType().getCode()))
					{
						continue;
					}
					if (!travellerPassengerTypeMap.containsKey(passengerTypeData.getCode()))
					{
						travellerPassengerTypeMap.put(passengerTypeData.getCode(), travellerNameMap);
					}

					populateTravellerNameMap(travellerData, travellerNameMap, counter);
				}
			}
		}
		return travellerPassengerTypeMap;
	}

	private void populateTravellerNameMap(final TravellerData travellerData, final Map<String, String> travellerNameMap,
			int counter)
	{
		final PassengerInformationData passengerInfoData = ((PassengerInformationData) travellerData.getTravellerInfo());
		if (StringUtils.isEmpty(passengerInfoData.getFirstName()) && StringUtils.isEmpty(passengerInfoData.getSurname()))
		{
			travellerNameMap
					.put(travellerData.getLabel(), passengerInfoData.getPassengerType().getName() + " " + counter++);//NOSONAR
		}
		else
		{
			travellerNameMap.put(travellerData.getLabel(), passengerInfoData.getFirstName() + " " + passengerInfoData.getSurname());
		}
	}

	/**
	 * Method adds a new traveller to the user's list of saved traveller.
	 *
	 * @return TravellerData
	 */
	@Override
	public TravellerData addCustomerSavedTravellers(final TravellerData travellerData)
	{
		return saveAndGetNewTravellerDetail(travellerData);
	}

	@Override
	public void updateTravellerInformation(
			final AddBundleToCartRequestData addBundleToCartRequestData, final List<TravellerData> travellerDataList) throws
			IntegrationException
	{
		updateTravellerDetails(travellerDataList);
		BCFMakeBookingResponse bcfMakeBookingResponse = null;
		final CartModel cart = getCartService().getSessionCart();

		if (bcfTravelCartFacade.isAmendmentCart() || (!getUserService().isAnonymousUser(cart.getUser())
				&& !AccountType.SUBSCRIPTION_ONLY.equals(((CustomerModel) cart.getUser()).getAccountType())))
		{
			final BCFModifyBookingResponse bcfModifyBookingResponse = modifyBookingIntegrationFacade
					.getModifyBookingResponse(addBundleToCartRequestData, BcfCoreConstants.MAKE_BOOKING_REQUEST);
			bcfMakeBookingResponse = bcfModifyBookingResponse.getMakeBookingResponse();
		}
		else

		{
			bcfMakeBookingResponse = makeBookingIntegrationFacade.getMakeBookingResponse(addBundleToCartRequestData);
		}
		final String cachingKey = bcfMakeBookingResponse.getItinerary().getBooking().getBookingReference().getCachingKey();
		final String bookingReference = bcfMakeBookingResponse.getItinerary().getBooking().getBookingReference()
				.getBookingReference();
		bcfTravelCartFacade.updatePaymentInfosWithCacheKeyOrBookingRef(bcfTravelCartService.getSessionCart(),
				addBundleToCartRequestData.getSelectedJourneyRefNumber(), addBundleToCartRequestData.getSelectedOdRefNumber(),
				cachingKey, bookingReference);
		bcfTravelCartFacade.updateCartEntriesWithCacheKeyOrBookingRef(bcfTravelCartService.getSessionCart(),
				addBundleToCartRequestData.getSelectedJourneyRefNumber(), addBundleToCartRequestData.getSelectedOdRefNumber(),
				cachingKey, bookingReference);

	}

	/**
	 * overridden to save the vehicle details as well along with passenger details
	 */
	@Override
	public void updateTravellerDetails(final List<TravellerData> travellers)
	{

		for (final TravellerData td : travellers)
		{
			final TravellerData newSavedTraveller;
			if (BcfFacadesConstants.TRAVELLER_TYPE_VEHICLE.equals(td.getTravellerType()))
			{
				updateAndGetExistingVehicleTravellerDetails(td);
				continue;
			}
			final PassengerInformationData passengerInformation = (PassengerInformationData) td.getTravellerInfo();
			// does the user want their details to be save
			if (passengerInformation.isSaveDetails())
			{
				// check if this user is the one making the booking and if so then update the current Users CustomerPassengerInstance
				// otherwise save the user against the current Users savedTravellers
				if (td.isBooker())
				{
					updateCurrentUserCustomerPassengerInstance(td, getPassengerInformationReverseConverter());
					newSavedTraveller = getCustomerTravellerInstanceData();
					td.setSavedTravellerUid(newSavedTraveller.getUid());
				}
				else
				{
					newSavedTraveller = saveAndGetTravellerAgainstCurrentUser(td);
					td.setSavedTravellerUid(newSavedTraveller.getUid());
				}
			}
			updateAndGetExistingTravellerDetails(td, getPassengerInformationReverseConverter());
		}
	}

	/**
	 * to be used when save traveler details option is enabled for the current user
	 */
	protected TravellerData saveAndGetVehicleTravellerAgainstCurrentUser(final TravellerData travellerData)
	{
		final VehicleInformationData vehicleInformationData = (VehicleInformationData) travellerData.getTravellerInfo();

		if (StringUtils.isNotBlank(vehicleInformationData.getSavedTravellerUId()))
		{
			travellerData.setUid(vehicleInformationData.getSavedTravellerUId());
			return updateAndGetSavedVehicleTravellerDetails(travellerData);
		}
		else
		{
			return saveAndGetNewTravellerDetail(travellerData);
		}
	}

	/**
	 * updates the existing vehicle traveler created during add to cart operation with the captured details like license
	 * plate, country etc
	 */
	protected TravellerData updateAndGetExistingVehicleTravellerDetails(final TravellerData travellerData)
	{
		final TravellerModel travellerModel = getTravellerService().getTravellerFromCurrentCart(travellerData.getLabel());
		return updateAndGetVehicleTravellerDetails(travellerData, travellerModel);
	}

	/**
	 * Method responsible for updating an existing saved traveller details with the data provided by travellerData and
	 * return the same
	 *
	 * @param travellerData the traveller data
	 * @return travellerData traveller data
	 */
	protected TravellerData updateAndGetSavedVehicleTravellerDetails(final TravellerData travellerData)
	{
		final TravellerModel travellerModel = getTravellerService().getExistingTraveller(travellerData.getUid());
		return updateAndGetVehicleTravellerDetails(travellerData, travellerModel);
	}

	/**
	 * Method responsible for updating an existing travellers details with the data provided by travellerData and return
	 * the same
	 *
	 * @param travellerData  the traveller data
	 * @param travellerModel the traveller model
	 * @return travellerData traveller data
	 */
	protected TravellerData updateAndGetVehicleTravellerDetails(final TravellerData travellerData,
			final TravellerModel travellerModel)
	{
		final TravellerModel vehicleTravellerModel = ((DefaultBcfTravellerService) getTravellerService())
				.updateAndGetVehicleTraveller(travellerData, travellerModel);

		return getTravellerDataConverter().convert(vehicleTravellerModel);
	}

	/**
	 * overridden to create vehicle traveler as well
	 */
	@Override
	protected TravellerModel createTravellerModel(final TravellerData travellerData)
	{
		if (TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER.equals(travellerData.getTravellerType())
				|| !(travellerData.getTravellerInfo() instanceof VehicleInformationData))
		{
			return super.createTravellerModel(travellerData);
		}
		final VehicleInformationData vehicleInformationData = (VehicleInformationData) travellerData.getTravellerInfo();


		final TravellerModel travellerModel = ((BCFTravellerService) getTravellerService()).createVehicleInformation(
				TravellerType.VEHICLE.toString(), vehicleInformationData, travellerData.getLabel(),
				Integer.parseInt(travellerData.getFormId()), StringUtils.EMPTY, StringUtils.EMPTY);

		final BCFVehicleInformationModel vehicleInformationModel = getVehicleInformationReverseConverter()
				.convert(vehicleInformationData);

		travellerModel.setInfo(vehicleInformationModel);
		travellerModel.setBooker(false);

		travellerModel.setSavedTravellerUid(travellerData.getSavedTravellerUid());

		getModelService().save(vehicleInformationModel);
		getModelService().save(travellerModel);

		return travellerModel;
	}

	protected Converter<VehicleInformationData, BCFVehicleInformationModel> getVehicleInformationReverseConverter()
	{
		return vehicleInformationReverseConverter;
	}

	@Required
	public void setVehicleInformationReverseConverter(
			final Converter<VehicleInformationData, BCFVehicleInformationModel> vehicleInformationReverseConverter)
	{
		this.vehicleInformationReverseConverter = vehicleInformationReverseConverter;
	}

	@Override
	protected TravellerData updateAndGetTravellerDetails(final TravellerData travellerData, final TravellerModel travellerModel,
			final Converter<PassengerInformationData, PassengerInformationModel> passengerInformationReverseConverter)
	{
		final PassengerInformationModel passengerInformationModel = Objects.nonNull(travellerModel.getInfo())
				? (PassengerInformationModel) travellerModel.getInfo()
				: new PassengerInformationModel();

		final PassengerInformationData passengerInformationData = (PassengerInformationData) travellerData.getTravellerInfo();
		passengerInformationReverseConverter.convert(passengerInformationData, passengerInformationModel);
		travellerModel.setInfo(passengerInformationModel);
		travellerModel.setSavedTravellerUid(travellerData.getSavedTravellerUid());
		travellerModel.setBooker(travellerData.isBooker());
		travellerModel.setLabel(travellerData.getLabel());

		getModelService().save(passengerInformationModel);
		getModelService().save(travellerModel);



		return getTravellerDataConverter().convert(travellerModel);
	}

	@Override
	public BCFTravellersData getTravellersGroupedByJourney(final CartFilterParamsDto cartFilterParamsDto)
	{
		return getTravellerDataPipelineManager().executePipeline(getCartService().getSessionCart(), cartFilterParamsDto);
	}

	@Override
	public BCFTravellersData getPassengersByTypeGroupedByJourney(final Integer journeyRefNum, final String eBookingRef,
			final List<String> routes, final Set<String> passengerTypes,
			final List<Pair<String, String>> passengerTypeAndUidsToBeRemoved)
	{
		return getTravellerDataPipelineManager()
				.executePipeline(getCartService().getSessionCart(), journeyRefNum, eBookingRef, routes, passengerTypes,
						passengerTypeAndUidsToBeRemoved);
	}


	@Override
	public Map<String, Long> getPassengersTypeAndQuantityFromCart(final Integer journeyRefNum, final String eBookingRef)
	{
		final List<AbstractOrderEntryModel> entries;
		entries = getCartService().getSessionCart().getEntries();

		final Map<String, Long> travellersMap = entries.stream()
				.filter(
						entry -> entry.getActive() && entry.getJourneyReferenceNumber() == journeyRefNum && StringUtils
								.equals(entry.getBookingReference(), eBookingRef) && Objects.nonNull(entry.getProduct())
								&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype())
								&& entry.getTravelOrderEntryInfo() != null && entry.getTravelOrderEntryInfo().getTravellers().iterator()
								.next().getType().equals(TravellerType.PASSENGER))
				.map(entry -> entry.getTravelOrderEntryInfo().getTravellers().iterator().next()).collect(Collectors
						.groupingBy(traveller -> ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
								.getCode(), Collectors.counting()));

		return travellersMap;

	}

	@Override
	public Map<String, Long> getAccessibilityPassengersTypeAndQuantityFromCart(final Integer journeyRefNum,
			final String eBookingRef)
	{
		final List<AbstractOrderEntryModel> entries;
		entries = getCartService().getSessionCart().getEntries();

		final Map<String, Long> travellersMap = entries.stream()
				.filter(
						entry -> entry.getActive() && entry.getJourneyReferenceNumber() == journeyRefNum && eBookingRef
								.equals(entry.getBookingReference()) && FareProductModel._TYPECODE
								.equals(entry.getProduct().getItemtype())
								&& entry.getTravelOrderEntryInfo() != null && entry.getTravelOrderEntryInfo()
								.getTravellers().iterator().next().getType().equals(TravellerType.PASSENGER)
								&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail() != null)
				.map(entry -> entry.getTravelOrderEntryInfo().getTravellers().iterator().next()).collect(Collectors
						.groupingBy(traveller -> ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
								.getCode(), Collectors.counting()));

		return travellersMap;

	}




	protected TravellerDataPipelineManager getTravellerDataPipelineManager()
	{
		return travellerDataPipelineManager;
	}

	@Required
	public void setTravellerDataPipelineManager(final TravellerDataPipelineManager travellerDataPipelineManager)
	{
		this.travellerDataPipelineManager = travellerDataPipelineManager;
	}

	@Override
	public void setTravellerPerOriginDestination(final Integer journey, final List<TravellerData> travellersPerJourney,
			final List<String> selectedServicesForJourneyAndTraveller)
	{
		for (final TravellerData travellerData : travellersPerJourney)
		{
			if (travellerData.getSpecialRequestDetail() != null
					&& CollectionUtils.isNotEmpty(travellerData.getSpecialRequestDetail().getSpecialServiceRequests()))
			{
				for (final SpecialServiceRequestData ssrd : travellerData.getSpecialRequestDetail().getSpecialServiceRequests())
				{
					final String combinedKey = journey + "-" + travellerData.getUid() + "-" + ssrd.getCode();

					if (!selectedServicesForJourneyAndTraveller.contains(combinedKey))
					{
						selectedServicesForJourneyAndTraveller.add(combinedKey);
					}
				}
			}
		}
	}


	@Override
	public AddBundleToCartRequestData getRequestData(
			final BCFTravellersData bcfTravellersData, final int journeyRefNo, final int odRefNo)
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
		final List<PassengerTypeData> passengerTypeDataList = new ArrayList<>();
		final List<VehicleTypeData> vehicleTypeDataList = new ArrayList<>();
		final List<PassengerTypeQuantityData> passengerTypeQuantityDataList = new ArrayList<>();
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();
		final List<AbstractOrderEntryModel> transportEntries = bcfTravelCartFacade.getCartEntriesForRefNo(journeyRefNo, odRefNo);
		final AbstractOrderEntryModel cartEntry = transportEntries.stream()
				.findFirst().get();
		addBundleToCartRequestData.setOpenTicket(cartEntry.getTravelOrderEntryInfo().isOpenTicket());
		final BCFTravellerDataPerJourney bcfTravellerDataPerJourney = bcfTravellersData.getTravellerDataPerJourney().stream()
				.filter(travellerData -> travellerData.getJourneyReferenceNumber() == journeyRefNo).findFirst().get();
		if (odRefNo == 0)
		{
			addBundleToCartRequestData
					.setPassengerTravellers(getPassengerTraveller(bcfTravellerDataPerJourney.getOutboundTravellerData().stream()
							.filter(travellerData -> travellerData.getTravellerType().equals(TravellerType.PASSENGER.getCode()))
							.collect(Collectors.toList()), passengerTypeDataList));
			final TravellerData travellerData = bcfTravellerDataPerJourney.getOutboundTravellerData().stream()
					.filter(traveller -> traveller.getTravellerType().equals(TravellerType.VEHICLE.getCode()))
					.findFirst().orElse(null);
			if (Objects.nonNull(travellerData))
			{
				addBundleToCartRequestData.setVehicleTraveller(getVehicleTraveller(travellerData, vehicleTypeDataList));
			}
		}
		else
		{
			addBundleToCartRequestData
					.setPassengerTravellers(getPassengerTraveller(bcfTravellerDataPerJourney.getInboundTravellerData().stream()
							.filter(travellerData -> travellerData.getTravellerType().equals(TravellerType.PASSENGER.getCode()))
							.collect(Collectors.toList()), passengerTypeDataList));
			final TravellerData travellerData = bcfTravellerDataPerJourney.getOutboundTravellerData().stream()
					.filter(traveller -> traveller.getTravellerType().equals(TravellerType.VEHICLE.getCode()))
					.findFirst().orElse(null);
			if (Objects.nonNull(travellerData))
			{
				addBundleToCartRequestData.setVehicleTraveller(getVehicleTraveller(travellerData, vehicleTypeDataList));
			}
		}

		populateQuantityData(passengerTypeDataList, vehicleTypeDataList, passengerTypeQuantityDataList,
				vehicleTypeQuantityDataList);

		final List<TravellerModel> travellers = transportEntries.stream().map(AbstractOrderEntryModel::getTravelOrderEntryInfo)
				.flatMap(travelOrderEntryInfoModel -> travelOrderEntryInfoModel.getTravellers().stream()).collect(
						Collectors.toList());
		final List<TravellerData> travellerDataList = getTravellerDataConverter().convertAll(travellers);
		final Supplier<Stream<VehicleInformationData>> vehicleInfoStreamSupplier = () -> travellerDataList.stream()
				.filter(travellerData -> TravellerType.VEHICLE.getCode().equals(travellerData.getTravellerType()))
				.map(traveller -> (VehicleInformationData) traveller.getTravellerInfo());
		if (vehicleInfoStreamSupplier.get().findFirst().isPresent())
		{
			final VehicleInformationData vehicleInformationData = vehicleInfoStreamSupplier.get().findFirst().get();
			vehicleTypeQuantityDataList.forEach(vehicle -> {
				vehicle.setLength((int) vehicleInformationData.getLength());
				vehicle.setHeight((int) vehicleInformationData.getHeight());
				vehicle.setWidth((int) vehicleInformationData.getWidth());
				vehicle.setWeight((int) vehicleInformationData.getWeight());
			});
		}
		addBundleToCartRequestData.setPassengerTypes(passengerTypeQuantityDataList);
		addBundleToCartRequestData.setVehicleTypes(vehicleTypeQuantityDataList);

		addBundleToCartRequestData.setSelectedJourneyRefNumber(journeyRefNo);
		addBundleToCartRequestData.setSelectedOdRefNumber(odRefNo);
		final BcfAddBundleToCartData addBundleToCartData = new BcfAddBundleToCartData();
		addBundleToCartData.setOriginDestinationRefNumber(odRefNo);
		addBundleToCartData.setTravelRouteCode(cartEntry.getTravelOrderEntryInfo().getTravelRoute().getCode());
		final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();
		cartEntry.getTravelOrderEntryInfo().getTransportOfferings().forEach(transportOfferingModel -> {
			final TransportOfferingData transportOfferingData = new TransportOfferingData();
			transportOfferingData.setDepartureTime(transportOfferingModel.getDepartureTime());
			transportOfferingData.setArrivalTime(transportOfferingModel.getArrivalTime());
			transportOfferingDataList.add(transportOfferingData);
		});

		addBundleToCartData.setTransportOfferingDatas(transportOfferingDataList);

		addBundleToCartRequestData.setAddBundleToCartData(Arrays.asList(addBundleToCartData));
		addBundleToCartRequestData.setBundleType(cartEntry.getBundleTemplate().getType().getCode());
		addBundleToCartRequestData.setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel());

		addBundleToCartRequestData.setHoldTimeInSeconds(getHoldTimeInSeconds(addBundleToCartRequestData));

		if (bcfTravelCartFacade.isAmendmentCart())
		{
			addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.MODIFICATION_JOURNEY);
		}
		return addBundleToCartRequestData;
	}

	private int getHoldTimeInSeconds(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		return bcfTravelCommerceStockService.getStockHoldTimeInMs(addBundleToCartRequestData.getSalesApplication()) / 1000;
	}

	@Override
	public void populateQuantityData(final List<PassengerTypeData> passengerTypeDataList,
			final List<VehicleTypeData> vehicleTypeDataList, final List<PassengerTypeQuantityData> passengerTypeQuantityDataList,
			final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList)
	{
		final Map<String, List<PassengerTypeData>> passengerTypeDataCodeMap = passengerTypeDataList.stream()
				.collect(Collectors.groupingBy(passengerType -> passengerType.getCode()));

		final Map<String, List<VehicleTypeData>> vehicleTypeDataCodeMap = vehicleTypeDataList.stream()
				.collect(Collectors.groupingBy(vehicleType -> vehicleType.getCode()));

		for (final Map.Entry<String, List<PassengerTypeData>> mapEntry : passengerTypeDataCodeMap.entrySet())
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(mapEntry.getValue().stream().findFirst().get());
			passengerTypeQuantityData.setQuantity(mapEntry.getValue().size());
			passengerTypeQuantityDataList.add(passengerTypeQuantityData);
		}

		for (final Map.Entry<String, List<VehicleTypeData>> mapEntry : vehicleTypeDataCodeMap.entrySet())
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			vehicleTypeQuantityData.setVehicleType(mapEntry.getValue().stream().findFirst().get());
			vehicleTypeQuantityData.setQty(mapEntry.getValue().size());
			vehicleTypeQuantityDataList.add(vehicleTypeQuantityData);
		}
	}

	@Override
	public List<PassengerRequestData> getPassengerTraveller(final List<TravellerData> travellerData,
			final List<PassengerTypeData> passengerTypeDataList)
	{
		final List<PassengerRequestData> passengerTravellerRequestDataList = new ArrayList<>();
		travellerData.forEach(traveller -> {
			final PassengerInformationData passenger = (PassengerInformationData) traveller.getTravellerInfo();
			final PassengerRequestData passengerRequestData = new PassengerRequestData();
			passengerRequestData.setCustomerNumber(passenger.getSavedTravellerUId());
			passengerRequestData.setFirstName(passenger.getFirstName());
			passengerRequestData.setLastName(passenger.getSurname());
			passengerRequestData.setPhone(passenger.getPhoneNumber());
			passengerRequestData.setEmailAddress(passenger.getEmail());
			passengerRequestData.setPassengerCode(passenger.getPassengerType().getBcfCode());
			passengerTypeDataList.add(passenger.getPassengerType());
			passengerTravellerRequestDataList.add(passengerRequestData);
		});
		return passengerTravellerRequestDataList;
	}

	@Override
	public VehicleRequestData getVehicleTraveller(final TravellerData traveller,
			final List<VehicleTypeData> vehicleTypeDataList)
	{
		final VehicleRequestData vehicleRequestData = new VehicleRequestData();
		final VehicleInformationData vehicleTraveller = (VehicleInformationData) traveller.getTravellerInfo();
		if (Objects.nonNull(vehicleTraveller) && StringUtils.isNotBlank(vehicleTraveller.getLicensePlateNumber()))
		{
			vehicleRequestData.setVehicleLicense(vehicleTraveller.getLicensePlateNumber());
			vehicleRequestData.setVehicleNationality(vehicleTraveller.getLicensePlateCountry());
			vehicleRequestData.setVehicleProvince(vehicleTraveller.getLicensePlateProvince());
		}
		vehicleTypeDataList.add(vehicleTraveller.getVehicleType());
		return vehicleRequestData;
	}


	@Override
	public void getUserSelectedSpecialServiceData(final BCFTravellersData bcfTravellersData,
			final List<AccessibilityRequestData> accessibilityRequestDataList,
			final List<AccessibilityRequestData> inboundAccessibilityRequestDataList)
	{

		if (CollectionUtils.isNotEmpty(bcfTravellersData.getTravellerDataPerJourney()))
		{
			for (final BCFTravellerDataPerJourney bCFTravellerDataPerJourney : bcfTravellersData.getTravellerDataPerJourney())
			{
				if (CollectionUtils.isNotEmpty(bCFTravellerDataPerJourney.getOutboundTravellerData()) && CollectionUtils
						.isNotEmpty(accessibilityRequestDataList))
					for (final TravellerData travellerData : bCFTravellerDataPerJourney.getOutboundTravellerData())
					{
						final AccessibilityRequestData accessibilityRequestData = accessibilityRequestDataList.stream()
								.filter(accessibilityRequest -> travellerData.getUid().equals(accessibilityRequest.getPassengerUid()))
								.findAny().orElse(null);


						setSpecialRequestDetails(travellerData, accessibilityRequestData);
					}

				if (CollectionUtils.isNotEmpty(bCFTravellerDataPerJourney.getInboundTravellerData()) && CollectionUtils
						.isNotEmpty(inboundAccessibilityRequestDataList))
				{
					for (final TravellerData travellerData : bCFTravellerDataPerJourney.getInboundTravellerData())
					{

						final AccessibilityRequestData accessibilityRequestData = inboundAccessibilityRequestDataList.stream()
								.filter(accessibilityRequest -> travellerData.getUid().equals(accessibilityRequest.getPassengerUid()))
								.findAny().orElse(null);


						setSpecialRequestDetails(travellerData, accessibilityRequestData);

					}
				}
			}

		}
	}

	private void setSpecialRequestDetails(final TravellerData travellerData,
			final AccessibilityRequestData accessibilityRequestData)
	{
		if (accessibilityRequestData != null)
		{

			SpecialRequestDetailData specialRequestDetailData = travellerData.getSpecialRequestDetail();
			if (specialRequestDetailData == null)
			{
				specialRequestDetailData = new SpecialRequestDetailData();
			}
			final List<SpecialServiceRequestData> specialServiceRequestDatas = new ArrayList<>();
			if (CollectionUtils
					.isNotEmpty(accessibilityRequestData.getSpecialServiceRequestDataList()))
			{
				specialServiceRequestDatas.addAll(accessibilityRequestData.getSpecialServiceRequestDataList().stream()
						.filter(SpecialServiceRequestData -> SpecialServiceRequestData.isSelection()).collect(
								Collectors.toList()));
			}
			if (accessibilityRequestData.getSelection() != null && CollectionUtils
					.isNotEmpty(accessibilityRequestData.getAncillaryRequestDataList()))
			{
				final List<ProductData> productDatas = accessibilityRequestData.getAncillaryRequestDataList().stream()
						.filter(
								ancillaryRequestData -> accessibilityRequestData.getSelection()
										.contains(ancillaryRequestData.getCode())).collect(Collectors.toList());

				for (final ProductData productData : productDatas)
				{
					final SpecialServiceRequestData specialServiceRequestData = new SpecialServiceRequestData();
					specialServiceRequestData.setCode(productData.getCode());
					specialServiceRequestData.setName(productData.getName());
					specialServiceRequestDatas.add(specialServiceRequestData);
				}
			}
			specialRequestDetailData.setSpecialServiceRequests(specialServiceRequestDatas);
			travellerData.setSpecialRequestDetail(specialRequestDetailData);


		}
	}



	@Override
	public List<TravellerData> getTravellersForCartEntries()
	{
		if (!this.getCartService().hasSessionCart())
		{
			return Collections.emptyList();
		}
		else
		{
			final List<TravellerModel> travellers = this.getTravellerService()
					.getTravellers(StreamUtil.safeStream(bcfTravelCartService.getOrCreateSessionCart().getEntries())
							.filter(entry -> entry.getActive()).collect(Collectors.toList()));
			return Converters.convertAll(travellers, this.getTravellerDataConverter());
		}
	}

	@Override
	public List<TravellerData> getTravellersForCartEntries(final int journeyRefNo, final int odRefNumber)
	{
		if (!this.getCartService().hasSessionCart())
		{
			return Collections.emptyList();
		}
		else
		{
			final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getOrCreateSessionCart().getEntries().stream().filter(
					entry -> entry.getTravelOrderEntryInfo() != null
							&& entry.getJourneyReferenceNumber() == journeyRefNo &&
							entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNumber).collect(Collectors.toList());
			final List<TravellerModel> travellers = this.getTravellerService()
					.getTravellers(entries);
			return Converters.convertAll(travellers, this.getTravellerDataConverter());
		}
	}

	protected MakeBookingIntegrationFacade getMakeBookingIntegrationFacade()
	{
		return makeBookingIntegrationFacade;
	}

	public ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}

	@Required
	public void setMakeBookingIntegrationFacade(final MakeBookingIntegrationFacade makeBookingIntegrationFacade)
	{
		this.makeBookingIntegrationFacade = makeBookingIntegrationFacade;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
