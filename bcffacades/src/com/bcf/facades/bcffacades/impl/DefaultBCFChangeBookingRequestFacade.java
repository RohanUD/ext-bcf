/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.ticket.enums.CsInterventionType;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketBusinessService;
import de.hybris.platform.ticket.service.TicketService;
import de.hybris.platform.ticketsystem.data.CsTicketParameter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.facades.bcffacades.BCFChangeBookingRequestFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.change.booking.CsTicketData;


public class DefaultBCFChangeBookingRequestFacade implements BCFChangeBookingRequestFacade
{

	private ModelService modelService;

	private TicketBusinessService ticketBusinessService;

	private TicketService ticketService;

	private BcfBookingFacade bcfBookingFacade;

	private Converter<CsTicketModel, CsTicketData> csTicketConverter;

	public Converter<CsTicketModel, CsTicketData> getCsTicketConverter()
	{
		return csTicketConverter;
	}

	
	public BcfBookingFacade getBcfBookingFacade()
	{
		return bcfBookingFacade;
	}

	public void setBcfBookingFacade(final BcfBookingFacade bcfBookingFacade)
	{
		this.bcfBookingFacade = bcfBookingFacade;
	}

	public void setCsTicketConverter(
			final Converter<CsTicketModel, CsTicketData> csTicketConverter)
	{
		this.csTicketConverter = csTicketConverter;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public TicketBusinessService getTicketBusinessService()
	{
		return ticketBusinessService;
	}

	public void setTicketBusinessService(final TicketBusinessService ticketBusinessService)
	{
		this.ticketBusinessService = ticketBusinessService;
	}

	public TicketService getTicketService()
	{
		return ticketService;
	}

	public void setTicketService(final TicketService ticketService)
	{
		this.ticketService = ticketService;
	}

	@Override
	public void createTicketForChangeBooking(final String name, final String email, final String contactNumber,
			final String description,
			final String title, final String orderCode)
	{
		final AbstractOrderModel order = bcfBookingFacade.getOrderFromBookingReferenceCode(orderCode);
		final CsTicketModel ticket = ticketBusinessService.createTicket(createCsTicketParameter(description,title,order));
		final CsCustomerEventModel csCustomerEvent = (CsCustomerEventModel) (ticketService.getEventsForTicket(ticket).get(0));
		csCustomerEvent.setContactNumber(contactNumber);
		csCustomerEvent.setName(name);
		csCustomerEvent.setEmail(email);
		modelService.save(csCustomerEvent);

	}

	@Override
	public List<CsTicketData> getAllOpenTickets()
	{
		List<CsTicketModel> tickets = ticketService.getTicketsForCategory(CsTicketCategory.CHANGEBOOKING);
		if(CollectionUtils.isNotEmpty(tickets)){

			final List<CsTicketState> states = Arrays.asList(CsTicketState.NEW, CsTicketState.OPEN,
					CsTicketState.INPROGRESS);
			tickets = tickets.stream().filter(ticket -> states.contains(ticket.getState())).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(tickets))
			{
				return Converters.convertAll(tickets, csTicketConverter);
			}
		}

		return Collections.EMPTY_LIST;

	}

	@Override
	public void changeTicketStatus(final String ticketId, final CsTicketState csTicketState)
	{

		final CsTicketModel ticket = ticketService.getTicketForTicketId(ticketId);
		ticket.setState(csTicketState);
		modelService.save(ticket);
	}

	private CsTicketParameter createCsTicketParameter(final String description, final String title, final AbstractOrderModel order)
	{
		final CsTicketParameter ticketParameter = new CsTicketParameter();
		ticketParameter.setPriority(CsTicketPriority.HIGH);
		ticketParameter.setCategory(CsTicketCategory.CHANGEBOOKING);
		ticketParameter.setInterventionType(CsInterventionType.TICKETMESSAGE);
		ticketParameter.setCreationNotes(description);
		ticketParameter.setAssociatedTo(order);
		ticketParameter.setCustomer(order.getUser());
		ticketParameter.setHeadline(title);
		return ticketParameter;
	}



}
