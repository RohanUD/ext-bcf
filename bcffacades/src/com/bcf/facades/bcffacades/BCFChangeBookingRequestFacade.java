/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.bcffacades;

import de.hybris.platform.ticket.enums.CsTicketState;
import java.util.List;
import com.bcf.facades.change.booking.CsTicketData;


public interface BCFChangeBookingRequestFacade
{
	void createTicketForChangeBooking(final String name, final String email, String contactNumber, final String description,
			String title, String orderCode);


	List<CsTicketData> getAllOpenTickets();

	void changeTicketStatus(String ticketId, CsTicketState csTicketState);

}
