/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodationoffering.search;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.travelservices.search.facetdata.AccommodationOfferingSearchPageData;
import java.util.Map;



public interface AccommodationOfferingSolrSearchFacade<ITEM extends AccommodationOfferingSearchPageData>
{
	AccommodationOfferingSearchPageData<SearchStateData, ITEM> searchAccommodations(SearchData searchData,
			PageableData pageableData);

	AccommodationOfferingSearchPageData<SearchStateData, ITEM> searchHotels(Map<String, String> filterTerms,
			PageableData pageableData);

}
