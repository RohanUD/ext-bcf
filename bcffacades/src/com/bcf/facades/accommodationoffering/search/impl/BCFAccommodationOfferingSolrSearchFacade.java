/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodationoffering.search.impl;

import de.hybris.platform.commercefacades.search.data.SearchFilterQueryData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.search.facetdata.AccommodationOfferingSearchPageData;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.accommodationOffering.solrfacetsearch.AccommodationOfferingSearchService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.accommodationoffering.search.AccommodationOfferingSolrSearchFacade;


public class BCFAccommodationOfferingSolrSearchFacade<ITEM extends AccommodationOfferingSearchPageData>
		implements AccommodationOfferingSolrSearchFacade
{

	private ThreadContextService threadContextService;
	private Converter<SearchQueryData, SolrSearchQueryData> solrSearchQueryDecoder;
	private AccommodationOfferingSearchService accommodationOfferingSearchService;
	private Converter<AccommodationOfferingSearchPageData<SolrSearchQueryData, SearchResultValueData>,
			AccommodationOfferingSearchPageData<SearchStateData, ITEM>> accommodationOfferingSearchPageConverter;
	private Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder;
	private static final String QUERY_ALL = "*:*";

	@Override
	public AccommodationOfferingSearchPageData searchAccommodations(final SearchData searchData, final PageableData pageableData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<AccommodationOfferingSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public AccommodationOfferingSearchPageData<SearchStateData, ITEM> execute()
					{
						final AccommodationOfferingSearchPageData accommodationOfferingSearchPageData = getAccommodationOfferingSearchService()
								.doSearch(decodeState(searchData), pageableData);
						return getAccommodationOfferingSearchPageConverter().convert(accommodationOfferingSearchPageData);
					}
				});
	}

	@Override
	public AccommodationOfferingSearchPageData searchHotels(final Map filterTerms,
			final PageableData pageableData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<AccommodationOfferingSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public AccommodationOfferingSearchPageData<SearchStateData, ITEM> execute()
					{
						final AccommodationOfferingSearchPageData accommodationOfferingSearchPageData = getAccommodationOfferingSearchService()
								.doSearch(decodeState(buildSearchStateData(filterTerms), pageableData.getSort()), pageableData);
						return getAccommodationOfferingSearchPageConverter().convert(accommodationOfferingSearchPageData);
					}
				});
	}

	public SearchStateData buildSearchStateData(final Map<String, Set<String>> filterTerms)
	{
		final List<SearchFilterQueryData> searchFilterQueryDataList = StreamUtil.safeStream(filterTerms.keySet())
				.map(filterKey -> getSearchFilterQueryDataForFilterKey(filterKey, filterTerms))
				.collect(Collectors.toList());
		final SearchQueryData query = new SearchQueryData();
		query.setValue(QUERY_ALL);
		query.setFilterQueries(searchFilterQueryDataList);
		final SearchStateData searchStateData = new SearchStateData();
		searchStateData.setQuery(query);
		return searchStateData;
	}

	private SearchFilterQueryData getSearchFilterQueryDataForFilterKey(final String filterKey,
			final Map<String, Set<String>> filterTerms)
	{
		final SearchFilterQueryData searchFilterQueryData = new SearchFilterQueryData();
		searchFilterQueryData.setKey(filterKey);
		searchFilterQueryData.setValues(filterTerms.get(filterKey));
		return searchFilterQueryData;
	}

	protected SolrSearchQueryData decodeState(final SearchStateData searchState, final String sort)
	{
		final SolrSearchQueryData searchQueryData = solrSearchQueryDecoder.convert(searchState.getQuery());
		searchQueryData.setSort(sort);
		return searchQueryData;
	}

	protected SolrSearchQueryData decodeState(final SearchData searchData)
	{
		final SolrSearchQueryData searchQueryData = this.getSolrTravelSearchQueryDecoder().convert(searchData);
		searchQueryData.setSort(searchData.getSort());
		return searchQueryData;
	}

	public ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	@Required
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

	public Converter<SearchQueryData, SolrSearchQueryData> getSolrSearchQueryDecoder()
	{
		return solrSearchQueryDecoder;
	}

	@Required
	public void setSolrSearchQueryDecoder(
			final Converter<SearchQueryData, SolrSearchQueryData> solrSearchQueryDecoder)
	{
		this.solrSearchQueryDecoder = solrSearchQueryDecoder;
	}

	public AccommodationOfferingSearchService getAccommodationOfferingSearchService()
	{
		return accommodationOfferingSearchService;
	}

	@Required
	public void setAccommodationOfferingSearchService(
			final AccommodationOfferingSearchService accommodationOfferingSearchService)
	{
		this.accommodationOfferingSearchService = accommodationOfferingSearchService;
	}

	public Converter<AccommodationOfferingSearchPageData<SolrSearchQueryData, SearchResultValueData>, AccommodationOfferingSearchPageData<SearchStateData, ITEM>> getAccommodationOfferingSearchPageConverter()
	{
		return accommodationOfferingSearchPageConverter;
	}

	@Required
	public void setAccommodationOfferingSearchPageConverter(
			final Converter<AccommodationOfferingSearchPageData<SolrSearchQueryData, SearchResultValueData>, AccommodationOfferingSearchPageData<SearchStateData, ITEM>> accommodationOfferingSearchPageConverter)
	{
		this.accommodationOfferingSearchPageConverter = accommodationOfferingSearchPageConverter;
	}

	public Converter<SearchData, SolrSearchQueryData> getSolrTravelSearchQueryDecoder()
	{
		return solrTravelSearchQueryDecoder;
	}

	@Required
	public void setSolrTravelSearchQueryDecoder(final Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder)
	{
		this.solrTravelSearchQueryDecoder = solrTravelSearchQueryDecoder;
	}
}
