/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodationoffering.search.populator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingGalleryService;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.accommodation.reviews.CustomerReviewData;
import com.bcf.facades.accommodationoffering.search.response.data.AccommodationOfferingResponseData;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class AccommodationOfferingSearchResultPopulator
		implements Populator<SearchResultValueData, AccommodationOfferingResponseData>
{
	private static final Logger LOG = Logger.getLogger(AccommodationOfferingSearchResultPopulator.class);
	private static final String HOTEL_LISTING = "Hotel-Listing";

	private TripAdvisorService tripAdvisorService;
	private Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter;
	private CatalogVersionService catalogVersionService;
	private AccommodationOfferingGalleryService accommodationOfferingGalleryService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private AccommodationOfferingService accommodationOfferingService;

	@Override
	public void populate(final SearchResultValueData source, final AccommodationOfferingResponseData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		final AccommodationOfferingModel accommodationOffering = getAccommodationOfferingService()
				.getAccommodationOffering(this.getValue(source, "code"));

		target.setCode(this.getValue(source, "code"));
		target.setName(this.getValue(source, "name"));
		target.setDescription(this.getValue(source, "description"));
		getImageData(target, accommodationOffering.getGalleryCode());
		target.setPropertyCategories(this.getValue(source, "propertyCategories"));
		target.setNextUrl(this.getValue(source, "nextUrl"));
		target.setLocationName(this.getValue(source, "locationName"));
		final String reviewLocationId = this.getValue(source, "reviewLocationId");
		try
		{
			final TripAdvisorResponseData reviewInformation = getTripAdvisorService().getReviewInformation(reviewLocationId);
			target.setCustomerReviewData(getCustomerReviewDataConverter().convert(reviewInformation));
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage());
		}
	}

	protected void getImageData(final AccommodationOfferingResponseData target,
			final String accommodationOfferingGalleryCode)
	{
		if (StringUtils.isEmpty(accommodationOfferingGalleryCode))
		{
			LOG.error("No accommodationOfferingGallery is present");
		}
		else
		{
			if (StringUtils.isNotEmpty(accommodationOfferingGalleryCode))
			{
				final CatalogVersionModel catalogVersion = getCatalogVersionService()
						.getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG,
								BcfCoreConstants.CATALOG_VERSION);
				final AccommodationOfferingGalleryModel accommodationOfferingGallery = getAccommodationOfferingGalleryService()
						.getAccommodationOfferingGallery(accommodationOfferingGalleryCode, catalogVersion);
				final List<MediaContainerModel> mediaContainer = accommodationOfferingGallery.getGallery();
				final List<ImageData> images = new ArrayList<>();
				for (final MediaContainerModel media : mediaContainer)
				{
					final ImageData imageData = new ImageData();
					imageData.setUrl(getBcfResponsiveMediaStrategy().getDataMediaForFormat(media, HOTEL_LISTING));
					imageData.setAltText(media.getMedias().stream().findFirst().get().getAltText());
					images.add(imageData);
				}
				target.setImages(images);
			}
		}
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		return (T) source.getValues().get(propertyName);
	}

	public TripAdvisorService getTripAdvisorService()
	{
		return tripAdvisorService;
	}

	@Required
	public void setTripAdvisorService(final TripAdvisorService tripAdvisorService)
	{
		this.tripAdvisorService = tripAdvisorService;
	}

	public Converter<TripAdvisorResponseData, CustomerReviewData> getCustomerReviewDataConverter()
	{
		return customerReviewDataConverter;
	}

	@Required
	public void setCustomerReviewDataConverter(
			final Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter)
	{
		this.customerReviewDataConverter = customerReviewDataConverter;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	public AccommodationOfferingGalleryService getAccommodationOfferingGalleryService()
	{
		return accommodationOfferingGalleryService;
	}

	@Required
	public void setAccommodationOfferingGalleryService(
			final AccommodationOfferingGalleryService accommodationOfferingGalleryService)
	{
		this.accommodationOfferingGalleryService = accommodationOfferingGalleryService;
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}
}
