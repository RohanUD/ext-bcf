/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.traveladvisory;

import de.hybris.platform.commercefacades.travel.TravelAdvisoryData;
import java.util.List;
import java.util.Optional;


public interface TravelAdvisoryFacade
{
	List<TravelAdvisoryData> findActiveTravelAdvisories();

	List<TravelAdvisoryData> findActiveTravelAdvisoriesForHomePageByRouteRegion(Optional<String> departureLocation,
			Optional<String> arrivalLocation);

	List<TravelAdvisoryData> findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion();

	List<TravelAdvisoryData> findActiveTravelAdvisoriesForHomePageWithoutRouteRegion();

	List<TravelAdvisoryData> findActiveTravelAdvisoriesForOtherPagesByRouteRegion(Optional<String> departureLocation,
			Optional<String> arrivalLocation);

	Optional<TravelAdvisoryData> getTravelAdvisoryByCode(String code);

	String getRoutesAtGlanceContent();

	Optional<TravelAdvisoryData> findHomePageTakeOverAdvisory();
}
