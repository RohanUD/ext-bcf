/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.traveladvisory.impl;

import de.hybris.platform.commercefacades.travel.TravelAdvisoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.core.services.traveladvisory.impl.TravelAdvisoryService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.traveladvisory.TravelAdvisoryFacade;


public class TravelAdvisoryFacadeImpl implements TravelAdvisoryFacade
{
	private TravelAdvisoryService travelAdvisoryService;
	private Converter<TravelAdvisoryModel, TravelAdvisoryData> travelAdvisoryConverter;

	@Override
	public List<TravelAdvisoryData> findActiveTravelAdvisories()
	{
		return getTravelAdvisoryDataList(getTravelAdvisoryService().findActiveTravelAdvisories());
	}

	@Override
	public List<TravelAdvisoryData> findActiveTravelAdvisoriesForHomePageByRouteRegion(final Optional<String> departureLocation,
			final Optional<String> arrivalLocation)
	{
		return getTravelAdvisoryDataList(
				getTravelAdvisoryService().findActiveTravelAdvisoriesForHomePageByRouteRegion(departureLocation, arrivalLocation));
	}

	@Override
	public List<TravelAdvisoryData> findActiveTravelAdvisoriesForOtherPagesByRouteRegion(final Optional<String> departureLocation,
			final Optional<String> arrivalLocation)
	{
		return getTravelAdvisoryDataList(
				getTravelAdvisoryService().findActiveTravelAdvisoriesForOtherPagesByRouteRegion(departureLocation, arrivalLocation));
	}

	@Override
	public List<TravelAdvisoryData> findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion()
	{
		return getTravelAdvisoryDataList(getTravelAdvisoryService().findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion());
	}

	@Override
	public List<TravelAdvisoryData> findActiveTravelAdvisoriesForHomePageWithoutRouteRegion()
	{
		return getTravelAdvisoryDataList(getTravelAdvisoryService().findActiveTravelAdvisoriesForHomePageWithoutRouteRegion());
	}

	@Override
	public Optional<TravelAdvisoryData> findHomePageTakeOverAdvisory()
	{
		return getTravelAdvisoryService().findHomePageTakeOverAdvisory().map(getTravelAdvisoryConverter()::convert);
	}

	@Override
	public Optional<TravelAdvisoryData> getTravelAdvisoryByCode(final String code)
	{
		return getTravelAdvisoryService().getTravelAdvisoryByCode(code).map(getTravelAdvisoryConverter()::convert);
	}

	@Override
	public String getRoutesAtGlanceContent()
	{
		return getTravelAdvisoryService().getRoutesAtGlanceContent();
	}

	private List<TravelAdvisoryData> getTravelAdvisoryDataList(final List<TravelAdvisoryModel> activeTravelAdvisories)
	{
		return StreamUtil.safeStream(activeTravelAdvisories).map(getTravelAdvisoryConverter()::convert)
				.collect(Collectors.toList());
	}

	public TravelAdvisoryService getTravelAdvisoryService()
	{
		return travelAdvisoryService;
	}

	@Required
	public void setTravelAdvisoryService(final TravelAdvisoryService travelAdvisoryService)
	{
		this.travelAdvisoryService = travelAdvisoryService;
	}

	public Converter<TravelAdvisoryModel, TravelAdvisoryData> getTravelAdvisoryConverter()
	{
		return travelAdvisoryConverter;
	}

	@Required
	public void setTravelAdvisoryConverter(final Converter<TravelAdvisoryModel, TravelAdvisoryData> travelAdvisoryConverter)
	{
		this.travelAdvisoryConverter = travelAdvisoryConverter;
	}
}
