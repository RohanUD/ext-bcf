/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies;


import de.hybris.platform.commercefacades.product.data.PriceData;
import java.math.BigDecimal;
import java.util.List;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.facades.order.cancel.CancelBookingResponseData;


public interface BookingCancellationStrategy
{

	BcfChangeCancellationVacationFeeData adjustCancellationFee(BigDecimal totalPaid,
			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData);

	PriceData getRefundTotal(BigDecimal totalPaid, int digits, String isoCode,
			BigDecimal totalCancelFee);


	CancelBookingResponseData cancelCompleteOrder(final String orderCode,
			List<RefundTransactionInfoData> refundTransactionInfoDatas) throws Exception;

}
