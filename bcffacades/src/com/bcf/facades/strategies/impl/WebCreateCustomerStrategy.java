/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.core.util.BCFCreateCustomerUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.strategies.CreateCustomerStrategy;
import com.bcf.facades.subscription.converters.populator.SignupReversePopulator;
import com.bcf.integration.activedirectory.service.BCFCustomerLDAPService;
import com.bcf.integration.data.CreateCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class WebCreateCustomerStrategy implements CreateCustomerStrategy
{
	private static final Logger LOG = Logger.getLogger(WebCreateCustomerStrategy.class);

	private BCFCustomerLDAPService customerLDAPService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private SignupReversePopulator signupReversePopulator;
	private CustomerAccountService customerAccountService;
	private CommonI18NService commonI18NService;
	private BusinessProcessService businessProcessService;
	private ModelService modelService;
	private BaseStoreService baseStoreService;
	private UserService userService;
	private CMSSiteService cmsSiteService;
	private BCFCreateCustomerUtil bcfCreateCustomerUtil;

	@Override
	public void createCustomer(final RegisterData registerData) throws IntegrationException, DuplicateUidException
	{
		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		final String webUserGroup;

		CreateCustomerResponseDTO response = null;
		response = getCustomerLDAPService().registerAtLDAP(registerData);

		if (response == null || response.getCustomer() == null)
		{
			LOG.error("Response received from registerAtLDAP() service is null");
			throw new IllegalArgumentException();
		}

		newCustomer.setAccountValidationKey(response.getCustomer().getAccountValidationKey());
		newCustomer.setAccountActivationKeyExpiryTime(DateUtils.addHours(new Date(), Integer.parseInt(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCOUNT_ACTIVATION_EXPIRY_TIME))));
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		webUserGroup = getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultWebUserGroup");
		newCustomer.setRegistrationSource(RegistrationSource.WEB);
		getSignupReversePopulator().populate(registerData, newCustomer);
		newCustomer.setGroups(getBcfCreateCustomerUtil().assignUserGroup(webUserGroup));
		getBcfCreateCustomerUtil().setUidForRegister(registerData.getLogin(), newCustomer);
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
		sendEmailProcess(newCustomer);
	}

	private void sendEmailProcess(final CustomerModel newCustomer)
	{
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = getBusinessProcessService()
				.createProcess(
						"customerRegistrationEmailProcess-" + newCustomer.getUid() + "-" + System.currentTimeMillis(),
						"customerRegistrationEmailProcess");
		executeBusinessProcess(storeFrontCustomerProcessModel, newCustomer, siteModel);
	}

	private void executeBusinessProcess(final StoreFrontCustomerProcessModel businessProcessModel,
			final CustomerModel customerModel, final CMSSiteModel siteModel)
	{
		businessProcessModel.setSite(siteModel);
		businessProcessModel.setCustomer(customerModel);
		businessProcessModel.setLanguage(getCommonI18NService().getCurrentLanguage());
		businessProcessModel.setCurrency(getCommonI18NService().getCurrentCurrency());
		businessProcessModel.setStore(getBaseStoreService().getCurrentBaseStore());
		getModelService().save(businessProcessModel);
		getBusinessProcessService().startProcess(businessProcessModel);
	}

	public BCFCustomerLDAPService getCustomerLDAPService()
	{
		return customerLDAPService;
	}

	@Required
	public void setCustomerLDAPService(final BCFCustomerLDAPService customerLDAPService)
	{
		this.customerLDAPService = customerLDAPService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public SignupReversePopulator getSignupReversePopulator()
	{
		return signupReversePopulator;
	}

	@Required
	public void setSignupReversePopulator(final SignupReversePopulator signupReversePopulator)
	{
		this.signupReversePopulator = signupReversePopulator;
	}

	public CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	public BCFCreateCustomerUtil getBcfCreateCustomerUtil()
	{
		return bcfCreateCustomerUtil;
	}

	@Required
	public void setBcfCreateCustomerUtil(final BCFCreateCustomerUtil bcfCreateCustomerUtil)
	{
		this.bcfCreateCustomerUtil = bcfCreateCustomerUtil;
	}
}
