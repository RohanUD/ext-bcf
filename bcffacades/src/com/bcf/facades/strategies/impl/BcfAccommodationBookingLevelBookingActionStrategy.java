/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.AccommodationBookingActionData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.travelfacades.booking.action.strategies.AccommodationBookingActionStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;


public class BcfAccommodationBookingLevelBookingActionStrategy implements AccommodationBookingActionStrategy
{
	private Map<String, String> accommodationBookingActionTypeUrlMap;
	private static final String BOOKING_REFERENCE_PLACEHOLDER = "\\{bookingReference}";

	@Override
	public void applyStrategy(List<AccommodationBookingActionData> bookingActionDataList, ActionTypeOption actionType,
			AccommodationReservationData reservationData)
	{
		AccommodationBookingActionData bookingActionData = new AccommodationBookingActionData();
		bookingActionData.setActionType(actionType);
		bookingActionData.setAlternativeMessages(new ArrayList());
		bookingActionData.setEnabled(true);
		this.populateUrl(bookingActionData, reservationData);
		bookingActionDataList.add(bookingActionData);
	}


	public void populateUrl(AccommodationBookingActionData bookingActionData, AccommodationReservationData accommodationReservationData) {
		String url = this.getAccommodationBookingActionTypeUrlMap().get(bookingActionData.getActionType().toString());
		url = url.replaceAll(BOOKING_REFERENCE_PLACEHOLDER, accommodationReservationData.getCode());
		if(accommodationReservationData.getJourneyRef()!=null){
			url = url.replaceAll("\\{journeyRef}", accommodationReservationData.getJourneyRef());
		}
		bookingActionData.setActionUrl(url);
	}

	protected Map<String, String> getAccommodationBookingActionTypeUrlMap() {
		return this.accommodationBookingActionTypeUrlMap;
	}

	@Required
	public void setAccommodationBookingActionTypeUrlMap(Map<String, String> accommodationBookingActionTypeUrlMap) {
		this.accommodationBookingActionTypeUrlMap = accommodationBookingActionTypeUrlMap;
	}
}
