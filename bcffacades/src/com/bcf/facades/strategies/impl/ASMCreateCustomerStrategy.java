/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.facades.strategies.CreateCustomerStrategy;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class ASMCreateCustomerStrategy implements CreateCustomerStrategy
{
	private static final Logger LOG = Logger.getLogger(ASMCreateCustomerStrategy.class);

	private CommonI18NService commonI18NService;
	private CustomerNameStrategy customerNameStrategy;
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private UserService userService;
	private CMSSiteService cmsSiteService;
	private BaseStoreService baseStoreService;
	private CRMUpdateCustomerService crmUpdateCustomerService;

	@Override
	public void createCustomer(final RegisterData registerData) throws IntegrationException
	{
		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		final String webUserGroup;

		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}
		newCustomer.setCountry(registerData.getCountry());
		newCustomer.setAccountType(AccountType.valueOf(registerData.getAccountType()));
		newCustomer.setPhoneNo(registerData.getPhoneNo());
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		newCustomer.setFirstName(registerData.getFirstName());
		newCustomer.setLastName(registerData.getLastName());
		newCustomer.setRegistrationSource(RegistrationSource.ASM);
		webUserGroup = registerData.getUserGroup();
		newCustomer.setGroups(assignUserGroup(webUserGroup));
		newCustomer.setCustomerID(UUID.randomUUID().toString());
		newCustomer.setConsumer(true);
		if (BcfCoreConstants.FULL_ACCOUNT.equals(registerData.getAccountType()))
		{
			newCustomer.setConsumer(false);
			createAddress(newCustomer, registerData);
		}
		setUidForRegister(registerData, newCustomer);
		final UpdateCRMCustomerResponseDTO response;
		response = crmUpdateCustomerService.updateCustomer(newCustomer);
		if (response == null || response.getBCF_Customer() == null)
		{
			LOG.error("Response received from updateCustomer() service is null");
			throw new IllegalArgumentException();
		}
		if (BcfCoreConstants.FULL_ACCOUNT.equals(newCustomer.getAccountType()))
		{
			getModelService().saveAll(newCustomer, newCustomer.getDefaultShipmentAddress());
		}
		else
		{
			getModelService().save(newCustomer);
		}
	}

	private AddressModel createAddress(final CustomerModel newCustomer, final RegisterData registerData)
	{
		final AddressModel addressModel = modelService.create(AddressModel.class);
		addressModel.setOwner(newCustomer);
		final CountryModel country = commonI18NService.getCountry(registerData.getCountry());
		addressModel.setCountry(country);

		if (StringUtils.isNotEmpty(registerData.getProvince()))
		{
			try
			{
				final RegionModel region = commonI18NService
						.getRegion(country, registerData.getProvince());
				addressModel.setRegion(region);
			}
			catch (final UnknownIdentifierException uiEx)
			{
				LOG.error(uiEx.getMessage(), uiEx);
			}
		}

		addressModel.setPostalcode(registerData.getZipCode());
		addressModel.setLine1(registerData.getAddressLine1());
		addressModel.setLine2(registerData.getAddressLine2());
		addressModel.setPrimaryInd(Boolean.TRUE);
		addressModel.setPhone1(registerData.getPhoneNo());
		final List<AddressModel> addressModels = new ArrayList<>();
		addressModels.add(addressModel);
		newCustomer.setAddresses(addressModels);
		return addressModel;
	}

	private void sendEmailProcess(final CustomerModel newCustomer)
	{
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = getBusinessProcessService()
				.createProcess(
						"customerRegistrationEmailProcess-" + newCustomer.getUid() + "-" + System.currentTimeMillis(),
						"customerRegistrationEmailProcess");
		executeBusinessProcess(storeFrontCustomerProcessModel, newCustomer, siteModel);
	}

	private Set<PrincipalGroupModel> assignUserGroup(final String webUserGroup)
	{
		final Set<PrincipalGroupModel> principalGroupModels = new HashSet<>();
		if (StringUtils.isNotEmpty(webUserGroup))
		{
			final UserGroupModel userGroupModel = getUserService().getUserGroupForUID(webUserGroup);
			principalGroupModels.add(userGroupModel);
		}
		return principalGroupModels;
	}

	private void executeBusinessProcess(final StoreFrontCustomerProcessModel businessProcessModel,
			final CustomerModel customerModel, final CMSSiteModel siteModel)
	{
		businessProcessModel.setSite(siteModel);
		businessProcessModel.setCustomer(customerModel);
		businessProcessModel.setLanguage(getCommonI18NService().getCurrentLanguage());
		businessProcessModel.setCurrency(getCommonI18NService().getCurrentCurrency());
		businessProcessModel.setStore(getBaseStoreService().getCurrentBaseStore());
		getModelService().save(businessProcessModel);
		getBusinessProcessService().startProcess(businessProcessModel);
	}

	private void setUidForRegister(final RegisterData registerData, final CustomerModel customer)
	{
		customer.setUid(registerData.getLogin().toLowerCase());
		customer.setOriginalUid(registerData.getLogin());
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public CRMUpdateCustomerService getCrmUpdateCustomerService()
	{
		return crmUpdateCustomerService;
	}

	@Required
	public void setCrmUpdateCustomerService(final CRMUpdateCustomerService crmUpdateCustomerService)
	{
		this.crmUpdateCustomerService = crmUpdateCustomerService;
	}
}
