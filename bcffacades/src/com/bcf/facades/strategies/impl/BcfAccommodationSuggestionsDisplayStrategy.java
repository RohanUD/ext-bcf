/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.travelfacades.accommodation.strategies.impl.DefaultAccommodationSuggestionsDisplayStrategy;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.travelroute.service.BCFTravelRouteService;


public class BcfAccommodationSuggestionsDisplayStrategy extends DefaultAccommodationSuggestionsDisplayStrategy
{
	private BCFTravelRouteService bcfTravelRouteService;

	@Override
	public List<GlobalSuggestionData> createGlobalSuggestionData(final String text,
			final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatas, final String searchType)
	{
		final List<GlobalSuggestionData> globalSuggestionDataList = new ArrayList();
		for (final AccommodationOfferingDayRateData accommodationOfferingDayRateData : accommodationOfferingDayRateDatas)
		{
			final String locationCode = accommodationOfferingDayRateData.getLocationCodes().split("\\|")[0];
			final Set<TravelRouteModel> travelRoutes = getBcfTravelRouteService()
					.getBcfVacationTravelRoutesForDestination(locationCode);
			if (CollectionUtils.isNotEmpty(travelRoutes))
			{
				final GlobalSuggestionData globalSuggestionData = new GlobalSuggestionData();
				globalSuggestionData.setCode(accommodationOfferingDayRateData.getLocationCodes());
				globalSuggestionData.setName(getLocationName(accommodationOfferingDayRateData.getLocationNames()));
				globalSuggestionData.setSuggestionType(SuggestionType.LOCATION);
				globalSuggestionDataList.add(globalSuggestionData);
			}
		}
		return globalSuggestionDataList;
	}

	protected BCFTravelRouteService getBcfTravelRouteService()
	{
		return bcfTravelRouteService;
	}

	@Required
	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		this.bcfTravelRouteService = bcfTravelRouteService;
	}
}
