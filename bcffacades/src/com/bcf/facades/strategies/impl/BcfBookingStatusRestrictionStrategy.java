/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.BookingActionResponseData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.travelfacades.booking.action.strategies.GlobalBookingActionEnabledEvaluatorStrategy;
import de.hybris.platform.travelfacades.booking.action.strategies.impl.AccommodationBookingStatusRestrictionStrategy;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;


public class BcfBookingStatusRestrictionStrategy  extends AccommodationBookingStatusRestrictionStrategy implements
      GlobalBookingActionEnabledEvaluatorStrategy
{

   public BcfBookingStatusRestrictionStrategy() {
      // empty Constructor
   }


   public void applyStrategy(List<BookingActionData> bookingActionDataList, GlobalTravelReservationData globalTravelReservationData, final BookingActionResponseData bookingActionResponseData) {
      List<BookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(BookingActionData::isEnabled).collect(Collectors.toList());
      if (!enabledBookingActions.isEmpty()) {
         boolean disabled = this.getNotAllowedStatuses().stream().filter((status) -> {
            return StringUtils.equalsIgnoreCase(status.getCode(), globalTravelReservationData.getBookingStatusCode());
         }).findFirst().isPresent();
         if (disabled) {
            enabledBookingActions.forEach((bookingActionData) -> {
               bookingActionData.setEnabled(Boolean.FALSE);
               bookingActionData.getAlternativeMessages().add("booking.action.status.alternative.message");
            });
         }

      }
   }


}
