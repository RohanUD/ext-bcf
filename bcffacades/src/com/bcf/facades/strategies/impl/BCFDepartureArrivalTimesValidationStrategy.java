/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.strategies.impl.DepartureArrivalTimesValidationStrategy;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import com.bcf.facades.cart.BcfAddBundleToCartData;


public class BCFDepartureArrivalTimesValidationStrategy extends DepartureArrivalTimesValidationStrategy
{
	@Override
	public AddToCartResponseData validate(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final Optional<Integer> odRefNumber = addBundleToCartRequestData.getAddBundleToCartData().stream()
				.map(AddBundleToCartData::getOriginDestinationRefNumber).distinct().findFirst();

		final int journeyRefNumber = ((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
				.getJourneyRefNumber();

		if (odRefNumber.isPresent() && !odRefNumber.get().equals(TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER))
		{
			if (!getCartService().hasSessionCart())
			{
				return createAddToCartResponse(false, ADD_BUNDLE_TO_CART_VALIDATION_ERROR_NO_SESSION_CART, null);
			}

			final Integer previousOdRefNumber = Integer.sum(odRefNumber.get(), -1);

			final List<AbstractOrderEntryModel> entriesByJourneyRefNumber = getCartService().getSessionCart().getEntries().stream()
					.filter(entry -> entry.getJourneyReferenceNumber() == journeyRefNumber).collect(Collectors.toList());

			final Optional<AbstractOrderEntryModel> previousEntry = entriesByJourneyRefNumber.stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
							&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()) && entry.getActive()
							&& previousOdRefNumber.equals(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
					.findAny();

			if (previousEntry.isPresent())
			{
				final List<TransportOfferingModel> transportOfferings = new ArrayList<>(
						previousEntry.get().getTravelOrderEntryInfo().getTransportOfferings());
				final Optional<TransportOfferingModel> lastTransportOffering = transportOfferings.stream()
						.sorted((to1, to2) -> getUTCDepartureTime(to2).compareTo(getUTCDepartureTime(to1))).findFirst();

				if (lastTransportOffering.isPresent())
				{
					final List<String> transportOfferingsToAdd = addBundleToCartRequestData.getAddBundleToCartData().get(0)
							.getTransportOfferings();
					final TransportOfferingModel firstTransportOffering = transportOfferingsToAdd.stream()
							.map(toCode -> getTransportOfferingService().getTransportOffering(toCode))
							.sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();

					if (lastTransportOffering.get().getArrivalTime().after(firstTransportOffering.getDepartureTime()))
					{
						return createAddToCartResponse(false, ADD_BUNDLE_TO_CART_VALIDATION_ERROR_DEPARTURE_ARRIVAL, null);
					}
				}
			}
		}
		return createAddToCartResponse(true, null, null);
	}
}
