/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.AccommodationBookingActionData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.travelfacades.booking.action.strategies.AccommodationBookingActionEnabledEvaluatorStrategy;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


public class RemainingAmountRestrictionStrategy implements AccommodationBookingActionEnabledEvaluatorStrategy
{
	private CustomerAccountService customerAccountService;
	private BaseStoreService baseStoreService;

	@Override
	public void applyStrategy(final List<AccommodationBookingActionData> bookingActionDataList,
			final AccommodationReservationData accommodationReservationData)
	{
		final List<AccommodationBookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(AccommodationBookingActionData::isEnabled).collect(
				Collectors.toList());
		if(CollectionUtils.isNotEmpty(enabledBookingActions))
		{
			final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
			final AbstractOrderModel orderModel = getCustomerAccountService().getOrderForCode(accommodationReservationData.getCode(), baseStore);
			boolean enabled = orderModel.getAmountToPay().compareTo(0d) > 0;
			if (!enabled) {
				enabledBookingActions.forEach((bookingActionData) -> {
					bookingActionData.setEnabled(Boolean.FALSE);
					bookingActionData.getAlternativeMessages().add("booking.action.total.payment.transaction.alternative.message");
				});
			}
		}
	}

	protected CustomerAccountService getCustomerAccountService() {
		return this.customerAccountService;
	}

	@Required
	public void setCustomerAccountService(CustomerAccountService customerAccountService) {
		this.customerAccountService = customerAccountService;
	}

	protected BaseStoreService getBaseStoreService() {
		return this.baseStoreService;
	}

	@Required
	public void setBaseStoreService(BaseStoreService baseStoreService) {
		this.baseStoreService = baseStoreService;
	}
}
