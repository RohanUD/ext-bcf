/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.strategies.AbstractAddBundleToCartValidationStrategy;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;


public class BCFDepartureTimeThresholdValidationStrategy extends AbstractAddBundleToCartValidationStrategy
{
	public static final String ADD_BUNDLE_TO_CART_VALIDATION_ERROR_DEPARTURETIME_THRESHOLD = "add.bundle.to.cart.validation.error.departuretime.threshold";
	public static final String ADD_BUNDLE_TO_CART_VALIDATION_ERROR_DEPARTURETIME_THRESHOLD_CONFIRM = "add.bundle.to.cart.validation.error.departuretime.threshold.confirm";

	private BCFReservationFacade bcfReservationFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;


	@Resource(name = "defaultBcfCheckoutService")
	private BcfCheckoutService bcfCheckoutService;

	@Override
	public AddToCartResponseData validate(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final Optional<Integer> odRefNumber = addBundleToCartRequestData.getAddBundleToCartData().stream()
				.map(AddBundleToCartData::getOriginDestinationRefNumber).distinct().findFirst();
		if (odRefNumber.isPresent() && odRefNumber.get().equals(TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER))
		{
			boolean enabled = false;
			final Optional<AddBundleToCartData> optionalAddBundleToCartData = addBundleToCartRequestData.getAddBundleToCartData()
					.stream().findFirst();
			if (optionalAddBundleToCartData.isPresent())
			{
				final BcfAddBundleToCartData bcfAddBundleToCartData = (BcfAddBundleToCartData) optionalAddBundleToCartData.get();
				final List<TransportOfferingData> transportOfferings = bcfAddBundleToCartData.getTransportOfferingDatas();
				final int thresholdTimeGap = getThresholdTimeGap(addBundleToCartRequestData);
				enabled = getBcfReservationFacade().isFirstTOMeetsDepartureTimeRestrictionGap(transportOfferings, thresholdTimeGap);
				if (!enabled)
				{
					if (bcfCheckoutService.checkSailingTimeThresholdOverrideApplicable())
					{
						final int minOriginDestinationRefNumber = odRefNumber.get() + 1;
						final AddToCartResponseData response = createAddToCartResponse(false,
								ADD_BUNDLE_TO_CART_VALIDATION_ERROR_DEPARTURETIME_THRESHOLD_CONFIRM,
								minOriginDestinationRefNumber);
						response.setNeedConfirmation(true);
						return response;
					}
					else
					{
						return createAddToCartResponse(false, ADD_BUNDLE_TO_CART_VALIDATION_ERROR_DEPARTURETIME_THRESHOLD, null);
					}
				}
			}
		}
		return createAddToCartResponse(true, null, null);
	}


	private int getThresholdTimeGap(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final List<String> commercialVehicleCodes = BCFPropertiesUtils
				.convertToList(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getVehicleTypes()) && Objects
				.nonNull(addBundleToCartRequestData.getVehicleTypes().get(0).getVehicleType()))
		{
			if (commercialVehicleCodes.contains(addBundleToCartRequestData.getVehicleTypes().get(0).getVehicleType().getCode()))
			{
				return Integer.parseInt(getBcfConfigurablePropertiesService()
						.getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_TRANSPORT_BOOKING_THRESHOLD_MINUTES));
			}
		}
		return Integer
				.parseInt(getBcfConfigurablePropertiesService()
						.getBcfPropertyValue(BcfCoreConstants.DEFAULT_TRANSPORT_BOOKING_THRESHOLD_MINUTES));
	}

	protected BCFReservationFacade getBcfReservationFacade()
	{
		return bcfReservationFacade;
	}

	@Required
	public void setBcfReservationFacade(final BCFReservationFacade bcfReservationFacade)
	{
		this.bcfReservationFacade = bcfReservationFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}
}
