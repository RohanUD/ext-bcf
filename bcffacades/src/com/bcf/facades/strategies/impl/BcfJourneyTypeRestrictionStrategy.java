/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.AccommodationBookingActionData;
import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.BookingActionResponseData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.travelfacades.booking.action.strategies.AccommodationBookingActionEnabledEvaluatorStrategy;
import de.hybris.platform.travelfacades.booking.action.strategies.GlobalBookingActionEnabledEvaluatorStrategy;
import java.util.List;
import java.util.stream.Collectors;


public class BcfJourneyTypeRestrictionStrategy implements AccommodationBookingActionEnabledEvaluatorStrategy,
      GlobalBookingActionEnabledEvaluatorStrategy
{

   public BcfJourneyTypeRestrictionStrategy() {
      //empty constructor
   }

   private List<String> allowedJourneyTypes;


   public void applyStrategy(List<AccommodationBookingActionData> bookingActionDataList, AccommodationReservationData accommodationReservationData) {
      List<AccommodationBookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(AccommodationBookingActionData::isEnabled).collect(Collectors.toList());
      if (!enabledBookingActions.isEmpty()) {
         boolean disabled =false;
         if(!getAllowedJourneyTypes().contains(accommodationReservationData.getBookingJourneyType())){
            disabled=true;

         }
         if (disabled) {
            enabledBookingActions.forEach((bookingActionData) -> {
               bookingActionData.setEnabled(Boolean.FALSE);
            });
         }

      }
   }

   public List<String> getAllowedJourneyTypes()
   {
      return allowedJourneyTypes;
   }

   public void setAllowedJourneyTypes(final List<String> allowedJourneyTypes)
   {
      this.allowedJourneyTypes = allowedJourneyTypes;
   }

   @Override
   public void applyStrategy(final List<BookingActionData> bookingActionDataList, final GlobalTravelReservationData globalTravelReservationData,
         final BookingActionResponseData bookingActionResponseData)
   {
      List<BookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(BookingActionData::isEnabled).collect(Collectors.toList());
      if (!enabledBookingActions.isEmpty()) {
         boolean disabled =false;
         if(!getAllowedJourneyTypes().contains(globalTravelReservationData.getBookingJourneyType())){
            disabled=true;

         }
         if (disabled) {
            enabledBookingActions.forEach((bookingActionData) -> {
               bookingActionData.setEnabled(Boolean.FALSE);
            });
         }

      }

   }
}
