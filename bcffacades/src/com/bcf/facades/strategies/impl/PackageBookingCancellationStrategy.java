/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.customer.TravelCustomerAccountService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.services.vacation.strategies.CancellationFeeStrategy;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.order.cancel.enums.CancelStatus;
import com.bcf.facades.strategies.BookingCancellationStrategy;
import com.bcf.integration.payment.exceptions.BcfRefundException;
import com.bcf.integration.payment.exceptions.InvalidPaymentInfoException;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public class PackageBookingCancellationStrategy implements BookingCancellationStrategy
{
	public static final String FAILED_WITH_REASON_CODE = "processing refund failed with reason code: ";
	private BcfBookingService bcfBookingService;
	private BcfCheckoutService bcfCheckoutService;
	private ModelService modelService;
	private BcfRefundIntegrationService bcfRefundIntegrationService;
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;
	private BcfBookingFacade bookingFacade;
	private CalculationService calculationService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BaseStoreService baseStoreService;
	private TravelCustomerAccountService customerAccountService;
	private CommonI18NService commerceI18NService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CancellationFeeStrategy cancellationFeeStrategy;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	private static final Logger LOG = Logger.getLogger(PackageBookingCancellationStrategy.class);

	@Override
	public CancelBookingResponseData cancelCompleteOrder(final String orderCode,
			List<RefundTransactionInfoData> refundTransactionInfoDatas) throws Exception
	{
		final CancelBookingResponseData cancelBookingResponseData = (CancelBookingResponseData) Transaction.current()
				.execute(new TransactionBody()
							{
								@Override
								public Object execute() throws Exception
								{
									CancelBookingResponseData cancelBookingResponseData = null;
									try
									{
										final OrderModel order = getBcfBookingService().getOrder(orderCode);
										BigDecimal totalPaid = bcfBookingService.calculateTotalToRefund(order);
										cancellationFeeStrategy.addNonRefundableCostToBcf(order);
										BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = cancellationFeeStrategy
												.calculateCancellationFee(order);
										bcfChangeCancellationVacationFeeData = adjustCancellationFee(totalPaid,
												bcfChangeCancellationVacationFeeData);

										if (CollectionUtils.isNotEmpty(order.getPaymentTransactions()))
										{
											BigDecimal totalCancelFee = getTotalCancellationFee(bcfChangeCancellationVacationFeeData);
											final PriceData priceData = getRefundTotal(totalPaid, order.getCurrency().getDigits(),
													order.getCurrency().getIsocode(), totalCancelFee);
											processRefund(order, priceData, refundTransactionInfoDatas);
										}
										getBcfBookingService().cancelAccommodationAndActivityEntries(order, bcfChangeCancellationVacationFeeData);

										cancelBookingResponseData = cancelTransportBookings(order);

										//this means there are no transport entries, set the cancellation status to success
										if (Objects.isNull(cancelBookingResponseData))
										{
											cancelBookingResponseData = new CancelBookingResponseData();
											cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_SUCCESS);
										}

										if (Objects
												.equals(CancelStatus.COMPLETE_SUCCESS, cancelBookingResponseData.getCancelStatus()))
										{
											order.setStatus(OrderStatus.CANCELLED);
											order.setAmountToPay(0d);
											order.setPayAtTerminal(0d);
											getModelService().save(order);

										}
										else
										{

											throw new CancellationException("cancellation failed");
										}

									}
									catch (final CancellationException | CalculationException | InvalidPaymentInfoException ex)
									{
										LOG.error("cancellation failed for the order [" + orderCode + "]", ex);
										throw ex;
									}
									return cancelBookingResponseData;
								}
							}
				);

		if (Objects.isNull(cancelBookingResponseData) || Objects
				.equals(CancelStatus.COMPLETE_SUCCESS, cancelBookingResponseData.getCancelStatus()))
		{
			final OrderModel order = getBcfBookingService().getOrder(orderCode);
			bcfBookingService.startCancelOrderEmailProcess(order);
		}
		return cancelBookingResponseData;
	}

	private void processRefund(final OrderModel order, final PriceData priceData,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas) throws IntegrationException, BcfRefundException
	{
		if (priceData.getValue().doubleValue() > 0d)
		{
			if (CollectionUtils.isNotEmpty(refundTransactionInfoDatas))
			{

				List<PaymentResponseDTO> paymentResponseDTOs = bcfRefundIntegrationService
						.processRefund(order, refundTransactionInfoDatas);

				validatePaymentResponse(paymentResponseDTOs);

			}
			else
			{
				final PaymentResponseDTO paymentResponseDTO = bcfRefundIntegrationService
						.processRefund(order, priceData.getValue().doubleValue());
				final String bcfResponseCode = paymentResponseDTO.getResponseStatus()
						.getBcfResponseCode();
				if (!isPaymentSuccessful(paymentResponseDTO))
				{
					LOG.error(FAILED_WITH_REASON_CODE + bcfResponseCode);
					throw new BcfRefundException(FAILED_WITH_REASON_CODE + bcfResponseCode);
				}

			}
		}
	}

	private void validatePaymentResponse(final List<PaymentResponseDTO> paymentResponseDTOs) throws BcfRefundException
	{
		if (CollectionUtils.isNotEmpty(paymentResponseDTOs))
		{

			for (PaymentResponseDTO paymentResponseDTO : paymentResponseDTOs)
			{
				final String bcfResponseCode = paymentResponseDTO.getResponseStatus()
						.getBcfResponseCode();
				if (!isPaymentSuccessful(paymentResponseDTO))
				{
					LOG.error(FAILED_WITH_REASON_CODE + bcfResponseCode);
					throw new BcfRefundException(
							FAILED_WITH_REASON_CODE + bcfResponseCode);
				}
			}
		}
	}

	public PriceData getRefundTotal(BigDecimal totalPaid, int digits, String isoCode,
			BigDecimal totalCancelFee)
	{
		BigDecimal totalToRefund = totalPaid.subtract(totalCancelFee);
		totalToRefund = totalToRefund.doubleValue() < 0.0D ?
				BigDecimal.ZERO :
				BigDecimal.valueOf(getCommerceI18NService().roundCurrency(totalToRefund.doubleValue(), digits));
		return getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalToRefund, isoCode);
	}


	public PriceData getTotalCancellationFeePriceData(BigDecimal totalFee, String isoCode)
	{
		return getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalFee, isoCode);
	}



	public BigDecimal getTotalCancellationFee(BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)
	{

		BigDecimal totalCancellationFee = BigDecimal.ZERO;

		if (bcfChangeCancellationVacationFeeData != null)
		{

			totalCancellationFee = totalCancellationFee
					.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getPackageFees()));
			totalCancellationFee = totalCancellationFee
					.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getComponentFees()));

		}
		return totalCancellationFee;

	}

	public BcfChangeCancellationVacationFeeData adjustCancellationFee(BigDecimal totalPaid,
			BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData)
	{

		BigDecimal packageCancellationFee = BigDecimal.ZERO;
		BigDecimal componentCancellationfee = BigDecimal.ZERO;

		if (bcfChangeCancellationVacationFeeData != null)
		{

			packageCancellationFee = packageCancellationFee
					.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getPackageFees()));
			componentCancellationfee = componentCancellationfee
					.add(BigDecimal.valueOf(bcfChangeCancellationVacationFeeData.getComponentFees()));
		}

		if (packageCancellationFee.compareTo(totalPaid) >= 0)
		{
			componentCancellationfee = BigDecimal.ZERO;
			packageCancellationFee = totalPaid;
		}
		else if (packageCancellationFee.add(componentCancellationfee).compareTo(totalPaid) > 0)
		{
			componentCancellationfee = totalPaid.subtract(packageCancellationFee);

		}
		if (bcfChangeCancellationVacationFeeData != null)
		{
			bcfChangeCancellationVacationFeeData.setPackageFees(packageCancellationFee.doubleValue());
			bcfChangeCancellationVacationFeeData.setComponentFees(componentCancellationfee.doubleValue());
		}
		return bcfChangeCancellationVacationFeeData;

	}

	protected boolean isPaymentSuccessful(final PaymentResponseDTO paymentResponseDTO)
	{
		return paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& org.apache.commons.lang.StringUtils.equals(paymentResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode"));
	}

	private CancelBookingResponseData cancelTransportBookings(final OrderModel order)
	{
		CancelBookingResponseData cancelBookingResponseData = null;

		try
		{

			if (bcfBookingService.checkIfAnyOrderEntryByType(order, OrderEntryType.TRANSPORT))
			{
				cancelBookingResponseData = new CancelBookingResponseData();
				final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
						.cancelBookings(order);
				doOrderCancellation(order, cancelBookingResponseData, removeSailingResponseDatas);
			}
		}
		catch (final ModelNotFoundException | IntegrationException ex)
		{
			bookingFacade.poulateCancelBookingResponseData(order.getCode(), cancelBookingResponseData, ex);
		}
		return cancelBookingResponseData;
	}


	/**
	 * Do order cancellation.
	 *
	 * @param order                      the order
	 * @param cancelBookingResponseData  the cancel booking response data
	 * @param removeSailingResponseDatas the remove sailing response datas
	 */
	protected void doOrderCancellation(final OrderModel order, final CancelBookingResponseData cancelBookingResponseData,
			final List<RemoveSailingResponseData> removeSailingResponseDatas)
	{
		final List<RemoveSailingResponseData> failureCancelEBookings = getCancelBookingIntegrationFacade()
				.filterCancelBookingList(removeSailingResponseDatas, false);
		if (CollectionUtils.isEmpty(failureCancelEBookings))
		{
			cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_SUCCESS);
			return;
		}
		cancelBookingResponseData.setFailureCancelEBookings(failureCancelEBookings);
		final List<RemoveSailingResponseData> successCancelEBookings = getCancelBookingIntegrationFacade()
				.filterCancelBookingList(removeSailingResponseDatas, true);
		if (CollectionUtils.isEmpty(successCancelEBookings))
		{
			cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_FAIL);
			return;
		}
	}


	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfRefundIntegrationService getBcfRefundIntegrationService()
	{
		return bcfRefundIntegrationService;
	}

	public void setBcfRefundIntegrationService(final BcfRefundIntegrationService bcfRefundIntegrationService)
	{
		this.bcfRefundIntegrationService = bcfRefundIntegrationService;
	}

	public CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

	public BcfBookingFacade getBookingFacade()
	{
		return bookingFacade;
	}

	public void setBookingFacade(final BcfBookingFacade bookingFacade)
	{
		this.bookingFacade = bookingFacade;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public TravelCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	public void setCustomerAccountService(final TravelCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}

	public TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	public CancellationFeeStrategy getCancellationFeeStrategy()
	{
		return cancellationFeeStrategy;
	}

	public void setCancellationFeeStrategy(final CancellationFeeStrategy cancellationFeeStrategy)
	{
		this.cancellationFeeStrategy = cancellationFeeStrategy;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
