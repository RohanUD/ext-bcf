/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.BookingActionResponseData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.travelfacades.booking.action.strategies.GlobalBookingActionEnabledEvaluatorStrategy;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import com.bcf.facades.reservation.data.ActivityReservationData;


public class BcfMoreThanOneUniqueActivityRestriction implements
		GlobalBookingActionEnabledEvaluatorStrategy
{
	@Override
	public void applyStrategy(final List<BookingActionData> bookingActionDataList,
			final GlobalTravelReservationData globalTravelReservationData,
			final BookingActionResponseData bookingActionResponseData)
	{
		List<BookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(BookingActionData::isEnabled).collect(
				Collectors.toList());
		if (!enabledBookingActions.isEmpty())
		{
			boolean disabled = false;
			if (BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode().equals(globalTravelReservationData.getBookingJourneyType())
					&& Objects.nonNull(globalTravelReservationData.getActivityReservations()))
			{
				Map<String, List<ActivityReservationData>> clubbedActivityReservationDatasByActivity = globalTravelReservationData
						.getActivityReservations().getClubbedActivityReservationDatasByActivity();
				if (MapUtils.isEmpty(clubbedActivityReservationDatasByActivity)
						|| clubbedActivityReservationDatasByActivity.size() == 1)
				{
					disabled = true;
				}
			}
			if (disabled)
			{
				enabledBookingActions.forEach((bookingActionData) -> {
					bookingActionData.setEnabled(Boolean.FALSE);
				});
			}

		}
	}
}
