/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.strategies.impl.OriginDestinationRefNumberValidationStrategy;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.order.BcfTravelCartFacade;


public class BCFOriginDestinationRefNumberValidationStrategy extends OriginDestinationRefNumberValidationStrategy
{

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;


	@Override
	public AddToCartResponseData validate(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final Optional<Integer> odRefNumber = addBundleToCartRequestData.getAddBundleToCartData().stream()
				.map(AddBundleToCartData::getOriginDestinationRefNumber).distinct().findFirst();

		final int journeyRefNumber = ((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
				.getJourneyRefNumber();

		if (!bcfTravelCartFacade.isAmendmentCart() && odRefNumber.isPresent() && !odRefNumber.get().equals(TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER))
		{
			if (!getCartService().hasSessionCart())
			{
				return createAddToCartResponse(false, ADD_BUNDLE_TO_CART_VALIDATION_ERROR_NO_PREVIOUS_ENTRIES, null);
			}
			final List<AbstractOrderEntryModel> entriesByJourneyRefNumber = getCartService().getSessionCart().getEntries().stream()
					.filter(entry -> entry.getJourneyReferenceNumber() == journeyRefNumber).collect(Collectors.toList());

			final List<AbstractOrderEntryModel> orderEntryList = entriesByJourneyRefNumber.stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
							&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()) && entry.getActive())
					.collect(Collectors.toList());

			final boolean allPreviousEntriesExist = IntStream.range(0, odRefNumber.get()).allMatch(i -> orderEntryList.stream()
					.anyMatch(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == i));
			if (!allPreviousEntriesExist)
			{
				return createAddToCartResponse(false, ADD_BUNDLE_TO_CART_VALIDATION_ERROR_NO_PREVIOUS_ENTRIES, null);
			}
		}
		return createAddToCartResponse(true, null, null);
	}
}
