/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.strategies.impl;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.strategies.AbstractAddBundleToCartValidationStrategy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.order.BcfTravelCartFacade;


public class BCFAddAdditionalSailingsValidationStrategy extends AbstractAddBundleToCartValidationStrategy
{
	@Resource
	private CartService cartService;
	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;
	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;
	@Resource
	private SessionService sessionService;
	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	public static final String ADD_BUNDLE_TO_CART_VALIDATION_ERROR_MAX_JOURNEYS_ENTRIES = "add.bundle.to.cart.request.max.journey";

	/**
	 * This method validates max sailings allowed for anonymous and loggedIn users on the basis of the cachekey for each sailing added to the cart.
	 */
	@Override
	public AddToCartResponseData validate(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		if (!addBundleToCartRequestData.isChangeOrReplan() && getCartService().hasSessionCart())
		{
			final AbstractOrderModel abstractOrderModel = getCartService().getSessionCart();
			if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
			{
				return createAddToCartResponse(true, null, null);
			}
			final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries()
					.stream().filter(entry -> Objects.isNull(entry.getEntryGroup())).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(entries))
			{
				return createAddToCartResponse(true, null, null);
			}
			Map<String, List<AbstractOrderEntryModel>> entriesByCacheKeyOrBookingRef = new HashMap<>();
			final List<AbstractOrderEntryModel> bookingRefEntries = StreamUtil.safeStream(entries)
					.filter(entry -> entry.getBookingReference() != null).collect(Collectors.toList());
			final List<AbstractOrderEntryModel> cacheKeyRefEntries = StreamUtil.safeStream(entries)
					.filter(entry -> entry.getCacheKey() != null).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(cacheKeyRefEntries))
			{
				entriesByCacheKeyOrBookingRef = StreamUtil.safeStream(cacheKeyRefEntries)
						.collect(Collectors.groupingBy(AbstractOrderEntryModel::getCacheKey));
			}
			else if (CollectionUtils.isNotEmpty(bookingRefEntries))
			{
				entriesByCacheKeyOrBookingRef = StreamUtil.safeStream(bookingRefEntries)
						.collect(Collectors.groupingBy(AbstractOrderEntryModel::getBookingReference));
			}
			final int sizeOfCart = entriesByCacheKeyOrBookingRef.size();
			return setCreateAddToCartResponse(sizeOfCart, addBundleToCartRequestData);
		}
		return createAddToCartResponse(true, null, null);
	}


	private AddToCartResponseData setCreateAddToCartResponse(final int sizeOfCart,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final int maxAllowed = cartFacade.getMaxSailingAllowed();
		if (cartFacade.getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
				addBundleToCartRequestData.getSelectedOdRefNumber()).size() > 0 || sizeOfCart < maxAllowed)
		{
			return createAddToCartResponse(true, null, null);
		}
		else
		{
			return createAddToCartResponse(false,
					propertySourceFacade.getPropertySourceValue(ADD_BUNDLE_TO_CART_VALIDATION_ERROR_MAX_JOURNEYS_ENTRIES), null);
		}
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

}


