/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.integration.processor.impl;

import de.hybris.platform.core.model.order.CartModel;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.integration.handler.BcfIntegrationHandler;
import com.bcf.facades.integration.processor.BcfIntegrationPipelineProcessor;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfIntegrationPipelineProcessor implements BcfIntegrationPipelineProcessor
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfIntegrationPipelineProcessor.class);

	private List<BcfIntegrationHandler> handlers;

	@Override
	public void executePipeline(final CartModel cart,
			final PlaceOrderResponseData typeWiseEntries) throws OrderProcessingException, IntegrationException
	{
		LOG.debug(
				String.format("Calling integration services for order [%s] with amount [%s]", cart.getCode(), cart.getAmountToPay()));
		for (final BcfIntegrationHandler handler : getHandlers())
		{
			handler.handle(cart, typeWiseEntries);
		}
	}

	protected List<BcfIntegrationHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<BcfIntegrationHandler> handlers)
	{
		this.handlers = handlers;
	}
}
