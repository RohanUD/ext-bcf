/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.integration.integration.payment.impl;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.user.BcfUserService;
import com.bcf.facades.integration.integration.payment.CRMPaymentMethodCreationHandler;
import com.bcf.integration.data.PaymentProfileInfo;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class CRMCTCTCPaymentInfoCreationHandler extends CRMPaymentMethodCreationHandler
{
	private UserService userService;
	private SessionService sessionService;

	@Override
	public PaymentInfoModel createPaymentMethod(final CustomerModel customerModel, final PaymentProfileInfo paymentProfileInfo)
	{
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = getModelService().create(CTCTCCardPaymentInfoModel.class);
		ctcTcCardPaymentInfoModel.setCardNumber(paymentProfileInfo.getNumber());
		ctcTcCardPaymentInfoModel.setUser(customerModel);
		ctcTcCardPaymentInfoModel.setCode(paymentProfileInfo.getNumber());
		ctcTcCardPaymentInfoModel.setSaved(true);

		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			ctcTcCardPaymentInfoModel.setB2bUnit(b2bCheckoutContextData.getB2bUnit());
		}

		if (paymentProfileInfo.getAddress() != null)
		{
			final AddressModel billingAddress = getPopulateBillingAddress(paymentProfileInfo.getAddress());
			billingAddress.setOwner(customerModel);
			ctcTcCardPaymentInfoModel.setBillingAddress(billingAddress);
		}

		getModelService().saveAll();

		return ctcTcCardPaymentInfoModel;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
