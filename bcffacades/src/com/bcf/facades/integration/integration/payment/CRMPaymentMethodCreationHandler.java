/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.integration.integration.payment;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.data.Address;
import com.bcf.integration.data.PaymentProfileInfo;


public abstract class CRMPaymentMethodCreationHandler
{
	private ModelService modelService;
	private CommonI18NService commonI18NService;

	public abstract PaymentInfoModel createPaymentMethod(final CustomerModel customerModel, final PaymentProfileInfo profileInfo);

	protected AddressModel getPopulateBillingAddress(final Address address)
	{
		final AddressModel addressModel = getModelService().create(AddressModel.class);
		addressModel.setLine1(address.getAddressLine1());
		addressModel.setLine2(address.getAddressLine2());
		addressModel.setTown(address.getCity());
		addressModel.setPostalcode(address.getPostalCode());
		final CountryModel countryModel = getCommonI18NService().getCountry(address.getCountryCode());
		addressModel.setCountry(countryModel);
		if (Objects.nonNull(address.getProvinceStateCode()))
		{
			final RegionModel regionModel = getCommonI18NService()
					.getRegion(countryModel, address.getCountryCode() + "-" + address.getProvinceStateCode());
			addressModel.setRegion(regionModel);
		}

		return addressModel;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
