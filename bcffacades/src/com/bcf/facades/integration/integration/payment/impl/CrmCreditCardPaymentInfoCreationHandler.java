/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.integration.integration.payment.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.user.BcfUserService;
import com.bcf.facades.integration.integration.payment.CRMPaymentMethodCreationHandler;
import com.bcf.integration.data.PaymentProfileInfo;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class CrmCreditCardPaymentInfoCreationHandler extends CRMPaymentMethodCreationHandler
{

	private static final String ESELECT_PLUS = "ESELECTPLUS";

	private UserService userService;
	private SessionService sessionService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Override
	public PaymentInfoModel createPaymentMethod(final CustomerModel customerModel, final PaymentProfileInfo paymentProfileInfo)
	{
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
		creditCardPaymentInfoModel.setToken(paymentProfileInfo.getNumber());
		creditCardPaymentInfoModel.setTokenType(ESELECT_PLUS);
		creditCardPaymentInfoModel.setNumber(paymentProfileInfo.getCreditCardNumber());
		final CreditCardType cardType = getCardTypeForCRMCardName(paymentProfileInfo.getCreditCardType());
		if (Objects.isNull(cardType))
		{
			throw new ModelNotFoundException(
					"could not find the card type for " + paymentProfileInfo.getCreditCardType() + ", for the customer "
							+ customerModel.getUid());
		}
		creditCardPaymentInfoModel.setType(cardType);
		creditCardPaymentInfoModel.setCcOwner(paymentProfileInfo.getCreditCardHolderName());
		creditCardPaymentInfoModel.setValidToMonth(paymentProfileInfo.getCardExpiry().substring(0, 2));
		creditCardPaymentInfoModel.setValidToYear(paymentProfileInfo.getCardExpiry().substring(2));
		creditCardPaymentInfoModel.setSaved(true);
		creditCardPaymentInfoModel.setOwner(customerModel);
		creditCardPaymentInfoModel.setCode(paymentProfileInfo.getNumber());
		creditCardPaymentInfoModel.setUser(customerModel);

		if (((BcfUserService) getUserService()).isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			creditCardPaymentInfoModel.setB2bUnit(b2bCheckoutContextData.getB2bUnit());
		}

		if (paymentProfileInfo.getAddress() != null)
		{
			final AddressModel billingAddress = getPopulateBillingAddress(paymentProfileInfo.getAddress());
			billingAddress.setOwner(customerModel);
			creditCardPaymentInfoModel.setBillingAddress(billingAddress);
			getModelService().save(billingAddress);
		}

		getModelService().save(creditCardPaymentInfoModel);

		return creditCardPaymentInfoModel;
	}

	private CreditCardType getCardTypeForCRMCardName(final String creditCardType)
	{
		final List<CreditCardType> enumerationValues = enumerationService.getEnumerationValues(CreditCardType.class);
		for (final CreditCardType cardType : enumerationValues)
		{
			if (StringUtils.equalsIgnoreCase(creditCardType, enumerationService.getEnumerationName(cardType)))
			{
				return cardType;
			}
		}
		return null;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
