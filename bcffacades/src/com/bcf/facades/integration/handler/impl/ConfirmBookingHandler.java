/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.integration.handler.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.ConfirmBookingIntegrationFacade;
import com.bcf.facades.ebooking.ConfirmBookingServiceSuccessData;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.integration.handler.BcfIntegrationHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.BcfRefundIntegrationFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class ConfirmBookingHandler implements BcfIntegrationHandler
{
	private static final Logger LOG = Logger.getLogger(ConfirmBookingHandler.class);
	private ConfirmBookingIntegrationFacade confirmBookingIntegrationFacade;
	private BcfCheckoutService bcfCheckoutService;
	private BcfRefundIntegrationFacade bcfRefundIntegrationFacade;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BCFTravelCartService bcfTravelCartService;
	private ModelService modelService;
	private BcfCalculationService bcfCalculationService;

	@Override
	public void handle(final CartModel cartModel, final PlaceOrderResponseData decisionData)
			throws OrderProcessingException, IntegrationException
	{
		if (bcfTravelCartService.hasValidCacheKeyOrBookingRef(cartModel))
		{
			try
			{
				LOG.debug(String.format("Calling ConfirmBooking for cart [%s]", cartModel.getCode()));
				final ConfirmBookingServiceSuccessData serviceSuccess = getConfirmBookingIntegrationFacade()
						.makeConfirmBookingRequest(cartModel);
				if (Objects.nonNull(serviceSuccess) && StringUtils.isBlank(serviceSuccess.getErrorCode())
						&& getBcfCheckoutService().updateCartWithEbookingDetails(serviceSuccess, cartModel))
				{
					LOG.debug(String.format("ConfirmBooking request for cart [%s] was successful", cartModel.getCode()));
					return;
				}
				LOG.error(String.format("Error encountered while processing order [%s]", cartModel.getCode()));
				decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
				if (BigDecimal.valueOf(cartModel.getAmountToPay()).compareTo(BigDecimal.ZERO) > 0)
				{
					startRefund(cartModel);
					resetPaymentTransactionsAndAmountToPay(cartModel);
				}
				getBcfTravelCommerceStockService().releaseStocks(cartModel);
				throw new OrderProcessingException("Exception encountered while calling ConfirmBooking service");
			}
			catch (final IntegrationException ie)
			{
				LOG.error("Integration Exception encountered while calling ConfirmBooking service");
				startRefund(cartModel);
				resetPaymentTransactionsAndAmountToPay(cartModel);
				getBcfTravelCommerceStockService().releaseStocks(cartModel);
				throw ie;
			}
		}
	}

	private void startRefund(final CartModel cartModel)
	{
		final Optional<PaymentTransactionModel> optionalPaymentTxn = cartModel.getPaymentTransactions().stream().filter(
				transaction -> !transaction.isExisting() && transaction.getInfo() instanceof CreditCardPaymentInfoModel).findAny();
		if (optionalPaymentTxn.isPresent())
		{
			final Optional<PaymentTransactionEntryModel> optionalEntry = optionalPaymentTxn.get().getEntries().stream()
					.filter(entry -> PaymentTransactionType.PURCHASE.equals(entry.getType())).findFirst();
			if (optionalEntry.isPresent() && BigDecimal.ZERO.compareTo(optionalEntry.get().getAmount()) < 0)
			{
				getBcfRefundIntegrationFacade().refundOrder(cartModel);
				LOG.debug(String.format("Refund has been initiated for order [%s]", cartModel.getCode()));
			}
		}
	}

	private void resetPaymentTransactionsAndAmountToPay(final CartModel cartModel)
	{
		final List<PaymentTransactionModel> paymentTransactions = cartModel.getPaymentTransactions().stream().filter(
				transaction -> !transaction.isExisting()).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(paymentTransactions))
		{
			final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransactions.stream()
					.flatMap(paymentTransaction -> paymentTransaction.getEntries().stream()).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(paymentTransactionEntries))
			{
				modelService.removeAll(paymentTransactionEntries);
			}
			modelService.removeAll(paymentTransactions);
		}
		cartModel.setDepositPayDate(null);

		bcfTravelCartService.resetAmountToPay(cartModel);

	}

	protected ConfirmBookingIntegrationFacade getConfirmBookingIntegrationFacade()
	{
		return confirmBookingIntegrationFacade;
	}

	@Required
	public void setConfirmBookingIntegrationFacade(final ConfirmBookingIntegrationFacade confirmBookingIntegrationFacade)
	{
		this.confirmBookingIntegrationFacade = confirmBookingIntegrationFacade;
	}

	protected BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	@Required
	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	protected BcfRefundIntegrationFacade getBcfRefundIntegrationFacade()
	{
		return bcfRefundIntegrationFacade;
	}

	@Required
	public void setBcfRefundIntegrationFacade(final BcfRefundIntegrationFacade bcfRefundIntegrationFacade)
	{
		this.bcfRefundIntegrationFacade = bcfRefundIntegrationFacade;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}
}
