/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.accommodation.BcfCommentModel;
import com.bcf.facades.accommodation.BcfCommentData;


public class BcfCommentPopulator implements Populator<BcfCommentModel, BcfCommentData>
{

	@Override
	public void populate(final BcfCommentModel source, final BcfCommentData target) throws ConversionException
	{
		target.setText(source.getText());
		target.setStartDate(source.getStartDate());
		target.setEndDate(source.getEndDate());
	}
}
