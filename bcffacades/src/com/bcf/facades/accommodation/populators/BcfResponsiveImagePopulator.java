/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


public class BcfResponsiveImagePopulator implements Populator<MediaModel, ImageData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	public void populate(final MediaModel mediaModel, final ImageData imageData) throws ConversionException
	{
		imageData.setUrl(getBcfResponsiveMediaStrategy().getResponsiveJson(mediaModel));
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	public void setBcfResponsiveMediaStrategy(
			final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
