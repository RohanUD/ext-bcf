/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.populators;

import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelfacades.populators.AccommodationOfferingBasicPopulator;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.PropertyCategoryType;
import com.bcf.core.model.accommodation.BcfCommentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.accommodation.BcfCommentData;
import com.bcf.facades.accommodation.reviews.CustomerReviewData;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class BcfAccommodationOfferingBasicPopulator extends AccommodationOfferingBasicPopulator
{
	private static final Logger LOG = Logger.getLogger(BcfAccommodationOfferingBasicPopulator.class);
	private AbstractPopulatingConverter<BcfCommentModel, BcfCommentData> bcfCommentConverter;
	private I18NService i18nService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter;
	private TripAdvisorService tripAdvisorService;
	private static final String HOTEL_DETAILS = "Hotel-Details";

	@Override
	public void populate(final AccommodationOfferingModel source, final PropertyData target) throws ConversionException
	{
		target.setAccommodationOfferingCode(source.getCode());
		target.setAccommodationOfferingName(source.getName());
		target.setDescription(source.getDescription());
		target.setAddress(getAddressData(source));
		target.setAwards(getAwards(source));
		target.setPosition(getPosition(source.getLocation()));
		target.setAmenities(getPropertyFacilityConverter().convertAll(source.getActivePropertyFacility()));
		target.setPropertyInformation(source.getPropertyInformation());
		populateImageGallery(source, target);
		target.setBcfComments(getBcfCommentConverter().convertAll(source.getBcfComments()));
		target.setReviewLocationId(source.getReviewLocationId());
		target.setPropertyCategoryTypes(
				source.getPropertyCategoryTypes().stream().map(PropertyCategoryType::getCode).collect(Collectors.toList()));
		if (Objects.nonNull(source.getTermsAndConditions(getI18nService().getCurrentLocale())))
		{
			target.setTermsAndConditions(source.getTermsAndConditions(getI18nService().getCurrentLocale()));
		}
		addCustomerReview(target);
	}

	private void addCustomerReview(final PropertyData target)
	{
		try
		{
			if (Objects.nonNull(target.getReviewLocationId()))
			{
				final TripAdvisorResponseData tripAdvisorResponseData = getTripAdvisorService()
						.getReviewInformation(target.getReviewLocationId());

				target.setCustomerReviewData(getCustomerReviewDataConverter().convert(tripAdvisorResponseData));
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage());
		}
	}

	@Override
	protected void addImagesInFormats(final MediaContainerModel mediaContainer, final ImageDataType imageType,
			final int galleryIndex, final List<ImageData> imagesList)
	{
		final Iterator imageFormats = getImageFormats().iterator();

		while (imageFormats.hasNext())
		{
			final String imageFormat = (String) imageFormats.next();
			final String mediaFormatQualifier = getImageFormatMapping().getMediaFormatQualifierForImageFormat(imageFormat);
			if (mediaFormatQualifier != null)
			{
				final MediaFormatModel mediaFormat = getMediaService().getFormat(mediaFormatQualifier);
				populateImagesForFormats(mediaContainer, imageType, galleryIndex, imagesList, imageFormat, mediaFormat);
			}

		}
	}

	private void populateImagesForFormats(final MediaContainerModel mediaContainer, final ImageDataType imageType,
			final int galleryIndex, final List<ImageData> imagesList, final String imageFormat, final MediaFormatModel mediaFormat)
	{
		if (mediaFormat != null)
		{
			try
			{
				final MediaModel media = getMediaContainerService().getMediaForFormat(mediaContainer, mediaFormat);
				if (media != null)
				{
					final ImageData imageData = getImageConverter().convert(media);
					imageData.setFormat(imageFormat);
					imageData.setImageType(imageType);
					if (ImageDataType.GALLERY.equals(imageType))
					{
						imageData.setGalleryIndex(galleryIndex);
						imageData.setUrl(bcfResponsiveMediaStrategy.getResponsiveJson(mediaContainer));
					}
					imagesList.add(imageData);
				}
			}
			catch (final ModelNotFoundException var11)
			{
				LOG.debug("Media model not found.", var11);
			}
		}
	}

	private void populateImageGallery(final AccommodationOfferingModel accommodationOfferingModel, final PropertyData propertyData)
	{
		if (StringUtils.isEmpty(accommodationOfferingModel.getGalleryCode()))
		{
			propertyData.setImages(Collections.emptyList());
		}
		else
		{
			final AccommodationOfferingGalleryModel accommodationOfferingGallery = getAccommodationOfferingGalleryService()
					.getAccommodationOfferingGallery(accommodationOfferingModel.getGalleryCode(), getCatalogVersion());
			final List<MediaContainerModel> mediaContainers = accommodationOfferingGallery.getGallery();
			populateImageData(propertyData, accommodationOfferingGallery, mediaContainers);

		}
	}

	private void populateImageData(final PropertyData propertyData,
			final AccommodationOfferingGalleryModel accommodationOfferingGallery, final List<MediaContainerModel> mediaContainers)
	{
		if (CollectionUtils.isNotEmpty(mediaContainers))
		{
			final List<ImageData> imageList = new ArrayList();
			int galleryIndex = 0;
			final Iterator var8 = mediaContainers.iterator();

			while (var8.hasNext())
			{
				final MediaContainerModel mediaContainer = (MediaContainerModel) var8.next();
				addImagesInFormats(mediaContainer, ImageDataType.GALLERY, galleryIndex++, imageList);
			}

			final MediaModel listImage = accommodationOfferingGallery.getListImage();
			ImageData imageData;
			if (Objects.nonNull(listImage))
			{
				imageData = new ImageData();
				imageData.setImageType(ImageDataType.PRIMARY);
				imageData.setUrl(getBcfResponsiveMediaStrategy().getDataMediaForFormat(listImage, HOTEL_DETAILS));
				imageData.setAltText(listImage.getAltText());
				imageList.add(imageData);
			}

			final Iterator var9 = imageList.iterator();

			while (var9.hasNext())
			{
				imageData = (ImageData) var9.next();
				if (imageData.getAltText() == null)
				{
					imageData.setAltText(propertyData.getAccommodationOfferingName());
				}
			}

			propertyData.setImages(imageList);
		}
	}


	/**
	 * @return the bcfCommentConverter
	 */
	protected AbstractPopulatingConverter<BcfCommentModel, BcfCommentData> getBcfCommentConverter()
	{
		return bcfCommentConverter;
	}

	/**
	 * @param bcfCommentConverter the bcfCommentConverter to set
	 */
	@Required
	public void setBcfCommentConverter(final AbstractPopulatingConverter<BcfCommentModel, BcfCommentData> bcfCommentConverter)
	{
		this.bcfCommentConverter = bcfCommentConverter;
	}

	protected I18NService getI18nService()
	{
		return i18nService;
	}

	@Required
	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

	protected BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	protected Converter<TripAdvisorResponseData, CustomerReviewData> getCustomerReviewDataConverter()
	{
		return customerReviewDataConverter;
	}

	@Required
	public void setCustomerReviewDataConverter(
			final Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter)
	{
		this.customerReviewDataConverter = customerReviewDataConverter;
	}

	protected TripAdvisorService getTripAdvisorService()
	{
		return tripAdvisorService;
	}

	@Required
	public void setTripAdvisorService(final TripAdvisorService tripAdvisorService)
	{
		this.tripAdvisorService = tripAdvisorService;
	}
}
