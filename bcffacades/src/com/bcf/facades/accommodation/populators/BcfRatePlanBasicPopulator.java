/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.populators;

import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.facades.accommodation.populators.RatePlanBasicPopulator;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import java.util.Collections;


public class BcfRatePlanBasicPopulator extends RatePlanBasicPopulator
{
	@Override
	public void populate(final RatePlanModel ratePlanModel, final RatePlanData ratePlanData) throws ConversionException
	{
		ratePlanData.setCode(ratePlanModel.getCode());
		ratePlanData.setMaxLengthOfStay(ratePlanModel.getMaxStay());
		ratePlanData.setMinLengthOfStay(ratePlanModel.getMinStay());
		ratePlanData.setDescription(ratePlanModel.getDescription());
		ratePlanData.setName(ratePlanModel.getName());
		ratePlanData.setOccupancies(getGuestOccupancyConverter().convertAll(ratePlanModel.getGuestOccupancies()));
		ratePlanData.setRatePlanInclusions(Collections.emptyList());
		ratePlanData.setCancelPenalties(Collections.emptyList());
		ratePlanData.setMealTypes(Collections.emptyList());
	}
}
