/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.DealAccommodationAvailabilityHandler;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.bcffacades.BcfProductFacade;


public class BcfDealAccommodationAvailabilityHandler extends DealAccommodationAvailabilityHandler
{
	private BcfProductFacade bcfProductFacade;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
				.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOfferingModel = getAccommodationOfferingService()//NOSONAR
				.getAccommodationOffering(accommodationOfferingCode);
		Instant instant = Instant.ofEpochMilli(availabilityRequestData.getCriterion().getStayDateRange().getStartTime().getTime());
		final LocalDateTime startDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());//NOSONAR
		instant = Instant.ofEpochMilli(availabilityRequestData.getCriterion().getStayDateRange().getEndTime().getTime());
		final LocalDateTime endDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());//NOSONAR
		accommodationAvailabilityResponseData.getRoomStays().forEach(roomStay ->
				roomStay.getRoomTypes().forEach(roomType -> {
					final AccommodationModel accommodation = (AccommodationModel) getProductService()//NOSONAR
							.getProductForCode(roomType.getCode());
					final List<Integer> stockLevels = new ArrayList<>();//NOSONAR
					Stream.iterate(startDate, date -> date.plusDays(1L)).limit(ChronoUnit.DAYS.between(startDate, endDate))
							.forEach(date -> collectAvailability(date, stockLevels, accommodationOfferingModel, accommodation,
									availabilityRequestData.getCriterion().getBundleTemplateId()));
					updateAccommodationData(roomStay, stockLevels, null);
				})
		);
	}

	@Override
	protected void collectAvailability(final LocalDateTime date, final List<Integer> stockLevels,
			final AccommodationOfferingModel accommodationOfferingModel, final AccommodationModel accommodation,
			final String accommodationBundleId)
	{
		final Date convertedDate = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
		final Collection<WarehouseModel> warehouses = Collections.singletonList(accommodationOfferingModel);
		final StockData stockData = getBcfProductFacade().getStockData(accommodation, convertedDate, warehouses);
		stockLevels.add(stockData.getStockLevel().intValue());
	}

	protected void updateAccommodationData(final RoomStayData roomStay, final List<Integer> stockLevels,
			final Integer defaultValue)
	{
		final Optional<RatePlanData> ratePlanData = roomStay.getRatePlans().stream().findFirst();
		if (ratePlanData.isPresent() && CollectionUtils.isNotEmpty(ratePlanData.get().getRoomRates()))
		{
			if (CollectionUtils.isNotEmpty(stockLevels))
			{
				ratePlanData.get().setAvailableQuantity(stockLevels.stream().mapToInt(Integer::intValue).min().getAsInt());
			}
			else if (Objects.nonNull(defaultValue))
			{
				ratePlanData.get().setAvailableQuantity(defaultValue);
			}
		}
	}

	/**
	 * @return the bcfProductFacade
	 */
	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	/**
	 * @param bcfProductFacade the bcfProductFacade to set
	 */
	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}
}
