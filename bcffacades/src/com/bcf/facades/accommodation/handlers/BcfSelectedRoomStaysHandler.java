/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.SelectedRoomStaysHandler;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;


public class BcfSelectedRoomStaysHandler extends SelectedRoomStaysHandler
{
	private BcfAccommodationService bcfAccommodationService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final List<String> propertyAccommodations = availabilityRequestData.getCriterion().getAccommodationReference()
				.getPropertyAccomodationCodes();
		final StayDateRangeData stayDateRange = availabilityRequestData.getCriterion().getStayDateRange();
		final List<RoomStayData> roomStays = new ArrayList<>(availabilityRequestData.getCriterion().getRoomStayCandidates().size());
		availabilityRequestData.getCriterion().getRoomStayCandidates().forEach(roomStayCandidate -> {
			if (StringUtils.isEmpty(roomStayCandidate.getAccommodationCode()))
			{
				final ReservedRoomStayData roomStayData = new ReservedRoomStayData();
				roomStayData.setRoomStayRefNumber(roomStayCandidate.getRoomStayCandidateRefNumber());
				roomStays.add(roomStayData);
			}

			if (CollectionUtils.isNotEmpty(propertyAccommodations)
					&& propertyAccommodations.contains(roomStayCandidate.getAccommodationCode()))
			{
				final AccommodationModel accommodation = getBcfAccommodationService()
						.getAccommodation(roomStayCandidate.getAccommodationCode());
				roomStays.add(createRoomStayData(accommodation, stayDateRange, roomStayCandidate));
			}

		});
		accommodationAvailabilityResponseData.setRoomStays(roomStays);
	}

	protected ReservedRoomStayData createRoomStayData(AccommodationModel accommodation, StayDateRangeData stayDateRange, RoomStayCandidateData roomStayCandidateData) {
		ReservedRoomStayData roomStay = new ReservedRoomStayData();
		roomStay.setRoomTypes(this.getRoomTypeConverter().convertAll(Collections.singletonList(accommodation)));
		roomStay.setRoomStayRefNumber(roomStayCandidateData.getRoomStayCandidateRefNumber());
		roomStay.setCheckInDate(stayDateRange.getStartTime());
		roomStay.setCheckOutDate(stayDateRange.getEndTime());
		roomStay.setGuestCounts(roomStayCandidateData.getPassengerTypeQuantityList());
		roomStay.setServices(roomStayCandidateData.getServices());
		roomStay.setAccommodationCode(accommodation.getCode());
		roomStay.setMaxOccupancy(accommodation.getMaxOccupancyCount());
		return roomStay;
	}

	/**
	 * @return the bcfAccommodationService
	 */
	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	/**
	 * @param bcfAccommodationService
	 *           the bcfAccommodationService to set
	 */
	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

}
