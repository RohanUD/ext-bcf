/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.PositionData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.PropertyDataBasicHandler;
import de.hybris.platform.travelservices.model.accommodation.AccommodationOfferingGalleryModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingGalleryService;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.PropertyCategoryType;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.accommodation.reviews.CustomerReviewData;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class BcfPropertyDataBasicHandler extends PropertyDataBasicHandler
{
	private static final Logger LOG = Logger.getLogger(BcfPropertyDataBasicHandler.class);
	private static final String HOTEL_LISTING = "Hotel-Listing";
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private TimeService timeService;
	private AccommodationOfferingGalleryService accommodationOfferingGalleryService;
	private MediaContainerService mediaContainerService;
	private CatalogVersionService catalogVersionService;
	private AccommodationOfferingService accommodationOfferingService;
	private Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter;
	private TripAdvisorService tripAdvisorService;
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Override
	protected void handlingAttributes(final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final PropertyData propertyData)
	{
		final Optional<Map.Entry<Integer, List<AccommodationOfferingDayRateData>>> optionalEntry = dayRatesForRoomStayCandidate
				.entrySet()
				.stream().findAny();
		if (!optionalEntry.isPresent())
		{
			return;
		}
		final AccommodationOfferingDayRateData accommodationOfferingDayRateData = optionalEntry.get().getValue().get(0);
		propertyData.setAddress(accommodationOfferingDayRateData.getAddressData());
		if (Objects.nonNull(accommodationOfferingDayRateData.getLatitude()) && Objects
				.nonNull(accommodationOfferingDayRateData.getLongitude()))
		{
			final PositionData positionData = new PositionData();
			positionData.setLatitude(accommodationOfferingDayRateData.getLatitude());
			positionData.setLongitude(accommodationOfferingDayRateData.getLongitude());
			propertyData.setPosition(positionData);
		}

		final String accommodationOfferingCode = accommodationOfferingDayRateData.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOffering = getAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);
		propertyData.setAccommodationOfferingCode(accommodationOfferingCode);
		propertyData.setAccommodationOfferingName(accommodationOfferingDayRateData.getAccommodationOfferingName());
		propertyData.setDescription(accommodationOffering.getDescription());
		propertyData.setChainCode(accommodationOfferingDayRateData.getChainCode());
		propertyData.setChainName(accommodationOfferingDayRateData.getChainName());
		propertyData.setPromoted(accommodationOfferingDayRateData.getBoosted());
		propertyData.setLocation(getBcfTravelLocationFacade().getLocation(accommodationOfferingDayRateData.getLocationCodes()));
		propertyData.setPropertyCategoryTypes(
				accommodationOffering.getPropertyCategoryTypes().stream().map(PropertyCategoryType::getCode)
						.collect(Collectors.toList()));
		if (StringUtils.isNotEmpty(accommodationOfferingDayRateData.getMainImageMediaUrl()))
		{
			final Collection<CatalogVersionModel> sessionCatalogVersions = getCatalogVersionService().getSessionCatalogVersions();
			final CatalogVersionModel activeProductCatalogVersion = sessionCatalogVersions.stream()
					.filter(cvm -> cvm.getCatalog().getClass().isAssignableFrom(CatalogModel.class)).findAny().orElse(null);

			populateImageData(propertyData, accommodationOfferingDayRateData, activeProductCatalogVersion);
		}
		final String reviewLocationId = accommodationOfferingDayRateData.getReviewLocationId();
		if (Objects.nonNull(accommodationOfferingDayRateData.getReviewLocationId()))
		{
			propertyData.setReviewLocationId(reviewLocationId);
			propertyData.setIsDealOfTheDay(isDealOfTheDay(accommodationOfferingDayRateData));
		}

		try
		{
			if (Objects.nonNull(propertyData.getReviewLocationId()))
			{
				final TripAdvisorResponseData tripAdvisorResponseData = getTripAdvisorService()
						.getReviewInformation(propertyData.getReviewLocationId());

				propertyData.setCustomerReviewData(getCustomerReviewDataConverter().convert(tripAdvisorResponseData));
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage());
		}

	}

	private void populateImageData(final PropertyData propertyData,
			final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			final CatalogVersionModel activeProductCatalogVersion)
	{
		if (Objects.isNull(activeProductCatalogVersion))
		{
			LOG.error("No activeProductCatalogVersion is present");

		}
		else
		{
			final AccommodationOfferingGalleryModel accommodationOfferingGallery = getAccommodationOfferingGalleryService()
					.getAccommodationOfferingGallery(accommodationOfferingDayRateData.getMainImageMediaUrl(),
							activeProductCatalogVersion);
			if (Objects.isNull(accommodationOfferingGallery))
			{
				LOG.error("No accommodationOfferingGallery is present");
			}
			else
			{
				if (CollectionUtils.isNotEmpty(accommodationOfferingGallery.getGallery()))
				{
					final List<MediaContainerModel> mediaContainer = accommodationOfferingGallery.getGallery();
					final List<ImageData> images = new ArrayList<>();
					for (final MediaContainerModel media : mediaContainer)
					{
						final ImageData imageData = new ImageData();
						imageData.setImageType(ImageDataType.PRIMARY);
						imageData.setUrl(getBcfResponsiveMediaStrategy().getDataMediaForFormat(media, HOTEL_LISTING));
						imageData.setAltText(media.getMedias().stream().findFirst().get().getAltText());
						images.add(imageData);
					}
					propertyData.setImages(images);
				}
			}
		}
	}

	private boolean isDealOfTheDay(final AccommodationOfferingDayRateData accommodationOfferingDayRateData)
	{
		if (accommodationOfferingDayRateData.isIsDealOfTheDay())
		{
			final Date currentTime = getTimeService().getCurrentTime();
			return currentTime.after(accommodationOfferingDayRateData.getDealOfTheDayStartDate()) && currentTime
					.before(accommodationOfferingDayRateData.getDealOfTheDayEndDate());
		}
		return false;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected AccommodationOfferingGalleryService getAccommodationOfferingGalleryService()
	{
		return accommodationOfferingGalleryService;
	}

	@Required
	public void setAccommodationOfferingGalleryService(
			final AccommodationOfferingGalleryService accommodationOfferingGalleryService)
	{
		this.accommodationOfferingGalleryService = accommodationOfferingGalleryService;
	}

	protected MediaContainerService getMediaContainerService()
	{
		return mediaContainerService;
	}

	@Required
	public void setMediaContainerService(final MediaContainerService mediaContainerService)
	{
		this.mediaContainerService = mediaContainerService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(
			final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected TripAdvisorService getTripAdvisorService()
	{
		return tripAdvisorService;
	}

	@Required
	public void setTripAdvisorService(final TripAdvisorService tripAdvisorService)
	{
		this.tripAdvisorService = tripAdvisorService;
	}

	protected Converter<TripAdvisorResponseData, CustomerReviewData> getCustomerReviewDataConverter()
	{
		return customerReviewDataConverter;
	}

	@Required
	public void setCustomerReviewDataConverter(
			final Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter)
	{
		this.customerReviewDataConverter = customerReviewDataConverter;
	}

	public BcfTravelLocationFacade getBcfTravelLocationFacade()
	{
		return bcfTravelLocationFacade;
	}

	@Required
	public void setBcfTravelLocationFacade(final BcfTravelLocationFacade bcfTravelLocationFacade)
	{
		this.bcfTravelLocationFacade = bcfTravelLocationFacade;
	}
}
