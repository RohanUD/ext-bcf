/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers;

import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.DealRoomRatesHandler;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BcfDealRoomRatesHandler extends DealRoomRatesHandler
{
	@Override
	protected List<RoomRateData> getRoomRates(final LocalDateTime date, final String bundleTemplateId)
	{
		final Date convertedDate = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
		final RoomRateProductModel roomRate = ((AccommodationBundleTemplateModel) this.getBundleTemplateService()
				.getBundleTemplateForCode(bundleTemplateId))
				.getRoomRateProduct();
		final List<RoomRateData> roomRates = new ArrayList();
		if (validateRoomRateAgainstDate(convertedDate, roomRate))
		{
			this.createRoomRateData(roomRates, roomRate, convertedDate);
		}
		return roomRates;
	}

	@Override
	public boolean validateRoomRateAgainstDate(final Date date, final ProductModel roomRate)
	{
		return super.validateRoomRateAgainstDate(date, roomRate);
	}
}
