/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;


public class AccommodationPerPersonPriceHandler implements AccommodationDetailsHandler
{

	private static final String[] DEFAULT_INCLUDED_GUEST_TYPES = { "adult", "child" };
	private static final String JOURNEY_REF_NUMBER = "journeyRefNum";
	private static final String COMMA = ",";

	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private SessionService sessionService;
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		int noOfPersons = 0;
		int prevNoOfGuests = 0;

		final String propertyValue = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfFacadesConstants.GUEST_TYPES_FOR_PERPERSON_PRICE);
		final String[] includedGuestTypes = Objects.isNull(propertyValue) ? DEFAULT_INCLUDED_GUEST_TYPES
				: StringUtils.split(propertyValue, COMMA);
		for (final RoomStayCandidateData roomStayCandidateData : availabilityRequestData.getCriterion().getRoomStayCandidates())
		{
			noOfPersons += roomStayCandidateData.getPassengerTypeQuantityList().stream().filter(
					passengerTypeQuantityData -> Arrays.asList(includedGuestTypes)
							.contains(passengerTypeQuantityData.getPassengerType().getCode()))
					.mapToInt(PassengerTypeQuantityData::getQuantity).sum();
		}

		final CartModel cart = bcfTravelCartService.getExistingSessionAmendedCart();

		if (bcfTravelCartService.isAmendmentCart(cart))
		{
			prevNoOfGuests = getBcfTravelCartService()
					.getNumberOfGuestsInCart(cart, getSessionService().getAttribute(JOURNEY_REF_NUMBER) != null ?
							getSessionService().getAttribute(JOURNEY_REF_NUMBER) :
							0);
		}
		noOfPersons = (noOfPersons == prevNoOfGuests) ? noOfPersons : (noOfPersons - prevNoOfGuests);
		addPerPersonPriceToAccommodation(accommodationAvailabilityResponseData, noOfPersons);
	}

	protected void addPerPersonPriceToAccommodation(
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData,
			final int noOfPersons)
	{
		final List<RoomStayData> roomStays = accommodationAvailabilityResponseData.getRoomStays();
		for (final RoomStayData roomStayData : roomStays)
		{
			for (final RatePlanData ratePlan : roomStayData.getRatePlans())
			{
				final double totalValue = ratePlan.getActualRate().getValue().doubleValue();
				final double perPersonPrice = totalValue / noOfPersons;
				final PriceData perPersonPlanPrice = getTravelCommercePriceFacade().createPriceData(perPersonPrice);
				ratePlan.setPerPersonPlanPrice(perPersonPlanPrice);
			}
		}
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
