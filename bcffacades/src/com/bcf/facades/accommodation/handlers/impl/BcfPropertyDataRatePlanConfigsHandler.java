/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.PropertyDataRatePlanConfigsHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;


public class BcfPropertyDataRatePlanConfigsHandler extends PropertyDataRatePlanConfigsHandler
{
	@Override
	public void handle(final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final AccommodationSearchRequestData accommodationSearchRequest, final PropertyData propertyData)
	{
		if (CollectionUtils.size(accommodationSearchRequest.getCriterion().getRoomStayCandidates()) != CollectionUtils
				.size(dayRatesForRoomStayCandidate.entrySet()))
		{
			return;
		}

		handlingAttributes(dayRatesForRoomStayCandidate, propertyData);
	}

	@Override
	protected void handlingAttributes(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMap,
			final PropertyData propertyData)
	{
		final List<String> validRatePlanConfigs = new ArrayList<>();
		for (final Map.Entry<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMapEntry : dayRatesForRoomStayCandidateRefNumberMap
				.entrySet())
		{
			final AccommodationOfferingDayRateData firstAccommodationOfferingDayRateData = dayRatesForRoomStayCandidateRefNumberMapEntry
					.getValue().stream().findFirst().get();
			validRatePlanConfigs.addAll(firstAccommodationOfferingDayRateData.getRatePlanConfigs());
		}
		propertyData.setRatePlanConfigs(validRatePlanConfigs);
	}
}
