/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.AccommodationTotalPriceHandler;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;


public class BcfAccommodationTotalPriceHandler extends AccommodationTotalPriceHandler
{

	@Override
	protected void populateBaseRoomStayPrice(final ReservedRoomStayData reservedRoomStayData)
	{
		if (!CollectionUtils.isEmpty(reservedRoomStayData.getRatePlans()))
		{
			BigDecimal totalActualPrice = BigDecimal.valueOf(0L);
			BigDecimal totalBasePrice = BigDecimal.valueOf(0L);
			BigDecimal totalWasPrice = BigDecimal.valueOf(0L);
			BigDecimal totalDisounts = BigDecimal.valueOf(0L);
			final List<TaxData> totalTaxes = new ArrayList();
			final Iterator var8 = reservedRoomStayData.getRatePlans().iterator();

			while (var8.hasNext())
			{
				final RatePlanData ratePlanData = (RatePlanData) var8.next();
				final Iterator var10 = ratePlanData.getRoomRates().iterator();

				while (var10.hasNext())
				{
					final RoomRateData roomRateData = (RoomRateData) var10.next();
					final RateData roomRate = roomRateData.getRate();
					totalBasePrice = totalBasePrice.add(roomRate.getBasePrice().getValue());
					totalActualPrice = totalActualPrice.add(roomRate.getActualRate().getValue());
					totalWasPrice = totalWasPrice.add(roomRate.getWasRate().getValue());
					if (Objects.nonNull(roomRate.getTotalDiscount()))
					{
						totalDisounts = totalDisounts.add(roomRate.getTotalDiscount().getValue());
					}
					if (CollectionUtils.isNotEmpty(roomRate.getTaxes()))
					{
						totalTaxes.addAll(roomRate.getTaxes());
					}
				}
			}

			final String currencyIso = reservedRoomStayData.getFromPrice().getCurrencyIso();
			final RateData rateData = new RateData();
			rateData
					.setBasePrice(this.getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalBasePrice, currencyIso));
			rateData.setActualRate(
					this.getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalActualPrice, currencyIso));
			rateData.setWasRate(this.getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalWasPrice, currencyIso));
			rateData.setTotalDiscount(
					this.getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, totalDisounts, currencyIso));
			rateData.setTaxes(totalTaxes);
			rateData.setTotalTax(this.createTaxData(totalTaxes.stream().mapToDouble((tax) -> {
				return tax.getPrice().getValue().doubleValue();
			}).sum(), currencyIso));
			reservedRoomStayData.setBaseRate(rateData);
		}
	}

	private TaxData createTaxData(final Double value, final String currencyIso)
	{
		final TaxData taxData = new TaxData();
		taxData.setPrice(
				this.getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, BigDecimal.valueOf(value), currencyIso));
		return taxData;
	}
}
