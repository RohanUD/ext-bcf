/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.ExtraGuestOccupancyData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.AccommodationPriceHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.RatePlanService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class BcfAccommodationPriceHandler extends AccommodationPriceHandler
{
	private BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade;
	private RatePlanService ratePlanService;
	private AccommodationOfferingService accommodationOfferingService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String currencyIso = getCurrentCurrency(availabilityRequestData);
		final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
				.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOfferingModel = getAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);

		for (final RoomStayData roomStayData : accommodationAvailabilityResponseData.getRoomStays())
		{
			populateRates(accommodationOfferingModel, roomStayData.getRatePlans());

			final BigDecimal fromPrice = roomStayData.getRatePlans().stream().map(ratePlan -> ratePlan.getActualRate().getValue())
					.min(BigDecimal::compareTo).orElse(null);
			if (Objects.nonNull(fromPrice))
			{
				roomStayData.setFromPrice(getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, fromPrice, currencyIso));
			}
		}
	}

	private void populateRates(final AccommodationOfferingModel accommodationOfferingModel, final List<RatePlanData> ratePlans)
	{
		for (final RatePlanData ratePlan : ratePlans)
		{
			BigDecimal wasRate = BigDecimal.ZERO;
			BigDecimal actualRate = BigDecimal.ZERO;
			BigDecimal discounts = BigDecimal.ZERO;
			for (final RoomRateData roomRateData : ratePlan.getRoomRates())
			{
				populatePricesForRoomRate(roomRateData, 1l, ratePlan, accommodationOfferingModel.getLocation());
				wasRate = wasRate.add(roomRateData.getRate().getWasRate().getValue());
				actualRate = actualRate.add(roomRateData.getRate().getActualRate().getValue());
				discounts = addRoomRateDiscount(discounts, roomRateData);
				if (CollectionUtils.isNotEmpty(roomRateData.getExtraGuestOccupancies()))
				{
					for (final ExtraGuestOccupancyData extraGuestOccupancyData : roomRateData.getExtraGuestOccupancies())
					{
						populatePricesForExtraGuestOccupancy(extraGuestOccupancyData, ratePlan,
								accommodationOfferingModel.getLocation(), roomRateData.getStayDateRange().getStartTime());
						wasRate = wasRate.add(extraGuestOccupancyData.getRate().getWasRate().getValue());
						actualRate = actualRate.add(extraGuestOccupancyData.getRate().getActualRate().getValue());
						discounts = addExtraGuestOccupancyDiscount(discounts, extraGuestOccupancyData);
					}
				}
			}

			ratePlan.setActualRate(createPriceData(actualRate));
			ratePlan.setWasRate(createPriceData(wasRate));
			ratePlan.setTotalDiscount(createPriceData(discounts));
		}
	}

	private BigDecimal addExtraGuestOccupancyDiscount(BigDecimal discounts,
			final ExtraGuestOccupancyData extraGuestOccupancyData)
	{
		if (Objects.nonNull(extraGuestOccupancyData.getRate().getTotalDiscount()))
		{
			discounts = discounts.add(extraGuestOccupancyData.getRate().getTotalDiscount().getValue());
		}
		return discounts;
	}

	private BigDecimal addRoomRateDiscount(BigDecimal discounts, final RoomRateData roomRateData)
	{
		if (Objects.nonNull(roomRateData.getRate().getTotalDiscount()))
		{
			discounts = discounts.add(roomRateData.getRate().getTotalDiscount().getValue());
		}
		return discounts;
	}

	protected PriceData createPriceData(final BigDecimal dayPrice)
	{
		return getBcfTravelCommercePriceFacade().createPriceData(PrecisionUtil.round(dayPrice.doubleValue()));
	}

	/**
	 * Populate prices for room rate.
	 *
	 * @param roomRate     the room rate
	 * @param quantity
	 * @param ratePlanData the currency iso
	 */
	protected void populatePricesForRoomRate(final RoomRateData roomRate, final long quantity, final RatePlanData ratePlanData,
			final LocationModel location)
	{
		final ProductModel product = getProductService().getProductForCode(roomRate.getCode());
		if (!(product instanceof RoomRateProductModel))
		{
			return;
		}

		final RoomRateProductModel roomRateProduct = (RoomRateProductModel) product;
		final RatePlanModel ratePlan = roomRateProduct.getSupercategories().stream().filter(RatePlanModel.class::isInstance)
				.map(RatePlanModel.class::cast).findFirst().orElse(null);
		if (Objects.nonNull(ratePlan))
		{
			roomRate.setRate(getBcfTravelCommercePriceFacade().calculateRateData(roomRateProduct, quantity, ratePlan, location));
		}
	}

	/**
	 * populate price for extra guest occupancies
	 *
	 * @param extraGuestOccupancyData
	 * @param ratePlanData
	 * @param location
	 * @param stayDate
	 */
	protected void populatePricesForExtraGuestOccupancy(final ExtraGuestOccupancyData extraGuestOccupancyData,
			final RatePlanData ratePlanData, final LocationModel location, final Date stayDate)
	{
		final ExtraGuestOccupancyProductModel product = (ExtraGuestOccupancyProductModel) getProductService()
				.getProductForCode(extraGuestOccupancyData.getCode());
		final RatePlanModel ratePlan = getRatePlanService().getRatePlanForCode(ratePlanData.getCode());
		extraGuestOccupancyData.setRate(getBcfTravelCommercePriceFacade()
				.calculateRateData(product, extraGuestOccupancyData.getQuantity().longValue(),
						extraGuestOccupancyData.getPassengerType(), ratePlan, location, stayDate));
	}

	/**
	 * @return the bcfTravelCommercePriceFacade
	 */
	protected BCFTravelCommercePriceFacade getBcfTravelCommercePriceFacade()
	{
		return bcfTravelCommercePriceFacade;
	}

	/**
	 * @param bcfTravelCommercePriceFacade the bcfTravelCommercePriceFacade to set
	 */
	@Required
	public void setBcfTravelCommercePriceFacade(final BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade)
	{
		this.bcfTravelCommercePriceFacade = bcfTravelCommercePriceFacade;
	}

	/**
	 * @return the ratePlanService
	 */
	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	/**
	 * @param ratePlanService the ratePlanService to set
	 */
	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}

	/**
	 * @return the accommodationOfferingService
	 */
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	/**
	 * @param accommodationOfferingService the accommodationOfferingService to set
	 */
	@Required
	public void setAccommodationOfferingService(final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}
}
