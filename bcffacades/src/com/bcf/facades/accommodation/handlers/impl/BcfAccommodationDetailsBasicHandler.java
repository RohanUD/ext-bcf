/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.AccommodationDetailsBasicHandler;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.accommodation.BcfCommentData;
import com.bcf.facades.accommodation.reviews.CustomerReviewData;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class BcfAccommodationDetailsBasicHandler extends AccommodationDetailsBasicHandler
{
	private static final Logger LOG = org.apache.log4j.Logger
			.getLogger(BcfAccommodationDetailsBasicHandler.class);
	private TripAdvisorService tripAdvisorService;
	private Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		super.handle(availabilityRequestData, accommodationAvailabilityResponseData);

		final PropertyData property = accommodationAvailabilityResponseData.getAccommodationReference();
		if (Objects.nonNull(property.getReviewLocationId()))
		{
			property.setCustomerReviewData(getCustomerReviewData(property));
		}
		property.setBcfComments(getValidBcfCommentDatas(availabilityRequestData, property));
	}

	protected CustomerReviewData getCustomerReviewData(final PropertyData propertyData)
	{
		try
		{
			final TripAdvisorResponseData tripAdvisorResponseData = getTripAdvisorService()
					.getReviewInformation(propertyData.getReviewLocationId());
			return getCustomerReviewDataConverter().convert(tripAdvisorResponseData);
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage());
		}
		return null;
	}

	protected List<BcfCommentData> getValidBcfCommentDatas(
			final AccommodationAvailabilityRequestData availabilityRequestData,
			final PropertyData property)
	{
		final Set<BcfCommentData> bcfComments = new HashSet<>();
		final StayDateRangeData stayDateRange = availabilityRequestData.getCriterion().getStayDateRange();

		for (final BcfCommentData commentData : property.getBcfComments())
		{
			for (Date dateToCheck = stayDateRange.getStartTime(); dateToCheck
					.before(stayDateRange.getEndTime()); dateToCheck = TravelDateUtils.addDays(dateToCheck, 1))
			{
				if (BCFDateUtils.isBetweenDatesInclusive(dateToCheck, commentData.getStartDate(), commentData.getEndDate()))
				{
					bcfComments.add(commentData);
				}
			}
		}
		return new ArrayList<>(bcfComments);
	}



	protected Converter<TripAdvisorResponseData, CustomerReviewData> getCustomerReviewDataConverter()
	{
		return customerReviewDataConverter;
	}

	@Required
	public void setCustomerReviewDataConverter(
			final Converter<TripAdvisorResponseData, CustomerReviewData> customerReviewDataConverter)
	{
		this.customerReviewDataConverter = customerReviewDataConverter;
	}

	protected TripAdvisorService getTripAdvisorService()
	{
		return tripAdvisorService;
	}

	@Required
	public void setTripAdvisorService(final TripAdvisorService tripAdvisorService)
	{
		this.tripAdvisorService = tripAdvisorService;
	}
}
