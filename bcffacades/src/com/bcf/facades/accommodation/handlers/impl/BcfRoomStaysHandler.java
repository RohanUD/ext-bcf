/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomTypeData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.RoomStaysHandler;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BcfRatePlanUtil;


public class BcfRoomStaysHandler extends RoomStaysHandler
{
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
				.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOffering = getBcfAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);
		List<AccommodationModel> accommodations = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(accommodationOffering.getAccommodations()))
		{
			accommodations = accommodationOffering.getAccommodations().stream()
					.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
					.collect(Collectors.toList());
		}

		final StayDateRangeData stayDateRange = availabilityRequestData.getCriterion().getStayDateRange();
		final List<RoomStayCandidateData> roomStayCandidates = availabilityRequestData.getCriterion().getRoomStayCandidates();

		final List<RoomStayData> updatedRoomStayDatas = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(roomStayCandidates))
		{
			final List<RoomStayData> roomStays = new ArrayList<>();

			for (final RoomStayCandidateData roomStayCandidateData : roomStayCandidates)
			{
				final int roomStayRefNumber = 0;
				for (final AccommodationModel accommodation : accommodations)
				{

					if(BcfRatePlanUtil.isValidForMinimumNightsStayRule(accommodation.getMinimumNightsStayRules(),stayDateRange))
					{
						final RoomStayData roomStayData = createRoomStayData(accommodation, roomStayRefNumber, stayDateRange);
						roomStayData.setRoomStayRefNumber(roomStayCandidateData.getRoomStayCandidateRefNumber());
						roomStays.add(roomStayData);
					}
				}

			}

			roomStays.forEach(roomStay -> {
				if(checkIfEligibleToAdd(roomStay, roomStayCandidates)){
					updatedRoomStayDatas.add(roomStay);
				}
			});
		}
		accommodationAvailabilityResponseData.setRoomStays(updatedRoomStayDatas);

		if (CollectionUtils.isEmpty(updatedRoomStayDatas))
		{
			final List<AccommodationModel> accommodationsModel = getAccommodationService()
					.getAccommodationForAccommodationOffering(accommodationOfferingCode);
			final List<RoomStayData> roomStays = new ArrayList(accommodations.size());
			accommodations.forEach((accommodation) -> {
				roomStays.add(this.createRoomStayData(accommodation, accommodationsModel.indexOf(accommodation), stayDateRange));
			});
			accommodationAvailabilityResponseData.setRoomStays(roomStays);
		}
	}

	protected RoomStayData createRoomStayData(AccommodationModel accommodation, Integer roomStayRefNumber, StayDateRangeData stayDateRange) {
		RoomStayData roomStay = new RoomStayData();
		roomStay.setRoomTypes(this.getRoomTypeConverter().convertAll(Collections.singletonList(accommodation)));
		roomStay.setRoomStayRefNumber(roomStayRefNumber);
		roomStay.setCheckInDate(stayDateRange.getStartTime());
		roomStay.setCheckOutDate(stayDateRange.getEndTime());
		roomStay.setAccommodationCode(accommodation.getCode());
		roomStay.setMaxOccupancy(accommodation.getMaxOccupancyCount());
		return roomStay;
	}


	protected boolean checkIfEligibleToAdd(
			 final RoomStayData roomStay,  List<RoomStayCandidateData> roomStayCandidates)
	{


		for(RoomStayCandidateData roomStayCandidate:roomStayCandidates)
		{
			int totalCount = 0;
			int adultCount = 0;
			final Optional<Integer> totalCountOption = roomStayCandidate.getPassengerTypeQuantityList().stream()
					.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum);

			if (totalCountOption.isPresent())
			{
				totalCount = totalCountOption.get().intValue();
			}

			final Optional<Integer> adultCountOption = roomStayCandidate.getPassengerTypeQuantityList().stream()
					.filter(passengerTypeQuantity -> passengerTypeQuantity.getPassengerType().getCode().equals(
							BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT))
					.map(passengerTypeQuantity -> passengerTypeQuantity.getQuantity()).reduce(Integer::sum);
			if (adultCountOption.isPresent())
			{
				adultCount = adultCountOption.get().intValue();
			}

			for (RoomTypeData roomTypeData : roomStay.getRoomTypes())
			{

				int adultMaxCount = roomTypeData.getOccupancies().stream()
						.filter(guestOccupancyData -> guestOccupancyData.getPassengerType().getCode()
								.equals(BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT))
						.map(guestOccupancyData -> guestOccupancyData.getQuantityMax()).reduce(Integer::sum).get();

				int totalMaxCount = roomTypeData.getOccupancies().stream()
						.map(guestOccupancyData -> guestOccupancyData.getQuantityMax()).reduce(Integer::sum).get();

				if (adultMaxCount < adultCount || totalMaxCount < totalCount)
				{
					return false;

				}

			}
		}

		return true;
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}
}
