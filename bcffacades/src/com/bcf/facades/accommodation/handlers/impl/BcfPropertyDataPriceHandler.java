/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.DayRateData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.search.RateRangeData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.PropertyHandler;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.AbstractDefaultPropertyHandler;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.RatePlanService;
import de.hybris.platform.util.PriceValue;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.services.accommodation.strategies.impl.RoomRatePriceCalculationStrategy;
import com.bcf.core.services.vacation.price.strategies.impl.VacationPriceCalculationStrategy;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.helper.impl.DefaultBcfTravelCartFacadeHelper;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class BcfPropertyDataPriceHandler extends AbstractDefaultPropertyHandler implements PropertyHandler
{

	private RatePlanService ratePlanService;
	private BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade;
	private RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy;
	private AccommodationOfferingService accommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;
	private ProductService productService;
	private CommonI18NService commonI18NService;
	private EnumerationService enumerationService;
	private PriceRowService priceRowService;
	private BcfProductFacade bcfProductFacade;
	private VacationPriceCalculationStrategy vacationPriceCalculationStrategy;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Override
	public void handle(final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final AccommodationSearchRequestData accommodationSearchRequest, final PropertyData propertyData)
	{

		final List<RoomStayCandidateData> roomStayCandidates = accommodationSearchRequest.getCriterion().getRoomStayCandidates();

		handlingAttributes(dayRatesForRoomStayCandidate, propertyData, roomStayCandidates, accommodationSearchRequest.getCriterion().getStayDateRange());

	}



	protected void handlingAttributes(final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final PropertyData propertyData, final List<RoomStayCandidateData> roomStayCandidates, StayDateRangeData stayDateRange)
	{
		BigDecimal wasRate = BigDecimal.ZERO;
		BigDecimal totalDiscount = BigDecimal.ZERO;
		BigDecimal actualPrice = BigDecimal.ZERO;

		final RateRangeData rateRange = new RateRangeData();
		final List<DayRateData> dayRates = getDayRateDatas(dayRatesForRoomStayCandidate, roomStayCandidates,stayDateRange);
		if(CollectionUtils.isEmpty(dayRates)){
			throw new SystemException(
					"no day rates found for accommodation offering");
		}
		for (final DayRateData dayRateData : dayRates)
		{
			actualPrice = actualPrice.add(dayRateData.getDailyActualRate().getValue());
			wasRate = wasRate.add(dayRateData.getDailyWasRate().getValue());
			if (Objects.nonNull(dayRateData.getDailyDiscount()))
			{
				totalDiscount = totalDiscount.add(dayRateData.getDailyDiscount().getValue());
				rateRange.setTotalDiscount(createPriceData(totalDiscount));
			}
		}

		rateRange.setDayRates(dayRates);
		rateRange.setCurrencyCode(getCommonI18NService().getCurrentCurrency().getIsocode());
		rateRange.setWasRate(createPriceData(wasRate));
		rateRange.setActualRate(createPriceData(actualPrice));
		propertyData.setRateRange(rateRange);
	}

	/**
	 * Gets the day rate datas.
	 *
	 * @param dayRatesForRoomStayCandidate the day rates for room stay candidate
	 * @return the day rate datas
	 */
	protected List<DayRateData> getDayRateDatas(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final List<RoomStayCandidateData> roomStayCandidates, StayDateRangeData stayDateRange)
	{
		final List<DayRateData> dayRates = new ArrayList<>();

		final List<Integer> dayRatesForRoomStayCandidateKeyList = new ArrayList<Integer>(dayRatesForRoomStayCandidate.keySet());
		for (int i = 0; i < dayRatesForRoomStayCandidateKeyList.size(); i++)
		{
			final int dayRatesForRoomStayCandidateKey = dayRatesForRoomStayCandidateKeyList.get(i);
			final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatas = dayRatesForRoomStayCandidate
					.get(dayRatesForRoomStayCandidateKey);

			final List<PassengerTypeQuantityData> passengerTypes = roomStayCandidates.get(i).getPassengerTypeQuantityList();

			dayRates.addAll(getDayRateData(accommodationOfferingDayRateDatas, roomStayCandidates,passengerTypes, stayDateRange));


		}
		return dayRates;
	}

	/**
	 * Gets the day rate data.
	 *
	 * @return the day rate data
	 */
	protected List<DayRateData> getDayRateData(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatas,
			final List<RoomStayCandidateData> roomStayCandidates,final List<PassengerTypeQuantityData> passengerTypes, StayDateRangeData stayDateRange)
	{
		final List<DayRateData> dayRates = new ArrayList<>();

		Map<Date, List<AccommodationOfferingDayRateData>> accommodationOfferingDayRateDataMap=accommodationOfferingDayRateDatas.stream().collect(Collectors.groupingBy(accommodationOfferingDayRateData->accommodationOfferingDayRateData.getDateOfStay()));

		for( Map.Entry<Date, List<AccommodationOfferingDayRateData>> accommodationOfferingDayRateDataMapEntry: accommodationOfferingDayRateDataMap.entrySet()){

			List<AccommodationOfferingDayRateData> groupedAccommodationOfferingDayRateDatas=accommodationOfferingDayRateDataMapEntry.getValue();

			RoomRateProductModel minAvailablePricedRoomRateProduct = null;
			double minAvailableValue = Double.MAX_VALUE;
			RatePlanModel minAvailableRatePlan = null;
			AccommodationModel minAvailableAccommodationModel = null;
			AccommodationOfferingDayRateData minAvailableAccommodationOfferingDayRateData=null;

			RoomRateProductModel minOnRequestPricedRoomRateProduct = null;
			double minOnRequestValue = Double.MAX_VALUE;
			RatePlanModel minOnRequestRatePlan = null;
			AccommodationModel minOnRequestAccommodationModel = null;
			AccommodationOfferingDayRateData minOnRequestAccommodationOfferingDayRateData=null;



			AccommodationOfferingModel accommodationOffering =null;
			int quantity =0;
			for (final AccommodationOfferingDayRateData accommodationOfferingDayRateData : groupedAccommodationOfferingDayRateDatas)
			{

				if (!BcfRatePlanUtil.checkIfEligibleToAdd(accommodationOfferingDayRateData,roomStayCandidates))
				{
					continue;
				}

				if (CollectionUtils.isEmpty(accommodationOfferingDayRateData.getRatePlanConfigs()))
				{
					continue;
				}

				final String ratePlanConfig = accommodationOfferingDayRateData.getRatePlanConfigs().stream().findFirst().get();
				quantity = Integer.parseInt(ratePlanConfig.split("\\|", 3)[2]);

				final Map<Pair<RatePlanModel, AccommodationModel>, List<RoomRateProductModel>> ratePlanRoomRateProductsMap = accommodationOfferingDayRateData
						.getRatePlanConfigs().stream()
						.map(ratePlanConfigStr -> getRatePlanPair(ratePlanConfigStr))
						.collect(Collectors
								.toMap(ratePlanPair -> ratePlanPair,
										ratePlanPair -> ratePlanPair.getLeft().getProducts().stream().filter(
												roomRate ->
														validateRoomRateAgainstDate(accommodationOfferingDayRateData.getDateOfStay(), roomRate)
																&& roomRate.getItemtype().equalsIgnoreCase(RoomRateProductModel._TYPECODE))
												.map(RoomRateProductModel.class::cast).collect(Collectors.toList())));


				if (MapUtils.isNotEmpty(ratePlanRoomRateProductsMap))
				{


					for (final Map.Entry<Pair<RatePlanModel, AccommodationModel>, List<RoomRateProductModel>> ratePlanRoomRateProductsMapEntry : ratePlanRoomRateProductsMap
							.entrySet())
					{
						final int ratePlanQuantity = quantity;
						final RatePlanModel ratePlan = ratePlanRoomRateProductsMapEntry.getKey().getLeft();
						final AccommodationModel accommodationModel = ratePlanRoomRateProductsMapEntry.getKey().getRight();
						accommodationOffering = getAccommodationOfferingService()
								.getAccommodationOffering(accommodationOfferingDayRateData.getAccommodationOfferingCode());


						if(!BcfRatePlanUtil.isValidForMinimumNightsStayRule(accommodationModel.getMinimumNightsStayRules(),stayDateRange)){

							continue;
						}


						final List<RoomRateProductModel> roomRateProducts =ratePlanRoomRateProductsMapEntry.getValue();

						final Map<RoomRateProductModel, Double> roomRateProductPriceValueMap = roomRateProducts.stream()
								.collect(Collectors
										.toMap(roomRateProductModel -> roomRateProductModel,
												roomRateProductModel -> getBasePriceAndMarginValue(roomRateProductModel, ratePlan,
														ratePlanQuantity)));
						if (MapUtils.isNotEmpty(roomRateProductPriceValueMap))
						{
							final Map.Entry<RoomRateProductModel, Double> minRoomRateProductPriceValueMapEntry = roomRateProductPriceValueMap
									.entrySet().stream().min(Map.Entry.comparingByValue()).get();

							final RoomRateProductModel roomRateProduct = minRoomRateProductPriceValueMapEntry.getKey();
							final double priceValue = minRoomRateProductPriceValueMapEntry.getValue();

							if(!bcfAccommodationFacadeHelper.checkifExtraProductEligible(accommodationOfferingDayRateData,accommodationModel,roomStayCandidates)){
							    continue;
							}

							AvailabilityStatus availabilityStatus =bcfAccommodationFacadeHelper.getStockAvailability(accommodationOffering,accommodationModel, stayDateRange, roomStayCandidates.size());

							if(availabilityStatus!=null && availabilityStatus.equals(AvailabilityStatus.AVAILABLE)){
								if (minAvailableValue > priceValue)
								{
									minAvailableAccommodationModel = accommodationModel;
									minAvailablePricedRoomRateProduct = roomRateProduct;
									minAvailableValue = priceValue;
									minAvailableRatePlan = ratePlan;
									minAvailableAccommodationOfferingDayRateData = accommodationOfferingDayRateData;


								}

							}else{

								if (minOnRequestValue > priceValue)
								{
									minOnRequestAccommodationModel = accommodationModel;
									minOnRequestPricedRoomRateProduct = roomRateProduct;
									minOnRequestValue = priceValue;
									minOnRequestRatePlan = ratePlan;
									minOnRequestAccommodationOfferingDayRateData = accommodationOfferingDayRateData;


								}
							}


						}
					}
				}

			}

			if(minAvailableAccommodationModel!=null){
				populateDayRates(minAvailableAccommodationOfferingDayRateData, passengerTypes, dayRates, quantity, minAvailablePricedRoomRateProduct,
						minAvailableRatePlan,
						minAvailableAccommodationModel, accommodationOffering);
			}else{
				populateDayRates(minOnRequestAccommodationOfferingDayRateData, passengerTypes, dayRates, quantity, minOnRequestPricedRoomRateProduct,
						minOnRequestRatePlan,
						minOnRequestAccommodationModel, accommodationOffering);
			}
		}



		return dayRates;
	}



	private List<RoomRateProductModel> getRoomRateProductModelsOrStock(
			final Map.Entry<Pair<RatePlanModel, AccommodationModel>, List<RoomRateProductModel>> ratePlanRoomRateProductsMapEntry,
			final StockData stockData)
	{
		if (stockData.getStockLevel() == null || stockData.getStockLevel() <= 0)
		{
			return Collections.emptyList();
		}
		final List<RoomRateProductModel> roomRateProducts = ratePlanRoomRateProductsMapEntry.getValue();
		if (CollectionUtils.isEmpty(roomRateProducts))
		{
			return Collections.emptyList();
		}
		return roomRateProducts;
	}

	private void populateDayRates(final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			final List<PassengerTypeQuantityData> passengerTypes, final List<DayRateData> dayRates, final int quantity,
			final RoomRateProductModel minPricedRoomRateProduct, final RatePlanModel minRatePlan,
			final AccommodationModel minAccommodationModel, final AccommodationOfferingModel accommodationOffering)
	{
		if (Objects.nonNull(minPricedRoomRateProduct))
		{

			final RateData rateData = getBcfTravelCommercePriceFacade()
					.calculateRateData(minPricedRoomRateProduct, quantity, minRatePlan,
							accommodationOffering.getLocation());

			dayRates.add(createDayRateData(accommodationOfferingDayRateData, rateData));


			if (Objects.nonNull(minRatePlan))
			{
				final Pair<Integer, Integer> extraGuestOccupancies = DefaultBcfTravelCartFacadeHelper
						.getExtraGuestOccupancies(passengerTypes, minAccommodationModel.getBaseOccupancyCount());

				if (extraGuestOccupancies.getLeft() > 0)
				{
					final DayRateData extraGuestOccupancyAdultDayRateData = getExtraGuestOccupancyDayRateData(
							accommodationOfferingDayRateData, minAccommodationModel, accommodationOffering, minRatePlan,
							BcfFacadesConstants.ADULT,
							extraGuestOccupancies.getLeft());
					addToDayRates(dayRates, extraGuestOccupancyAdultDayRateData);
				}

				if (extraGuestOccupancies.getRight() > 0)
				{
					final DayRateData extraGuestOccupancyChildDayRateData = getExtraGuestOccupancyDayRateData(
							accommodationOfferingDayRateData, minAccommodationModel, accommodationOffering, minRatePlan,
							BcfFacadesConstants.CHILD,
							extraGuestOccupancies.getRight());
					addToDayRates(dayRates, extraGuestOccupancyChildDayRateData);
				}
			}
		}
	}

	private void addToDayRates(final List<DayRateData> dayRates, final DayRateData dayRateData)
	{
		if (Objects.nonNull(dayRateData))
		{
			dayRates.add(dayRateData);
		}
	}

	private double getBasePriceAndMarginValue(final RoomRateProductModel roomRateProductModel, final RatePlanModel ratePlan,
			final int quantity)
	{
		final PriceValue priceValue = getRoomRatePriceCalculationStrategy().getBasePrice(roomRateProductModel, quantity);
		return priceValue.getValue() + getVacationPriceCalculationStrategy().getMargin(ratePlan, priceValue).getValue();

	}


	private Pair<RatePlanModel, AccommodationModel> getRatePlanPair(final String ratePlanConfigStr)
	{
		final RatePlanModel ratePlan = getRatePlanService().getRatePlanForCode(ratePlanConfigStr.split("\\|", 3)[0]);
		final AccommodationModel accommodationModel = bcfAccommodationService
				.getAccommodation(ratePlanConfigStr.split("\\|", 3)[1]);
		return Pair.of(ratePlan, accommodationModel);
	}

	protected boolean validateRoomRateAgainstDate(final Date date, final ProductModel roomRate)
	{
		final RoomRateProductModel roomRateProduct = (RoomRateProductModel) roomRate;
		final Iterator dateRangeIterator = roomRateProduct.getDateRanges().iterator();
		while (dateRangeIterator.hasNext())
		{
			final DateRangeModel dateRange = (DateRangeModel) dateRangeIterator.next();
			if (isValidRange(date, dateRange, roomRateProduct))
			{
				return true;
			}
		}
		return false;
	}

	protected boolean isValidDayOfWeek(final Date date, final List<DayOfWeek> daysOfWeek)
	{
		final LocalDate localDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		final DayOfWeek dayOfWeek = getEnumerationService()
				.getEnumerationValue(DayOfWeek.class, localDate.getDayOfWeek().toString());
		return daysOfWeek.contains(dayOfWeek);
	}


	protected boolean isValidRange(final Date date, final DateRangeModel dateRange, final RoomRateProductModel roomRateProduct)
	{
		return !date.before(dateRange.getStartingDate()) && !date.after(dateRange.getEndingDate()) && isValidDayOfWeek(date,
				roomRateProduct.getDaysOfWeek());
	}


	/**
	 * Gets the day rate data.
	 *
	 * @param accommodationOfferingDayRateData the accommodation offering day rate data for extra guests
	 * @return the day rate data
	 */
	protected DayRateData getExtraGuestOccupancyDayRateData(
			final AccommodationOfferingDayRateData accommodationOfferingDayRateData, final AccommodationModel minAccommodationModel,
			final AccommodationOfferingModel accommodationOffering, final RatePlanModel minRatePlan,
			final String passengerType, final int quantity)
	{
		final ExtraGuestOccupancyProductModel extraOccupancyProduct = getExtraGuestOccupancyProduct(minAccommodationModel,
				passengerType,
				accommodationOfferingDayRateData.getDateOfStay());

		if(extraOccupancyProduct==null){
			throw new SystemException("no price found for extra guest occuapancy for accommodation: "+minAccommodationModel.getCode());
		}

		final RateData rateData = getBcfTravelCommercePriceFacade()
				.calculateRateData(extraOccupancyProduct, Long.valueOf(quantity), passengerType, minRatePlan,
						accommodationOffering.getLocation(), accommodationOfferingDayRateData.getDateOfStay());

		if(rateData==null){
			throw new SystemException("no price found for extra guest occuapancy for accommodation: "+minAccommodationModel.getCode());
		}
		return createDayRateData(accommodationOfferingDayRateData, rateData);
	}

	protected ExtraGuestOccupancyProductModel getExtraGuestOccupancyProduct(final AccommodationModel accommodation,
			final String passengerType,
			final Date stayDate)
	{

		Collection<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodation.getExtraGuestProducts().stream()
				.filter(Objects::nonNull).distinct()
				.filter(extraGuestProduct -> priceRowService.getPriceRow(extraGuestProduct, passengerType, stayDate) != null)
				.collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(extraGuestProducts))
		{
			final Map<ExtraGuestOccupancyProductModel, Double> extraGuestPriceValueMap = extraGuestProducts.stream()
					.collect(Collectors
							.toMap(extraGuestProduct -> extraGuestProduct,
									extraGuestProduct -> priceRowService.getPriceRow(extraGuestProduct, passengerType, stayDate)
											.getPrice()));
			final Map.Entry<ExtraGuestOccupancyProductModel, Double> minExtraGuestPriceValueMapEntry = extraGuestPriceValueMap
					.entrySet().stream().min(Map.Entry.comparingByValue()).get();

			return minExtraGuestPriceValueMapEntry.getKey();
		}

		return null;
	}



	/**
	 * Creates the day rate data.
	 *
	 * @param accommodationOfferingDayRateData the accommodation offering day rate data
	 * @param rateData                         the rate data
	 * @return the day rate data
	 */
	protected DayRateData createDayRateData(final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			final RateData rateData)
	{
		final DayRateData dayRateData = new DayRateData();
		dayRateData.setDateOfStay(accommodationOfferingDayRateData.getDateOfStay());
		dayRateData.setDailyWasRate(rateData.getWasRate());
		dayRateData.setDailyDiscount(rateData.getTotalDiscount());
		dayRateData.setDailyActualRate(rateData.getActualRate());
		return dayRateData;
	}


	@Override
	protected void handlingAttributes(final Map<Integer, List<AccommodationOfferingDayRateData>> map,
			final PropertyData propertyData)
	{
		// TODO
	}

	/**
	 * Creates the price data.
	 *
	 * @param dayPrice the day price
	 * @return the price data
	 */
	protected PriceData createPriceData(final BigDecimal dayPrice)
	{
		return getBcfTravelCommercePriceFacade().createPriceData(PrecisionUtil.round(dayPrice.doubleValue()));
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the ratePlanService
	 */
	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	/**
	 * @param ratePlanService the ratePlanService to set
	 */
	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}

	/**
	 * @return the bcfTravelCommercePriceFacade
	 */
	protected BCFTravelCommercePriceFacade getBcfTravelCommercePriceFacade()
	{
		return bcfTravelCommercePriceFacade;
	}

	/**
	 * @param bcfTravelCommercePriceFacade the bcfTravelCommercePriceFacade to set
	 */
	@Required
	public void setBcfTravelCommercePriceFacade(final BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade)
	{
		this.bcfTravelCommercePriceFacade = bcfTravelCommercePriceFacade;
	}

	protected RoomRatePriceCalculationStrategy getRoomRatePriceCalculationStrategy()
	{
		return roomRatePriceCalculationStrategy;
	}

	@Required
	public void setRoomRatePriceCalculationStrategy(
			final RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy)
	{
		this.roomRatePriceCalculationStrategy = roomRatePriceCalculationStrategy;
	}

	/**
	 * @return the accommodationOfferingService
	 */
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	/**
	 * @param accommodationOfferingService the accommodationOfferingService to set
	 */
	@Required
	public void setAccommodationOfferingService(final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	public PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}

	public BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	public VacationPriceCalculationStrategy getVacationPriceCalculationStrategy()
	{
		return vacationPriceCalculationStrategy;
	}

	public void setVacationPriceCalculationStrategy(
			final VacationPriceCalculationStrategy vacationPriceCalculationStrategy)
	{
		this.vacationPriceCalculationStrategy = vacationPriceCalculationStrategy;
	}

	public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}
}

