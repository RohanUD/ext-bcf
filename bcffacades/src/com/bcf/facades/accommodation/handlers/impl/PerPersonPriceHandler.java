/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.AccommodationSearchHandler;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.facades.constants.BcfFacadesConstants;


public class PerPersonPriceHandler implements AccommodationSearchHandler
{
	private static final String[] DEFAULT_INCLUDED_GUEST_TYPES =
	{ "adult", "child" };
	private static final String JOURNEY_REF_NUMBER = "journeyRefNum";
	private static final String COMMA = ",";

	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BCFTravelCartService bcfTravelCartService;
	private SessionService sessionService;
	private BcfBookingService bcfBookingService;

	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		int noOfPersons = 0;
		final String propertyValue = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfFacadesConstants.GUEST_TYPES_FOR_PERPERSON_PRICE);
		final String[] includedGuestTypes = Objects.isNull(propertyValue) ? DEFAULT_INCLUDED_GUEST_TYPES
				: StringUtils.split(propertyValue, COMMA);
		for (final RoomStayCandidateData roomStayCandidateData : accommodationSearchResponse.getCriterion().getRoomStayCandidates())
		{
			noOfPersons = noOfPersons + roomStayCandidateData.getPassengerTypeQuantityList().stream()
					.filter(passengerTypeQuantityData -> Arrays.asList(includedGuestTypes)
							.contains(passengerTypeQuantityData.getPassengerType().getCode()))
					.mapToInt(PassengerTypeQuantityData::getQuantity).sum();
		}
		noOfPersons = (noOfPersons == 0) ? 1 : noOfPersons;

		int prevNoOfGuests = 0;
		final double amountPaid = 0;
		final CartModel cart = getBcfTravelCartService().getExistingSessionAmendedCart();
		if (getBcfTravelCartService().isAmendmentCart(cart))
		{

			prevNoOfGuests = getBcfTravelCartService().getNumberOfGuestsInCart(cart,
					getSessionService().getAttribute(JOURNEY_REF_NUMBER) != null ?
							getSessionService().getAttribute(JOURNEY_REF_NUMBER) :
							0);
		}
		int noOfPersonsToConsider = (noOfPersons == prevNoOfGuests) ? noOfPersons : (noOfPersons - prevNoOfGuests);
		noOfPersonsToConsider = (noOfPersonsToConsider == 0) ? 1 : noOfPersonsToConsider;
		for (final PropertyData propertyData : accommodationSearchResponse.getProperties())
		{
			if (Objects.nonNull(propertyData.getTotalPrice()))
			{
				final double totalPriceToConsider = propertyData.getTotalPrice().getValue().doubleValue();
				final double perPersonPrice = totalPriceToConsider / noOfPersonsToConsider;
				final double roundedPerPersonPrice = PrecisionUtil.round(perPersonPrice);
				propertyData.setPerPersonPrice(getTravelCommercePriceFacade().createPriceData(roundedPerPersonPrice));
			}
		}
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}
}
