/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.RatePlanService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;


public class BcfAccommodationAvailabilitySearchHandler extends AbstractAccommodationAvailabilitySearchHandler
{
	private RatePlanService ratePlanService;

	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		checkForValidRatePlanConfig(accommodationOfferingDayRates);
		checkForAccommodationAvailability(accommodationSearchRequest, accommodationSearchResponse);
	}

	protected void checkForValidRatePlanConfig(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates)
	{
		for (final AccommodationOfferingDayRateData accommodationOfferingDayRateData : accommodationOfferingDayRates)
		{
			if (CollectionUtils.isEmpty(accommodationOfferingDayRateData.getRatePlanConfigs()))
			{
				accommodationOfferingDayRateData.setPrice(null);
				continue;
			}

			for (final String ratePlanConfig : accommodationOfferingDayRateData.getRatePlanConfigs())
			{
				final String ratePlanCode = ratePlanConfig.split("\\|", 3)[0];
				final RatePlanModel ratePlan = getRatePlanService().getRatePlanForCode(ratePlanCode);
				if (Objects.isNull(ratePlan) || CollectionUtils.isEmpty(ratePlan.getProducts()))
				{
					continue;
				}

				if (BcfRatePlanUtil.isValidRatePlan(accommodationOfferingDayRateData.getDateOfStay(), ratePlan))
				{
					return;
				}
			}
			accommodationOfferingDayRateData.setPrice(null);
		}
	}

	protected void checkForAccommodationAvailability(final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		final List<PropertyData> availableProperties = new ArrayList<>();
		accommodationSearchResponse.getProperties().forEach(propertyData -> {
			checkAndSetAccommodationAvailability(propertyData, accommodationSearchRequest);
			availableProperties.add(propertyData);
		});
		accommodationSearchResponse.setProperties(availableProperties);
	}

	protected void checkAndSetAccommodationAvailability(final PropertyData propertyData,
			final AccommodationSearchRequestData accommodationSearchRequest)
	{
		final AccommodationOfferingModel accommodationOfferingModel = getBcfAccommodationOfferingService()
				.getAccommodationOffering(propertyData.getAccommodationOfferingCode());
		final SaleStatusType saleStatusType = getBcfAccommodationFacadeHelper()
				.getSaleStatus(accommodationOfferingModel.getSaleStatuses(),
						accommodationSearchRequest.getCriterion().getStayDateRange().getStartTime(),
						accommodationSearchRequest.getCriterion().getStayDateRange().getEndTime());

		AvailabilityStatus accommodationAvailabilityStatus = AvailabilityStatus.SOLD_OUT;

		if (Objects.equals(SaleStatusType.STOPSALE, saleStatusType))
		{
			accommodationAvailabilityStatus = AvailabilityStatus.SOLD_OUT;
		}
		else if (Objects.equals(SaleStatusType.OPEN, saleStatusType) || (
				Objects.equals(SaleStatusType.NOT_BOOKABLE_ONLINE, saleStatusType) && !getSaleStatusService()
						.toConsiderNonBookableOnlineSaleStatus()))
		{
			accommodationAvailabilityStatus = getAccommodationAvailabilityStatus(accommodationOfferingModel,
					accommodationSearchRequest.getCriterion().getStayDateRange(),
					accommodationSearchRequest.getCriterion().getRoomStayCandidates().size());
		}
		else if (getSaleStatusService().toConsiderNonBookableOnlineSaleStatus() && Objects
				.equals(SaleStatusType.NOT_BOOKABLE_ONLINE, saleStatusType))
		{
			accommodationAvailabilityStatus = AvailabilityStatus.NOT_BOOKABLE_ONLINE;
		}

		propertyData.setAvailabilityStatus(accommodationAvailabilityStatus.toString());
	}

	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}
}
