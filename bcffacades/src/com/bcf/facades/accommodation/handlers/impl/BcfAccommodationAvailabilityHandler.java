/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;


public class BcfAccommodationAvailabilityHandler implements AccommodationDetailsHandler
{

	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;


	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{

		accommodationAvailabilityResponseData.getRoomStays().forEach(roomStay -> roomStay.getRoomTypes().forEach(roomType -> {

			final AccommodationModel accommodation = getBcfAccommodationService().getAccommodation(roomType.getCode());

			final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
					.getAccommodationOfferingCode();
			final AccommodationOfferingModel accommodationOfferingModel = getBcfAccommodationOfferingService()
					.getAccommodationOffering(accommodationOfferingCode);

			final StayDateRangeData stayDateRangeData = availabilityRequestData.getCriterion().getStayDateRange();
			final Collection<RatePlanModel> ratePlanModels = accommodation.getRatePlan();
			List<RatePlanData> ratePlans=roomStay.getRatePlans();
			if(CollectionUtils.isNotEmpty(ratePlans)){
				ratePlans.forEach(ratePlan -> {

					RatePlanModel validRatePlan =ratePlanModels.stream().filter(ratePlanModel->ratePlanModel.getCode().equals(ratePlan.getCode())).findAny().orElse(null);
					Pair<AvailabilityStatus,Integer> availaibilityPair=bcfAccommodationFacadeHelper.getAccommodationAvailability(accommodationOfferingModel, stayDateRangeData.getStartTime(),stayDateRangeData.getEndTime(), accommodation,
							validRatePlan);
					ratePlan.setAvailabilityStatus(availaibilityPair.getLeft());
					ratePlan.setAvailableQuantity(availaibilityPair.getRight());
					if(!AvailabilityStatus.SOLD_OUT.equals(ratePlan.getAvailabilityStatus()) && availabilityRequestData.getCriterion().getRoomStayCandidates().size()>availaibilityPair.getRight()){
						ratePlan.setAvailabilityStatus(AvailabilityStatus.ON_REQUEST);
					}
				});
			}
		}));
	}


	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}
}
