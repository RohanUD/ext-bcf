/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.AccommodationPropertyResponseHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


public class BcfAccommodationPropertyResponseHandler extends AccommodationPropertyResponseHandler
{

	private static final Logger LOG = Logger.getLogger(BcfAccommodationPropertyResponseHandler.class);

	@Override
	public void handle(List<AccommodationOfferingDayRateData> accommodationOfferingDayRates, AccommodationSearchRequestData accommodationSearchRequest, AccommodationSearchResponseData accommodationSearchResponse) {
		List<PropertyData> properties = new ArrayList();
		Map<String, List<AccommodationOfferingDayRateData>> dayRatesForAccommodatioOfferingMap = this.groupByAccommodationOfferingCode(accommodationOfferingDayRates);
		dayRatesForAccommodatioOfferingMap.entrySet().forEach((entry) -> {

			try
			{
				properties.add(getPropertyPipelineManager().executePipeline(entry, accommodationSearchRequest));

			}catch(SystemException ex){

				LOG.error("Failed to add the accommodation data for offering: "+entry.getKey(), ex);
			}

		});
		accommodationSearchResponse.setProperties(properties);
	}
}
