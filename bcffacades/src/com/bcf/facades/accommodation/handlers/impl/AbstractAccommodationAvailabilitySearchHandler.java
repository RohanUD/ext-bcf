/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.AccommodationSearchHandler;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.service.BcfSaleStatusService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;


public abstract class AbstractAccommodationAvailabilitySearchHandler implements AccommodationSearchHandler
{
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfProductFacade bcfProductFacade;
	private BcfSaleStatusService saleStatusService;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Override
	public abstract void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse);

	protected AvailabilityStatus getAccommodationAvailabilityStatus(final AccommodationOfferingModel accommodationOfferingModel,
			final StayDateRangeData stayDateRange, final int numberOfRooms)
	{
		boolean toConsiderNonBookableOnlineSaleStatus = saleStatusService.toConsiderNonBookableOnlineSaleStatus();

//		if (toConsiderNonBookableOnlineSaleStatus && isAccommodationsWithStatusNonBookableOnlineExists(
//				accommodationOfferingModel, stayDateRange, toConsiderNonBookableOnlineSaleStatus))
//		{
//			return AvailabilityStatus.NOT_BOOKABLE_ONLINE;
//		}

		final List<AccommodationModel> openAccommodations = getAccommodationsInDateRangeWithSaleStatus(
				accommodationOfferingModel, stayDateRange, SaleStatusType.OPEN, toConsiderNonBookableOnlineSaleStatus);
		if (CollectionUtils.isEmpty(openAccommodations))
		{
			if (isAccommodationsWithStatusNonBookableOnlineExists(
					accommodationOfferingModel, stayDateRange, toConsiderNonBookableOnlineSaleStatus))
			{
				return AvailabilityStatus.NOT_BOOKABLE_ONLINE;
			}
			return AvailabilityStatus.SOLD_OUT;
		}

		final List<AccommodationModel> validAccommodations = openAccommodations.stream()
				.filter(accommodation -> checkValidityOfAccommodation(accommodation, stayDateRange)).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(validAccommodations))
		{
			if (isAccommodationsWithStatusNonBookableOnlineExists(
					accommodationOfferingModel, stayDateRange, toConsiderNonBookableOnlineSaleStatus))
			{
				return AvailabilityStatus.NOT_BOOKABLE_ONLINE;
			}
			return AvailabilityStatus.SOLD_OUT;
		}
		return getAvailabilityStatus(validAccommodations, accommodationOfferingModel, stayDateRange, numberOfRooms);
	}

	private List<AccommodationModel> getAccommodationsInDateRangeWithSaleStatus(
			final AccommodationOfferingModel accommodationOfferingModel,
			final StayDateRangeData stayDateRange, final SaleStatusType saleStatusType,
			boolean toConsiderNonBookableOnlineSaleStatus)
	{
		return accommodationOfferingModel.getAccommodations().stream()
				.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode)).filter(
						accommodation -> {

							SaleStatusType saleStatusTypeInRange = bcfAccommodationFacadeHelper
									.getSaleStatus(accommodation.getSaleStatuses(), stayDateRange.getStartTime(),
											stayDateRange.getEndTime());

							//if Not to consider Not bookable status, such as in case of ASM log in then consider it as Open status
							if (SaleStatusType.OPEN.equals(saleStatusType) && SaleStatusType.NOT_BOOKABLE_ONLINE
									.equals(saleStatusTypeInRange) && !toConsiderNonBookableOnlineSaleStatus)
							{
								return true;
							}
							else
							{
								return Objects
										.equals(saleStatusType,
												bcfAccommodationFacadeHelper
														.getSaleStatus(accommodation.getSaleStatuses(), stayDateRange.getStartTime(),
																stayDateRange.getEndTime()));
							}

						})
				.collect(Collectors.toList());

	}

	private boolean isAccommodationsWithStatusNonBookableOnlineExists(final AccommodationOfferingModel accommodationOfferingModel,
			final StayDateRangeData stayDateRange, boolean toConsiderNonBookableOnlineSaleStatus)
	{
		final List<AccommodationModel> nonBookableOnlineAccommodations = getAccommodationsInDateRangeWithSaleStatus(
				accommodationOfferingModel, stayDateRange, SaleStatusType.NOT_BOOKABLE_ONLINE, toConsiderNonBookableOnlineSaleStatus);

		return !CollectionUtils.isEmpty(nonBookableOnlineAccommodations);
	}

	protected boolean checkValidityOfAccommodation(final AccommodationModel accommodation, final StayDateRangeData stayDateRange)
	{
		final List<RoomRateProductModel> roomRateProducts = accommodation.getRatePlan().stream()
				.flatMap(ratePlanModel -> ratePlanModel.getProducts().stream()).filter(RoomRateProductModel.class::isInstance)
				.map(RoomRateProductModel.class::cast).collect(Collectors.toList());

		final Date endingDate = stayDateRange.getEndTime();
		for (Date date = stayDateRange.getStartTime(); !TravelDateUtils.isSameDate(date, endingDate); date = DateUtils
				.addDays(date, 1))
		{
			final Date stayDate = date;
			final boolean notValidAgainstStayDate = roomRateProducts.stream()
					.noneMatch(roomRateProduct -> BcfRatePlanUtil.isValidForDateRanges(stayDate, roomRateProduct));
			if (notValidAgainstStayDate)
			{
				return false;
			}
		}

		return true;
	}


	protected AvailabilityStatus getAvailabilityStatus(final List<AccommodationModel> validAccommodations,
			final AccommodationOfferingModel accommodationOfferingModel, final StayDateRangeData stayDateRange,
			final int numberOfRooms)
	{
		final Map<AccommodationModel, String> accommodationAvailabilityMap = validAccommodations.stream().collect(Collectors
				.toMap(accommodation -> accommodation,
						accommodation -> bcfAccommodationFacadeHelper
								.getStockAvailability(accommodationOfferingModel, accommodation, stayDateRange,
										numberOfRooms).toString()));

		final boolean isAnyAvailAccommodationExists = accommodationAvailabilityMap.values().stream()
				.anyMatch(availabilityStatus -> StringUtils.equals(AvailabilityStatus.AVAILABLE.toString(), availabilityStatus));
		if (isAnyAvailAccommodationExists)
		{
			return AvailabilityStatus.AVAILABLE;
		}
		final boolean isAnyOnReqAccommodationExists = accommodationAvailabilityMap.values().stream()
				.anyMatch(availabilityStatus -> StringUtils.equals(AvailabilityStatus.ON_REQUEST.toString(), availabilityStatus));
		if (isAnyOnReqAccommodationExists)
		{
			return AvailabilityStatus.ON_REQUEST;
		}
		return AvailabilityStatus.SOLD_OUT;
	}


	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	public BcfSaleStatusService getSaleStatusService()
	{
		return saleStatusService;
	}

	@Required
	public void setSaleStatusService(final BcfSaleStatusService saleStatusService)
	{
		this.saleStatusService = saleStatusService;
	}

	public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	@Required
	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}
}
