/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.AccommodationSearchHandler;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.util.PrecisionUtil;


public class BcfPropertyPriceHandler implements AccommodationSearchHandler
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BCFTravelCartService bcfTravelCartService;
	private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;

	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		if (CollectionUtils.isEmpty(accommodationSearchResponse.getProperties()))
		{
			return;
		}
		List<PropertyData> properties = accommodationSearchResponse.getProperties();
		final CartModel cart = getBcfTravelCartService().getExistingSessionAmendedCart();

		final Map<String, BigDecimal> changeFeeMap = getBcfChangeFeeCalculationService().calculateChangeFee(cart,
				createChangeAccommodationDataList(properties, accommodationSearchRequest), false);
		for (final PropertyData propertyData : properties)
		{
			final BigDecimal accommodationPrice = getAccommodationPrice(propertyData);
			if (Objects.isNull(accommodationPrice))
			{
				continue;
			}

			 BigDecimal totalPrice = accommodationPrice;
			if (changeFeeMap != null)
			{
				totalPrice = totalPrice.add(changeFeeMap.get(propertyData.getAccommodationOfferingCode()));
			}
			final double roundedFinalPrice = PrecisionUtil.round(totalPrice.doubleValue());
			propertyData.setTotalPrice(getTravelCommercePriceFacade().createPriceData(roundedFinalPrice));
		}

		if(CollectionUtils.isNotEmpty(properties)){

			Comparator<PropertyData> comparePropertyData= Comparator
					.comparing(PropertyData::getAvailabilityStatus)
					.thenComparing(propertyData -> propertyData.getTotalPrice().getValue());
			accommodationSearchResponse.setProperties(properties.stream().sorted(Comparator.nullsLast(comparePropertyData)).collect(Collectors.toList()));
		}



	}

	protected List<BcfChangeAccommodationData> createChangeAccommodationDataList(final List<PropertyData> propertyDatas,
			final AccommodationSearchRequestData accommodationSearchRequest)
	{
		final List<BcfChangeAccommodationData> changeAccommodationDatas = new ArrayList<>();
		BcfChangeAccommodationData changeAccommodationData = null;
		for (final PropertyData propertyData : propertyDatas)
		{
			changeAccommodationData = new BcfChangeAccommodationData();
			changeAccommodationData.setAccommodationOffering(propertyData.getAccommodationOfferingCode());
			changeAccommodationData.setStartDate(accommodationSearchRequest.getCriterion().getStayDateRange().getStartTime());
			changeAccommodationData.setEndDate(accommodationSearchRequest.getCriterion().getStayDateRange().getEndTime());
			changeAccommodationData.setNoofRooms(accommodationSearchRequest.getCriterion().getRoomStayCandidates().size());
			changeAccommodationDatas.add(changeAccommodationData);
		}
		return changeAccommodationDatas;
	}

	protected BigDecimal getAccommodationPrice(final PropertyData propertyData)
	{
		return propertyData.getRateRange() != null ? propertyData.getRateRange().getActualRate().getValue() : null;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	@Required
	public void setBcfChangeFeeCalculationService(final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}
}
