/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.ExtraGuestOccupancyData;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomTypeData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.dao.BcfRoomRateProductDao;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.AddToCartData;
import com.bcf.facades.fare.sorting.strategies.PriceSortingStrategyDesc;
import com.bcf.facades.promotion.PreemptivePromotionFacade;


public class BcfAccommodationAvailabilityResponsePriceHandler implements AccommodationDetailsHandler
{
	final RateData accommodationRateData = new RateData();
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CommonI18NService commonI18NService;
	private PriceSortingStrategyDesc priceSortingStrategyDesc;
	private SessionService sessionService;
	private PreemptivePromotionFacade preemptivePromotionFacade;
	private PromotionsService promotionsService;
	private UserService userService;
	private BcfRoomRateProductDao bcfRoomRateProductDao;
	private BCFTravelCartService bcfTravelCartService;
	BcfChangeFeeCalculationService bcfChangeFeeCalculationService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String currencyIso = getCommonI18NService().getCurrentCurrency().getIsocode();
		calculateTotalPriceForAccommodation(accommodationAvailabilityResponseData, currencyIso);

		final BcfChangeAccommodationData changeAccommodationData = createChangeAccommodationData(
				accommodationAvailabilityRequestData);
		final Map<String, BigDecimal> changeFeeMap = bcfChangeFeeCalculationService
				.calculateChangeFee(bcfTravelCartService.getExistingSessionAmendedCart(),
				Arrays.asList(changeAccommodationData),false);

		calculateAccommodationPackageDiscount(accommodationAvailabilityRequestData, accommodationAvailabilityResponseData,changeFeeMap,changeAccommodationData.getAccommodationOffering());
	}

	private BcfChangeAccommodationData createChangeAccommodationData(
			final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData)
	{

		final CriterionData criterion = accommodationAvailabilityRequestData.getCriterion();
		final BcfChangeAccommodationData changeAccommodationData = new BcfChangeAccommodationData();
		changeAccommodationData.setAccommodationOffering(criterion.getAccommodationReference().getAccommodationOfferingCode());
		changeAccommodationData.setStartDate(criterion.getStayDateRange().getStartTime());
		changeAccommodationData.setEndDate(criterion.getStayDateRange().getEndTime());
		changeAccommodationData.setNoofRooms(criterion.getRoomStayCandidates().size());
		return changeAccommodationData;
	}


	protected void calculateAccommodationPackageDiscount(
			final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData,
			final Map<String, BigDecimal> changeFeeMap, final String accommodationOffering)
	{
		if (Objects.isNull(accommodationAvailabilityRequestData) || Objects
				.isNull(accommodationAvailabilityRequestData.getCriterion()))
		{
			return;
		}

		if (Objects.isNull(accommodationAvailabilityResponseData) || Objects
				.isNull(accommodationAvailabilityResponseData.getRoomStays()))
		{
			return;
		}

		final List<RoomStayData> roomStays = accommodationAvailabilityResponseData.getRoomStays();
		if (CollectionUtils.isEmpty(roomStays))
		{
			return;
		}

		// This is for metadata alignment
		final List<RoomTypeData> roomTypes = roomStays.stream().flatMap(roomStayData -> roomStayData.getRoomTypes().stream())
				.collect(Collectors.toList());
		for (final RoomTypeData roomTypeData : roomTypes)
		{
			final Collection<PromotionData> potentialPromotions = new ArrayList<>();
			final PromotionData promotion = new PromotionData();
			promotion.setEnabled(true);
			potentialPromotions.add(promotion);
			roomTypeData.setPotentialPromotions(potentialPromotions);
		}

		final CurrencyModel currentCurrency = getCommonI18NService().getCurrentCurrency();
		final UserModel currentUser = getUserService().getCurrentUser();
		final List<PromotionGroupModel> promotionGroupModels = Collections
				.singletonList(getPromotionsService().getDefaultPromotionGroup());


		accommodationAvailabilityResponseData.setRoomStays(roomStays);
		groupRoomStays(accommodationAvailabilityResponseData, roomStays,currentCurrency.getIsocode());

		final List<RatePlanData> ratePlans = StreamUtil.safeStream(accommodationAvailabilityResponseData.getRoomStayGroups())
				.flatMap(roomStayData -> roomStayData.getRatePlans().stream()).collect(Collectors.toList());

		populateAccommodationPromotionDiscount(ratePlans, currentCurrency, currentUser, promotionGroupModels, changeFeeMap, accommodationOffering);


		final List<RoomStayData> roomStayGroupDatas = accommodationAvailabilityResponseData.getRoomStayGroups();
		if(CollectionUtils.isNotEmpty(roomStayGroupDatas)){
			final Comparator<Object> comparePropertyData = Comparator
					.comparing(roomStayData-> ((RoomStayData)roomStayData).getRatePlans().iterator().next().getAvailabilityStatus()).
							thenComparing(roomStayData->((RoomStayData)roomStayData).getRatePlans().iterator().next().getActualRate().getValue());

			accommodationAvailabilityResponseData.setRoomStayGroups(roomStayGroupDatas.stream().filter(roomStayGroupData->CollectionUtils.isNotEmpty(roomStayGroupData.getRatePlans())).sorted(Comparator.nullsLast(comparePropertyData)).collect(Collectors.toList()));
		}


		accommodationAvailabilityResponseData.setNoOfAvailableRoomTypes(Math.toIntExact(
				accommodationAvailabilityResponseData.getRoomStayGroups().stream()
						.filter(roomStayGroup -> CollectionUtils.isNotEmpty(roomStayGroup.getRatePlans()))
						.flatMap(roomStayGroup -> roomStayGroup.getRatePlans().stream())
						.count()));
	}

	private void groupRoomStays(final AccommodationAvailabilityResponseData accommodationPackageResponse,
			final List<RoomStayData> roomStays, final String isoCode)
	{
		final List<RoomStayData> updatedRoomStays = new ArrayList<>();
		final Map<String, List<RoomStayData>> roomStayMap = roomStays.stream().collect(
				Collectors.groupingBy(
						roomStay -> CollectionUtils.isNotEmpty(roomStay.getRoomTypes()) ? roomStay.getRoomTypes().get(0).getCode() :
								Strings.EMPTY));
		for (final Map.Entry<String, List<RoomStayData>> roomStayMapEntry : roomStayMap.entrySet())
		{
			final List<RoomStayData> roomStayEntryDatas = roomStayMapEntry.getValue();

			final List<RatePlanData> combinedRatePlans = roomStayEntryDatas.stream()
					.flatMap(roomStayEntryData -> roomStayEntryData.getRatePlans().stream()).collect(Collectors.toList());

			final RoomStayData roomStayData = roomStayEntryDatas.get(0);
			roomStayData.setRatePlans(combinedRatePlans);
			updatedRoomStays.add(roomStayData);

		}

		for (final RoomStayData roomStayData : updatedRoomStays)
		{
			final List<RatePlanData> updatedRatePlans = new ArrayList<>();
			final Map<String, List<RatePlanData>> ratePlanMap = roomStayData.getRatePlans().stream()
					.collect(Collectors.groupingBy(ratePlan -> ratePlan.getCode()));

			updateRatePlans( updatedRatePlans, ratePlanMap, isoCode);

			roomStayData.setRatePlans(updatedRatePlans);
		}


		accommodationPackageResponse.setRoomStayGroups(updatedRoomStays);
	}

	private void updateRatePlans(final List<RatePlanData> updatedRatePlans, final Map<String, List<RatePlanData>> ratePlanMap,
			final String isoCode)
	{
		for (final Map.Entry<String, List<RatePlanData>> ratePlanMapEntry : ratePlanMap.entrySet())
		{
			Double actualRate = 0d;
			Double wasRate = 0D;
			Double discounts = 0D;

			final List<RatePlanData> ratePlanMapEntryValues = ratePlanMapEntry.getValue();
			for (final RatePlanData ratePlan : ratePlanMapEntryValues)
			{

				actualRate = Double.sum(actualRate, ratePlan.getActualRate().getValue().doubleValue());
				wasRate = Double.sum(wasRate, ratePlan.getWasRate().getValue().doubleValue());
				if (Objects.nonNull(ratePlan.getTotalDiscount()))
				{
					discounts = Double.sum(discounts, ratePlan.getTotalDiscount().getValue().doubleValue());
				}

			}


			final RatePlanData ratePlanData = ratePlanMapEntryValues.get(0);
			ratePlanData.setActualRate(getTravelCommercePriceFacade().createPriceData(actualRate, isoCode));
			ratePlanData.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate, isoCode));
			ratePlanData.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(discounts, isoCode));

			updatedRatePlans.add(ratePlanData);

		}
	}
	protected void populateAccommodationPromotionDiscount(final List<RatePlanData> ratePlans,
			final CurrencyModel currentCurrency, final UserModel currentUser,
			final Collection<PromotionGroupModel> promotionGroupModels, final Map<String, BigDecimal> changeFeeMap,
			final String accommodationOffering)
	{
		BigDecimal changeFee=BigDecimal.ZERO;
		if(changeFeeMap!=null){
			changeFee=changeFeeMap.get(accommodationOffering);
		}

		if(CollectionUtils.isNotEmpty(ratePlans))
		{
			for (final RatePlanData ratePlanData : ratePlans)
			{
				final Map<String, List<RoomRateData>> roomRatesMap = ratePlanData.getRoomRates().stream()
						.collect(Collectors.groupingBy(RoomRateData::getCode));
				if (MapUtils.isEmpty(roomRatesMap))
				{
					continue;
				}
				final PriceData existingActualRate = ratePlanData.getActualRate();
				final BigDecimal newActualRate = existingActualRate.getValue().add(changeFee);
				final PriceData newActualRatePrice = getTravelCommercePriceFacade()
						.createPriceData(newActualRate.doubleValue(), currentCurrency.getIsocode());
				ratePlanData.setActualRate(newActualRatePrice);

				final Pair<Double, Boolean> promotionResult = getPromotionalDiscount(roomRatesMap, currentCurrency, currentUser,
						promotionGroupModels);
				if (promotionResult.getRight() && promotionResult.getLeft() > 0)
				{
					makeDiscountEffective(ratePlanData, currentCurrency, promotionResult.getLeft());
				}
			}
		}

	}

	protected Pair<Double, Boolean> getPromotionalDiscount(final Map<String, List<RoomRateData>> roomRatesMap,
			final CurrencyModel currentCurrency, final UserModel currentUser,
			final Collection<PromotionGroupModel> promotionGroupModels)
	{
		final List<AddToCartData> addToCartDataList = new ArrayList<>();
		for (final Map.Entry<String, List<RoomRateData>> roomRatesMapEntry : roomRatesMap.entrySet())
		{
			final String RoomRateProductCode = roomRatesMapEntry.getKey();
			final List<RoomRateData> roomRateDatas = roomRatesMapEntry.getValue();
			//should be RoomRateProductService
			final RoomRateProductModel roomRateProduct = getBcfRoomRateProductDao()
					.findRoomRateProductByCode(RoomRateProductCode);
			final int quantity = CollectionUtils.size(roomRateDatas);
			final RoomRateData roomRateData = roomRateDatas.stream().findAny().get();
			final RateData rateData = roomRateData.getRate();
			if (Objects.nonNull(rateData))
			{
				final AddToCartData addToCartData = new AddToCartData();
				final double basePrice = rateData.getBasePrice().getValue().doubleValue();
				addToCartData.setBasePrice(basePrice);
				addToCartData.setProduct(roomRateProduct);
				addToCartData.setQuantity(quantity);
				addToCartData.setUnit(roomRateProduct.getUnit());

				addToCartDataList.add(addToCartData);
			}
		}

		return getPreemptivePromotionFacade()
				.getPromotionResult(addToCartDataList, currentCurrency, currentUser, promotionGroupModels);
	}

	protected void makeDiscountEffective(final RatePlanData ratePlanData,
			final CurrencyModel currentCurrency, final double newDiscount)
	{
		final PriceData discountPriceData;
		final PriceData existingDiscount = ratePlanData.getTotalDiscount();
		if (Objects.nonNull(existingDiscount))
		{
			final BigDecimal totalDiscount = existingDiscount.getValue().add(BigDecimal.valueOf(newDiscount));
			discountPriceData = getTravelCommercePriceFacade()
					.createPriceData(totalDiscount.doubleValue(), currentCurrency.getIsocode());
		}
		else
		{
			discountPriceData = getTravelCommercePriceFacade()
					.createPriceData(newDiscount, currentCurrency.getIsocode());
		}
		ratePlanData.setTotalDiscount(discountPriceData);

		final PriceData existingActualRate = ratePlanData.getActualRate();
		final BigDecimal newActualRate = existingActualRate.getValue().subtract(BigDecimal.valueOf(newDiscount));
		final PriceData newActualRatePrice = getTravelCommercePriceFacade()
				.createPriceData(newActualRate.doubleValue(), currentCurrency.getIsocode());
		ratePlanData.setActualRate(newActualRatePrice);
	}

	protected void calculateTotalPriceForAccommodation(
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData,
			final String currencyIso)
	{
		final List<RoomStayData> roomStays = accommodationAvailabilityResponseData.getRoomStays();
		for (final RoomStayData roomStayData : roomStays)
		{
			final List<RatePlanData> ratePlans = roomStayData.getRatePlans().stream()
					.filter(ratePlanData -> Objects.nonNull(ratePlanData.getAvailableQuantity())
							&& ratePlanData.getAvailableQuantity() > 0)
					.collect(Collectors.toList());

			for (final RatePlanData ratePlan : ratePlans)
			{
				BigDecimal wasRate = BigDecimal.ZERO;
				BigDecimal actualRate = BigDecimal.ZERO;
				final List<RoomRateData> roomRates = ratePlan.getRoomRates();
				for (final RoomRateData roomRate : roomRates)
				{
					final RateData rateData = roomRate.getRate();
					actualRate =actualRate.add(rateData.getActualRate().getValue());
					wasRate = wasRate.add( rateData.getWasRate().getValue());

					if (CollectionUtils.isNotEmpty(roomRate.getExtraGuestOccupancies()))
					{
						for (final ExtraGuestOccupancyData extraGuestOccupancyData : roomRate.getExtraGuestOccupancies())
						{
							wasRate = wasRate.add(extraGuestOccupancyData.getRate().getWasRate().getValue());
							actualRate = actualRate.add(extraGuestOccupancyData.getRate().getActualRate().getValue());

						}
					}
				}


				ratePlan.setActualRate(getTravelCommercePriceFacade().createPriceData(actualRate.doubleValue(), currencyIso));
				ratePlan.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate.doubleValue(), currencyIso));
			}
		}
	}

	protected RateData getTotalPrice(final RateData accommodationPackageResponseData, final String currencyIso)
	{
		Double basePrice = 0d;
		Double totalPrice = 0d;
		Double wasRate = 0d;
		final List<TaxData> taxes = new ArrayList<>();

		basePrice = Double.sum(basePrice, accommodationPackageResponseData.getBasePrice().getValue().doubleValue());
		totalPrice = Double.sum(totalPrice, accommodationPackageResponseData.getActualRate().getValue().doubleValue());
		wasRate = Double.sum(wasRate, accommodationPackageResponseData.getWasRate().getValue().doubleValue());
		taxes.addAll(accommodationPackageResponseData.getTaxes());

		final RateData rateData = new RateData();
		rateData.setBasePrice(getTravelCommercePriceFacade().createPriceData(basePrice, currencyIso));
		rateData.setActualRate(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIso));
		rateData.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate, currencyIso));
		rateData.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(wasRate - totalPrice, currencyIso));
		rateData.setTaxes(taxes);

		final TaxData totalTaxData = new TaxData();
		final Double totalTaxValue = taxes.stream().mapToDouble(tax -> tax.getPrice().getValue().doubleValue()).sum();
		totalTaxData.setPrice(getTravelCommercePriceFacade().createPriceData(totalTaxValue, currencyIso));
		rateData.setTotalTax(totalTaxData);
		return rateData;
	}

	/**
	 * @return the travelCommercePriceFacade
	 */
	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	/**
	 * @param travelCommercePriceFacade the travelCommercePriceFacade to set
	 */

	@Required

	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


	/**
	 * @param commonI18NService the commonI18NService to set
	 */


	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the priceSortingStrategyDesc
	 */
	protected PriceSortingStrategyDesc getPriceSortingStrategyDesc()
	{
		return priceSortingStrategyDesc;
	}


	/**
	 * @param priceSortingStrategyDesc the priceSortingStrategyDesc to set
	 */
	@Required
	public void setPriceSortingStrategyDesc(final PriceSortingStrategyDesc priceSortingStrategyDesc)
	{
		this.priceSortingStrategyDesc = priceSortingStrategyDesc;
	}

	/**
	 * @return the sessionService
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService the sessionService to set
	 */
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	/**
	 * @return
	 */
	public PreemptivePromotionFacade getPreemptivePromotionFacade()
	{
		return preemptivePromotionFacade;
	}

	/**
	 * @param preemptivePromotionFacade
	 */
	@Required
	public void setPreemptivePromotionFacade(final PreemptivePromotionFacade preemptivePromotionFacade)
	{
		this.preemptivePromotionFacade = preemptivePromotionFacade;
	}

	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public BcfRoomRateProductDao getBcfRoomRateProductDao()
	{
		return bcfRoomRateProductDao;
	}

	public void setBcfRoomRateProductDao(final BcfRoomRateProductDao bcfRoomRateProductDao)
	{
		this.bcfRoomRateProductDao = bcfRoomRateProductDao;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(
			final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}
}
