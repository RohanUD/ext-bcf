/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.DayRateData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.search.RateRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.PropertyHandler;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.AbstractDefaultPropertyHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.RatePlanService;
import de.hybris.platform.util.PriceValue;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.accommodation.strategies.impl.RoomRatePriceCalculationStrategy;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;


public class OptimizedBcfPropertyDataPriceHandler extends AbstractDefaultPropertyHandler implements PropertyHandler
{
	private CommonI18NService commonI18NService;
	private RatePlanService ratePlanService;
	private BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade;
	private RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy;
	private AccommodationOfferingService accommodationOfferingService;

	@Override
	public void handle(final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMap,
			final AccommodationSearchRequestData accommodationSearchRequest, final PropertyData propertyData)
	{
		if (BooleanUtils.isTrue(validateDayRatesAgainstRequest(dayRatesForRoomStayCandidateRefNumberMap, accommodationSearchRequest)
				&& CollectionUtils.isNotEmpty(propertyData.getRatePlanConfigs())))
		{
			handlingAttributes(dayRatesForRoomStayCandidateRefNumberMap, propertyData);
		}
	}

	@Override
	protected Boolean validateDayRatesAgainstRequest(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMap,
			final AccommodationSearchRequestData accommodationRequest)
	{
		return Boolean
				.valueOf(validateDayRateAgainstRoomStayCandidates(dayRatesForRoomStayCandidateRefNumberMap, accommodationRequest)
						&& validateDayRateAgainstDates(dayRatesForRoomStayCandidateRefNumberMap, accommodationRequest));
	}

	@Override
	protected boolean validateDayRateAgainstRoomStayCandidates(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final AccommodationSearchRequestData accommodationRequest)
	{
		return dayRatesForRoomStayCandidate.entrySet().size() == accommodationRequest.getCriterion().getRoomStayCandidates().size();
	}

	@Override
	protected boolean validateDayRateAgainstDates(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidate,
			final AccommodationSearchRequestData accommodationRequest)
	{
		final long requestedDuration = getRequestedDuration(accommodationRequest);
		for (final Map.Entry<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateMapEntry : dayRatesForRoomStayCandidate
				.entrySet())
		{
			final Map<Date, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateGroupedByDateOfStay = dayRatesForRoomStayCandidateMapEntry
					.getValue().stream().collect(Collectors.groupingBy(AccommodationOfferingDayRateData::getDateOfStay));
			if (MapUtils.isEmpty(dayRatesForRoomStayCandidateGroupedByDateOfStay)
					|| dayRatesForRoomStayCandidateGroupedByDateOfStay.size() != requestedDuration)
			{
				return false;
			}
		}
		return true;
	}

	@Override
	protected void handlingAttributes(
			final Map<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMap,
			final PropertyData propertyData)
	{
		BigDecimal wasRate = BigDecimal.ZERO;
		BigDecimal totalDiscount = BigDecimal.ZERO;
		BigDecimal actualPrice = BigDecimal.ZERO;

		final RateRangeData rateRange = new RateRangeData();
		final List<DayRateData> dayRates = new ArrayList<>();
		for (final Map.Entry<Integer, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateRefNumberMapEntry : dayRatesForRoomStayCandidateRefNumberMap
				.entrySet())
		{
			final Integer roomStayCandidateRefNum = dayRatesForRoomStayCandidateRefNumberMapEntry.getKey();
			final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatasPerRoom = dayRatesForRoomStayCandidateRefNumberMapEntry
					.getValue();
			final Map<Date, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateDateOfStayMap = accommodationOfferingDayRateDatasPerRoom
					.stream().collect(Collectors.groupingBy(AccommodationOfferingDayRateData::getDateOfStay));
			final List<AccommodationOfferingDayRateData> filteredAccommodationOfferingDayRateDatasPerRoom = new ArrayList<>();
			for (final Map.Entry<Date, List<AccommodationOfferingDayRateData>> dayRatesForRoomStayCandidateDateOfStayMapEntry : dayRatesForRoomStayCandidateDateOfStayMap
					.entrySet())
			{
				final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatasPerDate = dayRatesForRoomStayCandidateDateOfStayMapEntry
						.getValue();
				final AbstractMap.SimpleEntry<AccommodationOfferingDayRateData, DayRateData> dayRateDataPair = getMinDayRateData(
						accommodationOfferingDayRateDatasPerDate);
				if (Objects.nonNull(dayRateDataPair))
				{
					final AccommodationOfferingDayRateData accommodationOfferingDayRateData = dayRateDataPair.getKey();
					final DayRateData dayRateData = dayRateDataPair.getValue();
					actualPrice = actualPrice.add(dayRateData.getDailyActualRate().getValue());
					wasRate = wasRate.add(dayRateData.getDailyWasRate().getValue());
					if (Objects.nonNull(dayRateData.getDailyDiscount()))
					{
						totalDiscount = totalDiscount.add(dayRateData.getDailyDiscount().getValue());
					}
					dayRates.add(dayRateData);
					filteredAccommodationOfferingDayRateDatasPerRoom.add(accommodationOfferingDayRateData);
				}
			}
			dayRatesForRoomStayCandidateRefNumberMap.put(roomStayCandidateRefNum, filteredAccommodationOfferingDayRateDatasPerRoom);
		}

		rateRange.setDayRates(dayRates);
		rateRange.setCurrencyCode(getCommonI18NService().getCurrentCurrency().getIsocode());
		rateRange.setWasRate(createPriceData(wasRate));
		rateRange.setTotalDiscount(createPriceData(totalDiscount));
		rateRange.setActualRate(createPriceData(wasRate.subtract(totalDiscount)));
		propertyData.setRateRange(rateRange);
	}

	/**
	 * Gets the min day rate data.
	 *
	 * @param accommodationOfferingDayRateDatasPerDate
	 *           the accommodation offering day rate datas per date
	 * @return the min day rate data
	 */
	protected AbstractMap.SimpleEntry<AccommodationOfferingDayRateData, DayRateData> getMinDayRateData(
			final List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatasPerDate)
	{
		RoomRateProductModel minPricedRoomRateProduct = null;
		double minValue = Double.MAX_VALUE;
		RatePlanModel minRatePlan = null;
		AccommodationOfferingDayRateData minAccommodationOfferingDayRateData = null;
		int quantityForMin = 0;
		for (final AccommodationOfferingDayRateData accommodationOfferingDayRateData : accommodationOfferingDayRateDatasPerDate)
		{
			if (CollectionUtils.isEmpty(accommodationOfferingDayRateData.getRatePlanConfigs()))
			{
				return null;
			}

			final String ratePlanConfig = accommodationOfferingDayRateData.getRatePlanConfigs().stream().findFirst().get();
			final String ratePlanCode = ratePlanConfig.split("\\|", 3)[0];
			final int quantity = Integer.parseInt(ratePlanConfig.split("\\|", 3)[2]);
			final RatePlanModel ratePlan = getRatePlanService().getRatePlanForCode(ratePlanCode);
			final RoomRateProductModel roomRateProduct = ratePlan.getProducts().stream()
					.filter(RoomRateProductModel.class::isInstance).map(RoomRateProductModel.class::cast).findFirst().get();

			final PriceValue basePriceValue = getRoomRatePriceCalculationStrategy().getBasePrice(roomRateProduct, quantity);
			final double priceValue = basePriceValue.getValue();
			if (minValue > priceValue)
			{
				minPricedRoomRateProduct = roomRateProduct;
				minValue = priceValue;
				minRatePlan = ratePlan;
				minAccommodationOfferingDayRateData = accommodationOfferingDayRateData;
				quantityForMin = quantity;
			}
		}

		if (Objects.nonNull(minPricedRoomRateProduct))
		{
			final DayRateData minDayRateData = createDayRateData(minAccommodationOfferingDayRateData, quantityForMin,
					minPricedRoomRateProduct, minRatePlan);
			return new AbstractMap.SimpleEntry<>(minAccommodationOfferingDayRateData, minDayRateData);
		}
		return null;
	}

	/**
	 * Creates the day rate data.
	 *
	 * @param accommodationOfferingDayRateData
	 *           the accommodation offering day rate data
	 * @param quantity
	 *           the quantity
	 * @param roomRateProduct
	 *           the room rate product
	 * @param ratePlan
	 *           the rate plan
	 * @return the day rate data
	 */
	protected DayRateData createDayRateData(final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			final int quantity, final RoomRateProductModel roomRateProduct, final RatePlanModel ratePlan)
	{
		final DayRateData dayRateData = new DayRateData();
		dayRateData.setDateOfStay(accommodationOfferingDayRateData.getDateOfStay());
		final String accommodationOfferingCode = accommodationOfferingDayRateData.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOffering = getAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);
		final RateData rateData = getBcfTravelCommercePriceFacade().calculateRateData(roomRateProduct, quantity, ratePlan,
				accommodationOffering.getLocation());
		dayRateData.setDailyWasRate(rateData.getWasRate());
		dayRateData.setDailyDiscount(rateData.getTotalDiscount());
		dayRateData.setDailyActualRate(rateData.getActualRate());
		return dayRateData;
	}

	/**
	 * Creates the price data.
	 *
	 * @param dayPrice
	 *           the day price
	 * @return the price data
	 */
	protected PriceData createPriceData(final BigDecimal dayPrice)
	{
		return getBcfTravelCommercePriceFacade().createPriceData(dayPrice.doubleValue());
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the ratePlanService
	 */
	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	/**
	 * @param ratePlanService
	 *           the ratePlanService to set
	 */
	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}

	/**
	 * @return the bcfTravelCommercePriceFacade
	 */
	protected BCFTravelCommercePriceFacade getBcfTravelCommercePriceFacade()
	{
		return bcfTravelCommercePriceFacade;
	}

	/**
	 * @param bcfTravelCommercePriceFacade
	 *           the bcfTravelCommercePriceFacade to set
	 */
	@Required
	public void setBcfTravelCommercePriceFacade(final BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade)
	{
		this.bcfTravelCommercePriceFacade = bcfTravelCommercePriceFacade;
	}

	/**
	 * @return roomRatePriceCalculationStrategy
	 */
	protected RoomRatePriceCalculationStrategy getRoomRatePriceCalculationStrategy()
	{
		return roomRatePriceCalculationStrategy;
	}

	/**
	 * @param roomRatePriceCalculationStrategy
	 *           the roomRatePriceCalculationStrategy to set
	 */
	@Required
	public void setRoomRatePriceCalculationStrategy(final RoomRatePriceCalculationStrategy roomRatePriceCalculationStrategy)
	{
		this.roomRatePriceCalculationStrategy = roomRatePriceCalculationStrategy;
	}

	/**
	 * @return the accommodationOfferingService
	 */
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	/**
	 * @param accommodationOfferingService
	 *           the accommodationOfferingService to set
	 */
	@Required
	public void setAccommodationOfferingService(final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}
}
