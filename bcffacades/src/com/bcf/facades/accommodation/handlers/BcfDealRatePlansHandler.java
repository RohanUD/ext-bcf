/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.accommodation.handlers;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.DealRatePlansHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;


public class BcfDealRatePlansHandler extends DealRatePlansHandler
{
	private BcfAccommodationService bcfAccommodationService;

	@Override
	public void handle(
			final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		accommodationAvailabilityResponseData.getRoomStays().forEach(roomStay -> {
			final RoomStayCandidateData roomStayCandidateData = availabilityRequestData.getCriterion()
					.getRoomStayCandidates().stream().filter(roomStayCandidate -> Objects
							.equals(roomStay.getRoomStayRefNumber(), roomStayCandidate.getRoomStayCandidateRefNumber())
					).findFirst().get();
			if (StringUtils.isNotEmpty(roomStayCandidateData.getRatePlanCode()))
			{
				final RatePlanModel ratePlanModel = getRatePlanService()
						.getRatePlanForCode(roomStayCandidateData.getRatePlanCode());
				roomStay.setRatePlans(getRatePlanConverter().convertAll(Collections.singletonList(ratePlanModel)));
				updateCancelPenaltiesDescription(Arrays.asList(ratePlanModel), roomStay);
			}

			final String accommodationCode = roomStay.getRoomTypes().get(0).getCode();
			if (availabilityRequestData.getCriterion().getAccommodationReference().getPropertyAccomodationCodes()
					.contains(accommodationCode))
			{
				final AccommodationModel accommodation = getBcfAccommodationService().getAccommodation(accommodationCode);
				updateGuestOccupancy(roomStay, accommodation);
			}
		});
	}

	/**
	 * @return the bcfAccommodationService
	 */
	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	/**
	 * @param bcfAccommodationService
	 *           the bcfAccommodationService to set
	 */
	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}
}
