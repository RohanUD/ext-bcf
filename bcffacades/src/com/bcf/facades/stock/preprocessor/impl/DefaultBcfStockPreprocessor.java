/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.stock.preprocessor.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.handler.AbstractCartPreprocessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.stock.preprocessor.BcfStockPreprocessor;


public class DefaultBcfStockPreprocessor implements BcfStockPreprocessor
{

   private static final Logger LOG = Logger.getLogger(DefaultBcfStockPreprocessor.class);
   private List<AbstractCartPreprocessorHandler> handlers;
   private BcfTravelCommerceStockService bcfTravelCommerceStockService;
   private BCFTravelCartService bcfTravelCartService;

   @Override
   public void validateAndReserve(final PlaceOrderResponseData decisionData, final CartModel cartModel)
         throws CartValidationException, InsufficientStockLevelException, OrderProcessingException
   {

      final boolean earlyStockAllocation = bcfTravelCommerceStockService.getEarlyStockAllocation(cartModel.getSalesApplication());
      decisionData.setEarlyStockAllocation(earlyStockAllocation);

      final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries = bcfTravelCartService
            .splitCartEntriesByType(cartModel);
      LOG.debug(String.format("PreProcessing cart [%s]", cartModel.getCode()));
      for (final AbstractCartPreprocessorHandler handler : getHandlers())
      {
         handler.handle(typeWiseEntries, decisionData, cartModel);
      }
   }

   protected List<AbstractCartPreprocessorHandler> getHandlers()
   {
      return handlers;
   }

   @Required
   public void setHandlers(final List<AbstractCartPreprocessorHandler> handlers)
   {
      this.handlers = handlers;
   }

   public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
   {
      return bcfTravelCommerceStockService;
   }


   public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
   {
      this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
   }


   public BCFTravelCartService getBcfTravelCartService()
   {
      return bcfTravelCartService;
   }

   public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
   {
      this.bcfTravelCartService = bcfTravelCartService;
   }
}
