/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.facades.stock.preprocessor;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;


public interface BcfStockPreprocessor
{

   void validateAndReserve(PlaceOrderResponseData decisionData, CartModel cartModel)
         throws CartValidationException, InsufficientStockLevelException, OrderProcessingException;

}
