/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades;

import de.hybris.platform.b2bcommercefacades.company.data.UserData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cmsfacades.data.UserGroupData;
import de.hybris.platform.cmsfacades.usergroups.UserGroupFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import java.util.List;


public interface BcfUserGroupFacade extends UserGroupFacade
{
	List<UserGroupData> getUserListForIds(List<String> groupIds) throws CMSItemNotFoundException;

	List<CustomerData> getAgents();
}
