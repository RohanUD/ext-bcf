/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.travelfacades.facades.packages.DealCartFacade;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.packages.cart.AddBcfdDealToCartData;


public interface BcfDealCartFacade extends DealCartFacade
{
	List<CartModificationData> addDealToCart(AddBcfdDealToCartData addBcfdDealToCartData);

	void populateAddBundleToCart(TravelBundleTemplateData bundleData,
			BcfAddBundleToCartData addBundleToCart, String travelRoute, boolean isDynamicPackage);

	void removeSelectedActivityEntries(String activityProductCode, int journeyRefNumber);

	void removeSelectedActivityEntries(final String activityProductCode, final int journeyRefNumber,String date, String time);

	void removeSelectedActivityEntries(String entryNumbers);

	void disableSelectedActivityEntries(final String activityProductCode, final int journeyRefNumber);

	List<AddActivityToCartData> getActivityDataFromCart();

	Pair<Map<String, ActivityPricePerPassengerWithSchedule>, Map<String, List<Date>>> getActivityPricePerPassengerWithSchedulesMapFromActivityData(
			List<AddActivityToCartData> activityCartDatas,
			Date commencementDate, String locationCode, final List<Date> scheduleDates, Date checkInDate, Date checkOutDate);

	Map<ActivityScheduleData, List<Date>> getScheduleDates(Date checkInDate, final Date checkOutDate,
			final List<ActivityScheduleData> availableSchedules);

	List<Date> getScheduleDates(Date checkInDate, final Date checkOutDate);

	boolean addActivityToCart(final ActivityPricePerPassengerWithSchedule addActivityPriceSchedule,
			final AddActivityToCartData data);

	boolean isActivityDateTimeMatchesInActivityOrderEntryInfo(final ActivityOrderEntryInfoModel activityOrderEntryInfoModel, final String date,
			final String time);

}
