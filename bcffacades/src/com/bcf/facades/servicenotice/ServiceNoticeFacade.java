/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.servicenotice;

import de.hybris.platform.commercefacades.travel.CompetitionData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.SubstantialPerformanceNoticeData;
import java.util.List;
import java.util.Optional;


public interface ServiceNoticeFacade
{
	List<ServiceNoticeData> getNewsData();

	List<CompetitionData> getCompetitionsData();

	List<SubstantialPerformanceNoticeData> getSubstantialPerformanceNoticesData();

	Optional<CompetitionData> getCompetitionDataForCode(String code);

	Optional<SubstantialPerformanceNoticeData> getSubstantialPerformanceNoticeDataForCode(String code);
}
