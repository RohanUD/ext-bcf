/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.servicenotice.impl;

import de.hybris.platform.commercefacades.travel.CompetitionData;
import de.hybris.platform.commercefacades.travel.NewsData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.SubstantialPerformanceNoticeData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;
import com.bcf.core.services.servicenotice.ServiceNoticesService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.servicenotice.ServiceNoticeFacade;


public class DefaultServiceNoticeFacade implements ServiceNoticeFacade
{
	private ServiceNoticesService serviceNoticesService;
	private Converter<NewsModel, NewsData> newsConverter;
	private Converter<CompetitionModel, CompetitionData> competitionConverter;
	private Converter<SubstantialPerformanceNoticeModel, SubstantialPerformanceNoticeData> substantialPerformanceNoticeDataConverter;

	@Override
	public List<ServiceNoticeData> getNewsData()
	{
		return StreamUtil.safeStream(getServiceNoticesService().findNews())
				.map(getNewsConverter()::convert)
				.collect(Collectors.toList());
	}

	@Override
	public List<CompetitionData> getCompetitionsData()
	{
		return StreamUtil.safeStream(getServiceNoticesService().findCompetitions())
				.map(getCompetitionConverter()::convert)
				.collect(Collectors.toList());
	}

	@Override
	public List<SubstantialPerformanceNoticeData> getSubstantialPerformanceNoticesData()
	{
		return StreamUtil.safeStream(getServiceNoticesService().findSubstantialPerformanceNotices())
				.map(getSubstantialPerformanceNoticeDataConverter()::convert)
				.collect(Collectors.toList());
	}

	public Optional<CompetitionData> getCompetitionDataForCode(final String code)
	{
		return Optional.ofNullable(getServiceNoticesService().findCompetitionForCode(code)).map(getCompetitionConverter()::convert);
	}

	public Optional<SubstantialPerformanceNoticeData> getSubstantialPerformanceNoticeDataForCode(final String code)
	{
		return Optional.ofNullable(getServiceNoticesService().findSubstantialPerformanceNoticeForCode(code))
				.map(getSubstantialPerformanceNoticeDataConverter()::convert);
	}

	protected ServiceNoticesService getServiceNoticesService()
	{
		return serviceNoticesService;
	}

	@Required
	public void setServiceNoticesService(final ServiceNoticesService serviceNoticesService)
	{
		this.serviceNoticesService = serviceNoticesService;
	}

	public Converter<NewsModel, NewsData> getNewsConverter()
	{
		return newsConverter;
	}

	@Required
	public void setNewsConverter(final Converter<NewsModel, NewsData> newsConverter)
	{
		this.newsConverter = newsConverter;
	}

	public Converter<CompetitionModel, CompetitionData> getCompetitionConverter()
	{
		return competitionConverter;
	}

	@Required
	public void setCompetitionConverter(final Converter<CompetitionModel, CompetitionData> competitionConverter)
	{
		this.competitionConverter = competitionConverter;
	}

	public Converter<SubstantialPerformanceNoticeModel, SubstantialPerformanceNoticeData> getSubstantialPerformanceNoticeDataConverter()
	{
		return substantialPerformanceNoticeDataConverter;
	}

	@Required
	public void setSubstantialPerformanceNoticeDataConverter(
			final Converter<SubstantialPerformanceNoticeModel, SubstantialPerformanceNoticeData> substantialPerformanceNoticeDataConverter)
	{
		this.substantialPerformanceNoticeDataConverter = substantialPerformanceNoticeDataConverter;
	}
}
