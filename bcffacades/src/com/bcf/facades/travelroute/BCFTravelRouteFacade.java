/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelroute;

import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelfacades.facades.TravelRouteFacade;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import java.util.List;
import com.bcf.core.enums.RouteType;
import com.bcf.facades.terminals.data.BcfRouteInfo;


public interface BCFTravelRouteFacade extends TravelRouteFacade
{
	/**
	 * Returns a list of all routes
	 */
	List<TravelRouteData> getAllTravelRoutes();

	/**
	 * Returns a list of all routes
	 */
	List<BcfRouteInfo> retrieveRouteInfo();

	/**
	 * Returns a list of TravelRouteData for locationCode
	 */
	List<TravelRouteData> getTravelRoutesForDestination(String locationCode);

	List<TravelRouteData> getBcfVacationTravelRoutesForDestination(String locationCode);

	List<TravelRouteInfo> getTravelRoutesFromTerminal(TransportFacilityModel transportFacility);

	List<BcfRouteInfo> retrieveRoutesForCurrentConditions();

	/**
	 * Returns Route type for given origin and destination locationCode
	 */
	RouteType getRouteType(String originCode, String destinationCode);

	boolean isWalkOnRoute(String originCode, String destinationCode);

	boolean walkOnOptionsAllowed(String originCode, String destinationCode);

	boolean getTravelRouteReservableValue(String travelRouteData);

	boolean isOpenTicketAllowedForRoute(final String travelRouteCode);

	TravelRouteData getDirectTravelRoutes(String originCode, String destinationCode);

	TravelRouteData getDirectTravelRoutesForCurrentConditions(String originCode, String destinationCode);

	boolean getAllowAdditionalPassengerTypes(final String originCode, final String destinationCode);

	public List<TravelRouteData> getTravelRoutesForAccommodationLocation(final String accommodationLocation);

	List<TravelRouteData> getTravelRoutesForActiveTerminals();

	TravelRouteData getTravelRoute(List<AbstractOrderEntryModel> entries);

}
