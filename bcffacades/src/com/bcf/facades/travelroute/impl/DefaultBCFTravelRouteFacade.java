/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelroute.impl;

import de.hybris.platform.commercefacades.travel.RouteInfoData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.impl.DefaultTravelRouteFacade;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.terminals.data.BcfRouteInfo;
import com.bcf.facades.travel.data.BcfDestinationData;
import com.bcf.facades.travel.data.BcfTerminalLocationData;
import com.bcf.facades.travel.data.BcfTerminalLocationInfoData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


public class DefaultBCFTravelRouteFacade extends DefaultTravelRouteFacade implements BCFTravelRouteFacade
{
	private BcfTravelLocationService travelLocationService;
	private Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter;
	private BcfTravelLocationFacade bcfTravelLocationFacade;
	private BCFTravelRouteService bcfTravelRouteService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;


	@Override
	public List<TravelRouteData> getAllTravelRoutes()
	{
		//fetch all travel routes
		final List<TravelRouteModel> travelRoutes = ((BCFTravelRouteService) getTravelRouteService()).getAllTravelRoutes();
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			return Converters.convertAll(travelRoutes, getTravelRouteConverter());
		}
		return Collections.emptyList();
	}

	@Override
	public List<BcfRouteInfo> retrieveRoutesForCurrentConditions()
	{
		final List<TravelRouteModel> travelRouteForCurrentConditions = ((BCFTravelRouteService) getTravelRouteService())
				.getTravelRouteForCurrentConditions();
		return getRouteInfos(travelRouteForCurrentConditions);
	}

	@Override
	public List<BcfRouteInfo> retrieveRouteInfo()
	{
		//fetch all travel routes
		final List<TravelRouteModel> travelRoutes = ((BCFTravelRouteService) getTravelRouteService()).getAllTravelRoutes();
		for (final TravelRouteModel travelroutemodel : travelRoutes)
		{
			if (travelroutemodel.isReservable() == true)
			{
				final BcfTerminalLocationData bcfTerminalLocationData = new BcfTerminalLocationData();
				bcfTerminalLocationData.setIsBookable(travelroutemodel.isReservable());
			}
		}
		return getRouteInfos(travelRoutes);

	}

	private List<BcfRouteInfo> getRouteInfos(final List<TravelRouteModel> travelRoutes)
	{
		//group by origin transport facility
		final Map<TransportFacilityModel, List<TravelRouteModel>> travelRoutesByOrigin = travelRoutes.stream()
				.filter(route -> Objects.nonNull(route.getOrigin()))
				.collect(Collectors.groupingBy(TravelRouteModel::getOrigin));

		final List<BcfRouteInfo> bcfRouteInfos = new ArrayList<>();
		travelRoutesByOrigin.forEach((originTransportFacility, linkedTravelRoutes) -> {
			final BcfRouteInfo bcfRouteInfo = new BcfRouteInfo();

			//population of terminal
			bcfRouteInfo.setCode(originTransportFacility.getCode());
			bcfRouteInfo.setName(originTransportFacility.getName());
			bcfRouteInfo.setTravelRouteName(BcfTravelRouteUtil.translateName(originTransportFacility.getName()));

			final BcfTerminalLocationInfoData terminalLocationInfoData = getBcfTravelLocationFacade()
					.getTerminalLocationInfo(Collections.singletonList(originTransportFacility.getLocation()));

			// population of city
			bcfRouteInfo.setCity(terminalLocationInfoData.getCity());

			// population of geographical area
			bcfRouteInfo.setGeoGraphicalArea(terminalLocationInfoData.getGeographicalArea());

			bcfRouteInfo.setDestinationRoutes(getDestinations(linkedTravelRoutes));

			final Optional<BcfDestinationData> isBookable = StreamUtil.safeStream(getDestinations(linkedTravelRoutes))
					.filter(dest -> Boolean.TRUE.equals(dest.isIsBookable())).findFirst();

			if (isBookable.isPresent())
			{
				bcfRouteInfo.setIsBookable(Boolean.TRUE);
			}
			else
			{
				bcfRouteInfo.setIsBookable(Boolean.FALSE);
			}
			bcfRouteInfos.add(bcfRouteInfo);
		});

		return sortRouteDataByGeoArea(bcfRouteInfos);
	}

	private List<BcfRouteInfo> sortRouteDataByGeoArea(final List<BcfRouteInfo> bcfRouteInfos)
	{
		List<BcfTerminalLocationData> geoLocations = new ArrayList<BcfTerminalLocationData>();

		Set<String> uniqueGeoArea=new HashSet<>();
		bcfRouteInfos.stream().forEach(routeInfo -> {
			if(Objects.nonNull(routeInfo.getGeoGraphicalArea()) && uniqueGeoArea.add(routeInfo.getGeoGraphicalArea().getCode()))
			{
				geoLocations.add(routeInfo.getGeoGraphicalArea());
			}
		});

		final List<BcfTerminalLocationData> sortedGeoLocations = new ArrayList<>();
		//sort geographical area based on sort order
		sortedGeoLocations.addAll(geoLocations.stream().filter(l -> Objects.nonNull(l.getSortOrder()))
				.sorted(Comparator.comparingInt(BcfTerminalLocationData::getSortOrder)).collect(
						Collectors.toList()));

		//sort geographical area based on names where sort order is not provided
		sortedGeoLocations.addAll(geoLocations.stream().filter(geoLoc -> Objects.isNull(geoLoc.getSortOrder()))
				.sorted(Comparator.comparing(BcfTerminalLocationData::getCode)).collect(
						Collectors.toList()));

		final Map<String, List<BcfRouteInfo>> groupByGeoLocation = bcfRouteInfos.stream()
				.filter(f -> Objects.nonNull(f.getGeoGraphicalArea()))
				.collect(Collectors.groupingBy(g -> g.getGeoGraphicalArea().getCode()));

		final List<BcfRouteInfo> sortedCityDatas = new ArrayList<>();
		sortedGeoLocations.stream().forEach(geoAreaLocation -> {
			final List<BcfRouteInfo> unsortedCityDatas = groupByGeoLocation.get(geoAreaLocation.getCode());

			//sort based on sortOrder field where sortOrder is provided
			sortedCityDatas.addAll(unsortedCityDatas.stream().filter(cityData -> Objects.nonNull(cityData.getCity().getSortOrder()))
					.sorted(Comparator.comparingInt(city->city.getCity().getSortOrder())).collect(
							Collectors.toList()));

			//sort based  for rest of the locations where sortOrder is not provided
			sortedCityDatas.addAll(unsortedCityDatas.stream().filter(cityData -> Objects.isNull(cityData.getCity().getSortOrder()))
					.sorted(Comparator.comparing(city->city.getCity().getCode())).collect(
							Collectors.toList()));
		});

		return sortedCityDatas;
	}

	@Override
	public List<TravelRouteData> getTravelRoutesForDestination(final String locationCode)
	{
		final Set<TravelRouteModel> travelRoutes = ((BCFTravelRouteService) getTravelRouteService())
				.getTravelRoutesForDestination(locationCode);
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			return Converters.convertAll(travelRoutes, getTravelRouteConverter());
		}
		return Collections.emptyList();
	}

	@Override
	public List<TravelRouteData> getTravelRoutesForAccommodationLocation(final String accommodationLocation)
	{
		final BcfVacationLocationModel bcfVacationLocation = travelLocationService
				.findBcfVacationLocationByCode(accommodationLocation);
		if (bcfVacationLocation != null)
		{
			return Converters.convertAll(bcfVacationLocation.getTravelRoutes(), getTravelRouteConverter());
		}
		return Collections.emptyList();
	}

	@Override
	public List<TravelRouteData> getBcfVacationTravelRoutesForDestination(final String locationCode)
	{
		final Set<TravelRouteModel> travelRoutes = ((BCFTravelRouteService) getTravelRouteService())
				.getBcfVacationTravelRoutesForDestination(locationCode);
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			return Converters.convertAll(travelRoutes, getTravelRouteConverter());
		}
		return Collections.emptyList();
	}



	@Override
	public List<TravelRouteInfo> getTravelRoutesFromTerminal(final TransportFacilityModel transportFacility)
	{
		final List<TravelRouteModel> travelRoutes = ((BCFTravelRouteService) getTravelRouteService())
				.getTravelRoutesForOrigin(transportFacility);
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			final List<TravelRouteModel> travelRouteModels = travelRoutes.stream()
					.filter(travelRouteModel -> Objects.nonNull(travelRouteModel.getIndirectRoute()) && travelRouteModel
							.getIndirectRoute().equals(Boolean.FALSE) && Objects.nonNull(travelRouteModel.getCurrentConditionEnabled())
							&& travelRouteModel
							.getCurrentConditionEnabled())
					.collect(Collectors.toList());
			return Converters.convertAll(travelRouteModels, getCurrentConditionsTravelRouteConverter());
		}
		return Collections.emptyList();
	}

	protected List<BcfDestinationData> getDestinations(final List<TravelRouteModel> linkedTravelRoutes)
	{
		final List<BcfDestinationData> bcfDestinationDatas = new ArrayList<>();
		linkedTravelRoutes.forEach(travelRoute -> {
			if (travelRoute.getRouteType() == null)
			{
				return;
			}
			final BcfDestinationData bcfDestinationData = new BcfDestinationData();
			bcfDestinationData.setDestination(travelRoute.getDestination().getCode());
			bcfDestinationData.setCode(travelRoute.getDestination().getCode());
			if (travelRoute.isReservable() || (bcfTravelCartFacadeHelper.isAlacateFlow() && travelRoute.isAllowOpenTicket()))
			{

				bcfDestinationData.setIsBookable(true);
			}
			bcfDestinationData.setName(travelRoute.getDestination().getName());
			bcfDestinationData.setTravelRouteName(BcfTravelRouteUtil.translateName(travelRoute.getDestination().getName()));
			final BcfTerminalLocationInfoData terminalLocationInfoData = getBcfTravelLocationFacade()
					.getTerminalLocationInfo(Collections.singletonList(travelRoute.getDestination().getLocation()));
			bcfDestinationData.setGeoGraphicalArea(terminalLocationInfoData.getGeographicalArea());
			bcfDestinationData.setCity(terminalLocationInfoData.getCity());
			bcfDestinationData.setIsWalkOn(Objects.nonNull(travelRoute.getWalkOn()) ? travelRoute.getWalkOn() : false);
			bcfDestinationData.setRouteType(travelRoute.getRouteType().getCode());
			bcfDestinationData.setMotorcycleAllowed(BooleanUtils.isTrue(travelRoute.getAllowMotorcycles()));
			bcfDestinationData.setAllowAdditionalPassengerTypes(
					getAllowAdditionalPassengerTypes(travelRoute.getOrigin().getCode(), travelRoute.getDestination().getCode()));
			bcfDestinationData.setLimitedAvailability(
					Objects.nonNull(travelRoute.getLimitedAvailability()) ? travelRoute.getLimitedAvailability() : false);
			bcfDestinationDatas.add(bcfDestinationData);
		});
		return bcfDestinationDatas;
	}

	@Override
	public RouteType getRouteType(final String originCode, final String destinationCode)
	{
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
		if (CollectionUtils.isEmpty(travelRoutes))
		{
			return null;
		}
		return travelRoutes.stream().findFirst().get().getRouteType();
	}

	@Override
	public boolean isWalkOnRoute(final String originCode, final String destinationCode)
	{
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			return StreamUtil.safeStream(travelRoutes)
					.anyMatch(route -> Objects.nonNull(route.getWalkOn()) && Boolean.TRUE.equals(route.getWalkOn()));
		}
		return false;
	}

	@Override
	public boolean walkOnOptionsAllowed(final String originCode, final String destinationCode)
	{
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
		if (CollectionUtils.isNotEmpty(travelRoutes))
		{
			return StreamUtil.safeStream(travelRoutes).anyMatch(
					route -> Objects.nonNull(route.getAllowsWalkOnOptions()) && Boolean.TRUE.equals(route.getAllowsWalkOnOptions()));
		}
		return false;
	}

	@Override
	public boolean isOpenTicketAllowedForRoute(final String travelRouteCode)
	{
		return getBcfTravelRouteService().isOpenTicketAllowedForRoute(travelRouteCode);
	}

	@Override
	public TravelRouteData getDirectTravelRoutes(final String originCode, final String destinationCode)
	{
		final List<TravelRouteModel> travelRoutes = getBcfTravelRouteService().getDirectTravelRoutes(originCode, destinationCode);
		final List<TravelRouteModel> travelRouteModelList = travelRoutes.stream().filter(
				travelRouteModel -> Objects.nonNull(travelRouteModel.getIndirectRoute()) && travelRouteModel.getIndirectRoute()
						.equals(Boolean.FALSE) && Objects.nonNull(travelRouteModel)).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(travelRouteModelList))
		{
			return getTravelRouteConverter().convert(travelRouteModelList.get(0));
		}
		return null;
	}

	@Override
	public TravelRouteData getDirectTravelRoutesForCurrentConditions(final String originCode, final String destinationCode)
	{
		final List<TravelRouteModel> travelRoutes = getBcfTravelRouteService().getTravelRoutes(originCode, destinationCode);
		final List<TravelRouteModel> travelRouteModelList = travelRoutes.stream().filter(
				travelRouteModel -> Objects.nonNull(travelRouteModel.getIndirectRoute()) && travelRouteModel.getIndirectRoute()
						.equals(Boolean.FALSE) && Objects.nonNull(travelRouteModel) && travelRouteModel.getCurrentConditionEnabled()
						.equals(Boolean.TRUE)).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(travelRouteModelList))
		{
			return getTravelRouteConverter().convert(travelRouteModelList.get(0));
		}
		return null;
	}

	@Override
	public boolean getTravelRouteReservableValue(final String travelRouteData)
	{
		return getTravelRoute(travelRouteData).isReservable();
	}

	@Override
	public boolean getAllowAdditionalPassengerTypes(final String originCode, final String destinationCode)
	{
		if (StringUtils.isNotBlank(originCode) && StringUtils.isNotBlank(destinationCode))
		{
			final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
			if (CollectionUtils.isNotEmpty(travelRoutes))
			{
				final TravelRouteModel travelRoute = travelRoutes.stream().findFirst().get();

				return RouteType.LONG.equals(travelRoute.getRouteType()) &&
						travelRoute.isAllowAdditionalPassengerTypes();
			}
		}
		return false;
	}

	@Override
	public List<TravelRouteData> getTravelRoutesForActiveTerminals()
	{
		final List<TravelRouteModel> activeTravelRoutes = bcfTravelRouteService.getTravelRoutesForActiveTerminals();
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(activeTravelRoutes))
		{
			return Converters.convertAll(activeTravelRoutes, getTravelRouteConverter());
		}
		return Collections.emptyList();

	}

	@Override
	public TravelRouteData getTravelRoute(final List<AbstractOrderEntryModel> entries)
	{
		final Optional<AbstractOrderEntryModel> optionalEntryModel = StreamUtil.safeStream(entries)
				.filter(entry -> entry.getActive() && Objects.nonNull(entry.getTravelOrderEntryInfo())).findFirst();
		if (optionalEntryModel.isPresent())
		{
			return getTravelRouteConverter().convert(optionalEntryModel.get().getTravelOrderEntryInfo().getTravelRoute());
		}
		return null;
	}

	protected BcfTravelLocationService getTravelLocationService()
	{
		return travelLocationService;
	}

	@Required
	public void setTravelLocationService(final BcfTravelLocationService travelLocationService)
	{
		this.travelLocationService = travelLocationService;
	}

	protected Converter<TravelRouteModel, TravelRouteInfo> getCurrentConditionsTravelRouteConverter()
	{
		return currentConditionsTravelRouteConverter;
	}

	@Required
	public void setCurrentConditionsTravelRouteConverter(
			final Converter<TravelRouteModel, TravelRouteInfo> currentConditionsTravelRouteConverter)
	{
		this.currentConditionsTravelRouteConverter = currentConditionsTravelRouteConverter;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	/**
	 * @return the bcfTravelLocationFacade
	 */
	protected BcfTravelLocationFacade getBcfTravelLocationFacade()
	{
		return bcfTravelLocationFacade;
	}

	/**
	 * @param bcfTravelLocationFacade the bcfTravelLocationFacade to set
	 */
	@Required
	public void setBcfTravelLocationFacade(final BcfTravelLocationFacade bcfTravelLocationFacade)
	{
		this.bcfTravelLocationFacade = bcfTravelLocationFacade;
	}


	@Required
	public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
	{
		super.setTravelRouteService(bcfTravelRouteService);
		this.bcfTravelRouteService = bcfTravelRouteService;
	}

	public BCFTravelRouteService getBcfTravelRouteService()
	{
		return bcfTravelRouteService;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}

