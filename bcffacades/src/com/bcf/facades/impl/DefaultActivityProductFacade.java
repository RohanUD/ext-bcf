/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import static java.util.stream.Collectors.toList;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.ActivityProductService;
import com.bcf.core.service.BcfSaleStatusService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.ActivityProductFacade;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.constants.BcfFacadesConstants;


public class DefaultActivityProductFacade implements ActivityProductFacade
{
	private static final String PRODUCTCATALOG = "bcfProductCatalog";
	private static final String CATALOGVERSION = CatalogManager.ONLINE_VERSION;
	private static final int DEFAULT_PAX_THRESHOLD_PER_PAX_TYPE = 5;

	private ActivityProductService activityProductService;
	private Converter<ActivityProductModel, ActivityDetailsData> activityDetailsConverter;
	private Converter<ActivityProductModel, ActivityDetailsData> activityProductDetailConverter;
	private CatalogVersionService catalogVersionService;
	private BcfSaleStatusService saleStatusService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public List<ActivityDetailsData> findActivityProducts()
	{
		final List<ActivityProductModel> activityProductModels = getActivityProductService().findAllActivityProducts();
		return Converters.convertAll(activityProductModels, getActivityDetailsConverter());
	}

	@Override
	public List<ActivityDetailsData> findActivityProductsDetails(final List<ProductModel> activityProducts)
	{
		final List<ActivityProductModel> activityProductDetailsModels = activityProducts.stream()
				.map(ActivityProductModel.class::cast)
				.collect(toList());

		return Converters.convertAll(activityProductDetailsModels, getActivityProductDetailConverter());
	}

	@Override
	public ActivityDetailsData getActivityDetailsData(final String activityCode)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);
		final ActivityProductModel activityProduct = getActivityProductService().getActivityProduct(activityCode, catalogVersion);
		final ActivityDetailsData activityDetailsData = getActivityProductDetailConverter().convert(activityProduct);
		if (CollectionUtils.isNotEmpty(activityDetailsData.getActivitySchedules()))
		{
			activityDetailsData.getActivitySchedules().sort(Comparator.comparing(o -> o.getStartTime()));
		}
		return activityDetailsData;
	}

	@Override
	public void removeInvalidActivities(final ActivitiesResponseData activitiesResponseData)
	{
		final List<ActivityResponseData> invalidActivityResponseData = activitiesResponseData.getActivityResponseData().stream()
				.filter(activityResponseData -> Objects.isNull(activityResponseData.getAdultPriceValue()) && Objects
						.isNull(activityResponseData.getChildPriceValue())).collect(Collectors.toList());
		activitiesResponseData.getActivityResponseData().removeAll(invalidActivityResponseData);
	}

	@Override
	public void setAvailableStatusForActivitiesResponseData(final ActivitiesResponseData activitiesResponseData,
			final Date checkInDate)
	{
		activitiesResponseData.getActivityResponseData().stream()
				.forEach(ard -> setAvailableStatus(ard, checkInDate));
	}

	protected void setAvailableStatus(final ActivityResponseData activityResponseData,
			final Date checkInDate)
	{
		if (Objects.isNull(checkInDate))
		{
			activityResponseData.setAvailable(Boolean.TRUE);
			activityResponseData.setAvailableStatus(SaleStatusType.OPEN.getCode());
			return;
		}

		if (saleStatusService.toConsiderNonBookableOnlineSaleStatus() && !isNotBookableOnlineRangeDatesEmpty(activityResponseData)
				&& isInNotBookableOnlineDateRange(checkInDate, activityResponseData))
		{
			activityResponseData.setAvailable(Boolean.FALSE);
			activityResponseData.setAvailableStatus(SaleStatusType.NOT_BOOKABLE_ONLINE.getCode());
		}
		else if (!isStopSaleRangeDatesEmpty(activityResponseData) && isInStopSaleDateRange(checkInDate, activityResponseData))
		{
			activityResponseData.setAvailable(Boolean.FALSE);
			activityResponseData.setAvailableStatus(SaleStatusType.STOPSALE.getCode());
		}
		else
		{
			activityResponseData.setAvailable(Boolean.TRUE);
			activityResponseData.setAvailableStatus(SaleStatusType.OPEN.getCode());
		}

	}

	private boolean isNotBookableOnlineRangeDatesEmpty(final ActivityResponseData activityResponse)
	{
		return Objects.isNull(activityResponse.getNotBookableOnlineStartDate()) || Objects
				.isNull(activityResponse.getNotBookableOnlineEndDate());
	}

	protected boolean isStopSaleRangeDatesEmpty(final ActivityResponseData activityResponse)
	{
		return Objects.isNull(activityResponse.getStartDate()) || Objects.isNull(activityResponse.getEndDate());
	}


	protected boolean isInStopSaleDateRange(final Date checkInDate, final ActivityResponseData activityResponse)
	{
		return BCFDateUtils
				.isDateInDateRange(checkInDate, activityResponse.getStartDate(), activityResponse.getEndDate());
	}

	protected boolean isInNotBookableOnlineDateRange(final Date checkInDate, final ActivityResponseData activityResponse)
	{
		return BCFDateUtils
				.isDateInDateRange(checkInDate, activityResponse.getNotBookableOnlineStartDate(),
						activityResponse.getNotBookableOnlineEndDate());
	}

	@Override
	public boolean checkAvailability(final AddActivityToCartData addActivityToCartData)
	{
		return false;
	}

	@Override
	public int getMaxPaxAllowedPerPaxTypePerActivity()
	{
		String maxPaxAllowedPerPaxType = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED_PER_PAX_TYPE_PER_ACTIVITY);

		return StringUtils.isNotEmpty(maxPaxAllowedPerPaxType) ?
				Integer.parseInt(maxPaxAllowedPerPaxType) :
				DEFAULT_PAX_THRESHOLD_PER_PAX_TYPE;
	}

	protected ActivityProductService getActivityProductService()
	{
		return activityProductService;
	}

	@Required
	public void setActivityProductService(final ActivityProductService activityProductService)
	{
		this.activityProductService = activityProductService;
	}

	protected Converter<ActivityProductModel, ActivityDetailsData> getActivityDetailsConverter()
	{
		return activityDetailsConverter;
	}

	@Required
	public void setActivityDetailsConverter(
			final Converter<ActivityProductModel, ActivityDetailsData> activityDetailsConverter)
	{
		this.activityDetailsConverter = activityDetailsConverter;
	}

	protected Converter<ActivityProductModel, ActivityDetailsData> getActivityProductDetailConverter()
	{
		return activityProductDetailConverter;
	}

	@Required
	public void setActivityProductDetailConverter(
			final Converter<ActivityProductModel, ActivityDetailsData> activityProductDetailConverter)
	{
		this.activityProductDetailConverter = activityProductDetailConverter;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	@Required
	public void setSaleStatusService(final BcfSaleStatusService saleStatusService)
	{
		this.saleStatusService = saleStatusService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
