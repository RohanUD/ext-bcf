/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.ScheduledRouteData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.TravelSectorData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.travelfacades.facades.impl.DefaultTransportOfferingFacade;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.search.facetdata.TransportOfferingSearchPageData;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.date.CalendarData;
import com.bcf.facades.schedules.DailySchedulesData;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.schedules.search.request.data.ScheduleSearchCriteriaData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.integration.common.data.Sailing;
import com.bcf.integration.common.data.SailingLeg;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.listSailingResponse.data.BcfListSailingsResponseData;


/**
 * BC Ferries implementation of the {@link DefaultTransportOfferingFacade}
 */
public class DefaultBcfTransportOfferingFacade extends DefaultTransportOfferingFacade implements BcfTransportOfferingFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfTransportOfferingFacade.class);
	private static final String ENABLE_MOCK = "ebooking.sailing.list.V2.service.url.mock.enable";

	private BcfTransportOfferingService bcftransportOfferingService;
	private BCFTransportFacilityFacade bcfTransportFacilityFacade;

	@Override
	public List<TransportOfferingData> getDefaultTransportOfferingsForTravelSector(final String travelSectorCode)
	{
		final List<TransportOfferingModel> transportOfferingModels = getBcftransportOfferingService()
				.getDefaultTransportOfferingBySector(travelSectorCode);
		return CollectionUtils.isNotEmpty(transportOfferingModels) ?
				getTransportOfferingConverter().convertAll(transportOfferingModels) : Collections.emptyList();
	}

	@Override
	protected Queue<List<TransportOfferingData>> buildTransportOfferingConnections(
			final OriginDestinationInfoData originDestinationInfo, final List<TravelSectorData> travelSectors,
			final FareSearchRequestData fareSearchRequestData)
	{
		boolean isFirstSector = true;
		final Queue<List<TransportOfferingData>> transportOfferingConnections = getNewPriorityQueue();
		for (final TravelSectorData travelSector : travelSectors)
		{
			// call to Solr to get a list of TransportOfferingData for the travel sector for the current departure date
			List<TransportOfferingData> transportOfferings = searchTransportOfferings(travelSector,
					originDestinationInfo.getDepartureTime(), fareSearchRequestData);
			LOG.debug(String.format("Sector code is [%s] and offerings are [%s]", travelSector.getCode(),
					transportOfferings.stream().map(TransportOfferingData::getCode).collect(Collectors.joining(","))));
			if (isFirstSector)
			{
				buildInitialTransportOfferings(transportOfferingConnections, transportOfferings);
				isFirstSector = false;
			}
			else
			{
				if (checkForOverNightTransportOfferingConnections(transportOfferingConnections))
				{
					final Date nextDate = TravelDateUtils.addDays(originDestinationInfo.getDepartureTime(), 1);
					if (CollectionUtils.isEmpty(transportOfferings))
					{
						transportOfferings = new ArrayList<>();
					}
					transportOfferings.addAll(searchTransportOfferings(travelSector, nextDate, fareSearchRequestData));
				}
				final Queue<List<TransportOfferingData>> updatedTransportOfferingConnections = buildTransportOfferingConnections(
						transportOfferingConnections, transportOfferings);
				transportOfferingConnections.addAll(updatedTransportOfferingConnections);
				// if no connected transport offerings are available on the subsequent sector, then the transport offerings from the previous sectors cannot be provided.
				if (transportOfferingConnections.isEmpty())
				{
					break;
				}
			}

		}
		transportOfferingConnections.forEach(transportOfferingDataList -> LOG.debug(String.format("Connection is  [%s]",
				StreamUtil.safeStream(transportOfferingDataList).map(TransportOfferingData::getCode)
						.collect(Collectors.joining(",")))));
		return transportOfferingConnections;
	}

	@Override
	protected boolean checkForOverNightTransportOfferingConnections(
			final Queue<List<TransportOfferingData>> transportOfferingConnections)
	{
		return transportOfferingConnections.stream().anyMatch(toConnection -> toConnection.stream().anyMatch(to -> {
			final Calendar cal = Calendar.getInstance();
			cal.setTime(to.getArrivalTime());
			final int arrivalDate = cal.get(Calendar.DATE);
			cal.setTime(to.getDepartureTime());
			final int departureDate = cal.get(Calendar.DATE);
			final int diff = arrivalDate - departureDate;
			return diff == 1;
		}));
	}

	@Override
	protected Queue<List<TransportOfferingData>> buildTransportOfferingConnections(
			final Queue<List<TransportOfferingData>> routeCombinations, final List<TransportOfferingData> transportOfferings)
	{
		final Queue<List<TransportOfferingData>> validTransportOfferingConnectionsQueue = getNewPriorityQueue();

		// loop through the queue routeCombinations
		while (!routeCombinations.isEmpty())
		{

			final List<TransportOfferingData> currentTransportOfferings = routeCombinations.remove();
			currentTransportOfferings.sort(Comparator.comparing(TransportOfferingData::getDepartureTime));

			transportOfferings.forEach(transportOffering -> {
				final Date lastArrival = StreamUtil.safeStream(currentTransportOfferings).reduce((first, second) -> second).get()
						.getArrivalTime();
				if (!transportOffering.getDepartureTime().before(lastArrival))
				{
					final List<TransportOfferingData> validTransportOfferingConnections = new LinkedList<>();
					validTransportOfferingConnections.addAll(currentTransportOfferings);
					validTransportOfferingConnections.add(transportOffering);
					validTransportOfferingConnectionsQueue.add(validTransportOfferingConnections);
				}
			});
		}

		return validTransportOfferingConnectionsQueue;
	}

	@Override
	protected void buildScheduledRoutes(final Queue<List<TransportOfferingData>> transportOfferingConnections,
			final TravelRouteData travelRoute, final List<ScheduledRouteData> scheduledRoutes, final int referenceNumber)
	{
		while (!transportOfferingConnections.isEmpty())
		{
			final ScheduledRouteData scheduledRoute = new ScheduledRouteData();
			scheduledRoute.setReferenceNumber(referenceNumber);
			scheduledRoute.setRoute(travelRoute);
			scheduledRoute.setTransportOfferings(transportOfferingConnections.remove());

			scheduledRoutes.add(scheduledRoute);
		}
	}

	@Override
	public TransportOfferingData getDefaultTransportOfferingForRoute(final String travelRouteCode)
	{
		final TransportOfferingModel transportOfferingModel = getBcftransportOfferingService()
				.getDefaultTransportOfferingByRoute(travelRouteCode);
		return Objects.isNull(transportOfferingModel) ? null : getTransportOfferingConverter().convert(transportOfferingModel);
	}

	@Override
	public DailySchedulesData searchTransportOfferings(final ScheduleSearchCriteriaData scheduleSearchCriteriaData)
	{

		final OriginDestinationInfoData originDestinationInfoData = scheduleSearchCriteriaData.getOriginDestinationInfo().get(0);
		final DestinationAndOriginForTravelRouteData travelRouteInfoForSourceAndDestination = bcfTransportFacilityFacade
				.getTravelRouteInfoForSourceAndDestination(originDestinationInfoData.getDepartureLocation(),
						originDestinationInfoData.getArrivalLocation());

		final DailySchedulesData dailySchedulesData = new DailySchedulesData();
		dailySchedulesData.setTravelRouteInfo(travelRouteInfoForSourceAndDestination);

		final FareSearchRequestData fareSearchRequestData = new FareSearchRequestData();
		fareSearchRequestData.setPassengerTypes(Collections.emptyList());
		fareSearchRequestData.setOriginDestinationInfo(scheduleSearchCriteriaData.getOriginDestinationInfo());

		final List<ScheduledRouteData> scheduledRoutes = getScheduledRoutes(fareSearchRequestData);
		dailySchedulesData.setScheduleRoutes(getFilteredScheduledRoutes(scheduledRoutes));
		return dailySchedulesData;
	}

	@Override
	public List<ScheduledRouteData> getScheduledRoutesFromSolr(final ScheduleSearchCriteriaData scheduleSearchCriteriaData)
	{
		final List<ScheduledRouteData> scheduledRoutes = new LinkedList();
		scheduleSearchCriteriaData.getOriginDestinationInfo().forEach(originDestinationInfo -> {
			final List<TravelRouteData> travelRoutes = this.getTravelRoutes(originDestinationInfo);
			travelRoutes.forEach(travelRoute -> {
				final List<TravelSectorData> travelSectors = travelRoute.getSectors();
				if (travelSectors != null)
				{
					final Queue<List<TransportOfferingData>> transportOfferingConnections = this
							.buildTOConnections(originDestinationInfo, travelSectors, scheduleSearchCriteriaData);
					this.buildScheduledRoutes(transportOfferingConnections, travelRoute, scheduledRoutes,
							originDestinationInfo.getReferenceNumber());
				}

			});
		});
		return scheduledRoutes;
	}

	@Override
	public Queue<List<TransportOfferingData>> buildTOConnections(
			final OriginDestinationInfoData originDestinationInfo, final List<TravelSectorData> travelSectors,
			final ScheduleSearchCriteriaData scheduleSearchCriteriaData)
	{
		final Queue<List<TransportOfferingData>> transportOfferingConnections = getNewPriorityQueue();
		for (final TravelSectorData travelSector : travelSectors)
		{
			// call to Solr to get a list of TransportOfferingData for the travel sector for the current departure date
			final List<TransportOfferingData> transportOfferings = searchTransportOfferingsFromSolr(travelSector,
					originDestinationInfo.getDepartureTime(), scheduleSearchCriteriaData);
			buildInitialTransportOfferings(transportOfferingConnections, transportOfferings);
		}
		return transportOfferingConnections;
	}

	@Override
	public List<CalendarData> getDepartureDates(final OriginDestinationInfoData originDestinationInfoData, final String startDate,
			final String endDate)
	{
		final List<TransportOfferingData> transportOfferings = fetchTransportOfferings(originDestinationInfoData, startDate,
				endDate);

		if (CollectionUtils.isEmpty(transportOfferings))
		{
			return Collections.emptyList();
		}

		final ZoneId zoneId = Calendar.getInstance().getTimeZone().toZoneId();
		final Set<LocalDate> departureDatesSet = transportOfferings.stream()
				.map(transportOffering -> transportOffering.getDepartureTime().toInstant().atZone(
						zoneId).toLocalDate()).collect(
						Collectors.toSet());

		final Map<Integer, List<LocalDate>> datesByYear = departureDatesSet.stream()
				.collect(Collectors.groupingBy(LocalDate::getYear));

		final Set<Map.Entry<Integer, List<LocalDate>>> datesEntry = datesByYear.entrySet();

		final List<CalendarData> calendarDataList = new ArrayList<>();
		datesEntry.forEach(entry -> {
			final Integer year = entry.getKey();
			final Map<Integer, List<Integer>> datesByMonth = entry.getValue().stream().collect(Collectors
					.groupingBy(LocalDate::getMonthValue, Collectors.collectingAndThen(Collectors.toList(),
							localDates -> localDates.stream().map(LocalDate::getDayOfMonth).collect(Collectors.toList()))));
			final Set<Map.Entry<Integer, List<Integer>>> dateEntriesByMonth = datesByMonth.entrySet();
			dateEntriesByMonth.forEach(dateEntry -> {
				final CalendarData calendarData = new CalendarData();
				calendarData.setYear(year);
				calendarData.setMonth(dateEntry.getKey());
				calendarData.setDates(dateEntry.getValue());
				calendarDataList.add(calendarData);
			});
		});

		return calendarDataList;

	}

	@Override
	public List<TransportOfferingData> fetchTransportOfferings(final OriginDestinationInfoData originDestinationInfo,
			final String startDate, final String endDate)
	{
		final List<TransportOfferingData> transportOfferings = new ArrayList<>();
		final List<TravelRouteData> travelRoutes = getTravelRoutes(originDestinationInfo);
		travelRoutes.forEach(route -> {
			final List<TravelSectorData> travelSectors = route.getSectors();
			final TravelSectorData travelSector = travelSectors.get(0);
			final TransportOfferingSearchPageData<SearchData, TransportOfferingData> searchPageData = getTransportOfferingSearchFacade()
					.transportOfferingSearch(createSearchDataForDateRange(travelSector, startDate, endDate));
			List<TransportOfferingData> transportOfferingsList = searchPageData.getResults();
			if (route.isIndirectRoute() && CollectionUtils.isNotEmpty(transportOfferingsList))
			{
				transportOfferingsList = transportOfferingsList.stream()
						.filter(transportOffering -> StringUtils.isNotBlank(transportOffering.getTransferIdentifier())).collect(
								Collectors.toList());
			}
			transportOfferings.addAll(transportOfferingsList);
		});
		return transportOfferings;
	}

	@Override
	public List<ScheduledRouteData> getSheduledRoutesFromResponse(final List<BcfListSailingsResponseData> sailingsList,
			final OriginDestinationInfoData originDestinationInfoData, final List<String> sailingCodesInResponse)
	{
		final List<Sailing> nonTransferSailings = StreamUtil.safeStream(sailingsList)
				.flatMap(bcfListSailingsResponseData -> bcfListSailingsResponseData.getSailing().stream())
				.filter(sailing -> StringUtils.isBlank(sailing.getTransferSailingIdentifier()))
				.collect(Collectors.toList());
		final List<ScheduledRouteData> scheduledRouteDataList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(nonTransferSailings))
		{
			final List<TravelRouteData> travelRouteDataList = getTravelRoutes(originDestinationInfoData);
			final Map<String, Integer> sectorCodeMap = getSectorCodeMap(travelRouteDataList);

			final boolean isMockEnable = getConfigurationService().getConfiguration()
					.getBoolean(ENABLE_MOCK, Boolean.FALSE);
			Date departureDate = null;
			if (isMockEnable)
			{
				departureDate = originDestinationInfoData.getDepartureTime();
			}
			final TransportOfferingSearchPageData<SearchData, TransportOfferingData> searchPageData = this
					.getTransportOfferingSearchFacade()
					.transportOfferingSearch(
							this.createTransportOfferingSearchData(sailingCodesInResponse, departureDate));
			for (final Sailing ferry : nonTransferSailings)
			{
				for (final SailingLine sailingLine : ferry.getLine())
				{
					populateScheduleRouteList(originDestinationInfoData, scheduledRouteDataList, travelRouteDataList, sectorCodeMap,
							searchPageData,
							sailingLine);
				}
			}
		}
		return scheduledRouteDataList;
	}

	private void populateScheduleRouteList(final OriginDestinationInfoData originDestinationInfoData,
			final List<ScheduledRouteData> scheduledRouteDataList, final List<TravelRouteData> travelRouteDataList,
			final Map<String, Integer> sectorCodeMap,
			final TransportOfferingSearchPageData<SearchData, TransportOfferingData> searchPageData, final SailingLine sailingLine)
	{
		if (isSameDay(sailingLine.getLineInfo().getDepartureDateTime(), originDestinationInfoData.getDepartureTime()))
		{
			final TravelRouteData travelRouteData;
			final List<String> responseSectors = new ArrayList<>();
			String sectorCodes = "";
			if (Objects.nonNull(sailingLine.getLineInfo().getSailingLegs()) && CollectionUtils
					.isNotEmpty(sailingLine.getLineInfo().getSailingLegs().getSailingLeg()))
			{
				for (final SailingLeg sailingLeg : sailingLine.getLineInfo().getSailingLegs().getSailingLeg())
				{
					final String[] arrivalSectorPort = sailingLeg.getArrivalPortCode().split("-");
					final String[] departureSectorPort = sailingLeg.getDeparturePortCode().split("-");
					sectorCodes = sectorCodes.concat(departureSectorPort[0].trim() + "-" + arrivalSectorPort[0].trim() + ",");
					responseSectors.add(departureSectorPort[0].trim() + "-" + arrivalSectorPort[0].trim());
				}

				travelRouteData = getTravelRouteData(travelRouteDataList, sectorCodeMap, sectorCodes);
			}
			else
			{
				sectorCodes = sailingLine.getLineInfo().getDeparturePortCode().trim() + "-" + sailingLine.getLineInfo()
						.getArrivalPortCode()
						.trim() + ",";
				responseSectors.add(sailingLine.getLineInfo().getDeparturePortCode().trim() + "-" + sailingLine.getLineInfo()
						.getArrivalPortCode().trim());

				travelRouteData = getTravelRouteData(travelRouteDataList, sectorCodeMap, sectorCodes);
			}
			final List<TransportOfferingData> transportOfferings = new ArrayList<>();
			transportOfferings.addAll(
					searchPageData.getResults().stream()
							.filter(offering -> responseSectors.contains(offering.getSector().getCode()) && offering
									.getEbookingSailingCode().equals(sailingLine.getLineInfo().getSailingCode()))
							.collect(Collectors.toList()));
			transportOfferings.sort(Comparator.comparing(TransportOfferingData::getDepartureTime));
			if (CollectionUtils.isNotEmpty(transportOfferings) && Objects.nonNull(travelRouteData))
			{
				scheduledRouteDataList
						.add(getSheduledRouteData(transportOfferings, originDestinationInfoData, travelRouteData));
			}
		}
	}

	private TravelRouteData getTravelRouteData(final List<TravelRouteData> travelRouteDataList,
			final Map<String, Integer> sectorCodeMap, final String sectorCodes)
	{
		final TravelRouteData travelRouteData;
		if (sectorCodeMap.containsKey(sectorCodes))
		{
			travelRouteData = travelRouteDataList.get(sectorCodeMap.get(sectorCodes));
		}
		else
			travelRouteData = null;
		return travelRouteData;
	}

	@Override
	public List<ScheduledRouteData> getsheduledRoutesForTransferSailingFromResponse(
			final List<BcfListSailingsResponseData> sailingsList,
			final OriginDestinationInfoData originDestinationInfoData, final List<String> transferIdentifierInResponse)
	{
		final List<Sailing> transferSailings = StreamUtil.safeStream(sailingsList)
				.flatMap(bcfListSailingsResponseData -> bcfListSailingsResponseData.getSailing().stream())
				.filter(sailing -> StringUtils.isNotBlank(sailing.getTransferSailingIdentifier()))
				.collect(Collectors.toList());
		final List<ScheduledRouteData> scheduledRouteDataList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(transferSailings))
		{
			final List<TravelRouteData> travelRouteDataList = getTravelRoutes(originDestinationInfoData);
			final Map<String, Integer> sectorCodeMap = getSectorCodeMap(travelRouteDataList);

			final boolean isMockEnable = getConfigurationService().getConfiguration()
					.getBoolean(ENABLE_MOCK, Boolean.FALSE);
			Date departureDate = null;
			if (isMockEnable)
			{
				departureDate = originDestinationInfoData.getDepartureTime();
			}
			final TransportOfferingSearchPageData<SearchData, TransportOfferingData> searchPageData = this
					.getTransportOfferingSearchFacade()
					.transportOfferingSearch(
							this.createTransportOfferingSearchDataForTransferIdentifier(transferIdentifierInResponse, departureDate));
			final Map<String, List<TransportOfferingData>> offeringSalingCodeMap = searchPageData.getResults().stream()
					.collect(Collectors.groupingBy(TransportOfferingData::getEbookingSailingCode));
			final List<TransportOfferingData> transportOfferings = new ArrayList<>();
			String transferCodes = "";
			for (final Map.Entry<String, List<TransportOfferingData>> mapEntry : offeringSalingCodeMap.entrySet())
			{
				transferCodes = getTransferCodes(originDestinationInfoData, transferSailings, transportOfferings, transferCodes,
						mapEntry);
				getTravelRouteData(travelRouteDataList, sectorCodeMap, transferCodes);
			}
			if (CollectionUtils.isNotEmpty(transportOfferings) && Objects.nonNull(travelRouteDataList.get(0)))
			{
				transportOfferings.stream().collect(Collectors.groupingBy(o -> o.getTransferIdentifier())).values()
						.forEach(transportOfferingDataList -> {
									scheduledRouteDataList
											.add(getSheduledRouteData(transportOfferingDataList, originDestinationInfoData,
													travelRouteDataList.get(0)));
								}

						);
			}
		}
		return scheduledRouteDataList;
	}

	private String getTransferCodes(final OriginDestinationInfoData originDestinationInfoData,
			final List<Sailing> transferSailings, final List<TransportOfferingData> transportOfferings, String transferCodes,
			final Map.Entry<String, List<TransportOfferingData>> mapEntry)
	{
		for (final Sailing ferry : transferSailings)
		{
			for (final SailingLine sailingLine : ferry.getLine())
			{
				transferCodes = addTransportOfferings(originDestinationInfoData, transportOfferings, transferCodes, mapEntry,
						sailingLine);
				if (transferCodes == null)
					break;
			}
		}
		return transferCodes;
	}

	private String addTransportOfferings(final OriginDestinationInfoData originDestinationInfoData,
			final List<TransportOfferingData> transportOfferings, String transferCodes,
			final Map.Entry<String, List<TransportOfferingData>> mapEntry, final SailingLine sailingLine)
	{
		if (sailingLine.getLineInfo().getSailingCode().equals(mapEntry.getKey()) && isSameDay(
				sailingLine.getLineInfo().getDepartureDateTime(), originDestinationInfoData.getDepartureTime()))
		{
			if (StringUtils.isBlank(transferCodes))
			{
				transferCodes = "";
			}
			transferCodes = transferCodes.concat(
					sailingLine.getLineInfo().getDeparturePortCode().trim() + "-" + sailingLine.getLineInfo()
							.getArrivalPortCode().trim() + ",");
			final List<String> responseSectors = new ArrayList<>();
			populateResponseSectors(sailingLine, responseSectors);
			transportOfferings.addAll(
					mapEntry.getValue().stream()
							.filter(offering -> responseSectors.contains(offering.getSector().getCode()) && offering
									.getEbookingSailingCode().equals(sailingLine.getLineInfo().getSailingCode()))
							.collect(Collectors.toList()));
			transportOfferings.sort(Comparator.comparing(TransportOfferingData::getDepartureTime));
		}
		else
			return null;
		return transferCodes;
	}

	private void populateResponseSectors(final SailingLine sailingLine, final List<String> responseSectors)
	{
		if (Objects.nonNull(sailingLine.getLineInfo().getSailingLegs()) && CollectionUtils
				.isNotEmpty(sailingLine.getLineInfo().getSailingLegs().getSailingLeg()))
		{
			for (final SailingLeg sailingLeg : sailingLine.getLineInfo().getSailingLegs().getSailingLeg())
			{
				final String[] arrivalSectorPort = sailingLeg.getArrivalPortCode().split("-");
				final String[] departureSectorPort = sailingLeg.getDeparturePortCode().split("-");
				responseSectors.add(departureSectorPort[0].trim() + "-" + arrivalSectorPort[0].trim());
			}
		}
		else
		{
			responseSectors
					.add(sailingLine.getLineInfo().getDeparturePortCode().trim() + "-" + sailingLine.getLineInfo()
							.getArrivalPortCode().trim());

		}
	}

	@Override
	public Map<String, String> getSailingDurations(final List<ScheduledRouteData> scheduledRouteData)
	{
		final Map<String, String> minMaxDurations = new HashMap<>(2);
		if (CollectionUtils.isEmpty(scheduledRouteData))
		{
			LOG.error("scheduledRoutesData must not be empty");
			return minMaxDurations;
		}
		int min = 0;
		int max = 0;
		Period minPeriod = null;
		Period maxPeriod = null;
		for (final ScheduledRouteData routeData : scheduledRouteData)
		{
			final TransportOfferingData firstOffering = StreamUtil.safeStream(routeData.getTransportOfferings()).findFirst().get();
			final TransportOfferingData lastOffering = StreamUtil.safeStream(routeData.getTransportOfferings())
					.reduce((first, second) -> second).get();
			final DateTime startTime = new DateTime(firstOffering.getDepartureTime());
			final DateTime endTime = new DateTime(lastOffering.getArrivalTime());
			final Period p = new Period(startTime, endTime);
			final int days = p.getDays();
			final int hours = p.getHours();
			final int minutes = p.getMinutes();
			final int inMins = (days * 24 * 60) + (hours * 60) + minutes;
			if (inMins < min || min == 0)
			{
				min = inMins;
				minPeriod = p;
			}
			if (inMins > max)
			{
				max = inMins;
				maxPeriod = p;
			}
		}
		minMaxDurations.put("minDuration", getDuration(minPeriod));
		minMaxDurations.put("maxDuration", getDuration(maxPeriod));
		return minMaxDurations;
	}

	private String getDuration(final Period period)
	{
		if (Objects.isNull(period))
		{
			return null;
		}
		String duration = "";
		if (period.getDays() > 0)
		{
			duration += period.getDays() + "d ";
		}
		if (period.getHours() > 0)
		{
			duration += period.getHours() + "h ";
		}
		if (period.getMinutes() > 0)
		{
			duration += period.getMinutes() + "m";
		}
		return duration;
	}

	protected Map<String, Integer> getSectorCodeMap(final List<TravelRouteData> travelRouteDataList)
	{
		final Map<String, Integer> sectorCodeMap = new HashedMap();
		for (int i = 0; i < travelRouteDataList.size(); i++)
		{
			final List<TravelSectorData> travelSectorDataList = travelRouteDataList.get(i).getSectors();
			if (CollectionUtils.isNotEmpty(travelSectorDataList))
			{
				String sectorCodes = StringUtils.EMPTY;
				for (final TravelSectorData travelSectorData : travelSectorDataList)
				{
					if (Objects.nonNull(travelSectorData.getOrigin()) && Objects.nonNull(travelSectorData.getDestination()))
					{
						sectorCodes = sectorCodes.concat(travelSectorData.getOrigin().getCode().trim() + "-"
								+ travelSectorData.getDestination().getCode().trim() + ",");
					}
				}
				if (StringUtils.isNotBlank(sectorCodes))
				{
					sectorCodeMap.put(sectorCodes, i);
				}
			}
		}
		return sectorCodeMap;
	}

	protected SearchData createTransportOfferingSearchData(final List<String> sailingCode, final Date departureDate)
	{
		final SearchData searchData = new SearchData();
		searchData.setSearchType(BcfFacadesConstants.TRANSPORT_OFFERING);
		final Map<String, String> filterTerms = new HashMap();
		String filterQuery = StringUtils.EMPTY;
		for (final String code : sailingCode)
		{
			filterQuery = filterQuery.concat(code + "#");
		}
		if (StringUtils.isNotEmpty(filterQuery))
		{
			filterTerms.put(BcfFacadesConstants.EBOOKING_SAILINGCODE, filterQuery);
		}
		if (Objects.nonNull(departureDate))
		{
			filterTerms.put(BcfFacadesConstants.DEPARTURE_DATE,
					TravelDateUtils.convertDateToStringDate(departureDate, BcfFacadesConstants.TRANSPORTOFFERING_DATE_FORMAT));
		}
		searchData.setFilterTerms(filterTerms);
		return searchData;
	}

	protected SearchData createTransportOfferingSearchDataForTransferIdentifier(final List<String> transferIdentifier,
			final Date departureDate)
	{
		final SearchData searchData = new SearchData();
		searchData.setSearchType(BcfFacadesConstants.TRANSPORT_OFFERING);
		final Map<String, String> filterTerms = new HashMap();
		String filterQuery = "";
		for (final String code : transferIdentifier)
		{
			filterQuery = filterQuery.concat(code + "#");
		}
		filterTerms.put(BcfFacadesConstants.EBOOKING_TRANSFERIDENTIFIER, filterQuery);
		if (Objects.nonNull(departureDate))
		{
			filterTerms.put(BcfFacadesConstants.DEPARTURE_DATE,
					TravelDateUtils.convertDateToStringDate(departureDate, BcfFacadesConstants.TRANSPORTOFFERING_DATE_FORMAT));
		}
		searchData.setFilterTerms(filterTerms);
		return searchData;
	}

	protected ScheduledRouteData getSheduledRouteData(final List<TransportOfferingData> transportOfferingDataList,
			final OriginDestinationInfoData originDestinationInfoData, final TravelRouteData travelRouteData)
	{
		final ScheduledRouteData scheduledRoute = new ScheduledRouteData();
		scheduledRoute.setReferenceNumber(originDestinationInfoData.getReferenceNumber());
		scheduledRoute.setTransportOfferings(transportOfferingDataList);
		scheduledRoute.setRoute(travelRouteData);

		return scheduledRoute;
	}

	protected SearchData createSearchDataForDateRange(final TravelSectorData travelSector, final String startingDepartureDate,
			final String endingDepartureDate)
	{
		final SearchData searchData = new SearchData();
		searchData.setSearchType(BcfFacadesConstants.TRANSPORT_OFFERING);
		final Map<String, String> filterTerms = new HashMap<>();
		final Map<String, String> rawQueryFilterTerms = new HashMap<>();
		filterTerms.put("originTransportFacility", travelSector.getOrigin().getCode());
		filterTerms.put("destinationTransportFacility", travelSector.getDestination().getCode());

		rawQueryFilterTerms.put("departureTime", startingDepartureDate + "_" + endingDepartureDate);

		searchData.setFilterTerms(filterTerms);
		searchData.setRawQueryFilterTerms(rawQueryFilterTerms);

		return searchData;
	}


	protected List<TransportOfferingData> searchTransportOfferingsFromSolr(final TravelSectorData travelSector,
			final Date departureDate,
			final ScheduleSearchCriteriaData scheduleSearchCriteriaData)
	{
		final TransportOfferingSearchPageData<SearchData, TransportOfferingData> searchPageData = this
				.getTransportOfferingSearchFacade().transportOfferingSearch(this.createSearchData(travelSector, departureDate));
		final List<TransportOfferingData> transportOfferings = searchPageData.getResults();
		if (CollectionUtils.isNotEmpty(transportOfferings))
		{
			return transportOfferings;
		}
		else
		{
			return Collections.emptyList();
		}
	}

	private boolean isSameDay(final String sailingDepartureTime, final Date requestedDepartureTime)
	{
		final boolean isMockEnable = getConfigurationService().getConfiguration()
				.getBoolean(ENABLE_MOCK, Boolean.FALSE);
		String datePattern = BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN;
		if (isMockEnable)
		{
			datePattern = BcfintegrationcoreConstants.DATE_TIME_FORMAT;
		}
		return DateTimeComparator.getDateOnlyInstance()
				.compare(TravelDateUtils.convertStringDateToDate(sailingDepartureTime, datePattern), requestedDepartureTime) == 0;
	}

	private List<ScheduledRouteData> getFilteredScheduledRoutes(final List<ScheduledRouteData> scheduledRoutes)
	{
		final List<ScheduledRouteData> uniqueScheduledRoutes = new ArrayList<>();
		final Map<String, ScheduledRouteData> scheduledRouteByUniqueOfferings = new LinkedHashMap<>();
		StreamUtil.safeStream(scheduledRoutes).forEach(scheduledRoute -> {
			if (CollectionUtils.isNotEmpty(scheduledRoute.getTransportOfferings()))
			{
				//sort transport offerings on the basis of departure time
				scheduledRoute.getTransportOfferings().sort(Comparator.comparing(TransportOfferingData::getDepartureTime));

				//either e-bookingSailingCode or transferIdentifier of every consecutive pair should match to qualify it as valid connection
				if (isValidConnection(scheduledRoute.getTransportOfferings()))
				{
					//filter out connection in which vehicle is missing in any transport offering
					final boolean hasAllScheduleWithVehicle = StreamUtil.safeStream(scheduledRoute.getTransportOfferings())
							.allMatch(transportOffering -> hasValidVehicle(transportOffering));

					if (hasAllScheduleWithVehicle)
					{
						//generate unique key by concatenating e-booking sailing codes
						final String uniqueKey = StreamUtil.safeStream(scheduledRoute.getTransportOfferings())
								.map(TransportOfferingData::getEbookingSailingCode).collect(Collectors.joining(","));

						//filter out duplicates
						scheduledRouteByUniqueOfferings.put(uniqueKey, scheduledRoute);
					}
				}
			}
		});

		//filter out similar routes by first transport offering but wrong combinations
		final Map<String, List<ScheduledRouteData>> scheduledRoutesByFirstTO = StreamUtil
				.safeStream(scheduledRouteByUniqueOfferings.values()).collect(Collectors.groupingBy(
						scheduledRoute -> scheduledRoute.getTransportOfferings().stream().findFirst().get().getEbookingSailingCode()));
		StreamUtil.safeStream(scheduledRoutesByFirstTO.values()).forEach(scheduledRouteList -> {
			scheduledRouteList.sort(Comparator.comparing(scheduledRouteData -> scheduledRouteData.getTransportOfferings().size()));
			Collections.reverse(scheduledRouteList);
			uniqueScheduledRoutes.add(scheduledRouteList.get(0));
		});

		//sort schedule routes on the basis of departure time of first transport offering
		uniqueScheduledRoutes.sort(Comparator.comparing(
				scheduledRouteData -> StreamUtil.safeStream(scheduledRouteData.getTransportOfferings()).findFirst().get()
						.getDepartureTime()));

		return uniqueScheduledRoutes;
	}

	private boolean isValidConnection(final List<TransportOfferingData> transportOfferings)
	{
		if (StreamUtil.safeStream(transportOfferings)
				.anyMatch(transportOffering -> StringUtils.isBlank(transportOffering.getEbookingSailingCode())))
		{
			return false;
		}
		if (CollectionUtils.size(transportOfferings) == 1)
		{
			return true;
		}
		final Iterator<TransportOfferingData> transportOfferingsIterator = transportOfferings.iterator();
		TransportOfferingData currentTransportOffering = null;
		while (transportOfferingsIterator.hasNext())
		{
			currentTransportOffering =
					currentTransportOffering == null ? transportOfferingsIterator.next() : currentTransportOffering;
			final TransportOfferingData nextTransportOffering = transportOfferingsIterator.next();
			if (!(StringUtils.equalsIgnoreCase(currentTransportOffering.getEbookingSailingCode(),
					nextTransportOffering.getEbookingSailingCode()) || (
					StringUtils.isNotBlank(currentTransportOffering.getTransferIdentifier()) && StringUtils
							.equalsIgnoreCase(currentTransportOffering.getTransferIdentifier(),
									nextTransportOffering.getTransferIdentifier()))))
			{
				return false;
			}
			currentTransportOffering = nextTransportOffering;
		}
		return true;
	}


	//Overriding this method to apply additional logging
	@Override
	public List<ScheduledRouteData> getScheduledRoutes(final FareSearchRequestData fareSearchRequestData)
	{
		final List<ScheduledRouteData> scheduledRoutes = new LinkedList();
		fareSearchRequestData.getOriginDestinationInfo().forEach((originDestinationInfo) -> {
			LOG.debug(String.format("Searching travel routes for source [%s] and destination [%s]",
					originDestinationInfo.getDepartureLocation(), originDestinationInfo.getArrivalLocation()));
			final List<TravelRouteData> travelRoutes = this.getTravelRoutes(originDestinationInfo);
			travelRoutes.forEach((travelRoute) -> {
				final List<TravelSectorData> travelSectors = travelRoute.getSectors();
				if (travelSectors != null)
				{
					LOG.debug(
							String.format("Route code is [%s] and sector codes are [%s]", travelRoute.getCode(), travelSectors.stream()
									.map(TravelSectorData::getCode).collect(Collectors.joining(","))));
					final Queue<List<TransportOfferingData>> transportOfferingConnections = this
							.buildTransportOfferingConnections(originDestinationInfo, travelSectors, fareSearchRequestData);
					this.buildScheduledRoutes(transportOfferingConnections, travelRoute, scheduledRoutes,
							originDestinationInfo.getReferenceNumber());
				}
			});
		});
		return scheduledRoutes;
	}

	private boolean hasValidVehicle(final TransportOfferingData transportOffering)
	{
		return Objects.nonNull(transportOffering) && Objects.nonNull(transportOffering.getTransportVehicle()) && Objects
				.nonNull(transportOffering.getTransportVehicle().getVehicleInfo()) && StringUtils
				.isNotBlank(transportOffering.getTransportVehicle().getVehicleInfo().getCode());
	}

	public BcfTransportOfferingService getBcftransportOfferingService()
	{
		return bcftransportOfferingService;
	}

	@Required
	public void setBcftransportOfferingService(final BcfTransportOfferingService bcftransportOfferingService)
	{
		this.bcftransportOfferingService = bcftransportOfferingService;
	}

	protected BCFTransportFacilityFacade getBcfTransportFacilityFacade()
	{
		return bcfTransportFacilityFacade;
	}

	@Required
	public void setBcfTransportFacilityFacade(final BCFTransportFacilityFacade bcfTransportFacilityFacade)
	{
		this.bcfTransportFacilityFacade = bcfTransportFacilityFacade;
	}
}
