/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PassengerFareData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationItineraryPricingInfoHandler;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


public class DefaultBcfReservationItineraryPricingInfoHandler extends ReservationItineraryPricingInfoHandler
{
	@Override
	protected ItineraryPricingInfoData createItineraryPricingInfo(final AbstractOrderModel abstractOrderModel,
			final ReservationItemData reservationItem)
	{


		final ItineraryPricingInfoData itineraryPricingInfo = new ItineraryPricingInfoData();
		itineraryPricingInfo.setAvailable(Boolean.TRUE);
		final List<AbstractOrderEntryModel> bundledEntries = abstractOrderModel.getEntries().stream()
				.filter(e -> OrderEntryType.TRANSPORT.equals(e.getType()))
				.filter(e -> e.getActive() && e.getBundleNo() != 0
						&& e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && e.getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber() == reservationItem.getOriginDestinationRefNumber())
				.collect(Collectors.toList());

		itineraryPricingInfo.setPtcFareBreakdownDatas(createPTCFareBreakdowns(bundledEntries, reservationItem));

		itineraryPricingInfo.setTravellerBreakdowns(createTravellerBreakdowns(bundledEntries, reservationItem));

		itineraryPricingInfo.setVehicleFareBreakdownDatas(createVehicleFareBreakdowns(bundledEntries, reservationItem));

		final Optional<AbstractOrderEntryModel> abstractOrderEntry = bundledEntries.stream()
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype())).findAny();
		if (!abstractOrderEntry.isPresent() || Objects.isNull(abstractOrderEntry.get().getBundleTemplate()))
		{
			return itineraryPricingInfo;
		}
		final BundleTemplateModel bundleTemplateModel = abstractOrderEntry.get().getBundleTemplate();

		final BundleType bundleType = Objects.nonNull(bundleTemplateModel.getType()) ? bundleTemplateModel.getType()
				: bundleTemplateModel.getParentTemplate().getType();
		itineraryPricingInfo.setBundleType(bundleType.getCode());
		itineraryPricingInfo.setBundleTypeName(getEnumerationService().getEnumerationName(bundleType));

		return itineraryPricingInfo;

	}

	protected List<VehicleFareBreakdownData> createVehicleFareBreakdowns(final List<AbstractOrderEntryModel> bundledEntries,
			final ReservationItemData reservationItem)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantities = retrieveVehicleTypeQuantitiesFromTravellers(
				reservationItem.getReservationItinerary().getTravellers());

		final List<VehicleFareBreakdownData> vehicleFareBreakdowns = new ArrayList<VehicleFareBreakdownData>(1);
		vehicleTypeQuantities.forEach(vehicleTypeQuantity -> {
			final VehicleFareBreakdownData vehicleFareBreakdown = createVehicleFareBreakdown(bundledEntries, vehicleTypeQuantity);
			vehicleFareBreakdowns.add(vehicleFareBreakdown);
		});

		return vehicleFareBreakdowns;
	}

	protected List<VehicleTypeQuantityData> retrieveVehicleTypeQuantitiesFromTravellers(final List<TravellerData> travellers)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantities = new ArrayList<VehicleTypeQuantityData>();
		travellers.forEach(traveller -> {
			if (TravellerType.VEHICLE.getCode().equals(traveller.getTravellerType()))
			{
				final VehicleInformationData vehicleInfo = (VehicleInformationData) traveller.getTravellerInfo();
				if (Objects.nonNull(vehicleInfo.getVehicleType()))
				{
					final VehicleTypeQuantityData veh = getVehicleTypeQuantityFromList(vehicleTypeQuantities,
							vehicleInfo.getVehicleType().getCode());
					if (veh != null)
					{
						final int updatedCount = veh.getQty() + 1;
						veh.setQty(updatedCount);
					}
					else
					{
						final VehicleTypeQuantityData vehicleTypeQuantity = new VehicleTypeQuantityData();
						vehicleTypeQuantity.setVehicleType(vehicleInfo.getVehicleType());
						vehicleTypeQuantity.setQty(1);
						vehicleTypeQuantity.setLength((int) vehicleInfo.getLength());
						vehicleTypeQuantity.setHeight((int) vehicleInfo.getHeight());
						vehicleTypeQuantity.setWidth((int) vehicleInfo.getWidth());
						vehicleTypeQuantity.setCarryingLivestock(vehicleInfo.isCarryingLivestock());
						vehicleTypeQuantity.setVehicleWithSidecarOrTrailer(vehicleInfo.isVehicleWithSidecarOrTrailer());
						vehicleTypeQuantities.add(vehicleTypeQuantity);
					}
				}
			}
		});
		return vehicleTypeQuantities;
	}

	protected VehicleFareBreakdownData createVehicleFareBreakdown(final List<AbstractOrderEntryModel> bundledEntries,
			final VehicleTypeQuantityData vehicleTypeQuantity)
	{
		final VehicleFareBreakdownData vehicleFareBreakdown = new VehicleFareBreakdownData();
		vehicleFareBreakdown.setVehicleTypeQuantity(vehicleTypeQuantity);
		final String vehicleTypeCode = vehicleTypeQuantity.getVehicleType().getCode();
		vehicleFareBreakdown.setFareBasisCodes(retreiveFareBasisCodesForVehicleType(bundledEntries, vehicleTypeCode));
		vehicleFareBreakdown.setFareInfos(createFareInfosForVehicleTypes(bundledEntries, vehicleTypeCode));
		vehicleFareBreakdown.setVehicleFare(retreivePerVehicleTypeFare(bundledEntries, vehicleTypeCode));
		return vehicleFareBreakdown;
	}

	protected PassengerFareData retreivePerVehicleTypeFare(final List<AbstractOrderEntryModel> bundledEntries,
			final String vehicleTypeCode)
	{
		BigDecimal basePrice = BigDecimal.ZERO;
		BigDecimal totalPrice = BigDecimal.ZERO;
		for (final AbstractOrderEntryModel entry : bundledEntries)
		{
			final String entryVehicleTypeCode = getVehicleTypeCodeFromEntry(entry);
			if (StringUtils.isNotEmpty(entryVehicleTypeCode) && entryVehicleTypeCode.equals(vehicleTypeCode))
			{
				totalPrice = totalPrice.add(BigDecimal.valueOf(entry.getTotalPrice()));
				if (FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				{
					basePrice = basePrice.add(BigDecimal.valueOf(entry.getBasePrice()));
				}
			}
		}
		final PassengerFareData vehicleFare = new PassengerFareData();
		vehicleFare.setBaseFare(
				getPriceDataFactory().create(PriceDataType.BUY, basePrice, getCommonI18NService().getCurrentCurrency().getIsocode()));
		vehicleFare.setTotalFare(getPriceDataFactory().create(PriceDataType.BUY, totalPrice,
				getCommonI18NService().getCurrentCurrency().getIsocode()));
		return vehicleFare;
	}

	protected String retreiveFareBasisCodesForVehicleType(final List<AbstractOrderEntryModel> bundledEntries,
			final String vehicleTypeCode)
	{
		final List<String> fareBasisCodes = new ArrayList<String>();
		final List<AbstractOrderEntryModel> fareProductEntries = bundledEntries.stream()
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		fareProductEntries.forEach(entry -> {
			final String entryVehicleTypeCode = getVehicleTypeCodeFromEntry(entry);
			if (StringUtils.isNotEmpty(entryVehicleTypeCode) && entryVehicleTypeCode.equals(vehicleTypeCode))
			{
				final String fareBasisCode = ((FareProductModel) entry.getProduct()).getFareBasisCode();
				if (!fareBasisCodes.contains(fareBasisCode))
				{
					fareBasisCodes.add(fareBasisCode);
				}
			}
		});

		return fareBasisCodes.get(0);
	}

	protected String getVehicleTypeCodeFromEntry(final AbstractOrderEntryModel entry)
	{
		if (CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTravellers()))
		{
			for (final TravellerModel traveller : entry.getTravelOrderEntryInfo().getTravellers())
			{
				if (TravellerType.VEHICLE.equals(traveller.getType()))
				{
					final BCFVehicleInformationModel vehicleInfo = (BCFVehicleInformationModel) traveller.getInfo();
					return vehicleInfo.getVehicleType().getType().getCode();
				}
			}
		}
		return StringUtils.EMPTY;
	}

	protected VehicleTypeQuantityData getVehicleTypeQuantityFromList(final List<VehicleTypeQuantityData> vehicleTypeQuantities,
			final String vehicleTypeCode)
	{
		for (final VehicleTypeQuantityData veh : vehicleTypeQuantities)
		{
			if (veh.getVehicleType().getCode().equals(vehicleTypeCode))
			{
				return veh;
			}
		}
		return null;
	}

	protected FareInfoData createFareInfosForVehicleTypes(final List<AbstractOrderEntryModel> bundledEntries,
			final String vehiclerTypeCode)
	{
		final List<FareDetailsData> fareDetailsList = new ArrayList<FareDetailsData>();
		final List<AbstractOrderEntryModel> fareProductEntries = bundledEntries.stream()
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		fareProductEntries.forEach(entry -> {
			final String entryVehicleTypeCode = getVehicleTypeCodeFromEntry(entry);
			if (StringUtils.isNotEmpty(entryVehicleTypeCode) && entryVehicleTypeCode.equals(vehiclerTypeCode))
			{
				final FareProductModel fareProduct = (FareProductModel) entry.getProduct();
				final FareDetailsData fareDetailsData = createFareDetails(getFareProductConverter().convert(fareProduct));
				fareDetailsList.add(fareDetailsData);

			}
		});

		final FareInfoData fareInfoData = createFareInfo(fareDetailsList);
		final List<FareInfoData> fareInfos = new ArrayList<FareInfoData>();
		fareInfos.add(fareInfoData);

		return fareInfoData;
	}
}
