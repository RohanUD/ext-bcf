/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.travelfacades.facades.impl.DefaultTravelCommercePriceFacade;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.price.data.PriceLevel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class DefaultBcfTravelCommercePriceFacade extends DefaultTravelCommercePriceFacade implements BCFTravelCommercePriceFacade
{
	private BCFTravelCommercePriceService bcfTravelCommercePriceService;

	@Override
	public void setPriceAndTaxSearchCriteriaInContext(final PriceLevel priceLevel, final List<String> transportOfferingCodes,
			final TravellerData travellerData)
	{
		String type = null;
		if (Objects.nonNull(travellerData))
		{
			if (StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_PASSENGER, travellerData.getTravellerType()))
			{
				final PassengerInformationData travellerInfo = (PassengerInformationData) travellerData.getTravellerInfo();
				type = travellerInfo.getPassengerType().getCode().toLowerCase();
			}
			else if (StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, travellerData.getTravellerType()))
			{
				final VehicleInformationData travellerInfo = (VehicleInformationData) travellerData.getTravellerInfo();
				type = travellerInfo.getVehicleType().getCode().toLowerCase();
			}
		}
		getBcfTravelCommercePriceService().setPriceAndTaxSearchCriteriaInContext(priceLevel, transportOfferingCodes, type);
	}

	@Override
	public PriceInformation getPriceInfoForAncillary(final int originDestinationRefNumber, final String productCode,
			final String transportOfferingCode, final String routeCode, final String offerGroupCode)
	{
		return getBcfTravelCommercePriceService().getPriceInfoForAncillary(originDestinationRefNumber, productCode,
				transportOfferingCode, routeCode, offerGroupCode);
	}

	@Override
	public RateData calculateRateData(final RoomRateProductModel roomRateProduct, final long quantity,
			final RatePlanModel ratePlan, final LocationModel location)
	{
		final BcfPriceInfo bcfPriceInfo = getBcfTravelCommercePriceService()
				.getBcfPriceInfo(roomRateProduct, quantity, ratePlan, location);
		return createRateData(bcfPriceInfo);
	}

	@Override
	public RateData calculateRateData(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct, final Long quantity,
			final String passengerType, final RatePlanModel ratePlan, final LocationModel location, final Date stayDate)
	{
		final BcfPriceInfo bcfPriceInfo = getBcfTravelCommercePriceService()
				.getBcfPriceInfo(extraGuestOccupancyProduct, quantity, passengerType, ratePlan, location, stayDate);
		return createRateData(bcfPriceInfo);
	}

	@Override
	public RateData calculateRateData(final ActivityProductModel activityProduct, final Long quantity,
			final PassengerTypeModel passengerType, final Date commencementDate, final LocationModel location)
	{
		final BcfPriceInfo bcfPriceInfo = getBcfTravelCommercePriceService()
				.getBcfPriceInfo(activityProduct, quantity, passengerType, commencementDate);
		if (Objects.isNull(bcfPriceInfo))
		{
			return null;
		}
		return createRateData(bcfPriceInfo);
	}

	/**
	 * Creates the rate data.lo
	 *
	 * @param bcfPriceInfo the BcfPriceInfo
	 * @return the rate data
	 */
	protected RateData createRateData(final BcfPriceInfo bcfPriceInfo)
	{
		final RateData rateData = new RateData();
		rateData.setBasePrice(createPriceData(bcfPriceInfo.getBasePrice(), bcfPriceInfo.getCurrencyIso()));
		if (CollectionUtils.isNotEmpty(bcfPriceInfo.getDiscountValues()))
		{
			double discountValue = bcfPriceInfo.getDiscountValues().stream().filter(Objects::nonNull).mapToDouble(DiscountValue::getValue).sum();
			rateData.setTotalDiscount(createPriceData(discountValue, bcfPriceInfo.getCurrencyIso()));
		}
		List<TaxData> taxes = Collections.emptyList();
		if (Objects.nonNull(bcfPriceInfo.getGstTaxValue()))
		{
			taxes = getTaxDatas(Collections.singletonList(bcfPriceInfo.getGstTaxValue()));
		}
		rateData.setTaxes(taxes);

		final TaxData totalTaxData = new TaxData();
		final double totalTaxValueForNetActualPrice = taxes.stream().mapToDouble(tax -> tax.getPrice().getValue().doubleValue())
				.sum();
		totalTaxData.setPrice(createPriceData(totalTaxValueForNetActualPrice));
		rateData.setTotalTax(totalTaxData);
		rateData.setActualRate(createPriceData(bcfPriceInfo.getNetPriceVal()*bcfPriceInfo.getQuantity(), bcfPriceInfo.getCurrencyIso()));
		rateData.setWasRate(rateData.getActualRate());

		return rateData;
	}

	/**
	 * Gets the tax datas.
	 *
	 * @param taxValues the tax values
	 * @return the taxes
	 */
	protected List<TaxData> getTaxDatas(final List<TaxValue> taxValues)
	{
		if (CollectionUtils.isEmpty(taxValues))
		{
			return Collections.emptyList();
		}

		return taxValues.stream().filter(Objects::nonNull).map(taxValue -> {
			final TaxData taxData = new TaxData();
			taxData.setCode(taxValue.getCode());
			taxData.setPrice(createPriceData(taxValue.getValue(), taxValue.getCurrencyIsoCode()));
			return taxData;
		}).collect(Collectors.toList());
	}

	/**
	 * @return the bcfTravelCommercePriceService
	 */
	protected BCFTravelCommercePriceService getBcfTravelCommercePriceService()
	{
		return bcfTravelCommercePriceService;
	}

	/**
	 * @param bcfTravelCommercePriceService the bcfTravelCommercePriceService to set
	 */
	@Required
	public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
	{
		this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
	}
}
