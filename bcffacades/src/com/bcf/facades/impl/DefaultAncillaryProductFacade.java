/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.catalog.synchronization.CatalogSynchronizationService;
import de.hybris.platform.catalog.synchronization.SyncConfig;
import de.hybris.platform.catalog.synchronization.SyncResult;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.CabinModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.extra.product.BCFExtraProductPerJourney;
import com.bcf.facades.extra.product.BCFExtraProducts;
import com.bcf.facades.extra.product.CabinProduct;
import com.bcf.facades.extra.product.ExtraProductData;
import com.bcf.facades.extra.product.ExtraProductPerVessel;
import com.bcf.facades.extra.product.LoungeProduct;
import com.bcf.facades.extra.product.MealProduct;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.vessel.CabinData;
import com.bcf.facades.webservices.populators.AncillaryProductReversePopulator;
import com.bcf.integration.catalogsync.service.CatalogVersionSyncJobService;
import com.bcf.integration.listSailingResponse.data.ProductAvailability;
import com.bcf.webservices.travel.data.ProductsData;


public class DefaultAncillaryProductFacade implements AncillaryProductFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAncillaryProductFacade.class);

	private BCFTravelCartService bcfTravelCartService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private SessionService sessionService;
	private BcfProductService bcfProductService;
	private ModelService modelService;
	private AncillaryProductReversePopulator ancillaryProductReversePopulator;
	private CatalogVersionService catalogVersionService;
	private CatalogSynchronizationService catalogSynchronizationService;
	private CatalogVersionSyncJobService catalogVersionSyncJobService;
	private SyncConfig syncConfig;
	private SearchRestrictionService searchRestrictionService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private Converter<CabinModel, CabinData> cabinDataConverter;

	private static final String CATALOG_SYNC_JOB_CODE = "sync " + BcfCoreConstants.PRODUCTCATALOG + ":Staged->Online";

	@Override
	public BCFExtraProducts getExtraProductsPerJourneyPerLegPerVessel()
	{
		final CartModel cart = getBcfTravelCartService().getSessionCart();
		final List<AbstractOrderEntryModel> passengerEntries = getActivePassengerCartEntries(cart);
		final List<AbstractOrderEntryModel> existingAncillaryEntries = getExistingAncillaryEntries(cart);
		final BCFExtraProducts bcfExtraProducts = getBcfExtraProducts(passengerEntries, existingAncillaryEntries);
		return bcfExtraProducts;
	}

	@Override
	public BCFExtraProducts getExtraProductsPerJourneyPerLegPerVessel(final int journeyRefNum, final int odRefNum)
	{
		final List<AbstractOrderEntryModel> passengerEntries = getBcfTravelCartService()
				.getCartEntriesForRefNo(journeyRefNum, odRefNum);
		final List<AbstractOrderEntryModel> existingAncillaryEntries = getExistingAncillaryEntries(
				getBcfTravelCartService().getSessionCart());
		final BCFExtraProducts bcfExtraProducts = getBcfExtraProducts(passengerEntries, existingAncillaryEntries);
		return bcfExtraProducts;
	}

	@Override
	public void createOrModifyProduct(final ProductsData productsData)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService
				.getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG, BcfCoreConstants.STAGED_CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		final List<ItemModel> modifiedProducts = new ArrayList<>();

		searchRestrictionService.disableSearchRestrictions();
		productsData.getProducts().stream().forEach(productData -> {
			final String productCode = productData.getCode();
			ProductModel productModel;
			try
			{
				productModel = bcfProductService.getProductForCode(catalogVersion, productCode);
				if (Objects.isNull(productModel))
				{
					productModel = creteProduct(productData.getType());
				}
			}
			catch (final UnknownIdentifierException unEx)
			{
				productModel = creteProduct(productData.getType());
			}
			getAncillaryProductReversePopulator().populate(productData, productModel);
			getModelService().save(productModel);
			modifiedProducts.add(productModel);
		});
		searchRestrictionService.enableSearchRestrictions();

		this.synchProducts(modifiedProducts);
	}

	ProductModel creteProduct(final String type)
	{
		if (StringUtils.equalsIgnoreCase(type, FeeProductModel._TYPECODE))
		{
			return getModelService().create(FeeProductModel.class);
		}
		else
		{
			return getModelService().create(AncillaryProductModel.class);
		}
	}

	private boolean synchProducts(final List<ItemModel> productModels)
	{
		LOG.info(String.format("Begin synchronizing Product Catalog [%s]", BcfCoreConstants.PRODUCTCATALOG));
		final CatalogVersionSyncJobModel catalogVersionSyncJob = getCatalogVersionSyncJobService()
				.getCatalogVersionSyncJob(CATALOG_SYNC_JOB_CODE);

		final SyncResult syncResult = getCatalogSynchronizationService()
				.performSynchronization(productModels, catalogVersionSyncJob, syncConfig);

		if (Objects.nonNull(syncResult) && (syncResult.isFinished() && syncResult.isSuccessful()))
		{
			LOG.info(String.format("Product Catalog [%s] sync finished successfully.", BcfCoreConstants.PRODUCTCATALOG));
			return true;
		}
		else
		{
			LOG.error(String.format("Product Catalog [%s] sync has issues.", BcfCoreConstants.PRODUCTCATALOG));
		}
		LOG.info(String.format("End synchronizing Product Catalog [%s]", BcfCoreConstants.PRODUCTCATALOG));
		return false;
	}

	@Override
	public void deleteProduct(final String productCode)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService
				.getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG, BcfCoreConstants.STAGED_CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final ProductModel productModel = bcfProductService.getProductForCode(catalogVersion, productCode);
		productModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		getModelService().save(productModel);
		this.synchProducts(Arrays.asList(productModel));
	}

	@Override
	public List<ProductData> getAncillaryProductsForCodes(final List<String> productCodes)
	{
		final List<ProductData> productDataList = new ArrayList<>();
		StreamUtil.safeStream(getBcfProductService().getAncillaryProductsForCodes(productCodes)).forEach(productModel -> {
			final ProductData productData = new ProductData();
			productData.setCode(productModel.getCode());
			productData.setName(productModel.getName());
			productDataList.add(productData);
		});
		return productDataList;
	}

	private BCFExtraProducts getBcfExtraProducts(final List<AbstractOrderEntryModel> passengerEntries,
			final List<AbstractOrderEntryModel> existingAncillaryEntries)
	{
		final Map<Integer, List<AbstractOrderEntryModel>> entriesPerJourney = passengerEntries.stream()
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
		final BCFExtraProducts bcfExtraProducts = new BCFExtraProducts();
		final Map<String, List<ProductAvailability>> sessionAvailabilityMap = getSessionService()
				.getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP);
		final Map<String, List<ProductAvailability>> sailingCodeProductAvailabilityMap = new HashMap<>();
		if (MapUtils.isNotEmpty(sessionAvailabilityMap))
		{
			sessionAvailabilityMap.entrySet().forEach(stringListEntry -> {
				if (CollectionUtils.isNotEmpty(stringListEntry.getValue()))
				{
					sailingCodeProductAvailabilityMap.put(stringListEntry.getKey(), stringListEntry.getValue());
				}
			});
		}
		final List<String> availableProductsIds = new ArrayList<>();
		StreamUtil.safeStream(StreamUtil.safeStream(sailingCodeProductAvailabilityMap.values())
				.flatMap(productAvailabilities -> productAvailabilities.stream()).collect(Collectors.toList()))
				.forEach(productAvailability -> availableProductsIds.add(productAvailability.getProductId()));
		final List<AncillaryProductModel> availableProducts = getBcfProductService()
				.getAncillaryProductsForCodes(availableProductsIds);
		final List<BCFExtraProductPerJourney> bcfExtraProductPerJourneyList = new ArrayList<>();
		for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> entry : entriesPerJourney.entrySet())
		{
			final Map<Integer, List<AbstractOrderEntryModel>> originDestinationEntryMap = entry.getValue().stream()
					.collect(Collectors.groupingBy(o -> o.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));

			for (final Map.Entry<Integer, List<AbstractOrderEntryModel>> originDestinationEntryMapEntrySet : originDestinationEntryMap
					.entrySet())
			{
				final BCFExtraProductPerJourney bcfExtraProductPerJourney = new BCFExtraProductPerJourney();
				createExtraProductPerJourney(bcfExtraProductPerJourney, originDestinationEntryMapEntrySet,
						existingAncillaryEntries, sailingCodeProductAvailabilityMap, availableProducts);

				bcfExtraProductPerJourney.setJourneyReferenceNumber(entry.getKey());
				bcfExtraProductPerJourney
						.setRouteCode(originDestinationEntryMapEntrySet.getValue().get(0).getTravelOrderEntryInfo().getTravelRoute()
								.getCode());
				bcfExtraProductPerJourney
						.setRouteName(originDestinationEntryMapEntrySet.getValue().get(0).getTravelOrderEntryInfo().getTravelRoute()
								.getName());
				bcfExtraProductPerJourney.setTransportOfferingCode(
						originDestinationEntryMapEntrySet.getValue().get(0).getTravelOrderEntryInfo().getTransportOfferings()
								.stream().findFirst().get()
								.getCode());
				bcfExtraProductPerJourneyList.add(bcfExtraProductPerJourney);
				bcfExtraProductPerJourney.setPaxCount(originDestinationEntryMapEntrySet.getValue().size());
			}
		}
		bcfExtraProducts.setBcfExtraProductPerJourneyList(bcfExtraProductPerJourneyList);
		return bcfExtraProducts;
	}

	protected void createExtraProductPerJourney(final BCFExtraProductPerJourney bcfExtraProductPerJourney,
			final Map.Entry<Integer, List<AbstractOrderEntryModel>> mapEntryByOdRefNumber,
			final List<AbstractOrderEntryModel> existingAncillaryEntries,
			final Map<String, List<ProductAvailability>> sailingCodeProductAvailabilityMap,
			final List<AncillaryProductModel> availableProducts)
	{
		final List<ExtraProductPerVessel> extraProductPerVesselList = new ArrayList<>();

		final TransportOfferingModel transportOfferingWithEmptyEbookingSailingCode = mapEntryByOdRefNumber.getValue().get(0)
				.getTravelOrderEntryInfo().getTransportOfferings().stream()
				.filter(transportOffering -> transportOffering.getEbookingSailingCode() == null).findAny().orElse(null);
		if (transportOfferingWithEmptyEbookingSailingCode == null)
		{
			final Map<String, TransportVehicleModel> transportVehiclesByEBookingSailingCode = mapEntryByOdRefNumber.getValue().get(0)
					.getTravelOrderEntryInfo().getTransportOfferings().stream().collect(Collectors
							.groupingBy(TransportOfferingModel::getEbookingSailingCode, Collectors.collectingAndThen(Collectors.toList(),
									transportOffering -> transportOffering.stream().findFirst().get().getTransportVehicle())));
			for (final Map.Entry<String, TransportVehicleModel> transportVehiclesByEBookingSailingCodeEntry : transportVehiclesByEBookingSailingCode
					.entrySet())
			{
				final TransportVehicleModel transportVehicle = transportVehiclesByEBookingSailingCodeEntry.getValue();
				final String eBookingSailingCode = transportVehiclesByEBookingSailingCodeEntry.getKey();
				final ExtraProductPerVessel extraProductPerVessel = new ExtraProductPerVessel();
				extraProductPerVessel.setVehicleCode(transportVehicle.getEBookingCode());
				extraProductPerVessel.setVehicleName(transportVehicle.getTransportVehicleInfo().getName());
				extraProductPerVessel.setSeatMapMedia(getBcfResponsiveMediaStrategy()
						.getResponsiveJson(((ShipInfoModel) transportVehicle.getTransportVehicleInfo()).getSeatMapImage()));
				extraProductPerVessel
						.setSeatMapName(((ShipInfoModel) transportVehicle.getTransportVehicleInfo()).getSeatMapName());
				extraProductPerVessel
						.setSeatMapInfo(((ShipInfoModel) transportVehicle.getTransportVehicleInfo()).getSeatMapDescription());
				extraProductPerVessel.setCabins(Converters
						.convertAll(((ShipInfoModel) transportVehicle.getTransportVehicleInfo()).getCabins(), getCabinDataConverter()));
				if (Objects.nonNull(sailingCodeProductAvailabilityMap) && Objects
						.nonNull(sailingCodeProductAvailabilityMap.get(eBookingSailingCode)))
				{
					populateExtraProductData(availableProducts, extraProductPerVessel,
							sailingCodeProductAvailabilityMap.get(eBookingSailingCode), existingAncillaryEntries,
							mapEntryByOdRefNumber.getValue().get(0).getJourneyReferenceNumber(),
							mapEntryByOdRefNumber.getValue().get(0).getTravelOrderEntryInfo().getOriginDestinationRefNumber());
				}
				extraProductPerVesselList.add(extraProductPerVessel);
			}
		}
		if (mapEntryByOdRefNumber.getValue().get(0).getTravelOrderEntryInfo().getOriginDestinationRefNumber()
				== BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER)
		{
			bcfExtraProductPerJourney.setOutboundExtraProductDataList(extraProductPerVesselList);
		}
		else
		{
			bcfExtraProductPerJourney.setInboundExtraProductDataList(extraProductPerVesselList);
		}
	}

	private List<AbstractOrderEntryModel> getActivePassengerCartEntries(final AbstractOrderModel cart)
	{

		final List<AbstractOrderEntryModel> activeEntries;

		activeEntries = cart.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE))
				.collect(Collectors.toList());

		final List<AbstractOrderEntryModel> passengerEntries = new ArrayList<>();
		activeEntries.stream().forEach(entry -> {
			entry.getTravelOrderEntryInfo().getTravellers().stream().forEach(travellerModel -> {
				if (travellerModel.getType().equals(TravellerType.PASSENGER) && StringUtils.equalsIgnoreCase(RouteType.LONG.getCode(),
						entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType().getCode()))
				{
					passengerEntries.add(entry);
				}
			});
		});
		return passengerEntries;
	}

	private List<AbstractOrderEntryModel> getExistingAncillaryEntries(final AbstractOrderModel cart)
	{
		return StreamUtil.safeStream(cart.getEntries()).filter(
				entry -> entry.getActive() && entry.getAmendStatus().equals(AmendStatus.NEW) && entry.getProduct().getItemtype()
						.equals(AncillaryProductModel._TYPECODE)).collect(Collectors.toList());
	}



	private void populateExtraProductData(final List<AncillaryProductModel> products,
			final ExtraProductPerVessel extraProductPerVessel,
			final List<ProductAvailability> productAvailabilities, final List<AbstractOrderEntryModel> existingAncillaryEntries,
			final int journeyRef, final int odRef)
	{
		if (CollectionUtils.isNotEmpty(productAvailabilities))
		{
			final Map<String, List<ProductAvailability>> productIdAvailabilityMap = productAvailabilities.stream()
					.collect(Collectors.groupingBy(product -> product.getProductId()));
			final List<CabinProduct> cabinProductList = new ArrayList<>();
			final List<MealProduct> mealProductList = new ArrayList<>();
			final List<LoungeProduct> loungeProductList = new ArrayList<>();
			products.forEach(productModel -> {
				if (AncillaryProductModel._TYPECODE.equals(productModel.getItemtype()))
				{
					if (Objects.nonNull(productModel.getFerryOptionDisplayType()) && productIdAvailabilityMap
							.containsKey(productModel.getCode())
							&& productIdAvailabilityMap.get(productModel.getCode()).stream().findFirst().get().getAvailableCapacity()
							!= 0)
					{
						switch (productModel.getFerryOptionDisplayType())
						{
							case CABIN:
								final CabinProduct cabinProduct = new CabinProduct();
								setExtraProductProperties(cabinProduct, productModel, existingAncillaryEntries, journeyRef, odRef);
								cabinProductList.add(cabinProduct);
								break;
							case MEAL:
								final MealProduct mealProduct = new MealProduct();
								setExtraProductProperties(mealProduct, productModel, existingAncillaryEntries, journeyRef, odRef);
								mealProductList.add(mealProduct);
								break;
							case LOUNGE:
								final LoungeProduct loungeProduct = new LoungeProduct();
								setExtraProductProperties(loungeProduct, productModel, existingAncillaryEntries, journeyRef, odRef);
								loungeProductList.add(loungeProduct);
								break;
							default:
								break;
						}
					}
				}
			});
			extraProductPerVessel.setCabinProductList(cabinProductList);
			extraProductPerVessel.setMealProductList(mealProductList);
			extraProductPerVessel.setLoungeProductList(loungeProductList);
		}
	}

	private void setExtraProductProperties(final ExtraProductData extra, final AncillaryProductModel ancillaryProduct,
			final List<AbstractOrderEntryModel> existingAncillaryEntries,
			final int journeyRef, final int odRef)
	{
		extra.setCode(ancillaryProduct.getCode());
		extra.setName(ancillaryProduct.getName());
		final Map<String, List<AbstractOrderEntryModel>> ancillariesPerJourneyAndODByProductID = StreamUtil
				.safeStream(existingAncillaryEntries)
				.filter(entry -> entry.getJourneyReferenceNumber() == journeyRef
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRef)
				.collect(Collectors.groupingBy(o -> o.getProduct().getCode()));
		StreamUtil.safeStream(ancillariesPerJourneyAndODByProductID.entrySet()).forEach(ancillaryEntry -> {
			if (StringUtils.equalsIgnoreCase(ancillaryEntry.getKey(), ancillaryProduct.getCode()))
			{
				final long count = StreamUtil.safeStream(ancillaryEntry.getValue()).mapToLong(AbstractOrderEntryModel::getQuantity)
						.sum();
				extra.setCount((int) count);
			}
		});

	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(
			final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BcfProductService getBcfProductService()
	{
		return bcfProductService;
	}

	@Required
	public void setBcfProductService(final BcfProductService bcfProductService)
	{
		this.bcfProductService = bcfProductService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected AncillaryProductReversePopulator getAncillaryProductReversePopulator()
	{
		return ancillaryProductReversePopulator;
	}

	@Required
	public void setAncillaryProductReversePopulator(
			final AncillaryProductReversePopulator ancillaryProductReversePopulator)
	{
		this.ancillaryProductReversePopulator = ancillaryProductReversePopulator;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected CatalogSynchronizationService getCatalogSynchronizationService()
	{
		return catalogSynchronizationService;
	}

	@Required
	public void setCatalogSynchronizationService(
			final CatalogSynchronizationService catalogSynchronizationService)
	{
		this.catalogSynchronizationService = catalogSynchronizationService;
	}

	protected CatalogVersionSyncJobService getCatalogVersionSyncJobService()
	{
		return catalogVersionSyncJobService;
	}

	@Required
	public void setCatalogVersionSyncJobService(
			final CatalogVersionSyncJobService catalogVersionSyncJobService)
	{
		this.catalogVersionSyncJobService = catalogVersionSyncJobService;
	}

	protected SyncConfig getSyncConfig()
	{
		return syncConfig;
	}

	@Required
	public void setSyncConfig(final SyncConfig syncConfig)
	{
		this.syncConfig = syncConfig;
	}

	protected SearchRestrictionService getSearchRestrictionService()
	{
		return searchRestrictionService;
	}

	@Required
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	protected Converter<CabinModel, CabinData> getCabinDataConverter()
	{
		return cabinDataConverter;
	}

	@Required
	public void setCabinDataConverter(
			final Converter<CabinModel, CabinData> cabinDataConverter)
	{
		this.cabinDataConverter = cabinDataConverter;
	}
}
