/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.accommodation.RoomRateCartData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.packages.cart.AddDealToCartData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.ruleengineservices.rule.services.RuleService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelfacades.facades.accommodation.forms.AccommodationAddToCartForm;
import de.hybris.platform.travelfacades.order.impl.DefaultAccommodationCartFacade;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.GuestCountService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.entrygroup.EntryGroupData;
import com.bcf.core.model.AdditionalProductAndQuantityModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfAccommodationCommerceCartService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.BcfAccommodationCartFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.helper.impl.DefaultBcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.stock.preprocessor.BcfStockPreprocessor;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class DefaultBcfAccommodationCartFacade extends DefaultAccommodationCartFacade implements BcfAccommodationCartFacade

{
	private SessionService sessionService;
	private BCFTravelCommerceCartService commerceCartService;
	private BcfBookingService bcfBookingService;
	private BCFTravelCartService bcfTravelCartService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private CalculationService calculationService;
	private AccommodationOfferingService accommodationOfferingService;
	private CategoryService categoryService;
	private GuestCountService guestCountService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private RuleService ruleService;
	private PriceRowService priceRowService;
	private BcfAccommodationService bcfAccommodationService;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;
	private BcfCalculationService bcfCalculationService;
	private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
	private BcfStockPreprocessor bcfStockPreprocessor;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfAccommodationCommerceCartService accommodationCommerceCartService;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	private static final Logger LOG = Logger.getLogger(DefaultBcfAccommodationCartFacade.class);



	@Override
	public List<CartModificationData> addAccommodationBundleToCart(
			final AccommodationBundleTemplateModel accommodationBundleTemplateModel, final AddDealToCartData addDealToCartData)
			throws CommerceCartModificationException
	{
		final EntryGroupData entryGroupData = addDealToCartData.getEntryGroupData();

		final List<CartModificationData> cartModificationDatas = new ArrayList<>();
		final List<RoomRateCartData> roomRates = collectRoomRates(accommodationBundleTemplateModel, addDealToCartData);
		if (CollectionUtils.isEmpty(roomRates))
		{
			throw new CommerceCartModificationException("No room rate can be added to the cart. Aborting operation");
		}

		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = new ArrayList<>();
		final List<AbstractOrderEntryModel> dealAccommodationEntries = new ArrayList<>();
		for (final RoomRateCartData roomRate : roomRates)
		{
			final CartModificationData cartModification = CollectionUtils.isEmpty(cartModificationDatas)
					? getBundleCartFacade().startBundle(accommodationBundleTemplateModel.getId(), roomRate.getCode(),
					roomRate.getCardinality())
					: getBundleCartFacade().addToCart(roomRate.getCode(), roomRate.getCardinality(),
					cartModificationDatas.stream().findAny().get().getEntry().getEntryGroupNumbers().stream().findFirst().get());
			getAccommodationCommerceCartService()
					.populateAccommodationDetailsOnRoomRateEntry(cartModification.getEntry().getEntryNumber(), roomRate.getDates());
			entryGroupData.getEntryNumbers().add(cartModification.getEntry().getEntryNumber());
			cartModificationDatas.add(cartModification);
		}

		final List<Integer> entryNumbers = cartModificationDatas.stream().map(cartModificationData -> cartModificationData.getEntry().getEntryNumber()).collect(
				Collectors.toList());
		dealAccommodationEntries.addAll(entryNumbers.stream().map(number -> {
			return getCartService().getEntryForNumber(getCartService().getSessionCart(), number);
		}).collect(Collectors.toList()));

		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getAccommodationCommerceCartService()
				.getNewAccommodationOrderEntryGroup(dealAccommodationEntries,  addDealToCartData.getStartingDate(), addDealToCartData.getEndingDate(), accommodationBundleTemplateModel.getAccommodationOffering().getCode(), accommodationBundleTemplateModel.getAccommodation().getCode(), accommodationBundleTemplateModel.getRatePlan().getCode(), 0);
		accommodationOrderEntryGroups.add(accommodationOrderEntryGroup);
		entryGroupData.setAccommodationOrderEntryGroups(accommodationOrderEntryGroups);
		return cartModificationDatas;

	}

	@Override
	protected List<RoomRateCartData> collectRoomRates(
			final AccommodationBundleTemplateModel accommodationBundleTemplate, final AddDealToCartData addDealToCartData)
	{
		Instant instant = Instant.ofEpochMilli(addDealToCartData.getStartingDate().getTime());
		final LocalDateTime startDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		instant = Instant.ofEpochMilli(addDealToCartData.getEndingDate().getTime());
		final LocalDateTime endDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		final List<RoomRateCartData> roomRates = new ArrayList();
		Stream.iterate(startDate, date -> date.plusDays(1L)).limit(ChronoUnit.DAYS.between(startDate, endDate)).forEach(date -> {

			final RoomRateProductModel roomRateProductModel = accommodationBundleTemplate.getRoomRateProduct();
			if (validateRoomRateAgainstDate(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()),
					roomRateProductModel))
			{
				final Optional<RoomRateCartData> roomRateCartDataOptional = roomRates.stream()
						.filter(roomRateCartData -> roomRateCartData.getCode().equals(roomRateProductModel.getCode())).findFirst();
				if (roomRateCartDataOptional.isPresent())
				{
					final RoomRateCartData roomRateCartData = roomRateCartDataOptional.get();
					roomRateCartData.setCardinality(roomRateCartData.getCardinality() + 1);
					roomRateCartData.getDates().add(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));
				}
				else
				{
					roomRates.add(buildNewRoomRateCartData(roomRateProductModel, date));
				}
			}

		});
		return roomRates;
	}

	@Override
	public List<RoomRateCartData> collectRoomRates(final AccommodationAddToCartForm form)
	{
		final List<RoomRateCartData> rates = new ArrayList();
		final List<String> roomRateCodes = form.getRoomRateCodes();
		final List<String> roomRateDates = form.getRoomRateDates();
		if (org.apache.commons.collections4.CollectionUtils.size(roomRateCodes) != org.apache.commons.collections4.CollectionUtils
				.size(roomRateDates))
		{
			return Collections.emptyList();
		}
		else
		{
			for (int i = 0; i < org.apache.commons.collections4.CollectionUtils.size(roomRateCodes); ++i)
			{
				final String roomRateCode = roomRateCodes.get(i);
				final String roomRateDate = roomRateDates.get(i);
				final Optional<RoomRateCartData> optionalRate = rates.stream().filter((ratex) -> {
					return org.apache.commons.lang.StringUtils.equalsIgnoreCase(ratex.getCode(), roomRateCode);
				}).findFirst();
				final RoomRateCartData rate;
				if (optionalRate.isPresent())
				{
					rate = optionalRate.get();
					rate.setCardinality(rate.getCardinality() + 1);
					rate.getDates()
							.add(TravelDateUtils.convertStringDateToDate(roomRateDate, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
				}
				else
				{
					rate = new RoomRateCartData();
					rate.setCode(roomRateCode);
					rate.setCardinality(1);
					final List<Date> dates = new ArrayList();
					dates.add(TravelDateUtils.convertStringDateToDate(roomRateDate, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
					rate.setDates(dates);
					rates.add(rate);
				}
			}

			return rates;
		}
	}

	@Override
	public void addAccommodationsToCart(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode,
			final String accommodationCode, final List<RoomRateCartData> rates, final int numberOfRooms, final String ratePlanCode,
			final List<RoomStayCandidateData> roomStayCandidates, final Integer modifyRoomRef, final boolean change)
			throws CommerceCartModificationException,CalculationException
	{
		final Transaction currentTransaction = Transaction.current();
		boolean executed = false;

		try
		{
			currentTransaction.begin();

			addAccommodationsToCartInternal(checkInDate, checkOutDate, accommodationOfferingCode,
					accommodationCode, rates, numberOfRooms, ratePlanCode, roomStayCandidates, modifyRoomRef,  change);


			executed = true;
		}
		catch (final CartValidationException | InsufficientStockLevelException | OrderProcessingException ex)
		{
			LOG.error("Error while addAccommodationsToCart:", ex);
			throw new CommerceCartModificationException("Stock Reserve Error:", ex);
		}
		catch (final CommerceCartModificationException ex)
		{
			throw ex;
		}
		finally
		{
			if (executed)
			{
				currentTransaction.commit();
			}
			else
			{
				currentTransaction.rollback();
			}
		}
	}



	private void addAccommodationsToCartInternal(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode,
			final String accommodationCode, final List<RoomRateCartData> rates, final int numberOfRooms, final String ratePlanCode,
			final List<RoomStayCandidateData> roomStayCandidates,
			final Integer modifyRoomRef, final boolean change)
			throws CommerceCartModificationException, CalculationException, OrderProcessingException, CartValidationException,
			InsufficientStockLevelException
	{
		Integer journeyRefNumber = getSessionService().getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		journeyRefNumber = Objects.isNull(journeyRefNumber) ? 0 : journeyRefNumber;

		CartModel cart = getCartService().getSessionCart();
		if (change)
		{
			removeAccommodationOrderEntryGroup(modifyRoomRef);
		}
		else if(bcfTravelCartService.isAmendmentCart(cart) && !bcfTravelCartFacadeHelper.isAlacateFlow()){
			disableAccommodationEntriesForRefNo(cart,journeyRefNumber);
		}

		final List<AccommodationOrderEntryGroupModel> dealOrderEntryGroups = getAccommodationCommerceCartService()
				.getNewDealOrderEntryGroups(accommodationOfferingCode, accommodationCode,ratePlanCode, journeyRefNumber);
		final int roomQuantityToAdd = numberOfRooms - CollectionUtils.size(dealOrderEntryGroups);
		if(roomQuantityToAdd > 0)
		{
			 addAccommodationToCartForDynamicDeal(checkInDate, checkOutDate, accommodationOfferingCode, accommodationCode, rates, roomQuantityToAdd, ratePlanCode, journeyRefNumber, roomStayCandidates);
		}
		else if(roomQuantityToAdd < 0)
		{
			// do nothing
		}

		cart = getBcfTravelCartService().getSessionCart();

		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();

		bcfStockPreprocessor.validateAndReserve(decisionData, cart);

		bcfTravelCartFacadeHelper.updateAvailabilityStatus();

		bcfChangeFeeCalculationService.calculateChangeFees(cart);
		bcfCalculationService.recalculate(cart);

	}


	@Override
	public boolean isModifyingSameAccommodation(final String accommodationOfferingCode,
			final String accommodationCode, final boolean onlyAccommodationChange, final int journeyRef)
	{
		if (bcfTravelCartFacade.isAmendmentCart() && onlyAccommodationChange)
		{

			final List<AccommodationOrderEntryGroupModel> entryGroups = bcfBookingService
					.getDealOrderEntryGroups(this.getTravelCartService().getSessionCart()).stream()
					.filter(entryGroup -> journeyRef == entryGroup.getJourneyRefNumber())
					.flatMap(entry -> entry.getAccommodationEntryGroups().stream()).filter(
							entry -> entry.getAccommodationOffering().getCode().equals(accommodationOfferingCode) && entry
									.getAccommodation().getCode().equals(accommodationCode)).
							collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(entryGroups))
			{
				return true;

			}
		}
		return false;

	}


	@Override
	public void addAccommodationsToCartForPromotions(final Date checkInDate, final Date checkOutDate,
			final String accommodationOfferingCode,
			final String accommodationCode, final List<RoomRateCartData> rates, final int numberOfRooms, final String ratePlanCode,
			final String promotionCode, final List<RoomStayCandidateData> roomStayCandidates,final boolean change)
			throws CommerceCartModificationException,CalculationException
	{
		try
		{

			addAccommodationsToCartForPromotionsInternal(checkInDate, checkOutDate, accommodationOfferingCode,
					accommodationCode, rates, numberOfRooms, ratePlanCode,
					promotionCode, roomStayCandidates,change);

		}
		catch (final CartValidationException | InsufficientStockLevelException | OrderProcessingException ex)
		{
			LOG.error("Error while addAccommodationsToCartForPromotions:", ex);
			throw new CommerceCartModificationException("Stock Reserve Error:", ex);
		}
		catch (final CommerceCartModificationException ex)
		{
			throw ex;
		}
	}

	private void addAccommodationsToCartForPromotionsInternal(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode,
			final String accommodationCode, final List<RoomRateCartData> rates, final int numberOfRooms, final String ratePlanCode,
			final String promotionCode, final List<RoomStayCandidateData> roomStayCandidates,final boolean change)
			throws CommerceCartModificationException, CalculationException, OrderProcessingException, CartValidationException,
			InsufficientStockLevelException
	{
		Integer journeyRefNumber = getSessionService().getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		journeyRefNumber = Objects.isNull(journeyRefNumber) ? 0 : journeyRefNumber;

		CartModel cart = getCartService().getSessionCart();

		if(bcfTravelCartService.isAmendmentCart(cart) && !bcfTravelCartFacadeHelper.isAlacateFlow()){
			disableAccommodationEntriesForRefNo(cart,journeyRefNumber);
		}
		int roomQuantityToAdd=numberOfRooms;
		if(change)
		{
			final	DealOrderEntryGroupModel dealOrderEntryGroupModel = bcfBookingService
					.getAccommodationDealOrderEntryGroup(cart, journeyRefNumber);
			if (dealOrderEntryGroupModel != null)
			{
				getAccommodationCommerceCartService()
						.removeDealOrderEntryGroups(Arrays.asList(dealOrderEntryGroupModel));
				getModelService().refresh(cart);

			}

		}else{
			final List<AccommodationOrderEntryGroupModel> dealOrderEntryGroups = getAccommodationCommerceCartService()
					.getNewDealOrderEntryGroups(accommodationOfferingCode, accommodationCode,ratePlanCode, journeyRefNumber);
			roomQuantityToAdd = roomQuantityToAdd- CollectionUtils.size(dealOrderEntryGroups);
		}

		if(roomQuantityToAdd > 0)
		{
			addAccommodationToCartForPromotions(checkInDate, checkOutDate, accommodationOfferingCode, accommodationCode, rates, roomQuantityToAdd, ratePlanCode, journeyRefNumber, promotionCode,  roomStayCandidates);
			getModelService().refresh(cart);
		}

		cart = getBcfTravelCartService().getSessionCart();
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();

		bcfStockPreprocessor.validateAndReserve(decisionData, cart);

		bcfTravelCartFacadeHelper.updateAvailabilityStatus();

		bcfTravelCartFacade.recalculateCart();
		bcfChangeFeeCalculationService.calculateChangeFees(cart);
	}



	protected CartModel addAccommodationToCartForPromotions(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode, final String accommodationCode, final List<RoomRateCartData> rates,
			final int numberOfRooms, final String ratePlanCode, final Integer journeyRefNumber, final String promotionCode,
			final List<RoomStayCandidateData> roomStayCandidates) throws CommerceCartModificationException
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = new ArrayList<>();
		final List<Integer> entryNumbers = createAndDisableAccommodationEntries(checkInDate, checkOutDate,
				accommodationOfferingCode,
				accommodationCode, rates, numberOfRooms,
				ratePlanCode, journeyRefNumber, accommodationOrderEntryGroups, roomStayCandidates);



		entryNumbers.addAll(addApplicablePromotionExtraProducts(accommodationOfferingCode, accommodationOrderEntryGroups,journeyRefNumber, promotionCode));


		final AddDealToCartData addDealToCartData = createAddDealToCartDataForPromotions(entryNumbers, accommodationOrderEntryGroups, journeyRefNumber, promotionCode, roomStayCandidates);
		final CartModel cart = getCartService().getSessionCart();

		getCommerceCartService().createOrUpdateOrderGroup(addDealToCartData.getEntryGroupData(), cart);

		return cart;

	}

	private List<Integer> addApplicablePromotionExtraProducts(final String accommodationOfferingCode,
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups, final Integer journeyRefNumber,
			final String promotionCode) throws CommerceCartModificationException
	{
		final List<Integer> entryNumbers = new ArrayList<>();

		final List<Pair<ProductModel, Integer>> additionalProducts = getValidAdditionalProductsList(accommodationOfferingCode,
				promotionCode);

		if (CollectionUtils.isNotEmpty(additionalProducts))
		{
			final CartModel cart = getCartService().getSessionCart();
			for (final  Pair<ProductModel,Integer> additionalProduct : additionalProducts)
			{
				entryNumbers.add(addExtraProductToCart(journeyRefNumber, cart,additionalProduct.getLeft(),additionalProduct.getRight()));
			}
		}
		if(CollectionUtils.isNotEmpty(accommodationOrderEntryGroups)){

			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = accommodationOrderEntryGroups.get(0);
			final List<AbstractOrderEntryModel> extraProductAccommodationEntries=
					entryNumbers.stream().map(number -> getCartService().getEntryForNumber(getCartService().getSessionCart(), number)
					).filter(entry->entry.getType().getCode().equals(OrderEntryType.ACCOMMODATION.getCode())).collect(Collectors.toList());

			final List<AbstractOrderEntryModel> dealAccommodationEntries = new ArrayList<>(
					accommodationOrderEntryGroupModel.getDealAccommodationEntries());
			if(CollectionUtils.isNotEmpty(extraProductAccommodationEntries)){
				dealAccommodationEntries.addAll(extraProductAccommodationEntries);
			}
			accommodationOrderEntryGroupModel.setDealAccommodationEntries(dealAccommodationEntries);
			getModelService().save(accommodationOrderEntryGroupModel);

		}

		return entryNumbers;
	}

	@Override
	public List<Pair<ProductModel, Integer>> getValidAdditionalProductsList(final String accommodationOfferingCode,
			final String promotionCode)
	{

		if (StringUtils.isNoneBlank(promotionCode))
		{
			final AbstractRuleModel ruleModel = ruleService.getRuleForCode(promotionCode);
			if (ruleModel != null && ruleModel instanceof PromotionSourceRuleModel
					&& CollectionUtils.isNotEmpty(((PromotionSourceRuleModel) ruleModel).getAdditionalProducts()))
			{
				final List<Pair<ProductModel, Integer>> additionalProducts = new ArrayList<>();

				final Set<AdditionalProductAndQuantityModel> additionalProductAndQuantityModels= ((PromotionSourceRuleModel) ruleModel).getAdditionalProducts();
				for (final AdditionalProductAndQuantityModel additionalProductAndQuantityModel : additionalProductAndQuantityModels)
				{
					additionalProducts.add(Pair.of(additionalProductAndQuantityModel.getProduct(),additionalProductAndQuantityModel.getQuantity()));
				}
				return additionalProducts;
			}
		}

		return Collections.EMPTY_LIST;
	}

	protected CartModel addAccommodationToCartForDynamicDeal(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode, final String accommodationCode, final List<RoomRateCartData> rates,
			final int numberOfRooms, final String ratePlanCode, final Integer journeyRefNumber,final List<RoomStayCandidateData> roomStayCandidates) throws CommerceCartModificationException
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = new ArrayList<>();

		final List<Integer> entryNumbers = createAndDisableAccommodationEntries(checkInDate, checkOutDate,
				accommodationOfferingCode,
				accommodationCode, rates, numberOfRooms,
				ratePlanCode, journeyRefNumber, accommodationOrderEntryGroups, roomStayCandidates);

		final AddDealToCartData addDealToCartData = createAddDealToCartData(entryNumbers, accommodationOrderEntryGroups, journeyRefNumber);
		final CartModel cart = getCartService().getSessionCart();

		if (BcfFacadesConstants.BOOKING_PACKAGE
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{

			getCommerceCartService().createOrUpdateOrderGroup(addDealToCartData.getEntryGroupData(), cart);
		}


		return cart;

	}

	@Override
	public List<Integer> getAccommodationOrderEntryGroupRefsForOtherJourneys(final int journeyRef)
	{

		return bcfBookingService.getDealOrderEntryGroups(this.getTravelCartService().getSessionCart()).stream().filter(entryGroup->journeyRef!=entryGroup.getJourneyRefNumber()).flatMap(entryGroup->entryGroup.getAccommodationEntryGroups().stream()).map((entryGroup) -> {
			return entryGroup.getRoomStayRefNumber();
		}).collect(Collectors.toList());
	}

	private Integer addExtraProductToCart(final int journeyReferenceNumber,
			final CartModel cartModel,
			final ProductModel extraProduct, final int qty) throws CommerceCartModificationException
	{
		final CartModificationData cartModificationData = bcfTravelCartFacade.addToCart(extraProduct.getCode(), qty);
		final Map<String, Object> propertiesMap = new HashMap<>();
		if(extraProduct.getItemtype().equals(ExtraProductModel._TYPECODE))
		{
			bcfTravelCartFacade.setOrderEntryType(OrderEntryType.ACCOMMODATION, cartModificationData.getEntry().getEntryNumber());
		}else{
			bcfTravelCartFacade.setOrderEntryType(OrderEntryType.TRANSPORT, cartModificationData.getEntry().getEntryNumber());
			final AbstractOrderEntryModel abstractOrderEntryModel = cartModel.getEntries().stream().filter(
					entry -> BooleanUtils.isTrue(entry.getActive()) && entry.getType() != null && OrderEntryType.TRANSPORT.getCode()
							.equals(entry.getType().getCode()) && entry.getJourneyReferenceNumber() == journeyReferenceNumber
							&& entry.getTravelOrderEntryInfo() != null).findAny().orElse(null);
			if(abstractOrderEntryModel!=null){
				final AbstractOrderEntryModel newEntry = this.getTravelCartService()
						.getEntryForNumber(cartModel, cartModificationData.getEntry().getEntryNumber());
				newEntry.setTravelOrderEntryInfo(abstractOrderEntryModel.getTravelOrderEntryInfo());
		 		getModelService().save(newEntry);
			}
		}
		propertiesMap.put(AbstractOrderEntryModel.ACTIVE, Boolean.TRUE);
		propertiesMap.put(AbstractOrderEntryModel.AMENDSTATUS, AmendStatus.NEW);
		propertiesMap.put(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER, journeyReferenceNumber);

		bcfTravelCommerceCartService.addPropertiesToCartEntry(cartModel, cartModificationData.getEntry().getEntryNumber(),
				extraProduct, propertiesMap);
		return cartModificationData.getEntry().getEntryNumber();

	}

	private List<Integer> createAndDisableAccommodationEntries
			(final Date checkInDate, final Date checkOutDate,
					final String accommodationOfferingCode, final String accommodationCode, final List<RoomRateCartData> rates,
					final int numberOfRooms, final String ratePlanCode, final Integer journeyRefNumber,
					final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups,
					final List<RoomStayCandidateData> roomStayCandidates) throws CommerceCartModificationException
	{
		final int currentMaxRefNumber = getAccommodationCommerceCartService()
				.getMaxRoomStayRefNumber(journeyRefNumber);
		final int newRefNumber = currentMaxRefNumber != -1 ? currentMaxRefNumber + 1 : 0;
		final List<Integer> entryNumbers = new ArrayList<>(rates.size());

		final AccommodationModel accommodationModel = bcfAccommodationService.getAccommodation(accommodationCode);
		final int baseOccupancy = accommodationModel.getBaseOccupancyCount();

		int extraProductAdultsNeeded=0;
		int extraProductChildsNeeded=0;
		for (int i = 0; i < numberOfRooms; ++i)
		{
			final List<Integer> roomEntryNumbers =new ArrayList<>();
			final Iterator<RoomRateCartData> itr = rates.iterator();

			Pair<Integer,Integer> extraGuestOccupancies=null;
			if(roomStayCandidates.size()>i)
			{
				final RoomStayCandidateData roomStayCandidateData = roomStayCandidates.get(i);
				final List<PassengerTypeQuantityData> passengerTypeQuantityList = roomStayCandidateData
						.getPassengerTypeQuantityList();

			 extraGuestOccupancies= DefaultBcfTravelCartFacadeHelper
						.getExtraGuestOccupancies(passengerTypeQuantityList,baseOccupancy);

			}
			if(extraGuestOccupancies!=null){
				 extraProductAdultsNeeded=extraGuestOccupancies.getLeft();
				 extraProductChildsNeeded=extraGuestOccupancies.getRight();
			}

			while (itr.hasNext())
			{
				final RoomRateCartData rate = itr.next();
				addRoomRateToCart(rate, roomEntryNumbers, accommodationCode, accommodationOfferingCode, ratePlanCode);
				if (extraProductAdultsNeeded > 0)
				{
					addExtraGuestOccupancyProduct(accommodationModel, extraProductAdultsNeeded,roomEntryNumbers,  BcfFacadesConstants.ADULT,rate);
				}

				if (extraProductChildsNeeded > 0)
				{
					addExtraGuestOccupancyProduct(accommodationModel, extraProductChildsNeeded,roomEntryNumbers,  BcfFacadesConstants.CHILD,rate);
				}
			}

			final List<AbstractOrderEntryModel> dealAccommodationEntries=
					roomEntryNumbers.stream().map(number -> getCartService().getEntryForNumber(getCartService().getSessionCart(), number)
					).collect(Collectors.toList());

			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getNewAccommodationOrderEntryGroup(dealAccommodationEntries, checkInDate, checkOutDate, accommodationOfferingCode,
					accommodationCode, ratePlanCode, newRefNumber + i, i, roomStayCandidates);
			accommodationOrderEntryGroups.add(accommodationOrderEntryGroup);
			entryNumbers.addAll(roomEntryNumbers);
		}

		return entryNumbers;
	}

	@Override
	protected void addRoomRateToCart(
			final RoomRateCartData rate, final List<Integer> entryNumbers, final String accCode, final String accOffCode,
			final String ratePlanCode) throws CommerceCartModificationException
	{
		final CartModificationData cartModification = this.addToCart(rate.getCode(), (long) rate.getCardinality());
		if (cartModification.getQuantityAdded() < (long)rate.getCardinality()) {
			this.rollbackAccommodationEntries(accCode, accOffCode, ratePlanCode);
			throw new CommerceCartModificationException("The request cannot be entirely fulfilled");
		} else {
			this.getAccommodationCommerceCartService().populateAccommodationDetailsOnRoomRateEntry(cartModification.getEntry().getEntryNumber(), rate.getDates());
			entryNumbers.add(cartModification.getEntry().getEntryNumber());

			final CartModel cart = getCartService().getSessionCart();
			if (OptionBookingUtil.isOptionBooking(cart))
			{
				getBcfOptionBookingNotificationService().startOptionBookingEmailProcess(cart, Collections.singletonList(cartModification));
			}
		}
	}

	private void addExtraGuestOccupancyProduct(final AccommodationModel accommodationModel, final long extraProductAdultsNeeded,
			final List<Integer> roomEntryNumbers, final String passengerType, final RoomRateCartData rate)
			throws CommerceCartModificationException
	{
		if (CollectionUtils.isNotEmpty(accommodationModel.getExtraGuestProducts()))
		{
			final ExtraGuestOccupancyProductModel extraGuestOccupancyProductModel = accommodationModel.getExtraGuestProducts()
					.stream().filter(extraGuestProduct ->
							priceRowService.getPriceRow(extraGuestProduct, passengerType,
									rate.getDates().get(0)) != null).findAny().orElse(null);

			if (extraGuestOccupancyProductModel != null)
			{
				final CartModificationData cartModification = this
						.addToCart(extraGuestOccupancyProductModel.getCode(), extraProductAdultsNeeded*rate.getCardinality());
				populateAccommodationDetailsOnExtraGuestOccupancyEntry(cartModification.getEntry().getEntryNumber(),
						rate.getDates(), passengerType);
				roomEntryNumbers.add(cartModification.getEntry().getEntryNumber());
			}

		}
	}

	public void populateAccommodationDetailsOnExtraGuestOccupancyEntry(final int entryNumber, final List<Date> dates,
			final String passengerType)
	{
		final AbstractOrderEntryModel entryToUpdate = this.getCartService()
				.getEntryForNumber(this.getCartService().getSessionCart(), entryNumber);
		AccommodationOrderEntryInfoModel orderEntryInfo = entryToUpdate.getAccommodationOrderEntryInfo();
		if (orderEntryInfo == null) {
			orderEntryInfo = this.getModelService().create(AccommodationOrderEntryInfoModel.class);
			entryToUpdate.setAccommodationOrderEntryInfo(orderEntryInfo);
		}
		orderEntryInfo.setPassengerType(passengerType);
		orderEntryInfo.setDates(dates);
		this.getModelService().save(orderEntryInfo);
		entryToUpdate.setType(OrderEntryType.ACCOMMODATION);
		entryToUpdate.setAmendStatus(AmendStatus.NEW);
		entryToUpdate.setActive(Boolean.TRUE);
		this.getModelService().save(entryToUpdate);
	}

	private AccommodationOrderEntryGroupModel getNewAccommodationOrderEntryGroup(
			final List<AbstractOrderEntryModel> dealAccommodationEntries, final Date checkInDate, final Date checkOutDate,
			final String accOffCode, final String accCode, final String ratePlanCode, final int refNumber, final int count,
			final List<RoomStayCandidateData> roomStayCandidates)
	{
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = getModelService().create(AccommodationOrderEntryGroupModel.class);
		accommodationOrderEntryGroup.setRoomStayRefNumber(refNumber);
		accommodationOrderEntryGroup.setAccommodation((AccommodationModel)getProductService().getProductForCode(accCode));
		accommodationOrderEntryGroup.setAccommodationOffering(getAccommodationOfferingService().getAccommodationOffering(accOffCode));
		accommodationOrderEntryGroup.setRatePlan((RatePlanModel)getCategoryService().getCategoryForCode(ratePlanCode));
		accommodationOrderEntryGroup.setStartingDate(checkInDate);
		accommodationOrderEntryGroup.setEndingDate(checkOutDate);
		accommodationOrderEntryGroup.setDealAccommodationEntries(dealAccommodationEntries);
		if(roomStayCandidates.size()>count)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = roomStayCandidates.get(count).getPassengerTypeQuantityList()
					.stream().filter(passengerTypeQuantity -> CollectionUtils.isNotEmpty(passengerTypeQuantity.getChildAges()))
					.findAny().orElse(null);
			if(passengerTypeQuantityData!=null){
				accommodationOrderEntryGroup.setChildAges(passengerTypeQuantityData.getChildAges().stream().map(childAge->String.valueOf(childAge)).collect(Collectors.toList()));
			}

		}
		final List<GuestCountModel> guestCounts=getGuestCounts(count, roomStayCandidates);
		accommodationOrderEntryGroup.setGuestCounts(guestCounts);

		getModelService().save(accommodationOrderEntryGroup);


		dealAccommodationEntries.forEach(dealAccommodationEntry -> {
				dealAccommodationEntry.setEntryGroup(accommodationOrderEntryGroup);
			});
		getModelService().saveAll(dealAccommodationEntries);

		return accommodationOrderEntryGroup;
	}

	protected List<GuestCountModel> getGuestCounts(final int count, final List<RoomStayCandidateData> roomStayCandidates)
	{
		final List<GuestCountModel> guestCounts=new ArrayList<>();

		if(roomStayCandidates.size()>count){
			final RoomStayCandidateData roomStayCandidateData = roomStayCandidates.get(count);
			final List<PassengerTypeQuantityData> passengerTypeQuantityList = roomStayCandidateData.getPassengerTypeQuantityList();
			if(CollectionUtils.isNotEmpty(passengerTypeQuantityList))
			{
				for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
				{
					if(passengerTypeQuantityData.getQuantity()>0){

						final GuestCountModel guestCountModel = getGuestCountService()
								.getGuestCount(passengerTypeQuantityData.getPassengerType().getCode(), passengerTypeQuantityData.getQuantity());

						if (Objects.nonNull(guestCountModel))
						{
							guestCounts.add(guestCountModel);
						}
					}
				}
			}
		}
		return guestCounts;
	}

	protected AddDealToCartData createAddDealToCartData(final List<Integer> entryNumbers, final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups, final Integer journeyRefNumber)
	{
		final AddDealToCartData addDealToCartData = new AddDealToCartData();
		addDealToCartData.setEntryGroupData(getCommerceCartService().createEntryGroupWithAccommodation(entryNumbers, accommodationOrderEntryGroups, journeyRefNumber));
		return addDealToCartData;
	}

	protected AddDealToCartData createAddDealToCartDataForPromotions(final List<Integer> entryNumbers, final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups,
			final Integer journeyRefNumber, final String promotionCode, final List<RoomStayCandidateData> roomStayCandidates)
	{
		final AddDealToCartData addDealToCartData = new AddDealToCartData();
		addDealToCartData.setEntryGroupData(getCommerceCartService().createEntryGroupForPromotions(entryNumbers, accommodationOrderEntryGroups, journeyRefNumber, promotionCode));
		return addDealToCartData;
	}

	@Override
	public void disableAccommodationEntriesForRefNo(final CartModel cart, final int journeyRefNo)
	{
		final List<AbstractOrderEntryModel> filteredEntries = cart.getEntries().stream()
				.filter(entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType())
						&& entry.getJourneyReferenceNumber() == journeyRefNo)
				.collect(Collectors.toList());

		accommodationCommerceCartService.inActivateOrRemoveAccommodationEntries(filteredEntries);

		cart.getEntries().forEach(entry->{
			getModelService().refresh(entry);
		});

	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Override
	public BCFTravelCommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	public void setCommerceCartService(final BCFTravelCommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public GuestCountService getGuestCountService()
	{
		return guestCountService;
	}

	public void setGuestCountService(final GuestCountService guestCountService)
	{
		this.guestCountService = guestCountService;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public RuleService getRuleService()
	{
		return ruleService;
	}

	public void setRuleService(final RuleService ruleService)
	{
		this.ruleService = ruleService;
	}

	public PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}

	public BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(
			final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}

	public BcfStockPreprocessor getBcfStockPreprocessor()
	{
		return bcfStockPreprocessor;
	}

	public void setBcfStockPreprocessor(final BcfStockPreprocessor bcfStockPreprocessor)
	{
		this.bcfStockPreprocessor = bcfStockPreprocessor;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	@Override
	public BcfAccommodationCommerceCartService getAccommodationCommerceCartService()
	{
		return accommodationCommerceCartService;
	}

	public void setAccommodationCommerceCartService(final BcfAccommodationCommerceCartService accommodationCommerceCartService)
	{
		this.accommodationCommerceCartService = accommodationCommerceCartService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}
}

