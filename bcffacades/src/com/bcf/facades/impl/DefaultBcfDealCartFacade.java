/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.packages.cart.AddDealToCartData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelfacades.facades.packages.impl.DefaultDealCartFacade;
import de.hybris.platform.travelfacades.packages.strategies.AddBundleToCartByTypeStrategy;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.entrygroup.EntryGroupData;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFCommerceAddToCartStrategy;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.activity.data.ActivityPricePerPassenger;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.DealBundleDetailsFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.packages.cart.AddBcfdDealToCartData;
import com.bcf.facades.stock.preprocessor.BcfStockPreprocessor;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.facades.vacations.GuestData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfDealCartFacade extends DefaultDealCartFacade implements BcfDealCartFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultDealCartFacade.class);
	private static final String DEFAULT = "DEFAULT";
	private static final String ERROR_MESSAGE = "Problem occured while adding deal bundle to the cart";
	public static final String TIME_SEPERATOR = ":";
	private BCFTravelCommerceCartService bcfCommerceCartService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfTransportOfferingFacade bcfTransportOfferingFacade;
	private BcfProductFacade bcfProductFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BCFTravelCartService travelCartService;
	private ModelService modelService;
	private BCFTravelRouteFacade travelRouteFacade;
	BcfBookingService bcfBookingService;
	private CalculationService calculationService;
	private SessionService sessionService;
	private ActivityProductSearchFacade activityProductSearchFacade;
	private BcfDealCartFacade dealCartFacade;
	private PassengerTypeService passengerTypeService;
	private CommerceAddToCartStrategy commerceAddToCartStrategy;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;
	private BcfStockPreprocessor bcfStockPreprocessor;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Override
	public List<CartModificationData> addDealToCart(final AddBcfdDealToCartData addBcfDealToCartData)
	{
		try
		{
			getBcfTravelCartFacade()
					.removeDealFromCart(addBcfDealToCartData.getParentDealBundleId(), addBcfDealToCartData.getStartingDate());

		}
		catch (final ModelRemovalException | IntegrationException ex)
		{
			LOG.error(ERROR_MESSAGE, ex);
			return Collections.singletonList(createCartModificationData(CommerceCartModificationStatus.UNAVAILABLE, 0));
		}
		final List<CartModificationData> totalCartModifications = new ArrayList<>();
		final DealBundleTemplateModel masterBundleTemplate = getDealBundleTemplateFacade()
				.getDealBundleTemplateById(addBcfDealToCartData.getParentDealBundleId());
		final List<BundleTemplateModel> dealBundleTemplateModelList = new ArrayList<>();
		getDealBundleDetailsFacade().populateDealBundles(masterBundleTemplate, dealBundleTemplateModelList);
		try
		{
			final EntryGroupData entryGroupData = createEntryGroupData(masterBundleTemplate, addBcfDealToCartData.getStartingDate());
			for (final BundleTemplateModel deal : dealBundleTemplateModelList)
			{
				final List<CartModificationData> cartModifications = new ArrayList<>();
				final AddDealToCartData addDealToCartData = createAddDealToCartData(addBcfDealToCartData, deal);
				entryGroupData.setChildPackageId(deal.getId());
				addDealToCartData.setEntryGroupData(entryGroupData);
				for (final BundleTemplateModel childTemplate : deal.getChildTemplates())
				{
					final String key = childTemplate.getClass().getSimpleName();
					final AddBundleToCartByTypeStrategy strategy = Objects.nonNull(getAddBundleByTypeStrategyMap().get(key))
							? getAddBundleByTypeStrategyMap().get(key) : getAddBundleByTypeStrategyMap().get(DEFAULT);
					cartModifications.addAll(strategy.addBundleToCart(childTemplate, addDealToCartData));
				}

				addDealToCartData.setStartingDate(addDealToCartData.getEndingDate());
				populateEntryGroup(addDealToCartData.getEntryGroupData(), cartModifications);
				getBcfCommerceCartService()
						.createOrderGroup(addDealToCartData.getEntryGroupData(), getCartService().getSessionCart());
				totalCartModifications.addAll(cartModifications);
			}

			// Add fees and discounts
			getCartFacade().recalculateCart();

			PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
			bcfStockPreprocessor.validateAndReserve(decisionData, getCartService().getSessionCart());

		}
		catch (CartValidationException | InsufficientStockLevelException | OrderProcessingException | CommerceCartModificationException modEx)
		{
			LOG.error(ERROR_MESSAGE, modEx);
			try
			{
				getBcfTravelCartFacade()
						.removeDealFromCart(addBcfDealToCartData.getParentDealBundleId(), addBcfDealToCartData.getStartingDate());
			}
			catch (final ModelRemovalException | IntegrationException ex)
			{
				LOG.error(ERROR_MESSAGE, ex);
			}

			return Collections.singletonList(createCartModificationData(CommerceCartModificationStatus.UNAVAILABLE, 0));
		}

		return totalCartModifications;
	}

	protected void populateEntryGroup(final EntryGroupData entryGroupData,
			final List<CartModificationData> cartModificationDataList)
	{
		entryGroupData.getEntryNumbers().addAll(
				cartModificationDataList.stream().map(cartModificationData -> cartModificationData.getEntry().getEntryNumber())
						.collect(
								Collectors.toList()));
	}

	protected AddDealToCartData createAddDealToCartData(final AddBcfdDealToCartData addBcfdDealToCartData,
			final BundleTemplateModel dealBundleTemplateModel)
	{
		final AddDealToCartData addDealToCartData = new AddDealToCartData();

		addDealToCartData.setStartingDate(addBcfdDealToCartData.getStartingDate());
		addDealToCartData.setEndingDate(TravelDateUtils
				.addDays(addBcfdDealToCartData.getStartingDate(), ((DealBundleTemplateModel) dealBundleTemplateModel).getLength()));
		addDealToCartData.setDealBundleId(dealBundleTemplateModel.getId());

		final List<ItineraryPricingInfoData> itineraryPricingInfos = addBcfdDealToCartData.getSelectedItineraryInfos()
				.get(dealBundleTemplateModel.getId());
		if (itineraryPricingInfos.stream().anyMatch(ItineraryPricingInfoData::isSelected))
		{
			addDealToCartData.setItineraryPricingInfos(itineraryPricingInfos);
		}
		addDealToCartData
				.setCheckInDate(
						itineraryPricingInfos.get(0).getBundleTemplates().get(0).getTransportOfferings().get(0).getArrivalTime());
		addDealToCartData
				.setCheckOutDate(
						itineraryPricingInfos.get(1).getBundleTemplates().get(0).getTransportOfferings().get(0).getDepartureTime());

		addDealToCartData.setPassengerTypes(addBcfdDealToCartData.getPassengerTypes());

		addDealToCartData.setVehicleTypes(addBcfdDealToCartData.getVehicleTypes());
		return addDealToCartData;
	}

	protected EntryGroupData createEntryGroupData(final DealBundleTemplateModel masterBundle, final Date departureDate)
	{
		final EntryGroupData entryGroupData = new EntryGroupData();
		entryGroupData.setPackageId(masterBundle.getId());
		entryGroupData.setPackageName(masterBundle.getName());
		entryGroupData.setDepartureDate(departureDate);
		entryGroupData.setReturnDate(TravelDateUtils.addDays(departureDate, masterBundle.getLength()));
		entryGroupData.setEntryNumbers(new ArrayList<>());
		return entryGroupData;
	}

	@Override
	public void populateAddBundleToCart(final TravelBundleTemplateData bundleData,
			final BcfAddBundleToCartData addBundleToCart, final String travelRouteCode, final boolean isDynamicPackage)
	{
		if (isDynamicPackage)
		{
			final TravelRouteData travelRouteData = getTravelRouteFacade().getTravelRoute(travelRouteCode);
			final List<TransportOfferingData> transportOfferingDatas = getBcfTransportOfferingFacade()
					.getDefaultTransportOfferingsForTravelSector(travelRouteData.getSectors().stream().findFirst().get().getCode());
			if (CollectionUtils.isNotEmpty(transportOfferingDatas))
			{
				final TransportOfferingData transportOfferingData = transportOfferingDatas.get(0);
				addBundleToCart.setTransportOfferings(Collections.singletonList(transportOfferingData.getCode()));
				addBundleToCart.setTransportOfferingDatas(Collections.singletonList(transportOfferingData));
			}
			addBundleToCart.setProductCode(getBcfProductFacade()
					.getFareProductForFareBasisCode(
							getBcfConfigurablePropertiesService()
									.getBcfPropertyValue(BcfFacadesConstants.DEFAULT_FARE_BASIS_CODE_FOR_PASSENGER),
							FareProductType.PASSENGER).getCode());
			addBundleToCart.setVehicleProductCode(getBcfProductFacade()
					.getFareProductForFareBasisCode(
							getBcfConfigurablePropertiesService().getBcfPropertyValue(
									BcfFacadesConstants.DEFAULT_FARE_BASIS_CODE_FOR_VEHICLE),
							FareProductType.VEHICLE).getCode());
		}
		else
		{
			final Optional<FareProductData> passengerFareProductCode = bundleData.getFareProducts().stream()
					.filter(product -> FareProductType.PASSENGER.getCode().equals(product.getFareProductType())).findFirst();
			final Optional<FareProductData> vehicleFareProductCode = bundleData.getFareProducts().stream()
					.filter(product -> FareProductType.VEHICLE.getCode().equals(product.getFareProductType())).findFirst();
			addBundleToCart.setProductCode(
					passengerFareProductCode.isPresent() ? passengerFareProductCode.get().getCode() : StringUtils.EMPTY);
			addBundleToCart.setVehicleProductCode(
					vehicleFareProductCode.isPresent() ? vehicleFareProductCode.get().getCode() : StringUtils.EMPTY);
			if(CollectionUtils.isNotEmpty(bundleData.getTransportOfferings())){
			addBundleToCart.setTransportOfferings(
					bundleData.getTransportOfferings().stream().map(TransportOfferingData::getCode).collect(Collectors.toList()));
			addBundleToCart.setTransportOfferingDatas(bundleData.getTransportOfferings());
			}else{
				final TravelRouteData travelRouteData = getTravelRouteFacade().getTravelRoute(travelRouteCode);
				final TransportOfferingData transportOfferingData = getBcfTransportOfferingFacade()
						.getDefaultTransportOfferingsForTravelSector(travelRouteData.getSectors().stream().findFirst().get().getCode())
						.get(0);
				addBundleToCart.setTransportOfferings(Collections.singletonList(transportOfferingData.getCode()));
				addBundleToCart.setTransportOfferingDatas(Collections.singletonList(transportOfferingData));
			}

		}
	}

	@Override
	public void removeSelectedActivityEntries(final String activityProductCode, final int journeyRefNumber)
	{
		final AbstractOrderModel cart = getCartService().getSessionCart();
		if(bcfTravelCartFacade.isAmendmentCart()){
			List<AbstractOrderEntryModel> dealActivityEntries =cart.getEntries().stream().filter(entry ->entry.getActive()  && activityProductCode.equals(entry.getProduct().getCode()) && entry.getJourneyReferenceNumber() == journeyRefNumber).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(dealActivityEntries)){
				List<AbstractOrderEntryModel> entriesToRemove =new ArrayList<>();
				for(AbstractOrderEntryModel dealActivityEntry :dealActivityEntries){
					saveDealActivities(entriesToRemove, dealActivityEntry);
				}
				if(CollectionUtils.isNotEmpty(entriesToRemove)){
					getTravelCartService().removeActivityOrderEntries(entriesToRemove,true);
				}
			}

		}else{
			final List<AbstractOrderEntryModel> dealActivityEntries = cart.getEntries().stream().filter(entry ->entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode()) && activityProductCode.equals(entry.getProduct().getCode()) && entry.getJourneyReferenceNumber() == journeyRefNumber).collect(
					Collectors.toList());
			getTravelCartService().removeActivityOrderEntries(dealActivityEntries,true);

		}

		getModelService().refresh(cart);
		bcfTravelCartFacadeHelper.updateAvailabilityStatus();
		bcfTravelCartFacadeHelper.calculateChangeFees();
	}

	private void saveDealActivities(final List<AbstractOrderEntryModel> entriesToRemove,
			final AbstractOrderEntryModel dealActivityEntry)
	{
		if (dealActivityEntry.getActive() && (dealActivityEntry.getAmendStatus().equals(AmendStatus.NEW) || dealActivityEntry
				.getAmendStatus().equals(AmendStatus.CHANGED)))
		{
			entriesToRemove.add(dealActivityEntry);

		}
		else
		{
			dealActivityEntry.setActive(false);
			dealActivityEntry.setAmendStatus(AmendStatus.CHANGED);
			getModelService().save(dealActivityEntry);
		}
	}


	@Override
	public void  removeSelectedActivityEntries(final String activityProductCode, final int journeyRefNumber, String date, String time)
	{
		final AbstractOrderModel cart = getCartService().getSessionCart();
		if(bcfTravelCartFacade.isAmendmentCart()){
			List<AbstractOrderEntryModel> dealActivityEntries =cart.getEntries().stream().filter(entry->OrderEntryType.ACTIVITY.equals(entry.getType())).filter(entry ->isMatchedEntryWithAmend(entry,activityProductCode,journeyRefNumber,date,time)).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(dealActivityEntries)){
				List<AbstractOrderEntryModel> entriesToRemove =new ArrayList<>();
				for(AbstractOrderEntryModel dealActivityEntry :dealActivityEntries){
					saveDealActivities(entriesToRemove, dealActivityEntry);
				}
				if(CollectionUtils.isNotEmpty(entriesToRemove)){
					getTravelCartService().removeActivityOrderEntries(entriesToRemove,true);
				}
			}

		}else{

			final List<AbstractOrderEntryModel> dealActivityEntries = cart.getEntries().stream().filter(entry->OrderEntryType.ACTIVITY.equals(entry.getType())).filter(entry ->isMatchedEntryWithAmendSame(entry,activityProductCode,journeyRefNumber,date,time)).collect(
					Collectors.toList());
			getTravelCartService().removeActivityOrderEntries(dealActivityEntries,true);

		}

		getModelService().refresh(cart);
		bcfTravelCartFacadeHelper.updateAvailabilityStatus();
		bcfTravelCartFacadeHelper.calculateChangeFees();
		bcfTravelCartFacade.recalculateCart();
	}

	private boolean isMatchedEntryWithAmendSame(final AbstractOrderEntryModel entry,final String activityProductCode, final int journeyRefNumber,String date, String time){
		ActivityOrderEntryInfoModel activityOrderEntryInfoModel =  entry.getActivityOrderEntryInfo();
		return entry.getActive() && !entry.getAmendStatus().equals(AmendStatus.SAME) &&
				activityProductCode.equals(entry.getProduct().getCode()) && entry.getJourneyReferenceNumber() == journeyRefNumber &&
				isActivityDateTimeMatchesInActivityOrderEntryInfo(activityOrderEntryInfoModel, date, time);
	}

	private boolean isMatchedEntryWithAmend(final AbstractOrderEntryModel entry,final String activityProductCode, final int journeyRefNumber,String date, String time){
		ActivityOrderEntryInfoModel activityOrderEntryInfoModel =  entry.getActivityOrderEntryInfo();
		return entry.getActive() &&
				activityProductCode.equals(entry.getProduct().getCode()) && entry.getJourneyReferenceNumber() == journeyRefNumber &&
				isActivityDateTimeMatchesInActivityOrderEntryInfo(activityOrderEntryInfoModel, date, time);
	}

	public boolean isActivityDateTimeMatchesInActivityOrderEntryInfo(final ActivityOrderEntryInfoModel activityOrderEntryInfoModel, final String date,
			final String time)
	{
		Date activityDateFronOrderEntry = DateUtils.truncate(activityOrderEntryInfoModel.getActivityDate(), Calendar.DATE);
		String activityTimeFromOrderEntry = activityOrderEntryInfoModel.getActivityTime();
		Date selectedActivityDate = TravelDateUtils.convertStringDateToDate(date,
				BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);

		return activityDateFronOrderEntry.equals(selectedActivityDate) && isTimeMatched(activityTimeFromOrderEntry, time);
	}

	boolean isTimeMatched(final String activityTimeFromOrderEntry,final String time){
		if(StringUtils.isBlank(activityTimeFromOrderEntry) &&  StringUtils.isBlank(time))
		{
			return true;
		}
		return StringUtils.equals(activityTimeFromOrderEntry, time);
	}

	@Override
	public void removeSelectedActivityEntries(String entryNumbers)
	{
		final CartModel cart = getCartService().getSessionCart();
		List<Integer> entryNumberArray=Arrays.asList(StringUtils.split(entryNumbers,"-")).stream().map(entryNumber->Integer.valueOf(entryNumber)).collect(
				Collectors.toList());
		final List<AbstractOrderEntryModel> activityEntries = cart.getEntries().stream().filter(entry ->entry.getActive()  && entryNumberArray.contains(entry.getEntryNumber())).collect(Collectors.toList());

		for(AbstractOrderEntryModel activityEntry :activityEntries){

			if(bcfTravelCartFacade.isAmendmentCart()){
				if (activityEntry.getActive() && activityEntry.getAmendStatus().equals(AmendStatus.NEW) || activityEntry.getAmendStatus().equals(AmendStatus.CHANGED))
				{
					getTravelCartService().removeActivityOrderEntries(Arrays.asList(activityEntry),true);
				}else{
					activityEntry.setActive(false);
					activityEntry.setAmendStatus(AmendStatus.CHANGED);
					getModelService().save(activityEntry);
				}
			}else{
				getTravelCartService().removeActivityOrderEntries(Arrays.asList(activityEntry),true);

			}
		}
		getModelService().refresh(cart);
		((BCFCommerceAddToCartStrategy) getCommerceAddToCartStrategy()).normalizeEntryNumbersIfNeeded(cart);
		bcfTravelCartFacade.recalculateCart();

		getModelService().refresh(cart);
	}

	@Override
	public void disableSelectedActivityEntries(final String activityProductCode, final int journeyRefNumber)
	{
		final AbstractOrderModel cart = getCartService().getSessionCart();
		final List<AbstractOrderEntryModel> dealActivityEntries = cart.getEntries().stream().filter(entry -> entry.getActive() && entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode()) && activityProductCode.equals(entry.getProduct().getCode()) && entry.getJourneyReferenceNumber() == journeyRefNumber).collect(
				Collectors.toList());

		if(CollectionUtils.isNotEmpty(dealActivityEntries)){
			final List<ItemModel> itemsToSave = new ArrayList<>();
			dealActivityEntries.forEach(dealActivityEntry->{
				dealActivityEntry.setActive(false);
				dealActivityEntry.setAmendStatus(AmendStatus.CHANGED);
				itemsToSave.add(dealActivityEntry);
			});
			modelService.saveAll(itemsToSave);


		}
	}

	@Override
	public boolean addActivityToCart(final ActivityPricePerPassengerWithSchedule addActivityPriceSchedule,
			final AddActivityToCartData data)
	{
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			dealCartFacade.disableSelectedActivityEntries(addActivityPriceSchedule.getProductCode(),
					addActivityPriceSchedule.getJourneyRefNumber());
		}

		if (addActivityPriceSchedule.isChangeActivity())
		{
			dealCartFacade.removeSelectedActivityEntries(addActivityPriceSchedule.getChangeActivityCode(),
					addActivityPriceSchedule.getJourneyRefNumber());
		}

		data.setGuestData(createGuestData(addActivityPriceSchedule.getActivityPricePerPassenger()));
		data.setJourneyRefNumber(addActivityPriceSchedule.getJourneyRefNumber());
		try
		{
			final List<CartModificationData> cartModificationDatas = bcfTravelCartFacade
					.createActivityOrderEntryInfoForDeal(addActivityPriceSchedule.getProductCode(), data);
			if (CollectionUtils.isNotEmpty(cartModificationDatas) )
			{
				final CartModel cart = getCartService().getSessionCart();
				if (OptionBookingUtil.isOptionBooking(cart))
				{
					getBcfOptionBookingNotificationService().startOptionBookingEmailProcess(cart, cartModificationDatas);
				}
				CartModificationData cartModificationData =cartModificationDatas.stream().filter(cartModification->cartModification.getQuantityAdded()<= 0).findAny().orElse(null);
				return cartModificationData == null;

			}
			return true;
		}
		catch (final CommerceCartModificationException ex)
		{
			return false;
		}
	}

	protected List<GuestData> createGuestData(final List<ActivityPricePerPassenger> activityPricePerPassengerList)
	{
		final List<GuestData> guestDataList = new ArrayList<>();
		final List<ActivityPricePerPassenger> selectedPassengers = activityPricePerPassengerList.stream()
				.filter(passenger -> passenger.getPassengerType().getQuantity() > 0).collect(
				Collectors.toList());
		selectedPassengers.forEach(activityPricePerPassenger -> {
			final GuestData participantData = new GuestData();
			final PassengerTypeModel passengerType = getPassengerTypeService().getPassengerType(activityPricePerPassenger.getPassengerType().getCode());
			String guestType;
			if(passengerType instanceof SpecializedPassengerTypeModel)
			{
				guestType = ((SpecializedPassengerTypeModel) passengerType).getBasePassengerType().getCode();
			}
			else
			{
				guestType = passengerType.getCode();
			}
			participantData.setGuestType(guestType);
			participantData.setQuantity(activityPricePerPassenger.getPassengerType().getQuantity());
			guestDataList.add(participantData);
		});
		return guestDataList;
	}

	@Override
	public List<AddActivityToCartData> getActivityDataFromCart()
	{
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			final int journeyRefNumber = getSessionService().getAttribute("journeyRefNum");
			final CartModel cartModel = (CartModel) travelCartService.getOrCreateSessionCart();
			final List<AbstractOrderEntryModel> activityEntries = cartModel.getEntries().stream().filter(
					entry -> entry.getActive() && entry.getJourneyReferenceNumber() == journeyRefNumber && entry.getType() != null
							&& entry.getType().getCode().equals(OrderEntryType.ACTIVITY.getCode())).collect(
					Collectors.toList());

			final List<AddActivityToCartData> activityDatas = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(activityEntries))
			{

				final Map<String, List<AbstractOrderEntryModel>> activityEntryMap = activityEntries.stream()
						.collect(Collectors.groupingBy(entry -> entry.getProduct().getCode()));

				for (final Map.Entry<String, List<AbstractOrderEntryModel>> activityEntryMapEntry : activityEntryMap.entrySet())
				{
					final AddActivityToCartData activityData = new AddActivityToCartData();
					activityData.setActivityCode(activityEntryMapEntry.getKey());
					activityData.setActivityDate(TravelDateUtils.convertDateToStringDate(
							activityEntryMapEntry.getValue().get(0).getActivityOrderEntryInfo().getActivityDate(),
							BcfFacadesConstants.ACTIVITY_DATE_PATTERN));
					activityData.setCheckInDate(activityEntryMapEntry.getValue().get(0).getActivityOrderEntryInfo().getActivityDate());
					activityData.setJourneyRefNumber(journeyRefNumber);
					activityData.setGuestData(populateGuestData(activityEntryMapEntry.getValue()));
					activityDatas.add(activityData);
				}

				return activityDatas;

			}
		}
		return new ArrayList<>();
	}

	private List<GuestData> populateGuestData(final List<AbstractOrderEntryModel> activityEntries)
	{
		final List<GuestData> guests = new ArrayList<>();

		final Map<String, Long> guestMap = new HashMap<>();
		for (final AbstractOrderEntryModel activityEntry : activityEntries)
		{
			if(guestMap.containsKey(activityEntry.getActivityOrderEntryInfo().getGuestType().getCode())){

				guestMap.compute(activityEntry.getActivityOrderEntryInfo().getGuestType().getCode(), (key, value) -> value+activityEntry.getQuantity());
			}else{
				guestMap.putIfAbsent(activityEntry.getActivityOrderEntryInfo().getGuestType().getCode(), activityEntry.getQuantity());
			}

		}
		for (final Map.Entry<String, Long> guestMapEntry : guestMap.entrySet())
		{
			final GuestData guest = new GuestData();
			guest.setGuestType(guestMapEntry.getKey());
			guest.setQuantity(guestMapEntry.getValue().intValue());
			guests.add(guest);
		}
		return guests;
	}

	@Override
	public Pair<Map<String, ActivityPricePerPassengerWithSchedule>,Map<String, List<Date>>> getActivityPricePerPassengerWithSchedulesMapFromActivityData(
			final List<AddActivityToCartData> activityCartDatas, final Date commencementDate, final String locationCode, final List<Date> scheduleDates,Date checkInDate,Date checkOutDate)
	{
		final Map<String, ActivityPricePerPassengerWithSchedule> activityPricePerPassengerWithScheduleMap = new HashMap<>();
		final Map<String, List<Date>> activityProductWithScheduleDatesMap =new HashMap<>();
		if (CollectionUtils.isNotEmpty(activityCartDatas))
		{
			for (final AddActivityToCartData activityCartData : activityCartDatas)
			{
				final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule = activityProductSearchFacade
						.createActivityPricePerPassengerAndSchedule(activityCartData.getActivityCode(), commencementDate, locationCode);
				activityPricePerPassengerWithSchedule.setSelectedDate(activityCartData.getActivityDate());
				activityPricePerPassengerWithSchedule.setCheckInDate(activityCartData.getCheckInDate());
				if(CollectionUtils.isEmpty(activityPricePerPassengerWithSchedule.getAvailableSchedules())){
					final Date selectedDate = TravelDateUtils
							.convertStringDateToDate(activityCartData.getActivityDate(), BcfFacadesConstants.ACTIVITY_DATE_PATTERN);
					activityPricePerPassengerWithSchedule.setSelectedDate(TravelDateUtils.convertDateToStringDate(selectedDate,"dd/MM/yyyy"));
				}else{
					activityPricePerPassengerWithSchedule.setSelectedDate(activityCartData.getActivityDate());
				}

				populateActivityPricePerPassenger(activityCartData, activityPricePerPassengerWithSchedule);
				if (CollectionUtils.isNotEmpty(activityPricePerPassengerWithSchedule.getAvailableSchedules()))
				{
					final String startTime = activityPricePerPassengerWithSchedule.getAvailableSchedules().stream().findFirst().get()
							.getStartTime();
					final String[] time = startTime.split(":", 2);
					final int startTimeHours = Integer.parseInt(time[0]) + (Integer.parseInt(time[1]) / 60);
					Date activityCheckInDate = TravelDateUtils.addHours(checkInDate, startTimeHours);
					activityProductWithScheduleDatesMap.put(activityPricePerPassengerWithSchedule.getProductCode(),getScheduleDates(activityCheckInDate, checkOutDate));
				}else{
					activityProductWithScheduleDatesMap.put(activityPricePerPassengerWithSchedule.getProductCode(),scheduleDates);
				}
				activityPricePerPassengerWithScheduleMap
						.putIfAbsent(activityPricePerPassengerWithSchedule.getProductCode(), activityPricePerPassengerWithSchedule);
			}
		}
		return Pair.of(activityPricePerPassengerWithScheduleMap,activityProductWithScheduleDatesMap);
	}

	private void populateActivityPricePerPassenger(final AddActivityToCartData activityCartData,
			final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule)
	{
		if (CollectionUtils.isNotEmpty(activityPricePerPassengerWithSchedule.getActivityPricePerPassenger())
				&& CollectionUtils.isNotEmpty(activityCartData.getGuestData()))
		{
			for (final ActivityPricePerPassenger activityPricePerPassenger : activityPricePerPassengerWithSchedule
					.getActivityPricePerPassenger())
			{

				final GuestData guest = activityCartData.getGuestData().stream().filter(
						guestData -> guestData.getGuestType().equalsIgnoreCase(activityPricePerPassenger.getPassengerType().getPassengerType()))
						.findAny().orElse(null);
				if (guest != null)
				{
					activityPricePerPassenger.getPassengerType().setQuantity(guest.getQuantity());
				}
			}
		}
	}

	@Override
	public List<Date> getScheduleDates(Date checkInDate, final Date checkOutDate)
	{
		final long numOfDaysBetween = TravelDateUtils.getDaysBetweenDates(checkInDate, checkOutDate);
		final List<Date> dates = new ArrayList<>((int) numOfDaysBetween + 1);
		final Date incrementedCheckOutDate = TravelDateUtils.addDays(checkOutDate, 1);
		while (!TravelDateUtils.isSameDate(checkInDate, incrementedCheckOutDate))
		{
			dates.add(checkInDate);
			checkInDate = TravelDateUtils.addDays(checkInDate, 1);
		}
		return dates;
	}

	@Override
	public Map<ActivityScheduleData, List<Date>> getScheduleDates(Date checkInDate, final Date checkOutDate,
			final List<ActivityScheduleData> availableSchedules)
	{
		final long numOfDaysBetween = TravelDateUtils.getDaysBetweenDates(checkInDate, checkOutDate);

		Map<ActivityScheduleData, List<Date>> datesGroupByScheduleMap = new HashMap<>();
		final Date incrementedCheckOutDate = TravelDateUtils.addDays(checkOutDate, 1);
		Date scheduleDate;

		Date checkinDateTemp;
		Date incrementedCheckOutDateTemp;

		for (ActivityScheduleData availableSchedule : availableSchedules)
		{
			final List<Date> dates = new ArrayList<>((int) numOfDaysBetween + 1);
			checkinDateTemp = checkInDate;
			incrementedCheckOutDateTemp = incrementedCheckOutDate;
			while (!TravelDateUtils.isSameDate(checkinDateTemp, incrementedCheckOutDateTemp))
			{
				if (BCFDateUtils
						.isBetweenDatesInclusive(checkinDateTemp, availableSchedule.getStartDate(), availableSchedule.getEndDate()))
				{
					scheduleDate = getScheduleDateWithTime(checkinDateTemp,availableSchedule.getStartTime());
					dates.add(scheduleDate);
				}
				checkinDateTemp = TravelDateUtils.addDays(checkinDateTemp, 1);
			}
			datesGroupByScheduleMap.put(availableSchedule, dates);
		}
		return datesGroupByScheduleMap;
	}

	private Date getScheduleDateWithTime(final Date checkinDate, final String startTimeStr)
	{
		Date scheduleDate = checkinDate;
		final String[] time = startTimeStr.split(TIME_SEPERATOR, 2);
		scheduleDate = TravelDateUtils.addHours(scheduleDate, Integer.parseInt(time[0]));
		scheduleDate = BCFDateUtils.addMinutes(scheduleDate, Integer.parseInt(time[1]));
		return scheduleDate;
	}

	protected DealBundleDetailsFacade getDealBundleDetailsFacade()
	{
		return (DealBundleDetailsFacade) getDealBundleTemplateFacade();
	}

	/**
	 * @return the bcfCommerceCartService
	 */
	protected BCFTravelCommerceCartService getBcfCommerceCartService()
	{
		return bcfCommerceCartService;
	}

	/**
	 * @param bcfCommerceCartService the bcfCommerceCartService to set
	 */
	@Required
	public void setBcfCommerceCartService(final BCFTravelCommerceCartService bcfCommerceCartService)
	{
		this.bcfCommerceCartService = bcfCommerceCartService;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfTransportOfferingFacade getBcfTransportOfferingFacade()
	{
		return bcfTransportOfferingFacade;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setBcfTransportOfferingFacade(final BcfTransportOfferingFacade bcfTransportOfferingFacade)
	{
		this.bcfTransportOfferingFacade = bcfTransportOfferingFacade;
	}

	public BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	protected BCFTravelCartService getTravelCartService()
	{
		return travelCartService;
	}

	@Required
	public void setTravelCartService(final BCFTravelCartService travelCartService)
	{
		this.travelCartService = travelCartService;
	}

	protected BCFTravelRouteFacade getTravelRouteFacade()
	{
		return travelRouteFacade;
	}
	@Required
	public void setTravelRouteFacade(final BCFTravelRouteFacade travelRouteFacade)
	{
		this.travelRouteFacade = travelRouteFacade;
	}


	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public ActivityProductSearchFacade getActivityProductSearchFacade()
	{
		return activityProductSearchFacade;
	}

	public void setActivityProductSearchFacade(final ActivityProductSearchFacade activityProductSearchFacade)
	{
		this.activityProductSearchFacade = activityProductSearchFacade;
	}

	public BcfDealCartFacade getDealCartFacade()
	{
		return dealCartFacade;
	}

	public void setDealCartFacade(final BcfDealCartFacade dealCartFacade)
	{
		this.dealCartFacade = dealCartFacade;
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	public CommerceAddToCartStrategy getCommerceAddToCartStrategy()
	{
		return commerceAddToCartStrategy;
	}

	public void setCommerceAddToCartStrategy(final CommerceAddToCartStrategy commerceAddToCartStrategy)
	{
		this.commerceAddToCartStrategy = commerceAddToCartStrategy;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}

	public BcfStockPreprocessor getBcfStockPreprocessor()
	{
		return bcfStockPreprocessor;
	}

	@Required
	public void setBcfStockPreprocessor(final BcfStockPreprocessor bcfStockPreprocessor)
	{
		this.bcfStockPreprocessor = bcfStockPreprocessor;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}
}
