/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.impl;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cmsfacades.data.UserGroupData;
import de.hybris.platform.cmsfacades.usergroups.impl.DefaultUserGroupFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfUserGroupService;
import com.bcf.facades.BcfUserGroupFacade;


public class DefaultBcfUserGroupFacade extends DefaultUserGroupFacade implements BcfUserGroupFacade
{
	private BcfUserGroupService bcfUserGroupService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "defaultCustomerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;

	private static final String AGENT_DECLARE_PERIOD = "agent.declare.max.period";
	public static final String ASMAGENTGROUPS = "declare.salesagent.groups";

	@Override
	public List<UserGroupData> getUserListForIds(final List<String> groupIds) throws CMSItemNotFoundException
	{
		try
		{
			return getUserGroupDataConverter().convertAll(getBcfUserGroupService().getUserGroups(groupIds));
		}
		catch (final UnknownIdentifierException e)
		{
			throw new CMSItemNotFoundException("UserGroup not found", e);
		}
	}

	@Override
	public List<CustomerData> getAgents()
	{
		String asmAgentGroups = configurationService.getConfiguration().getString(ASMAGENTGROUPS, StringUtils.EMPTY);

		List<String> salesAgentGroupList = new ArrayList<String>();
		if (StringUtils.isNotEmpty(asmAgentGroups))
		{
			final String[] salesAgentGroupArray = StringUtils
					.split(configurationService.getConfiguration().getString(ASMAGENTGROUPS), ',');
			salesAgentGroupList = Arrays.asList(salesAgentGroupArray);
		}

		List<CustomerData> customerDataList = new ArrayList<CustomerData>();
		if (CollectionUtils.isNotEmpty(salesAgentGroupList))
		{
			long declarePeriod = configurationService.getConfiguration()
					.getLong(AGENT_DECLARE_PERIOD);

			List<UserModel> userModels = bcfUserGroupService.getUsersByGroup(salesAgentGroupList);

			if (Objects.nonNull(userModels) && CollectionUtils.isNotEmpty(userModels))
			{
				for (UserModel userModel : userModels)
				{
					EmployeeModel employee = (EmployeeModel) userModel;

					Date currentTime = new Date();
					Date agentDeclareTime = employee.getEndofdayDeclareTime();

					boolean customerDeclared = Boolean.FALSE;

					if (Objects.nonNull(agentDeclareTime))
					{
						long declareDifference = currentTime.getTime() - agentDeclareTime.getTime();
						long daysDifference = (declareDifference / (1000 * 60 * 60 * 24));
						if (daysDifference > declarePeriod)
						{
							customerDeclared = Boolean.TRUE;
						}
					}

					CustomerData customerData = new CustomerData();
					customerData.setCustomerId(employee.getUid());
					customerData.setName(employee.getName());
					customerData.setEndofdayDeclareTime(employee.getEndofdayDeclareTime());
					customerData.setSelected(customerDeclared);
					customerDataList.add(customerData);
				}
			}
		}

		return customerDataList;
	}

	protected BcfUserGroupService getBcfUserGroupService()
	{
		return bcfUserGroupService;
	}

	@Required
	public void setBcfUserGroupService(final BcfUserGroupService bcfUserGroupService)
	{
		this.bcfUserGroupService = bcfUserGroupService;
	}
}
