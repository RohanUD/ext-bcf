/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades;

import de.hybris.platform.core.model.product.ProductModel;
import java.util.Date;
import java.util.List;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.activityProduct.ActivityDetailsData;


public interface ActivityProductFacade
{
	List<ActivityDetailsData> findActivityProducts();

	List<ActivityDetailsData> findActivityProductsDetails(List<ProductModel> activityProducts);

	ActivityDetailsData getActivityDetailsData(String activityCode);

	boolean checkAvailability(AddActivityToCartData addActivityToCartData);

	void removeInvalidActivities(ActivitiesResponseData activitiesResponseData);

	void setAvailableStatusForActivitiesResponseData(ActivitiesResponseData activitiesResponseData, Date checkInDate);

	int getMaxPaxAllowedPerPaxTypePerActivity();
}
