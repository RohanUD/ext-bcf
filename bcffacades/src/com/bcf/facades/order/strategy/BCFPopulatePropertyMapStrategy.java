/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.strategy;

import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.List;
import java.util.Map;
import com.bcf.facades.order.CartEntryPropertiesData;


public interface BCFPopulatePropertyMapStrategy
{
	Map<String, Map<String, Object>> populatePropertiesMap(CartEntryPropertiesData addPropertiesToCartEntryData,
			List<TravellerModel> travellersList, TravelOrderEntryInfoModel travelEntryInfoModel);
}
