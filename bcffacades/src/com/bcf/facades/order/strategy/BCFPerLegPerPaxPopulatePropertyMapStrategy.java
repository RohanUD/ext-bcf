/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.strategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.TransportOfferingService;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.order.CartEntryPropertiesData;


public class BCFPerLegPerPaxPopulatePropertyMapStrategy implements BCFPopulatePropertyMapStrategy
{

	private TravelRouteService travelRouteService;
	private TransportOfferingService transportOfferingService;

	@Override
	public Map<String, Map<String, Object>> populatePropertiesMap(final CartEntryPropertiesData addPropertiesToCartEntryData,
			final List<TravellerModel> travellersList, final TravelOrderEntryInfoModel travelEntryInfoModel)
	{
		final Map<String, Map<String, Object>> propertiesMaps = new HashMap<>();

		final Map<String, Object> propertiesMapForOrderEntry = new HashMap<>();
		final Map<String, Object> propertiesMapForOrderEntryInfo = new HashMap<>();

		if (CollectionUtils.isNotEmpty(addPropertiesToCartEntryData.getTransportOfferingCodes()))
		{
			final List<TransportOfferingModel> transportOfferings = new ArrayList<>();
			addPropertiesToCartEntryData.getTransportOfferingCodes().forEach(transportOffering -> transportOfferings
					.add(getTransportOfferingService().getTransportOffering(transportOffering)));
			propertiesMapForOrderEntryInfo.put(TravelOrderEntryInfoModel.TRANSPORTOFFERINGS, transportOfferings);
		}

		if (StringUtils.isNotEmpty(addPropertiesToCartEntryData.getTravelRoute()))
		{
			final TravelRouteModel travelRoute = getTravelRouteService()
					.getTravelRoute(addPropertiesToCartEntryData.getTravelRoute());
			propertiesMapForOrderEntryInfo.put(TravelOrderEntryInfoModel.TRAVELROUTE, travelRoute);
		}

		propertiesMapForOrderEntryInfo.put(TravelOrderEntryInfoModel.TRAVELLERS, travellersList);
		propertiesMapForOrderEntryInfo.put(TravelOrderEntryInfoModel.ORIGINDESTINATIONREFNUMBER,
				addPropertiesToCartEntryData.getOriginDestinationRefNo());
		propertiesMapForOrderEntry.put(AbstractOrderEntryModel.ACTIVE, addPropertiesToCartEntryData.isActive());
		propertiesMapForOrderEntry.put(AbstractOrderEntryModel.AMENDSTATUS, addPropertiesToCartEntryData.getAmendStatus());
		propertiesMapForOrderEntry.put(AbstractOrderEntryModel.JOURNEYREFERENCENUMBER,
				addPropertiesToCartEntryData.getJourneyRefNumber());
		propertiesMapForOrderEntry.put(AbstractOrderEntryModel.TRAVELORDERENTRYINFO, travelEntryInfoModel);

		propertiesMaps.put(AbstractOrderEntryModel._TYPECODE, propertiesMapForOrderEntry);
		propertiesMaps.put(TravelOrderEntryInfoModel._TYPECODE, propertiesMapForOrderEntryInfo);
		return propertiesMaps;
	}

	public TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	public TransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final TransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}
}
