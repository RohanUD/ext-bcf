/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.enums.TripType;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.extra.product.BCFExtraProducts;
import com.bcf.facades.travel.data.PrePaymentCartValidationResponseData;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfTravelCartFacade extends TravelCartFacade
{
	/**
	 * Returns the list of AddProductToCartRequestData using cart
	 *
	 * @param sessionAmendedCart
	 * @return
	 */
	List<AddProductToCartRequestData> createAddProductToCartRequestForAncillaries(
			AbstractOrderModel sessionAmendedCart, List<String> transportOfferingsCodes);

	/**
	 * disable cart entries during an amendment journey for speciic scenarios e.g. amend destination and departure date
	 *
	 * @param journeyRefNo
	 * @param odRefNo
	 * @param isSingle
	 */
	void disableCartEntriesForRefNo(int journeyRefNo, int odRefNo, boolean isSingle);

	CartRestorationData restoreSavedCart() throws CommerceCartRestorationException;

	void setAmountToPay(BCFMakeBookingResponse response, CartModel cartModel);

	/**
	 * Removes the entries for transport offering codes.
	 *
	 * @param transportOfferingCodes the transport offering codes
	 */
	void removeEntriesForTransportOfferingCodes(List<String> transportOfferingCodes);

	/**
	 * updates originDestinationRefNo for the entries for transport offering codes.
	 *
	 * @param transportOfferingCodes the transport offering codes
	 */

	List<RemoveSailingResponseData> modifyCartEntriesForRefNo(int journeyRefNo, int odRefNo, boolean isSingle)
			throws IntegrationException, CalculationException;//NOSONAR

	void modifyAbstractOrderEntriesForRefNo(AbstractOrderModel orderModel, int journeyRefNo, int odRefNo, boolean isSingle,
			boolean disableEntries);

	void removeCartEntriesForRefNo(int journeyRefNo, int odRefNo, boolean isSingle, boolean removeInboundEntries);

	TripType getTripType(int journeyRefNumber);

	List<AbstractOrderEntryModel> getCartEntriesForRefNo(int journeyRefNo, int odRefNo);

	/**
	 * wrapper to add a product to the cart, it takes an extra cachedPrice so that we can differentiate the cached price
	 * and the E-Booking price
	 *
	 * @param addProductToCartRequestData
	 * @return
	 * @throws CommerceCartModificationException
	 */
	CartModificationData addToCart(AddProductToCartRequestData addProductToCartRequestData)
			throws CommerceCartModificationException, IntegrationException;//NOSONAR


	/**
	 * Fetches the Expiration time for the cart in milliseconds.
	 */
	Long getQuotationExpirationTime();

	/**
	 * returns the selected journey reference number from session if the journey type is replan, otherwise returns the
	 * next journey reference number based on cart
	 *
	 * @return
	 */
	int getNextJourneyRefNumber();

	int getCurrentJourneyRefNumber();

	void cleanUpCartForMinOriginDestinationRefNumber(Integer journeyRefNumber, Integer odRefNum) throws ModelRemovalException;


	boolean hasFareProductEntries();

	/**
	 * save Agent Comment
	 *
	 * @param agentComment
	 * @return Boolean true on success
	 */
	Boolean saveAgentComment(String agentComment);

	/**
	 * get Agent Comment
	 *
	 * @return String
	 */
	String getAgentComment();

	/**
	 * modifies the opposite leg if present during change fare type and replan journey functionalities
	 */
	void modifyTheOppositeLeg(int currentJourneyRefNo, int odRefNo);

	/**
	 * add the properties for {@link CartEntryPropertiesData} to a cart entry
	 *
	 * @param addPropertiesToCartEntryData
	 */
	void addPropertiesToCartEntry(CartEntryPropertiesData addPropertiesToCartEntryData);

	/**
	 * get Max Sailing Allowed count
	 *
	 * @return String
	 */
	int getMaxSailingAllowed();

	PriceData getAmountToPay();

	PriceData getPayAtTerminal();

	List<RemoveSailingResponseData> emptyBasket() throws IntegrationException;

	/**
	 * Call make Booking integration service and on the success of that remove the particular leg of the sailing from the
	 * cart
	 *
	 * @param addProductToCartRequestData
	 * @return List<CartModificationData>
	 * @throws CommerceCartModificationException
	 */
	List<CartModificationData> updateSailing(AddBundleToCartRequestData addBundleToCartRequestData,
			boolean updateLinkedSailing) throws CommerceCartModificationException, IntegrationException;//NOSONAR

	CartModificationData updateCart(AddProductToCartRequestData addProductToCartRequestData, Integer entryNumber, long newQuantity)
			throws CommerceCartModificationException, IntegrationException;//NOSONAR

	/**
	 * returns CartFilterParamsDto
	 *
	 * @param journeyRefNo
	 * @param odRefNo
	 * @param commit
	 * @param routes
	 * @return
	 */
	CartFilterParamsDto initializeCartFilterParams(Integer journeyRefNo, Integer odRefNo, Boolean commit,
			List<String> routes);

	void setPriceQuotationInContext(BCFMakeBookingResponse response);

	boolean updateAmendBookingFeesPrice(String amount, String bookingReferenceNumber);

	void removeAmendBookingFees() throws ModelRemovalException;

	PrePaymentCartValidationResponseData verifyCartForPayment();

	void updateCartEntriesWithCacheKeyOrBookingRef(AbstractOrderModel abstractOrderModel, int journeyRefNum, int odRefNo,
			String cacheKey, String bookingRef);

	List<String> getProductCodeListFromResponse(List<AddProductToCartRequestData> addProductToCartRequestDataList,
			BCFMakeBookingResponse response);


	void updateCartWithAncillaryProducts(AbstractOrderModel sessionAmendedCart, List<String> ancillaryProductCodeList,
			AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException;

	List<CartModificationData> updateCart(BCFMakeBookingResponse response,
			AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException;

	/**
	 * Creates an AddToCartResponseData
	 *
	 * @param valid
	 * @param errorMessage
	 * @param minOriginDestinationRefNumber
	 * @param errorCode
	 * @return the AddToCartResponseData
	 */
	AddToCartResponseData createAddToCartResponse(boolean valid, String errorMessage,
			Integer minOriginDestinationRefNumber, String errorCode);

	List<CartModificationData> createActivityOrderEntryInfo(String code,
			AddActivityToCartData addActivityToCartData) throws CommerceCartModificationException;

	List<CartModificationData> addTransportBundleToCart(AddBundleToCartRequestData addBundleToCartRequestData, boolean recalculate)
			throws CommerceCartModificationException, IntegrationException;

	List<RemoveSailingResponseData> removeDealFromCart(String dealId, Date departureDate)
			throws ModelRemovalException, IntegrationException;

	void evaluateCartForJourneyRefNumber(int journeyRefNum, int odRefNum);

	void addExtraProductToCart(final BCFExtraProducts bcfExtraProducts) throws CommerceCartModificationException;

	List<CartModificationData> createActivityOrderEntryInfoForDeal(final String code,
			final AddActivityToCartData addActivityToCartData) throws CommerceCartModificationException;

	List<DealOrderEntryGroupModel> getDealOrderEntryGroups(final AbstractOrderModel abstractOrder);

	List<RoomGuestData> getRoomDatas(List<AbstractOrderEntryModel> orders);

	List<ActivityGuestData> getUniqueActivityDatas(final List<AbstractOrderEntryModel> orderEntries);

	void updateCartWithDealOrderEntryGroups(List<DealOrderEntryGroupModel> dealOrderEntryGroups, AbstractOrderModel abstractOrder);

	String getPackageAvailabilityStatus();

	OrderEntryData getExistingAncillaryOrderEntry(String productCode, String travelRouteCode, List<String> transportOfferingCodes,
			String travellerCode, int journeyRefNumber, int odRefNumber);

	void attachDealOrderEntryGroup(int journeyRefNumber, List<CartModificationData> cartModificationList);

	void createAndSetAmendableCart(String bookingReference, boolean searchEbooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException;

	boolean getActivityAvailabilityStatus(final String activityProductCode, final String activitySelectedDate,
			final String activitySelectedTime, int passengerQuantity) throws ParseException;

	String getActivityAvailabilityStatus(final String activityProductCode, final Date activitySelectedDate,
			final String activitySelectedTime, final int passengerQuantity);

	void updatePaymentInfosWithCacheKeyOrBookingRef(AbstractOrderModel abstractOrderModel, int journeyRefNum,
			int ODRefNo, String cacheKey, String bookingRef);

	StockData getStockDataForDateAndTime(final String activityProductCode, final Date activityDate,
			final String activitySelectedTime, final int passengerQuantity);

	boolean isAmendingSailing();

	int getOriginalJourneyReference();

	int getOriginalODReference();

	void removePreviousCartEntries(int journeyRefNo, int odRefNo);

	void removePreviousAmountToPayInfos(String cacheKey);

	void removeAmountToPayInfos(final AbstractOrderModel abstractOrderModel);

	OrderEntryData getAncillaryOrderEntry(String productCode, int journeyRefNumber, int odRefNumber);

	/**
	 * Based on the journey type, it predicts if the
	 * cart will be removed.
	 *
	 * @param journeyType
	 */
	boolean shallCartBeRemoved(String journeyType);

	/**
	 * Simply remove the current cart.
	 */
	boolean removeCurrentCart();

	boolean isAmendCurrentCartValid();

	boolean isOnRequestBookingPresent(final AbstractOrderModel abstractOrder);

	String getStockLevelType(final List<AbstractOrderEntryModel> abstractOrderEntries);

	void removeNewCartEntriesForRefNo(final int journeyRefNo, final int odRefNo, final boolean isSingle,
			final boolean removeInboundEntries);

	boolean isCarryingDangerousGoodsWalkOn(int journeyRef, int odRef);

	boolean isCarryingDangerousGoodsWithVehicle(int journeyRef, int odRef);

	void addAncillaryProductToCart(AbstractOrderModel cart, BCFMakeBookingResponse response,
			AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException;

	boolean updateCartWithEbookingDataForOptionBooking(final CartModel cartModel)
			throws IntegrationException, ModelRemovalException, ModelSavingException, CalculationException;

	boolean hasNonOptionSailingInCart();

	public void setTermsAndConditionsCode(final AbstractOrderModel abstractOrder, final BCFMakeBookingResponse response);
}
