/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.entry.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.math.BigDecimal;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.entry.data.OnHoldAccommodationEntryData;
import com.bcf.core.services.order.BCFTravelCommerceCartService;


public class OnHoldAccommodationEntryDataPopulator implements Populator<AbstractOrderEntryModel, OnHoldAccommodationEntryData>
{
	private PriceDataFactory priceDataFactory;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OnHoldAccommodationEntryData target)
			throws ConversionException
	{
		final AccommodationOrderEntryGroupModel group = getBcfTravelCommerceCartService().getAccommodationOrderEntryGroup(source);
		if (Objects.nonNull(group))
		{
			target.setAccommodationOfferingName(group.getAccommodationOffering().getName());
			target.setAccommodationOfferingLocation(
					group.getAccommodationOffering().getLocation().getPointOfService().iterator().next().getAddress().getLine1());
			target.setAccommodationName(group.getAccommodation().getName());
			final String contact = group.getAccommodationOffering().getLocation().getPointOfService().iterator().next().getAddress()
					.getCellphone();
			if (Objects.nonNull(contact))
			{
				target.setAccommodationProviderContact(contact);
			}
		}
		target.setAccommodationCheckInDate(source.getAccommodationOrderEntryInfo().getDates().get(0));
		target.setAccommodationCheckOutDate(
				source.getAccommodationOrderEntryInfo().getDates().stream().reduce((first, second) -> second).get());
		if (source.getEntryGroup() instanceof AccommodationOrderEntryGroupModel)
		{
			target.setAccommodationGuestName(
					((AccommodationOrderEntryGroupModel) source.getEntryGroup()).getContactName());
		}
		else if (source.getEntryGroup() instanceof DealOrderEntryGroupModel)
		{
			target.setAccommodationGuestName(
					((DealOrderEntryGroupModel) source.getEntryGroup()).getAccommodationEntryGroups().iterator().next()
							.getContactName());
		}
		target.setBookingReference(source.getOrder().getCode());
		target.setQuantity(source.getQuantity());
		target.setTotalPrice(getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(source.getTotalPrice()), source.getOrder().getCurrency()));
		target.setOrderEntryNumber(source.getEntryNumber());
		target.setComments(source.getComments());
		target.setApprovalStatus(
				Objects.nonNull(source.getAsmAgentStatus()) ? source.getAsmAgentStatus().getCode() : StringUtils.EMPTY);
		target.setOrderEntryNumber(source.getEntryNumber());

		//just need the type of product
		if (Objects.nonNull(source.getProduct()))
		{
			ProductData productData = target.getProduct();
			if (Objects.isNull(productData))
			{
				productData = new ProductData();
			}
			productData.setProductType(source.getProduct().getItemtype());
			target.setProduct(productData);
		}
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}
}
