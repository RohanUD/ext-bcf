/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.entry.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.math.BigDecimal;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.entry.data.OnHoldActivityEntryData;


public class OnHoldActivityEntryDataPopulator implements Populator<AbstractOrderEntryModel, OnHoldActivityEntryData>
{
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OnHoldActivityEntryData target)
			throws ConversionException
	{
		target.setBookingReference(source.getOrder().getCode());
		target.setActivityDate(source.getActivityOrderEntryInfo().getActivityDate());
		target.setActivityName(source.getProduct().getName());
		target.setQuantity(source.getQuantity());
		target.setActivityGuestType(source.getActivityOrderEntryInfo().getGuestType().getCode());
		target.setTotalPrice(getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(source.getTotalPrice()), source.getOrder().getCurrency()));
		target.setOrderEntryNumber(source.getEntryNumber());
		target.setComments(source.getComments());
		target.setApprovalStatus(Objects.nonNull(source.getAsmAgentStatus()) ? source.getAsmAgentStatus().getCode() : StringUtils.EMPTY);
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

}
