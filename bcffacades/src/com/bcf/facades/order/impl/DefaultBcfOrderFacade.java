/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.impl;

import static com.bcf.core.constants.BcfCoreConstants.ORDER_ALREADY_APPROVED;
import static com.bcf.core.constants.BcfCoreConstants.ORDER_ALREADY_REJECTED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_LOCK_REQUIRED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_UPDATE_EXCEPTION;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_UPDATE_NOT_SUCCESSFUL;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.order.data.OnHoldOrderData;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.strategies.BcfPaymentTransactionStrategy;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.assistedservicefacades.BCFAssistedServiceFacade;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.order.BcfOrderFacade;
import com.bcf.facades.order.UpdatePendingOrderEntryRequestData;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.order.cancel.enums.CancelStatus;
import com.bcf.facades.payment.handler.impl.AgentPaymentHandler;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfOrderFacade extends DefaultOrderFacade implements BcfOrderFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOrderFacade.class);

	private BcfOrderService bcfOrderService;

	private Converter<AbstractOrderModel, OnHoldOrderData> onHoldOrderDataConverter;

	private BcfBookingFacade bcfTravelBookingFacade;

	private BcfCustomerAccountService customerAccountService;

	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	private AgentPaymentHandler agentPaymentHandler;

	private BcfPaymentTransactionStrategy paymentTransactionStrategy;

	private BcfBookingService bcfBookingService;

	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;

	private BCFGlobalReservationFacade globalReservationFacade;

	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	private SearchBookingFacade searchBookingFacade;

	private BcfOrderFacade bcfOrderFacade;

	private BcfBookingFacade bookingFacade;

	private BCFAssistedServiceFacade assistedServiceFacade;

	@Autowired
	private Converter<CartModel, CartData> cartConverter;

	@Override
	public SearchPageData<OnHoldOrderData> getPagedPendingOrdersForInventoryApproval(final PageableData pageableData)
	{
		final SearchPageData<AbstractOrderModel> quoteModelSearchPageData = getBcfOrderService()
				.getPagedPendingOrdersForInventoryApproval(pageableData);
		final SearchPageData<OnHoldOrderData> result = new SearchPageData<OnHoldOrderData>();
		result.setPagination(quoteModelSearchPageData.getPagination());
		result.setSorts(quoteModelSearchPageData.getSorts());
		if (CollectionUtils.isNotEmpty(quoteModelSearchPageData.getResults()))
		{
			final List<OnHoldOrderData> onHoldOrderDatas = new ArrayList<>();

			for (final AbstractOrderModel order : quoteModelSearchPageData.getResults())
			{
				try
				{
					onHoldOrderDatas.add(getOnHoldOrderDataConverter().convert(order));
				}
				catch (final Exception ex)
				{
					LOG.error("exception occured while converting on hold order:" + order.getCode(), ex);
				}
			}
			result.setResults(onHoldOrderDatas);
		}

		return result;
	}



	@Override
	public OnHoldOrderData getOrderForCode(final String code)
	{
		final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		final OrderModel orderModel = getCustomerAccountService().getOrderForCode(code, baseStoreModel);
		if (orderModel == null)
		{
			throw new UnknownIdentifierException("Order with orderGUID " + code + " not found in current BaseStore");
		}
		return getOnHoldOrderDataConverter().convert(orderModel);
	}

	@Override
	public boolean updateOrderEntry(final UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequestData)
	{
		return bcfOrderService
				.updateOrderEntry(updatePendingOrderEntryRequestData, assistedServiceFacade.getAsmSession().getAgent().getUid());

	}

	@Override
	public String lockOrder(final String orderCode) throws BcfOrderUpdateException
	{
		final OrderModel order = getBcfTravelBookingFacade().getOrderFromBookingReferenceCode(orderCode);
		if (Objects.equals(OrderStatus.APPROVED_BY_MERCHANT, order.getStatus()))
		{
			throw new BcfOrderUpdateException(ORDER_ALREADY_APPROVED);
		}
		if (Objects.equals(OrderStatus.REJECTED_BY_MERCHANT, order.getStatus()))
		{
			throw new BcfOrderUpdateException(ORDER_ALREADY_REJECTED);
		}
		return getBcfOrderService().lockOrder(order);
	}

	@Override
	public String lockOrderByAgent(final String orderCode) throws BcfOrderUpdateException
	{
		final OrderModel order = getBcfTravelBookingFacade().getOrderFromBookingReferenceCode(orderCode);
		return getBcfOrderService().lockOrderAndGetAgentName(order);
	}

	@Override
	public String unlockOrder(final String orderCode)
	{
		final OrderModel order = getBcfTravelBookingFacade().getOrderFromBookingReferenceCode(orderCode);
		return getBcfOrderService().unlockOrder(order);
	}

	@Override
	public String rejectOrder(final String orderCode, final String comments) throws BcfOrderUpdateException
	{

		final String result;
		final OrderModel order = getBcfTravelBookingFacade().getOrderFromBookingReferenceCode(orderCode);

		if (!Objects.equals(OrderStatus.LOCKED, order.getStatus()))
		{
			throw new BcfOrderUpdateException(PENDING_ORDERS_LOCK_REQUIRED);
		}
		final Boolean isTransportBookingCancelled = cancelTransportBooking(order);
		if (isTransportBookingCancelled)
		{
			result = getBcfOrderService().rejectOrder(order, comments, assistedServiceFacade.getAsmSession().getAgent().getUid());
			getBcfBookingService().deActivateVacationEntriesAndReleaseStock(order, null);
			return result;
		}
		throw new BcfOrderUpdateException(PENDING_ORDERS_UPDATE_NOT_SUCCESSFUL);
	}

	protected Boolean cancelTransportBooking(final OrderModel order)
	{
		if (getBcfBookingService().checkIfAnyOrderEntryByType(order, OrderEntryType.TRANSPORT))
		{
			final CancelBookingResponseData cancelBookingResponseData = new CancelBookingResponseData();
			try
			{
				final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
						.cancelBookings(order);
				final List<RemoveSailingResponseData> failureCancelEBookings =
						getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, false);
				if (CollectionUtils.isEmpty(failureCancelEBookings))
				{
					cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_SUCCESS);

					getBcfOrderService().deActivateOrderEntries(order, order.getEntries().stream()
							.filter(entry -> entry.getType() != null && entry.getType().getCode()
									.equals(OrderEntryType.TRANSPORT.getCode()))
							.collect(Collectors.toList()));
					return Boolean.TRUE;

				}
				else
				{
					cancelBookingResponseData.setFailureCancelEBookings(failureCancelEBookings);
					final List<RemoveSailingResponseData> successCancelEBookings =
							getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, true);
					if (CollectionUtils.isEmpty(successCancelEBookings))
					{
						cancelBookingResponseData.setCancelStatus(CancelStatus.COMPLETE_FAIL);
						return Boolean.FALSE;
					}

					final List<AbstractOrderEntryModel> entries = order.getEntries().stream().filter(
							entry -> entry.getType() != null && entry.getType().getCode().equals(OrderEntryType.TRANSPORT.getCode())
									&& successCancelEBookings.stream()
									.anyMatch(successCancelEBooking -> successCancelEBooking.getBookingReference()
											.equals(entry.getBookingReference())))
							.collect(Collectors.toList());
					getBcfOrderService().deActivateOrderEntries(order, entries);
					cancelBookingResponseData.setCancelStatus(CancelStatus.PARTIAL_SUCCESS);
					return Boolean.FALSE;
				}
			}
			catch (final IntegrationException ex)
			{
				LOG.info(ex.getMessage());
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	@Override
	public String acceptOrder(final String orderCode, final String comments) throws BcfOrderUpdateException
	{
		final OrderModel order = getBcfTravelBookingFacade().getOrderFromBookingReferenceCode(orderCode);
		if (!Objects.equals(OrderStatus.LOCKED, order.getStatus()))
		{
			throw new BcfOrderUpdateException(PENDING_ORDERS_LOCK_REQUIRED);
		}
		try
		{
			TransactionDetails transactionDetails = null;
			final PaymentInfoModel paymentInfoModel = order.getPaymentTransactions().get(0).getInfo();

			if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
			{
				final Map<String, String> resultMap = new HashMap<>();
				final double amountToPay = order.getAmountToPay();
				final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) order
						.getPaymentTransactions().get(0).getInfo();
				resultMap.put(BcfFacadesConstants.Payment.TOKEN, creditCardPaymentInfoModel.getToken());
				resultMap.put(BcfFacadesConstants.Payment.TOKEN_TYPE, creditCardPaymentInfoModel.getTokenType());
				resultMap.put(BcfFacadesConstants.Payment.CARD_EXPIRATION,
						creditCardPaymentInfoModel.getValidToMonth() + "/" + creditCardPaymentInfoModel.getValidToYear());
				resultMap.put(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
				resultMap.put(BookingJourneyType._TYPECODE, order.getBookingJourneyType().getCode());
				transactionDetails = agentPaymentHandler.handlePayment(resultMap, amountToPay);

				final CustomerModel customerModel = (CustomerModel) order.getUser();

				final boolean isTransactionCreated = getPaymentTransactionStrategy()
						.createPaymentTransactions(customerModel, transactionDetails, PaymentTransactionType.PURCHASE,
								BCFPaymentMethodType.CARD, order, resultMap,
								null, false);
				transactionDetails.setIsSuccessful(transactionDetails.getIsSuccessful() && isTransactionCreated);
			}

			if (!(paymentInfoModel instanceof CreditCardPaymentInfoModel) || (transactionDetails != null && transactionDetails
					.getIsSuccessful()))
			{
				return getBcfOrderService().acceptOrder(order, comments, assistedServiceFacade.getAsmSession().getAgent().getUid());
			}
		}
		catch (final IntegrationException ex)
		{
			LOG.info(ex.getMessage());
			throw new BcfOrderUpdateException(PENDING_ORDERS_UPDATE_EXCEPTION);
		}
		throw new BcfOrderUpdateException(PENDING_ORDERS_UPDATE_NOT_SUCCESSFUL);
	}

	@Override
	public List<BcfGlobalReservationData> findVacationsForCurrentUser(final boolean isUpcoming)
	{

		final List<OrderModel> filteredOrders = getFilteredOrdersForCurrentCustomer(isUpcoming);
		final List<BcfGlobalReservationData> globalReservationDataList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(filteredOrders))
		{
			filteredOrders.stream().forEach(order -> {
				globalReservationDataList.add(getGlobalReservationFacade().getbcfGlobalReservationDataList(order));
			});
		}

		try
		{
			final ReservationDataList reservationDataList =
					searchBookingFacade.getVacationBookingsFromEBooking(bcfOrderFacade.findBookingRefForCurrentUser(true), null);
			return updateLatestVacationBookingDetails(reservationDataList, globalReservationDataList);
		}
		catch (final IntegrationException e)
		{
			LOG.error(e);
		}

		return globalReservationDataList;
	}

	private List<BcfGlobalReservationData> updateLatestVacationBookingDetails(final ReservationDataList reservationDataList,
			final List<BcfGlobalReservationData> globalReservationDataList)
	{

		for (final BcfGlobalReservationData bcfGlobalReservationData : globalReservationDataList)
		{

			final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bcfGlobalReservationData.getCode());
			final List<String> bookingRefs = findBookingRefForOrder(order);

			if (CollectionUtils.isNotEmpty(bookingRefs))
			{
				for (final ReservationData data : reservationDataList.getReservationDatas())
				{
					for (final ReservationItemData itemData : data.getReservationItems())
					{
						if (bookingRefs.contains(itemData.getBcfBookingReference())
								&& bcfGlobalReservationData.getPackageReservations() != null)
						{
							for (final GlobalTravelReservationData globalTravelReservationData : bcfGlobalReservationData
									.getPackageReservations()
									.getPackageReservationDatas())
							{
								globalTravelReservationData.setReservationData(globalTravelReservationData.getReservationData());
							}
						}
					}
				}
			}
		}
		return globalReservationDataList;
	}

	private List<OrderModel> getFilteredOrdersForCurrentCustomer(final boolean isUpcoming)
	{

		final UserModel user = getUserService().getCurrentUser();
		final List<OrderModel> orders = bcfOrderService.findVacationsForCurrentUser(user);
		final List<OrderModel> filteredOrders = new ArrayList<>();

		if (isUpcoming == true)
		{
			StreamUtil.safeStream(orders).forEach(order -> {
				final Date endingTravelDate = getEndingTravelDate(order.getEntries());
				if (endingTravelDate.after(new Date()) || endingTravelDate.equals(new Date()))
				{
					filteredOrders.add(order);
				}
			});
		}
		else
		{
			StreamUtil.safeStream(orders).forEach(order -> {
				final Date endingTravelDate = getEndingTravelDate(order.getEntries());
				if (endingTravelDate.before(new Date()))
				{
					filteredOrders.add(order);
				}
			});
		}

		final List<OrderModel> notOptionalBookingOrders =
				StreamUtil.safeStream(filteredOrders).filter(o -> !OptionBookingUtil.isOptionBooking(o)).collect(Collectors.toList());
		return notOptionalBookingOrders;
	}

	@Override
	public List<String> findBookingRefForCurrentUser(final boolean isUpcoming)
	{

		final List<OrderModel> filteredOrders = getFilteredOrdersForCurrentCustomer(isUpcoming);
		final List<String> bookingReferences = new ArrayList<>();
		for (final OrderModel order : filteredOrders)
		{
			for (final AbstractOrderEntryModel orderEntry : order.getEntries())
			{
				if (orderEntry.getType().equals(OrderEntryType.TRANSPORT))
				{
					bookingReferences.add(orderEntry.getBookingReference());
				}
			}
		}
		return bookingReferences.stream().distinct().collect(Collectors.toList());
	}

	@Override
	public List<String> findBookingRefForOrder(final AbstractOrderModel order)
	{

		return order.getEntries().stream()
				.filter(orderEntry -> orderEntry.getActive() && orderEntry.getType().equals(OrderEntryType.TRANSPORT) && StringUtils
						.isNotEmpty(orderEntry.getBookingReference())).map(AbstractOrderEntryModel::getBookingReference).distinct()
				.collect(Collectors.toList());
	}

	private Date getEndingTravelDate(final List<AbstractOrderEntryModel> entries)
	{

		final List<AbstractOrderEntryModel> transportEntries = entries.stream().filter(
				entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
						&& entry.getTravelOrderEntryInfo() != null && !entry
						.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getDefault()).collect(Collectors.toList());

		final Date firstTransportDate =
				StreamUtil.safeStream(transportEntries)
						.map(entry -> entry.getTravelOrderEntryInfo().getTransportOfferings().iterator().next().getArrivalTime())
						.min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> accommodationEntries =
				entries.stream().filter(entry -> entry.getActive() && OrderEntryType.ACCOMMODATION.equals(entry.getType()))
						.collect(Collectors.toList());

		final Date firstAccommodationDate =
				getAccommodationOrderEntryGroup(accommodationEntries).stream().filter(Objects::nonNull)
						.map(entryGroup -> entryGroup.getEndingDate())
						.min(Date::compareTo).orElse(null);

		final List<AbstractOrderEntryModel> activityEntries =
				entries.stream().filter(entry -> entry.getActive() && OrderEntryType.ACTIVITY.equals(entry.getType()))
						.collect(Collectors.toList());

		final Date firstActivityDate =
				StreamUtil.safeStream(activityEntries).map(entry -> entry.getActivityOrderEntryInfo().getActivityDate())
						.min(Date::compareTo).orElse(null);

		return Stream.of(firstTransportDate, firstAccommodationDate, firstActivityDate).filter(Objects::nonNull)
				.max(Date::compareTo).orElse(null);

	}

	private List<AccommodationOrderEntryGroupModel> getAccommodationOrderEntryGroup(final List<AbstractOrderEntryModel> entries)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new ArrayList<>();

		for (final AbstractOrderEntryModel entry : entries)
		{

			accommodationOrderEntryGroupModels.add(bcfTravelCommerceCartService.getAccommodationOrderEntryGroup(entry));
		}

		return accommodationOrderEntryGroupModels;
	}

	@Override
	public Boolean isWaitlistedOrder(final AbstractOrderModel orderModel)
	{
		Boolean isWaitlistedOrder = Boolean.FALSE;
		final Collection<AmountToPayInfoModel> amountToPayInfoModelList = orderModel.getAmountToPayInfos();
		if (CollectionUtils.isNotEmpty(amountToPayInfoModelList))
		{
			isWaitlistedOrder = amountToPayInfoModelList.stream()
					.filter(amountToPayInfoModel -> Objects.nonNull(amountToPayInfoModel))
					.filter(amountToPayInfoModel -> BooleanUtils.isTrue(amountToPayInfoModel.getIsWaitListed())).findAny().isPresent();
		}
		return isWaitlistedOrder;
	}

	@Override
	public Map<String, Collection<String>> getEntryMarkers(final AbstractOrderModel order)
	{
		final Map<String, Collection<String>> entryProductMarkers = new HashMap<>();
		for (final AbstractOrderEntryModel entryModel : order.getEntries())
		{
			final List<String> entryMarkerList = new ArrayList<>();
			if (BooleanUtils.isTrue(entryModel.isModifiedFromEbooking()))
			{
				entryMarkerList.add("ModifiedFromEbooking");
			}
			if (BooleanUtils.isTrue(entryModel.isScheduleModified()))
			{
				entryMarkerList.add("ScheduleModified");
			}
			if (BooleanUtils.isTrue(entryModel.isVesselModified()))
			{
				entryMarkerList.add("VesselModified");
			}
			final String bookingReference = entryModel.getBookingReference();
			if (StringUtils.isNotBlank(bookingReference) && CollectionUtils.isNotEmpty(entryMarkerList))
			{
				if (entryProductMarkers.containsKey(bookingReference))
				{
					Collection<String> existingMarkerList = entryProductMarkers.get(bookingReference);
					existingMarkerList = CollectionUtils.union(existingMarkerList, entryMarkerList);
					entryProductMarkers.put(bookingReference, existingMarkerList);
				}
				else
				{
					entryProductMarkers.put(bookingReference, entryMarkerList);
				}
			}
		}
		return entryProductMarkers;
	}

	@Override
	public OrderModel getOrderByCode(final String orderCode, final String versionId)
	{
		return getBcfOrderService().getOrderByCode(orderCode, versionId);
	}

	@Override
	public List<OrderModel> getOrdersByCode(final String orderCode)
	{
		return getBcfOrderService().getOrdersByCode(orderCode);
	}


	@Override
	public Boolean updateEntryMarkers(final AbstractOrderModel order)
	{
		return bcfOrderService.updateEntryMarkers(order);
	}

	protected BcfOrderService getBcfOrderService()
	{
		return bcfOrderService;
	}

	@Required
	public void setBcfOrderService(final BcfOrderService bcfOrderService)
	{
		this.bcfOrderService = bcfOrderService;
	}

	protected Converter<AbstractOrderModel, OnHoldOrderData> getOnHoldOrderDataConverter()
	{
		return onHoldOrderDataConverter;
	}

	@Required
	public void setOnHoldOrderDataConverter(final Converter<AbstractOrderModel, OnHoldOrderData> onHoldOrderDataConverter)
	{
		this.onHoldOrderDataConverter = onHoldOrderDataConverter;
	}

	protected BcfBookingFacade getBcfTravelBookingFacade()
	{
		return bcfTravelBookingFacade;
	}

	@Required
	public void setBcfTravelBookingFacade(final BcfBookingFacade bcfTravelBookingFacade)
	{
		this.bcfTravelBookingFacade = bcfTravelBookingFacade;
	}

	@Override
	protected BcfCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final BcfCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setAgentPaymentHandler(final AgentPaymentHandler agentPaymentHandler)
	{
		this.agentPaymentHandler = agentPaymentHandler;
	}

	protected BcfPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	@Required
	public void setPaymentTransactionStrategy(final BcfPaymentTransactionStrategy paymentTransactionStrategy)
	{
		this.paymentTransactionStrategy = paymentTransactionStrategy;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	@Required
	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

	public BCFGlobalReservationFacade getGlobalReservationFacade()
	{
		return globalReservationFacade;
	}

	@Required
	public void setGlobalReservationFacade(final BCFGlobalReservationFacade globalReservationFacade)
	{
		this.globalReservationFacade = globalReservationFacade;
	}

	public BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	public SearchBookingFacade getSearchBookingFacade()
	{
		return searchBookingFacade;
	}

	@Required
	public void setSearchBookingFacade(final SearchBookingFacade searchBookingFacade)
	{
		this.searchBookingFacade = searchBookingFacade;
	}

	public BcfOrderFacade getBcfOrderFacade()
	{
		return bcfOrderFacade;
	}

	@Required
	public void setBcfOrderFacade(final BcfOrderFacade bcfOrderFacade)
	{
		this.bcfOrderFacade = bcfOrderFacade;
	}

	public BcfBookingFacade getBookingFacade()
	{
		return bookingFacade;
	}

	@Required
	public void setBookingFacade(final BcfBookingFacade bookingFacade)
	{
		this.bookingFacade = bookingFacade;
	}

	public Converter<CartModel, CartData> getCartConverter()
	{
		return cartConverter;
	}

	public BCFAssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final BCFAssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}
}
