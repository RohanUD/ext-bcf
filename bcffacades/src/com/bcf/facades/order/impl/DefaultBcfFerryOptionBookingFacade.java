/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.RouteType;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.PassengerRequestData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.order.BcfFerryOptionBookingFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfFerryOptionBookingFacade implements BcfFerryOptionBookingFacade
{
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BCFTravellerFacade travellerFacade;
	private ConfigurationService configurationService;

	@Override
	public PlaceOrderResponseData processFerryOptionBooking()
			throws OrderProcessingException, IntegrationException, InvalidCartException
	{
		final CartModel cart = getBcfTravelCheckoutFacade().getSessionCart();
		if (AccountType.FULL_ACCOUNT.equals(((CustomerModel) cart.getUser()).getAccountType()) && cart.isFerryOptionBooking())
		{
			final List<AddBundleToCartRequestData> addBundleToCartRequestDataList = createAddBundleToCartRequestDataList(cart);
			if (CollectionUtils.isNotEmpty(addBundleToCartRequestDataList))
			{
				return getBcfTravelCheckoutFacade().placeFerryOptionBooking(addBundleToCartRequestDataList);
			}
		}
		return null;
	}

	private List<AddBundleToCartRequestData> createAddBundleToCartRequestDataList(final CartModel cart)
	{
		final SalesApplication salesApplication = SalesApplication
				.valueOf(getBcfSalesApplicationResolverFacade().getCurrentSalesChannel());
		final List<AddBundleToCartRequestData> addBundleToCartRequestDataList = new ArrayList<>();
		StreamUtil.safeStream(cart.getEntries()).filter(entry -> entry.getActive()
				&& OrderEntryType.TRANSPORT.equals(entry.getType())
				&& Objects.nonNull(entry.getTravelOrderEntryInfo())
				&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber))
				.forEach((journeyRef, entriesByJourney) -> {
					entriesByJourney.stream().collect(Collectors
							.groupingBy(entryByJourney -> entryByJourney.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
							.forEach((od, entriesByOD) -> {
								final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
								addBundleToCartRequestData.setSelectedJourneyRefNumber(journeyRef);
								addBundleToCartRequestData.setSelectedOdRefNumber(od);
								addBundleToCartRequestData.setSalesApplication(salesApplication);
								final BcfAddBundleToCartData addBundleToCartData = new BcfAddBundleToCartData();
								addBundleToCartData.setOriginDestinationRefNumber(od);
								addBundleToCartData
										.setTravelRouteCode(entriesByOD.get(0).getTravelOrderEntryInfo().getTravelRoute().getCode());
								final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();
								entriesByOD.get(0).getTravelOrderEntryInfo().getTransportOfferings()
										.forEach(transportOfferingModel -> {
											final TransportOfferingData transportOfferingData = new TransportOfferingData();
											transportOfferingData.setDepartureTime(transportOfferingModel.getDepartureTime());
											transportOfferingData.setArrivalTime(transportOfferingModel.getArrivalTime());
											transportOfferingDataList.add(transportOfferingData);
										});
								addBundleToCartData.setTransportOfferingDatas(transportOfferingDataList);
								addBundleToCartRequestData.setAddBundleToCartData(Arrays.asList(addBundleToCartData));
								addBundleToCartRequestData.setHoldTimeInSeconds(
										getFerryOptionBookingExpiryInSeconds(salesApplication, transportOfferingDataList));
								final List<PassengerTypeData> passengerTypeDataList = new ArrayList<>();
								final List<PassengerTypeQuantityData> passengerTypeQuantityDataList = new ArrayList<>();
								final List<TravellerData> travellersData = getTravellerFacade()
										.getTravellersForCartEntries(journeyRef, od);
								final List<PassengerRequestData> paxTravellers = getTravellerFacade()
										.getPassengerTraveller(travellersData.stream().filter(
												travellerData -> travellerData.getTravellerType()
														.equals(TravellerType.PASSENGER.getCode()))
												.collect(Collectors.toList()), passengerTypeDataList);
								if (RouteType.LONG
										.equals(entriesByOD.get(0).getTravelOrderEntryInfo().getTravelRoute().getRouteType()))
								{
									addBundleToCartRequestData.setPassengerTravellers(paxTravellers);
								}
								final List<VehicleTypeData> vehicleTypeDataList = new ArrayList<>();
								final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();
								final Optional<TravellerData> optionalVehTraveller = StreamUtil.safeStream(travellersData)
										.filter(traveller -> traveller.getTravellerType().equals(TravellerType.VEHICLE.getCode()))
										.findFirst();
								if (optionalVehTraveller.isPresent())
								{
									addBundleToCartRequestData
											.setVehicleTraveller(
													getTravellerFacade()
															.getVehicleTraveller(optionalVehTraveller.get(), vehicleTypeDataList));
								}
								getTravellerFacade()
										.populateQuantityData(passengerTypeDataList, vehicleTypeDataList, passengerTypeQuantityDataList,
												vehicleTypeQuantityDataList);
								if (optionalVehTraveller.isPresent())
								{
									final VehicleInformationData vehicleInformationData = (VehicleInformationData) optionalVehTraveller
											.get().getTravellerInfo();
									vehicleTypeQuantityDataList.forEach(vehicle -> {
										vehicle.setLength(vehicleInformationData.getLength());
										vehicle.setHeight(vehicleInformationData.getHeight());
										vehicle.setWidth(vehicleInformationData.getWidth());
										vehicle.setWeight(vehicleInformationData.getWeight());
									});
								}
								addBundleToCartRequestData.setPassengerTypes(passengerTypeQuantityDataList);
								addBundleToCartRequestData.setVehicleTypes(vehicleTypeQuantityDataList);
								addBundleToCartRequestData.setBundleType(entriesByOD.get(0).getBundleTemplate().getType().getCode());
								if (getBcfTravelCartFacade().isAmendmentCart())
								{
									addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.MODIFICATION_JOURNEY);
								}
								addBundleToCartRequestDataList.add(addBundleToCartRequestData);
							});
				});
		return addBundleToCartRequestDataList;
	}

	private int getFerryOptionBookingExpiryInSeconds(final SalesApplication salesApplication,
			final List<TransportOfferingData> transportOfferingDataList)
	{
		final long maxExpiryInSeconds = getConfigurationService().getConfiguration()
				.getInt("optional.booking.max.hold.time.in.hr." + salesApplication.getCode()) * 3600;
		transportOfferingDataList.sort(Comparator.comparing(TransportOfferingData::getDepartureTime));
		final long timeLeftForDepartureInSeconds =
				(transportOfferingDataList.get(0).getDepartureTime().getTime() - (new Date()).getTime()) / 1000;
		return (int) Math.min(maxExpiryInSeconds, timeLeftForDepartureInSeconds);
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	protected BcfTravelCheckoutFacade getBcfTravelCheckoutFacade()
	{
		return bcfTravelCheckoutFacade;
	}

	@Required
	public void setBcfTravelCheckoutFacade(final BcfTravelCheckoutFacade bcfTravelCheckoutFacade)
	{
		this.bcfTravelCheckoutFacade = bcfTravelCheckoutFacade;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	protected BCFTravellerFacade getTravellerFacade()
	{
		return travellerFacade;
	}

	@Required
	public void setTravellerFacade(final BCFTravellerFacade travellerFacade)
	{
		this.travellerFacade = travellerFacade;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
