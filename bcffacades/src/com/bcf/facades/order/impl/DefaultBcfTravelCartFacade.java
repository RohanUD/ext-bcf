/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.configurablebundleservices.constants.ConfigurableBundleServicesConstants;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TravelRestrictionFacade;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.order.impl.DefaultTravelCartFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.enums.TripType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.order.CommerceBundleCartModificationException;
import de.hybris.platform.travelservices.price.data.PriceLevel;
import de.hybris.platform.travelservices.services.BookingService;
import de.hybris.platform.travelservices.services.TransportOfferingService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.ExtrasType;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.enums.GuestType;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.model.ActivitySaleStatusModel;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.service.BcfTravelStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.travelsector.service.TravelSectorService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.ExternalPriceMapData;
import com.bcf.facades.cart.ExternalPriceTaxes;
import com.bcf.facades.cart.ProductForTravellerData;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.ebooking.ConfirmBookingIntegrationFacade;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.extra.product.BCFExtraProductPerJourney;
import com.bcf.facades.extra.product.BCFExtraProducts;
import com.bcf.facades.extra.product.CabinProduct;
import com.bcf.facades.extra.product.ExtraProductData;
import com.bcf.facades.extra.product.ExtraProductPerVessel;
import com.bcf.facades.extra.product.LoungeProduct;
import com.bcf.facades.extra.product.MealProduct;
import com.bcf.facades.ferry.AccessibilitytData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.CartEntryPropertiesData;
import com.bcf.facades.order.strategy.BCFPopulatePropertyMapStrategy;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.strategy.PrePaymentCartValidationHandler;
import com.bcf.facades.reservation.entry.data.BcfOrderEntryData;
import com.bcf.facades.stock.preprocessor.BcfStockPreprocessor;
import com.bcf.facades.travel.data.PrePaymentCartValidationResponseData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.GuestData;
import com.bcf.facades.vacations.RoomGuestData;
import com.bcf.integration.common.data.FareDetail;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.common.data.Tax;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfTravelCartFacade extends DefaultTravelCartFacade implements BcfTravelCartFacade
{
	protected static final Logger LOG = Logger.getLogger(DefaultBcfTravelCartFacade.class);
	/**
	 * This constant value dictates the quantity of the fare product to be added in the cart.
	 */
	protected static final long PRODUCT_QUANTITY = 1;
	private static final String PRICE_NOT_AVAILABLE = "Price not available for the product ";
	private static final String SESSION_CART_PARAMETER_NAME = "cart";
	private Map<String, BCFPopulatePropertyMapStrategy> bcfPopulateCartEntryPropertyStrategyMap;
	private BCFTravelCartService bcfTravelCartService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private ConfirmBookingIntegrationFacade confirmBookingIntegrationFacade;
	private UserService userService;
	private ModelService modelService;
	private PropertySourceFacade propertySourceFacade;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;
	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;
	private TravelSectorService travelSectorService;
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;
	private Converter<AbstractOrderEntryModel, BcfOrderEntryData> bcfOrderEntryConverter;
	private ProductService productService;
	private CalculationService calculationService;
	private BcfBookingService bcfBookingService;
	private BcfBookingFacade bcfTravelBookingFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private Converter<TravellerModel, TravellerData> travellerDataConverter;
	private WarehouseService warehouseService;
	private BcfProductFacade bcfProductFacade;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BCFTransportFacilityFacade bcfTransportFacilityFacade;
	private Map<String, Map<String, List>> salesApplicationJourneyTypeCompatibilityMap;
	private BcfStockPreprocessor bcfStockPreprocessor;

	@Resource(name = "prePaymentCartValidationHandlers")
	private List<PrePaymentCartValidationHandler> prePaymentCartValidationHandlers;

	@Resource(name = "travellerFacade")
	private TravellerFacade travellerFacade;

	@Resource(name = "travelRestrictionFacade")
	private TravelRestrictionFacade travelRestrictionFacade;

	@Resource(name = "transportOfferingService")
	private TransportOfferingService transportOfferingService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingService")
	private BookingService bookingService;

	@Resource(name = "bcfTravelStockService")
	private BcfTravelStockService bcfTravelStockService;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Override
	public OrderEntryData getExistingAncillaryOrderEntry(final String productCode, final String travelRouteCode,
			final List<String> transportOfferingCodes, final String travellerCode, final int journeyRefNumber, final int odRefNumber)
	{
		final List<String> travellerCodes = new ArrayList();
		if (StringUtils.isNotBlank(travellerCode))
		{
			travellerCodes.add(travellerCode);
		}

		final AbstractOrderEntryModel orderEntryModel = getBcfBookingService()
				.getExistingAncillaryOrderEntry(getCartService().getSessionCart(), productCode, travelRouteCode,
						transportOfferingCodes, travellerCodes, journeyRefNumber, odRefNumber);
		return orderEntryModel != null ? getOrderEntryConverter().convert(orderEntryModel) : null;
	}

	@Override
	public OrderEntryData getAncillaryOrderEntry(final String productCode, final int journeyRefNumber, final int odRefNumber)
	{
		final Optional<AbstractOrderEntryModel> existingOrderEntryOptional = getTravelCartService().getSessionCart().getEntries()
				.stream().filter(entry -> entry.getActive() && productCode.equals(entry.getProduct().getCode())
						&& entry.getJourneyReferenceNumber() == journeyRefNumber && Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNumber).findFirst();
		if (existingOrderEntryOptional.isPresent())
		{
			return getOrderEntryConverter().convert(existingOrderEntryOptional.get());
		}
		return null;
	}

	@Override
	public boolean shallCartBeRemoved(final String journeyType)
	{
		boolean compatible = false;
		if (bcfTravelCartService.hasSessionCart()
				&& bcfTravelCartService.getSessionCart().getSalesApplication() != null
				&& bcfTravelCartService.getSessionCart().getBookingJourneyType() != null)
		{
			final CartModel cart = bcfTravelCartService.getSessionCart();
			final Map<String, List> journeyTypeList = salesApplicationJourneyTypeCompatibilityMap
					.get(cart.getSalesApplication().getCode().toUpperCase());

			if (MapUtils.isNotEmpty(journeyTypeList))
			{
				final List<String> compatibleJourneys = journeyTypeList.get(journeyType);
				if (CollectionUtils.isNotEmpty(compatibleJourneys))
				{
					compatible = compatibleJourneys.stream()
							.anyMatch(currentJourney -> currentJourney.equalsIgnoreCase(cart.getBookingJourneyType().getCode()));
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			return !compatible;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean removeCurrentCart()
	{
		try
		{
			if (bcfTravelCartService.hasSessionCart())
			{
				bcfTravelCartService.removeCart(bcfTravelCartService.getSessionCart());
				sessionService.removeAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
			}
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to remove the cart:" + ex);
			return false;
		}

		return true;
	}

	@Override
	public Boolean isAmendmentCart()
	{
		if (!getTravelCartService().hasSessionCart())
		{
			return Boolean.FALSE;
		}
		else
		{
			final CartModel sessionCart = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
			if (Objects.nonNull(sessionCart) && Objects.nonNull(sessionCart.getOriginalOrder()))
			{
				return Boolean.TRUE;
			}
			return sessionCart.getCreatedFromEBooking() != null ? sessionCart.getCreatedFromEBooking() : Boolean.FALSE;
		}
	}

	@Override
	public int getOriginalJourneyReference()
	{
		final Optional<AbstractOrderEntryModel> optionalEntry = StreamUtil
				.safeStream(getTravelCartService().getSessionCart().getEntries()).filter(entry -> StringUtils
						.equalsIgnoreCase(entry.getBookingReference(),
								sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE))).findFirst();
		if (optionalEntry.isPresent())
		{
			return optionalEntry.get().getJourneyReferenceNumber();
		}
		return 0;
	}

	@Override
	public int getOriginalODReference()
	{
		final Optional<AbstractOrderEntryModel> optionalEntry = StreamUtil
				.safeStream(getTravelCartService().getSessionCart().getEntries()).filter(entry -> StringUtils
						.equalsIgnoreCase(entry.getBookingReference(),
								sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE))).findFirst();
		if (optionalEntry.isPresent() && Objects.nonNull(optionalEntry.get().getTravelOrderEntryInfo()))
		{
			return optionalEntry.get().getTravelOrderEntryInfo().getOriginDestinationRefNumber().intValue();
		}
		return 0;
	}

	@Override
	public List<AddProductToCartRequestData> createAddProductToCartRequestForAncillaries(
			final AbstractOrderModel sessionAmendedCart, final List<String> transportOfferingsCodes)
	{
		final List<AddProductToCartRequestData> addProductToCartRequestDatas = new ArrayList<>();
		final List<TransportVehicleModel> transportVehicleList = new ArrayList<>();
		final List<AbstractOrderEntryModel> orderEntries = sessionAmendedCart.getEntries().stream()
				.filter(abstractOrderEntryModel -> BooleanUtils.isTrue(abstractOrderEntryModel.getActive()))
				.collect(Collectors.toList());


		transportOfferingService.getTransportOfferings(transportOfferingsCodes)
				.forEach(transportOfferingModel -> transportVehicleList.add(transportOfferingModel.getTransportVehicle()));

		final List<AbstractOrderEntryModel> entriesToBeUpdated = new ArrayList<>();
		for (final AbstractOrderEntryModel entry : orderEntries)
		{
			if (entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE))
			{
				final ProductModel productToBeAdded = entry.getProduct();
				if (isProductMappedToVehicle(productToBeAdded, transportVehicleList))
				{
					final AddProductToCartRequestData addProductToCartRequestData = new AddProductToCartRequestData();
					addProductToCartRequestData.setProductCode(entry.getProduct().getCode());
					addProductToCartRequestData.setProductType(AncillaryProductModel._TYPECODE);
					addProductToCartRequestData.setQty(entry.getQuantity());
					addProductToCartRequestDatas.add(addProductToCartRequestData);
				}
				else
				{
					entry.setActive(Boolean.FALSE);
					entry.setAmendStatus(AmendStatus.CHANGED);
					entriesToBeUpdated.add(entry);
				}
			}
		}
		getModelService().saveAll(entriesToBeUpdated);
		return addProductToCartRequestDatas;
	}

	/**
	 * checks if a product that is being added to AddProductToCartRequestData for make booking request is present in the
	 * route for which the make booking call is happening
	 *
	 * @param productToBeAdded
	 * @param transportVehicleList
	 * @return
	 */
	protected boolean isProductMappedToVehicle(final ProductModel productToBeAdded,
			final List<TransportVehicleModel> transportVehicleList)
	{
		final List<List<ProductModel>> productsByEachVehicle = new ArrayList<>();
		transportVehicleList
				.forEach(vehicle -> productsByEachVehicle.add(vehicle.getProducts().stream().collect(Collectors.toList())));

		return productsByEachVehicle.stream().allMatch(products -> products.contains(productToBeAdded));
	}

	@Override
	public void disableCartEntriesForRefNo(final int journeyRefNo, final int odRefNo, final boolean isSingle)
	{
		bcfTravelCartService
				.disableAmendedCartEntries(initializeCartFilterParams(journeyRefNo, odRefNo, isSingle, Collections.emptyList()));
	}

	@Override
	public void removePreviousCartEntries(final int journeyRefNo, final int odRefNo)
	{
		if (hasSessionCart())
		{
			final List<AbstractOrderEntryModel> entriesToRemove = new ArrayList<>();
			final CartModel cart = getBcfTravelCartService().getSessionCart();
			final List<AbstractOrderEntryModel> filteredEntries = cart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
							.nonNull(entry.getTravelOrderEntryInfo())
							&& entry.getJourneyReferenceNumber() == journeyRefNo
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNo
							&& AmendStatus.NEW.equals(entry.getAmendStatus()))
					.collect(Collectors.toList());
			filteredEntries.forEach(entry -> {
				entriesToRemove.add(entry);
			});
			if (CollectionUtils.isNotEmpty(entriesToRemove))
			{
				modelService.removeAll(entriesToRemove);
				modelService.refresh(cart);
			}
		}
	}

	@Override
	public void removePreviousAmountToPayInfos(final String cacheKey)
	{
		if (hasSessionCart())
		{
			final List<AmountToPayInfoModel> infosToRemove = new ArrayList<>();
			final CartModel cart = getBcfTravelCartService().getSessionCart();
			final List<AmountToPayInfoModel> filteredInfos = cart.getAmountToPayInfos().stream()
					.filter(amountToPayInfo -> StringUtils.equals(cacheKey, amountToPayInfo.getCacheKey()))
					.collect(Collectors.toList());
			filteredInfos.forEach(entry -> {
				infosToRemove.add(entry);
			});
			if (CollectionUtils.isNotEmpty(infosToRemove))
			{
				modelService.removeAll(infosToRemove);
				modelService.refresh(cart);
			}
		}
	}

	@Override
	public void removeAmountToPayInfos(final AbstractOrderModel abstractOrderModel)
	{
		bcfTravelCartService.removeAmountToPayInfos(abstractOrderModel);
	}

	@Override
	public CartRestorationData restoreSavedCart() throws CommerceCartRestorationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		CartModel cartModel = null;
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();

			parameter.setEnableHooks(true);

			final List<CartModel> cartsForGuidAndSiteAndUser = getCommerceCartService()
					.getCartsForSiteAndUser(getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
			boolean isCartValid = false;
			if (CollectionUtils.isNotEmpty(cartsForGuidAndSiteAndUser))
			{
				final List<CartModel> savedCarts = new ArrayList<>(cartsForGuidAndSiteAndUser);

				if (CollectionUtils.size(savedCarts) > 1)
				{
					final Comparator<AbstractOrderModel> modifiedTimeComparator = (b1, b2) -> b1.getModifiedtime()
							.compareTo(b2.getModifiedtime());
					Collections.sort(savedCarts, modifiedTimeComparator);
					getModelService().removeAll(savedCarts.subList(0, savedCarts.size() - 1));
				}
				cartModel = savedCarts.get(savedCarts.size() - 1);
				isCartValid = validateAndUpdateCartWithEbookingData(cartModel);
				parameter.setCart(isCartValid ? cartModel : null);
			}
		}
		catch (final IntegrationException | ModelRemovalException | ModelSavingException | CalculationException ex)
		{
			LOG.error(ex.getMessage(), ex);
			tx.rollback();
			throw new CommerceCartRestorationException(ex.getMessage());
		}

		if (tx.isRunning())
		{
			tx.commit();
		}
		return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
	}

	@Override
	public PrePaymentCartValidationResponseData verifyCartForPayment()
	{

		PrePaymentCartValidationResponseData response = null;
		for (final PrePaymentCartValidationHandler handler : prePaymentCartValidationHandlers)
		{
			response = handler.validate(getBcfTravelCartService().getOrCreateSessionCart());
		}
		return response;
	}

	protected boolean validateAndUpdateCartWithEbookingData(final CartModel cartModel)
			throws IntegrationException, ModelRemovalException, ModelSavingException, CalculationException//NOSONAR
	{
		if (Objects.isNull(cartModel.getQuoteExpirationDate()))
		{
			return Boolean.FALSE;
		}
		if (new Date().after(cartModel.getQuoteExpirationDate()))
		{
			final List<TravelRouteModel> removeSailingsList = new ArrayList<>();
			final int expirationTime = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.EBOOKING_MAX_SAILING_DEPARTURE_EXPIRATION_SECONDS));
			final Date expiryDate = BCFDateUtils.addSeconds(new Date(), expirationTime);
			final Map<Integer, List<AbstractOrderEntryModel>> jourenytEntriesMap = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			final boolean isRefresh = removeEntries(jourenytEntriesMap, expiryDate, removeSailingsList, cartModel);

			if (CollectionUtils.isNotEmpty(removeSailingsList))
			{
				getSessionService().setAttribute(BcfCoreConstants.SAILINGS_REMOVED_FROM_CART, removeSailingsList);
			}

			if (isRefresh)
			{
				getModelService().refresh(cartModel);
			}

			if (cartModel.getEntries().stream().filter(AbstractOrderEntryModel::getActive)
					.allMatch(entry -> entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)))
			{
				getBcfTravelCartService().removeCart(cartModel);
				return false;
			}

			getMakeBookingIntegrationFacade().updateCartWithMakeBookingResponse(cartModel);
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
			getTravelCommerceCartService().recalculateCart(cartModel);

		}

		return true;
	}

	private boolean removeEntries(final Map<Integer, List<AbstractOrderEntryModel>> jourenytEntriesMap, final Date expiryDate,
			final List<TravelRouteModel> removeSailingsList, final CartModel cartModel)
	{
		for (final Entry<Integer, List<AbstractOrderEntryModel>> entrySet : jourenytEntriesMap.entrySet())
		{
			final List<AbstractOrderEntryModel> entries = jourenytEntriesMap.get(entrySet.getKey());


			final Map<Integer, List<AbstractOrderEntryModel>> removeEntries = entries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTransportOfferings())
							&& entry.getTravelOrderEntryInfo()
							.getTransportOfferings().iterator().next().getDepartureTime()
							.before(expiryDate))
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
			removeSailingsList.addAll(removeEntries.values().stream().flatMap(List<AbstractOrderEntryModel>::stream)
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getTravelRoute())).keySet().stream()
					.collect(Collectors.toList()));

			if (Objects.equals(TripType.RETURN, getTripType(entries)) && removeEntries.keySet().size() == 2)
			{
				getBcfTravelCommerceCartService().removeAllTravelOrderEntries(entries);
				return true;
			}
			else if (MapUtils.isNotEmpty(removeEntries))
			{

				modifyAbstractOrderEntriesForRefNo(cartModel, entrySet.getKey(), removeEntries.keySet().iterator().next(),
						Objects.equals(TripType.SINGLE, getTripType(entries)), false);
				return true;
			}
		}
		return false;
	}

	/**
	 * updates the cart with option booking entries. basically it removes all the previous transport  bookings and add new transport booking with option booking expiration time
	 *
	 * @param cartModel cart
	 * @return
	 * @throws IntegrationException
	 * @throws CalculationException
	 */
	@Override
	public boolean updateCartWithEbookingDataForOptionBooking(final CartModel cartModel)
			throws IntegrationException, CalculationException
	{
		getMakeBookingIntegrationFacade().updateCartWithMakeBookingResponse(cartModel);
		modelService.save(cartModel);
		getTravelCommerceCartService().recalculateCart(cartModel);
		getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
		return true;
	}

	/**
	 * Call make Booking integration service and on the success of that remove the particular leg of the sailing from the
	 * cart
	 *
	 * @param addBundleToCartRequestData
	 * @return List<CartModificationData>
	 * @throws CommerceCartModificationException
	 */
	@Override
	public List<CartModificationData> updateSailing(final AddBundleToCartRequestData addBundleToCartRequestData,
			final boolean updateLinkedSailing) throws CommerceCartModificationException, IntegrationException//NOSONAR
	{
		BCFMakeBookingResponse response = null;
		response = getMakeBookingIntegrationFacade().getMakeBookingResponse(addBundleToCartRequestData);
		if (Objects.nonNull(response))
		{
			final BcfAddBundleToCartData addBundleToCart = (BcfAddBundleToCartData) addBundleToCartRequestData
					.getAddBundleToCartData().get(0);
			// remove sailing from cart
			removeCartEntriesForRefNo(addBundleToCart.getJourneyRefNumber(), addBundleToCart.getOriginDestinationRefNumber(), true,
					false);
			if (!updateLinkedSailing)
			{
				addBundleToCart.setOriginDestinationRefNumber(TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER);
			}
		}

		return updateCart(response, addBundleToCartRequestData);
	}

	@Override
	public List<String> getProductCodeListFromResponse(final List<AddProductToCartRequestData> addProductToCartRequestDataList,
			final BCFMakeBookingResponse response)
	{
		final List<String> productFares = new ArrayList<>();
		response.getItinerary().getSailing().getLine().forEach(sailingLine -> {
			final List<ProductFare> productFareList = new ArrayList<>();
			sailingLine.getSailingPrices()
					.forEach(sailingPrice -> {
						productFareList.addAll(sailingPrice.getProductFares().stream()
								.filter(productFare -> ("AMENITY").equals(productFare.getProduct().getProductCategory()))
								.collect(Collectors.toList()));
					});

			final List<String> productCodeList = addProductToCartRequestDataList.stream().filter(
					addProductToCartRequestData -> AncillaryProductModel._TYPECODE
							.equals(addProductToCartRequestData.getProductType()))
					.map(AddProductToCartRequestData::getProductCode).collect(Collectors.toList());
			final List<ProductFare> removeProductFare = productFareList.stream()
					.filter(productFare -> !productCodeList.contains(productFare.getProduct().getProductId()))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(removeProductFare))
			{
				productFareList.removeAll(removeProductFare);
			}
			productFares.addAll(
					productFareList.stream().map(productFare -> productFare.getProduct().getProductId()).collect(Collectors.toList()));
		});
		return productFares;
	}

	@Override
	public void updateCartWithAncillaryProducts(final AbstractOrderModel sessionAmendedCart,
			final List<String> ancillaryProductCodeList,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException
	{
		final AddBundleToCartData addBundleToCartData = addBundleToCartRequestData.getAddBundleToCartData().get(0);
		final List<AbstractOrderEntryModel> orderEntries = sessionAmendedCart.getEntries().stream()
				.filter(entry -> entry.getActive()
						&& entry.getJourneyReferenceNumber() == (((BcfAddBundleToCartData) addBundleToCartData).getJourneyRefNumber())
						&& entry.getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber() == (addBundleToCartData.getOriginDestinationRefNumber())
						&& entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE))
				.collect(Collectors.toList());

		final List<ProductForTravellerData> productForTravellerDataList = new ArrayList<>();


		for (final AbstractOrderEntryModel entry : orderEntries)
		{
			entry.setActive(Boolean.FALSE);
			entry.setAmendStatus(AmendStatus.CHANGED);


			if (ancillaryProductCodeList.contains(entry.getProduct().getCode()))
			{
				final ProductForTravellerData productForTravellerData = new ProductForTravellerData();
				productForTravellerData.setProduct(entry.getProduct().getCode());
				productForTravellerData.setQuantity(entry.getQuantity());
				productForTravellerData.setTraveller(entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get());
				productForTravellerDataList.add(productForTravellerData);

			}


		}

		getModelService().saveAll();
		addAncillaryToCart(productForTravellerDataList, addBundleToCartData);
	}

	protected void addAncillaryToCart(final List<ProductForTravellerData> productForTravellerDataList,
			final AddBundleToCartData addBundleToCartData) throws CommerceCartModificationException

	{


		for (final ProductForTravellerData productForTravellerData : productForTravellerDataList)
		{

			final CartModificationData cartModificationData = super.addToCart(productForTravellerData.getProduct(),
					productForTravellerData.getQuantity());

			if (cartModificationData.getQuantityAdded() >= PRODUCT_QUANTITY)
			{
				final TravellerModel travellerModel = productForTravellerData.getTraveller();
				final String travelRouteCode = StringUtils.isNotBlank(addBundleToCartData.getTravelRouteCode())
						? addBundleToCartData.getTravelRouteCode()
						: null;

				final String productCode = cartModificationData.getEntry().getProduct().getCode();
				setOrderEntryType(de.hybris.platform.travelservices.enums.OrderEntryType.TRANSPORT,
						cartModificationData.getEntry().getEntryNumber());


				final CartEntryPropertiesData cartEntryPropertiesData = new CartEntryPropertiesData();
				cartEntryPropertiesData
						.setTravellerCode(Objects.nonNull(travellerModel.getLabel()) ? travellerModel.getLabel() : null);
				cartEntryPropertiesData.setOriginDestinationRefNo(addBundleToCartData.getOriginDestinationRefNumber());
				cartEntryPropertiesData.setTravelRoute(travelRouteCode);
				cartEntryPropertiesData.setTransportOfferingCodes(addBundleToCartData.getTransportOfferings());
				cartEntryPropertiesData.setActive(Boolean.TRUE);
				cartEntryPropertiesData.setAmendStatus(AmendStatus.NEW);
				cartEntryPropertiesData.setCartModification(cartModificationData);
				cartEntryPropertiesData.setJourneyRefNumber(((BcfAddBundleToCartData) addBundleToCartData).getJourneyRefNumber());

				final String addToCartCriteria = travelRestrictionFacade.getAddToCartCriteria(productCode);

				cartEntryPropertiesData.setProductCode(productCode);
				cartEntryPropertiesData.setAddToCartCriteria(addToCartCriteria);
				cartEntryPropertiesData.setEntryNumber(cartEntryPropertiesData.getCartModification().getEntry().getEntryNumber());


				addPropertiesToAncillaryCartEntry(cartEntryPropertiesData, addBundleToCartData, travellerModel);

			}

		}
	}

	@Override
	public List<CartModificationData> updateCart(final BCFMakeBookingResponse response,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		if (Objects.nonNull(response))
		{
			setPriceQuotationInContext(response);
			setQuotationExpirationDate(cartModel);
			setAmountToPay(response, cartModel);
			setTermsAndConditionsCode(cartModel, response);
			setFerryOptionBooking(cartModel, addBundleToCartRequestData);
		}
		Map<String, PriceLevel> productPriceLevelMap = null;
		final List<AddBundleToCartData> addBundleToCartDataList = addBundleToCartRequestData.getAddBundleToCartData();
		try
		{
			productPriceLevelMap = getProductPriceLevel(addBundleToCartDataList, FareProductType.PASSENGER);
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.warn("Prices not found for the bundle products: " + e.getMessage());
			throw new CommerceBundleCartModificationException(e.getMessage(), e);
		}
		final int bundleNo = -1;
		final List<CartModificationData> cartModifications = new ArrayList<>();
		final String currentCartCode = getCurrentCartCode();
		final List<TravellerData> travellerDataList = addPassengersToCart(currentCartCode, productPriceLevelMap,
				addBundleToCartRequestData, addBundleToCartDataList, bundleNo,
				cartModifications);

		if (Objects.nonNull(addBundleToCartRequestData.getVehicleTypes()) && addBundleToCartRequestData.getVehicleTypes().stream()
				.anyMatch(vehicle -> Objects.nonNull(vehicle.getVehicleType()) && StringUtils
						.isNotBlank(vehicle.getVehicleType().getCode())))
		{
			productPriceLevelMap = getProductPriceLevel(addBundleToCartDataList, FareProductType.VEHICLE);
			addVehiclesToCart(currentCartCode, productPriceLevelMap, addBundleToCartRequestData, addBundleToCartDataList, bundleNo,
					cartModifications);
		}
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getLargeItems()))
		{
			productPriceLevelMap = getLargeItemsProductPriceLevel(addBundleToCartDataList.stream().findFirst().get(),
					addBundleToCartRequestData.getLargeItems());
			addLargeItemsToCart(productPriceLevelMap, addBundleToCartRequestData, addBundleToCartDataList, bundleNo,
					cartModifications);
		}
		addPerLegBundleProductToCart(addBundleToCartDataList, bundleNo, cartModifications);

		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getAccessibilityDatas()))
		{
			productPriceLevelMap = getAmenityProductPriceLevel(addBundleToCartDataList.stream().findFirst().get(),
					addBundleToCartRequestData.getAccessibilityDatas());
			addAmenitiesToCart(productPriceLevelMap, addBundleToCartRequestData, addBundleToCartDataList, 0,
					cartModifications, travellerDataList);
		}
		updateTravelOrderEntryInfoForDifferentPAXorVEH(cartModel, addBundleToCartRequestData);
		recalculateCart();

		cartModifications
				.forEach(modification -> setOrderEntryType(OrderEntryType.TRANSPORT, modification.getEntry().getEntryNumber()));
		return cartModifications;
	}

	@Override
	public void addAncillaryProductToCart(final AbstractOrderModel cart, final BCFMakeBookingResponse response,
			final AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException
	{
		final AddBundleToCartData addBundleToCartData = addBundleToCartRequestData.getAddBundleToCartData().get(0);
		final AbstractOrderEntryModel anyEntry = cart.getEntries().stream().filter(entry -> entry.getActive()
				&& entry.getJourneyReferenceNumber() == (((BcfAddBundleToCartData) addBundleToCartData).getJourneyRefNumber())
				&& entry.getTravelOrderEntryInfo() != null
				&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == (addBundleToCartData
				.getOriginDestinationRefNumber())
				&& entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)).findFirst().get();

		final List<ProductForTravellerData> productForTravellerDataList = new ArrayList<>();

		final List<String> accessibilityCodes = BCFPropertiesUtils.convertToList(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
		for (final SailingLine line : response.getItinerary().getSailing().getLine())
		{
			for (final SailingPrices prices : line.getSailingPrices())
			{
				for (final ProductFare productFare : prices.getProductFares())
				{
					if (BcfCoreConstants.CATEGORY_AMENITY.equalsIgnoreCase(productFare.getProduct().getProductCategory())
							&& !accessibilityCodes.contains(productFare.getProduct().getProductId()))
					{
						final ProductForTravellerData productForTravellerData = new ProductForTravellerData();
						productForTravellerData.setProduct(productFare.getProduct().getProductId());
						productForTravellerData.setQuantity(productFare.getProduct().getProductNumber());
						productForTravellerData
								.setTraveller(anyEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get());
						productForTravellerDataList.add(productForTravellerData);
					}
				}
			}
		}
		addAncillaryToCart(productForTravellerDataList, addBundleToCartData);
	}

	@Override
	public List<CartModificationData> addTransportBundleToCart(final AddBundleToCartRequestData addBundleToCartRequestData,
			final boolean recalculate)
			throws CommerceCartModificationException
	{
		final List<AddBundleToCartData> addBundleToCartDataList = addBundleToCartRequestData.getAddBundleToCartData();

		Map<String, PriceLevel> productPriceLevelMap;
		try
		{
			productPriceLevelMap = getProductPriceLevels(addBundleToCartDataList);
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.warn("Prices not found for the bundle products: " + e.getMessage());
			throw new CommerceBundleCartModificationException(e.getMessage(), e);
		}

		final List<CartModificationData> totalCartModifications = new ArrayList<>();
		List<CartModificationData> bundleModifications = new ArrayList<>();
		final String currentCartCode = getCurrentCartCode();
		final int bundleNo = ConfigurableBundleServicesConstants.NEW_BUNDLE;

		if (isAmendmentCart())
		{
			disableCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
					addBundleToCartRequestData.getSelectedOdRefNumber(), true);
			removeNewCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
					addBundleToCartRequestData.getSelectedOdRefNumber(), Boolean.TRUE, Boolean.FALSE);
		}
		else
		{
			removeCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
					addBundleToCartRequestData.getSelectedOdRefNumber(), true, false);
		}

		final List<TravellerData> travellerDataList = addPassengersToCart(currentCartCode, productPriceLevelMap,
				addBundleToCartRequestData, addBundleToCartDataList,
				bundleNo, bundleModifications);

		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getAccessibilityDatas()))
		{
			productPriceLevelMap = getAmenityProductPriceLevel(addBundleToCartDataList.stream().findFirst().get(),
					addBundleToCartRequestData.getAccessibilityDatas());
			addAmenitiesToCart(productPriceLevelMap, addBundleToCartRequestData, addBundleToCartDataList, bundleNo,
					bundleModifications, travellerDataList);
		}
		if (Objects.nonNull(addBundleToCartRequestData.getVehicleTypes()) && addBundleToCartRequestData.getVehicleTypes().stream()
				.anyMatch(vehicle -> Objects.nonNull(vehicle.getVehicleType()) && StringUtils
						.isNotBlank(vehicle.getVehicleType().getCode())))
		{
			productPriceLevelMap = getProductPriceLevel(addBundleToCartDataList, FareProductType.VEHICLE);
			addVehiclesToCart(currentCartCode, productPriceLevelMap, addBundleToCartRequestData, addBundleToCartDataList, bundleNo,
					bundleModifications);
		}

		totalCartModifications.addAll(bundleModifications);
		bundleModifications = addOtherChargesToCart(addBundleToCartDataList, addBundleToCartRequestData.getOtherChargeDatas());
		if (CollectionUtils.isNotEmpty(bundleModifications))
		{
			bundleModifications
					.forEach(modification -> setOrderEntryType(OrderEntryType.TRANSPORT, modification.getEntry().getEntryNumber()));
			totalCartModifications.addAll(bundleModifications);
		}
		if (recalculate)
		{
			recalculateCart();
		}


		return totalCartModifications;
	}

	protected List<CartModificationData> addOtherChargesToCart(final List<AddBundleToCartData> addBundleToCartDataList,
			final List<OtherChargeData> otherChargeDatas) throws CommerceCartModificationException
	{
		final List<CartModificationData> modifications = new ArrayList<>();
		for (final AddBundleToCartData addBundleToCartData : addBundleToCartDataList)
		{
			if (CollectionUtils.isNotEmpty(otherChargeDatas) && addBundleToCartData instanceof BcfAddBundleToCartData)
			{
				final BcfAddBundleToCartData bcfAddBundleToCartData = (BcfAddBundleToCartData) addBundleToCartData;
				modifications
						.addAll(getBcfTravelCartFacadeHelper().addReservationFeeToCart(bcfAddBundleToCartData.getJourneyRefNumber(),
								addBundleToCartData.getOriginDestinationRefNumber(), otherChargeDatas));
			}
		}
		return modifications;
	}

	protected Map<String, PriceLevel> getProductPriceLevel(final List<AddBundleToCartData> addBundleToCartDataList, final
	FareProductType fareProductType) throws CommerceCartModificationException//NOSONAR
	{
		final Map<String, PriceLevel> productPriceLevelMap = new HashMap<>();
		final Iterator fareBundleDataItr = addBundleToCartDataList.iterator();

		while (fareBundleDataItr.hasNext())//NOSONAR
		{
			final AddBundleToCartData addBundleToCartData = (AddBundleToCartData) fareBundleDataItr.next();
			String productCode = StringUtils.EMPTY;
			if (FareProductType.PASSENGER.equals(fareProductType))
			{
				productCode = addBundleToCartData.getProductCode();
			}
			else if (FareProductType.VEHICLE.equals(fareProductType))
			{
				productCode = ((BcfAddBundleToCartData) addBundleToCartData).getVehicleProductCode();
			}

			final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();
			final PriceLevel priceLevel = getTravelCommercePriceFacade()
					.getPriceLevelInfo(productCode, addBundleToCartData.getTransportOfferings(),
							addBundleToCartData.getTravelRouteCode());
			if (priceLevel == null)
			{
				throw new CommerceCartModificationException(PRICE_NOT_AVAILABLE + productCode);
			}

			productPriceLevelMap.put(productCode + "_" + originDestinationRefNumber, priceLevel);
		}

		return productPriceLevelMap;
	}

	protected Map<String, PriceLevel> getAmenityProductPriceLevel(final AddBundleToCartData addBundleToCartData, final
	List<AccessibilitytData> accessibilitytDataList) throws CommerceCartModificationException//NOSONAR
	{
		final Map<String, List<AccessibilitytData>> accessibilityDataCodeMap = accessibilitytDataList
				.stream()
				.collect(Collectors.groupingBy(accessibility -> accessibility.getCode()));
		final Map<String, PriceLevel> productPriceLevelMap = new HashMap<>();

		for (final Map.Entry<String, List<AccessibilitytData>> mapEntry : accessibilityDataCodeMap.entrySet())
		{
			final String productCode = mapEntry.getKey();
			final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();
			final PriceLevel priceLevel = getTravelCommercePriceFacade()
					.getPriceLevelInfo(productCode, addBundleToCartData.getTransportOfferings(),
							addBundleToCartData.getTravelRouteCode());
			if (priceLevel == null)
			{
				throw new CommerceCartModificationException(PRICE_NOT_AVAILABLE + productCode);
			}

			productPriceLevelMap.put(productCode + "_" + originDestinationRefNumber, priceLevel);
		}
		return productPriceLevelMap;
	}

	protected Map<String, PriceLevel> getLargeItemsProductPriceLevel(final AddBundleToCartData addBundleToCartData, final
	List<LargeItemData> largeItemDataList) throws CommerceCartModificationException
	{
		final Map<String, List<LargeItemData>> largeItemDataCodeMap = StreamUtil.safeStream(largeItemDataList)
				.collect(Collectors.groupingBy(largeItemData -> largeItemData.getCode()));
		final Map<String, PriceLevel> productPriceLevelMap = new HashMap<>();

		for (final Map.Entry<String, List<LargeItemData>> mapEntry : largeItemDataCodeMap.entrySet())
		{
			final String productCode = mapEntry.getKey();
			final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();
			final PriceLevel priceLevel = getTravelCommercePriceFacade()
					.getPriceLevelInfo(productCode, addBundleToCartData.getTransportOfferings(),
							addBundleToCartData.getTravelRouteCode());
			if (priceLevel == null)
			{
				throw new CommerceCartModificationException(PRICE_NOT_AVAILABLE + productCode);
			}

			productPriceLevelMap.put(productCode + "_" + originDestinationRefNumber, priceLevel);
		}
		return productPriceLevelMap;
	}

	private List<TravellerData> addPassengersToCart(final String currentCartCode,
			final Map<String, PriceLevel> productPriceLevelMap,
			final AddBundleToCartRequestData addBundleToCartRequestData, final List<AddBundleToCartData> addBundleToCartDataList,
			final int bundleNo, final List<CartModificationData> cartModifications) throws CommerceCartModificationException
	{
		final Map<String, List<SpecialServicesData>> specialServiceMapByPassengerCode = Objects
				.nonNull(addBundleToCartRequestData.getSpecialServicesData()) ? addBundleToCartRequestData
				.getSpecialServicesData().stream()
				.collect(Collectors.groupingBy(specialRequest -> specialRequest.getPassengerType())) : Collections.emptyMap();
		final List<PassengerTypeQuantityData> passengerTypeQuantityDataList = addBundleToCartRequestData.getPassengerTypes();
		final List<TravellerData> travellerDataList = new ArrayList<>();
		for (final PassengerTypeQuantityData passengerData : passengerTypeQuantityDataList)
		{
			Iterator<List<SpecialServicesData>> specialServiceIterator = Collections.emptyIterator();
			if (passengerData.getQuantity() > 0 && specialServiceMapByPassengerCode
					.containsKey(passengerData.getPassengerType().getCode()))
			{
				final List<SpecialServicesData> specialServiceByCode = specialServiceMapByPassengerCode
						.get(passengerData.getPassengerType().getCode());
				final Map<Integer, List<SpecialServicesData>> specialServiceByIndex =
						specialServiceByCode.stream().collect(Collectors.groupingBy(service -> service.getIndex()));

				specialServiceIterator = specialServiceByIndex.values().iterator();
			}

			for (int count = 1; count <= passengerData.getQuantity(); count++)
			{
				final Iterator<AddBundleToCartData> addBundleToCartDataIterator = addBundleToCartDataList.iterator();

				TravellerData travellerData = null;

				final StringBuilder travellerCode = new StringBuilder(passengerData.getPassengerType().getCode()).append(count)
						.append(((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
								.getJourneyRefNumber())
						.append(addBundleToCartRequestData.getAddBundleToCartData().get(0)
								.getOriginDestinationRefNumber());
				if (CollectionUtils.isEmpty(addBundleToCartRequestData.getPassengerUidsToBeRemoved()))
				{
					travellerData = getTravellerData(travellerCode.toString());

				}
				else
				{
					travellerData = getTravellerData(passengerData.getPassengerType().getCode(),
							((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
									.getJourneyRefNumber(), addBundleToCartRequestData.getAddBundleToCartData().get(0)
									.getOriginDestinationRefNumber(),
							addBundleToCartRequestData.getPassengerUidsToBeRemoved(),travellerCode.toString());
				}

				if (travellerData == null)
				{
					travellerData = getTravellerData(currentCartCode, passengerData, specialServiceIterator, count, travellerCode);
				}
				else
				{
					if (!CollectionUtils.isEmpty(addBundleToCartRequestData.getPassengerUidsToBeRemoved()))
					{
						final List<SpecialServicesData> specialServiceByCode = specialServiceMapByPassengerCode
								.get(passengerData.getPassengerType().getCode());
						final String passengerUid = travellerData.getUid();
						if (CollectionUtils.isNotEmpty(specialServiceByCode))
						{
							final List<SpecialServicesData> specialServiceByCodes = specialServiceByCode.stream()
									.filter(specialService -> passengerUid.equals(specialService.getPassengerUid())).collect(
											Collectors.toList());
							((BCFTravellerFacade) getTravellerFacade())
									.updateTravellerWithSpecialServiceRequest(travellerData, specialServiceByCodes);
						}
					}else if (specialServiceIterator.hasNext())
					{
						((BCFTravellerFacade) getTravellerFacade())
								.updateTravellerWithSpecialServiceRequest(travellerData, specialServiceIterator.next());
					}


				}
				travellerDataList.add(travellerData);
				/*
				 * at this moment sending the product code from first the bundle as we only submit one bundle at a time
				 */
				addProductToCart(addBundleToCartDataIterator, travellerData, bundleNo, cartModifications, productPriceLevelMap,
						addBundleToCartDataList.get(0).getProductCode());
			}
		}
		return travellerDataList;
	}

	protected TravellerData getTravellerData(final String travellerCode, final int journeyRefNo, final int odRefNo,
			final List<String> passengerUidsToBeRemoved, final String travellerLabel)
	{

		final List<TravellerData> travellerDataList = ((BCFTravellerFacade) getTravellerFacade())
				.getTravellersForCartEntries(journeyRefNo, odRefNo);
		if (CollectionUtils.isNotEmpty(travellerDataList))
		{
			final List<TravellerData> travellerDatas = travellerDataList.stream().filter((traveller) -> {
				return traveller.getTravellerType().equals(BcfCoreConstants.CATEGORY_PASSENGER)
						&& ((PassengerInformationData) traveller.getTravellerInfo())
						.getPassengerType().getCode().equals(travellerCode) && !passengerUidsToBeRemoved.contains(traveller.getUid());
			}).collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(travellerDatas))
			{
				final TravellerData traveller=travellerDatas.stream().filter(travellerData->travellerData.getLabel().equals(travellerLabel)).findAny().orElse(null);

				if(traveller!=null){
					return traveller;

				}else{

					return travellerDatas.get(0);
				}
			}
		}

		return null;
	}


	private TravellerData getTravellerData(final String currentCartCode, final PassengerTypeQuantityData passengerData,
			final Iterator<List<SpecialServicesData>> specialServiceIterator, final int count, final StringBuilder travellerCode)
	{
		final TravellerData travellerData;
		if (specialServiceIterator.hasNext())
		{
			travellerData = ((BCFTravellerFacade) getTravellerFacade())
					.createTravellerWithSpecialServiceRequest(TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER,
							passengerData.getPassengerType().getCode(), travellerCode.toString(), count, currentCartCode,
							getCartCode(currentCartCode), specialServiceIterator.next());
		}
		else
		{
			travellerData = getTravellerFacade().createTraveller(TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER,
					passengerData.getPassengerType().getCode(), travellerCode.toString(), count, currentCartCode,
					getCartCode(currentCartCode));
		}
		return travellerData;
	}


	private void addAmenitiesToCart(final Map<String, PriceLevel> productPriceLevelMap,
			final AddBundleToCartRequestData addBundleToCartRequestData, final List<AddBundleToCartData> addBundleToCartDataList,
			final int bundleNo, final List<CartModificationData> cartModifications,
			final List<TravellerData> travellerDataList) throws CommerceCartModificationException
	{
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getAccessibilityDatas()))
		{

			final List<AddBundleToCartData> addAmenityToCartDataList = new ArrayList<>();

			if (CollectionUtils.isNotEmpty(addBundleToCartDataList))
			{
				addAmenityToCartDataList.addAll(addBundleToCartDataList);
				addAmenityToCartDataList.forEach(addAmenityToCartData -> {
					addAmenityToCartData.setBundleTemplateId(null);
				});
			}



			final Map<String, List<AccessibilitytData>> accessibilityDataCodeMap = addBundleToCartRequestData
					.getAccessibilityDatas()
					.stream()
					.collect(Collectors.groupingBy(accessibility -> accessibility.getCode()));

			final Map<String, List<TravellerData>> travellerDataCodeMap = travellerDataList.stream()
					.filter(travellerData -> travellerData.getTravellerType().equals(TravellerType.PASSENGER.getCode()))
					.collect(Collectors
							.groupingBy(travellerData -> ((PassengerInformationData) travellerData.getTravellerInfo())
									.getPassengerType().getCode()));

			addAmenityTocart(productPriceLevelMap, bundleNo, cartModifications, travellerDataList, addAmenityToCartDataList,
					accessibilityDataCodeMap, travellerDataCodeMap);
		}
	}

	private void addAmenityTocart(final Map<String, PriceLevel> productPriceLevelMap, final int bundleNo,
			final List<CartModificationData> cartModifications, final List<TravellerData> travellerDataList,
			final List<AddBundleToCartData> addAmenityToCartDataList,
			final Map<String, List<AccessibilitytData>> accessibilityDataCodeMap,
			final Map<String, List<TravellerData>> travellerDataCodeMap) throws CommerceBundleCartModificationException
	{
		for (final Entry<String, List<AccessibilitytData>> mapEntry : accessibilityDataCodeMap.entrySet())
		{
			for (int count = 1; count <= mapEntry.getValue().size(); count++)
			{
				final Iterator<AddBundleToCartData> addAmenityToCartDataIterator = addAmenityToCartDataList.iterator();
				String travellerCode = "";



				if (travellerDataCodeMap.containsKey(mapEntry.getValue().get(count - 1).getPassengerType()))
				{
					travellerCode = travellerDataCodeMap.get(mapEntry.getValue().get(count - 1).getPassengerType()).stream()
							.findAny().get()
							.getLabel();
				}
				final TravellerData travellerData = getTravellerData(travellerCode) != null ?
						getTravellerData(travellerCode) :
						travellerDataList.stream().findAny().get();

				/*
				 * at this moment sending the product code from first the bundle as we only submit one bundle at a time
				 */
				addProductToCart(addAmenityToCartDataIterator, travellerData, bundleNo, cartModifications, productPriceLevelMap,
						mapEntry.getKey());
			}
		}
	}

	private void addLargeItemsToCart(final Map<String, PriceLevel> productPriceLevelMap,
			final AddBundleToCartRequestData addBundleToCartRequestData, final List<AddBundleToCartData> addBundleToCartDataList,
			final int bundleNo, final List<CartModificationData> cartModifications) throws CommerceBundleCartModificationException
	{
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getLargeItems()))
		{
			for (final LargeItemData largeItemData : addBundleToCartRequestData.getLargeItems())
			{
				for (int i = 0; i < largeItemData.getQuantity(); i++)
				{
					addProductToCart(addBundleToCartDataList.iterator(), null, bundleNo, cartModifications, productPriceLevelMap,
							largeItemData.getCode());
				}
			}
		}
	}

	private void addVehiclesToCart(final String currentCartCode, final Map<String, PriceLevel> productPriceLevelMap,
			final AddBundleToCartRequestData addBundleToCartRequestData, final List<AddBundleToCartData> addBundleToCartDataList,
			final int bundleNo, final List<CartModificationData> cartModifications) throws CommerceCartModificationException
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = addBundleToCartRequestData.getVehicleTypes();
		for (final VehicleTypeQuantityData vehicleData : vehicleTypeQuantityDataList)
		{
			for (int count = 1; count <= vehicleData.getQty(); count++)
			{
				final StringBuilder travellerCode;
				final Iterator<AddBundleToCartData> addBundleToCartDataIterator = addBundleToCartDataList.iterator();
				travellerCode = new StringBuilder(vehicleData.getVehicleType().getCode()).append(count).append(
						((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0)).getJourneyRefNumber());

				appendOdNumberIfReturnWithDifferentVehicle(travellerCode, addBundleToCartRequestData);
				TravellerData travellerData = getTravellerData(travellerCode.toString());
				if (travellerData == null)
				{
					travellerData = ((BCFTravellerFacade) getTravellerFacade()).createVehicleTraveller(
							BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, vehicleData, travellerCode.toString(), count, currentCartCode,
							getCartCode(currentCartCode));
				}

				/*
				 * at this moment sending the product code from the first bundle as we only submit one bundle at a time
				 */
				addProductToCart(addBundleToCartDataIterator, travellerData, bundleNo, cartModifications, productPriceLevelMap,
						((BcfAddBundleToCartData) addBundleToCartDataList.get(0)).getVehicleProductCode());
			}
		}
	}

	/**
	 * @param addBundleToCartDataIterator
	 * @param travellerData
	 * @param bundleNo
	 * @param cartModifications
	 * @param productPriceLevelMap
	 * @param productId,                  once we start submitting more than one bundles then either we will need to send the list of product codes
	 *                                    by consolidating product codes from all bundles or write specific methods for vehicles and passengers
	 * @return
	 * @throws CommerceBundleCartModificationException
	 */
	protected int addProductToCart(final Iterator<AddBundleToCartData> addBundleToCartDataIterator,
			final TravellerData travellerData,
			int bundleNo, final List<CartModificationData> cartModifications, final Map<String, PriceLevel> productPriceLevelMap,
			final String productId)
			throws CommerceBundleCartModificationException
	{
		while (addBundleToCartDataIterator.hasNext())
		{
			final AddBundleToCartData addBundleToCartData = addBundleToCartDataIterator.next();
			final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();
			final PriceLevel priceLevel = productPriceLevelMap.get(productId + "_" + originDestinationRefNumber);

			try
			{
				bundleNo = addProduct(productId, MINIMUM_PRODUCT_QUANTITY, bundleNo, addBundleToCartData, travellerData, false,
						priceLevel, Boolean.TRUE, AmendStatus.NEW, cartModifications);

			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't add product of code " + productId + " to cart.", ex);
				throw new CommerceBundleCartModificationException(ex.getMessage(), ex);
			}
			bundleNo = Math.abs(bundleNo + 1) * -1;
		} // while
		bundleNo = Math.abs(bundleNo + 1) * -1;
		return bundleNo;
	}

	private int addProductToCart(final Iterator<AddBundleToCartData> addBundleToCartDataIterator,
			final TravellerData travellerData,
			int bundleNo, final List<CartModificationData> cartModifications, final Map<String, PriceLevel> productPriceLevelMap)
			throws CommerceBundleCartModificationException
	{
		while (addBundleToCartDataIterator.hasNext())
		{
			final AddBundleToCartData addBundleToCartData = addBundleToCartDataIterator.next();
			final String productId = addBundleToCartData.getProductCode();
			final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();
			final PriceLevel priceLevel = productPriceLevelMap.get(productId + "_" + originDestinationRefNumber);

			try
			{
				bundleNo = addProduct(productId, MINIMUM_PRODUCT_QUANTITY, bundleNo, addBundleToCartData, travellerData, false,
						priceLevel, Boolean.TRUE, AmendStatus.NEW, cartModifications);
			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't add product of code " + productId + " to cart.", ex);
				throw new CommerceBundleCartModificationException(ex.getMessage(), ex);
			}
			bundleNo = Math.abs(bundleNo + 1) * -1;
		} // while
		bundleNo = Math.abs(bundleNo + 1) * -1;
		return bundleNo;
	}

	private void appendOdNumberIfReturnWithDifferentVehicle(final StringBuilder travellerCode,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		if (addBundleToCartRequestData.isReturnWithDifferentVehicle())
		{
			travellerCode.append(addBundleToCartRequestData.getAddBundleToCartData().get(0)
					.getOriginDestinationRefNumber());
		}
	}

	@Override
	protected int addProduct(final String code, final long qty, final int bundleNo, final AddBundleToCartData addBundleToCartData,
			final TravellerData travellerData, final boolean removeCurrentProducts, final PriceLevel priceLevel,
			final Boolean active, final AmendStatus amendStatus, final List<CartModificationData> cartModifications)
			throws CommerceCartModificationException
	{
		final String bundleTemplateId = addBundleToCartData.getBundleTemplateId();
		final List<String> transportOfferingCodes = addBundleToCartData.getTransportOfferings();
		final String travelRouteCode = addBundleToCartData.getTravelRouteCode();
		final int originDestinationRefNumber = addBundleToCartData.getOriginDestinationRefNumber();

		int bundleNoToReturn = bundleNo;
		getTravelCommercePriceFacade().setPriceAndTaxSearchCriteriaInContext(priceLevel, transportOfferingCodes, travellerData);
		setAddToCartParametersInContext(transportOfferingCodes, travelRouteCode, travellerData, priceLevel,
				originDestinationRefNumber, active, amendStatus, ((BcfAddBundleToCartData) addBundleToCartData).getJourneyRefNumber(),
				((BcfAddBundleToCartData) addBundleToCartData).isCarryingDangerousGoodsInOutbound(),
				((BcfAddBundleToCartData) addBundleToCartData).isCarryingDangerousGoodsInReturn());
		final List<CartModificationData> cartModificationDataList = getBundleCartFacade().addToCart(code, qty, bundleNo,
				bundleTemplateId, removeCurrentProducts);

		if (CollectionUtils.isNotEmpty(cartModificationDataList))
		{
			cartModifications.addAll(cartModificationDataList);
			bundleNoToReturn = cartModificationDataList.get(cartModificationDataList.size() - 1).getEntry().getBundleNo();
		}

		return bundleNoToReturn;
	}

	protected void setAddToCartParametersInContext/*NOSONAR*/(final List<String> transportOfferingCodes,
			final String travelRouteCode,
			final TravellerData travellerData, final PriceLevel priceLevel, final int originDestinationRefNumber,
			final Boolean active, final AmendStatus amendStatus, final int journeyRefNumber,
			final Boolean carryingDangerousGoodsInOutbound, final Boolean carryingDangerousGoodsInReturn)
	{
		final Map<String, Object> params = new HashMap<>();

		final List<TransportOfferingModel> transportOfferingModels = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(transportOfferingCodes))
		{
			transportOfferingCodes.forEach(transportOffering -> transportOfferingModels
					.add(getTransportOfferingService().getTransportOffering(transportOffering)));
		}
		params.put(TravelservicesConstants.CART_ENTRY_TRANSPORT_OFFERINGS, transportOfferingModels);

		final TravelRouteModel travelRouteModel = getTravelRouteService().getTravelRoute(travelRouteCode);
		params.put(TravelservicesConstants.CART_ENTRY_TRAVEL_ROUTE, travelRouteModel);

		if (travellerData != null)
		{
			final TravellerModel travellerModel = getTravellerService().getExistingTraveller(travellerData.getUid(),
					travellerData.getVersionID());
			params.put(TravelservicesConstants.CART_ENTRY_TRAVELLER, travellerModel);
		}


		if (Objects.nonNull(priceLevel))
		{
			params.put(TravelservicesConstants.CART_ENTRY_PRICELEVEL, priceLevel.getCode());
		}

		params.put(TravelservicesConstants.CART_ENTRY_ORIG_DEST_REF_NUMBER, originDestinationRefNumber);
		params.put(TravelservicesConstants.CART_ENTRY_ACTIVE, active);
		params.put(TravelservicesConstants.CART_ENTRY_AMEND_STATUS, amendStatus);
		params.put(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, journeyRefNumber);
		params.put(BcfCoreConstants.CART_ENTRY_TYPE, OrderEntryType.TRANSPORT);
		if (originDestinationRefNumber == BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER)
		{
			params.put(BcfCoreConstants.CARRYING_DANGEROUS_GOODS, carryingDangerousGoodsInOutbound);
		}
		else
		{
			params.put(BcfCoreConstants.CARRYING_DANGEROUS_GOODS, carryingDangerousGoodsInReturn);
		}

		getSessionService().setAttribute(TravelservicesConstants.ADDBUNDLE_TO_CART_PARAM_MAP, params);
	}


	@Override
	public CartModificationData addToCart(final AddProductToCartRequestData addProductToCartRequestData)
			throws CommerceCartModificationException, IntegrationException//NOSONAR
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		BCFMakeBookingResponse response = null;

		if (isAmendmentCart() || (!getUserService().isAnonymousUser(cartModel.getUser()) && !AccountType.SUBSCRIPTION_ONLY
				.equals(((CustomerModel) cartModel.getUser()).getAccountType())))
		{
			final BCFModifyBookingResponse bcfModifyBookingResponse = modifyBookingIntegrationFacade
					.getModifyBookingResponse(cartModel,
							Collections.singletonList(addProductToCartRequestData));
			response = bcfModifyBookingResponse.getMakeBookingResponse();
		}
		else

		{
			response = makeBookingIntegrationFacade.getMakeBookingResponse(cartModel,
					Collections.singletonList(addProductToCartRequestData));
		}
		if (Objects.nonNull(response))
		{
			setPriceQuotationInContext(response);
			setQuotationExpirationDate(cartModel);
			removeOldPaymentInfos(cartModel, addProductToCartRequestData.getJourneyRefNumber(),
					addProductToCartRequestData.getOriginDestinationRefNumber());
			updateCartEntriesWithCacheKeyOrBookingRef(cartModel, addProductToCartRequestData.getJourneyRefNumber(),
					addProductToCartRequestData.getOriginDestinationRefNumber(),
					response.getItinerary().getBooking().getBookingReference().getCachingKey(),
					response.getItinerary().getBooking().getBookingReference().getBookingReference());
			setAmountToPay(response, cartModel);
			getModelService().saveAll();

		}
		else
		{
			return super.addToCart(addProductToCartRequestData.getProductCode(), addProductToCartRequestData.getQty());
		}
		for (final SailingLine sailingLine : response.getItinerary().getSailing().getLine())
		{
			final Optional<ProductFare> ancillaryProduct = sailingLine.getSailingPrices().stream()
					.findAny().get()
					.getProductFares().stream()
					.filter(
							productFare -> addProductToCartRequestData.getProductCode().equals(productFare.getProduct().getProductId()))
					.findAny();
			/**
			 * this needs to be modified a bit once we have the actual response
			 */
			if (ancillaryProduct.isPresent())
			{
				final CartModificationData cartModificationData = super.addToCart(addProductToCartRequestData.getProductCode(),
						addProductToCartRequestData.getQty());

				cartModificationData.setMakeBookingResponse(response);
				return cartModificationData;
			}
			else
			{
				throw new CommerceCartModificationException("Product Not available");
			}
		}
		throw new CommerceCartModificationException("Product Not available");
	}

	private void removeOldPaymentInfos(final AbstractOrderModel abstractOrderModel, final int journeyRefNum,
			final int ODRefNo)
	{
		if (Objects.nonNull(abstractOrderModel) && CollectionUtils.isNotEmpty(abstractOrderModel.getEntries()) && CollectionUtils
				.isNotEmpty(abstractOrderModel.getAmountToPayInfos()))
		{
			final List<AmountToPayInfoModel> infosToRemove = new ArrayList<>();
			final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries().stream()
					.filter(entry -> entry.getJourneyReferenceNumber() == journeyRefNum
							&& entry.getTravelOrderEntryInfo() != null
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && ODRefNo == entry
							.getTravelOrderEntryInfo().getOriginDestinationRefNumber() && AmendStatus.NEW.equals(entry.getAmendStatus()))
					.collect(Collectors.toList());
			if (getBcfTravelCartService().useCacheKey(abstractOrderModel))
			{
				final String previousCacheKey = entries.get(0).getCacheKey();
				abstractOrderModel.getAmountToPayInfos().stream().forEach(amountToPayInfoModel -> {
					if (StringUtils.equals(previousCacheKey, amountToPayInfoModel.getCacheKey()))
					{
						infosToRemove.add(amountToPayInfoModel);
					}
				});
			}
			else
			{
				final String previousBookingReference = entries.get(0).getBookingReference();
				abstractOrderModel.getAmountToPayInfos().stream().forEach(amountToPayInfoModel -> {
					if (StringUtils.equals(previousBookingReference, amountToPayInfoModel.getBookingReference()))
					{
						infosToRemove.add(amountToPayInfoModel);
					}
				});
			}
			saveRefreshModel(abstractOrderModel, infosToRemove);
		}
	}

	private void saveRefreshModel(final AbstractOrderModel abstractOrderModel, final List<AmountToPayInfoModel> infosToRemove)
	{
		if (CollectionUtils.isNotEmpty(infosToRemove))
		{
			getModelService().removeAll(infosToRemove);
			getModelService().saveAll();
			getModelService().refresh(abstractOrderModel);
		}
	}

	@Override
	public void updatePaymentInfosWithCacheKeyOrBookingRef(final AbstractOrderModel abstractOrderModel, final int journeyRefNum,
			final int ODRefNo, final String cacheKey, final String bookingRef)
	{
		if (Objects.nonNull(abstractOrderModel) && CollectionUtils.isNotEmpty(abstractOrderModel.getEntries()) && CollectionUtils
				.isNotEmpty(abstractOrderModel.getAmountToPayInfos()))
		{
			final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries().stream()
					.filter(entry -> entry.getJourneyReferenceNumber() == journeyRefNum
							&& entry.getTravelOrderEntryInfo() != null
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && ODRefNo == entry
							.getTravelOrderEntryInfo().getOriginDestinationRefNumber() && AmendStatus.NEW.equals(entry.getAmendStatus())
							&& entry.getActive())
					.collect(Collectors.toList());
			if (getBcfTravelCartService().useCacheKey(abstractOrderModel))
			{
				final String previousCacheKey = entries.get(0).getCacheKey();
				abstractOrderModel.getAmountToPayInfos().stream().forEach(amountToPayInfoModel -> {
					if (StringUtils.equals(previousCacheKey, amountToPayInfoModel.getCacheKey()))
					{
						amountToPayInfoModel.setCacheKey(cacheKey);
					}
				});
			}
			else
			{
				final String previousBookingReference = entries.get(0).getBookingReference();
				abstractOrderModel.getAmountToPayInfos().stream().forEach(amountToPayInfoModel -> {
					if (StringUtils.equals(previousBookingReference, amountToPayInfoModel.getBookingReference()))
					{
						amountToPayInfoModel.setBookingReference(bookingRef);
					}
				});
			}
			getModelService().saveAll();
			getModelService().refresh(abstractOrderModel);
		}
	}

	@Override
	public void updateCartEntriesWithCacheKeyOrBookingRef(final AbstractOrderModel abstractOrderModel, final int journeyRefNum,
			final int ODRefNo, final String cacheKey, final String bookingRef)
	{
		if (Objects.isNull(abstractOrderModel) && CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return;
		}
		final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getJourneyReferenceNumber() == journeyRefNum
						&& entry.getTravelOrderEntryInfo() != null && entry.getActive()
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && ODRefNo == entry
						.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(entries))
		{
			entries.forEach(entry -> {
				if (StringUtils.isNotEmpty(cacheKey))
				{
					entry.setCacheKey(cacheKey);
				}
				if (StringUtils.isNotEmpty(bookingRef))
				{
					entry.setBookingReference(bookingRef);
				}
			});
		}

		getModelService().saveAll();
		getModelService().refresh(abstractOrderModel);
	}

	@Override
	public CartModificationData updateCart(final AddProductToCartRequestData addProductToCartRequestData,
			final Integer entryNumber, final long newQuantity)
			throws CommerceCartModificationException, IntegrationException//NOSONAR
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final BCFMakeBookingResponse response;
		if (isAmendmentCart() || (!getUserService().isAnonymousUser(cartModel.getUser()) && !AccountType.SUBSCRIPTION_ONLY
				.equals(((CustomerModel) cartModel.getUser()).getAccountType())))
		{
			final BCFModifyBookingResponse bcfModifyBookingResponse = modifyBookingIntegrationFacade
					.getModifyBookingResponse(cartModel,
							Collections.singletonList(addProductToCartRequestData));
			response = bcfModifyBookingResponse.getMakeBookingResponse();
		}
		else

		{
			response = makeBookingIntegrationFacade.getMakeBookingResponse(cartModel,
					Collections.singletonList(addProductToCartRequestData));
		}
		if (Objects.nonNull(response))
		{
			setPriceQuotationInContext(response);
			setQuotationExpirationDate(cartModel);
			removeOldPaymentInfos(cartModel, addProductToCartRequestData.getJourneyRefNumber(),
					addProductToCartRequestData.getOriginDestinationRefNumber());
			updateCartEntriesWithCacheKeyOrBookingRef(cartModel, addProductToCartRequestData.getJourneyRefNumber(),
					addProductToCartRequestData.getOriginDestinationRefNumber(),
					response.getItinerary().getBooking().getBookingReference().getCachingKey(),
					response.getItinerary().getBooking().getBookingReference().getBookingReference());
			setAmountToPay(response, cartModel);
			return updateCartEntry(entryNumber, newQuantity);
		}
		return new CartModificationData();
	}



	@Override
	public void setAmountToPay(final BCFMakeBookingResponse response, final CartModel cartModel
	)
	{
		final BigInteger outstandingToPayInCents = response.getItinerary().getBooking().getOutstandingToPayInCents();
		if (isAmendmentCart() && outstandingToPayInCents != null)
		{
			updateAmountToPayInfos(response, cartModel, outstandingToPayInCents);
		}
		else
		{
			BigInteger minimumToPayNowInCents = response.getItinerary().getBooking().getMinimumToPayNowInCents();
			minimumToPayNowInCents = Objects.nonNull(minimumToPayNowInCents) ? minimumToPayNowInCents : BigInteger.ZERO;
			updateAmountToPayInfos(response, cartModel, minimumToPayNowInCents);
		}
	}

	private void updateAmountToPayInfos(final BCFMakeBookingResponse response, final CartModel cartModel,
			final BigInteger amountToPayNowInCents)
	{
		final BigInteger totalFaresInCents = Objects.nonNull(response.getItinerary().getBooking().getGrossAmountInCents())
				? response.getItinerary().getBooking().getGrossAmountInCents() : BigInteger.ZERO;
		final String cachingKey = response.getItinerary().getBooking().getBookingReference().getCachingKey();
		final String bookingReference = response.getItinerary().getBooking().getBookingReference().getBookingReference();
		if (amountToPayNowInCents != null)
		{
			if (CollectionUtils.isEmpty(cartModel.getAmountToPayInfos()))
			{
				final AmountToPayInfoModel amountToPayInfo = createAmountToPayEntry(cachingKey, bookingReference,
						amountToPayNowInCents,
						totalFaresInCents, cartModel);
				final List<AmountToPayInfoModel> amountToPayInfos = new ArrayList<>();
				amountToPayInfos.add(amountToPayInfo);
				cartModel.setAmountToPayInfos(amountToPayInfos);
				getModelService().save(cartModel);
				return;
			}
			final AmountToPayInfoModel amountToPayInfo = getOrCreateAmountToPayInfo(cartModel, cachingKey, bookingReference);
			if (Objects.nonNull(amountToPayInfo))
			{
				amountToPayInfo.setAmountToPay(amountToPayNowInCents.doubleValue());
				final Double totalRemaining = totalFaresInCents.doubleValue()
						- (amountToPayNowInCents.doubleValue() + amountToPayInfo.getPreviouslyPaid());
				amountToPayInfo.setPayAtTerminal(totalRemaining);
			}
			else
			{
				final List<AmountToPayInfoModel> amountToPayInfos = new ArrayList<>();
				amountToPayInfos.addAll(cartModel.getAmountToPayInfos());
				amountToPayInfos
						.add(createAmountToPayEntry(cachingKey, bookingReference, amountToPayNowInCents, totalFaresInCents,
								cartModel));
				cartModel.setAmountToPayInfos(amountToPayInfos);
			}
			getModelService().save(cartModel);
		}
	}


	private AmountToPayInfoModel getOrCreateAmountToPayInfo(final CartModel cartModel, final String cachingKey,
			final String bookingReference)
	{
		AmountToPayInfoModel amountToPayInfo = null;
		if (bcfTravelCartService.useCacheKey(cartModel))
		{
			amountToPayInfo = cartModel.getAmountToPayInfos().stream()
					.filter(amountToPayInfoModel -> cachingKey != null && cachingKey.equals(amountToPayInfoModel.getCacheKey()))
					.findAny().orElse(null);
		}
		else
		{
			amountToPayInfo = cartModel.getAmountToPayInfos().stream()
					.filter(amountToPayInfoModel -> bookingReference != null && bookingReference
							.equals(amountToPayInfoModel.getBookingReference())).findAny().orElse(null);
		}
		return amountToPayInfo;
	}


	protected AmountToPayInfoModel createAmountToPayEntry(final String cachingKey, final String bookingReference,
			final BigInteger minimumToPayNowInCents,
			final BigInteger totalFaresInCents, final CartModel cartModel)
	{
		final AmountToPayInfoModel amountToPayInfoModel = modelService.create(AmountToPayInfoModel.class);
		amountToPayInfoModel.setCacheKey(cachingKey);
		amountToPayInfoModel.setBookingReference(bookingReference);
		amountToPayInfoModel.setPreviouslyPaid((double) 0);
		amountToPayInfoModel.setAmountToPay(minimumToPayNowInCents.doubleValue());
		amountToPayInfoModel.setPayAtTerminal((totalFaresInCents.doubleValue() - minimumToPayNowInCents.doubleValue()));
		return amountToPayInfoModel;
	}

	@Override
	public void setPriceQuotationInContext(final BCFMakeBookingResponse response)
	{
		final Map<String, Object> makeBookingResponseMap = new HashMap<>();
		final String cachingKey = response.getItinerary().getBooking().getBookingReference().getCachingKey();
		final String bookingReference = response.getItinerary().getBooking().getBookingReference().getBookingReference();
		final List<ExternalPriceMapData> externalPriceMapDatas = convertResponseToPriceLevelMap(response);

		makeBookingResponseMap.put(BcfCoreConstants.CACHING_KEY, cachingKey);
		makeBookingResponseMap.put(BcfCoreConstants.BOOKING_REFERENCE, bookingReference);
		makeBookingResponseMap.put(BcfCoreConstants.PRICE_MAP, externalPriceMapDatas);

		getSessionService().setAttribute(BcfCoreConstants.MAKE_BOOKING_RESPONSE, makeBookingResponseMap);
		if (!StringUtils.equals(BcfCoreConstants.BOOKING_TRANSPORT_ONLY,
				getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			getSessionService().setAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY, Boolean.TRUE);
		}
	}

	protected List<ExternalPriceMapData> convertResponseToPriceLevelMap(final BCFMakeBookingResponse response)
	{
		final List<ExternalPriceMapData> externalPriceMapDatas = new ArrayList<>();
		for (final SailingLine sailingLine : response.getItinerary().getSailing().getLine())
		{
			int index = 0;
			final Double marginRate = null;

			final List<ExternalPriceMapData> externalPriceMapDatasPerLine = new ArrayList<>();
			for (final SailingPrices sailingPrice : sailingLine.getSailingPrices())
			{
				index = addPriceData(response, index, externalPriceMapDatasPerLine, sailingPrice);
			}

			if (Objects.nonNull(marginRate) && marginRate > 0)
			{
				addMarginToExternalPriceMapData(externalPriceMapDatasPerLine, marginRate);
			}

			externalPriceMapDatas.addAll(externalPriceMapDatasPerLine);
		}

		if (Objects.nonNull(response.getItinerary().getSailing().getTransferSailingIdentifier()))
		{
			final Map<String, List<ExternalPriceMapData>> productPriceMap = externalPriceMapDatas.stream()
					.collect(Collectors.groupingBy(price -> price.getProductId()));
			final List<ExternalPriceMapData> transferPriceMap = new ArrayList<>();

			for (final Map.Entry<String, List<ExternalPriceMapData>> productPrice : productPriceMap.entrySet())
			{
				final ExternalPriceMapData priceData = new ExternalPriceMapData();
				Double value = 0.0;
				final List<ExternalPriceTaxes> taxes = new ArrayList<>();
				for (final ExternalPriceMapData externalPrice : productPrice.getValue())
				{
					value = value + externalPrice.getValue();
					taxes.addAll(externalPrice.getTaxes());
				}
				priceData.setFareDetails(productPrice.getValue().stream().findFirst().get().getFareDetails());
				priceData.setProductId(productPrice.getValue().stream().findFirst().get().getProductId().split("_")[0]);
				priceData.setTaxes(taxes);
				priceData.setValue(value);
				transferPriceMap.add(priceData);
			}
			return transferPriceMap;
		}
		return externalPriceMapDatas;
	}

	private int addPriceData(final BCFMakeBookingResponse response, int index,
			final List<ExternalPriceMapData> externalPriceMapDatasPerLine, final SailingPrices sailingPrice)
	{
		for (final ProductFare product : sailingPrice.getProductFares())
		{
			final ExternalPriceMapData priceData = createPriceMapData(response, index, product);
			externalPriceMapDatasPerLine.add(priceData);
			index++;
		}

		index = 0;
		if (CollectionUtils.isNotEmpty(response.getItinerary().getSailing().getOtherCharges()))
		{

			for (final ProductFare product : response.getItinerary().getSailing().getOtherCharges())
			{
				final ExternalPriceMapData priceData = createPriceMapData(response, index, product);
				externalPriceMapDatasPerLine.add(priceData);
				index++;
			}
		}
		return index;
	}

	private void addMarginToExternalPriceMapData(final List<ExternalPriceMapData> externalPriceMapDatas, final Double marginRate)
	{
		for (final ExternalPriceMapData externalPriceMapData : externalPriceMapDatas)
		{
			final double farePriceVal = externalPriceMapData.getValue();
			final double marginVal = farePriceVal * marginRate / 100;
			final double effectiveFarePriceVal = farePriceVal + marginVal;
			externalPriceMapData.setValue(effectiveFarePriceVal);
		}
	}

	private ExternalPriceMapData createPriceMapData(final BCFMakeBookingResponse response, final int index,
			final ProductFare product)
	{
		final ExternalPriceMapData priceData = new ExternalPriceMapData();
		if (Objects.nonNull(response.getItinerary().getSailing().getTransferSailingIdentifier()))
		{
			priceData.setProductId(product.getProduct().getProductId() + "_" + index);
		}
		else
		{
			priceData.setProductId(product.getProduct().getProductId());
		}
		priceData.setConsumed(false);
		priceData.setValue(
				(product.getFareDetail().stream().findFirst().get().getNetAmountInCents() / (100.0 * product.getProduct()
						.getProductNumber())));
		priceData.setTaxes(populateExternalTaxes(product.getFareDetail().stream().findFirst().get().getTaxes()));
		final List<com.bcf.facades.cart.FareDetail> fareDetailList = new ArrayList<>();
		final List<FareDetail> fareDetails = product.getFareDetail().stream()
				.filter(fareDetail -> !product.getProduct().getProductId().equals(fareDetail.getFareCode()))
				.collect(Collectors.toList());
		fareDetails.forEach(fareDetail -> {
			final com.bcf.facades.cart.FareDetail fareDetailData = new com.bcf.facades.cart.FareDetail();
			fareDetailData.setCode(fareDetail.getFareCode());
			fareDetailData.setAmount(fareDetail.getNetAmountInCents());
			fareDetailData.setDescription(fareDetail.getFareDescription());
			fareDetailList.add(fareDetailData);
		});
		priceData.setFareDetails(fareDetailList);
		return priceData;
	}

	protected List<ExternalPriceTaxes> populateExternalTaxes(final List<Tax> taxes)
	{
		final List<ExternalPriceTaxes> externalPriceTaxesList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(taxes))
		{
			for (final Tax tax : taxes)
			{
				if (tax.getAmountInCents() != 0)
				{
					final ExternalPriceTaxes externalPriceTaxes = new ExternalPriceTaxes();
					externalPriceTaxes.setAmountInCents(tax.getAmountInCents());
					externalPriceTaxes.setTaxCode(tax.getTaxCode());
					externalPriceTaxes.setTaxDescription(tax.getTaxCode());
					externalPriceTaxesList.add(externalPriceTaxes);
				}
			}
		}

		return externalPriceTaxesList;
	}

	protected void setQuotationExpirationDate(final AbstractOrderModel abstractOrder)
	{
		if (Objects.nonNull(abstractOrder))
		{
			final int expirationTime = Integer.parseInt(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.EBOOKING_MAX_QUOTATION_EXPIRATION_MINUTES));
			abstractOrder.setQuoteExpirationDate(BCFDateUtils.addMinutes(new Date(), expirationTime));
		}

	}

	@Override
	public void setTermsAndConditionsCode(final AbstractOrderModel abstractOrder, final BCFMakeBookingResponse response)
	{
		final List<Object> itemsToSave = new ArrayList<>();
		if (Objects.nonNull(abstractOrder) && CollectionUtils.isNotEmpty(abstractOrder.getAmountToPayInfos()))
		{
			StreamUtil.safeStream(abstractOrder.getAmountToPayInfos()).forEach(paymentInfo -> {
				if (StringUtils
						.equals(paymentInfo.getCacheKey(), response.getItinerary().getBooking().getBookingReference().getCachingKey())
						|| StringUtils.equals(paymentInfo.getBookingReference(),
						response.getItinerary().getBooking().getBookingReference().getBookingReference()))
				{
					paymentInfo.setTermsAndConditionsCode(response.getItinerary().getBooking().getTermsAndConditionsCode());
					itemsToSave.add(paymentInfo);
				}
			});
			itemsToSave.add(abstractOrder);
			getModelService().saveAll(itemsToSave);
		}
	}

	protected void setFerryOptionBooking(final AbstractOrderModel abstractOrder,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		if (Objects.nonNull(abstractOrder) && addBundleToCartRequestData.isFerryOptionBooking())
		{
			abstractOrder.setFerryOptionBooking(true);
			getModelService().save(abstractOrder);
		}
	}

	protected void updateTravelOrderEntryInfoForDifferentPAXorVEH(final AbstractOrderModel abstractOrder,
			final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		if (Objects.nonNull(abstractOrder))
		{
			StreamUtil.safeStream(abstractOrder.getEntries()).forEach(entry -> {
				if (entry.getActive() && AmendStatus.NEW.equals(entry.getAmendStatus())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
						&& addBundleToCartRequestData.getSelectedJourneyRefNumber() == entry.getJourneyReferenceNumber()
						&& addBundleToCartRequestData.getSelectedOdRefNumber() == entry.getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber())
				{
					entry.getTravelOrderEntryInfo()
							.setReturnWithDifferentPassenger(addBundleToCartRequestData.isReturnWithDifferentPassenger());
					entry.getTravelOrderEntryInfo()
							.setReturnWithDifferentVehicle(addBundleToCartRequestData.isReturnWithDifferentVehicle());
				}
			});
			getModelService().save(abstractOrder);
		}
	}

	@Override
	public void removeEntriesForTransportOfferingCodes(final List<String> transportOfferingCodes)
	{
		getBcfTravelCartService().removeEntriesForTransportOfferingCodes(getBcfTravelCartService().getOrCreateSessionCart(),
				transportOfferingCodes);
	}


	@Override
	public List<AbstractOrderEntryModel> getCartEntriesForRefNo(final int journeyRefNo, final int odRefNo)
	{
		return getBcfTravelCartService().getCartEntriesForRefNo(journeyRefNo, odRefNo);
	}

	/**
	 * method to be used to remove only static deals strictly
	 *
	 * @param dealId
	 * @param departureDate
	 * @return
	 * @throws ModelRemovalException
	 * @throws IntegrationException
	 */
	@Override
	public List<RemoveSailingResponseData> removeDealFromCart(final String dealId, final Date departureDate)
			throws ModelRemovalException, IntegrationException//NOSONAR
	{
		if (!getBcfTravelCartService().hasSessionCart())
		{
			return Collections.emptyList();
		}
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService().getDealOrderEntryGroups(cartModel);
		if (CollectionUtils.isEmpty(dealOrderEntryGroupModels))
		{
			return Collections.emptyList();
		}
		dealOrderEntryGroupModels = dealOrderEntryGroupModels.stream().filter(
				dealOrderEntryGroupModel -> Objects.isNull(dealOrderEntryGroupModel.getJourneyRefNumber())
						&& dealOrderEntryGroupModel.getPackageId().equals(dealId) && dealOrderEntryGroupModel
						.getDepartureDate().equals(departureDate)).collect(Collectors.toList());

		if (CollectionUtils.isEmpty(dealOrderEntryGroupModels))
		{
			return Collections.emptyList();
		}
		final List<RemoveSailingResponseData> removeSailingResponseDatas = cancelSailingInDeals(dealOrderEntryGroupModels);
		if (CollectionUtils.isEmpty(removeSailingResponseDatas) || CollectionUtils
				.isEmpty(getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, false)))
		{
			getBcfBookingService().removeEntriesForDeal(dealOrderEntryGroupModels, true);
		}

		if (CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			deleteCurrentCart();
		}

		getModelService().refresh(cartModel);
		return removeSailingResponseDatas;
	}

	protected List<RemoveSailingResponseData> cancelSailingInDeals(final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels)
			throws IntegrationException
	{
		final List<CartFilterParamsDto> cartFilterParamsDtos = new ArrayList<>();
		dealOrderEntryGroupModels.stream().forEach(dealOrderEntryGroupModel -> {
			final List<AbstractOrderEntryModel> cancelBookingEntries = dealOrderEntryGroupModel.getEntries().stream()
					.filter(entry -> (entry.getProduct() instanceof FareProductModel) && Objects.nonNull(entry.getCacheKey()))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(cancelBookingEntries))
			{
				cancelBookingEntries
						.forEach(entry -> cartFilterParamsDtos.add(initializeCartFilterParams(entry.getJourneyReferenceNumber(),
								entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber(), false, Collections.emptyList())));
			}
		});
		if (CollectionUtils.isEmpty(cartFilterParamsDtos))
		{
			return Collections.emptyList();
		}

		return getCancelBookingIntegrationFacade()
				.cancelSailingsInCart(cartFilterParamsDtos, Boolean.FALSE);
	}


	@Override
	public void removeCartEntriesForRefNo(final int journeyRefNo, final int odRefNo, final boolean isSingle,
			final boolean removeInboundEntries)
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();

		final CartFilterParamsDto cartFilterParams = initializeCartFilterParams(journeyRefNo, odRefNo, true,
				Collections.emptyList());

		getBcfTravelCartService().removeEntriesForJourneyODRefNo(cartModel, cartFilterParams, isSingle, removeInboundEntries);
	}

	@Override
	public void removeNewCartEntriesForRefNo(final int journeyRefNo, final int odRefNo, final boolean isSingle,
			final boolean removeInboundEntries)
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();

		final CartFilterParamsDto cartFilterParams = initializeCartFilterParams(journeyRefNo, odRefNo, true,
				Collections.emptyList());

		getBcfTravelCartService().removeNewEntriesForJourneyODRefNo(cartModel, cartFilterParams, isSingle, removeInboundEntries);
	}


	@Override
	public List<RemoveSailingResponseData> modifyCartEntriesForRefNo(final int journeyRefNo, final int odRefNo,
			final boolean isSingle) throws IntegrationException, CalculationException//NOSONAR
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		List<RemoveSailingResponseData> removeSailingResponseDatas = new ArrayList<>();
		final CartFilterParamsDto cartFilterParams = initializeCartFilterParams(journeyRefNo, odRefNo, false,
				Collections.emptyList());

		final boolean amendmentCart = isAmendmentCart();

		if (amendmentCart)
		{
			modifyAbstractOrderEntriesForRefNo(cartModel, journeyRefNo, odRefNo, isSingle, amendmentCart);
		}
		else
		{
			if (getUserService().isAnonymousUser(getUserService().getCurrentUser()) || isGuestUserCart(cartModel)
					|| AccountType.SUBSCRIPTION_ONLY.equals(((CustomerModel) cartModel.getUser()).getAccountType()))
			{

				modifyAbstractOrderEntriesForRefNo(cartModel, journeyRefNo, odRefNo, isSingle, amendmentCart);

			}
			else
			{

				removeSailingResponseDatas = getCancelBookingIntegrationFacade()
						.cancelSailingsInCart(Collections.singletonList(cartFilterParams), Boolean.FALSE);

				modifyAbstractOrderEntriesForRefNo(cartModel, journeyRefNo, odRefNo, isSingle, amendmentCart);

			}

		}
		bcfTravelCartFacadeHelper.calculateChangeFees();
		calculateCart(cartModel);

		return CollectionUtils.isNotEmpty(removeSailingResponseDatas) ?
				getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, false) :
				removeSailingResponseDatas;
	}

	protected boolean isGuestUserCart(final AbstractOrderModel abstractOrderModel)
	{
		final ItemModel customer = abstractOrderModel.getUser();
		return customer instanceof CustomerModel && CustomerType.GUEST.equals(((CustomerModel) customer).getType());
	}

	private void calculateCart(final CartModel cart) throws CalculationException
	{
		getCalculationService().recalculate(cart);
		getSessionService().removeAttribute(TravelservicesConstants.ADDBUNDLE_TO_CART_PARAM_MAP);
	}

	@Override
	public void modifyAbstractOrderEntriesForRefNo(final AbstractOrderModel orderModel, final int journeyRefNo, final int odRefNo,
			final boolean isSingle, final boolean disableEntries)
	{
		final CartFilterParamsDto cartFilterParams = initializeCartFilterParams(journeyRefNo, odRefNo, true,
				Collections.emptyList());
		List<AbstractOrderEntryModel> filteredEntries = null;
		if (hasSessionCart())
		{
			final CartModel cart = bcfTravelCartService.getSessionCart();
			filteredEntries = cart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()))
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& entry.getJourneyReferenceNumber() == cartFilterParams.getJourneyRefNo()
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartFilterParams.getOdRefNo()
					)
					.collect(Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(filteredEntries))
		{

			final boolean sameEntries = filteredEntries.stream()
					.anyMatch(entry -> AmendStatus.SAME.equals(entry.getAmendStatus()));//NOSONAR
			if (disableEntries && sameEntries)
			{
				bcfTravelCartService
						.disableAmendedCartEntries(cartFilterParams);
			}
			else
			{
				getBcfTravelCartService().removeEntriesForJourneyODRefNo(orderModel, cartFilterParams, isSingle, false);
			}

		}

		if (!isSingle && odRefNo == TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER)
		{
			cartFilterParams.setOdRefNo(TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);
			getBcfTravelCartService().updateEntriesForJourneyODRefNo(orderModel, cartFilterParams);
		}
	}

	@Override
	public void modifyTheOppositeLeg(final int currentJourneyRefNo, final int odRefNo)
	{
		boolean flag = Boolean.FALSE;
		int odRefNumToUpdate = TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER;
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		if (odRefNo == TravelfacadesConstants.INBOUND_REFERENCE_NUMBER)
		{
			flag = Boolean.TRUE;
		}
		else
		{
			final List<AbstractOrderEntryModel> abstractOrderEntries = getCartEntriesForRefNo(currentJourneyRefNo,
					TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);

			if (CollectionUtils.isNotEmpty(abstractOrderEntries))
			{
				odRefNumToUpdate = TravelfacadesConstants.INBOUND_REFERENCE_NUMBER;
				flag = Boolean.TRUE;
			}
		}
		if (flag)
		{
			final CartFilterParamsDto cartFilterParams = initializeCartFilterParams(currentJourneyRefNo, odRefNumToUpdate, true,
					Collections.emptyList());
			getBcfTravelCartService().updateEntriesForJourneyODRefNo(cartModel, cartFilterParams);
		}
	}

	@Override
	public void recalculateCart()
	{
		if (getCartService().hasSessionCart())
		{
			super.recalculateCart();
		}
	}

	@Override
	public TripType getTripType(final int journeyRefNumber)
	{
		if (getBcfTravelCartService().hasSessionCart())
		{
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber = getBcfTravelCartService()
					.getOrCreateSessionCart().getEntries().stream()
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			if (MapUtils.isNotEmpty(cartEntriesByJourneyRefNumber))
			{
				return getTripType(cartEntriesByJourneyRefNumber.get(journeyRefNumber));
			}
		}
		return null;
	}

	protected TripType getTripType(final List<AbstractOrderEntryModel> journeyOrderEntries)
	{
		if (CollectionUtils.isEmpty(journeyOrderEntries))
		{
			return null;
		}

		final Optional<AbstractOrderEntryModel> inboundCartEntry = journeyOrderEntries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
						.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()) && entry
						.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == TravelfacadesConstants.INBOUND_REFERENCE_NUMBER)
				.findAny();
		return inboundCartEntry.isPresent() ? TripType.RETURN : TripType.SINGLE;

	}


	@Override
	public Long getQuotationExpirationTime()
	{
		return getBcfTravelCartService().hasSessionCart()
				&& Objects.nonNull(getBcfTravelCartService().getOrCreateSessionCart().getQuoteExpirationDate())
				? getBcfTravelCartService().getOrCreateSessionCart().getQuoteExpirationDate().getTime()
				: null;
	}


	@Override
	public void cleanUpCartForMinOriginDestinationRefNumber(final Integer journeyRefNumber, final Integer odRefNum)
			throws ModelRemovalException
	{
		((BCFTravelCommerceCartService) getTravelCommerceCartService())
				.removeCartEntriesForMinODRefNumber(journeyRefNumber, odRefNum);
	}

	@Override
	public int getNextJourneyRefNumber()
	{
		final Integer selectedJourneyRefNumber = getSessionService().getAttribute("selectedJourneyRefNumber");
		if (Objects.nonNull(selectedJourneyRefNumber))
		{
			return selectedJourneyRefNumber;
		}
		if (!hasFareProductEntries())
		{
			return 0;
		}
		final CartModel cartModel = (CartModel) bcfTravelCartService.getOrCreateSessionCart();


		return cartModel.getCurrentJourneyRefNum() + 1;

	}

	@Override
	public int getCurrentJourneyRefNumber()
	{
		if (!hasFareProductEntries())
		{
			return 0;
		}
		final CartModel cartModel = (CartModel) bcfTravelCartService.getOrCreateSessionCart();


		return cartModel.getCurrentJourneyRefNum();

	}

	@Override
	public boolean hasFareProductEntries()
	{
		if (!bcfTravelCartService.hasSessionCart())
		{
			return Boolean.FALSE;
		}
		return CollectionUtils
				.isNotEmpty(bcfTravelCartService.getFareProductEntries(bcfTravelCartService.getOrCreateSessionCart()));
	}

	protected void addPropertiesToAncillaryCartEntry(final CartEntryPropertiesData cartEntryPropertiesData,
			final AddBundleToCartData addBundleToCartData, final TravellerModel travellerModel)
	{

		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final ProductModel product = getProductService().getProductForCode(cartEntryPropertiesData.getProductCode());

		final List<TravellerModel> travellersList = new ArrayList<>();

		travellersList.add(travellerModel);

		final Optional<AbstractOrderEntryModel> optionalOrderEntryModel = cartModel.getEntries().stream()
				.filter(aoem -> FareProductModel._TYPECODE.equals(aoem.getProduct().getItemtype())
						&& aoem.getJourneyReferenceNumber() == cartEntryPropertiesData.getJourneyRefNumber()
						&& aoem.getTravelOrderEntryInfo() != null
						&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartEntryPropertiesData
						.getOriginDestinationRefNo()
						&& aoem.getTravelOrderEntryInfo().getTravelRoute().getCode().equals(addBundleToCartData.getTravelRouteCode())
						&& aoem.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get().getDepartureTime().equals(
						((BcfAddBundleToCartData) (addBundleToCartData)).getTransportOfferingDatas().get(0).getDepartureTime())

						&& aoem.getTravelOrderEntryInfo().getTravellers().contains(travellerModel))
				.findFirst();

		TravelOrderEntryInfoModel travelEntryInfoModel = null;
		if (optionalOrderEntryModel.isPresent())
		{
			travelEntryInfoModel = optionalOrderEntryModel.get().getTravelOrderEntryInfo();
		}
		final Map<String, Map<String, Object>> propertiesMaps = populatePropertiesMap(cartEntryPropertiesData, travellersList,
				travelEntryInfoModel);
		((BCFTravelCommerceCartService) getTravelCommerceCartService()).persistAdditionalPropertiesForCartEntry(cartModel,
				cartEntryPropertiesData.getEntryNumber(), product, propertiesMaps);
	}

	@Override
	public void addPropertiesToCartEntry(final CartEntryPropertiesData cartEntryPropertiesData)
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final ProductModel product = getProductService().getProductForCode(cartEntryPropertiesData.getProductCode());

		final List<TravellerModel> travellersList = new ArrayList<>();
		final TravellerModel traveller = getTravellerService()
				.getTravellerFromCurrentCart(cartEntryPropertiesData.getTravellerCode());
		final Optional<AbstractOrderEntryModel> optionalOrderEntryModel;
		if (Objects.nonNull(traveller))
		{
			travellersList.add(traveller);
			optionalOrderEntryModel = cartModel.getEntries().stream()
					.filter(aoem -> aoem.getJourneyReferenceNumber() == cartEntryPropertiesData.getJourneyRefNumber()
							&& aoem.getTravelOrderEntryInfo() != null
							&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
							&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartEntryPropertiesData
							.getOriginDestinationRefNo()
							&& aoem.getTravelOrderEntryInfo().getTravellers().contains(traveller))
					.findFirst();
		}
		else
		{
			optionalOrderEntryModel = cartModel.getEntries().stream()
					.filter(aoem -> aoem.getJourneyReferenceNumber() == cartEntryPropertiesData.getJourneyRefNumber()
							&& aoem.getTravelOrderEntryInfo() != null
							&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
							&& aoem.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == cartEntryPropertiesData
							.getOriginDestinationRefNo())
					.findFirst();
		}

		TravelOrderEntryInfoModel travelEntryInfoModel = null;
		if (optionalOrderEntryModel.isPresent())
		{
			travelEntryInfoModel = optionalOrderEntryModel.get().getTravelOrderEntryInfo();
		}
		final Map<String, Map<String, Object>> propertiesMaps = populatePropertiesMap(cartEntryPropertiesData, travellersList,
				travelEntryInfoModel);
		((BCFTravelCommerceCartService) getTravelCommerceCartService()).persistAdditionalPropertiesForCartEntry(cartModel,
				cartEntryPropertiesData.getEntryNumber(), product, propertiesMaps);
	}

	protected Map<String, Map<String, Object>> populatePropertiesMap(final CartEntryPropertiesData addPropertiesToCartEntryData,
			final List<TravellerModel> travellersList, final TravelOrderEntryInfoModel travelEntryInfoModel)
	{
		String addToCartCriteria = addPropertiesToCartEntryData.getAddToCartCriteria();
		if (StringUtils.isEmpty(addToCartCriteria))
		{
			addToCartCriteria = TravelfacadesConstants.DEFAULT_ADD_TO_CART_CRITERIA;
		}

		final BCFPopulatePropertyMapStrategy strategy = getBcfPopulateCartEntryPropertyStrategyMap().get(addToCartCriteria);

		return strategy.populatePropertiesMap(addPropertiesToCartEntryData, travellersList, travelEntryInfoModel);
	}

	@Override
	public Boolean saveAgentComment(final String agentComment)
	{
		return getBcfTravelCartService().saveAgentComment(agentComment);
	}

	@Override
	public String getAgentComment()
	{
		return getBcfTravelCartService().getAgentComment();
	}

	/*
	 * get max sailing count allowed for different user type
	 *
	 */
	@Override
	public int getMaxSailingAllowed()
	{
		final UserModel user = userService.getCurrentUser();
		String maxAllowed = null;
		if (userService.isAnonymousUser(user))
		{
			maxAllowed = getBcfConfigurablePropertiesService().getBcfPropertyValue("maxSailingAllowedForAnonymousUser");
		}
		else
		{
			maxAllowed = getBcfConfigurablePropertiesService().getBcfPropertyValue("maxSailingAllowedForLoggedInUser");
		}
		return Integer.parseInt(maxAllowed);
	}

	@Override
	public void evaluateCart()
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries().stream().filter(AbstractOrderEntryModel::getActive)
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(cartEntries))
		{
			return;
		}
		cartEntries.forEach(cartEntry -> {
			final Map<String, Object> propertiesMap = new HashMap<>();
			propertiesMap.put(AbstractOrderEntryModel.ACTIVE, Boolean.TRUE);
			propertiesMap.put(AbstractOrderEntryModel.AMENDSTATUS, AmendStatus.NEW);
			getTravelCommerceCartService().addPropertiesToCartEntry(cartModel, cartEntry.getEntryNumber(), cartEntry.getProduct(),
					propertiesMap);
		});
	}

	@Override
	public void evaluateCartForJourneyRefNumber(final int journeyRefNumber, final int odRefNumber)
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries().stream().filter(AbstractOrderEntryModel::getActive)
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(cartEntries))
		{
			return;
		}
		cartEntries.stream().filter(
				entry -> entry.getJourneyReferenceNumber() == journeyRefNumber && entry.getTravelOrderEntryInfo() != null
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && odRefNumber == entry
						.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).forEach(cartEntry -> {
			final Map<String, Object> propertiesMap = new HashMap<>();
			propertiesMap.put(AbstractOrderEntryModel.ACTIVE, Boolean.TRUE);
			propertiesMap.put(AbstractOrderEntryModel.AMENDSTATUS, AmendStatus.NEW);
			getTravelCommerceCartService().addPropertiesToCartEntry(cartModel, cartEntry.getEntryNumber(), cartEntry.getProduct(),
					propertiesMap);
		});
	}

	@Override
	public List<RemoveSailingResponseData> emptyBasket() throws IntegrationException
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		//no need to call cancel service if user is of type GUEST
		if (getUserService().isAnonymousUser(getUserService().getCurrentUser()) || isGuestUserCart(cartModel))
		{
			deleteCurrentCart();
			return Collections.emptyList();
		}

		final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
				.cancelSailingsInCart(null, true);

		if (CollectionUtils.isNotEmpty(removeSailingResponseDatas) && CollectionUtils
				.isEmpty(getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, false)))
		{
			deleteCurrentCart();
			return Collections.emptyList();
		}
		final List<RemoveSailingResponseData> cancelBookingSuccessData = getCancelBookingIntegrationFacade()
				.filterCancelBookingList(removeSailingResponseDatas, true);
		cancelBookingSuccessData.forEach(data -> {
			final List<AbstractOrderEntryModel> journeyOrderEntries = cartModel.getEntries().stream()
					.filter(entry -> entry.getActive() && entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& entry.getJourneyReferenceNumber() == data.getJourneyRefNumber()
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == data.getOriginDestinationRefNumber())
					.collect(Collectors.toList());
			modifyAbstractOrderEntriesForRefNo(cartModel, data.getJourneyRefNumber(), data.getOriginDestinationRefNumber(),
					getTripType(journeyOrderEntries).equals(TripType.SINGLE), false);
		});
		return getCancelBookingIntegrationFacade().filterCancelBookingList(removeSailingResponseDatas, false);
	}



	/**
	 * @return the bcfTravelCartService
	 */
	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	public CartFilterParamsDto initializeCartFilterParams(final Integer journeyRefNo, final Integer odRefNo, final Boolean commit,
			final List<String> routes)
	{
		final CartFilterParamsDto cartFilterParams = new CartFilterParamsDto();
		cartFilterParams.setCommit(commit);
		cartFilterParams.setJourneyRefNo(journeyRefNo);
		cartFilterParams.setOdRefNo(odRefNo);
		cartFilterParams.setRoutes(routes);
		return cartFilterParams;
	}

	@Override
	public boolean updateAmendBookingFeesPrice(final String amount, final String cachingKey)
	{
		final boolean canAmendFees = Boolean.parseBoolean(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.AMEND_EBOOKING_AMENDMENT_BOOKING_FEES));
		if (!canAmendFees)
		{
			return Boolean.FALSE;
		}
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		final Optional<AbstractOrderEntryModel> amendFees = cartModel.getEntries().stream()
				.filter(entry -> entry.getActive() && entry.getAmendStatus().equals(AmendStatus.NEW)
						&& entry.getProduct() instanceof FeeProductModel && entry.getCacheKey().equals(cachingKey))
				.findAny();
		if (!amendFees.isPresent())
		{
			return Boolean.FALSE;
		}

		amendFees.get().setBasePrice(amendFees.get().getBasePrice() - Integer.parseInt(amount));
		cartModel.setAmountToPay(cartModel.getAmountToPay() - Integer.parseInt(amount));
		getModelService().saveAll();
		getModelService().refresh(cartModel);
		recalculateCart();
		return Boolean.TRUE;
	}


	@Override
	public void removeAmendBookingFees() throws ModelRemovalException
	{
		if (getTravelCartService().hasSessionCart())
		{
			final CartModel cart = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
			if (cart.getEntries().stream().noneMatch(entry -> entry.getActive() && entry.getAmendStatus().equals(AmendStatus.NEW)
					&& !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE)))
			{
				final List<AbstractOrderEntryModel> feeEntries = cart.getEntries().stream().filter(entry -> entry.getActive()
						&& entry.getAmendStatus().equals(AmendStatus.NEW) && (entry.getProduct() instanceof FeeProductModel))
						.collect(Collectors.toList());
				getModelService().removeAll(feeEntries);
				getModelService().refresh(cart);
			}
		}
	}

	public Map<String, BCFPopulatePropertyMapStrategy> getBcfPopulateCartEntryPropertyStrategyMap()
	{
		return bcfPopulateCartEntryPropertyStrategyMap;
	}

	public void setBcfPopulateCartEntryPropertyStrategyMap(
			final Map<String, BCFPopulatePropertyMapStrategy> bcfPopulateCartEntryPropertyStrategyMap)
	{
		this.bcfPopulateCartEntryPropertyStrategyMap = bcfPopulateCartEntryPropertyStrategyMap;
	}

	@Override
	public PriceData getAmountToPay()
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		return getTravelCommercePriceFacade().createPriceData(
				cartModel.getAmountToPay(),
				cartModel.getCurrency().getIsocode());
	}

	@Override
	public PriceData getPayAtTerminal()
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		return getTravelCommercePriceFacade()
				.createPriceData(((Objects.isNull(cartModel.getPayAtTerminal()) ? 0d : cartModel.getPayAtTerminal()) / 100.0),
						cartModel.getCurrency().getIsocode());
	}

	@Override
	public AddToCartResponseData createAddToCartResponse(final boolean valid, final String errorMessage,
			final Integer minOriginDestinationRefNumber, final String errorCode)
	{
		final AddToCartResponseData response = new AddToCartResponseData();
		response.setValid(valid);
		if (StringUtils.isNotEmpty(errorMessage))
		{
			response.setErrors(Collections.singletonList(errorMessage));
		}
		if (minOriginDestinationRefNumber != null)
		{
			response.setMinOriginDestinationRefNumber(minOriginDestinationRefNumber);
		}
		if (StringUtils.isNotBlank(errorCode))
		{
			response.setErrorCode(errorCode);
		}
		return response;
	}

	@Override
	public List<CartModificationData> createActivityOrderEntryInfo(final String code,
			final AddActivityToCartData addActivityToCartData) throws CommerceCartModificationException
	{
		List<CartModificationData> cartModificationList = null;
		final Transaction currentTransaction = Transaction.current();
		boolean executed = false;

		try
		{
			currentTransaction.begin();

			cartModificationList = createActivityOrderEntryInfoInternal(code, addActivityToCartData);

			final CartModel cart = getCartService().getSessionCart();

			final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();

			bcfStockPreprocessor.validateAndReserve(decisionData, cart);
			executed = true;
		}
		catch (final CartValidationException | InsufficientStockLevelException | OrderProcessingException ex)
		{
			LOG.error("Error while addAccommodationsToCartForPromotions:", ex);
			throw new CommerceCartModificationException("Stock Reserve Error:", ex);
		}
		catch (final CommerceCartModificationException ex)
		{
			throw ex;
		}
		finally
		{
			if (executed)
			{
				currentTransaction.commit();
			}
			else
			{
				currentTransaction.rollback();
			}
		}
		return cartModificationList;
	}


	private List<CartModificationData> createActivityOrderEntryInfoInternal(final String code,
			final AddActivityToCartData addActivityToCartData) throws CommerceCartModificationException
	{
		final List<CartModificationData> cartModificationList = new ArrayList<>();
		for (final GuestData guestData : addActivityToCartData.getGuestData())
		{
			if (guestData.getQuantity() > 0)
			{
				final CartModificationData cartModification = addToCart(code, guestData.getQuantity());
				final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
				setOrderEntryType(OrderEntryType.ACTIVITY, cartModification.getEntry().getEntryNumber());

				final Map<String, Object> propertiesMap = new HashMap<>();
				propertiesMap.put(AbstractOrderEntryModel.ACTIVE, Boolean.TRUE);
				propertiesMap.put(AbstractOrderEntryModel.AMENDSTATUS, AmendStatus.NEW);
				if (StringUtils.isNotBlank(addActivityToCartData.getActivityDate()))
				{
					propertiesMap
							.put(ActivityOrderEntryInfoModel.ACTIVITYDATE,
									convertStringDateToDate(addActivityToCartData.getActivityDate(),
											BcfFacadesConstants.ACTIVITY_DATE_PATTERN)
							);
				}
				if (StringUtils.isNotBlank(addActivityToCartData.getStartTime()))
				{
					propertiesMap.put(ActivityOrderEntryInfoModel.ACTIVITYTIME, addActivityToCartData.getStartTime());
				}
				propertiesMap.put(ActivityOrderEntryInfoModel.GUESTTYPE, GuestType.valueOf(guestData.getGuestType()));
				final ProductModel product = getProductService().getProductForCode(code);
				bcfTravelCartService.addPropertiesToCartEntry(cartModel, cartModification.getEntry().getEntryNumber(),
						product, propertiesMap);
				cartModificationList.add(cartModification);
			}
		}
		recalculateCart();
		return cartModificationList;
	}


	public static Date convertStringDateToDate(final String date, final String pattern)
	{
		try
		{
			final LocalDateTime dateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(pattern));
			return java.util.Date
					.from(dateTime.atZone(ZoneId.systemDefault())
							.toInstant());
		}
		catch (final DateTimeParseException var3)
		{
			LOG.error("Unable to parse String " + date + " to Date.");
			LOG.debug(var3);
			return null;
		}

	}

	@Override
	public List<CartModificationData> createActivityOrderEntryInfoForDeal(final String code,
			final AddActivityToCartData addActivityToCartData) throws CommerceCartModificationException
	{
		final List<CartModificationData> cartModificationList = createActivityOrderEntryInfo(code, addActivityToCartData);
		attachDealOrderEntryGroup(addActivityToCartData.getJourneyRefNumber(), cartModificationList);
		return cartModificationList;
	}

	@Override
	public void attachDealOrderEntryGroup(final int journeyRefNumber,
			final List<CartModificationData> cartModificationList)
	{
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		getBcfTravelCartService()
				.updateDealOrderEntryGroup(cartModel, journeyRefNumber,
						cartModificationList.stream().map(cartModification -> cartModification.getEntry().getEntryNumber())
								.collect(Collectors.toList()));
	}

	@Override
	public String getOriginalOrderCode()
	{
		if (!getTravelCartService().hasSessionCart())
		{
			return "";
		}
		else
		{
			final OrderModel orderModel = getBcfTravelCartService().getOrCreateSessionCart().getOriginalOrder();
			return orderModel == null ? "" : orderModel.getCode();
		}
	}

	@Override
	public void addExtraProductToCart(final BCFExtraProducts bcfExtraProducts) throws CommerceCartModificationException
	{
		bcfExtraProducts.getBcfExtraProductPerJourneyList().forEach(journeyProduduct -> {

			List<AbstractOrderEntryModel> fareProductEntries = Collections.emptyList();
			fareProductEntries = getCartEntriesForRefNo(journeyProduduct.getJourneyReferenceNumber(), 0);
			AddBundleToCartRequestData outboundMakeBookingRequestData = null;
			if (Objects.nonNull(journeyProduduct.getOutboundExtraProductDataList()))
			{
				outboundMakeBookingRequestData = getAddBundleToCartRequestData(
						fareProductEntries,
						journeyProduduct.getOutboundExtraProductDataList(), journeyProduduct.getJourneyReferenceNumber(), 0);
			}
			try
			{
				if (Objects.nonNull(outboundMakeBookingRequestData)
						&& outboundMakeBookingRequestData.getAccessibilityDatas().size() > 0)
				{
					final BCFMakeBookingResponse outBoundresponse = getMakeBookingIntegrationFacade()
							.getMakeBookingResponse(outboundMakeBookingRequestData);
				}
				populateInboundResponse(journeyProduduct);
			}
			catch (final IntegrationException ex)
			{
				LOG.error("MakeBooking Failure ", ex);
			}

		});
		final CartModel cartModel = (CartModel) getBcfTravelCartService().getOrCreateSessionCart();
		for (final BCFExtraProductPerJourney bcfExtraProductPerJourney : bcfExtraProducts.getBcfExtraProductPerJourneyList())
		{
			if (Objects.nonNull(bcfExtraProductPerJourney.getOutboundExtraProductDataList()))
			{
				updateOrAddExtraProductToCart(getExtraProductsPerOD(bcfExtraProductPerJourney.getOutboundExtraProductDataList()),
						bcfExtraProductPerJourney.getJourneyReferenceNumber(), BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER,
						cartModel,
						createTravelOrderEntryInfoForAncillary(bcfExtraProductPerJourney, BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER));
			}
			if (Objects.nonNull(bcfExtraProductPerJourney.getInboundExtraProductDataList()))
			{
				updateOrAddExtraProductToCart(getExtraProductsPerOD(bcfExtraProductPerJourney.getInboundExtraProductDataList()),
						bcfExtraProductPerJourney.getJourneyReferenceNumber(), BcfCoreConstants.INBOUND_REFERENCE_NUMBER,
						cartModel,
						createTravelOrderEntryInfoForAncillary(bcfExtraProductPerJourney, BcfCoreConstants.INBOUND_REFERENCE_NUMBER));
			}
		}
		getModelService().refresh(cartModel);
	}

	private void populateInboundResponse(final BCFExtraProductPerJourney journeyProduduct) throws IntegrationException
	{
		final List<AbstractOrderEntryModel> fareProductEntries;
		if (Objects.nonNull(journeyProduduct.getInboundExtraProductDataList()))
		{
			fareProductEntries = getCartEntriesForRefNo(journeyProduduct.getJourneyReferenceNumber(), 1);
			final AddBundleToCartRequestData inboundMakeBookingRequestData = getAddBundleToCartRequestData(
					fareProductEntries,
					journeyProduduct.getInboundExtraProductDataList(), journeyProduduct.getJourneyReferenceNumber(), 1);
			if (Objects.nonNull(inboundMakeBookingRequestData)
					&& inboundMakeBookingRequestData.getAccessibilityDatas().size() > 0)
			{
				getMakeBookingIntegrationFacade()
						.getMakeBookingResponse(inboundMakeBookingRequestData);
			}
		}
	}

	protected AddBundleToCartRequestData getAddBundleToCartRequestData(final List<AbstractOrderEntryModel> fareProductEntries,
			final List<ExtraProductPerVessel> extraProducts, final int journeyRefNo, final int odRefNo)
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
		final List<AccessibilitytData> accessibilitytDataList = new ArrayList<>();
		extraProducts.forEach(extraProductPerVessel -> {
			accessibilitytDataList
					.addAll(getAccessibilityData(extraProductPerVessel.getCabinProductList(), ExtrasType.CABIN));
			accessibilitytDataList.addAll(getAccessibilityData(extraProductPerVessel.getLoungeProductList(), ExtrasType.LOUNGE));
			accessibilitytDataList.addAll(getAccessibilityData(extraProductPerVessel.getMealProductList(), ExtrasType.MEAL));
		});
		addBundleToCartRequestData.setAccessibilityDatas(accessibilitytDataList);
		addBundleToCartRequestData.setSelectedJourneyRefNumber(journeyRefNo);
		addBundleToCartRequestData.setSelectedOdRefNumber(odRefNo);
		final BcfAddBundleToCartData addBundleToCartData = new BcfAddBundleToCartData();
		if (CollectionUtils.isNotEmpty(fareProductEntries) && accessibilitytDataList.size() > 0)
		{
			final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();
			final AbstractOrderEntryModel firstFareProductEntryInCart = fareProductEntries.get(0);
			addBundleToCartData.setTravelRouteCode(firstFareProductEntryInCart.getTravelOrderEntryInfo().getTravelRoute().getCode());
			firstFareProductEntryInCart.getTravelOrderEntryInfo().getTransportOfferings().forEach(transportOfferingModel -> {
				final TransportOfferingData transportOfferingData = new TransportOfferingData();
				transportOfferingData.setDepartureTime(transportOfferingModel.getDepartureTime());
				transportOfferingData.setArrivalTime(transportOfferingModel.getArrivalTime());
				transportOfferingDataList.add(transportOfferingData);
			});
			final List<TravellerModel> travellers = fareProductEntries.stream().map(AbstractOrderEntryModel::getTravelOrderEntryInfo)
					.flatMap(travelOrderEntryInfoModel -> travelOrderEntryInfoModel.getTravellers().stream()).collect(
							Collectors.toList());

			final List<TravellerData> travellerDataList = getTravellerDataConverter().convertAll(travellers);

			final List<PassengerTypeQuantityData> passengerTypes = new ArrayList<>();
			final List<VehicleTypeQuantityData> vehicleTypes = new ArrayList<>();

			addBundleToCartRequestData.setPassengerTypes(getPassengerTravellers(travellerDataList, passengerTypes));
			addBundleToCartRequestData.setVehicleTypes(getVehicleTraveller(travellerDataList, vehicleTypes));
			addBundleToCartData.setTransportOfferingDatas(transportOfferingDataList);
			addBundleToCartRequestData.setBundleType(firstFareProductEntryInCart.getBundleTemplate().getType().getCode());
		}
		addBundleToCartData.setOriginDestinationRefNumber(odRefNo);

		addBundleToCartRequestData.setAddBundleToCartData(Arrays.asList(addBundleToCartData));
		return addBundleToCartRequestData;
	}

	protected List<PassengerTypeQuantityData> getPassengerTravellers(final List<TravellerData> travellers,
			final List<PassengerTypeQuantityData> passengerTypes)
	{
		final Map<String, List<PassengerTypeData>> passengersByCode = travellers.stream()
				.filter(travellerData -> TravellerType.PASSENGER.getCode().equals(travellerData.getTravellerType()))
				.map(traveller -> (PassengerInformationData) traveller.getTravellerInfo())
				.map(PassengerInformationData::getPassengerType)
				.collect(Collectors.groupingBy(passengerTypeData -> passengerTypeData.getBcfCode()));

		for (final Map.Entry<String, List<PassengerTypeData>> mapEntry : passengersByCode.entrySet())
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(mapEntry.getValue().stream().findFirst().get());
			passengerTypeQuantityData.setQuantity(mapEntry.getValue().size());
			passengerTypes.add(passengerTypeQuantityData);
		}

		return passengerTypes;
	}

	protected List<VehicleTypeQuantityData> getVehicleTraveller(final List<TravellerData> travellers,
			final List<VehicleTypeQuantityData> vehicleTypes)
	{
		final Supplier<Stream<VehicleInformationData>> vehicleInfoStreamSupplier = () -> travellers.stream()
				.filter(travellerData -> TravellerType.VEHICLE.getCode().equals(travellerData.getTravellerType()))
				.map(traveller -> (VehicleInformationData) traveller.getTravellerInfo());

		final Map<String, List<VehicleTypeData>> vehiclesByCode = vehicleInfoStreamSupplier.get()
				.map(VehicleInformationData::getVehicleType)
				.collect(Collectors.groupingBy(vehicleTypeData -> vehicleTypeData.getCode()));

		for (final Map.Entry<String, List<VehicleTypeData>> mapEntry : vehiclesByCode.entrySet())
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			vehicleTypeQuantityData.setVehicleType(mapEntry.getValue().stream().findFirst().get());
			vehicleTypeQuantityData.setQty(mapEntry.getValue().size());
			final VehicleInformationData vehicleInformationData = vehicleInfoStreamSupplier.get().findFirst().get();
			vehicleTypeQuantityData.setLength((int) vehicleInformationData.getLength());
			vehicleTypeQuantityData.setWeight((int) vehicleInformationData.getWeight());
			vehicleTypeQuantityData.setWidth((int) vehicleInformationData.getWidth());
			vehicleTypeQuantityData.setHeight((int) vehicleInformationData.getHeight());

			vehicleTypes.add(vehicleTypeQuantityData);
		}

		return vehicleTypes;
	}

	protected List<AccessibilitytData> getAccessibilityData(final List<? extends ExtraProductData> extraProductData,
			final ExtrasType extrasType)
	{
		final List<AccessibilitytData> accessibilitytDataList = new ArrayList<>();
		if (Objects.nonNull(extraProductData) && extraProductData.size() > 0)
		{
			extraProductData.forEach(extraProduct -> {
				if (extraProduct.getCount() > 0)
				{
					for (int count = 0; count < extraProduct.getCount(); count++)
					{
						final AccessibilitytData accessibilitytData = new AccessibilitytData();
						accessibilitytData.setCode(extraProduct.getCode());
						accessibilitytData.setType(extrasType.getCode());
						accessibilitytDataList.add(accessibilitytData);
					}
				}
			});
		}
		return accessibilitytDataList;

	}

	private Map<String, Integer> getExtraProductsPerOD(final List<ExtraProductPerVessel> bcfExtraProductPerJourney)
	{
		final Map<String, Integer> extraProductPerODMap = new HashMap<>();
		for (final ExtraProductPerVessel extraProductPerVessel : bcfExtraProductPerJourney)
		{
			extraProductPerCabin(extraProductPerODMap, extraProductPerVessel);
			if (Objects.nonNull(extraProductPerVessel.getMealProductList()) && extraProductPerVessel.getMealProductList().size() > 0)
			{
				for (final MealProduct mealProduct : extraProductPerVessel.getMealProductList())
				{
					extraProductPerODMap.put(mealProduct.getCode(), mealProduct.getCount());
				}
			}
			if (Objects.nonNull(extraProductPerVessel.getLoungeProductList())
					&& extraProductPerVessel.getLoungeProductList().size() > 0)
			{
				for (final LoungeProduct loungeProduct : extraProductPerVessel.getLoungeProductList())
				{
					extraProductPerODMap.put(loungeProduct.getCode(), loungeProduct.getCount());
				}
			}
		}
		return extraProductPerODMap;
	}

	private void extraProductPerCabin(final Map<String, Integer> extraProductPerODMap,
			final ExtraProductPerVessel extraProductPerVessel)
	{
		if (Objects.nonNull(extraProductPerVessel.getCabinProductList())
				&& extraProductPerVessel.getCabinProductList().size() > 0)
		{
			for (final CabinProduct cabinProduct : extraProductPerVessel.getCabinProductList())
			{
				extraProductPerODMap.put(cabinProduct.getCode(), cabinProduct.getCount());
			}
		}
	}

	private void updateOrAddExtraProductToCart(final Map<String, Integer> extraProductPerODMap, final int journeyReferenceNumber,
			final int originDestinationNum, final CartModel cartModel, final TravelOrderEntryInfoModel travelOrderEntryInfoModel)
			throws CommerceCartModificationException
	{
		for (final Map.Entry<String, Integer> extraProductPerOD : extraProductPerODMap.entrySet())
		{
			if (extraProductPerOD.getValue() > 0)
			{
				if (getCartEntriesForRefNo(journeyReferenceNumber, originDestinationNum)
						.stream().filter(cartEntry -> cartEntry.getProduct().getCode().equals(extraProductPerOD.getKey())).findFirst()
						.isPresent())
				{
					final AbstractOrderEntryModel entry = getCartEntriesForRefNo(journeyReferenceNumber, originDestinationNum)
							.stream().filter(cartEntry -> cartEntry.getProduct().getCode().equals(extraProductPerOD.getKey()))
							.findFirst().get();

					entry.setQuantity(extraProductPerOD.getValue().longValue());
					getModelService().save(entry);
				}
				else
				{
					final CartModificationData cartModificationData = super
							.addToCart(extraProductPerOD.getKey(), extraProductPerOD.getValue());
					setOrderEntryType(OrderEntryType.TRANSPORT, cartModificationData.getEntry().getEntryNumber());
					getBcfTravelCartFacadeHelper()
							.addProductToCartEntry(extraProductPerOD.getKey(), cartModel, cartModificationData, originDestinationNum,
									journeyReferenceNumber);
					final AbstractOrderEntryModel createdEntry = cartModel.getEntries().stream()
							.filter(entryModel -> entryModel.getEntryNumber() == cartModificationData.getEntry().getEntryNumber())
							.findFirst().get();
					createdEntry.setTravelOrderEntryInfo(travelOrderEntryInfoModel);
					getModelService().save(createdEntry);
				}
			}
		}
	}

	private TravelOrderEntryInfoModel createTravelOrderEntryInfoForAncillary(
			final BCFExtraProductPerJourney bcfExtraProductPerJourney, final int odReferenceNumber)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo = getModelService().create(TravelOrderEntryInfoModel.class);
		travelOrderEntryInfo.setTravelRoute(getTravelRouteService().getTravelRoute(bcfExtraProductPerJourney.getRouteCode()));
		travelOrderEntryInfo.setTransportOfferings(Collections.singletonList(
				getTransportOfferingService().getTransportOffering(bcfExtraProductPerJourney.getTransportOfferingCode())));
		travelOrderEntryInfo.setOriginDestinationRefNumber(odReferenceNumber);
		return travelOrderEntryInfo;
	}

	@Override
	public List<DealOrderEntryGroupModel> getDealOrderEntryGroups(final AbstractOrderModel abstractOrder)
	{
		final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService()
				.getDealOrderEntryGroups(abstractOrder);
		if (CollectionUtils.isEmpty(dealOrderEntryGroupModels))
		{
			return Collections.emptyList();
		}
		return dealOrderEntryGroupModels;
	}

	@Override
	public void updateCartWithDealOrderEntryGroups(final List<DealOrderEntryGroupModel> dealOrderEntryGroups,
			final AbstractOrderModel abstractOrder)
	{
		for (final DealOrderEntryGroupModel dealOrderEntryGroup : dealOrderEntryGroups)
		{
			abstractOrder.getEntries().stream().filter(entry -> entry.getActive())
					.forEach(entry -> entry.setEntryGroup(dealOrderEntryGroup));
		}
		getModelService().saveAll();
	}

	@Override
	public String getPackageAvailabilityStatus()
	{
		final CartModel sessionCart = getBcfTravelCartService().getSessionCart();
		return sessionCart.getAvailabilityStatus();
	}


	@Override
	public boolean getActivityAvailabilityStatus(final String activityProductCode, final String activitySelectedDate,
			final String activitySelectedTime, final int passengerQuantity) throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);
		final Date activityDate;
		if (StringUtils.isNotEmpty(activitySelectedDate))
		{
			activityDate = sdf.parse(activitySelectedDate);
		}
		else
		{
			activityDate = sdf.parse(sdf.format(new Date()));
		}

		return isActivityStockAvailable(activityProductCode, passengerQuantity, activityDate, activitySelectedTime);
	}

	private boolean isActivityStockAvailable(final String activityProductCode, final int passengerQuantity,
			final Date activityDate, final String activitySelectedTime)
	{

		final ActivityProductModel activityProduct = (ActivityProductModel) getProductService()
				.getProductForCode(activityProductCode);

		StockData stockData = null;

		if (StringUtils.isNotEmpty(activitySelectedTime))
		{
			stockData = getStockDataForDateAndTime(activityProduct, activityDate, activitySelectedTime, passengerQuantity);
		}
		else
		{
			stockData = ((BcfProductFacade) getProductFacade()).getStockData(activityProduct, activityDate,
					passengerQuantity,
					Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
		}

		if (Objects.isNull(stockData))
		{
			return false;
		}

		return StockLevelType.ONREQUEST.equals(stockData.getStockLevelType()) || StockLevelType.STANDARD
				.equals(stockData.getStockLevelType()) || StockLevelType.FREE.equals(stockData.getStockLevelType());
	}

	private String getActivityStockAvailableStatus(final ActivityProductModel activityProduct, final int passengerQuantity,
			final Date activityDate, final String activitySelectedTime)
	{

		if (StockLevelType.ONREQUEST.equals(activityProduct.getStockLevelType()))
		{
			return StockLevelType.ONREQUEST.getCode();
		}

		final StockData stockData;

		if (StringUtils.isNotEmpty(activitySelectedTime))
		{
			stockData = getStockDataForDateAndTime(activityProduct, activityDate, activitySelectedTime, passengerQuantity);
		}
		else
		{
			stockData = ((BcfProductFacade) getProductFacade()).getStockData(activityProduct, activityDate,
					passengerQuantity,
					Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
		}

		return Objects.isNull(stockData) ? null : stockData.getStockLevelType().getCode();
	}

	@Override
	public StockData getStockDataForDateAndTime(final String activityProductCode, final Date activityDate,
			final String activitySelectedTime, final int passengerQuantity)
	{
		final ActivityProductModel activityProduct = (ActivityProductModel) getProductService()
				.getProductForCode(activityProductCode);
		return getStockDataForDateAndTime(activityProduct, activityDate, activitySelectedTime, passengerQuantity);
	}

	private StockData getStockDataForDateAndTime(final ActivityProductModel activityProduct, final Date activityDate,
			final String activitySelectedTime, final int passengerQuantity)
	{
		StockLevelModel stockLevelWithMatchedDateAndTime = null;
		final StockLevelType stockLevelType = getBcfTravelCommerceStockService().getStockLevelType(activityProduct);
		if (Objects.equals(StockLevelType.STANDARD, stockLevelType))
		{
			final List<StockLevelModel> stockLevels = bcfTravelStockService
					.getStockLevelsForProductAndDate(activityProduct.getCode(), activityDate,
							getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE));

			if (CollectionUtils.isEmpty(stockLevels))
			{
				return null;
			}

			stockLevelWithMatchedDateAndTime = stockLevels.stream()
					.filter(s -> StringUtils.isNotEmpty(s.getTime()) && s.getTime().equals(activitySelectedTime)).findFirst()
					.orElse(null);

			if (Objects.isNull(stockLevelWithMatchedDateAndTime))
			{
				return null;
			}
		}

		return ((BcfProductFacade) getProductFacade()).getStockData(activityProduct, activityDate,
				passengerQuantity, stockLevelWithMatchedDateAndTime);
	}


	/**
	 * method that checks the availability of the activity product. This method should be used as a main method to check availability of any activity product.
	 * <p>
	 * It also takes consideration of SOLD_OUT sales status.
	 * </p>
	 *
	 * @param activityProductCode
	 * @param activitySelectedDate
	 * @param activitySelectedTime
	 * @param passengerQuantity
	 * @return
	 */
	@Override
	public String getActivityAvailabilityStatus(final String activityProductCode, final Date activitySelectedDate,
			final String activitySelectedTime, final int passengerQuantity)
	{

		final ActivityProductModel activityProduct = (ActivityProductModel) getProductService()
				.getProductForCode(activityProductCode);

		if (!isActivityAvailableForSale(activityProduct.getSaleStatuses(), activitySelectedDate))
		{
			return AvailabilityStatus.SOLD_OUT.name();
		}

		if (StockLevelType.STANDARD.equals(activityProduct.getStockLevelType()))
		{

			final String stockAvailabilityStatus = getActivityStockAvailableStatus(activityProduct, passengerQuantity,
					activitySelectedDate, activitySelectedTime);
			if (StringUtils.isNotBlank(stockAvailabilityStatus))
			{
				if (StockLevelType.ONREQUEST.name().equals(stockAvailabilityStatus))
				{
					return AvailabilityStatus.ON_REQUEST.name();
				}
				else if (StockLevelType.STANDARD.getCode().equals(stockAvailabilityStatus) || StockLevelType.FREE.getCode()
						.equals(stockAvailabilityStatus))
				{
					return AvailabilityStatus.AVAILABLE.name();
				}
			}
			else
			{
				return AvailabilityStatus.SOLD_OUT.name();
			}
		}

		if (StockLevelType.ONREQUEST.equals(activityProduct.getStockLevelType()))
		{
			return AvailabilityStatus.ON_REQUEST.name();
		}
		else
		{
			return AvailabilityStatus.AVAILABLE.name();
		}
	}

	@Override
	public boolean isAmendCurrentCartValid()
	{
		//this is added to handle the case when Quote is converted to Cart.
		//The amend status in this case will be SAME, since we have clonned the cart while creating quote
		if (!isAmendmentCart())
		{
			return true;
		}
		if (this.getCartService().hasSessionCart())
		{
			final CartModel sessionCart = this.getCartService().getSessionCart();
			if (CollectionUtils.isNotEmpty(sessionCart.getEntries()))
			{
				return sessionCart.getEntries().stream().anyMatch(
						entry -> AmendStatus.NEW.equals(entry.getAmendStatus()) || AmendStatus.CHANGED.equals(entry.getAmendStatus()));
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isCurrentCartValid()
	{
		if (this.getCartService().hasSessionCart())
		{
			return super.isCurrentCartValid();
		}
		else
		{
			return false;
		}
	}

	private boolean isActivityAvailableForSale(final Collection<ActivitySaleStatusModel> saleStatuses,
			final Date activitySelectedDate)
	{
		if (org.apache.commons.collections4.CollectionUtils.isEmpty(saleStatuses))
		{
			return true;
		}


		final ActivitySaleStatusModel openSaleStatus = saleStatuses.stream()
				.filter(status -> SaleStatusType.OPEN.equals(status.getSaleStatusType()) && BCFDateUtils
						.isDateInDateRange(activitySelectedDate, BCFDateUtils.setToStartOfDay(status.getStartDate()),
								BCFDateUtils.setToEndOfDay(status.getEndDate())))
				.findFirst().orElse(null);

		if (openSaleStatus != null)
		{
			return true;
		}


		final ActivitySaleStatusModel stopSaleStatus = saleStatuses.stream()
				.filter(status -> (SaleStatusType.STOPSALE.equals(status.getSaleStatusType()) || SaleStatusType.CLOSED
						.equals(status.getSaleStatusType()) || SaleStatusType.NOT_BOOKABLE_ONLINE.equals(status.getSaleStatusType()))
						&& BCFDateUtils
						.isDateInDateRange(activitySelectedDate, BCFDateUtils.setToStartOfDay(status.getStartDate()),
								BCFDateUtils.setToEndOfDay(status.getEndDate())))
				.findFirst().orElse(null);

		if (stopSaleStatus == null)
		{
			return true;
		}

		return false;
	}


	@Override
	public boolean isOnRequestBookingPresent(final AbstractOrderModel abstractOrder)
	{
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(abstractOrder.getBookingJourneyType()))
		{
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	@Override
	public String getStockLevelType(final List<AbstractOrderEntryModel> abstractOrderEntries)
	{

		final List<AbstractOrderEntryModel> activityEntries = abstractOrderEntries.stream()
				.filter(entry -> ActivityProductModel._TYPECODE.equals(entry.getProduct().getItemtype())).collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(activityEntries))
		{
			final boolean activityStockNotAllocated = activityEntries.stream().anyMatch(
					activityEntry -> !activityEntry.isStockAllocated());

			if (activityStockNotAllocated)
			{
				return BcfCoreConstants.PACKAGE_ON_REQUEST;
			}

		}

		final List<AbstractOrderEntryModel> accommodationEntries = abstractOrderEntries.stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION
						.equals(entry.getType()) && RoomRateProductModel._TYPECODE.equals(entry.getProduct().getItemtype())).collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{
			final boolean accommodationStockNotAllocated = accommodationEntries.stream()
					.anyMatch(accommodationEntry -> !accommodationEntry.isStockAllocated());

			if (accommodationStockNotAllocated)
			{
				return BcfCoreConstants.PACKAGE_ON_REQUEST;
			}

		}

		return "";
	}

	private boolean isOnRequestAccommodation(final AbstractOrderEntryGroupModel abstractOrderEntryGroupModel)
	{

		Date startDate = null;
		Date endDate = null;
		AccommodationModel accommodationModel = null;
		AccommodationOfferingModel accommodationOfferingModel = null;
		AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = null;

		if (abstractOrderEntryGroupModel instanceof AccommodationOrderEntryGroupModel)
		{
			accommodationOrderEntryGroupModel = ((AccommodationOrderEntryGroupModel) abstractOrderEntryGroupModel);

		}
		else
		{
			accommodationOrderEntryGroupModel = ((DealOrderEntryGroupModel) abstractOrderEntryGroupModel)
					.getAccommodationEntryGroups().iterator().next();
		}

		startDate = accommodationOrderEntryGroupModel.getStartingDate();
		endDate = accommodationOrderEntryGroupModel.getEndingDate();
		accommodationModel = accommodationOrderEntryGroupModel.getAccommodation();
		accommodationOfferingModel = accommodationOrderEntryGroupModel.getAccommodationOffering();

		if (StockLevelType.ONREQUEST.equals(accommodationModel.getStockLevelType()))
		{
			return true;
		}

		for (Date date = startDate; !TravelDateUtils
				.isSameDate(date, endDate); date = DateUtils
				.addDays(date, 1))
		{
			final StockData stockData = getBcfProductFacade().getStockData(accommodationModel, date,
					Collections.singletonList(accommodationOfferingModel));

			if (stockData != null && (StockLevelType.ONREQUEST.equals(stockData.getStockLevelType())
					|| stockData.getStockLevel() != null && stockData.getStockLevel() <= 0))
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public void createAndSetAmendableCart(final String yBookingReference, final boolean searchEBooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException
	{
		final CartModel sessionAmendedCart = bcfTravelCartService.getExistingSessionAmendedCart();
		if (Objects.nonNull(sessionAmendedCart))
		{
			bcfTravelCartService.removeSessionCart();
		}
		String currentUserUid = bcfTravelBookingFacade.getCurrentUserUid();
		final OrderModel orderModel = getBcfBookingService().getOrder(yBookingReference);
		if (Objects.nonNull(orderModel) && Objects.equals(CustomerType.GUEST, ((CustomerModel) orderModel.getUser()).getType()))
		{
			currentUserUid = orderModel.getUser().getUid();
		}
		bcfTravelBookingFacade.createAmendableCart(yBookingReference, currentUserUid, searchEBooking);
	}

	@Override
	public List<RoomGuestData> getRoomDatas(final List<AbstractOrderEntryModel> entries)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels =  entries.stream().map(entry-> getBcfTravelCommerceCartService().getAccommodationOrderEntryGroup(entry)).filter(Objects::nonNull).distinct().collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{
			accommodationOrderEntryGroupModels.sort(Comparator.comparing(accommodationOrderEntryGroupModel->accommodationOrderEntryGroupModel.getStartingDate()));
			
			final List<RoomGuestData> roomGuestDataS = new ArrayList<>();
			for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel : accommodationOrderEntryGroupModels)
			{

				final RoomGuestData roomGuestData = new RoomGuestData();
				roomGuestData.setAccommodationCode(accommodationOrderEntryGroupModel.getAccommodation().getCode());
				roomGuestData.setRoomRefNumber(accommodationOrderEntryGroupModel.getRoomStayRefNumber());
				roomGuestData.setAccommodatonName(accommodationOrderEntryGroupModel.getAccommodationOffering().getName() + " - "
						+ accommodationOrderEntryGroupModel.getAccommodation().getName());
				roomGuestData.setCheckInDate(TravelDateUtils
						.convertDateToStringDate(accommodationOrderEntryGroupModel.getStartingDate(),
								BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
				roomGuestData.setFirstName(accommodationOrderEntryGroupModel.getFirstName());
				roomGuestData.setLastName(accommodationOrderEntryGroupModel.getLastName());
				roomGuestDataS.add(roomGuestData);
			}

			roomGuestDataS.sort(Comparator.comparing(roomGuestData->roomGuestData.getCheckInDate()));

			return roomGuestDataS;
		}

		return null;

	}

	@Override
	public List<ActivityGuestData> getUniqueActivityDatas(final List<AbstractOrderEntryModel> orderEntries)
	{
		final List<AbstractOrderEntryModel> entries = orderEntries.stream()
				.filter(entry -> entry.getActivityOrderEntryInfo() != null && OrderEntryType.ACTIVITY.equals(entry.getType()))
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(entries))
		{
			final List<ActivityGuestData> activityGuestDatas = new ArrayList<>();

			for (final AbstractOrderEntryModel entry : entries)
			{
				if (!activityGuestDatas.stream()
						.anyMatch(activityGuest -> activityGuest.getActivityCode().equals(entry.getProduct().getCode())))
				{
					final ActivityGuestData activityGuestData = new ActivityGuestData();
					activityGuestData.setActivityCode(entry.getProduct().getCode());
					activityGuestData.setFirstName(entry.getActivityOrderEntryInfo().getLeadPaxFirstName());
					activityGuestData.setLastName(entry.getActivityOrderEntryInfo().getLeadPaxLastName());
					activityGuestDatas.add(activityGuestData);
				}
			}

			return activityGuestDatas;

		}
		return null;
	}



	@Override
	public boolean isAmendingSailing()
	{
		final CartModel sessionAmendedCart = getBcfTravelCartService().getExistingSessionAmendedCart();
		return Objects.nonNull(sessionAmendedCart) && !getUserService()
				.isAnonymousUser(getUserService().getCurrentUser()) && !isGuestUserCart(sessionAmendedCart);
	}

	@Override
	public boolean isCarryingDangerousGoodsWalkOn(final int journeyRef, final int odRef)
	{
		final CartModel sessionCart = getTravelCartService().getSessionCart();
		if (Objects.nonNull(sessionCart))
		{
			final List<AbstractOrderEntryModel> transportEntries = sessionCart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
							&& journeyRef == entry.getJourneyReferenceNumber() && Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
							&& odRef == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(transportEntries))
			{
				final List<AbstractOrderEntryModel> vehicleEntries = StreamUtil.safeStream(transportEntries)
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.VEHICLE)).collect(Collectors.toList());
				if (CollectionUtils.isEmpty(vehicleEntries))
				{
					return transportEntries.stream().findFirst().get().getTravelOrderEntryInfo().isCarryingDangerousGoods();
				}
			}
		}
		return false;
	}

	@Override
	public boolean isCarryingDangerousGoodsWithVehicle(final int journeyRef, final int odRef)
	{
		final CartModel sessionCart = getTravelCartService().getSessionCart();
		if (Objects.nonNull(sessionCart))
		{
			final List<AbstractOrderEntryModel> transportEntries = sessionCart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
							&& journeyRef == entry.getJourneyReferenceNumber() && Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
							&& odRef == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(transportEntries))
			{
				final List<AbstractOrderEntryModel> vehicleEntries = StreamUtil.safeStream(transportEntries)
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.VEHICLE)).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(vehicleEntries))
				{
					return transportEntries.stream().findFirst().get().getTravelOrderEntryInfo().isCarryingDangerousGoods();
				}
			}
		}
		return false;
	}

	@Override
	public boolean hasNonOptionSailingInCart()
	{
		final AbstractOrderModel cart = getBcfTravelCartService().getSessionCart();
		final List<AbstractOrderEntryModel> transportEntries = StreamUtil.safeStream(cart.getEntries())
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
						.nonNull(entry.getTravelOrderEntryInfo())).collect(Collectors.toList());
		return CollectionUtils.isNotEmpty(transportEntries) && !cart.isFerryOptionBooking();
	}

	protected ConfirmBookingIntegrationFacade getConfirmBookingIntegrationFacade()
	{
		return confirmBookingIntegrationFacade;
	}

	@Required
	public void setConfirmBookingIntegrationFacade(final ConfirmBookingIntegrationFacade confirmBookingIntegrationFacade)
	{
		this.confirmBookingIntegrationFacade = confirmBookingIntegrationFacade;
	}

	@Override
	protected ModelService getModelService()
	{
		return modelService;
	}


	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected UserService getUserService()
	{
		return userService;
	}

	@Override
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	/**
	 * @return the makeBookingIntegrationFacade
	 */
	protected MakeBookingIntegrationFacade getMakeBookingIntegrationFacade()
	{
		return makeBookingIntegrationFacade;
	}

	/**
	 * @param makeBookingIntegrationFacade the makeBookingIntegrationFacade to set
	 */
	@Required
	public void setMakeBookingIntegrationFacade(final MakeBookingIntegrationFacade makeBookingIntegrationFacade)
	{
		this.makeBookingIntegrationFacade = makeBookingIntegrationFacade;
	}

	/**
	 * @return the cancelBookingIntegrationFacade
	 */
	protected CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	/**
	 * @param cancelBookingIntegrationFacade the cancelBookingIntegrationFacade to set
	 */
	@Required
	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

	protected TravelSectorService getTravelSectorService()
	{
		return travelSectorService;
	}

	@Required
	public void setTravelSectorService(final TravelSectorService travelSectorService)
	{
		this.travelSectorService = travelSectorService;
	}

	/**
	 * @return the bcfOrderEntryConverter
	 */
	protected Converter<AbstractOrderEntryModel, BcfOrderEntryData> getBcfOrderEntryConverter()
	{
		return bcfOrderEntryConverter;
	}

	/**
	 * @param bcfOrderEntryConverter the bcfOrderEntryConverter to set
	 */
	@Required
	public void setBcfOrderEntryConverter(final Converter<AbstractOrderEntryModel, BcfOrderEntryData> bcfOrderEntryConverter)
	{
		this.bcfOrderEntryConverter = bcfOrderEntryConverter;
	}

	@Override
	protected ProductService getProductService()
	{
		return productService;
	}

	@Override
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}


	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected BcfCalculationService getBcfCalculationService()
	{
		return (BcfCalculationService) getCalculationService();
	}

	protected Converter<TravellerModel, TravellerData> getTravellerDataConverter()
	{
		return travellerDataConverter;
	}

	@Required
	public void setTravellerDataConverter(final Converter<TravellerModel, TravellerData> travellerDataConverter)
	{
		this.travellerDataConverter = travellerDataConverter;
	}

	public ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}

	public BcfBookingFacade getBcfTravelBookingFacade()
	{
		return bcfTravelBookingFacade;
	}

	public void setBcfTravelBookingFacade(final BcfBookingFacade bcfTravelBookingFacade)
	{
		this.bcfTravelBookingFacade = bcfTravelBookingFacade;
	}

	protected BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	@Required
	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	public BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	protected BCFTransportFacilityFacade getBcfTransportFacilityFacade()
	{
		return bcfTransportFacilityFacade;
	}

	@Required
	public void setBcfTransportFacilityFacade(final BCFTransportFacilityFacade bcfTransportFacilityFacade)
	{
		this.bcfTransportFacilityFacade = bcfTransportFacilityFacade;
	}

	public Map<String, Map<String, List>> getSalesApplicationJourneyTypeCompatibilityMap()
	{
		return salesApplicationJourneyTypeCompatibilityMap;
	}

	@Required
	public void setSalesApplicationJourneyTypeCompatibilityMap(
			final Map<String, Map<String, List>> salesApplicationJourneyTypeCompatibilityMap)
	{
		this.salesApplicationJourneyTypeCompatibilityMap = salesApplicationJourneyTypeCompatibilityMap;
	}

	public BcfStockPreprocessor getBcfStockPreprocessor()
	{
		return bcfStockPreprocessor;
	}

	public void setBcfStockPreprocessor(final BcfStockPreprocessor bcfStockPreprocessor)
	{
		this.bcfStockPreprocessor = bcfStockPreprocessor;
	}
}
