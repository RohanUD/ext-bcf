/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.impl;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.service.BcfAsmCommentsService;
import com.bcf.facades.assistedservicefacades.BCFAssistedServiceFacade;
import com.bcf.facades.cart.AsmCommentsData;
import com.bcf.facades.order.BcfAsmCommentsFacade;


public class DefaultBcfAsmCommentsFacade implements BcfAsmCommentsFacade
{

	private Converter<CommentModel, AsmCommentsData> bcfAsmCommentsConverter;
	private BCFAssistedServiceFacade assistedServiceFacade;
	private BcfAsmCommentsService bcfAsmCommentsService;

	@Override
	public List<AsmCommentsData> getAsmComments(final String orderCode){

		final List<CommentModel> asmComments = bcfAsmCommentsService.getAsmComments(orderCode);
		if( CollectionUtils.isNotEmpty(asmComments)){
			return bcfAsmCommentsConverter.convertAll(asmComments);
		}
		return Collections.emptyList();

	}
	@Override
	public List<AsmCommentsData> getAsmAccommodationSupplierComments(final String code, final String ref, final String orderCode)
	{

		final List<CommentModel> asmComments = bcfAsmCommentsService.getAsmAccommodationSupplierComments(code, ref, orderCode);
		if( CollectionUtils.isNotEmpty(asmComments)){
			return bcfAsmCommentsConverter.convertAll(asmComments);
		}
		return Collections.emptyList();
	}

	@Override
	public List<AsmCommentsData> getAsmActivitySupplierComments(final String code, final String orderCode){

		final List<CommentModel> asmComments = bcfAsmCommentsService.getAsmActivitySupplierComments(code,orderCode);
		if( CollectionUtils.isNotEmpty(asmComments)){
			return bcfAsmCommentsConverter.convertAll(asmComments);
		}
		return Collections.emptyList();
	}

	@Override
	public void addAsmComment(final String orderCode, final String comment){
		if(Objects.nonNull(assistedServiceFacade.getAsmSession()))
		{
			bcfAsmCommentsService.addAsmComment(orderCode, comment, assistedServiceFacade.getAsmSession().getAgent().getUid());
		}
	}

	@Override
	public void addAsmAccommodationSupplierComment(final String code, final String ref, final String comment,
			final String orderCode)
	{

		bcfAsmCommentsService
				.addAsmAccommodationSupplierComment(code, ref, comment, assistedServiceFacade.getAsmSession().getAgent().getUid(),
						orderCode);

	}

	@Override
	public void addAsmActivitySupplierComment( final String code,final String comment, final String orderCode){

		bcfAsmCommentsService.addAsmActivitySupplierComment(code,comment,assistedServiceFacade.getAsmSession().getAgent().getUid(),orderCode);

	}


	public Converter<CommentModel, AsmCommentsData> getBcfAsmCommentsConverter()
	{
		return bcfAsmCommentsConverter;
	}

	public void setBcfAsmCommentsConverter(
			final Converter<CommentModel, AsmCommentsData> bcfAsmCommentsConverter)
	{
		this.bcfAsmCommentsConverter = bcfAsmCommentsConverter;
	}

	public BCFAssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final BCFAssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

	public BcfAsmCommentsService getBcfAsmCommentsService()
	{
		return bcfAsmCommentsService;
	}

	public void setBcfAsmCommentsService(final BcfAsmCommentsService bcfAsmCommentsService)
	{
		this.bcfAsmCommentsService = bcfAsmCommentsService;
	}
}
