/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelfacades.order.impl.DefaultTravelCheckoutFacade;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.CountryDropDownUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.integration.processor.BcfIntegrationPipelineProcessor;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.order.preprocessor.BcfOrderPreprocessor;
import com.bcf.facades.order.processor.BcfOrderProcessor;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.BcfRefundIntegrationFacade;
import com.bcf.facades.payment.processor.BcfPaymentProcessor;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.financial.FinancialData;


public class DefaultBcfTravelCheckoutFacade extends DefaultTravelCheckoutFacade implements BcfTravelCheckoutFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfTravelCheckoutFacade.class);
	private static final String LOG_MESSAGE = "Placing order [%s] with amount [%s]";
	private BCFTravelCartService bcfTravelCartService;
	private BcfOrderPreprocessor transportOnlyOrderPreprocessor;
	private BcfOrderPreprocessor transportPlusOrderPreprocessor;
	private BcfPaymentProcessor bcfPaymentProcessor;
	private BcfIntegrationPipelineProcessor integrationPipelineProcessor;
	private BcfOrderProcessor bcfOrderProcessor;
	private BcfBookingService bcfBookingService;
	private BcfCheckoutService bcfCheckoutService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfRefundIntegrationFacade bcfRefundIntegrationFacade;
	private BcfOrderService orderService;
	private CancelBookingService cancelBookingService;
	BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "createOrderFinancialDataPipelineManager")
	private OrderFinancialDataPipelineManager orderFinancialDataPipelineManager;

	@Resource(name = "notificationEngineRestService")
	private NotificationEngineRestService notificationEngineRestService;

	private static final String FINANCIAL_DATA_NOTIFICATION_URL = "financialDataServiceUrl";

	@Override
	public Boolean containsDefaultSailing()
	{
		final CartModel cart = getBcfTravelCartService().getSessionCart();
		if (Objects.isNull(cart))
		{
			return Boolean.FALSE;
		}

		if (BookingJourneyType.BOOKING_PACKAGE
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			return cart.getEntries().stream().anyMatch(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
					&& Objects.nonNull(entry.getTravelOrderEntryInfo())
					&& CollectionUtils.isNotEmpty(entry.getTravelOrderEntryInfo().getTransportOfferings())
					&& entry.getTravelOrderEntryInfo().getTransportOfferings().stream()
					.anyMatch(transportOffering -> Boolean.TRUE.equals(transportOffering.getDefault())));
		}

		return Boolean.FALSE;

	}

	/**
	 * method checks if there is a long route available of all the routes
	 */
	@Override
	public Boolean containsLongRoute()
	{
		final CartModel cart = getBcfTravelCartService().getSessionCart();
		if (Objects.isNull(cart))
		{
			return Boolean.TRUE;
		}
		if (BookingJourneyType.BOOKING_PACKAGE
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			return cart.getEntries().stream().anyMatch(entry -> Objects.nonNull(entry.getEntryGroup())
					&& entry.getEntryGroup() instanceof DealOrderEntryGroupModel
					&& Objects.nonNull(((DealOrderEntryGroupModel) entry.getEntryGroup()).getTravelRoute())
					&& RouteType.LONG
					.equals(((DealOrderEntryGroupModel) entry.getEntryGroup()).getTravelRoute().getRouteType()));
		}
		return cart.getEntries().stream()
				.anyMatch(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType())
						&& Objects.nonNull(entry.getActive()) && entry.getActive()
						&& RouteType.LONG.equals(entry.getTravelOrderEntryInfo().getTravelRoute().getRouteType()));
	}


	@Override
	public List<CountryData> getDeliveryCountries()
	{
		final List<CountryData> allCountry = getCartFacade().getDeliveryCountries();
		return new CountryDropDownUtil().getAllCountry(allCountry);
	}

	/**
	 * Places order when payment details are submitted from payment page. Method is responsible for invoking payment
	 * services, creating payment entries and placing order to eBooking system.
	 *
	 * @param resultMap    parameters having user and payment details
	 * @param channel
	 * @param decisionData
	 * @return PlaceOrderResponseData decision data which holds success/error details
	 */
	@Override
	public PlaceOrderResponseData placeOrder(final AddressData addressData, final Map<String, String> resultMap,
			final String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException
	{

		final Transaction currentTransaction = Transaction.current();
		boolean executed = false;

		try
		{
			currentTransaction.begin();

			decisionData.setAvailabilityStatus(resultMap.get(BcfCoreConstants.ON_REQUEST_ORDER));
			populatePaymentAddressInCart(resultMap, addressData);
			final CartModel cartModel = preProcessOrder(decisionData);
			addAttributesToMap(cartModel.getAmountToPay(), resultMap, channel, cartModel.getBookingJourneyType().getCode());

			//STEP-2 Process the payment transaction if payment service enabled else save payment details only
			getBcfPaymentProcessor().processTransaction(resultMap, cartModel, decisionData);

			callConfirmBookingAndInitiateBusinessProcess(decisionData, cartModel);
			if (StringUtils.isBlank(decisionData.getError()))
			{
				executed = true;
			}
		}
		finally
		{
			if (executed)
			{
				currentTransaction.commit();
			}
			else
			{
				currentTransaction.rollback();
			}
		}


		return decisionData;
	}

	private CartModel preProcessOrder(final PlaceOrderResponseData decisionData)
			throws CartValidationException, InsufficientStockLevelException, OrderProcessingException
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();

		LOG.debug(String.format(LOG_MESSAGE, cartModel.getCode(), cartModel.getAmountToPay()));

		final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries = bcfTravelCartService
				.splitCartEntriesByType(cartModel);

		//STEP-1 PreProcess order to validate, check stock and reserve stock
		getBcfOrderPreprocessorForEntryType(typeWiseEntries.keySet()).processOrder(typeWiseEntries, decisionData, cartModel);
		return cartModel;
	}

	private void populatePaymentAddressInCart(final Map<String, String> resultMap, final AddressData addressData)
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();

		//if it's a new card, we are populating addressData
		if (Objects.nonNull(addressData))
		{
			addressData.setBillingAddress(true);
			final AddressModel addressModel = getModelService().create(AddressModel.class);
			getAddressReversePopulator().populate(addressData, addressModel);
			addressModel.setOwner(cartModel);
			cartModel.setPaymentAddress(addressModel);

			final CustomerModel customermodel = (CustomerModel) cartModel.getUser();
			if (Objects.nonNull(addressData.getPhone()))
			{
				customermodel.setPhoneNo(addressData.getPhone());
			}
			getModelService().save(cartModel);
		}
	}

	@Override
	public void callConfirmBookingAndInitiateBusinessProcess(final PlaceOrderResponseData decisionData,
			CartModel cartModel) throws OrderProcessingException, IntegrationException, InvalidCartException
	{
		if (Objects.isNull(cartModel))
		{
			cartModel = getBcfTravelCartService().getSessionCart();
		}
		final List<AbstractOrderEntryModel> updatedTransportEntries = cartModel.getEntries().stream().filter(
				entry -> entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
						&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
				Collectors.toList());
		//STEP-3 Initiate confirm booking for transport related entries
		if (CollectionUtils.isNotEmpty(updatedTransportEntries))
		{
			getIntegrationPipelineProcessor().executePipeline(cartModel, decisionData);
		}

		//STEP-4 Initiate cancel sailing , for alacarte booking
		if (BcfFacadesConstants.BOOKING_ALACARTE
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{

			final List<AbstractOrderEntryModel> removedTravelEntries = cartModel.getEntries().stream().filter(
					entry -> !entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
							&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
					Collectors.toList());

			if (CollectionUtils.isNotEmpty(removedTravelEntries))
			{
				cancelBookingService.cancelBooking(cartModel,
						removedTravelEntries);

			}

		}
		bcfTravelCartFacadeHelper.removePreviousOnRequestTransaction();

		bcfChangeFeeCalculationService.addNonRefundableCostToBcf(cartModel);

		//STEP-4 Convert cart to order and initiates business process

		getBcfOrderProcessor().processOrder(cartModel, decisionData);
		if (StringUtils.isNotBlank(decisionData.getError()))
		{
			decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
		}
	}

	@Override
	public boolean isRequestValid()
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();
		return cartModel.getPaymentTransactions().stream().flatMap(paymentTransaction -> paymentTransaction.getEntries().stream())
				.map(PaymentTransactionEntryModel::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add).compareTo(BigDecimal.valueOf(cartModel.getAmountToPay())) == 0;
	}

	@Override
	public void makePayment(final AddressData addressData, final Map<String, String> resultMap,
			final String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();

		final String depositPayDateString = resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_PAY_DATE);
		if (StringUtils.isNotBlank(depositPayDateString))
		{

			final Date depositPayDate = TravelDateUtils.convertStringDateToDate(depositPayDateString,
					BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

			if (depositPayDate != null)
			{
				cartModel.setDepositPayDate(depositPayDate);
				getModelService().save(cartModel);
			}
		}

		/*
		 * TODO below block is to be moved out of this class and probably moved to CommerceCheckoutService
		 * and then delegated to yet to be created CommercePaymentAddressStrategy
		 * to follow single responsibility principle and not to mention to make this method less ugly
		 */
		if (Objects.nonNull(addressData))
		{
			addressData.setBillingAddress(true);
			final AddressModel addressModel = getModelService().create(AddressModel.class);
			getAddressReversePopulator().populate(addressData, addressModel);
			addressModel.setOwner(cartModel);
			cartModel.setPaymentAddress(addressModel);
			getModelService().save(cartModel);
		}
		LOG.debug(String.format(LOG_MESSAGE, cartModel.getCode(), cartModel.getAmountToPay()));
		addAttributesToMap(cartModel.getAmountToPay(), resultMap, channel, null);

		final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries = bcfTravelCartService
				.splitCartEntriesByType(cartModel);

		//STEP-1 PreProcess order to validate, check stock and reserve stock
		getBcfOrderPreprocessorForEntryType(typeWiseEntries.keySet()).processOrder(typeWiseEntries, decisionData, cartModel);

		//STEP-2 Process the payment transaction if payment service enabled else save payment details only
		getBcfPaymentProcessor().processTransaction(resultMap, cartModel, decisionData);

		if (StringUtils.isBlank(decisionData.getError()))
		{
			decisionData.setValid(true);
		}
	}

	@Override
	public void makePaymentForPayNow(final Map<String, String> resultMap, final String channel, final String bookingReference,
			final PlaceOrderResponseData decisionData) throws IntegrationException, OrderProcessingException
	{
		final AbstractOrderModel orderModel = getBcfBookingService().getOrder(bookingReference);
		LOG.debug(String.format("Making Payment for order [%s] with amount [%s]", bookingReference, orderModel.getAmountToPay()));
		addAttributesToMap(orderModel.getAmountToPay(), resultMap, channel, orderModel.getBookingJourneyType().getCode());

		//STEP-2 Process the payment transaction if payment service enabled else save payment details only
		getBcfPaymentProcessor().processTransaction(resultMap, orderModel, decisionData);

		if (StringUtils.isBlank(decisionData.getError()))
		{
			decisionData.setValid(true);
		}
	}

	@Override
	public PlaceOrderResponseData completePayment(final Map<String, String> resultMap, final String channel,
			final PlaceOrderResponseData decisionData, final String orderCode) throws
			IntegrationException, OrderProcessingException
	{
		final OrderModel orderModel = getBcfBookingService().getOrder(orderCode);
		if (Objects.isNull(orderModel))
		{
			LOG.debug(String.format("Placing order for code [%s] failed", orderCode));
			decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
			return decisionData;
		}
		LOG.debug(String.format(LOG_MESSAGE, orderModel.getCode(), orderModel.getAmountToPay()));
		addAttributesToMap(orderModel.getAmountToPay(), resultMap, channel, orderModel.getBookingJourneyType().getCode());

		//STEP-2 Process the payment transaction if payment service enabled else save payment details only
		if (!SalesApplication.TRAVELCENTRE.equals(SalesApplication.valueOf(channel)))
		{
			getBcfPaymentProcessor().processTransaction(resultMap, orderModel, decisionData);
		}

		if (decisionData.getError() == null)
		{
			completPaymentForOrder(orderModel);
			return decisionData;
		}
		decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
		return decisionData;
	}

	protected void completPaymentForOrder(final AbstractOrderModel orderModel)
	{

		final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService().getDealOrderEntryGroups(orderModel);
		dealOrderEntryGroupModels.forEach(dealOrderEntryGroupModel -> {
			dealOrderEntryGroupModel.setAmountToPay(0.0d);
			dealOrderEntryGroupModel.setAmountPaid(dealOrderEntryGroupModel.getTotalAmount());
		});
		orderModel.setAmountToPay(0.0d);
		orderModel.setPaymentStatus(PaymentStatus.PAID);
		getModelService().saveAll(dealOrderEntryGroupModels);
		getModelService().save(orderModel);

		try
		{
			final FinancialData financialData = orderFinancialDataPipelineManager.executePipeline(orderModel);
			notificationEngineRestService.sendRestRequest(financialData, FinancialData.class, HttpMethod.POST,
					FINANCIAL_DATA_NOTIFICATION_URL);
			orderModel.setCreateOrderFinancialDataSent(true);
			getModelService().save(orderModel);
		}
		catch (final IntegrationException ex)
		{
			LOG.error("Failed to sent financial information for order: " + orderModel.getCode(), ex);
		}
	}

	private void addAttributesToMap(final double amountToPay,
			final Map<String, String> resultMap, final String channel, final String bookingJourneyType)
	{
		resultMap.put(BcfFacadesConstants.SALES_CHANNEL, channel);
		resultMap.put(BcfFacadesConstants.Payment.PAYMENT_ACTION, definePaymentAction(amountToPay));
		if (bookingJourneyType != null)
		{
			resultMap.put(BookingJourneyType._TYPECODE, bookingJourneyType);
		}
		else
		{
			resultMap.put(BookingJourneyType._TYPECODE, getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY));
		}

	}

	private BcfOrderPreprocessor getBcfOrderPreprocessorForEntryType(final Set<OrderEntryType> orderEntryTypes)
	{
		if (isCartTransportOnly(orderEntryTypes))
		{
			return getTransportOnlyOrderPreprocessor();
		}
		return getTransportPlusOrderPreprocessor();
	}

	private boolean isCartTransportOnly(final Set<OrderEntryType> orderEntryTypes)
	{
		return (orderEntryTypes.size() == 1 && orderEntryTypes.contains(OrderEntryType.TRANSPORT));
	}

	/**
	 * Places order when initiates refund. Method is responsible for invoking refund services, creating payment entries
	 * and placing order to ebooking system.
	 *
	 * @return PlaceOrderResponseData decision data which holds success/error details
	 */
	@Override
	public PlaceOrderResponseData placeOrderAndRefund(final String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException
	{

		final CartModel cartModel = preProcessOrder(decisionData);

		bcfRefundIntegrationFacade.refundOrder(cartModel);

		callConfirmBookingAndInitiateBusinessProcess(decisionData, cartModel);

		return decisionData;
	}


	@Override
	public PlaceOrderResponseData placeOrderAfterRefund(final String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException
	{

		final CartModel cartModel = preProcessOrder(decisionData);

		callConfirmBookingAndInitiateBusinessProcess(decisionData, cartModel);

		return decisionData;
	}


	/**
	 * Places order when amount to pay is zero. Method is responsible for placing order to ebooking system.
	 *
	 * @param channel
	 * @param decisionData
	 * @return PlaceOrderResponseData decision data which holds success/error details
	 */
	@Override
	public PlaceOrderResponseData placeOrderWithoutPayment(final String channel,
			final PlaceOrderResponseData decisionData)
			throws IntegrationException, InvalidCartException, OrderProcessingException
	{
		final CartModel cartModel = getBcfTravelCartService().getSessionCart();
		final List<AbstractOrderEntryModel> updatedTransportEntries = cartModel.getEntries().stream().filter(
				entry -> entry.getActive() && !entry.getAmendStatus().getCode().equals(AmendStatus.SAME.getCode())
						&& entry.getType() != null && OrderEntryType.TRANSPORT.getCode().equals(entry.getType().getCode())).collect(
				Collectors.toList());

		//STEP-3 Initiate confirm booking for transport related entries
		if (CollectionUtils.isNotEmpty(updatedTransportEntries))
		{
			getIntegrationPipelineProcessor().executePipeline(cartModel, decisionData);
		}

		decisionData.setAvailabilityStatus(cartModel.getAvailabilityStatus());
		//STEP-4 Convert cart to order and initiates business process
		getBcfOrderProcessor().processOrder(cartModel, decisionData);
		if (StringUtils.isNotBlank(decisionData.getError()))
		{
			decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
		}

		return decisionData;
	}

	@Override
	public boolean isAnonymousCheckout()
	{
		return getCheckoutCustomerStrategy().isAnonymousCheckout();
	}

	/**
	 * Checks the toPay value to define if user needs to pay or be refunded
	 *
	 * @param toPayValue
	 * @return ACTION_PAY, NO_ACTION or ACTION_REFUND
	 */
	@Override
	public String definePaymentAction(final Double toPayValue)
	{
		if (toPayValue < 0d)
		{
			return BcfFacadesConstants.Payment.ACTION_REFUND;
		}
		else if (Double.compare(toPayValue, 0d) == 0)
		{
			return BcfFacadesConstants.Payment.NO_ACTION;
		}
		return BcfFacadesConstants.Payment.ACTION_PAY;
	}

	@Override
	public void populateMapWithCustomerData(final Map<String, String> resultMap)
	{
		final CustomerModel customerModel = getBcfTravelCartService().getSessionCart() instanceof CartModel ?
				getCheckoutCustomerStrategy().getCurrentUserForCheckout() :
				(CustomerModel) getUserService().getCurrentUser();
		resultMap.put("firstName", customerModel.getFirstName());
		resultMap.put("lastName", customerModel.getLastName());
		resultMap.put("email", customerModel.getContactEmail());
		resultMap.put("phoneNumber", customerModel.getPhoneNo());
		final Optional<AddressModel> optionalAddress = customerModel.getAddresses().stream().findFirst();
		if (optionalAddress.isPresent())
		{
			resultMap.put("country", customerModel.getAddresses().stream().findFirst().get().getCountry().getIsocode());
		}
	}

	/*
	The logic of this method should be enhanced to to find out exact booking journey
	 */
	@Override
	public String getBookingJourney()
	{
		final String sessionBookingJourney = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			return sessionBookingJourney;
		}
		else
		{
			return BcfFacadesConstants.BOOKING_TRANSPORT_ONLY;
		}
	}

	@Override
	public CustomerData getCustomerData()
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		if (Objects.nonNull(customerModel))
		{
			final CustomerData customerData = new CustomerData();
			customerData.setFirstName(customerModel.getFirstName());
			customerData.setLastName(customerModel.getLastName());
			customerData.setEmail(customerModel.getContactEmail());
			customerData.setContactNumber(customerModel.getPhoneNo());

			final Optional<AddressModel> optionalAddress = customerModel.getAddresses().stream().findFirst();
			if (optionalAddress.isPresent())
			{
				final CountryModel country = customerModel.getAddresses().stream().findFirst().get().getCountry();
				customerData.setCountry(country != null ? country.getIsocode() : "");
				customerData.setPostcode(customerModel.getAddresses().stream().findFirst().get().getPostalcode());

			}
			else if (customerModel.getCountry() != null)
			{
				customerData.setCountry(customerModel.getCountry());
				final CartModel cart = getBcfTravelCartService().getSessionCart();
				if (cart.getPaymentAddress() != null)
				{
					customerData.setPostcode(cart.getPaymentAddress().getPostalcode());
				}
			}
			else
			{
				final CartModel cart = getBcfTravelCartService().getSessionCart();
				if (cart.getPaymentAddress() != null)
				{
					customerData.setCountry(
							cart.getPaymentAddress().getCountry() != null ? cart.getPaymentAddress().getCountry().getIsocode() : "");
					customerData.setPostcode(cart.getPaymentAddress().getPostalcode());
				}
			}

			return customerData;
		}
		return null;
	}

	@Override
	public boolean hasCommercialVehiclesOnly()
	{
		return getBcfTravelCartService().hasCommercialVehiclesOnly();
	}

	@Override
	public void saveAgentComment(final String agentComment)
	{
		getBcfCheckoutService().saveAgentComment(agentComment);
	}

	@Override
	public void saveAdditionalInstructions(final boolean isAllowChangeAtPOS, final String specialInstructions,
			final String vacationInstructions, final boolean deferPaymentConsent, final String paymentType)
	{
		final CartModel cartModel = getSessionCart();
		cartModel.setIsAllowChangesAtPOS(isAllowChangeAtPOS);
		cartModel.setVacationInstructions(vacationInstructions);
		cartModel.setSpecialInstructions(specialInstructions);
		cartModel.setDeferPayment(isDeferPayment(paymentType, deferPaymentConsent));
		getModelService().save(cartModel);
	}

	@Override
	public void saveReferenceCode(final List<String> referenceCodes, final List<String> bookingRefs)
	{
		getBcfCheckoutService().saveReferenceCodes(referenceCodes, bookingRefs);
	}

	@Override
	public boolean isMaxSailingExceedsForUser(final CartModel sessionCart)
	{
		final Map<String, List<AbstractOrderEntryModel>> cartEntriesByCacheKey = getBcfTravelCartService()
				.groupEntriesByCacheKeyOrBookingRef(sessionCart);
		final int maxSailingAllowed = bcfTravelCartFacade.getMaxSailingAllowed();
		return cartEntriesByCacheKey.size() >= maxSailingAllowed;
	}

	private boolean isDeferPayment(final String paymentType, final boolean deferByConsent)
	{
		final boolean isB2B = ((BcfUserService) getUserService()).isB2BCustomer();
		return (isB2B && (StringUtils.equalsIgnoreCase(BcfCoreConstants.CTC_TC_CARD, paymentType) || (deferByConsent
				&& StringUtils.equalsIgnoreCase(BcfCoreConstants.CREDIT_CARD, paymentType))))
				|| (!isB2B && StringUtils.equalsIgnoreCase(BcfCoreConstants.CTC_TC_CARD, paymentType) && getBcfTravelCartService()
				.hasCommercialVehiclesOnly())
				|| (!isB2B && StringUtils.equalsIgnoreCase(BcfCoreConstants.CREDIT_CARD, paymentType)
				&& getBcfTravelCartService().hasCommercialVehiclesOnly() && deferByConsent);
	}

	@Override
	public PlaceOrderResponseData placeFerryOptionBooking(final List<AddBundleToCartRequestData> addBundleToCartRequestDataList)
			throws IntegrationException, OrderProcessingException, InvalidCartException
	{
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		final CartModel cart = getBcfTravelCartService().getSessionCart();
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestDataList))
		{
			for (final AddBundleToCartRequestData addBundleToCartRequestData : addBundleToCartRequestDataList)
			{
				if (Objects.nonNull(addBundleToCartRequestData))
				{
					getModifyBookingIntegrationFacade()
							.getModifyBookingResponse(addBundleToCartRequestData, BcfCoreConstants.MAKE_BOOKING_REQUEST);
				}
			}
		}
		getBcfOrderProcessor().processOrder(cart, decisionData);
		if (StringUtils.isNotBlank(decisionData.getError()))
		{
			decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
		}
		return decisionData;
	}

	@Override
	public CartModel getSessionCart()
	{
		return getBcfTravelCartService().getSessionCart();
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfOrderPreprocessor getTransportOnlyOrderPreprocessor()
	{
		return transportOnlyOrderPreprocessor;
	}

	@Required
	public void setTransportOnlyOrderPreprocessor(final BcfOrderPreprocessor transportOnlyOrderPreprocessor)
	{
		this.transportOnlyOrderPreprocessor = transportOnlyOrderPreprocessor;
	}

	protected BcfOrderPreprocessor getTransportPlusOrderPreprocessor()
	{
		return transportPlusOrderPreprocessor;
	}

	@Required
	public void setTransportPlusOrderPreprocessor(final BcfOrderPreprocessor transportPlusOrderPreprocessor)
	{
		this.transportPlusOrderPreprocessor = transportPlusOrderPreprocessor;
	}

	protected BcfPaymentProcessor getBcfPaymentProcessor()
	{
		return bcfPaymentProcessor;
	}

	@Required
	public void setBcfPaymentProcessor(final BcfPaymentProcessor bcfPaymentProcessor)
	{
		this.bcfPaymentProcessor = bcfPaymentProcessor;
	}

	protected BcfIntegrationPipelineProcessor getIntegrationPipelineProcessor()
	{
		return integrationPipelineProcessor;
	}

	@Required
	public void setIntegrationPipelineProcessor(
			final BcfIntegrationPipelineProcessor integrationPipelineProcessor)
	{
		this.integrationPipelineProcessor = integrationPipelineProcessor;
	}

	protected BcfOrderProcessor getBcfOrderProcessor()
	{
		return bcfOrderProcessor;
	}

	@Required
	public void setBcfOrderProcessor(final BcfOrderProcessor bcfOrderProcessor)
	{
		this.bcfOrderProcessor = bcfOrderProcessor;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	@Required
	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfRefundIntegrationFacade getBcfRefundIntegrationFacade()
	{
		return bcfRefundIntegrationFacade;
	}

	public void setBcfRefundIntegrationFacade(final BcfRefundIntegrationFacade bcfRefundIntegrationFacade)
	{
		this.bcfRefundIntegrationFacade = bcfRefundIntegrationFacade;
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	public CancelBookingService getCancelBookingService()
	{
		return cancelBookingService;
	}

	public void setCancelBookingService(final CancelBookingService cancelBookingService)
	{
		this.cancelBookingService = cancelBookingService;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(
			final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	@Required
	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
