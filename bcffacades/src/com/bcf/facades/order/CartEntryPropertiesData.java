/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.travelservices.enums.AmendStatus;
import java.util.List;


public class CartEntryPropertiesData
{
	String travellerCode;
	int originDestinationRefNo;
	String travelRoute;
	List<String> transportOfferingCodes;
	boolean active;
	AmendStatus amendStatus;
	CartModificationData cartModification;
	private String productCode;
	private String addToCartCriteria;
	private Integer entryNumber;
	private int journeyRefNumber;

	public String getTravellerCode()
	{
		return travellerCode;
	}

	public void setTravellerCode(final String travellerCode)
	{
		this.travellerCode = travellerCode;
	}

	public int getOriginDestinationRefNo()
	{
		return originDestinationRefNo;
	}

	public void setOriginDestinationRefNo(final int originDestinationRefNo)
	{
		this.originDestinationRefNo = originDestinationRefNo;
	}

	public String getTravelRoute()
	{
		return travelRoute;
	}

	public void setTravelRoute(final String travelRoute)
	{
		this.travelRoute = travelRoute;
	}

	public List<String> getTransportOfferingCodes()
	{
		return transportOfferingCodes;
	}

	public void setTransportOfferingCodes(final List<String> transportOfferingCodes)
	{
		this.transportOfferingCodes = transportOfferingCodes;
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(final boolean active)
	{
		this.active = active;
	}

	public AmendStatus getAmendStatus()
	{
		return amendStatus;
	}

	public void setAmendStatus(final AmendStatus amendStatus)
	{
		this.amendStatus = amendStatus;
	}

	public CartModificationData getCartModification()
	{
		return cartModification;
	}

	public void setCartModification(final CartModificationData cartModification)
	{
		this.cartModification = cartModification;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public String getAddToCartCriteria()
	{
		return addToCartCriteria;
	}

	public void setAddToCartCriteria(final String addToCartCriteria)
	{
		this.addToCartCriteria = addToCartCriteria;
	}

	public Integer getEntryNumber()
	{
		return entryNumber;
	}

	public void setEntryNumber(final Integer entryNumber)
	{
		this.entryNumber = entryNumber;
	}

	public int getJourneyRefNumber()
	{
		return journeyRefNumber;
	}

	public void setJourneyRefNumber(final int journeyRefNumber)
	{
		this.journeyRefNumber = journeyRefNumber;
	}


}
