/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.order.data.OnHoldOrderData;
import com.bcf.core.order.entry.data.OnHoldAccommodationEntryData;
import com.bcf.core.order.entry.data.OnHoldActivityEntryData;
import com.bcf.core.service.BcfTravelCommerceStockService;


public class OnHoldOrderDataPopulator implements Populator<OrderModel, OnHoldOrderData>
{
	private PriceDataFactory priceDataFactory;
	private Converter<AbstractOrderEntryModel, OnHoldAccommodationEntryData> onHoldAccommodationEntryDataConverter;
	private Converter<AbstractOrderEntryModel, OnHoldActivityEntryData> onHoldActivityEntryDataConverter;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;


	@Override
	public void populate(final OrderModel source, final OnHoldOrderData target) throws ConversionException
	{
		if (source != null)
		{
			target.setLockedBy(source.getLockedBy());
			final AbstractOrderEntryModel abstractOrderEntryModel=source.getEntries().stream().filter(entry -> entry.getType().equals(OrderEntryType.TRANSPORT)).findFirst().orElse(null);
			if(abstractOrderEntryModel!=null )
			{
				if (Objects.nonNull(abstractOrderEntryModel.getTravelOrderEntryInfo()) && CollectionUtils.isNotEmpty(abstractOrderEntryModel.getTravelOrderEntryInfo().getTransportOfferings()))
				{
					target.setDepartureTime(
							abstractOrderEntryModel.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get()
									.getDepartureTime());
				}

			}
			else
			{
				final AbstractOrderEntryModel accommodationOrderEntryModel = source.getEntries().stream()
						.filter(entry -> entry.getType().equals(OrderEntryType.ACCOMMODATION)).findFirst().orElse(null);
				if (Objects.nonNull(accommodationOrderEntryModel) &&  Objects.nonNull(accommodationOrderEntryModel.getAccommodationOrderEntryInfo()))
				{
					final Date date = accommodationOrderEntryModel.getAccommodationOrderEntryInfo().getDates().stream().min(Date::compareTo)
							.get();
					target.setDepartureTime(date);
				}
				else
				{
					final AbstractOrderEntryModel activityOrderEntryModel = source.getEntries().stream()
							.filter(entry -> entry.getType().equals(OrderEntryType.ACTIVITY)).findFirst().orElse(null);
					if (Objects.nonNull(activityOrderEntryModel) && Objects.nonNull(activityOrderEntryModel.getActivityOrderEntryInfo()))
					{
						final Date date = activityOrderEntryModel.getActivityOrderEntryInfo().getActivityDate();
						target.setDepartureTime(date);
					}
				}
			}

			target.setCode(source.getCode());
			target.setCreated(source.getDate());
			target.setTotalPrice(
					getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getTotalPrice()), source.getCurrency()));
			target.setPlacedBy(getPlacedByName(source));
			final List<AbstractOrderEntryModel> eligibleEntries = source.getEntries().stream()
					.filter(e -> Objects.nonNull(e.getProduct()))
					.filter(entry -> entry.getActive() && !entry.isStockAllocated())
					.collect(Collectors.toList());
			final List<AbstractOrderEntryModel> activities = eligibleEntries.stream()
					.filter(entry -> Objects.equals(OrderEntryType.ACTIVITY, entry.getType())).collect(Collectors.toList());
			target.setActivityEntries(getOnHoldActivityEntryDataConverter().convertAll(activities));
			final List<AbstractOrderEntryModel> accommodations = eligibleEntries.stream()
					.filter(entry -> Objects.equals(OrderEntryType.ACCOMMODATION, entry.getType())).collect(Collectors.toList());
			target.setAccommodationEntries(getOnHoldAccommodationEntryDataConverter().convertAll(accommodations));
		}
	}

	private String getPlacedByName(final OrderModel source)
	{
		if (source.getUser() instanceof CustomerModel)
		{
			return ((CustomerModel) source.getUser()).getLastName() + " " + ((CustomerModel) source.getUser()).getFirstName();
		}
		else
		{
			return source.getUser().getName();
		}
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected Converter<AbstractOrderEntryModel, OnHoldAccommodationEntryData> getOnHoldAccommodationEntryDataConverter()
	{
		return onHoldAccommodationEntryDataConverter;
	}

	@Required
	public void setOnHoldAccommodationEntryDataConverter(
			final Converter<AbstractOrderEntryModel, OnHoldAccommodationEntryData> onHoldAccommodationEntryDataConverter)
	{
		this.onHoldAccommodationEntryDataConverter = onHoldAccommodationEntryDataConverter;
	}

	protected Converter<AbstractOrderEntryModel, OnHoldActivityEntryData> getOnHoldActivityEntryDataConverter()
	{
		return onHoldActivityEntryDataConverter;
	}

	@Required
	public void setOnHoldActivityEntryDataConverter(
			final Converter<AbstractOrderEntryModel, OnHoldActivityEntryData> onHoldActivityEntryDataConverter)
	{
		this.onHoldActivityEntryDataConverter = onHoldActivityEntryDataConverter;
	}
}
