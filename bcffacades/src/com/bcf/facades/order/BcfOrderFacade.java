/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order;

import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.order.data.OnHoldOrderData;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public interface BcfOrderFacade extends OrderFacade
{
	SearchPageData<OnHoldOrderData> getPagedPendingOrdersForInventoryApproval(PageableData pageableData);

	OnHoldOrderData getOrderForCode(final String code);

	boolean updateOrderEntry(UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequestData);

	String lockOrder(String orderCode) throws BcfOrderUpdateException;

	String unlockOrder(String orderCode);

	String lockOrderByAgent(final String orderCode) throws BcfOrderUpdateException;

	String rejectOrder(String orderCode, final String comments) throws BcfOrderUpdateException;

	String acceptOrder(String orderCode, final String comments) throws BcfOrderUpdateException;

	List<BcfGlobalReservationData> findVacationsForCurrentUser(boolean isUpcoming);

	List<String> findBookingRefForCurrentUser(boolean isUpcoming);

	List<String> findBookingRefForOrder(AbstractOrderModel order);

	Boolean isWaitlistedOrder(AbstractOrderModel orderModel);

	Map<String, Collection<String>> getEntryMarkers(AbstractOrderModel order);

	Boolean updateEntryMarkers(AbstractOrderModel order);

	OrderModel getOrderByCode(final String orderCode, final String versionId);

	List<OrderModel> getOrdersByCode(final String orderCode);
}
