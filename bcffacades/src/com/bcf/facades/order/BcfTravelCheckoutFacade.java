/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import java.util.List;
import java.util.Map;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfTravelCheckoutFacade extends AcceleratorCheckoutFacade
{

	Boolean containsLongRoute();

	PlaceOrderResponseData placeOrder(final AddressData addressData, final Map<String, String> requestParams, String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException;

	PlaceOrderResponseData completePayment(final Map<String, String> requestParams, String channel,
			final PlaceOrderResponseData decisionData, String orderCode) throws
			IntegrationException, OrderProcessingException;


	PlaceOrderResponseData placeOrderAndRefund(final String channel,
			final PlaceOrderResponseData decisionData)
			throws CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException;

	PlaceOrderResponseData placeOrderWithoutPayment(final String channel,
			final PlaceOrderResponseData decisionData)
			throws IntegrationException, InvalidCartException, OrderProcessingException, CartValidationException,
			InsufficientStockLevelException;

	PlaceOrderResponseData placeOrderAfterRefund(final String channel,
			final PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException,
			InvalidCartException;

	String definePaymentAction(final Double totalToPay);

	boolean isAnonymousCheckout();

	void populateMapWithCustomerData(Map<String, String> resultMap);

	String getBookingJourney();

	CustomerData getCustomerData();

	void saveAgentComment(String agentComment);

	void saveReferenceCode(List<String> referenceCode, List<String> bookingRefs);

	Boolean containsDefaultSailing();

	void makePayment(AddressData addressData, Map<String, String> resultMap,
			String channel, PlaceOrderResponseData decisionData) throws
			CartValidationException, InsufficientStockLevelException, IntegrationException, OrderProcessingException;

	void callConfirmBookingAndInitiateBusinessProcess(PlaceOrderResponseData decisionData,
			CartModel cartModel) throws OrderProcessingException, IntegrationException, InvalidCartException;

	boolean isRequestValid();

	void makePaymentForPayNow(Map<String, String> resultMap, String channel, String bookingReference,
			PlaceOrderResponseData decisionData) throws IntegrationException, OrderProcessingException;

	boolean isMaxSailingExceedsForUser(CartModel sessionCart);

	CartModel getSessionCart();

	void saveAdditionalInstructions(boolean isAllowChangeAtPOS, String specialInstructions, String vacationInstructions,
			boolean deferPaymentConsent, final String paymentType);

	boolean hasCommercialVehiclesOnly();

	PlaceOrderResponseData placeFerryOptionBooking(List<AddBundleToCartRequestData> addBundleToCartRequestDataList)
			throws IntegrationException, OrderProcessingException, InvalidCartException;
}
