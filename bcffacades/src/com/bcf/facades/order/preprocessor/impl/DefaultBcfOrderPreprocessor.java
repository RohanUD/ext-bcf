/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.preprocessor.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.handler.AbstractCartPreprocessorHandler;
import com.bcf.facades.order.preprocessor.BcfOrderPreprocessor;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;

public class DefaultBcfOrderPreprocessor implements BcfOrderPreprocessor
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOrderPreprocessor.class);
	private List<AbstractCartPreprocessorHandler> handlers;
	private ModelService modelService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	@Override
	public void processOrder(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final PlaceOrderResponseData decisionData, final CartModel cartModel)
			throws CartValidationException, InsufficientStockLevelException, OrderProcessingException
	{
		LOG.debug(String.format("PreProcessing order [%s]", cartModel.getCode()));

		boolean earlyStockAllocation = !(bcfTravelCommerceStockService.getEarlyStockAllocation(cartModel.getSalesApplication()));
		decisionData.setEarlyStockAllocation(earlyStockAllocation);

		if (cartModel.isValidated())
		{
			return;
		}
		for (final AbstractCartPreprocessorHandler handler : getHandlers())
		{
			handler.handle(typeWiseEntries, decisionData, cartModel);
		}
		cartModel.setValidated(Boolean.TRUE);
		getModelService().save(cartModel);
	}

	protected List<AbstractCartPreprocessorHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<AbstractCartPreprocessorHandler> handlers)
	{
		this.handlers = handlers;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
