/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.preprocessor;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.List;
import java.util.Map;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;


public interface BcfOrderPreprocessor
{
	void processOrder(Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final PlaceOrderResponseData decisionData, final CartModel cartModel)
			throws CartValidationException, InsufficientStockLevelException, OrderProcessingException;
}
