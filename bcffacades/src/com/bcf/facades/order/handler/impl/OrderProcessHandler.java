/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.order.handler.AbstractCartProcessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.fulfilmentprocess.constants.BcfFulfilmentProcessConstants;


public class OrderProcessHandler implements AbstractCartProcessorHandler
{
	private static final Logger LOG = Logger.getLogger(OrderProcessHandler.class);
	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	@Override
	public void handle(final CartModel cart, final PlaceOrderResponseData decisionData)
	{
		final OrderModel order = decisionData.getOrder();
		String businessProcessId = getBusinessProcessid(order);

		if (StringUtils.isBlank(businessProcessId))
		{
			return;
		}

		LOG.info(
				String.format("Initiating business process [%s] for order [%s], order status [%s]", businessProcessId,
						order.getCode(),
						order.getStatus().getCode()));
		final OrderProcessModel orderProcessModel = getBusinessProcessService()
				.createProcess(businessProcessId + "-" + order.getCode() + "-" + System.currentTimeMillis(),
						businessProcessId);

		orderProcessModel.setOrder(order);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	/**
	 * If it is an ON_REQUEST booking, sends on_request initial email only if we haven't send "create order" request to customer and only for the first time,
	 * no need to sent emails for subsequent amendment of an on_request booking
	 * <br>
	 * Else if it is amendment, send amend order process
	 * <br>
	 * Else, send order create process
	 *
	 * @param order order
	 * @return business process id
	 */
	private String getBusinessProcessid(OrderModel order)
	{

		if (AvailabilityStatus.ON_REQUEST.name().equals(order.getAvailabilityStatus()) && !order.isCreateOrderNotificationSent())
		{
			if (Objects.nonNull(order.getOriginalOrder()))
			{
				LOG.info(
						String.format("Not initiating business process [%s] for order [%s] as it is amendment of a Unapproved On-Request booking",
								BcfFulfilmentProcessConstants.ONREQUEST_INITIAL_ORDER_PROCESS_NAME, order.getCode()));
				return StringUtils.EMPTY;
			}
			return BcfFulfilmentProcessConstants.ONREQUEST_INITIAL_ORDER_PROCESS_NAME;
		}
		if (Objects.nonNull(order.getOriginalOrder()))
		{
			return BcfFulfilmentProcessConstants.AMEND_ORDER_PROCESS_NAME;
		}
		return BcfFulfilmentProcessConstants.ORDER_PROCESS_NAME;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

}
