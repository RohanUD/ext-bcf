/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler.impl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.handler.AbstractCartProcessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.BcfRefundIntegrationFacade;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.core.exception.IntegrationException;


public class PlaceOrderHandler implements AbstractCartProcessorHandler
{
	private static final Logger LOG = Logger.getLogger(PlaceOrderHandler.class);
	private BcfCheckoutService bcfCheckoutService;
	private BCFTravelCartService bcfTravelCartService;
	private CancelBookingService cancelBookingService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfRefundIntegrationFacade bcfRefundIntegrationFacade;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private Converter<OrderModel, OrderData> orderConverter;
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private ModelService modelService;
	private UserService userService;
	final private static String PACKAGE_ON_REQUEST = "ON_REQUEST";

	@Override
	public void handle(final CartModel cart, final PlaceOrderResponseData decisionData)
			throws OrderProcessingException, InvalidCartException, IntegrationException
	{
		LOG.debug(String.format("Processing order [%s] with amount [%s]", cart.getCode(), cart.getAmountToPay()));
		final OrderModel order = placeOrderWithCart(cart, decisionData.getAvailabilityStatus());
		if (Objects.nonNull(order))
		{
			final OrderData orderData = getOrderConverter().convert(order);
			decisionData.setOrderData(orderData);
			decisionData.setOrder(order);
			LOG.debug(String.format("Order [%s] with amount [%s] is placed successfully", order.getCode(), order.getAmountToPay()));
			return;
		}
		decisionData.setError(BcfFacadesConstants.ERROR_PLACE_ORDER);
		getCancelBookingService().cancelBooking(cart,
				cart.getEntries().stream().filter(entry -> Objects.equals(OrderEntryType.TRANSPORT, entry.getType()))
						.collect(Collectors.toList()));
		getBcfRefundIntegrationFacade().refundOrder(cart);
		LOG.debug(String.format("Refund has been initiated for order [%s]", cart.getCode()));
		getBcfTravelCommerceStockService().releaseStocks(cart);
		throw new OrderProcessingException("Exception encountered while procession payment");
	}




	/**
	 * Converts cart to order after validating the cart. Also creates history snapshot in case of amended order.
	 *
	 * @param cartModel
	 * @return OrderModel
	 */
	private OrderModel placeOrderWithCart(final CartModel cartModel, final String availableStatus) throws InvalidCartException
	{
		if (cartModel != null && CartModel._TYPECODE.equals(cartModel.getItemtype()) && (
				cartModel.getUser().equals(getCheckoutCustomerStrategy().getCurrentUserForCheckout())
						|| getCheckoutCustomerStrategy().isAnonymousCheckout()))
		{
			beforePlaceOrder(cartModel);
			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);
			parameter.setSalesApplication(cartModel.getSalesApplication());
			final OrderModel orderModel = getBcfCheckoutService().placeOrder(parameter).getOrder();
			if (StringUtils.equals(PACKAGE_ON_REQUEST, availableStatus))
			{
				//if an Approved On_request Order is Amended, we should set the correct order status.
				if (cartModel.isCreateOrderNotificationSent())
				{
					orderModel.setStatus(OrderStatus.APPROVED_BY_MERCHANT);
				}
				else
				{
					orderModel.setStatus(OrderStatus.PENDING_APPROVAL_FROM_MERCHANT);
				}
			}
			else
			{
				orderModel.setStatus(OrderStatus.ACTIVE);
			}
			if (Objects.nonNull(orderModel.getOriginalOrder()))
			{
				LOG.debug(String.format("Creating HistorySnapshot for order [%s]", orderModel.getOriginalOrder().getCode()));
				getBcfCheckoutService().createHistorySnapshot(orderModel);
			}
			getBcfCheckoutService().updateAmountToPay(orderModel);
			orderModel.setEBookingVersion(orderModel.getEBookingVersion() + 1);
			if (Objects.nonNull(orderModel.getOriginalOrder()))
			{
				orderModel.setCartId(orderModel.getOriginalOrder().getCartId());
			}
			else
			{
				orderModel.setCartId(cartModel.getCode());
			}
			getModelService().save(orderModel);
			afterPlaceOrder(cartModel, orderModel);
			return orderModel;
		}
		return null;
	}

	private void beforePlaceOrder(final CartModel cartModel)
	{
		getBcfTravelCartService().updateAmountToPayInfo(cartModel);
	}

	private void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		getBcfCheckoutService().postProcessOrder(cartModel, orderModel);
	}

	/**
	 * TO-DO need to remove this method as part of defect #377 followed by testing of story #340
	 *
	 * @param cartModel
	 * @return
	 * @throws InvalidCartException
	 */
	protected OrderModel placeOrderIfComingFromEBooking(final CartModel cartModel) throws InvalidCartException
	{
		final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
		parameter.setEnableHooks(false);
		parameter.setCart(cartModel);
		// Setting Sales Application to CALLCENTER as order is imported from EBooking
		parameter.setSalesApplication(SalesApplication.CALLCENTER);
		return getBcfCheckoutService().placeOrder(parameter).getOrder();
	}

	protected BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	@Required
	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected CancelBookingService getCancelBookingService()
	{
		return cancelBookingService;
	}

	@Required
	public void setCancelBookingService(final CancelBookingService cancelBookingService)
	{
		this.cancelBookingService = cancelBookingService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BcfRefundIntegrationFacade getBcfRefundIntegrationFacade()
	{
		return bcfRefundIntegrationFacade;
	}

	@Required
	public void setBcfRefundIntegrationFacade(final BcfRefundIntegrationFacade bcfRefundIntegrationFacade)
	{
		this.bcfRefundIntegrationFacade = bcfRefundIntegrationFacade;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setOrderConverter(
			final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}

