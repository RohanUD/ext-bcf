/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.order.handler.AbstractCartPreprocessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;


public class CartValidationHandler implements AbstractCartPreprocessorHandler
{
	private static final Logger LOG = Logger.getLogger(CartValidationHandler.class);
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private TimeService timeService;
	private BCFTravelCartService bcfTravelCartService;


	@Override
	public void handle(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final PlaceOrderResponseData decisionData, final CartModel cartModel) throws CartValidationException
	{
		LOG.debug(String.format("Validating cart [%s]", cartModel.getCode()));
		List<AbstractOrderEntryModel> entries = null;
		if (Objects.nonNull(typeWiseEntries.get(OrderEntryType.TRANSPORT)))
		{
			entries = typeWiseEntries.get(OrderEntryType.TRANSPORT).stream().filter(
					entry -> !entry.getAmendStatus().equals(AmendStatus.SAME) && entry.getActive() && entry
							.getProduct() instanceof FareProductModel && !isDefault(entry))
					.collect(Collectors.toList());
			final int thresholdTimeGap = getBcfTravelCartService().getThresholdTimeGap(cartModel);
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0
						&& !TravelDateUtils
						.isAfter(entry.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get().getDepartureTime(),
								ZoneId.of(
										entry.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get().getTravelSector()
												.getOrigin().getPointOfService().get(0).getTimeZoneId()),
								DateUtils.addMinutes(getTimeService().getCurrentTime(), thresholdTimeGap),
								ZoneId.systemDefault()))
				{
					LOG.error(
							String.format("Validation failed for cart [%s], departure time is invalid", entry.getOrder().getCode()));
					decisionData.setError(BcfFacadesConstants.ERROR_INVALID_DEPARTURE_TIME);
					throw new CartValidationException("Invalid departure time");
				}
				if (entry.getTravelOrderEntryInfo().getTransportOfferings().stream().findFirst().get().getDepartureTime() != null
						&& !bcfTravelCartService.hasValidCacheKeyOrBookingRef(cartModel, entry))
				{

					LOG.error(
							String.format("Validation failed for cart [%s], cache key or booking reference is null",
									entry.getOrder().getCode()));
					decisionData.setError(BcfFacadesConstants.ERROR_CACHE_KEY_OR_BOOKING_REF_NULL);
					throw new CartValidationException("Cache key or booking reference is null");
				}

			}
		}
	}

	private boolean isDefault(final AbstractOrderEntryModel entry)
	{
		return entry.getTravelOrderEntryInfo().getTransportOfferings().stream().anyMatch(WarehouseModel::getDefault);
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
