/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.handler.AbstractCartPreprocessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;


public class StockCheckHandler implements AbstractCartPreprocessorHandler
{
	private static final Logger LOG = Logger.getLogger(StockCheckHandler.class);

	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private WarehouseService warehouseService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfProductFacade bcfProductFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public void handle(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final PlaceOrderResponseData decisionData, final CartModel cartModel) throws OrderProcessingException
	{
		checkStockAvailability(typeWiseEntries, cartModel);
	}

	protected void checkStockAvailability(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final CartModel cartModel) throws OrderProcessingException
	{
		if (Objects.nonNull(typeWiseEntries.get(OrderEntryType.ACCOMMODATION)))
		{
			final List<AbstractOrderEntryModel> accommodationEntries = typeWiseEntries.get(OrderEntryType.ACCOMMODATION).stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(RoomRateProductModel._TYPECODE) &&  !entry.isStockAllocated()&& entry.getActive())
					.collect(Collectors.toList());
			if(CollectionUtils.isNotEmpty(accommodationEntries))
			{
				final Map<AccommodationModel, List<AbstractOrderEntryModel>> accommodationEntriesMap = accommodationEntries.stream()
						.collect(Collectors.groupingBy(accommodationEntry -> getBcfAccommodationOfferingService()
								.getAccommodation((RoomRateProductModel) accommodationEntry.getProduct())));


				final boolean isAllStockAvailable = accommodationEntriesMap.keySet().stream().allMatch(
						accommodation -> isAccommodationStockAvailable(accommodation, accommodationEntriesMap.get(accommodation)));
				if (!isAllStockAvailable)
				{
					throwStockNotAvailableException(cartModel.getCode());
				}
			}
      }
      if (Objects.nonNull(typeWiseEntries.get(OrderEntryType.ACTIVITY)))
      {
         final List<AbstractOrderEntryModel> activityEntries = typeWiseEntries.get(OrderEntryType.ACTIVITY).stream()
               .filter(entry -> entry.getActive() && !entry.isStockAllocated()).collect(Collectors.toList());
         if(CollectionUtils.isNotEmpty(activityEntries))
			{
				final boolean isAllStockAvailable = activityEntries.stream().allMatch(this::isActivityStockAvailable);
				if (!isAllStockAvailable)
				{
					throwStockNotAvailableException(cartModel.getCode());
				}
			}
      }
   }

	/**
	 * Checks if is accommodation stock available.
	 *
	 * @param accommodation the accommodation
	 * @return true, if is accommodation stock available
	 */
	protected boolean isAccommodationStockAvailable(final AccommodationModel accommodation,
			final List<AbstractOrderEntryModel> accommodationEntries)
	{
		if (Objects.isNull(accommodation))
		{
			return false;
		}
		if (Objects.nonNull(accommodation.getStockLevelType())
				&& !Objects.equals(StockLevelType.STANDARD, accommodation.getStockLevelType()))
		{
			return true;
		}

		final int numberOfRooms = getBcfTravelCommerceCartService().getAccommodationOrderEntryGroups(accommodationEntries).size();

		final boolean isSufficientStocksAvailable = accommodationEntries.stream()
				.allMatch(accommodationEntry -> isSufficientStocksAvailable(accommodation, accommodationEntry, numberOfRooms));
		return isSufficientStocksAvailable;
	}

	protected boolean isSufficientStocksAvailable(final AccommodationModel accommodation,
			final AbstractOrderEntryModel accommodationEntry,
			final int numberOfRooms)
	{
		final AccommodationOrderEntryGroupModel accommodationEntryGroup = getBcfTravelCommerceCartService()
				.getAccommodationOrderEntryGroup(accommodationEntry);
		if (Objects.isNull(accommodationEntryGroup))
		{
			return false;
		}

		final Date endingDate = accommodationEntryGroup.getEndingDate();
		for (Date date = accommodationEntryGroup.getStartingDate(); !TravelDateUtils
				.isSameDate(date, endingDate); date = DateUtils
				.addDays(date, 1))
		{

			final Integer stockForDate = getBcfProductFacade().getStockForDate(accommodation, date,
					numberOfRooms, Collections.singletonList(accommodationEntryGroup.getAccommodationOffering()));

			if (stockForDate==null || stockForDate < numberOfRooms)
			{
				LOG.error(String.format("Accommodation stock not available for cart [%s]", accommodationEntry.getOrder().getCode()));
				return false;
			}
		}
		return true;
	}


	/**
	 * Checks if is activity stock available.
	 *
	 * @param activityEntry the activity entry
	 * @return true, if is activity stock available
	 */
	protected boolean isActivityStockAvailable(final AbstractOrderEntryModel activityEntry)
	{
		final int qty = activityEntry.getQuantity().intValue();

		final ProductModel activityProduct = activityEntry.getProduct();

		final Date activityDate = activityEntry.getActivityOrderEntryInfo().getActivityDate();
		final String activityTime = activityEntry.getActivityOrderEntryInfo().getActivityTime();

		final String activityDateStr = TravelDateUtils.convertDateToStringDate(activityDate, BcfFacadesConstants.ACTIVITY_DATE_FORMAT);

		final String availabilityStatus = bcfTravelCartFacade
				.getActivityAvailabilityStatus(activityProduct.getCode(), activityDate,
						activityTime, qty);

		if (qty > 0 && (AvailabilityStatus.AVAILABLE.name().equals(availabilityStatus) || AvailabilityStatus.ON_REQUEST.name().equals(availabilityStatus)))
		{
			return true;
		}

		LOG.error(
				String.format("Activity stock not available for cart [%s], activity [%s], Date [%s%s]",
						activityEntry.getOrder().getCode(),
						activityProduct.getCode(), activityDateStr, activityTime));
		return false;
	}

	/**
	 * Throw stock not available exception.
	 *
	 * @param cartCode the cart code
	 * @throws OrderProcessingException the order processing exception
	 */
	private void throwStockNotAvailableException(final String cartCode) throws OrderProcessingException
	{
		LOG.error(String.format("Stock not available for order [%s]", cartCode));
		throw new OrderProcessingException("Stock not available");
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}
}
