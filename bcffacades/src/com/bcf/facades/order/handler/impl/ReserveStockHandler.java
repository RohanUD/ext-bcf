/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler.impl;

import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.strategies.stock.TravelManageStockByEntryTypeStrategy;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import de.hybris.platform.tx.Transaction;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.handler.AbstractCartPreprocessorHandler;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;


public class ReserveStockHandler implements AbstractCartPreprocessorHandler
{
	private static final Logger LOG = Logger.getLogger(ReserveStockHandler.class);

	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private WarehouseService warehouseService;
	private TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private ModelService modelService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfProductFacade bcfProductFacade;

   @Override
   public void handle(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
         final PlaceOrderResponseData decisionData, final CartModel cartModel) throws InsufficientStockLevelException
   {
      reserveStock(typeWiseEntries, cartModel, decisionData.isEarlyStockAllocation());
   }

   protected void reserveStock(final Map<OrderEntryType, List<AbstractOrderEntryModel>> typeWiseEntries,
			final CartModel cartModel, final boolean earlyStockAllocation) throws InsufficientStockLevelException
   {
      if (earlyStockAllocation)
      {
      	boolean stockAllocated=false;
         final Transaction currentTransaction = Transaction.current();
         boolean executed = false;
         try
         {
            LOG.debug(String.format("Reserving accommodation stock for cart [%s]", cartModel.getCode()));
            currentTransaction.begin();

            if (Objects.nonNull(typeWiseEntries.get(OrderEntryType.ACCOMMODATION)))
				{
					for (final AbstractOrderEntryModel entry : typeWiseEntries.get(OrderEntryType.ACCOMMODATION))
					{
						if (entry.getProduct().getItemtype().equals(RoomRateProductModel._TYPECODE) && entry.getActive() && !entry
								.isStockAllocated() && isStockTypeStandard(entry))
						{
							getAccommodationManageStockStrategy().reserve(entry);
							entry.setStockAllocated(true);
							modelService.save(entry);
							stockAllocated = true;
						}
					}
				}
            LOG.debug(String.format("Reserving activity stock for cart [%s]", cartModel.getCode()));
            if (Objects.nonNull(typeWiseEntries.get(OrderEntryType.ACTIVITY)))
            {
               final List<AbstractOrderEntryModel> activityEntries = typeWiseEntries.get(OrderEntryType.ACTIVITY).stream()
                     .filter(entry -> entry.getActive() && !entry.isStockAllocated() && isStockTypeStandardForActivityProduct(
                           entry))
                     .collect(Collectors.toList());
               for (final AbstractOrderEntryModel activityEntry : activityEntries)
               {
                  reserveActivityStock(activityEntry);
                  activityEntry.setStockAllocated(true);
                  modelService.save(activityEntry);
						stockAllocated=true;
               }
            }

            if(stockAllocated && cartModel.getStockHoldTime()==0l){
					cartModel.setStockHoldTime(bcfTravelCommerceStockService.getStockHoldExpiryTimeInMs(cartModel.getSalesApplication()));
				}


            modelService.save(cartModel);
            executed = true;
         } catch (final ModelSavingException | InsufficientStockLevelException ex) {
            LOG.error("Error in reserveStock, Transaction RollBack : " + ex);
            throw ex;
         } finally
         {
            if (executed)
            {
               currentTransaction.commit();
            }
            else {
               currentTransaction.rollback();
            }
         }
      }
   }

	protected boolean isStockTypeStandardForActivityProduct(final AbstractOrderEntryModel entry)
	{
		if (!Objects.equals(StockLevelType.STANDARD, ((ActivityProductModel) entry.getProduct()).getStockLevelType()))
		{
			return false;
		}
		StockData stockData = null;
		final ActivityOrderEntryInfoModel activityOrderEntryInfoModel = entry.getActivityOrderEntryInfo();
		if (StringUtils.isNotEmpty(activityOrderEntryInfoModel.getActivityTime()))
		{
			stockData = bcfTravelCartFacade.getStockDataForDateAndTime(entry.getProduct().getCode(), activityOrderEntryInfoModel.getActivityDate(), activityOrderEntryInfoModel.getActivityTime(), entry.getQuantity().intValue());
		}
		else
		{
			stockData = getBcfProductFacade().getStockData(entry.getProduct(), activityOrderEntryInfoModel.getActivityDate(),
					entry.getQuantity().intValue(),
					Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
		}

		if (Objects.isNull(stockData))
		{
			return false;
		}

		return StockLevelType.STANDARD
				.equals(stockData.getStockLevelType());

	}

	protected boolean isStockTypeStandard(final AbstractOrderEntryModel accommodationEntry)
	{
		final AccommodationModel accommodation = getBcfAccommodationOfferingService()
				.getAccommodation((RoomRateProductModel) accommodationEntry.getProduct());
		if (!Objects.equals(StockLevelType.STANDARD, accommodation.getStockLevelType()))
		{
			return false;
		}

		final AccommodationOrderEntryGroupModel accommodationEntryGroup = getBcfTravelCommerceCartService()
				.getAccommodationOrderEntryGroup(accommodationEntry);

		if (Objects.isNull(accommodationEntryGroup))
		{
			return false;
		}

		final Date endingDate = accommodationEntryGroup.getEndingDate();
		for (Date date = accommodationEntryGroup.getStartingDate(); !TravelDateUtils
				.isSameDate(date, endingDate); date = DateUtils
				.addDays(date, 1))
		{
			final StockLevelModel stockLevelModel = getBcfTravelCommerceStockService()
					.getStockLevelModelForDate(accommodation, date,
							Collections.singletonList(accommodationEntryGroup.getAccommodationOffering()));
			if (Objects.nonNull(stockLevelModel) && Objects.equals(StockLevelType.STANDARD, stockLevelModel.getStockLevelType()))
			{
				final long stockQuantity = Long.valueOf(stockLevelModel.getAvailable() - stockLevelModel.getReserved());

				if (stockQuantity < 1)
				{
					stockLevelModel.setStockLevelType(StockLevelType.ONREQUEST);
					modelService.save(stockLevelModel);
					return false;
				}

			}
			else
			{
				return false;
			}
		}
		return true;
	}

	private void reserveActivityStock(final AbstractOrderEntryModel activityEntry) throws InsufficientStockLevelException
	{

		if(StringUtils.isNotEmpty(activityEntry.getActivityOrderEntryInfo().getActivityTime())){
			getBcfTravelCommerceStockService().reservePerDateTimeProduct(activityEntry.getProduct(),activityEntry.getActivityOrderEntryInfo().getActivityTime(),
					activityEntry.getActivityOrderEntryInfo().getActivityDate(), activityEntry.getQuantity().intValue(),
					Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));

		}else{
			getBcfTravelCommerceStockService().reservePerDateProduct(activityEntry.getProduct(),
					activityEntry.getActivityOrderEntryInfo().getActivityDate(), activityEntry.getQuantity().intValue(),
					Collections.singletonList(getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE)));
		}

	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected TravelManageStockByEntryTypeStrategy getAccommodationManageStockStrategy()
	{
		return accommodationManageStockStrategy;
	}

	@Required
	public void setAccommodationManageStockStrategy(final TravelManageStockByEntryTypeStrategy accommodationManageStockStrategy)
	{
		this.accommodationManageStockStrategy = accommodationManageStockStrategy;
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

   @Required
   public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
   {
      this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
   }

   public ModelService getModelService()
   {
      return modelService;
   }

   @Required
   public void setModelService(final ModelService modelService)
   {
      this.modelService = modelService;
   }

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}
}
