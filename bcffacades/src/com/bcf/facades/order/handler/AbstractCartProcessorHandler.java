/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.handler;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface AbstractCartProcessorHandler
{
	void handle(CartModel order,
			PlaceOrderResponseData decisionData)
			throws OrderProcessingException, InvalidCartException, IntegrationException;
}
