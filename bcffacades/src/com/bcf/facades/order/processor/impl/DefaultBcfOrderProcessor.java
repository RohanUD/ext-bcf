/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order.processor.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.handler.AbstractCartProcessorHandler;
import com.bcf.facades.order.processor.BcfOrderProcessor;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfOrderProcessor implements BcfOrderProcessor
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOrderProcessor.class);
	private List<AbstractCartProcessorHandler> handlers;

	@Override
	public void processOrder(final CartModel cart, final PlaceOrderResponseData decisionData)
			throws OrderProcessingException, InvalidCartException, IntegrationException
	{
		LOG.debug(String.format("Processing order [%s] with amount [%s]", cart.getCode(), cart.getAmountToPay()));
		for (final AbstractCartProcessorHandler handler : getHandlers())
		{
			handler.handle(cart, decisionData);
		}
	}

	protected List<AbstractCartProcessorHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<AbstractCartProcessorHandler> handlers)
	{
		this.handlers = handlers;
	}
}
