/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.order;

import java.util.List;
import com.bcf.facades.cart.AsmCommentsData;


public interface BcfAsmCommentsFacade
{
	List<AsmCommentsData> getAsmComments(final String orderCode);

	public List<AsmCommentsData> getAsmAccommodationSupplierComments(final String code, String ref, final String orderCode);

	public List<AsmCommentsData> getAsmActivitySupplierComments(final String code,String orderCode);

	void addAsmComment(final String orderCode, String comment);

	public void addAsmAccommodationSupplierComment(final String code, String ref, final String comment, String orderCode);

	public void addAsmActivitySupplierComment( final String code,final String comment,String orderCode);

}
