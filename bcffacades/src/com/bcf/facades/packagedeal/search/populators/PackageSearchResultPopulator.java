/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packagedeal.search.populators;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.packagedeal.search.response.data.PackageResponseData;


public class PackageSearchResultPopulator implements Populator<SearchResultValueData, PackageResponseData>
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private MediaService mediaService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;
	private CatalogVersionService catalogVersionService;

	@Override
	public void populate(final SearchResultValueData source, final PackageResponseData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(this.getValue(source, "code"));
		target.setDescription(this.getValue(source, "description"));
		target.setEndDate(this.getValue(source, "endDate"));
		target.setImageUrls(getImageData(this.getValue(source, "imageUrls")));
		target.setName(this.getValue(source, "name"));
		target.setPromotext(this.getValue(source, "promotext"));
		target.setStartDate(this.getValue(source, "startDate"));
		target.setUuid(this.getValue(source, "uuid"));
		target.setVersion(this.getValue(source, "version"));
		target.setHotels(this.getValue(source, "hotels"));
		target.setHotelLocationNames(this.getValue(source, "hotelLocationNames"));
		target.setHotelCityCodes(this.getValue(source, "hotelCityCodes"));
		target.setHotelGeographicalAreaCodes(this.getValue(source, "hotelGeographicalAreaCodes"));
		target.setRoomRateProductCodes(this.getValue(source, "roomRateProductCodes"));
		target.setActivityProductCodes(this.getValue(source, "activityProductCodes"));
		target.setPackageUrls(this.getValue(source, "packageUrls"));
		target.setRoomRateProductCodes(this.getValue(source, "roomRateProductCodes"));
	}

	private List<ImageData> getImageData(final List<String> packageImageUrls)
	{
		return StreamUtil.safeStream(packageImageUrls).map(this::createImageData).collect(Collectors.toList());
	}

	private ImageData createImageData(final String responsiveUrl)
	{
		final ImageData imageData = new ImageData();
		imageData.setUrl(responsiveUrl);
		return imageData;
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		return (T) source.getValues().get(propertyName);
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	public MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
