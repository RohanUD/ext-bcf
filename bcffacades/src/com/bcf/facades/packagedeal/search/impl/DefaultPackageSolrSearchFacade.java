/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packagedeal.search.impl;

import de.hybris.platform.commercefacades.search.data.SearchFilterQueryData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.LocationType;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.service.packagedeal.solrfacetsearch.PackageSearchService;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.packagedeal.search.PackageSolrSearchFacade;
import com.bcf.facades.packagedeal.search.response.data.PackageResponseData;
import com.bcf.facades.search.facetdata.PackageSearchPageData;
import com.bcf.facades.travel.data.BcfLocationData;
import com.google.common.collect.Sets;


public class DefaultPackageSolrSearchFacade<ITEM extends PackageSearchPageData> implements PackageSolrSearchFacade
{
	private static final String HOTEL_CITY_CODES = "hotelCityCodes";
	private static final String HOTEL_GEOGRAPHICAL_AREA_CODES = "hotelGeographicalAreaCodes";
	private static final String ACTIVITY_CODE = "activityProductCodes";
	private static final String QUERY_ALL = "*:*";
	private static final String BCF_VACATION_LOCATION_CODE = "bcfVacationLocationCode";
	private static final String SLASH = "/";

	@Resource
	private ThreadContextService threadContextService;

	@Resource
	private Converter<SearchQueryData, SolrSearchQueryData> solrSearchQueryDecoder;

	@Resource
	private PackageSearchService packageSearchService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource
	private Converter<PackageSearchPageData<SolrSearchQueryData, SearchResultValueData>,
			PackageSearchPageData<SearchStateData, ITEM>> packageSearchPageConverter;

	@Override
	public PackageSearchPageData searchDeals(final SearchStateData searchStateData)
	{
		return threadContextService.executeInContext(
				new ThreadContextService.Executor<PackageSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public PackageSearchPageData<SearchStateData, ITEM> execute()
					{
						final PackageSearchPageData packageSearchPageData = packageSearchPageConverter
								.convert(packageSearchService.doSearch(decodeState(searchStateData, null), null));
						return populateViewPackageUrl(packageSearchPageData);
					}
				});
	}

	@Override
	public PackageSearchPageData searchDeals(final SearchStateData searchStateData, final PageableData pageableData)
	{
		return threadContextService.executeInContext(
				new ThreadContextService.Executor<PackageSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public PackageSearchPageData<SearchStateData, ITEM> execute()
					{
						final PackageSearchPageData packageSearchPageData = packageSearchPageConverter
								.convert(packageSearchService.doSearch(decodeState(searchStateData, null), pageableData));
						return populateViewPackageUrl(packageSearchPageData);
					}
				});
	}

	@Override
	public PackageSearchPageData searchForActivity(final String activityCode)
	{
		return searchDeals(buildSearchStateData(ACTIVITY_CODE, Sets.newHashSet(activityCode)));
	}

	@Override
	public PackageSearchPageData searchForCity(final String cityCode)
	{
		return searchDeals(buildSearchStateData(HOTEL_CITY_CODES, Sets.newHashSet(cityCode)));
	}

	@Override
	public PackageSearchPageData searchForRegion(final String regionCode)
	{
		return searchDeals(buildSearchStateData(HOTEL_GEOGRAPHICAL_AREA_CODES, Sets.newHashSet(regionCode)));
	}

	@Override
	public PackageSearchPageData searchByBcfVacationLocationCode(final String bcfVacationLocationCode)
	{
		return searchDeals(buildSearchStateData(BCF_VACATION_LOCATION_CODE, Sets.newHashSet(bcfVacationLocationCode)));
	}

	@Override
	public PackageSearchPageData searchForLocation(final String locationCode)
	{
		PackageSearchPageData packageSearchPageData = null;
		final BcfLocationData location = bcfTravelLocationFacade.getLocation(locationCode);
		if (LocationType.GEOGRAPHICAL_AREA.getCode().equals(location.getLocationType()))
		{
			packageSearchPageData = searchForRegion(locationCode);
		}
		else if (LocationType.CITY.getCode().equals(location.getLocationType()))
		{
			packageSearchPageData = searchForCity(locationCode);
		}
		return packageSearchPageData;
	}

	@Override
	public SearchStateData buildSearchStateData(final String searchFilterKey, final Set searchFilterValues)
	{
		final SearchQueryData query = new SearchQueryData();
		final SearchStateData searchStateData = new SearchStateData();

		if (CollectionUtils.isNotEmpty(searchFilterValues))
		{
			final SearchFilterQueryData searchFilterQueryData = new SearchFilterQueryData();
			searchFilterQueryData.setKey(searchFilterKey);
			searchFilterQueryData.setValues(searchFilterValues);
			query.setFilterQueries(Arrays.asList(searchFilterQueryData));
		}
		query.setValue(QUERY_ALL);
		searchStateData.setQuery(query);
		return searchStateData;
	}

	protected SolrSearchQueryData decodeState(final SearchStateData searchState, final String categoryCode)
	{
		final SolrSearchQueryData searchQueryData = solrSearchQueryDecoder.convert(searchState.getQuery());
		if (categoryCode != null)
		{
			searchQueryData.setCategoryCode(categoryCode);
		}

		return searchQueryData;
	}

	/**
	 * @param packageSearchPageData
	 */
	protected PackageSearchPageData populateViewPackageUrl(final PackageSearchPageData packageSearchPageData)
	{
		if (packageSearchPageData != null && CollectionUtils.isNotEmpty(packageSearchPageData.getResults()))
		{
			final List<PackageResponseData> packageResponseList = packageSearchPageData.getResults();

			for (final PackageResponseData packageResponseData : packageResponseList)
			{
				final StringBuilder packageDealHotelListingUrlBuilder = new StringBuilder();
				packageDealHotelListingUrlBuilder.append(BcfTravelRouteUtil.translateName(packageResponseData.getName()));
				packageDealHotelListingUrlBuilder.append(SLASH);
				packageDealHotelListingUrlBuilder.append(packageResponseData.getCode());
				packageResponseData.setPackageDealHotelListingUrl(packageDealHotelListingUrlBuilder.toString());
			}
		}

		return packageSearchPageData;
	}


}
