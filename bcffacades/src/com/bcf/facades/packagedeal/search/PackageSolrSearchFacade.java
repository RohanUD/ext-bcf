/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packagedeal.search;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import java.util.Set;
import com.bcf.facades.search.facetdata.PackageSearchPageData;



public interface PackageSolrSearchFacade<ITEM extends PackageSearchPageData>
{
	PackageSearchPageData<SearchStateData, ITEM> searchDeals(SearchStateData searchStateData);

	PackageSearchPageData<SearchStateData, ITEM> searchDeals(final SearchStateData searchStateData,
			final PageableData pageableData);

	PackageSearchPageData<SearchStateData, ITEM> searchForCity(String cityCode);

	PackageSearchPageData<SearchStateData, ITEM> searchForRegion(String regionCode);

	PackageSearchPageData<SearchStateData, ITEM> searchForLocation(String locationCode);

	SearchStateData buildSearchStateData(String searchFilterKey, Set<String> searchFilterValues);

	PackageSearchPageData searchForActivity(String activityCode);

	PackageSearchPageData searchByBcfVacationLocationCode(String bcfVacationLocationCode);
}
