/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.fare.sorting.strategies;

import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.travelfacades.fare.sorting.strategies.AbstractResultSortingStrategy;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Implementation of {@link AbstractResultSortingStrategy} to sort the {@link FareSelectionData} in descending based on
 * the price
 */
public class PriceSortingStrategyDesc extends AbstractResultSortingStrategy
{
	@Override
	public void sortFareSelectionData(final FareSelectionData fareSelectionData)
	{
		Collections.sort(fareSelectionData.getPricedItineraries(), (pIt1, pIt2) -> {
			final int compareResult = getMaximumPrice(pIt2).compareTo(getMaximumPrice(pIt1));
			return compareResult != 0 ? compareResult : comparePricedItineraryByDepartureDate(pIt1, pIt2);

		});
	}

	/**
	 * Gets the maximum price.
	 *
	 * @param pricedItinerary
	 *           the priced itinerary
	 * @return the maximum price
	 */
	protected BigDecimal getMaximumPrice(final PricedItineraryData pricedItinerary)
	{
		final List<BigDecimal> prices = pricedItinerary.getItineraryPricingInfos().stream()
				.filter(ItineraryPricingInfoData::isAvailable).map(item -> item.getTotalFare().getTotalPrice().getValue())
				.collect(Collectors.toList());
		final Optional<BigDecimal> opt = prices.stream().max((p1, p2) -> p1.compareTo(p2));
		return opt.isPresent() ? opt.get() : BigDecimal.valueOf(Double.MIN_VALUE);
	}
}
