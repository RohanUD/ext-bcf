/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.fare.search;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import java.util.Date;
import java.util.List;
import com.bcf.facades.sailing.list.CalenderPriceData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfFareSearchFacade
{
	FareSelectionData performSearch(FareSearchRequestData fareSearchRequestData) throws IntegrationException;

	List<Date> getNextAvailabilityDates(FareSearchRequestData fareSearchRequestData,
			List<CalenderPriceData> calenderPriceDataList);
}
