/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */

package com.bcf.facades.fare.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.ScheduledRouteData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.travelfacades.fare.search.handlers.FareSearchHandler;
import de.hybris.platform.travelfacades.fare.search.handlers.impl.PricedItineraryBundleHandler;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.model.travel.CabinClassModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


/**
 * Concrete implementation of the {@link FareSearchHandler} interface. Handler is responsible for populating the
 * {@link ItineraryPricingInfoData} for each {@link PricedItineraryData} on {@link FareSelectionData}
 */
public class SailingPricedItineraryBundleHandler extends PricedItineraryBundleHandler
{

	/**
	 * Populates ItinerarypricingInformation to pricedItinerary For each transport offering there must be always only one
	 * bundle per bundle type.
	 */
	@Override
	public void handle(final List<ScheduledRouteData> scheduledRoutes, final FareSearchRequestData fareSearchRequestData,
			final FareSelectionData fareSelectionData)
	{
		for (final PricedItineraryData pricedItinerary : fareSelectionData.getPricedItineraries())
		{
			final CabinClassModel cabinClassModel = getCabinClass(fareSearchRequestData);
			populateItineraryPricingInformations(pricedItinerary, fareSearchRequestData, cabinClassModel);

			if (CollectionUtils.isEmpty(pricedItinerary.getItineraryPricingInfos()))
			{
				continue;
			}
			pricedItinerary.getItineraryPricingInfos().forEach(itineraryPricingInfo -> {
				if (itineraryPricingInfo.isAvailable())
				{
					populateFareInfoWithOriginDestinationTransportFacility(itineraryPricingInfo, fareSearchRequestData,
							pricedItinerary);
				}
			});
		}
	}

	@Override
	protected void populateItineraryPricingInformations(final PricedItineraryData pricedItinerary,
			final FareSearchRequestData fareSearchRequestData, final CabinClassModel cabinClassModel)
	{
		final List<TransportOfferingData> transportOfferings = getTransportOfferings(pricedItinerary);
		final List<ItineraryPricingInfoData> itineraryPricingInfos = new ArrayList<>();
		final Map<BundleType, List<BundleTemplateModel>> bundleMap = new HashMap<>();
		final List<BundleTemplateModel> defaultBundleTemplates = this.getTravelBundleTemplateService().getDefaultBundleTemplates(cabinClassModel);

		populateBundleMap(defaultBundleTemplates, bundleMap, false);
		populateItineraryPricingInfosFromBundleMap(bundleMap, transportOfferings, itineraryPricingInfos);
		setDueAtTerminalFlag(defaultBundleTemplates, itineraryPricingInfos, pricedItinerary);
		pricedItinerary.setItineraryPricingInfos(itineraryPricingInfos);
		pricedItinerary.setAvailable(pricedItinerary.getItineraryPricingInfos().stream().anyMatch(ItineraryPricingInfoData::isAvailable));
	}

	protected void populateFareInfoWithOriginDestinationTransportFacility(final ItineraryPricingInfoData itineraryPricingInfo,
			final FareSearchRequestData fareSearchRequestData, final PricedItineraryData pricedItinerary)
	{
		final List<PTCFareBreakdownData> ptsFareBreakdownList = new ArrayList<>();
		fareSearchRequestData.getPassengerTypes().forEach(
				passengerTypeQtyData -> populatePtcFareBreakdown(itineraryPricingInfo, passengerTypeQtyData, ptsFareBreakdownList));
		final List<VehicleFareBreakdownData> vehicleFareBreakdownList = new ArrayList<>();

		if (Objects.nonNull(fareSearchRequestData.getVehicleInfoData()))
		{
			populateVehicleFareBreakdown(itineraryPricingInfo, fareSearchRequestData.getVehicleInfoData()
					.get(0), vehicleFareBreakdownList);
		}
		if (CollectionUtils.isNotEmpty(fareSearchRequestData.getLargeItems()))
		{
			populateLargeItemsFareBreakdown(itineraryPricingInfo, fareSearchRequestData.getLargeItems());
		}
		final List<TransportOfferingData> transportOfferings = itineraryPricingInfo.getBundleTemplates().get(0)
				.getTransportOfferings();
		if (transportOfferings != null)
		{
			itineraryPricingInfo.getPtcFareBreakdownDatas().forEach(ptcBreakdown -> ptcBreakdown.getFareInfos().forEach(fareInfo -> {
				fareInfo.setOrigin(transportOfferings.get(0).getSector().getOrigin());
				fareInfo.setDestination(transportOfferings.get(transportOfferings.size() - 1).getSector().getDestination());
			}));
		}
	}

	private void setDueAtTerminalFlag(final List<BundleTemplateModel> defaultBundleTemplates,
			final List<ItineraryPricingInfoData> itineraryPricingInfos, final PricedItineraryData pricedItinerary)
	{
		StreamUtil.safeStream(defaultBundleTemplates).forEach(bundleTemplateModel -> {
					StreamUtil.safeStream(itineraryPricingInfos).forEach(itineraryPricingInfo -> {
								if (StringUtils.equals(bundleTemplateModel.getType().getCode(), itineraryPricingInfo.getBundleType()))
								{
									itineraryPricingInfo.setCalculateDueAtTerminal(bundleTemplateModel.getCalculateDueAtTerminal());
								}
							}
					);
				}
		);
		if (pricedItinerary.getItinerary().getRoute().isFullyPrepaid() && CollectionUtils.isNotEmpty(itineraryPricingInfos))
		{
			final List<ItineraryPricingInfoData> removeFareAtTerminalBundleType = new ArrayList<>();
			itineraryPricingInfos.stream().forEach(itineraryPricingInfoData -> {
				if (BundleType.FARE_AT_TERMINAL.getCode().equals(itineraryPricingInfoData.getBundleType()))
				{
					removeFareAtTerminalBundleType.add(itineraryPricingInfoData);
				}
			});
			itineraryPricingInfos.removeAll(removeFareAtTerminalBundleType);
		}
	}

	private void populateVehicleFareBreakdown(final ItineraryPricingInfoData itineraryPricingInfo,
			final VehicleTypeQuantityData vehicleTypeQuantity, final List<VehicleFareBreakdownData> vehicleFareBreakdownList)
	{
		final VehicleFareBreakdownData vehicleFareBreakDownData = new VehicleFareBreakdownData();
		vehicleFareBreakDownData.setVehicleTypeQuantity(vehicleTypeQuantity);
		vehicleFareBreakdownList.add(vehicleFareBreakDownData);
		itineraryPricingInfo.setVehicleFareBreakdownDatas(vehicleFareBreakdownList);
	}

	private void populatePtcFareBreakdown(final ItineraryPricingInfoData itineraryPricingInfo,
			final PassengerTypeQuantityData passengerTypeQtyData, final List<PTCFareBreakdownData> ptsFareBreakdownList)
	{
		final List<FareInfoData> fareInfos = new ArrayList<>();
		final FareInfoData fareInfo = new FareInfoData();

		final List<FareDetailsData> fareDetails = new ArrayList<>();
		final FareDetailsData fareDetailsData = new FareDetailsData();

		final FareProductData fareProductData = new FareProductData();
		/**
		 * Need to fetch it from products, but for now it is okay as everywhere like in BundleType below, the
		 * fareBasisCode and bundleType value are same
		 */
		fareProductData.setFareBasisCode(itineraryPricingInfo.getBundleTemplates().get(0).getBundleType());

		fareDetailsData.setFareProduct(fareProductData);

		fareDetails.add(fareDetailsData);

		fareInfo.setFareDetails(fareDetails);

		fareInfos.add(fareInfo);


		final PTCFareBreakdownData ptcFareBreakDownData = new PTCFareBreakdownData();
		ptcFareBreakDownData.setPassengerTypeQuantity(passengerTypeQtyData);
		ptcFareBreakDownData.setFareInfos(fareInfos);
		ptsFareBreakdownList.add(ptcFareBreakDownData);
		itineraryPricingInfo.setPtcFareBreakdownDatas(ptsFareBreakdownList);
	}

	private void populateLargeItemsFareBreakdown(final ItineraryPricingInfoData itineraryPricingInfo,
			final List<LargeItemData> largeItemData)
	{
		final List<LargeItemData> largeItemDataList = new ArrayList<>();
		StreamUtil.safeStream(largeItemData).forEach(item -> {
			final LargeItemData itemData = new LargeItemData();
			itemData.setCode(item.getCode());
			itemData.setName(item.getName());
			largeItemDataList.add(item);
		});
		itineraryPricingInfo.setLargeItemsBreakdownDataList(largeItemDataList);
	}
}
