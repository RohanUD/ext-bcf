/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.facades.fare.search.handlers.impl;

import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.travelfacades.fare.search.handlers.impl.FareAvailabilityHandler;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.log4j.Logger;


public class SailingAvailabilityHandler extends FareAvailabilityHandler
{
	private static final Logger LOG = Logger.getLogger(SailingAvailabilityHandler.class);

	@Override
	protected void checkFareProductAvailability(final List<String> transportOfferingCodes, final int passengerNumber,
			final Map<String, TransportOfferingModel> transportOfferingModelMap, final List<FareProductData> filteredFares,
			final FareProductData fareProduct, final ProductModel productModel)
	{
		Long finalStockLevel = Long.valueOf(-1);

		boolean available = true;
		for (final String transportOfferingCode : transportOfferingCodes)
		{
			try
			{
				final Collection<TransportOfferingModel> transportOfferings = new ArrayList<>();
				transportOfferings.add(transportOfferingModelMap.get(transportOfferingCode));

				Long stockLevel = getCommerceStockService().getStockLevel(productModel, transportOfferings);
				if (stockLevel != null && stockLevel.intValue() < passengerNumber)
				{
					LOG.debug("Insufficient stock for Fare Product (code: " + fareProduct.getCode()
							+ ") and Transport Offering (code: " + transportOfferingCode);
					available = false;
					break;
				}
				if (Objects.isNull(stockLevel))
				{
					stockLevel = Long.valueOf(-1);
				}
				if (stockLevel < finalStockLevel || finalStockLevel.longValue() == -1)
				{
					finalStockLevel = stockLevel;
				}
			}
			catch (final StockLevelNotFoundException ex)
			{
				LOG.debug("Stocklevel not found for FareProduct with code: " + fareProduct.getCode() + " and TransportOffering: "
						+ transportOfferingCode, ex);
				available = false;
			}
		}

		if (available)
		{
			filteredFares.add(fareProduct);

			final StockData stock = new StockData();
			stock.setStockLevel(finalStockLevel);
			fareProduct.setStock(stock);
		}
	}

}
