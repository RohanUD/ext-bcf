/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.fare.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.OriginDestinationOptionData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.ScheduledRouteData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TransportVehicleData;
import de.hybris.platform.travelfacades.fare.search.handlers.impl.ItineraryHandler;
import de.hybris.platform.travelfacades.util.TransportOfferingUtils;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.sailing.list.LayOverData;


public class BcfItineraryHandler extends ItineraryHandler
{
	@Override
	public void handle(
			final List<ScheduledRouteData> scheduledRoutes, final FareSearchRequestData fareSearchRequestData,
			final FareSelectionData fareSelectionData)
	{
		List<PricedItineraryData> pricedItineraryDataList = this
				.populateItineraryInformations(scheduledRoutes, fareSelectionData.getPricedItineraries(), fareSearchRequestData);
		populateTransferOfferingData(pricedItineraryDataList);
		if (CollectionUtils.isNotEmpty(pricedItineraryDataList))
		{
			pricedItineraryDataList = pricedItineraryDataList.stream().filter(
					pricedItineraryData -> pricedItineraryData.getItinerary().getOriginDestinationOptions().stream().noneMatch(
							originDestinationOptionData -> CollectionUtils.isEmpty(originDestinationOptionData.getTransportOfferings())))
					.collect(
							Collectors.toList());
		}
		fareSelectionData.setPricedItineraries(pricedItineraryDataList);
	}

	@Override
	protected ItineraryData createItineraryData(final FareSearchRequestData fareSearchRequestData,
			final ScheduledRouteData scheduledRoute)
	{
		final ItineraryData itineraryData = new ItineraryData();
		itineraryData.setRoute(scheduledRoute.getRoute());
		itineraryData.setOriginDestinationOptions(this.createOriginDestinationOptionData(fareSearchRequestData, scheduledRoute));
		itineraryData.setDuration(TransportOfferingUtils.calculateJourneyDuration(
				itineraryData.getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings()));
		itineraryData.setTripType(fareSearchRequestData.getTripType());
		return itineraryData;
	}

	protected void populateTransferOfferingData(final List<PricedItineraryData> pricedItineraryDataList)
	{
		for (final PricedItineraryData pricedItineraryData : pricedItineraryDataList)
		{
			pricedItineraryData.getItinerary().getOriginDestinationOptions().forEach(originDestinationOptionData -> {
				final Map<String, List<TransportOfferingData>> transferOfferingSalingCodeMap = originDestinationOptionData
						.getTransportOfferings().stream().collect(Collectors.groupingBy(offering -> offering.getEbookingSailingCode()));
				originDestinationOptionData.setTransferTransportOfferings(sortByDepartureTime(transferOfferingSalingCodeMap));
			});
		}
	}

	protected List<OriginDestinationOptionData> createOriginDestinationOptionData(
			final FareSearchRequestData fareSearchRequestData,
			final ScheduledRouteData scheduledRoute)
	{

		List<TransportOfferingData> transportOfferingDataList = scheduledRoute.getTransportOfferings();
		final Optional<OriginDestinationInfoData> originDestinationOptionDataOptional = fareSearchRequestData
				.getOriginDestinationInfo().stream().filter(
						originDestinationInfoData -> originDestinationInfoData.getReferenceNumber() == scheduledRoute
								.getReferenceNumber()).findFirst();
		if (originDestinationOptionDataOptional.isPresent() && CollectionUtils
				.isNotEmpty(originDestinationOptionDataOptional.get().getTransportOfferings()))
		{
			final List<String> transportOfferingCodes = originDestinationOptionDataOptional.get().getTransportOfferings();
			transportOfferingDataList = scheduledRoute.getTransportOfferings().stream().filter(
					transportOfferingData -> transportOfferingCodes.stream()
							.anyMatch(code -> code.equals(transportOfferingData.getCode()))).collect(
					Collectors.toList());
		}
		final List<OriginDestinationOptionData> originDestinationOptions = new ArrayList();

		final int numberOfConnections = transportOfferingDataList.size();
		final List<LayOverData> layOverDataList = new ArrayList<>();

		if (numberOfConnections > 1)
		{
			for (int i = 0; i < numberOfConnections - 1; i++)
			{
				layOverDataList.add(createLayOverData(transportOfferingDataList.get(i), transportOfferingDataList.get(i + 1)));
			}
		}

		final OriginDestinationOptionData originDestinationOptionData = new OriginDestinationOptionData();
		originDestinationOptionData.setTransportOfferings(transportOfferingDataList);
		originDestinationOptionData.setDistinctTransportVehicles(getDistinctTransportVehicles(transportOfferingDataList));
		originDestinationOptionData.setLayOverData(layOverDataList);
		originDestinationOptions.add(originDestinationOptionData);

		return originDestinationOptions;
	}

	protected LayOverData createLayOverData(final TransportOfferingData firstTransportOffering,
			final TransportOfferingData nextTransportOffering)
	{
		final LayOverData layOverData = new LayOverData();
		final long diff = nextTransportOffering.getDepartureTime().getTime() - firstTransportOffering.getArrivalTime().getTime();
		final long diffDays = diff / (24 * 60 * 60 * 1000);
		final long diffHours = diff / (60 * 60 * 1000) % 24;
		final long diffMinutes = diff / (60 * 1000) % 60;
		layOverData.setLayoverDays((int) diffDays);
		layOverData.setLayoverHour((int) diffHours);
		layOverData.setLayoverMinutes((int) diffMinutes);

		return layOverData;
	}

	private List<TransportVehicleData> getDistinctTransportVehicles(final List<TransportOfferingData> transportOfferingDataList)
	{
		final List<TransportVehicleData> transportVehicleDataList = new ArrayList<>();
		StreamUtil.safeStream(transportOfferingDataList)
				.forEach(transportOfferingData -> transportVehicleDataList.add(transportOfferingData.getTransportVehicle()));
		return StreamUtil.safeStream(transportVehicleDataList).filter(StreamUtil.distinctByKey(p -> p.getVehicleInfo().getCode()))
				.collect(Collectors.toList());
	}

	private Map<String, List<TransportOfferingData>> sortByDepartureTime(
			final Map<String, List<TransportOfferingData>> transferOfferings)
	{
		final LinkedHashMap collect = transferOfferings.entrySet().stream()
				.sorted(Comparator.comparing(stringListEntry -> stringListEntry.getValue().get(0).getDepartureTime()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return collect;
	}
}
