/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.fare.search.impl;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.FeeProductData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.ScheduledRouteData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.fare.search.impl.DefaultFareSearchFacade;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.TransportOfferingService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.ProductAvailabilityEnum;
import com.bcf.core.enums.RouteType;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.ferry.AccessibilityItemData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfCalenderViewHelper;
import com.bcf.facades.price.calculation.BcfPriceCalculationFacade;
import com.bcf.facades.sailing.list.CalenderPriceData;
import com.bcf.integration.common.data.FareDetail;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integration.common.data.Sailing;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.service.BcfListSailingsIntegrationService;
import com.bcf.integration.listSailingResponse.data.BcfListSailingsResponseData;
import com.bcf.integration.listSailingResponse.data.ProductAvailability;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfFareSearchFacade extends DefaultFareSearchFacade implements BcfFareSearchFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfFareSearchFacade.class);
	private BcfListSailingsIntegrationService bcfListSailingsIntegrationService;
	private TimeService timeService;
	private TransportOfferingService transportOfferingService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private ConfigurationService configurationService;
	private SessionService sessionService;
	private BcfTransportVehicleService bcfTransportVehicleService;
	private AncillaryProductFacade ancillaryProductFacade;
	/**
	 * @deprecated Deprecated since version 3.0.
	 */
	@Deprecated
	private PriceDataFactory priceDataFactory;
	private CommerceCommonI18NService commerceCommonI18NService;
	private AssistedServiceFacade assistedServiceFacade;
	private BcfCalenderViewHelper bcfCalenderViewHelper;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BcfPriceCalculationFacade bcfPriceCalculationFacade;
	private BcfProductFacade bcfProductFacade;

	@Override
	public FareSelectionData performSearch(final FareSearchRequestData fareSearchRequestData) throws IntegrationException
	{
		FareSelectionData fareSelectionData = null;
		final String origin = fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get().getDepartureLocation();
		final String destination = fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get().getArrivalLocation();

		LOG.debug("STEP-1 :" + String.format("Calling getSailingsList() for route [%s] to [%s]", origin, destination));
		final List<BcfListSailingsResponseData> sailingsList = getBcfListSailingsIntegrationService()
				.getSailingsList(fareSearchRequestData);

		LOG.debug("STEP-2 :" + String.format("Calling FilterSailingIfPriceAreNull for route [%s] to [%s]", origin, destination));
		filterSailingForNullPrices(sailingsList);

		LOG.debug("STEP-3 :" + String.format("Calling getScheduledRoutes() for route [%s] to [%s]", origin, destination));
		final List<ScheduledRouteData> internalScheduledRoutes = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(getAllTransferIdentifiers(sailingsList)))
		{
			internalScheduledRoutes.addAll(BcfTransportOfferingFacade.class
					.cast(getTransportOfferingFacade())
					.getsheduledRoutesForTransferSailingFromResponse(sailingsList,
							fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get(),
							getAllTransferIdentifiers(sailingsList)));
		}
		internalScheduledRoutes.addAll(BcfTransportOfferingFacade.class
				.cast(getTransportOfferingFacade())
				.getSheduledRoutesFromResponse(sailingsList,
						fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get(),
						getAllSailingCodes(sailingsList)));

		final List<ScheduledRouteData> matchedRoutes = new ArrayList<>();

		final List<String> externalKeyList = new ArrayList<>();

		LOG.debug("STEP-4 :" + String.format("Calling populateExternalKeyList() for route [%s] to [%s]", origin, destination));
		populateExternalKeyList(sailingsList, externalKeyList,
				fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get().getDepartureTime(),
				CollectionUtils.isNotEmpty(getAllTransferIdentifiers(sailingsList)));

		LOG.debug("STEP-5 :" + String.format("Calling filterScheduleRoutes() for route [%s] to [%s]", origin, destination));
		filterScheduleRoutes(internalScheduledRoutes, matchedRoutes, externalKeyList, fareSearchRequestData.getVehicleInfoData(),
				fareSearchRequestData.isTravellingAsWalkOn(),
				fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get().getDepartureTime());
		if (StringUtils.equals((RouteType.LONG.getCode()), fareSearchRequestData.getRouteType()))
		{
			setProductAvailabilityInSession(sailingsList, matchedRoutes);
		}
		LOG.debug("STEP-6 :" + String
				.format("Calling getFareSearchPipelineManager().executePipeline() for route [%s] to [%s]", origin, destination));
		fareSelectionData = getFareSearchPipelineManager().executePipeline(matchedRoutes, fareSearchRequestData);

		LOG.debug(
				"STEP-7 :" + String.format("Calling updateFareSelectionDataWithPrice() for route [%s] to [%s]", origin, destination));
		updateFareSelectionDataWithPrice(fareSelectionData, sailingsList);

		final boolean addMarginPrice = StringUtils.equals(BcfFacadesConstants.BOOKING_ALACARTE,
				getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY));

		if (addMarginPrice)
		{
			LOG.debug(
					"STEP-8 :" + String.format("Adding margin to priced itineraries for route [%s] to [%s]", origin, destination));
			getBcfPriceCalculationFacade().addMarginToPricedItineraries(fareSelectionData.getPricedItineraries());
		}

		LOG.debug("STEP-9 :" + String.format("Calling checkDepartedSailing() for route [%s] to [%s]", origin, destination));
		checkDepartedSailing(fareSelectionData);

		if (fareSearchRequestData.isShowCalenderView())
		{
			LOG.debug("STEP-10 :" + String.format("Updating calender data for route [%s] to [%s]", origin, destination));
			getBcfCalenderViewHelper()
					.updateCalenderDataWithMinPrice(fareSearchRequestData, sailingsList, fareSelectionData, addMarginPrice);
		}
		return fareSelectionData;
	}

	@Override
	public List<Date> getNextAvailabilityDates(final FareSearchRequestData fareSearchRequestData,
			final List<CalenderPriceData> calenderPriceDataList)
	{
		final List<Date> nextAvailabilityDates = new ArrayList<>();
		final Date requestedDate = fareSearchRequestData.getOriginDestinationInfo().stream().findFirst().get().getDepartureTime();
		StreamUtil.safeStream(calenderPriceDataList).forEach(calenderPriceData -> {
			if (calenderPriceData.getPricingDate().after(requestedDate))
			{
				nextAvailabilityDates.add(calenderPriceData.getPricingDate());
			}
		});
		return nextAvailabilityDates;
	}

	private void setProductAvailabilityInSession(final List<BcfListSailingsResponseData> sailingsList,
			final List<ScheduledRouteData> matchedSchedules)
	{
		final HashMap<String, List<ProductAvailability>> sailingCodeProductAvailabilityMap = getOrCreateAvailabilityMap();
		for (final BcfListSailingsResponseData listSailingResponse : sailingsList)
		{
			for (final Sailing sailing : listSailingResponse.getSailing())
			{
				for (final SailingLine line : sailing.getLine())
				{
					sailingCodeProductAvailabilityMap.put(line.getLineInfo().getSailingCode(),
							line.getProductAvailability());
				}
			}
		}
		for (final ScheduledRouteData schedule : matchedSchedules)
		{
			schedule.getTransportOfferings().forEach(offering -> {
				if (!sailingCodeProductAvailabilityMap.containsKey(offering.getEbookingSailingCode()))
				{
					sailingCodeProductAvailabilityMap.remove(offering.getSailingCode());
				}
				else
				{
					final TransportVehicleModel transportVehicle = getBcfTransportVehicleService()
							.getTransportVehicle(offering.getTransportVehicle().getVehicleInfo().getCode());
					final int minimumProductAvailability = Integer
							.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("minimumProductAvailability"));
					populateAvailibility(sailingCodeProductAvailabilityMap, offering, transportVehicle, minimumProductAvailability);
				}
			});
		}
		getSessionService()
				.setAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP, sailingCodeProductAvailabilityMap);
	}

	private void populateAvailibility(final HashMap<String, List<ProductAvailability>> sailingCodeProductAvailabilityMap,
			final TransportOfferingData offering, final TransportVehicleModel transportVehicle, final int minimumProductAvailability)
	{
		if (transportVehicle != null)
		{
			final Map<String, List<ProductModel>> ancillaryProductCodeMap = transportVehicle.getProducts().stream()
					.collect(Collectors.groupingBy(ancillary -> ancillary.getCode()));
			final List<ProductAvailability> productsAvailable = new ArrayList<>();
			if (Objects.nonNull(sailingCodeProductAvailabilityMap.get(offering.getEbookingSailingCode())))
			{
				productsAvailable.addAll(sailingCodeProductAvailabilityMap
						.get(offering.getEbookingSailingCode())
						.stream().filter(
								productAvailability -> productAvailability.getProductCategory().equals("AMENITY")
										&& ancillaryProductCodeMap.containsKey(productAvailability.getProductId()))
						.collect(Collectors.toList()));
			}
			if (productsAvailable.stream().allMatch(
					productAvailability ->
							productAvailability.getAvailableCapacity()
									== 0))
			{
				offering.setAvailability(ProductAvailabilityEnum.SOLDOUT);
			}
			else if (productsAvailable.stream().allMatch(
					productAvailability ->
							productAvailability.getAvailableCapacity()
									< minimumProductAvailability))
			{
				offering.setAvailability(ProductAvailabilityEnum.LIMITED);
			}
			else
			{
				offering.setAvailability(ProductAvailabilityEnum.AVAILABLE);
			}
		}
	}

	private HashMap<String, List<ProductAvailability>> getOrCreateAvailabilityMap()
	{
		final HashMap<String, List<ProductAvailability>> sailingCodeProductAvailabilityMap = new HashMap<>();
		if (Objects.nonNull(getSessionService().getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP)))
		{
			sailingCodeProductAvailabilityMap
					.putAll(getSessionService().getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP));
		}
		return sailingCodeProductAvailabilityMap;
	}

	protected List<String> getAllSailingCodes(final List<BcfListSailingsResponseData> sailingsList)
	{
		final List<String> sailingCodesInResponse = new ArrayList<>();
		for (final BcfListSailingsResponseData sailingsResponse : sailingsList)
		{
			for (final Sailing ferry : sailingsResponse.getSailing())
			{
				for (final SailingLine sailingLine : ferry.getLine())
				{
					sailingCodesInResponse.add(sailingLine.getLineInfo().getSailingCode());
				}
			}
		}
		return sailingCodesInResponse;
	}

	protected List<String> getAllTransferIdentifiers(final List<BcfListSailingsResponseData> sailingsList)
	{
		final List<String> transferIdentifiersInResponse = new ArrayList<>();
		for (final BcfListSailingsResponseData sailingsResponse : sailingsList)
		{
			for (final Sailing ferry : sailingsResponse.getSailing())
			{
				if (StringUtils.isNotEmpty(ferry.getTransferSailingIdentifier()))
				{
					transferIdentifiersInResponse.add(ferry.getTransferSailingIdentifier());
				}
			}
		}
		return transferIdentifiersInResponse;
	}

	/**
	 * This below logic will generate a key using origin, destination, arrival, departure time so that a sailing coming
	 * from eBooking can be compared with the schedules maintained in hybris
	 */
	protected void populateExternalKeyList(final List<BcfListSailingsResponseData> sailingsList,
			final List<String> externalKeyList, final Date currentDate, final boolean isTransferSailing)
	{
		for (final BcfListSailingsResponseData sailingsResponse : sailingsList)
		{
			for (final Sailing ferry : sailingsResponse.getSailing())
			{
				for (final SailingLine sailingLine : ferry.getLine())
				{
					addExternalKey(externalKeyList, currentDate, isTransferSailing, sailingLine);
				}
			}
		}
	}

	private void addExternalKey(final List<String> externalKeyList, final Date currentDate, final boolean isTransferSailing,
			final SailingLine sailingLine)
	{
		if (isTransferSailing)
		{
			externalKeyList.add(sailingLine.getLineInfo().getSailingCode());
		}
		else
		{
			final boolean isMockEnable = getConfigurationService().getConfiguration()
					.getBoolean("ebooking.sailing.list.V2.service.url.mock.enable", Boolean.FALSE);
			String datepattern = BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN;
			if (isMockEnable)
			{
				datepattern = BcfintegrationcoreConstants.DATE_TIME_FORMAT;
			}
			final Date date = TravelDateUtils.convertStringDateToDate(sailingLine.getLineInfo().getDepartureDateTime(),
					datepattern);
			if (DateTimeComparator.getDateOnlyInstance().compare(date, currentDate) == 0)
			{
				externalKeyList.add(sailingLine.getLineInfo().getSailingCode());
			}
		}
	}

	/**
	 * The below logic will create an internal key using origin, destination, departureTime, arrivalTime so that a
	 * schedule maintained in hybris can be filtered out if it is not coming from eBookings
	 */
	protected void filterScheduleRoutes(final List<ScheduledRouteData> internalScheduledRoutes,
			final List<ScheduledRouteData> matchedRoutes, final List<String> externalKeyList,
			final List<VehicleTypeQuantityData> vehicleInfoData, final boolean travellingAsWalkOn,
			final Date departureTime)
	{
		final List<String> internalMatchedKeys = new ArrayList<>();
		for (final ScheduledRouteData route : internalScheduledRoutes)
		{
			for (final TransportOfferingData transportOfferingData : route.getTransportOfferings())
			{
				if (matchRoutes(matchedRoutes, externalKeyList, vehicleInfoData, travellingAsWalkOn, internalMatchedKeys, route,
						transportOfferingData))
					break;
			}
		}

		if (externalKeyList.size() != internalMatchedKeys.size())
		{
			externalKeyList.removeAll(internalMatchedKeys);
			LOG.error("eBooking response schedules not present in hybris" + externalKeyList);
		}
	}

	private boolean matchRoutes(final List<ScheduledRouteData> matchedRoutes, final List<String> externalKeyList,
			final List<VehicleTypeQuantityData> vehicleInfoData, final boolean travellingAsWalkOn,
			final List<String> internalMatchedKeys, final ScheduledRouteData route,
			final TransportOfferingData transportOfferingData)
	{
		if (route.getTransportOfferings().stream().allMatch(
				transportOfferingDataStream -> externalKeyList.contains(transportOfferingDataStream.getEbookingSailingCode()))
				)
		{
			if (travellingAsWalkOn)
			{
				internalMatchedKeys.add(transportOfferingData.getEbookingSailingCode());
				matchedRoutes.add(route);
				return true;
			}
			else if (CollectionUtils.isNotEmpty(vehicleInfoData) &&
					(StringUtils.equals(BcfCoreConstants.MOTORCYCLE_WITH_TRAILER, vehicleInfoData.get(0).getVehicleType().getCode()))
					|| (StringUtils.equals(BcfCoreConstants.MOTORCYCLE_STANDARD, vehicleInfoData.get(0).getVehicleType().getCode())))
			{
				internalMatchedKeys.add(transportOfferingData.getEbookingSailingCode());
				matchedRoutes.add(route);
				return true;
			}
			else
			{
				final TransportOfferingModel offering = getTransportOfferingService()
						.getTransportOffering(transportOfferingData.getCode());
				final ShipInfoModel shipInfo = (ShipInfoModel) offering.getTransportVehicle()
						.getTransportVehicleInfo();
				if (Objects.isNull(shipInfo) || Objects.isNull(shipInfo.getLengthThreshold()) || Objects
						.isNull(shipInfo.getHeightThreshold()))
				{
					LOG.warn(String.format("Either shipInfo or threshold values are missing for vessel [%s] for schedule [%s]",
							offering.getTransportVehicle().getCode(), offering.getCode()));
					return false;
				}
				if (CollectionUtils.isNotEmpty(vehicleInfoData) && (
						vehicleInfoData.get(0).getLength() <= shipInfo.getLengthThreshold()
								&& vehicleInfoData.get(0).getHeight() <= shipInfo.getHeightThreshold()))
				{
					internalMatchedKeys.add(transportOfferingData.getEbookingSailingCode());
					matchedRoutes.add(route);
					return true;
				}
				else
				{
					LOG.warn(String.format(
							"Requested vehicle dimensions are exceeding vessel threshold for vessel [%s] and schedule [%s], "
									+ "requested length is [%s] and vessel length threshold is [%s], "
									+ "requested height is [%s] and vessel height threshold is [%s],",
							offering.getTransportVehicle().getCode(), offering.getCode(),
							vehicleInfoData.get(0).getLength(), shipInfo.getLengthThreshold(), vehicleInfoData.get(0).getHeight(),
							shipInfo.getHeightThreshold()
					));
				}
			}
		}
		else
		{
			return true;
		}
		return false;
	}


	/**
	 * This method updates the PricedItinerary with the prices coming from eBooking
	 */
	protected void updateFareSelectionDataWithPrice(final FareSelectionData fareSelectionData,
			final List<BcfListSailingsResponseData> sailingsList)
	{
		for (final PricedItineraryData pricedItinerary : fareSelectionData.getPricedItineraries())
		{
			final List<TransportOfferingData> transportOfferings = pricedItinerary.getItinerary().getOriginDestinationOptions()
					.get(0).getTransportOfferings();

			List<Sailing> sailings = new ArrayList<>();

			if (sailingsList.stream().findFirst().isPresent())
			{
				sailings = sailingsList.stream().findFirst().get().getSailing();
			}

			for (final Sailing sailing : sailings)
			{
				if (Objects.nonNull(sailing.getTransferSailingIdentifier()))
				{
					pricedItinerary.setTransferSailingIdentifier(sailing.getTransferSailingIdentifier());
				}
				populateItineraryPricingInfo(transportOfferings, sailing, pricedItinerary);
				if (!pricedItinerary.getItinerary().getRoute().isFullyPrepaid())
				{
					setFareAtTerminal(transportOfferings, sailing, pricedItinerary);
				}
				checkAvailability(pricedItinerary, sailing.getLine());
			}
			populateSailingBookingDetails(pricedItinerary, sailings);
		}
	}

	protected void populateSailingBookingDetails(final PricedItineraryData pricedItineraryData,
			final List<Sailing> bcfSailingDataList)
	{
		String sailingCode = "";
		int bucketSumCapacity = 0;
		if (pricedItineraryData.getItinerary().getOriginDestinationOptions().stream().findFirst().isPresent() && pricedItineraryData
				.getItinerary().getOriginDestinationOptions().stream().findFirst().get().getTransportOfferings().stream().findFirst()
				.isPresent())
		{
			sailingCode = pricedItineraryData.getItinerary().getOriginDestinationOptions().stream().findFirst().get()
					.getTransportOfferings().stream().findFirst().get().getEbookingSailingCode();
		}
		for (final Sailing bcfSailingData : bcfSailingDataList)
		{
			if (Objects.nonNull(bcfSailingData.getLine()))
			{
				for (final SailingLine sailingLine : bcfSailingData.getLine())
				{
					bucketSumCapacity = getBucketSumCapacity(pricedItineraryData, sailingCode, bucketSumCapacity, sailingLine);
				}
			}
		}
		if (bucketSumCapacity == 0)
		{
			pricedItineraryData.setZeroSubBucketFreeCapacity(true);
		}
	}

	private int getBucketSumCapacity(final PricedItineraryData pricedItineraryData, final String sailingCode,
			int bucketSumCapacity, final SailingLine sailingLine)
	{
		if (sailingLine.getLineInfo().getSailingCode().equals(sailingCode))
		{
			pricedItineraryData
					.setIsBookable(sailingLine.getLineInfo().getIsBookable());
			for (final SailingPrices sailingPrice : sailingLine.getSailingPrices())
			{
				bucketSumCapacity += sailingPrice.getSubBucketFreeCapacity();
			}

		}
		return bucketSumCapacity;
	}

	private void setFareAtTerminal(final List<TransportOfferingData> transportOfferings, final Sailing sailing,
			final PricedItineraryData pricedItinerary)
	{
		for (int j = 0; j < transportOfferings.size(); j++)
		{
			if (isSameTransportOffering(transportOfferings.get(j), sailing.getLine()))
			{
				pricedItinerary.getItineraryPricingInfos().forEach(itineraryPricingInfoData -> {
					for (final SailingLine sailingLine : sailing.getLine())
					{
						sailingLine.getSailingPrices().forEach(sailingPrice -> {
							if (BundleType.STANDARD.getCode().equalsIgnoreCase(itineraryPricingInfoData.getBundleType()) && sailingPrice
									.getTariffType().equalsIgnoreCase(itineraryPricingInfoData.getBundleType()))
							{
								final String currencyIsoCode = getCommerceCommonI18NService().getCurrentCurrency().getIsocode();
								final long fareAtTerminalInCents =
										Long.valueOf(sailingPrice.getTotalAmountInCents());
								pricedItinerary.setFareAtTerminal(getTravelCommercePriceFacade()
										.createPriceData(
												(fareAtTerminalInCents
														- (StreamUtil.safeStream(itineraryPricingInfoData.getOtherCharges())
														.filter(otherChargeData -> Objects.nonNull(otherChargeData.getPrice())
														).mapToDouble(value -> value.getPrice().getValue().doubleValue()).sum() * 100)) / 100.0,
												currencyIsoCode));
							}
						});
					}
				});
			}
		}
	}

	private void populateItineraryPricingInfo(final List<TransportOfferingData> transportOfferings, final Sailing sailing,
			final PricedItineraryData pricedItinerary)
	{
		final List<SailingLine> lines = sailing.getLine();
		final List<ProductFare> otherChargesProductFares = sailing.getOtherCharges();
		transportOfferings.stream().collect(Collectors.groupingBy(TransportOfferingData::getEbookingSailingCode)).entrySet()
				.forEach(stringListEntry -> {
					if (isSameTransportOffering(stringListEntry.getValue().get(0), lines))
					{//make changes here to delete extra bundle data in fareselectiondata.
						for (final SailingLine sailingLine : lines)
						{
							if (CollectionUtils
									.isNotEmpty(sailingLine.getSailingPrices()))
							{
								populateItineraryPricining(sailingLine, pricedItinerary);
								populateBreakDownData(sailingLine, pricedItinerary, otherChargesProductFares,
										sailingLine.getLineInfo().getSailingCode());
								setAvailablilityToItineraryPriceInfo(pricedItinerary, sailingLine);
							}
						}
					}
				});
	}

	private void setAvailablilityToItineraryPriceInfo(final PricedItineraryData pricedItinerary, final SailingLine sailingLine)
	{
		StreamUtil.safeStream(pricedItinerary.getItineraryPricingInfos()).forEach(itineraryPricingInfoData -> {
			if (StringUtils.equalsIgnoreCase(itineraryPricingInfoData.getBundleType(), BundleType.FARE_AT_TERMINAL.getCode()))
			{
				itineraryPricingInfoData.setAvailable(true);
			}
			else
			{
				itineraryPricingInfoData.setAvailable(false);
			}
		});
		StreamUtil.safeStream(pricedItinerary.getItineraryPricingInfos()).forEach(itineraryPricingInfoData ->
				StreamUtil.safeStream(sailingLine.getSailingPrices()).forEach(sailingPrice ->
				{
					if (StringUtils.equalsIgnoreCase(sailingPrice.getTariffType(), itineraryPricingInfoData.getBundleType()))
					{
						itineraryPricingInfoData.setAvailable(true);
					}
				})
		);
	}

	protected void populateItineraryPricining(final SailingLine sailingLine,
			final PricedItineraryData pricedItinerary)
	{
		sailingLine.getSailingPrices().forEach(sailingPrice -> {
			if (Objects.nonNull(sailingPrice))
			{
				final Optional<ItineraryPricingInfoData> itineraryPricingInfo = pricedItinerary.getItineraryPricingInfos()
						.stream()
						.filter(pricingInfo -> sailingPrice.getTariffType().equalsIgnoreCase(pricingInfo.getBundleType()))
						.findFirst();
				if (itineraryPricingInfo.isPresent())
				{
					populateItinerayPricingInfoWithTotalFare(sailingPrice, itineraryPricingInfo.get());
				}
			}
		});
	}

	protected void populateBreakDownData(final SailingLine sailingLine, final PricedItineraryData pricedItinerary,
			final List<ProductFare> otherChargesProductFares, final String eBookingSailingCode)
	{
		pricedItinerary.getItineraryPricingInfos().forEach(itineraryPricingInfoData -> {
			sailingLine.getSailingPrices().forEach(sailingPrice -> {
				if (StringUtils.equalsIgnoreCase(sailingPrice.getTariffType(), itineraryPricingInfoData.getBundleType()))
				{
					if (Objects.nonNull(sailingPrice.getProductFares()) && !StringUtils
							.equalsIgnoreCase(eBookingSailingCode, itineraryPricingInfoData.getEBookingSailingCode()))
					{
						itineraryPricingInfoData.setEBookingSailingCode(eBookingSailingCode);
						sailingPrice.getProductFares().forEach(productFare -> {
							setItineraryPricingInfoData(itineraryPricingInfoData, productFare);
						});
					}
					final List<ProductFare> applicableOtherCharges = getApplicableOtherCharges(itineraryPricingInfoData,
							otherChargesProductFares);
					populateItineraryPricingInfo(applicableOtherCharges, itineraryPricingInfoData, sailingPrice,
							pricedItinerary.getItinerary().getRoute().isFullyPrepaid());
				}
			});
			final List<ProductFare> stowageFares = new ArrayList<>();
			StreamUtil.safeStream(sailingLine.getSailingPrices()).forEach(sailingPrice -> {
				StreamUtil.safeStream(sailingPrice.getProductFares()).forEach(productFare -> {
					if (StringUtils.equals(BcfCoreConstants.CATEGORY_STOWAGE, productFare.getProduct().getProductCategory()))
					{
						stowageFares.add(productFare);
					}
				});
			});
			populateLargeItemsBreakdownData(itineraryPricingInfoData, stowageFares);

			final List<ProductFare> accessibilityFares = new ArrayList<>();
			StreamUtil.safeStream(sailingLine.getSailingPrices()).forEach(sailingPrice -> {
				StreamUtil.safeStream(sailingPrice.getProductFares()).forEach(productFare -> {
					if (StringUtils.equals(BcfCoreConstants.CATEGORY_AMENITY, productFare.getProduct().getProductCategory()))
					{
						accessibilityFares.add(productFare);
					}
				});
			});
			populateAccessibilityBreakdownData(itineraryPricingInfoData, accessibilityFares);

			final List<ProductFare> applicableOtherCharges = getApplicableOtherCharges(itineraryPricingInfoData,
					otherChargesProductFares);
			if (CollectionUtils.isNotEmpty(applicableOtherCharges))
			{
				populateOtherCharges(itineraryPricingInfoData, applicableOtherCharges);
			}
		});
	}

	private void setItineraryPricingInfoData(final ItineraryPricingInfoData itineraryPricingInfoData,
			final ProductFare productFare)
	{
		if (productFare.getProduct().getProductCategory().equals(TravellerType.PASSENGER.getCode()))
		{
			itineraryPricingInfoData.getPtcFareBreakdownDatas().forEach(ptcFareBreakdownData -> {
				populatePTCBreakDownData(productFare, ptcFareBreakdownData);
			});
		}
		else if (productFare.getProduct().getProductCategory()
				.equals(TravellerType.VEHICLE.getCode()))
		{
			itineraryPricingInfoData.getVehicleFareBreakdownDatas().forEach(vehicleFareBreakdownData -> {
				populateVehicleBreakdownData(productFare, vehicleFareBreakdownData);
			});
		}
	}

	private void populateItineraryPricingInfo(final List<ProductFare> otherChargesProductFares,
			final ItineraryPricingInfoData itineraryPricingInfoData, final SailingPrices sailingPrice, final boolean isFullyPrepaid)
	{
		if (!isFullyPrepaid)
		{
			final long totalFare = Long.parseLong(sailingPrice.getTotalAmountInCents());
			final long otherCharges = StreamUtil.safeStream(otherChargesProductFares)
					.mapToLong(fare -> StreamUtil.safeStream(fare.getFareDetail()).findFirst().get().getGrossAmountInCents()).sum();
			final long previousToPayNow = (Objects.nonNull(itineraryPricingInfoData.getAmountToPayNow())
					&& itineraryPricingInfoData.getAmountToPayNow().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					Math.round(itineraryPricingInfoData.getAmountToPayNow().getValue().doubleValue() * 100) : 0;
			itineraryPricingInfoData.setAmountToPayNow(getTravelCommercePriceFacade()
					.createPriceData(Long.sum(otherCharges, previousToPayNow) / 100d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			final long previousDueAtTerminal = (Objects.nonNull(itineraryPricingInfoData.getAmountDueAtTerminal())
					&& itineraryPricingInfoData.getAmountDueAtTerminal().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					Math.round(itineraryPricingInfoData.getAmountDueAtTerminal().getValue().doubleValue() * 100) : 0;
			itineraryPricingInfoData.setAmountDueAtTerminal(getTravelCommercePriceFacade()
					.createPriceData(Long.sum((totalFare - otherCharges), previousDueAtTerminal) / 100d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
		}
		else
		{
			final long previousToPayNow = (Objects.nonNull(itineraryPricingInfoData.getAmountToPayNow())
					&& itineraryPricingInfoData.getAmountToPayNow().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					Math.round(itineraryPricingInfoData.getAmountToPayNow().getValue().doubleValue() * 100) : 0;
			itineraryPricingInfoData.setAmountToPayNow(getTravelCommercePriceFacade()
					.createPriceData(Long.sum(Long.parseLong(sailingPrice.getTotalAmountInCents()), previousToPayNow) / 100d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			itineraryPricingInfoData.setAmountDueAtTerminal(getTravelCommercePriceFacade()
					.createPriceData(0, getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
		}
	}

	protected void populateOtherCharges(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<ProductFare> otherChargesProductFares)
	{
		final List<OtherChargeData> otherCharges = otherChargesProductFares.stream().map(otherCategoryProductFare -> {
			OtherChargeData otherChargeData = new OtherChargeData();
			otherChargeData.setCode(otherCategoryProductFare.getProduct().getProductId());
			otherChargeData.setQuantity((int) (otherCategoryProductFare.getProduct().getProductNumber()));
			final long grossAmountInCents = otherCategoryProductFare.getFareDetail().stream()
					.mapToLong(fareDetail -> fareDetail.getGrossAmountInCents()).sum();
			otherChargeData.setPrice(getTravelCommercePriceFacade()
					.createPriceData(grossAmountInCents / 100d, getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));

			return otherChargeData;
		}).collect(Collectors.toList());
		itineraryPricingInfoData.setOtherCharges(otherCharges);
	}


	protected void populatePTCBreakDownData(final ProductFare productFare, final PTCFareBreakdownData ptcFareBreakdownData)
	{
		if ((Objects.nonNull(ptcFareBreakdownData.getPassengerTypeQuantity().getPassengerType().getBcfCode())
				&& ptcFareBreakdownData
				.getPassengerTypeQuantity().getPassengerType().getBcfCode()
				.equals(productFare.getProduct().getProductId())) && ptcFareBreakdownData.getFareInfos().stream().findFirst()
				.isPresent() && ptcFareBreakdownData.getFareInfos().stream()
				.findFirst().get().getFareDetails().stream().findFirst().isPresent())
		{
			final FareProductData fareProductData = new FareProductData();
			long totalAmount = 0;
			for (final FareDetail fareDetail : productFare.getFareDetail())
			{
				if (StringUtils.equals(fareDetail.getFareCode(), productFare.getProduct().getProductId()))
				{
					totalAmount += fareDetail.getGrossAmountInCents();
				}
			}
			final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(
					totalAmount / 100d),
					getCommerceCommonI18NService().getCurrentCurrency());
			fareProductData.setPrice(priceData);
			if (ptcFareBreakdownData.getFareInfos().stream().findFirst().isPresent() && ptcFareBreakdownData.getFareInfos()
					.stream()
					.findFirst().get().getFareDetails().stream().findFirst().isPresent())
			{
				ptcFareBreakdownData.getFareInfos().stream().findFirst().get().getFareDetails().stream().findFirst().get()
						.setFareProduct(fareProductData);
				if (Objects.nonNull(ptcFareBreakdownData.getTotalPrice())
						&& ptcFareBreakdownData.getTotalPrice().getValue().compareTo(BigDecimal.ZERO) > 0)
				{
					final double totalPrice = ptcFareBreakdownData.getTotalPrice().getValue().doubleValue() + (totalAmount / 100d);
					ptcFareBreakdownData.setTotalPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(
							totalPrice), getCommerceCommonI18NService().getCurrentCurrency()));
				}
				else
				{
					ptcFareBreakdownData.setTotalPrice(priceData);
				}
			}
		}
	}

	protected void populateVehicleBreakdownData(final ProductFare productFare,
			final VehicleFareBreakdownData vehicleFareBreakdownData)
	{
		if (vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode()
				.equals(productFare.getProduct().getProductId()) || (
				vehicleFareBreakdownData.getVehicleTypeQuantity().isCarryingLivestock() && StringUtils
						.equals(vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode()
										+ BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX,
								productFare.getProduct().getProductId())) || isCommercialVehicleExceedingWidth(productFare,
				vehicleFareBreakdownData))
		{
			final FareInfoData fareInfoData = new FareInfoData();
			long totalAmount = 0;
			for (final FareDetail fareDetail : productFare.getFareDetail())
			{
				if (StringUtils.equals(fareDetail.getFareCode(), productFare.getProduct().getProductId()))
				{
					totalAmount += fareDetail.getGrossAmountInCents();
				}
			}
			final List<FareDetailsData> fareDetailsDataList = new ArrayList<>();
			final FareDetailsData fareDetailsData = new FareDetailsData();
			final FareProductData fareProductData = new FareProductData();
			final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal
							.valueOf(totalAmount / 100d),
					getCommerceCommonI18NService().getCurrentCurrency());
			fareProductData.setPrice(priceData);
			fareDetailsData.setFareProduct(fareProductData);
			fareDetailsDataList.add(fareDetailsData);
			fareInfoData.setFareDetails(fareDetailsDataList);
			vehicleFareBreakdownData.setFareInfos(fareInfoData);
		}
	}

	private boolean isCommercialVehicleExceedingWidth(final ProductFare productFare,
			final VehicleFareBreakdownData vehicleFareBreakdownData)
	{
		final List<String> commercialVehicleCodes = BCFPropertiesUtils
				.convertToList(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
		if (commercialVehicleCodes.contains(vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode()))
		{
			final String longRouteVehicleCode =
					BcfCoreConstants.C_TYPE_VEHICLE_PREFIX + (
							((int) Math.ceil(vehicleFareBreakdownData.getVehicleTypeQuantity().getWidth())) - 1);
			final String shortRouteVehicleCode =
					BcfCoreConstants.S_TYPE_VEHICLE_PREFIX + (
							((int) Math.ceil(vehicleFareBreakdownData.getVehicleTypeQuantity().getWidth())) - 1);
			if (StringUtils.equals(productFare.getProduct().getProductId(), longRouteVehicleCode) ||
					StringUtils.equals(productFare.getProduct().getProductId(), shortRouteVehicleCode))
			{
				return true;
			}
		}
		return false;
	}

	protected void populateLargeItemsBreakdownData(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<ProductFare> stowageFares)
	{
		final Map<String, String> ancillaryProductNamesMap = getAncillaryProductNamesMap();
		final List<LargeItemData> largeItemDataList = new ArrayList<>();
		final Map<String, List<ProductFare>> stringListMap = StreamUtil.safeStream(stowageFares)
				.collect(Collectors.groupingBy(o -> o.getProduct().getProductId()));
		StreamUtil.safeStream(stringListMap.entrySet()).forEach(stringListEntry -> {
			final long amountInCents = StreamUtil.safeStream(stringListEntry.getValue())
					.mapToLong(value -> value.getFareDetail().get(0).getGrossAmountInCents()).sum();
			final LargeItemData largeItemData = new LargeItemData();
			largeItemData.setCode(stringListEntry.getKey());
			largeItemData.setName(ancillaryProductNamesMap.get(stringListEntry.getKey()));
			largeItemData.setQuantity(stringListEntry.getValue().size());
			largeItemData.setPrice(getTravelCommercePriceFacade()
					.createPriceData(amountInCents > 0 ? amountInCents / 100d : 0d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			largeItemDataList.add(largeItemData);
		});
		itineraryPricingInfoData.setLargeItemsBreakdownDataList(largeItemDataList);
	}

	protected void populateAccessibilityBreakdownData(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<ProductFare> accessibilityFares)
	{
		final Map<String, String> accessibilityProductNamesMap = getAccessibilityProductNamesMap();
		final List<AccessibilityItemData> accessibilityItemDataList = new ArrayList<>();
		final Map<String, List<ProductFare>> stringListMap = StreamUtil.safeStream(accessibilityFares)
				.collect(Collectors.groupingBy(o -> o.getProduct().getProductId()));
		StreamUtil.safeStream(stringListMap.entrySet()).forEach(stringListEntry -> {
			final long amountInCents = StreamUtil.safeStream(stringListEntry.getValue())
					.mapToLong(value -> value.getFareDetail().get(0).getGrossAmountInCents()).sum();
			final AccessibilityItemData accessibilityItemData = new AccessibilityItemData();
			accessibilityItemData.setCode(stringListEntry.getKey());
			accessibilityItemData.setName(accessibilityProductNamesMap.get(stringListEntry.getKey()));
			accessibilityItemData.setQuantity(
					(int) StreamUtil.safeStream(stringListEntry.getValue()).mapToLong(value -> value.getProduct().getProductNumber())
							.sum());
			accessibilityItemData.setPrice(getTravelCommercePriceFacade()
					.createPriceData(amountInCents > 0 ? amountInCents / 100d : 0d,
							getCommerceCommonI18NService().getCurrentCurrency().getIsocode()));
			accessibilityItemDataList.add(accessibilityItemData);
		});
		itineraryPricingInfoData.setAccessibilityBreakdownDataList(accessibilityItemDataList);
	}

	private Map<String, String> getAncillaryProductNamesMap()
	{
		final Map<String, String> ancillaryProductNamesMap = new HashMap<>();
		StreamUtil.safeStream(getAncillaryProductFacade().getAncillaryProductsForCodes(BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES))))
				.forEach(productData -> {
					ancillaryProductNamesMap.put(productData.getCode(), productData.getName());
				});
		return ancillaryProductNamesMap;
	}

	private Map<String, String> getAccessibilityProductNamesMap()
	{
		final Map<String, String> accessibilityProductNamesMap = new HashMap<>();
		StreamUtil.safeStream(getAncillaryProductFacade().getAncillaryProductsForCodes(BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS))))
				.forEach(productData -> {
					accessibilityProductNamesMap.put(productData.getCode(), productData.getName());
				});
		return accessibilityProductNamesMap;
	}

	/**
	 * handles a scenario where a line is not bookable
	 */
	protected void checkAvailability(final PricedItineraryData pricedItinerary, final List<SailingLine> lines)
	{
		/*
		 * TODO enable the logic to check if the logged in user is an agent or a normal customers as non-bookable sailings
		 * are bookable for agents
		 */
		pricedItinerary.setIsBookable(Boolean.TRUE);
		if (lines.stream().allMatch(sailingLine -> sailingLine.getLineInfo().getIsBookable()))
		{
			pricedItinerary.setIsBookable(Boolean.FALSE);
		}
	}

	/**
	 * Set departedSailing to true if it's a past sailing else true
	 */
	private void checkDepartedSailing(final FareSelectionData fareSelectionData)
	{
		for (final PricedItineraryData pricedItinerary : fareSelectionData.getPricedItineraries())
		{
			final TransportOfferingData transportOfferingData = pricedItinerary.getItinerary().getOriginDestinationOptions().stream()
					.findFirst().get().getTransportOfferings().stream().findFirst().get();
			final Date departureTime = transportOfferingData.getDepartureTime();

			final ZoneId departureTimeZoneId = transportOfferingData.getDepartureTimeZoneId();

			pricedItinerary.setDepartedSailing(TravelDateUtils.isBefore(departureTime, departureTimeZoneId,
					getTimeService().getCurrentTime(), ZoneId.systemDefault()));
		}
	}

	/**
	 * This method checks if the details (origin, destination, arrivalTime, departureTime) of a transport offering coming
	 * from each PricedItinerary is same as the details of a sailing line.
	 * <p>
	 * This logic could be replaced when eBooking will send transport offering code or number along with other line
	 * details so that instead of matching the above mentioned details, just code will be used to match a transport
	 * offering from the PricedItinerary and a line from the Sailing
	 */
	protected boolean isSameTransportOffering(final TransportOfferingData transportOffering, final List<SailingLine> line)
	{
		return line.stream().anyMatch(
				sailingLine -> sailingLine.getLineInfo().getSailingCode().equals(transportOffering.getEbookingSailingCode()));
	}

	protected void populateItinerayPricingInfoWithTotalFare(
			final SailingPrices price,
			final ItineraryPricingInfoData itineraryPricingInfo)
	{
		final String currencyIsoCode = getCommerceCommonI18NService().getCurrentCurrency().getIsocode();
		final TotalFareData totalFare = new TotalFareData();
		final PriceData totalPrice = getTravelCommercePriceFacade()
				.createPriceData(new Double(price.getTotalAmountInCents()) / 100.0, currencyIsoCode);
		totalFare.setTotalPrice(totalPrice);
		itineraryPricingInfo.setTotalFare(getTotalFare(totalFare, itineraryPricingInfo.getTotalFare()));
	}

	protected TotalFareData getTotalFare(final TotalFareData totalFare, final TotalFareData previousTotalFare)
	{
		final String currencyIsoCode = getCommerceCommonI18NService().getCurrentCurrency().getIsocode();
		if (Objects.nonNull(totalFare.getBasePrice()))
		{
			final BigDecimal previousBaseFare = (Objects.nonNull(previousTotalFare)
					&& Objects.nonNull(previousTotalFare.getBasePrice())
					&& previousTotalFare.getBasePrice().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					previousTotalFare.getBasePrice().getValue() : BigDecimal.ZERO;
			totalFare.setBasePrice(
					getPriceDataFactory().create(PriceDataType.BUY,
							totalFare.getBasePrice().getValue().add(previousBaseFare), currencyIsoCode));
		}
		if (Objects.nonNull(totalFare.getTaxPrice()))
		{
			final BigDecimal previousTaxPrice = (Objects.nonNull(previousTotalFare)
					&& Objects.nonNull(previousTotalFare.getTaxPrice())
					&& previousTotalFare.getTaxPrice().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					previousTotalFare.getTaxPrice().getValue() : BigDecimal.ZERO;
			totalFare.setTaxPrice(getPriceDataFactory().create(PriceDataType.BUY,
					totalFare.getTaxPrice().getValue().add(previousTaxPrice), currencyIsoCode));
		}
		if (Objects.nonNull(totalFare.getTotalPrice()))
		{
			final BigDecimal previousTotalPrice = (Objects.nonNull(previousTotalFare)
					&& Objects.nonNull(previousTotalFare.getTotalPrice())
					&& previousTotalFare.getTotalPrice().getValue().compareTo(BigDecimal.ZERO) > 0) ?
					previousTotalFare.getTotalPrice().getValue() : BigDecimal.ZERO;
			totalFare.setTotalPrice(getPriceDataFactory().create(PriceDataType.BUY,
					totalFare.getTotalPrice().getValue().add(previousTotalPrice), currencyIsoCode));
		}
		return totalFare;
	}

	protected void filterSailingForNullPrices(
			final List<BcfListSailingsResponseData> bcfListSailingsResponseDatas)
	{
		for (final BcfListSailingsResponseData bcfListSailingsResponseData : bcfListSailingsResponseDatas)
		{
			for (final Sailing sailing : bcfListSailingsResponseData.getSailing())
			{
				final List<SailingLine> sailingLinesWithPrices = new ArrayList<>();
				for (final SailingLine sailingLine : sailing.getLine())
				{
					final List<SailingPrices> sailingPrices = sailingLine.getSailingPrices().stream()
							.filter(sailingPrice -> StringUtils.isNotBlank(sailingPrice.getTotalAmountInCents())
									&& StringUtils.isNumeric(sailingPrice.getTotalAmountInCents()))
							.collect(Collectors.toList());
					if (CollectionUtils.isNotEmpty(sailingPrices) || !BooleanUtils
							.toBoolean(sailingLine.getLineInfo().getIsBookable()))
					{
						sailingLine.setSailingPrices(sailingPrices);
						sailingLinesWithPrices.add(sailingLine);
					}

				}
				sailing.setLine(sailingLinesWithPrices);
			}
			final List<Sailing> sailingWithPrices = bcfListSailingsResponseData.getSailing().stream()
					.filter(sailing -> CollectionUtils.isNotEmpty(sailing.getLine())).collect(
							Collectors.toList());
			bcfListSailingsResponseData.setSailing(sailingWithPrices);
		}
	}

	private List<ProductFare> getApplicableOtherCharges(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<ProductFare> otherChargesProductFares)
	{
		final List<ProductFare> applicableOtherCharges = new ArrayList<>();
		final List<FeeProductData> feeProductsForBundle = StreamUtil.safeStream(getBcfProductFacade().getFeeProducts())
				.filter(feeProductData -> StringUtils
						.equalsIgnoreCase(itineraryPricingInfoData.getBundleType(), feeProductData.getFlexibilityLevel()))
				.collect(Collectors.toList());
		final List<String> applicableOtherChargesCodes;
		if (CollectionUtils.isNotEmpty(feeProductsForBundle))
		{
			applicableOtherChargesCodes = StreamUtil.safeStream(feeProductsForBundle).map(feeProductData -> feeProductData.getCode())
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(applicableOtherChargesCodes))
			{
				StreamUtil.safeStream(otherChargesProductFares).forEach(productFare -> {
					if (applicableOtherChargesCodes.contains(productFare.getProduct().getProductId()))
					{
						applicableOtherCharges.add(productFare);
					}
				});
			}
		}
		return applicableOtherCharges;
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}


	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected BcfListSailingsIntegrationService getBcfListSailingsIntegrationService()
	{
		return bcfListSailingsIntegrationService;
	}

	@Required
	public void setBcfListSailingsIntegrationService(
			final BcfListSailingsIntegrationService bcfListSailingsIntegrationService)
	{
		this.bcfListSailingsIntegrationService = bcfListSailingsIntegrationService;
	}

	protected TransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final TransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BcfTransportVehicleService getBcfTransportVehicleService()
	{
		return bcfTransportVehicleService;
	}

	@Required
	public void setBcfTransportVehicleService(final BcfTransportVehicleService bcfTransportVehicleService)
	{
		this.bcfTransportVehicleService = bcfTransportVehicleService;
	}

	protected AncillaryProductFacade getAncillaryProductFacade()
	{
		return ancillaryProductFacade;
	}

	@Required
	public void setAncillaryProductFacade(final AncillaryProductFacade ancillaryProductFacade)
	{
		this.ancillaryProductFacade = ancillaryProductFacade;
	}

	protected BcfCalenderViewHelper getBcfCalenderViewHelper()
	{
		return bcfCalenderViewHelper;
	}

	@Required
	public void setBcfCalenderViewHelper(final BcfCalenderViewHelper bcfCalenderViewHelper)
	{
		this.bcfCalenderViewHelper = bcfCalenderViewHelper;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	protected BcfPriceCalculationFacade getBcfPriceCalculationFacade()
	{
		return bcfPriceCalculationFacade;
	}

	@Required
	public void setBcfPriceCalculationFacade(final BcfPriceCalculationFacade bcfPriceCalculationFacade)
	{
		this.bcfPriceCalculationFacade = bcfPriceCalculationFacade;
	}

	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

}
