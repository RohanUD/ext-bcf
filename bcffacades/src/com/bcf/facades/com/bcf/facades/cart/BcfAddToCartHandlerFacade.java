/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.com.bcf.facades.cart;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import java.util.List;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfAddToCartHandlerFacade
{
	AddBundleToCartResponseData performAddToCart(AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException, IntegrationException;//NOSONAR

	List<CartModificationData> performAddDealToCart(AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException, IntegrationException;//NOSONAR
}
