/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.manager.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;
import com.bcf.facades.reservation.manager.BcfPackageReservationDataListPipelineManager;


public class DefaultBcfPackageReservationDataListPipelineManager implements BcfPackageReservationDataListPipelineManager
{

	private List<ReservationDataListHandler> handlers;
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public ReservationDataList executePipeline(final AbstractOrderModel abstractOrderModel, final String travelRoute)
	{
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservationDatas = new ArrayList<>();

		final List<AbstractOrderEntryModel> abstractOrderEntries = abstractOrderModel.getEntries().stream()
				.filter(
						entry ->
						{
							if (OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects.nonNull(entry.getTravelOrderEntryInfo()))
							{
								entry.getTravelOrderEntryInfo().getTravelRoute()
										.getCode().equals(travelRoute);
								return true;
							}
							return false;
						}).sorted(Comparator.comparing(entry->entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())).collect(Collectors.toList());

		Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber = null;


		cartEntriesByJourneyRefNumber = abstractOrderEntries.stream()
				.filter(entry -> entry.getActive())
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		cartEntriesByJourneyRefNumber.keySet().forEach(key -> {
			final ReservationData reservationData = new ReservationData();
			reservationData.setJourneyReferenceNumber(key);
			reservationDatas.add(reservationData);
		});
		reservationDataList.setReservationDatas(reservationDatas);

		for (final ReservationDataListHandler handler : handlers)
		{
			handler.handle(abstractOrderModel, cartEntriesByJourneyRefNumber, reservationDataList);
		}

		return reservationDataList;
	}

	protected List<ReservationDataListHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ReservationDataListHandler> handlers)
	{
		this.handlers = handlers;
	}


	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

}
