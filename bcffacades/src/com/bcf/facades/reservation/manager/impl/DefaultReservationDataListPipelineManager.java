/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.manager.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;
import com.bcf.facades.reservation.manager.ReservationDataListPipelineManager;


public class DefaultReservationDataListPipelineManager implements ReservationDataListPipelineManager
{

	private List<ReservationDataListHandler> handlers;

	@Override
	public ReservationDataList executePipeline(final AbstractOrderModel abstractOrderModel)
	{
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservationDatas = new ArrayList<>();

		final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getActive())
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		cartEntriesByJourneyRefNumber.keySet().forEach(key -> {
			final ReservationData reservationData = new ReservationData();
			reservationData.setJourneyReferenceNumber(key);
			reservationDatas.add(reservationData);
		});
		reservationDataList.setReservationDatas(reservationDatas);

		for (final ReservationDataListHandler handler : handlers)
		{
			handler.handle(abstractOrderModel, cartEntriesByJourneyRefNumber, reservationDataList);
		}

		return reservationDataList;
	}

	protected List<ReservationDataListHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ReservationDataListHandler> handlers)
	{
		this.handlers = handlers;
	}

}
