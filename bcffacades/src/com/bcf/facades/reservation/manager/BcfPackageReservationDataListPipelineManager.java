/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.manager;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import com.bcf.facades.reservation.data.ReservationDataList;


public interface BcfPackageReservationDataListPipelineManager
{
	/**
	 * Execute pipeline reservation data.
	 *
	 * @param abstractOrderModel the abstract order model
	 * @param travelRoute        the travelRoute code
	 * @return the reservation data list
	 */
	ReservationDataList executePipeline(AbstractOrderModel abstractOrderModel, String travelRoute);
}
