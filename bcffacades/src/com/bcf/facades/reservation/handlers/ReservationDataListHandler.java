/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import java.util.Map;
import com.bcf.facades.reservation.data.ReservationDataList;


public interface ReservationDataListHandler
{
	/**
	 * Handle method.
	 *
	 * @param abstractOrderModel the abstract order model
	 * @param reservationData    the reservation data
	 */
	void handle(AbstractOrderModel abstractOrderModel, Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			ReservationDataList reservationDataList);
}
