/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.booking.action.strategies.impl.BCFOriginDestinationLevelActionStrategy;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListBookingActionHandler implements ReservationDataListHandler
{

	private BCFOriginDestinationLevelActionStrategy bookingActionStrategy;

	private List<ActionTypeOption> globalBookingActions;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		reservationDatas.forEach(reservationData -> {
			reservationData.setCode(abstractOrderModel.getCode());
			getBookingActionStrategy().applyStrategy(reservationDataList, reservationData);
		});
		populateGlobalBookingActionData(reservationDataList);
	}

	void populateGlobalBookingActionData(final ReservationDataList reservationDataList)
	{
		final List<ReservationItemData> reservationDataItems = reservationDataList.getReservationDatas().stream()
				.flatMap(reservationData -> reservationData.getReservationItems().stream()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(reservationDataItems))
		{
			return;
		}

		final List<BookingActionData> bookingActionList = new ArrayList<>();
		getGlobalBookingActions().forEach(globalAction -> {
			final List<ReservationItemData> filteredReservationDataItems = reservationDataItems.stream()
					.filter(reservationDataItem -> reservationDataItem.getBookingActionData().stream()
							.filter(bookingAction -> bookingAction.getActionType().equals(globalAction)).findAny().isPresent())
					.collect(Collectors.toList());
			if (CollectionUtils.size(filteredReservationDataItems) == CollectionUtils.size(reservationDataItems))
			{
				bookingActionList.add(filteredReservationDataItems.get(0).getBookingActionData().stream()
						.filter(action -> action.getActionType().equals(globalAction)).findAny().get());
			}
		});

		reservationDataList.setBookingActionData(bookingActionList);
	}


	protected BCFOriginDestinationLevelActionStrategy getBookingActionStrategy()
	{
		return bookingActionStrategy;
	}

	@Required
	public void setBookingActionStrategy(final BCFOriginDestinationLevelActionStrategy bookingActionStrategy)
	{
		this.bookingActionStrategy = bookingActionStrategy;
	}

	protected List<ActionTypeOption> getGlobalBookingActions()
	{
		return globalBookingActions;
	}

	@Required
	public void setGlobalBookingActions(final List<ActionTypeOption> globalBookingActions)
	{
		this.globalBookingActions = globalBookingActions;
	}

}
