/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationOfferPricingInfoHandler;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListOfferPricingInfoHandler extends ReservationOfferPricingInfoHandler
		implements ReservationDataListHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();
			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			final List<ReservationItemData> reservationItems = reservationData.getReservationItems();
			if (CollectionUtils.isEmpty(reservationItems))
			{
				return;
			}

			final List<AbstractOrderEntryModel> ancillaryEntries = entries.stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
					.filter(entry -> isAncillaryEntry(entry) && (isPerPaxEntry(entry) || isPerBookingEntry(entry)))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(ancillaryEntries))
			{
				reservationData.setOfferPricingInfos(createOfferPricingInfo(ancillaryEntries));
			}

		}

	}

}
