/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import reactor.util.CollectionUtils;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.AccommodationReservationReservedRoomStayHandler;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.reservation.handlers.BcfAccommodationReservationHandler;


public class BcfAccommodationReservationReservedRoomStayHandler extends AccommodationReservationReservedRoomStayHandler implements
		BcfAccommodationReservationHandler
{
	private BcfBookingService bcfBookingService;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;
	private BcfTravelLocationService bcfTravelLocationService;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Override
	public void handle(final AbstractOrderModel abstractOrder, final AccommodationReservationData accommodationReservationData)
	{

		final List<DealOrderEntryGroupModel> dealEntryGroups = bcfBookingService.getDealOrderEntryGroups(abstractOrder);

		List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = new ArrayList<>();
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(dealEntryGroups))
		{

			accommodationOrderEntryGroupModels = dealEntryGroups.stream()
					.flatMap(entryGroup -> entryGroup.getAccommodationEntryGroups().stream()).collect(Collectors.toList());
		}
		final List<ReservedRoomStayData> roomStays = new ArrayList<>();

		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{

			accommodationOrderEntryGroupModels.forEach(entryGroup -> {
				final ReservedRoomStayData roomStay = new ReservedRoomStayData();
				roomStay.setCheckInDate(entryGroup.getStartingDate());
				roomStay.setCheckOutDate(entryGroup.getEndingDate());
				roomStay.setAvailabilityStatus(getAvailabilityStatus(entryGroup));
				roomStay.setRoomStayRefNumber(entryGroup.getRoomStayRefNumber());
				roomStay.setRatePlans(Collections.singletonList(getRatePlanConverter().convert(entryGroup.getRatePlan())));
				roomStay.setRoomTypes(Collections.singletonList(getRoomTypeConverter().convert(entryGroup.getAccommodation())));
				final boolean isModifiable = entryGroup.getEntries().stream()
						.allMatch(entry -> entry.getActive() && AmendStatus.NEW.equals(entry.getAmendStatus()));
				roomStay.setNonModifiable(!isModifiable);
				roomStay.setLocationCode(getCityLocationCode(entryGroup.getAccommodationOffering()));
				setServices(entryGroup, roomStay, abstractOrder);
				setGuestCounts(entryGroup, roomStay);
				setGuestData(entryGroup, roomStay);
				setTotalsPerRoom(entryGroup, abstractOrder, roomStay);
				setSpecialRequestDetails(entryGroup, roomStay);
				setRoomPreferences(entryGroup, roomStay);
				setArrivalTime(entryGroup, roomStay);
				roomStays.add(roomStay);
			});
		}
		accommodationReservationData.setRoomStays(roomStays);

	}

	protected String getCityLocationCode(final AccommodationOfferingModel accommodationOffering)
	{
		final LocationModel location = bcfTravelLocationService
				.getLocationWithLocationType(Collections.singletonList(accommodationOffering.getLocation()),
						LocationType.CITY);

		final BcfVacationLocationModel bcfVacationLocation = bcfTravelLocationService
				.findBcfVacationLocationByOriginalLocation(location);

		if (Objects.isNull(bcfVacationLocation))
		{
			return StringUtils.EMPTY;
		}

		return bcfVacationLocation.getCode();
	}

	private String getAvailabilityStatus(final AccommodationOrderEntryGroupModel entryGroup)
	{
		return StreamUtil.safeStream(entryGroup.getEntries()).anyMatch(e -> !e.isStockAllocated()) ?
				AvailabilityStatus.ON_REQUEST.name() :
				AvailabilityStatus.AVAILABLE.name();
	}

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationEntries,
			final AccommodationReservationData reservationData, final boolean isDealEntry)
	{
		final Set<AccommodationOrderEntryGroupModel> entryGroups=new HashSet<>();
		if (isDealEntry)
		{
			entryGroups.addAll(((DealOrderEntryGroupModel) accommodationEntries.get(0).getEntryGroup())
					.getAccommodationEntryGroups());
		}
		else
		{
			entryGroups.addAll(accommodationEntries.stream()
					.map(entry -> (AccommodationOrderEntryGroupModel) entry.getEntryGroup()).collect(
							Collectors.toList()));
		}

		final List<ReservedRoomStayData> roomStays = new ArrayList<>();
		entryGroups.forEach(entryGroup -> {
			final ReservedRoomStayData roomStay = new ReservedRoomStayData();
			roomStay.setCheckInDate(entryGroup.getStartingDate());
			roomStay.setCheckOutDate(entryGroup.getEndingDate());
			roomStay.setRoomStayRefNumber(entryGroup.getRoomStayRefNumber());
			roomStay.setRatePlans(Collections.singletonList(getRatePlanConverter().convert(entryGroup.getRatePlan())));
			roomStay.setRoomTypes(Collections.singletonList(getRoomTypeConverter().convert(entryGroup.getAccommodation())));
			final boolean isModifiable = entryGroup.getEntries().stream()
					.allMatch(entry -> entry.getActive() && AmendStatus.NEW.equals(entry.getAmendStatus()));
			roomStay.setNonModifiable(!isModifiable);
			roomStay.setAvailabilityStatus(getAvailabilityStatus(entryGroup));
			roomStay.setAccommodationOfferingName(entryGroup.getAccommodationOffering().getName());
			roomStay.setLocationCode(getCityLocationCode(entryGroup.getAccommodationOffering()));
			roomStay.setAccommodationCode(entryGroup.getAccommodation().getCode());
			if (!CollectionUtils.isEmpty(entryGroup.getDealAccommodationEntries()))
			{
				roomStay.setPromotions(
						bcfTravelCartFacadeHelper.getPromotionMessages(abstractOrderModel, entryGroup.getDealAccommodationEntries()));
			}
			else
			{
				roomStay.setPromotions(bcfTravelCartFacadeHelper.getPromotionMessages(abstractOrderModel, entryGroup.getEntries()));
			}
			setServices(entryGroup, roomStay, abstractOrderModel);
			setGuestCounts(entryGroup, roomStay);
			setGuestData(entryGroup, roomStay);
			setTotalsPerRoom(entryGroup, abstractOrderModel, roomStay);
			setSpecialRequestDetails(entryGroup, roomStay);
			setRoomPreferences(entryGroup, roomStay);
			setArrivalTime(entryGroup, roomStay);
			roomStays.add(roomStay);
		});

		reservationData.setRoomStays(roomStays);
	}


	/**
	 * Returns the list of AbstractOrderEntryModels for the given entryGroup that are active and with quantityStatus
	 * different from DEAD.
	 *
	 * @param entryGroup as the entryGroup
	 * @return a list of AbstractOrderEntryModels
	 */
	@Override
	protected List<AbstractOrderEntryModel> getEntries(final AccommodationOrderEntryGroupModel entryGroup)
	{
		Collection<AbstractOrderEntryModel> orderEntries = entryGroup.getEntries();
		if (CollectionUtils.isEmpty(orderEntries) && entryGroup.getDealOrderEntryGroup() != null)
		{

			orderEntries = entryGroup.getDealOrderEntryGroup().getEntries();
		}

		return orderEntries.stream().filter(
				entry -> BooleanUtils.isTrue(entry.getActive()) && !Objects.equals(OrderEntryStatus.DEAD, entry.getQuantityStatus()))
				.collect(Collectors.toList());
	}

	protected BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	@Required
	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
