/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationItemHandler;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.ProductAvailabilityEnum;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;
import com.bcf.integration.listSailingResponse.data.ProductAvailability;


public class ReservationDataListItemHandler extends ReservationItemHandler implements ReservationDataListHandler
{
	private SessionService sessionService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();

			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			final List<ReservationItemData> reservationItems = new ArrayList<>();

			final List<AbstractOrderEntryModel> fareProductEntries = entries.stream()
					.filter(entry -> entry.getActive() && FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
					.collect(Collectors.toList());
			createReservationItems(fareProductEntries, reservationItems);
			final Map<Integer, List<TravellerModel>> travellersMap = ((BCFTravellerService) getTravellerService())
					.getTravellersPerJourneyPerLeg(abstractOrderModel, reservationData.getJourneyReferenceNumber());

			reservationItems.forEach(reservationItem -> {
				reservationItem
						.setReservationItinerary(createItinerary(fareProductEntries, reservationItem.getOriginDestinationRefNumber(),
								travellersMap.get(reservationItem.getOriginDestinationRefNumber())));
				reservationItem.setExternalBookingReference(
						getReferenceCode(abstractOrderModel, entries, reservationItem.getOriginDestinationRefNumber()));
				setProductAvailability(reservationItem);
			});


			reservationData.setReservationItems(reservationItems);
		}
	}

	private void setProductAvailability(ReservationItemData reservationItem) {
		final Map<String, List<ProductAvailability>> sessionAvailabilityMap = getSessionService()
				.getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP);

		final int minimumProductAvailability = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.MINIMUM_PRODUCT_AVAILABILITY));

		final List<ProductAvailability> productsAvailable = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(reservationItem.getReservationItinerary().getOriginDestinationOptions())){
			StreamUtil.safeStream(reservationItem.getReservationItinerary().getOriginDestinationOptions().
					get(0).getTransportOfferings()).forEach(transportOfferingData -> {
				if (MapUtils.isNotEmpty(sessionAvailabilityMap) && Objects.nonNull(sessionAvailabilityMap.get(transportOfferingData.getCode())))
				{
					productsAvailable.addAll(sessionAvailabilityMap
							.get(reservationItem.getReservationItinerary().getOriginDestinationOptions().get(0).getTransportOfferings().get(0)
									.getEbookingSailingCode())
							.stream().filter(
									productAvailability -> productAvailability.getProductCategory().equals(BcfFacadesConstants.AMENITY))
							.collect(Collectors.toList()));
				}
				if (productsAvailable.stream().allMatch(productAvailability -> productAvailability.getAvailableCapacity() == 0))
				{
					transportOfferingData.setAvailability(ProductAvailabilityEnum.SOLDOUT);
				}
				else if (productsAvailable.stream().allMatch(productAvailability -> productAvailability.getAvailableCapacity()
						< minimumProductAvailability))
				{
					transportOfferingData.setAvailability(ProductAvailabilityEnum.LIMITED);
				}
				else
				{
					transportOfferingData.setAvailability(ProductAvailabilityEnum.AVAILABLE);
				}
			});
		}
	}

	protected void populateOpenTicket(final ReservationItemData reservationItem,
			final AbstractOrderEntryModel fareProductEntry)
	{
		if (fareProductEntry != null && fareProductEntry.getActive() && FareProductModel._TYPECODE
				.equals(fareProductEntry.getProduct().getItemtype()))
		{
			if (fareProductEntry.getTravelOrderEntryInfo() != null && fareProductEntry.getTravelOrderEntryInfo().isOpenTicket())
			{
				reservationItem.setOpenTicketSelected(true);
			}
		}
	}

	@Override
	protected void createNewReservationItem(final List<ReservationItemData> reservationItems, final AbstractOrderEntryModel entry)
	{
		final ReservationItemData reservationItem = new ReservationItemData();
		reservationItem.setOriginDestinationRefNumber(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber());
		reservationItem.setCarryingDangerousGoods(entry.getTravelOrderEntryInfo().isCarryingDangerousGoods());
		reservationItem.setBcfBookingReference(entry.getBookingReference());
		populateOpenTicket(reservationItem, entry);

		reservationItems.add(reservationItem);
	}

	private String getReferenceCode(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> cartEntriesByJourneyRefNumber, final int odRef)
	{
		final Optional<AbstractOrderEntryModel> entryByOD = cartEntriesByJourneyRefNumber.stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()) && Objects
						.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRef).findFirst();
		if (entryByOD.isPresent() && StringUtils.isNotBlank(entryByOD.get().getBookingReference()))
		{
			final Optional<AmountToPayInfoModel> amountToPayInfo = abstractOrderModel.getAmountToPayInfos().stream()
					.filter(amountToPayInfoModel -> StringUtils
							.equals(amountToPayInfoModel.getBookingReference(), entryByOD.get().getBookingReference())).findFirst();
			if (amountToPayInfo.isPresent() && StringUtils.isNotBlank(amountToPayInfo.get().getReferenceCode()))
			{
				return amountToPayInfo.get().getReferenceCode();
			}
		}
		return null;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
