/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.DiscountData;
import de.hybris.platform.commercefacades.travel.FeeData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.util.DiscountValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.handlers.BcfGlobalReservationHandler;
import com.bcf.facades.vacation.fees.VacationFeesData;


public class BcfGlobalTotalFareReservationHandler implements BcfGlobalReservationHandler
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
	private BcfCalculationService bcfCalculationService;

	@Override
	public void handle(final AbstractOrderModel abstractOrder, final BcfGlobalReservationData globalReservationData)
	{
		final String currencyIsoCode = abstractOrder.getCurrency().getIsocode();
		final TotalFareData totalFare = new TotalFareData();
		final double totalTaxes = abstractOrder.getTotalTax();
		totalFare.setTaxPrice(getTravelCommercePriceFacade().createPriceData(totalTaxes, currencyIsoCode));
		final double totalPrice;
		final TotalFareData previouslyPaidData = new TotalFareData();
		populateCommonAttributes(abstractOrder, globalReservationData);

		if (abstractOrder.getBookingJourneyType().equals(BookingJourneyType.BOOKING_TRANSPORT_ONLY))
		{
			totalPrice = abstractOrder.getTotalPrice();
			globalReservationData
					.setAmountToPay(getTravelCommercePriceFacade().createPriceData(abstractOrder.getAmountToPay(),
							abstractOrder.getCurrency().getIsocode()));
			globalReservationData
					.setPayAtTerminal(getTravelCommercePriceFacade().createPriceData(abstractOrder.getPayAtTerminal(),
							abstractOrder.getCurrency().getIsocode()));
			final double previouslyPaid = Objects.nonNull(abstractOrder.getAmountPaid()) ? abstractOrder.getAmountPaid() : 0d;
			previouslyPaidData.setTotalPrice(getTravelCommercePriceFacade().createPriceData(previouslyPaid, currencyIsoCode));
			globalReservationData.setPreviouslyPaid(previouslyPaidData);
		}
		else
		{
			totalPrice = bcfCalculationService.getTotalPriceWithOutGoodWillRefund(abstractOrder);
			double depositPaid = 0.0d;
			if (CollectionUtils.isNotEmpty(abstractOrder.getPaymentTransactions()))
			{
				depositPaid = abstractOrder.getPaymentTransactions().stream()
						.flatMap(paymentTransaction -> paymentTransaction.getEntries().stream())
						.filter(paymentTransactionEntry -> paymentTransactionEntry.isDepositPayment()).
								map(paymentTransactionEntry -> paymentTransactionEntry.getAmount())
						.reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();
				if (depositPaid > 0.0d)
				{
					globalReservationData.setDepositAmount(getTravelCommercePriceFacade().createPriceData(depositPaid));
				}
			}
			double previouslyPaid = Objects.nonNull(abstractOrder.getAmountPaid()) ? abstractOrder.getAmountPaid() : 0d;
			previouslyPaid = previouslyPaid - depositPaid;
			previouslyPaidData.setTotalPrice(getTravelCommercePriceFacade().createPriceData(previouslyPaid, currencyIsoCode));
			globalReservationData.setPreviouslyPaid(previouslyPaidData);
			if (Objects.nonNull(abstractOrder.getAmountToPay()))
			{
				globalReservationData
						.setAmountToPay(getTravelCommercePriceFacade().createPriceData(abstractOrder.getAmountToPay(),
								abstractOrder.getCurrency().getIsocode()));

				if (abstractOrder.getAmountToPay() < 0)
				{
					globalReservationData
							.setRefundAmount(getTravelCommercePriceFacade().createPriceData(-abstractOrder.getAmountToPay(),
									abstractOrder.getCurrency().getIsocode()));
				}
			}
			final List<AbstractOrderEntryModel> changeFeeEntries = bcfChangeFeeCalculationService
					.getExistingChangeAndCancelFeeEntries(abstractOrder);
			if (CollectionUtils.isNotEmpty(changeFeeEntries))
			{
				final List<VacationFeesData> vacationFeesDatas = new ArrayList<>();
				for (final AbstractOrderEntryModel changeFeeEntry : changeFeeEntries)
				{

					final VacationFeesData vacationFeesData = new VacationFeesData();
					vacationFeesData.setFeeType(changeFeeEntry.getProduct().getName());
					vacationFeesData.setVacationFee(getTravelCommercePriceFacade().createPriceData(changeFeeEntry.getTotalPrice(),
							abstractOrder.getCurrency().getIsocode()));
					vacationFeesDatas.add(vacationFeesData);

				}
				globalReservationData.setVacationFees(vacationFeesDatas);
			}
		}

		totalFare.setTotalPrice(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIsoCode));
		final Collection<GoodWillRefundOrderInfoModel> refundInfos = abstractOrder.getGoodwillRefundOrderInfos();
		if (CollectionUtils.isNotEmpty(refundInfos))
		{
			final Double refundAmount = refundInfos.stream().mapToDouble(refundInfo -> refundInfo.getAmount()).sum();
			totalFare.setGoodWillRefund(getTravelCommercePriceFacade().createPriceData(refundAmount, currencyIsoCode));
		}

		final double totalPriceExcludingTaxes = totalPrice - totalTaxes;
		totalFare.setSubTotalPrice(getTravelCommercePriceFacade().createPriceData(totalPriceExcludingTaxes, currencyIsoCode));
		populateDiscounts(abstractOrder, totalFare);
		populateFees(abstractOrder, totalFare);
		globalReservationData.setTotalFare(totalFare);
	}

	private void populateCommonAttributes(final AbstractOrderModel abstractOrder,
			final BcfGlobalReservationData globalReservationData)
	{
		if (getBcfTravelCartFacade().isAmendmentCart())
		{
			globalReservationData.setAmendBooking(true);
		}
		if (abstractOrder.getStatus() != null)
		{
			globalReservationData.setBookingStatusName(abstractOrder.getStatus().name());
		}
		if (OptionBookingUtil.isOptionBooking(abstractOrder))
		{
			globalReservationData.setOptionBooking(((CartModel) abstractOrder).isVacationOptionBooking());
		}
		globalReservationData.setCode(abstractOrder.getCode());
		globalReservationData.setBookingJourneyType(
				abstractOrder.getBookingJourneyType() != null ? abstractOrder.getBookingJourneyType().getCode() : null);
	}

	private void populateDiscounts(final AbstractOrderModel abstractOrderModel, final TotalFareData totalFare)
	{
		final String currencyIsocode = abstractOrderModel.getCurrency().getIsocode();
		final List<DiscountData> totalDiscounts = new ArrayList<>();

		//order level discounts
		for (final DiscountValue discount : abstractOrderModel.getGlobalDiscountValues())
		{
			final DiscountData discountData = new DiscountData();
			discountData.setPrice(getTravelCommercePriceFacade().createPriceData(discount.getAppliedValue(), currencyIsocode));
			totalDiscounts.add(discountData);
		}

		//entry level discounts
		final Map<AbstractOrderEntryModel, List<DiscountValue>> entriesDiscounts = abstractOrderModel.getEntries().stream()
				.collect(Collectors.toMap(entry -> entry, entry -> entry.getDiscountValues()));

		for (final Map.Entry<AbstractOrderEntryModel, List<DiscountValue>> discountMapEntry : entriesDiscounts.entrySet())
		{
			final DiscountData discountData = new DiscountData();
			final double entryDiscountSingleItem = discountMapEntry.getValue().stream().mapToDouble(discount -> discount.getValue())
					.sum();
			final Long entryQuantity = discountMapEntry.getKey().getQuantity();
			discountData.setPrice(getTravelCommercePriceFacade()
					.createPriceData(entryDiscountSingleItem * entryQuantity,
							currencyIsocode));
			totalDiscounts.add(discountData);
		}

		totalFare.setDiscounts(totalDiscounts);

		final Double totalDiscount = totalDiscounts.stream().map(d -> d.getPrice()).mapToDouble(e -> e.getValue().doubleValue())
				.sum();
		totalFare.setDiscountPrice(
				getTravelCommercePriceFacade().createPriceData(totalDiscount, currencyIsocode));
	}

	protected void populateFees(final AbstractOrderModel abstractOrderModel, final TotalFareData totalFare)
	{
		final List<FeeData> totalFeeList = new ArrayList<>();
		double totalFees = 0.0D;
		final List<AbstractOrderEntryModel> feeEntries = abstractOrderModel.getEntries().stream()
				.filter(e -> e.getProduct() instanceof FeeProductModel
						|| Objects.equals(FeeProductModel._TYPECODE, e.getProduct().getItemtype()))
				.collect(Collectors.toList());
		final Iterator fee = feeEntries.iterator();

		while (fee.hasNext())
		{
			final AbstractOrderEntryModel entryModel = (AbstractOrderEntryModel) fee.next();
			totalFees += entryModel.getTotalPrice();
			totalFeeList.add(createFeeData(entryModel));
		}

		totalFare.setTotalFees(
				getTravelCommercePriceFacade().createPriceData(totalFees, abstractOrderModel.getCurrency().getIsocode()));
		totalFare.setFees(totalFeeList);
	}

	protected FeeData createFeeData(final AbstractOrderEntryModel entryModel)
	{
		final double feeValue = entryModel.getTotalPrice();
		final FeeData feeData = new FeeData();
		feeData.setName(entryModel.getProduct().getName());
		feeData.setPrice(
				getTravelCommercePriceFacade().createPriceData(feeValue, entryModel.getOrder().getCurrency().getIsocode()));
		feeData.setAmendable(entryModel.getActive() && entryModel.getAmendStatus().equals(AmendStatus.NEW));
		if (feeData.isAmendable())
		{
			feeData.setCacheKey(entryModel.getCacheKey());
		}
		return feeData;
	}
	
	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(
			final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}
}
