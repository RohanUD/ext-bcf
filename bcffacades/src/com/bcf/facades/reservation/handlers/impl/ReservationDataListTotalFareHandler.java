/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.DiscountData;
import de.hybris.platform.commercefacades.travel.FeeData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationTotalFareHandler;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.util.DiscountValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListTotalFareHandler extends ReservationTotalFareHandler implements ReservationDataListHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();

			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			double totalPrice = 0d;

			final List<AbstractOrderEntryModel> transportEntries = entries.stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())).collect(Collectors.toList());

			for (final AbstractOrderEntryModel entry : transportEntries)
			{
				totalPrice += entry.getTotalPrice();
			}

			final String currencyIsocode = abstractOrderModel.getCurrency().getIsocode();
			final TotalFareData totalFare = new TotalFareData();
			totalFare.setBasePrice(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIsocode));

			final List<TaxData> taxes = createTaxData(
					entries.stream().filter(entry -> Objects.equals(OrderEntryType.TRANSPORT, entry.getType()))
							.flatMap(entry -> entry.getTaxValues().stream()).collect(Collectors.toList()));
			totalFare.setTaxes(taxes);

			populateDiscounts(abstractOrderModel, entries, totalFare);
			populateGlobalExtras(abstractOrderModel, entries, totalFare);

			if (abstractOrderModel.getNet())
			{
				totalPrice += taxes.stream().mapToDouble(tax -> tax.getPrice().getValue().doubleValue()).sum();
			}
			totalFare.setTotalPrice(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIsocode));

			reservationData.setTotalFare(totalFare);

			if (abstractOrderModel.getOriginalOrder() != null)
			{
				reservationData.setTotalToPay(calculateTotalToPay(abstractOrderModel));
			}
			else
			{
				reservationData.setTotalToPay(totalFare.getTotalPrice());
			}
		}
		populateTotalFare(reservationDataList, abstractOrderModel);

	}

	@Override
	protected void populateFees(final AbstractOrderModel abstractOrderModel, final TotalFareData totalFare)
	{
		final List<FeeData> totalFeeList = new ArrayList();
		double totalFees = 0.0D;
		final List<AbstractOrderEntryModel> feeEntries = abstractOrderModel.getEntries().stream().filter(e ->
				e.getProduct() instanceof FeeProductModel || Objects.equals(FeeProductModel._TYPECODE, e.getProduct().getItemtype())
		).collect(Collectors.toList());
		final Iterator fee = feeEntries.iterator();

		while (fee.hasNext())
		{
			final AbstractOrderEntryModel entryModel = (AbstractOrderEntryModel) fee.next();
			totalFees += entryModel.getTotalPrice();
			totalFeeList.add(this.createFeeData(entryModel));
		}

		totalFare.setTotalFees(
				this.getTravelCommercePriceFacade().createPriceData(totalFees, abstractOrderModel.getCurrency().getIsocode()));
		totalFare.setFees(totalFeeList);
	}

	@Override
	protected FeeData createFeeData(final AbstractOrderEntryModel entryModel)
	{
		final double feeValue = entryModel.getTotalPrice();
		final FeeData feeData = new FeeData();
		feeData.setName(entryModel.getProduct().getName());
		feeData.setPrice(
				this.getTravelCommercePriceFacade().createPriceData(feeValue, entryModel.getOrder().getCurrency().getIsocode()));
		feeData.setAmendable(entryModel.getActive() && entryModel.getAmendStatus().equals(AmendStatus.NEW));
		if (feeData.isAmendable())
		{
			feeData.setCacheKey(entryModel.getCacheKey());
		}
		return feeData;
	}

	private void populateTotalFare(final ReservationDataList reservationDataList, final AbstractOrderModel abstractOrderModel)
	{
		final TotalFareData totalFare = new TotalFareData();

		final List<TotalFareData> reservationDataTotalFares = reservationDataList.getReservationDatas().stream()
				.map(ReservationData::getTotalFare).collect(Collectors.toList());
		final Double totalBasePrice = reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getBasePrice()))
				.mapToDouble(totalFareData -> totalFareData.getBasePrice().getValue().doubleValue()).sum();
		final Double totalDiscountPrice = reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getDiscountPrice()))
				.mapToDouble(totalFareData -> totalFareData.getDiscountPrice().getValue().doubleValue()).sum();
		final Double totalExtrasPrice = reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getExtrasPrice()))
				.mapToDouble(totalFareData -> totalFareData.getExtrasPrice().getValue().doubleValue()).sum();
		final Double totalTaxPrice = reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getTaxPrice()))
				.mapToDouble(totalFareData -> totalFareData.getTaxPrice().getValue().doubleValue()).sum();

		reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getTotalFees()))
				.mapToDouble(totalFareData -> totalFareData.getTotalFees().getValue().doubleValue()).sum();

		totalFare.setBasePrice(
				getTravelCommercePriceFacade().createPriceData(totalBasePrice, abstractOrderModel.getCurrency().getIsocode()));
		totalFare.setDiscountPrice(
				getTravelCommercePriceFacade().createPriceData(totalDiscountPrice, abstractOrderModel.getCurrency().getIsocode()));
		totalFare.setExtrasPrice(
				getTravelCommercePriceFacade().createPriceData(totalExtrasPrice, abstractOrderModel.getCurrency().getIsocode()));
		totalFare.setTaxPrice(
				getTravelCommercePriceFacade().createPriceData(totalTaxPrice, abstractOrderModel.getCurrency().getIsocode()));

		populateFees(abstractOrderModel, totalFare);
		final Double totalPrice = reservationDataTotalFares.stream()
				.filter(totalFareData -> Objects.nonNull(totalFareData.getTotalPrice()))
				.mapToDouble(totalFareData -> totalFareData.getTotalPrice().getValue().doubleValue()).sum();
		
		totalFare.setTotalPrice(
				getTravelCommercePriceFacade().createPriceData(totalPrice, abstractOrderModel.getCurrency().getIsocode()));


		final List<DiscountData> totalDiscounts = reservationDataTotalFares.stream()
				.filter(totalFareData -> CollectionUtils.isNotEmpty(totalFareData.getDiscounts()))
				.flatMap(totalFareData -> totalFareData.getDiscounts().stream()).collect(Collectors.toList());
		totalFare.setDiscounts(totalDiscounts);
		final List<TaxData> totaTaxes = reservationDataTotalFares.stream()
				.filter(totalFareData -> CollectionUtils.isNotEmpty(totalFareData.getTaxes()))
				.flatMap(totalFareData -> totalFareData.getTaxes().stream()).collect(Collectors.toList());
		totalFare.setTaxes(totaTaxes);


		reservationDataList.setTotalFare(totalFare);
		if (Objects.nonNull(abstractOrderModel.getAmountToPay()))
		{
			reservationDataList.setAmountToPay(getTravelCommercePriceFacade().createPriceData(abstractOrderModel.getAmountToPay(),
					abstractOrderModel.getCurrency().getIsocode()));
		}
		if (Objects.nonNull(abstractOrderModel.getPayAtTerminal()))
		{
			reservationDataList
					.setPayAtTerminal(getTravelCommercePriceFacade().createPriceData(abstractOrderModel.getPayAtTerminal(),
							abstractOrderModel.getCurrency().getIsocode()));
		}
	}

	private void populateDiscounts(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> entries,
			final TotalFareData totalFare)
	{
		final String currencyIsocode = abstractOrderModel.getCurrency().getIsocode();
		final List<DiscountData> totalDiscounts = new ArrayList<>();
		for (final DiscountValue discount : abstractOrderModel.getGlobalDiscountValues())
		{
			final DiscountData discountData = new DiscountData();
			discountData.setPrice(getTravelCommercePriceFacade().createPriceData(discount.getAppliedValue(), currencyIsocode));
			totalDiscounts.add(discountData);
		}

		// Get the discounts only for fare products and non-included ancillaries. Not for fees and included ancillaries
		entries.stream().filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entryModel -> FareProductModel._TYPECODE.equals(entryModel.getProduct().getItemtype())
						|| entryModel.getBundleNo() == 0)
				.forEach(entryModel -> {
					final List<DiscountData> discounts = createDiscountData(entryModel.getDiscountValues());
					totalDiscounts.addAll(discounts);
				});

		totalFare.setDiscounts(totalDiscounts);
		totalFare.setDiscountPrice(
				getTravelCommercePriceFacade().createPriceData(abstractOrderModel.getTotalDiscounts(), currencyIsocode));
	}

	private void populateGlobalExtras(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> abstractOrderEntries, final TotalFareData totalFare)
	{
		final List<AbstractOrderEntryModel> entries = abstractOrderEntries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entry -> !FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype())
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == null
						&& entry.getTravelOrderEntryInfo().getTravelRoute() == null)
				.collect(Collectors.toList());

		BigDecimal totalExtrasPrice = BigDecimal.valueOf(0);

		for (final AbstractOrderEntryModel entry : entries)
		{
			if (entry.getBundleNo() == 0)
			{
				final double extrasPriceValue;
				if (entry.getQuantity() == 0)
				{
					extrasPriceValue = (CollectionUtils.isEmpty(entry.getDiscountValues())) ? entry.getTotalPrice()
							: entry.getBasePrice();
				}
				else
				{
					extrasPriceValue = entry.getBasePrice() * entry.getQuantity();
				}
				totalExtrasPrice = totalExtrasPrice.add(BigDecimal.valueOf(extrasPriceValue));
			}
		}
		final PriceData extrasPriceData = getTravelCommercePriceFacade().createPriceData(totalExtrasPrice.doubleValue(),
				abstractOrderModel.getCurrency().getIsocode());
		totalFare.setExtrasPrice(extrasPriceData);
	}

}
