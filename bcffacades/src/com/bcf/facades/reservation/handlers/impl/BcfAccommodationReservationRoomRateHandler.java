/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.AccommodationReservationRoomRateHandler;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryInfoModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.reservation.handlers.BcfAccommodationReservationHandler;


public class BcfAccommodationReservationRoomRateHandler extends AccommodationReservationRoomRateHandler implements
		BcfAccommodationReservationHandler
{
	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationEntries,
			final AccommodationReservationData reservationData, final boolean isDealEntry)
	{
		Collection<AccommodationOrderEntryGroupModel> entryGroups;
		if(isDealEntry)
		{
			entryGroups = ((DealOrderEntryGroupModel) accommodationEntries.get(0).getEntryGroup()).getAccommodationEntryGroups();
		}
		else
		{
			entryGroups = accommodationEntries.stream().map(entry -> (AccommodationOrderEntryGroupModel) entry.getEntryGroup()).collect(
					Collectors.toList());
		}
		final String currencyIso = abstractOrderModel.getCurrency().getIsocode();

		for (final ReservedRoomStayData roomStay : reservationData.getRoomStays())
		{
			final List<RoomRateData> roomRates = new ArrayList<>();
			final List<AccommodationOrderEntryGroupModel> entryList = entryGroups.stream().collect(Collectors.toList());
			for (final AbstractOrderEntryModel entry : getEntries(entryList, abstractOrderModel, roomStay))
			{
				if (entry.getProduct() instanceof RoomRateProductModel)
				{
					final AccommodationOrderEntryInfoModel entryInfo = entry.getAccommodationOrderEntryInfo();
					entryInfo.getDates().forEach(date -> roomRates.add(createRoomRate(entry, date, currencyIso)));
				}
			}

			final Comparator<RoomRateData> byStartDate = (left,
					right) -> (left.getStayDateRange().getStartTime().before(right.getStayDateRange().getStartTime())) ? -1 : 1;

			roomRates.sort(byStartDate);
			roomStay.getRatePlans().get(0).setRoomRates(roomRates);
		}
	}

	/**
	 * Creates room rate
	 *
	 * @param entry, abstract order entry
	 *
	 * @param date, the start date for the stay
	 *
	 * @param currencyIso, currency from an order
	 *
	 * @return created RoomRateData
	 */
	protected RoomRateData createRoomRate(final AbstractOrderEntryModel entry, final Date date, final String currencyIso)
	{
		final RoomRateData roomRate = new RoomRateData();
		roomRate.setCode(entry.getProduct().getCode());
		final StayDateRangeData stayDateRange = new StayDateRangeData();
		stayDateRange.setStartTime(date);
		roomRate.setStayDateRange(stayDateRange);
		final RateData rate = new RateData();
		final Double basePrice = entry.getBasePrice();
		rate.setBasePrice(
				getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, BigDecimal.valueOf(basePrice), currencyIso));
		final List<TaxData> taxes = entry.getTaxValues().stream()
				.map(taxValue -> createTaxDataFromValue(taxValue.getAppliedValue() / entry.getQuantity(), currencyIso))
				.collect(Collectors.toList());
		rate.setTaxes(taxes);
		final Double totalTax = taxes.stream().mapToDouble(taxData -> taxData.getPrice().getValue().doubleValue()).sum();
		rate.setTotalTax(createTaxDataFromValue(totalTax, currencyIso));
		final Double totalPrice = Double.sum(entry.getTotalPrice() / entry.getQuantity(), totalTax);
		rate.setActualRate(
				getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY, BigDecimal.valueOf(totalPrice), currencyIso));
		rate.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(PriceDataType.BUY,
				BigDecimal.valueOf(basePrice + totalTax - totalPrice), currencyIso));
		roomRate.setRate(rate);
		return roomRate;
	}
}
