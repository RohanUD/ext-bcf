/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.DiscountData;
import de.hybris.platform.commercefacades.travel.FeeData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationPricingInfoData;
import de.hybris.platform.commercefacades.travel.seatmap.data.SegmentInfoData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationPricingInfoHandler;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.util.TaxValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.FareDetailModel;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListPricingInfoHandler extends ReservationPricingInfoHandler implements ReservationDataListHandler
{
	private static final String PORT_AUTHORITY_FEE_CODE = "NFA";
	private static final String TEXT_TAX_PST = "text.payment.reservation.transport.fare.breakup.tax.pst";
	private static final String TEXT_TAX_GST = "text.payment.reservation.transport.fare.breakup.tax.gst";
	private static final String TEXT_FEE_OTHER_CHARGES = "text.payment.reservation.transport.fare.breakup.fee.other.charges";
	private static final String TEXT_FEE_PORT_AUTHORITY = "text.payment.reservation.transport.fare.breakup.fee.port.authority";
	private static final String TEXT_DISCOUNT_FUEL_REBATE = "text.payment.reservation.transport.fare.breakup.discount.fuel.rebate";
	private static final String FUEL_REBATE_PRODUCTS = "fuel.rebate.product.codes";

	private List<String> bookingFeeCodes;
	private ConfigurationService configurationService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();

			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			final List<ReservationItemData> reservationItems = reservationData.getReservationItems();

			if (CollectionUtils.isNotEmpty(reservationItems))
			{
				final List<SegmentInfoData> segmentInfoDatas = getSegmentInfoData(entries);
				reservationData.getReservationItems().forEach(reservationItem -> {
					createReservationPricingInfo(abstractOrderModel, entries, reservationItem);
					reservationItem.getReservationPricingInfo().setSegmentInfoDatas(segmentInfoDatas);
				});
			}

		}
	}

	private List<SegmentInfoData> getSegmentInfoData(final List<AbstractOrderEntryModel> entries)
	{
		final List<SegmentInfoData> segmentInfoDatas = new ArrayList<>();
		final List<AbstractOrderEntryModel> fareProductEntries = entries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()) && entry.getActive())
				.collect(Collectors.toList());

		fareProductEntries.forEach(
				fareProductEntry -> fareProductEntry.getTravelOrderEntryInfo().getTransportOfferings().forEach(transportOffering -> {
					final SegmentInfoData segmentInfoData = new SegmentInfoData();
					segmentInfoData.setTransportOfferingCode(transportOffering.getCode());
					segmentInfoData.setFareBasisCode(fareProductEntry.getProduct().getCode());
					segmentInfoDatas.add(segmentInfoData);
				}));
		return segmentInfoDatas;
	}

	/**
	 * Creates a new Reservation Pricing Info for given Reservation Item
	 *
	 * @param abstractOrderModel - given abstract order
	 * @param reservationItem    the reservation item
	 */
	protected void createReservationPricingInfo(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> entries, final ReservationItemData reservationItem)
	{
		final ReservationPricingInfoData reservationPricingInfo = new ReservationPricingInfoData();
		reservationPricingInfo
				.setTotalFare(calculateSubtotalForLeg(abstractOrderModel, entries, reservationItem.getOriginDestinationRefNumber()));
		reservationItem.setReservationPricingInfo(reservationPricingInfo);
	}

	/**
	 * Sums up all prices of Abstract Order Entries which are associated with given leg
	 *
	 * @param abstractOrderModel         - given abstract order
	 * @param originDestinationRefNumber - leg indicator
	 * @return total fare data
	 */
	protected TotalFareData calculateSubtotalForLeg(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> entries, final int originDestinationRefNumber)
	{
		final List<AbstractOrderEntryModel> fareProductEntries = entries.stream()
				.filter(e -> OrderEntryType.TRANSPORT.equals(e.getType()))
				.filter(e -> e.getActive() && e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
						&& e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationRefNumber)
				.collect(Collectors.toList());

		BigDecimal basePrice = BigDecimal.valueOf(0);
		final BigDecimal totalTaxPrice = BigDecimal.valueOf(0);
		final BigDecimal totalDiscountPrice = BigDecimal.valueOf(0);
		BigDecimal totalExtrasPrice = BigDecimal.valueOf(0);

		final String currencyIsoCode = abstractOrderModel.getCurrency().getIsocode();

		final List<TaxData> totalTaxes = new ArrayList<>();
		final List<DiscountData> totalDiscounts = new ArrayList<>();
		for (final AbstractOrderEntryModel entry : fareProductEntries)
		{
			basePrice = getBasePriceFareProducts(entry, basePrice);
			// Get the prices for included ancillaries
			if (AncillaryProductModel._TYPECODE.equals(entry.getProduct().getItemtype()) && entry.getBundleNo() > 0)
			{
				final double extrasForPassengers = entry.getQuantity() == 0 ? entry.getTotalPrice()
						: entry.getTotalPrice() * entry.getQuantity();
				totalExtrasPrice = totalExtrasPrice.add(BigDecimal.valueOf(extrasForPassengers));
			}
			// Get the prices only for non-included ancillaries. Not for fees, fare products and included ancillaries
			if (entry.getBundleNo() == 0)
			{
				final double extrasForPassengers = getPriceForAncillaryEntry(entry);
				totalExtrasPrice = totalExtrasPrice.add(BigDecimal.valueOf(extrasForPassengers));
			}

			getTotalTaxes(totalTaxes, entry);

			// Get the discounts only for fare products and non-included ancillaries. Not for fees and included ancillaries
			getTotalDiscounts(currencyIsoCode, totalDiscounts, entry);
		}
		BigDecimal totalPrice = basePrice.add(totalExtrasPrice).subtract(totalDiscountPrice);
		if (abstractOrderModel.getNet())
		{
			totalPrice = totalPrice.add(totalTaxPrice);
		}
		final TotalFareData totalFare = new TotalFareData();
		totalFare.setTaxes(createTaxes(fareProductEntries, currencyIsoCode));
		totalFare.setTaxPrice(getTravelCommercePriceFacade()
				.createPriceData(totalFare.getTaxes().stream().map(TaxData::getPrice).map(PriceData::getValue)
						.mapToDouble(BigDecimal::doubleValue).sum(), currencyIsoCode));
		totalFare.setDiscounts(createDiscounts(fareProductEntries, currencyIsoCode));
		totalFare.setDiscountPrice(getTravelCommercePriceFacade()
				.createPriceData(totalFare.getDiscounts().stream().map(DiscountData::getPrice).map(PriceData::getValue)
						.mapToDouble(BigDecimal::doubleValue).sum(), currencyIsoCode));
		totalFare.setFees(createFees(fareProductEntries, currencyIsoCode));
		totalFare.setTotalFees(getTravelCommercePriceFacade()
				.createPriceData(totalFare.getFees().stream().map(FeeData::getPrice).map(PriceData::getValue)
						.mapToDouble(BigDecimal::doubleValue).sum(), currencyIsoCode));
		totalFare.setBasePrice(getTravelCommercePriceFacade().createPriceData(basePrice.doubleValue(), currencyIsoCode));
		totalFare.setExtrasPrice(getTravelCommercePriceFacade().createPriceData(totalExtrasPrice.doubleValue(), currencyIsoCode));
		totalFare.setTotalBaseExtrasPrice(getTravelCommercePriceFacade()
				.createPriceData(basePrice.doubleValue() + totalExtrasPrice.doubleValue(), currencyIsoCode));
		totalFare.setTotalPrice(getTravelCommercePriceFacade().createPriceData(totalPrice.doubleValue(), currencyIsoCode));
		return totalFare;
	}

	private void getTotalDiscounts(final String currencyIsoCode, final List<DiscountData> totalDiscounts,
			final AbstractOrderEntryModel entry)
	{
		if (FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()) || entry.getBundleNo() == 0)
		{
			final List<DiscountData> discountDataList = new ArrayList<>();
			final Collection<FareDetailModel> fareDetails = entry.getFareDetailList();
			for (final FareDetailModel fare : fareDetails)
			{
				if (fare.getAmount() < 0d)
				{
					final DiscountData previousDiscountData = totalDiscounts.stream()
							.filter(discountData -> StringUtils.equals(discountData.getPurpose(), fare.getDescription())).findFirst()
							.orElse(null);
					if (Objects.nonNull(previousDiscountData))
					{
						previousDiscountData.setPrice(getTravelCommercePriceFacade()
								.createPriceData(fare.getAmount() + previousDiscountData.getPrice().getValue().doubleValue(),
										currencyIsoCode));
					}
					else
					{
						final DiscountData discountData = new DiscountData();
						discountData.setPurpose(fare.getDescription());
						discountData.setPrice(
								getTravelCommercePriceFacade().createPriceData(fare.getAmount(), currencyIsoCode));
						discountDataList.add(discountData);
					}
				}
			}
			totalDiscounts.addAll(discountDataList);
		}
	}

	private void getTotalTaxes(final List<TaxData> totalTaxes, final AbstractOrderEntryModel entry)
	{
		final Collection<TaxValue> taxValues = entry.getTaxValues();
		for (final TaxValue taxValue : taxValues)
		{
			if (taxValue.getValue() > 0d)
			{
				final TaxData previousTax = totalTaxes.stream()
						.filter(taxData -> StringUtils.equals(taxData.getCode(), taxValue.getCode())).findFirst().orElse(null);
				if (Objects.nonNull(previousTax))
				{
					previousTax.setPrice(getTravelCommercePriceFacade()
							.createPriceData(taxValue.getValue() + previousTax.getPrice().getValue().doubleValue(),
									taxValue.getCurrencyIsoCode()));
				}
				else
				{
					final TaxData taxData = new TaxData();
					taxData.setCode(taxValue.getCode());
					taxData.setPrice(
							getTravelCommercePriceFacade().createPriceData(taxValue.getValue(), taxValue.getCurrencyIsoCode()));
					totalTaxes.add(taxData);
				}
			}
		}
	}

	/**
	 * Get the base prices only for fare products. Not for fees and ancillaries
	 */
	private BigDecimal getBasePriceFareProducts(final AbstractOrderEntryModel entry, final BigDecimal basePrice)
	{
		if (FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
		{
			final double basePriceForPassengers = (entry.getQuantity() == 0) ? entry.getBasePrice()
					: entry.getBasePrice() * entry.getQuantity();
			return basePrice.add(BigDecimal.valueOf(basePriceForPassengers));
		}
		return basePrice;
	}

	private List<FeeData> createFees(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<FeeData> feeDataList = new ArrayList<>();
		feeDataList.add(createPortAuthorityFeesTotal(entries, currencyIsoCode));
		feeDataList.add(createOtherFeeChargesTotal(entries, currencyIsoCode));
		return feeDataList;
	}


	private FeeData createPortAuthorityFeesTotal(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<Double> portAuthorityFeeValues = new ArrayList<>();
		entries.stream().forEach(entry -> {
			entry.getFareDetailList().stream().forEach(fareDetailModel -> {
				if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(PORT_AUTHORITY_FEE_CODE, fareDetailModel.getCode()))
				{
					portAuthorityFeeValues.add(fareDetailModel.getAmount());
				}
			});
		});

		final FeeData portAuthorityFee = new FeeData();
		portAuthorityFee.setPrice(getTravelCommercePriceFacade()
				.createPriceData(portAuthorityFeeValues.stream().mapToDouble(Double::doubleValue).sum() / 100d, currencyIsoCode));
		portAuthorityFee.setName(TEXT_FEE_PORT_AUTHORITY);
		return portAuthorityFee;
	}

	private FeeData createOtherFeeChargesTotal(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<Double> otherChargesValues = new ArrayList<>();
		entries.stream().forEach(entry -> {
			if (getBookingFeeCodes().contains(entry.getProduct().getCode()))
			{
				otherChargesValues.add(entry.getTotalPrice());
			}
		});
		final FeeData otherCharges = new FeeData();
		otherCharges.setPrice(getTravelCommercePriceFacade()
				.createPriceData(otherChargesValues.stream().mapToDouble(Double::doubleValue).sum(), currencyIsoCode));
		otherCharges.setName(TEXT_FEE_OTHER_CHARGES);
		return otherCharges;
	}

	private List<DiscountData> createDiscounts(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<DiscountData> discountsList = new ArrayList<>();
		discountsList.add(createFuelRebateTotal(entries, currencyIsoCode));
		return discountsList;
	}

	private DiscountData createFuelRebateTotal(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<String> discountProductCodes = BCFPropertiesUtils
				.convertToList(getConfigurationService().getConfiguration().getString(FUEL_REBATE_PRODUCTS));
		final List<Double> discountValues = new ArrayList<>();
		entries.stream().forEach(entry -> {
			entry.getFareDetailList().stream().forEach(fareDetailModel -> {
				if (discountProductCodes.contains(fareDetailModel.getCode()))
				{
					discountValues.add(fareDetailModel.getAmount());
				}
			});
		});
		final DiscountData discountTotal = new DiscountData();
		discountTotal.setPrice(getTravelCommercePriceFacade()
				.createPriceData(discountValues.stream().mapToDouble(Double::doubleValue).sum() / 100d, currencyIsoCode));
		discountTotal.setPurpose(TEXT_DISCOUNT_FUEL_REBATE);
		return discountTotal;
	}

	private List<TaxData> createTaxes(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<TaxData> taxDataList = new ArrayList<>();
		taxDataList.add(createPstTotal(entries, currencyIsoCode));
		taxDataList.add(createGstTotal(entries, currencyIsoCode));
		return taxDataList;
	}

	private TaxData createPstTotal(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<Double> pstTaxValues = new ArrayList<>();
		entries.stream().forEach(entry -> {
			entry.getTaxValues().stream().forEach(taxValue -> {
				if (org.apache.commons.lang.StringUtils.equals(BcfCoreConstants.PST, taxValue.getCode()))
				{
					pstTaxValues.add(taxValue.getValue());
				}
			});
		});
		final TaxData totalPST = new TaxData();
		totalPST.setPrice(getTravelCommercePriceFacade()
				.createPriceData(pstTaxValues.stream().mapToDouble(Double::doubleValue).sum() / 100d, currencyIsoCode));
		totalPST.setCode(TEXT_TAX_PST);
		return totalPST;
	}

	private TaxData createGstTotal(final List<AbstractOrderEntryModel> entries, final String currencyIsoCode)
	{
		final List<Double> gstTaxValues = new ArrayList<>();
		entries.stream().forEach(entry -> {
			entry.getTaxValues().stream().forEach(taxValue -> {
				if (org.apache.commons.lang.StringUtils.equals(BcfCoreConstants.GST, taxValue.getCode()))
				{
					gstTaxValues.add(taxValue.getValue());
				}
			});
		});
		final TaxData totalGST = new TaxData();
		totalGST.setPrice(getTravelCommercePriceFacade()
				.createPriceData(gstTaxValues.stream().mapToDouble(Double::doubleValue).sum() / 100d, currencyIsoCode));
		totalGST.setCode(TEXT_TAX_GST);
		return totalGST;
	}

	protected List<String> getBookingFeeCodes()
	{
		return bookingFeeCodes;
	}

	@Required
	public void setBookingFeeCodes(final List<String> bookingFeeCodes)
	{
		this.bookingFeeCodes = bookingFeeCodes;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
