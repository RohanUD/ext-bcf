/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.data.ReservationEntriesData;
import com.bcf.facades.reservation.entry.data.BcfOrderEntryData;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListChangedEntriesHandler implements ReservationDataListHandler
{

	private Converter<AbstractOrderEntryModel, BcfOrderEntryData> bcfOrderEntryConverter;
	private PriceDataFactory priceDataFactory;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		if (Objects.isNull(abstractOrderModel) || Objects.isNull(abstractOrderModel.getOriginalOrder()))
		{
			reservationDataList.setChangedReservationDatas(Collections.emptyList());
			return;
		}

		final List<AbstractOrderEntryModel> orderEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> (Objects.equals(OrderEntryType.TRANSPORT, entry.getType())
						&& (Objects.equals(AmendStatus.NEW, entry.getAmendStatus())
						|| Objects.equals(AmendStatus.CHANGED, entry.getAmendStatus()))))
				.collect(Collectors.toList());

		reservationDataList.setChangedReservationDatas(getReservationEntriesDatas(orderEntries));
	}

	/**
	 * Gets the reservation entries datas.
	 *
	 * @param orderEntries the order entries
	 * @return the reservation entries datas
	 */
	protected List<ReservationEntriesData> getReservationEntriesDatas(final List<AbstractOrderEntryModel> orderEntries)
	{
		final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupByJourney = orderEntries.stream()
				.filter(entry -> !(entry.getProduct() instanceof FeeProductModel))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
		final List<ReservationEntriesData> reservationEntriesDatas = new ArrayList<>();

		entriesGroupByJourney.forEach((journeyRefNo, journeyEntries) -> {
			final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupByOriginDest = journeyEntries.stream()
					.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()))
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));

			entriesGroupByOriginDest.forEach((originDestRefNo, outInBoundEntries) -> reservationEntriesDatas
					.add(createReservationEntriesData(journeyRefNo, originDestRefNo, outInBoundEntries)));
		});

		return reservationEntriesDatas;
	}

	/**
	 * Creates the reservation entries data.
	 *
	 * @param journeyRefNo      the journey ref no
	 * @param originDestRefNo   the origin dest ref no
	 * @param outInBoundEntries the out in bound entries
	 * @return the reservation entries data
	 */
	protected ReservationEntriesData createReservationEntriesData(final int journeyRefNo, final int originDestRefNo,
			final Collection<AbstractOrderEntryModel> outInBoundEntries)
	{
		final ReservationEntriesData reservationEntriesData = new ReservationEntriesData();

		reservationEntriesData.setJourneyRefNumber(journeyRefNo);
		reservationEntriesData.setOriginDestinationRefNumber(originDestRefNo);

		final List<AbstractOrderEntryModel> changedOrderEntries = outInBoundEntries.stream()
				.filter(entry -> (entry.getActive() && Objects.equals(AmendStatus.CHANGED, entry.getAmendStatus())))
				.collect(Collectors.toList());
		final List<BcfOrderEntryData> changedOrderEntriesGroupByTraveller = createReservationEntriesGroupByTravellerType(
				changedOrderEntries);
		reservationEntriesData.setChangedOrderEntries(changedOrderEntriesGroupByTraveller);

		final List<AbstractOrderEntryModel> removedOrderEntries = outInBoundEntries.stream()
				.filter(entry -> (!entry.getActive() && Objects.equals(AmendStatus.CHANGED, entry.getAmendStatus())))
				.collect(Collectors.toList());
		final List<BcfOrderEntryData> removedOrderEntriesGroupByTraveller = createReservationEntriesGroupByTravellerType(
				removedOrderEntries);
		reservationEntriesData.setRemovedOrderEntries(removedOrderEntriesGroupByTraveller);

		final List<AbstractOrderEntryModel> newOrderEntries = outInBoundEntries.stream()
				.filter(entry -> Objects.equals(AmendStatus.NEW, entry.getAmendStatus())).collect(Collectors.toList());
		final List<BcfOrderEntryData> newOrderEntriesGroupByTraveller = createReservationEntriesGroupByTravellerType(
				newOrderEntries);
		reservationEntriesData.setNewOrderEntries(newOrderEntriesGroupByTraveller);

		return reservationEntriesData;
	}

	/**
	 * Creates the reservation entries group by traveller type.
	 *
	 * @param changedOrderEntries the changed order entries
	 * @return the map
	 */
	protected List<BcfOrderEntryData> createReservationEntriesGroupByTravellerType(
			final List<AbstractOrderEntryModel> orderEntries)
	{
		final Map<String, List<AbstractOrderEntryModel>> entriesByTraveller = new HashMap<>();
		orderEntries.stream()
				.filter(orderEntry -> Objects.equals(FareProductModel._TYPECODE, orderEntry.getProduct().getItemtype()))
				.forEach(orderEntry -> {
					String travellerCode = StringUtils.EMPTY;
					if (CollectionUtils.isNotEmpty(orderEntry.getTravelOrderEntryInfo().getTravellers()))
					{
						final TravellerModel traveller = orderEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst()
								.get();
						if (Objects.equals(TravellerType.PASSENGER, traveller.getType())
								&& traveller.getInfo() instanceof PassengerInformationModel)
						{
							final PassengerInformationModel passenger = (PassengerInformationModel) traveller.getInfo();
							travellerCode = passenger.getPassengerType().getCode();
						}
						else if (Objects.equals(TravellerType.VEHICLE, traveller.getType())
								&& traveller.getInfo() instanceof BCFVehicleInformationModel)
						{
							final BCFVehicleInformationModel vehicle = (BCFVehicleInformationModel) traveller.getInfo();
							travellerCode = vehicle.getVehicleType().getType().getCode();
						}
						List<AbstractOrderEntryModel> changedOrderEntriesByTraveller = entriesByTraveller.get(travellerCode);
						if (Objects.isNull(changedOrderEntriesByTraveller))
						{
							changedOrderEntriesByTraveller = new ArrayList<>();
							entriesByTraveller.put(travellerCode, changedOrderEntriesByTraveller);
						}
						changedOrderEntriesByTraveller.add(orderEntry);
					}
				});

		final List<BcfOrderEntryData> bcfOrderEntries = new ArrayList<>();
		bcfOrderEntries.addAll(getOrderEntriesData(entriesByTraveller));

		final Map<String, List<AbstractOrderEntryModel>> entriesByAncillary = orderEntries.stream()
				.filter(orderEntry -> Objects.equals(AncillaryProductModel._TYPECODE, orderEntry.getProduct().getItemtype()))
				.collect(Collectors.groupingBy(orderEntry -> orderEntry.getProduct().getCode()));
		bcfOrderEntries.addAll(getOrderEntriesData(entriesByAncillary));

		return bcfOrderEntries;
	}

	/**
	 * Gets the order entries data.
	 *
	 * @param entriesMap the entries map
	 * @return the order entries data
	 */
	protected List<BcfOrderEntryData> getOrderEntriesData(final Map<String, List<AbstractOrderEntryModel>> entriesMap)
	{
		final List<BcfOrderEntryData> bcfOrderEntries = new ArrayList<>();
		entriesMap.forEach((travellerCode, entries) -> {
			final AbstractOrderEntryModel orderEntry = entries.stream().findFirst().get();
			final BcfOrderEntryData bcfOrderEntryData = getBcfOrderEntryConverter().convert(orderEntry);
			bcfOrderEntryData.setQuantity(CollectionUtils.size(entries));
			bcfOrderEntryData
					.setBasePrice(getPriceFormattedValue(orderEntry, orderEntry.getBasePrice() * bcfOrderEntryData.getQuantity()));
			bcfOrderEntries.add(bcfOrderEntryData);
		});
		return bcfOrderEntries;
	}

	/**
	 * Gets the price formatted value.
	 *
	 * @param orderEntry the order entry
	 * @param val        the val
	 * @return the price data
	 */
	protected String getPriceFormattedValue(final AbstractOrderEntryModel orderEntry, final Double val)
	{
		return getPriceDataFactory()
				.create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()), orderEntry.getOrder().getCurrency())
				.getFormattedValue();
	}

	protected Converter<AbstractOrderEntryModel, BcfOrderEntryData> getBcfOrderEntryConverter()
	{
		return bcfOrderEntryConverter;
	}

	@Required
	public void setBcfOrderEntryConverter(final Converter<AbstractOrderEntryModel, BcfOrderEntryData> bcfOrderEntryConverter)
	{
		this.bcfOrderEntryConverter = bcfOrderEntryConverter;
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

}
