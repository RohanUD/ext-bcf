/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.OfferBreakdownData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationPricingInfoData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationPricingInfoOfferBreakdownHandler;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListPricingInfoOfferBreakdownHandler extends ReservationPricingInfoOfferBreakdownHandler
		implements ReservationDataListHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();

			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			if (CollectionUtils.isEmpty(reservationData.getReservationItems()))
			{
				return;
			}

			reservationData.getReservationItems().forEach(item -> {
				final ReservationPricingInfoData reservationPricingInfo = item.getReservationPricingInfo();
				if (reservationPricingInfo != null)
				{
					reservationPricingInfo.setOfferBreakdowns(createOfferBreakdowns(entries, item.getOriginDestinationRefNumber()));

				}
			});

		}

	}

	/**
	 * Creates a list of Offer Breakdowns for each ancillary product
	 *
	 * @param abstractOrderModel         the abstract order model
	 * @param originDestinationRefNumber the origin destination ref number
	 * @return list of offer breakdowns
	 */
	protected List<OfferBreakdownData> createOfferBreakdowns(final List<AbstractOrderEntryModel> entries,
			final int originDestinationRefNumber)
	{
		final List<AbstractOrderEntryModel> ancillaryEntries = entries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entry -> !(getNotAncillaryProductTypes().contains(entry.getProduct().getProductType())) && entry.getActive()
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationRefNumber)
				.collect(Collectors.toList());

		final List<OfferBreakdownData> offerBreakdowns = new ArrayList<OfferBreakdownData>();

		final List<AbstractOrderEntryModel> includedEntries = ancillaryEntries.stream().filter(entry -> entry.getBundleNo() > 0)
				.collect(Collectors.toList());

		retreiveOfferBreakdownsFromEntries(includedEntries, offerBreakdowns, Boolean.TRUE);

		final List<AbstractOrderEntryModel> extraEntries = ancillaryEntries.stream().filter(entry -> entry.getBundleNo() == 0)
				.collect(Collectors.toList());

		retreiveOfferBreakdownsFromEntries(extraEntries, offerBreakdowns, Boolean.FALSE);

		return offerBreakdowns;
	}

}
