/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.GlobalTravelReservationBasicHandler;


public class BcfGlobalTravelReservationBasicHandler extends GlobalTravelReservationBasicHandler
{

	@Override
	public void handle(AbstractOrderModel abstractOrderModel, GlobalTravelReservationData globalTravelReservationData) {

		super.handle(abstractOrderModel,globalTravelReservationData);
		globalTravelReservationData.setBookingJourneyType(abstractOrderModel.getBookingJourneyType()!=null?abstractOrderModel.getBookingJourneyType().getCode():"");
	}
}
