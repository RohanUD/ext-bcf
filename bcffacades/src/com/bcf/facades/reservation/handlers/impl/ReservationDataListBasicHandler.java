/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationBasicHandler;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListBasicHandler extends ReservationBasicHandler implements ReservationDataListHandler
{

	private ModelService modelSerivce;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();
			reservationData.setCode(abstractOrderModel.getCode());
			final SalesApplication salesApplication = getModelSerivce()
					.getAttributeValue(abstractOrderModel, "salesApplication");
			if (Objects.nonNull(salesApplication))
			{
				reservationData.setSalesApplication(salesApplication.getCode());
			}
			boolean isDefaultSailingPresent = cartEntriesByJourneyRefNumber.entrySet().stream().flatMap(entry -> entry.getValue().stream())
					.filter(e -> Objects.nonNull(e.getTravelOrderEntryInfo()))
					.flatMap(e -> e.getTravelOrderEntryInfo().getTransportOfferings().stream())
					.anyMatch(to -> Boolean.TRUE.equals(to.getDefault()));

			reservationData.setDefaultSailing(isDefaultSailingPresent);
		}
	}

	protected ModelService getModelSerivce()
	{
		return modelSerivce;
	}

	@Required
	public void setModelSerivce(final ModelService modelSerivce)
	{
		this.modelSerivce = modelSerivce;
	}

}
