/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.handlers.ActivityReservationDataHandler;


public class ActivityDetailsDataHandler implements ActivityReservationDataHandler
{
	private EnumerationService enumerationService;
	private I18NService i18NService;
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Override
	public void handle(final AbstractOrderModel abstractOrder, final AbstractOrderEntryModel abstractOrderEntry,
			final ActivityReservationData activityReservationData)
	{
		final ActivityDetailsData activityDetailsData = new ActivityDetailsData();
		activityDetailsData.setCode(abstractOrderEntry.getProduct().getCode());
		activityDetailsData.setName(abstractOrderEntry.getProduct().getName());
		activityReservationData.setEntryNumber(abstractOrderEntry.getEntryNumber());
		final ActivityProductModel activityProduct = (ActivityProductModel) abstractOrderEntry.getProduct();
		final List<String> activityCategoryTypes = activityProduct.getActivityCategoryTypes().stream()
				.map(name -> enumerationService.getEnumerationName(name)).collect(Collectors.toList());

		activityDetailsData.setActivityCategoryTypes(activityCategoryTypes);
		activityDetailsData.setTermsAndConditions(activityProduct.getTermsAndConditions());
		activityReservationData.setActivityDetails(activityDetailsData);
		activityReservationData.setQuantity(abstractOrderEntry.getQuantity().intValue());
		final ActivityOrderEntryInfoModel activityOrderEntryInfo = abstractOrderEntry.getActivityOrderEntryInfo();
		final SimpleDateFormat format = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);
		activityReservationData.setActivityDate(format.format(activityOrderEntryInfo.getActivityDate()));
		activityReservationData.setActivityTime(activityOrderEntryInfo.getActivityTime());
		final String guestType = getEnumerationService().getEnumerationName(activityOrderEntryInfo.getGuestType());
		activityReservationData.setGuestType(guestType);
		activityReservationData
				.setPromotions(bcfTravelCartFacadeHelper.getPromotionMessages(abstractOrder, Arrays.asList(abstractOrderEntry)));

		activityReservationData.setAvailabilityStatus(getAvailabilityStatus(abstractOrderEntry));

		if (Objects.nonNull(activityProduct.getDestination()))
		{
			activityReservationData.setDestination(activityProduct.getDestination().getName());
		}

		final TotalFareData totalFareData = new TotalFareData();
		final String currencyIso = abstractOrder.getCurrency().getIsocode();
		final double basePrice = abstractOrderEntry.getBasePrice();
		totalFareData.setBasePrice(getTravelCommercePriceFacade().createPriceData(basePrice, currencyIso));

		final double totalPrice = abstractOrderEntry.getTotalPrice();
		totalFareData.setTotalPrice(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIso));

		final double taxPrice = bcfTravelCartFacadeHelper.getGSTTaxForEntry(abstractOrderEntry);
		totalFareData.setTaxPrice(getTravelCommercePriceFacade().createPriceData(taxPrice, currencyIso));

		activityReservationData.setTotalFare(totalFareData);
	}

	private String getAvailabilityStatus(final AbstractOrderEntryModel abstractOrderEntry)
	{
		return !abstractOrderEntry.isStockAllocated() ? AvailabilityStatus.ON_REQUEST.name() :
				AvailabilityStatus.AVAILABLE.name();
	}

	/**
	 * Gets enumeration service.
	 *
	 * @return the enumerationService
	 */
	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	/**
	 * Sets enumeration service.
	 *
	 * @param enumerationService the enumerationService to set
	 */
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	/**
	 * Gets i 18 n service.
	 *
	 * @return the i18NService
	 */
	protected I18NService getI18NService()
	{
		return i18NService;
	}

	/**
	 * Sets i 18 n service.
	 *
	 * @param i18nService the i18NService to set
	 */
	@Required
	public void setI18NService(final I18NService i18nService)
	{
		i18NService = i18nService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
