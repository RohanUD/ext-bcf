/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.AccommodationReservationPropertyHandler;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.exceptions.AccommodationPipelineException;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.reservation.handlers.BcfAccommodationReservationHandler;


public class BcfAccommodationReservationPropertyHandler extends AccommodationReservationPropertyHandler implements
		BcfAccommodationReservationHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationEntries, final AccommodationReservationData reservationData, final boolean isDealEntry)
	{
		AccommodationOfferingModel accommodationOffering = null;
		if(isDealEntry)
		{
			accommodationOffering = ((DealOrderEntryGroupModel) accommodationEntries.get(0).getEntryGroup())
					.getAccommodationEntryGroups().stream().findFirst().get().getAccommodationOffering();
			reservationData.setJourneyRef(String.valueOf(((DealOrderEntryGroupModel) accommodationEntries.get(0).getEntryGroup()).getJourneyRefNumber()));

		}
		else
		{
			accommodationOffering = ((AccommodationOrderEntryGroupModel) accommodationEntries.get(0).getEntryGroup())
					.getAccommodationOffering();
			AbstractOrderEntryModel transportEntry= accommodationEntries.get(0).getEntryGroup().getEntries().stream().filter(entry-> OrderEntryType.TRANSPORT.equals(entry.getType())).findAny().orElse(null);
			if(transportEntry!=null){
				reservationData.setJourneyRef(String.valueOf(transportEntry.getJourneyReferenceNumber()));
			}

		}

		reservationData.setAccommodationReference(getAccommodationOfferingConverter().convert(accommodationOffering));
	}

	@Override
	public void handle(AbstractOrderModel abstractOrder, AccommodationReservationData accommodationReservationData) throws AccommodationPipelineException {
		List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = this.getBookingService().getAccommodationOrderEntryGroups(abstractOrder);
		if (CollectionUtils.isEmpty(accommodationOrderEntryGroups)) {
			throw new AccommodationPipelineException("No abstract order entry group in cart!");
		} else {
			AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = accommodationOrderEntryGroups.get(0);
			AccommodationOfferingModel accommodationOffering = accommodationOrderEntryGroup.getAccommodationOffering();
			accommodationReservationData.setAccommodationReference(
					this.getAccommodationOfferingConverter().convert(accommodationOffering));
			if(accommodationOrderEntryGroups.get(0).getDealOrderEntryGroup()!=null){
				accommodationReservationData.setJourneyRef(String.valueOf(accommodationOrderEntryGroups.get(0).getDealOrderEntryGroup().getJourneyRefNumber()));

			}else{
				AbstractOrderEntryModel transportEntry=accommodationOrderEntryGroups.get(0).getEntries().stream().filter(entry-> OrderEntryType.TRANSPORT.equals(entry.getType())).findAny().orElse(null);
				if(transportEntry!=null){
					accommodationReservationData.setJourneyRef(String.valueOf(transportEntry.getJourneyReferenceNumber()));
				}
			}


		}
		accommodationReservationData.setBookingJourneyType(abstractOrder.getBookingJourneyType()!=null?abstractOrder.getBookingJourneyType().getCode():"");
	}

}
