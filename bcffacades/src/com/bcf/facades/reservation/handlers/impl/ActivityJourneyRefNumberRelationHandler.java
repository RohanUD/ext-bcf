/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Optional;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.handlers.ActivityReservationDataHandler;


public class ActivityJourneyRefNumberRelationHandler implements ActivityReservationDataHandler
{
	@Override
	public void handle(final AbstractOrderModel abstractOrder, final AbstractOrderEntryModel abstractOrderEntry,
			final ActivityReservationData activityReservationData)
	{
		final Optional<AbstractOrderEntryModel> transportOrderEntryOptional =  abstractOrder.getEntries().stream().filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())).findFirst();
		if(transportOrderEntryOptional.isPresent())
		{
			final AbstractOrderEntryModel transportOrderEnty = transportOrderEntryOptional.get();
			final DealOrderEntryGroupModel dealOrderEntryGroup = (DealOrderEntryGroupModel) transportOrderEnty.getEntryGroup();
			activityReservationData.setJourneyRefNumber(dealOrderEntryGroup.getJourneyRefNumber());
		}
	}
}
