/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.travel.order.PaymentTransactionData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.travelfacades.reservation.handlers.impl.ReservationPaymentTransactionHandler;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class ReservationDataListPaymentTransactionHandler extends ReservationPaymentTransactionHandler
		implements ReservationDataListHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<PaymentTransactionData> paymentTransactionDataList = new LinkedList<>();
		for (final PaymentTransactionModel paymentTransactionModel : abstractOrderModel.getPaymentTransactions())
		{
			final PaymentTransactionData paymentTransactionData = new PaymentTransactionData();
			getPaymentTransactionConverter().convert(paymentTransactionModel, paymentTransactionData);
			paymentTransactionDataList.add(paymentTransactionData);
		}

		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();
			reservationData.setPaymentTransaction(paymentTransactionDataList);

		}
	}

}
