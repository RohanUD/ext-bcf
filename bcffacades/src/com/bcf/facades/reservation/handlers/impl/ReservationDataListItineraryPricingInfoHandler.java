/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.handlers.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commercefacades.travel.reservation.data.OfferBreakdownData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationPricingInfoData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.ProductType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.AccessibilityItemData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.impl.DefaultBcfReservationItineraryPricingInfoHandler;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;



public class ReservationDataListItineraryPricingInfoHandler extends DefaultBcfReservationItineraryPricingInfoHandler
		implements ReservationDataListHandler
{
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private AncillaryProductFacade ancillaryProductFacade;
	private NotificationUtils notificationUtils;
	private Converter<ProductModel, ProductData> productConverter;
	private Converter<ProductModel, ProductData> ancillaryProductConverter;
	private Converter<SpecialServiceRequestModel, SpecialServiceRequestData> specialServiceRequestConverter;
	private List<ProductType> notAncillaryProductTypes;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final Map<Integer, List<AbstractOrderEntryModel>> cartEntriesByJourneyRefNumber,
			final ReservationDataList reservationDataList)
	{
		final List<ReservationData> reservationDatas = reservationDataList.getReservationDatas();
		final ListIterator<ReservationData> itr = reservationDatas.listIterator();
		while (itr.hasNext())
		{
			final ReservationData reservationData = itr.next();

			final List<AbstractOrderEntryModel> entries = cartEntriesByJourneyRefNumber
					.get(reservationData.getJourneyReferenceNumber());

			final List<ReservationItemData> reservationItems = reservationData.getReservationItems();


			reservationItems.forEach(reservationItem -> {
				final ReservationPricingInfoData reservationPricingInfo = reservationItem.getReservationPricingInfo();
				if (reservationPricingInfo != null)
				{
					reservationPricingInfo.setItineraryPricingInfo(createItineraryPricingInfo(entries, reservationItem));
					reservationPricingInfo
							.setAncilliaryDatas(createOfferBreakdowns(entries, reservationItem.getOriginDestinationRefNumber()));
				}
			});

		}

	}

	/**
	 * Creates a new Itinerary Pricing Info to hold fare products summary for each leg of the journey
	 *
	 * @param entries         - given List of Abstract Order Entry Model
	 * @param reservationItem - Reservation Item of current leg
	 * @return new itinerary pricing info
	 */
	protected ItineraryPricingInfoData createItineraryPricingInfo(final List<AbstractOrderEntryModel> entries,
			final ReservationItemData reservationItem)
	{
		final ItineraryPricingInfoData itineraryPricingInfo = new ItineraryPricingInfoData();
		itineraryPricingInfo.setAvailable(Boolean.TRUE);
		final List<AbstractOrderEntryModel> bundledEntries = entries.stream()
				.filter(e -> OrderEntryType.TRANSPORT.equals(e.getType()))
				.filter(e -> e.getActive() && e.getBundleNo() != 0
						&& e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && e.getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber() == reservationItem.getOriginDestinationRefNumber())
				.collect(Collectors.toList());

		final List<PTCFareBreakdownData> pTCFareBreakdownDatas = createPTCFareBreakdowns(bundledEntries, reservationItem);
		itineraryPricingInfo.setPtcFareBreakdownDatas(createPTCFareBreakdowns(bundledEntries, reservationItem));


		final List<PassengerTypeQuantityData> passengerTypeQuantityDatas = pTCFareBreakdownDatas.stream()
				.map(pTCFareBreakdownData -> pTCFareBreakdownData.getPassengerTypeQuantity()).collect(Collectors.toList());


		final List<AbstractOrderEntryModel> accessibilityEntries = entries.stream()
				.filter(e -> OrderEntryType.TRANSPORT.equals(e.getType()))
				.filter(e -> e.getActive()
						&& e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null && e.getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber() == reservationItem.getOriginDestinationRefNumber())
				.collect(Collectors.toList());

		itineraryPricingInfo.setVehicleFareBreakdownDatas(createVehicleFareBreakdowns(bundledEntries, reservationItem));

		itineraryPricingInfo.setTravellerBreakdowns(createTravellerBreakdowns(bundledEntries, reservationItem));

		itineraryPricingInfo.setAccessibilityBreakdownDataList(
				createAccessibilityBreakdownDataList(entries, reservationItem.getOriginDestinationRefNumber()));

		itineraryPricingInfo.setAccessibilities(getAccessibilityDatas(accessibilityEntries, passengerTypeQuantityDatas));

		itineraryPricingInfo.setLargeItemsBreakdownDataList(createLargeItemBreakdowns(bundledEntries));

		final Optional<AbstractOrderEntryModel> abstractOrderEntry = bundledEntries.stream()
				.filter(entry -> FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype())).findAny();
		if (!abstractOrderEntry.isPresent() || Objects.isNull(abstractOrderEntry.get().getBundleTemplate()))
		{
			return itineraryPricingInfo;
		}
		final BundleTemplateModel bundleTemplateModel = abstractOrderEntry.get().getBundleTemplate();

		itineraryPricingInfo.setBundleTemplates(createBundleTemplates(bundleTemplateModel));

		final BundleType bundleType = Objects.nonNull(bundleTemplateModel.getType()) ? bundleTemplateModel.getType()
				: bundleTemplateModel.getParentTemplate().getType();
		itineraryPricingInfo.setBundleType(bundleType.getCode());
		itineraryPricingInfo.setBundleTypeName(getEnumerationService().getEnumerationName(bundleType));

		return itineraryPricingInfo;
	}

	private List<AccessibilityRequestData> getAccessibilityDatas(final List<AbstractOrderEntryModel> entries,
			final List<PassengerTypeQuantityData> passengerTypeQuantityDatas)
	{

		final List<AccessibilityRequestData> accessibilityRequestDataList = new ArrayList<>();

		final List<AbstractOrderEntryModel> specialReqEntries = StreamUtil.safeStream(entries).filter(
				entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
						&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail() != null
						&& CollectionUtils.isNotEmpty(
						entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail()
								.getSpecialServiceRequest())).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(specialReqEntries))
		{
			for (final AbstractOrderEntryModel specialReqEntry : specialReqEntries)
			{
				final List<SpecialServiceRequestData> specialServiceRequestDataList = specialReqEntry.getTravelOrderEntryInfo()
						.getTravellers().iterator().next().getSpecialRequestDetail().getSpecialServiceRequest().stream()
						.filter(specialReq -> !specialReq.getCode().equals("OTHR"))
						.map(specialReq -> specialServiceRequestConverter.convert(specialReq)).collect(Collectors.toList());

				if (CollectionUtils.isNotEmpty(specialServiceRequestDataList))
				{
					final AccessibilityRequestData accessibilityRequestData = new AccessibilityRequestData();
					accessibilityRequestDataList.add(accessibilityRequestData);
					accessibilityRequestData.setPassengerType(passengerTypeQuantityDatas.stream().filter(
							passengerTypeQuantityData -> passengerTypeQuantityData.getPassengerType().getCode().equals(
									((PassengerInformationModel) specialReqEntry.getTravelOrderEntryInfo().getTravellers().iterator()
											.next().getInfo()).getPassengerType().getCode())).findFirst()
							.map(passengerTypeQuantityData -> passengerTypeQuantityData.getPassengerType()).get());

					if (CollectionUtils.isNotEmpty(specialServiceRequestDataList))
					{
						accessibilityRequestData.setSpecialServiceRequestDataList(specialServiceRequestDataList);
					}
				}
			}
		}
		return accessibilityRequestDataList;
	}

	protected List<OfferBreakdownData> createOfferBreakdowns(final List<AbstractOrderEntryModel> entries,
			final int originDestinationRefNumber)
	{
		final List<String> accessibilityList = BCFPropertiesUtils.convertToList(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
		final List<String> largeItems = BCFPropertiesUtils.convertToList(
				getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES));
		final List<AbstractOrderEntryModel> ancillaryEntries = entries.stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()))
				.filter(entry -> !(getNotAncillaryProductTypes().contains(entry.getProduct().getProductType())) && entry.getActive()
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationRefNumber)
				.filter(entry -> !accessibilityList.contains(entry.getProduct().getCode()))
				.filter(entry -> !largeItems.contains(entry.getProduct().getCode()))
				.collect(Collectors.toList());

		final List<OfferBreakdownData> offerBreakdowns = new ArrayList<OfferBreakdownData>();

		final List<AbstractOrderEntryModel> includedEntries = ancillaryEntries.stream().filter(entry -> entry.getBundleNo() > 0)
				.collect(Collectors.toList());

		retreiveOfferBreakdownsFromEntries(includedEntries, offerBreakdowns, Boolean.TRUE);

		final List<AbstractOrderEntryModel> extraEntries = ancillaryEntries.stream().filter(entry -> entry.getBundleNo() == 0)
				.collect(Collectors.toList());

		retreiveOfferBreakdownsFromEntries(extraEntries, offerBreakdowns, Boolean.FALSE);

		return offerBreakdowns;
	}

	protected OfferBreakdownData createOfferBreakdown(final AbstractOrderEntryModel entry)
	{
		final OfferBreakdownData offerBreakdown = new OfferBreakdownData();
		offerBreakdown.setProduct(this.getProductConverter().convert(entry.getProduct()));
		offerBreakdown.setQuantity(entry.getQuantity().intValue());
		offerBreakdown.setIncluded(entry.getBundleNo() > 0 ? Boolean.TRUE : Boolean.FALSE);
		final TotalFareData totalFare = new TotalFareData();
		final String currencyIsocode = entry.getOrder().getCurrency().getIsocode();
		totalFare.setBasePrice(this.getTravelCommercePriceFacade().createPriceData(entry.getBasePrice(), currencyIsocode));
		totalFare.setTotalPrice(this.getTravelCommercePriceFacade().createPriceData(entry.getTotalPrice(), currencyIsocode));
		offerBreakdown.setTotalFare(totalFare);
		return offerBreakdown;
	}

	protected OfferBreakdownData getOfferBreakdownFromList(final List<OfferBreakdownData> offerBreakdowns,
			final String productCode,
			final Boolean included)
	{
		final Iterator var5 = offerBreakdowns.iterator();

		OfferBreakdownData offerBreakdown;
		do
		{
			if (!var5.hasNext())
			{
				return null;
			}

			offerBreakdown = (OfferBreakdownData) var5.next();
		}
		while (!offerBreakdown.getProduct().getCode().equals(productCode) || !included.equals(offerBreakdown.getIncluded()));

		return offerBreakdown;
	}

	protected void updateOfferBreakdown(final OfferBreakdownData offerBreakdown, final AbstractOrderEntryModel entry)
	{
		final int updatedQuantity = offerBreakdown.getQuantity() + entry.getQuantity().intValue();
		offerBreakdown.setQuantity(updatedQuantity);
		final BigDecimal totalPrice = updatedQuantity == 0 ?
				BigDecimal.valueOf(entry.getTotalPrice()) :
				offerBreakdown.getTotalFare().getTotalPrice().getValue().add(BigDecimal.valueOf(entry.getTotalPrice()));
		final String currencyIisocode = entry.getOrder().getCurrency().getIsocode();
		offerBreakdown.getTotalFare()
				.setTotalPrice(this.getTravelCommercePriceFacade().createPriceData(totalPrice.doubleValue(), currencyIisocode));
	}


	protected void retreiveOfferBreakdownsFromEntries(final List<AbstractOrderEntryModel> entries,
			final List<OfferBreakdownData> offerBreakdowns, final Boolean included)
	{
		final Iterator var5 = entries.iterator();

		while (var5.hasNext())
		{
			final AbstractOrderEntryModel entry = (AbstractOrderEntryModel) var5.next();
			final OfferBreakdownData offBreak;
			if (CollectionUtils.isEmpty(offerBreakdowns))
			{
				offBreak = this.createOfferBreakdown(entry);
				offerBreakdowns.add(offBreak);
			}
			else
			{
				offBreak = this.getOfferBreakdownFromList(offerBreakdowns, entry.getProduct().getCode(), included);
				if (offBreak == null)
				{
					final OfferBreakdownData offerBreakdown = this.createOfferBreakdown(entry);
					offerBreakdowns.add(offerBreakdown);
				}
				else
				{
					this.updateOfferBreakdown(offBreak, entry);
				}
			}
		}

	}

	private List<TravelBundleTemplateData> createBundleTemplates(final BundleTemplateModel bundleTemplateModel)
	{
		final List<TravelBundleTemplateData> bundleTemplates = new ArrayList<>();
		final TravelBundleTemplateData bundleTemplate = new TravelBundleTemplateData();
		bundleTemplate.setFareProductBundleTemplateId(bundleTemplateModel.getId());
		bundleTemplates.add(bundleTemplate);
		return bundleTemplates;
	}

	protected List<AccessibilityItemData> createAccessibilityBreakdownDataList(final List<AbstractOrderEntryModel> entries,
			final int odRef)
	{
		final List<AbstractOrderEntryModel> bundledEntries = entries.stream()
				.filter(e -> OrderEntryType.TRANSPORT.equals(e.getType()))
				.filter(e -> e.getActive() && e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
						&& e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRef).collect(Collectors.toList());
		final List<ProductData> productDataList = getAncillaryProductFacade().getAncillaryProductsForCodes(BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS)));
		final List<AccessibilityItemData> accessibilityItems = new ArrayList<>();
		StreamUtil.safeStream(bundledEntries).forEach(entry -> {
			if (Objects.nonNull(entry.getProduct()))
			{
				StreamUtil.safeStream(productDataList).forEach(productData -> {
					if (StringUtils.equals(productData.getCode(), entry.getProduct().getCode()))
					{
						final AccessibilityItemData accessibilityItemData = new AccessibilityItemData();
						accessibilityItemData.setCode(productData.getCode());
						accessibilityItemData.setName(productData.getName());
						accessibilityItemData.setQuantity(1);
						accessibilityItemData.setPrice(this.getTravelCommercePriceFacade().createPriceData(entry.getTotalPrice()));
						accessibilityItems.add(accessibilityItemData);
					}
				});
			}
		});
		final List<AccessibilityItemData> accessibilityItemDataList = new ArrayList<>();
		StreamUtil.safeStream(accessibilityItems)
				.collect(Collectors.groupingBy(accessibilityItemData -> accessibilityItemData.getCode())).entrySet()
				.forEach(entry -> {
					final AccessibilityItemData accessibilityItemData = entry.getValue().get(0);
					accessibilityItemData.setQuantity(entry.getValue().size());
					final double totalPrice = StreamUtil.safeStream(entry.getValue())
							.mapToDouble(value -> value.getPrice().getValue().doubleValue()).sum();
					accessibilityItemData.setPrice(this.getTravelCommercePriceFacade().createPriceData(totalPrice));
					accessibilityItemDataList.add(accessibilityItemData);
				});
		return accessibilityItemDataList;
	}

	protected List<LargeItemData> createLargeItemBreakdowns(final List<AbstractOrderEntryModel> bundledEntries)
	{
		final List<ProductData> productDataList = getAncillaryProductFacade().getAncillaryProductsForCodes(BCFPropertiesUtils
				.convertToList(
						getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES)));
		final List<LargeItemData> largeItems = new ArrayList<>();
		StreamUtil.safeStream(bundledEntries).forEach(entry -> {
			if (Objects.nonNull(entry.getProduct()))
			{
				StreamUtil.safeStream(productDataList).forEach(productData -> {
					if (StringUtils.equals(productData.getCode(), entry.getProduct().getCode()))
					{
						final LargeItemData largeItemData = new LargeItemData();
						largeItemData.setCode(productData.getCode());
						largeItemData.setName(productData.getName());
						largeItemData.setQuantity(1);
						largeItemData.setPrice(this.getTravelCommercePriceFacade().createPriceData(entry.getTotalPrice()));
						largeItems.add(largeItemData);
					}
				});
			}
		});
		final List<LargeItemData> largeItemDataList = new ArrayList<>();
		StreamUtil.safeStream(largeItems).collect(Collectors.groupingBy(largeItemData -> largeItemData.getCode())).entrySet()
				.forEach(entry -> {
					final LargeItemData largeItemData = entry.getValue().get(0);
					largeItemData.setQuantity(entry.getValue().size());
					final double totalPrice = StreamUtil.safeStream(entry.getValue())
							.mapToDouble(value -> value.getPrice().getValue().doubleValue()).sum();
					largeItemData.setPrice(this.getTravelCommercePriceFacade().createPriceData(totalPrice));
					largeItemDataList.add(largeItemData);
				});
		return largeItemDataList;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected AncillaryProductFacade getAncillaryProductFacade()
	{
		return ancillaryProductFacade;
	}

	@Required
	public void setAncillaryProductFacade(final AncillaryProductFacade ancillaryProductFacade)
	{
		this.ancillaryProductFacade = ancillaryProductFacade;
	}

	public Converter<ProductModel, ProductData> getAncillaryProductConverter()
	{
		return ancillaryProductConverter;
	}

	public void setAncillaryProductConverter(
			final Converter<ProductModel, ProductData> ancillaryProductConverter)
	{
		this.ancillaryProductConverter = ancillaryProductConverter;
	}

	public Converter<SpecialServiceRequestModel, SpecialServiceRequestData> getSpecialServiceRequestConverter()
	{
		return specialServiceRequestConverter;
	}

	public void setSpecialServiceRequestConverter(
			final Converter<SpecialServiceRequestModel, SpecialServiceRequestData> specialServiceRequestConverter)
	{
		this.specialServiceRequestConverter = specialServiceRequestConverter;
	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}

	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public void setProductConverter(
			final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	public List<ProductType> getNotAncillaryProductTypes()
	{
		return notAncillaryProductTypes;
	}

	public void setNotAncillaryProductTypes(final List<ProductType> notAncillaryProductTypes)
	{
		this.notAncillaryProductTypes = notAncillaryProductTypes;
	}
}
