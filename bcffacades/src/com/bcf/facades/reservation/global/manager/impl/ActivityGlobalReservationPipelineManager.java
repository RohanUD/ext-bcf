/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager.impl;

import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.data.ActivityReservationDataList;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.global.manager.BcfTravelPipelineManager;
import com.bcf.facades.reservation.handlers.ActivityReservationDataHandler;


public class ActivityGlobalReservationPipelineManager implements BcfTravelPipelineManager
{
	private List<ActivityReservationDataHandler> handlers;

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> entries, final BcfGlobalReservationData globalReservationData)
	{
		final List<AbstractOrderEntryModel> transportOrActivitiesEntries = entries.stream().filter(entry -> Objects.isNull(entry.getEntryGroup())).collect(
				Collectors.toList());

		if(CollectionUtils.isNotEmpty(transportOrActivitiesEntries))
		{
			final List<AbstractOrderEntryModel> activityEntries = transportOrActivitiesEntries.stream().filter(entry -> OrderEntryType.ACTIVITY.equals(entry.getType())).collect(Collectors.toList());
			final ActivityReservationDataList activityReservationDataList = executeHandlers(abstractOrderModel, activityEntries);
			globalReservationData.setActivityReservations(activityReservationDataList);
		}

	}

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> activityEntries, final GlobalTravelReservationData globalTravelReservationData)
	{
		final ActivityReservationDataList activityReservationDataList = executeHandlers(abstractOrderModel, activityEntries);
		globalTravelReservationData.setActivityReservations(activityReservationDataList);
	}

	protected ActivityReservationDataList  executeHandlers(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> activityEntries)
	{
		final ActivityReservationDataList activityReservationDataList = new ActivityReservationDataList();
		final List<ActivityReservationData> activityReservationDatas = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(activityEntries))
		{
			activityEntries.forEach(key -> {
				final ActivityReservationData activityReservationData = new ActivityReservationData();
				for(final ActivityReservationDataHandler activityHandler : getHandlers())
				{
					activityHandler.handle(abstractOrderModel, key, activityReservationData);
				}
				activityReservationDatas.add(activityReservationData);
			});
			activityReservationDataList.setActivityReservationDatas(activityReservationDatas);
			return activityReservationDataList;
		}
		return null;
	}

	protected List<ActivityReservationDataHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ActivityReservationDataHandler> handlers)
	{
		this.handlers = handlers;
	}
}
