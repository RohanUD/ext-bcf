/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager;

import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public interface BcfTravelPipelineManager
{
	/**
	 * Execute pipeline reservation data.
	 *
	 * @param abstractOrderModel the abstract order model
	 * @return the global reservation data
	 */
	void executePipeline(AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> entries, BcfGlobalReservationData globalReservationData);

	void executePipeline(AbstractOrderModel abstractOrderModel, List<AbstractOrderEntryModel> accommodationEntries, GlobalTravelReservationData globalTravelReservationData);

}






