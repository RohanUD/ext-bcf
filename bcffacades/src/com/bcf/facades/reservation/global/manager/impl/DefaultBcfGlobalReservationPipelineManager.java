/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotionengineservices.promotionengine.report.data.PromotionEngineResult;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.global.manager.BcfGlobalReservationPipelineManager;
import com.bcf.facades.reservation.global.manager.BcfTravelPipelineManager;
import com.bcf.facades.reservation.handlers.BcfGlobalReservationHandler;


public class DefaultBcfGlobalReservationPipelineManager implements BcfGlobalReservationPipelineManager
{
	private Map<OrderEntryType, BcfTravelPipelineManager> pipeLineManagers;
	private List<BcfGlobalReservationHandler> globalHandlers;

	private static final Logger LOG = Logger.getLogger(DefaultBcfGlobalReservationPipelineManager.class);

	@Override
	public BcfGlobalReservationData executePipeline(final AbstractOrderModel abstractOrderModel, final Collection<OrderEntryType> options)
	{
		final BcfGlobalReservationData globalReservationData = new BcfGlobalReservationData();
		populateReservationData(abstractOrderModel, globalReservationData, options);
		for (final BcfGlobalReservationHandler handler : globalHandlers)
		{
			handler.handle(abstractOrderModel, globalReservationData);
		}
		return globalReservationData;
	}

	protected void populateReservationData(final AbstractOrderModel abstractOrderModel, final BcfGlobalReservationData globalReservationData, final Collection<OrderEntryType> options)
	{
		final List<AbstractOrderEntryModel> cartEntries = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getActive() || OrderStatus.CANCELLED.equals(
						abstractOrderModel.getStatus())).collect(
				Collectors.toList());
			for (final OrderEntryType option : options)
			{
				final BcfTravelPipelineManager pipelineManager = getPipeLineManagers().get(option);
				if (Objects.isNull(pipelineManager))
				{
					LOG.warn("No pipelineManager configured for option [" + option + "]");
				}
				else
				{
					pipelineManager.executePipeline(abstractOrderModel, cartEntries, globalReservationData);
				}
			}
		globalReservationData.setPromotions(getPromotions(abstractOrderModel,cartEntries));
	}

	private List<String> getPromotions(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> orderEntries)
	{

		final Set<PromotionResultModel> promotionResults = abstractOrderModel.getAllPromotionResults();

		if (CollectionUtils.isNotEmpty(promotionResults))
		{

			return promotionResults.stream().map(promotionResult -> promotionResult.getMessageFired()).collect(Collectors.toList());
		}

		return Collections.emptyList();
	}

	private String getPromotionMessage(final PromotionEngineResult promotionEngineResult)
	{

		if (promotionEngineResult.getPromotionResult() != null && StringUtils
				.isNotBlank(promotionEngineResult.getPromotionResult().getMessageFired()))
		{

			return promotionEngineResult.getPromotionResult().getMessageFired();
		}
		return promotionEngineResult.getDescription();
	}


	protected Map<OrderEntryType, BcfTravelPipelineManager> getPipeLineManagers()
	{
		return pipeLineManagers;
	}

	@Required
	public void setPipeLineManagers(
			final Map<OrderEntryType, BcfTravelPipelineManager> pipeLineManagers)
	{
		this.pipeLineManagers = pipeLineManagers;
	}

	protected List<BcfGlobalReservationHandler> getGlobalHandlers()
	{
		return globalHandlers;
	}

	@Required
	public void setGlobalHandlers(final List<BcfGlobalReservationHandler> globalHandlers)
	{
		this.globalHandlers = globalHandlers;
	}

}
