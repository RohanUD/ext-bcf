/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager.impl;

import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.data.PackageReservationDataList;
import com.bcf.facades.reservation.global.manager.BcfTravelPipelineManager;


public class DealGlobalReservationPipelineManager implements BcfTravelPipelineManager
{
	private BcfTravelPipelineManager dealTransportPipelineManager;
	private BcfTravelPipelineManager dealAccommodationPipelineManager;
	private BcfTravelPipelineManager dealActivityPipelineManager;

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> entries,
			final BcfGlobalReservationData globalReservationData)
	{
		final List<AbstractOrderEntryModel> dealEntries = entries.stream().filter(entry -> entry.getActive() && entry.getEntryGroup() instanceof DealOrderEntryGroupModel).collect(
				Collectors.toList());
		if(CollectionUtils.isEmpty(dealEntries))
		{
			return;
		}
		final PackageReservationDataList packageReservationDataList = new PackageReservationDataList();
		final List<GlobalTravelReservationData> packages = new ArrayList<>();
		final Map<String, List<AbstractOrderEntryModel>> dealEntriesByPk = dealEntries.stream().collect(Collectors.groupingBy(entry -> entry
				.getEntryGroup()
				.getPk().getLongValueAsString()));
		dealEntriesByPk.forEach((key, entriesPerDeal) -> {
			final GlobalTravelReservationData globalTravelReservationData = new GlobalTravelReservationData();
			final List<AbstractOrderEntryModel> transportEntries = entriesPerDeal.stream().filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
					.nonNull(entry.getTravelOrderEntryInfo())).sorted(Comparator.comparing(entry->entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(transportEntries)){
				getDealTransportPipelineManager().executePipeline(abstractOrderModel, transportEntries, globalTravelReservationData);
			}
			final List<AbstractOrderEntryModel> accommodationEntries = entriesPerDeal.stream().filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType())).collect(
					Collectors.toList());
			if(CollectionUtils.isNotEmpty(accommodationEntries)){
				getDealAccommodationPipelineManager().executePipeline(abstractOrderModel, accommodationEntries, globalTravelReservationData);
			}
			final List<AbstractOrderEntryModel> activityEntries = entriesPerDeal.stream().filter(entry -> OrderEntryType.ACTIVITY.equals(entry.getType())).collect(
					Collectors.toList());

			if(CollectionUtils.isNotEmpty(activityEntries)){
				getDealActivityPipelineManager().executePipeline(abstractOrderModel, activityEntries, globalTravelReservationData);
			}


			globalTravelReservationData.setParentPackageId(((DealOrderEntryGroupModel) entriesPerDeal.get(0).getEntryGroup()).getPackageId());
			packages.add(globalTravelReservationData);
		});


		packageReservationDataList.setPackageReservationDatas(packages);
		globalReservationData.setPackageReservations(packageReservationDataList);
	}

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationEntries,
			final GlobalTravelReservationData globalTravelReservationData)
	{
		// empty implementation
	}

	protected BcfTravelPipelineManager getDealTransportPipelineManager()
	{
		return dealTransportPipelineManager;
	}

	@Required
	public void setDealTransportPipelineManager(
			final BcfTravelPipelineManager dealTransportPipelineManager)
	{
		this.dealTransportPipelineManager = dealTransportPipelineManager;
	}

	protected BcfTravelPipelineManager getDealAccommodationPipelineManager()
	{
		return dealAccommodationPipelineManager;
	}

	@Required
	public void setDealAccommodationPipelineManager(
			final BcfTravelPipelineManager dealAccommodationPipelineManager)
	{
		this.dealAccommodationPipelineManager = dealAccommodationPipelineManager;
	}

	protected BcfTravelPipelineManager getDealActivityPipelineManager()
	{
		return dealActivityPipelineManager;
	}

	@Required
	public void setDealActivityPipelineManager(
			final BcfTravelPipelineManager dealActivityPipelineManager)
	{
		this.dealActivityPipelineManager = dealActivityPipelineManager;
	}
}
