/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Collection;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


public interface BcfGlobalReservationPipelineManager
{
	/**
	 * Execute pipeline reservation data.
	 *
	 * @param abstractOrderModel
	 * 		the abstract order model
	 *
	 * @return the global reservation data
	 */
	BcfGlobalReservationData executePipeline(AbstractOrderModel abstractOrderModel, final Collection<OrderEntryType> options);

}
