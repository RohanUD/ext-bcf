/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager.impl;

import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.global.manager.BcfTravelPipelineManager;
import com.bcf.facades.reservation.handlers.ReservationDataListHandler;


public class TransportGlobalReservationPipelineManager implements BcfTravelPipelineManager
{
	private List<ReservationDataListHandler> handlers;
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> cartEntries,
			final BcfGlobalReservationData globalReservationData)
	{
		final List<AbstractOrderEntryModel> transportOrActivitiesEntries = cartEntries.stream()
				.filter(entry -> Objects.isNull(entry.getEntryGroup())).collect(
						Collectors.toList());
		if (CollectionUtils.isEmpty(transportOrActivitiesEntries))
		{
			return;
		}

		final List<AbstractOrderEntryModel> transportEntries;
		if (abstractOrderModel.getBookingJourneyType().equals(BookingJourneyType.BOOKING_TRANSPORT_ONLY) && CartModel._TYPECODE.equalsIgnoreCase(abstractOrderModel.getItemtype()) && getBcfTravelCartFacade().isAmendmentCart())
		{
			transportEntries = transportOrActivitiesEntries.stream()
					.filter(
							entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && AmendStatus.NEW.equals(entry.getAmendStatus()))
					.collect(Collectors.toList());

		}
		else
		{
			transportEntries = transportOrActivitiesEntries.stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())).collect(Collectors.toList());

		}
		final ReservationDataList reservationDataList = executeHandlers(abstractOrderModel, transportEntries);

		if (reservationDataList != null && !CollectionUtils.isEmpty(reservationDataList.getReservationDatas())){

			reservationDataList.getReservationDatas().forEach(reservationData->{
				reservationData.getReservationItems().sort(Comparator.comparing(reservationItemData->reservationItemData.getOriginDestinationRefNumber()));
			});

			reservationDataList.getReservationDatas().sort(Comparator.comparing(reservationData->getReservationItemData(reservationData).getReservationItinerary().getOriginDestinationOptions().get(0).getTransportOfferings().get(0).getDepartureTime()));
		}


		globalReservationData.setTransportReservations(reservationDataList);

	}


	private ReservationItemData getReservationItemData(ReservationData reservationData){

		reservationData.getReservationItems().sort(Comparator.comparing(reservationItemData->reservationItemData.getOriginDestinationRefNumber()));

		return reservationData.getReservationItems().get(0);

	}


	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> trannsportEntries,
			final GlobalTravelReservationData globalTravelReservationData)
	{
		final ReservationDataList reservationDataList = executeHandlers(abstractOrderModel, trannsportEntries);
		globalTravelReservationData.setReservationData(reservationDataList.getReservationDatas().get(0));
	}

	protected ReservationDataList executeHandlers(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> transportEntries)
	{
		final ReservationDataList reservationDataList = new ReservationDataList();
		if (CollectionUtils.isNotEmpty(transportEntries))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> transportEntriesByJourneyRefNumber = transportEntries.stream()
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));
			final List<ReservationData> reservationDatas = new ArrayList<>();

			transportEntriesByJourneyRefNumber.keySet().forEach(key -> {
				final ReservationData reservationData = new ReservationData();
				reservationData.setJourneyReferenceNumber(key);
				reservationDatas.add(reservationData);
			});

			reservationDataList.setReservationDatas(reservationDatas);
			setAdditionalMessages(abstractOrderModel, reservationDataList);

			for (final ReservationDataListHandler handler : handlers)
			{
				handler.handle(abstractOrderModel, transportEntriesByJourneyRefNumber, reservationDataList);
			}
		}
		return reservationDataList;
	}

	private void setAdditionalMessages(final AbstractOrderModel abstractOrderModel, final ReservationDataList reservationDataList)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			final PaymentTransactionModel transactionModel = StreamUtil.safeStream(abstractOrderModel.getPaymentTransactions())
					.filter(transaction -> !transaction.isExisting()).findFirst().orElse(null);

			if (Objects.nonNull(transactionModel))
			{
				final List<PaymentTransactionEntryModel> transactionEntries = StreamUtil.safeStream(transactionModel.getEntries())
						.filter(entry -> PaymentTransactionType.PURCHASE.equals(entry.getType())).collect(Collectors.toList());
				if ((!CollectionUtils.isEmpty(transactionEntries)))
				{
					final PaymentTransactionEntryModel transactionEntry = transactionEntries.stream().findFirst().get();
					reservationDataList.setOperatorDisplayMessage(transactionEntry.getOperatorDisplayMessage());
					reservationDataList.setApprovalOrDeclineMessage(transactionEntry.getApprovalOrDeclineMessage());
					reservationDataList.setPaymentProcessingMethod(transactionEntry.getPaymentProcessingMethod());
					reservationDataList.setSignatureRequiredFlag(transactionEntry.isSignatureRequiredFlag());
				}
			}
		}
	}

	protected List<ReservationDataListHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ReservationDataListHandler> handlers)
	{
		this.handlers = handlers;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}


}
