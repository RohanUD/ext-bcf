/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.global.manager.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.reservation.data.AccommodationReservationDataList;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.global.manager.BcfTravelPipelineManager;
import com.bcf.facades.reservation.handlers.BcfAccommodationReservationHandler;


public class AccommodationGlobalReservationPipelineManager implements BcfTravelPipelineManager
{
	private List<BcfAccommodationReservationHandler> handlers;

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> cartEntries, final BcfGlobalReservationData globalReservationData)
	{
		final List<AbstractOrderEntryModel> accommodationEntries = cartEntries.stream().filter(entry -> entry.getEntryGroup() instanceof AccommodationOrderEntryGroupModel && RoomRateProductModel._TYPECODE.equals(entry.getProduct().getItemtype())).collect(
				Collectors.toList());
		final AccommodationReservationDataList accommodationReservationDataList = executeHandlers(abstractOrderModel, accommodationEntries, false);
		globalReservationData.setAccommodationReservations(accommodationReservationDataList);

	}

	@Override
	public void executePipeline(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationDealEntries, final GlobalTravelReservationData globalTravelReservationData)
	{
		final AccommodationReservationDataList accommodationReservationDataList = executeHandlers(abstractOrderModel, accommodationDealEntries, true);
		globalTravelReservationData.setAccommodationReservationData(accommodationReservationDataList.getAccommodationReservations().get(0));
	}

	protected AccommodationReservationDataList executeHandlers(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> accommodationEntries, final boolean isDealEntry)
	{
		if(CollectionUtils.isNotEmpty(accommodationEntries))
		{
			Map<String, List<AbstractOrderEntryModel>> accommodationEntriesByCode = MapUtils.EMPTY_SORTED_MAP;
			if(accommodationEntries.stream().allMatch(entry -> entry.getEntryGroup() instanceof DealOrderEntryGroupModel))
			{
				accommodationEntriesByCode = accommodationEntries.stream().collect(Collectors.groupingBy(entry -> entry
						.getEntryGroup()
						.getPk().getLongValueAsString()));
			}
			else if(accommodationEntries.stream().allMatch(entry -> entry.getEntryGroup() instanceof AccommodationOrderEntryGroupModel))
			{
				accommodationEntriesByCode = accommodationEntries.stream().collect(Collectors.groupingBy(entry -> ((AccommodationOrderEntryGroupModel) entry
						.getEntryGroup())
						.getAccommodationOffering().getCode()));

			}

			final AccommodationReservationDataList accommodationReservationDataList = new AccommodationReservationDataList();
			final List<AccommodationReservationData> accommodationReservationDatas = new ArrayList<>();
			accommodationEntriesByCode.forEach((key, entries) -> {
				final AccommodationReservationData accommodationReservationData = new AccommodationReservationData();
				for(final BcfAccommodationReservationHandler accommodationReservationHandler : handlers)
				{
					accommodationReservationHandler.handle(abstractOrderModel, entries, accommodationReservationData, isDealEntry);
				}
				accommodationReservationDatas.add(accommodationReservationData);
			});

			accommodationReservationDataList.setAccommodationReservations(accommodationReservationDatas);
			return accommodationReservationDataList;
		}
		return null;
	}

	protected List<BcfAccommodationReservationHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<BcfAccommodationReservationHandler> handlers)
	{
		this.handlers = handlers;
	}
}
