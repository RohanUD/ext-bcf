/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reservation.order.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.facades.reservation.entry.data.BcfOrderEntryData;


public class BcfOrderEntryPopulator implements Populator<AbstractOrderEntryModel, BcfOrderEntryData>
{
	private Converter<FareProductModel, FareProductData> fareProductConverter;
	private Converter<ProductModel, ProductData> ancillaryProductConverter;
	private Converter<TravellerModel, TravellerData> travellerDataConverter;

	@Override
	public void populate(final AbstractOrderEntryModel source, final BcfOrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getProduct() instanceof FareProductModel)
		{
			target.setProduct(getFareProductConverter().convert((FareProductModel) source.getProduct()));
		}
		else if (Objects.equals(AncillaryProductModel._TYPECODE, source.getProduct().getItemtype()))
		{
			target.setProduct(getAncillaryProductConverter().convert(source.getProduct()));
		}
		if (Objects.nonNull(source.getTravelOrderEntryInfo())
				&& CollectionUtils.isNotEmpty(source.getTravelOrderEntryInfo().getTravellers()))
		{
			final TravellerModel traveller = source.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get();
			target.setTraveller(getTravellerDataConverter().convert(traveller));
		}
	}

	/**
	 * @return the fareProductConverter
	 */
	protected Converter<FareProductModel, FareProductData> getFareProductConverter()
	{
		return fareProductConverter;
	}

	/**
	 * @param fareProductConverter the fareProductConverter to set
	 */
	@Required
	public void setFareProductConverter(final Converter<FareProductModel, FareProductData> fareProductConverter)
	{
		this.fareProductConverter = fareProductConverter;
	}

	/**
	 * @return the ancillaryProductConverter
	 */
	protected Converter<ProductModel, ProductData> getAncillaryProductConverter()
	{
		return ancillaryProductConverter;
	}

	/**
	 * @param ancillaryProductConverter the ancillaryProductConverter to set
	 */
	@Required
	public void setAncillaryProductConverter(final Converter<ProductModel, ProductData> ancillaryProductConverter)
	{
		this.ancillaryProductConverter = ancillaryProductConverter;
	}

	/**
	 * @return the travellerDataConverter
	 */
	protected Converter<TravellerModel, TravellerData> getTravellerDataConverter()
	{
		return travellerDataConverter;
	}

	/**
	 * @param travellerDataConverter the travellerDataConverter to set
	 */
	@Required
	public void setTravellerDataConverter(final Converter<TravellerModel, TravellerData> travellerDataConverter)
	{
		this.travellerDataConverter = travellerDataConverter;
	}
}
