/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.utils;

import java.util.Map;
import javax.ws.rs.core.UriBuilder;


public class BcfFacadeUtil
{
   private static final String BASE_SITE_DEFAULT = "";

   private BcfFacadeUtil()
   {
      //Utility classes
   }

   public static String getRelativeSiteUrl(final String path, final Map<String, String> params, final Object... pathVariables)
   {
      final UriBuilder builder = UriBuilder
            .fromPath(BASE_SITE_DEFAULT)
            .path(path);

      params.entrySet().forEach(param -> builder.queryParam(param.getKey(), param.getValue()));

      return builder.build(pathVariables).toString();
   }

}
