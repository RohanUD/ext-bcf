/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;


public class CsvWriteUtils
{
	private static final Logger LOG = Logger.getLogger(CsvWriteUtils.class);

	private CsvWriteUtils()
	{
		//Utility classes
	}

	public static void writeToCSV(final String[] csvHeader, final String fileName, final List<List<String>> records)
	{
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(csvHeader);
		try (CSVPrinter csvFilePrinter = new CSVPrinter(new FileWriter(fileName), csvFileFormat))
		{
			for (List<String> columns : records)
			{
				csvFilePrinter.printRecord(columns);
			}
		}
		catch (IOException e)
		{
			LOG.error("Error in CSV File Write!");
			LOG.error(e.getMessage(), e);
		}
	}
}
