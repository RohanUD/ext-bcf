/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.droptrailer.impl;

import com.bcf.facades.droptrailer.DropTrailerReferenceDataIntegrationFacade;
import com.bcf.integration.droptrailer.service.DropTrailerReferenceDataIntegrationService;
import com.bcf.integration.droptrailer.trailermovement.data.EventTypesResponseData;
import com.bcf.integration.droptrailer.trailermovement.data.TrailerMovementLocationsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultDropTrailerReferenceDataIntegrationFacade implements DropTrailerReferenceDataIntegrationFacade
{

	private DropTrailerReferenceDataIntegrationService dropTrailerReferenceDataIntegrationService;

	@Override
	public TrailerMovementLocationsResponseData getTrailerMovementLocations() throws IntegrationException
	{
		return getDropTrailerReferenceDataIntegrationService().getTrailerMovementLocations();
	}

	@Override
	public EventTypesResponseData getEventTypes() throws IntegrationException
	{
		return getDropTrailerReferenceDataIntegrationService().getEventTypes();
	}


	public DropTrailerReferenceDataIntegrationService getDropTrailerReferenceDataIntegrationService()
	{
		return dropTrailerReferenceDataIntegrationService;
	}

	public void setDropTrailerReferenceDataIntegrationService(
			final DropTrailerReferenceDataIntegrationService dropTrailerReferenceDataIntegrationService)
	{
		this.dropTrailerReferenceDataIntegrationService = dropTrailerReferenceDataIntegrationService;
	}
}
