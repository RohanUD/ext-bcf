/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.droptrailer.impl;

import com.bcf.facades.droptrailer.SearchMovementsIntegrationFacade;
import com.bcf.integration.droptrailer.search.data.SearchMovementsResponseData;
import com.bcf.integration.droptrailer.service.SearchMovementsIntegrationService;
import com.bcf.integration.droptrailer.trailermovement.data.SearchMovementsRequestData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultSearchMovementsIntegrationFacade implements SearchMovementsIntegrationFacade
{

	private SearchMovementsIntegrationService searchMovementsIntegrationService;

	@Override
	public SearchMovementsResponseData searchMovements(final SearchMovementsRequestData searchMovementsRequestData)
			throws IntegrationException
	{
		return getSearchMovementsIntegrationService().searchMovements(searchMovementsRequestData);
	}


	public SearchMovementsIntegrationService getSearchMovementsIntegrationService()
	{
		return searchMovementsIntegrationService;
	}

	public void setSearchMovementsIntegrationService(
			final SearchMovementsIntegrationService searchMovementsIntegrationService)
	{
		this.searchMovementsIntegrationService = searchMovementsIntegrationService;
	}
}
