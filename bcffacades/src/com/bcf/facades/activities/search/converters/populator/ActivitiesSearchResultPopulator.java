/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.activities.search.converters.populator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.media.container.BcfMediaContainerService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;


public class ActivitiesSearchResultPopulator implements Populator<SearchResultValueData, ActivityResponseData>
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CatalogVersionService catalogVersionService;
	private BcfMediaContainerService bcfMediaContainerService;
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	public void populate(final SearchResultValueData source, final ActivityResponseData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDescription(this.getValue(source, "description"));
		target.setCode(this.getValue(source, "code"));
		target.setActivityCategoryTypes(this.<List<String>>getValue(source, "activityCategoryTypes"));
		target.setDestination(this.getValue(source, "destination"));
		target.setImages(getImageData(this.getValue(source, "galleries")));
		target.setItemtype(this.getValue(source, "itemtype"));
		target.setName(this.getValue(source, "name"));
		target.setUrl(this.getValue(source, "url"));
		target.setChildPriceValue(getValue(source, "childPriceValue"));
		target.setAdultPriceValue(getValue(source, "adultPriceValue"));
		target.setStartDate(getValue(source, "startDate"));
		target.setEndDate(getValue(source, "endDate"));
		target.setNotBookableOnlineStartDate(getValue(source, "notBoolableStatusStartDate"));
		target.setNotBookableOnlineEndDate(getValue(source, "notBookableStatusEndDate"));
		target.setBlocktypeStatus(getValue(source, "blocktypeStatus"));
	}

	protected List<ImageData> getImageData(final List<String> galleries)
	{
		return StreamUtil.safeStream(galleries).map(this::createImageData).collect(Collectors.toList());
	}

	private ImageData createImageData(final String responsiveUrl)
	{
		final ImageData imageData = new ImageData();
		imageData.setUrl(responsiveUrl);
		return imageData;
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		return (T) source.getValues().get(propertyName);
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the bcfMediaContainerService
	 */
	protected BcfMediaContainerService getBcfMediaContainerService()
	{
		return bcfMediaContainerService;
	}

	/**
	 * @param bcfMediaContainerService the bcfMediaContainerService to set
	 */
	@Required
	public void setBcfMediaContainerService(final BcfMediaContainerService bcfMediaContainerService)
	{
		this.bcfMediaContainerService = bcfMediaContainerService;
	}

	/**
	 * @return the bcfResponsiveMediaStrategy
	 */
	protected BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	/**
	 * @param bcfResponsiveMediaStrategy the bcfResponsiveMediaStrategy to set
	 */
	@Required
	public void setBcfResponsiveMediaStrategy(final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}
}
