/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.promotion.helper;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.facades.cart.AddToCartData;


public class BCFCartModelBuilder
{

   private BCFCartModelBuilder()
   {
   }

   public static BCFCartModelDraft newCart(final String code)
   {
      return (new BCFCartModelDraft()).setCode(code);
   }


   protected static AbstractOrderEntryModel getBasicAbstractOrderEntry(final CartModel cart, final AddToCartData cartData,
         final int entryNumber)
   {
      final AbstractOrderEntryModel entryModel = new CartEntryModel();
      entryModel.setProduct(cartData.getProduct());
      entryModel.setQuantity(cartData.getQuantity());
      entryModel.setBasePrice(cartData.getBasePrice());
      entryModel.setTotalPrice(cartData.getQuantity() * cartData.getBasePrice());
      entryModel.setEntryNumber(entryNumber);
      entryModel.setOrder(cart);
      entryModel.setUnit(cartData.getUnit());

      return entryModel;
   }

   public static void addHotelProduct(final CartModel cart, final AddToCartData cartData, final int entryNumber)
   {
      final AbstractOrderEntryModel entryModel = getBasicAbstractOrderEntry(cart, cartData, entryNumber);

      final DealOrderEntryGroupModel dealOrderEntryGroup = new DealOrderEntryGroupModel();
      AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = new AccommodationOrderEntryGroupModel();
      accommodationOrderEntryGroup.setStartingDate(cartData.getCheckInDate());
      accommodationOrderEntryGroup.setEndingDate(cartData.getCheckOutDate());
      dealOrderEntryGroup.setAccommodationEntryGroups(Collections.singletonList(accommodationOrderEntryGroup));
      entryModel.setEntryGroup(dealOrderEntryGroup);

      cart.getEntries().add(entryModel);
   }

   public static void addFareProduct(final CartModel cart, final AddToCartData cartData, final int entryNumber)
   {
      final AbstractOrderEntryModel entryModel = getBasicAbstractOrderEntry(cart, cartData, entryNumber);

      final TravelOrderEntryInfoModel travelOrderEntryInfo = new TravelOrderEntryInfoModel();
      travelOrderEntryInfo.setOriginDestinationRefNumber(cartData.getOriginDestinationRefNumber());
      TransportOfferingModel transportOffering = new TransportOfferingModel();
      transportOffering.setDepartureTime(cartData.getCheckInDate());
      travelOrderEntryInfo.setTransportOfferings(Collections.singletonList(transportOffering));
      travelOrderEntryInfo.setTravelRoute(cartData.getTravelRoute());
      travelOrderEntryInfo.setTravellers(Collections.singletonList(cartData.getTraveller()));
      entryModel.setTravelOrderEntryInfo(travelOrderEntryInfo);

      cart.getEntries().add(entryModel);
   }

   public static void addProduct(final CartModel cart, final ProductModel product, final long quantity,
         final double basePrice,
         final int entryNumber, final UnitModel unit)
   {
      final AbstractOrderEntryModel entryModel = new CartEntryModel();
      entryModel.setProduct(product);
      entryModel.setQuantity(quantity);
      entryModel.setBasePrice(basePrice);
      entryModel.setTotalPrice(quantity * basePrice);
      entryModel.setEntryNumber(entryNumber);
      entryModel.setOrder(cart);
      entryModel.setUnit(unit);
      cart.getEntries().add(entryModel);
   }


   public static class BCFCartModelDraft
   {
      private final CartModel cartModel = new CartModel();

      public BCFCartModelDraft()
      {
         this.cartModel.setEntries(new ArrayList());
         this.cartModel.setDiscounts(new ArrayList());
      }

      public BCFCartModelDraft setCode(final String code)
      {
         this.cartModel.setCode(code);
         return this;
      }

      public BCFCartModelDraft setCurrency(final CurrencyModel currencyModel)
      {
         this.cartModel.setCurrency(currencyModel);
         return this;
      }

      public BCFCartModelDraft setTotalPrice(final double totalPrice)
      {
         this.cartModel.setTotalPrice(totalPrice);
         return this;
      }

      public BCFCartModelDraft setDelivery(final DeliveryModeModel deliveryModeModel, final double deliveryCost)
      {
         this.cartModel.setDeliveryMode(deliveryModeModel);
         this.cartModel.setDeliveryCost(deliveryCost);
         return this;
      }

      public BCFCartModelDraft setUser(final UserModel user)
      {
         this.cartModel.setUser(user);
         return this;
      }

      public CartModel getModel()
      {
         return this.cartModel;
      }
   }


}
