/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.promotion;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengine.RuleEvaluationResult;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.facades.cart.AddToCartData;


public interface PreemptivePromotionFacade
{

   /**
    *
    * @param addToCartData
    * @param currency
    * @param user
    * @param promotionGroups
    * @return
    */
   RuleEvaluationResult getRuleEvaluationResult(List<AddToCartData> addToCartData, CurrencyModel currency, UserModel user,
         Collection<PromotionGroupModel> promotionGroups);

   /**
    *
    * @param addToCartData
    * @param currency
    * @param user
    * @param promotionGroups
    * @return
    */
   Set<AbstractRuleActionRAO> getActions(List<AddToCartData> addToCartData, CurrencyModel currency, UserModel user,
         Collection<PromotionGroupModel> promotionGroups);

   /**
    *
    * @param addToCartData
    * @param currency
    * @param user
    * @param promotionGroups
    * @return
    */
   public Pair<Double, Boolean> getPromotionResult(List<AddToCartData> addToCartData, CurrencyModel currency, UserModel user,
         Collection<PromotionGroupModel> promotionGroups);


}
