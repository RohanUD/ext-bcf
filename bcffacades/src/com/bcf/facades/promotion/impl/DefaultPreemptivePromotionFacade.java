/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.promotion.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotionengineservices.promotionengine.PromotionEngineService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengine.RuleEvaluationResult;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.facades.cart.AddToCartData;
import com.bcf.facades.promotion.PreemptivePromotionFacade;
import com.bcf.facades.promotion.helper.BCFCartModelBuilder;


public class DefaultPreemptivePromotionFacade implements PreemptivePromotionFacade
{

   private static final Logger LOG = LogManager.getLogger(DefaultPreemptivePromotionFacade.class);
   public static final String DEFAULT_RULE_ORDER_PERCENTAGE_DISCOUNT_RAOACTION = "defaultRuleOrderPercentageDiscountRAOAction";

   @Resource
   private PromotionEngineService promotionEngineService;

   @Resource
   private PriceRowService priceRowService;

   @Resource
   private ProductService productService;

   @Override
   public RuleEvaluationResult getRuleEvaluationResult(final List<AddToCartData> addToCartData, final CurrencyModel currency,
         final UserModel user, final Collection<PromotionGroupModel> promotionGroups)
   {

      final CartModel cart = BCFCartModelBuilder.newCart(UUID.randomUUID().toString()).setUser(user).setCurrency(currency)
            .getModel();

      int entryNumber = 1;
      for (final AddToCartData cartData : addToCartData)
      {

         if (cartData.getProduct() instanceof RoomRateProductModel)
         {
            BCFCartModelBuilder
                  .addHotelProduct(cart, cartData, entryNumber);
         }
         else if (cartData.getProduct() instanceof FareProductModel)
         {
            BCFCartModelBuilder
                  .addFareProduct(cart, cartData, entryNumber);
         }
         else
         {
            BCFCartModelBuilder
                  .addProduct(cart, cartData.getProduct(), cartData.getQuantity(), cartData.getBasePrice(), entryNumber,
                        cartData.getUnit());
         }

         entryNumber++;
      }

      return promotionEngineService.evaluate(cart, promotionGroups);
   }

   @Override
   public Set<AbstractRuleActionRAO> getActions(final List<AddToCartData> addToCartData, final CurrencyModel currency,
         final UserModel user, final Collection<PromotionGroupModel> promotionGroups)
   {
      final RuleEvaluationResult ruleEvaluationResult = getRuleEvaluationResult(addToCartData, currency, user, promotionGroups);
      if (ruleEvaluationResult != null && ruleEvaluationResult.getResult() != null && CollectionUtils
            .isNotEmpty(ruleEvaluationResult.getResult().getActions()))
      {
         return ruleEvaluationResult.getResult().getActions();
      }

      return Collections.EMPTY_SET;
   }

   @Override
   public Pair<Double, Boolean> getPromotionResult(final List<AddToCartData> addToCartData, final CurrencyModel currency,
         final UserModel user,
         final Collection<PromotionGroupModel> promotionGroups)
   {
      Double totalDiscount = Double.valueOf(0.0);
      boolean promotionApplied = false;

      try
      {
         final Set<AbstractRuleActionRAO> actions = getActions(addToCartData, currency, user, promotionGroups);
         if (CollectionUtils.isNotEmpty(actions))
         {
            promotionApplied = true;
            for (final AbstractRuleActionRAO action : actions)
            {
               if (action instanceof DiscountRAO)
               {
                  totalDiscount = getTotalDiscount(totalDiscount, (DiscountRAO) action);
               }
            }
         }
      }
      catch (final Exception ex)
      {
         LOG.error("Unable to calculate the total discount due to this error : {}", ex);
      }

      return Pair.of(totalDiscount, promotionApplied);
   }

   private Double getTotalDiscount(Double totalDiscount, final DiscountRAO action)
   {
      final DiscountRAO discountRAO = action;
      if (discountRAO.isPerUnit())
      {
         totalDiscount += discountRAO.getAppliedToQuantity() * discountRAO.getValue().doubleValue();
      }
      else
      {
         if (DEFAULT_RULE_ORDER_PERCENTAGE_DISCOUNT_RAOACTION
               .equalsIgnoreCase(discountRAO.getActionStrategyKey()) && discountRAO
               .getAppliedToObject() instanceof CartRAO)
         {
            final CartRAO cartRAO = (CartRAO) discountRAO.getAppliedToObject();
            totalDiscount += cartRAO.getSubTotal().subtract(cartRAO.getTotal()).doubleValue();
         }
         else
         {
            totalDiscount += discountRAO.getValue().doubleValue();
         }
      }
      return totalDiscount;
   }



}
