/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.booking.action.strategies.impl;


import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.ReservationDataList;


public class BCFOriginDestinationLevelActionStrategy
{
	private Map<ActionTypeOption, List<String>> bookingActionTypeAltMessagesMap;
	private List<OrderStatus> notAllowedOrderStatuses;
	private TimeService timeService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private List<ActionTypeOption> bookingActions;
	private List<ActionTypeOption> longRouteBookingActions;
	private BCFReservationFacade bcfReservationFacade;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private Map<String, String> bookingActionTypeUrlMap;
	private List<ActionTypeOption> globalBookingActions;

	public void applyStrategy(final ReservationDataList reservationDataList,
			final ReservationData reservationData)
	{
		final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
		final List<String> routes = Stream.of(routeType.split(",")).collect(Collectors.toList());

		for (final ReservationItemData reservationItemData : reservationData.getReservationItems())
		{
			final List<BookingActionData> bookingActionList = new ArrayList<>();
			final boolean isEligible = applyStrategy(reservationData, reservationItemData);
			if (isEligible)
			{
				// add common booking actions
				getBookingActions().forEach(bookingActionType -> {
					final BookingActionData bookingActionData = createBookingActionData(bookingActionType,
							reservationItemData.getOriginDestinationRefNumber(), reservationData);
					bookingActionList.add(bookingActionData);
					reservationItemData.setBookingActionData(bookingActionList);
					if (Objects.equals(ActionTypeOption.CANCEL_SAILING, bookingActionType))
					{
						bookingActionData.setEnabled(testCancelSailingActionEnabling(reservationDataList, reservationData));
					}
				});

				//add long route booking actions
				if (CollectionUtils.isNotEmpty(routes)
						&& routes.contains(reservationItemData.getReservationItinerary().getRoute().getRouteType()))
				{
					final List<BookingActionData> longRouteBookingActionList = new ArrayList<>();
					getLongRouteBookingActions().forEach(bookingActionType -> {
						final BookingActionData bookingActionData = createBookingActionData(bookingActionType,
								reservationItemData.getOriginDestinationRefNumber(), reservationData);
						longRouteBookingActionList.add(bookingActionData);
					});

					reservationItemData.getBookingActionData().addAll(longRouteBookingActionList);
				}
				populateGlobalBookingActionData(reservationDataList);
			}
			else
			{
				reservationItemData.setBookingActionData(Collections.emptyList());
			}
		}
	}

	/**
	 * Test cancel sailing action enabling.
	 *
	 * @param reservationData the reservation data
	 * @return true, if successful
	 */
	protected boolean testCancelSailingActionEnabling(final ReservationDataList reservationDataList,
			final ReservationData reservationData)
	{
		final int numberOfJourneys = CollectionUtils.size(reservationDataList.getReservationDatas().stream()
				.flatMap(reservation -> reservation.getReservationItems().stream()).collect(Collectors.toList()));

		if (numberOfJourneys <= 1)
		{
			return false;
		}

		if (getBcfSalesApplicationResolverService().getCurrentSalesChannel().equals(SalesApplication.CALLCENTER))
		{
			return true;
		}
		final int thresholdTimeGap = Integer.parseInt(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.CANCEL_BOOKING_RESTRICTION_TIMEGAP));
		return getBcfReservationFacade().isFirstTOMeetsDepartureTimeRestrictionGap(reservationData, thresholdTimeGap);
	}

	protected boolean applyStrategy(final ReservationData reservationData, final ReservationItemData reservationItemData)
	{
		if (applyStrategyForBookingStatusRestriction(reservationData))
		{
			return false;
		}
		else
		{
			return applyStrategyForPastLegRestriction(reservationItemData);
		}
	}

	protected boolean applyStrategyForBookingStatusRestriction(final ReservationData reservationData)
	{
		return this.getNotAllowedOrderStatuses().stream().anyMatch(status ->
				StringUtils.equalsIgnoreCase(status.getCode(), reservationData.getBookingStatusCode())
		);
	}

	@SuppressWarnings("unchecked")
	private boolean applyStrategyForPastLegRestriction(final ReservationItemData reservationItem)
	{
		final List<TransportOfferingData> transportOfferings = reservationItem.getReservationItinerary()
				.getOriginDestinationOptions().stream()
				.flatMap(odOption -> odOption.getTransportOfferings().stream()).collect(Collectors.toList());

		final Optional<TransportOfferingData> optionalTransportOffering = transportOfferings.stream()
				.sorted(Comparator.comparing(to ->
						TravelDateUtils.getUtcZonedDateTime(to.getDepartureTime(),
								to.getDepartureTimeZoneId())
				)).findFirst();
		TransportOfferingData firstTransportOffering = new TransportOfferingData();
		if (optionalTransportOffering.isPresent())
		{
			firstTransportOffering = optionalTransportOffering.get();
		}

		final boolean isAfter = TravelDateUtils.isAfter(firstTransportOffering.getDepartureTime(),
				firstTransportOffering.getDepartureTimeZoneId(), this.getTimeService().getCurrentTime(), ZoneId.systemDefault());

		final Date newDate = DateUtils.addHours(this.getTimeService().getCurrentTime(),
				Integer.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("delayTimeFromDeparture")));
		final boolean timeDelay = firstTransportOffering.getDepartureTime().getTime() > newDate.getTime();
		return isAfter && timeDelay;
	}

	protected String replaceUrlPlaceholdersWithValues(final ReservationData reservationData,
			final BookingActionData bookingActionData, final String url)
	{
		String replaceUrl = url.replaceAll("\\{orderCode}", reservationData.getCode() == null ? " " : reservationData.getCode());
		replaceUrl = replaceUrl
				.replaceAll("\\{originDestinationRefNumber}", String.valueOf(bookingActionData.getOriginDestinationRefNumber()));
		if (bookingActionData.getTraveller() != null)
		{
			replaceUrl = replaceUrl.replaceAll("\\{travellerUid}", bookingActionData.getTraveller().getUid());
		}
		replaceUrl = replaceUrl.replaceAll("\\{journeyReferenceNumber}",
				String.valueOf(reservationData.getJourneyReferenceNumber()));
		return replaceUrl;
	}

	protected BookingActionData createBookingActionData(final ActionTypeOption bookingActionType, final int odRefNo,
			final ReservationData reservationData)
	{
		final BookingActionData bookingActionData = new BookingActionData();
		bookingActionData.setActionType(bookingActionType);
		bookingActionData.setOriginDestinationRefNumber(odRefNo);
		populateUrl(bookingActionData, reservationData);
		bookingActionData.setEnabled(Boolean.TRUE);
		bookingActionData.setAlternativeMessages(Collections.emptyList());
		return bookingActionData;
	}

	protected void populateUrl(final BookingActionData bookingActionData, final ReservationData reservationData)
	{
		String url = this.getBookingActionTypeUrlMap().get(bookingActionData.getActionType().toString());
		url = this.replaceUrlPlaceholdersWithValues(reservationData, bookingActionData, url);
		bookingActionData.setActionUrl(url);
	}

	void populateGlobalBookingActionData(final ReservationDataList reservationDataList)
	{
		final List<ReservationItemData> reservationDataItems = reservationDataList.getReservationDatas().stream()
				.flatMap(reservationData -> reservationData.getReservationItems().stream()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(reservationDataItems))
		{
			return;
		}

		final List<BookingActionData> bookingActionList = new ArrayList<>();
		getGlobalBookingActions().forEach(globalAction -> {
			final List<ReservationItemData> filteredReservationDataItems = reservationDataItems.stream()
					.filter(reservationDataItem -> reservationDataItem.getBookingActionData()!=null && reservationDataItem.getBookingActionData().stream().anyMatch(bookingAction->bookingAction.getActionType().equals(globalAction))
							)
					.collect(Collectors.toList());
			if (CollectionUtils.size(filteredReservationDataItems) == CollectionUtils.size(reservationDataItems))
			{
				bookingActionList.add(filteredReservationDataItems.get(0).getBookingActionData().stream()
						.filter(action -> action.getActionType().equals(globalAction)).findAny().get());
			}
		});

		reservationDataList.setBookingActionData(bookingActionList);
	}

	protected Map<ActionTypeOption, List<String>> getBookingActionTypeAltMessagesMap()
	{
		return this.bookingActionTypeAltMessagesMap;
	}

	public void setBookingActionTypeAltMessagesMap(final Map<ActionTypeOption, List<String>> bookingActionTypeAltMessagesMap)
	{
		this.bookingActionTypeAltMessagesMap = bookingActionTypeAltMessagesMap;
	}

	protected List<OrderStatus> getNotAllowedOrderStatuses()
	{
		return notAllowedOrderStatuses;
	}

	@Required
	public void setNotAllowedOrderStatuses(final List<OrderStatus> notAllowedOrderStatuses)
	{
		this.notAllowedOrderStatuses = notAllowedOrderStatuses;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected List<ActionTypeOption> getBookingActions()
	{
		return bookingActions;
	}

	@Required
	public void setBookingActions(final List<ActionTypeOption> bookingActions)
	{
		this.bookingActions = bookingActions;
	}

	protected List<ActionTypeOption> getLongRouteBookingActions()
	{
		return longRouteBookingActions;
	}

	@Required
	public void setLongRouteBookingActions(final List<ActionTypeOption> longRouteBookingActions)
	{
		this.longRouteBookingActions = longRouteBookingActions;
	}

	/**
	 * @return the bcfReservationFacade
	 */
	protected BCFReservationFacade getBcfReservationFacade()
	{
		return bcfReservationFacade;
	}

	/**
	 * @param bcfReservationFacade the bcfReservationFacade to set
	 */
	@Required
	public void setBcfReservationFacade(final BCFReservationFacade bcfReservationFacade)
	{
		this.bcfReservationFacade = bcfReservationFacade;
	}

	/**
	 * @return the bcfSalesApplicationResolverService
	 */
	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	/**
	 * @param bcfSalesApplicationResolverService the bcfSalesApplicationResolverService to set
	 */
	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	protected Map<String, String> getBookingActionTypeUrlMap()
	{
		return this.bookingActionTypeUrlMap;
	}

	@Required
	public void setBookingActionTypeUrlMap(final Map<String, String> bookingActionTypeUrlMap)
	{
		this.bookingActionTypeUrlMap = bookingActionTypeUrlMap;
	}

	protected List<ActionTypeOption> getGlobalBookingActions()
	{
		return globalBookingActions;
	}

	@Required
	public void setGlobalBookingActions(final List<ActionTypeOption> globalBookingActions)
	{
		this.globalBookingActions = globalBookingActions;
	}
}
