/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.booking.action.strategies.impl;

import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.travelfacades.booking.action.strategies.impl.GlobalBookingLevelBookingActionStrategy;


public class BcfGlobalBookingLevelBookingActionStrategy extends GlobalBookingLevelBookingActionStrategy
{
	@Override
	public void populateUrl(final BookingActionData bookingActionData, final GlobalTravelReservationData globalReservationData)
	{
		super.populateUrl(bookingActionData, globalReservationData);

		String url = this.getGlobalBookingActionTypeUrlMap().get(bookingActionData.getActionType().toString());
		String bookingReference = "";
		if(globalReservationData.getCode()!=null){
			bookingReference = globalReservationData.getCode();
		}
		url = url.replaceAll("\\{bookingReference}", bookingReference);
		bookingActionData.setActionUrl(url);
	}
}
