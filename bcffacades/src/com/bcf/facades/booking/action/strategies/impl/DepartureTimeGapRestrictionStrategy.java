/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.booking.action.strategies.impl;

import de.hybris.platform.commercefacades.travel.BookingActionData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.travelfacades.booking.action.strategies.BookingActionEnabledEvaluatorStrategy;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;


public class DepartureTimeGapRestrictionStrategy implements BookingActionEnabledEvaluatorStrategy
{
	private static final String ALTERNATIVE_MESSAGE = "booking.action.past.legs.alternative.message";
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BCFReservationFacade bcfReservationFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void applyStrategy(final List<BookingActionData> bookingActionDataList, final ReservationData reservationData)
	{
		final List<BookingActionData> enabledBookingActions = bookingActionDataList.stream().filter(BookingActionData::isEnabled)
				.collect(Collectors.toList());
		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		if (CollectionUtils.isEmpty(enabledBookingActions) || StringUtils.equals(channel, SalesApplication.CALLCENTER.getCode()))
		{
			return;
		}

		final int thresholdTimeGap = Integer.parseInt(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.CANCEL_BOOKING_RESTRICTION_TIMEGAP));
		final boolean enabled = getBcfReservationFacade().isFirstTOMeetsDepartureTimeRestrictionGap(reservationData,
				thresholdTimeGap);

		if (!enabled)
		{
			for (final BookingActionData bookingActionData : enabledBookingActions)
			{
				bookingActionData.setEnabled(Boolean.FALSE);
				bookingActionData.getAlternativeMessages().add(ALTERNATIVE_MESSAGE);
			}
		}
	}

	/**
	 * @return the bcfSalesApplicationResolverFacade
	 */
	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	/**
	 * @param bcfSalesApplicationResolverFacade the bcfSalesApplicationResolverFacade to set
	 */
	@Required
	public void setBcfSalesApplicationResolverFacade(final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	/**
	 * @return the bcfReservationFacade
	 */
	protected BCFReservationFacade getBcfReservationFacade()
	{
		return bcfReservationFacade;
	}

	/**
	 * @param bcfReservationFacade the bcfReservationFacade to set
	 */
	@Required
	public void setbcfReservationFacade(final BCFReservationFacade bcfReservationFacade)
	{
		this.bcfReservationFacade = bcfReservationFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
