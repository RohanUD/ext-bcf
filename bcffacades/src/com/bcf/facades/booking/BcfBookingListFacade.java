/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.booking;

import de.hybris.platform.travelfacades.facades.BookingListFacade;
import java.util.List;
import com.bcf.facades.reservation.data.ReservationDataList;


public interface BcfBookingListFacade extends BookingListFacade
{

	/**
	 * Returns a list of reservation data list which corresponds to all the bookings of the current customer in session
	 *
	 * @return list of current customer's bookings
	 */
	List<ReservationDataList> getCurrentCustomerReservations();

}
