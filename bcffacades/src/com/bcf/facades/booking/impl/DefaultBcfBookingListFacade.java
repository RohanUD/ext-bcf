/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.booking.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.travelfacades.facades.impl.DefaultBookingListFacade;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.booking.BcfBookingListFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.reservation.manager.ReservationDataListPipelineManager;


public class DefaultBcfBookingListFacade extends DefaultBookingListFacade implements BcfBookingListFacade
{

	private ReservationDataListPipelineManager reservationDataListPipeLineManager;

	@Override
	public List<ReservationDataList> getCurrentCustomerReservations()
	{

		final List<OrderModel> allOrders = getCustomerOrders();
		if (CollectionUtils.isEmpty(allOrders))
		{
			return Collections.emptyList();
		}
		final List<ReservationDataList> myBookings = allOrders.stream()
				.map(orderModel -> getReservationDataListPipeLineManager().executePipeline(orderModel)).collect(Collectors.toList());

		final List<ReservationDataList> mySortedBookings = new ArrayList<>();
		for (final ReservationDataList reservationDataList : myBookings)
		{
			final ReservationDataList reservationData = new ReservationDataList();
			reservationData.setReservationDatas(sortBookings(reservationDataList.getReservationDatas()));
			mySortedBookings.add(reservationData);
		}
		return mySortedBookings;
	}

	@Override
	protected List<AccommodationReservationData> sortAccommodationBookings(List<AccommodationReservationData> myBookings)
	{
		if (CollectionUtils.isNotEmpty(myBookings))
		{
			myBookings = myBookings.stream().filter(entry -> Objects.nonNull(entry)).collect(Collectors.toList());
			return super.sortAccommodationBookings(myBookings);
		}
		return Collections.emptyList();
	}

	@Override
	protected List<GlobalTravelReservationData> sortTravelBookings(List<GlobalTravelReservationData> myBookings)
	{
		if (CollectionUtils.isNotEmpty(myBookings))
		{
			myBookings = myBookings.stream().filter(
					entry -> Objects.nonNull(entry.getAccommodationReservationData()) && Objects.nonNull(entry.getReservationData()))
					.collect(Collectors.toList());
			return super.sortTravelBookings(myBookings);
		}
		return Collections.emptyList();
	}

	protected ReservationDataListPipelineManager getReservationDataListPipeLineManager()
	{
		return reservationDataListPipeLineManager;
	}

	@Required
	public void setReservationDataListPipeLineManager(final ReservationDataListPipelineManager reservationDataListPipeLineManager)
	{
		this.reservationDataListPipeLineManager = reservationDataListPipeLineManager;
	}


}
