/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.strategies.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.packages.cart.AddDealToCartData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelfacades.packages.strategies.impl.AddStandardBundleToCartStrategy;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.vacations.GuestData;


public class BcfAddActivityBundleToCartStrategy extends AddStandardBundleToCartStrategy
{
	BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	public List<CartModificationData> addBundleToCart(final BundleTemplateModel bundleTemplate,
			final AddDealToCartData addDealToCartData) throws CommerceCartModificationException
	{
		final AddActivityToCartData data = new AddActivityToCartData();
		data.setGuestData(createGuestData(addDealToCartData.getPassengerTypes()));
		data.setActivityDate(TravelDateUtils.convertDateToStringDate(addDealToCartData.getCheckInDate(),
				BcfFacadesConstants.ACTIVITY_DATE_PATTERN));
		final List<CartModificationData> cartModificationDataList = new ArrayList<>();
		for (final ProductModel productModel : bundleTemplate.getProducts())
		{
			cartModificationDataList.addAll(getBcfTravelCartFacade()
					.createActivityOrderEntryInfo(productModel.getCode(), data));
		}
		return cartModificationDataList;
	}

	protected List<GuestData> createGuestData(final List<PassengerTypeQuantityData> passengerTypes)
	{
		final List<GuestData> guestDataList = new ArrayList<>();
		passengerTypes.forEach(passengerTypeQuantityData -> {
			final GuestData participantData = new GuestData();
			participantData.setGuestType(passengerTypeQuantityData.getPassengerType().getCode());
			participantData.setQuantity(passengerTypeQuantityData.getQuantity());
			participantData.setAge(Collections.singletonList(passengerTypeQuantityData.getAge()));
			guestDataList.add(participantData);
		});
		return guestDataList;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

}
