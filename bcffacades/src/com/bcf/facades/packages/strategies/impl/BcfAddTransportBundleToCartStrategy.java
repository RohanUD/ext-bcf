/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.strategies.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.packages.cart.AddDealToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelfacades.packages.strategies.impl.AddTransportBundleToCartStrategy;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.deal.RouteBundleTemplateModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.FareProductType;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.com.bcf.facades.cart.BcfAddToCartHandlerFacade;
import com.bcf.facades.deals.search.DealBundleDetailsFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class BcfAddTransportBundleToCartStrategy extends AddTransportBundleToCartStrategy
{
	private DealBundleDetailsFacade dealBundleDetailsFacade;
	private BcfTransportOfferingFacade bcfTransportOfferingFacade;
	private BcfAddToCartHandlerFacade bcfAddToCartHandlerFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private ProductService productService;
	private ConfigurationService configurationService;

	@Override
	public List<CartModificationData> addBundleToCart(final BundleTemplateModel bundleTemplate,
			final AddDealToCartData addDealToCartData) throws CommerceCartModificationException
	{

		final List<AddBundleToCartRequestData> addBundleToCartRequestDatas = buildAddBundleToCartRequestDatas(bundleTemplate,
				addDealToCartData);

		final List<CartModificationData> cartModificationDataList = new ArrayList<>();
		try
		{
			for (final AddBundleToCartRequestData addBundleToCartRequestData : addBundleToCartRequestDatas)
			{
				cartModificationDataList.addAll(getBcfAddToCartHandlerFacade().performAddDealToCart(addBundleToCartRequestData));
			}

			if (CollectionUtils.isNotEmpty(cartModificationDataList))
			{
				cartModificationDataList
						.forEach(modification -> getBcfTravelCartFacade()
								.setOrderEntryType(OrderEntryType.TRANSPORT, modification.getEntry().getEntryNumber()));
			}
		}
		catch (final IntegrationException e)
		{
			throw new CommerceCartModificationException("Error occurred from Integration");
		}

		return cartModificationDataList;
	}

	protected List<AddBundleToCartRequestData> buildAddBundleToCartRequestDatas(final BundleTemplateModel bundleTemplate,
			final AddDealToCartData addDealToCartData)//NOSONAR
	{
		final List<AddBundleToCartRequestData> addBundleToCartRequestDatas = new ArrayList<>();

		for (final BundleTemplateModel childBundleTemplate : bundleTemplate.getChildTemplates())
		{
			final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
			addBundleToCartRequestData.setPassengerTypes(addDealToCartData.getPassengerTypes());
			addBundleToCartRequestData.setVehicleTypes(addDealToCartData.getVehicleTypes());
			addBundleToCartRequestData.setUseEbookingIntegration(Boolean.TRUE);
			final List<AddBundleToCartData> addBundleToCartDatas = new ArrayList<>();
			final int journeyRefNum = getBcfTravelCartFacade().getNextJourneyRefNumber();
			final RouteBundleTemplateModel routeBundleTemplate = (RouteBundleTemplateModel) childBundleTemplate;
			final Integer originDestinationRefNumber = routeBundleTemplate.getOriginDestinationRefNumber();
			final List<TransportOfferingData> dealTransportOfferings = getDealBundleDetailsFacade()
					.getRouteBundleTemplateTransportOfferingDatas(routeBundleTemplate.getId());
			final String travelRouteCode = routeBundleTemplate.getTravelRoute().getCode();

			if (CollectionUtils.isNotEmpty(dealTransportOfferings))
			{
				final Map<FareProductType, String> fareProductTypeListMap = routeBundleTemplate.getFareProducts().stream()
						.collect(Collectors
								.groupingBy(FareProductModel::getFareProductType,
										Collectors.collectingAndThen(Collectors.toList(),
												fareProduct -> fareProduct.stream().findFirst().get().getCode())));

				addBundleToCartDatas.add(createBcfAddBundleToCartData(routeBundleTemplate.getId(),
						fareProductTypeListMap, dealTransportOfferings, journeyRefNum,
						originDestinationRefNumber,
						travelRouteCode));

			}
			else if (CollectionUtils.isEmpty(addDealToCartData.getItineraryPricingInfos()))
			{
				final Map<FareProductType, String> fareProductCodesByType = collectFareProducts(routeBundleTemplate);
				final TransportOfferingData transportOfferingData = getBcfTransportOfferingFacade()
						.getDefaultTransportOfferingForRoute(travelRouteCode);
				addBundleToCartDatas.add(createBcfAddBundleToCartData(childBundleTemplate.getId(), fareProductCodesByType,
						Collections.singletonList(transportOfferingData), journeyRefNum, originDestinationRefNumber, travelRouteCode));
				addBundleToCartRequestData.setUseEbookingIntegration(Boolean.FALSE);
			}
			else
			{
				final ItineraryPricingInfoData itineraryPricingInfoData = addDealToCartData.getItineraryPricingInfos()
						.get(originDestinationRefNumber);
				for (final TravelBundleTemplateData bundleData : itineraryPricingInfoData.getBundleTemplates())
				{

					final Map<FareProductType, String> fareProductsByType;
					if (CollectionUtils.isEmpty(bundleData.getFareProducts()))
					{
						fareProductsByType = collectFareProducts(routeBundleTemplate);
					}
					else
					{
						fareProductsByType = bundleData.getFareProducts().stream().collect(Collectors.
								groupingBy(fareProduct -> FareProductType.valueOf(fareProduct.getFareProductType()),
										Collectors.collectingAndThen(Collectors.toList(),
												fareProducts -> fareProducts.stream().findFirst().get().getCode())));
					}

					addBundleToCartDatas.add(createBcfAddBundleToCartData(bundleData.getFareProductBundleTemplateId(),
							fareProductsByType, bundleData.getTransportOfferings(), journeyRefNum,
							originDestinationRefNumber,
							travelRouteCode));
				}
			}
			addBundleToCartRequestData.setAddBundleToCartData(addBundleToCartDatas);
			addBundleToCartRequestDatas.add(addBundleToCartRequestData);
		}
		return addBundleToCartRequestDatas;
	}

	protected Map<FareProductType, String> collectFareProducts(final RouteBundleTemplateModel routeBundleTemplate)
	{
		final List<FareProductModel> fareProducts = new ArrayList<>();
		final FareProductModel passengerFroduct = (FareProductModel) getProductService()
				.getProductForCode(getConfigurationService().getConfiguration().getString("passenger.fareproduct"));
		fareProducts.add(passengerFroduct);

		if (Objects.nonNull(routeBundleTemplate.getVehicleType()))
		{
			final FareProductModel vehicleFareProduct = (FareProductModel) getProductService()
					.getProductForCode(getConfigurationService().getConfiguration().getString("vehicle.fareproduct"));
			fareProducts.add(vehicleFareProduct);
		}

		return fareProducts.stream().collect(Collectors
				.groupingBy(FareProductModel::getFareProductType,
						Collectors.collectingAndThen(Collectors.toList(),
								fareProduct -> fareProduct.stream().findFirst().get().getCode())));
	}

	protected BcfAddBundleToCartData createBcfAddBundleToCartData(final String bundleTemplateId,
			final Map<FareProductType, String> fareProductTypeListMap,
			final List<TransportOfferingData> transportOfferingDatas, final int journeyRefNum,
			final Integer originDestinationRefNumber,
			final String travelRouteCode)
	{
		final String passengerFareProductCode = fareProductTypeListMap.get(FareProductType.PASSENGER);
		final String vehicleFareProductCode = fareProductTypeListMap.get(FareProductType.VEHICLE);

		final BcfAddBundleToCartData addBundleToCart = new BcfAddBundleToCartData();
		addBundleToCart.setBundleTemplateId(bundleTemplateId);
		addBundleToCart.setProductCode(passengerFareProductCode);
		addBundleToCart
				.setVehicleProductCode(StringUtils.isNotBlank(vehicleFareProductCode) ? vehicleFareProductCode : StringUtils.EMPTY);
		addBundleToCart.setTransportOfferingDatas(transportOfferingDatas);
		addBundleToCart.setTransportOfferings(
				transportOfferingDatas.stream().map(TransportOfferingData::getCode)
						.collect(
								Collectors.toList()));
		addBundleToCart.setJourneyRefNumber(journeyRefNum);
		addBundleToCart.setOriginDestinationRefNumber(originDestinationRefNumber);
		addBundleToCart.setTravelRouteCode(travelRouteCode);
		return addBundleToCart;
	}

	protected DealBundleDetailsFacade getDealBundleDetailsFacade()
	{
		return dealBundleDetailsFacade;
	}

	@Required
	public void setDealBundleDetailsFacade(final DealBundleDetailsFacade dealBundleDetailsFacade)
	{
		this.dealBundleDetailsFacade = dealBundleDetailsFacade;
	}

	public BcfTransportOfferingFacade getBcfTransportOfferingFacade()
	{
		return bcfTransportOfferingFacade;
	}

	@Required
	public void setBcfTransportOfferingFacade(final BcfTransportOfferingFacade bcfTransportOfferingFacade)
	{
		this.bcfTransportOfferingFacade = bcfTransportOfferingFacade;
	}

	protected BcfAddToCartHandlerFacade getBcfAddToCartHandlerFacade()
	{
		return bcfAddToCartHandlerFacade;
	}

	@Required
	public void setBcfAddToCartHandlerFacade(final BcfAddToCartHandlerFacade bcfAddToCartHandlerFacade)
	{
		this.bcfAddToCartHandlerFacade = bcfAddToCartHandlerFacade;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
