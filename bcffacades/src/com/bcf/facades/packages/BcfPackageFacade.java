/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages;

import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.travel.PromotionSourceRuleData;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;


public interface BcfPackageFacade extends PackageFacade
{

	PackageResponseData getPackageDataByAccommodationOfferingCode(String accommodationOfferingCode, String[] roomRateProducts);

	PackageResponseData getPackageDataByAccommodationRequest(PackageRequestData packageRequestData);

	PromotionSourceRuleData getPromotionData(String promotionCode);
}
