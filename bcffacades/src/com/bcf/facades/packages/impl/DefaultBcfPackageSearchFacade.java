/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.travelfacades.facades.packages.impl.DefaultPackageSearchFacade;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.fare.sorting.strategies.PriceSortingStrategyDesc;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.packages.BcfPackageSearchFacade;
import com.bcf.facades.packages.handlers.impl.BcfPackageFareSearchResponseHandler;
import com.bcf.facades.price.calculation.BcfPriceCalculationFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfPackageSearchFacade extends DefaultPackageSearchFacade implements BcfPackageSearchFacade
{
	private static final Logger LOG = Logger.getLogger(BcfPackageFareSearchResponseHandler.class);

	private static final String EMPTY_OUTBOUND_FARE_SELECTION_DATA = "empty priced itineraries for outbound fare selection data";
	private static final String EMPTY_INBOUND_FARE_SELECTION_DATA = "empty priced itineraries for inbound fare selection data";

	private BcfFareSearchFacade bcfFareSearchFacade;
	private PriceSortingStrategyDesc priceSortingStrategyDesc;
	private BcfPriceCalculationFacade bcfPriceCalculationFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Override
	public FareSelectionData getFareSelectionForPackage(final FareSearchRequestData fareSearchRequestData)
			throws IntegrationException
	{
		final List<OriginDestinationInfoData> originDestinationInfos = fareSearchRequestData.getOriginDestinationInfo();
		//making fare search request data for outbound
		final List<OriginDestinationInfoData> outBoundOriginDestinationInfo = Collections
				.singletonList(originDestinationInfos.get(0));
		fareSearchRequestData.setOriginDestinationInfo(outBoundOriginDestinationInfo);
		final FareSelectionData outBoundFareSelectionData = getBcfFareSearchFacade()
				.performSearch(fareSearchRequestData);
		if (CollectionUtils.isEmpty(outBoundFareSelectionData.getPricedItineraries()))
		{
			LOG.error(EMPTY_OUTBOUND_FARE_SELECTION_DATA);
			return null;
		}
		//setting fareSelectionData to outBoundFareSelectionData
		final FareSelectionData fareSelectionData = outBoundFareSelectionData;
		//making fare search request data for inbound
		final List<OriginDestinationInfoData> inBoundOriginDestinationInfo = Collections
				.singletonList(originDestinationInfos.get(1));
		fareSearchRequestData.setOriginDestinationInfo(inBoundOriginDestinationInfo);
		final FareSelectionData inBoundFareSelectionData = getBcfFareSearchFacade()
				.performSearch(fareSearchRequestData);
		if (CollectionUtils.isEmpty(inBoundFareSelectionData.getPricedItineraries()))
		{
			LOG.error(EMPTY_INBOUND_FARE_SELECTION_DATA);
			return null;
		}
		//restoring original OriginDestinationInfos in request data
		fareSearchRequestData.setOriginDestinationInfo(originDestinationInfos);
		// adding inbound priced itineraries
		fareSelectionData.getPricedItineraries().addAll(inBoundFareSelectionData.getPricedItineraries());
		//filter priced itineraries
		filterPricedItineraries(fareSelectionData);
		// filtering and populated FareSelectionData with max priced itinerary for both bound journeys
		filterFareSelectionDataForMaxPrice(fareSelectionData);
		// adding margin on priced itineraries
		getBcfPriceCalculationFacade().addMarginToPricedItineraries(fareSelectionData.getPricedItineraries());
		// set max fare price map to session for cart calculation
		getBcfTravelCartFacadeHelper().setMaxFarePriceMapToSession(fareSelectionData);

		return fareSelectionData;
	}

	/**
	 * Filter priced itineraries for first bundle type and non null prices.
	 *
	 * @param fareSelectionData the fare selection data
	 */
	protected void filterPricedItineraries(final FareSelectionData fareSelectionData)
	{
		if (CollectionUtils.isEmpty(fareSelectionData.getPricedItineraries()))
		{
			return;
		}

		final List<PricedItineraryData> nonEmptyPricedItineraries = fareSelectionData.getPricedItineraries().stream()
				.filter(pricedItineraryData -> CollectionUtils.isNotEmpty(pricedItineraryData.getItineraryPricingInfos()))
				.collect(Collectors.toList());
		final PricedItineraryData firstPricedItinerary = nonEmptyPricedItineraries.stream().findFirst().get();
		final ItineraryPricingInfoData firstItineraryPricingInfo = firstPricedItinerary.getItineraryPricingInfos().stream()
				.filter(itineraryPricingInfoData -> Objects.nonNull(itineraryPricingInfoData.getTotalFare())).findFirst().get();

		final String fareBundleTypeToFilter = firstItineraryPricingInfo.getBundleType();
		for (final PricedItineraryData pricedItineraryData : nonEmptyPricedItineraries)
		{
			final List<ItineraryPricingInfoData> matchingItineraryPricingInfos = pricedItineraryData.getItineraryPricingInfos()
					.stream().filter(itineraryPricingInfoData -> Objects.nonNull(itineraryPricingInfoData.getTotalFare())).filter(
							itineraryPricingInfo -> StringUtils.equals(fareBundleTypeToFilter, itineraryPricingInfo.getBundleType()))
					.collect(Collectors.toList());
			pricedItineraryData.setItineraryPricingInfos(matchingItineraryPricingInfos);
		}
	}

	/**
	 * Filter fare selection data for max price.
	 *
	 * @param fareSelectionData the fare selection data
	 */
	protected void filterFareSelectionDataForMaxPrice(final FareSelectionData fareSelectionData)
	{
		// sorting priced itineraries based on price (price desc)
		getPriceSortingStrategyDesc().sortFareSelectionData(fareSelectionData);

		// Finding first PricedItineraryData to set based on price
		final Map<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMap = fareSelectionData.getPricedItineraries()
				.stream().collect(Collectors.groupingBy(PricedItineraryData::getOriginDestinationRefNumber));
		final List<PricedItineraryData> filteredPricedItineraries = new ArrayList<>();
		for (final Map.Entry<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMapEntry : originDestRefNoPricedItinerariesMap
				.entrySet())
		{
			final List<PricedItineraryData> pricedItineraryDatas = originDestRefNoPricedItinerariesMapEntry.getValue();
			final PricedItineraryData pricedItineraryData = pricedItineraryDatas.stream().findFirst().get();
			filteredPricedItineraries.add(pricedItineraryData);
		}
		fareSelectionData.setPricedItineraries(filteredPricedItineraries);
	}

	protected BcfFareSearchFacade getBcfFareSearchFacade()
	{
		return bcfFareSearchFacade;
	}

	@Required
	public void setBcfFareSearchFacade(final BcfFareSearchFacade bcfFareSearchFacade)
	{
		this.bcfFareSearchFacade = bcfFareSearchFacade;
	}

	protected PriceSortingStrategyDesc getPriceSortingStrategyDesc()
	{
		return priceSortingStrategyDesc;
	}

	@Required
	public void setPriceSortingStrategyDesc(final PriceSortingStrategyDesc priceSortingStrategyDesc)
	{
		this.priceSortingStrategyDesc = priceSortingStrategyDesc;
	}

	protected BcfPriceCalculationFacade getBcfPriceCalculationFacade()
	{
		return bcfPriceCalculationFacade;
	}

	@Required
	public void setBcfPriceCalculationFacade(final BcfPriceCalculationFacade bcfPriceCalculationFacade)
	{
		this.bcfPriceCalculationFacade = bcfPriceCalculationFacade;
	}

	protected BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	@Required
	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
