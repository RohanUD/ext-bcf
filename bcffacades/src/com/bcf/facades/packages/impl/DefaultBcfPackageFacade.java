/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomTypeData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.AccommodationPackageResponseData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commercefacades.travel.PromotionSourceRuleData;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.ruleengineservices.rule.dao.RuleDao;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import de.hybris.platform.travelfacades.facades.packages.impl.DefaultPackageFacade;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;
import org.apache.commons.lang.ArrayUtils;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.facades.packages.BcfPackageFacade;


public class DefaultBcfPackageFacade extends DefaultPackageFacade implements BcfPackageFacade
{

	@Resource(name = "accommodationOfferingService")
	AccommodationOfferingService accommodationOfferingService;

	@Resource(name = "bcfAccommodationService")
	BcfAccommodationService bcfAccommodationService;

	@Resource(name = "roomStaysHandler")
	private AccommodationDetailsHandler roomStaysHandler;

	@Resource(name = "accommodationDetailsBasicHandler")
	private AccommodationDetailsHandler accommodationDetailsBasicHandler;

	@Resource(name = "bcfAccommodationOfferingService")
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;

	@Resource(name = "ruleDao")
	private RuleDao ruleDao;

	@Resource(name = "promotionSourceRuleConverter")
	private Converter<PromotionSourceRuleModel, PromotionSourceRuleData> promotionSourceRuleDataConverter;

	@Override
	public PackageResponseData getPackageDataByAccommodationOfferingCode(final String accommodationOfferingCode,
			final String[] roomRateProducts)
	{
		Objects.requireNonNull(accommodationOfferingCode, "The accommodationOfferingCode cannot be null");


		final PackageResponseData packageResponseData = new PackageResponseData();

		final AccommodationPackageResponseData accommodationPackageResponseData = new AccommodationPackageResponseData();
		final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData = new AccommodationAvailabilityResponseData();

		final AccommodationAvailabilityRequestData availabilityRequestData = new AccommodationAvailabilityRequestData();

		final CriterionData criterionData = new CriterionData();
		final PropertyData accommodationReference = new PropertyData();
		accommodationReference.setAccommodationOfferingCode(accommodationOfferingCode);
		criterionData.setAccommodationReference(accommodationReference);

		final StayDateRangeData stayDateRangeData = new StayDateRangeData();
		stayDateRangeData.setStartTime(new Date());
		stayDateRangeData.setEndTime(new Date());
		criterionData.setStayDateRange(stayDateRangeData);
		availabilityRequestData.setCriterion(criterionData);

		accommodationDetailsBasicHandler.handle(availabilityRequestData, accommodationAvailabilityResponseData);
		roomStaysHandler.handle(availabilityRequestData, accommodationAvailabilityResponseData);

		final List<ReservedRoomStayData> reservedRoomStays = new ArrayList<>();

		for (final RoomStayData room : accommodationAvailabilityResponseData.getRoomStays())
		{
			final ReservedRoomStayData reservedRoomStayData = new ReservedRoomStayData();
			reservedRoomStayData.setRoomStayRefNumber(room.getRoomStayRefNumber());
			reservedRoomStays.add(reservedRoomStayData);
		}

		accommodationAvailabilityResponseData.setReservedRoomStays(reservedRoomStays);
		accommodationAvailabilityResponseData.setConfigRoomsUnavailable(Boolean.TRUE);
		accommodationPackageResponseData.setAccommodationAvailabilityResponse(accommodationAvailabilityResponseData);
		packageResponseData.setAccommodationPackageResponse(accommodationPackageResponseData);
		packageResponseData.setAvailable(true);
		populatePromotions(roomRateProducts, accommodationAvailabilityResponseData);

		return packageResponseData;
	}

	protected void populatePromotions(final String[] roomRateProducts,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		if (ArrayUtils.isNotEmpty(roomRateProducts))
		{
			final List<String> roomsOnPromotion = new ArrayList<>();

			final Set<String> roomsOnPromotionSet = new HashSet<>(Arrays.asList(roomRateProducts));

			final List<AccommodationModel> accommodations = bcfAccommodationOfferingService
					.getAccommodationsByRoomRateProducts(roomsOnPromotionSet);

			accommodations.stream().forEach(accommodation -> roomsOnPromotion.add(accommodation.getCode()));

			for (final RoomStayData room : accommodationAvailabilityResponseData.getRoomStays())
			{
				for (final RoomTypeData roomType : room.getRoomTypes())
				{
					final boolean matched = roomsOnPromotion.stream()
							.anyMatch(roomOnPromotion -> roomOnPromotion.equalsIgnoreCase(roomType.getCode()));

					if (matched)
					{
						final Collection<PromotionData> potentialPromotions = new ArrayList<>();
						final PromotionData promotion = new PromotionData();
						promotion.setEnabled(true);
						potentialPromotions.add(promotion);
						roomType.setPotentialPromotions(potentialPromotions);
					}
				}
			}

		}
	}


	@Override
	public PackageResponseData getPackageDataByAccommodationRequest(final PackageRequestData packageRequestData)
	{
		return getPackageResponse(packageRequestData);
	}

	@Override
	public PromotionSourceRuleData getPromotionData(final String promotionCode)
	{
		return promotionSourceRuleDataConverter.convert(ruleDao.findRuleByCode(promotionCode));
	}
}
