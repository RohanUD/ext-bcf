/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.manager.impl;

import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.TransportPackageResponseData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.packages.manager.impl.DefaultPackageDetailsPipelineManager;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.packages.BcfPackageSearchFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfPackageDetailsPipelineManager extends DefaultPackageDetailsPipelineManager
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfPackageDetailsPipelineManager.class);
	private static final String PACKAGE_TRANSPORT_RESPONSE_ERROR = "package.transport.response.error";
	private static final String TRANSPORT_PACKAGE_REQUEST_NULL = "transport.package.request.null";
	private static final String INVALID_FARE_SEARCH_REQUEST_DATA = "invalid fare search request data";

	private BcfPackageSearchFacade bcfPackageSearchFacade;
	private SessionService sessionService;

	@Override
	protected TransportPackageResponseData evaluateTransportPackageDetails(final PackageRequestData packageRequestData)
	{

		final TransportPackageResponseData transportPackageResponseData = new TransportPackageResponseData();

		if (Objects.isNull(packageRequestData.getTransportPackageRequest()))
		{
			transportPackageResponseData.setErrorMessage(TRANSPORT_PACKAGE_REQUEST_NULL);
			return transportPackageResponseData;
		}

		final FareSearchRequestData fareSearchRequestData = packageRequestData.getTransportPackageRequest()
				.getFareSearchRequest();
		final List<OriginDestinationInfoData> originDestinationInfos = fareSearchRequestData.getOriginDestinationInfo();
		if (CollectionUtils.isEmpty(originDestinationInfos) || CollectionUtils.size(originDestinationInfos) < 2)
		{
			LOG.error(INVALID_FARE_SEARCH_REQUEST_DATA);
			transportPackageResponseData.setErrorMessage(INVALID_FARE_SEARCH_REQUEST_DATA);
			return transportPackageResponseData;
		}

		final Object fareResultData = sessionService.getAttribute("fareSelectionData");
		if (fareResultData != null)
		{
			transportPackageResponseData.setFareSearchResponse((FareSelectionData) fareResultData);
			return transportPackageResponseData;
		}



		try
		{
			final FareSelectionData fareSelectionData = getBcfPackageSearchFacade()
					.getFareSelectionForPackage(fareSearchRequestData);
			sessionService.setAttribute("fareSelectionData", fareSelectionData);
			transportPackageResponseData.setFareSearchResponse(fareSelectionData);
		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex.getMessage());
			transportPackageResponseData.setErrorMessage(PACKAGE_TRANSPORT_RESPONSE_ERROR);
			return transportPackageResponseData;
		}
		return transportPackageResponseData;
	}

	protected BcfPackageSearchFacade getBcfPackageSearchFacade()
	{
		return bcfPackageSearchFacade;
	}

	@Required
	public void setBcfPackageSearchFacade(final BcfPackageSearchFacade bcfPackageSearchFacade)
	{
		this.bcfPackageSearchFacade = bcfPackageSearchFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
