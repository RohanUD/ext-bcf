/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.manager.impl;

import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.packages.handlers.BcfStandardPackageProductHandler;
import com.bcf.facades.packages.manager.BcfStandardPackagePipelineManager;
import com.bcf.facades.packages.response.BcfPackageProductData;


public class DefaultBcfStandardPackagePipelineManager implements BcfStandardPackagePipelineManager
{
	private List<BcfStandardPackageProductHandler> handlers;

	@Override
	public List<BcfPackageProductData> executePipeline(final BundleTemplateData bundleTemplate)
	{
		final List<BcfPackageProductData> packageProductDataList = new ArrayList<>();

		for (final BcfStandardPackageProductHandler handler : getHandlers())
		{
			handler.handle(bundleTemplate, packageProductDataList);
		}
		return packageProductDataList;
	}

	/**
	 * @return the handlers
	 */
	protected List<BcfStandardPackageProductHandler> getHandlers()
	{
		return handlers;
	}

	/**
	 * @param handlers
	 *           the handlers to set
	 */
	@Required
	public void setHandlers(final List<BcfStandardPackageProductHandler> handlers)
	{
		this.handlers = handlers;
	}

}
