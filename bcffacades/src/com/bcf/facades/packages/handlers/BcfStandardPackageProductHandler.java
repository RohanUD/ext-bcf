/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers;

import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import java.util.List;
import com.bcf.facades.packages.response.BcfPackageProductData;


public interface BcfStandardPackageProductHandler
{
	/**
	 * Handle method.
	 *
	 * @param bundleTemplate
	 *           the bundle template data
	 * @param packageProductDataList
	 *           the list of package product data list
	 */
	void handle(BundleTemplateData bundleTemplate, List<BcfPackageProductData> packageProductDataList);
}
