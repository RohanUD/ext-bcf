/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.packages.response.PackageProductData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.deals.handlers.BcfPackageResponseHandler;
import com.bcf.facades.fare.sorting.strategies.PriceSortingStrategyDesc;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackageResponseData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


public class BcfDealPackageResponsePriceHandler implements BcfPackageResponseHandler
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private CommonI18NService commonI18NService;
	private PriceSortingStrategyDesc priceSortingStrategyDesc;

	@Override
	public void handle(final BcfPackageRequestData packageRequestData, final BcfPackagesResponseData packagesResponseData)
	{
		if (packagesResponseData.isAvailable())
		{
			final String currencyIso = getCommonI18NService().getCurrentCurrency().getIsocode();
			final List<BcfPackageResponseData> bcfPackageResponseDatas = packagesResponseData.getPackageResponses().stream()
					.filter(BcfPackageResponseData.class::isInstance).map(BcfPackageResponseData.class::cast)
					.collect(Collectors.toList());
			bcfPackageResponseDatas.forEach(bcfPackageResponseData -> calculateTotalPrice(bcfPackageResponseData, currencyIso));
			packagesResponseData.setPrice(getTotalPackagePrice(bcfPackageResponseDatas, currencyIso));
		}
	}


	/**
	 * Calculate total price.
	 *
	 * @param bcfPackageResponseData
	 *           the bcf package response data
	 * @param currencyIso
	 */
	protected void calculateTotalPrice(final BcfPackageResponseData bcfPackageResponseData, String currencyIso)
	{
		Double basePrice = 0d;
		Double totalPrice = 0d;
		Double wasRate = 0d;
		final List<TaxData> taxes = new ArrayList<>();

		// Price Calculation for Transports
		final FareSelectionData fareSelectionData = bcfPackageResponseData.getTransportPackageResponse().getFareSearchResponse();
		getPriceSortingStrategyDesc().sortFareSelectionData(fareSelectionData);

		final Map<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMap = fareSelectionData.getPricedItineraries()
				.stream()
				.collect(Collectors.groupingBy(PricedItineraryData::getOriginDestinationRefNumber));
		for (final Map.Entry<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMapEntry : originDestRefNoPricedItinerariesMap
				.entrySet())
		{
			final List<PricedItineraryData> pricedItineraries = originDestRefNoPricedItinerariesMapEntry.getValue();
			final PricedItineraryData pricedItineraryData = pricedItineraries.stream().findFirst().orElse(null);
			if (Objects.nonNull(pricedItineraryData) && CollectionUtils.isNotEmpty(pricedItineraryData.getItineraryPricingInfos()))
			{
				final ItineraryPricingInfoData itineraryPricingInfo = pricedItineraryData.getItineraryPricingInfos().stream()
						.findFirst().get();
				final TotalFareData totalFare = itineraryPricingInfo.getTotalFare();
				final double price = Objects.nonNull(totalFare) ? totalFare.getTotalPrice().getValue().doubleValue() : 0d;
				basePrice = Double.sum(basePrice, totalPrice);
				totalPrice = Double.sum(totalPrice, price);
				wasRate = Double.sum(wasRate, price);
				if (Objects.nonNull(totalFare))
				{
					Optional.ofNullable(totalFare.getTaxes()).ifPresent(taxes::addAll);
				}
			}
		}
		// Price Calculation for Accommodations
		final List<RateData> accommodationRateDataList = bcfPackageResponseData.getAccommodationPackageResponse()
				.getAccommodationAvailabilityResponse().getRoomStays().stream().map(ReservedRoomStayData.class::cast)
				.map(ReservedRoomStayData::getBaseRate).collect(Collectors.toList());

		for (final RateData accommodationRateData : accommodationRateDataList)
		{
			basePrice = Double.sum(basePrice, accommodationRateData.getBasePrice().getValue().doubleValue());
			totalPrice = Double.sum(totalPrice, accommodationRateData.getActualRate().getValue().doubleValue());
			wasRate = Double.sum(wasRate, accommodationRateData.getWasRate().getValue().doubleValue());
			taxes.addAll(accommodationRateData.getTaxes());

			if (StringUtils.isBlank(currencyIso))
			{
				currencyIso = accommodationRateData.getBasePrice().getCurrencyIso();
			}
		}

		// Price Calculation for standard products/services
		final List<PackageProductData> packageProductDatas = bcfPackageResponseData.getStandardPackageResponses().stream()
				.flatMap(response -> response.getPackageProducts().stream()).collect(Collectors.toList());

		for (final PackageProductData packageProductData : packageProductDatas)
		{
			final RateData rateData = packageProductData.getPrice();
			final Integer quantity = packageProductData.getQuantity();
			basePrice = Double.sum(basePrice, rateData.getBasePrice().getValue().doubleValue() * quantity);
			totalPrice = Double.sum(totalPrice, rateData.getActualRate().getValue().doubleValue() * quantity);
			taxes.addAll(rateData.getTaxes());
		}

		bcfPackageResponseData.setPrice(createAndGetRateData(basePrice, totalPrice, wasRate, taxes, currencyIso));
	}


	protected RateData getTotalPackagePrice(final List<BcfPackageResponseData> bcfPackageResponseDatas, final String currencyIso)
	{
		Double basePrice = 0d;
		Double totalPrice = 0d;
		Double wasRate = 0d;
		final List<TaxData> taxes = new ArrayList<>();

		for (final BcfPackageResponseData bcfPackageResponseData : bcfPackageResponseDatas)
		{
			basePrice = Double.sum(basePrice, bcfPackageResponseData.getPrice().getBasePrice().getValue().doubleValue());
			totalPrice = Double.sum(totalPrice, bcfPackageResponseData.getPrice().getActualRate().getValue().doubleValue());
			wasRate = Double.sum(wasRate, bcfPackageResponseData.getPrice().getWasRate().getValue().doubleValue());
			taxes.addAll(bcfPackageResponseData.getPrice().getTaxes());
		}
		return createAndGetRateData(basePrice, totalPrice, wasRate, taxes, currencyIso);
	}

	/**
	 * Creates the and get rate data.
	 *
	 * @param basePrice
	 *           the base price
	 * @param totalPrice
	 *           the total price
	 * @param wasRate
	 *           the was rate
	 * @param taxes
	 *           the taxes
	 * @param currencyIso
	 *           the currency iso
	 * @return the rate data
	 */
	protected RateData createAndGetRateData(final double basePrice, final double totalPrice, final double wasRate,
			final List<TaxData> taxes, final String currencyIso)
	{
		final RateData rateData = new RateData();
		rateData.setBasePrice(getTravelCommercePriceFacade().createPriceData(basePrice, currencyIso));
		rateData.setActualRate(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIso));
		rateData.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate, currencyIso));
		rateData.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(wasRate - totalPrice, currencyIso));
		rateData.setTaxes(taxes);

		final TaxData totalTaxData = new TaxData();
		final Double totalTaxValue = taxes.stream().mapToDouble(tax -> tax.getPrice().getValue().doubleValue()).sum();
		totalTaxData.setPrice(getTravelCommercePriceFacade().createPriceData(totalTaxValue, currencyIso));
		rateData.setTotalTax(totalTaxData);
		return rateData;
	}

	/**
	 * @return the travelCommercePriceFacade
	 */
	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	/**
	 * @param travelCommercePriceFacade
	 *           the travelCommercePriceFacade to set
	 */
	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the priceSortingStrategyDesc
	 */
	protected PriceSortingStrategyDesc getPriceSortingStrategyDesc()
	{
		return priceSortingStrategyDesc;
	}

	/**
	 * @param priceSortingStrategyDesc
	 *           the priceSortingStrategyDesc to set
	 */
	@Required
	public void setPriceSortingStrategyDesc(final PriceSortingStrategyDesc priceSortingStrategyDesc)
	{
		this.priceSortingStrategyDesc = priceSortingStrategyDesc;
	}
}
