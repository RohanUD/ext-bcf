/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.travelfacades.facades.packages.handlers.impl.PackageAvailabilityHandler;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;


public class BcfPackageAvailabilityHandler extends PackageAvailabilityHandler
{
	@Override
	protected boolean isTransportAvailable(final PackageResponseData packageResponseData)
	{
		if (Objects.isNull(packageResponseData.getTransportPackageResponse())
				|| Objects.isNull(packageResponseData.getTransportPackageResponse().getFareSearchResponse()))
		{
			return false;
		}

		final List<PricedItineraryData> pricedItineraries = packageResponseData.getTransportPackageResponse()
				.getFareSearchResponse().getPricedItineraries();

		if (CollectionUtils.isEmpty(pricedItineraries))
		{
			return false;
		}

		final boolean isSelectedUnavailable = pricedItineraries.stream()
				.anyMatch(pricedItineraryData -> pricedItineraryData.getItineraryPricingInfos().stream().anyMatch(
						itineraryPricingInfoData -> itineraryPricingInfoData.isSelected() && !itineraryPricingInfoData.isAvailable()));

		// In case at least one of the selected options is no longer available, further evaluation of this package won't happen.
		if (isSelectedUnavailable)
		{
			return false;
		}

		return pricedItineraries.stream().anyMatch(pricedItinerary -> pricedItinerary.getItineraryPricingInfos().stream()
				.anyMatch(ItineraryPricingInfoData::isAvailable));
	}
}
