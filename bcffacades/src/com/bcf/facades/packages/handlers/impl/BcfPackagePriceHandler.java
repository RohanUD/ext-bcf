/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.travelfacades.facades.packages.handlers.impl.PackagePriceHandler;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;


public class BcfPackagePriceHandler extends PackagePriceHandler
{
	private BCFTravelCartService bcfTravelCartService;
	private CommonI18NService commerceI18NService;
	BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Override
	public void handle(
			final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		if (!CollectionUtils.isEmpty(accommodationSearchResponse.getProperties()))
		{
			final List<PackageData> packages = accommodationSearchResponse.getProperties().stream()
					.filter(PackageData.class::isInstance).map(PackageData.class::cast).collect(Collectors.toList());

			final CurrencyModel curr = commerceI18NService.getCurrentCurrency();
			final int digits = curr.getDigits().intValue();
			double amountPaid = 0d;
			final CartModel cart = bcfTravelCartService.getExistingSessionAmendedCart();
			if (bcfTravelCartService.isAmendmentCart(cart))
			{
				amountPaid = bcfAccommodationFacadeHelper.previousAccommodationAndTransportPaid(cart);
			}
			final Map<String, BigDecimal> changeFeeMap = getBcfChangeFeeCalculationService()
					.calculateChangeFee(cart, createChangeAccommodationDataList(packages, accommodationSearchRequest), true);
			for (final PackageData packageData : packages)
			{
				final BigDecimal accommodationPrice = getAccommodationPrice(packageData);
				final BigDecimal farePrice = getFarePrice(packageData);
				if (Objects.nonNull(accommodationPrice))
				{
					BigDecimal totalPrice = accommodationPrice.add(farePrice != null ? farePrice : BigDecimal.ZERO);
					totalPrice = BigDecimal.valueOf(getCommerceI18NService().roundCurrency(totalPrice.doubleValue(), digits));

					BigDecimal finalPrice = getfinalPrice(amountPaid, changeFeeMap, packageData, totalPrice);

					packageData.setTotalPackagePrice(getTravelCommercePriceFacade().createPriceData(finalPrice.doubleValue()));
				}
			}

			if(CollectionUtils.isNotEmpty(packages)){

				Comparator<PackageData> comparePropertyData= Comparator
						.comparing(PackageData::getAvailabilityStatus)
						.thenComparing(packageData -> packageData.getTotalPackagePrice().getValue());

				accommodationSearchResponse.setProperties(packages.stream().sorted(Comparator.nullsLast(comparePropertyData)).collect(Collectors.toList()));
			}

		}
	}

	private BigDecimal getfinalPrice(final double amountPaid, final Map<String, BigDecimal> changeFeeMap,
			final PackageData packageData, final BigDecimal totalPrice)
	{
		BigDecimal finalPrice = totalPrice.subtract(BigDecimal.valueOf(amountPaid));
		if (changeFeeMap != null)
		{
			finalPrice = finalPrice.add(changeFeeMap.get(packageData.getAccommodationOfferingCode()));
		}
		return finalPrice;
	}


	private List<BcfChangeAccommodationData> createChangeAccommodationDataList(final List<PackageData> packages,
			final AccommodationSearchRequestData accommodationSearchRequest)
	{

		final List<BcfChangeAccommodationData> changeAccommodationDatas = new ArrayList<>();
		BcfChangeAccommodationData changeAccommodationData = null;
		for (final PackageData packageData : packages)
		{

			changeAccommodationData = new BcfChangeAccommodationData();
			changeAccommodationData.setAccommodationOffering(packageData.getAccommodationOfferingCode());
			changeAccommodationData.setStartDate(accommodationSearchRequest.getCriterion().getStayDateRange().getStartTime());
			changeAccommodationData.setEndDate(accommodationSearchRequest.getCriterion().getStayDateRange().getEndTime());
			changeAccommodationData.setNoofRooms(accommodationSearchRequest.getCriterion().getRoomStayCandidates().size());
			changeAccommodationDatas.add(changeAccommodationData);
		}
		return changeAccommodationDatas;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}

	public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
	{
		return bcfChangeFeeCalculationService;
	}

	public void setBcfChangeFeeCalculationService(final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
	{
		this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
	}

	public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}
}
