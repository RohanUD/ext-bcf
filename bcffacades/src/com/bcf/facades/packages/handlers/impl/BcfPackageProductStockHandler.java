/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.packages.handlers.BcfStandardPackageProductHandler;
import com.bcf.facades.packages.response.BcfPackageProductData;


public class BcfPackageProductStockHandler implements BcfStandardPackageProductHandler
{
	private ProductService productService;
	private BcfProductFacade bcfProductFacade;
	private WarehouseService warehouseService;

	@Override
	public void handle(final BundleTemplateData bundleTemplate, final List<BcfPackageProductData> packageProductDataList)
	{
		packageProductDataList.forEach(packageProductData -> {
			final ProductModel product = getProductService().getProductForCode(packageProductData.getProduct().getCode());
			final WarehouseModel warehouse = getWarehouseService().getWarehouseForCode(BcfCoreConstants.DEFAULT_WAREHOUSE);
			packageProductData.getProduct().setStock(getBcfProductFacade().getStockData(product,
					packageProductData.getCommencementDate(), Collections.singleton(warehouse)));
		});
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService
	 *           the productService to set
	 */
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @return the bcfProductFacade
	 */
	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	/**
	 * @param bcfProductFacade
	 *           the bcfProductFacade to set
	 */
	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}

	/**
	 * @return the warehouseService
	 */
	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	/**
	 * @param warehouseService
	 *           the warehouseService to set
	 */
	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}
}
