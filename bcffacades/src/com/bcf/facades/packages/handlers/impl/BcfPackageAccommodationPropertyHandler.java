/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.impl.AccommodationPropertyResponseHandler;
import de.hybris.platform.travelfacades.facades.packages.manager.PackagePipelineManager;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.util.BcfRatePlanUtil;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;


public class BcfPackageAccommodationPropertyHandler extends AccommodationPropertyResponseHandler {
	private PackagePipelineManager packagePipelineManager;
	private static final String HYPHEN = "-";
	private BcfAccommodationService bcfAccommodationService;
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;


	private static final Logger LOG = Logger.getLogger(BcfPackageAccommodationPropertyHandler.class);


	public void handle(List<AccommodationOfferingDayRateData> accommodationOfferingDayRates, AccommodationSearchRequestData accommodationSearchRequest, AccommodationSearchResponseData accommodationSearchResponse) {
		List<PackageData> packages = new ArrayList();
		Map<String, List<AccommodationOfferingDayRateData>> dayRatesForMarketinginfoMap =accommodationOfferingDayRates.stream().collect(Collectors.groupingBy(accommodationOfferingDayRate->accommodationOfferingDayRate.getAccommodationOfferingCode()+HYPHEN+LocalDate.fromDateFields(accommodationOfferingDayRate.getDateOfStay()).toString()+HYPHEN+accommodationOfferingDayRate.getRoomStayCandidateRefNumber()));
		List<AccommodationOfferingDayRateData> filteredAccommodationOfferingDayRates=new ArrayList<>();
		for (Map.Entry<String, List<AccommodationOfferingDayRateData>> dayRatesForMarketinginfoMapEntry: dayRatesForMarketinginfoMap.entrySet()){

			List<AccommodationOfferingDayRateData> accommodationOfferingDayRateDatas=dayRatesForMarketinginfoMapEntry.getValue();
			accommodationOfferingDayRateDatas.removeIf(accommodationOfferingDayRateData->!checkIfEligible(accommodationOfferingDayRateData,accommodationSearchResponse.getCriterion().getRoomStayCandidates(),accommodationSearchResponse.getCriterion().getStayDateRange()));

			if(CollectionUtils.isNotEmpty(accommodationOfferingDayRateDatas))
			{
				filteredAccommodationOfferingDayRates.add(accommodationOfferingDayRateDatas.stream().min(Comparator
						.comparingDouble(accommodationOfferingDayRateData -> accommodationOfferingDayRateData.getAdultsCount())).get());

			}
		}

		Map<String, List<AccommodationOfferingDayRateData>> dayRatesForAccommodationOfferingMap = this.groupByAccommodationOfferingCode(filteredAccommodationOfferingDayRates);

		for(Map.Entry<String, List<AccommodationOfferingDayRateData>> dayRatesForAccommodationOfferingMapEntry:dayRatesForAccommodationOfferingMap.entrySet()){
			try
			{
				packages.add(this.packagePipelineManager
						.executePipeline(dayRatesForAccommodationOfferingMapEntry, accommodationSearchRequest));

			}catch(SystemException ex){

				LOG.error("Failed to add the accommodation data for offering: "+dayRatesForAccommodationOfferingMapEntry.getKey(), ex);
			}

		}

		List<PropertyData> properties = new ArrayList(packages);
		accommodationSearchResponse.setProperties(properties);
	}


	private boolean checkIfEligible(AccommodationOfferingDayRateData accommodationOfferingDayRateData,List<RoomStayCandidateData> roomStayCandidates,
			StayDateRangeData stayDateRange ){

		if(!BcfRatePlanUtil.checkIfEligibleToAdd(accommodationOfferingDayRateData,roomStayCandidates)){

			return false;
		}

		List<AccommodationModel> accommodations=accommodationOfferingDayRateData
				.getRatePlanConfigs().stream()
				.map(ratePlanConfigStr -> bcfAccommodationService
						.getAccommodation(ratePlanConfigStr.split("\\|", 3)[1])).collect(Collectors.toList());

		if(!isAccommodationsEligible(accommodationOfferingDayRateData, accommodations,stayDateRange)){

			return false;
		}

		return isExtraProductEligible(accommodationOfferingDayRateData, accommodations, roomStayCandidates);

	}

	private boolean isAccommodationsEligible(final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			List<AccommodationModel> accommodations,final StayDateRangeData stayDateRange)
	{

		if(CollectionUtils.isNotEmpty(accommodations)){

			for(AccommodationModel accommodationModel:accommodations)
			{
				if (BcfRatePlanUtil.isValidForMinimumNightsStayRule(accommodationModel.getMinimumNightsStayRules(), stayDateRange))
				{

					return true;
				}
			}
		}

		return false;
	}

	private boolean isExtraProductEligible(final AccommodationOfferingDayRateData accommodationOfferingDayRateData,
			List<AccommodationModel> accommodations,List<RoomStayCandidateData>  roomStayCandidates)
	{

		if(CollectionUtils.isNotEmpty(accommodations)){

			for(AccommodationModel accommodationModel:accommodations)
			{
				if (bcfAccommodationFacadeHelper.checkifExtraProductEligible(accommodationOfferingDayRateData, accommodationModel,roomStayCandidates))
				{
					return true;
				}
			}
		}

		return false;
	}




	protected PackagePipelineManager getPackagePipelineManager() {
		return this.packagePipelineManager;
	}

	@Required
	public void setPackagePipelineManager(PackagePipelineManager packagePipelineManager) {
		this.packagePipelineManager = packagePipelineManager;
	}

	public BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
	{
		return bcfAccommodationFacadeHelper;
	}

	public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
	{
		this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
	}
}
