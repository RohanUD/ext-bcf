/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.ExtraGuestOccupancyData;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomTypeData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.AccommodationPackageResponseData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.packages.response.TransportPackageResponseData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.FareProductData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TaxData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.packages.handlers.PackageResponseHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BcfChangeAccommodationData;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.dao.BcfRoomRateProductDao;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.BCFTravelCommercePriceService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.services.vacation.BcfChangeFeeCalculationService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.BcfPriceInfo;
import com.bcf.facades.cart.AddToCartData;
import com.bcf.facades.deals.search.BcfAccommodationCartFacade;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.promotion.PreemptivePromotionFacade;


public class BcfPackageResponsePriceHandler implements PackageResponseHandler
{
   private TravelCommercePriceFacade travelCommercePriceFacade;
   private CommonI18NService commonI18NService;
   private PreemptivePromotionFacade preemptivePromotionFacade;
   private PromotionsService promotionsService;
   private UserService userService;
   private BcfRoomRateProductDao bcfRoomRateProductDao;
   private BCFTravelCartService bcfTravelCartService;
   private BcfAccommodationCartFacade bcfAccommodationCartFacade;
   private PriceRowService priceRowService;
   private ProductService productService;
   private BcfProductService bcfProductService;
   private BCFTravelRouteService bcfTravelRouteService;
   private BCFPassengerTypeService passengerTypeService;
   private BcfChangeFeeCalculationService bcfChangeFeeCalculationService;
   private BCFTravelCommercePriceService bcfTravelCommercePriceService;
   private BcfAccommodationOfferingService bcfAccommodationOfferingService;
   private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

   @Override
   public void handle(final PackageRequestData packageRequestData, final PackageResponseData packageResponseData)
   {
      if (packageResponseData == null)
         return;
      final CurrencyModel currentCurrency = getCommonI18NService().getCurrentCurrency();
      final RateData transportRateData = calculateTotalPriceForTransport(packageResponseData.getTransportPackageResponse(),
            currentCurrency.getIsocode());

      final List<AddToCartData> additionalProductsAddToCartData = getValidAdditionalProductsAddtoCartData(packageRequestData);

      calculateAccommodationPackageDiscount(packageRequestData, packageResponseData, additionalProductsAddToCartData);

      final double extraProductsPrice = getValidAdditionalProductsPrice(additionalProductsAddToCartData, currentCurrency);
      final BcfChangeAccommodationData changeAccommodationData = createChangeAccommodationData(packageRequestData);
      final Map<String, BigDecimal> changeFeeMap = bcfChangeFeeCalculationService
            .calculateChangeFee(bcfTravelCartService.getExistingSessionAmendedCart(),
                  Arrays.asList(changeAccommodationData), true);

      groupRoomStaysAndCalculateRatePlan(packageResponseData.getAccommodationPackageResponse(), transportRateData,
            extraProductsPrice, changeFeeMap, changeAccommodationData.getAccommodationOffering(), currentCurrency.getIsocode());

   }

   private AccommodationOfferingModel getAccommodationOfferingCode(final PackageRequestData packageRequestData)
   {
      final String accommodationOfferingCode = packageRequestData.getAccommodationPackageRequest().getAccommodationSearchRequest()
            .getCriterion().getAccommodationReference().getAccommodationOfferingCode();

      return bcfAccommodationOfferingService
            .getAccommodationOffering(accommodationOfferingCode);
   }



   private BcfChangeAccommodationData createChangeAccommodationData(final PackageRequestData packageRequestData)
   {

      final CriterionData criterion = packageRequestData.getAccommodationPackageRequest().getAccommodationSearchRequest()
            .getCriterion();
      final BcfChangeAccommodationData changeAccommodationData = new BcfChangeAccommodationData();
      changeAccommodationData.setAccommodationOffering(criterion.getAccommodationReference().getAccommodationOfferingCode());
      changeAccommodationData.setStartDate(criterion.getStayDateRange().getStartTime());
      changeAccommodationData.setEndDate(criterion.getStayDateRange().getEndTime());
      changeAccommodationData.setNoofRooms(criterion.getRoomStayCandidates().size());
      return changeAccommodationData;
   }

   public void groupRoomStaysAndCalculateRatePlan(final AccommodationPackageResponseData accommodationPackageResponse,
         final RateData transportRateData,
         final double extraProductsPrice, final Map<String, BigDecimal> changeFeeMap, final String accommodationOffering,
         final String currencyIso)
   {

      BigDecimal changeFee = BigDecimal.ZERO;
      if (changeFeeMap != null)
      {
         changeFee = changeFeeMap.get(accommodationOffering);
      }

      final List<RoomStayData> roomStays = accommodationPackageResponse.getAccommodationAvailabilityResponse().getRoomStays();
      if (CollectionUtils.isNotEmpty(roomStays))
      {
         groupRoomStays(accommodationPackageResponse, roomStays);

         final List<RoomStayData> roomStayGroups = accommodationPackageResponse.getAccommodationAvailabilityResponse()
               .getRoomStayGroups();
         for (final RoomStayData roomStayData : roomStayGroups)
         {
            final List<RatePlanData> updatedRatePlans = new ArrayList<>();
            final Map<String, List<RatePlanData>> ratePlanMap = roomStayData.getRatePlans().stream()
                  .collect(Collectors.groupingBy(ratePlan -> ratePlan.getCode()));

            updateRatePlans(transportRateData, extraProductsPrice, currencyIso, changeFee, updatedRatePlans, ratePlanMap);

            roomStayData.setRatePlans(updatedRatePlans);
         }

         final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData = accommodationPackageResponse
               .getAccommodationAvailabilityResponse();

         final List<RoomStayData> roomStayGroupDatas = accommodationAvailabilityResponseData.getRoomStayGroups();
         if(CollectionUtils.isNotEmpty(roomStayGroupDatas)){
            final Comparator<Object> comparePropertyData = Comparator
                  .comparing(roomStayData-> ((RoomStayData)roomStayData).getRatePlans().iterator().next().getAvailabilityStatus()).
                        thenComparing(roomStayData->((RoomStayData)roomStayData).getRatePlans().iterator().next().getActualRate().getValue());

            accommodationAvailabilityResponseData.setRoomStayGroups(roomStayGroupDatas.stream().filter(roomStayGroupData->CollectionUtils.isNotEmpty(roomStayGroupData.getRatePlans())).sorted(Comparator.nullsLast(comparePropertyData)).collect(Collectors.toList()));
         }

         accommodationAvailabilityResponseData.setNoOfAvailableRoomTypes(Math.toIntExact(
               accommodationAvailabilityResponseData.getRoomStayGroups().stream()
                     .filter(roomStayGroup -> CollectionUtils.isNotEmpty(roomStayGroup.getRatePlans()))
                     .flatMap(roomStayGroup -> roomStayGroup.getRatePlans().stream())
                     .count()));
      }


   }

   private void updateRatePlans(final RateData transportRateData, final double extraProductsPrice, final String currencyIso,
         final BigDecimal changeFee, final List<RatePlanData> updatedRatePlans, final Map<String, List<RatePlanData>> ratePlanMap)
   {
      for (final Map.Entry<String, List<RatePlanData>> ratePlanMapEntry : ratePlanMap.entrySet())
      {
         Double actualRate = 0d;
         Double wasRate = 0D;
         Double discounts = 0D;

         final List<RatePlanData> ratePlanMapEntryValues = ratePlanMapEntry.getValue();
         for (final RatePlanData ratePlan : ratePlanMapEntryValues)
         {

            actualRate = Double.sum(actualRate, ratePlan.getActualRate().getValue().doubleValue());
            wasRate = Double.sum(wasRate, ratePlan.getWasRate().getValue().doubleValue());
            if (Objects.nonNull(ratePlan.getTotalDiscount()))
            {
               discounts = Double.sum(discounts, ratePlan.getTotalDiscount().getValue().doubleValue());
            }

         }

         double amountPaid = 0d;
         final CartModel cart = bcfTravelCartService.getExistingSessionAmendedCart();
         if (bcfTravelCartService.isAmendmentCart(cart))
         {
            amountPaid = bcfAccommodationFacadeHelper.previousAccommodationAndTransportPaid(cart);
         }



         actualRate = Double.sum(actualRate, transportRateData.getActualRate().getValue().doubleValue());
         wasRate = Double.sum(wasRate, transportRateData.getActualRate().getValue().doubleValue());

         actualRate = actualRate - amountPaid;
         wasRate = wasRate - amountPaid;

         actualRate = Double.sum(actualRate, extraProductsPrice);
         wasRate = Double.sum(wasRate, extraProductsPrice);

         actualRate = Double.sum(actualRate, changeFee.doubleValue());
         wasRate = Double.sum(wasRate, changeFee.doubleValue());

         final RatePlanData ratePlanData = ratePlanMapEntryValues.get(0);
         ratePlanData.setActualRate(getTravelCommercePriceFacade().createPriceData(actualRate, currencyIso));
         ratePlanData.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate, currencyIso));
         ratePlanData.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(discounts, currencyIso));

         updatedRatePlans.add(ratePlanData);

      }
   }

   private void groupRoomStays(final AccommodationPackageResponseData accommodationPackageResponse,
         final List<RoomStayData> roomStays)
   {
      final List<RoomStayData> updatedRoomStays = new ArrayList<>();
      final Map<String, List<RoomStayData>> roomStayMap = roomStays.stream().collect(
            Collectors.groupingBy(
                  roomStay -> CollectionUtils.isNotEmpty(roomStay.getRoomTypes()) ? roomStay.getRoomTypes().get(0).getCode() :
                        Strings.EMPTY));
      for (final Map.Entry<String, List<RoomStayData>> roomStayMapEntry : roomStayMap.entrySet())
      {
         final List<RoomStayData> roomStayEntryDatas = roomStayMapEntry.getValue();

         final List<RatePlanData> combinedRatePlans = roomStayEntryDatas.stream()
               .flatMap(roomStayEntryData -> roomStayEntryData.getRatePlans().stream()).collect(Collectors.toList());

         final RoomStayData roomStayData = roomStayEntryDatas.get(0);
         roomStayData.setRatePlans(combinedRatePlans);
         updatedRoomStays.add(roomStayData);

      }

      accommodationPackageResponse.getAccommodationAvailabilityResponse().setRoomStayGroups(updatedRoomStays);
   }


   private double getValidAdditionalProductsPrice(final List<AddToCartData> addToCartDataList,
         final CurrencyModel currentCurrency)
   {
      Double extraProductTotalPrice = 0d;

      if (CollectionUtils.isNotEmpty(addToCartDataList))
      {
         for (final AddToCartData addToCartData : addToCartDataList)
         {
            extraProductTotalPrice = Double
                  .sum(extraProductTotalPrice, addToCartData.getBasePrice() * addToCartData.getQuantity());
         }
      }
      return commonI18NService.roundCurrency(extraProductTotalPrice, currentCurrency.getDigits());
   }

   private List<AddToCartData> getValidAdditionalProductsAddtoCartData(final PackageRequestData packageRequestData)
   {

      final List<AddToCartData> addToCartDataList = new ArrayList<>();
      final CriterionData criterionData = packageRequestData.getAccommodationPackageRequest().getAccommodationSearchRequest()
            .getCriterion();
      final String accommodationOfferingCode = criterionData.getAccommodationReference().getAccommodationOfferingCode();

      final List<Pair<ProductModel, Integer>> validAdditionalProductsList = bcfAccommodationCartFacade
            .getValidAdditionalProductsList(accommodationOfferingCode, packageRequestData.getPromotionId());

      if (CollectionUtils.isNotEmpty(validAdditionalProductsList))
      {

         for (final Pair<ProductModel, Integer> additionalProduct : validAdditionalProductsList)
         {
            final AddToCartData addToCartData = new AddToCartData();
            final StayDateRangeData stayDateRangeData = packageRequestData.getAccommodationPackageRequest()
                  .getAccommodationSearchRequest().getCriterion().getStayDateRange();

            final PriceRowModel priceRowModel = priceRowService
                  .getPriceRow(additionalProduct.getLeft(), null, stayDateRangeData.getStartTime(),
                        stayDateRangeData.getEndTime());
            if (priceRowModel != null && priceRowModel.getPrice() != null)
            {
               addToCartData.setBasePrice(priceRowModel.getPrice());
            }

            addToCartData.setProduct(additionalProduct.getLeft());
            addToCartData.setQuantity(additionalProduct.getRight().longValue());
            addToCartData.setUnit(additionalProduct.getLeft().getUnit());
            addToCartDataList.add(addToCartData);
         }
      }
      return addToCartDataList;

   }


   protected List<RoomStayData> getRoomStayData(final PackageRequestData packageRequestData,
         final PackageResponseData packageResponseData)
   {
      List<RoomStayData> roomStays = new ArrayList<>();

      if (Objects.isNull(packageRequestData.getAccommodationPackageRequest()) || Objects
            .isNull(packageRequestData.getAccommodationPackageRequest()
                  .getAccommodationAvailabilityRequest()) || Objects.isNull(packageRequestData.getAccommodationPackageRequest()
            .getAccommodationAvailabilityRequest().getCriterion()))
      {
         return roomStays;
      }

      if (Objects.isNull(packageResponseData.getAccommodationPackageResponse()) || Objects
            .isNull(packageResponseData.getAccommodationPackageResponse().getAccommodationAvailabilityResponse()) || Objects
            .isNull(packageResponseData.getAccommodationPackageResponse().getAccommodationAvailabilityResponse().getRoomStays()))
      {
         return roomStays;
      }

      roomStays = packageResponseData.getAccommodationPackageResponse()
            .getAccommodationAvailabilityResponse().getRoomStays();

      if (CollectionUtils.isEmpty(roomStays))
      {
         return roomStays;
      }

      alignRoomTypeMetaData(roomStays);

      return roomStays;

   }

   protected void alignRoomTypeMetaData(final List<RoomStayData> roomStays)
   {
      // This is for metadata alignment
      final List<RoomTypeData> roomTypes = roomStays.stream().flatMap(roomStayData -> roomStayData.getRoomTypes().stream())
            .collect(Collectors.toList());
      for (final RoomTypeData roomTypeData : roomTypes)
      {
         final Collection<PromotionData> potentialPromotions = new ArrayList<>();
         final PromotionData promotion = new PromotionData();
         promotion.setEnabled(true);
         potentialPromotions.add(promotion);
         roomTypeData.setPotentialPromotions(potentialPromotions);
      }
   }

   protected void calculateAccommodationPackageDiscount(final PackageRequestData packageRequestData,
         final PackageResponseData packageResponseData,
         final List<AddToCartData> additionalProductsAddToCartData)
   {

      final List<RoomStayData> roomStays = getRoomStayData(packageRequestData, packageResponseData);

      final AccommodationOfferingModel accommodationOfferingCode = getAccommodationOfferingCode(packageRequestData);


      final List<RoomStayData> roomStayDatas = roomStays.stream()
            .filter(roomStayData -> CollectionUtils.isNotEmpty(roomStayData.getRatePlans())).collect(Collectors.toList());


      if (CollectionUtils.isNotEmpty(roomStayDatas))
      {
         final Date checkInDate = roomStays.get(0).getCheckInDate();
         final Date checkOutDate = roomStays.get(0).getCheckOutDate();

         final List<AddToCartData> paxAndVehicleAddToCartData = getPaxAndVehicleAddToCartData(packageResponseData, checkInDate,
               checkOutDate);

         final Map<RoomStayData, List<AddToCartData>> roomStayDataAddToCartDataList = getAccommodationAddToCartData(roomStayDatas,
               checkInDate,
               checkOutDate, accommodationOfferingCode);

         final List<AddToCartData> paxAndVehicleAdditionalAddToCartData = new ArrayList<>();
         paxAndVehicleAdditionalAddToCartData.addAll(paxAndVehicleAddToCartData);
         paxAndVehicleAdditionalAddToCartData.addAll(additionalProductsAddToCartData);

         populateAccommodationPromotionDiscount(roomStayDataAddToCartDataList,
               paxAndVehicleAdditionalAddToCartData);
      }
   }

   protected List<AddToCartData> getPaxAndVehicleAddToCartData(final PackageResponseData packageResponseData,
         final Date checkInDate, final Date checkOutDate)
   {
      final List<AddToCartData> paxAndVehicleAddToCartDataList = new ArrayList<>();

      if (packageResponseData.getTransportPackageResponse() != null
            && packageResponseData.getTransportPackageResponse().getFareSearchResponse() != null && CollectionUtils
            .isNotEmpty(packageResponseData.getTransportPackageResponse().getFareSearchResponse().getPricedItineraries()))
      {
         final PricedItineraryData outGoingPricedItinerary = packageResponseData.getTransportPackageResponse()
               .getFareSearchResponse().getPricedItineraries().get(0);

         PricedItineraryData returnPricedItinerary = null;

         if (packageResponseData.getTransportPackageResponse()
               .getFareSearchResponse().getPricedItineraries().size() > 1)
         {
            returnPricedItinerary = packageResponseData.getTransportPackageResponse()
                  .getFareSearchResponse().getPricedItineraries().get(1);
         }

         final List<AddToCartData> addToCartDataByOutGoingPricedItinerary = getAddToCartDataByPricedItinerary(
               outGoingPricedItinerary, false, checkInDate, checkOutDate);
         final List<AddToCartData> addToCartDataByReturnPricedItinerary = getAddToCartDataByPricedItinerary(
               returnPricedItinerary, true, checkInDate, checkOutDate);

         paxAndVehicleAddToCartDataList.addAll(addToCartDataByOutGoingPricedItinerary);
         paxAndVehicleAddToCartDataList.addAll(addToCartDataByReturnPricedItinerary);
      }

      return paxAndVehicleAddToCartDataList;
   }


   protected List<AddToCartData> getAddToCartDataByPricedItinerary(final PricedItineraryData pricedItinerary,
         final boolean isReturn, final Date checkInDate, final Date checkOutDate)
   {

      final TravelRouteModel travelRoute = bcfTravelRouteService
            .getTravelRoute(pricedItinerary.getItinerary().getRoute().getCode());

      final TravellerModel travellerModel = new TravellerModel();

      final PassengerInformationModel passengerInformationModel = new PassengerInformationModel();
      final PassengerTypeModel passengerType = passengerTypeService.getPassengerType(BcfCoreConstants.PASSENGER_TYPE_CODE_ADULT);
      passengerInformationModel.setPassengerType(passengerType);
      travellerModel.setInfo(passengerInformationModel);

      final List<AddToCartData> pricedItineraryAddToCartDataList = new ArrayList<>();
      if (CollectionUtils.isNotEmpty(pricedItinerary.getItineraryPricingInfos()))
      {
         final ItineraryPricingInfoData itineraryPricingInfoData = pricedItinerary.getItineraryPricingInfos().get(0);
         if (itineraryPricingInfoData != null)
         {
            final String bundleType = itineraryPricingInfoData.getBundleType();
            final FareProductModel paxFareProductModel = bcfProductService
                  .getFareProductForFareBasisCode(bundleType, FareProductType.PASSENGER);
            final FareProductModel vehicleFareProductModel = bcfProductService
                  .getFareProductForFareBasisCode(bundleType, FareProductType.VEHICLE);

            if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getPtcFareBreakdownDatas()))
            {
               final Optional<PTCFareBreakdownData> ptcFareBreakdownDataOptional = itineraryPricingInfoData
                     .getPtcFareBreakdownDatas().stream()
                     .filter(itineraryPricingInfo -> itineraryPricingInfo.getPassengerTypeQuantity() != null)
                     .filter(itineraryPricingInfo -> itineraryPricingInfo.getPassengerTypeQuantity().getQuantity() > 0)
                     .findFirst();

               final Optional<Integer> paxQty = itineraryPricingInfoData
                     .getPtcFareBreakdownDatas().stream()
                     .filter(itineraryPricingInfo -> itineraryPricingInfo.getPassengerTypeQuantity() != null)
                     .filter(itineraryPricingInfo -> itineraryPricingInfo.getPassengerTypeQuantity().getQuantity() > 0)
                     .findFirst()
                     .map(itineraryPricingInfo -> itineraryPricingInfo.getPassengerTypeQuantity().getQuantity());

               updatePricedItineraryAddToCartDataList(pricedItinerary, isReturn, checkInDate, checkOutDate, travelRoute,
                     travellerModel,
                     pricedItineraryAddToCartDataList, paxFareProductModel, ptcFareBreakdownDataOptional, paxQty);
            }

            populateVehicleCartData(pricedItinerary, isReturn, checkInDate, checkOutDate, travelRoute, travellerModel,
                  pricedItineraryAddToCartDataList, itineraryPricingInfoData, vehicleFareProductModel);
         }
      }

      return pricedItineraryAddToCartDataList;
   }

   private void updatePricedItineraryAddToCartDataList(final PricedItineraryData pricedItinerary, final boolean isReturn,
         final Date checkInDate, final Date checkOutDate, final TravelRouteModel travelRoute, final TravellerModel travellerModel,
         final List<AddToCartData> pricedItineraryAddToCartDataList, final FareProductModel paxFareProductModel,
         final Optional<PTCFareBreakdownData> ptcFareBreakdownDataOptional, final Optional<Integer> paxQty)
   {
      if (ptcFareBreakdownDataOptional.isPresent() && paxQty.isPresent())
      {

         for (int count = 0; count < paxQty.get(); count++)
         {
            final PTCFareBreakdownData ptcFareBreakdownData = ptcFareBreakdownDataOptional.get();

            populateFareCartData(pricedItinerary, isReturn, checkInDate, checkOutDate, travelRoute, travellerModel,
                  pricedItineraryAddToCartDataList, paxFareProductModel, ptcFareBreakdownData);
         }
      }
   }

   private void populateFareCartData(final PricedItineraryData pricedItinerary, final boolean isReturn, final Date checkInDate,
         final Date checkOutDate, final TravelRouteModel travelRoute, final TravellerModel travellerModel,
         final List<AddToCartData> pricedItineraryAddToCartDataList, final FareProductModel paxFareProductModel,
         final PTCFareBreakdownData ptcFareBreakdownData)
   {
      if (CollectionUtils.isNotEmpty(ptcFareBreakdownData.getFareInfos()))
      {
         final FareInfoData fareInfoData = ptcFareBreakdownData.getFareInfos().get(0);

         if (CollectionUtils.isNotEmpty(fareInfoData.getFareDetails()))
         {
            final FareDetailsData fareDetailsData = fareInfoData.getFareDetails().get(0);
            final FareProductData fareProduct = fareDetailsData.getFareProduct();

            final AddToCartData addToCartData = new AddToCartData();
            if (fareProduct.getPrice() != null && fareProduct.getPrice().getValue() != null)
            {
               addToCartData.setBasePrice(fareProduct.getPrice().getValue().doubleValue());
            }
            addToCartData.setProduct(paxFareProductModel);
            addToCartData.setQuantity(1);
            addToCartData.setUnit(paxFareProductModel.getUnit());
            if (isReturn)
            {
               addToCartData.setCheckInDate(checkOutDate);
               addToCartData.setCheckOutDate(checkOutDate);
               addToCartData.setReturnJourney(isReturn);
            }
            else
            {
               addToCartData.setCheckInDate(checkInDate);
               addToCartData.setCheckOutDate(checkOutDate);
            }
            addToCartData.setOriginDestinationRefNumber(pricedItinerary.getOriginDestinationRefNumber());
            addToCartData.setTravelRoute(travelRoute);
            addToCartData.setTraveller(travellerModel);

            pricedItineraryAddToCartDataList.add(addToCartData);
         }
      }
   }

   private void populateVehicleCartData(final PricedItineraryData pricedItinerary, final boolean isReturn, final Date checkInDate,
         final Date checkOutDate, final TravelRouteModel travelRoute, final TravellerModel travellerModel,
         final List<AddToCartData> pricedItineraryAddToCartDataList, final ItineraryPricingInfoData itineraryPricingInfoData,
         final FareProductModel vehicleFareProductModel)
   {
      if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()))
      {
         final VehicleFareBreakdownData vehicleFareBreakdownData = itineraryPricingInfoData
               .getVehicleFareBreakdownDatas().get(0);

         if (vehicleFareBreakdownData.getFareInfos() != null && CollectionUtils
               .isNotEmpty(vehicleFareBreakdownData.getFareInfos().getFareDetails()))
         {
            final FareDetailsData fareDetailsData = vehicleFareBreakdownData.getFareInfos().getFareDetails().get(0);
            final FareProductData vehicleFareProduct = fareDetailsData.getFareProduct();

            final AddToCartData addToCartData = new AddToCartData();
            if (vehicleFareProduct.getPrice() != null && vehicleFareProduct.getPrice().getValue() != null)
            {
               addToCartData.setBasePrice(vehicleFareProduct.getPrice().getValue().doubleValue());
            }
            addToCartData.setProduct(vehicleFareProductModel);
            addToCartData.setQuantity(1);
            addToCartData.setUnit(vehicleFareProductModel.getUnit());
            if (isReturn)
            {
               addToCartData.setCheckInDate(checkOutDate);
               addToCartData.setCheckOutDate(checkOutDate);
               addToCartData.setReturnJourney(isReturn);
            }
            else
            {
               addToCartData.setCheckInDate(checkInDate);
               addToCartData.setCheckOutDate(checkOutDate);
            }
            addToCartData.setVehicleProduct(true);
            addToCartData.setOriginDestinationRefNumber(pricedItinerary.getOriginDestinationRefNumber());
            addToCartData.setTravelRoute(travelRoute);
            addToCartData.setTraveller(travellerModel);

            pricedItineraryAddToCartDataList.add(addToCartData);
         }
      }
   }


   protected Map<RoomStayData, List<AddToCartData>> getAccommodationAddToCartData(final List<RoomStayData> roomStays,
         final Date checkInDate,
         final Date checkOutDate, final AccommodationOfferingModel accommodationOfferingCode)
   {

      final Map<RoomStayData, List<AddToCartData>> roomStayDataAddToCartDataList = new HashMap<>();
      final CurrencyModel currentCurrency = getCommonI18NService().getCurrentCurrency();


      for (final RoomStayData roomStayData : roomStays)
      {
         final List<AddToCartData> accommodationAddToCartData = new ArrayList<>();

         for (final RatePlanData ratePlanData : roomStayData.getRatePlans())
         {
            final PriceData actualRate = getTravelCommercePriceFacade()
                  .createPriceData(BigDecimal.valueOf(getCommonI18NService()
                        .roundCurrency(ratePlanData.getActualRate().getValue().doubleValue(), currentCurrency.getDigits()))
                        .doubleValue());
            ratePlanData.setActualRate(actualRate);

            final Map<String, List<RoomRateData>> roomRatesMap = ratePlanData.getRoomRates().stream()
                  .collect(Collectors.groupingBy(RoomRateData::getCode));
            if (MapUtils.isEmpty(roomRatesMap))
            {
               continue;
            }

            for (final Map.Entry<String, List<RoomRateData>> roomRatesMapEntry : roomRatesMap.entrySet())
            {
               final String roomRateProductCode = roomRatesMapEntry.getKey();
               final List<RoomRateData> roomRateDatas = roomRatesMapEntry.getValue();

               final List<ExtraGuestOccupancyData> extraOccupancies = roomRateDatas.stream()
                     .filter(roomrate -> CollectionUtils.isNotEmpty(roomrate.getExtraGuestOccupancies()))
                     .flatMap(roomrate -> roomrate.getExtraGuestOccupancies().stream()).collect(Collectors.toList());

               populateRoomRate(checkInDate, checkOutDate, accommodationOfferingCode, accommodationAddToCartData,
                     roomRateProductCode,
                     roomRateDatas);

               populateExtraGuestOccupancy(checkInDate, checkOutDate, accommodationAddToCartData, extraOccupancies);
            }
         }


         if (CollectionUtils.isNotEmpty(accommodationAddToCartData))
         {
            roomStayDataAddToCartDataList.put(roomStayData, accommodationAddToCartData);
         }
      }

      return roomStayDataAddToCartDataList;
   }

   private void populateRoomRate(final Date checkInDate, final Date checkOutDate,
         final AccommodationOfferingModel accommodationOfferingCode, final List<AddToCartData> accommodationAddToCartData,
         final String roomRateProductCode, final List<RoomRateData> roomRateDatas)
   {
      //Should be RoomRateProductService
      final RoomRateProductModel roomRateProduct = getBcfRoomRateProductDao()
            .findRoomRateProductByCode(roomRateProductCode);

      final long quantity = CollectionUtils.size(roomRateDatas);
      final RoomRateData roomRateData = roomRateDatas.stream().findAny().get();
      final RateData rateData = roomRateData.getRate();
      if (Objects.nonNull(rateData))
      {
         BcfPriceInfo price = null;

         final RatePlanModel ratePlan = roomRateProduct.getSupercategories().stream()
               .filter(RatePlanModel.class::isInstance)
               .map(RatePlanModel.class::cast).findFirst().orElse(null);

         if (Objects.nonNull(ratePlan))
         {
            price = bcfTravelCommercePriceService
                  .getBcfPriceInfo(roomRateProduct, quantity, ratePlan, accommodationOfferingCode.getLocation());
         }

         final AddToCartData addToCartData = new AddToCartData();
         if (price != null)
         {
            addToCartData.setBasePrice(price.getNetPriceVal());
         }
         addToCartData.setProduct(roomRateProduct);
         addToCartData.setQuantity(quantity);
         addToCartData.setUnit(roomRateProduct.getUnit());
         addToCartData.setCheckInDate(checkInDate);
         addToCartData.setCheckOutDate(checkOutDate);
         accommodationAddToCartData.add(addToCartData);
      }
   }

   private void populateExtraGuestOccupancy(final Date checkInDate, final Date checkOutDate,
         final List<AddToCartData> accommodationAddToCartData, final List<ExtraGuestOccupancyData> extraOccupancies)
   {
      if (CollectionUtils.isNotEmpty(extraOccupancies))
      {
         for (final ExtraGuestOccupancyData extraGuestOccupancyData : extraOccupancies)
         {
            final AddToCartData addToCartData = new AddToCartData();
            final double basePrice = extraGuestOccupancyData.getRate().getBasePrice().getValue().doubleValue();
            addToCartData.setBasePrice(basePrice);
            final ProductModel product = productService.getProductForCode(extraGuestOccupancyData.getCode());
            addToCartData.setProduct(product);
            addToCartData.setQuantity(extraGuestOccupancyData.getQuantity());
            addToCartData.setUnit(product.getUnit());
            addToCartData.setCheckInDate(checkInDate);
            addToCartData.setCheckOutDate(checkOutDate);
            accommodationAddToCartData.add(addToCartData);

         }
      }
   }



   protected void populateAccommodationPromotionDiscount(
         final Map<RoomStayData, List<AddToCartData>> roomStayDataAddToCartDataList,
         final List<AddToCartData> paxAndVehicleAdditionalAddToCartData)
   {
      final CurrencyModel currentCurrency = getCommonI18NService().getCurrentCurrency();
      final UserModel currentUser = getUserService().getCurrentUser();
      final List<PromotionGroupModel> promotionGroupModels = Collections
            .singletonList(getPromotionsService().getDefaultPromotionGroup());

      for (final Map.Entry<RoomStayData, List<AddToCartData>> roomStayDataAddToCartData : roomStayDataAddToCartDataList
            .entrySet())
      {
         final List<RatePlanData> ratePlans = roomStayDataAddToCartData.getKey().getRatePlans().stream().filter(
               ratePlanData -> Objects.nonNull(ratePlanData.getAvailableQuantity()) && ratePlanData.getAvailableQuantity() > 0)
               .collect(Collectors.toList());

         for (final RatePlanData ratePlanData : ratePlans)
         {
            final List<AddToCartData> allAddToCartDataList = new ArrayList<>();

            final Map<String, List<RoomRateData>> roomRatesMap = ratePlanData.getRoomRates().stream()
                  .collect(Collectors.groupingBy(RoomRateData::getCode));
            if (MapUtils.isEmpty(roomRatesMap))
            {
               continue;
            }

            allAddToCartDataList.addAll(roomStayDataAddToCartData.getValue());
            allAddToCartDataList.addAll(paxAndVehicleAdditionalAddToCartData);

            final Pair<Double, Boolean> promotionResultPair = getPreemptivePromotionFacade()
                  .getPromotionResult(allAddToCartDataList, currentCurrency, currentUser, promotionGroupModels);

            if (promotionResultPair.getRight())
            {
               if (promotionResultPair.getLeft() > 0)
               {
                  makeDiscountEffective(ratePlanData, currentCurrency, promotionResultPair.getLeft());
               }
            }
            else
            {
               roomStayDataAddToCartData.getKey().getRoomTypes().forEach(roomType -> roomType.setPotentialPromotions(null));
            }
         }
      }
   }

   protected void makeDiscountEffective(final RatePlanData ratePlanData,
         final CurrencyModel currentCurrency, final double newDiscount)
   {
      final PriceData promotionalDiscountPriceData = getTravelCommercePriceFacade()
            .createPriceData(newDiscount, currentCurrency.getIsocode());
      PriceData discountPriceData = promotionalDiscountPriceData;
      final PriceData existingDiscount = ratePlanData.getTotalDiscount();
      if (Objects.nonNull(existingDiscount))
      {
         final BigDecimal totalDiscount = existingDiscount.getValue().add(BigDecimal.valueOf(newDiscount));
         discountPriceData = getTravelCommercePriceFacade()
               .createPriceData(totalDiscount.doubleValue(), currentCurrency.getIsocode());
      }
      ratePlanData.setTotalDiscount(discountPriceData);
      ratePlanData.setPromotionalDiscount(promotionalDiscountPriceData);

      final PriceData existingActualRate = ratePlanData.getActualRate();
      final BigDecimal newActualRate = existingActualRate.getValue().subtract(BigDecimal.valueOf(newDiscount));
      final PriceData newActualRatePrice = getTravelCommercePriceFacade()
            .createPriceData(newActualRate.doubleValue(), currentCurrency.getIsocode());
      ratePlanData.setActualRate(newActualRatePrice);
   }

   /**
    * Calculate total price.
    *
    * @param transportPackageResponseData the bcf package response data
    * @param currencyIso
    */

   protected RateData calculateTotalPriceForTransport(final TransportPackageResponseData transportPackageResponseData,
         final String currencyIso)
   {
      if (transportPackageResponseData.getFareSearchResponse() == null)
      {

         final PriceData defaultPrice = new PriceData();
         defaultPrice.setValue(BigDecimal.valueOf(0.0));

         final RateData transportRateData = new RateData();
         transportRateData.setBasePrice(defaultPrice);
         transportRateData.setActualRate(defaultPrice);
         transportRateData.setWasRate(defaultPrice);
         transportRateData.setTaxes(new ArrayList<>());
         transportRateData.setTotalDiscount(defaultPrice);

         return transportRateData;
      }

      Double basePrice = 0d;
      Double totalPrice = 0d;
      Double wasRate = 0d;
      final List<TaxData> taxes = new ArrayList<>();

      final FareSelectionData fareSelectionData = transportPackageResponseData.getFareSearchResponse();
      final Map<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMap = fareSelectionData
            .getPricedItineraries()
            .stream().collect(Collectors.groupingBy(PricedItineraryData::getOriginDestinationRefNumber));
      for (final Map.Entry<Integer, List<PricedItineraryData>> originDestRefNoPricedItinerariesMapEntry : originDestRefNoPricedItinerariesMap
            .entrySet())
      {
         final List<PricedItineraryData> pricedItineraries = originDestRefNoPricedItinerariesMapEntry.getValue();
         final PricedItineraryData pricedItineraryData = pricedItineraries.stream().findFirst().orElse(null);
         if (Objects.nonNull(pricedItineraryData) && CollectionUtils
               .isNotEmpty(pricedItineraryData.getItineraryPricingInfos()))
         {
            final ItineraryPricingInfoData itineraryPricingInfo = pricedItineraryData.getItineraryPricingInfos().stream()
                  .findFirst().get();
            final TotalFareData totalFare = itineraryPricingInfo.getTotalFare();
            if (Objects.nonNull(totalFare))
            {
               final double price = Objects.nonNull(totalFare) ? totalFare.getTotalPrice().getValue().doubleValue() : 0d;
               basePrice = Double.sum(basePrice, price);
               totalPrice = Double.sum(totalPrice, price);
               wasRate = Double.sum(wasRate, price);
               Optional.ofNullable(totalFare.getTaxes()).ifPresent(taxes::addAll);
            }
         }
      }
      final RateData transportRateData = new RateData();
      transportRateData.setBasePrice(getTravelCommercePriceFacade().createPriceData(basePrice, currencyIso));
      transportRateData.setActualRate(getTravelCommercePriceFacade().createPriceData(totalPrice, currencyIso));
      transportRateData.setWasRate(getTravelCommercePriceFacade().createPriceData(wasRate, currencyIso));
      transportRateData.setTotalDiscount(getTravelCommercePriceFacade().createPriceData(wasRate - totalPrice, currencyIso));
      transportRateData.setTaxes(taxes);

      final TaxData totalTaxData = new TaxData();
      final Double totalTaxValue = taxes.stream().mapToDouble(tax -> tax.getPrice().getValue().doubleValue()).sum();
      totalTaxData.setPrice(getTravelCommercePriceFacade().createPriceData(totalTaxValue, currencyIso));
      transportRateData.setTotalTax(totalTaxData);
      return transportRateData;
   }

   protected TravelCommercePriceFacade getTravelCommercePriceFacade()
   {
      return travelCommercePriceFacade;
   }

   @Required
   public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
   {
      this.travelCommercePriceFacade = travelCommercePriceFacade;
   }

   protected CommonI18NService getCommonI18NService()
   {
      return commonI18NService;
   }

   @Required
   public void setCommonI18NService(final CommonI18NService commonI18NService)
   {
      this.commonI18NService = commonI18NService;
   }

   public PreemptivePromotionFacade getPreemptivePromotionFacade()
   {
      return preemptivePromotionFacade;
   }

   @Required
   public void setPreemptivePromotionFacade(final PreemptivePromotionFacade preemptivePromotionFacade)
   {
      this.preemptivePromotionFacade = preemptivePromotionFacade;
   }

   protected PromotionsService getPromotionsService()
   {
      return promotionsService;
   }

   @Required
   public void setPromotionsService(final PromotionsService promotionsService)
   {
      this.promotionsService = promotionsService;
   }

   protected UserService getUserService()
   {
      return userService;
   }

   @Required
   public void setUserService(final UserService userService)
   {
      this.userService = userService;
   }

   protected BcfRoomRateProductDao getBcfRoomRateProductDao()
   {
      return bcfRoomRateProductDao;
   }

   @Required
   public void setBcfRoomRateProductDao(final BcfRoomRateProductDao bcfRoomRateProductDao)
   {
      this.bcfRoomRateProductDao = bcfRoomRateProductDao;
   }

   protected BCFTravelCartService getBcfTravelCartService()
   {
      return bcfTravelCartService;
   }

   @Required
   public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
   {
      this.bcfTravelCartService = bcfTravelCartService;
   }

   public BcfAccommodationCartFacade getBcfAccommodationCartFacade()
   {
      return bcfAccommodationCartFacade;
   }

   public void setBcfAccommodationCartFacade(final BcfAccommodationCartFacade bcfAccommodationCartFacade)
   {
      this.bcfAccommodationCartFacade = bcfAccommodationCartFacade;
   }

   public PriceRowService getPriceRowService()
   {
      return priceRowService;
   }

   public void setPriceRowService(final PriceRowService priceRowService)
   {
      this.priceRowService = priceRowService;
   }

   public ProductService getProductService()
   {
      return productService;
   }

   public void setProductService(final ProductService productService)
   {
      this.productService = productService;
   }

   public BcfProductService getBcfProductService()
   {
      return bcfProductService;
   }

   @Required
   public void setBcfProductService(final BcfProductService bcfProductService)
   {
      this.bcfProductService = bcfProductService;
   }

   public BCFTravelRouteService getBcfTravelRouteService()
   {
      return bcfTravelRouteService;
   }

   @Required
   public void setBcfTravelRouteService(final BCFTravelRouteService bcfTravelRouteService)
   {
      this.bcfTravelRouteService = bcfTravelRouteService;
   }

   public BCFPassengerTypeService getPassengerTypeService()
   {
      return passengerTypeService;
   }

   @Required
   public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
   {
      this.passengerTypeService = passengerTypeService;
   }

   public BcfChangeFeeCalculationService getBcfChangeFeeCalculationService()
   {
      return bcfChangeFeeCalculationService;
   }

   public void setBcfChangeFeeCalculationService(final BcfChangeFeeCalculationService bcfChangeFeeCalculationService)
   {
      this.bcfChangeFeeCalculationService = bcfChangeFeeCalculationService;
   }

   public BCFTravelCommercePriceService getBcfTravelCommercePriceService()
   {
      return bcfTravelCommercePriceService;
   }

   @Required
   public void setBcfTravelCommercePriceService(final BCFTravelCommercePriceService bcfTravelCommercePriceService)
   {
      this.bcfTravelCommercePriceService = bcfTravelCommercePriceService;
   }

   public BcfAccommodationOfferingService getBcfAccommodationOfferingService()
   {
      return bcfAccommodationOfferingService;
   }

   @Required
   public void setBcfAccommodationOfferingService(
         final BcfAccommodationOfferingService bcfAccommodationOfferingService)
   {
      this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
   }

   public BcfAccommodationFacadeHelper getBcfAccommodationFacadeHelper()
   {
      return bcfAccommodationFacadeHelper;
   }

   public void setBcfAccommodationFacadeHelper(final BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper)
   {
      this.bcfAccommodationFacadeHelper = bcfAccommodationFacadeHelper;
   }
}
