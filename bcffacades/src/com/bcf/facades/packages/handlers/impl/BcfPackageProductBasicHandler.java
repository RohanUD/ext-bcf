/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.packages.handlers.BcfStandardPackageProductHandler;
import com.bcf.facades.packages.response.BcfPackageProductData;


public class BcfPackageProductBasicHandler implements BcfStandardPackageProductHandler
{
	private BundleTemplateService bundleTemplateService;
	private Converter<PassengerTypeModel, PassengerTypeData> passengerTypeConverter;

	@Override
	public void handle(final BundleTemplateData bundleTemplate, final List<BcfPackageProductData> packageProductDataList)
	{
		final Map<PassengerTypeData, Integer> guestTypeAndQuantityMap = getGuestTypeAndQuantityMap(bundleTemplate.getId());

		if (MapUtils.isEmpty(guestTypeAndQuantityMap))
		{
			return;
		}

		final List<BcfPackageProductData> newPackageProducts = new ArrayList<>();
		guestTypeAndQuantityMap.forEach((passengerType, quantity) -> {
			final List<BcfPackageProductData> packageProductsPerGuestType = bundleTemplate.getProducts().stream()
					.map(productData -> {
						final BcfPackageProductData packageProductData = new BcfPackageProductData();
						packageProductData.setProduct(productData);
						packageProductData.setQuantity(quantity);
						packageProductData.setPassengerType(passengerType);
						packageProductData.setCommencementDate(bundleTemplate.getCommencementDate());
						return packageProductData;
					}).collect(Collectors.toList());
			newPackageProducts.addAll(packageProductsPerGuestType);
		});

		packageProductDataList.addAll(newPackageProducts);
	}

	/**
	 * Gets the guest type and quantity map.
	 *
	 * @param bundleTemplateId
	 *           the bundle template id
	 * @return the guest type and quantity map
	 */
	protected Map<PassengerTypeData, Integer> getGuestTypeAndQuantityMap(final String bundleTemplateId)
	{
		final BundleTemplateModel standardBundleTemplate = getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
		final BundleTemplateModel rootBundleTemplate = getBundleTemplateService().getRootBundleTemplate(standardBundleTemplate);

		if (!(rootBundleTemplate instanceof DealBundleTemplateModel))
		{
			return MapUtils.EMPTY_MAP;
		}
		final DealBundleTemplateModel dealBundleTemplate = (DealBundleTemplateModel) rootBundleTemplate;

		return dealBundleTemplate.getGuestCounts().stream().collect(
				Collectors.groupingBy(guestCount -> getPassengerTypeConverter().convert(guestCount.getPassengerType()),
						Collectors.summingInt(GuestCountModel::getQuantity)));
	}

	/**
	 * @return the bundleTemplateService
	 */
	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	/**
	 * @return the passengerTypeConverter
	 */
	protected Converter<PassengerTypeModel, PassengerTypeData> getPassengerTypeConverter()
	{
		return passengerTypeConverter;
	}

	/**
	 * @param passengerTypeConverter
	 *           the passengerTypeConverter to set
	 */
	@Required
	public void setPassengerTypeConverter(final Converter<PassengerTypeModel, PassengerTypeData> passengerTypeConverter)
	{
		this.passengerTypeConverter = passengerTypeConverter;
	}

}
