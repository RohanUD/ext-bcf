/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;


public class MergeRatePlansHandler implements AccommodationDetailsHandler
{
	@Override
	public void handle(final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		for (final RoomStayData roomStayData : accommodationAvailabilityResponseData.getRoomStays())
		{
			final List<RatePlanData> ratePlanDatas = roomStayData.getRatePlans();

			if (CollectionUtils.size(ratePlanDatas) > 1)
			{
				final List<RatePlanData> mergedRatePlanDatas = mergeRatePlanDatas(ratePlanDatas);
				roomStayData.setRatePlans(mergedRatePlanDatas);
			}
		}
	}

	protected List<RatePlanData> mergeRatePlanDatas(final List<RatePlanData> ratePlanDatas)
	{
		final RatePlanData anyRatePlanData = ratePlanDatas.stream().findAny().get();
		final List<RoomRateData> roomRates = ratePlanDatas.stream().flatMap(ratePlanData -> ratePlanData.getRoomRates().stream())
				.collect(Collectors.toList());

		final RatePlanData ratePlanData = new RatePlanData();
		ratePlanData.setCode(anyRatePlanData.getCode());
		ratePlanData.setMaxLengthOfStay(anyRatePlanData.getMaxLengthOfStay());
		ratePlanData.setMinLengthOfStay(anyRatePlanData.getMinLengthOfStay());
		ratePlanData.setDescription(anyRatePlanData.getDescription());
		ratePlanData.setName(anyRatePlanData.getName());
		ratePlanData.setOccupancies(anyRatePlanData.getOccupancies());
		ratePlanData.setRoomRates(roomRates);

		return Collections.singletonList(ratePlanData);
	}
}
