/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.AccommodationSearchHandler;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;


public class PerPersonPackagePriceHandler implements AccommodationSearchHandler
{
	private static final String[] DEFAULT_INCLUDED_GUEST_TYPES =
	{ "adult", "child" };
	private static final String JOURNEY_REF_NUMBER = "journeyRefNum";
	private static final String COMMA = ",";

	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BCFTravelCartService bcfTravelCartService;
	private SessionService sessionService;

	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		final String propertyValue = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfFacadesConstants.GUEST_TYPES_FOR_PERPERSON_PRICE);
		final String[] includedGuestTypes = Objects.isNull(propertyValue) ? DEFAULT_INCLUDED_GUEST_TYPES
				: StringUtils.split(propertyValue, COMMA);
		final List<String> includedGuestTypeCodes = Arrays.asList(includedGuestTypes);

		final int noOfPersons = accommodationSearchResponse.getCriterion().getRoomStayCandidates().stream()
				.flatMap(roomStayCandidateData -> roomStayCandidateData.getPassengerTypeQuantityList().stream())
				.filter(passengerTypeQuantityData -> includedGuestTypeCodes
						.contains(passengerTypeQuantityData.getPassengerType().getCode()))
				.mapToInt(PassengerTypeQuantityData::getQuantity).sum();

		final List<PackageData> packages = accommodationSearchResponse.getProperties().stream()
				.filter(PackageData.class::isInstance).map(PackageData.class::cast).collect(Collectors.toList());

		int prevNoOfGuests = 0;
		final CartModel cart = getBcfTravelCartService().getExistingSessionAmendedCart();

		if (getBcfTravelCartService().isAmendmentCart(cart))
		{
			prevNoOfGuests = getBcfTravelCartService().getNumberOfGuestsInCart(cart,
					getSessionService().getAttribute(JOURNEY_REF_NUMBER));
		}
		final int finalNoOfPersons = noOfPersons == 0 ? 1 : noOfPersons;
		for (final PackageData packageData : packages)
		{
			if (Objects.nonNull(packageData.getTotalPackagePrice()))
			{
				final BigDecimal perPersonPrice = packageData.getTotalPackagePrice().getValue()
						.divide(
								BigDecimal.valueOf(Math
										.abs(finalNoOfPersons == prevNoOfGuests ? finalNoOfPersons : finalNoOfPersons - prevNoOfGuests)),
								2, RoundingMode.HALF_EVEN);
				packageData.setPerPersonPackagePrice(getTravelCommercePriceFacade().createPriceData(perPersonPrice.doubleValue()));
			}
		}
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
