/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.AccommodationPackageResponseData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.packages.handlers.PackageResponseHandler;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;


public class PackagePerPersonPriceHandler implements PackageResponseHandler
{
	private static final String[] DEFAULT_INCLUDED_GUEST_TYPES =
			{ "adult", "child" };
	private static final String JOURNEY_REF_NUMBER = "journeyRefNum";
	private static final String COMMA = ",";

	private TravelCommercePriceFacade travelCommercePriceFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private SessionService sessionService;
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public void handle(final PackageRequestData packageRequestData, final PackageResponseData packageResponseData)
	{
		final String propertyValue = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfFacadesConstants.GUEST_TYPES_FOR_PERPERSON_PRICE);
		final String[] includedGuestTypes = Objects.isNull(propertyValue) ? DEFAULT_INCLUDED_GUEST_TYPES
				: StringUtils.split(propertyValue, COMMA);
		final List<String> includedGuestTypeCodes = Arrays.asList(includedGuestTypes);

		int noOfPersons = packageRequestData.getAccommodationPackageRequest().getAccommodationSearchRequest().getCriterion()
				.getRoomStayCandidates().stream()
				.flatMap(roomStayCandidateData -> roomStayCandidateData.getPassengerTypeQuantityList().stream())
				.filter(passengerTypeQuantityData -> includedGuestTypeCodes
						.contains(passengerTypeQuantityData.getPassengerType().getCode()))
				.mapToInt(PassengerTypeQuantityData::getQuantity).sum();

		int prevNoOfGuests = 0;
		final CartModel cart = getBcfTravelCartService().getExistingSessionAmendedCart();
		if (getBcfTravelCartService().isAmendmentCart(cart))
		{
			prevNoOfGuests = getBcfTravelCartService().getNumberOfGuestsInCart(cart,
					getSessionService().getAttribute(JOURNEY_REF_NUMBER));
		}

		noOfPersons = (noOfPersons == prevNoOfGuests) ? noOfPersons : (noOfPersons - prevNoOfGuests);
		addPerPersonPriceToAccommodation(packageResponseData.getAccommodationPackageResponse(), noOfPersons, prevNoOfGuests);
	}

	protected void addPerPersonPriceToAccommodation(final AccommodationPackageResponseData accommodationPackageResponse,
			final int noOfPersons, int prevNoOfGuests)
	{
		final List<RoomStayData> roomStays = accommodationPackageResponse.getAccommodationAvailabilityResponse().getRoomStays();
		for (final RoomStayData roomStayData : roomStays)
		{

			for (final RatePlanData ratePlan : roomStayData.getRatePlans())
			{
				final BigDecimal totalValue = ratePlan.getActualRate().getValue();


				BigDecimal perPersonPrice = totalValue
						.divide(BigDecimal.valueOf(Math.abs(noOfPersons)), 2, RoundingMode.HALF_EVEN);


				final PriceData perPersonPlanPrice = getTravelCommercePriceFacade().createPriceData(perPersonPrice.doubleValue());
				ratePlan.setPerPersonPlanPrice(perPersonPlanPrice);
			}
		}
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
