/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.travelfacades.facades.packages.handlers.impl.PackageRoomStaysHandler;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.util.BcfRatePlanUtil;


public class BcfPackageRoomStaysHandler extends PackageRoomStaysHandler
{
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;

	@Override
	public void handle(final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
				.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOffering = getBcfAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);
		List<AccommodationModel> accommodations = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(accommodationOffering.getAccommodations()))
		{
			accommodations = accommodationOffering.getAccommodations().stream()
					.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
					.collect(Collectors.toList());
		}

		final StayDateRangeData stayDateRange = availabilityRequestData.getCriterion().getStayDateRange();
		final List<RoomStayCandidateData> roomStayCandidates = availabilityRequestData.getCriterion().getRoomStayCandidates();

		final List<RoomStayData> updatedRoomStayDatas = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(roomStayCandidates))
		{
			for (final RoomStayCandidateData roomStayCandidateData : roomStayCandidates)
			{
				final List<RoomStayData> roomStays = new ArrayList<>();
				final int roomStayRefNumber = 0;
				for (final AccommodationModel accommodation : accommodations)
				{
					if(BcfRatePlanUtil.isValidForMinimumNightsStayRule(accommodation.getMinimumNightsStayRules(),stayDateRange)){
						final RoomStayData roomStayData = createRoomStayData(accommodation, roomStayRefNumber, stayDateRange);
						roomStays.add(roomStayData);
					}

				}
				Optional<Integer> totalCount =roomStayCandidateData.getPassengerTypeQuantityList().stream().map(passengerTypeQuantity->passengerTypeQuantity.getQuantity()).reduce(Integer::sum);

				roomStays.forEach(roomStay -> checkIfEligibleToAdd(updatedRoomStayDatas, roomStay, roomStayCandidateData,totalCount.get()));
			}
		}
		accommodationAvailabilityResponseData.setRoomStays(updatedRoomStayDatas);
	}

	protected void checkIfEligibleToAdd(List<RoomStayData> roomStayDatas, RoomStayData roomStay, RoomStayCandidateData roomStayCandidate, Integer totalCount) {
		if(roomStay.getMaxOccupancy()!= null && roomStay.getMaxOccupancy()>=totalCount){
			roomStay.setRoomStayRefNumber(roomStayCandidate.getRoomStayCandidateRefNumber());
			roomStayDatas.add(roomStay);
		}
	}

	protected RoomStayData createRoomStayData(AccommodationModel accommodation, Integer roomStayRefNumber, StayDateRangeData stayDateRange) {
		RoomStayData roomStay = new RoomStayData();
		roomStay.setRoomTypes(this.getRoomTypeConverter().convertAll(Collections.singletonList(accommodation)));
		roomStay.setRoomStayRefNumber(roomStayRefNumber);
		roomStay.setCheckInDate(stayDateRange.getStartTime());
		roomStay.setCheckOutDate(stayDateRange.getEndTime());
		roomStay.setAccommodationCode(accommodation.getCode());
		roomStay.setMaxOccupancy(accommodation.getMaxOccupancyCount());
		return roomStay;
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}
}
