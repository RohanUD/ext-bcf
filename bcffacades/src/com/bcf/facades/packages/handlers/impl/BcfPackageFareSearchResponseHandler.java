/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.commercefacades.packages.request.PackageSearchRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageSearchResponseData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.packages.handlers.impl.PackageFareSearchResponseHandler;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.packages.BcfPackageSearchFacade;
import com.bcf.integrations.core.exception.IntegrationException;

public class BcfPackageFareSearchResponseHandler extends PackageFareSearchResponseHandler
{
	private static final Logger LOG = Logger.getLogger(BcfPackageFareSearchResponseHandler.class);

	private static final String PACKAGE_TRANSPORT_RESPONSE_ERROR = "package.transport.response.error";
	private static final String INVALID_FARE_SEARCH_REQUEST_DATA = "invalid fare search request data";

	private BcfPackageSearchFacade bcfPackageSearchFacade;
	private SessionService sessionService;

	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{

		sessionService.removeAttribute("fareSelectionData");
		if ((accommodationSearchRequest instanceof PackageSearchRequestData)
				&& (accommodationSearchResponse instanceof PackageSearchResponseData))
		{
			final PackageSearchRequestData packageSearchRequestData = (PackageSearchRequestData) accommodationSearchRequest;
			final PackageSearchResponseData packageSearchResponseData = (PackageSearchResponseData) accommodationSearchResponse;
			final List<OriginDestinationInfoData> originDestinationInfos = packageSearchRequestData.getFareSearchRequestData()
					.getOriginDestinationInfo();
			if (CollectionUtils.isEmpty(originDestinationInfos) || CollectionUtils.size(originDestinationInfos) < 2)
			{
				LOG.error(INVALID_FARE_SEARCH_REQUEST_DATA);
				packageSearchResponseData.setErrorMessage(INVALID_FARE_SEARCH_REQUEST_DATA);
				return;
			}

			try
			{
				final FareSelectionData fareSelectionData = getBcfPackageSearchFacade()
						.getFareSelectionForPackage(packageSearchRequestData.getFareSearchRequestData());
				sessionService.setAttribute("fareSelectionData", fareSelectionData);
				//Populate packages with fare selection data
				populatePackagesWithFareSelectionData(packageSearchResponseData, fareSelectionData);
			}
			catch (final IntegrationException ex)
			{
				LOG.error(ex.getMessage());
				packageSearchResponseData.setErrorMessage(PACKAGE_TRANSPORT_RESPONSE_ERROR);
			}
		}
	}

	/**
	 * Populate packages with fare selection data.
	 *
	 * @param accommodationSearchResponse the accommodation search response
	 * @param fareSelectionData           the fare selection data
	 */
	protected void populatePackagesWithFareSelectionData(final AccommodationSearchResponseData accommodationSearchResponse,
			final FareSelectionData fareSelectionData)
	{
		for (final PropertyData propertyData : accommodationSearchResponse.getProperties())
		{
			if (propertyData instanceof PackageData)
			{
				final PackageData packageData = (PackageData) propertyData;
				packageData.setFareSelectionData(fareSelectionData);
			}
		}
	}

	protected BcfPackageSearchFacade getBcfPackageSearchFacade()
	{
		return bcfPackageSearchFacade;
	}

	@Required
	public void setBcfPackageSearchFacade(final BcfPackageSearchFacade bcfPackageSearchFacade)
	{
		this.bcfPackageSearchFacade = bcfPackageSearchFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
