/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationOfferingDayRateData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchResponseData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.commercefacades.packages.request.PackageSearchRequestData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.accommodation.handlers.impl.AbstractAccommodationAvailabilitySearchHandler;


public class BcfPackageSearchAvailabilityHandler extends AbstractAccommodationAvailabilitySearchHandler
{
	@Override
	public void handle(final List<AccommodationOfferingDayRateData> accommodationOfferingDayRates,
			final AccommodationSearchRequestData accommodationSearchRequest,
			final AccommodationSearchResponseData accommodationSearchResponse)
	{
		if (!CollectionUtils.isEmpty(accommodationSearchResponse.getProperties())
				&& accommodationSearchRequest instanceof PackageSearchRequestData)
		{
			final PackageSearchRequestData packageSearchRequest = (PackageSearchRequestData) accommodationSearchRequest;
			final List<PropertyData> availablePackages = new ArrayList<>();
			accommodationSearchResponse.getProperties().stream().filter(PackageData.class::isInstance).forEach(propertyData -> {
				final PackageData packageData = (PackageData) propertyData;
				if (isTransportAvailable(packageSearchRequest, packageData))
				{
					checkAndSetAccommodationAvailability(packageData, packageSearchRequest);
					availablePackages.add(packageData);
				}
			});
			accommodationSearchResponse.setProperties(availablePackages);
		}
	}

	protected boolean isTransportAvailable(final PackageSearchRequestData packageSearchRequest, final PackageData packageData)
	{
		if (Objects.isNull(packageData.getFareSelectionData()))
		{
			return false;
		}

		final List<ItineraryPricingInfoData> itineraryPricingInfos = packageData.getFareSelectionData().getPricedItineraries()
				.stream().flatMap(pricedItineraryData -> pricedItineraryData.getItineraryPricingInfos().stream())
				.collect(Collectors.toList());

		return itineraryPricingInfos.stream().allMatch(ItineraryPricingInfoData::isAvailable)
				&& CollectionUtils.size(packageSearchRequest.getFareSearchRequestData().getOriginDestinationInfo()) == CollectionUtils
				.size(packageData.getFareSelectionData().getPricedItineraries());
	}

	protected void checkAndSetAccommodationAvailability(final PackageData packageData,
			final PackageSearchRequestData packageSearchRequest)
	{
		final AccommodationOfferingModel accommodationOfferingModel = getBcfAccommodationOfferingService()
				.getAccommodationOffering(packageData.getAccommodationOfferingCode());
		final SaleStatusType saleStatusType = getBcfAccommodationFacadeHelper()
				.getSaleStatus(accommodationOfferingModel.getSaleStatuses(),
						packageSearchRequest.getCriterion().getStayDateRange().getStartTime(),
						packageSearchRequest.getCriterion().getStayDateRange().getEndTime());

		AvailabilityStatus accommodationAvailabilityStatus = AvailabilityStatus.SOLD_OUT;

		if (Objects.equals(SaleStatusType.STOPSALE, saleStatusType))
		{
			accommodationAvailabilityStatus = AvailabilityStatus.SOLD_OUT;
		}
		else if (Objects.equals(SaleStatusType.OPEN, saleStatusType) || (
				Objects.equals(SaleStatusType.NOT_BOOKABLE_ONLINE, saleStatusType) && !getSaleStatusService()
						.toConsiderNonBookableOnlineSaleStatus()))
		{
			accommodationAvailabilityStatus = getAccommodationAvailabilityStatus(accommodationOfferingModel,
					packageSearchRequest.getCriterion().getStayDateRange(),
					packageSearchRequest.getCriterion().getRoomStayCandidates().size());
		}
		else if (getSaleStatusService().toConsiderNonBookableOnlineSaleStatus() &&
				Objects.equals(SaleStatusType.NOT_BOOKABLE_ONLINE, saleStatusType))
		{
			accommodationAvailabilityStatus = AvailabilityStatus.NOT_BOOKABLE_ONLINE;
		}

		packageData.setAvailabilityStatus(accommodationAvailabilityStatus.toString());
	}
}
