/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.ExtraGuestOccupancyData;
import de.hybris.platform.commercefacades.accommodation.RoomRateData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.AccommodationDetailsHandler;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.impl.DefaultBcfTravelCartFacadeHelper;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class BcfExtraGuestOccupancyHandler implements AccommodationDetailsHandler
{

	private BcfAccommodationService bcfAccommodationService;
	private PriceRowService priceRowService;

	public void handle(AccommodationAvailabilityRequestData availabilityRequestData, AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{

		Map<Integer,List<RoomStayData>> roomStayMap= accommodationAvailabilityResponseData.getRoomStays().stream().collect(Collectors.groupingBy(roomStay->roomStay.getRoomStayRefNumber()));

		List<RoomStayData> unAvailableRoomStays=new ArrayList<>();

		int index = 0;
		for(Map.Entry<Integer,List<RoomStayData>> roomStayMapEntry: roomStayMap.entrySet()) {

			List<RoomStayData> roomStays=roomStayMapEntry.getValue();
			for(int i=roomStays.size()-1;i >= 0;i--)

			{
				RoomStayData roomStayData = roomStays.get(i);

				String accommodationCode = roomStayData.getAccommodationCode();
				AccommodationModel accommodation = bcfAccommodationService.getAccommodation(accommodationCode);
				List<PassengerTypeQuantityData> passengerTypes = availabilityRequestData.getCriterion().getRoomStayCandidates()
						.get(index)
						.getPassengerTypeQuantityList();

				int baseOccupancy = accommodation.getBaseOccupancyCount();

				if(CollectionUtils.isNotEmpty(passengerTypes))
				{
					Pair<Integer, Integer> extraGuestOccupancies = DefaultBcfTravelCartFacadeHelper
							.getExtraGuestOccupancies(passengerTypes, baseOccupancy);

					boolean isAvailable=checkAndPopulateExtraGuestOccupancies(roomStayData, accommodation, extraGuestOccupancies);

					if(!isAvailable){

					unAvailableRoomStays.add(roomStayData);
					}
				}

			}
			index++;
		}

		accommodationAvailabilityResponseData.getRoomStays().removeAll(unAvailableRoomStays);


	}

	private boolean checkAndPopulateExtraGuestOccupancies(final RoomStayData roomStayData, final AccommodationModel accommodation,
			final Pair<Integer, Integer> extraGuestOccupancies)
	{
		if (extraGuestOccupancies.getLeft() > 0 || extraGuestOccupancies.getRight() > 0)
		{

			if(CollectionUtils.isEmpty(roomStayData.getRatePlans())){
				throw new SystemException("no rate plan found for accommodation: "+accommodation.getCode());
			}

			if (extraGuestOccupancies.getLeft() > 0)
			{
				Collection<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodation
						.getExtraGuestProducts().stream()
						.filter(Objects::nonNull).distinct()
						.filter(extraGuestProduct -> priceRowService
								.getPriceRow(extraGuestProduct, BcfFacadesConstants.ADULT,
										roomStayData.getCheckInDate()) != null)
						.collect(
								Collectors.toList());

				if (CollectionUtils.isEmpty(extraGuestProducts))
				{
					return false;
				}
			}


			if (extraGuestOccupancies.getRight()  > 0)
			{
				Collection<ExtraGuestOccupancyProductModel> extraGuestProducts = accommodation
						.getExtraGuestProducts().stream()
						.filter(Objects::nonNull).distinct()
						.filter(extraGuestProduct -> priceRowService
								.getPriceRow(extraGuestProduct, BcfFacadesConstants.CHILD,
										roomStayData.getCheckInDate()) != null)
						.collect(
								Collectors.toList());


				if (CollectionUtils.isEmpty(extraGuestProducts))
				{
					return false;
				}
			}
			roomStayData.getRatePlans().stream().flatMap(plan -> plan.getRoomRates().stream()).forEach(roomrate -> {

				List<ExtraGuestOccupancyData> extraGuestOccupancyDatas = new ArrayList();
				if (extraGuestOccupancies.getLeft() > 0)
				{

					this.populateExtraGuestOccupanyForRatePlan(roomrate, accommodation,extraGuestOccupancyDatas, BcfFacadesConstants.ADULT, extraGuestOccupancies.getLeft());
				}
				if (extraGuestOccupancies.getRight()  > 0)
				{

					this.populateExtraGuestOccupanyForRatePlan(roomrate,accommodation, extraGuestOccupancyDatas, BcfFacadesConstants.CHILD, extraGuestOccupancies.getRight() );
				}
				roomrate.setExtraGuestOccupancies(extraGuestOccupancyDatas);

			});
		}
		return true;
	}


	protected void populateExtraGuestOccupanyForRatePlan(RoomRateData roomRateData,AccommodationModel accommodation,List<ExtraGuestOccupancyData> extraGuestOccupancyDatas,String passengerType,int quantity) {
		ExtraGuestOccupancyProductModel extraGuestOccupancyProductModel=Optional.ofNullable(accommodation.getExtraGuestProducts()).orElse(Collections.emptySet()).stream().filter(extraGuestProduct-> priceRowService.getPriceRow(extraGuestProduct, passengerType,
				roomRateData.getStayDateRange().getStartTime())!=null).findAny().orElse(null);
		if(extraGuestOccupancyProductModel!=null)
		{
			this.createExtraGuestOccupancyData(extraGuestOccupancyDatas, extraGuestOccupancyProductModel, passengerType, quantity);
		}

	}

	protected void createExtraGuestOccupancyData(List<ExtraGuestOccupancyData> extraGuestOccupancyDatas, ExtraGuestOccupancyProductModel extraGuestOccupancyProductModel,String passengerType, int quantity) {
		ExtraGuestOccupancyData extraGuestOccupancyData = new ExtraGuestOccupancyData();
		extraGuestOccupancyData.setCode(extraGuestOccupancyProductModel.getCode());
		extraGuestOccupancyData.setQuantity(quantity);
		extraGuestOccupancyData.setPassengerType(passengerType);
		extraGuestOccupancyDatas.add(extraGuestOccupancyData);
	}





	public BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}
}


