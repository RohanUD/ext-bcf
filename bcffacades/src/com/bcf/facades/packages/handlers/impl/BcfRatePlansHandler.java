/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData;
import de.hybris.platform.commercefacades.accommodation.RatePlanData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomTypeData;
import de.hybris.platform.travelfacades.facades.accommodation.handlers.impl.RatePlansHandler;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.util.BcfRatePlanUtil;


public class BcfRatePlansHandler extends RatePlansHandler
{
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;

	@Override
	public void handle(
			final AccommodationAvailabilityRequestData availabilityRequestData,
			final AccommodationAvailabilityResponseData accommodationAvailabilityResponseData)
	{
		final String accommodationOfferingCode = availabilityRequestData.getCriterion().getAccommodationReference()
				.getAccommodationOfferingCode();
		final AccommodationOfferingModel accommodationOffering = getBcfAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingCode);

		for (final RoomStayData roomStayData : accommodationAvailabilityResponseData.getRoomStays())
		{
			final RoomTypeData roomTypeData = roomStayData.getRoomTypes().stream().findFirst().orElse(null);
			if (Objects.nonNull(roomTypeData))
			{
				final String accommodationCode = roomTypeData.getCode();
				if (CollectionUtils.isNotEmpty(accommodationOffering.getAccommodations()) && accommodationOffering.getAccommodations()
						.contains(accommodationCode))
				{
					final AccommodationModel accommodation = getBcfAccommodationService().getAccommodation(accommodationCode);
					final Set<RatePlanModel> validRatePlans = BcfRatePlanUtil
							.getValidRatePlansAgainstDates(accommodation.getRatePlan(),
									roomStayData.getCheckInDate(), roomStayData.getCheckOutDate());
					final List<RatePlanData> ratePlanDatas = CollectionUtils.isEmpty(validRatePlans) ?
							Collections.emptyList() :
							getRatePlanConverter().convertAll(validRatePlans);

					roomStayData.setRatePlans(ratePlanDatas);
					updateGuestOccupancy(roomStayData, accommodation);
				}
			}
		}
	}

	protected BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}
}
