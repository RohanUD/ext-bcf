/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.travelfacades.facades.packages.handlers.impl.PackageSelectedFareBundlesHandler;
import org.apache.commons.collections.CollectionUtils;


public class BcfPackageSelectedFareBundlesHandler extends PackageSelectedFareBundlesHandler
{
	@Override
	protected ItineraryPricingInfoData getFirstAvailableItineraryPricingInfo(final PricedItineraryData pricedItinerary)
	{
		if (CollectionUtils.isEmpty(pricedItinerary.getItineraryPricingInfos()))
		{
			return null;
		}

		return pricedItinerary.getItineraryPricingInfos().stream().filter(ItineraryPricingInfoData::isAvailable).findFirst()
				.orElse(null);
	}

}
