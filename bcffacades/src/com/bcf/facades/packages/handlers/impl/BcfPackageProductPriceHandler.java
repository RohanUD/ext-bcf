/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages.handlers.impl;

import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.packages.handlers.BcfStandardPackageProductHandler;
import com.bcf.facades.packages.response.BcfPackageProductData;


public class BcfPackageProductPriceHandler implements BcfStandardPackageProductHandler
{
	private PassengerTypeService passengerTypeService;
	private ProductService productService;
	private BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade;

	@Override
	public void handle(final BundleTemplateData bundleTemplate, final List<BcfPackageProductData> packageProductDataList)
	{
		packageProductDataList.stream().filter(packageProduct -> Objects.nonNull(packageProduct.getProduct().getStock())
				&& packageProduct.getProduct().getStock().getStockLevel() > 0).forEach(packageProductData -> {
					final ProductModel productModel = getProductService().getProductForCode(packageProductData.getProduct().getCode());
					final PassengerTypeModel passengerType = getPassengerTypeService()
							.getPassengerType(packageProductData.getPassengerType().getCode());
					final Date commencementDate = packageProductData.getCommencementDate();
					if (productModel.getItemtype().equals(ActivityProductModel._TYPECODE))
					{
						final ActivityProductModel activityProduct = (ActivityProductModel) productModel;
						final RateData rateData = getBcfTravelCommercePriceFacade().calculateRateData(activityProduct,
								packageProductData.getQuantity().longValue(), passengerType, commencementDate,
								activityProduct.getDestination());
						packageProductData.setPrice(rateData);
					}
				});
	}

	/**
	 * @return the passengerTypeService
	 */
	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	/**
	 * @param passengerTypeService
	 *           the passengerTypeService to set
	 */
	@Required
	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService
	 *           the productService to set
	 */
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @return the bcfTravelCommercePriceFacade
	 */
	protected BCFTravelCommercePriceFacade getBcfTravelCommercePriceFacade()
	{
		return bcfTravelCommercePriceFacade;
	}

	/**
	 * @param bcfTravelCommercePriceFacade
	 *           the bcfTravelCommercePriceFacade to set
	 */
	@Required
	public void setBcfTravelCommercePriceFacade(final BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade)
	{
		this.bcfTravelCommercePriceFacade = bcfTravelCommercePriceFacade;
	}
}
