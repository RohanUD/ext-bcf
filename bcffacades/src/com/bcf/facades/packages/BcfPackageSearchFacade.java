/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.packages;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.travelfacades.facades.packages.PackageSearchFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfPackageSearchFacade extends PackageSearchFacade
{
	/**
	 * To get FareSelectionData for packages search. It would search for list sailings based on FareSearchRequest->OutBound and InBound,
	 * sort priced itineraries by price in desc order and then populate the first one (Max Price) per bound.
	 *
	 * @param fareSearchRequestData
	 * @return
	 * @throws IntegrationException
	 */
	FareSelectionData getFareSelectionForPackage(FareSearchRequestData fareSearchRequestData)
			throws IntegrationException;
}
