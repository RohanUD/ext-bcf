/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.shipinfo;

import java.util.List;
import com.bcf.facades.ship.BcfShipInfoData;
import com.bcf.facades.travel.data.ShipInfoPaginationData;
import com.bcf.webservices.travel.data.ShipInfosData;


public interface BcfShipInfoFacade
{
	void createOrModifyShipInfo(ShipInfosData shipInfosData);

	void deleteShipInfo(String shipInfoCode);

	List<BcfShipInfoData> getShipInfos(int startIndex, int pageNumber,
			int recordsPerPage);

	List<BcfShipInfoData> getShipInfos();

	BcfShipInfoData getShipInfoByCode(String code);

	ShipInfoPaginationData getPaginatedShipInfos(final int pageSize, final int currentPage);
}
