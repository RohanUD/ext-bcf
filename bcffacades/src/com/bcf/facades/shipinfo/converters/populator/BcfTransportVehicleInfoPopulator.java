/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.shipinfo.converters.populator;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleInfoModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.facades.ship.BcfShipFacilityData;
import com.bcf.facades.ship.BcfShipInfoData;


public class BcfTransportVehicleInfoPopulator implements Populator<TransportVehicleInfoModel, BcfShipInfoData>
{
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final TransportVehicleInfoModel source, final BcfShipInfoData target)
	{
		if (source instanceof ShipInfoModel)
		{
			final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
			target.setBuilt(((ShipInfoModel) source).getBuilt());
			target
					.setOverallLength(((ShipInfoModel) source).getOverallLength());
			target.setMaximumDisplacement(
					((ShipInfoModel) source).getMaximumDisplacement());
			target.setCarCapacity(((ShipInfoModel) source).getCarCapacity());
			target.setTotalTravellerCapacity(
					((ShipInfoModel) source).getTotalTravellerCapacity());
			target.setMaximumSpeed(((ShipInfoModel) source).getMaximumSpeed());
			target.setPower(((ShipInfoModel) source).getPower());
			target.setName(source.getName(currentLocale));
			target.setTranslatedName(BcfTravelRouteUtil.translateName(source.getName(currentLocale)));
			target.setCode(source.getCode());
			target.setDescription(source.getDescription(currentLocale));
			target.setSafetyDescription(source.getSafetyDescription());
			target.setAccessibilityDescription(source.getAccessibilityDescription(currentLocale));
			target.setGeneralDescription(source.getGeneralDescription(currentLocale));
			target.setSafetyPdfHelpInfo(source.getSafetyPdfHelpInfo(currentLocale));
			target.setSafetyPdfName(source.getSafetyPdfName());
			target.setAccessibilityPdfName(source.getAccessibilityPdfName());
			final MediaModel shipIcon= ((ShipInfoModel) source).getShipIcon();
			if(Objects.nonNull(shipIcon)) {
				target.setShipIcon(getBcfResponsiveMediaStrategy().getResponsiveJson(shipIcon));
				target.setShipIconAltText(shipIcon.getAltText());
			}
			target.setImage(getBcfResponsiveMediaStrategy().getResponsiveJson(source.getImage()));
			if(Objects.nonNull(source.getImage())) {
				target.setImageAltText(source.getImage().getAltText());
			}
			target.setShipDisplay(((ShipInfoModel) source).isShipDisplay());
			target.setShipDescription(((ShipInfoModel) source).getShipDescription());
			if (source.getSafetyPdf() != null)
			{
				target.setSafetyPdfDownloadUrl(source.getSafetyPdf(currentLocale).getDownloadURL());
			}
			target.setAccessibilityPdfHelpInfo(source.getAccessibilityPdfHelpInfo(currentLocale));
			if (source.getAccessibilityPdf() != null)
			{
				target.setAccessibilityPdfDownloadUrl(source.getAccessibilityPdf(currentLocale).getDownloadURL());
			}

			final List<ImageData> images = new ArrayList<>();
			for (final MediaModel media : source.getOtherImages())
			{
				if (Objects.nonNull(media))
				{
					final ImageData imageData = new ImageData();
					imageData.setUrl(getBcfResponsiveMediaStrategy().getResponsiveJson(media));
					imageData.setAltText(media.getAltText());
					images.add(imageData);
				}
			}
			target.setOtherImages(images);

			shipFacilityData(source, target);
		}
	}

	private void shipFacilityData(final TransportVehicleInfoModel source, final BcfShipInfoData target)
	{
		final List<BcfShipFacilityData> shipFacilityList = new ArrayList<>();
		((ShipInfoModel) source).getShipFacilities().stream().forEach(shipFacility -> {
			final BcfShipFacilityData shipFacilityData = new BcfShipFacilityData();
			shipFacilityData.setLogo(getBcfResponsiveMediaStrategy().getResponsiveJson(shipFacility.getLogo()));
			shipFacilityData.setShortDescription(shipFacility.getShortDescription());
			shipFacilityData.setCode(shipFacility.getCode());
			shipFacilityData.setName(shipFacility.getShortDescription());
			shipFacilityList.add(shipFacilityData);
		});

		target.setShipFacilities(shipFacilityList);
	}

	public BCFResponsiveMediaStrategy getBcfResponsiveMediaStrategy()
	{
		return bcfResponsiveMediaStrategy;
	}

	@Required
	public void setBcfResponsiveMediaStrategy(
			final BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy)
	{
		this.bcfResponsiveMediaStrategy = bcfResponsiveMediaStrategy;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
