/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.shipinfo.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.TransportVehicleType;
import de.hybris.platform.travelservices.model.travel.ShipInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.BcfTransportVehicleService;
import com.bcf.core.shipinfo.service.BcfShipInfoService;
import com.bcf.facades.ship.BcfShipInfoData;
import com.bcf.facades.shipinfo.BcfShipInfoFacade;
import com.bcf.facades.travel.data.ShipInfoPaginationData;
import com.bcf.facades.webservices.populators.ShipInfoReversePopulator;
import com.bcf.webservices.travel.data.ShipInfosData;


public class DefaultBcfShipInfoFacade implements BcfShipInfoFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultBcfShipInfoFacade.class);

	private BcfShipInfoService shipInfoService;
	private ModelService modelService;
	private ShipInfoReversePopulator shipInfoReversePopulator;
	private Converter<TransportVehicleInfoModel, BcfShipInfoData> transportVehicleInfoConverter;
	private BcfTransportVehicleService transportVehicleService;

	@Override
	public void createOrModifyShipInfo(final ShipInfosData shipInfosData)
	{
		shipInfosData.getShipInfos().stream().forEach(shipInfoData -> {
			ShipInfoModel shipInfoModel = getShipInfoService().getShipInfoForCode(shipInfoData.getCode());
			if (Objects.isNull(shipInfoModel))
			{
				shipInfoModel = getModelService().create(ShipInfoModel.class);
			}
			shipInfoReversePopulator.populate(shipInfoData, shipInfoModel);
			getModelService().save(shipInfoModel);
			TransportVehicleModel transportVehicleModel = getTransportVehicleService().getTransportVehicle(shipInfoModel.getCode());
			if (Objects.isNull(transportVehicleModel))
			{
				transportVehicleModel = getModelService().create(TransportVehicleModel.class);
				transportVehicleModel.setCode(shipInfoModel.getCode());
			}
			transportVehicleModel.setType(TransportVehicleType.SHIP);
			transportVehicleModel.setTransportVehicleInfo(shipInfoModel);
			transportVehicleModel.setActive(Boolean.TRUE);
			getModelService().save(transportVehicleModel);
		});
	}

	@Override
	public void deleteShipInfo(final String shipInfoCode)
	{
		LOG.debug("Setting ShipInfo and TransportVehicle to inactive for the code " + shipInfoCode);
		final ShipInfoModel shipInfoModel = getShipInfoService().getShipInfoForCode(shipInfoCode);
		if (Objects.nonNull(shipInfoModel))
		{
			shipInfoModel.setActive(Boolean.FALSE);
			getModelService().save(shipInfoModel);
		}
		final TransportVehicleModel transportVehicleModel = getTransportVehicleService()
				.getTransportVehicle(shipInfoModel.getCode());
		if (Objects.nonNull(transportVehicleModel))
		{
			transportVehicleModel.setActive(Boolean.FALSE);
			getModelService().save(transportVehicleModel);
		}
	}

	@Override
	public List<BcfShipInfoData> getShipInfos(final int startIndex, final int pageNumber,
			final int recordsPerPage)
	{
		final List<ShipInfoModel> shipInfos = getShipInfoService()
				.getShipInfos(startIndex, pageNumber, recordsPerPage);
		return Converters.convertAll(shipInfos, getTransportVehicleInfoConverter());
	}

	@Override
	public List<BcfShipInfoData> getShipInfos()
	{
		final List<ShipInfoModel> shipInfos = getShipInfoService().getShipInfos();
		final List<BcfShipInfoData> shipInfoDataList = Converters.convertAll(shipInfos, getTransportVehicleInfoConverter());
		return shipInfoDataList;
	}

	@Override
	public ShipInfoPaginationData getPaginatedShipInfos(final int pageSize, final int currentPage)
	{
		final ShipInfoPaginationData shipInfoPaginationData = new ShipInfoPaginationData();
		final SearchPageData<ShipInfoModel> searchPageData = getShipInfoService().getPaginatedShipInfos(pageSize, currentPage);
		shipInfoPaginationData.setPaginationData(searchPageData.getPagination());
		shipInfoPaginationData.setResults(Converters
				.convertAll(searchPageData.getResults(), getTransportVehicleInfoConverter()));
		return shipInfoPaginationData;
	}

	@Override
	public BcfShipInfoData getShipInfoByCode(final String code)
	{
		final ShipInfoModel shipInfo = getShipInfoService()
				.getShipInfoForCode(code);
		return getTransportVehicleInfoConverter().convert(shipInfo);
	}

	protected BcfShipInfoService getShipInfoService()
	{
		return shipInfoService;
	}

	@Required
	public void setShipInfoService(final BcfShipInfoService shipInfoService)
	{
		this.shipInfoService = shipInfoService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ShipInfoReversePopulator getShipInfoReversePopulator()
	{
		return shipInfoReversePopulator;
	}

	@Required
	public void setShipInfoReversePopulator(final ShipInfoReversePopulator shipInfoReversePopulator)
	{
		this.shipInfoReversePopulator = shipInfoReversePopulator;
	}

	protected Converter<TransportVehicleInfoModel, BcfShipInfoData> getTransportVehicleInfoConverter()
	{
		return transportVehicleInfoConverter;
	}

	@Required
	public void setTransportVehicleInfoConverter(
			final Converter<TransportVehicleInfoModel, BcfShipInfoData> transportVehicleInfoConverter)
	{
		this.transportVehicleInfoConverter = transportVehicleInfoConverter;
	}

	protected BcfTransportVehicleService getTransportVehicleService()
	{
		return transportVehicleService;
	}

	@Required
	public void setTransportVehicleService(final BcfTransportVehicleService transportVehicleService)
	{
		this.transportVehicleService = transportVehicleService;
	}
}
