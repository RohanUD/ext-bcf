/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.subscription.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.travel.CRMSubscriptionMasterDetailsData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeByGroupData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.core.model.UpdateProfileProcessModel;
import com.bcf.core.service.BcfCustomerService;
import com.bcf.core.services.BCFCustomerSubscriptionService;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.data.BusinessOpportunitiesSubscriptionsData;
import com.bcf.facades.customer.data.CustomerSubscriptionData;
import com.bcf.facades.customer.data.CustomerSubscriptionsData;
import com.bcf.facades.subscription.CustomerSubscriptionsFacade;
import com.bcf.integration.crm.service.CRMCustomerSubscriptionService;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CustomerSubscriptionResponseDTO;
import com.bcf.integration.data.Subscription;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCustomerSubscriptionsFacade implements CustomerSubscriptionsFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultCustomerSubscriptionsFacade.class);

	private BCFCustomerSubscriptionService bcfCustomerSubscriptionService;
	private EnumerationService enumerationService;
	private ModelService modelService;
	private UserService userService;
	private CRMCustomerSubscriptionService crmCustomerSubscriptionService;
	private BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService;
	private Converter<CRMSubscriptionMasterDetailModel, CRMSubscriptionMasterDetailsData> subscriptionMasterDetailsConverter;
	private CMSSiteService cmsSiteService;
	private BusinessProcessService businessProcessService;
	private CommonI18NService commonI18NService;
	private BaseStoreService baseStoreService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfCustomerService")
	private BcfCustomerService bcfCustomerService;

	@Resource(name = "crmUpdateCustomerService")
	private CRMUpdateCustomerService crmUpdateCustomerService;

	@Override
	public CustomerSubscriptionsData getCustomerSubscriptionsForActivateProfile()
	{
		final CustomerSubscriptionsData customerSubscriptionsData = new CustomerSubscriptionsData();

		final String subscriptionsDisplayString = getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfFacadesConstants.ACTIVATE_PROFILE_SUBSCRIPTION_DISPLAY_LIST);
		final List<String> subscriptionsDisplayList = new ArrayList<>();
		if (StringUtils.isNotBlank(subscriptionsDisplayString))
		{
			subscriptionsDisplayList.addAll(Arrays.asList(subscriptionsDisplayString.split(",")));
		}


		for (final CustomerSubscriptionType subscription : enumerationService.getEnumerationValues(CustomerSubscriptionType.class))
		{
			if (subscriptionsDisplayList.contains(subscription.getCode()))
			{
				final CustomerSubscriptionData customerSubscriptionData = this.getCustomerSubscriptionDataForCode(subscription);
				if (CustomerSubscriptionType.NEWS_RELEASE.equals(subscription))
				{
					customerSubscriptionsData.setNewsReleases(customerSubscriptionData);
				}
				else if (CustomerSubscriptionType.VACATION_NEWSLETTER.equals(subscription))
				{
					customerSubscriptionsData.setPromotions(customerSubscriptionData);
				}
			}
		}

		return customerSubscriptionsData;
	}

	@Override
	public CustomerSubscriptionsData getAllSubscriptions()
	{

		updateSubscriptionsFromCRM();

		final CustomerSubscriptionsData customerSubscriptionsData = new CustomerSubscriptionsData();

		final CustomerSubscriptionData customerNewsSubscriptionData = this
				.getCustomerSubscriptionDataForCode(CustomerSubscriptionType.NEWS_RELEASE);
		customerSubscriptionsData.setNewsReleases(customerNewsSubscriptionData);

		final CustomerSubscriptionData customerPromotionsSubscriptionData = this
				.getCustomerSubscriptionDataForCode(CustomerSubscriptionType.VACATION_NEWSLETTER);
		customerSubscriptionsData.setPromotions(customerPromotionsSubscriptionData);

		final List<CRMSubscriptionMasterDetailModel> allSubscriptions = subscriptionsMasterDetailsService
				.getSubscriptionsMasterDetailsForType(CustomerSubscriptionType.SERVICE_NOTICE);
		customerSubscriptionsData.setServiceNotices(this.sortByNoticeGroup(allSubscriptions));
		return customerSubscriptionsData;
	}

	private void updateSubscriptionsFromCRM()
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		try
		{
			final CustomerSubscriptionResponseDTO crmCustomerSubscriptions = crmCustomerSubscriptionService
					.getCustomerSubscriptionsFromCRM(currentUser);
			// Delete all customer subscriptions if the crm subscriptions are empty
			if (Objects.isNull(crmCustomerSubscriptions) || Objects
					.isNull(crmCustomerSubscriptions.getCustomerSubscriptions().getSubscriptionList()) ||
					CollectionUtils
							.isEmpty(crmCustomerSubscriptions.getCustomerSubscriptions().getSubscriptionList().getSubscription()))
			{
				final Collection<CustomerSubscriptionModel> subscriptionList = currentUser.getSubscriptionList();
				if (CollectionUtils.isNotEmpty(subscriptionList))
				{
					modelService.removeAll(subscriptionList);
				}
				return;
			}

			// Create customer subscriptions for missing once
			final List<String> optInSubscriptionCodes = new ArrayList<>();
			final List<Subscription> crmSubscriptions = crmCustomerSubscriptions.getCustomerSubscriptions().getSubscriptionList()
					.getSubscription();
			crmSubscriptions.stream().forEach(subscription -> {
				if (Objects.nonNull(subscription.getOptInDetail()) && Objects.isNull(subscription.getOptOutDetail()))
				{
					optInSubscriptionCodes.add(subscription.getCode());
				}

				if (Objects.nonNull(subscription.getOptInDetail()) && Objects.nonNull(subscription.getOptInDetail().getDateTime())
						&& (Objects.isNull(subscription.getOptOutDetail()) || Objects
						.isNull(subscription.getOptOutDetail().getDateTime())))
				{
					optInSubscriptionCodes.add(subscription.getCode());
				}

				if (Objects.nonNull(subscription.getOptInDetail()) && Objects.nonNull(subscription.getOptInDetail().getDateTime())
						&& Objects.nonNull(subscription.getOptOutDetail()) && Objects
						.nonNull(subscription.getOptOutDetail().getDateTime()))
				{
					final SimpleDateFormat optInDf = new SimpleDateFormat(BcfCoreConstants.SUBSCRIPTION_TIME_STAMP);
					final SimpleDateFormat optOutDf = new SimpleDateFormat(BcfCoreConstants.SUBSCRIPTION_TIME_STAMP);
					try
					{
						final Date optInDate = optInDf.parse(subscription.getOptInDetail().getDateTime());
						final Date optOutDate = optOutDf.parse(subscription.getOptOutDetail().getDateTime());
						if (optInDate.after(optOutDate))
						{
							optInSubscriptionCodes.add(subscription.getCode());
						}
					}
					catch (final ParseException e)
					{
						LOG.error("Error while parsing optIn and optOut date for customer " + currentUser.getUid());
						LOG.error(e.getMessage(), e);
					}
				}
			});

			getBcfCustomerSubscriptionService().saveCustomerSubscription(currentUser, optInSubscriptionCodes);
			getModelService().saveAll(currentUser.getSubscriptionList());
			getModelService().save(currentUser);
		}

		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	private List<ServiceNoticeByGroupData> sortByNoticeGroup(final List<CRMSubscriptionMasterDetailModel> masterSubscriptionModels)
	{
		final List<ServiceNoticeByGroupData> serviceNoticesBySubscriptionGroup = new ArrayList<>();

		// Get all the regions from the routes.
		final Set<SubscriptionGroup> subscriptionGroupSet = masterSubscriptionModels.stream()
				.filter(crmSubscriptionMasterDetailModel -> crmSubscriptionMasterDetailModel.getSubscriptionGroup() != null)
				.map(masterSubscriptionModel -> masterSubscriptionModel.getSubscriptionGroup())
				.collect(Collectors.toSet());

		// Sort the subscriptions by groups
		subscriptionGroupSet.stream().forEach(subscriptionGroup -> {
			final ServiceNoticeByGroupData serviceNoticeByGroupData = new ServiceNoticeByGroupData();
			serviceNoticeByGroupData.setGroupCode(subscriptionGroup.getCode());
			serviceNoticeByGroupData.setGroupName(getEnumerationService().getEnumerationName(subscriptionGroup));

			final List<CRMSubscriptionMasterDetailModel> CRMSubscriptionMasterDetailModelList = masterSubscriptionModels.stream()
					.filter(
							masterSubscriptionModel -> Objects.nonNull(masterSubscriptionModel.getSubscriptionGroup())
									&& masterSubscriptionModel.getSubscriptionGroup()
									.equals(subscriptionGroup)).collect(Collectors.toList());
			final List<CRMSubscriptionMasterDetailsData> subscriptionMasterDetailsData = Converters
					.convertAll(CRMSubscriptionMasterDetailModelList, subscriptionMasterDetailsConverter);
			serviceNoticeByGroupData.setSubscriptionMasterDetails(sortServiceNoticesRoutes(subscriptionMasterDetailsData));
			serviceNoticesBySubscriptionGroup.add(serviceNoticeByGroupData);
		});

		return  this.sortByGroupNames(serviceNoticesBySubscriptionGroup);
	}

	private List<ServiceNoticeByGroupData> sortByGroupNames(
			final List<ServiceNoticeByGroupData> serviceNoticeByGroupData)
	{
		if (CollectionUtils.isNotEmpty(serviceNoticeByGroupData))
		{
			Collections.sort(serviceNoticeByGroupData, new Comparator<ServiceNoticeByGroupData>()
			{
				@Override
				public int compare(final ServiceNoticeByGroupData serviceNoticeByGroupData1,
						final ServiceNoticeByGroupData serviceNoticeByGroupData2)
				{
					return compareRoutesData(serviceNoticeByGroupData1.getGroupName(), serviceNoticeByGroupData2.getGroupName());
				}
			});
		}

		return serviceNoticeByGroupData;
	}

	private List<CRMSubscriptionMasterDetailsData> sortServiceNoticesRoutes(
			final List<CRMSubscriptionMasterDetailsData> subscriptionMasterDetailsData)
	{
		if (CollectionUtils.isNotEmpty(subscriptionMasterDetailsData))
		{
			Collections.sort(subscriptionMasterDetailsData, new Comparator<CRMSubscriptionMasterDetailsData>()
			{
				@Override
				public int compare(final CRMSubscriptionMasterDetailsData masterDetailsData1,
						final CRMSubscriptionMasterDetailsData masterDetailsData2)
				{
					return compareRoutesData(masterDetailsData1.getName(), masterDetailsData2.getName());
				}
			});
		}

		return subscriptionMasterDetailsData;
	}

	protected int compareRoutesData(final String string1,
			final String string2)
	{
		if (Objects.nonNull(string1))
		{
			if (Objects.nonNull(string2))
			{
				return string1.compareTo(string2);
			}
			else
			{
				return 1;
			}
		}
		else
		{
			if (Objects.nonNull(string2))
			{
				return -1;
			}
		}
		return 0;
	}



	@Override
	public List<CRMSubscriptionMasterDetailsData> getSubscriptionMasterDetailsForGroup(
			final List<SubscriptionGroup> subscriptionGroups)
	{
		final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetailModelList = subscriptionsMasterDetailsService
				.getSubscriptionMasterDetailsForSubscriptionGroups(subscriptionGroups);
		final List<CRMSubscriptionMasterDetailsData> subscriptionMasterDetailsData = Converters
				.convertAll(crmSubscriptionMasterDetailModelList, subscriptionMasterDetailsConverter);

		return subscriptionMasterDetailsData;
	}

	private CustomerSubscriptionData getCustomerSubscriptionDataForCode(final CustomerSubscriptionType customerSubscriptionType)
	{
		final CustomerSubscriptionData customerSubscriptionData = new CustomerSubscriptionData();
		final List<CRMSubscriptionMasterDetailModel> subscriptionsMasterDetailsForType = subscriptionsMasterDetailsService
				.getSubscriptionsMasterDetailsForType(customerSubscriptionType);
		if (CollectionUtils.isEmpty(subscriptionsMasterDetailsForType))
		{
			return customerSubscriptionData;
		}

		final CRMSubscriptionMasterDetailModel crmSubscriptionMasterDetailModel = subscriptionsMasterDetailsForType.get(0);
		customerSubscriptionData.setCode(crmSubscriptionMasterDetailModel.getSubscriptionCode());
		customerSubscriptionData.setName(crmSubscriptionMasterDetailModel.getName());
		customerSubscriptionData.setType(crmSubscriptionMasterDetailModel.getSubscriptionType().getCode());
		return customerSubscriptionData;
	}

	@Override
	public void unsubscribe(final String customerId, final String unsubscribeCodes)
			throws IntegrationException, UnsupportedEncodingException
	{

		if (Objects.isNull(unsubscribeCodes))
		{
			LOG.error("unsubscribeCodes must not null.");
			return;
		}

		if (StringUtils.equalsIgnoreCase(unsubscribeCodes, BcfCoreConstants.UNSUBSCRIBE_ALL))
		{
			this.unsubscribeAll(customerId);
			return;
		}

		final CustomerModel currentUser = bcfCustomerService.getCustomerForUId(customerId);
		getBcfCustomerSubscriptionService().unsubscribeByCodes(currentUser, unsubscribeCodes);
		getModelService().save(currentUser);
	}

	@Override
	public void unsubscribeAll(final String customerId) throws IntegrationException, UnsupportedEncodingException
	{
		final CustomerModel currentUser;
		if (StringUtils.isNotEmpty(customerId))
		{
			currentUser = bcfCustomerService.getCustomerForUId(customerId);
			if (Objects.isNull(currentUser))
			{
				throw new ModelNotFoundException("Couldn't find any customer for the customer id " + customerId);
			}
		}
		else
		{
			currentUser = (CustomerModel) getUserService().getCurrentUser();
		}

		getCrmCustomerSubscriptionService().unsubscribeAll(currentUser);
		getBcfCustomerSubscriptionService().unsubscribeAll(currentUser);
	}

	@Override
	public void checkCustomerExists(final String uId)
	{
		final CustomerModel currentUser = bcfCustomerService.getCustomerForUId(uId);
		if (Objects.isNull(currentUser))
		{
			throw new ModelNotFoundException("Couldn't find any customer for the customer id " + uId);
		}
	}

	@Override
	public void createServiceNoticesSubscriptions(final List<String> optInSubscriptionCodes) throws IntegrationException
	{

		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		getBcfCustomerSubscriptionService().saveCustomerSubscription(currentUser, optInSubscriptionCodes);

		updateToCRM(currentUser);

		// Update customer after the subscriptions, to update the subscriber indicator
		final UpdateCRMCustomerResponseDTO response = crmUpdateCustomerService.updateCustomer(currentUser);
	}

	@Override
	public void createBusinessOpportunitiesSubscriptions(
			final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData) throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final boolean modified = getBcfCustomerSubscriptionService()
				.createBusinessOpportunitiesSubscriptions(currentUser, businessOpportunitiesSubscriptionsData);
		if (modified)
		{
			updateToCRM(currentUser);
		}
	}

	@Override
	public void createNewsReleaseSubscriptions(final boolean serviceNotice) throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final boolean modified = getBcfCustomerSubscriptionService()
				.createNewsReleaseSubscriptions(currentUser, serviceNotice);

		if (modified)
		{
			updateToCRM(currentUser);
		}
	}

	@Override
	public void createOffersAndPromotionsSubscriptions(final boolean offersAndPromotions) throws IntegrationException
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final boolean modified = getBcfCustomerSubscriptionService()
				.createOffersAndPromotionsSubscriptions(currentUser, offersAndPromotions);

		if (modified)
		{
			updateToCRM(currentUser);
		}
	}

	protected void updateToCRM(final CustomerModel currentUser) throws IntegrationException
	{
		getCrmCustomerSubscriptionService().createOrUpdateCustomerSubscriptions(currentUser);
		getModelService().save(currentUser);
		getModelService().saveAll(currentUser.getSubscriptionList());
		final CustomerSubscriptionModel subscriptionWithBuniessOpportunities = currentUser.getSubscriptionList().stream()
				.filter(customerSubscriptionModel -> Objects.nonNull(customerSubscriptionModel.getBusinessOpportunities()))
				.findFirst().orElse(null);
		if (Objects.nonNull(subscriptionWithBuniessOpportunities))
		{
			getModelService().save(subscriptionWithBuniessOpportunities.getBusinessOpportunities());
		}

		// Update customer after the subscriptions, to update the subscriber indicator
		final UpdateCRMCustomerResponseDTO response = crmUpdateCustomerService.updateCustomer(currentUser);
	}

	private void startBusinessProcess(final CustomerModel customerModel)
	{
		final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
		final UpdateProfileProcessModel updateProfileProcessModel = getBusinessProcessService()
				.createProcess(
						"customerUpdateSubscriptionEmailProcess-" + customerModel.getUid() + "-" + System.currentTimeMillis(),
						"customerUpdateSubscriptionEmailProcess");
		updateProfileProcessModel.setSite(siteModel);
		updateProfileProcessModel.setCustomer(customerModel);
		updateProfileProcessModel.setLanguage(getCommonI18NService().getCurrentLanguage());
		updateProfileProcessModel.setCurrency(getCommonI18NService().getCurrentCurrency());
		updateProfileProcessModel.setStore(getBaseStoreService().getCurrentBaseStore());
		getModelService().save(updateProfileProcessModel);
		getBusinessProcessService().startProcess(updateProfileProcessModel);
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	protected BCFCustomerSubscriptionService getBcfCustomerSubscriptionService()
	{
		return bcfCustomerSubscriptionService;
	}

	@Required
	public void setBcfCustomerSubscriptionService(final BCFCustomerSubscriptionService bcfCustomerSubscriptionService)
	{
		this.bcfCustomerSubscriptionService = bcfCustomerSubscriptionService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CRMCustomerSubscriptionService getCrmCustomerSubscriptionService()
	{
		return crmCustomerSubscriptionService;
	}

	@Required
	public void setCrmCustomerSubscriptionService(
			final CRMCustomerSubscriptionService crmCustomerSubscriptionService)
	{
		this.crmCustomerSubscriptionService = crmCustomerSubscriptionService;
	}


	@Required
	public void setSubscriptionsMasterDetailsService(
			final BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService)
	{
		this.subscriptionsMasterDetailsService = subscriptionsMasterDetailsService;
	}

	@Required
	public void setSubscriptionMasterDetailsConverter(
			final Converter<CRMSubscriptionMasterDetailModel, CRMSubscriptionMasterDetailsData> subscriptionMasterDetailsConverter)
	{
		this.subscriptionMasterDetailsConverter = subscriptionMasterDetailsConverter;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
