/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.subscription.converters.populator;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


public class SignupReversePopulator implements Populator<RegisterData, CustomerModel>
{
	private static final String EMAIL_PLACE_HOLDER = "{EMAIL_PLACE_HOLDER}";
	private String LDAP_LOGIN;

	private CommonI18NService commonI18NService;
	private ConfigurationService configurationService;

	@PostConstruct
	public void init()
	{
		LDAP_LOGIN = getConfigurationService().getConfiguration().getString("customer.ldap.login");
	}

	@Override
	public void populate(final RegisterData source, final CustomerModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setName(source.getLogin());
		target.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		target.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		target.setLdapaccount(Boolean.TRUE);
		target.setLdaplogin(LDAP_LOGIN.replace(EMAIL_PLACE_HOLDER, source.getLogin()));
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
