/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.subscription.converters.populator;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.core.enums.AccountType;


public class CustomerRegisterReversePopulator implements Populator<RegisterData, CustomerModel>
{
	private static final String EMAIL_PLACE_HOLDER = "{EMAIL_PLACE_HOLDER}";
	private String LDAP_LOGIN;

	private ModelService modelService;
	private ConfigurationService configurationService;
	private UserService userService;
	private CustomerNameStrategy customerNameStrategy;
	private CommonI18NService commonI18NService;

	@PostConstruct
	public void init()
	{
		LDAP_LOGIN = getConfigurationService().getConfiguration().getString("customer.ldap.login");
	}

	@Override
	public void populate(final RegisterData source, final CustomerModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setName(getCustomerNameStrategy().getName(source.getFirstName(), source.getLastName()));
		target.setAccountType(AccountType.valueOf(source.getAccountType()));
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setPhoneNo(source.getPhoneNo());
		target.setCountry(source.getCountry());
		target.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		target.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		target.setLdapaccount(Boolean.TRUE);
		target.setLdaplogin(LDAP_LOGIN.replace(EMAIL_PLACE_HOLDER, source.getLogin()));
		target.setWebEnabled(source.isWebEnabled());
		target.setConsumer(source.isConsumer());
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
