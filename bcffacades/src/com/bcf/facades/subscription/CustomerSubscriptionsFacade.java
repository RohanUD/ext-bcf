/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.subscription;

import de.hybris.platform.commercefacades.travel.CRMSubscriptionMasterDetailsData;
import java.io.UnsupportedEncodingException;
import java.util.List;
import com.bcf.core.enums.SubscriptionGroup;
import com.bcf.facades.customer.data.BusinessOpportunitiesSubscriptionsData;
import com.bcf.facades.customer.data.CustomerSubscriptionsData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CustomerSubscriptionsFacade
{
	CustomerSubscriptionsData getCustomerSubscriptionsForActivateProfile();

	CustomerSubscriptionsData getAllSubscriptions();

	void unsubscribe(final String customerId, final String unsubscribeCodes)
			throws IntegrationException, UnsupportedEncodingException;

	void createOffersAndPromotionsSubscriptions(boolean offersAndPromotions) throws IntegrationException;

	void createServiceNoticesSubscriptions(List<String> optInSubscriptionCodes) throws IntegrationException;

	void unsubscribeAll(final String customerId) throws IntegrationException, UnsupportedEncodingException;

	void checkCustomerExists(String customerId);

	List<CRMSubscriptionMasterDetailsData> getSubscriptionMasterDetailsForGroup(List<SubscriptionGroup> subscriptionGroups);

	void createNewsReleaseSubscriptions(boolean serviceNotice) throws IntegrationException;

	void createBusinessOpportunitiesSubscriptions(BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData)
			throws IntegrationException;

	;
}
