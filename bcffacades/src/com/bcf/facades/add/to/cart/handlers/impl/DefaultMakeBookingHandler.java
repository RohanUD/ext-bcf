/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.handlers.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.tx.Transaction;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.facades.add.to.cart.handlers.AddToCartHandler;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultMakeBookingHandler implements AddToCartHandler
{
	private BCFTravelCartService bcfTravelCartService;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;
	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;
	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Override
	public void handle(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AddBundleToCartResponseData addBundleToCartResponseData)
			throws IntegrationException, CommerceCartModificationException
	{
		final Transaction tx = Transaction.current();

		CartModel cart = getBcfTravelCartService().getSessionCart();
		boolean result = false;
		BCFMakeBookingResponse bcfMakeBookingResponse = null;



		/*Get the makebookingResonse in ManageBooking Flow (Amend order)*/
		if (bcfTravelCartFacade.getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
				addBundleToCartRequestData.getSelectedOdRefNumber()).size() > 0)
		{
			final List<AddProductToCartRequestData> addProductToCartRequestForAncillaries = getBcfTravelCartFacade()
					.createAddProductToCartRequestForAncillaries(cart,
							addBundleToCartRequestData.getAddBundleToCartData().get(0).getTransportOfferings());

			final MakeBookingRequestData makeBookingRequestData = getMakeBookingIntegrationFacade().createMakeBookingRequestData(
					cart, Collections.emptyList(), Collections.emptyMap(), Collections.emptyList(),
					addProductToCartRequestForAncillaries, addBundleToCartRequestData);
			final BCFModifyBookingResponse bcfModifyBookingResponse = getModifyBookingIntegrationFacade()
					.getModifyBookingResponse(makeBookingRequestData,
							BcfCoreConstants.MAKE_BOOKING_REQUEST);

			bcfMakeBookingResponse = bcfModifyBookingResponse.getMakeBookingResponse();
			final int journeyRefNumber = ((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
					.getJourneyRefNumber();
			final int odRefNum = addBundleToCartRequestData.getAddBundleToCartData().get(0)
					.getOriginDestinationRefNumber();
			final boolean isAmendment = bcfTravelCartFacade.isAmendmentCart();
			if (isAmendment)
			{
				getBcfTravelCartFacade().removeNewCartEntriesForRefNo(journeyRefNumber, odRefNum, Boolean.TRUE, Boolean.FALSE);
				getBcfTravelCartFacade().disableCartEntriesForRefNo(journeyRefNumber, odRefNum, Boolean.TRUE);
				final Optional<AbstractOrderEntryModel> previousEntry = cart.getEntries().stream().filter(
						entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
								.nonNull(entry.getTravelOrderEntryInfo()) && entry.getJourneyReferenceNumber() == journeyRefNumber
								&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == odRefNum
								&& AmendStatus.NEW.equals(entry.getAmendStatus())).findAny();
				if (previousEntry.isPresent() && StringUtils.isNotBlank(previousEntry.get().getCacheKey()))
				{
					final String previousCacheKey = previousEntry.get().getCacheKey();
					getBcfTravelCartFacade().removePreviousAmountToPayInfos(previousCacheKey);
					getBcfTravelCartFacade().removePreviousCartEntries(journeyRefNumber, odRefNum);
				}
			}
			getBcfTravelCartFacade().updateCartEntriesWithCacheKeyOrBookingRef(cart,
					journeyRefNumber, odRefNum,
					bcfMakeBookingResponse.getItinerary().getBooking().getBookingReference().getCachingKey(),
					bcfMakeBookingResponse.getItinerary().getBooking().getBookingReference().getBookingReference());


		}
		/*MakeBookingResponse for normal ferry flow(Add entries to cart)*/
		else
		{
			bcfMakeBookingResponse = getMakeBookingIntegrationFacade().getMakeBookingResponse(addBundleToCartRequestData);
		}
		/*Add entries to cart after MakeBooking*/
		try
		{
			tx.begin();
			final List<CartModificationData> cartModificationDataList = getBcfTravelCartFacade().updateCart(bcfMakeBookingResponse,
					addBundleToCartRequestData);

			final BcfAddBundleToCartData bcfAddBundleToCartData = (BcfAddBundleToCartData) addBundleToCartRequestData
					.getAddBundleToCartData().get(0);

			getBcfTravelCartFacadeHelper().addFeesProductToCart(bcfMakeBookingResponse, bcfAddBundleToCartData.getJourneyRefNumber(),
					bcfAddBundleToCartData.getOriginDestinationRefNumber());
			addBundleToCartResponseData.setCartModificationDataList(cartModificationDataList);

			if (bcfTravelCartFacade.isAmendmentCart())
			{
				getBcfTravelCartFacade().addAncillaryProductToCart(cart, bcfMakeBookingResponse, addBundleToCartRequestData);
			}

			if (addBundleToCartRequestData.isOpenTicket())
			{
				bcfTravelCartFacadeHelper.updateOpenTicketStatus(cartModificationDataList);
			}

			tx.commit();

			cart = getBcfTravelCartService().getSessionCart();
			result = true;
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}



	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	protected MakeBookingIntegrationFacade getMakeBookingIntegrationFacade()
	{
		return makeBookingIntegrationFacade;
	}

	@Required
	public void setMakeBookingIntegrationFacade(final MakeBookingIntegrationFacade makeBookingIntegrationFacade)
	{
		this.makeBookingIntegrationFacade = makeBookingIntegrationFacade;
	}

	public ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}
}
