/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.handlers.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.travelservices.enums.TripType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.add.to.cart.handlers.AddToCartHandler;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCancelBookingHandler implements AddToCartHandler
{
	private BcfTravelCartFacade bcfTravelCartFacade;
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;

	@Override
	public void handle(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AddBundleToCartResponseData addBundleToCartResponseData) throws IntegrationException
	{
		final List<CartFilterParamsDto> filterParams = new ArrayList<>();
		boolean isCancellationSuccess = false;
		boolean cancelOpposite = false;
		if (addBundleToCartRequestData.getSelectedOdRefNumber() == 0 && getBcfTravelCartFacade()
				.getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(), 1).size() > 0)
		{
			cancelOpposite = true;
		}
		filterParams
				.add(getBcfTravelCartFacade().initializeCartFilterParams(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
						addBundleToCartRequestData.getSelectedOdRefNumber(), false, Collections.emptyList()));
		if (cancelOpposite)
		{
			filterParams
					.add(getBcfTravelCartFacade().initializeCartFilterParams(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							(addBundleToCartRequestData.getSelectedOdRefNumber() == 0) ? 1 : 0, false, Collections.emptyList()));
		}
		final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
				.cancelSailingsInCart(filterParams, false);
		isCancellationSuccess = removeSailingResponseDatas.get(0).isSuccess();
		final TripType tripType = getBcfTravelCartFacade().getTripType(addBundleToCartRequestData.getSelectedJourneyRefNumber());
		if (isCancellationSuccess)
		{
			getBcfTravelCartFacade().removeCartEntriesForRefNo(
					addBundleToCartRequestData.getSelectedJourneyRefNumber(), addBundleToCartRequestData.getSelectedOdRefNumber(),
					tripType.getCode().equals(TripType.SINGLE), false);
		}
		addBundleToCartResponseData.setRemoveSailingResponseDatas(removeSailingResponseDatas);
		addBundleToCartResponseData.setCancellationFail(!isCancellationSuccess);
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	protected CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	@Required
	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

}
