/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.handlers.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.TripType;
import de.hybris.platform.tx.Transaction;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.facades.add.to.cart.handlers.AddToCartHandler;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultModifyBookingHandler implements AddToCartHandler
{

	private BcfTravelCartFacade bcfTravelCartFacade;
	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;
	private UserService userService;
	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;
	private BCFTravelCartService bcfTravelCartService;
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Override
	public void handle(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AddBundleToCartResponseData addBundleToCartResponseData)
			throws IntegrationException, CommerceCartModificationException
	{

		boolean result = false;


		final BCFMakeBookingResponse bcfMakeBookingResponse;
		final UserModel user = getUserService().getCurrentUser();
		if (getUserService().isAnonymousUser(user) || AccountType.SUBSCRIPTION_ONLY
				.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()))
		{
			bcfMakeBookingResponse = getMakeBookingIntegrationFacade().getMakeBookingResponse(addBundleToCartRequestData);
		}
		else
		{
			addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.MODIFICATION_JOURNEY);
			final BCFModifyBookingResponse bcfModifyBookingResponse = getModifyBookingIntegrationFacade()
					.getModifyBookingResponse(addBundleToCartRequestData, BcfCoreConstants.MAKE_BOOKING_REQUEST);
			bcfMakeBookingResponse = bcfModifyBookingResponse.getMakeBookingResponse();
		}
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();


			BookingJourneyType bookingJourneyType=bcfTravelCartService.getSessionCart().getBookingJourneyType();

			if (getBcfTravelCartFacade().getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
					addBundleToCartRequestData.getSelectedOdRefNumber()).size() > 0)
			{
				final TripType originalTripType = getBcfTravelCartFacade()
						.getTripType(addBundleToCartRequestData.getSelectedJourneyRefNumber());


				if(BookingJourneyType.BOOKING_ALACARTE
						.equals(bookingJourneyType)){
					getBcfTravelCartFacade().removeCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							addBundleToCartRequestData.getSelectedOdRefNumber(),
							originalTripType.getCode().equalsIgnoreCase(TripType.SINGLE.getCode()), false);
				}

				else if ( Objects.equals(de.hybris.platform.commercefacades.travel.enums.TripType.SINGLE,
						addBundleToCartRequestData.getTripType()) && Objects.equals(originalTripType, TripType.RETURN))
				{
					getBcfTravelCartFacade().removeCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							addBundleToCartRequestData.getSelectedOdRefNumber(),
							originalTripType.getCode().equalsIgnoreCase(TripType.SINGLE.getCode()), true);
				}
				else
				{
					getBcfTravelCartFacade().removeCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							addBundleToCartRequestData.getSelectedOdRefNumber(),
							originalTripType.getCode().equalsIgnoreCase(TripType.SINGLE.getCode()), false);
				}


			}

			final List<CartModificationData> cartModificationDataList = getBcfTravelCartFacade().updateCart(bcfMakeBookingResponse,
					addBundleToCartRequestData);


			if(BookingJourneyType.BOOKING_ALACARTE
					.equals(bookingJourneyType)){
				getBcfTravelCartFacade().modifyTheOppositeLeg(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
						addBundleToCartRequestData.getSelectedOdRefNumber());
			}

			final BcfAddBundleToCartData bcfAddBundleToCartData = (BcfAddBundleToCartData) addBundleToCartRequestData
					.getAddBundleToCartData().get(0);
			getBcfTravelCartFacadeHelper().addFeesProductToCart(bcfMakeBookingResponse, bcfAddBundleToCartData.getJourneyRefNumber(),
					bcfAddBundleToCartData.getOriginDestinationRefNumber());

			if (addBundleToCartRequestData.isOpenTicket())
			{
				bcfTravelCartFacadeHelper.updateOpenTicketStatus(cartModificationDataList);
			}

			addBundleToCartResponseData.setCartModificationDataList(cartModificationDataList);
			tx.commit();

			final CartModel cart = getBcfTravelCartService().getSessionCart();
			result = true;
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}


	public ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected MakeBookingIntegrationFacade getMakeBookingIntegrationFacade()
	{
		return makeBookingIntegrationFacade;
	}

	@Required
	public void setMakeBookingIntegrationFacade(final MakeBookingIntegrationFacade makeBookingIntegrationFacade)
	{
		this.makeBookingIntegrationFacade = makeBookingIntegrationFacade;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public void setBcfOptionBookingNotificationService(
			final BcfOptionBookingNotificationService bcfOptionBookingNotificationService)
	{
		this.bcfOptionBookingNotificationService = bcfOptionBookingNotificationService;
	}
}
