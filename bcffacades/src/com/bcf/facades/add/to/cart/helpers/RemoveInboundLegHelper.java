/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.helpers;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class RemoveInboundLegHelper
{
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;
	private BcfTravelCartFacade bcfTravelCartFacade;
	private BCFTravelCartService bcfTravelCartService;


	public void removeInboundLeg() throws IntegrationException
	{
		final AbstractOrderModel cart = getBcfTravelCartService().getOrCreateSessionCart();
		final List<AbstractOrderEntryModel> selectedODCartEntries = cart.getEntries().stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
						&& entry.getJourneyReferenceNumber() == cart.getCurrentJourneyRefNum()
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == BcfCoreConstants.INBOUND_REFERENCE_NUMBER)
				.collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(selectedODCartEntries))
		{
			final CartFilterParamsDto cartFilterParamsDto = new CartFilterParamsDto();
			final int journeyRefNum = cart.getCurrentJourneyRefNum();
			if (cancelSailing(journeyRefNum, BcfCoreConstants.INBOUND_REFERENCE_NUMBER))
			{
				cartFilterParamsDto.setJourneyRefNo(journeyRefNum);
				cartFilterParamsDto.setOdRefNo(BcfCoreConstants.INBOUND_REFERENCE_NUMBER);
				cartFilterParamsDto.setCommit(true);
				getBcfTravelCartService()
						.removeEntriesForJourneyODRefNo(getBcfTravelCartService().getOrCreateSessionCart(), cartFilterParamsDto, false,
								false);
			}
		}
	}

	private boolean cancelSailing(final int journeyRefNum, final int odNum) throws IntegrationException
	{
		final boolean isCancellationSuccess;
		final List<CartFilterParamsDto> filterParams = new ArrayList<>();
		filterParams
				.add(getBcfTravelCartFacade().initializeCartFilterParams(journeyRefNum, odNum, false, Collections.emptyList()));
		final List<RemoveSailingResponseData> removeSailingResponseDatas = getCancelBookingIntegrationFacade()
				.cancelSailingsInCart(filterParams, false);
		isCancellationSuccess = removeSailingResponseDatas.get(0).isSuccess();
		return isCancellationSuccess;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected CancelBookingIntegrationFacade getCancelBookingIntegrationFacade()
	{
		return cancelBookingIntegrationFacade;
	}

	@Required
	public void setCancelBookingIntegrationFacade(final CancelBookingIntegrationFacade cancelBookingIntegrationFacade)
	{
		this.cancelBookingIntegrationFacade = cancelBookingIntegrationFacade;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}
}
