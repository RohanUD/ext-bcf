/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.manager.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import java.util.List;
import com.bcf.facades.add.to.cart.handlers.AddToCartHandler;
import com.bcf.facades.add.to.cart.manager.AddToCartPipelineManager;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultAddToCartPipelineManager implements AddToCartPipelineManager
{

	private List<AddToCartHandler> handlers;

	@Override
	public AddBundleToCartResponseData executePipeline(final AddBundleToCartRequestData addBundleToCartRequestData)
			throws CommerceCartModificationException, IntegrationException
	{
		final AddBundleToCartResponseData addBundleToCartResponseData = new AddBundleToCartResponseData();
		for (final AddToCartHandler handler : getHandlers())
		{
			handler.handle(addBundleToCartRequestData, addBundleToCartResponseData);
		}
		return addBundleToCartResponseData;
	}

	protected List<AddToCartHandler> getHandlers()
	{
		return handlers;
	}

	public void setHandlers(final List<AddToCartHandler> handlers)
	{
		this.handlers = handlers;
	}

}
