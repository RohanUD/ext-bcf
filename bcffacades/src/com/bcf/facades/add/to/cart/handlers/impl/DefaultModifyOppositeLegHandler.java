/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.add.to.cart.handlers.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.add.to.cart.handlers.AddToCartHandler;
import com.bcf.facades.cart.AddBundleToCartResponseData;
import com.bcf.facades.order.BcfTravelCartFacade;


public class DefaultModifyOppositeLegHandler implements AddToCartHandler
{
	private BcfTravelCartFacade bcfTravelCartFacade;
	private SessionService sessionService;

	@Override
	public void handle(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AddBundleToCartResponseData addBundleToCartResponseData)
	{
		final String journeyType = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode().equals(journeyType) || BookingJourneyType.BOOKING_ALACARTE.getCode()
				.equals(journeyType))
		{
				getBcfTravelCartFacade().modifyTheOppositeLeg(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
						addBundleToCartRequestData.getSelectedOdRefNumber());
			}

	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	@Required
	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
