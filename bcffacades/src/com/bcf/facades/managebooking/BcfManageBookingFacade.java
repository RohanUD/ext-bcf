/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.managebooking;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.facades.cart.SelectedJourneyData;
import com.bcf.facades.order.BcfTravelCartFacade;


public interface BcfManageBookingFacade extends BcfTravelCartFacade
{

	void updatePassenger(final List<AbstractOrderEntryModel> cartEntries,
			final List<com.bcf.integration.common.data.ProductFare> passengerProducts)
			throws CommerceCartModificationException;

	void amendStatusOfVehicleCartEntries(final AbstractOrderEntryModel presentEntry, final boolean status);

	void addNewVehicleEntry(final AbstractOrderEntryModel cartEntry,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException;

	void updateCartWithNewTravellersDetails(final List<com.bcf.integration.common.data.ProductFare> productFares,
			final int journeyRefNo,
			final int odRefNo) throws CommerceCartModificationException;

	void updateCartWithNewVehicleDetails(final AddBundleToCartRequestData addBundleToCartRequestData,
			final int journeyRefNo, final int odRefNo) throws CommerceCartModificationException;

	void amendVehicleData(final AbstractOrderEntryModel vehicleCartEntry,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException;

	void updateAddBundleToCartRequestUsingCart(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AbstractOrderModel cart);

	AddBundleToCartRequestData createAddBundleToCartRequestDataUsingFareFinder(final int journeyReferenceNumber,
			final int originDestinationRefNumber, final SelectedJourneyData fareFinderForm);

	void updateCartWithDangerousGoodsInfo(final int journeyReferenceNumber,
			final int originDestinationRefNumber, final SelectedJourneyData fareFinderForm);

}
