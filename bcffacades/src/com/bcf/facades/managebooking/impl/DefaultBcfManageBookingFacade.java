/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.managebooking.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.price.data.PriceLevel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.SelectedJourneyData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.managebooking.BcfManageBookingFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;
import com.bcf.facades.traveller.BCFTravellerFacade;


public class DefaultBcfManageBookingFacade extends DefaultBcfTravelCartFacade implements BcfManageBookingFacade
{
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;

	private Converter<TransportOfferingModel, TransportOfferingData> transportOfferingConverter;

	/**
	 * updates the passenger entries in the cart after comparing the passenger entries with the list of products from
	 * make booking response oldsize:size of passenger present in cart newSize:size of passenger after amendment if the
	 * size of passenger entries in a cart ( oldSize ) > 0 ie entries are present in cart if size of passenger in make
	 * booking response( newSize ) > oldSize new passenger is added else if newSize < oldSize remove entries from cart
	 * ie, change the active status to false for the entry.
	 * <p>
	 * ProductNumber in mapEntry represents quantity of the selected product.
	 */
	@Override
	public void updatePassenger(final List<AbstractOrderEntryModel> cartEntries,
			final List<com.bcf.integration.common.data.ProductFare> passengerProducts)
			throws CommerceCartModificationException
	{
		final Map<String, List<com.bcf.integration.common.data.ProductFare>> productFareCodeMap = passengerProducts.stream()
				.collect(Collectors.groupingBy(productFare -> productFare.getProduct().getProductId()));
		for (final Map.Entry<String, List<com.bcf.integration.common.data.ProductFare>> mapEntry : productFareCodeMap.entrySet())
		{
			final List<AbstractOrderEntryModel> presentEntries = cartEntries.stream()
					.filter(cartEntry -> Objects.nonNull(cartEntry.getTravelOrderEntryInfo()))
					.filter(cartEntry -> CollectionUtils.isNotEmpty(cartEntry.getTravelOrderEntryInfo().getTravellers()))
					.filter(cartEntry -> TravellerType.PASSENGER
							.equals(cartEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType())
							&& FareProductModel._TYPECODE.equals(cartEntry.getProduct().getItemtype())
							&& mapEntry.getKey()
							.equals(((PassengerInformationModel) cartEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst()
									.get().getInfo())
									.getPassengerType().getEBookingCode()))
					.collect(Collectors.toList());
			final long newSize = mapEntry.getValue().size();
			final int oldSize = CollectionUtils.size(presentEntries);
			if (oldSize > 0)
			{
				if (newSize > oldSize)
				{
					addNewCartEntry(oldSize, newSize, presentEntries, mapEntry);
					//clone the diff
				}
				else if (oldSize > newSize)
				{
					final long diff = oldSize - newSize;
					amendStatusOfCartEntries(diff, presentEntries);
					// in active the diff
				}
			}
			else if (newSize > 0)
			{
				addNewCartEntry(oldSize, newSize, cartEntries, mapEntry);
			}
		}
		updateCartForRemovedTravellers(cartEntries, productFareCodeMap);

		cartFacade.recalculateCart();

	}


	private void amendStatusOfCartEntries(final long diff, final List<AbstractOrderEntryModel> presentEntries)
	{
		final List<AbstractOrderEntryModel> entriesDeActivated = new ArrayList<>();
		LongStream.range(0, diff).forEach(index -> {
			final AbstractOrderEntryModel presentEntry = presentEntries.get((int) index);
			// amend status should be amended here
			presentEntry.setActive(false);
			presentEntry.setAmendStatus(AmendStatus.CHANGED);
			entriesDeActivated.add(presentEntry);
		});
		getModelService().saveAll(entriesDeActivated);
	}

	protected void addNewCartEntry(final int oldSize, final long newSize, final List<AbstractOrderEntryModel> cartEntries,
			final Map.Entry<String, List<com.bcf.integration.common.data.ProductFare>> mapEntry)
			throws CommerceCartModificationException
	{
		final AbstractOrderEntryModel cartEntry = cartEntries.get(0);
		final List<CartModificationData> cartModificationDatas = new ArrayList<>();
		final Map<Integer, List<AbstractOrderEntryModel>> entriesGroupedByBundleNo = cartEntries.stream()
				.filter(entry -> entry.getActive() && !entry.getProduct().getItemtype().equals(FeeProductModel._TYPECODE))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getBundleNo));
		int bundleNo = Collections.max(entriesGroupedByBundleNo.keySet()) <= 0 ? 2
				: Collections.max(entriesGroupedByBundleNo.keySet());
		final String productCode = cartEntry.getProduct().getCode();
		final String passengercode = getPassengerTypeFacade().getPassengerCodeForEbookingCode(mapEntry.getKey());

		final BcfAddBundleToCartData bcfAddBundleToCartData = getBundleData(cartEntry, productCode);

		final String currentCartCode = getCurrentCartCode();
		Map<String, PriceLevel> productPriceLevelMap = null;
		final List<AddBundleToCartData> addBundleToCartDataList = new ArrayList<>();
		addBundleToCartDataList.add(bcfAddBundleToCartData);

		productPriceLevelMap = getProductPriceLevels(addBundleToCartDataList);

		final PriceLevel priceLevel = productPriceLevelMap
				.get(productCode + "_" + bcfAddBundleToCartData.getOriginDestinationRefNumber());
		for (int count = oldSize + 1; count <= newSize; count++)
		{
			final StringBuilder travellerCode = new StringBuilder(passengercode).append(count)
					.append(cartEntry.getJourneyReferenceNumber());
			TravellerData travellerData = getTravellerData(travellerCode.toString());
			if (travellerData == null)
			{
				travellerData = getTravellerFacade().createTraveller(TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER, passengercode,
						travellerCode.toString(), count, currentCartCode, getCartCode(currentCartCode));
			}
			bundleNo = addProduct(productCode, MINIMUM_PRODUCT_QUANTITY, bundleNo, bcfAddBundleToCartData, travellerData, false,
					priceLevel, true, AmendStatus.NEW, cartModificationDatas);//cartModifications
			bundleNo = Math.abs(bundleNo + 1) * -1;
		}
		cartModificationDatas
				.forEach(modification -> setOrderEntryType(OrderEntryType.TRANSPORT, modification.getEntry().getEntryNumber()));
	}


	protected void updateCartForRemovedTravellers(final List<AbstractOrderEntryModel> cartEntries,
			final Map<String, List<com.bcf.integration.common.data.ProductFare>> productFareCodeMap)
	{
		final List<AbstractOrderEntryModel> presentEntries = cartEntries.stream()
				.filter(cartEntry -> Objects.nonNull(cartEntry.getTravelOrderEntryInfo()))
				.filter(cartEntry -> CollectionUtils.isNotEmpty(cartEntry.getTravelOrderEntryInfo().getTravellers()))
				.filter(cartEntry -> TravellerType.PASSENGER
						.equals(cartEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType())
						&& !productFareCodeMap.containsKey(((PassengerInformationModel) cartEntry.getTravelOrderEntryInfo()
						.getTravellers().stream().findFirst().get().getInfo())
						.getPassengerType().getEBookingCode()))
				.collect(Collectors.toList());
		presentEntries.forEach(cart -> {
			// amend status should be amended here
			cart.setAmendStatus(AmendStatus.CHANGED);
			cart.setActive(false);
		});
		getModelService().saveAll();
	}

	@Override
	public void addNewVehicleEntry(final AbstractOrderEntryModel cartEntry,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException
	{

		final List<CartModificationData> cartModificationDatas = new ArrayList<>();
		int bundleNo = cartEntry.getBundleNo();
		final String productCode = cartEntry.getProduct().getCode();


		final BcfAddBundleToCartData bcfAddBundleToCartData = getBundleData(cartEntry, productCode);

		final String currentCartCode = getCurrentCartCode();
		Map<String, PriceLevel> productPriceLevelMap = null;
		final List<AddBundleToCartData> addBundleToCartDataList = new ArrayList<>();
		addBundleToCartDataList.add(bcfAddBundleToCartData);

		productPriceLevelMap = getProductPriceLevels(addBundleToCartDataList);

		final PriceLevel priceLevel = productPriceLevelMap
				.get(productCode + "_" + bcfAddBundleToCartData.getOriginDestinationRefNumber());

		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = addBundleToCartRequestData.getVehicleTypes();
		for (final VehicleTypeQuantityData vehicleData : vehicleTypeQuantityDataList)
		{
			for (int count = 1; count <= vehicleData.getQty(); count++)
			{
				final StringBuilder travellerCode = new StringBuilder(vehicleData.getVehicleType().getCode()).append(count)
						.append(cartEntry.getJourneyReferenceNumber());
				TravellerData travellerData = getTravellerData(travellerCode.toString());
				if (travellerData == null)
				{
					travellerData = ((BCFTravellerFacade) getTravellerFacade()).createVehicleTraveller(
							BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, vehicleData, travellerCode.toString(), count, currentCartCode,
							getCartCode(currentCartCode));
				}

				bundleNo = addProduct(productCode, MINIMUM_PRODUCT_QUANTITY, bundleNo, bcfAddBundleToCartData, travellerData, false,
						priceLevel, true, AmendStatus.NEW, cartModificationDatas);//cartModifications
				bundleNo = Math.abs(bundleNo + 1) * -1;
			}
			bundleNo = Math.abs(bundleNo + 1) * -1;
		}
		cartModificationDatas
				.forEach(modification -> setOrderEntryType(OrderEntryType.TRANSPORT, modification.getEntry().getEntryNumber()));
	}

	@Override
	public void updateCartWithNewTravellersDetails(final List<com.bcf.integration.common.data.ProductFare> productFares,
			final int journeyRefNo, final int odRefNo)
			throws CommerceCartModificationException
	{
		final List<com.bcf.integration.common.data.ProductFare> passengerProducts = productFares.stream()
				.filter(product -> product.getProduct().getProductCategory().equals(TravellerType.PASSENGER.getCode()))
				.collect(Collectors.toList());
		final List<AbstractOrderEntryModel> cartEntries = getBcfTravelCartService().getCartEntriesForRefNo(journeyRefNo, odRefNo);

		updatePassenger(cartEntries, passengerProducts);
	}

	@Override
	public void updateCartWithNewVehicleDetails(final AddBundleToCartRequestData addBundleToCartRequestData,
			final int journeyRefNo, final int odRefNo) throws CommerceCartModificationException
	{
		final List<AbstractOrderEntryModel> vehicleCartEntries = getBcfTravelCartService()
				.getCartEntriesForRefNo(journeyRefNo, odRefNo).stream()
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo()) && entry.getTravelOrderEntryInfo().getTravellers()
						.stream().findFirst().get().getType().equals(TravellerType.VEHICLE))
				.collect(Collectors.toList());



		if (!CollectionUtils.isEmpty(addBundleToCartRequestData.getVehicleTypes()) && !CollectionUtils.isEmpty(vehicleCartEntries))
		{
			amendVehicleData(vehicleCartEntries.stream().findFirst().get(), addBundleToCartRequestData);
		}
		else if (!CollectionUtils.isEmpty(addBundleToCartRequestData.getVehicleTypes()))
		{
			final List<AbstractOrderEntryModel> cartEntries = getBcfTravelCartService().getCartEntriesForRefNo(journeyRefNo, odRefNo)
					.stream().filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())).collect(Collectors.toList());
			//create new entry
			addNewVehicleEntry(cartEntries.stream().findFirst().get(), addBundleToCartRequestData);
		}
		else if (!CollectionUtils.isEmpty(vehicleCartEntries))
		{
			//inactive entry and amendstatus - changed
			amendStatusOfVehicleCartEntries(vehicleCartEntries.stream().findFirst().get(), false);
		}
		cartFacade.recalculateCart();
	}


	@Override
	public void updateCartWithDangerousGoodsInfo(final int journeyRefNo, final int odRefNo,
			final SelectedJourneyData selectedJourneyData)
	{
		final boolean carryingDangerousGoods;
		if (odRefNo == BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER)
		{
			carryingDangerousGoods = selectedJourneyData.isCarryingDangerousGoodsInOutbound();
		}
		else
		{
			carryingDangerousGoods = selectedJourneyData.isCarryingDangerousGoodsInReturn();
		}
		final List<AbstractOrderEntryModel> entries = cartFacade.getCartEntriesForRefNo(journeyRefNo, odRefNo).stream()
				.filter(AbstractOrderEntryModel::getActive).collect(Collectors.toList());
		entries.forEach(entry -> entry.getTravelOrderEntryInfo().setCarryingDangerousGoods(carryingDangerousGoods));
		entries.stream().filter(entry -> AmendStatus.SAME.equals(entry.getAmendStatus()))
				.forEach(entry -> entry.setAmendStatus(AmendStatus.CHANGED));
		getModelService().saveAll();
	}




	@Override
	public void amendVehicleData(final AbstractOrderEntryModel vehicleCartEntry,
			final AddBundleToCartRequestData addBundleToCartRequestData) throws CommerceCartModificationException
	{


		final VehicleTypeQuantityData vehicleTypeQuantityData = addBundleToCartRequestData.getVehicleTypes().stream().findFirst()
				.get();
		final double height = vehicleTypeQuantityData.getHeight();
		final double width = vehicleTypeQuantityData.getWidth();
		final double length = vehicleTypeQuantityData.getLength();
		final boolean isCarryingLiveStock = vehicleTypeQuantityData.isCarryingLivestock();
		final boolean isVehicleWithSidecarOrTrailer = vehicleTypeQuantityData.isVehicleWithSidecarOrTrailer();
		final String vehicleType = vehicleTypeQuantityData.getVehicleType().getCode();

		final BCFVehicleInformationModel bcfVehicleInfoCartEntry = BCFVehicleInformationModel.class
				.cast(vehicleCartEntry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getInfo());
		final double heightCart = bcfVehicleInfoCartEntry.getHeight();
		final double widthCart = bcfVehicleInfoCartEntry.getWidth();
		final double lengthCart = bcfVehicleInfoCartEntry.getLength();
		final boolean isCarryingLiveStockCart = bcfVehicleInfoCartEntry.getCarryingLivestock();
		final boolean isVehicleWithSidecarOrTrailerCart = bcfVehicleInfoCartEntry.getVehicleWithSidecarOrTrailer();
		final String vehicleTypeCart = bcfVehicleInfoCartEntry.getVehicleType().getType().getCode();

		final boolean dimensionChanged = height != heightCart || width != widthCart || length != lengthCart;
		final boolean vehicleTypeChanged = !vehicleType.equals(vehicleTypeCart);
		final boolean vehicleAddonsChanged = !((isCarryingLiveStock == isCarryingLiveStockCart) && (isVehicleWithSidecarOrTrailer
				== isVehicleWithSidecarOrTrailerCart));


		if (dimensionChanged || vehicleAddonsChanged)
		{
			bcfVehicleInfoCartEntry.setLength(length);
			bcfVehicleInfoCartEntry.setHeight(height);
			bcfVehicleInfoCartEntry.setWidth(width);
			bcfVehicleInfoCartEntry.setCarryingLivestock(isCarryingLiveStock);
			bcfVehicleInfoCartEntry.setVehicleWithSidecarOrTrailer(isVehicleWithSidecarOrTrailer);
			//active-true and amend-status changed
			amendStatusOfVehicleCartEntries(vehicleCartEntry, true);
		}
		if (vehicleTypeChanged)
		{
			//active-false and amend-status changed
			//create new entry
			amendStatusOfVehicleCartEntries(vehicleCartEntry, false);
			addNewVehicleEntry(vehicleCartEntry, addBundleToCartRequestData);
		}
	}


	protected BcfAddBundleToCartData getBundleData(final AbstractOrderEntryModel cartEntry, final String productCode)
	{
		final BcfAddBundleToCartData bcfAddBundleToCartData = new BcfAddBundleToCartData();
		final List<String> transportOferingCodes = new ArrayList<>();
		cartEntry.getTravelOrderEntryInfo().getTransportOfferings()
				.forEach(offering -> transportOferingCodes.add(offering.getCode()));
		bcfAddBundleToCartData.setBundleTemplateId(cartEntry.getBundleTemplate().getId());
		bcfAddBundleToCartData.setTransportOfferings(transportOferingCodes);
		bcfAddBundleToCartData.setTravelRouteCode(cartEntry.getTravelOrderEntryInfo().getTravelRoute().getCode());
		bcfAddBundleToCartData.setOriginDestinationRefNumber(cartEntry.getTravelOrderEntryInfo().getOriginDestinationRefNumber());
		bcfAddBundleToCartData.setJourneyRefNumber(cartEntry.getJourneyReferenceNumber());
		bcfAddBundleToCartData.setCarryingDangerousGoodsInOutbound(cartEntry.getTravelOrderEntryInfo().isCarryingDangerousGoods());
		bcfAddBundleToCartData.setCarryingDangerousGoodsInReturn(false);
		bcfAddBundleToCartData.setProductCode(productCode);

		return bcfAddBundleToCartData;
	}

	@Override
	public void amendStatusOfVehicleCartEntries(final AbstractOrderEntryModel presentEntry, final boolean activestatus)
	{
		// amend status should be amended here
		final BCFVehicleInformationModel bcfVehicleInfoCartEntry = (BCFVehicleInformationModel) presentEntry
				.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getInfo();
		presentEntry.setActive(activestatus);
		presentEntry.setAmendStatus(AmendStatus.CHANGED);
		getModelService().saveAll(presentEntry, bcfVehicleInfoCartEntry);
	}

	@Override
	public void updateAddBundleToCartRequestUsingCart(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AbstractOrderModel cart)
	{
		final BcfAddBundleToCartData addBundleToCartData = ((BcfAddBundleToCartData) addBundleToCartRequestData
				.getAddBundleToCartData().get(0));
		final TravelOrderEntryInfoModel travelOrderEntryInfo = cart.getEntries().stream()
				.filter(entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
						&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == addBundleToCartData
						.getOriginDestinationRefNumber()
						&& entry.getJourneyReferenceNumber() == addBundleToCartData.getJourneyRefNumber())
				.findFirst().get().getTravelOrderEntryInfo();

		addBundleToCartData.setTransportOfferingDatas(Converters.convertAll(
				travelOrderEntryInfo.getTransportOfferings().stream().collect(Collectors.toList()), getTransportOfferingConverter()));

		final String travelRouteCode = travelOrderEntryInfo.getTravelRoute().getCode();
		addBundleToCartData.setTravelRouteCode(travelRouteCode);
	}

	/**
	 * creates AddBundleToCartRequestData using SelectedJourneyData submitted from amend-travelers and re-plan journey
	 * pop ups
	 *
	 * @param journeyReferenceNumber
	 * @param originDestinationRefNumber
	 * @param selectedJourneyData
	 */
	@Override
	public AddBundleToCartRequestData createAddBundleToCartRequestDataUsingFareFinder(final int journeyReferenceNumber,
			final int originDestinationRefNumber, final SelectedJourneyData selectedJourneyData)
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
		final List<AddBundleToCartData> addBundleToCartDatas = new ArrayList<>();
		final BcfAddBundleToCartData addBundleToCart = new BcfAddBundleToCartData();
		addBundleToCartRequestData.setPassengerTypes(selectedJourneyData.getPassengerTypeQuantityList());

		if (selectedJourneyData.isTravellingWithVehicle())
		{
			if (originDestinationRefNumber == 0)
			{
				addBundleToCartRequestData.setVehicleTypes(Arrays.asList(selectedJourneyData.getVehicleInfo().get(0)));
			}
			else
			{
				if (selectedJourneyData.isReturnWithDifferentVehicle())
				{
					addBundleToCartRequestData.setVehicleTypes(Arrays.asList(selectedJourneyData.getVehicleInfo().get(1)));
				}
				else
				{
					addBundleToCartRequestData.setVehicleTypes(Arrays.asList(selectedJourneyData.getVehicleInfo().get(0)));
				}
			}
		}
		addBundleToCart.setOriginDestinationRefNumber(originDestinationRefNumber);
		addBundleToCart.setJourneyRefNumber(journeyReferenceNumber);
		addBundleToCart.setCarryingDangerousGoodsInOutbound(selectedJourneyData.isCarryingDangerousGoodsInOutbound());
		addBundleToCart.setCarryingDangerousGoodsInReturn(selectedJourneyData.isCarryingDangerousGoodsInReturn());
		addBundleToCartDatas.add(addBundleToCart);
		addBundleToCartRequestData.setAddBundleToCartData(addBundleToCartDatas);
		return addBundleToCartRequestData;
	}




	protected BCFPassengerTypeFacade getPassengerTypeFacade()
	{
		return passengerTypeFacade;
	}

	@Required
	public void setPassengerTypeFacade(final BCFPassengerTypeFacade passengerTypeFacade)
	{
		this.passengerTypeFacade = passengerTypeFacade;
	}


	protected Converter<TransportOfferingModel, TransportOfferingData> getTransportOfferingConverter()
	{
		return transportOfferingConverter;
	}

	@Required
	public void setTransportOfferingConverter(
			final Converter<TransportOfferingModel, TransportOfferingData> transportOfferingConverter)
	{
		this.transportOfferingConverter = transportOfferingConverter;
	}
}
