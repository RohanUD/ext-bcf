/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 10/7/19 8:38 AM
 */

package com.bcf.facades.exception;

public class OptionBookingException extends RuntimeException
{
	public OptionBookingException(final String message)
	{
		super(message);
	}
}
