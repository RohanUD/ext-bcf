/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.receipt.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.receipt.PaymentReceiptFacade;
import com.bcf.facades.receipt.ReceiptData;
import com.bcf.integration.payment.data.TransactionDetails;


public class DefaultPaymentReceiptFacade implements PaymentReceiptFacade
{
	private BcfBookingFacade bcfBookingFacade;
	private Converter<PaymentTransactionEntryModel, ReceiptData> successReceiptConverter;
	private Converter<TransactionDetails, ReceiptData> failureReceiptConverter;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Override
	public ReceiptData getReceiptDataForBooking(final String bookingCode)
	{
		final OrderModel order = getBcfBookingFacade().getOrderFromBookingReferenceCode(bookingCode);
		if (Objects.nonNull(order) && StringUtils.equalsIgnoreCase(SalesApplication.TRAVELCENTRE.getCode(),
				getBcfSalesApplicationResolverFacade().getCurrentSalesChannel()) && CollectionUtils
				.isNotEmpty(order.getPaymentTransactions()))
		{
			final PaymentTransactionEntryModel transactionEntry = StreamUtil.safeStream(order.getPaymentTransactions())
					.filter(transaction -> !transaction.isExisting())
					.findFirst().get().getEntries().stream()
					.filter(entry -> PaymentTransactionType.PURCHASE.equals(entry.getType()))
					.findFirst().get();
			if (StringUtils.equalsIgnoreCase(BcfFacadesConstants.Payment.PAYMENT_METHOD_PINPAD,
					transactionEntry.getPaymentProcessingMethod()))
			{
				final ReceiptData receiptData = getSuccessReceiptConverter().convert(transactionEntry);
				if (Objects.nonNull(receiptData))
				{
					receiptData.setCartOrOrderId(bookingCode);
					return receiptData;
				}
			}
		}
		return null;
	}

	@Override
	public ReceiptData getReceiptDataForCart(final TransactionDetails transactionDetails)
	{
		if (StringUtils.equalsIgnoreCase(SalesApplication.TRAVELCENTRE.getCode(),
				getBcfSalesApplicationResolverFacade().getCurrentSalesChannel()) && Objects.nonNull(transactionDetails))
		{
			final ReceiptData receiptData = getFailureReceiptConverter().convert(transactionDetails);
			if (Objects.nonNull(receiptData))
			{
				return receiptData;
			}
		}
		return null;
	}


	protected BcfBookingFacade getBcfBookingFacade()
	{
		return bcfBookingFacade;
	}

	@Required
	public void setBcfBookingFacade(final BcfBookingFacade bcfBookingFacade)
	{
		this.bcfBookingFacade = bcfBookingFacade;
	}

	protected Converter<PaymentTransactionEntryModel, ReceiptData> getSuccessReceiptConverter()
	{
		return successReceiptConverter;
	}

	@Required
	public void setSuccessReceiptConverter(
			final Converter<PaymentTransactionEntryModel, ReceiptData> successReceiptConverter)
	{
		this.successReceiptConverter = successReceiptConverter;
	}

	protected Converter<TransactionDetails, ReceiptData> getFailureReceiptConverter()
	{
		return failureReceiptConverter;
	}

	@Required
	public void setFailureReceiptConverter(
			final Converter<TransactionDetails, ReceiptData> failureReceiptConverter)
	{
		this.failureReceiptConverter = failureReceiptConverter;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

}
