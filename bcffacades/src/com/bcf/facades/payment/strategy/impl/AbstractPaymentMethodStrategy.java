/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.strategy.impl;


import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.services.strategies.BcfPaymentTransactionStrategy;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.policies.RoundingPolicyService;


public abstract class AbstractPaymentMethodStrategy
{

	private UserService userService;
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private BcfPaymentTransactionStrategy paymentTransactionStrategy;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private RoundingPolicyService ceilingRoundingPolicyService;
	private ModelService modelService;

	protected void populateResponse(final Map<String, String> resultMap, final AbstractOrderModel abstractOrderModel,
			final PlaceOrderResponseData decisionData)
	{
		if (StringUtils.isNotEmpty(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT))) {
			final double depositAmount = Double.valueOf(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT));

			final double amountPaid = getAmountPaidForCart(abstractOrderModel);
			final double remainingAmount = BigDecimal.valueOf(depositAmount)
					.subtract(BigDecimal.valueOf(amountPaid)).doubleValue();


			if (remainingAmount < 0) {
				// Customer paid more than the deposit, so we will create the order
				decisionData.setAmountToPay(0.0d);
			} else {
				// There is a balance left to be paid in respect of deposit
				decisionData.setAmountToPay(getCeilingRoundingPolicyService().round(remainingAmount, 2));
			}
		} else {
			// Standard approach, without deposit
			decisionData.setAmountToPay(getRemainingAmount(abstractOrderModel));
		}

		decisionData.setPaymentType(resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE));
	}

	protected boolean isValidAmountSubmitted(final AbstractOrderModel abstractOrderModel, final Double submittedAmount )
	{
		final Double remainingAmountToBePaidBeforeTransaction = getRemainingAmount(abstractOrderModel);
		return remainingAmountToBePaidBeforeTransaction.compareTo(submittedAmount) >= 0;

	}

	protected boolean isValidDepositAmountSubmitted(final AbstractOrderModel abstractOrderModel,final Double submittedAmount, final Double depositAmount)
	{
		final Double remainingAmountToBePaidBeforeTransaction = getRemainingAmount(abstractOrderModel);
		return remainingAmountToBePaidBeforeTransaction.compareTo(depositAmount) >= 0 &&  depositAmount.compareTo(submittedAmount) >= 0;

	}

	protected boolean isValidDepositAmountSubmitted(final AbstractOrderModel abstractOrderModel, final Double depositAmount)
	{
		final Double remainingAmountToBePaidBeforeTransaction = getRemainingAmount(abstractOrderModel);
		return remainingAmountToBePaidBeforeTransaction.compareTo(depositAmount) >= 0;

	}

	protected double getAmountPaid(final AbstractOrderModel abstractOrderModel, final double amountToPay)
	{
		double amountPaid = abstractOrderModel.getAmountPaid()!=null? abstractOrderModel.getAmountPaid(): 0d;
		amountPaid=BigDecimal.valueOf(amountPaid).add(BigDecimal.valueOf(amountToPay)).doubleValue();

		return  getCeilingRoundingPolicyService().round(amountPaid, 2);
	}

	protected double getRemainingAmount(final AbstractOrderModel abstractOrderModel)
	{
		if(CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			return abstractOrderModel.getAmountToPay();
		}

		/**
		 * amount paid is added to handle pay now scenarios because for pay now amount to pay is always less than total price
		 * and there is always some amount already paid. And since we are considering all the payment transactions
		 * this was a necessary step.
		 *
		 * For create order flow, amount paid is always 0 and we only have to consider amount to pay attribute.
		 * It will always work for with deposit and without deposit scenarios as for without deposit scenarios
		 * amount to pay  = total Price and for deposit scenarios
		 * amount to pay = deposit of total price and there is no amount paid
		 */
		final double remainingAmount =abstractOrderModel.getAmountToPay();
		return  getCeilingRoundingPolicyService().round(remainingAmount, 2);
	}

	protected double getAmountPaidForCart(final AbstractOrderModel abstractOrderModel)
	{
		if(CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			return 0.0d;
		}

		final double amountPaid = abstractOrderModel.getPaymentTransactions().stream()
				.flatMap(paymentTransaction -> paymentTransaction.getEntries().stream()).
				filter(paymentTransactionEntry->!paymentTransactionEntry.isExisting() && PaymentTransactionType.PURCHASE.equals(paymentTransactionEntry.getType())).map(paymentTransactionEntry->paymentTransactionEntry.getAmount()).
				reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();

		return  getCeilingRoundingPolicyService().round(amountPaid, 2);
	}


	protected boolean createPaymentTransactionEntries(final AbstractOrderModel orderModel,
			final Double amountToPay, final Map<String, String> resultMap, final BCFPaymentMethodType bCFPaymentMethodType,final PaymentTransactionType paymentTransactionType)
	{
		final CustomerModel customerModel = orderModel instanceof CartModel ?
				getCheckoutCustomerStrategy().getCurrentUserForCheckout() :
				(CustomerModel) getUserService().getCurrentUser();
		final boolean hasCreated = getPaymentTransactionStrategy()
				.createPaymentTransactions(customerModel, amountToPay, paymentTransactionType,
				orderModel, bCFPaymentMethodType,resultMap);


		saveAmount(orderModel, amountToPay);

		return hasCreated;
	}

	protected void saveAmount(final AbstractOrderModel orderModel, final double amountToPay)
	{
		orderModel.setAmountPaid(getAmountPaid(orderModel, amountToPay));
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(orderModel.getBookingJourneyType()))
		{
			orderModel.setAmountToPay(BigDecimal.valueOf(orderModel.getAmountToPay()).subtract(BigDecimal.valueOf(orderModel.getAmountPaid())).doubleValue());
		}
		else
		{
			orderModel.setAmountToPay(BigDecimal.valueOf(orderModel.getTotalPrice()).subtract(BigDecimal.valueOf(orderModel.getAmountPaid())).doubleValue());
		}
		getModelService().save(orderModel);
	}


	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	protected BcfPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	@Required
	public void setPaymentTransactionStrategy(final BcfPaymentTransactionStrategy paymentTransactionStrategy)
	{
		this.paymentTransactionStrategy = paymentTransactionStrategy;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected RoundingPolicyService getCeilingRoundingPolicyService()
	{
		return ceilingRoundingPolicyService;
	}

	@Required
	public void setCeilingRoundingPolicyService(final RoundingPolicyService ceilingRoundingPolicyService)
	{
		this.ceilingRoundingPolicyService = ceilingRoundingPolicyService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
