/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.order.BcfCheckoutService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.payment.strategy.PrePaymentCartValidationHandler;
import com.bcf.facades.travel.data.PrePaymentCartValidationResponseData;


public class DepartureTimeThresholdValidationHandler implements PrePaymentCartValidationHandler
{
	private static final Integer OUTBOUND_ODREF_NUM = 0;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BCFTravelCartService bcfTravelCartService;
	private TimeService timeService;
	private BcfCheckoutService bcfCheckoutService;
	private SessionService sessionService;

	@Override
	public PrePaymentCartValidationResponseData validate(final AbstractOrderModel cartModel)
	{
		final int thresholdTimeGap = getBcfTravelCartService().getThresholdTimeGap(cartModel);
		final String sessionBookingJourney = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		final PrePaymentCartValidationResponseData prePaymentCartValidationResponseData = new PrePaymentCartValidationResponseData();
		if (StringUtils.equals(sessionBookingJourney, TravelfacadesConstants.BOOKING_TRANSPORT_ONLY))
		{
			final List<AbstractOrderEntryModel> entries = cartModel.getEntries().stream().filter(
					entry -> entry.getActive()
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == OUTBOUND_ODREF_NUM)
					.collect(Collectors.toList());
			for (final AbstractOrderEntryModel entry : entries)
			{
				final Optional<TransportOfferingModel> transportOfferingModel = entry.getTravelOrderEntryInfo()
						.getTransportOfferings()
						.stream().findFirst();
				final boolean satisfiesThreshold = checkDepartureTimeRestrictionGap(transportOfferingModel, thresholdTimeGap);
				if (!satisfiesThreshold && transportOfferingModel.isPresent())
				{
					prePaymentCartValidationResponseData
							.setOrigin(transportOfferingModel.get().getOriginTransportFacility().getCode());
					prePaymentCartValidationResponseData
							.setDestination(transportOfferingModel.get().getDestinationTransportFacility().getCode());
					prePaymentCartValidationResponseData
							.setArrivalTime(TravelDateUtils.convertDateToStringDate(transportOfferingModel.get().getArrivalTime(),
									BcfFacadesConstants.BCF_SAILING_DATE_TIME_PATTERN));
					prePaymentCartValidationResponseData
							.setDepartureTime(TravelDateUtils.convertDateToStringDate(transportOfferingModel.get().getDepartureTime(),
									BcfFacadesConstants.BCF_SAILING_DATE_TIME_PATTERN));
					prePaymentCartValidationResponseData.setValid(false);
					if (getBcfCheckoutService().checkSailingTimeThresholdOverrideApplicable())
					{
						prePaymentCartValidationResponseData.setNeedConfirmation(true);
					}
					return prePaymentCartValidationResponseData;
				}
			}
		}
		prePaymentCartValidationResponseData.setValid(true);
		return prePaymentCartValidationResponseData;
	}


	public boolean checkDepartureTimeRestrictionGap(final Optional<TransportOfferingModel> transportOffering,
			final int thresholdTimeGap)
	{
		boolean satisfiesThreshold = false;
		if (transportOffering.isPresent())
		{
			satisfiesThreshold = TravelDateUtils.isAfter(transportOffering.get().getDepartureTime(),
					ZoneId.of(transportOffering.get().getTravelSector().getOrigin().getPointOfService().get(0).getTimeZoneId()),
					DateUtils.addMinutes(getTimeService().getCurrentTime(), thresholdTimeGap),
					ZoneId.systemDefault());
		}
		return satisfiesThreshold;
	}



	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	protected BcfCheckoutService getBcfCheckoutService()
	{
		return bcfCheckoutService;
	}

	@Required
	public void setBcfCheckoutService(final BcfCheckoutService bcfCheckoutService)
	{
		this.bcfCheckoutService = bcfCheckoutService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
