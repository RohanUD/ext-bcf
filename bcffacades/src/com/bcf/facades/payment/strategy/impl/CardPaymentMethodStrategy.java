/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.strategies.BcfCreditCardPaymentInfoCreateStrategy;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.handler.AbstractTransactionHandler;
import com.bcf.facades.payment.strategy.PaymentMethodStrategy;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;


public class CardPaymentMethodStrategy extends AbstractPaymentMethodStrategy implements PaymentMethodStrategy
{
	private static final Logger LOG = Logger.getLogger(CardPaymentMethodStrategy.class);
	private Map<String, AbstractTransactionHandler> handlersMap;
	private BcfCreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy;
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public void handlePayment(final Map<String, String> resultMap, final AbstractOrderModel orderModel,
			final PlaceOrderResponseData decisionData)
			throws IntegrationException, OrderProcessingException
	{
		LOG.debug(
				String.format("Processing payment of amount [%s] for order [%s] and sales channel is [%s] ",
						orderModel.getAmountToPay(),
						orderModel.getCode(), resultMap.get(BcfFacadesConstants.SALES_CHANNEL)));
		final TransactionDetails transactionDetails;
		final double amountToPay;

		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT)))
		{
			final double depositAmount = Double.valueOf(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT));


			if (!isValidDepositAmountSubmitted(orderModel, depositAmount))
			{
				decisionData.setValid(false);
				decisionData.setAmountToPay(0d);
				decisionData.setError(BcfCoreConstants.INVALID_DEPOSIT_ERROR);
				return;
			}
			amountToPay = depositAmount;
			transactionDetails = createPaymentTransactionEntries(orderModel, PaymentTransactionType.PURCHASE, resultMap,
					amountToPay);
		}

		else if (getBcfTravelCartService().isAmendmentCart(orderModel))
		{
			amountToPay = orderModel.getAmountToPay();
			transactionDetails = createPaymentTransactionEntries(orderModel,
					PaymentTransactionType.PURCHASE, resultMap, orderModel.getAmountToPay());
		}
		else
		{

			amountToPay =
					getRemainingAmount(orderModel);

			transactionDetails = createPaymentTransactionEntries(orderModel, PaymentTransactionType.PURCHASE, resultMap,
					amountToPay);
		}
		decisionData.setTransactionDetails(transactionDetails);
		if (transactionDetails.getIsSuccessful())
		{
			LOG.debug(
					String.format("Payment of amount [%s] for order [%s] is successful from PaymentService payment",
							amountToPay, orderModel.getCode()));
			saveAmount(orderModel, amountToPay);
			return;
		}
		handleRollback(orderModel, decisionData, transactionDetails);
		return;
	}

	protected TransactionDetails createPaymentTransactionEntries(final AbstractOrderModel orderModel,
			final PaymentTransactionType paymentTransactionType,
			final Map<String, String> resultMap, final double amountToPay) throws IntegrationException
	{
		final TransactionDetails transactionDetails = getHandlersMap().get(resultMap.get(BcfFacadesConstants.SALES_CHANNEL))
				.handlePayment(resultMap, amountToPay);

		/**
		 * TODO this below setter needs to be removed onces we integrate with real services
		 * or the whole creation of payment transaction needs to be modified for card payment method
		 */
		transactionDetails.setTransactionAmountInCents(Math.round(amountToPay * 100));
		if (transactionDetails.getIsSuccessful())
		{
			final CustomerModel customerModel = orderModel instanceof CartModel ?
					getCheckoutCustomerStrategy().getCurrentUserForCheckout() :
					(CustomerModel) getUserService().getCurrentUser();

			final boolean isTransactionCreated = getPaymentTransactionStrategy()
					.createPaymentTransactions(customerModel, transactionDetails, paymentTransactionType, BCFPaymentMethodType.CARD,
							orderModel, resultMap, null, false);
			transactionDetails.setIsSuccessful(isTransactionCreated);
		}
		return transactionDetails;
	}

	private void handleRollback(final AbstractOrderModel orderModel, final PlaceOrderResponseData decisionData,
			final TransactionDetails transactionDetails)
			throws OrderProcessingException
	{
		if (orderModel instanceof CartModel)
		{
			getBcfTravelCommerceStockService().releaseStocks((CartModel) orderModel);
		}
		decisionData.setValid(false);
		decisionData.setMethod(transactionDetails.getCallerMethod());
		decisionData.setError(transactionDetails.getBcfResponseDescription());
		decisionData.setResponseCode(transactionDetails.getBcfResponseCode());
		decisionData.setPaymentType(transactionDetails.getPaymentProcessingMethod());
		decisionData.setOperatorDisplayMessage(transactionDetails.getOperatorDisplayMessage());
		decisionData.setCustomerDisplayMessage(transactionDetails.getCustomerDisplayMessage());
		decisionData.setApprovalOrDeclineMessage(transactionDetails.getApprovalOrDeclineMessage());
		LOG.error("Exception encountered while processing payment");
		throw new OrderProcessingException("Exception encountered while payment processing");
	}

	protected Map<String, AbstractTransactionHandler> getHandlersMap()
	{
		return handlersMap;
	}

	@Required
	public void setHandlersMap(final Map<String, AbstractTransactionHandler> handlersMap)
	{
		this.handlersMap = handlersMap;
	}

	protected BcfCreditCardPaymentInfoCreateStrategy getCreditCardPaymentInfoCreateStrategy()
	{
		return creditCardPaymentInfoCreateStrategy;
	}

	@Required
	public void setCreditCardPaymentInfoCreateStrategy(
			final BcfCreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy)
	{
		this.creditCardPaymentInfoCreateStrategy = creditCardPaymentInfoCreateStrategy;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
