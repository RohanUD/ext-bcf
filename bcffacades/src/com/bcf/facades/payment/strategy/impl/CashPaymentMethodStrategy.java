/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.strategy.PaymentMethodStrategy;


public class CashPaymentMethodStrategy extends AbstractPaymentMethodStrategy implements PaymentMethodStrategy
{
	private static final Logger LOG = Logger.getLogger(CashPaymentMethodStrategy.class);

	@Override
	public void handlePayment(final Map<String, String> resultMap, final AbstractOrderModel orderModel,
			final PlaceOrderResponseData decisionData)
	{
		LOG.debug(
				String.format("Processing payment of amount [%s] for order [%s] and sales channel is [%s] ",
						orderModel.getAmountToPay(),
						orderModel.getCode(), resultMap.get(BcfFacadesConstants.SALES_CHANNEL)));

		final Double amountToPay = Double.parseDouble(resultMap.get("amount"));

		if (!isValidAmountSubmitted(orderModel, amountToPay))
		{
			decisionData.setValid(false);
			decisionData.setAmountToPay(0d);
			decisionData.setError(BcfCoreConstants.INVALID_PAYMENT_ERROR);
			return;
		}
		if (StringUtils.isNotEmpty(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT)))
		{
			if (!isValidDepositAmountSubmitted(orderModel,amountToPay, Double.valueOf(resultMap.get(BcfFacadesConstants.Payment.DEPOSIT_AMOUNT))))
			{
				decisionData.setValid(false);
				decisionData.setAmountToPay(0d);
				decisionData.setError(BcfCoreConstants.INVALID_DEPOSIT_ERROR);
				return;
			}
		}


		/**
		 * only one cash transaction is allowed and hence the boolean isTransactionCreated. the logic to check is inside createPaymentTransactionEntries method
		 */
		final boolean isTransactionCreated = createPaymentTransactionEntries(orderModel, amountToPay, resultMap,
				BCFPaymentMethodType.CASH,
				PaymentTransactionType.PURCHASE);
		if (!isTransactionCreated)
		{
			decisionData.setValid(false);
			decisionData.setError(BcfCoreConstants.PAYMENT_ERROR);
			return;
		}
		LOG.debug(
				String.format("Payment of amount [%s] for order [%s] is successful from PaymentService payment",
						orderModel.getAmountToPay(), orderModel.getCode()));
		populateResponse(resultMap, orderModel, decisionData);

		return;
	}
}
