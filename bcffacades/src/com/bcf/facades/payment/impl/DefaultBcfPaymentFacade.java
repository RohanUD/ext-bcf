/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.impl;

import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.order.CommerceCardTypeService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.CardType;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.payment.PartialPaymentInfoData;
import com.bcf.core.paymnet.BcfPaymentOptionsData;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.guarantee.GuaranteeService;
import com.bcf.core.services.price.calculation.impl.BcfCalculationService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.payment.BcfPaymentFacade;


public class DefaultBcfPaymentFacade extends DefaultPaymentFacade implements BcfPaymentFacade
{
	private BCFTravelCartService bcfTravelCartService;

	private GuaranteeService guaranteeService;

	private TravelCommercePriceFacade travelCommercePriceFacade;

	private BcfCalculationService bcfCalculationService;

	private BcfBookingService bcfBookingService;

	private Converter<CardType, CardTypeData> cardTypeConverter;

	private CommerceCardTypeService commerceCardTypeService;

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public PriceData getPendingPaymentForOrder(final String orderCode)
	{
		final OrderModel orderModel = getBcfBookingService().getOrder(orderCode);
		if (Objects.isNull(orderModel))
		{
			return null;
		}
		double amountToPay = 0.0d;
		final List<DealOrderEntryGroupModel> dealOrderEntryGroupModels = getBcfBookingService().getDealOrderEntryGroups(orderModel);
		if (CollectionUtils.isEmpty(dealOrderEntryGroupModels))
		{
			return getTravelCommercePriceFacade().createPriceData(amountToPay);
		}

		return getTravelCommercePriceFacade().createPriceData(amountToPay);
	}

	protected BcfPaymentOptionsData getDefaultPaymentOptionsData(final AbstractOrderModel abstractOrderModel,
			final boolean hasUserSelectedPaymentOption)
	{
		final double totalAmount  = abstractOrderModel.getTotalPrice() + abstractOrderModel.getTotalTax();
		final BcfPaymentOptionsData bcfPaymentOptionsData = new BcfPaymentOptionsData();
		final PartialPaymentInfoData paymentInfoData = new PartialPaymentInfoData();
		paymentInfoData.setSelected(!hasUserSelectedPaymentOption);
		paymentInfoData.setTotalAmount(getTravelCommercePriceFacade().createPriceData(totalAmount));
		paymentInfoData
				.setAmountToPay(getTravelCommercePriceFacade()
						.createPriceData(totalAmount));
		bcfPaymentOptionsData.setPaymentOptions(Collections.singletonList(paymentInfoData));
		bcfPaymentOptionsData.setSelected(!hasUserSelectedPaymentOption);
		bcfPaymentOptionsData.setName(BcfCoreConstants.DEFAULT_PAYMENT_OPTION);
		return bcfPaymentOptionsData;
	}

	@Override
	public Collection<CardTypeData> getPaymentCardTypes()
	{
		final Collection<CardTypeData> paymentCardTypes = new ArrayList<>();

		final List<CardTypeData> availableCardTypes = getCardTypeConverter()
				.convertAll(getCommerceCardTypeService().getCardTypes());
		for (final CardTypeData availableCardType : availableCardTypes)
		{
			final String propertyValue = bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfFacadesConstants.Payment.BCF_SUPPORTED_CARD_TYPES);
			final List<String> supportedCardTypes = Stream.of(propertyValue.split(",")).collect(Collectors.toList());

			if (supportedCardTypes.contains(availableCardType.getCode()))
			{
				paymentCardTypes.add(
						createCardTypeData(availableCardType.getCode(), availableCardType.getName()));
			}
		}
		return paymentCardTypes;
	}

	private CardTypeData createCardTypeData(final String code, final String name)
	{
		final CardTypeData cardTypeData = new CardTypeData();
		cardTypeData.setCode(code);
		cardTypeData.setName(name);
		return cardTypeData;
	}

	protected double getDealValue(final List<AbstractOrderEntryModel> entryModels)
	{
		final double totalTax = entryModels.get(0).getOrder().getTotalTax();
		return entryModels.stream().mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum() + totalTax ;
	}

	/**
	 * @return the bcfTravelCartService
	 */
	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}



	protected GuaranteeService getGuaranteeService()
	{
		return guaranteeService;
	}

	@Required
	public void setGuaranteeService(final GuaranteeService guaranteeService)
	{
		this.guaranteeService = guaranteeService;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return this.travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected BcfCalculationService getBcfCalculationService()
	{
		return bcfCalculationService;
	}

	@Required
	public void setBcfCalculationService(final BcfCalculationService bcfCalculationService)
	{
		this.bcfCalculationService = bcfCalculationService;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public Converter<CardType, CardTypeData> getCardTypeConverter()
	{
		return cardTypeConverter;
	}

	public void setCardTypeConverter(
			final Converter<CardType, CardTypeData> cardTypeConverter)
	{
		this.cardTypeConverter = cardTypeConverter;
	}

	public CommerceCardTypeService getCommerceCardTypeService()
	{
		return commerceCardTypeService;
	}

	public void setCommerceCardTypeService(final CommerceCardTypeService commerceCardTypeService)
	{
		this.commerceCardTypeService = commerceCardTypeService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
