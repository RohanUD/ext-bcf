/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.BcfRefundIntegrationFacade;
import com.bcf.integration.payment.exceptions.InvalidPaymentInfoException;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public class DefaultBcfRefundIntegrationFacade implements BcfRefundIntegrationFacade
{
	private BcfRefundIntegrationService bcfRefundIntegrationService;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private ModelService modelService;
	private static final Logger LOG = Logger.getLogger(DefaultBcfRefundIntegrationFacade.class);
	private static final String INTEGRATION_EXCEPTION_MESSAGE = "Integration exception encountered for refundWithToken";
	private static final String INVALID_PAYMENTINFO_EXCEPTION_MESSAGE = "processing refund failed, original card payment transaction does not exist";

	@Override
	public PlaceOrderResponseData refundOrder(final AbstractOrderModel order)
	{
		PaymentResponseDTO paymentResponseDTO = null;
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		try
		{
			paymentResponseDTO = getBcfRefundIntegrationService().refundOrder(order);
		}
		catch (final IntegrationException e)
		{
			LOG.error(INTEGRATION_EXCEPTION_MESSAGE);
			decisionData.setError(INTEGRATION_EXCEPTION_MESSAGE);
			return decisionData;
		}
		catch (final InvalidPaymentInfoException e)
		{
			LOG.error(INVALID_PAYMENTINFO_EXCEPTION_MESSAGE);
			decisionData.setError(INVALID_PAYMENTINFO_EXCEPTION_MESSAGE);
			return decisionData;
		}
		final boolean responseStatus = paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus();
		final String responseCode = paymentResponseDTO.getResponseStatus().getBcfResponseCode();
		final String responseDesc = paymentResponseDTO.getResponseStatus().getBcfResponseDescription();
		if (responseStatus && StringUtils
				.equals(responseCode, getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode")))
		{
			decisionData.setError(null);
			final double txnAmount = paymentResponseDTO.getTransactionInfoResponse().getTransactionAmountInCents() / 100d;
			updateAmounts(order, txnAmount);
			return decisionData;
		}
		LOG.error("Refund Request failed");
		decisionData.setResponseCode(responseCode);
		decisionData.setError(responseDesc);
		decisionData.setMethod(BcfFacadesConstants.REFUND_ORDER);
		return decisionData;
	}


	@Override
	public PlaceOrderResponseData refundOrder(final AbstractOrderModel abstractOrderModel,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas)
	{
		List<PaymentResponseDTO> paymentResponseDTOs = null;
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		try
		{
			paymentResponseDTOs = getBcfRefundIntegrationService().processRefund(abstractOrderModel, refundTransactionInfoDatas);
		}
		catch (final IntegrationException e)
		{
			LOG.error(INTEGRATION_EXCEPTION_MESSAGE);
			decisionData.setError(INTEGRATION_EXCEPTION_MESSAGE);
			return decisionData;
		}
		catch (final InvalidPaymentInfoException e)
		{
			LOG.error(INVALID_PAYMENTINFO_EXCEPTION_MESSAGE);
			decisionData.setError(INVALID_PAYMENTINFO_EXCEPTION_MESSAGE);
			return decisionData;
		}
		if (CollectionUtils.isNotEmpty(paymentResponseDTOs))
		{

			for (final PaymentResponseDTO paymentResponseDTO : paymentResponseDTOs)
			{

				final boolean responseStatus = paymentResponseDTO.getResponseStatus()
						.getBcfPaymentResponseBooleanStatus();
				final String responseCode = paymentResponseDTO.getResponseStatus().getBcfResponseCode();
				final String responseDesc = paymentResponseDTO.getResponseStatus().getBcfResponseDescription();
				if (!responseStatus
						&& StringUtils
						.equals(responseCode, getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode")))
				{

					LOG.error("Refund Request failed");
					decisionData.setResponseCode(responseCode);
					decisionData.setError(responseDesc);
					decisionData.setMethod(BcfFacadesConstants.REFUND_ORDER);
					return decisionData;
				}

			}
		}
		decisionData.setError(null);

		double totalRefunded=refundTransactionInfoDatas.stream().mapToDouble(refundTransactionInfoData->refundTransactionInfoData.getAmount()).sum();

		abstractOrderModel.setAmountToPay(0d);
		abstractOrderModel.setAmountPaid(abstractOrderModel.getAmountPaid()-totalRefunded);
		modelService.save(abstractOrderModel);

		return decisionData;


	}

	protected void updateAmounts(final AbstractOrderModel order, final double amountRefunded)
	{
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(order.getBookingJourneyType()))
		{
			//assuming in transport only bookings, user will never pay more than amount to pay now
			order.setAmountPaid(
					BigDecimal.valueOf(order.getAmountPaid()).subtract(BigDecimal.valueOf(amountRefunded)).doubleValue());
			order.setAmountToPay(amountRefunded);
		}
		getModelService().save(order);
	}

	protected BcfRefundIntegrationService getBcfRefundIntegrationService()
	{
		return bcfRefundIntegrationService;
	}

	@Required
	public void setBcfRefundIntegrationService(final BcfRefundIntegrationService bcfRefundIntegrationService)
	{
		this.bcfRefundIntegrationService = bcfRefundIntegrationService;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
