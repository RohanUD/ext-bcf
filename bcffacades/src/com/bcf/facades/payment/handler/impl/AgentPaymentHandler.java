/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.handler.impl;

import java.util.Map;
import com.bcf.facades.payment.handler.AbstractTransactionHandler;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;


public class AgentPaymentHandler extends AbstractTransactionHandler
{
   @Override
   public TransactionDetails handlePayment(final Map<String, String> resultMap, final double amountToPay)
         throws IntegrationException
   {
         return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.PAYMENT_SERVICE_URL);
      }

}
