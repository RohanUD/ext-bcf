/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.handler.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.payment.handler.AbstractTravelCentreTransactionHandler;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;
import com.bcf.processpayment.request.data.ProcessingParameters;


public class TravelCentrePaymentHandler extends AbstractTravelCentreTransactionHandler
{
	@Override
	public TransactionDetails handlePayment(final Map<String, String> resultMap, final double amountToPay)
			throws IntegrationException
	{

		final String paymentType = resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE);

		if (paymentType.equals(BcfFacadesConstants.Payment.PAYMENT_TYPE_CARD_PRESENT))
		{
			if (StringUtils.equals(resultMap.get(BcfCoreConstants.ON_REQUEST_ORDER), BcfCoreConstants.PACKAGE_ON_REQUEST))
			{
				return handlePermanentTokenRequest(resultMap);
			}

			return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.PAYMENT_SERVICE_URL);
		}
		else if (paymentType.equals(BcfFacadesConstants.Payment.PAYMENT_TYPE_CARD_NOT_PRESENT))
		{
			return processCardNotPresentPayment(amountToPay, resultMap);
		}
		else if (paymentType.equals(BcfFacadesConstants.Payment.PAYMENT_TYPE_SAVED_CARD_PRESENT))
		{
			return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.PAYMENT_SERVICE_URL);
		}
		else if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)))
		{
			return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.CTC_CARD_PAYMENT_SERVICE_URL);
		}

		return null;
	}

	@Override
	protected ProcessingParameters createProcessingParameters(final Map<String, String> resultMap)
	{
		final ProcessingParameters processingParameters = super.createProcessingParameters(resultMap);
		if (StringUtils.equals(resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE),
				BcfFacadesConstants.Payment.PAYMENT_TYPE_CARD_PRESENT))
		{

			processingParameters.setPaymentProcessingMethod(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.PAYMENT_PROCESSING_METHOD));
			processingParameters.setReceiptInfoRequested(Boolean.TRUE);

		}
		return processingParameters;
	}

	@Override
	protected PaymentSourceSystemInfo createSourceSystemInfo(final Map<String, String> resultMap)
	{
		final PaymentSourceSystemInfo sourceSystemInfo = new PaymentSourceSystemInfo();
		final String applicationId = getPaymentApplicationIdMap().get(resultMap.get(BookingJourneyType._TYPECODE))
				.get(resultMap.get(BcfFacadesConstants.SALES_CHANNEL));
		sourceSystemInfo
				.setAppliId(applicationId);
		sourceSystemInfo
				.setLocationId(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfintegrationserviceConstants.LOCATION_ID));
		if (StringUtils.equalsIgnoreCase(SalesApplication.TRAVELCENTRE.getCode(), resultMap.get(BcfFacadesConstants.SALES_CHANNEL))
				&& resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE).equals("cardNotPresent"))
		{
			sourceSystemInfo.setAgentId(
					getSessionService().getCurrentSession().getAttribute(BcfintegrationserviceConstants.SESSION_ICEBAR_ID));
		}
		else
		{
			sourceSystemInfo.setStationId(
					getSessionService().getCurrentSession().getAttribute(BcfintegrationserviceConstants.SESSION_PINPAD_ID));
		}
		sourceSystemInfo
				.setPassword(getPaymentClientCodePasswordMap().get(applicationId));
		return sourceSystemInfo;
	}
}
