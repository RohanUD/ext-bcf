/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.handler.impl;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.impl.CustomBCFCustomerFacade;
import com.bcf.facades.payment.handler.AbstractTransactionHandler;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.PaymentRequestDTO;


public class WebPaymentHandler extends AbstractTransactionHandler
{
	@Resource(name = "travelCustomerFacade")
	private CustomBCFCustomerFacade travelCustomerFacade;

	@Override
	public TransactionDetails handlePayment(final Map<String, String> resultMap, final double amountToPay)
			throws IntegrationException
	{
		resultMap.put(BcfFacadesConstants.Payment.TOKEN, resultMap.get(BcfFacadesConstants.Payment.CARD_NUMBER));
		resultMap.put(BcfFacadesConstants.Payment.TOKEN_TYPE,
				getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.TOKEN_TYPE_TEMPORARY));

		if (StringUtils.equals(resultMap.get(BcfCoreConstants.ON_REQUEST_ORDER), BcfCoreConstants.PACKAGE_ON_REQUEST))
		{
			if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE)))
			{
				return makePaymentToVaultUsingSavedCard(resultMap);
			}
			return makePaymentToVault(resultMap, BcfintegrationserviceConstants.PAYMENT_VAULT_SERVICE_URL);
		}
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)))
		{
			return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.CTC_CARD_PAYMENT_SERVICE_URL);
		}

		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.SAVE_CC_CARD)))
		{
			final PaymentInfoModel paymentInfoModel = travelCustomerFacade.addPaymentCard(resultMap);
			if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
			{
				final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) paymentInfoModel;
				resultMap.put(BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE, creditCardPaymentInfoModel.getPk().toString());
			}

		}

		return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.PAYMENT_SERVICE_URL);
	}

	@Override
	protected PaymentRequestDTO createPaymentRequestDTO(final Map<String, String> resultMap, final double amountToPay)
	{
		final PaymentRequestDTO paymentRequestDTO = super.createPaymentRequestDTO(resultMap, amountToPay);
		if (StringUtils.isBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)))
		{
			paymentRequestDTO.setProcessingParameters(createProcessingParameters(resultMap));
		}
		return paymentRequestDTO;
	}
}
