/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.handler;

import java.util.Map;
import org.apache.commons.lang.StringUtils;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentRequestDTO;


public abstract class AbstractTravelCentreTransactionHandler extends AbstractTransactionHandler
{
	@Override
	protected PaymentRequestDTO createPaymentRequestDTO(final Map<String, String> resultMap, final double amountToPay)
	{
		final PaymentRequestDTO paymentRequestDTO = super.createPaymentRequestDTO(resultMap, amountToPay);
		paymentRequestDTO.setProcessingParameters(createProcessingParameters(resultMap));
		return paymentRequestDTO;
	}

	@Override
	protected CardInfoRequest createCardInfoRequest(final Map<String, String> resultMap)
	{
		final CardInfoRequest cardInfoRequest = new CardInfoRequest();
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE)))
		{
			return super.createCardInfoRequest(resultMap);
		}
		else
		{
			cardInfoRequest.setExpectedCardType(getConfigurationService().getConfiguration()
					.getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
		}
		return cardInfoRequest;
	}
}
