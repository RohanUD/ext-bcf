/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.handler;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.payment.handler.impl.WebPaymentHandler;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.TokenRequestDTO;
import com.bcf.integration.data.TokenResponseDTO;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integration.payment.service.BcfPaymentIntegrationService;
import com.bcf.integration.payment.service.BcfPaymentVaultIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.CardDate;
import com.bcf.processpayment.request.data.CardIdentifier;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentCardAvsInfoInfo;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;
import com.bcf.processpayment.request.data.PaymentVaultRequestDTO;
import com.bcf.processpayment.request.data.ProcessingParameters;
import com.bcf.processpayment.request.data.TokenRef;
import com.bcf.processpayment.request.data.TransactionInfoRequest;
import com.bcf.processpayment.response.data.CardInfoResponse;
import com.bcf.processpayment.response.data.CardRef;
import com.bcf.processpayment.response.data.PaymentResponseDTO;
import com.bcf.processpayment.response.data.PaymentVaultResponseDTO;


public abstract class AbstractTransactionHandler
{
	private static final Logger LOG = Logger.getLogger(WebPaymentHandler.class);
	private static final String GET_TOKEN_REQUEST = "GetTokenRequest";
	private ConfigurationService configurationService;
	private BcfPaymentIntegrationService bcfPaymentIntegrationService;
	private BcfPaymentVaultIntegrationService bcfPaymentVaultIntegrationService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter;
	private Converter<PaymentVaultResponseDTO, TransactionDetails> paymentVaultResponseDTOConverter;
	private SessionService sessionService;
	private Map<String, Map<String, String>> paymentApplicationIdMap;
	private Map<String, String> paymentClientCodePasswordMap;
	private static final String PAYMENT_SUCCESS_CODE = "paymentSuccessCode";
	private static final String SPACE = " ";

	@Resource(name = "customerAccountService")
	private BcfCustomerAccountService customerAccountService;

	@Resource(name = "userService")
	private UserService userService;

	public abstract TransactionDetails handlePayment(Map<String, String> resultMap, double amountToPay)
			throws IntegrationException;

	protected PaymentVaultRequestDTO createPaymentVaultRequestDTO(final Map<String, String> resultMap)
	{
		final PaymentVaultRequestDTO paymentVaultRequestDTO = new PaymentVaultRequestDTO();
		paymentVaultRequestDTO.setSourceSystemInfo(createSourceSystemInfo(resultMap));
		paymentVaultRequestDTO.setCardAvsInfo(createAvsAddress(resultMap));
		paymentVaultRequestDTO.setCardIdentifier(createVaultCardInfoRequest(resultMap).getCardIdentifier());
		final CardDate cardExpiryDate = new CardDate();
		if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION))
		{
			cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(0, 2));
			cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(3));
		}
		else if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH) && resultMap
				.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR))
		{
			cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH));
			cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR).substring(2));
		}

		paymentVaultRequestDTO.setCardExpiryDate(cardExpiryDate);

		return paymentVaultRequestDTO;
	}

	private PaymentCardAvsInfoInfo createAvsAddress(final Map<String, String> resultMap)
	{
		PaymentCardAvsInfoInfo paymentCardAvsInfoInfo = null;
		if (resultMap.get(BcfFacadesConstants.Payment.AVS_STREET_NAME) != null)
		{
			paymentCardAvsInfoInfo = new PaymentCardAvsInfoInfo();
			paymentCardAvsInfoInfo.setAvsStreetName(resultMap.get(BcfFacadesConstants.Payment.AVS_STREET_NAME));
			paymentCardAvsInfoInfo.setAvsStreetNumber(resultMap.get(BcfFacadesConstants.Payment.AVS_STREET_NO));
			paymentCardAvsInfoInfo.setAvsZipCode(resultMap.get(BcfFacadesConstants.Payment.AVS_ZIPCODE));
		}
		return paymentCardAvsInfoInfo;
	}

	protected ProcessingParameters createProcessingParameters(final Map<String, String> resultMap) //NOSONAR
	{
		final ProcessingParameters processingParameters = new ProcessingParameters();
		processingParameters.setStoreCreditCardToVaultAndReturnToken(Boolean.TRUE);

		return processingParameters;
	}

	protected PaymentRequestDTO createPaymentRequestDTO(final Map<String, String> resultMap, final double amountToPay)
	{
		final PaymentRequestDTO paymentRequestDTO = new PaymentRequestDTO();
		paymentRequestDTO.setSourceSystemInfo(createSourceSystemInfo(resultMap));
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)))
		{
			paymentRequestDTO.setCardIdentifier(createCTCCardIdentifier(resultMap));
		}
		else
		{
			paymentRequestDTO.setCardInfoRequest(createCardInfoRequest(resultMap));
			paymentRequestDTO.setCardAvsInfo(createAvsAddress(resultMap));
			paymentRequestDTO.setTransactionInfoRequest(createTransactionInfo(amountToPay));
		}
		return paymentRequestDTO;
	}

	protected PaymentSourceSystemInfo createSourceSystemInfo(final Map<String, String> resultMap)
	{
		final PaymentSourceSystemInfo sourceSystemInfo = new PaymentSourceSystemInfo();
		final String applicationId = getPaymentApplicationIdMap().get(resultMap.get(BookingJourneyType._TYPECODE))
				.get(resultMap.get(BcfFacadesConstants.SALES_CHANNEL));
		sourceSystemInfo.setAppliId(applicationId);
		sourceSystemInfo
				.setLocationId(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfintegrationserviceConstants.LOCATION_ID));
		sourceSystemInfo.setAgentId(
				getSessionService().getCurrentSession().getAttribute(BcfintegrationserviceConstants.SESSION_ICEBAR_ID));
		final String stationId = getSessionService().getCurrentSession()
				.getAttribute(BcfintegrationserviceConstants.SESSION_PINPAD_ID);
		sourceSystemInfo.setStationId(StringUtils.isNotBlank(stationId) ? stationId : SPACE);
		sourceSystemInfo.setPassword(getPaymentClientCodePasswordMap().get(applicationId));
		return sourceSystemInfo;
	}

	protected CardInfoRequest createCardInfoRequest(final Map<String, String> resultMap)
	{
		final CardInfoRequest cardInfoRequest = new CardInfoRequest();
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE)))
		{
			final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
			final CreditCardPaymentInfoModel creditCardPaymentInfoModel = customerAccountService
					.getCreditCardPaymentInfoForCode(customerModel, resultMap.get(BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE));
			if (Objects.nonNull(creditCardPaymentInfoModel))
			{
				cardInfoRequest.setCardIdentifier(createSavedCardIdentifier(creditCardPaymentInfoModel));
				cardInfoRequest.setExpectedCardType(getConfigurationService().getConfiguration()
						.getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
				final CardDate cardExpiryDate = new CardDate();
				cardExpiryDate.setMonth(creditCardPaymentInfoModel.getValidToMonth());
				cardExpiryDate.setYear(creditCardPaymentInfoModel.getValidToYear());
				cardInfoRequest.setCardExpiryDate(cardExpiryDate);

				resultMap.put(BcfFacadesConstants.Payment.CARD_EXPIRATION,
						creditCardPaymentInfoModel.getValidToMonth() + "/" + creditCardPaymentInfoModel.getValidToYear());
			}
		}
		else
		{
			cardInfoRequest.setCardIdentifier(createCardIdentifier(resultMap));
			cardInfoRequest.setExpectedCardType(getConfigurationService().getConfiguration()
					.getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
			final CardDate cardExpiryDate = new CardDate();
			if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION))
			{
				cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(0, 2));
				cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(3));
			}
			else if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH) && resultMap
					.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR))
			{
				cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH));
				cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR).substring(2));
			}
			cardInfoRequest.setCardExpiryDate(cardExpiryDate);
			cardInfoRequest.setCardCvvCode(resultMap.get(BcfFacadesConstants.Payment.CARD_CVV_NUMBER));

		}
		return cardInfoRequest;
	}


	private CardIdentifier createCardIdentifier(final Map<String, String> resultMap)
	{
		CardIdentifier cardIdentifier = null;
		if (resultMap.get(BcfFacadesConstants.Payment.TOKEN) != null)
		{
			cardIdentifier = new CardIdentifier();
			cardIdentifier.setCardIdentifierType(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.CARD_IDENTIFIER_TYPE));
			final TokenRef tokenRef = new TokenRef();
			tokenRef.setTokenValue(resultMap.get(BcfFacadesConstants.Payment.TOKEN));
			tokenRef.setTokenType(resultMap.get(BcfFacadesConstants.Payment.TOKEN_TYPE));
			cardIdentifier.setTokenRef(tokenRef);
		}
		return cardIdentifier;
	}

	private CardIdentifier createSavedCardIdentifier(final CreditCardPaymentInfoModel creditCardPaymentInfoModel)
	{
		final CardIdentifier cardIdentifier = new CardIdentifier();
		cardIdentifier.setCardIdentifierType(
				getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.CARD_IDENTIFIER_TYPE));
		final TokenRef tokenRef = new TokenRef();
		if (Objects.nonNull(creditCardPaymentInfoModel))
		{
			tokenRef.setTokenValue(creditCardPaymentInfoModel.getToken());
			tokenRef.setTokenType(creditCardPaymentInfoModel.getTokenType());
		}
		cardIdentifier.setTokenRef(tokenRef);
		return cardIdentifier;
	}

	private CardIdentifier createCTCCardIdentifier(final Map<String, String> resultMap)
	{
		CardIdentifier cardIdentifier = null;
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)))
		{
			cardIdentifier = new CardIdentifier();
			cardIdentifier.setCardIdentifierType(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.CTC_CARD_IDENTIFIER_TYPE));
			final CardRef cardRef = new CardRef();
			cardRef.setCardValue(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER));
			cardRef.setCardValueType(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.CTC_CARD_VALUE_TYPE));
			cardIdentifier.setCardRef(cardRef);
		}
		return cardIdentifier;
	}

	protected CardInfoRequest createVaultCardInfoRequest(final Map<String, String> resultMap)
	{
		return createCardInfoRequest(resultMap);
	}

	protected TransactionInfoRequest createTransactionInfo(final double amountToPay)
			throws ConversionException
	{
		final TransactionInfoRequest transactionInfoRequest = new TransactionInfoRequest();
		transactionInfoRequest.setTransactionType(
				getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.TRANSACTION_TYPE_PURCHASE));
		transactionInfoRequest.setAmountInCents(Math.round(amountToPay * 100));
		return transactionInfoRequest;
	}

	protected TransactionDetails makePayment(final Map<String, String> resultMap, final double amountToPay,
			final String serviceURL) throws IntegrationException
	{
		final PaymentRequestDTO paymentRequestDTO = createPaymentRequestDTO(resultMap, amountToPay);
		final PaymentResponseDTO paymentResponseDTO = getBcfPaymentIntegrationService().makePayment(serviceURL, paymentRequestDTO);
		TransactionDetails transactionDetails = new TransactionDetails();
		final Boolean bcfResponseStatus = paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus();
		final String bcfResponseCode = paymentResponseDTO.getResponseStatus().getBcfResponseCode();
		final String bcfResponseDesc = paymentResponseDTO.getResponseStatus().getBcfResponseDescription();
		if (isPaymentSuccessful(paymentResponseDTO))
		{
			if (Objects.isNull(resultMap.get(BcfFacadesConstants.CARD_TYPE)))
			{
				resultMap.put(BcfFacadesConstants.CARD_TYPE, paymentResponseDTO.getCardInfoResponse().getCardType());
			}
			transactionDetails = getPaymentResponseDTOConverter().convert(paymentResponseDTO);
			transactionDetails.setIsSuccessful(true);
			if (Objects.nonNull(paymentRequestDTO.getProcessingParameters()))
			{
				transactionDetails
						.setPaymentProcessingMethod(paymentRequestDTO.getProcessingParameters().getPaymentProcessingMethod());
			}
			final CardInfoResponse cardInfoResponse = paymentResponseDTO.getCardInfoResponse();
			if (cardInfoResponse != null)
			{
				resultMap.put(BcfFacadesConstants.Payment.CARD_NUMBER, cardInfoResponse.getCardRef().getCardValue());
				final TokenRef tokenRef = cardInfoResponse.getTokenRef();
				if (tokenRef != null)
				{
					resultMap.put(BcfFacadesConstants.Payment.TOKEN, tokenRef.getTokenValue());
					resultMap.put(BcfFacadesConstants.Payment.TOKEN_TYPE, tokenRef.getTokenType());
				}
			}
			transactionDetails.setCallerMethod(BcfFacadesConstants.PROCESS_PAYMENT);
			return transactionDetails;
		}
		LOG.error(String.format("Payment Request failed with status : [%s], code : [%s] and description : [%s] ", bcfResponseStatus,
				bcfResponseCode, bcfResponseDesc));
		if (Objects.nonNull(paymentResponseDTO))
		{
			transactionDetails = getPaymentResponseDTOConverter().convert(paymentResponseDTO);
			if (Objects.nonNull(paymentResponseDTO.getDisplayInfo()))
			{
				transactionDetails.setOperatorDisplayMessage(paymentResponseDTO.getDisplayInfo().getOperatorDisplayMessage());
				transactionDetails.setCustomerDisplayMessage(paymentResponseDTO.getDisplayInfo().getCustomerDisplayMessage());
			}
		}
		if (Objects.nonNull(paymentRequestDTO.getProcessingParameters()))
		{
			transactionDetails.setPaymentProcessingMethod(paymentRequestDTO.getProcessingParameters().getPaymentProcessingMethod());
		}
		transactionDetails.setIsSuccessful(false);
		transactionDetails.setBcfResponseCode(bcfResponseCode);
		transactionDetails.setBcfResponseDescription(bcfResponseDesc);
		transactionDetails.setCallerMethod(BcfFacadesConstants.PROCESS_PAYMENT);
		return transactionDetails;
	}

	protected TransactionDetails getPaymentToken(final Map<String, String> resultMap) throws IntegrationException
	{
		final TransactionDetails tokenTransactionDetails = handlePermanentTokenRequest(resultMap);
		return tokenTransactionDetails;
	}

	protected TransactionDetails makePaymentToVaultUsingSavedCard(final Map<String, String> resultMap)
	{
		final TransactionDetails transactionDetails = new TransactionDetails();
		transactionDetails.setIsSuccessful(true);
		transactionDetails.setCallerMethod(BcfFacadesConstants.GET_PERMANENT_TOKEN);
		return transactionDetails;
	}

	protected TransactionDetails makePaymentToVault(final Map<String, String> resultMap,
			final String serviceURL) throws IntegrationException
	{
		final PaymentVaultRequestDTO paymentVaultRequestDTO = createPaymentVaultRequestDTO(resultMap);
		final PaymentVaultResponseDTO paymentVaultResponseDTO = getBcfPaymentVaultIntegrationService()
				.makeVaultPayment(serviceURL, paymentVaultRequestDTO);
		TransactionDetails transactionDetails = new TransactionDetails();
		final Boolean bcfResponseStatus = paymentVaultResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus();
		final String bcfResponseCode = paymentVaultResponseDTO.getResponseStatus().getBcfResponseCode();
		final String bcfResponseDesc = paymentVaultResponseDTO.getResponseStatus().getBcfResponseDescription();
		if (isPaymentVaultSuccessful(paymentVaultResponseDTO))
		{
			updateTokenInfoInResultMap(resultMap, paymentVaultResponseDTO);
			transactionDetails = getPaymentVaultResponseDTOConverter().convert(paymentVaultResponseDTO);
			transactionDetails.setIsSuccessful(true);
			transactionDetails.setCallerMethod(BcfFacadesConstants.GET_PERMANENT_TOKEN);
			return transactionDetails;
		}
		transactionDetails.setIsSuccessful(false);
		LOG.error(String.format("Payment Request failed with status : [%s], code : [%s] and description : [%s] ", bcfResponseStatus,
				bcfResponseCode, bcfResponseDesc));
		transactionDetails.setCallerMethod(BcfFacadesConstants.GET_PERMANENT_TOKEN);
		transactionDetails.setBcfResponseCode(bcfResponseCode);
		transactionDetails.setBcfResponseDescription(bcfResponseDesc);
		return transactionDetails;
	}

	protected boolean isPaymentSuccessful(final PaymentResponseDTO paymentResponseDTO)
	{
		if (Objects.nonNull(paymentResponseDTO.getCardInfoResponse())
				&& StringUtils
				.equalsIgnoreCase(paymentResponseDTO.getCardInfoResponse().getCardType(), CreditCardType.CTC.getCode())
				&& StringUtils
				.equalsIgnoreCase(paymentResponseDTO.getCardInfoResponse().getCardStatus(), BcfCoreConstants.CTC_CARD_INACTIVE))
		{
			LOG.error(String.format("Provided CTC card with number [%s] is INACTIVE ",
					paymentResponseDTO.getCardInfoResponse().getCardRef().getCardValue()));
			return false;
		}
		return paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& StringUtils.equals(paymentResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue(PAYMENT_SUCCESS_CODE));
	}

	protected boolean isPaymentVaultSuccessful(final PaymentVaultResponseDTO paymentVaultResponseDTO)
	{
		return paymentVaultResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& StringUtils.equals(paymentVaultResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue(PAYMENT_SUCCESS_CODE));
	}

	private void updateTokenInfoInResultMap(final Map<String, String> resultMap,
			final PaymentVaultResponseDTO paymentVaultResponseDTO)
	{
		final TokenRef tokenRef = paymentVaultResponseDTO.getTokenRef();
		resultMap.put(BcfFacadesConstants.Payment.TOKEN, tokenRef.getTokenValue());
		resultMap.put(BcfFacadesConstants.Payment.TOKEN_TYPE, tokenRef.getTokenType());
	}

	protected TransactionDetails handlePermanentTokenRequest(final Map<String, String> resultMap) throws IntegrationException
	{
		final TransactionDetails transactionDetails = new TransactionDetails();
		final TokenRequestDTO tokenRequestDTO = new TokenRequestDTO();
		populatePermanentTokenRequest(tokenRequestDTO, resultMap);
		if (StringUtils.isNotBlank(resultMap.get(BcfFacadesConstants.Payment.SAVE_CC_CARD)))
		{
			tokenRequestDTO.setCardIdentifier(createVaultCardInfoRequest(resultMap).getCardIdentifier());
			final CardDate cardExpiryDate = new CardDate();
			if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION))
			{
				cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(0, 2));
				cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION).substring(3));
			}
			else if (resultMap.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH) && resultMap
					.containsKey(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR))
			{
				cardExpiryDate.setMonth(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_MONTH));
				cardExpiryDate.setYear(resultMap.get(BcfFacadesConstants.Payment.CARD_EXPIRATION_YEAR).substring(2));
			}

			tokenRequestDTO.setCardExpiryDate(cardExpiryDate);

		}
		final TokenResponseDTO response = getBcfPaymentIntegrationService().getPermanentToken(tokenRequestDTO);
		final Boolean bcfResponseStatus = response.getResponseStatus().getBcfPaymentResponseBooleanStatus();
		final String bcfResponseCode = response.getResponseStatus().getBcfResponseCode();
		final String bcfResponseDesc = response.getResponseStatus().getBcfResponseDescription();
		transactionDetails.setCallerMethod(BcfFacadesConstants.GET_PERMANENT_TOKEN);
		transactionDetails.setBcfResponseCode(bcfResponseCode);
		transactionDetails.setBcfResponseDescription(bcfResponseDesc);
		if (isGetTokenSuccessful(response))
		{
			transactionDetails.setIsSuccessful(true);
			populateMapWithTokenResponse(response, resultMap);

			final CardRef cardNumberToPrint = response.getCardNumberToPrint();
			if (Objects.nonNull(cardNumberToPrint))
			{
				transactionDetails.setCardNumberToPrint(cardNumberToPrint.getCardValue());
				transactionDetails.setCardType(cardNumberToPrint.getCardValueType());
			}

			final TokenRef tokenRef = response.getTokenRef();
			if (Objects.nonNull(tokenRef))
			{
				transactionDetails.setTokenType(tokenRef.getTokenType());
				transactionDetails.setTokenValue(tokenRef.getTokenValue());
			}
			return transactionDetails;
		}
		transactionDetails.setIsSuccessful(false);
		LOG.error(String.format("Token Request failed with status : [%s], code : [%s] and description : [%s] ", bcfResponseStatus,
				bcfResponseCode, bcfResponseDesc));
		return transactionDetails;
	}

	protected void populatePermanentTokenRequest(final TokenRequestDTO tokenRequestDTO, final Map<String, String> resultMap)
	{
		tokenRequestDTO.setType(GET_TOKEN_REQUEST);
		final PaymentSourceSystemInfo sourceSystemInfo = createSourceSystemInfo(resultMap);
		tokenRequestDTO.setSourceSystemInfo(sourceSystemInfo);
	}

	private boolean isGetTokenSuccessful(final TokenResponseDTO tokenResponseDTO)
	{
		return tokenResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& StringUtils.equals(tokenResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue(PAYMENT_SUCCESS_CODE));
	}

	protected void populateMapWithTokenResponse(final TokenResponseDTO tokenResponseDTO, final Map<String, String> resultMap)
	{
		resultMap.put(BcfFacadesConstants.Payment.TOKEN_TYPE, tokenResponseDTO.getTokenRef().getTokenType());
		resultMap.put(BcfFacadesConstants.Payment.TOKEN, tokenResponseDTO.getTokenRef().getTokenValue());
		final CardDate cardExpiryDate = tokenResponseDTO.getCardExpiryDate();
		if (cardExpiryDate != null)
		{
			resultMap.put(BcfFacadesConstants.Payment.CARD_EXPIRATION, cardExpiryDate.getMonth() + "/" + cardExpiryDate.getYear());
		}
		resultMap.put(BcfFacadesConstants.Payment.CARD_NUMBER, tokenResponseDTO.getTokenRef().getTokenValue());
		resultMap.put(BcfFacadesConstants.Payment.CARD_TYPE, tokenResponseDTO.getCardType());
	}

	protected TransactionDetails processCardNotPresentPayment(final double amountToPay, final Map<String, String> resultMap)
			throws IntegrationException
	{
		final TransactionDetails tokenTransactionDetails = handlePermanentTokenRequest(resultMap);
		if (Boolean.parseBoolean(resultMap.get("receiveTokenOnly")))
		{
			return tokenTransactionDetails;
		}
		if (tokenTransactionDetails.getIsSuccessful() && !StringUtils
				.equals(resultMap.get(BcfCoreConstants.ON_REQUEST_ORDER), BcfCoreConstants.PACKAGE_ON_REQUEST))
		{
			return makePayment(resultMap, amountToPay, BcfintegrationserviceConstants.PAYMENT_SERVICE_URL);
		}
		return tokenTransactionDetails;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected BcfPaymentIntegrationService getBcfPaymentIntegrationService()
	{
		return bcfPaymentIntegrationService;
	}

	@Required
	public void setBcfPaymentIntegrationService(final BcfPaymentIntegrationService bcfPaymentIntegrationService)
	{
		this.bcfPaymentIntegrationService = bcfPaymentIntegrationService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected Converter<PaymentResponseDTO, TransactionDetails> getPaymentResponseDTOConverter()
	{
		return paymentResponseDTOConverter;
	}

	@Required
	public void setPaymentResponseDTOConverter(
			final Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter)
	{
		this.paymentResponseDTOConverter = paymentResponseDTOConverter;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfPaymentVaultIntegrationService getBcfPaymentVaultIntegrationService()
	{
		return bcfPaymentVaultIntegrationService;
	}

	@Required
	public void setBcfPaymentVaultIntegrationService(
			final BcfPaymentVaultIntegrationService bcfPaymentVaultIntegrationService)
	{
		this.bcfPaymentVaultIntegrationService = bcfPaymentVaultIntegrationService;
	}

	public Converter<PaymentVaultResponseDTO, TransactionDetails> getPaymentVaultResponseDTOConverter()
	{
		return paymentVaultResponseDTOConverter;
	}

	@Required
	public void setPaymentVaultResponseDTOConverter(
			final Converter<PaymentVaultResponseDTO, TransactionDetails> paymentVaultResponseDTOConverter)
	{
		this.paymentVaultResponseDTOConverter = paymentVaultResponseDTOConverter;
	}

	public Map<String, Map<String, String>> getPaymentApplicationIdMap()
	{
		return paymentApplicationIdMap;
	}

	public void setPaymentApplicationIdMap(final Map<String, Map<String, String>> paymentApplicationIdMap)
	{
		this.paymentApplicationIdMap = paymentApplicationIdMap;
	}

	public Map<String, String> getPaymentClientCodePasswordMap()
	{
		return paymentClientCodePasswordMap;
	}

	public void setPaymentClientCodePasswordMap(final Map<String, String> paymentClientCodePasswordMap)
	{
		this.paymentClientCodePasswordMap = paymentClientCodePasswordMap;
	}
}
