/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.processor.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.processor.BcfPaymentProcessor;
import com.bcf.facades.payment.strategy.PaymentMethodStrategy;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfPaymentProcessor implements BcfPaymentProcessor
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfPaymentProcessor.class);

	private Map<String, String> paymentTypeMethodMap;
	private Map<String, PaymentMethodStrategy> paymentMethodStrategy;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private ConfigurationService configurationService;

	@Override
	public void processTransaction(final Map<String, String> resultMap, final AbstractOrderModel orderModel,
			final PlaceOrderResponseData decisionData)
			throws OrderProcessingException, IntegrationException
	{
		final String paymentType = resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE);
		if (BigDecimal.valueOf(orderModel.getAmountToPay()).compareTo(BigDecimal.ZERO) > 0 || StringUtils
				.equalsIgnoreCase("ctccard", getPaymentTypeMethodMap().get(paymentType)))
		{
			final String paymentTypeMethodMapping;
			if (StringUtils.isBlank(paymentType))
			{
				putBookingApplicationIdForVaultPayment(orderModel, resultMap);
				if (Objects.nonNull(orderModel.getPaymentAddress()))
				{
					populateAvsAddress(orderModel, resultMap);
				}
				paymentTypeMethodMapping = BcfFacadesConstants.Payment.PAYMENT_METHOD_CARD;
				getPaymentMethodStrategy().get(paymentTypeMethodMapping).handlePayment(resultMap, orderModel, decisionData);
				return;
			}
			paymentTypeMethodMapping = getPaymentTypeMethodMap().get(paymentType);
			getPaymentMethodStrategy().get(paymentTypeMethodMapping).handlePayment(resultMap, orderModel, decisionData);
			return;
		}
		else
		{
			LOG.warn("There is nothing to pay for the order " + orderModel.getCode());
		}

	}

	protected void populateAvsAddress(final AbstractOrderModel orderModel, final Map<String, String> resultMap)
	{
		resultMap.put(BcfFacadesConstants.Payment.AVS_STREET_NAME, orderModel.getPaymentAddress().getStreetname());
		resultMap.put(BcfFacadesConstants.Payment.AVS_STREET_NO, orderModel.getPaymentAddress().getStreetnumber());
		resultMap.put(BcfFacadesConstants.Payment.AVS_ZIPCODE, orderModel.getPaymentAddress().getPostalcode());
	}

	protected void putBookingApplicationIdForVaultPayment(final AbstractOrderModel orderModel, final Map<String, String> resultMap)
	{

		final String bookingType = getConfigurationService().getConfiguration()
				.getString(orderModel.getBookingJourneyType().getCode());

		if (resultMap.get(BcfFacadesConstants.SALES_CHANNEL) != null && bookingType != null)
		{
			resultMap.put(BcfFacadesConstants.BOOKING_APPLICATION_ID,
					bookingType + "_" + resultMap.get(BcfFacadesConstants.SALES_CHANNEL));
		}
	}


	protected Map<String, PaymentMethodStrategy> getPaymentMethodStrategy()
	{
		return paymentMethodStrategy;
	}

	@Required
	public void setPaymentMethodStrategy(
			final Map<String, PaymentMethodStrategy> paymentMethodStrategy)
	{
		this.paymentMethodStrategy = paymentMethodStrategy;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected Map<String, String> getPaymentTypeMethodMap()
	{
		return paymentTypeMethodMap;
	}

	@Required
	public void setPaymentTypeMethodMap(final Map<String, String> paymentTypeMethodMap)
	{
		this.paymentTypeMethodMap = paymentTypeMethodMap;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
