/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.processpayment.response.data.PaymentVaultResponseDTO;
import com.bcf.processpayment.response.data.ResponseStatus;


public class PaymentVaultResponseDTOPopulator implements Populator<PaymentVaultResponseDTO, TransactionDetails>
{

	@Override
	public void populate(final PaymentVaultResponseDTO src, final TransactionDetails target) throws ConversionException
	{
		ResponseStatus status = src.getResponseStatus();

		if (status != null)
		{
			target.setBcfPaymentResponseBooleanStatus(status.getBcfPaymentResponseBooleanStatus());
			target.setServiceProviderResponseCode(status.getServiceProviderResponseCode());
			target.setServiceProviderResponseDescription(status.getServiceProviderResponseDescription());
			target.setIsoResponseCode(status.getIsoResponseCode());
			target.setBcfResponseCode(status.getBcfResponseCode());
			target.setBcfResponseDescription(status.getBcfResponseDescription());
		}
	}
}
