/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.payment.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.processpayment.request.data.TokenRef;
import com.bcf.processpayment.response.data.CardInfoResponse;
import com.bcf.processpayment.response.data.DisplayInfo;
import com.bcf.processpayment.response.data.PaymentResponseDTO;
import com.bcf.processpayment.response.data.ReceiptInfo;
import com.bcf.processpayment.response.data.ResponseStatus;
import com.bcf.processpayment.response.data.TransactionInfoResponse;


public class PaymentResponseDTOPopulator implements Populator<PaymentResponseDTO, TransactionDetails>
{

	@Override
	public void populate(final PaymentResponseDTO src, final TransactionDetails target) throws ConversionException
	{
		if (Objects.nonNull(src))
		{

			final ResponseStatus responseStatus = src.getResponseStatus();
			if (responseStatus != null)
			{
				target.setBcfResponseCode(responseStatus.getBcfResponseCode());
				target.setBcfResponseDescription(responseStatus.getBcfResponseDescription());
				target.setBcfPaymentResponseBooleanStatus(responseStatus.getBcfPaymentResponseBooleanStatus());
				target.setServiceProviderResponseCode(responseStatus.getServiceProviderResponseCode());
				target.setServiceProviderResponseDescription(responseStatus.getServiceProviderResponseDescription());
				target.setIsoResponseCode(responseStatus.getIsoResponseCode());
			}

			final TransactionInfoResponse transactionInfoResponse = src.getTransactionInfoResponse();
			if (src.getTransactionInfoResponse() != null)
			{
				target.setTransactionTimeStamp(transactionInfoResponse.getTransTimeStamp());
				target.setTransactionAmountInCents(transactionInfoResponse.getTransactionAmountInCents());
				target.setApprovalNumber(transactionInfoResponse.getApprovalNumber());
				target.setTransactionRefNumber(transactionInfoResponse.getTransactionRefNumber());
				target.setInvoiceNumber(transactionInfoResponse.getInvoiceNumber());
				target.setServiceProviderPaymentTerminalId(transactionInfoResponse.getServiceProviderPaymentTerminalId());
				target.setBcfRefRecord(transactionInfoResponse.getBcfRefRecord());
				target.setTransactionType(transactionInfoResponse.getTransactionType());
				target.setCardEntryMethod(transactionInfoResponse.getCardEntryMethod());
				target.setSafIndicator(transactionInfoResponse.getSafIndicator());
				target.setCvmIndicator(transactionInfoResponse.getCvmIndicator());
				target.setEmvTvr(transactionInfoResponse.getEmvTvr());
				target.setMotoIndicator(transactionInfoResponse.getMotoIndicator());
				target.setEmvTsi(transactionInfoResponse.getEmvTsi());
			}

			final CardInfoResponse cardInfoResponse = src.getCardInfoResponse();
			if (cardInfoResponse != null)
			{
				target.setDebitAccountType(cardInfoResponse.getDebitAccountType());
				target.setCardHolderLanguage(cardInfoResponse.getCardHolderLanguage());
				target.setEmvApplicationLabel(cardInfoResponse.getEmvApplicationLabel());
				target.setEmvAppPreferredName(cardInfoResponse.getEmvApplicationPreferredName());
				target.setEmvAid(cardInfoResponse.getEmvAid());
				target.setFormFactor(cardInfoResponse.getFormFactor());
				target.setCardType(cardInfoResponse.getCardType());
				if(cardInfoResponse.getCardNumberToPrint() != null)
				{
					target.setCardNumberToPrint(cardInfoResponse.getCardNumberToPrint().getCardValue());
				}
			}

			populateTokenDetails(src, target);
			populateDisplayInfo(src, target);
			populateReceiptInfo(src, target);
		}
	}

	private void populateReceiptInfo(final PaymentResponseDTO src, final TransactionDetails target)
	{
		final ReceiptInfo receiptInfo = src.getReceiptInfo();
		if (receiptInfo != null)
		{
			target.setApprovalOrDeclineMessage(receiptInfo.getApprovalOrDeclineMessage());
			target.setTransactionType(receiptInfo.getTransactionType());
			target.setApprovalOrDeclineMessage(receiptInfo.getApprovalOrDeclineMessage());
			target.setSupplementalMessage(receiptInfo.getSupplementalMessage());
			target.setSignatureRequiredFlag(
					Objects.nonNull(receiptInfo.getSignatureRequiredFlag()) ? receiptInfo.getSignatureRequiredFlag() : false);
			target.setEmvPinEntryMessage(receiptInfo.getEmvPinEntryMessage());
		}
	}

	private void populateDisplayInfo(final PaymentResponseDTO src, final TransactionDetails target)
	{
		final DisplayInfo displayInfo = src.getDisplayInfo();
		if (displayInfo != null)
		{
			target.setOperatorDisplayMessage(displayInfo.getOperatorDisplayMessage());
			target.setCustomerDisplayMessage(displayInfo.getCustomerDisplayMessage());
		}
	}

	private void populateTokenDetails(final PaymentResponseDTO src, final TransactionDetails target)
	{
		if (Objects.nonNull(src.getCardInfoResponse()))
		{
			final TokenRef tokenRef = src.getCardInfoResponse().getTokenRef();
			if (Objects.nonNull(tokenRef))
			{
				target.setTokenType(tokenRef.getTokenType());
				target.setTokenValue(tokenRef.getTokenValue());

			}
		}
	}
}
