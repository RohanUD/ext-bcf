/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.VehicleType.impl;


import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.vehicletype.VehicleTypeService;
import com.bcf.facades.VehicleType.VehicleTypeFacade;
import com.bcf.facades.ferry.VehicleTypeData;


/**
 * Facade that provides Vehicle Type specific services. The facade uses the VehicleTypeService to get VehicleTypeModel
 * and uses converter/populators to transfer to VehicleTypeData types.
 */

public class DefaultVehicleTypeFacade implements VehicleTypeFacade
{

	private VehicleTypeService vehicleTypeService;
	private Converter<VehicleTypeModel, VehicleTypeData> vehicleTypeConverter;

	public VehicleTypeService getVehicleTypeService()
	{
		return vehicleTypeService;
	}

	public void setVehicleTypeService(final VehicleTypeService vehicleTypeService)
	{
		this.vehicleTypeService = vehicleTypeService;
	}

	public Converter<VehicleTypeModel, VehicleTypeData> getVehicleTypeConverter()
	{
		return vehicleTypeConverter;
	}

	public void setVehicleTypeConverter(final Converter<VehicleTypeModel, VehicleTypeData> vehicleTypeConverter)
	{
		this.vehicleTypeConverter = vehicleTypeConverter;
	}

	@Override
	public List<VehicleTypeData> getVehicleTypesForCategoryType(final String categoryTypeCode)
	{
		final List<VehicleTypeModel> vehicleModels = getVehicleTypeService().getVehicleTypesForCategoryType(categoryTypeCode);
		return Converters.convertAll(vehicleModels, getVehicleTypeConverter());
	}

	@Override
	public List<VehicleTypeData> getAllVehicleTypes()
	{
		return Converters.convertAll(getVehicleTypeService().getAllVehicleTypes(), getVehicleTypeConverter());
	}

	@Override
	public VehicleTypeData getVehicleTypeForVehicleCode(final String vehicleCode)
	{
		final VehicleTypeModel vehicleModel = getVehicleTypeService().getVehicleForCode(vehicleCode);
		return getVehicleTypeConverter().convert(vehicleModel);
	}
}



