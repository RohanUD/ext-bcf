/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.VehicleType;

import java.util.List;
import com.bcf.facades.ferry.VehicleTypeData;


/**
 * Facade that exposes Vehicle Type specific services
 */
public interface VehicleTypeFacade
{

	/**
	 * Gets the vehicle types for category type.
	 *
	 * @param categoryTypeCode the category type code
	 * @return List<VehicleTypeData> Vehicle types
	 */
	List<VehicleTypeData> getVehicleTypesForCategoryType(String categoryTypeCode);

	/**
	 * Facade which returns all vehicle types
	 *
	 * @return List<VehicleTypeData> Vehicle types
	 */
	List<VehicleTypeData> getAllVehicleTypes();

	/**
	 * Gets the vehicle type for vehicle code.
	 *
	 * @param vehicleCode the category type code
	 * @return VehicleTypeData Vehicle type
	 */
	VehicleTypeData getVehicleTypeForVehicleCode(final String vehicleCode);


}
