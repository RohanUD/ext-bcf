/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import com.bcf.facades.deals.search.request.data.DealSearchCriteriaData;
import com.bcf.facades.deals.search.response.data.DealResponseData;
import com.bcf.facades.deals.search.response.data.DealsResponseData;
import com.bcf.facades.search.facetdata.DealSearchPageData;


/**
 * Deal Bundles Search Facade
 */
public interface DealBundleSearchFacade
{

	/**
	 * method to fetch deals from Solr
	 *
	 * @param searchCriteria
	 * @return
	 */
	DealsResponseData doSearch(DealSearchCriteriaData searchCriteria);
	
	DealSearchPageData<SearchStateData, DealResponseData> searchDeals(DealSearchCriteriaData searchCriteria);

}
