/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.converters.populator;

import de.hybris.platform.commercefacades.accommodation.search.PositionData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.TransportBundleTemplateModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import com.bcf.facades.deals.search.DealBundleDetailsFacade;
import com.bcf.facades.deals.search.data.DateRangeData;
import com.bcf.facades.deals.search.response.data.DealResponseData;


public class DealsSearchResultPopulator implements Populator<SearchResultValueData, DealResponseData>
{
	private TravelCommercePriceFacade travelCommercePriceFacade;
	private DealBundleDetailsFacade dealBundleDetailsFacade;

	@Override
	public void populate(final SearchResultValueData source, final DealResponseData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setDealId(this.getValue(source, "dealId"));
		target.setDealName(this.getValue(source, "dealName"));
		target.setDealType(this.getValue(source, "dealType"));
		target.setDealDescription(this.getValue(source, "dealDescription"));

		target.setDestinationLocation(Collections.singletonList(getValue(source, "destinationLocationCode")));
		target.setDestinationLocationName(Collections.singletonList(getValue(source, "destinationLocation")));

		final Date startDate = this.getValue(source, "startDate");
		final Date endDate = this.getValue(source, "endDate");

		final DateRangeData dateRangeData = new DateRangeData();
		dateRangeData.setStartingDate(startDate);
		dateRangeData.setEndingDate(endDate);
		target.setApplicableDateRange(dateRangeData);

		target.setMainImageMediaUrl(Collections.singletonList(this.getValue(source, "accommodationImageUrl")));
		target.setIsMultiCity(this.<Boolean>getValue(source, "isMultiCity"));

		target.setOriginLocation(Collections.singletonList(getValue(source, "originLocationCode")));
		target.setOriginLocationName(Collections.singletonList(getValue(source, "originLocation")));

		target.setHotelName(Collections.singletonList(getValue(source, "hotelName")));
		target.setSeoUrl(this.getValue(source, "seoUrl"));

		final List<String> positionCoordinates = Collections.singletonList(getValue(source, "hotel_latlon_location"));
		if (CollectionUtils.isNotEmpty(positionCoordinates))
		{
			final List<PositionData> hotelPositions = new ArrayList<>();
			for (final String positionCoordinate : positionCoordinates)
			{
				if (StringUtils.contains(positionCoordinate, ","))
				{
					final String[] formattedAddress = positionCoordinate.split(",");
					final PositionData positionData = new PositionData();
					positionData.setLatitude(Double.valueOf(formattedAddress[0]));
					positionData.setLongitude(Double.valueOf(formattedAddress[1]));
					hotelPositions.add(positionData);
				}
			}
			target.setPosition(hotelPositions);
		}

		final Double priceValue = this.getValue(source, "fromPrice");
		if (priceValue != null)
		{
			final PriceData taxValue = this.getTravelCommercePriceFacade().createPriceData(priceValue.doubleValue());
			target.setPrice(taxValue);
		}
		target.setDiscount(getValue(source, "discount"));
		target.setDiscountType(getValue(source, "discountType"));
		target.setStarRating(Integer.parseInt(this.getValue(source, "starRating")));

		if (!target.isIsMultiCity() && Objects.nonNull(source.getDepartureDate()))
		{
			populateStockInformation(target.getDealId(), source.getDepartureDate(), target);


		}
	}

	protected void populateStockInformation(final String dealId, final Date departureDate, final DealResponseData target)
	{

		final DealBundleTemplateModel dealBundleTemplateModel = dealBundleDetailsFacade.getDealBundleTemplateById(dealId);
		if (Objects.isNull(dealBundleTemplateModel))
		{
			return;
		}
		final Optional<BundleTemplateModel> bundleTemplateModel = dealBundleTemplateModel.getChildTemplates().stream()
				.filter(child -> child instanceof AccommodationBundleTemplateModel).findFirst();
		if (!bundleTemplateModel.isPresent())
		{
			return;
		}

		final AccommodationBundleTemplateModel accommodationBundleTemplateModel = (AccommodationBundleTemplateModel) bundleTemplateModel
				.get();
		final Optional<BundleTemplateModel> optionalActivityBundleTemplate = dealBundleTemplateModel.getChildTemplates().stream()
				.filter(child -> !(child instanceof AccommodationBundleTemplateModel)
						&& !(child instanceof TransportBundleTemplateModel)).findFirst();

		BundleTemplateModel activityBundleTemplateModel = null;
		if(optionalActivityBundleTemplate.isPresent())
		{
			activityBundleTemplateModel = optionalActivityBundleTemplate.get();
		}
		target.setAvailable(
				dealBundleDetailsFacade
						.isDealAvailable(activityBundleTemplateModel, accommodationBundleTemplateModel, departureDate,
								dealBundleTemplateModel.getLength()));
		target.setStockType(accommodationBundleTemplateModel.getAccommodation().getStockLevelType());

	}



	/**
	 * Gets value.
	 *
	 * @param <T>          the type parameter
	 * @param source       the source
	 * @param propertyName the property name
	 * @return the value
	 */
	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		return (T) source.getValues().get(propertyName);
	}

	protected List<DateRangeData> convertSolrDateValuesTODateRanges(final List<String> dateRangesList)
	{
		final List<DateRangeData> dateRangeList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(dateRangesList))
		{
			for (final String dateRange : dateRangesList)
			{
				final String[] datesArray = dateRange.split("_");
				final Date startDate = TravelDateUtils.convertStringDateToDate(datesArray[0], TravelservicesConstants.DATE_PATTERN);
				final Date endDate = TravelDateUtils.convertStringDateToDate(datesArray[1], TravelservicesConstants.DATE_PATTERN);

				final DateRangeData dateRangeData = new DateRangeData();
				dateRangeData.setStartingDate(startDate);
				dateRangeData.setEndingDate(endDate);
				dateRangeList.add(dateRangeData);
			}
		}
		return dateRangeList;
	}

	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return this.travelCommercePriceFacade;
	}

	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}

	protected DealBundleDetailsFacade getDealBundleDetailsFacade()
	{
		return dealBundleDetailsFacade;
	}

	@Required
	public void setDealBundleDetailsFacade(final DealBundleDetailsFacade dealBundleDetailsFacade)
	{
		this.dealBundleDetailsFacade = dealBundleDetailsFacade;
	}

}
