/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.converters.populator;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.search.facetdata.FilteredFacetSearchPageData;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.deals.search.response.data.DealResponseData;
import com.bcf.facades.search.facetdata.DealSearchPageData;


public class DealsSearchPageDataPopulator<QUERY, STATE, RESULT, ITEM extends DealResponseData>
		implements Populator<DealSearchPageData<QUERY, RESULT>, DealSearchPageData<STATE, ITEM>>
{

	private Converter<QUERY, STATE> searchStateConverter;
	private Converter<RESULT, ITEM> dealsSearchResultConverter;
	private Converter<FacetData<QUERY>, FacetData<STATE>> facetConverter;
	private Converter<FilteredFacetSearchPageData<QUERY>, FilteredFacetSearchPageData<STATE>> filteredFacetSearchPageConverter;

	@	Override
	public void populate(final DealSearchPageData<QUERY, RESULT> source,
			final DealSearchPageData<STATE, ITEM> target) throws ConversionException
	{
		target.setPagination(source.getPagination());
		target.setSorts(source.getSorts());

		target.setCurrentQuery(this.getSearchStateConverter().convert(source.getCurrentQuery()));

		if (CollectionUtils.isNotEmpty(source.getFacets()))
		{
			target.setFacets(Converters.convertAll(source.getFacets(), this.getFacetConverter()));
		}

		if (CollectionUtils.isNotEmpty(source.getFilteredFacets()))
		{
			target.setFilteredFacets(Converters.convertAll(source.getFilteredFacets(), this.getFilteredFacetSearchPageConverter()));
		}
		if (source.getResults() != null)
		{
			target.setResults(Converters.convertAll(source.getResults(), this.getDealsSearchResultConverter()));
		}
	}

	protected Converter<RESULT, ITEM> getDealsSearchResultConverter()
	{
		return dealsSearchResultConverter;
	}

	@Required
	public void setDealsSearchResultConverter(
			final Converter<RESULT, ITEM> dealsSearchResultConverter)
	{
		this.dealsSearchResultConverter = dealsSearchResultConverter;
	}

	protected Converter<QUERY, STATE> getSearchStateConverter()

	{
		return this.searchStateConverter;
	}

	@Required
	public void setSearchStateConverter(final Converter<QUERY, STATE> searchStateConverter)
	{
		this.searchStateConverter = searchStateConverter;
	}

	protected Converter<FacetData<QUERY>, FacetData<STATE>> getFacetConverter()
	{
		return this.facetConverter;
	}

	@Required
	public void setFacetConverter(final Converter<FacetData<QUERY>, FacetData<STATE>> facetConverter)
	{
		this.facetConverter = facetConverter;
	}

	protected Converter<FilteredFacetSearchPageData<QUERY>, FilteredFacetSearchPageData<STATE>> getFilteredFacetSearchPageConverter()
	{
		return this.filteredFacetSearchPageConverter;
	}

	@Required
	public void setFilteredFacetSearchPageConverter(
			final Converter<FilteredFacetSearchPageData<QUERY>, FilteredFacetSearchPageData<STATE>> filteredFacetSearchPageConverter)
	{
		this.filteredFacetSearchPageConverter = filteredFacetSearchPageConverter;
	}
}
