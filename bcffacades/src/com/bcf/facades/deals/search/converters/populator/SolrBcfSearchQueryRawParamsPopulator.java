/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.converters.populator;

import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;


public class SolrBcfSearchQueryRawParamsPopulator implements Populator<SearchData, SolrSearchQueryData>
{
	@Override
	public void populate(SearchData searchData, SolrSearchQueryData solrSearchQueryData) throws ConversionException
	{
		List<SolrSearchQueryTermData> terms = new ArrayList<>();
		Map<String, String> rawQueryFilterTerms = searchData.getRawQueryFilterTerms();
		if(MapUtils.isNotEmpty(rawQueryFilterTerms))
		{
			rawQueryFilterTerms.entrySet().forEach(entry -> {
				SolrSearchQueryTermData termData = new SolrSearchQueryTermData();
				termData.setKey(entry.getKey());
				termData.setValue(entry.getValue());
				terms.add(termData);
			});
		}
		solrSearchQueryData.setRawQueryFilterTerms(terms);
	}
}
