/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.converters.populator;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.travelfacades.search.converters.populator.SolrTravelSearchQueryDecoderPopulator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


public class SolrDealsSearchQueryDecoderPopulator extends SolrTravelSearchQueryDecoderPopulator
{

	@Override
	protected void populateQuery(final String query, final SolrSearchQueryData target)
	{
		if (!StringUtils.isEmpty(query))
		{
			final List<SolrSearchQueryTermData> filterTerms = new ArrayList<>(1);
			final String[] facets = query.split(":");
			// TODO needs to confirm the default sorting method to execute the below if block
			if (facets.length > 1 && StringUtils.isEmpty(target.getSort()))
			{
				target.setSort(facets[1]);
			}
			setFilterTerms(target, filterTerms, facets);
		}
	}

	private void setFilterTerms(final SolrSearchQueryData target, final List<SolrSearchQueryTermData> filterTerms,
			final String[] facets)
	{
		for (int idx = 2; idx < facets.length; idx += 2)
		{
			final String value = facets[idx];
			if (!getRequiredFacetCodes().contains(value) && !StringUtils.isEmpty(value))
			{
				final SolrSearchQueryTermData filterTerm = new SolrSearchQueryTermData();
				filterTerm.setKey(value);
				if (idx + 1 >= facets.length)
				{
					filterTerm.setValue("");
				}
				else
				{
					filterTerm.setValue(facets[idx + 1]);
				}

				filterTerms.add(filterTerm);
			}
		}

		if (CollectionUtils.isNotEmpty(target.getFilterTerms()))
		{
			filterTerms.addAll(target.getFilterTerms());
		}

		target.setFilterTerms(filterTerms);
	}

	@Override
	protected List<String> getRequiredFacetCodes()
	{
		final String requiredFacetCodes = getConfigurationService().getConfiguration()
				.getString("deals.listing.facet.codes");
		return StringUtils.isEmpty(requiredFacetCodes) && !StringUtils.contains(requiredFacetCodes, ",") ?
				Collections.emptyList() :
				Arrays.asList(requiredFacetCodes.split(","));
	}

}
