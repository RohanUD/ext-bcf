/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.converters.populator;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;


public class SolrSearchDepartureDateRequestResponsePopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
		implements
		Populator<SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE>, SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult>>

{
	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source the source object
	 * @param target the target to fill
	 * @throws ConversionException if an error occurs
	 */
	@Override
	public void populate(
			final SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> source,
			final SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult> target)
			throws ConversionException
	{
		if (Objects.nonNull(source) && Objects.nonNull(source.getSearchQueryData()) && CollectionUtils
				.isNotEmpty(source.getSearchQueryData().getRawQueryFilterTerms()))
		{
			final Optional<SolrSearchQueryTermData> departureDate = source.getSearchQueryData().getRawQueryFilterTerms().stream()
					.filter(term -> term.getKey().equals("departureDate")).findAny();
			if (departureDate.isPresent())
			{
				target.setDepartureDate(TravelDateUtils.convertStringDateToDate(departureDate.get().getValue(), "MMM dd yyyy"));
			}
		}
	}
}
