/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.impl;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.DealBundleSearchFacade;
import com.bcf.facades.deals.search.DealsSolrSearchFacade;
import com.bcf.facades.deals.search.request.data.DealSearchCriteriaData;
import com.bcf.facades.deals.search.response.data.DealResponseData;
import com.bcf.facades.deals.search.response.data.DealsResponseData;
import com.bcf.facades.search.facetdata.DealSearchPageData;


public class DefaultBcfDealBundleSearchFacade implements DealBundleSearchFacade
{
	private DealsSolrSearchFacade solrDealsSearchFacade;
	private ConfigurationService configurationService;

	@Override
	public DealsResponseData doSearch(final DealSearchCriteriaData searchCriteria)
	{
		final DealsResponseData deals = new DealsResponseData();
		final DealSearchPageData<SearchStateData, DealResponseData> dealSearchPageData = searchDeals(searchCriteria);
		final List<DealResponseData> dealResponseData = dealSearchPageData.getResults();
		deals.setFacets(dealSearchPageData.getFacets());
		deals.setSorts(searchCriteria.getSorts());
		deals.setDealResponseData(dealResponseData);

		return deals;
	}

	@Override
	public DealSearchPageData<SearchStateData, DealResponseData> searchDeals(final DealSearchCriteriaData searchCriteria)
	{
		final SearchData searchData = new SearchData();

		final Map<String, String> filterTerms = new HashMap<>();
		final Map<String, String> rawQueryFilterTerms = new HashMap<>();
		if (Objects.nonNull(searchCriteria.getDestinationLocation()))
		{
			filterTerms.put(BcfFacadesConstants.DESTINATION_LOCATION_CODE, searchCriteria.getDestinationLocation());
		}
		// TODO handle deal type search criteria as deal type is a list of values against a deal

		filterTerms.put(BcfFacadesConstants.LENGTH, String.valueOf(searchCriteria.getDurationOfStay()));
		if (Objects.nonNull(searchCriteria.getStarRating()))
		{
			filterTerms.put(BcfFacadesConstants.STAR_RATING, String.valueOf(searchCriteria.getStarRating()));
		}

		if (Objects.nonNull(searchCriteria.getOriginLocation()))
		{
			filterTerms.put(BcfFacadesConstants.ORIGIN_LOCATION_CODE, searchCriteria.getOriginLocation());
		}

		if (Objects.nonNull(searchCriteria.getHotelName()))
		{
			filterTerms.put(BcfFacadesConstants.ACCOMMODATION_OFFERING_CODE, searchCriteria.getHotelName());
		}

		if (Objects.nonNull(searchCriteria.getLocationCode()))
		{
			filterTerms.put(BcfFacadesConstants.LOCATION_CODES, searchCriteria.getLocationCode());
		}

		if (StringUtils.isNotBlank(searchCriteria.getDepartureDate()))
		{
			rawQueryFilterTerms.put(BcfFacadesConstants.DEPARTURE_DATE, searchCriteria.getDepartureDate());
			searchData.setRawQueryFilterTerms(rawQueryFilterTerms);
		}

		if(Objects.nonNull(getConfigurationService().getConfiguration().getString("bcffacades.mandatory.raw.query.params")))
		{
			final String mandatoryRawParamsString = getConfigurationService().getConfiguration().getString("bcffacades.mandatory.raw.query.params");
			final String[] mandatoryRawParams = mandatoryRawParamsString.split(",");
			Arrays.stream(mandatoryRawParams).forEach(rawParam -> searchData.getRawQueryFilterTerms().put(rawParam, StringUtils.EMPTY));
		}

		searchData.setSort(searchCriteria.getSort());
		searchData.setFilterTerms(filterTerms);
		final String query = StringUtils.isNotEmpty(searchCriteria.getQuery())
				? searchCriteria.getQuery() : StringUtils.EMPTY;
		searchData.setQuery(query);
		final DealSearchPageData<SearchStateData, DealResponseData> dealSearchPageData;
		if (StringUtils.isNotBlank(searchCriteria.getSort()))
		{
			final PageableData pageableData = new PageableData();
			pageableData.setSort(searchCriteria.getSort());
			searchData.setSort(searchCriteria.getSort());
			dealSearchPageData = getSolrDealsSearchFacade().searchDeals(searchData, pageableData);
		}
		else
		{
			dealSearchPageData = getSolrDealsSearchFacade().searchDeals(searchData);
		}

		searchCriteria.setSorts(dealSearchPageData.getSorts());

		return dealSearchPageData;
	}

	protected DealsSolrSearchFacade getSolrDealsSearchFacade()
	{
		return solrDealsSearchFacade;
	}

	@Required
	public void setSolrDealsSearchFacade(final DealsSolrSearchFacade solrDealsSearchFacade)
	{
		this.solrDealsSearchFacade = solrDealsSearchFacade;
	}

	protected ConfigurationService getConfigurationService() {
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
