/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.impl;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.deals.solrfacetsearch.DealsSearchService;
import com.bcf.facades.deals.search.DealsSolrSearchFacade;
import com.bcf.facades.deals.search.response.data.DealResponseData;
import com.bcf.facades.search.facetdata.DealSearchPageData;


public class DefaultBcfDealsSolrSearchFacade<ITEM extends DealResponseData> implements DealsSolrSearchFacade
{
	private ThreadContextService threadContextService;
	private Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder;
	private DealsSearchService dealsSearchService;
	private Converter<DealSearchPageData<SolrSearchQueryData, SearchResultValueData>,
			DealSearchPageData<SearchStateData, ITEM>> dealsSearchPageConverter;

	@Override
	public DealSearchPageData<SearchStateData, ITEM> searchDeals(final SearchData searchData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<DealSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public DealSearchPageData<SearchStateData, ITEM> execute()
					{
						return getDealsSearchPageConverter()
								.convert(getDealsSearchService().doSearch(decodeSearchData(searchData), null));
					}
				});
	}

	@Override
	public DealSearchPageData<SearchStateData, ITEM> searchDeals(final SearchData searchData, final PageableData pageableData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<DealSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public DealSearchPageData<SearchStateData, ITEM> execute()
					{
						return getDealsSearchPageConverter()
								.convert(getDealsSearchService().doSearch(decodeSearchData(searchData), pageableData));
					}
				});
	}

	/**
	 * Decode search data solr search query data.
	 *
	 * @param searchData the search data
	 * @return the solr search query data
	 */
	protected SolrSearchQueryData decodeSearchData(final SearchData searchData)
	{
		return getSolrTravelSearchQueryDecoder().convert(searchData);
	}

	protected ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	@Required
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

	protected Converter<SearchData, SolrSearchQueryData> getSolrTravelSearchQueryDecoder()
	{
		return solrTravelSearchQueryDecoder;
	}

	@Required
	public void setSolrTravelSearchQueryDecoder(
			final Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder)
	{
		this.solrTravelSearchQueryDecoder = solrTravelSearchQueryDecoder;
	}

	protected DealsSearchService getDealsSearchService()
	{
		return dealsSearchService;
	}

	@Required
	public void setDealsSearchService(final DealsSearchService dealsSearchService)
	{
		this.dealsSearchService = dealsSearchService;
	}

	protected Converter<DealSearchPageData<SolrSearchQueryData, SearchResultValueData>, DealSearchPageData<SearchStateData, ITEM>>

	getDealsSearchPageConverter()
	{
		return dealsSearchPageConverter;
	}

	@Required
	public void setDealsSearchPageConverter(
			final Converter<DealSearchPageData<SolrSearchQueryData, SearchResultValueData>, DealSearchPageData<SearchStateData, ITEM>> dealsSearchPageConverter)
	{
		this.dealsSearchPageConverter = dealsSearchPageConverter;
	}
}
