/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search.impl;

import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.travel.DealOriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.SearchProcessingInfoData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.enums.FareSelectionDisplayOrder;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.packages.impl.DefaultDealBundleTemplateFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.RouteBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.TransportBundleTemplateModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelBundleTemplateService;
import com.bcf.core.enums.VehicleTypeCode;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.accommodation.handlers.BcfDealRoomRatesHandler;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.deals.manager.BcfDealSearchResponsePipelineManager;
import com.bcf.facades.deals.search.DealBundleDetailsFacade;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.packages.request.BcfPackageProductRequestData;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


public class DefaultDealBundleDetailsFacade extends DefaultDealBundleTemplateFacade implements DealBundleDetailsFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultDealBundleDetailsFacade.class);
	private BcfDealSearchResponsePipelineManager bcfDealSearchResponsePipelineManager;
	private BcfTravelBundleTemplateService bcfTravelBundleTemplateService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private Converter<TransportOfferingModel, TransportOfferingData> transportOfferingConverter;
	private BcfDealRoomRatesHandler bcfDealRoomRatesHandler;
	private WarehouseService warehouseService;
	private BcfProductFacade bcfProductFacade;

	@Override
	public BcfPackageRequestData getBcfPackageRequestData(final String dealBundleTemplateId, final String selectedDepartureDate)
	{
		Date departureDate = null;
		if (StringUtils.isNotEmpty(selectedDepartureDate))
		{
			departureDate = TravelDateUtils.convertStringDateToDate(selectedDepartureDate, TravelservicesConstants.DATE_PATTERN);
			final Date currentDate = getTimeService().getCurrentTime();
			if (TravelDateUtils.isSameDate(currentDate, departureDate) || departureDate.before(currentDate))
			{
				return null;
			}
		}

		return getBcfPackageRequestData(dealBundleTemplateId, departureDate);
	}

	@Override
	public boolean isValidDealDepartureDate(final String date, final String dealBundleTemplateId)
	{
		return isValidDealDepartureDate(date, getDealBundleTemplateById(dealBundleTemplateId));
	}

	protected boolean isValidDealDepartureDate(final String date, final DealBundleTemplateModel dealBundleTemplateModel)
	{
		final Date departureDate = TravelDateUtils.convertStringDateToDate(date, TravelservicesConstants.DATE_PATTERN);
		boolean isValidDate = true;
		if (StringUtils.isNotEmpty(dealBundleTemplateModel.getStartingDatePattern()))
		{
			final List<Date> validDates = getDealValidDates(dealBundleTemplateModel.getStartingDatePattern(), date);
			isValidDate = validDates.contains(departureDate);
		}

		if (CollectionUtils.isNotEmpty(dealBundleTemplateModel.getDateRanges()))
		{
			isValidDate = BCFDateUtils.isDateInDateRange(departureDate, dealBundleTemplateModel.getDateRanges());
		}
		return isValidDate;
	}


	protected BcfPackageRequestData getBcfPackageRequestData(final String dealBundleTemplateId, final Date departureDate)
	{
		final DealBundleTemplateModel dealBundleTemplateModel = getDealBundleTemplateById(dealBundleTemplateId);
		if (Objects.isNull(dealBundleTemplateModel))
		{
			return null;
		}

		final Collection<BundleTemplateModel> dealBundleTemplateModels = new HashSet<>();
		final BcfPackageRequestData packageRequestData = new BcfPackageRequestData();
		populateDealBundles(dealBundleTemplateModel, dealBundleTemplateModels);
		packageRequestData.setPackageId(dealBundleTemplateModel.getId());
		packageRequestData.setStartingDatePattern(dealBundleTemplateModel.getStartingDatePattern());
		final List<BcfPackageProductRequestData> bcfPackageProductRequestDataList = new ArrayList<>();
		packageRequestData.setBcfPackageProductDatas(bcfPackageProductRequestDataList);
		Date dealDepartureDate = null;
		for (final BundleTemplateModel bundleTemplate : dealBundleTemplateModels)
		{
			final DealBundleTemplateModel dealBundleTemplate = (DealBundleTemplateModel) bundleTemplate;
			if (Objects.isNull(dealDepartureDate))
			{
				dealDepartureDate = Objects.nonNull(departureDate) ? departureDate : getFirstDealDate(dealBundleTemplate);
			}
			if (Objects.isNull(dealDepartureDate))
			{
				LOG.warn("No available dates for dealBundleTemplate id: " + dealBundleTemplateId);
				break;
			}
			final Date returnDate = TravelDateUtils.addDays(dealDepartureDate, dealBundleTemplateModel.getLength());
			bcfPackageProductRequestDataList
					.add(populateDeal(dealBundleTemplate, dealDepartureDate, returnDate));
			dealDepartureDate = returnDate;
		}

		return packageRequestData;
	}

	@Override
	public void populateDealBundles(final DealBundleTemplateModel dealBundleTemplateModel,
			final Collection<BundleTemplateModel> dealBundleTemplateModels)
	{
		if (dealBundleTemplateModel.getIsMultiCityDeal())
		{
			dealBundleTemplateModel.getChildTemplates().forEach(
					dealBundleTemplate -> populateDealBundles((DealBundleTemplateModel) dealBundleTemplate, dealBundleTemplateModels));
		}
		else
		{
			dealBundleTemplateModels.add(dealBundleTemplateModel);
		}

	}

	@Override
	public BcfPackagesResponseData getPackageResponseDetails(final BcfPackageRequestData packageRequestData)
	{
		if (Objects.isNull(packageRequestData))
		{
			return null;
		}
		return getBcfDealSearchResponsePipelineManager().executePipeline(packageRequestData);
	}

	protected BcfPackageProductRequestData populateDeal(final DealBundleTemplateModel dealBundleTemplateModel,
			final Date dealDepartureDate, final Date returnDate)
	{
		final BcfPackageProductRequestData bcfPackageProductRequestData = new BcfPackageProductRequestData();
		bcfPackageProductRequestData.setPackageId(dealBundleTemplateModel.getId());
		bcfPackageProductRequestData.setTransportPackageRequest(
				getTransportPackageRequest(dealBundleTemplateModel, dealDepartureDate, returnDate));
		bcfPackageProductRequestData.setAccommodationPackageRequest(
				getAccommodationPackageRequest(dealBundleTemplateModel, dealDepartureDate, returnDate));
		bcfPackageProductRequestData.setBundleTemplates(getBundleTemplates(dealBundleTemplateModel, dealDepartureDate));
		return bcfPackageProductRequestData;
	}

	/**
	 * Gets the bundle templates.
	 *
	 * @param dealBundleTemplateModel the deal bundle template model
	 * @param dealDepartureDate       the deal departure date
	 * @return the bundle templates
	 */
	protected List<BundleTemplateData> getBundleTemplates(final DealBundleTemplateModel dealBundleTemplateModel,
			final Date dealDepartureDate)
	{
		final List<BundleTemplateModel> standardBundleTemplates = dealBundleTemplateModel.getChildTemplates().stream()
				.filter(bundleTemplateModel -> !(bundleTemplateModel instanceof TransportBundleTemplateModel)
						&& !(bundleTemplateModel instanceof AccommodationBundleTemplateModel))
				.collect(Collectors.toList());

		final List<BundleTemplateData> bundleTemplateDatas = standardBundleTemplates.stream()
				.map(bundleTemplateModel -> getDealBundleTemplateConverter().convert(bundleTemplateModel))
				.collect(Collectors.toList());
		bundleTemplateDatas.forEach(bundleTemplateData -> bundleTemplateData.setCommencementDate(dealDepartureDate));

		return bundleTemplateDatas;
	}

	@Override
	protected FareSearchRequestData getFareSearchRequest(final BundleTemplateModel transportBundleTemplateModel,
			final Date departureDate, final Date returnDate)
	{
		final FareSearchRequestData fareSearchRequestData = super
				.getFareSearchRequest(transportBundleTemplateModel, departureDate, returnDate);
		if (Objects.isNull(fareSearchRequestData))
		{
			return null;
		}
		fareSearchRequestData.getOriginDestinationInfo().forEach(originDestinationInfoData -> {
			final String routeBundleTemplateId = ((DealOriginDestinationInfoData) originDestinationInfoData).getPackageId();

			final List<String> transportOfferingModels = getRouteBundleTemplateTransportOfferings(routeBundleTemplateId);
			if (CollectionUtils.isNotEmpty(transportOfferingModels))
			{
				originDestinationInfoData.setTransportOfferings(transportOfferingModels);
			}

		});
		fareSearchRequestData.getTravelPreferences().setCabinPreference(
				((DealOriginDestinationInfoData) fareSearchRequestData.getOriginDestinationInfo().get(0)).getCabinClass());

		final VehicleTypeCode vehicleTypeCode = getVehicleType(transportBundleTemplateModel);

		// to be handled
		if(Objects.nonNull(vehicleTypeCode))
		{
			fareSearchRequestData.setVehicleInfoData(createVehicleTypes(transportBundleTemplateModel, vehicleTypeCode.getCode()));
		}

		final SearchProcessingInfoData searchProcessingInfoData = new SearchProcessingInfoData();
		searchProcessingInfoData.setDisplayOrder(FareSelectionDisplayOrder.PRICE_DESC.toString());
		fareSearchRequestData.setSearchProcessingInfo(searchProcessingInfoData);
		return fareSearchRequestData;
	}

	protected VehicleTypeCode getVehicleType(final BundleTemplateModel transportBundleTemplateModel)
	{
		final BundleTemplateModel childTemplate = transportBundleTemplateModel.getChildTemplates().get(0);
		if(childTemplate instanceof RouteBundleTemplateModel)
		{
			return ((RouteBundleTemplateModel) childTemplate).getVehicleType();
		}
		return null;
	}

	@Override
	protected AccommodationAvailabilityRequestData getAccommodationAvailabilityRequest(
			final AccommodationBundleTemplateModel accommodationBundleTemplateModel, final Date departureDate, final Date returnDate)
	{
		final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData = new AccommodationAvailabilityRequestData();
		final CriterionData criterion = new CriterionData();
		final StayDateRangeData stayDateRange = new StayDateRangeData();
		stayDateRange.setStartTime(departureDate);
		stayDateRange.setEndTime(returnDate);
		criterion.setStayDateRange(stayDateRange);
		final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
		roomStayCandidateData.setAccommodationCode(accommodationBundleTemplateModel.getAccommodation().getCode());
		roomStayCandidateData.setRatePlanCode(accommodationBundleTemplateModel.getRatePlan().getCode());
		roomStayCandidateData.setRoomStayCandidateRefNumber(0);
		roomStayCandidateData.setPassengerTypeQuantityList(
				createPassengerTypeQuantities(accommodationBundleTemplateModel.getParentTemplate()));
		criterion.setRoomStayCandidates(Arrays.asList(roomStayCandidateData));
		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData
				.setAccommodationOfferingCode(accommodationBundleTemplateModel.getAccommodationOffering().getCode());
		accommodationReferenceData
				.setPropertyAccomodationCodes(accommodationBundleTemplateModel.getAccommodationOffering().getAccommodations());
		criterion.setAccommodationReference(accommodationReferenceData);
		criterion.setBundleTemplateId(accommodationBundleTemplateModel.getId());
		accommodationAvailabilityRequestData.setCriterion(criterion);
		return accommodationAvailabilityRequestData;
	}

	@Override
	public boolean isDealAvailable(final BundleTemplateModel activityBundle,
			final AccommodationBundleTemplateModel accommodationBundleTemplateModel, final Date startDate,
			final int dealLength)
	{

		if (Objects.isNull(accommodationBundleTemplateModel))
		{
			return Boolean.FALSE;
		}

		for (int length = 0; length < dealLength; length++)
		{
			final Date date = TravelDateUtils.addDays(startDate, length);

			final boolean isAccommodationProductAvailable = isAccommodationProductAvailable(accommodationBundleTemplateModel, date);
			if (!isAccommodationProductAvailable)
			{
				return Boolean.FALSE;
			}

			if (Objects.nonNull(activityBundle))
			{
				final boolean isActivityProductAvailable = isActivityProductAvailable(activityBundle, date);
				if (!isActivityProductAvailable)
				{
					return Boolean.FALSE;
				}
			}
		}
		return Boolean.TRUE;
	}

	protected boolean isAccommodationProductAvailable(final AccommodationBundleTemplateModel accommodationBundleTemplateModel,
			final Date date)
	{
		final Integer availableStock = getBcfProductFacade()
				.getStockForDate(accommodationBundleTemplateModel.getAccommodation(), date,
						Collections.singletonList(accommodationBundleTemplateModel.getAccommodationOffering()));
		if (Objects.isNull(availableStock) || availableStock.intValue() <= 0 || !getBcfDealRoomRatesHandler()
				.validateRoomRateAgainstDate(date, accommodationBundleTemplateModel.getRoomRateProduct()))
		{
			LOG.error("Hotel :" + accommodationBundleTemplateModel.getAccommodationOffering().getName()
					+ " do not have stock for room:" + accommodationBundleTemplateModel.getAccommodation().getName() + " on date:"
					+ date);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	protected boolean isActivityProductAvailable(final BundleTemplateModel activityBundle, final Date date)
	{
		if (Objects.nonNull(activityBundle) && CollectionUtils.isNotEmpty(activityBundle.getProducts()))
		{
			final WarehouseModel defaultWarehouse = getWarehouseService().getWarehouseForCode("default");
			for (final ProductModel productModel : activityBundle.getProducts())
			{
				final Integer activityAvailableStock = getBcfTravelCommerceStockService()
						.getStockForDate(productModel, date, Collections.singletonList(defaultWarehouse));

				if (Objects.isNull(activityAvailableStock) || activityAvailableStock.intValue() <= 0)
				{
					LOG.error("Product :" + productModel.getName() + " do not have stock on date:" + date);
					return Boolean.FALSE;
				}
			}

		}
		return Boolean.TRUE;
	}

	@Override
	public List<String> getRouteBundleTemplateTransportOfferings(final String id)
	{
		final List<TransportOfferingModel> transportOfferingModels = getBcfTravelBundleTemplateService()
				.getRouteBundleTemplateTransportOfferings(id);
		if (CollectionUtils.isEmpty(transportOfferingModels))
		{
			return Collections.emptyList();
		}
		return transportOfferingModels.stream().map(TransportOfferingModel::getCode)
				.collect(Collectors.toList());
	}

	@Override
	public List<TransportOfferingData> getRouteBundleTemplateTransportOfferingDatas(final String id)
	{
		final List<TransportOfferingModel> transportOfferingModels = getBcfTravelBundleTemplateService()
				.getRouteBundleTemplateTransportOfferings(id);
		if (CollectionUtils.isEmpty(transportOfferingModels))
		{
			return Collections.emptyList();
		}
		return getTransportOfferingConverter().convertAll(transportOfferingModels);
	}

	protected List<VehicleTypeQuantityData> createVehicleTypes(final BundleTemplateModel transportBundleTemplateModel, final String vehicleTypeCode)//NOSONAR
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityList = new ArrayList<>();
		final List<RouteBundleTemplateModel> routeBundles = transportBundleTemplateModel.getChildTemplates().stream()
				.filter(child -> child instanceof RouteBundleTemplateModel).map(child -> (RouteBundleTemplateModel) child)
				.collect(Collectors.toList());//NOSONAR
		final int numberOfVehicles = CollectionUtils.size(routeBundles);

		for (int i = 0; i < numberOfVehicles; i++)
		{
			final VehicleTypeQuantityData vehicleInfo = new VehicleTypeQuantityData();
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			vehicleTypeData.setCode(vehicleTypeCode);
			vehicleInfo.setVehicleType(vehicleTypeData);
			vehicleInfo.setQty(1);
			/*
				TODO we either need to configure the length/height/width while creating the deals
			 	or we need to fetch the max value from the DB based on a vehicle code and use the values below which I think is a better solution
			 	as eBooking expects a positive number and not "0"
			 */
			vehicleInfo.setLength(10);
			vehicleInfo.setHeight(10);
			vehicleInfo.setWidth(10);
			vehicleTypeQuantityList.add(vehicleInfo);
		}
		return vehicleTypeQuantityList;
	}

	protected BcfDealSearchResponsePipelineManager getBcfDealSearchResponsePipelineManager()
	{
		return this.bcfDealSearchResponsePipelineManager;
	}

	@Required
	public void setBcfDealSearchResponsePipelineManager(
			final BcfDealSearchResponsePipelineManager bcfDealSearchResponsePipelineManager)
	{
		this.bcfDealSearchResponsePipelineManager = bcfDealSearchResponsePipelineManager;
	}

	/**
	 * @return the bcfTravelBundleTemplateService
	 */
	protected BcfTravelBundleTemplateService getBcfTravelBundleTemplateService()
	{
		return bcfTravelBundleTemplateService;
	}

	/**
	 * @param bcfTravelBundleTemplateService the bcfTravelBundleTemplateService to set
	 */
	@Required
	public void setBcfTravelBundleTemplateService(final BcfTravelBundleTemplateService bcfTravelBundleTemplateService)
	{
		this.bcfTravelBundleTemplateService = bcfTravelBundleTemplateService;
	}

	protected Converter<TransportOfferingModel, TransportOfferingData> getTransportOfferingConverter()
	{
		return transportOfferingConverter;
	}

	@Required
	public void setTransportOfferingConverter(
			final Converter<TransportOfferingModel, TransportOfferingData> transportOfferingConverter)
	{
		this.transportOfferingConverter = transportOfferingConverter;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected BcfDealRoomRatesHandler getBcfDealRoomRatesHandler()
	{
		return bcfDealRoomRatesHandler;
	}

	@Required
	public void setBcfDealRoomRatesHandler(final BcfDealRoomRatesHandler bcfDealRoomRatesHandler)
	{
		this.bcfDealRoomRatesHandler = bcfDealRoomRatesHandler;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected BcfProductFacade getBcfProductFacade()
	{
		return bcfProductFacade;
	}

	@Required
	public void setBcfProductFacade(final BcfProductFacade bcfProductFacade)
	{
		this.bcfProductFacade = bcfProductFacade;
	}
}
