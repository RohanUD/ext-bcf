/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search;

import de.hybris.platform.commercefacades.accommodation.RoomRateCartData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.packages.cart.AddDealToCartData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.travelfacades.order.AccommodationCartFacade;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;


public interface BcfAccommodationCartFacade extends AccommodationCartFacade
{
	@Override
	List<CartModificationData> addAccommodationBundleToCart(
			final AccommodationBundleTemplateModel accommodationBundleTemplateModel, final AddDealToCartData addDealToCartData)
			throws CommerceCartModificationException;

	void disableAccommodationEntriesForRefNo(CartModel cart, final int journeyRefNo);

	void addAccommodationsToCart(final Date checkInDate, final Date checkOutDate, final String accommodationOfferingCode,
			final String accommodationCode, final List<RoomRateCartData> rates, final int numberOfRooms, final String ratePlanCode,
			List<RoomStayCandidateData> roomStayCandidates, Integer modifyRoomRef, boolean change)
			throws CommerceCartModificationException, CalculationException;

	void addAccommodationsToCartForPromotions(Date checkInDate, Date checkOutDate, String accommodationOfferingCode,
			String accommodationCode, List<RoomRateCartData> rates, int numberOfRooms, String ratePlanCode, String promotionCode, List<RoomStayCandidateData> roomStayCandidates,boolean change)
			throws CommerceCartModificationException, CalculationException;

	List<Integer> getAccommodationOrderEntryGroupRefsForOtherJourneys(int journeyRef);

	public boolean isModifyingSameAccommodation(final String accommodationOfferingCode,
			final String accommodationCode, boolean onlyAccommodationChange, final int journeyRef);

	List<Pair<ProductModel,Integer>>  getValidAdditionalProductsList(String accommodationOfferingCode, String promotionCode);

}
