/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.search;

import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.travelfacades.facades.packages.DealBundleTemplateFacade;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


public interface DealBundleDetailsFacade extends DealBundleTemplateFacade
{
	BcfPackageRequestData getBcfPackageRequestData(String dealBundleTemplateId, String departureDate);

	BcfPackagesResponseData getPackageResponseDetails(BcfPackageRequestData packageRequestData);

	void populateDealBundles(DealBundleTemplateModel dealBundleTemplateModel,
			Collection<BundleTemplateModel> dealBundleTemplateModels);

	List<String> getRouteBundleTemplateTransportOfferings(String id);

	List<TransportOfferingData> getRouteBundleTemplateTransportOfferingDatas(String id);

	boolean isValidDealDepartureDate(final String date, final String dealBundleTemplateId);

	boolean isDealAvailable(final BundleTemplateModel activityBundle,
			AccommodationBundleTemplateModel accommodationBundleTemplateModel, Date startDate, int dealLength);

}
