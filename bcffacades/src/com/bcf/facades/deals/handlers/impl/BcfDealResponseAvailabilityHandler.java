/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.handlers.impl;

import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationOfferingFacade;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.deals.handlers.BcfPackageResponseHandler;
import com.bcf.facades.packages.request.BcfPackageProductRequestData;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackageResponseData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


public class BcfDealResponseAvailabilityHandler implements BcfPackageResponseHandler
{
	private AccommodationOfferingFacade accommodationOfferingFacade;

	@Override
	public void handle(final BcfPackageRequestData bcfPackageRequestData, final BcfPackagesResponseData bcfPackagesResponseData)
	{
		bcfPackagesResponseData.setAvailable(Boolean.TRUE);
		for (final PackageResponseData packageResponseData : bcfPackagesResponseData.getPackageResponses())
		{
			if (!(packageResponseData instanceof BcfPackageResponseData))
			{
				continue;
			}
			final BcfPackageResponseData bcfPackageResponseData = (BcfPackageResponseData) packageResponseData;
			final BcfPackageProductRequestData packageRequestData = bcfPackageRequestData.getBcfPackageProductDatas().stream()
					.filter(bcfPackageProductRequestData -> bcfPackageProductRequestData.getPackageId()
							.equals(bcfPackageResponseData.getPackageId()))
					.findFirst().get();
			final boolean isTransportPackageResponseAvailable = isTransportPackageResponseAvailable(packageRequestData,
					bcfPackageResponseData);
			final boolean isAccommodationPackageResponseAvailable = isAccommodationPackageResponseAvailable(bcfPackageResponseData);
			final boolean isStandardPackageResponsesAvailable = isStandardPackageResponsesAvailable(bcfPackageResponseData);
			packageResponseData.setAvailable(isTransportPackageResponseAvailable && isAccommodationPackageResponseAvailable
					&& isStandardPackageResponsesAvailable);
			if (!packageResponseData.isAvailable())
			{
				bcfPackagesResponseData.setAvailable(Boolean.FALSE);
				return;
			}
		}

	}

	protected boolean isTransportPackageResponseAvailable(final BcfPackageProductRequestData packageRequestData,
			final BcfPackageResponseData packageResponseData)
	{
		if (Objects.isNull(packageResponseData.getTransportPackageResponse())
				|| Objects.isNull(packageResponseData.getTransportPackageResponse().getFareSearchResponse()))
		{
			return false;
		}
		final List<ItineraryPricingInfoData> itineraryPricingInfos = packageResponseData.getTransportPackageResponse()
				.getFareSearchResponse().getPricedItineraries().stream()
				.flatMap(pricedItineraryData -> pricedItineraryData.getItineraryPricingInfos().stream()).collect(Collectors.toList());
		return itineraryPricingInfos.stream().allMatch(ItineraryPricingInfoData::isAvailable);
	}

	protected boolean isAccommodationPackageResponseAvailable(final BcfPackageResponseData packageResponseData)
	{
		return this.getAccommodationOfferingFacade().isAccommodationAvailableForQuickSelection(
				packageResponseData.getAccommodationPackageResponse().getAccommodationAvailabilityResponse());
	}

	protected boolean isStandardPackageResponsesAvailable(final BcfPackageResponseData packageResponseData)
	{
		return packageResponseData.getStandardPackageResponses().stream()
				.flatMap(response -> response.getPackageProducts().stream())
				.allMatch(packageProductData -> packageProductData.getProduct().getStock() == null
						|| packageProductData.getProduct().getStock().getStockLevel() > 0L);
	}

	protected AccommodationOfferingFacade getAccommodationOfferingFacade()
	{
		return this.accommodationOfferingFacade;
	}

	@Required
	public void setAccommodationOfferingFacade(final AccommodationOfferingFacade accommodationOfferingFacade)
	{
		this.accommodationOfferingFacade = accommodationOfferingFacade;
	}
}
