/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.handlers.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.deals.handlers.BcfPackageResponseHandler;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


public class BcfPackageFareBundlesPriceDifferenceHandler implements BcfPackageResponseHandler
{
	private static final String PLUS_SIGN = "+";
	private PriceDataFactory priceDataFactory;

	@Override
	public void handle(final BcfPackageRequestData bcfPackageRequestData, final BcfPackagesResponseData bcfPackagesResponseData)
	{
		if (bcfPackagesResponseData.isAvailable())
		{
			bcfPackagesResponseData.getPackageResponses().forEach(packageResponseData -> {
				final FareSelectionData fareSearchResponse = packageResponseData.getTransportPackageResponse()
						.getFareSearchResponse();
				if (fareSearchResponse != null && !CollectionUtils.isEmpty(fareSearchResponse.getPricedItineraries()))
				{
					final List<PricedItineraryData> selectedPricedItineraries = fareSearchResponse.getPricedItineraries()
							.stream()
							.filter(pricedItineraryData -> pricedItineraryData.getItineraryPricingInfos().stream()
									.anyMatch(ItineraryPricingInfoData::isSelected)
							).collect(Collectors.toList());
					final Map<Integer, ItineraryPricingInfoData> selectedItineraryPricingInfos = new HashMap<>();
					selectedPricedItineraries.forEach(pricedItinerary ->
							pricedItinerary.getItineraryPricingInfos().stream().filter(ItineraryPricingInfoData::isSelected).findAny()
									.ifPresent(itineraryPricingInfo ->
											selectedItineraryPricingInfos
													.put(pricedItinerary.getOriginDestinationRefNumber(), itineraryPricingInfo)
									)
					);
					fareSearchResponse.getPricedItineraries().forEach(pricedItineraryData -> {

						final ItineraryPricingInfoData selectedItineraryPricingInfo = selectedItineraryPricingInfos
								.get(pricedItineraryData.getOriginDestinationRefNumber());
						if (Objects.isNull(selectedItineraryPricingInfo))
						{
							return;
						}
						final BigDecimal selectedPrice = selectedItineraryPricingInfo.getTotalFare().getTotalPrice().getValue();
						pricedItineraryData.getItineraryPricingInfos().stream().filter(ItineraryPricingInfoData::isAvailable)
								.forEach(itineraryPricingInfo -> {
									final BigDecimal priceDifference = itineraryPricingInfo.getTotalFare().getTotalPrice().getValue()
											.subtract(selectedPrice);
									itineraryPricingInfo.getTotalFare().setPriceDifference(this.getPriceDataFactory()
											.create(PriceDataType.BUY, priceDifference,
													itineraryPricingInfo.getTotalFare().getTotalPrice().getCurrencyIso()));
									this.updateFormattedValue(itineraryPricingInfo.getTotalFare().getPriceDifference());
								});
					});
				}
			});
		}
	}

	protected void updateFormattedValue(final PriceData priceData)
	{
		if (priceData.getValue().compareTo(BigDecimal.ZERO) >= 0)
		{
			priceData.setFormattedValue(PLUS_SIGN + priceData.getFormattedValue());
		}

	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return this.priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}
}
