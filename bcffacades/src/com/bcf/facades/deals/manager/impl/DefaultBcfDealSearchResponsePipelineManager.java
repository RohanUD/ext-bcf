/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.deals.manager.impl;

import de.hybris.platform.commercefacades.packages.request.AccommodationPackageRequestData;
import de.hybris.platform.commercefacades.packages.request.TransportPackageRequestData;
import de.hybris.platform.commercefacades.packages.response.AccommodationPackageResponseData;
import de.hybris.platform.commercefacades.packages.response.PackageProductData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.packages.response.StandardPackageResponseData;
import de.hybris.platform.commercefacades.packages.response.TransportPackageResponseData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.travelfacades.facades.accommodation.manager.AccommodationDetailsPipelineManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.deals.handlers.BcfPackageResponseHandler;
import com.bcf.facades.deals.manager.BcfDealSearchResponsePipelineManager;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.packages.manager.BcfStandardPackagePipelineManager;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackageResponseData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;
import com.bcf.facades.price.calculation.BcfPriceCalculationFacade;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfDealSearchResponsePipelineManager implements BcfDealSearchResponsePipelineManager
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfDealSearchResponsePipelineManager.class);

	private BcfFareSearchFacade dealFareSearchFacade;
	private AccommodationDetailsPipelineManager dealAccommodationDetailsPipelineManager;
	private BcfStandardPackagePipelineManager bcfStandardPackagePipelineManager;
	private List<BcfPackageResponseHandler> handlers;
	private BcfPriceCalculationFacade bcfPriceCalculationFacade;
	private static final String PACKAGE_TRANSPORT_RESPONSE_ERROR = "package.transport.response.error";

	@Override
	public BcfPackagesResponseData executePipeline(final BcfPackageRequestData packageRequestData)
	{
		final BcfPackagesResponseData bcfPackagesResponseData = new BcfPackagesResponseData();
		final List<PackageResponseData> packageResponseDatas = new ArrayList<>();
		bcfPackagesResponseData.setPackageResponses(packageResponseDatas);
		packageRequestData.getBcfPackageProductDatas().forEach(bcfPackageProductRequestData -> {
			final BcfPackageResponseData bcfPackageProductResponseData = new BcfPackageResponseData();
			bcfPackageProductResponseData.setPackageId(bcfPackageProductRequestData.getPackageId());
			bcfPackageProductResponseData.setTransportPackageResponse(
					getTransportPackageResponse(bcfPackageProductRequestData.getTransportPackageRequest()));
			if (Objects.nonNull(bcfPackageProductResponseData.getTransportPackageResponse())
					&& Objects.nonNull(bcfPackageProductResponseData.getTransportPackageResponse().getFareSearchResponse())
					&& CollectionUtils.isNotEmpty(
					bcfPackageProductResponseData.getTransportPackageResponse().getFareSearchResponse().getPricedItineraries()))
			{
				bcfPackageProductResponseData.getTransportPackageResponse().getFareSearchResponse().getPricedItineraries().forEach(
						pricedItineraryData -> pricedItineraryData.setDealBundleId(bcfPackageProductRequestData.getPackageId()));
			}
			bcfPackageProductResponseData.setAccommodationPackageResponse(
					getAccommodationPackageResponse(bcfPackageProductRequestData.getAccommodationPackageRequest()));
			bcfPackageProductResponseData
					.setStandardPackageResponses(getStandardPackageResponses(bcfPackageProductRequestData.getBundleTemplates()));

			packageResponseDatas.add(bcfPackageProductResponseData);
		});

		getHandlers().forEach(handler -> handler.handle(packageRequestData, bcfPackagesResponseData));
		return bcfPackagesResponseData;
	}


	protected TransportPackageResponseData getTransportPackageResponse(
			final TransportPackageRequestData transportPackageRequestData)
	{
		final TransportPackageResponseData transportPackageResponseData = new TransportPackageResponseData();
		try
		{
			final FareSelectionData fareSelectionData = getDealFareSearchFacade()
					.performSearch(transportPackageRequestData.getFareSearchRequest());
			if (CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
			{
				getBcfPriceCalculationFacade().addMarginToPricedItineraries(fareSelectionData.getPricedItineraries());
			}
			transportPackageResponseData.setFareSearchResponse(fareSelectionData);
		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex.getMessage());
			transportPackageResponseData.setErrorMessage(PACKAGE_TRANSPORT_RESPONSE_ERROR);
		}
		return transportPackageResponseData;
	}

	protected AccommodationPackageResponseData getAccommodationPackageResponse(
			final AccommodationPackageRequestData accommodationPackageRequest)
	{
		final AccommodationPackageResponseData accommodationPackageResponseData = new AccommodationPackageResponseData();
		accommodationPackageResponseData.setAccommodationAvailabilityResponse(getDealAccommodationDetailsPipelineManager()
				.executePipeline(accommodationPackageRequest.getAccommodationAvailabilityRequest()));
		return accommodationPackageResponseData;
	}

	protected List<StandardPackageResponseData> getStandardPackageResponses(final List<BundleTemplateData> bundleTemplates)
	{
		final StandardPackageResponseData standardPackageResponseData = new StandardPackageResponseData();
		final List<PackageProductData> packageProductDatas = bundleTemplates.stream()
				.flatMap(bundleTemplate -> getBcfStandardPackagePipelineManager().executePipeline(bundleTemplate).stream())
				.collect(Collectors.toList());
		standardPackageResponseData.setPackageProducts(packageProductDatas);
		return Collections.singletonList(standardPackageResponseData);
	}

	protected BcfFareSearchFacade getDealFareSearchFacade()
	{
		return dealFareSearchFacade;
	}

	@Required
	public void setDealFareSearchFacade(final BcfFareSearchFacade dealFareSearchFacade)
	{
		this.dealFareSearchFacade = dealFareSearchFacade;
	}

	protected AccommodationDetailsPipelineManager getDealAccommodationDetailsPipelineManager()
	{
		return dealAccommodationDetailsPipelineManager;
	}

	@Required
	public void setDealAccommodationDetailsPipelineManager(
			final AccommodationDetailsPipelineManager dealAccommodationDetailsPipelineManager)
	{
		this.dealAccommodationDetailsPipelineManager = dealAccommodationDetailsPipelineManager;
	}

	/**
	 * @return the bcfStandardPackagePipelineManager
	 */
	protected BcfStandardPackagePipelineManager getBcfStandardPackagePipelineManager()
	{
		return bcfStandardPackagePipelineManager;
	}

	/**
	 * @param bcfStandardPackagePipelineManager the bcfStandardPackagePipelineManager to set
	 */
	@Required
	public void setBcfStandardPackagePipelineManager(final BcfStandardPackagePipelineManager bcfStandardPackagePipelineManager)
	{
		this.bcfStandardPackagePipelineManager = bcfStandardPackagePipelineManager;
	}

	protected List<BcfPackageResponseHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<BcfPackageResponseHandler> handlers)
	{
		this.handlers = handlers;
	}

	/**
	 * @return the bcfPriceCalculationFacade
	 */
	protected BcfPriceCalculationFacade getBcfPriceCalculationFacade()
	{
		return bcfPriceCalculationFacade;
	}

	/**
	 * @param bcfPriceCalculationFacade the bcfPriceCalculationFacade to set
	 */
	@Required
	public void setBcfPriceCalculationFacade(final BcfPriceCalculationFacade bcfPriceCalculationFacade)
	{
		this.bcfPriceCalculationFacade = bcfPriceCalculationFacade;
	}
}
