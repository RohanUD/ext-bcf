/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.activity.search;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import java.util.Date;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.search.request.data.ActivitySearchCriteriaData;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;
import com.bcf.facades.search.facetdata.ActivitySearchPageData;



public interface ActivityProductSearchFacade
{
	ActivitiesResponseData doSearch(ActivitySearchCriteriaData searchCriteria, PageableData pageableData);

	ActivitySearchPageData<SearchStateData, ActivityResponseData> searchActivities(ActivitySearchCriteriaData searchCriteria,
			PageableData pageableData);

	/**
	 * create actvity price per pasenger and schedules  {@link ActivityPricePerPassengerWithSchedule}
	 *
	 * @param productCode
	 * @param commencementDate
	 * @param locationCode
	 * @return
	 */
	ActivityPricePerPassengerWithSchedule createActivityPricePerPassengerAndSchedule(String productCode, Date commencementDate,
			String locationCode);

	/**
	 * splits location code coming from accommodation finder form by a pipe symbol and return the first location code from the hierarchy
	 *
	 * @param destination
	 * @return
	 */
	String createActivityLocation(String destination);

	void getValidAvailableSchedules(final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule,
			final Date checkInDate, final Date checkOutDate);
}
