/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.activity.search.impl;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.activities.solrfacetsearch.ActivitiesSearchService;
import com.bcf.facades.activity.search.ActivitiesSolrSearchFacade;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;
import com.bcf.facades.search.facetdata.ActivitySearchPageData;


public class DefaultActivitiesSolrSearchFacade<ITEM extends ActivityResponseData> implements ActivitiesSolrSearchFacade
{
	private ThreadContextService threadContextService;
	private Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder;
	private ActivitiesSearchService activitiesSearchService;
	private Converter<ActivitySearchPageData<SolrSearchQueryData, SearchResultValueData>,
			ActivitySearchPageData<SearchStateData, ITEM>> activitiesSearchPageConverter;

	@Override
	public ActivitySearchPageData searchActivities(final SearchData searchData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<ActivitySearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public ActivitySearchPageData<SearchStateData, ITEM> execute()
					{
						return getActivitiesSearchPageConverter()
								.convert(getActivitiesSearchService().doSearch(decodeSearchData(searchData), null));
					}
				});
	}

	@Override
	public ActivitySearchPageData searchActivities(final SearchData searchData, final PageableData pageableData)
	{
		return getThreadContextService().executeInContext(
				new ThreadContextService.Executor<ActivitySearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public ActivitySearchPageData<SearchStateData, ITEM> execute()
					{
						return getActivitiesSearchPageConverter()
								.convert(getActivitiesSearchService().doSearch(decodeSearchData(searchData), pageableData));
					}
				});
	}

	protected SolrSearchQueryData decodeSearchData(final SearchData searchData)
	{
		return getSolrTravelSearchQueryDecoder().convert(searchData);
	}

	protected ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	@Required
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

	protected Converter<SearchData, SolrSearchQueryData> getSolrTravelSearchQueryDecoder()
	{
		return solrTravelSearchQueryDecoder;
	}

	@Required
	public void setSolrTravelSearchQueryDecoder(
			final Converter<SearchData, SolrSearchQueryData> solrTravelSearchQueryDecoder)
	{
		this.solrTravelSearchQueryDecoder = solrTravelSearchQueryDecoder;
	}


	protected Converter<ActivitySearchPageData<SolrSearchQueryData, SearchResultValueData>, ActivitySearchPageData<SearchStateData, ITEM>> getActivitiesSearchPageConverter()
	{
		return activitiesSearchPageConverter;
	}

	@Required
	public void setActivitiesSearchPageConverter(
			final Converter<ActivitySearchPageData<SolrSearchQueryData, SearchResultValueData>, ActivitySearchPageData<SearchStateData, ITEM>> activitiesSearchPageConverter)
	{
		this.activitiesSearchPageConverter = activitiesSearchPageConverter;
	}

	protected ActivitiesSearchService getActivitiesSearchService()
	{
		return activitiesSearchService;
	}

	@Required
	public void setActivitiesSearchService(
			final ActivitiesSearchService activitiesSearchService)
	{
		this.activitiesSearchService = activitiesSearchService;
	}
}
