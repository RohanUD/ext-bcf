/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.activity.search.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.accommodation.RateData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.travel.search.data.SearchData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.product.ActivityScheduleModel;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.services.BcfProductService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.activity.data.ActivityPricePerPassenger;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.activity.data.SpecializedPassengerTypeData;
import com.bcf.facades.activity.search.ActivitiesSolrSearchFacade;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.activity.search.request.data.ActivitySearchCriteriaData;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.search.facetdata.ActivitySearchPageData;


public class DefaultActivityProductSearchFacade implements ActivityProductSearchFacade
{
	private static final int MAX_PAGE_LIMIT = 100;
	private static final String BCF_BASE_PASSENGERS = "bcfBasePassengers";

	private ActivitiesSolrSearchFacade<ActivityResponseData> activtiesSolrSearchFacade;
	private BcfProductService bcfProductService;
	private CatalogVersionService catalogVersionService;
	private BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade;
	private BcfTravelLocationService travelLocationService;
	private BCFPassengerTypeService passengerTypeService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private Converter<ActivityScheduleModel, ActivityScheduleData> activityScheduleConverter;
	private ConfigurationService configurationService;

	@Override
	public ActivitiesResponseData doSearch(final ActivitySearchCriteriaData searchCriteria, final PageableData pageableData)
	{
		final ActivitiesResponseData activitiesResponseData = new ActivitiesResponseData();
		final ActivitySearchPageData<SearchStateData, ActivityResponseData> activitySearchPageData = searchActivities(
				searchCriteria, pageableData);
		activitiesResponseData.setFacets(activitySearchPageData.getFacets());
		activitiesResponseData.setSorts(activitySearchPageData.getSorts());
		activitiesResponseData.setActivityResponseData(activitySearchPageData.getResults());
		activitiesResponseData.setFilteredFacets(activitySearchPageData.getFilteredFacets());
		activitiesResponseData.setPagination(activitySearchPageData.getPagination());

		return activitiesResponseData;
	}

	/**
	 * @param productCode
	 * @param commencementDate
	 * @param locationCode
	 * @return {@link ActivityPricePerPassengerWithSchedule}
	 */
	@Override
	public ActivityPricePerPassengerWithSchedule createActivityPricePerPassengerAndSchedule(final String productCode,
			final Date commencementDate, String locationCode)
	{
		final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule = new ActivityPricePerPassengerWithSchedule();
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(BcfCoreConstants.PRODUCTCATALOG,
				BcfCoreConstants.CATALOG_VERSION);
		final ActivityProductModel activityProduct = (ActivityProductModel) getBcfProductService()
				.getProductForCode(catalogVersion, productCode);
		if (StringUtils.isBlank(locationCode))
		{
			locationCode = activityProduct.getDestination().getCode();
		}
		final List<SpecializedPassengerTypeModel> specializedPassengerTypes = new ArrayList<>(
				activityProduct.getSpecializedPassengerTypes());

		final List<ActivityScheduleData> activityScheduleDataList = getActivityScheduleConverter()
				.convertAll(activityProduct.getActivityScheduleList());
		activityPricePerPassengerWithSchedule.setAvailableSchedules(activityScheduleDataList);
		activityPricePerPassengerWithSchedule.setProductCode(activityProduct.getCode());
		activityPricePerPassengerWithSchedule.setBlocktypeStatus(activityProduct.getStockLevelType().getCode());

		final List<ActivityPricePerPassenger> activityPricePerPassengerList = new ArrayList<>();
		populateActivityPricePerPassenger(commencementDate, locationCode, activityProduct, specializedPassengerTypes,
				activityPricePerPassengerList);

		//sort based on pax min age desc
		Collections.sort(activityPricePerPassengerList,
				(o1, o2) -> o2.getPassengerType().getMinAge() - o1.getPassengerType().getMinAge());
		activityPricePerPassengerWithSchedule.setActivityPricePerPassenger(activityPricePerPassengerList);
		return activityPricePerPassengerWithSchedule;
	}

	@Override
	public void getValidAvailableSchedules(final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule,
			final Date checkInDate, final Date checkOutDate)
	{
		final List<ActivityScheduleData> validAvailableSchedules = new ArrayList<>();
		if (activityPricePerPassengerWithSchedule != null)
		{
			if (CollectionUtils
					.isNotEmpty(activityPricePerPassengerWithSchedule.getAvailableSchedules()))
			{
				final List<ActivityScheduleData> availableSchedules = activityPricePerPassengerWithSchedule.getAvailableSchedules();
				for (final ActivityScheduleData availableScheduleData : availableSchedules)
				{
					if ((TravelDateUtils
							.isBefore(availableScheduleData.getStartDate(), ZoneId.systemDefault(), checkInDate, ZoneId.systemDefault())
							|| TravelDateUtils
							.isSameDate(availableScheduleData.getStartDate(), checkInDate)) &&
							(TravelDateUtils.isAfter(availableScheduleData.getEndDate(), ZoneId.systemDefault(), checkOutDate,
									ZoneId.systemDefault()) || TravelDateUtils
									.isSameDate(availableScheduleData.getEndDate(), checkOutDate)))
					{
						validAvailableSchedules.add(availableScheduleData);
					}
				}
				activityPricePerPassengerWithSchedule.setCheckInDate(checkInDate);
			}
			activityPricePerPassengerWithSchedule.setAvailableSchedules(validAvailableSchedules);
		}

	}

	/**
	 * @param commencementDate
	 * @param locationCode
	 * @param activityProduct
	 * @param specializedPassengerTypes
	 * @param activityPricePerPassengerList
	 */
	protected void populateActivityPricePerPassenger(final Date commencementDate, final String locationCode,
			final ActivityProductModel activityProduct, final List<SpecializedPassengerTypeModel> specializedPassengerTypes,
			final List<ActivityPricePerPassenger> activityPricePerPassengerList)
	{
		if (CollectionUtils.isNotEmpty(specializedPassengerTypes))
		{
			specializedPassengerTypes.forEach(passengerType -> {
				createActivityPricePerPassenger(commencementDate, locationCode, activityProduct, activityPricePerPassengerList,
						passengerType);
			});
		}
		else
		{
			final List<String> configuredPassengerTypes = BCFPropertiesUtils
					.convertToList(getBcfConfigurablePropertiesService().getBcfPropertyValue(BCF_BASE_PASSENGERS));
			getPassengerTypeService().getPassengerTypes().stream()
					.filter(passengerType -> configuredPassengerTypes.contains(passengerType.getCode())).forEach(passengerType -> {
				createActivityPricePerPassenger(commencementDate, locationCode, activityProduct, activityPricePerPassengerList,
						passengerType);
			});
		}
	}

	protected void createActivityPricePerPassenger(final Date commencementDate, final String locationCode,
			final ActivityProductModel activityProduct, final List<ActivityPricePerPassenger> activityPricePerPassengerList,
			final PassengerTypeModel passengerType)
	{
		final ActivityPricePerPassenger activityPricePerPassenger = new ActivityPricePerPassenger();
		final PassengerTypeModel basePassenger = passengerType instanceof SpecializedPassengerTypeModel
				? ((SpecializedPassengerTypeModel) passengerType).getBasePassengerType()
				: passengerType;
		final RateData rateData = getBcfTravelCommercePriceFacade().calculateRateData(activityProduct, 1L, basePassenger,
				commencementDate, getTravelLocationService().getLocation(locationCode));
		if (Objects.nonNull(rateData))
		{
			activityPricePerPassenger.setPassengerType(convertModel(passengerType, basePassenger));
			activityPricePerPassenger.setPrice(rateData);
			activityPricePerPassengerList.add(activityPricePerPassenger);
		}
	}

	private SpecializedPassengerTypeData convertModel(final PassengerTypeModel passengerType,
			final PassengerTypeModel basePassenger)
	{
		final SpecializedPassengerTypeData specializedPassengerTypeData = new SpecializedPassengerTypeData();
		specializedPassengerTypeData.setCode(passengerType.getCode());
		final String name = passengerTypeService.getAgeRangeForPassengerType(passengerType);
		specializedPassengerTypeData.setName(name);
		specializedPassengerTypeData.setMinAge(passengerType.getMinAge());
		specializedPassengerTypeData.setMaxAge(Objects.nonNull(passengerType.getMaxAge()) ? passengerType.getMaxAge() : 0);
		specializedPassengerTypeData.setPassengerType(basePassenger.getCode());
		return specializedPassengerTypeData;
	}

	@Override
	public ActivitySearchPageData<SearchStateData, ActivityResponseData> searchActivities(
			final ActivitySearchCriteriaData activitySearchCriteriaData, final PageableData pageableData)
	{
		final SearchData searchData = new SearchData();

		final Map<String, String> filterTerms = new HashMap<>();
		final Map<String, String> rawQueryFilterTerms = new HashMap<>();
		if (StringUtils.isNotBlank(activitySearchCriteriaData.getDestination()))
		{
			filterTerms.put(BcfFacadesConstants.Activity.DESTINATION, activitySearchCriteriaData.getDestination());
		}
		if (CollectionUtils.isNotEmpty(activitySearchCriteriaData.getActivityCategoryTypes()))
		{
			filterTerms.put(BcfFacadesConstants.Activity.ACTIVITY_CATEGORY_TYPES,
					StringUtils.join(activitySearchCriteriaData.getActivityCategoryTypes(), "#"));
		}
		if (StringUtils.isNotBlank(activitySearchCriteriaData.getActivity())
				&& !StringUtils.equalsIgnoreCase(BcfFacadesConstants.Activity.ANY, activitySearchCriteriaData.getActivity()))
		{
			filterTerms.put(BcfFacadesConstants.Activity.ACTIVITY_CODE, String.valueOf(activitySearchCriteriaData.getActivity()));
		}
		if (StringUtils.isNotBlank(activitySearchCriteriaData.getDate()))
		{
			rawQueryFilterTerms.put(BcfFacadesConstants.DEPARTURE_DATE, activitySearchCriteriaData.getDate());
			searchData.setRawQueryFilterTerms(rawQueryFilterTerms);
		}
		searchData.setFilterTerms(filterTerms);

		final String query = StringUtils.isNotBlank(activitySearchCriteriaData.getQuery()) ? activitySearchCriteriaData.getQuery()
				: StringUtils.EMPTY;
		searchData.setQuery(query);

		if (StringUtils.isNotBlank(activitySearchCriteriaData.getSort()))
		{
			searchData.setSort(activitySearchCriteriaData.getSort());
		}

		return getActivtiesSolrSearchFacade().searchActivities(searchData, pageableData);
	}

	protected int getActivityListingPageSize()
	{
		final int activityListingPageSize = getConfigurationService().getConfiguration()
				.getInt(BcfFacadesConstants.ACTIVITY_SEARCH_RESULT_PAGE_SIZE);

		return activityListingPageSize > 0 ? activityListingPageSize : MAX_PAGE_LIMIT;
	}

	@Override
	public String createActivityLocation(final String destination)
	{
		return destination.contains("|") ? destination.split("\\|")[0] : destination;
	}

	protected ActivitiesSolrSearchFacade<ActivityResponseData> getActivtiesSolrSearchFacade()
	{
		return activtiesSolrSearchFacade;
	}

	@Required
	public void setActivtiesSolrSearchFacade(final ActivitiesSolrSearchFacade<ActivityResponseData> activtiesSolrSearchFacade)
	{
		this.activtiesSolrSearchFacade = activtiesSolrSearchFacade;
	}

	/**
	 * @return the bcfProductService
	 */
	protected BcfProductService getBcfProductService()
	{
		return bcfProductService;
	}

	@Required
	public void setBcfProductService(final BcfProductService bcfProductService)
	{
		this.bcfProductService = bcfProductService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected BCFTravelCommercePriceFacade getBcfTravelCommercePriceFacade()
	{
		return bcfTravelCommercePriceFacade;
	}

	@Required
	public void setBcfTravelCommercePriceFacade(final BCFTravelCommercePriceFacade bcfTravelCommercePriceFacade)
	{
		this.bcfTravelCommercePriceFacade = bcfTravelCommercePriceFacade;
	}

	protected BcfTravelLocationService getTravelLocationService()
	{
		return travelLocationService;
	}

	@Required
	public void setTravelLocationService(final BcfTravelLocationService travelLocationService)
	{
		this.travelLocationService = travelLocationService;
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected Converter<ActivityScheduleModel, ActivityScheduleData> getActivityScheduleConverter()
	{
		return activityScheduleConverter;
	}

	@Required
	public void setActivityScheduleConverter(
			final Converter<ActivityScheduleModel, ActivityScheduleData> activityScheduleConverter)
	{
		this.activityScheduleConverter = activityScheduleConverter;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
