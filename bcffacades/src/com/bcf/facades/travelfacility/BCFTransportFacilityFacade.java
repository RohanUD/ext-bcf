/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelfacility;

import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import java.util.List;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.travel.data.TransportFacilityPaginationData;


public interface BCFTransportFacilityFacade extends TransportFacilityFacade
{
	/**
	 * Return travel facility details
	 *
	 * @param travelFacilityCode
	 * @return TransportFacilityData
	 */
	TransportFacilityData getTravelFacilityDetailsForCode(String travelFacilityCode);

	String getTravelFacilityNameForCode(String travelFacilityCode);

	List<TransportFacilityData> getAllTransportFacilities();

	TransportFacilityData getTerminalTransportFacility(String transportFacilityCode);

	List<TravelRouteData> getTravelRoutesFromAndToTerminal(String transportFacilityCode);

	DestinationAndOriginForTravelRouteData getTravelRouteInfoForSourceAndDestination(String originCode, String destinationCode);

	TransportFacilityPaginationData getPaginatedTransportFacilities(final int pageSize, final int currentPage);
}
