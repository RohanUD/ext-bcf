/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.travelfacility.impl;

import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.facades.impl.DefaultTransportFacilityFacade;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.BcfTransportFacilityService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.travel.data.TransportFacilityPaginationData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;


public class BCFTransportFacilityFacadeImpl extends DefaultTransportFacilityFacade
		implements BCFTransportFacilityFacade
{
	private static final Logger LOG = Logger.getLogger(BCFTransportFacilityFacadeImpl.class);

	private BcfTransportFacilityService bcfTransportFacilityService;
	private Converter<TransportFacilityModel, TransportFacilityData> terminalTransportFacilityConverter;
	private Converter<TravelRouteModel, TravelRouteData> travelRouteConverter;

	@Resource(name = "travelRouteService")
	private BCFTravelRouteService bcfTravelRouteService;

	@Override
	public TransportFacilityData getTravelFacilityDetailsForCode(final String travelFacilityCode)
	{
		return getTransportFacilityConverter()
				.convert(bcfTransportFacilityService.getTransportFacilityForCode(travelFacilityCode));
	}

	@Override
	public String getTravelFacilityNameForCode(final String travelFacilityCode)
	{
		return bcfTransportFacilityService.getTransportFacilityForCode(travelFacilityCode).getName();
	}

	@Override
	public List<TransportFacilityData> getAllTransportFacilities()
	{
		return StreamUtil.safeStream(bcfTransportFacilityService.getAllTransportFacilities())
				.map(getTransportFacilityConverter()::convert).collect(Collectors.toList());
	}

	@Override
	public TransportFacilityPaginationData getPaginatedTransportFacilities(final int pageSize, final int currentPage)
	{
		final TransportFacilityPaginationData transportFacilityPaginationData = new TransportFacilityPaginationData();
		final SearchPageData<TransportFacilityModel> paginatedTransportFacilities = bcfTransportFacilityService
				.getPaginatedTransportFacilities(pageSize, currentPage);
		transportFacilityPaginationData
				.setResults(getTransportFacilityConverter().convertAll(paginatedTransportFacilities.getResults()));
		transportFacilityPaginationData.setPaginationData(paginatedTransportFacilities.getPagination());
		return transportFacilityPaginationData;
	}

	@Override
	public TransportFacilityData getTerminalTransportFacility(final String transportFacilityCode)
	{
		return Optional.ofNullable(this.getTransportFacilityService().getTransportFacility(transportFacilityCode))
				.map(terminalTransportFacilityConverter::convert).orElse(null);
	}


	@Override
	public List<TravelRouteData> getTravelRoutesFromAndToTerminal(final String transportFacilityCode)
	{
		final TransportFacilityModel transportFacility = this.getTransportFacilityService()
				.getTransportFacility(transportFacilityCode);
		final List<TravelRouteModel> allTravelRoutes = new ArrayList<>();
		allTravelRoutes.addAll(bcfTravelRouteService.getTravelRoutesForOrigin(transportFacility).stream()
				.filter(
						travelRouteData -> Objects.nonNull(travelRouteData.getIndirectRoute()) && (Boolean.FALSE).equals(travelRouteData
								.getIndirectRoute()) && Objects.nonNull(travelRouteData.getMapKmlFile())).collect(
						Collectors.toList()));
		allTravelRoutes.addAll(bcfTravelRouteService.getTravelRoutesForDestination(transportFacility).stream()
				.filter(
						travelRouteData -> Objects.nonNull(travelRouteData.getIndirectRoute()) && (Boolean.FALSE).equals(travelRouteData
								.getIndirectRoute()) && Objects.nonNull(travelRouteData.getMapKmlFile())).collect(
						Collectors.toList()));
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(allTravelRoutes))
		{
			final List<TravelRouteData> travelRouteDatalist = new ArrayList<>();
			allTravelRoutes.stream().forEach(travelRoute -> {
				travelRouteDatalist.add(getTravelRouteConverter().convert(travelRoute));
			});

			return travelRouteDatalist;
		}
		return Collections.emptyList();
	}

	@Override
	public DestinationAndOriginForTravelRouteData getTravelRouteInfoForSourceAndDestination(final String originCode,
			final String destinationCode)
	{
		final TransportFacilityModel depTransportFacility;
		final TransportFacilityModel arrTransportFacility;

		final List<TravelRouteModel> directTravelRoutes = bcfTravelRouteService
				.getDirectTravelRoutes(originCode, destinationCode);
		final DestinationAndOriginForTravelRouteData destinationAndOriginForTravelRouteData = new DestinationAndOriginForTravelRouteData();
		if (CollectionUtils.isNotEmpty(directTravelRoutes))
		{
			final TravelRouteModel travelRouteModel = directTravelRoutes.iterator().next();
			destinationAndOriginForTravelRouteData.setReservable(travelRouteModel.isReservable());
			depTransportFacility = travelRouteModel.getOrigin();
			arrTransportFacility = travelRouteModel.getDestination();
		}
		else
		{
			depTransportFacility = this.getTransportFacilityService().getTransportFacility(originCode);
			arrTransportFacility = this.getTransportFacilityService().getTransportFacility(destinationCode);

			if (Objects.isNull(depTransportFacility))
			{
				LOG.error("Transport facility couldn't find for the code " + originCode);
				return null;
			}

			if (Objects.isNull(arrTransportFacility))
			{
				LOG.error("Transport facility couldn't find for the code " + destinationCode);
				return null;
			}
		}

		destinationAndOriginForTravelRouteData.setDepartureTerminalCode(depTransportFacility.getCode());
		destinationAndOriginForTravelRouteData.setDepartureTerminalName(depTransportFacility.getName());
		destinationAndOriginForTravelRouteData
				.setDepartureRouteName(BcfTravelRouteUtil.translateName(depTransportFacility.getName()));
		if (Objects.nonNull(depTransportFacility.getLocation()))
		{
			destinationAndOriginForTravelRouteData.setDepartureLocation(depTransportFacility.getLocation().getCode());
			destinationAndOriginForTravelRouteData
					.setDepartureLocationName(depTransportFacility.getLocation().getName());
		}

		destinationAndOriginForTravelRouteData.setArrivalTerminalName(arrTransportFacility.getName());
		destinationAndOriginForTravelRouteData.setArrivalTerminalCode(arrTransportFacility.getCode());
		destinationAndOriginForTravelRouteData
				.setArrivalRouteName(BcfTravelRouteUtil.translateName(arrTransportFacility.getName()));
		if (Objects.nonNull(arrTransportFacility.getLocation()))
		{
			destinationAndOriginForTravelRouteData.setArrivalLocation(arrTransportFacility.getLocation().getCode());
			destinationAndOriginForTravelRouteData.setArrivalLocationName(arrTransportFacility.getLocation().getName());
		}

		return destinationAndOriginForTravelRouteData;
	}

	@Required
	public void setBcfTransportFacilityService(final BcfTransportFacilityService bcfTransportFacilityService)
	{
		this.bcfTransportFacilityService = bcfTransportFacilityService;
	}

	@Required
	public void setTerminalTransportFacilityConverter(
			final Converter<TransportFacilityModel, TransportFacilityData> terminalTransportFacilityConverter)
	{
		this.terminalTransportFacilityConverter = terminalTransportFacilityConverter;
	}

	public Converter<TravelRouteModel, TravelRouteData> getTravelRouteConverter()
	{
		return travelRouteConverter;
	}

	@Required
	public void setTravelRouteConverter(final Converter<TravelRouteModel, TravelRouteData> travelRouteConverter)
	{
		this.travelRouteConverter = travelRouteConverter;
	}
}
