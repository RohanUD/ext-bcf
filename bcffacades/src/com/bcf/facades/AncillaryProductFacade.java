/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades;

import de.hybris.platform.commercefacades.product.data.ProductData;
import java.util.List;
import com.bcf.facades.extra.product.BCFExtraProducts;
import com.bcf.webservices.travel.data.ProductsData;


public interface AncillaryProductFacade
{
	BCFExtraProducts getExtraProductsPerJourneyPerLegPerVessel();

	BCFExtraProducts getExtraProductsPerJourneyPerLegPerVessel(int journeyRefNo, int odRefNum);

	void createOrModifyProduct(ProductsData productsData);

	void deleteProduct(String productCode);

	List<ProductData> getAncillaryProductsForCodes(List<String> productCodes);
}
