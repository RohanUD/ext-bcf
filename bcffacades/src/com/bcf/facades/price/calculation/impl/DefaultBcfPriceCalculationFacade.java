/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.price.calculation.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.FareDetailsData;
import de.hybris.platform.commercefacades.travel.FareInfoData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.PrecisionUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.price.calculation.BcfPriceCalculationFacade;


public class DefaultBcfPriceCalculationFacade implements BcfPriceCalculationFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfPriceCalculationFacade.class);

	private TravelCommercePriceFacade travelCommercePriceFacade;

	@Override
	public void addMarginToPricedItineraries(final List<PricedItineraryData> pricedItineraries)
	{
		for (final PricedItineraryData pricedItinerary : pricedItineraries)
		{
			final TravelRouteData travelRoute = pricedItinerary.getItinerary().getRoute();
			final Double marginRate = travelRoute.getDestination().getMarginRate();
			LOG.info("Margin applied for " + travelRoute.getCode() + " : " + marginRate);
			if (Objects.nonNull(marginRate) && marginRate > 0)
			{
				for (final ItineraryPricingInfoData itineraryPricingInfo : pricedItinerary.getItineraryPricingInfos())
				{
					// adding margin to passenger fare price and get total passenger fare price value
					final double totalPassengerFarePriceVal = addMarginToPassengerFarePrice(itineraryPricingInfo, marginRate);
					// adding margin to vehicle fare price and get total vehicle fare price value
					final double totalVehicleFarePriceVal = addMarginToVehicleFarePrice(itineraryPricingInfo, marginRate);
					// adding margin to other charges and get total other charges price value
					final double totalOtherChargesPriceVal = addMarginToOtherCharges(itineraryPricingInfo, marginRate);
					// adding margin to total fare price
					final double totalFarePriceVal = PrecisionUtil
							.round(totalPassengerFarePriceVal + totalVehicleFarePriceVal + totalOtherChargesPriceVal);
					addMarginToTotalFarePrice(itineraryPricingInfo, totalFarePriceVal);
				}
			}
		}
	}

	/**
	 * adding margin to passenger fare down price
	 *
	 * @param itineraryPricingInfo
	 * @param marginRate
	 * @return total passenger fare price value
	 */
	protected double addMarginToPassengerFarePrice(final ItineraryPricingInfoData itineraryPricingInfo, final Double marginRate)
	{
		final List<PTCFareBreakdownData> ptcFareBreakdownDatas = itineraryPricingInfo.getPtcFareBreakdownDatas().stream()
				.filter(
						ptcFareBreakdownData -> ((ptcFareBreakdownData.getPassengerTypeQuantity().getQuantity() > 0)
								&& CollectionUtils
								.isNotEmpty(ptcFareBreakdownData.getFareInfos()))).collect(Collectors.toList());
		double totalPassengerFarePriceVal = 0d;
		if (CollectionUtils.isNotEmpty(ptcFareBreakdownDatas))
		{
			totalPassengerFarePriceVal = getTotalPassengerFarePriceVal(marginRate, ptcFareBreakdownDatas,
					totalPassengerFarePriceVal);
		}
		return totalPassengerFarePriceVal;
	}

	private double getTotalPassengerFarePriceVal(final Double marginRate, final List<PTCFareBreakdownData> ptcFareBreakdownDatas,
			double totalPassengerFarePriceVal)
	{
		for (final PTCFareBreakdownData ptcFareBreakdownData : ptcFareBreakdownDatas)
		{
			final List<FareDetailsData> filteredFareDetails = ptcFareBreakdownData.getFareInfos().stream()
					.flatMap(fareInfo -> fareInfo.getFareDetails().stream()).filter(
							fareDetails -> (Objects.nonNull(fareDetails.getFareProduct()) && Objects
									.nonNull(fareDetails.getFareProduct().getPrice()))).collect(Collectors.toList());
			if(CollectionUtils.isNotEmpty(filteredFareDetails))
			{
				for (final FareDetailsData fareDetails : filteredFareDetails)
				{
					final PriceData farePrice = getPriceAfterMargin(fareDetails.getFareProduct().getPrice(), marginRate);
					fareDetails.getFareProduct().setPrice(farePrice);


				}
				final PriceData totalPrice = getPriceAfterMargin(ptcFareBreakdownData.getTotalPrice(), ptcFareBreakdownData.getPassengerTypeQuantity()!=null?ptcFareBreakdownData.getPassengerTypeQuantity().getQuantity():1,marginRate);
				ptcFareBreakdownData.setTotalPrice(totalPrice);
				totalPassengerFarePriceVal = totalPassengerFarePriceVal + totalPrice.getValue().doubleValue();
			}


		}
		return totalPassengerFarePriceVal;
	}

	/**
	 * adding margin to vehicle fare price
	 *
	 * @param itineraryPricingInfo
	 * @param marginRate
	 * @return total vehicle fare price value
	 */
	protected double addMarginToVehicleFarePrice(final ItineraryPricingInfoData itineraryPricingInfo, final Double marginRate)
	{
		final List<VehicleFareBreakdownData> vehicleFareBreakdownDatas = StreamUtil.safeStream(itineraryPricingInfo
				.getVehicleFareBreakdownDatas())
				.filter(vehicleFareBreakdownData -> ((vehicleFareBreakdownData.getVehicleTypeQuantity().getQty() > 0)
						&& Objects.nonNull(vehicleFareBreakdownData.getFareInfos()))).collect(Collectors.toList());
		double totalVehicleFarePriceVal = 0d;
		if (CollectionUtils.isNotEmpty(vehicleFareBreakdownDatas))
		{
			for (final VehicleFareBreakdownData vehicleFareBreakdownData : vehicleFareBreakdownDatas)
			{
				final FareInfoData fareInfoData = vehicleFareBreakdownData.getFareInfos();
				for (final FareDetailsData fareDetails : fareInfoData.getFareDetails())
				{
					final PriceData farePrice = getPriceAfterMargin(fareDetails.getFareProduct().getPrice(), marginRate);
					fareDetails.getFareProduct().setPrice(farePrice);

					totalVehicleFarePriceVal = totalVehicleFarePriceVal + farePrice.getValue().doubleValue();
				}
			}
		}
		return totalVehicleFarePriceVal;
	}

	/**
	 * adding margin to other charges
	 *
	 * @param itineraryPricingInfo
	 * @param marginRate
	 * @return total other charges value
	 */
	protected double addMarginToOtherCharges(final ItineraryPricingInfoData itineraryPricingInfo, final Double marginRate)
	{
		double totalOtherChargesPriceVal = 0d;
		if (CollectionUtils.isNotEmpty(itineraryPricingInfo.getOtherCharges()))
		{
			for (final OtherChargeData otherChargeData : itineraryPricingInfo.getOtherCharges())
			{
				final PriceData otherChargePrice = getPriceAfterMargin(otherChargeData.getPrice(), marginRate);
				otherChargeData.setPrice(otherChargePrice);

				totalOtherChargesPriceVal = totalOtherChargesPriceVal + otherChargePrice.getValue().doubleValue();
			}
		}
		return totalOtherChargesPriceVal;
	}

	/**
	 * adding margin to total fare price
	 */
	protected void addMarginToTotalFarePrice(final ItineraryPricingInfoData itineraryPricingInfo, final double totalFarePriceVal)
	{
		if (Objects.nonNull(itineraryPricingInfo.getTotalFare()))
		{
			final PriceData priceData = itineraryPricingInfo.getTotalFare().getTotalPrice();
			final PriceData totalPrice = getTravelCommercePriceFacade()
					.createPriceData(totalFarePriceVal, priceData.getCurrencyIso());
			itineraryPricingInfo.getTotalFare().setTotalPrice(totalPrice);
		}
	}

	protected PriceData getPriceAfterMargin(final PriceData priceData,int quantity, final double marginRate)
	{
		double farePriceVal = priceData.getValue().doubleValue()/quantity;
		farePriceVal =PrecisionUtil.round(farePriceVal);
		final double marginVal = farePriceVal * marginRate / 100;
		final double effectiveFarePriceVal = PrecisionUtil.round(farePriceVal +marginVal)*quantity;
		return getTravelCommercePriceFacade()
				.createPriceData(effectiveFarePriceVal, priceData.getCurrencyIso());
	}

	/**
	 * get Price Data after margin
	 *
	 * @param priceData
	 * @param marginRate
	 * @return
	 */
	protected PriceData getPriceAfterMargin(final PriceData priceData, final double marginRate)
	{
		 double farePriceVal = priceData.getValue().doubleValue();
		 farePriceVal =PrecisionUtil.round(farePriceVal);
		final double marginVal = farePriceVal * marginRate / 100;
		final double effectiveFarePriceVal = PrecisionUtil.round(farePriceVal +marginVal);
		return getTravelCommercePriceFacade()
				.createPriceData(effectiveFarePriceVal, priceData.getCurrencyIso());
	}

	/**
	 * @return the travelCommercePriceFacade
	 */
	protected TravelCommercePriceFacade getTravelCommercePriceFacade()
	{
		return travelCommercePriceFacade;
	}

	/**
	 * @param travelCommercePriceFacade the travelCommercePriceFacade to set
	 */
	@Required
	public void setTravelCommercePriceFacade(final TravelCommercePriceFacade travelCommercePriceFacade)
	{
		this.travelCommercePriceFacade = travelCommercePriceFacade;
	}
}
