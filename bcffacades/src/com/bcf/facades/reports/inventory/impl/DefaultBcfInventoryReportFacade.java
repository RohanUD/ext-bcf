/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.reports.inventory.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfAccommodationOfferingService;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.enums.SaleStatusType;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.ActivitySaleStatusModel;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.service.ActivityProductService;
import com.bcf.core.service.BcfTravelStockService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reports.inventory.BcfInventoryReportFacade;
import com.bcf.request.data.reports.inventory.AccommodationInventoryRequestData;
import com.bcf.request.data.reports.inventory.AccommodationInventoryResponseData;
import com.bcf.request.data.reports.inventory.AccommodationOfferingInventoryResponseData;
import com.bcf.request.data.reports.inventory.ActivityInventoryRequestData;
import com.bcf.request.data.reports.inventory.ActivityInventoryResponseData;
import com.bcf.request.data.reports.inventory.StockLevelInventoryResponseData;
import com.bcf.request.data.reports.inventory.StopSaleDateRangeData;


public class DefaultBcfInventoryReportFacade implements BcfInventoryReportFacade
{
	private static final String PRODUCTCATALOG = "bcfProductCatalog";
	private static final String CATALOGVERSION = CatalogManager.ONLINE_VERSION;

	private BcfTravelStockService bcfTravelStockService;
	private BcfTravelLocationService bcfTravelLocationService;
	private BcfAccommodationOfferingService bcfAccommodationOfferingService;
	private BcfAccommodationService bcfAccommodationService;
	private CatalogVersionService catalogVersionService;
	private ActivityProductService activityProductService;
	private static final Logger LOG = Logger.getLogger(DefaultBcfInventoryReportFacade.class);

	@Override
	public List<AccommodationOfferingInventoryResponseData> getAccommodationInventoryResults(
			final AccommodationInventoryRequestData accommodationInventoryRequestData)
	{
		if (StringUtils.isBlank(accommodationInventoryRequestData.getGeoAreaCode()) )
		{
			return Collections.emptyList();
		}
		final List<StockLevelModel> accommodationStockLevels = getBcfTravelStockService()
				.getAccommodationStockLevels(accommodationInventoryRequestData.getStartDate(),
						accommodationInventoryRequestData.getEndDate());
		if (CollectionUtils.isEmpty(accommodationStockLevels))
		{
			return Collections.emptyList();
		}

		final Map<WarehouseModel, Map<String, List<StockLevelModel>>> stockLevelsGroupByWareHouse = accommodationStockLevels
				.stream().collect(
						Collectors.groupingBy(StockLevelModel::getWarehouse, Collectors.groupingBy(StockLevelModel::getProductCode)));
		return getAccommodationOfferingInventoryResponseDatas(stockLevelsGroupByWareHouse, accommodationInventoryRequestData);
	}

	protected List<AccommodationOfferingInventoryResponseData> getAccommodationOfferingInventoryResponseDatas(
			final Map<WarehouseModel, Map<String, List<StockLevelModel>>> stockLevelsGroupByWareHouse,
			final AccommodationInventoryRequestData accommodationInventoryRequestData)
	{
		final List<AccommodationOfferingInventoryResponseData> accommodationOfferingInventoryResponseDatas = new ArrayList<>();
		final Set<WarehouseModel> warehouseModels = stockLevelsGroupByWareHouse.keySet();
		final Set<AccommodationOfferingModel> accommodationOfferingModels = warehouseModels.stream()
				.filter(AccommodationOfferingModel.class::isInstance).map(AccommodationOfferingModel.class::cast)
				.collect(Collectors.toSet());
		if (CollectionUtils.isEmpty(accommodationOfferingModels))
		{
			return Collections.emptyList();
		}

		if(StringUtils
			.isNotBlank(accommodationInventoryRequestData.getAccommodationOfferingCode())){
			//check if hotel is available in list of warehouse models, if not, this means that all the rooms are on request
			if (!isRequestedAccommodationOfferingPresent(accommodationInventoryRequestData, accommodationOfferingModels))
			{
				return populateResponseForOnRequestAccommodationOffering(accommodationInventoryRequestData,
						accommodationOfferingInventoryResponseDatas,accommodationInventoryRequestData.getStartDate(),accommodationInventoryRequestData.getEndDate());
			}

		}


		for (final AccommodationOfferingModel accommodationOfferingModel : accommodationOfferingModels)
		{
			final LocationModel cityLocation = getBcfTravelLocationService().getLocationWithLocationType(
					Collections.singletonList(accommodationOfferingModel.getLocation()), LocationType.CITY);
			final LocationModel geoAreaLocation = getBcfTravelLocationService().getLocationWithLocationType(
					Collections.singletonList(accommodationOfferingModel.getLocation()), LocationType.GEOGRAPHICAL_AREA);

			if(geoAreaLocation==null){
				continue;
			}
			final BcfVacationLocationModel bcfVacationLocationModel = getBcfTravelLocationService()
					.getBcfVacationLocationByCode(geoAreaLocation.getCode());
			if (Objects.isNull(bcfVacationLocationModel))
			{
				continue;
			}

			// filter results based on Geo Area Location
			if (StringUtils.isNotBlank(accommodationInventoryRequestData.getGeoAreaCode())
					&& !StringUtils.equals(accommodationInventoryRequestData.getGeoAreaCode(), bcfVacationLocationModel.getCode()))
			{
				continue;
			}
			// filter results based on city Location
			if (StringUtils.isNotBlank(accommodationInventoryRequestData.getCityCode())
					&& !StringUtils.equals(accommodationInventoryRequestData.getCityCode(), cityLocation.getCode()))
			{
				continue;
			}
			// filter results based on accommodation offering
			if (StringUtils.isNotBlank(accommodationInventoryRequestData.getAccommodationOfferingCode())
					&& !StringUtils
					.equals(accommodationInventoryRequestData.getAccommodationOfferingCode(), accommodationOfferingModel.getCode()))
			{
				continue;
			}
			Map<String, List<StockLevelModel>> stockLevelsGroupByAccommodation = stockLevelsGroupByWareHouse
					.get(accommodationOfferingModel);

			boolean filterByAccommodation = false;
			// filter results based on accommodation
			if (StringUtils.isNotBlank(accommodationInventoryRequestData.getAccommodationCode()))
			{
				filterByAccommodation = true;
				stockLevelsGroupByAccommodation = stockLevelsGroupByAccommodation.entrySet().stream()
						.filter(stockLevelsGroupByAccommodationEntry -> StringUtils.equals(
								accommodationInventoryRequestData.getAccommodationCode(), stockLevelsGroupByAccommodationEntry.getKey()))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			}

			final AccommodationOfferingInventoryResponseData accommodationOfferingInventoryResponseData = new AccommodationOfferingInventoryResponseData();
			accommodationOfferingInventoryResponseData.setAccommodationOfferingCode(accommodationOfferingModel.getCode());
			accommodationOfferingInventoryResponseData.setAccommodationOfferingName(accommodationOfferingModel.getName());
			accommodationOfferingInventoryResponseData.setCityCode(cityLocation.getCode());
			accommodationOfferingInventoryResponseData.setGeoAreaCode(bcfVacationLocationModel.getCode());
			final List<AccommodationInventoryResponseData> accommodationInventoryResponseDatas = getAccommodationInventoryResponseDatas(
					stockLevelsGroupByAccommodation, accommodationInventoryRequestData.getDaysOfWeek(),
					getRequiredAccommodations(accommodationInventoryRequestData,
							filterByAccommodation, accommodationOfferingModel.getAccommodations()),accommodationOfferingModel,accommodationInventoryRequestData.getStartDate(),accommodationInventoryRequestData.getEndDate()
			);
			accommodationOfferingInventoryResponseData.setAccommodations(accommodationInventoryResponseDatas);
			accommodationOfferingInventoryResponseDatas.add(accommodationOfferingInventoryResponseData);
		}

		return accommodationOfferingInventoryResponseDatas;
	}

	private boolean isRequestedAccommodationOfferingPresent(
			final AccommodationInventoryRequestData accommodationInventoryRequestData,
			final Set<AccommodationOfferingModel> accommodationOfferingModels)
	{
		final List<String> accommodationNameList = accommodationOfferingModels.stream()
				.map(AccommodationOfferingModel::getCode).collect(Collectors.toList());

		return accommodationNameList.contains(accommodationInventoryRequestData.getAccommodationOfferingCode());
	}

	/*
	 * A hotel is said to be ON_REQUEST, if all its rooms are ON_REQUEST
	 */
	private List<AccommodationOfferingInventoryResponseData> populateResponseForOnRequestAccommodationOffering(
			final AccommodationInventoryRequestData accommodationInventoryRequestData,
			final List<AccommodationOfferingInventoryResponseData> accommodationOfferingInventoryResponseDatas, final Date startDate,
			final Date endDate)
	{
		AccommodationOfferingModel accommodationOfferingModel = null;

		try
		{
			accommodationOfferingModel = getBcfAccommodationOfferingService()
					.getAccommodationOffering(accommodationInventoryRequestData.getAccommodationOfferingCode());
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error(" unable to get accommodation offering for code:" + accommodationInventoryRequestData
					.getAccommodationOfferingCode(), ex);
			return accommodationOfferingInventoryResponseDatas;
		}

		final AccommodationOfferingInventoryResponseData accommodationOfferingInventoryResponseData = new AccommodationOfferingInventoryResponseData();
		accommodationOfferingInventoryResponseData.setAccommodationOfferingCode(accommodationOfferingModel.getCode());
		accommodationOfferingInventoryResponseData.setAccommodationOfferingName(accommodationOfferingModel.getName());

		List<String> accommodationNames = accommodationOfferingModel.getAccommodations();
		//filter by accommodations
		if (StringUtils.isNotBlank(accommodationInventoryRequestData.getAccommodationCode()))
		{
			accommodationNames = accommodationNames.stream()
					.filter(a -> a.equals(accommodationInventoryRequestData.getAccommodationCode())).collect(
							Collectors.toList());
		}

		final Set<AccommodationModel> accommodationModels = StreamUtil.safeStream(accommodationNames)
				.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
				.collect(Collectors.toSet());

		final List<AccommodationInventoryResponseData> accommodationInventoryResponseDatas = new ArrayList<>();
		for (final AccommodationModel accommodationModel : accommodationModels)
		{
			final AccommodationInventoryResponseData accommodationInventoryResponseData = new AccommodationInventoryResponseData();
			accommodationInventoryResponseData.setAccommodationCode(accommodationModel.getCode());
			accommodationInventoryResponseData.setAccommodationName(accommodationModel.getName());
			accommodationInventoryResponseData.setStockLevelType(StockLevelType.ONREQUEST.getCode());
			accommodationInventoryResponseData.setStopSellDateRanges(getStopSaleDateRange(accommodationOfferingModel,accommodationModel,startDate,endDate));
			accommodationInventoryResponseDatas.add(accommodationInventoryResponseData);
		}
		accommodationOfferingInventoryResponseData.setAccommodations(accommodationInventoryResponseDatas);
		accommodationOfferingInventoryResponseDatas.add(accommodationOfferingInventoryResponseData);
		return accommodationOfferingInventoryResponseDatas;
	}

	protected List<AccommodationInventoryResponseData> getAccommodationInventoryResponseDatas(
			final Map<String, List<StockLevelModel>> stockLevelsGroupByAccommodation, final List<String> daysOfWeek,
			final List<AccommodationModel> accommodationModels, final AccommodationOfferingModel accommodationOfferingModel,
			final Date startDate, final Date endDate)
	{
		final List<AccommodationInventoryResponseData> accommodationInventoryResponseDatas = new ArrayList<>();

		for (final AccommodationModel accommodationModel : accommodationModels)
		{
			final AccommodationInventoryResponseData accommodationInventoryResponseData = new AccommodationInventoryResponseData();
			accommodationInventoryResponseData.setAccommodationCode(accommodationModel.getCode());
			accommodationInventoryResponseData.setAccommodationName(accommodationModel.getName());
			accommodationInventoryResponseData.setStockLevelType(accommodationModel.getStockLevelType().getCode());

			List<StockLevelInventoryResponseData> stockLevelInventoryResponseDatas = null;
			if (!StockLevelType.ONREQUEST.equals(accommodationModel.getStockLevelType()))
			{
				stockLevelInventoryResponseDatas = getStockLevelInventoryResponseDatas(
						stockLevelsGroupByAccommodation.get(accommodationModel.getCode()), accommodationOfferingModel.getSaleStatuses(),accommodationModel,daysOfWeek);
			}else{

				accommodationInventoryResponseData.setStopSellDateRanges(getStopSaleDateRange(accommodationOfferingModel,accommodationModel,startDate,endDate));

			}
			accommodationInventoryResponseData.setStockLevelInventories(stockLevelInventoryResponseDatas);
			accommodationInventoryResponseDatas.add(accommodationInventoryResponseData);
		}
		return accommodationInventoryResponseDatas;
	}

	private List<StopSaleDateRangeData> getStopSaleDateRange(final AccommodationOfferingModel accommodationOfferingModel, final AccommodationModel accommodationModel,
			final Date startDate, final Date endDate){

		final List<StopSaleDateRangeData> stopSaleDateRangeDatas = getStopSaleDateRangeData(accommodationOfferingModel.getSaleStatuses(), startDate,
				endDate,null);

		stopSaleDateRangeDatas.addAll(getStopSaleDateRangeData(accommodationModel.getSaleStatuses(), startDate,
				endDate,stopSaleDateRangeDatas));

		return stopSaleDateRangeDatas;
	}


	private List<StopSaleDateRangeData> getStopSaleDateRangeData(Set<SaleStatusModel> saleStatusModels,
			final Date startDate, final Date endDate, final List<StopSaleDateRangeData> hotelStopSaleDateRangeDatas )
	{
		final List<StopSaleDateRangeData> dateRangeDatas=new ArrayList<>();
		if(CollectionUtils.isNotEmpty(saleStatusModels)){
			saleStatusModels=saleStatusModels.stream()
					.filter(saleStatusModel -> SaleStatusType.STOPSALE.equals(saleStatusModel.getSaleStatusType())).collect(
							Collectors.toSet());
			if(CollectionUtils.isNotEmpty(saleStatusModels)){

				for(final SaleStatusModel saleStatusModel:saleStatusModels){

					if(TravelDateUtils
							.isBefore(endDate, ZoneId.systemDefault(),BCFDateUtils.setToStartOfDay(saleStatusModel.getStartDate()), ZoneId.systemDefault())
							||(TravelDateUtils
							.isBefore(BCFDateUtils.setToEndOfDay(saleStatusModel.getEndDate()), ZoneId.systemDefault(),startDate, ZoneId.systemDefault())))
					{

						continue;
					}

						if(!isAccommodationStopSaleInBetweenHotelStopSale(hotelStopSaleDateRangeDatas, saleStatusModel))
						{
							final StopSaleDateRangeData dateRangeData = new StopSaleDateRangeData();
							dateRangeData.setStartDate(TravelDateUtils.convertDateToStringDate(saleStatusModel.getStartDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY) );
							dateRangeData.setEndDate( TravelDateUtils.convertDateToStringDate(saleStatusModel.getEndDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY) );
							dateRangeDatas.add(dateRangeData);
						}

				}

			}
		}
		return dateRangeDatas;
	}

	private boolean isAccommodationStopSaleInBetweenHotelStopSale(final List<StopSaleDateRangeData> hotelStopSaleDateRangeDatas,
			final SaleStatusModel saleStatusModel)
	{
		if(CollectionUtils.isNotEmpty(hotelStopSaleDateRangeDatas))
		{
			for (final StopSaleDateRangeData hotelStopSaleDateRangeData : hotelStopSaleDateRangeDatas)
			{
				if (BCFDateUtils
						.isBetweenDatesInclusive(saleStatusModel.getStartDate(),
								BCFDateUtils.setToStartOfDay(TravelDateUtils.convertStringDateToDate(hotelStopSaleDateRangeData.getStartDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY)),
								BCFDateUtils.setToEndOfDay(TravelDateUtils.convertStringDateToDate(hotelStopSaleDateRangeData.getEndDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY)))
						&& (BCFDateUtils
						.isBetweenDatesInclusive(saleStatusModel.getEndDate(),
								BCFDateUtils.setToStartOfDay(TravelDateUtils.convertStringDateToDate(hotelStopSaleDateRangeData.getStartDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY)),
								BCFDateUtils.setToEndOfDay(TravelDateUtils.convertStringDateToDate(hotelStopSaleDateRangeData.getEndDate(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY)))))
				{
					return true;
				}

			}
		}
			return false;
	}

	private List<AccommodationModel> getRequiredAccommodations(
			final AccommodationInventoryRequestData accommodationInventoryRequestData,
			final boolean filterByAccommodations,
			final List<String> allAccommodationCodesOfAHotel)
	{
		final List<String> accommodationCodes = !filterByAccommodations ? allAccommodationCodesOfAHotel :
				StreamUtil.safeStream(allAccommodationCodesOfAHotel)
						.filter(h -> h.equals(accommodationInventoryRequestData.getAccommodationCode())).collect(
						Collectors.toList());
		return accommodationCodes.stream()
				.map(accommodationCode -> getBcfAccommodationService().getAccommodation(accommodationCode))
				.collect(Collectors.toList());
	}

	protected List<StockLevelInventoryResponseData> getStockLevelInventoryResponseDatas(
			final List<StockLevelModel> stockLevelModels, final Set<SaleStatusModel> saleStatusModels, final ProductModel product
			 ,final List<String> daysOfWeek)
	{
		final List<StockLevelInventoryResponseData> stockLevelInventoryResponseDatas = new ArrayList<>();
		Map<Date, List<StockLevelModel>> stockLevelGroupByDate = StreamUtil.safeStream(stockLevelModels)
				.collect(Collectors.groupingBy(StockLevelModel::getDate));
		//sort map by date ascending
		stockLevelGroupByDate = stockLevelGroupByDate.entrySet().stream()
				.sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		final List<SaleStatusModel> hotelStopSaleStatusModels =new ArrayList<>();
		final List<SaleStatusModel> accommodationStopSaleStatusModels =new ArrayList<>();
		final List<ActivitySaleStatusModel> activityStopSaleStatusModels =new ArrayList<>();

		boolean isAccommodation=false;
		if(product instanceof AccommodationModel){
			isAccommodation=true;
			final AccommodationModel accommodationModel= (AccommodationModel)product;

			if(CollectionUtils.isNotEmpty(accommodationModel.getSaleStatuses()))
			{
				accommodationStopSaleStatusModels.addAll(accommodationModel.getSaleStatuses().stream()
						.filter(saleStatusModel -> SaleStatusType.STOPSALE.equals(saleStatusModel.getSaleStatusType())).collect(
								Collectors.toList()));
			}
		}else{

			final ActivityProductModel activityProductModel= (ActivityProductModel)product;

			if(CollectionUtils.isNotEmpty(activityProductModel.getSaleStatuses()))
			{
				activityStopSaleStatusModels.addAll(activityProductModel.getSaleStatuses().stream()
						.filter(saleStatusModel -> SaleStatusType.STOPSALE.equals(saleStatusModel.getSaleStatusType())).collect(
								Collectors.toList()));
			}
		}

		if(CollectionUtils.isNotEmpty(saleStatusModels))
		{

			hotelStopSaleStatusModels.addAll(saleStatusModels.stream()
					.filter(saleStatusModel -> SaleStatusType.STOPSALE.equals(saleStatusModel.getSaleStatusType())).collect(
							Collectors.toList()));

		}

		if(product instanceof AccommodationModel){


		}
		for (final Map.Entry<Date, List<StockLevelModel>> stockLevelGroupByDateEntry : stockLevelGroupByDate.entrySet())
		{
			final Date date = stockLevelGroupByDateEntry.getKey();
			// filter results based on days of week
			if (CollectionUtils.isNotEmpty(daysOfWeek))
			{
				final String dayOfWeek = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
						.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).toUpperCase();
				if (!daysOfWeek.contains(dayOfWeek))
				{
					continue;
				}
			}

			final List<StockLevelModel> stockLevelsByDate = stockLevelGroupByDateEntry.getValue();
			Map<String, List<StockLevelModel>> stockLevelGroupByTime = stockLevelsByDate.stream()
					.collect(Collectors
							.groupingBy(stockLevel -> Objects.isNull(stockLevel.getTime()) ? StringUtils.EMPTY : stockLevel.getTime()));

			//sort by time, if time not present show on top
			stockLevelGroupByTime = stockLevelGroupByTime.entrySet().stream()
					.sorted((e1, e2) -> compareTime(e2.getKey(), e1.getKey()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));

			for (final Map.Entry<String, List<StockLevelModel>> stockLevelGroupByTimeEntry : stockLevelGroupByTime.entrySet())
			{
				final String time = stockLevelGroupByTimeEntry.getKey();
				final List<StockLevelModel> stockLevelsByTime = stockLevelGroupByTimeEntry.getValue();

				final int initialAvailable = stockLevelsByTime.stream().mapToInt(StockLevelModel::getAvailable).sum();
				final int reserved = stockLevelsByTime.stream().mapToInt(StockLevelModel::getReserved).sum();
				int available = initialAvailable - reserved;
				if (available <= 0)
				{
					available = 0;
				}

				final StockLevelInventoryResponseData stockLevelInventoryResponseData = new StockLevelInventoryResponseData();
				stockLevelInventoryResponseData
						.setDate(TravelDateUtils.convertDateToStringDate(date, BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY));
				stockLevelInventoryResponseData.setTime(time);
				stockLevelInventoryResponseData.setInitialAvailable(initialAvailable);
				stockLevelInventoryResponseData.setAvailable(available);
				stockLevelInventoryResponseData.setReserved(reserved);

				final StockLevelModel anyStockLevelForTime = stockLevelsByTime.stream().findFirst().get();
				stockLevelInventoryResponseData.setStockType(anyStockLevelForTime.getStockLevelType());
				if(isAccommodation){
					stockLevelInventoryResponseData.setStopSaleStatus(isAccommodationStopSale(hotelStopSaleStatusModels,accommodationStopSaleStatusModels,date));
				}else{
					stockLevelInventoryResponseData.setStopSaleStatus(isActivityStopSale(activityStopSaleStatusModels,date));
				}


				if (Objects.nonNull(anyStockLevelForTime.getReleaseBlockDay()))
				{
					final long releaseDays = TravelDateUtils.getDaysBetweenDates(date, anyStockLevelForTime.getReleaseBlockDay());
					stockLevelInventoryResponseData.setReleaseDays(releaseDays);
				}
				stockLevelInventoryResponseDatas.add(stockLevelInventoryResponseData);
			}
		}
		return stockLevelInventoryResponseDatas;
	}

	private boolean isAccommodationStopSale(final List<SaleStatusModel> hotelStopSaleStatusModels, final List<SaleStatusModel> accommodationStopSaleStatusModels,final Date date)
	{

		final boolean stopSale=isAccommodationStopSale(hotelStopSaleStatusModels,date);

		if(stopSale){
			return true;
		}
		return isAccommodationStopSale(accommodationStopSaleStatusModels,date);

	}

	private boolean isAccommodationStopSale(final List<SaleStatusModel> stopSaleStatusModels,final Date date){

		if(CollectionUtils.isNotEmpty(stopSaleStatusModels)){

			for(final SaleStatusModel saleStatusModel:stopSaleStatusModels){

				if(BCFDateUtils
						.isBetweenDatesInclusive(date, BCFDateUtils.setToStartOfDay(saleStatusModel.getStartDate()), BCFDateUtils.setToEndOfDay(saleStatusModel.getEndDate()))){
					return true;

				}
			}

		}
		return false;
	}

	private boolean isActivityStopSale(final List<ActivitySaleStatusModel> stopSaleStatusModels,final Date date){

		if(CollectionUtils.isNotEmpty(stopSaleStatusModels)){

			for(final ActivitySaleStatusModel saleStatusModel:stopSaleStatusModels){
				if(BCFDateUtils
						.isDateInDateRange(date, BCFDateUtils.setToStartOfDay(saleStatusModel.getStartDate()), BCFDateUtils.setToEndOfDay(saleStatusModel.getEndDate()))){
					return true;

				}
			}

		}
		return false;
	}

	private int compareTime(final String key2, final String key1)
	{
		if (StringUtils.isEmpty(key2) && StringUtils.isEmpty(key1))
		{
			return 0;
		}

		if (StringUtils.isEmpty(key2))
		{
			return 1;
		}

		if (StringUtils.isEmpty(key1))
		{
			return -1;
		}

		return getDate(key1).compareTo(getDate(key2));
	}

	private Date getDate(final String timePart)
	{
		return BCFDateUtils.convertStringToDateTime(timePart, "HH:mm");
	}

	@Override
	public List<ActivityInventoryResponseData> getActivityInventoryResults(
			final ActivityInventoryRequestData activityInventoryRequestData)
	{
		if (StringUtils.isBlank(activityInventoryRequestData.getGeoAreaCode()) || Objects
				.isNull(activityInventoryRequestData.getCityCode()) || StringUtils
				.isBlank(activityInventoryRequestData.getActivityCode()))
		{
			return Collections.emptyList();
		}
		final List<StockLevelModel> activityStockLevels = getBcfTravelStockService()
				.getActivityStockLevels(activityInventoryRequestData.getStartDate(),
						activityInventoryRequestData.getEndDate());
		if (CollectionUtils.isEmpty(activityStockLevels))
		{
			return Collections.emptyList();
		}

		final Map<String, List<StockLevelModel>> stockLevelsGroupByActivityProduct = activityStockLevels
				.stream().collect(Collectors.groupingBy(StockLevelModel::getProductCode));
		return getActivityInventoryResponseDatas(stockLevelsGroupByActivityProduct, activityInventoryRequestData);
	}

	private List<ActivityInventoryResponseData> getActivityInventoryResponseDatas(
			final Map<String, List<StockLevelModel>> stockLevelsGroupByActivityProduct,
			final ActivityInventoryRequestData activityInventoryRequestData)
	{
		final List<ActivityInventoryResponseData> activityInventoryResponseDatas = new ArrayList<>();
		final Set<String> activityCodes = stockLevelsGroupByActivityProduct.keySet();

		//check if activity is available in list of activity codes, if not, this means that all the activity is on_request
		if (!isRequestedActivityPresent(activityInventoryRequestData, activityCodes))
		{
			return populateActivityResponseDataForOnRequestActivity(activityInventoryRequestData,
					activityInventoryResponseDatas);
		}

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);

		final Set<ActivityProductModel> activityProductModels = activityCodes.stream()
				.map(activityCode -> getActivityProductService().getActivityProduct(activityCode, catalogVersion)).collect(
						Collectors.toSet());
		if (CollectionUtils.isEmpty(activityProductModels))
		{
			return Collections.emptyList();
		}

		for (final ActivityProductModel activityProductModel : activityProductModels)
		{
			final LocationModel cityLocation = getBcfTravelLocationService().getLocationWithLocationType(
					Collections.singletonList(activityProductModel.getDestination()), LocationType.CITY);
			final LocationModel geoAreaLocation = getBcfTravelLocationService().getLocationWithLocationType(
					Collections.singletonList(activityProductModel.getDestination()), LocationType.GEOGRAPHICAL_AREA);

			if (geoAreaLocation == null)
			{
				continue;
			}

			// filter results based on Geo Area Location
			if (StringUtils.isNotBlank(activityInventoryRequestData.getGeoAreaCode())
					&& !StringUtils.equals(activityInventoryRequestData.getGeoAreaCode(), geoAreaLocation.getCode()))
			{
				continue;
			}
			// filter results based on city Location
			if (StringUtils.isNotBlank(activityInventoryRequestData.getCityCode())
					&& !StringUtils.equals(activityInventoryRequestData.getCityCode(), cityLocation.getCode()))
			{
				continue;
			}
			// filter results based on activity
			if (StringUtils.isNotBlank(activityInventoryRequestData.getActivityCode()) && !StringUtils
					.equals(activityInventoryRequestData.getActivityCode(), activityProductModel.getCode()))
			{
				continue;
			}

			final ActivityInventoryResponseData activityInventoryResponseData = new ActivityInventoryResponseData();
			activityInventoryResponseData.setActivityCode(activityProductModel.getCode());
			activityInventoryResponseData.setActivityName(activityProductModel.getName());
			activityInventoryResponseData.setStockLevelType(activityProductModel.getStockLevelType().getCode());

			final List<StockLevelInventoryResponseData> stockLevelInventoryResponseDatas = getStockLevelInventoryResponseDatas(
					stockLevelsGroupByActivityProduct.get(activityProductModel.getCode()),null,activityProductModel,
					activityInventoryRequestData.getDaysOfWeek());
			activityInventoryResponseData.setStockLevelInventories(stockLevelInventoryResponseDatas);
			activityInventoryResponseDatas.add(activityInventoryResponseData);
		}

		return activityInventoryResponseDatas;
	}

	private List<ActivityInventoryResponseData> populateActivityResponseDataForOnRequestActivity(
			final ActivityInventoryRequestData activityInventoryRequestData,
			final List<ActivityInventoryResponseData> activityInventoryResponseDatas)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOGVERSION);
		final ActivityProductModel activityProductModel = getActivityProductService()
				.getActivityProduct(activityInventoryRequestData.getActivityCode(), catalogVersion);

		final ActivityInventoryResponseData activityInventoryResponseData = new ActivityInventoryResponseData();
		activityInventoryResponseData.setActivityCode(activityProductModel.getCode());
		activityInventoryResponseData.setActivityName(activityProductModel.getName());
		activityInventoryResponseData.setStockLevelType(activityProductModel.getStockLevelType().getCode());
		activityInventoryResponseData.setStockLevelInventories(null);
		activityInventoryResponseDatas.add(activityInventoryResponseData);
		return activityInventoryResponseDatas;
	}

	private boolean isRequestedActivityPresent(final ActivityInventoryRequestData activityInventoryRequestData,
			final Set<String> activityProductCodes)
	{
		return StreamUtil.safeStream(activityProductCodes)
				.anyMatch(activityCode -> activityCode.equals(activityInventoryRequestData.getActivityCode()));
	}

	protected BcfTravelStockService getBcfTravelStockService()
	{
		return bcfTravelStockService;
	}

	@Required
	public void setBcfTravelStockService(final BcfTravelStockService bcfTravelStockService)
	{
		this.bcfTravelStockService = bcfTravelStockService;
	}

	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected ActivityProductService getActivityProductService()
	{
		return activityProductService;
	}

	@Required
	public void setActivityProductService(final ActivityProductService activityProductService)
	{
		this.activityProductService = activityProductService;
	}

	public BcfAccommodationOfferingService getBcfAccommodationOfferingService()
	{
		return bcfAccommodationOfferingService;
	}

	@Required
	public void setBcfAccommodationOfferingService(
			final BcfAccommodationOfferingService bcfAccommodationOfferingService)
	{
		this.bcfAccommodationOfferingService = bcfAccommodationOfferingService;
	}

}
