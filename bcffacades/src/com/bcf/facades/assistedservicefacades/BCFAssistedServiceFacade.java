/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.assistedservicefacades;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import java.io.UnsupportedEncodingException;
import java.util.List;
import com.bcf.bcfassistedservicesstorefront.ASMSalesChannelsData;
import com.bcf.facades.customer.search.CustomerSearchData;
import com.bcf.facades.customer.search.SearchParams;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BCFAssistedServiceFacade extends AssistedServiceFacade
{
	void authenticateAndCheckPrivileges(String username, String password, final String salesChannel)
			throws AssistedServiceException;

	List<ASMSalesChannelsData> getAsmSalesChannels();

	CustomerData getCustomer(String username, boolean guestInd, String mobileNumber)
			throws IntegrationException, AssistedServiceException, UnsupportedEncodingException;

	CustomerSearchData searchCustomer(SearchParams searchParams)
			throws AssistedServiceException, IntegrationException;

	CartModel getCartByCode(String cartId, UserModel customer);

	void emulateByeBookingReference(String eBookingRefNumber) throws AssistedServiceException;
}
