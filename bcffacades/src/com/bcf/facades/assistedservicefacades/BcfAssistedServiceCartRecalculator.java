/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.facades.assistedservicefacades;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.ticket.enums.EventType;
import de.hybris.platform.ticketsystem.events.SessionEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Assisted service event listener for {@link SessionEvent} event. Recalculates cart because there can be specific
 * promotions that are only applicable if the ASM emulation is active.
 */
public class BcfAssistedServiceCartRecalculator extends AbstractEventListener<SessionEvent>
{
	private static final Set<EventType> RECALCULATE_EVENTS = new HashSet<EventType>(Arrays.asList(EventType.START_SESSION_EVENT,
			EventType.END_SESSION_EVENT, EventType.AGENT_LOGOUT));
	private static final Logger LOG = Logger.getLogger(BcfAssistedServiceCartRecalculator.class);
	private CommerceCartService commerceCartService;
	private CartService cartService;
	private SessionService sessionService;

	@Override
	protected void onEvent(final SessionEvent event)
	{
		if (RECALCULATE_EVENTS.contains(event.getEventType()) && cartService.hasSessionCart())
		{
			try
			{
				final CommerceCartParameter recalcParam = new CommerceCartParameter();
				recalcParam.setEnableHooks(true);
				recalcParam.setCart(cartService.getSessionCart());
				getCommerceCartService().recalculateCart(recalcParam);
				if (LOG.isDebugEnabled())
				{
					LOG.debug(String.format("Cart %s has been recalculated", cartService.getSessionCart().toString()));
				}
			}
			catch (final CalculationException e)
			{
				LOG.error(e);
			}
		}

		if (EventType.AGENT_LOGOUT.equals(event.getEventType()))
		{
			//removing cart when asgent is logging out
			sessionService.removeAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME);
		}
	}

	protected CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
