/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.assistedservicefacades.impl;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedservicefacades.constants.AssistedservicefacadesConstants;
import de.hybris.platform.assistedservicefacades.impl.DefaultAssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceAgentBadCredentialsException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceAgentBlockedException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceAgentNoCustomerAndCartIdException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceAgentNotLoggedInException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceWrongCartIdException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceWrongCustomerIdException;
import de.hybris.platform.assistedserviceservices.utils.CustomerEmulationParams;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.ticket.enums.EventType;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.util.localization.Localization;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.bcf.bcfassistedservicesstorefront.ASMSalesChannelsData;
import com.bcf.bcfassistedservicesstorefront.enums.ASMSalesChannels;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.customer.service.CustomerUpdateService;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.facades.assistedservicefacades.BCFAssistedServiceFacade;
import com.bcf.facades.customer.search.CustomerSearchData;
import com.bcf.facades.customer.search.SearchParams;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integration.activedirectory.service.ADUserService;
import com.bcf.integration.crm.service.CRMSearchCustomerService;
import com.bcf.integration.data.BCF_Customer;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.CRMGetSearchCustomerResponseDTO;
import com.bcf.integration.data.EmailAddress;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.ADServiceException;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBCFAssistedServiceFacade extends DefaultAssistedServiceFacade implements BCFAssistedServiceFacade
{
	public static final String DEFAULT_CART_SUMMARY_PAGE = "/cart/summary";
	public static final String ALACARTE_CART_SUMMARY_PAGE = "/alacarte-review";
	public static final String QUOTE_DETAILS_PAGE_PREFIX = "/my-account/my-quotes/";
	public static final String ANONYMOUS_QUOTE_DETAILS_PAGE_PREFIX = "/quote/";
	public static final String OPTION_BOOKING_DETAILS_PAGE_PREFIX = "/option-booking/details/";
	private ADUserService adUserService;
	private CRMSearchCustomerService crmSearchCustomerService;
	private Converter<BCF_Customer, CustomerData> crmCustomerConverter;
	private Converter<BCF_Customer, CustomerData> crmSearchCustomerConverter;
	private CustomerUpdateService customerUpdateService;
	private BcfOrderService bcfOrderService;
	private BCFTravelCartService bcfTravelCartService;
	private CommerceCartService commerceCartService;
	private BaseSiteService baseSiteService;
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;
	private UserDetailsService userDetailsService;
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource
	private UserService userService;

	@Resource
	private EnumerationService enumerationService;

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	private static final Logger LOG = Logger.getLogger(DefaultBCFAssistedServiceFacade.class);

	private static final String AS_SAVED_ON_AGENTLOGIN_CUSTOMER = AssistedServiceFacade.class.getName() + "_saved_customer";

	private static final String ROLE_PREFIX = "authoritiesFor";

	@Override
	public void authenticateAndCheckPrivileges(final String username, final String password, final String salesChannel)
			throws AssistedServiceException
	{
		//Call to Active Directory API for AS Agent User Authentication and updating the user roles
		UserModel user = null;
		try
		{
			user = getAdUserService().authenticateAndUpdateADUser(username, password);
			if (!checkUserRolesForSalesChannels(username, salesChannel))
			{
				throw new AssistedServiceException(Localization.getLocalizedString("asm.selected.salesChannel.not.allowed"));
			}

		}
		catch (final ADServiceException e)
		{
			if (BcfintegrationcoreConstants.NOT_AUTHORISED.equals(e.getMessage()))
			{
				throw new AssistedServiceException(BcfintegrationcoreConstants.NOT_AUTHORISED_MESSAGE_CODE);
			}
			else if (BcfintegrationcoreConstants.SERVICE_UNAVAILABLE.equals(e.getMessage()))
			{
				throw new AssistedServiceException(BcfintegrationcoreConstants.SERVICE_UNAVAILABLE_MESSAGE_CODE);
			}
		}
		catch (final IntegrationException e)
		{
			final ErrorDTO error = e.getErorrListDTO().getErrorDto().get(0);

			if (BcfintegrationcoreConstants.SERVICE_UNAVAILABLE.equals(error.getErrorCode())
					|| BcfintegrationcoreConstants.EXCEPTION.equals(error.getErrorCode())
					|| BcfintegrationcoreConstants.INVALID_CLIENT_INFO.equals(error.getErrorCode()))
			{
				throw new AssistedServiceException(BcfintegrationcoreConstants.SERVICE_UNAVAILABLE_MESSAGE_CODE);
			}
			else if (BcfintegrationcoreConstants.INVALID_CREDENTIALS.equals(error.getErrorCode()))
			{
				throw new AssistedServiceAgentBadCredentialsException(BcfintegrationcoreConstants.INVALID_CREDENTIALS);
			}
			// For any unknown error code.
			throw new AssistedServiceException(BcfintegrationcoreConstants.SERVICE_UNAVAILABLE_MESSAGE_CODE);
		}

		doLogin(user);
	}

	private boolean checkUserRolesForSalesChannels(final String userId, final String salesChannel)
	{
		final UserModel user = userService.getUserForUID(userId);
		final Set<PrincipalGroupModel> allGroups = user.getAllGroups();

		final List<String> allowedAuthorities = BCFPropertiesUtils
				.convertToList(
						bcfConfigurablePropertiesService.getBcfPropertyValue(ROLE_PREFIX + salesChannel, salesChannel));

		final List<String> userAuthorities = allGroups.stream().map(o -> o.getUid()).collect(Collectors.toList());

		return CollectionUtils.containsAny(allowedAuthorities, userAuthorities);
	}

	@Override
	public List<ASMSalesChannelsData> getAsmSalesChannels()
	{
		final List<ASMSalesChannels> asmSalesChannels = enumerationService.getEnumerationValues(ASMSalesChannels.class);
		final List<ASMSalesChannelsData> salesChannels = new ArrayList<>();
		asmSalesChannels.stream().forEach(asmSalesChannel -> {
			final ASMSalesChannelsData asmSalesChannelsData = new ASMSalesChannelsData();
			asmSalesChannelsData.setCode(asmSalesChannel.getCode());
			asmSalesChannelsData.setName(enumerationService.getEnumerationName(asmSalesChannel));
			salesChannels.add(asmSalesChannelsData);
		});
		return salesChannels;
	}

	@Override
	public CustomerData getCustomer(final String username, final boolean guestInd, final String mobileNumber)
			throws AssistedServiceException, IntegrationException, UnsupportedEncodingException
	{
		validateSession();
		final BCF_Customer customer = getCRMCustomer(username, guestInd, mobileNumber);

		if (Objects.isNull(customer))
		{
			return null;
		}
		createUpdateCustomerInHybris(customer);
		return getCrmCustomerConverter().convert(customer);
	}

	@Override
	public CustomerSearchData searchCustomer(final SearchParams searchParams)
			throws AssistedServiceException, IntegrationException
	{
		validateSession();
		final CustomerSearchData customerSearchData = new CustomerSearchData();
		final List<CustomerData> customerDataList = new ArrayList<>();
		final CRMGetSearchCustomerResponseDTO crmGetSearchCustomerResponseDTO = searchCRMCustomer(searchParams);


		if (Objects.isNull(crmGetSearchCustomerResponseDTO) || CollectionUtils
				.isEmpty(crmGetSearchCustomerResponseDTO.getBCF_Customer()))
		{
			customerSearchData.setCustomers(Collections.emptyList());
			return customerSearchData;
		}

		final List<BCF_Customer> customerList = crmGetSearchCustomerResponseDTO.getBCF_Customer();
		for (final BCF_Customer customer : customerList)
		{
			customerDataList.add(getCrmSearchCustomerConverter().convert(customer));
		}

		customerSearchData.setCustomers(customerDataList);
		customerSearchData.setCurrentIndex(crmGetSearchCustomerResponseDTO.getCurrentIndex());
		customerSearchData.setLastPageInd(crmGetSearchCustomerResponseDTO.isLastPageInd());
		return customerSearchData;
	}

	@Override
	public void emulateAfterLogin() throws AssistedServiceException
	{
		// try to emulate using previous emulation call parameters
		final CustomerEmulationParams savedEmulationParams = getAsmSession().getSavedEmulationData();
		CartModel cart = null;
		if (getBcfTravelCartService().hasSessionCart())
		{
			cart = getBcfTravelCartService().getSessionCart();
		}
		final UserModel savedUser = getSessionService().getCurrentSession().getAttribute(AS_SAVED_ON_AGENTLOGIN_CUSTOMER);

		// in case we saved params after Deep Link were called without logging in first
		if (savedEmulationParams != null)
		{
			if (Objects.nonNull(cart))
			{
				getAssistedServiceService().restoreCartToUser(cart, savedUser);
			}
			emulateCustomer(savedEmulationParams.getUserId(), savedEmulationParams.getCartId(), savedEmulationParams.getOrderId());
			getAsmSession().setSavedEmulationData(null);
			return;
		}

		// in case there were a user before we signed in
		if (savedUser != null)
		{
			getSessionService().getCurrentSession().removeAttribute(AS_SAVED_ON_AGENTLOGIN_CUSTOMER);
			checkCart(cart, savedUser);

			if (isAssistedServiceAgent(getUserService().getCurrentUser()))
			{
				getUserService().setCurrentUser(getUserService().getAnonymousUser());
			}
		}
		else
		{
			getUserService().setCurrentUser(getUserService().getAnonymousUser());
		}
	}

	private void checkCart(final CartModel cart, final UserModel savedUser) throws AssistedServiceException
	{
		if (Objects.nonNull(cart))
		{
			if (CollectionUtils.isNotEmpty(cart.getEntries()))
			{
				getAssistedServiceService().restoreCartToUser(cart, savedUser);
				if (!getUserService().isAnonymousUser(savedUser))
				{
					emulateCustomer(savedUser.getUid(), cart.getCode());
				}
				else
				{
					emulateCustomer(null, cart.getCode());
				}
			}
			else
			{
				if (!getUserService().isAnonymousUser(savedUser))
				{
					emulateCustomer(savedUser.getUid(), null);
				}
			}
		}
		else
		{
			if (!getUserService().isAnonymousUser(savedUser))
			{
				emulateCustomer(savedUser.getUid(), null);
			}
		}
	}

	@Override
	public void emulateCustomer(final String customerId, final String cartId) throws AssistedServiceException
	{
		try
		{
			// validate session at first
			validateSession();
		}
		catch (final AssistedServiceAgentNotLoggedInException e)
		{
			// in case AS agent isn't logged in yet - save parameters in session
			final String orderId = null;
			getAsmSession().setSavedEmulationData(new CustomerEmulationParams(customerId, cartId, orderId));
			throw e;
		}

		if (StringUtils.isBlank(customerId) && StringUtils.isBlank(cartId))
		{
			throw new AssistedServiceAgentNoCustomerAndCartIdException(
					Localization.getLocalizedString("asm.emulate.error.no_customer_or_cart_id_provided"));
		}

		try
		{
			UserModel customer = getAssistedServiceService().getCustomer(customerId);
			// emulate only customers and not agents
			if (!(customer instanceof CustomerModel) || isAssistedServiceAgent(customer))
			{
				throw new AssistedServiceWrongCustomerIdException(
						"This id belongs to non-customer person. Will not add customer and/or cart to the session.");
			}
			if (cartId != null)
			{
				customer = findCartAndAttachToSession(cartId, customer);
			}
			getUserService().setCurrentUser(customer);


			getAsmSession().setEmulatedCustomer(customer);

			createSessionEvent(getAsmSession().getAgent(), customer, EventType.START_SESSION_EVENT);

		}
		catch (final UnknownIdentifierException e)
		{
			throw new AssistedServiceWrongCustomerIdException(
					"Unknown customer id. Will not add customer and/or cart to the session.", e);
		}
	}

	@Override
	public CartModel getCartByCode(final String cartId, final UserModel customer)
	{
		return getAssistedServiceService().getCartByCode(cartId, customer);
	}

	@Override
	public void emulateByeBookingReference(final String eBookingRefNumber) throws AssistedServiceException
	{
		try
		{
			final String salesChannel = getSessionService().getCurrentSession().getAttribute(BcfCoreConstants.SALES_CHANNEL);
			if (Objects.isNull(salesChannel))
			{
				throw new AssistedServiceException("Can not find the sales channel");
			}

			final ReservationDataList reservationDataList = searchBookingFacade
					.getVacationBookingsFromEBooking(Arrays.asList(eBookingRefNumber), null);

			if (Objects.nonNull(reservationDataList) && CollectionUtils.isNotEmpty(reservationDataList.getReservationDatas())
					&& CollectionUtils.isNotEmpty(reservationDataList.getReservationDatas().get(0).getReservationItems()))
			{
				final List<ReservationItemData> reservationItems = reservationDataList.getReservationDatas().get(0)
						.getReservationItems();
				final String bcfBookingReference = reservationItems.get(0).getBcfBookingReference();
				final AbstractOrderModel order = getBcfOrderService().getOrderByEBookingCode(bcfBookingReference);
				if (isTravelCentre())
				{
					handleTravelCentre(order, bcfBookingReference, salesChannel);
				}
				else
				{
					handleNonTravelCentre(order, bcfBookingReference, salesChannel);
				}
			}
			else
			{
				LOG.error("No details has been found the code eBooking reference number provided " + eBookingRefNumber);
				throw new AssistedServiceException("text.no.booking.found");
			}
		}
		catch (final IntegrationException e)

		{
			LOG.error(e.getMessage(), e);
			throw new AssistedServiceException(e.getMessage());
		}

	}

	private void handleNonTravelCentre(final AbstractOrderModel order, final String bcfBookingReference, final String salesChannel)
			throws AssistedServiceException
	{
		if (Objects.isNull(order))
		{
			this.getAsmSession().setForwardUrl(BcfCoreConstants.GUEST_BOOKING_DETAILS_PAGE + bcfBookingReference);
		}
		else if (!order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) && !order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			this.emulateCustomer(order.getUser().getUid(), order.getCode());
			getSessionService().setAttribute(AssistedservicefacadesConstants.ASM_ORDER_ID_TO_REDIRECT, order.getCode());
		}
		else
		{
			LOG.error("Vocation booking " + bcfBookingReference + " is not allowed for " + salesChannel);
			throw new AssistedServiceWrongCartIdException("text.ebooking.search.not.allowed");
		}
	}

	private void handleTravelCentre(final AbstractOrderModel order, final String bcfBookingReference, final String salesChannel)
			throws AssistedServiceException
	{
		if (Objects.isNull(order))
		{
			LOG.error("Order not found for the eBookingReference " + bcfBookingReference
					+ " in Hybris, ferry search is not allowed for " + salesChannel);
			throw new AssistedServiceWrongCartIdException("Ebooking search is not allowed");
		}
		else if (order.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || order.getBookingJourneyType()
				.equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			this.emulateCustomer(order.getUser().getUid(), order.getCode());
			getSessionService().setAttribute(AssistedservicefacadesConstants.ASM_ORDER_ID_TO_REDIRECT, order.getCode());
		}
		else
		{
			LOG.error("Order found for the eBookingReference " + bcfBookingReference
					+ " in Hybris, ferry search is not allowed for " + salesChannel);
			throw new AssistedServiceWrongCartIdException("text.ebooking.search.not.allowed");
		}
	}

	private boolean isTravelCentre()
	{
		final String salesChannel = getSessionService().getCurrentSession().getAttribute(BcfCoreConstants.SALES_CHANNEL);
		return StringUtils.equalsIgnoreCase(salesChannel, ASMSalesChannels.TRAVELCENTRE.getCode());
	}

	protected UserModel findCartAndAttachToSession(final String cartId, final UserModel customer)
			throws AssistedServiceWrongCartIdException
	{
		CartModel cart = null;
		final String customerName = customer.getName();
		if (BcfCoreConstants.ANONYMOUS.equals(customerName))
		{
			cart = getBcfTravelCommerceCartService().getCartForCodeAndSite(cartId, baseSiteService.getCurrentBaseSite());
		}
		else
		{
			cart = getAssistedServiceService().getCartByCode(cartId, customer);

			if (cart == null)
			{
				cart = tryLinkCartToCustomer(cartId, customer);
			}
		}

		if (cart == null)
		{
			OrderModel order = getAssistedServiceService().getOrderByCode(cartId, customer);
			if (order == null)
			{
				order = getBcfOrderService().getOrderByBookingReference(cartId, customer);
				if (order == null)
				{
					throw new AssistedServiceWrongCartIdException("Cart ID/Order ID not found");
				}
			}

			if (Objects.nonNull(order))
			{
				getSessionService()
						.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, order.getBookingJourneyType().getCode());
				final CustomerModel customerModel = (CustomerModel) order.getUser();
				if (customerModel.getType().equals(CustomerType.GUEST))
				{
					getAsmSession().setForwardUrl("/manage-booking/guest-booking-details?guestBookingIdentifier=" + order.getGuid());
					getSessionService().setAttribute(AssistedservicefacadesConstants.ASM_ORDER_ID_TO_REDIRECT, order.getCode());
					return customerModel;
				}
				else
				{
					getAsmSession().setForwardUrl("/manage-booking/booking-details/" + order.getCode());
					getSessionService().setAttribute(AssistedservicefacadesConstants.ASM_ORDER_ID_TO_REDIRECT, order.getCode());
					attachLatestSessionCart(customerModel);
					getUserService().setCurrentUser(customerModel);
					getAsmSession().setEmulatedCustomer(customerModel);
					return customerModel;
				}
			}
		}
		else if (!cart.getUser().equals(customer))
		{
			final BookingJourneyType bookingJourneyType = cart.getBookingJourneyType();
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourneyType.getCode());


			if (OptionBookingUtil.isOptionBooking(cart))
			{
				getAsmSession().setForwardUrl(OPTION_BOOKING_DETAILS_PAGE_PREFIX + cart.getCode());
			}
			else if (bcfTravelCartFacadeHelper.isQuoteCart(cart))
			{
				if (BcfCoreConstants.ANONYMOUS.equals(customerName))
				{
					getAsmSession().setForwardUrl(
							ANONYMOUS_QUOTE_DETAILS_PAGE_PREFIX + cart.getCode() + "?guestBookingIdentifier=" + cart.getGuid());
				}
				else
				{
					getAsmSession().setForwardUrl(QUOTE_DETAILS_PAGE_PREFIX + cart.getCode());
				}
			}
			else if (BookingJourneyType.BOOKING_ALACARTE.equals(bookingJourneyType))
			{
				getAsmSession().setForwardUrl(ALACARTE_CART_SUMMARY_PAGE);
			}
			else
			{
				getAsmSession().setForwardUrl(DEFAULT_CART_SUMMARY_PAGE);
			}
			cart.setCurrency(getCommerceCommonI18NService().getCurrentCurrency());
			getCartService().setSessionCart(cart);
			getUserService().setCurrentUser(cart.getUser());
			getAsmSession().setEmulatedCustomer(cart.getUser());

			return cart.getUser();
		}
		else
		{
			final BookingJourneyType bookingJourneyType = cart.getBookingJourneyType();

			if (bookingJourneyType != null)
			{
				getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
						bookingJourneyType.getCode());
			}

			if (bcfTravelCartFacadeHelper.isQuoteCart(cart))
			{
				if (BcfCoreConstants.ANONYMOUS.equals(customerName))
				{
					getAsmSession().setForwardUrl(
							ANONYMOUS_QUOTE_DETAILS_PAGE_PREFIX + cart.getCode() + "?guestBookingIdentifier=" + cart.getGuid());
				}
				else
				{
					getAsmSession().setForwardUrl(QUOTE_DETAILS_PAGE_PREFIX + cart.getCode());
				}

			}
			else if (bcfTravelCartFacadeHelper.isAlacateFlow() || BookingJourneyType.BOOKING_ALACARTE.equals(bookingJourneyType))
			{
				getAsmSession().setForwardUrl(ALACARTE_CART_SUMMARY_PAGE);
			}
			else
			{
				getAsmSession().setForwardUrl(DEFAULT_CART_SUMMARY_PAGE);
			}
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourneyType.getCode());
			cart.setCurrency(getCommerceCommonI18NService().getCurrentCurrency());
			getCartService().setSessionCart(cart);
			getUserService().setCurrentUser(customer);
			getAsmSession().setEmulatedCustomer(customer);
		}

		return customer;
	}

	// this is the case when ASM creates a cart and than tries to assign this cart to customer which might initially belonged to Anonymous
	private CartModel tryLinkCartToCustomer(final String cartId, final UserModel customer)
			throws AssistedServiceWrongCartIdException
	{
		final CartModel cart = getBcfTravelCommerceCartService()
				.getCartForCodeAndSite(cartId, baseSiteService.getCurrentBaseSite());
		if (cart != null)
		{
			//if the cart is assigned to a different owner, we should not change the owner and simply throws an exception
			if (!BcfCoreConstants.ANONYMOUS.equals(cart.getUser().getName()) && !cart.getUser().equals(customer))
			{
				throw new AssistedServiceWrongCartIdException(
						String.format("Cart with cart Id %s is assigned to different user", cartId));
			}
			cart.setUser(customer);
		}
		return cart;
	}

	private CustomerModel createUpdateCustomerInHybris(final BCF_Customer customer)
	{
		final String customerId = customer.getEmailAddress().stream().filter(EmailAddress::isPrimaryInd).findFirst().get()
				.getEmailAddress();
		CustomerModel customerModel = null;
		try
		{
			customerModel = getUserService().getUserForUID(customerId, CustomerModel.class);
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.debug("Can't find customer in Hybris for username:" + customerId);
			customerModel = getCustomerUpdateService().createCustomerProfile(customerId, customer);
			return customerModel;
		}
		getCustomerUpdateService().updateCustomerProfile(customerModel.getUid(), customer);
		return customerModel;
	}

	private BCF_Customer getCRMCustomer(final String username, final boolean guestind, final String mobileNumber)
			throws IntegrationException,
			UnsupportedEncodingException
	{
		final CRMGetCustomerResponseDTO response = getCrmSearchCustomerService().getCustomer(username, guestind);
		if (!validateResponse(response))
		{
			// show error message to user based on error message in response
			final String message = "Can't find customer in CRM for username:" + username + " and mobileNumber:" + mobileNumber;
			LOG.error(message);
		}
		return response.getBCF_Customer();
	}

	private CRMGetSearchCustomerResponseDTO searchCRMCustomer(final SearchParams searchParams) throws IntegrationException
	{
		final CRMGetSearchCustomerResponseDTO response = getCrmSearchCustomerService().searchCustomer(searchParams);
		if (!validateSearchResponse(response))
		{
			// show error message to user based on error message in response
			final String message =
					"No customers in CRM for firstName:" + searchParams.getFirstName() + ", lastName:" + searchParams.getLastName()
							+ ", companyName:" + searchParams.getCompanyName() + ", mobile number " + searchParams.getMobileNumber()
							+ ", email address " + searchParams.getEmailAddress();
			LOG.error(message);
		}
		return response;
	}

	private boolean validateResponse(final CRMGetCustomerResponseDTO response)
	{
		if (Objects.isNull(response) || Objects.isNull(response.getBCF_Customer()))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	private boolean validateSearchResponse(final CRMGetSearchCustomerResponseDTO response)
	{
		if (Objects.isNull(response) || Objects.isNull(response.getBCF_Customer()) || CollectionUtils
				.isEmpty(response.getBCF_Customer()))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	private void doLogin(final UserModel agent) throws AssistedServiceException
	{
		//Check if ASM user account is disabled from Hybris backoffice...
		if (agent.isLoginDisabled())
		{
			throw new AssistedServiceAgentBlockedException("Account was blocked.");
		}

		verifyFormLoginAbility();
		if (getAsmSession() == null)
		{
			launchAssistedServiceMode();
		}

		loginAssistedServiceAgent(agent);
		LOG.info(String.format("Agent [%s] has been loged in using login form", agent.getUid()));

		// reset brute force counter after successful login
		resetBruteForceCounter(agent);
	}

	@Override
	public void stopEmulateCustomer()
	{
		getAsmSession().setEmulatedCustomer(null);
		getSessionService().removeAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME);
		getUserService().setCurrentUser(getUserService().getAnonymousUser());

		createSessionEvent(getAsmSession().getAgent(), getAsmSession().getEmulatedCustomer(), EventType.END_SESSION_EVENT);
	}

	public ADUserService getAdUserService()
	{
		return adUserService;
	}

	@Required
	public void setAdUserService(final ADUserService adUserService)
	{
		this.adUserService = adUserService;
	}

	protected Converter<BCF_Customer, CustomerData> getCrmCustomerConverter()
	{
		return crmCustomerConverter;
	}

	@Required
	public void setCrmCustomerConverter(final Converter<BCF_Customer, CustomerData> crmCustomerConverter)
	{
		this.crmCustomerConverter = crmCustomerConverter;
	}

	protected CustomerUpdateService getCustomerUpdateService()
	{
		return customerUpdateService;
	}

	@Required
	public void setCustomerUpdateService(final CustomerUpdateService customerUpdateService)
	{
		this.customerUpdateService = customerUpdateService;
	}

	protected CRMSearchCustomerService getCrmSearchCustomerService()
	{
		return crmSearchCustomerService;
	}

	@Required
	public void setCrmSearchCustomerService(final CRMSearchCustomerService crmSearchCustomerService)
	{
		this.crmSearchCustomerService = crmSearchCustomerService;
	}

	protected BcfOrderService getBcfOrderService()
	{
		return bcfOrderService;
	}

	@Required
	public void setBcfOrderService(final BcfOrderService bcfOrderService)
	{
		this.bcfOrderService = bcfOrderService;
	}

	public Converter<BCF_Customer, CustomerData> getCrmSearchCustomerConverter()
	{
		return crmSearchCustomerConverter;
	}

	@Required
	public void setCrmSearchCustomerConverter(
			final Converter<BCF_Customer, CustomerData> crmSearchCustomerConverter)
	{
		this.crmSearchCustomerConverter = crmSearchCustomerConverter;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected BCFTravelCommerceCartService getBcfTravelCommerceCartService()
	{
		return bcfTravelCommerceCartService;
	}

	@Required
	public void setBcfTravelCommerceCartService(final BCFTravelCommerceCartService bcfTravelCommerceCartService)
	{
		this.bcfTravelCommerceCartService = bcfTravelCommerceCartService;
	}

	protected UserDetailsService getUserDetailsService()
	{
		return userDetailsService;
	}

	@Required
	public void setUserDetailsService(final UserDetailsService userDetailsService)
	{
		this.userDetailsService = userDetailsService;
	}

	public BcfTravelCartFacadeHelper getBcfTravelCartFacadeHelper()
	{
		return bcfTravelCartFacadeHelper;
	}

	public void setBcfTravelCartFacadeHelper(final BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper)
	{
		this.bcfTravelCartFacadeHelper = bcfTravelCartFacadeHelper;
	}
}
