package com.bcf.facades.optionbooking.impl;

import static com.bcf.facades.constants.BcfFacadesConstants.SESSION_CART_PARAMETER_NAME;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.util.CommerceUtils;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.service.BcfOptionBookingService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OptionBookingException;
import com.bcf.facades.exception.UserValidationException;
import com.bcf.facades.optionbooking.BcfOptionBookingFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Facade Class that is responsible for handling option booking functionality
 */
public class DefaultBcfOptionBookingFacade implements BcfOptionBookingFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultBcfOptionBookingFacade.class);

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "bcfOptionBookingService")
	private BcfOptionBookingService bcfOptionBookingService;

	@Resource(name = "bcfOptionBookingNotificationService")
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "cancelBookingService")
	private CancelBookingService cancelBookingService;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "cartConverter")
	private Converter<CartModel, CartData> cartConverter;

	@Resource(name = "bcfTravelCommerceStockService")
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;


	public boolean isAllowedToAccessOptionBooking()
	{
		final String salesChannel = bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode();

		return salesChannel != null && BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(
				BcfCoreConstants.OPTION_BOOKING_SALES_CHANNELS)).contains(salesChannel);
	}

	@Override
	public String createOrUpdateOptionBooking(final CartModel cart) throws Exception
	{
		CartModel sessionCart = validateAndGetCart(cart);

		if (OptionBookingUtil.isOptionBookingUpdate(sessionCart))
		{
			return updateOptionBooking(sessionCart);
		}
		else
		{
			return createOptionBooking(sessionCart);
		}
	}

	private CartModel validateAndGetCart(CartModel cartModel) throws CartValidationException, UserValidationException
	{
		if (Objects.isNull(cartModel))
		{
			cartModel = getCartService().getSessionCart();
		}

		validateCartForOptionBooking(cartModel);
		return cartModel;
	}

	private void validateCartForOptionBooking(final CartModel cartModel) throws CartValidationException, UserValidationException
	{
		if (Objects.isNull(cartModel) || CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			throw new CartValidationException("Either cart is not available or has no line entries!");
		}

		if (getUserService().isAnonymousUser(cartModel.getUser()))
		{
			throw new UserValidationException("Please select customer first!");
		}
	}

	private String createOptionBooking(final CartModel cartModel) throws CalculationException, IntegrationException
	{
		cartModel.setVacationOptionBooking(true);
		cartModel.setOptionBookingExpiryDate(createOptionBookingExpiryDate(cartModel, null));
		if (Objects.isNull(cartModel.getOptionBookingCreationDate()))
		{
			cartModel.setOptionBookingCreationDate(new Date());
		}

		if (!cartModel.isAlreadyCreatedFromOptionBooking())
		{
			cartModel.setAlreadyCreatedFromOptionBooking(true);
			boolean eBookingUpdateSuccess = updateCartWithEBookingDataForOptionBooking(cartModel);

			if (eBookingUpdateSuccess)
			{
				getBcfOptionBookingNotificationService().startOptionBookingEmailProcess(cartModel);
				LOG.info(String.format("Option booking [%s] created. Triggering email process.", cartModel.getCode()));
			}
			else
			{
				throw new OptionBookingException("Error in updating Cart with EBooking Response for option booking");
			}
		}
		getModelService().save(cartModel);

		//so that agent can create new cart
		getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, null);
		return cartModel.getCode();
	}

	/**
	 * this will be used in the scenario mentioned below:
	 * <p>
	 * Lets say Agent has convert option booking to normal cart and customer wishes to add a new activity to the cart before checkout.
	 * But after adding new activity, for some reason agent is unable to proceed with the payment and wants to save this cart again as option booking.
	 * <br>
	 * This method will track the changes done ,e.g.,but not limited,  adding/ removal of any new activity and send the vendor email(if required) to that particular vendor only
	 *
	 * @param cartModel cart model
	 * @return option booking id
	 */
	private String updateOptionBooking(final CartModel cartModel) throws CalculationException, IntegrationException
	{

		String optionBookingId = createOptionBooking(cartModel);

		List<AbstractOrderEntryModel> modifiedEntries = new ArrayList<>();

		modifiedEntries.addAll(getTransportEntriesModified(cartModel));
		modifiedEntries.addAll(getHotelEntriesModified(cartModel));
		modifiedEntries.addAll(getActivityEntriesModified(cartModel));

		getBcfOptionBookingNotificationService().startOptionBookingForEntriesEmailProcess(cartModel, modifiedEntries);
		updateLastModifiedDate(cartModel, true);
		LOG.info(String.format("Option booking [%s] updated.", optionBookingId));
		return optionBookingId;
	}

	private List<AbstractOrderEntryModel> getActivityEntriesModified(final CartModel cartModel)
	{
		if (getBcfBookingService().checkIfActivityEntriesExist(cartModel))
		{
			List<AbstractOrderEntryModel> activityEntries = bcfBookingService.getActivityEntries(cartModel);

			return StreamUtil.safeStream(activityEntries)
					.filter(e -> e.getModifiedtime().after(cartModel.getOptionBookingLastModifiedDate())).collect(
							Collectors.toList());
		}
		return Collections.emptyList();
	}

	private List<AbstractOrderEntryModel> getHotelEntriesModified(final CartModel cartModel)
	{
		if (getBcfBookingService().checkIfOrderEntriesExistOfType(cartModel, OrderEntryType.ACCOMMODATION))
		{
			List<AbstractOrderEntryModel> accommodationEntries = bcfBookingService.getAccommodationEntries(cartModel);

			return StreamUtil.safeStream(accommodationEntries)
					.filter(e -> e.getModifiedtime().after(cartModel.getOptionBookingLastModifiedDate())).collect(
							Collectors.toList());
		}
		return Collections.emptyList();
	}

	private List<AbstractOrderEntryModel> getTransportEntriesModified(final CartModel cartModel)
	{
		if (getBcfBookingService().checkIfOrderEntriesExistOfType(cartModel, OrderEntryType.TRANSPORT))
		{
			List<AbstractOrderEntryModel> transportEntries = bcfBookingService.getTransportEntries(cartModel);

			return StreamUtil.safeStream(transportEntries)
					.filter(e -> e.getModifiedtime().after(cartModel.getOptionBookingLastModifiedDate())).collect(
							Collectors.toList());
		}
		return Collections.emptyList();
	}



	/**
	 * updates the cart with option booking entries.<br>
	 * it reset amountToPayInfoModels, we are setting it again with new cache key or booking reference while updating cart
	 *
	 * @param cartModel cart
	 * @return boolean
	 * @throws IntegrationException if error while integration API call
	 * @throws CalculationException if error while calculating cart totals
	 */
	private boolean updateCartWithEBookingDataForOptionBooking(final CartModel cartModel)
			throws CalculationException, IntegrationException
	{
		List<AbstractOrderEntryModel> transportOrderEntryModels = StreamUtil.safeStream(cartModel.getEntries())
				.filter(entry -> Objects.nonNull(entry.getTravelOrderEntryInfo())).collect(
						Collectors.toList());

		if (CollectionUtils.isEmpty(transportOrderEntryModels))
		{
			return true;
		}
		getBcfTravelCartFacade().removeAmountToPayInfos(cartModel);
		return getBcfTravelCartFacade().updateCartWithEbookingDataForOptionBooking(cartModel);
	}

	/**
	 * returns the date that will be used as expiration date for the option booking.
	 * <br>
	 * This method checks if the expiration date is greater than the departure date, then return departure date - 1 day else return expiration date
	 * <br>
	 * This method also sets the time part of the date to the "end of the day" time.
	 * <br>
	 * <p>
	 * The parameter expiration date is option, is not provided, the system considers the default expiration days and adds that into the current date to calculate expirationDate
	 *
	 * @param abstractOrder         cart id
	 * @param updatedExpirationDate optional updated expiration date
	 * @return departure date
	 */
	public Date createOptionBookingExpiryDate(final AbstractOrderModel abstractOrder, final Date updatedExpirationDate)
	{
		Date expirationDateToUse = updatedExpirationDate;
		if (Objects.isNull(expirationDateToUse))
		{
			int defaultExpiryDays = Integer.valueOf(getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.OPTION_BOOKING_DEFAULT_EXPIRY_TIME_IN_DAYS));

			expirationDateToUse = DateUtils.addDays(abstractOrder.getCreationtime(), defaultExpiryDays);
			expirationDateToUse = BCFDateUtils.setToEndOfDay(expirationDateToUse);
		}

		Date departureDate = OptionBookingUtil.getDepartureDate(abstractOrder);

		if (Objects.nonNull(departureDate) && expirationDateToUse.after(departureDate))
		{
			return departureDate;
		}
		else
		{
			return expirationDateToUse;
		}
	}

	private void updateLastModifiedDate(CartModel optionBooking, final boolean toSaveCart)
	{
		optionBooking.setOptionBookingLastModifiedDate(new Date());

		if (toSaveCart)
		{
			getModelService().save(optionBooking);
		}
	}

	@Override
	public SearchPageData<CartData> getOptionBookings(final PageableData pageableData)
	{
		getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, null);
		final SearchPageData<CartModel> optionBookingsSearchPageData = getBcfOptionBookingService()
				.getOptionBookings(pageableData);

		return CommerceUtils
				.convertPageData(optionBookingsSearchPageData, getCartConverter());
	}

	@Override
	public CartData getOptionBookingDetailsForCode(final String optionBookingId)
	{
		final CartModel cart = getBcfOptionBookingService().getCartForCode(optionBookingId);

		CartData optionBookingDetail = null;
		if (Objects.nonNull(cart))
		{
			optionBookingDetail = cartConverter.convert(cart);

			if (!getUserService().getCurrentUser().equals(cart.getUser()))
			{
				getUserService().setCurrentUser(cart.getUser());
			}

			getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, cart);
			getSessionService()
					.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, cart.getBookingJourneyType().getCode());
		}

		return optionBookingDetail;
	}

	@Override
	public boolean convertOptionBookingToCart(final String optionBookingId)
	{
		boolean isConversionSuccess = false;
		try
		{
			final CartModel cart = getBcfOptionBookingService().getCartForCode(optionBookingId);
			if (Objects.nonNull(cart))
			{
				cart.setVacationOptionBooking(false);
				cart.setOptionBookingExpiryDate(null);
				cart.setStockHoldTime(getBcfTravelCommerceStockService().getStockHoldExpiryTimeInMs(cart.getSalesApplication()));
				updateLastModifiedDate(cart, false);
				getModelService().save(cart);

				getCalculationService().recalculate(cart);
				getCartService().setSessionCart(cart);
				getSessionService()
						.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, cart.getBookingJourneyType().getCode());
				isConversionSuccess = true;
				LOG.info(String.format("Option booking [%s] converted to cart.", optionBookingId));
			}
			return isConversionSuccess;
		}
		catch (Exception e)
		{
			LOG.error(String.format("Error in conversion of option booking [%s] to Cart!", optionBookingId), e);
			return isConversionSuccess;
		}
	}

	@Override
	public String convertQuoteToOptionBooking(final String quoteBookingId) throws Exception
	{
		final CartModel cart = getBcfOptionBookingService().getCartForCode(quoteBookingId);

		if (Objects.isNull(cart))
		{
			throw new OptionBookingException(
					String.format("Error in converting Quote to OptionBooking. No Quote found for id %s", quoteBookingId));
		}
		cart.setQuote(false);
		cart.setEmailAddress(null);
		return createOptionBooking(cart);
	}

	@Override
	public void updateExpiryDate(final String optionBookingId, final Date updatedDate)
			throws CalculationException, IntegrationException, UserValidationException, CartValidationException
	{
		final CartModel cart = getBcfOptionBookingService().getCartForCode(optionBookingId);
		validateCartForOptionBooking(cart);


		Date expirationDate = createOptionBookingExpiryDate(cart, BCFDateUtils.setToEndOfDay(updatedDate));
		Date oldExpirationDate = cart.getOptionBookingExpiryDate();
		cart.setOptionBookingExpiryDate(expirationDate);
		getModelService().save(cart);
		updateCartWithEBookingDataForOptionBooking(cart);
		LOG.info(String.format("Option booking [%s] - expiration date updated to [%s], old [%s]", optionBookingId,
				expirationDate != null ? expirationDate.toString() : "",
				oldExpirationDate != null ? oldExpirationDate.toString() : ""));
	}

	@Override
	public BcfGlobalReservationData getCurrentGlobalReservationData()
	{
		return getGlobalReservationFacade().getCurrentGlobalReservationData();
	}

	@Override
	public void cancelOptionBooking(final String optionBookingId) throws Exception
	{
		final CartModel optionBookingCart = getBcfOptionBookingService().getCartForCode(optionBookingId);
		getBcfOptionBookingService().cancelAndReleaseOptionBookingInventory(optionBookingCart);
		getBcfOptionBookingNotificationService().startCancelOrderEmailProcess(optionBookingCart);
		getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, null);
		LOG.info(String.format("Option booking [%s] cancelled", optionBookingId));
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public BcfOptionBookingService getBcfOptionBookingService()
	{
		return bcfOptionBookingService;
	}

	public void setBcfOptionBookingService(final BcfOptionBookingService bcfOptionBookingService)
	{
		this.bcfOptionBookingService = bcfOptionBookingService;
	}

	public Converter<CartModel, CartData> getCartConverter()
	{
		return cartConverter;
	}

	public void setCartConverter(
			final Converter<CartModel, CartData> cartConverter)
	{
		this.cartConverter = cartConverter;
	}

	public BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	public void setBcfTravelCartFacade(final BcfTravelCartFacade bcfTravelCartFacade)
	{
		this.bcfTravelCartFacade = bcfTravelCartFacade;
	}

	public CancelBookingService getCancelBookingService()
	{
		return cancelBookingService;
	}

	public void setCancelBookingService(final CancelBookingService cancelBookingService)
	{
		this.cancelBookingService = cancelBookingService;
	}

	public BCFGlobalReservationFacade getGlobalReservationFacade()
	{
		return globalReservationFacade;
	}

	public void setGlobalReservationFacade(final BCFGlobalReservationFacade globalReservationFacade)
	{
		this.globalReservationFacade = globalReservationFacade;
	}

	public BcfOptionBookingNotificationService getBcfOptionBookingNotificationService()
	{
		return bcfOptionBookingNotificationService;
	}

	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}

