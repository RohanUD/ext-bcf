package com.bcf.facades.optionbooking;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import java.util.Date;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.UserValidationException;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BcfOptionBookingFacade
{
	boolean isAllowedToAccessOptionBooking();

	String createOrUpdateOptionBooking(final CartModel cart) throws Exception;

	SearchPageData<CartData> getOptionBookings(final PageableData pageableData);

	CartData getOptionBookingDetailsForCode(String optionBookingId);

	void updateExpiryDate(String optionBookingId, Date updatedDate)
			throws CalculationException, IntegrationException, UserValidationException, CartValidationException;

	boolean convertOptionBookingToCart(String optionBookingId);

	void cancelOptionBooking(String optionBookingId) throws Exception;

	String convertQuoteToOptionBooking(String quoteBookingId) throws CalculationException, IntegrationException, Exception;

	BcfGlobalReservationData getCurrentGlobalReservationData();

}
