/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.manager.impl;

import reactor.util.CollectionUtils;

import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.ancillary.search.manager.AncillarySearchResponseDataListPipelineManager;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultAncillarySearchResponseDataListPipelineManager implements AncillarySearchResponseDataListPipelineManager
{
	private List<AncillarySearchResponseDataListHandler> handlers;

	@Override
	public OfferResponseDataList executePipeline(final OfferRequestDataList offerRequestDataList)
	{
		if (Objects.isNull(offerRequestDataList) || CollectionUtils.isEmpty(offerRequestDataList.getOfferRequestDatas()))
		{
			return null;
		}

		final OfferResponseDataList offerResponseDataList = new OfferResponseDataList();
		final List<OfferResponseData> offerResponseDatas = new ArrayList<>(offerRequestDataList.getOfferRequestDatas().size());

		offerResponseDataList.setOfferResponseDatas(offerResponseDatas);


		for (final AncillarySearchResponseDataListHandler handler : getHandlers())
		{
			handler.handle(offerRequestDataList, offerResponseDataList);
		}

		return offerResponseDataList;
	}

	public List<AncillarySearchResponseDataListHandler> getHandlers()
	{
		return handlers;
	}

	public void setHandlers(final List<AncillarySearchResponseDataListHandler> handlers)
	{
		this.handlers = handlers;
	}

}
