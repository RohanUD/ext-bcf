/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.travelfacades.ancillary.search.handlers.impl.AbstractOfferRestrictionHandler;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultOfferGroupOfferRestrictionResponseDataListHandler extends AbstractOfferRestrictionHandler
		implements AncillarySearchResponseDataListHandler
{

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		for (final OfferResponseData offerResponseData : offerResponseDataList.getOfferResponseDatas())
		{
			offerResponseData.getOfferGroups().stream()
					.filter(offerGroup -> CollectionUtils.isNotEmpty(offerGroup.getOfferPricingInfos()))
					.forEach(offerGroupData -> offerGroupData.getOfferPricingInfos().forEach(this::setTravelRestriction));
		}
	}

}
