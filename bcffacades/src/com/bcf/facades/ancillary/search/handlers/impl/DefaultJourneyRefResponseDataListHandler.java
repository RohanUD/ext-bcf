/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.ancillary.data.OfferRequestData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


/**
 * Handler to populate basic details such as journey number. <br>
 * This handler must be the first handler in the handler chain, as it is setting the basic attributes upon which
 * subsequent handlers are based.
 */
public class DefaultJourneyRefResponseDataListHandler implements AncillarySearchResponseDataListHandler
{

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		for (final OfferRequestData requestData : offerRequestDataList.getOfferRequestDatas())
		{
			final OfferResponseData offerResponseData = new OfferResponseData();
			offerResponseData.setJourneyRefNumber(requestData.getJourneyRefNumber());
			offerResponseDataList.getOfferResponseDatas().add(offerResponseData);
		}
	}

}
