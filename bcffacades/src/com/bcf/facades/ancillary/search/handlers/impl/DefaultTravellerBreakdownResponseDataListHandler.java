/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferGroupData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferPricingInfoData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferRequestData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OriginDestinationOfferInfoData;
import de.hybris.platform.commercefacades.travel.ancillary.data.TravellerBreakdownData;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.travelfacades.ancillary.search.handlers.impl.AbstractBreakdownHandler;
import de.hybris.platform.travelfacades.util.TransportOfferingUtils;
import de.hybris.platform.travelservices.enums.AddToCartCriteriaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultTravellerBreakdownResponseDataListHandler extends AbstractBreakdownHandler
		implements AncillarySearchResponseDataListHandler
{

	private static final Logger LOG = Logger.getLogger(DefaultTravellerBreakdownResponseDataListHandler.class);

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		for (final OfferResponseData offerResponseData : offerResponseDataList.getOfferResponseDatas())
		{
			for (final OfferGroupData offerGroupData : offerResponseData.getOfferGroups())
			{
				if (CollectionUtils.isEmpty(offerGroupData.getOriginDestinationOfferInfos()))
				{
					continue;
				}

				for (final OriginDestinationOfferInfoData odOfferInfo : offerGroupData.getOriginDestinationOfferInfos())
				{
					final List<OfferPricingInfoData> filteredOfferPricingInfos = getFilteredPricingInfos(odOfferInfo);

					for (final OfferPricingInfoData offerPricingInfoData : filteredOfferPricingInfos)
					{
						final List<TravellerBreakdownData> travellerBreakdownDataList = getTravellerBreakdownData(
								offerPricingInfoData.getProduct().getCode(),
								getAppropriateOfferRequestData(offerRequestDataList, offerResponseData.getJourneyRefNumber()),
								offerGroupData.getCode(), odOfferInfo);
						offerPricingInfoData.setTravellerBreakdowns(travellerBreakdownDataList);
					}
				}
			}
		}
	}

	/**
	 * For each TravellerData in ItineraryData(represents travellers of the Itinerary), create TravellerBreakdownData as
	 * <p>
	 * Get price for the Product in offer
	 * <p>
	 * Set TravellerData in TravellerBreakdownData
	 * <p>
	 * Determine the quantity of offer from the selectedOffers.
	 * <p>
	 * Calculate and set PassengerFareData for the product in offer.
	 *
	 * @param productCode      product in offer
	 * @param offerRequestData OfferRequestData
	 * @param offerGroupCode   category of product in offer
	 * @param odOfferInfo      OriginDestinationOfferInfoData
	 * @return a list of TravellerBreakdownData.
	 */
	protected List<TravellerBreakdownData> getTravellerBreakdownData(final String productCode,
			final OfferRequestData offerRequestData, final String offerGroupCode, final OriginDestinationOfferInfoData odOfferInfo)
	{
		final PriceInformation priceInfo = getOfferPricingInformation(offerGroupCode, odOfferInfo, productCode);
		PriceData priceData = null;
		if (Optional.ofNullable(priceInfo).isPresent())
		{
			priceData = createPriceData(priceInfo);
		}
		final List<TravellerBreakdownData> travellerBreakdownDataList = new ArrayList<>();
		for (final TravellerData travellerData : getTravellerData(offerRequestData, odOfferInfo))
		{
			final TravellerBreakdownData travellerBreakdownData = new TravellerBreakdownData();
			travellerBreakdownData.setTraveller(travellerData);

			final int quantity = getTravellerQuantity(offerRequestData, travellerData, offerGroupCode, odOfferInfo, productCode);
			travellerBreakdownData.setQuantity(quantity);

			if (Optional.ofNullable(priceData).isPresent())
			{
				travellerBreakdownData.setPassengerFare(getPassengerFareData(priceData, quantity));
			}

			travellerBreakdownDataList.add(travellerBreakdownData);

		}
		return travellerBreakdownDataList;
	}

	/**
	 * Overrides the super implementation to get prices based on searchCriteria
	 *
	 * @param offerGroupCode
	 * @param odOfferInfo
	 * @param productCode
	 * @return
	 */
	protected PriceInformation getOfferPricingInformation(final String offerGroupCode,
			final OriginDestinationOfferInfoData odOfferInfo, final String productCode)
	{
		PriceInformation priceInfo = null;

		if (Objects.isNull(odOfferInfo) || CollectionUtils.isEmpty(odOfferInfo.getTransportOfferings()))
		{
			LOG.debug("No price information for productData (code: " + productCode + ")");
			return priceInfo;
		}

		final int originDestinationRefNumber = odOfferInfo.getOriginDestinationRefNumber();
		final Optional<TransportOfferingData> transportOffering = odOfferInfo.getTransportOfferings().stream().findFirst();
		final String routeCode = odOfferInfo.getTravelRouteCode();

		if (!transportOffering.isPresent())
		{
			LOG.debug("No price information for productData (code: " + productCode + ")");
			return priceInfo;
		}

		priceInfo = ((BCFTravelCommercePriceFacade) getTravelCommercePriceFacade()).getPriceInfoForAncillary(
				originDestinationRefNumber, productCode, transportOffering.get().getCode(), routeCode, offerGroupCode);

		return priceInfo;
	}

	/**
	 * Filters the offerGroups by the PER_LEG_PER_PAX {@link AddToCartCriteriaType}
	 *
	 * @param odOfferInfo
	 * @return
	 */
	protected List<OfferPricingInfoData> getFilteredPricingInfos(final OriginDestinationOfferInfoData odOfferInfo)
	{
		return odOfferInfo.getOfferPricingInfos().stream()
				.filter(opi -> opi.getTravelRestriction() == null || opi.getTravelRestriction().getAddToCartCriteria() == null
						|| StringUtils.equalsIgnoreCase(opi.getTravelRestriction().getAddToCartCriteria(),
						AddToCartCriteriaType.PER_LEG_PER_PAX.getCode()))
				.collect(Collectors.toList());
	}

	private OfferRequestData getAppropriateOfferRequestData(final OfferRequestDataList offerRequestDataList,
			final int journeyRefNumber)
	{
		return offerRequestDataList.getOfferRequestDatas().stream()
				.filter(offerRequestData -> offerRequestData.getJourneyRefNumber() == journeyRefNumber).findFirst().get();
	}

	/**
	 * Method to get travellers data from ItineraryData of OfferRequestData which match to OriginDestinationOfferInfoData
	 *
	 * @param offerRequestData
	 * @param odOfferInfo
	 * @return
	 */
	protected List<TravellerData> getTravellerData(final OfferRequestData offerRequestData,
			final OriginDestinationOfferInfoData odOfferInfo)
	{
		final List<ItineraryData> itineraryDataForODInfoData = offerRequestData.getItineraries().stream()
				.filter(itineraryData -> itineraryData.getRoute().getCode().equals(odOfferInfo.getTravelRouteCode()))
				.collect(Collectors.toList());
		return CollectionUtils.isNotEmpty(itineraryDataForODInfoData) ? itineraryDataForODInfoData.get(0).getTravellers()
				: Collections.emptyList();
	}

	/**
	 * This method determines the quantity of product in offer as
	 * <p>
	 * if the product in offer is already selected by customer (either bundled or selected) then determine the quantity
	 * from SelectedOffers in offerRequestData.
	 * <p>
	 * if no match found, then quantity is zero.
	 *
	 * @param offerRequestData OfferRequestData
	 * @param travellerData    TravellerData
	 * @param offerGroupCode   category of product in offer
	 * @param odOfferInfo      OriginDestinationOfferInfoData
	 * @param productCode      product in offer
	 * @return
	 */
	protected Integer getTravellerQuantity(final OfferRequestData offerRequestData, final TravellerData travellerData,
			final String offerGroupCode, final OriginDestinationOfferInfoData odOfferInfo, final String productCode)
	{
		Integer quantity = 0;

		final Optional<OfferGroupData> offerGroupData = offerRequestData.getSelectedOffers().getOfferGroups().stream()
				.filter(offerGroup -> offerGroup.getCode().equals(offerGroupCode)).findFirst();

		if (!offerGroupData.isPresent())
		{
			return quantity;
		}

		for (final OriginDestinationOfferInfoData selectedOdOfferInfoData : offerGroupData.get().getOriginDestinationOfferInfos())
		{
			if (!(selectedOdOfferInfoData.getTravelRouteCode().equals(odOfferInfo.getTravelRouteCode()) && TransportOfferingUtils
					.compareTransportOfferings(selectedOdOfferInfoData.getTransportOfferings(), odOfferInfo.getTransportOfferings())))
			{
				continue;
			}
			for (final OfferPricingInfoData offerPricingInfoData : selectedOdOfferInfoData.getOfferPricingInfos())
			{
				if (!offerPricingInfoData.getProduct().getCode().equals(productCode))
				{
					continue;
				}
				final Optional<TravellerBreakdownData> selectedTravellerBreakdownData = offerPricingInfoData.getTravellerBreakdowns()
						.stream()
						.filter(travellerBreakdown -> travellerBreakdown.getTraveller().getLabel().equals(travellerData.getLabel()))
						.findFirst();
				if (selectedTravellerBreakdownData.isPresent())
				{
					quantity = quantity + selectedTravellerBreakdownData.get().getQuantity();
				}
			}
		}

		return quantity;
	}
}
