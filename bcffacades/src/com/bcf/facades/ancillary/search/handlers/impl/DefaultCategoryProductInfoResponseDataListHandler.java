/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.OriginDestinationOptionData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferRequestData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.services.BCFTravelCategoryService;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultCategoryProductInfoResponseDataListHandler implements AncillarySearchResponseDataListHandler
{

	private BCFTravelCategoryService travelCategoryService;

	private String assistanceCategoryCode;

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		final Set<String> allVehiclesFromRequestData = new HashSet<>();

		// collects all the vehicles and sectors from all the journey's transport offerings
		for (final OfferRequestData offerRequestData : offerRequestDataList.getOfferRequestDatas())
		{
			for (final ItineraryData anItineraryData : offerRequestData.getItineraries())
			{
				for (final OriginDestinationOptionData originDestinationOption : anItineraryData.getOriginDestinationOptions())
				{
					for (final TransportOfferingData transportOffering : originDestinationOption.getTransportOfferings())
					{
						allVehiclesFromRequestData.add(transportOffering.getTransportVehicle().getCode());
					}
				}
			}
		}

		updateProducts(offerRequestDataList, offerResponseDataList, new ArrayList<>(allVehiclesFromRequestData));
	}

	private void updateProducts(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList,
			final List<String> allVehiclesFromRequestData)
	{
		final List<ProductModel> allProductsForVehicles = getTravelCategoryService()
				.getAllProductsForVehicles(allVehiclesFromRequestData);

		updateProductsForVehicles(offerRequestDataList, offerResponseDataList, allProductsForVehicles);

	}

	private void updateProductsForVehicles(final OfferRequestDataList offerRequestDataList,
			final OfferResponseDataList offerResponseDataList, final List<ProductModel> allProducts)
	{
		for (final OfferRequestData offerRequestData : offerRequestDataList.getOfferRequestDatas())
		{
			for (final ItineraryData anItineraryData : offerRequestData.getItineraries())
			{
				for (final OriginDestinationOptionData originDestinationOption : anItineraryData.getOriginDestinationOptions())
				{
					for (final TransportOfferingData transportOffering : originDestinationOption.getTransportOfferings())
					{
						final List<ProductModel> productsForTransportOffering = filterProductsForThisTransportOffering(allProducts,
								transportOffering);
						updateProductsPerTransportOffering(offerResponseDataList, offerRequestData.getJourneyRefNumber(),
								transportOffering, productsForTransportOffering);
					}
				}
			}
		}
	}

	private List<ProductModel> filterProductsForThisTransportOffering(final List<ProductModel> products,
			final TransportOfferingData transportOffering)
	{
		return products
				.stream().filter(product -> product.getVehicles().stream().map(TransportVehicleModel::getCode)
						.collect(Collectors.toList()).contains(transportOffering.getTransportVehicle().getCode()))
				.collect(Collectors.toList());
	}

	private void updateProductsPerTransportOffering(final OfferResponseDataList offerResponseDataList,
			final int journeyRefNumber, final TransportOfferingData transportOffering, final List<ProductModel> products)
	{
		Map<String, List<ProductModel>> productPerOffering = null;

		final Optional<OfferResponseData> optionalOfferResponseData = offerResponseDataList.getOfferResponseDatas().stream()
				.filter(responseData -> responseData.getJourneyRefNumber() == journeyRefNumber).findFirst();

		if (optionalOfferResponseData.isPresent())
		{
			final OfferResponseData offerResponseData = optionalOfferResponseData.get();

			if (MapUtils.isEmpty(offerResponseData.getProductsPerTransportOfferings()))
			{
				productPerOffering = new HashMap<>();
			}
			else
			{
				productPerOffering = offerResponseData.getProductsPerTransportOfferings();
			}

			if (productPerOffering.containsKey(transportOffering.getCode()))
			{
				final List<ProductModel> allProducts = new ArrayList<>();
				allProducts.addAll(productPerOffering.get(transportOffering.getCode()));
				allProducts.addAll(products);
				productPerOffering.put(transportOffering.getCode(), allProducts);
			}
			else
			{
				productPerOffering.put(transportOffering.getCode(), products);
			}
			offerResponseData.setProductsPerTransportOfferings(productPerOffering);
		}
	}

	public BCFTravelCategoryService getTravelCategoryService()
	{
		return travelCategoryService;
	}

	@Required
	public void setTravelCategoryService(final BCFTravelCategoryService travelCategoryService)
	{
		this.travelCategoryService = travelCategoryService;
	}

	public String getAssistanceCategoryCode()
	{
		return assistanceCategoryCode;
	}

	@Required
	public void setAssistanceCategoryCode(final String assistanceCategoryCode)
	{
		this.assistanceCategoryCode = assistanceCategoryCode;
	}

}
