/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferRequestData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.stream.Collectors;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultBasicOfferResponseDataListHandler implements AncillarySearchResponseDataListHandler
{

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		for (final OfferRequestData offerRequestData : offerRequestDataList.getOfferRequestDatas())
		{
			final List<ItineraryData> itineraries = new ArrayList<>(offerRequestData.getItineraries().size());
			final ListIterator<ItineraryData> itr = offerRequestData.getItineraries().listIterator();
			while (itr.hasNext())
			{
				final ItineraryData itinerary = itr.next();
				final List<TravellerData> travellers = itinerary.getTravellers().stream()
						.filter(traveler -> traveler.getTravellerInfo() instanceof PassengerInformationData)
						.collect(Collectors.toList());
				itinerary.setTravellers(travellers);
				itineraries.add(itinerary);
			}
			updateItinerariesInResponseData(offerResponseDataList, offerRequestData.getJourneyRefNumber(), itineraries);
		}

	}

	private void updateItinerariesInResponseData(final OfferResponseDataList offerResponseDataList, final int journeyRefNumber,
			final List<ItineraryData> itineraries)
	{
		final Optional<OfferResponseData> optionalOfferResponseData = offerResponseDataList.getOfferResponseDatas().stream()
				.filter(responseData -> responseData.getJourneyRefNumber() == journeyRefNumber).findFirst();

		if (optionalOfferResponseData.isPresent())
		{
			optionalOfferResponseData.get().setItineraries(itineraries);
		}
	}

}
