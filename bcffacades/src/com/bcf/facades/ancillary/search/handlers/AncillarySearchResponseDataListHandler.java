/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers;

import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


/**
 * Interface for handlers classes that will be updating the {@link OfferResponseDataList} based on the
 * {@link OfferRequestDataList} given in input
 */
public interface AncillarySearchResponseDataListHandler
{
	/**
	 * Handle method.
	 *
	 * @param offerRequestDataList  the list of offer request data
	 * @param offerResponseDataList the offer response data
	 */
	void handle(OfferRequestDataList offerRequestDataList, OfferResponseDataList offerResponseDataList);
}
