/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferGroupData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferPricingInfoData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OriginDestinationOfferInfoData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.travelfacades.ancillary.search.handlers.impl.OfferPricingInfoHandler;
import de.hybris.platform.travelfacades.promotion.TravelPromotionsFacade;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.order.TravelCartService;
import de.hybris.platform.travelservices.services.TransportOfferingService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.core.services.BCFTravelCategoryService;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultOfferPricingInfoResponseDataListHandler implements AncillarySearchResponseDataListHandler
{
	private static final Logger LOGGER = Logger.getLogger(OfferPricingInfoHandler.class);

	private TravelPromotionsFacade travelPromotionsFacade;
	private ProductService productService;
	private CategoryService categoryService;
	private TransportOfferingService transportOfferingService;
	private Converter<ProductModel, ProductData> productConverter;
	private TravelCartService travelCartService;
	private BCFTravelCategoryService travelCategoryService;

	/**
	 * Creates and populates OfferPricingInfoData for products from the category. Retrieve list of mapped products for
	 * each category (set in the hanlder {@link CategoryProductInfoHandler}) Collect common products across all
	 * transportOfferings, whose product code matches with the products from above list and stock level is more than
	 * zero. For each of those collected products create OfferPricingInfoData and set the productData.
	 */
	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		final boolean applyPromotion = getTravelPromotionsFacade().isCurrentUserEligibleForTravelPromotions();

		for (final OfferResponseData offerResponseData : offerResponseDataList.getOfferResponseDatas())
		{
			final List<OfferGroupData> offerGroups = offerResponseData.getOfferGroups();
			for (final OfferGroupData offerGroupData : offerGroups)
			{

				if (CollectionUtils.isEmpty(offerGroupData.getOriginDestinationOfferInfos()))
				{
					continue;
				}
				for (final OriginDestinationOfferInfoData originDestinationOfferInfo : offerGroupData
						.getOriginDestinationOfferInfos())
				{
					final List<ProductModel> productsForCategory = getProductsForOriginDestinationAndCategoryCode(
							originDestinationOfferInfo, offerResponseData, offerGroupData.getCode());
					createOfferPricingInfos(applyPromotion, productsForCategory, originDestinationOfferInfo);
				}
			}
		}
	}

	/**
	 * it gets the products per transport offering and also only for the provided category code
	 *
	 * @param originDestinationOfferInfo
	 * @param offerResponseData
	 * @param categoryCode
	 * @return
	 */
	private List<ProductModel> getProductsForOriginDestinationAndCategoryCode(
			final OriginDestinationOfferInfoData originDestinationOfferInfo, final OfferResponseData offerResponseData,
			final String categoryCode)
	{
		final List<ProductModel> products = new ArrayList<>();
		for (final TransportOfferingData tod : originDestinationOfferInfo.getTransportOfferings())
		{

			if (!offerResponseData.getProductsPerTransportOfferings().containsKey(tod.getCode()))
			{
				continue;
			}

			final List<ProductModel> productPerTransportOffering = offerResponseData.getProductsPerTransportOfferings()
					.get(tod.getCode());

			products.addAll(
					getTravelCategoryService().getProductModelForProductCodesAndCategory(categoryCode, productPerTransportOffering));
		}
		return products;
	}

	protected void createOfferPricingInfos(final boolean applyPromotion, final List<ProductModel> productsForCategory,
			final OriginDestinationOfferInfoData originDestinationOfferInfo)
	{
		final List<ProductModel> qualifiedProductsInOffer = getQualifiedProductsForOfferGroup(productsForCategory,
				originDestinationOfferInfo);
		final List<OfferPricingInfoData> pricedOffers = new ArrayList<OfferPricingInfoData>();
		for (final ProductModel productModel : qualifiedProductsInOffer)
		{
			final OfferPricingInfoData offerPricingInfoData = new OfferPricingInfoData();
			final ProductData productData = getProductConverter().convert(productModel);
			if (applyPromotion)
			{
				getTravelPromotionsFacade().populatePotentialPromotions(productModel, productData);
			}
			offerPricingInfoData.setProduct(productData);
			pricedOffers.add(offerPricingInfoData);
		}
		originDestinationOfferInfo.setOfferPricingInfos(pricedOffers);
	}

	/**
	 * This method collects the products that belong to a category(offerGroupData.getCode()), common across all
	 * transportOfferings and stockLevel more than zero.
	 *
	 * @param productsForCategory        List of ProductModels which belong to a category
	 * @param originDestinationOfferInfo OriginDestinationOfferInfoData object, holds list of TransportOfferings.
	 * @return List of ProductModel.
	 */
	protected List<ProductModel> getQualifiedProductsForOfferGroup(final List<ProductModel> productsForCategory,
			final OriginDestinationOfferInfoData originDestinationOfferInfo)
	{
		final Map<String, List<ProductModel>> productsPerTransportOffering = getAvailableProductsForTransportOffering(
				productsForCategory, originDestinationOfferInfo);

		return getCommonProducts(productsPerTransportOffering);
	}



	/**
	 * This method returns available products for each transportOffering. The product in offer should be of category of
	 * products as retrieved using the category code(offerGroupData.getCode()) and the available stockLevel of the
	 * product should be more than zero.
	 *
	 * @param productsForCategory
	 * @param originDestinationOfferInfo
	 * @return
	 */
	protected Map<String, List<ProductModel>> getAvailableProductsForTransportOffering(
			final List<ProductModel> productsForCategory, final OriginDestinationOfferInfoData originDestinationOfferInfo)
	{
		final Map<String, List<ProductModel>> productsPerTransportOffering = new HashMap<>();

		for (final TransportOfferingData transportOfferingData : originDestinationOfferInfo.getTransportOfferings())
		{
			final TransportOfferingModel transportOfferingModel = getTransportOfferingService()
					.getTransportOffering(transportOfferingData.getCode());
			productsPerTransportOffering.put(transportOfferingModel.getCode(), productsForCategory);
		}
		return productsPerTransportOffering;
	}

	/**
	 * checks the stock level of a product and filters out with no valid stocks
	 *
	 * @param productsForCategory
	 * @param transportOfferingModel stock level warehouse
	 * @return
	 */
	protected List<ProductModel> getAvailableProductsInOffer(final List<ProductModel> productsForCategory,
			final TransportOfferingModel transportOfferingModel)
	{
		final List<ProductModel> availableProductsInOffer = new ArrayList<>();

		for (final ProductModel productModel : productsForCategory)
		{
			try
			{
				final Long stockLevel = getTravelCartService().getAvailableStock(productModel, transportOfferingModel);
				if ((stockLevel == null || stockLevel.intValue() > 0) && !availableProductsInOffer.contains(productModel))
				{
					availableProductsInOffer.add(productModel);
				}
			}
			catch (final StockLevelNotFoundException slNotFoundExec)
			{
				LOGGER.debug("No Stock configured for Product: " + productModel.getCode() + " in TransportOffering: "
						+ transportOfferingModel.getCode(), slNotFoundExec);
			}

		}
		return availableProductsInOffer;
	}

	/**
	 * This method collects the common products across transportOfferings in case of multisectors or returns the
	 * available products if single sector.
	 *
	 * @param productsPerTransportOffering
	 * @return
	 */
	protected List<ProductModel> getCommonProducts(final Map<String, List<ProductModel>> productsPerTransportOffering)
	{

		if (productsPerTransportOffering.entrySet().stream().anyMatch(entry -> entry.getValue().isEmpty()))
		{
			return Collections.emptyList();
		}

		final List<ProductModel> qualifiedProductsInOffer = new ArrayList<ProductModel>();
		for (final Entry<String, List<ProductModel>> entry : productsPerTransportOffering.entrySet())
		{
			if (qualifiedProductsInOffer.isEmpty())
			{
				qualifiedProductsInOffer.addAll(entry.getValue());
			}
			else
			{
				final List<ProductModel> commonProducts = entry.getValue().stream().filter(qualifiedProductsInOffer::contains)
						.collect(Collectors.toList());
				qualifiedProductsInOffer.clear();
				qualifiedProductsInOffer.addAll(commonProducts);
			}
		}
		return qualifiedProductsInOffer;
	}

	public TravelPromotionsFacade getTravelPromotionsFacade()
	{
		return travelPromotionsFacade;
	}

	public void setTravelPromotionsFacade(final TravelPromotionsFacade travelPromotionsFacade)
	{
		this.travelPromotionsFacade = travelPromotionsFacade;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public TransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	public void setTransportOfferingService(final TransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	public TravelCartService getTravelCartService()
	{
		return travelCartService;
	}

	public void setTravelCartService(final TravelCartService travelCartService)
	{
		this.travelCartService = travelCartService;
	}

	public BCFTravelCategoryService getTravelCategoryService()
	{
		return travelCategoryService;
	}

	public void setTravelCategoryService(final BCFTravelCategoryService travelCategoryService)
	{
		this.travelCategoryService = travelCategoryService;
	}

}
