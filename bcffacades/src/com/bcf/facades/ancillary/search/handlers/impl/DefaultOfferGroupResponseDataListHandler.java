/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferGroupData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.commercefacades.travel.enums.CategoryOption;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.strategies.OfferSortStrategy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import com.bcf.core.services.BCFTravelCategoryService;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultOfferGroupResponseDataListHandler implements AncillarySearchResponseDataListHandler
{
	private BCFTravelCategoryService travelCategoryService;
	private Converter<CategoryModel, OfferGroupData> categoryConverter;
	private OfferSortStrategy offerSortStrategy;
	private ConfigurablePopulator<CategoryModel, OfferGroupData, CategoryOption> offerGroupDataConfiguredPopulator;
	private Collection<CategoryOption> categoryOptionList;

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		for (final OfferResponseData offerResponseData : offerResponseDataList.getOfferResponseDatas())
		{
			final List<CategoryModel> ancillaryCategories = getAllCategoriesForProducts(offerResponseData);

			final List<OfferGroupData> offerGroups = new ArrayList<>();

			for (final CategoryModel aCategory : ancillaryCategories)
			{
				final OfferGroupData anOfferGroup = new OfferGroupData();
				getCategoryConverter().convert(aCategory, anOfferGroup);
				anOfferGroup.setDescription(aCategory.getDescription());
				if (!getCategoryOptionList().isEmpty())
				{
					getOfferGroupDataConfiguredPopulator().populate(aCategory, anOfferGroup, getCategoryOptionList());
				}
				offerGroups.add(anOfferGroup);
			}

			final List<OfferGroupData> sortedOfferGroups = getOfferSortStrategy().applyStrategy(offerGroups);

			offerResponseData.setOfferGroups(sortedOfferGroups);
		}
	}

	public List<CategoryModel> getAllCategoriesForProducts(final OfferResponseData offerResponseData)
	{
		final Iterator<List<ProductModel>> iterator = offerResponseData.getProductsPerTransportOfferings().values().iterator();
		final List<ProductModel> products = new ArrayList<>();
		while (iterator.hasNext())
		{
			products.addAll(iterator.next());
		}
		return getTravelCategoryService().getAncillaryCategoriesForProducts(products);
	}

	public BCFTravelCategoryService getTravelCategoryService()
	{
		return travelCategoryService;
	}

	public void setTravelCategoryService(final BCFTravelCategoryService travelCategoryService)
	{
		this.travelCategoryService = travelCategoryService;
	}

	public Converter<CategoryModel, OfferGroupData> getCategoryConverter()
	{
		return categoryConverter;
	}

	public void setCategoryConverter(final Converter<CategoryModel, OfferGroupData> categoryConverter)
	{
		this.categoryConverter = categoryConverter;
	}

	public ConfigurablePopulator<CategoryModel, OfferGroupData, CategoryOption> getOfferGroupDataConfiguredPopulator()
	{
		return offerGroupDataConfiguredPopulator;
	}

	public void setOfferGroupDataConfiguredPopulator(
			final ConfigurablePopulator<CategoryModel, OfferGroupData, CategoryOption> offerGroupDataConfiguredPopulator)
	{
		this.offerGroupDataConfiguredPopulator = offerGroupDataConfiguredPopulator;
	}

	public Collection<CategoryOption> getCategoryOptionList()
	{
		return categoryOptionList;
	}

	public void setCategoryOptionList(final Collection<CategoryOption> categoryOptionList)
	{
		this.categoryOptionList = categoryOptionList;
	}

	public OfferSortStrategy getOfferSortStrategy()
	{
		return offerSortStrategy;
	}

	public void setOfferSortStrategy(final OfferSortStrategy offerSortStrategy)
	{
		this.offerSortStrategy = offerSortStrategy;
	}

}
