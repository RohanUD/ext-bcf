/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.ancillary.search.handlers.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferGroupData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferPricingInfoData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.travelfacades.promotion.TravelPromotionsFacade;
import de.hybris.platform.travelservices.enums.AddToCartCriteriaType;
import de.hybris.platform.travelservices.order.TravelCartService;
import de.hybris.platform.travelservices.stock.TravelCommerceStockService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.bcf.facades.ancillary.search.handlers.AncillarySearchResponseDataListHandler;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;


public class DefaultOfferGroupOfferPricingInfoResponseDataListHandler implements AncillarySearchResponseDataListHandler
{
	private static final Logger LOGGER = Logger.getLogger(DefaultOfferGroupOfferPricingInfoResponseDataListHandler.class);

	private TravelPromotionsFacade travelPromotionsFacade;
	private ProductService productService;
	private CategoryService categoryService;
	private Converter<ProductModel, ProductData> productConverter;
	private TravelCartService travelCartService;
	private TravelCommerceStockService travelCommerceStockService;

	@Override
	public void handle(final OfferRequestDataList offerRequestDataList, final OfferResponseDataList offerResponseDataList)
	{
		final boolean applyPromotion = getTravelPromotionsFacade().isCurrentUserEligibleForTravelPromotions();

		for (final OfferResponseData offerResponseData : offerResponseDataList.getOfferResponseDatas())
		{
			final List<OfferGroupData> offerGroups = getFilteredOfferGroups(offerResponseData);

			for (final OfferGroupData offerGroupData : offerGroups)
			{
				final List<ProductModel> productsForCategory = getProductService()
						.getProductsForCategory(getCategoryService().getCategoryForCode(offerGroupData.getCode()));
				createOfferPricingInfos(applyPromotion, productsForCategory, offerGroupData);
			}
		}
	}

	/**
	 * Returns the list of OfferGroups of {@link AddToCartCriteriaType} PER_BOOKING or PER_PAX
	 *
	 * @param offerResponseData
	 * @return
	 */
	protected List<OfferGroupData> getFilteredOfferGroups(final OfferResponseData offerResponseData)
	{
		return offerResponseData.getOfferGroups().stream()
				.filter(offerGroup -> offerGroup.getTravelRestriction() != null
						&& StringUtils.isNotBlank(offerGroup.getTravelRestriction().getAddToCartCriteria())
						&& (StringUtils.equalsIgnoreCase(offerGroup.getTravelRestriction().getAddToCartCriteria(),
						AddToCartCriteriaType.PER_BOOKING.getCode())
						|| StringUtils.equalsIgnoreCase(offerGroup.getTravelRestriction().getAddToCartCriteria(),
						AddToCartCriteriaType.PER_PAX.getCode())))
				.collect(Collectors.toList());
	}

	/**
	 * Creates the offerPricingInfos and set it on the offerGroupData for the given products
	 *
	 * @param applyPromotion
	 * @param productsForCategory
	 * @param offerGroupData
	 */
	protected void createOfferPricingInfos(final boolean applyPromotion, final List<ProductModel> productsForCategory,
			final OfferGroupData offerGroupData)
	{
		final List<ProductModel> qualifiedProductsInOffer = getAvailableProducts(productsForCategory, offerGroupData);

		final List<OfferPricingInfoData> offerPricingInfos = new ArrayList<OfferPricingInfoData>();
		for (final ProductModel productModel : qualifiedProductsInOffer)
		{
			final OfferPricingInfoData offerPricingInfoData = new OfferPricingInfoData();
			final ProductData productData = getProductConverter().convert(productModel);
			if (applyPromotion)
			{
				getTravelPromotionsFacade().populatePotentialPromotions(productModel, productData);
			}
			offerPricingInfoData.setProduct(productData);

			offerPricingInfos.add(offerPricingInfoData);
		}

		offerGroupData.setOfferPricingInfos(offerPricingInfos);

	}

	/**
	 * Returns the list of available products based on the stock level
	 *
	 * @param productsForCategory
	 * @param offerGroupData
	 * @return the list of available products
	 */
	protected List<ProductModel> getAvailableProducts(final List<ProductModel> productsForCategory,
			final OfferGroupData offerGroupData)
	{
		final List<ProductModel> availableProductsInOffer = new ArrayList<>();
		for (final ProductModel productModel : productsForCategory)
		{
			try
			{
				final Long stockLevel = getTravelCommerceStockService().getStockLevel(productModel, Collections.emptyList());
				if ((stockLevel == null || stockLevel.intValue() > 0) && !availableProductsInOffer.contains(productModel))
				{
					availableProductsInOffer.add(productModel);
				}
			}
			catch (final StockLevelNotFoundException slNotFoundExec)
			{
				LOGGER.debug("No Stock configured for Product: " + productModel.getCode() + " in default warehouse", slNotFoundExec);
			}
		}
		return availableProductsInOffer;
	}

	public TravelPromotionsFacade getTravelPromotionsFacade()
	{
		return travelPromotionsFacade;
	}

	public void setTravelPromotionsFacade(final TravelPromotionsFacade travelPromotionsFacade)
	{
		this.travelPromotionsFacade = travelPromotionsFacade;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	public TravelCartService getTravelCartService()
	{
		return travelCartService;
	}

	public void setTravelCartService(final TravelCartService travelCartService)
	{
		this.travelCartService = travelCartService;
	}

	public TravelCommerceStockService getTravelCommerceStockService()
	{
		return travelCommerceStockService;
	}

	public void setTravelCommerceStockService(final TravelCommerceStockService travelCommerceStockService)
	{
		this.travelCommerceStockService = travelCommerceStockService;
	}
}
