/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.thirdpartyintegration.impl;

import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.thirdpartyintegration.TripAdvisorIntegrationFacade;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultTripAdvisorIntegrationFacade implements TripAdvisorIntegrationFacade
{

	private TripAdvisorService tripAdvisorService;

	@Override
	public TripAdvisorResponseData getReviewInformation(final String locationId) throws IntegrationException
	{
		return getTripAdvisorService().getReviewInformation(locationId);
	}

	protected TripAdvisorService getTripAdvisorService()
	{
		return tripAdvisorService;
	}

	@Required
	public void setTripAdvisorService(final TripAdvisorService tripAdvisorService)
	{
		this.tripAdvisorService = tripAdvisorService;
	}
}
