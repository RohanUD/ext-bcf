/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.facades.thirdpartyintegration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.facades.accommodation.reviews.CustomerReviewData;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;


public class CustomerReviewDataPopulator implements Populator<TripAdvisorResponseData, CustomerReviewData>
{
	@Override
	public void populate(final TripAdvisorResponseData source, final CustomerReviewData target)
			throws ConversionException
	{
		if (source != null)
		{
			target.setLocationId(source.getLocationId());
			target.setName(source.getName());
			target.setRating(source.getRating());
			target.setWriteReviewUrl(source.getWriteReview());
			target.setWebUrl(source.getWebUrl());
			target.setReviewRatingCount(source.getReviewRatingCount());
			target.setRatingImageUrl(source.getRatingImageUrl());
			target.setNumOfReviews(source.getNumOfReviews());

		}
	}
}
