/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfqrcodegenerator.service.impl;


import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.bcfqrcodegenerator.processor.OrderTextDataGenerator;
import com.bcf.bcfqrcodegenerator.service.BcfQrCodeGeneratorService;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


public class DefaultBcfQrCodeGeneratorService implements BcfQrCodeGeneratorService
{

	@Resource(name = "orderTextDataGeneratorMap")
	private Map<String,OrderTextDataGenerator> orderTextDataGeneratorMap;


	@Override
	public byte[] generateQRCode(final AbstractOrderModel order,final List<AbstractOrderEntryModel> orderEntries,final OrderEntryType productType)
			throws IOException, WriterException
	{
		return generateQRByteCode(orderTextDataGeneratorMap.get(productType).generateEncodingText(order,orderEntries));
	}

	private byte[] generateQRByteCode(final String textToBeEncoded)
			throws WriterException, IOException
	{
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(textToBeEncoded, BarcodeFormat.QR_CODE, 400, 400);
	   ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
		return pngOutputStream.toByteArray();
	}

}
