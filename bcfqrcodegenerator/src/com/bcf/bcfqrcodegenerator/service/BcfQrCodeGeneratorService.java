/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfqrcodegenerator.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.List;
import com.google.zxing.WriterException;


public interface BcfQrCodeGeneratorService
{
	byte[] generateQRCode(final AbstractOrderModel order,final List<AbstractOrderEntryModel> orderEntries,final OrderEntryType productType)	throws IOException, WriterException;
}
