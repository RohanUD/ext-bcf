/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfqrcodegenerator.processor.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.ProductType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bcf.bcfqrcodegenerator.constants.BcfqrcodegeneratorConstants;
import com.bcf.bcfqrcodegenerator.processor.OrderTextDataGenerator;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;


public class TransportTextDataGenerator implements OrderTextDataGenerator
{
	@Override
	public String generateEncodingText(final AbstractOrderModel order, final List<AbstractOrderEntryModel> orderEntries)
	{
		final StringBuilder text = new StringBuilder();
		final Map<String, Integer> passengerTypeCountMap = new HashMap<>();
		final Map<String, Integer> vehicleTypeCountMap = new HashMap<>();
		final Map<String, Integer> productTypeCountMap = new HashMap<>();
		final TravelOrderEntryInfoModel travelOrderEntryInfo = orderEntries.get(0).getTravelOrderEntryInfo();
		text.append(BcfqrcodegeneratorConstants.ORDER + orderEntries.get(0).getBookingReference());
		return text.toString();
	}
}
