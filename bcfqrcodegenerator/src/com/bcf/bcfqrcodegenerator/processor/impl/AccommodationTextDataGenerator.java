/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfqrcodegenerator.processor.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import java.util.List;
import com.bcf.bcfqrcodegenerator.constants.BcfqrcodegeneratorConstants;
import com.bcf.bcfqrcodegenerator.processor.OrderTextDataGenerator;
import com.bcf.core.model.order.DealOrderEntryGroupModel;


public class AccommodationTextDataGenerator implements OrderTextDataGenerator
{
	@Override
	public String generateEncodingText(final AbstractOrderModel order, final List<AbstractOrderEntryModel> orderEntries)
	{
		final StringBuilder text = new StringBuilder();
		orderEntries.stream().filter(
				orderEntry -> (orderEntry.getProduct() instanceof RoomRateProductModel)).forEach(orderEntry -> {
			if((orderEntry.getEntryGroup() instanceof AccommodationOrderEntryGroupModel)){
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = (AccommodationOrderEntryGroupModel) orderEntry
						.getEntryGroup();
				text.append(BcfqrcodegeneratorConstants.ORDER + order.getCode() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.CHECKIN_DATE + accommodationOrderEntryGroupModel.getStartingDate() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.CHECKOUT_DATE + accommodationOrderEntryGroupModel.getEndingDate() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.ROOM_TYPE + accommodationOrderEntryGroupModel.getAccommodation().getRoomType().getCode() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.NUMBER_OF_GUESTS + accommodationOrderEntryGroupModel.getGuestCounts() + BcfqrcodegeneratorConstants.COMMA);
			}else if(orderEntry.getEntryGroup() instanceof DealOrderEntryGroupModel){
				final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) orderEntry.getEntryGroup();
				final AccommodationOrderEntryGroupModel accommodationOrderEntryGroup = dealOrderEntryGroupModel
						.getAccommodationEntryGroups().stream().findFirst().orElse(null);
				text.append(BcfqrcodegeneratorConstants.ORDER + order.getCode() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.CHECKIN_DATE + accommodationOrderEntryGroup.getCheckInTime() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.CHECKOUT_DATE + accommodationOrderEntryGroup.getEndingDate() + BcfqrcodegeneratorConstants.COMMA);
				text.append(BcfqrcodegeneratorConstants.ROOM_TYPE + accommodationOrderEntryGroup.getAccommodation().getRoomType().getCode() + BcfqrcodegeneratorConstants.COMMA);
				//TO DO: APPEND GUEST COUNT
			}
		});
		orderEntries.stream().filter(orderEntry->(orderEntry.getProduct() instanceof ExtraProductModel)).forEach(orderEntry->
			text.append(BcfqrcodegeneratorConstants.EXTRA_PRODUCTS + orderEntry.getProduct().getCode() + BcfqrcodegeneratorConstants.COMMA )
		);
		return text.toString();
	}
}
