/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfqrcodegenerator.processor.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.bcfqrcodegenerator.constants.BcfqrcodegeneratorConstants;
import com.bcf.bcfqrcodegenerator.processor.OrderTextDataGenerator;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;


public class ActivityTextDataGenerator implements OrderTextDataGenerator
{
	@Override
	public String generateEncodingText(final AbstractOrderModel order, final List<AbstractOrderEntryModel> orderEntries)
	{
		StringBuilder text = new StringBuilder();
		orderEntries.forEach(orderEntry->{
		ActivityOrderEntryInfoModel activityOrderEntryInfo = orderEntry.getActivityOrderEntryInfo();
		text.append(BcfqrcodegeneratorConstants.ORDER +order.getCode()+ BcfqrcodegeneratorConstants.COMMA);
		text.append(BcfqrcodegeneratorConstants.DATE+ activityOrderEntryInfo.getActivityDate().toString()+BcfqrcodegeneratorConstants.COMMA);
		text.append(BcfqrcodegeneratorConstants.CODE+ orderEntry.getProduct().getCode()+BcfqrcodegeneratorConstants.COMMA);
		text.append(BcfqrcodegeneratorConstants.NAME+ orderEntry.getProduct().getName()+BcfqrcodegeneratorConstants.COMMA);
		text.append(BcfqrcodegeneratorConstants.PASSENGER + orderEntry.getActivityOrderEntryInfo().getGuestType().getCode()+BcfqrcodegeneratorConstants.COMMA);
		text.append(BcfqrcodegeneratorConstants.QUANTITY + orderEntry.getQuantity());
		});
		return text.toString();
	}
}
