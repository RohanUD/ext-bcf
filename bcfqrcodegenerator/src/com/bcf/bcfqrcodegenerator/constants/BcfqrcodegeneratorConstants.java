/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfqrcodegenerator.constants;


public final class BcfqrcodegeneratorConstants extends GeneratedBcfqrcodegeneratorConstants
{
	public static final String EXTENSIONNAME = "bcfqrcodegenerator";

	private BcfqrcodegeneratorConstants()
	{
	}
	// implement here constants used by this extension
	public static final String ORDER = "Booking:";
	public static final String DEPARTURE_TERMINAL = "DepartureTerminal:";
	public static final String DEPARTURE_DATE = "DepartureDate:";
	public static final String ARRIVAL_TERMINAL = "ArrivalTerminal:";
	public static final String ARRIVAL_DATE = "ArrivalDate:";
	public static final String PASSENGER = "Passenger:";
	public static final String VEHICLE = "Vehicle:";
	public static final String ANCILLARY = "Ancillary:";
	public static final String TYPE = "Type:";
	public static final String QUANTITY = "Quantity:";
	public static final String DATE = "Date:";
	public static final String CODE = "Code:";
	public static final String NAME = "Name:";
	public static final String CHECKIN_DATE = "CheckIn Date:";
	public static final String CHECKOUT_DATE = "CheckOut Date:";
	public static final String ROOM_TYPE = "Room Type:";
	public static final String NUMBER_OF_GUESTS = "No. of Guests:";
	public static final String EXTRA_PRODUCTS = "Extra Product:";
	public static final String OPEN_CURLY_BRACE = "{";
	public static final String CLOSE_CURLY_BRACE = "}";
	public static final String COMMA = ",";

}
