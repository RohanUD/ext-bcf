/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfqrcodegenerator.facade.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.List;
import com.bcf.bcfqrcodegenerator.facade.BcfQrcodeGeneratorFacade;
import com.bcf.bcfqrcodegenerator.service.BcfQrCodeGeneratorService;
import com.google.zxing.WriterException;


public class DefaultBcfQrCodeGeneratorFacade implements BcfQrcodeGeneratorFacade
{
	private BcfQrCodeGeneratorService bcfQrCodeGeneratorService;

	@Override
	public byte[] generateQRCodeByteData(final AbstractOrderModel order,final List<AbstractOrderEntryModel> orderEntries,final OrderEntryType productType) throws IOException, WriterException
	{
		return getBcfQrCodeGeneratorService().generateQRCode(order,orderEntries,productType);
	}

	public BcfQrCodeGeneratorService getBcfQrCodeGeneratorService()
	{
		return bcfQrCodeGeneratorService;
	}

	public void setBcfQrCodeGeneratorService(final BcfQrCodeGeneratorService bcfQrCodeGeneratorService)
	{
		this.bcfQrCodeGeneratorService = bcfQrCodeGeneratorService;
	}
}
