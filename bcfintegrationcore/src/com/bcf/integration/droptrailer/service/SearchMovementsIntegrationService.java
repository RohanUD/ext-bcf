/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.droptrailer.service;

import com.bcf.integration.droptrailer.search.data.SearchMovementsResponseData;
import com.bcf.integration.droptrailer.trailermovement.data.SearchMovementsRequestData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface SearchMovementsIntegrationService
{
	SearchMovementsResponseData searchMovements(SearchMovementsRequestData searchMovementsRequestData)
			throws IntegrationException;
}
