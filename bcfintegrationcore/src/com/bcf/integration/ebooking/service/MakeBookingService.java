/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public interface MakeBookingService
{

	/**
	 * sets price for each product, booking reference and caching key in a session after fetching it from make booking
	 * service call
	 *
	 * @throws IntegrationException
	 */
	BCFMakeBookingResponse getMakeBookingResponse(AddBundleToCartRequestData requestData) throws IntegrationException;


	BCFMakeBookingResponse getMakeBookingResponse(MakeBookingRequestData makeBookingRequestData) throws IntegrationException;

}
