/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service;

import de.hybris.platform.core.model.user.CustomerModel;
import java.util.List;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface SearchBookingService
{
	/**
	 * Searches bookings of customer from eBooking
	 *
	 * @param customerModel     customer model.
	 * @param currentPageNumber The number of search result page to be retrieved. If the number is not defined, the first page will be
	 *                          returned by default.
	 */
	SearchBookingResponseDTO searchBookings(CustomerModel customerModel, Integer currentPageNumber, Boolean isUpcomingBooking,
			boolean fetchAll)
			throws IntegrationException;


	/**
	 * Searches list of bookings from eBooking for a customer.
	 *
	 * @param customerModel     customer model.
	 * @param bookingReferences list of booking references.
	 * @param currentPageNumber The number of search result page to be retrieved. If the number is not defined, the first page will be
	 *                          returned by default.
	 */
	SearchBookingResponseDTO searchBookings(CustomerModel customerModel, List<String> bookingReferencesList,
			final Integer currentPageNumber)
			throws IntegrationException;

	SearchBookingResponseDTO advanceSearchBookings(CustomerModel customerModel, Integer currentPageNumber,
			final BookingsAdvanceSearchData bookingsAdvanceSearchData) throws IntegrationException;
}
