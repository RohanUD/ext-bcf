/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.cancelbooking.order.response.CancelBookingResponseForOrder;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CancelBookingService
{

	CancelBookingResponseForCart cancelBooking(CartModel cart, List<AbstractOrderEntryModel> removeEntries)
			throws IntegrationException;

	/**
	 * Cancel bookings.
	 *
	 * @param order the order
	 * @return the cancel booking response for order
	 * @throws IntegrationException the integration exception
	 */
	CancelBookingResponseForOrder cancelBookings(OrderModel order) throws IntegrationException;

	/**
	 * Cancel bookings.
	 *
	 * @param order              the order
	 * @param removeOrderEntries the remove order entries
	 * @return the cancel booking response for order
	 * @throws IntegrationException
	 */
	CancelBookingResponseForOrder cancelBookings(OrderModel order, List<AbstractOrderEntryModel> removeOrderEntries)
			throws IntegrationException;

	void updateCancelOrder(List<OrderEntryModel> orderEntryModels);
}
