/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 7/6/19 7:46 AM
 */

package com.bcf.integration.base.service;

import java.util.List;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CurrentConditionRestService extends BaseRestService
{
	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO[] sendRestRequestWithArrayResponse(final BaseRequestDTO body, final Class<?> responseClass,
			HttpMethod httpMethod,
			String serviceURLKey, HttpHeaders requestHeaders, Map<String, List<String>> params) throws IntegrationException;

	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO[] sendRestRequestWithArrayResponse(final BaseRequestDTO body, final Class<?> responseClass,
			String serviceURLKey,
			HttpMethod httpMethod, Map<String, List<String>> params) throws IntegrationException;

	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO[] sendRestRequestForNextSailingsWithArrayResponse(final BaseRequestDTO body, final Class<?> responseClass,
			HttpMethod httpMethod,
			String serviceURLKey, HttpHeaders requestHeaders, Map<String, Object> params) throws IntegrationException;

	Map<String, Map<String, List<TerminalSchedulesRouteData>>> sendRestRequestForTerminalsWithMapResponse(
			final BaseRequestDTO body, final Class<?> responseClass,
			HttpMethod httpMethod,
			String serviceURLKey, HttpHeaders requestHeaders, Map<String, List<String>> params) throws IntegrationException;
}
