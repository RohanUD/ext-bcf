/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service;

import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BaseRestService
{
	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO sendRestRequest(final BaseRequestDTO body, final Class<?> responseClass, String serviceURLKey,
			HttpMethod httpMethod) throws IntegrationException;

	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO sendRestRequest(final BaseRequestDTO body, final Class<?> responseClass, HttpMethod httpMethod,
			String serviceURLKey, HttpHeaders requestHeaders, Map<String, Object> params) throws IntegrationException;

	/**
	 * Method to call Webservice
	 */
	BaseResponseDTO sendRestRequest(final BaseRequestDTO body, final Class<?> responseClass, String serviceURLKey,
			HttpMethod httpMethod, Map<String, Object> params) throws IntegrationException;

}
