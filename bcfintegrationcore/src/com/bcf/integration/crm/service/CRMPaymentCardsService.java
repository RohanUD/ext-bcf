/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.integration.data.CRMAddOrUpdatePaymentCardsResponseDTO;
import com.bcf.integration.data.CRMGetPaymentCardsResponseDTO;
import com.bcf.integration.data.CRMRemovePaymentCardResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CRMPaymentCardsService
{

	CRMGetPaymentCardsResponseDTO getPaymentsCards(CustomerModel uid) throws IntegrationException;

	CRMAddOrUpdatePaymentCardsResponseDTO updatePaymentCards(final CreditCardPaymentInfoModel creditCardPaymentInfoModel)
			throws IntegrationException;

	CRMRemovePaymentCardResponseDTO removePaymentCard(CustomerModel customerModel, String paymentInfoId)
			throws IntegrationException;

	CRMAddOrUpdatePaymentCardsResponseDTO updateAccountPaymentCard(CTCTCCardPaymentInfoModel ctcTcCardPaymentInfo)
			throws IntegrationException;
}
