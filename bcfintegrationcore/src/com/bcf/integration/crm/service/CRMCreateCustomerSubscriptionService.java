/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service;

import de.hybris.platform.core.model.user.CustomerModel;
import com.bcf.integration.data.CustomerSubscriptionResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CRMCreateCustomerSubscriptionService
{
	/**
	 * Creates customer's subscriptions at CRM first time.
	 *
	 * @param customer customer model with latest/updated details.
	 */
	CustomerSubscriptionResponseDTO createCustomerSubscription(CustomerModel customer) throws IntegrationException;
}
