/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service;

import java.io.UnsupportedEncodingException;
import com.bcf.facades.customer.search.SearchParams;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.CRMGetSearchCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CRMSearchCustomerService
{
	/**
	 * Searches customer from CRM on basis of user name
	 *
	 * @param userName unique user id or userName
	 */
	CRMGetCustomerResponseDTO getCustomer(String userName,boolean guestind) throws IntegrationException,
			UnsupportedEncodingException;

	/**
	 * Searches b2bcustomer from CRM on basis of firstName, lastname, companyName
	 *
	 * @param searchParams
	 */
	CRMGetSearchCustomerResponseDTO searchCustomer(final SearchParams searchParams) throws IntegrationException;
}
