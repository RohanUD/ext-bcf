/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.core.model.user.CustomerModel;
import com.bcf.integration.data.CRMCustomerEmailUpdateResponseDTO;
import com.bcf.integration.data.CRMGetCustomerAccountResponseDTO;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CRMUpdateCustomerService
{

	/**
	 * Create / update customer at CRM
	 *
	 * @param customerModel customer model with latest/updated details.
	 */
	UpdateCRMCustomerResponseDTO updateCustomer(CustomerModel customerModel) throws IntegrationException;

	UpdateCRMCustomerResponseDTO updateAccount(CustomerModel customerModel, RegisterData registerData) throws IntegrationException;

	CRMCustomerEmailUpdateResponseDTO updateEmail(CustomerModel customerModel, String newEmail) throws IntegrationException;

	CRMGetCustomerAccountResponseDTO getCustomerAccount(B2BUnitModel b2BUnitModel) throws IntegrationException;
}
