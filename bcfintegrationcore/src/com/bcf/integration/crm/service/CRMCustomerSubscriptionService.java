/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service;

import de.hybris.platform.core.model.user.CustomerModel;
import java.io.UnsupportedEncodingException;
import java.util.List;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.integration.data.CustomerSubscriptionResponseDTO;
import com.bcf.integration.data.SubscriptionCountsByRouteResponseDTO;
import com.bcf.integration.data.SubscriptionCustomerDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CRMCustomerSubscriptionService
{
	/**
	 * Create or Updates customer's subscriptions at CRM.
	 *
	 * @param customerModel
	 */
	CustomerSubscriptionResponseDTO createOrUpdateCustomerSubscriptions(final CustomerModel customerModel)
			throws IntegrationException;

	CustomerSubscriptionResponseDTO getCustomerSubscriptionsFromCRM(CustomerModel customerModel) throws IntegrationException;

	List<SubscriptionCustomerDTO> getSubscribersForRouteFromCRM(String subscriptionName) throws IntegrationException;

	List<CRMSubscriptionMasterDetailModel> getSubscriptionMasterDetailsForServiceNotice(ServiceNoticeModel serviceNotice);

	CustomerSubscriptionResponseDTO unsubscribeAll(CustomerModel currentUser)
			throws IntegrationException, UnsupportedEncodingException;
}
