/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.activedirectory.service;

import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import java.util.Map;
import com.bcf.integration.data.ADUserLoginResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface ADUserService
{

	UserModel authenticateAndUpdateADUser(String username, String password) throws IntegrationException;

	ADUserLoginResponseDTO authenticateActiveDirectoryUser(String username, String password)
			throws IntegrationException;

	EmployeeModel createUser(String username, ADUserLoginResponseDTO userResponse,
			Map<String, String> lDAPUserRoleMap);

	void updateUserRoles(EmployeeModel agent, ADUserLoginResponseDTO userResponse,
			Map<String, String> lDAPUserRoleMap);
}
