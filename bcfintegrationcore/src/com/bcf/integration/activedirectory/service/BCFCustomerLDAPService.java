/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.activedirectory.service;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import com.bcf.integration.data.ChangeEmailResponseDTO;
import com.bcf.integration.data.CreateCustomerResponseDTO;
import com.bcf.integration.data.PasswordChangeResponseDTO;
import com.bcf.integration.data.PasswordResetResponseDTO;
import com.bcf.integration.data.ValidateCustomerCredentialsResponseDTO;
import com.bcf.integration.data.VerifyCustomerResponseDTO;
import com.bcf.integration.data.VerifyPasswordResetResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface BCFCustomerLDAPService
{
	CreateCustomerResponseDTO registerAtLDAP(RegisterData registerData) throws IntegrationException;

	VerifyCustomerResponseDTO verifyAtLDAP(String accountValidationKey, String uid) throws IntegrationException;

	VerifyCustomerResponseDTO verifyEmailChangeAtLDAP(String accountValidationKey, String oldEmail,
			String newEmail)
			throws IntegrationException;

	ValidateCustomerCredentialsResponseDTO validateCredentials(String username, Object password)
			throws IntegrationException;

	PasswordResetResponseDTO resetPassword(String email) throws IntegrationException;

	VerifyPasswordResetResponseDTO verifyPasswordReset(String key, String newPassword)
			throws IntegrationException;

	PasswordChangeResponseDTO changePassword(String uid, String newPassword) throws IntegrationException;

	ChangeEmailResponseDTO changeEmail(String oldEmail, String newEmail) throws IntegrationException;
}
