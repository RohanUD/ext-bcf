/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.releasecenter.strategy;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.crm.service.CRMCustomerSubscriptionService;
import com.bcf.integration.data.SubscriptionCustomerDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public abstract class AbstractReleaseCenterStrategy implements ReleaseCenterNotificationStrategy
{
	private NotificationEngineRestService notificationEngineRestService;

	private CRMCustomerSubscriptionService crmCustomerSubscriptionService;

	private Converter<SubscriptionCustomerDTO, CustomerData> bcfReleaseCenterCustomerDataConverter;

	private Converter<ServiceNoticeModel, ReleaseCenterNotificationData> bcfServiceNoticeBaseConverter;

	abstract protected String getServiceNoticePublishURL();

	abstract protected Class getServiceNoticeNotificationDataClass();

	abstract protected ReleaseCenterNotificationData getServiceNoticeTargetInstance();

	@Override
	public void sendNotice(ServiceNoticeModel model) throws IntegrationException
	{
		final ReleaseCenterNotificationData releaseCenterNotificationData = getBcfServiceNoticeBaseConverter()
				.convert(model, getServiceNoticeTargetInstance());

		setSubscribers(releaseCenterNotificationData, model);

		getNotificationEngineRestService()
				.sendRestRequest(releaseCenterNotificationData, getServiceNoticeNotificationDataClass(),
						HttpMethod.POST, getServiceNoticePublishURL());
	}

	protected void setSubscribers(final ReleaseCenterNotificationData releaseCenterNotificationData, ServiceNoticeModel model)
			throws IntegrationException
	{
		List<SubscriptionCustomerDTO> subscribers = new ArrayList<>();

		List<CRMSubscriptionMasterDetailModel> selectedSubscriptionMasterDetails = crmCustomerSubscriptionService
				.getSubscriptionMasterDetailsForServiceNotice(model);

		for (CRMSubscriptionMasterDetailModel subscriptionMasterDetail : selectedSubscriptionMasterDetails)
		{
			List<SubscriptionCustomerDTO> list = crmCustomerSubscriptionService
					.getSubscribersForRouteFromCRM(subscriptionMasterDetail.getSubscriptionCode());

			if (CollectionUtils.isNotEmpty(list))
			{
				subscribers.addAll(list.stream().filter(Objects::nonNull).collect(Collectors.toList()));
			}
		}
		List<CustomerData> customerDataList = bcfReleaseCenterCustomerDataConverter
				.convertAll(subscribers);
		releaseCenterNotificationData.setSubscribers(getUniqueSubscribers(customerDataList));
	}

	/**
	 * gets the distinct SubscriberDetailDTO based on emails, equals method have been overrided to find uniqueness
	 *
	 * @param subscribers
	 * @return
	 */
	private List<CustomerData> getUniqueSubscribers(final List<CustomerData> subscribers)
	{
		Collection<CustomerData> collections = subscribers.stream().filter(s -> StringUtils.isNotBlank(s.getEmail()))
				.collect(Collectors.toMap(CustomerData::getEmail, e -> e, (e1, e2) -> e1)).values();

		return new ArrayList<>(collections);
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	protected Converter<ServiceNoticeModel, ReleaseCenterNotificationData> getBcfServiceNoticeBaseConverter()
	{
		return bcfServiceNoticeBaseConverter;
	}

	@Required
	public void setBcfServiceNoticeBaseConverter(
			final Converter<ServiceNoticeModel, ReleaseCenterNotificationData> bcfServiceNoticeBaseConverter)
	{
		this.bcfServiceNoticeBaseConverter = bcfServiceNoticeBaseConverter;
	}

	@Required
	public void setNotificationEngineRestService(NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	@Required
	public void setCrmCustomerSubscriptionService(
			final CRMCustomerSubscriptionService crmCustomerSubscriptionService)
	{
		this.crmCustomerSubscriptionService = crmCustomerSubscriptionService;
	}

	@Required
	public void setBcfReleaseCenterCustomerDataConverter(
			final Converter<SubscriptionCustomerDTO, CustomerData> bcfReleaseCenterCustomerDataConverter)
	{
		this.bcfReleaseCenterCustomerDataConverter = bcfReleaseCenterCustomerDataConverter;
	}
}
