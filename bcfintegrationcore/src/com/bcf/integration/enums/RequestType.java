/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.enums;

import java.util.HashMap;
import java.util.Map;


public enum RequestType
{

	MAKEBOOKING("MakeBooking"),
	UPDATEQUOTATIONEXPIRATION("UpdateQuotationExpiration"),
	CONVERTQUOTATIONTOOPTION("ConvertQuotationToOption");

	private final String stringValue;

	public String getStringValue()
	{
		return stringValue;
	}

	// Reverse-lookup map for getting a day from an abbreviation
	private static final Map<String, RequestType> LOOKUP = new HashMap<String, RequestType>();

	static {
		for (final RequestType requestType : RequestType.values()) {
			LOOKUP.put(requestType.getStringValue(), requestType);
		}
	}

	public static RequestType get(final String stringValue) {
		return LOOKUP.get(stringValue);
	}

	RequestType(final String stringValue) {
		this.stringValue = stringValue.intern();
	}
}
