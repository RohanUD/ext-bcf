/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.List;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.integration.payment.exceptions.InvalidPaymentInfoException;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public interface BcfRefundIntegrationService
{

	PaymentResponseDTO refundOrder(final AbstractOrderModel abstractOrder)
			throws IntegrationException, InvalidPaymentInfoException;

	PaymentResponseDTO processRefund(final AbstractOrderModel abstractOrder, double refundAmount)
			throws IntegrationException, InvalidPaymentInfoException;

	List<PaymentResponseDTO> processRefund(final AbstractOrderModel abstractOrder,
			List<RefundTransactionInfoData> refundTransactionInfoDatas) throws IntegrationException, InvalidPaymentInfoException;

}
