/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.service;

import com.bcf.integration.data.TokenRequestDTO;
import com.bcf.integration.data.TokenResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public interface BcfPaymentIntegrationService
{
	TokenResponseDTO getPermanentToken(TokenRequestDTO tokenRequestDTO) throws IntegrationException;

	PaymentResponseDTO makePayment(String paymentServiceEndpoint, PaymentRequestDTO paymentRequestDTO)
			throws IntegrationException;
}
