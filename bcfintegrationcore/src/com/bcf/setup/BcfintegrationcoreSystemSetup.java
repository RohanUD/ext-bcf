/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.setup;

import static com.bcf.constants.BcfintegrationcoreConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.service.BcfintegrationcoreService;


@SystemSetup(extension = BcfintegrationcoreConstants.EXTENSIONNAME)
public class BcfintegrationcoreSystemSetup
{
	private final BcfintegrationcoreService bcfintegrationcoreService;

	public BcfintegrationcoreSystemSetup(final BcfintegrationcoreService bcfintegrationcoreService)
	{
		this.bcfintegrationcoreService = bcfintegrationcoreService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		bcfintegrationcoreService.createLogo(PLATFORM_LOGO_CODE);
	}
}
