/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integrations.core.exception;

import com.bcf.integration.error.ErrorListDTO;


public class IntegrationException extends Exception
{
	private static final long serialVersionUID = 1L;
	private ErrorListDTO erorrListDTO;

	public IntegrationException(final String message)
	{
		super(message);
	}

	public IntegrationException(final String message, final ErrorListDTO errorListDTO)
	{
		super(message);
		this.erorrListDTO = errorListDTO;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public IntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
	}


	/**
	 * @param cause
	 */
	public IntegrationException(final Throwable cause)
	{
		super(cause);
	}

	public ErrorListDTO getErorrListDTO()
	{
		return erorrListDTO;
	}

}
