/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.constants;

/**
 * Global class for all Bcfintegrationcore constants. You can add global constants for your extension into this class.
 */
public final class BcfintegrationcoreConstants extends GeneratedBcfintegrationcoreConstants
{
	public static final String EXTENSIONNAME = "bcfintegrationcore";

	private BcfintegrationcoreConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
	public static final String SERVICE_UNAVAILABLE = "integration.systemerror";
	public static final String NOT_AUTHORISED = "NOT_AUTHORISED";
	public static final String INVALID_CREDENTIALS = "INVALID_CREDENTIALS";
	public static final String EXCEPTION = "EXCEPTION";
	public static final String INVALID_CLIENT_INFO = "INVALID_CLIENT_INFO";

	public static final String SERVICE_UNAVAILABLE_MESSAGE_CODE = "asm.login.service.unavailable";
	public static final String NOT_AUTHORISED_MESSAGE_CODE = "asm.login.not.allowed";

	public static final String PLATFORM_LOGO_CODE = "bcfintegrationcorePlatformLogo";

	public static final String BCF_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm";
	public static final String BCF_SAILING_DATE_TIME_PATTERN = "yyyy-MM-dd";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String DATE_PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String INTEGRATION_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

	public final static String BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER = "OPTION";
	public final static String BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER_FOR_MODIFICATION_NOT_CONFIRMED_BOOKING = "OPTION_MODIFICATION";
	public final static String BOOKING_TYPE_FOR_GUEST_CUSTOMER = "QUOTATION";
	public final static String BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER_FOR_MODIFICATION = "MODIFICATION_QUOTATION";
	public static final String BCF_CALANDER_DATE_FORMAT = "MMM d yyyy";
	public static final String HYBRIS_DATE_PATTERN_FOR_FERRY = "EEE MMM dd HH:mm:ss zzz yyyy";
	public static final String BCF_HOTEL_CALANDER_DATE_FORMAT = "dd/MM/yyyy";
	public final static String MODIFY_BOOKING_TYPE_FOR_GUEST_CUSTOMER = "MODIFICATION_QUOTATION";
	public final static String MODIFY_BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER = "OPTION_MODIFICATION";
	public static final String BOOKINGS_ADVANCE_SEARCH_DATE_FORMAT = "dd/MM/yyyy";
}
