/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.test.beans;

import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;


/**
 *
 */
public class ActionExecution
{
	private final BusinessProcessModel process;
	private final AbstractAction action;

	public ActionExecution(final BusinessProcessModel process, final AbstractAction action)
	{
		this.process = process;
		this.action = action;
	}

	public AbstractAction getAction()
	{
		return action;
	}

	public BusinessProcessModel getProcess()
	{
		return process;
	}

}
