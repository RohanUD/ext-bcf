/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.customer.populator;

import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.forget.password.ForgotPasswordData;


public class ForgotPasswordNotificationPopulator
		implements Populator<StoreFrontCustomerProcessModel, ForgotPasswordData>
{
	private ConfigurationService configurationService;


	@Override
	public void populate(final StoreFrontCustomerProcessModel source, final ForgotPasswordData target)
			throws ConversionException
	{
		final CustomerData customerData = new CustomerData();

		final CustomerModel customer = source.getCustomer();
		if (Objects.nonNull(customer))
		{
			if (StringUtils.isNotEmpty(customer.getFirstName()))
			{
				customerData.setFirstName(customer.getFirstName());
			}
			if (StringUtils.isNotEmpty(customer.getLastName()))
			{
				customerData.setLastName(customer.getLastName());
			}
			if (StringUtils.isNotEmpty(customer.getContactEmail()))
			{
				customerData.setEmail(customer.getContactEmail());
			}
			if (StringUtils.isNotEmpty(customer.getPhoneNo()))
			{
				customerData.setContactNumber(customer.getPhoneNo());
			}
		}

		target.setEventType("forgotPassword");
		final String baseSiteUrl = getConfigurationService().getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL);
		final String urlMapping = getConfigurationService().getConfiguration().getString(BcfCoreConstants.PASSWORD_RESET_URL);

		String key = "";
		if (Objects.nonNull(customer))
		{
			key = customer.getPasswordRecoveryKey();
		}
		else if (source instanceof ForgottenPasswordProcessModel)
		{
			key = ((ForgottenPasswordProcessModel) source).getToken();
		}
		target.setRequestResetPasswordUrl(baseSiteUrl+urlMapping+key);
		target.setCustomerData(customerData);
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}
@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
