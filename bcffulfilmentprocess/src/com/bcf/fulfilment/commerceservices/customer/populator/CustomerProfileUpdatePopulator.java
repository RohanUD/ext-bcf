/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.customer.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.model.UpdateProfileProcessModel;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.update.profile.UpdateProfileData;
import com.google.common.collect.Lists;


public class CustomerProfileUpdatePopulator implements Populator<UpdateProfileProcessModel, UpdateProfileData>
{

	@Override
	public void populate(final UpdateProfileProcessModel source, final UpdateProfileData target)
			throws ConversionException
	{
		final CustomerData customerData = new CustomerData();

		if (StringUtils.isNotEmpty(source.getCustomer().getFirstName()))
		{
			customerData.setFirstName(source.getCustomer().getFirstName());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getLastName()))
		{
			customerData.setLastName(source.getCustomer().getLastName());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getContactEmail()))
		{
			customerData.setEmail(source.getCustomer().getContactEmail());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getPhoneNo()))
		{
			customerData.setContactNumber(source.getCustomer().getPhoneNo());
		}

		target.setEventType("updateProfile");
		target.setUpdateProfileDataList(Lists.newArrayList(source.getUpdateProfileDataList()));
		target.setCustomerData(customerData);

	}
}
