/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.customer.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.customer.register.CustomerRegistrationData;


public class RegistrationNotificationPopulator implements Populator<CustomerModel, CustomerRegistrationData>
{
	private ConfigurationService configurationService;

	@Override
	public void populate(final CustomerModel source, final CustomerRegistrationData target) throws ConversionException
	{

		final CustomerData customerData = new CustomerData();
		if (StringUtils.isNotEmpty(source.getFirstName()))
		{
			customerData.setFirstName(source.getFirstName());
		}
		if (StringUtils.isNotEmpty(source.getLastName()))
		{
			customerData.setLastName(source.getLastName());
		}
		if (StringUtils.isNotEmpty(source.getContactEmail()))
		{
			customerData.setEmail(source.getContactEmail());
		}
		if (StringUtils.isNotEmpty(source.getPhoneNo()))
		{
			customerData.setContactNumber(source.getPhoneNo());
		}
		if(StringUtils.isNotEmpty(source.getRequestedUpdateEmail()))
		{
			customerData.setRequestedUpdateEmail(source.getRequestedUpdateEmail());
		}
		String siteUrl = getConfigurationService().getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL);
		String requestMapping = getConfigurationService().getConfiguration().getString(BcfCoreConstants.REGISTRATION_VERIFYACCOUNT_URL);
		target.setAccountConfirmationUrl(
				siteUrl+requestMapping+source.getAccountValidationKey());

		target.setCustomerData(customerData);
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}
@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
