/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.customer.populator;

import static com.bcf.core.constants.BcfCoreConstants.SAVE_QUOTE_URL;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.SaveQuoteProcessModel;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.savequote.SaveQuoteData;


public class SaveQuotePopulator implements Populator<SaveQuoteProcessModel, SaveQuoteData>
{
	private ConfigurationService configurationService;
	private UserService userService;

	@Override
	public void populate(final SaveQuoteProcessModel source, final SaveQuoteData target)
			throws ConversionException
	{
		final CustomerData customerData = new CustomerData();

		if (StringUtils.isNotEmpty(source.getCustomer().getFirstName()))
		{
			customerData.setFirstName(source.getCustomer().getFirstName());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getLastName()))
		{
			customerData.setLastName(source.getCustomer().getLastName());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getContactEmail()))
		{
			customerData.setEmail(source.getCustomer().getContactEmail());
		}
		if (StringUtils.isNotEmpty(source.getCustomer().getPhoneNo()))
		{
			customerData.setContactNumber(source.getCustomer().getPhoneNo());
		}
		if(StringUtils.isNotBlank(source.getEmailAddress()))
		{
			customerData.setEmail(source.getEmailAddress());
		}
		target.setEventType("saveQuote");
		target.setCustomerData(customerData);
		target.setQuotecode(source.getQuoteCode());
		final String siteUrl = getConfigurationService().getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL);
		final String quoteUrl =getConfigurationService().getConfiguration().getString(SAVE_QUOTE_URL);
		if (userService.isAnonymousUser(source.getCustomer()) || CustomerType.GUEST.equals(source.getCustomer().getType())){
			target.setQuoteUrl(siteUrl + "quote/"+target.getQuotecode()+"?guestBookingIdentifier="+source.getGuestBookingIdentifier());
		}else{
			target.setQuoteUrl(siteUrl + quoteUrl+target.getQuotecode());
		}
		target.setQuoteExpiryDate(source.getQuoteExpiryDate());
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}
@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
