/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.model.ShareItineraryProcessModel;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;

public class ShareItineraryNotificationsAction extends AbstractSimpleDecisionAction<ShareItineraryProcessModel> {

	@Autowired
	private NotificationEngineRestService notificationEngineRestService;
	@Autowired
	private CreateOrderNotificationPipelineManager createOrderNotificationPipelineManager;

	private Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap;

	@Override
	public Transition executeAction(final ShareItineraryProcessModel shareItineraryProcessModel)
			throws Exception {
		final OrderModel order = shareItineraryProcessModel.getOrder();
		if (Objects.isNull(order)) {
			return Transition.NOK;
		}
		final CreateOrderNotificationPipelineManager pipelineManager = getPipeLineManagerMap().get(order.getBookingJourneyType());
		final CreateOrderNotificationData createOrderContext = pipelineManager.executePipeline(order);
		final CustomerData customerData = createOrderContext.getCustomerData();
		customerData.setEmails(new ArrayList<>(shareItineraryProcessModel.getShareItineraryEmails()));
		createOrderContext.setEventType("shareItinerary");
		createOrderContext.setCustomerData(customerData);
		getNotificationEngineRestService()
				.sendRestRequest(createOrderContext, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
						BcfintegrationserviceConstants.SHARE_ITINERARY_SERVICE_URL);

		return Transition.OK;
	}

	public NotificationEngineRestService getNotificationEngineRestService() {
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService) {
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public CreateOrderNotificationPipelineManager getCreateOrderNotificationPipelineManager()
	{
		return createOrderNotificationPipelineManager;
	}

	public void setCreateOrderNotificationPipelineManager(
			final CreateOrderNotificationPipelineManager createOrderNotificationPipelineManager)
	{
		this.createOrderNotificationPipelineManager = createOrderNotificationPipelineManager;
	}

	protected Map<BookingJourneyType, CreateOrderNotificationPipelineManager> getPipeLineManagerMap()
	{
		return pipeLineManagerMap;
	}

	@Required
	public void setPipeLineManagerMap(
			final Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap)
	{
		this.pipeLineManagerMap = pipeLineManagerMap;
	}
}
