/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.action;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.FORGOT_PASSWORD_SERVICE_URL;

import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.notification.request.customer.register.CustomerRegistrationData;
import com.bcf.notification.request.forget.password.ForgotPasswordData;

public class ForgottenPasswordEmailAction extends AbstractSimpleDecisionAction<StoreFrontCustomerProcessModel> {

	private Converter<StoreFrontCustomerProcessModel, ForgotPasswordData> notificationForgotPasswordDataConverter;
	private NotificationEngineRestService notificationEngineRestService;

	@Override
	public Transition executeAction(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
			throws Exception {

		if (Objects.isNull(storeFrontCustomerProcessModel) ||
				!(storeFrontCustomerProcessModel instanceof ForgottenPasswordProcessModel) ||
				Objects.isNull(((ForgottenPasswordProcessModel) storeFrontCustomerProcessModel).getToken()))
		{
			return Transition.NOK;
		}

		final ForgotPasswordData forgotPasswordData = getNotificationForgotPasswordDataConverter()
				.convert(storeFrontCustomerProcessModel);
		getNotificationEngineRestService()
				.sendRestRequest(forgotPasswordData, CustomerRegistrationData.class, HttpMethod.POST,
						FORGOT_PASSWORD_SERVICE_URL);
		return Transition.OK;
	}

	public Converter<StoreFrontCustomerProcessModel, ForgotPasswordData> getNotificationForgotPasswordDataConverter() {
		return notificationForgotPasswordDataConverter;
	}

	public void setNotificationForgotPasswordDataConverter(
			final Converter<StoreFrontCustomerProcessModel, ForgotPasswordData> notificationForgotPasswordDataConverter) {
		this.notificationForgotPasswordDataConverter = notificationForgotPasswordDataConverter;
	}

	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService) {
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public NotificationEngineRestService getNotificationEngineRestService() {
		return notificationEngineRestService;
	}

}
