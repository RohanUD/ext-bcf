/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.action;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.CUSTOMER_PROFILE_UPDATE_URL;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.UpdateProfileProcessModel;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.notification.request.update.profile.UpdateProfileData;


public class CustomerProfileUpdateAction extends AbstractSimpleDecisionAction<UpdateProfileProcessModel>
{
	private NotificationEngineRestService notificationEngineRestService;
	private Converter<UpdateProfileProcessModel, UpdateProfileData> notificationUpdateProfileDataConverter;

	@Override
	public Transition executeAction(final UpdateProfileProcessModel updateProfileProcessModel)
			throws Exception
	{
		if (Objects.isNull(updateProfileProcessModel) || Objects
				.isNull(updateProfileProcessModel.getCustomer()))
		{
			return Transition.NOK;
		}
		final UpdateProfileData updateProfileData = getNotificationUpdateProfileDataConverter()
				.convert(updateProfileProcessModel);
		getNotificationEngineRestService()
				.sendRestRequest(updateProfileData, UpdateProfileData.class, HttpMethod.POST,
						CUSTOMER_PROFILE_UPDATE_URL);
		return Transition.OK;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}
@Required
	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public Converter<UpdateProfileProcessModel, UpdateProfileData> getNotificationUpdateProfileDataConverter()
	{
		return notificationUpdateProfileDataConverter;
	}

	public void setNotificationUpdateProfileDataConverter(
			final Converter<UpdateProfileProcessModel, UpdateProfileData> notificationUpdateProfileDataConverter)
	{
		this.notificationUpdateProfileDataConverter = notificationUpdateProfileDataConverter;
	}
}


