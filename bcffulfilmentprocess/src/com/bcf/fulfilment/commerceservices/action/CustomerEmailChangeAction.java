/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.action;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.CUSTOMER_REGISTRATION_SERVICE_URL;
import static com.bcf.integration.constants.BcfintegrationserviceConstants.UPDATE_NOTIFICATION_EMAIL_EVENT;

import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.notification.request.customer.register.CustomerRegistrationData;


public class CustomerEmailChangeAction extends AbstractSimpleDecisionAction<StoreFrontCustomerProcessModel>
{

	private Converter<CustomerModel, CustomerRegistrationData> notificationCustomerDataConverter;
	private NotificationEngineRestService notificationEngineRestService;

	@Override
	public AbstractSimpleDecisionAction.Transition executeAction(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
			throws Exception {

		if (Objects.isNull(storeFrontCustomerProcessModel) || Objects
				.isNull(storeFrontCustomerProcessModel.getCustomer())) {
			return AbstractSimpleDecisionAction.Transition.NOK;
		}
		final CustomerRegistrationData customerRegistrationData = getNotificationCustomerDataConverter()
				.convert(storeFrontCustomerProcessModel.getCustomer());
		customerRegistrationData.setEventType(UPDATE_NOTIFICATION_EMAIL_EVENT+"Email");
		getNotificationEngineRestService()
				.sendRestRequest(customerRegistrationData, CustomerRegistrationData.class, HttpMethod.POST,
						CUSTOMER_REGISTRATION_SERVICE_URL);
		return AbstractSimpleDecisionAction.Transition.OK;

	}
@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService) {
		this.notificationEngineRestService = notificationEngineRestService;
	}

	protected NotificationEngineRestService getNotificationEngineRestService() {
		return notificationEngineRestService;
	}

	protected Converter<CustomerModel, CustomerRegistrationData> getNotificationCustomerDataConverter() {
		return notificationCustomerDataConverter;
	}
@Required
	public void setNotificationCustomerDataConverter(
			final Converter<CustomerModel, CustomerRegistrationData> notificationCustomerDataConverter) {
		this.notificationCustomerDataConverter = notificationCustomerDataConverter;
	}
}

