/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilment.commerceservices.action;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.SAVE_QUOTE_NOTIFICATION_URL;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.SaveQuoteProcessModel;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.notification.request.savequote.SaveQuoteData;


public class SaveQuoteEmailAction extends AbstractSimpleDecisionAction<SaveQuoteProcessModel>
{
	private Converter<SaveQuoteProcessModel, SaveQuoteData> notificationQuoterDataConverter;
	private NotificationEngineRestService notificationEngineRestService;


	@Override
	public Transition executeAction(final SaveQuoteProcessModel saveQuoteProcessModel) throws Exception
	{
		if (Objects.isNull(saveQuoteProcessModel) || Objects.isNull(saveQuoteProcessModel.getCode()))
		{
			return Transition.NOK;
		}
		final SaveQuoteData saveQuoteData = getNotificationQuoterDataConverter().convert(saveQuoteProcessModel);
		getNotificationEngineRestService()
				.sendRestRequest(saveQuoteData, SaveQuoteData.class, HttpMethod.POST, SAVE_QUOTE_NOTIFICATION_URL);
		return Transition.OK;

	}

	protected Converter<SaveQuoteProcessModel, SaveQuoteData> getNotificationQuoterDataConverter()
	{
		return notificationQuoterDataConverter;
	}

	@Required
	public void setNotificationQuoterDataConverter(
			final Converter<SaveQuoteProcessModel, SaveQuoteData> notificationQuoterDataConverter)
	{
		this.notificationQuoterDataConverter = notificationQuoterDataConverter;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}
