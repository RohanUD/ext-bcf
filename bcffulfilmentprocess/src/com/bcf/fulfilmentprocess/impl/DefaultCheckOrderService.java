/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.travelfulfilmentprocess.impl.DefaultTravelCheckOrderService;
import com.bcf.fulfilmentprocess.CheckOrderService;


/**
 * Default implementation of {@link CheckOrderService}
 */
public class DefaultCheckOrderService extends DefaultTravelCheckOrderService implements CheckOrderService
{
	@Override
	public boolean check(final OrderModel order)
	{
		return super.check(order);
	}
}
