/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.create.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.List;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public interface CreateOrderNotificationPipelineManager
{
	/**
	 * Creates OrderPlaceNotificationData using order
	 *
	 * @param abstractOrderModel
	 * @return
	 */
	CreateOrderNotificationData executePipeline(AbstractOrderModel abstractOrderModel) throws IOException, WriterException;

	CreateOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel,
			List<AbstractOrderEntryModel> orderEntries, List<OrderEntryType> orderEntryTypes)
			throws IOException, WriterException;
}
