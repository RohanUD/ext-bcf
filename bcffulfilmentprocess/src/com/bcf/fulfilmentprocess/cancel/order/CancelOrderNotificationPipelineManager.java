/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.cancel.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.google.zxing.WriterException;


public interface CancelOrderNotificationPipelineManager
{
	CancelOrderNotificationData executePipeline(AbstractOrderModel abstractOrderModel) throws IOException, WriterException;

	CancelOrderNotificationData executePipeline(AbstractOrderModel abstractOrderModel, List<AbstractOrderEntryModel> entries) throws IOException, WriterException;

	CancelOrderNotificationData executePipeline(SearchBookingResponseDTO searchBookingResponse)
			throws IOException, WriterException, ParseException;

}
