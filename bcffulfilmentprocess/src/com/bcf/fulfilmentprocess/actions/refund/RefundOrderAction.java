/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.refund;

import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.services.strategies.BcfPaymentTransactionStrategy;
import com.bcf.core.util.BCFPaymentMethodUtil;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public class RefundOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private BcfRefundIntegrationService bcfRefundIntegrationService;
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private BcfPaymentTransactionStrategy paymentTransactionStrategy;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter;

	private static final Logger LOG = Logger.getLogger(RefundOrderAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}
		try
		{
			final PaymentResponseDTO paymentResponseDTO = getBcfRefundIntegrationService().refundOrder(order);

			final boolean responseStatus = paymentResponseDTO.getResponseStatus()
					.getBcfPaymentResponseBooleanStatus();
			final String responseCode = paymentResponseDTO.getResponseStatus().getBcfResponseCode();
			if (responseStatus && StringUtils.equals(responseCode,
					getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode")))
			{
				final CustomerModel customerModel = (CustomerModel) order.getUser();
				final PaymentTransactionType paymentTransactionType = PaymentTransactionType.REFUND;
				final TransactionDetails transactionDetails = getPaymentResponseDTOConverter()
						.convert(paymentResponseDTO);

				BCFPaymentMethodType bcfPaymentMethodType = BCFPaymentMethodUtil.getBCFPaymentMethodType(order);

				final boolean creatPaymentEntrySuccess = getPaymentTransactionStrategy().createPaymentTransactions(customerModel,
						transactionDetails, paymentTransactionType, bcfPaymentMethodType, order, MapUtils.EMPTY_SORTED_MAP, null,
						false);
				return creatPaymentEntrySuccess ? Transition.OK : Transition.NOK;
			}
			else
			{
				return Transition.NOK;
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error("Integration exception encountered for refundOrder");
			return Transition.NOK;
		}
	}



	protected Converter<PaymentResponseDTO, TransactionDetails> getPaymentResponseDTOConverter()
	{
		return paymentResponseDTOConverter;
	}

	@Required
	public void setPaymentResponseDTOConverter(
			final Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter)
	{
		this.paymentResponseDTOConverter = paymentResponseDTOConverter;
	}

	protected BcfRefundIntegrationService getBcfRefundIntegrationService()
	{
		return bcfRefundIntegrationService;
	}

	@Required
	public void setBcfRefundIntegrationService(final BcfRefundIntegrationService bcfRefundIntegrationService)
	{
		this.bcfRefundIntegrationService = bcfRefundIntegrationService;
	}

	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected BcfPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	@Required
	public void setPaymentTransactionStrategy(final BcfPaymentTransactionStrategy paymentTransactionStrategy)
	{
		this.paymentTransactionStrategy = paymentTransactionStrategy;
	}
}
