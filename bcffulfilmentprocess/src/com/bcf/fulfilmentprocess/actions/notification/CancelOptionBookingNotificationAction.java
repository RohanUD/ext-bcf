/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 9/7/19 11:43 AM
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.drools.core.util.StringUtils;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.OptionBookingProcessModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.fulfilmentprocess.cancel.order.CancelOrderNotificationPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.service.SearchBookingService;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.google.zxing.WriterException;


public class CancelOptionBookingNotificationAction extends AbstractSimpleDecisionAction<OptionBookingProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CancelOptionBookingNotificationAction.class);

	private NotificationEngineRestService notificationEngineRestService;

	private Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap;

	private BcfOrderService bcfOrderService;
	private SearchBookingService searchBookingService;
	private UserService userService;
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public Transition executeAction(final OptionBookingProcessModel optionBookingProcessModel)
			throws RetryLaterException, Exception
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("CancelOptionBookingNotificationAction --> sending order cancellation notification to customer");
		}
		AbstractOrderModel cart = optionBookingProcessModel.getCart();
		boolean isClonedCart = false;
		String clonedCartCode = StringUtils.EMPTY;
		try
		{
			if (Objects.nonNull(cart))
			{
				CancelOrderNotificationData cancelOrderNotificationData;
				if (CollectionUtils.isNotEmpty(optionBookingProcessModel.getCartEntries()))
				{
					if (OptionBookingUtil.isClonedOptionBooking(cart))
					{
						isClonedCart = true;
						clonedCartCode = cart.getCode();
						cart.setCode(cart.getCode().substring(0, cart.getCode().indexOf("_cloned")));
					}
					cancelOrderNotificationData = getCancelOrderNotification(cart, optionBookingProcessModel.getCartEntries());
				}
				else
				{
					cancelOrderNotificationData = getCancelOrderNotification(cart);
				}
				return sendCancelOrderNotification(cancelOrderNotificationData);
			}
		}
		catch (IOException | IntegrationException | WriterException e)
		{
			LOG.error("exception occurred while creating/sending cancel option booking context", e);
		}
		finally
		{
			//since the cart and its entries were cloned specifically for this action only, removing it here
			if (Objects.nonNull(cart) && isClonedCart)
			{
				cart.setCode(clonedCartCode);
				LOG.info("Removing cloned cart " + cart.getCode());
				bcfTravelCartService.removeAssociatedItemsFromCart((CartModel) cart);
				getModelService().remove(cart);
			}
		}
		return Transition.NOK;
	}

	protected CancelOrderNotificationData getCancelOrderNotification(final AbstractOrderModel order)
			throws IOException, WriterException
	{
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap()
				.get(bookingJourneyType);

		final CancelOrderNotificationData cancelOrderNotificationData = cancelOrderNotificationPipelineManager
				.executePipeline(order);
		return cancelOrderNotificationData;
	}

	private CancelOrderNotificationData getCancelOrderNotification(final AbstractOrderModel order,
			final List<AbstractOrderEntryModel> cartEntries) throws IOException, WriterException
	{
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap()
				.get(bookingJourneyType);

		final CancelOrderNotificationData cancelOrderNotificationData = cancelOrderNotificationPipelineManager
				.executePipeline(order, cartEntries);
		return cancelOrderNotificationData;
	}

	protected Transition sendCancelOrderNotification(final CancelOrderNotificationData cancelOrderNotificationData)
			throws IntegrationException
	{
		if (Objects.isNull(cancelOrderNotificationData))
		{
			return Transition.NOK;
		}
		getNotificationEngineRestService()
				.sendRestRequest(cancelOrderNotificationData, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
						BcfintegrationserviceConstants.ORDER_CANCEL_NOTIFICATION_URL);
		return Transition.OK;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public Map<BookingJourneyType, CancelOrderNotificationPipelineManager> getCancelOrderPipeLineManagerMap()
	{
		return cancelOrderPipeLineManagerMap;
	}

	public void setCancelOrderPipeLineManagerMap(
			final Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap)
	{
		this.cancelOrderPipeLineManagerMap = cancelOrderPipeLineManagerMap;
	}

	public BcfOrderService getBcfOrderService()
	{
		return bcfOrderService;
	}

	public void setBcfOrderService(final BcfOrderService bcfOrderService)
	{
		this.bcfOrderService = bcfOrderService;
	}

	public SearchBookingService getSearchBookingService()
	{
		return searchBookingService;
	}

	public void setSearchBookingService(final SearchBookingService searchBookingService)
	{
		this.searchBookingService = searchBookingService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
