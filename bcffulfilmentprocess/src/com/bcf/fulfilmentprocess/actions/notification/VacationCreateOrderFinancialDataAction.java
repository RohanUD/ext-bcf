/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.notification.request.order.financial.FinancialData;


public class VacationCreateOrderFinancialDataAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager;
	private NotificationEngineRestService notificationEngineRestService;


	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws Exception
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		if (bookingJourneyType.equals(BookingJourneyType.BOOKING_PACKAGE) || bookingJourneyType
				.equals(BookingJourneyType.BOOKING_ACTIVITY_ONLY) || bookingJourneyType.equals(BookingJourneyType.BOOKING_ALACARTE))
		{
			final FinancialData financialData = getCreateOrderFinancialDataPipelineManager().executePipeline(order);
			if (Objects.isNull(financialData))
			{
				return Transition.NOK;
			}
			getNotificationEngineRestService()
					.sendRestRequest(financialData, FinancialData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.FINANCIAL_DATA_NOTIFICATION_URL);

			order.setCreateOrderFinancialDataSent(true);
			getModelService().save(order);
			return Transition.OK;
		}
		return Transition.OK;
	}

	protected OrderFinancialDataPipelineManager getCreateOrderFinancialDataPipelineManager()
	{
		return createOrderFinancialDataPipelineManager;
	}

	@Required
	public void setCreateOrderFinancialDataPipelineManager(
			final OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager)
	{
		this.createOrderFinancialDataPipelineManager = createOrderFinancialDataPipelineManager;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}
