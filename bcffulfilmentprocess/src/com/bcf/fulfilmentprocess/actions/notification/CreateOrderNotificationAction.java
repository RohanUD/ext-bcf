/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public class CreateOrderNotificationAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CreateOrderNotificationAction.class);
	private NotificationEngineRestService notificationEngineRestService;
	private Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap;

	@Override
	public String execute(final OrderProcessModel orderProcessModel) throws Exception
	{
		return executeAction(orderProcessModel).toString();
	}

	public Set<String> getTransitions() {
		return CreateOrderNotificationAction.Transition.getStringValues();
	}

	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}
		if(OrderStatus.REJECTED_BY_MERCHANT.equals(order.getStatus()))
		{
			return Transition.REJECT;
		}
		LOG.debug("CreateOrderNotificationAction for order --> "+order.getCode());
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CreateOrderNotificationPipelineManager pipelineManager = getPipeLineManagerMap().get(bookingJourneyType);
		try
		{
			final CreateOrderNotificationData createOrderContext = pipelineManager.executePipeline(order);
			getNotificationEngineRestService()
					.sendRestRequest(createOrderContext, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.ORDER_CREATE_NOTIFICATION_URL);

			order.setCreateOrderNotificationSent(true);
			getModelService().save(order);
		}
		catch (final IOException | WriterException | IntegrationException e)
		{
			LOG.error(e);
			return Transition.NOK;
		}
		return Transition.OK;
	}

	public enum Transition
	{
		OK,
		NOK,
		REJECT;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet();
			Transition[] transitions = values();
			int len = transitions.length;

			for(int i = 0; i < len; ++i)
			{
				Transition t = transitions[i];
				res.add(t.toString());
			}

			return res;
		}
	}

	protected void sendOnRequestRejectionNotification(final OrderModel order)
	{
		//TODO
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	protected Map<BookingJourneyType, CreateOrderNotificationPipelineManager> getPipeLineManagerMap()
	{
		return pipeLineManagerMap;
	}

	@Required
	public void setPipeLineManagerMap(
			final Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap)
	{
		this.pipeLineManagerMap = pipeLineManagerMap;
	}
}
