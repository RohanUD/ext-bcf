/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.order.BcfOrderService;
import com.bcf.fulfilmentprocess.cancel.order.CancelOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.util.CustomerUtils;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integration.ebooking.service.SearchBookingService;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.google.zxing.WriterException;


public class CancelOrderNotificationAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CancelOrderNotificationAction.class);

	private NotificationEngineRestService notificationEngineRestService;
	private Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap;
	private BcfOrderService bcfOrderService;
	private SearchBookingService searchBookingService;
	private UserService userService;

	private static final String STRING_TOKENIZER = "-";

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("CancelOrderNotificationAction --> sending order cancellation notification to customer");
		}
		OrderModel order = orderProcessModel.getOrder();
		try
		{
			if (Objects.isNull(order))
			{
				if (StringUtils.isBlank(orderProcessModel.getBookingReferences()))
				{
					return Transition.NOK;
				}
					final String bookingReferences = orderProcessModel.getBookingReferences();
					final String[] eBookingReferenceList = bookingReferences.split(STRING_TOKENIZER);
					for (final String bookingRefernce : eBookingReferenceList)
					{
						order = (OrderModel) getBcfOrderService().getOrderByEBookingCode(bookingRefernce);
						if (Objects.nonNull(order))
						{
							final CancelOrderNotificationData cancelOrderNotificationData = getCancelOrderNotification(order,
									bookingRefernce);
							return sendCancelOrderNotification(cancelOrderNotificationData);
						}else{

							final CustomerModel customer = (CustomerModel) orderProcessModel.getUser();
							final SearchBookingResponseDTO searchBookingResponse = getSearchBookingService()
									.searchBookings(customer, Arrays.asList(bookingRefernce), 1);
							LOG.info(String.format(
									"Order with eBooking reference [%s] is not present in hybris, creating cancelOrderNotificationData from search booking response",
									bookingReferences));
							final CancelOrderNotificationData cancelOrderNotificationData = getCancelOrderNotification(
									searchBookingResponse, customer);
							return sendCancelOrderNotification(cancelOrderNotificationData);
							}
					}

			}
			else
			{
				final CancelOrderNotificationData cancelOrderNotificationData = getCancelOrderNotification(order);
				return sendCancelOrderNotification(cancelOrderNotificationData);
			}
		}
		catch (ParseException | IOException | IntegrationException | WriterException e)
		{
			LOG.error("exception occured while creting cancel order context", e);
		}
		return Transition.NOK;
	}

	protected CancelOrderNotificationData getCancelOrderNotification(final SearchBookingResponseDTO searchBookingResponse, final CustomerModel customer)
			throws IOException, WriterException, ParseException
	{
		final BookingJourneyType bookingJourneyType = BookingJourneyType.BOOKING_TRANSPORT_ONLY;
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap().get(bookingJourneyType);

		final CancelOrderNotificationData cancelOrderNotificationData = cancelOrderNotificationPipelineManager.executePipeline(searchBookingResponse);

		populateCustomerData(customer, cancelOrderNotificationData);
		return cancelOrderNotificationData;
	}

	protected void populateCustomerData(final UserModel userModel, final CancelOrderNotificationData cancelOrderContext)
	{
		final CustomerData customerData = new CustomerData();
		final CustomerModel customerModel = (CustomerModel) userModel;
		CustomerUtils.populateCustomerData(null, customerModel, customerData);
		cancelOrderContext.setCustomerData(customerData);
	}

	protected CancelOrderNotificationData getCancelOrderNotification(final OrderModel order) throws IOException, WriterException
	{
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap().get(bookingJourneyType);

		final CancelOrderNotificationData cancelOrderNotificationData = cancelOrderNotificationPipelineManager.executePipeline(order);
		return cancelOrderNotificationData;
	}

	protected CancelOrderNotificationData getCancelOrderNotification(final OrderModel order, final String bookingReference) throws IOException, WriterException
	{
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final List<AbstractOrderEntryModel> matchingEntries = order.getEntries().stream().filter(entry -> bookingReference.equals(entry
		.getBookingReference())).collect(Collectors.toList());
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap().get(bookingJourneyType);

		final CancelOrderNotificationData cancelOrderNotificationData = cancelOrderNotificationPipelineManager.executePipeline(order, matchingEntries);
		return cancelOrderNotificationData;
	}

	protected Transition sendCancelOrderNotification(final CancelOrderNotificationData cancelOrderNotificationData) throws IntegrationException
	{
		if(Objects.isNull(cancelOrderNotificationData))
		{
			return Transition.NOK;
		}
		getNotificationEngineRestService()
				.sendRestRequest(cancelOrderNotificationData, OrderPlaceNotificationResponseData.class, HttpMethod.POST, BcfintegrationserviceConstants.ORDER_CANCEL_NOTIFICATION_URL);
		return Transition.OK;
	}

	protected NotificationEngineRestService getNotificationEngineRestService() {
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService) {
		this.notificationEngineRestService = notificationEngineRestService;
	}

	protected Map<BookingJourneyType, CancelOrderNotificationPipelineManager> getCancelOrderPipeLineManagerMap()
	{
		return cancelOrderPipeLineManagerMap;
	}

	@Required
	public void setCancelOrderPipeLineManagerMap(
			final Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap)
	{
		this.cancelOrderPipeLineManagerMap = cancelOrderPipeLineManagerMap;
	}

	protected BcfOrderService getBcfOrderService()
	{
		return bcfOrderService;
	}

	@Required
	public void setBcfOrderService(final BcfOrderService bcfOrderService)
	{
		this.bcfOrderService = bcfOrderService;
	}

	/**
	 * @return the searchBookingService
	 */
	protected SearchBookingService getSearchBookingService()
	{
		return searchBookingService;
	}

	/**
	 * @param searchBookingService the searchBookingService to set
	 */
	@Required
	public void setSearchBookingService(final SearchBookingService searchBookingService)
	{
		this.searchBookingService = searchBookingService;
	}

	protected UserService getUserService() {
		return this.userService;
	}

	@Required
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
