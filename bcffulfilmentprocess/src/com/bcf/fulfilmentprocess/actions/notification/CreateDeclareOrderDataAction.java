/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.DeclareOrderEntryModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.base.service.NotificationEngineRestService;


public class CreateDeclareOrderDataAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private NotificationEngineRestService notificationEngineRestService;
	private ModelService modelService;
	private BcfOrderService orderService;
	private BCFTravelCartService bcfTravelCartService;

	private static final Logger LOG = Logger.getLogger(CreateDeclareOrderDataAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws Exception
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}

		final Map<String, List<AbstractOrderEntryModel>> entriesSplit = getEntriesByTypeTotal(order);

		final Map<String, Double> declareOrderDataByType = new HashMap<String, Double>();

		for (String entryType : entriesSplit.keySet())
		{
			final List<AbstractOrderEntryModel> entriesPerType = entriesSplit.get(entryType);
			for (AbstractOrderEntryModel entry : entriesPerType)
			{
				Double entryTotalPrice = entry.getTotalPrice();
				if (declareOrderDataByType.containsKey(entryType))
				{
					Double totalPriceByEntryType = declareOrderDataByType.get(entryType);

					if (AmendStatus.NEW.equals(entry.getAmendStatus()))
					{
						entryTotalPrice = totalPriceByEntryType + entryTotalPrice;
					}
					else if (AmendStatus.CHANGED.equals(entry.getAmendStatus()))
					{
						entryTotalPrice = totalPriceByEntryType - entryTotalPrice;
					}
				}
				else
				{
					if (AmendStatus.CHANGED.equals(entry.getAmendStatus()))
					{
						entryTotalPrice = -entryTotalPrice;
					}
				}
				declareOrderDataByType.put(entryType, entryTotalPrice);
			}
		}

		Transaction.current().execute(new TransactionBody()
		{
			public Object execute() throws Exception
			{
				List<ItemModel> itemsToSave = new ArrayList<>();

				entriesSplit.values().stream().forEach(abstractOrderEntryModels -> {
					abstractOrderEntryModels.stream().filter(abstractOrderEntryModel -> !abstractOrderEntryModel.isWebDeclared())
							.forEach(abstractOrderEntryModel -> {
								abstractOrderEntryModel.setWebDeclared(true);
								itemsToSave.add(abstractOrderEntryModel);
							});
				});

				for (String entryType : declareOrderDataByType.keySet())
				{
					DeclareOrderEntryModel declareOrderEntryModel = modelService.create(DeclareOrderEntryModel.class);
					declareOrderEntryModel.setComponentType(entryType);
					declareOrderEntryModel.setTotalPrice(declareOrderDataByType.get(entryType));
					declareOrderEntryModel.setAgent(order.getAgent());
					declareOrderEntryModel.setSalesApplication(order.getSalesApplication());
					declareOrderEntryModel.setOrderCode(order.getCode());
					itemsToSave.add(declareOrderEntryModel);
				}

				modelService.saveAll(itemsToSave);

				return Transition.OK;

			}
		});

		return Transition.OK;
	}

	private Map<String, List<AbstractOrderEntryModel>> getEntriesByTypeTotal(OrderModel order)
	{
		Map<OrderEntryType, List<AbstractOrderEntryModel>> entriesByType;
		if (getOrderService().isAmendOrderProcess(order))
		{
			entriesByType = orderService.getDeltaOrderEntries(order);
		}
		else
		{
			entriesByType = bcfTravelCartService.splitCartEntriesByType(order);
		}
		final Map<String, List<AbstractOrderEntryModel>> entriesSplit = new HashMap<String, List<AbstractOrderEntryModel>>();

		for (OrderEntryType entryType : entriesByType.keySet())
		{
			if (OrderEntryType.TRANSPORT.equals(entryType))
			{
				populateFerryEntries(entriesByType, entriesSplit, entryType);
			}
			else if (OrderEntryType.FEE.equals(entryType))
			{
				populateFeeEntries(entriesByType, entriesSplit, entryType);
			}
			else
			{
				entriesSplit.put(entryType.getCode(), entriesByType.get(entryType));
			}
		}
		return entriesSplit;
	}

	private void populateFerryEntries(final Map<OrderEntryType, List<AbstractOrderEntryModel>> entriesByType,
			final Map<String, List<AbstractOrderEntryModel>> entriesSplit, final OrderEntryType entryType)
	{
		List<AbstractOrderEntryModel> reservedSailingEntries = new ArrayList<AbstractOrderEntryModel>();
		List<AbstractOrderEntryModel> openTicketEntries = new ArrayList<AbstractOrderEntryModel>();
		final List<AbstractOrderEntryModel> entriesPerType = entriesByType.get(entryType);
		for (AbstractOrderEntryModel abstractOrderEntryModel : entriesPerType)
		{
			if (abstractOrderEntryModel.getTravelOrderEntryInfo().isOpenTicket())
			{
				openTicketEntries.add(abstractOrderEntryModel);
			}
			else
			{
				reservedSailingEntries.add(abstractOrderEntryModel);
			}
		}
		entriesSplit.put(BcfCoreConstants.OPEN_TICKET, openTicketEntries);
		entriesSplit.put(BcfCoreConstants.RESERVERD_SAILING, reservedSailingEntries);
	}

	private void populateFeeEntries(final Map<OrderEntryType, List<AbstractOrderEntryModel>> entriesByType,
			final Map<String, List<AbstractOrderEntryModel>> entriesSplit, final OrderEntryType entryType)
	{
		final Map<String, List<AbstractOrderEntryModel>> feeEntries = new HashMap<String, List<AbstractOrderEntryModel>>();

		final List<AbstractOrderEntryModel> entriesPerType = entriesByType.get(entryType);
		for (AbstractOrderEntryModel abstractOrderEntryModel : entriesPerType)
		{
			String entryProductCode = OrderEntryType.FEE.getCode();
			if (Objects.nonNull(abstractOrderEntryModel.getProduct()))
			{
				entryProductCode = abstractOrderEntryModel.getProduct().getCode().toLowerCase();
			}

			List<AbstractOrderEntryModel> existingFeeModels = new ArrayList<AbstractOrderEntryModel>();
			if (feeEntries.containsKey(entryProductCode))
			{
				existingFeeModels = feeEntries.get(entryProductCode);
			}

			existingFeeModels.add(abstractOrderEntryModel);
			feeEntries.put(entryProductCode, existingFeeModels);
		}
		entriesSplit.putAll(feeEntries);
	}

	public static Logger getLOG()
	{
		return LOG;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	@Override
	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BcfOrderService getOrderService()
	{
		return orderService;
	}

	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
