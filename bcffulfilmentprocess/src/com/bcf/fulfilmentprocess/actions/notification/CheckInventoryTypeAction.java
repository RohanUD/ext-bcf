/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelCommerceStockService;


public class CheckInventoryTypeAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CheckInventoryTypeAction.class);
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private CatalogVersionService catalogVersionService;
	private String PRODUCTCATALOG = "bcfProductCatalog";

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{

		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}

		if (OrderStatus.APPROVED_BY_MERCHANT.equals(order.getStatus()))
		{
			return Transition.OK;
		}

		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion( PRODUCTCATALOG, CatalogManager.ONLINE_VERSION);
		catalogVersionService.setSessionCatalogVersions( Collections.singletonList( catalogVersionModel ) );

		LOG.debug("CheckInventoryTypeAction --> sending order confirmation to customer");
		final List<AbstractOrderEntryModel> entries = order.getEntries().stream()
				.filter(entry -> entry.getActive() && (Objects.equals(OrderEntryType.ACCOMMODATION, entry.getType())
						|| Objects.equals(OrderEntryType.ACTIVITY, entry.getType())))
				.collect(Collectors.toList());
		for (final AbstractOrderEntryModel entry : entries)
		{
			if (!entry.isStockAllocated())
			{
				LOG.info(String.format("Sending NOK for order [%s] since one of the entries have on_request item and order is not approved.",order.getCode(),order));
				order.setStatus(OrderStatus.PENDING_APPROVAL_FROM_MERCHANT);
				getModelService().save(order);
				return Transition.NOK;
			}
		}
		return Transition.OK;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
