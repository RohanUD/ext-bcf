/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 4/7/19 1:08 PM
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.OptionBookingProcessModel;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public class OptionBookingNotificationAction extends AbstractSimpleDecisionAction<OptionBookingProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CreateOrderNotificationAction.class);

	private NotificationEngineRestService notificationEngineRestService;

	private Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap;

	@Override
	public Transition executeAction(final OptionBookingProcessModel optionBookingProcessModel)
			throws RetryLaterException, Exception
	{
		AbstractOrderModel cartModel = optionBookingProcessModel.getCart();

		if (Objects.isNull(cartModel))
		{
			LOG.error("Error in OptionBookingNotificationAction. No cart available!");
			return Transition.NOK;
		}
		LOG.debug("Starting OptionBookingNotificationAction for cart --> " + cartModel.getCode());
		final BookingJourneyType bookingJourneyType = cartModel.getBookingJourneyType();
		final CreateOrderNotificationPipelineManager pipelineManager = getPipeLineManagerMap().get(bookingJourneyType);
		try
		{

			CreateOrderNotificationData createOptionBookingContext;
			if (CollectionUtils.isNotEmpty(optionBookingProcessModel.getCartEntries()))
			{
				createOptionBookingContext = pipelineManager
						.executePipeline(cartModel, optionBookingProcessModel.getCartEntries(), null);
			}
			else
			{
				createOptionBookingContext = pipelineManager.executePipeline(cartModel);
			}

			createOptionBookingContext.setOptionBooking(Boolean.TRUE);
			if (OptionBookingUtil.wasOptionBookingAtAnyTime(cartModel))
			{
				createOptionBookingContext.setCreatedFromOptionBooking(((CartModel) cartModel).isAlreadyCreatedFromOptionBooking());
			}
			getNotificationEngineRestService()
					.sendRestRequest(createOptionBookingContext, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.ORDER_CREATE_NOTIFICATION_URL);
		}
		catch (final IOException | WriterException | IntegrationException e)
		{
			LOG.error("Error in OptionBookingNotificationAction for cart --> " + cartModel.getCode(), e);
			return Transition.NOK;
		}
		LOG.debug("Finished OptionBookingNotificationAction for cart --> " + cartModel.getCode());
		return Transition.OK;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public Map<BookingJourneyType, CreateOrderNotificationPipelineManager> getPipeLineManagerMap()
	{
		return pipeLineManagerMap;
	}

	@Required
	public void setPipeLineManagerMap(
			final Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap)
	{
		this.pipeLineManagerMap = pipeLineManagerMap;
	}
}
