/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.modify.order.ModifyOrderNotificationPipelineManager;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.modify.ModifyOrderNotificationData;
import com.google.zxing.WriterException;


public class ModifyOrderNotificationAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(ModifyOrderNotificationAction.class);

	private NotificationEngineRestService notificationEngineRestService;
	private Map<BookingJourneyType, CreateOrderNotificationPipelineManager> modifyOrderPipeLineManagerMap;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}
		LOG.info(String.format("Preparing amend order notification context for order [%s].", order.getCode()));

		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CreateOrderNotificationPipelineManager modifyOrderNotificationPipelineManager = getModifyOrderPipeLineManagerMap()
				.get(bookingJourneyType);

		List<AbstractOrderEntryModel> validEntries = StreamUtil.safeStream(order.getEntries()).filter(
				AbstractOrderEntryModel::getActive)
				.collect(Collectors.toList());
		try
		{
			final CreateOrderNotificationData modifyOrderNotificationData = modifyOrderNotificationPipelineManager
					.executePipeline(order, validEntries, null);
			modifyOrderNotificationData.setEventType("modifyOrder_" + order.getBookingJourneyType().getCode());
			if (Objects.isNull(modifyOrderNotificationData))
			{
				LOG.info("Returning NOK.Context is null");
				return Transition.NOK;
			}
			LOG.info(String.format("Sending amend order notification for order [%s]", order.getCode()));
			getNotificationEngineRestService()
					.sendRestRequest(modifyOrderNotificationData, CreateOrderNotificationData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.ORDER_MODIFY_NOTIFICATION_URL);
		}
		catch (final IntegrationException | IOException | WriterException e)
		{
			return Transition.NOK;
		}
		return Transition.OK;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public Map<BookingJourneyType, CreateOrderNotificationPipelineManager> getModifyOrderPipeLineManagerMap()
	{
		return modifyOrderPipeLineManagerMap;
	}

	@Required
	public void setModifyOrderPipeLineManagerMap(
			final Map<BookingJourneyType, CreateOrderNotificationPipelineManager> modifyOrderPipeLineManagerMap)
	{
		this.modifyOrderPipeLineManagerMap = modifyOrderPipeLineManagerMap;
	}
}
