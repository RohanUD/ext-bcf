package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.AbstractOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.accommodation.AccommodationProviderModel;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;


public class SaveVacationOrderDetailsAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{

	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;

	private BcfOrderService orderService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}

		Transaction.current().execute(new TransactionBody()
		{
			@Override
			public Object execute() throws Exception
			{

				final Date vacationDepartureDate = changeAndCancellationFeeCalculationHelper
						.getFirstTravelDate(order.getEntries(), null);
				order.setVacationDepartureDate(vacationDepartureDate);

				final AbstractOrderEntryModel accommodationOrderEntryModel =
						order.getEntries().stream().filter(entry -> entry.getType().equals(OrderEntryType.ACCOMMODATION)).findFirst()
								.orElse(null);
				if (Objects.nonNull(accommodationOrderEntryModel) && Objects
						.nonNull(accommodationOrderEntryModel.getAccommodationOrderEntryInfo()))
				{
					final AbstractOrderEntryGroupModel abstractOrderEntryGroupModel = accommodationOrderEntryModel.getEntryGroup();
					if (Objects.nonNull(abstractOrderEntryGroupModel)
							&& (abstractOrderEntryGroupModel instanceof DealOrderEntryGroupModel))
					{
						final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) abstractOrderEntryGroupModel;
						final Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = dealOrderEntryGroupModel
								.getAccommodationEntryGroups();
						if (Objects.nonNull(accommodationOrderEntryGroupModels))
						{
							final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = accommodationOrderEntryGroupModels
									.stream().findFirst().orElse(null);
							if (Objects.nonNull(accommodationOrderEntryGroupModel) && Objects
									.nonNull(accommodationOrderEntryGroupModel.getAccommodationOffering())
									&& Objects.nonNull(accommodationOrderEntryGroupModel.getAccommodationOffering().getProvider()))
							{
								final AccommodationProviderModel accommodationProviderModel = accommodationOrderEntryGroupModel
										.getAccommodationOffering().getProvider();
								order.setVacationSupplier(accommodationProviderModel.getCode());
							}
						}
					}
				}

				modelService.save(order);

				return Transition.OK;

			}
		});

		return Transition.OK;
	}

	public BcfOrderService getOrderService()
	{
		return orderService;
	}

	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	public ChangeAndCancellationFeeCalculationHelper getChangeAndCancellationFeeCalculationHelper()
	{
		return changeAndCancellationFeeCalculationHelper;
	}

	public void setChangeAndCancellationFeeCalculationHelper(
			final ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper)
	{
		this.changeAndCancellationFeeCalculationHelper = changeAndCancellationFeeCalculationHelper;
	}
}
