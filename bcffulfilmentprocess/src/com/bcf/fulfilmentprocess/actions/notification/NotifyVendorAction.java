/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.cancel.order.CancelOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public class NotifyVendorAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(NotifyVendorAction.class);

	private NotificationEngineRestService notificationEngineRestService;
	private Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap;
	private Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (!Objects.equals(OrderStatus.REJECTED_BY_MERCHANT, order.getStatus()))
		{
			LOG.debug("NotifyVendorAction --> sending order confirmation to vendor");

			//get the entries that are deleted and create and send notification
			createAndSendVendorNotificationForCancelledEntries(order);

			//get the entries that are newly added and create and send notification
			createAndSendVendorNotificationForNewEntries(order);
		}
		return Transition.OK;
	}

	private void createAndSendVendorNotificationForNewEntries(final OrderModel order)
	{
		List<AbstractOrderEntryModel> newEntries = StreamUtil.safeStream(order.getEntries())
				.filter(e -> e.getActive() && AmendStatus.NEW.equals(e.getAmendStatus()) && !OrderEntryType.FEE.equals(e.getType()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(newEntries))
		{
			LOG.debug("No entry deleted. Not sending any cancellation notifications");
			return;
		}
		LOG.info(String.format("Found %d new entries during amendment. Sending vendor notifications to respective vendors",
				newEntries.size()));
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CreateOrderNotificationPipelineManager pipelineManager = getPipeLineManagerMap().get(bookingJourneyType);
		try
		{
			final CreateOrderNotificationData createOrderContext = pipelineManager.executePipeline(order, newEntries, null);
			getNotificationEngineRestService()
					.sendRestRequest(createOrderContext, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.ORDER_MODIFY_NOTIFY_VENDOR_NOTIFICATION_URL);

			createOrderContext.setEventType("modifyOrderVendor");
		}
		catch (final IOException | WriterException | IntegrationException e)
		{
			LOG.error(e);
		}
	}

	private void createAndSendVendorNotificationForCancelledEntries(final OrderModel order)
	{
		List<AbstractOrderEntryModel> deletedEntries = StreamUtil.safeStream(order.getEntries())
				.filter(e -> !e.getActive() && AmendStatus.CHANGED.equals(e.getAmendStatus()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(deletedEntries))
		{
			LOG.debug("No entry deleted. Not sending any cancellation notifications");
			return;
		}
		LOG.info(String.format(
				"Found %d entries that is/are deleted during amendment. Sending cancellation notifications to respective vendors",
				deletedEntries.size()));
		try
		{
			CancelOrderNotificationData cancelOrderNotification = getCancelOrderNotification(order, deletedEntries);
			sendCancelOrderNotification(cancelOrderNotification);
		}
		catch (final IOException | WriterException | IntegrationException e)
		{
			LOG.error("Error occurred while creating/sending cancel booking context", e);
		}
	}

	private CancelOrderNotificationData getCancelOrderNotification(final AbstractOrderModel order,
			final List<AbstractOrderEntryModel> cartEntries) throws IOException, WriterException
	{
		final BookingJourneyType bookingJourneyType = order.getBookingJourneyType();
		final CancelOrderNotificationPipelineManager cancelOrderNotificationPipelineManager = getCancelOrderPipeLineManagerMap()
				.get(bookingJourneyType);

		return cancelOrderNotificationPipelineManager.executePipeline(order, cartEntries);
	}

	protected void sendCancelOrderNotification(final CancelOrderNotificationData cancelOrderNotificationData)
			throws IntegrationException
	{
		if (Objects.isNull(cancelOrderNotificationData))
		{
			return;
		}
		getNotificationEngineRestService()
				.sendRestRequest(cancelOrderNotificationData, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
						BcfintegrationserviceConstants.ORDER_CANCEL_NOTIFICATION_URL);
	}

	public Map<BookingJourneyType, CreateOrderNotificationPipelineManager> getPipeLineManagerMap()
	{
		return pipeLineManagerMap;
	}

	public void setPipeLineManagerMap(
			final Map<BookingJourneyType, CreateOrderNotificationPipelineManager> pipeLineManagerMap)
	{
		this.pipeLineManagerMap = pipeLineManagerMap;
	}

	public NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	public void setNotificationEngineRestService(
			final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}

	public Map<BookingJourneyType, CancelOrderNotificationPipelineManager> getCancelOrderPipeLineManagerMap()
	{
		return cancelOrderPipeLineManagerMap;
	}

	public void setCancelOrderPipeLineManagerMap(
			final Map<BookingJourneyType, CancelOrderNotificationPipelineManager> cancelOrderPipeLineManagerMap)
	{
		this.cancelOrderPipeLineManagerMap = cancelOrderPipeLineManagerMap;
	}
}
