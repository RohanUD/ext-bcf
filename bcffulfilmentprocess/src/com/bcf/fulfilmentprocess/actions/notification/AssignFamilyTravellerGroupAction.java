/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.GuestType;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.fulfilmentprocess.constants.BcfFulfilmentProcessConstants;


public class AssignFamilyTravellerGroupAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private UserService userService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.nonNull(order) && isChildExistsInOrder(order))
			{
				final UserModel user = order.getUser();
				if (((CustomerModel) user).getType().equals(CustomerType.REGISTERED) && !user.getGroups().stream()
						.anyMatch(group -> StringUtils.equals(group.getUid(), BcfFulfilmentProcessConstants.FAMILY_TRAVELLER)))
				{
					final UserGroupModel userGroupModel = getUserService()
							.getUserGroupForUID(BcfFulfilmentProcessConstants.FAMILY_TRAVELLER);
					final Set<PrincipalGroupModel> principalGroupModelSet = new HashSet<>(user.getGroups());
					principalGroupModelSet.add(userGroupModel);
					user.setGroups(principalGroupModelSet);
					getModelService().save(user);
				}
				return Transition.OK;
			}
		return Transition.OK;
	}

	private boolean isChildExistsInOrder(final OrderModel order)
	{
		return isChildPassengerExistsForTransportPart(order) || isChildGuestsExistsForActivityPart(order)
				|| isChildGuestExistsForAccommodationPart(order);
	}

	protected boolean isChildPassengerExistsForTransportPart(final OrderModel order)
	{
		return order.getEntries().stream()
				.filter(entry -> entry.getActive() && Objects.equals(OrderEntryType.TRANSPORT, entry.getType()) && Objects
						.nonNull(entry.getTravelOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getTravelOrderEntryInfo)
				.flatMap(travelOrderEntryInfoModel -> travelOrderEntryInfoModel.getTravellers().stream()).distinct()
				.map(travellerModel -> travellerModel.getInfo())
				.filter(PassengerInformationModel.class::isInstance).map(PassengerInformationModel.class::cast)
				.anyMatch(passengerInformationModel -> StringUtils
						.equals(BcfFulfilmentProcessConstants.CHILD, passengerInformationModel.getPassengerType().getCode()));
	}

	protected boolean isChildGuestsExistsForActivityPart(final OrderModel order)
	{
		return order.getEntries().stream()
				.filter(entry -> entry.getActive() && Objects.equals(OrderEntryType.ACTIVITY, entry.getType()) && Objects
						.nonNull(entry.getActivityOrderEntryInfo()))
				.map(AbstractOrderEntryModel::getActivityOrderEntryInfo).map(ActivityOrderEntryInfoModel::getGuestType)
				.anyMatch(guestType -> Objects.equals(GuestType.CHILD, guestType));
	}

	protected boolean isChildGuestExistsForAccommodationPart(final OrderModel order)
	{
		final List<AbstractOrderEntryModel> accommodationEntries = order.getEntries().stream()
				.filter(entry -> entry.getActive() && Objects.equals(OrderEntryType.ACCOMMODATION, entry.getType())).collect(Collectors.toList());

		if (CollectionUtils.isEmpty(accommodationEntries))
		{
			return false;
		}

		if (Objects.equals(BookingJourneyType.BOOKING_PACKAGE, order.getBookingJourneyType()))
		{
			final List<DealOrderEntryGroupModel> dealOrderEntryGroups = accommodationEntries.stream()
					.filter(entry -> entry.getEntryGroup() instanceof DealOrderEntryGroupModel)
					.map(entry -> (DealOrderEntryGroupModel) entry.getEntryGroup()).collect(Collectors.toList());

			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = dealOrderEntryGroups.stream()
					.flatMap(dealOrderEntryGroupModel -> dealOrderEntryGroupModel.getAccommodationEntryGroups().stream())
					.collect(Collectors.toList());

			return isChildGuestsExistsForGroup(accommodationOrderEntryGroups);
		}

		if (Objects.equals(BookingJourneyType.BOOKING_ACCOMMODATION_ONLY, order.getBookingJourneyType()))
		{
			final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = accommodationEntries.stream()
					.filter(entry -> entry.getEntryGroup() instanceof AccommodationOrderEntryGroupModel)
					.map(entry -> (AccommodationOrderEntryGroupModel) entry.getEntryGroup()).collect(Collectors.toList());

			return isChildGuestsExistsForGroup(accommodationOrderEntryGroups);
		}

		return false;
	}

	protected boolean isChildGuestsExistsForGroup(final List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups)
	{
		final Stream<GuestCountModel> guestCountStream = accommodationOrderEntryGroups.stream()
				.flatMap(accommodationOrderEntryGroupModel -> accommodationOrderEntryGroupModel.getGuestCounts().stream());
		return guestCountStream.map(GuestCountModel::getPassengerType).anyMatch(
				passengerTypeModel -> StringUtils.equals(BcfFulfilmentProcessConstants.CHILD, passengerTypeModel.getCode()));
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
