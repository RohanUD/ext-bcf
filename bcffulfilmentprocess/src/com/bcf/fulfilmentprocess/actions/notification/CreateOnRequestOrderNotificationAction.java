/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.actions.notification;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.order.notification.response.OrderPlaceNotificationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.order.onrequest.OnRequestBookingData;

public class CreateOnRequestOrderNotificationAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CreateOnRequestOrderNotificationAction.class);
	private NotificationEngineRestService notificationEngineRestService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		Transition transition = Transition.OK;
		final OrderModel order = orderProcessModel.getOrder();
		if (Objects.isNull(order))
		{
			return Transition.NOK;
		}
		LOG.debug("sending on request booking notification for order --> "+order.getCode());
		try
		{
			final OnRequestBookingData onRequestBookingData = getOnRequestBookingData(order);
			onRequestBookingData.setEventType("onRequestInitialEmail");
			if(OrderStatus.REJECTED_BY_MERCHANT.equals(order.getStatus()))
			{
				onRequestBookingData.setEventType("onRequestBookingRejection");
				transition = Transition.NOK;
			}
			getNotificationEngineRestService()
					.sendRestRequest(onRequestBookingData, OrderPlaceNotificationResponseData.class, HttpMethod.POST,
							BcfintegrationserviceConstants.ON_REQUEST_BOOKING_URL);
		}
		catch (final IntegrationException e)
		{
			LOG.error("error while sending on request booking notification", e);
			return Transition.NOK;
		}
		return transition;
	}

	protected OnRequestBookingData getOnRequestBookingData(final OrderModel order)
	{
		final OnRequestBookingData onRequestBookingData = new OnRequestBookingData();
		onRequestBookingData.setOrderCode(order.getCode());
		if (Objects.nonNull(order.getUser()) && order.getUser() instanceof CustomerModel)
		{
			final CustomerData customerData = new CustomerData();
			final CustomerModel customerModel = (CustomerModel) order.getUser();
			if (StringUtils.isNotEmpty(customerModel.getFirstName()))
			{
				customerData.setFirstName(customerModel.getFirstName());
			}
			if (StringUtils.isNotEmpty(customerModel.getLastName()))
			{
				customerData.setLastName(customerModel.getLastName());
			}
			if(StringUtils.isNotBlank(customerModel.getContactEmail()))
			{
				customerData.setEmail(customerModel.getContactEmail());
			}
			onRequestBookingData.setCustomerData(customerData);
		}
		return onRequestBookingData;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}
