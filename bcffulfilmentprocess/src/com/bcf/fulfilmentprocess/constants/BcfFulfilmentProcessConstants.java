/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.constants;

public final class BcfFulfilmentProcessConstants extends GeneratedBcfFulfilmentProcessConstants
{
	public static final String CONSIGNMENT_SUBPROCESS_END_EVENT_NAME = "ConsignmentSubprocessEnd";
	public static final String ORDER_PROCESS_NAME = "order-process";
	public static final String AMEND_ORDER_PROCESS_NAME = "amend-order-process";
	public static final String ONREQUEST_INITIAL_ORDER_PROCESS_NAME = "onrequest-order-process";
	public static final String CONSIGNMENT_SUBPROCESS_NAME = "consignment-process";
	public static final String OPTION_BOOKING_PROCESS_NAME = "option-booking-process";
	public static final String OPTION_BOOKING_CANCEL_PROCESS_NAME = "cancel-optionBooking-process";
	public static final String WAIT_FOR_WAREHOUSE = "WaitForWarehouse";
	public static final String CONSIGNMENT_PICKUP = "ConsignmentPickup";
	public static final String CONSIGNMENT_COUNTER = "CONSIGNMENT_COUNTER";
	public static final String PARENT_PROCESS = "PARENT_PROCESS";
	public static final String CUSTOMER = "CUSTOMER";
	public static final String BASE_STORE = "BASE_STORE";
	public static final String CHILD = "child";
	public static final String FAMILY_TRAVELLER = "familytravellersgroup";
	public static final String AMERICA_VANCOUVER = "America/Vancouver";

	public static final String DEFAULT_CURRENCY_CODE = "USD";
}
