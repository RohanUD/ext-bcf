/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.exceptions;

/**
 *
 */
public class PaymentMethodException extends RuntimeException
{

	/**
	 * @param message
	 */
	public PaymentMethodException(final String message)
	{
		super(message);
	}

}
