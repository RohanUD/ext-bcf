/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.modify.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.modify.order.ModifyOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.PackageReservationDataList;
import com.bcf.notification.request.order.modify.ModifyOrderNotificationData;


public class DefaultVacationModifyOrderNotificationPipelineManager extends AbstractModifyOrderNotificationManager implements
		ModifyOrderNotificationPipelineManager
{
	private Map<String, VacationOrderCreateNotificationHandler> createOrderNotificationHandlersMap;
	private OrderFinancialDataPipelineManager modifyOrderFinancialDataPipelineManager;

	@Override
	public ModifyOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel)
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}
		final ModifyOrderNotificationData modifyOrderNotificationData = new ModifyOrderNotificationData();
		modifyOrderNotificationData.setCode(abstractOrderModel.getCode());
		final PackageReservationDataList packageReservationDataList = new PackageReservationDataList();
		final List<PackageReservationData> listPackageReservationData = new ArrayList<>();
		final PackageReservationData packageReservationData = new PackageReservationData();

		//there might be cases when there are inactive entries in case if any entry is deleted, get only active entries
		final Map<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByType = StreamUtil
				.safeStream(abstractOrderModel.getEntries())
				.filter(AbstractOrderEntryModel::getActive)
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));

		for (final OrderEntryType currentEntryType : dealEntriesByType.keySet())
		{
			getCreateOrderNotificationHandlersMap().get(currentEntryType.getCode())
					.handle(abstractOrderModel, dealEntriesByType.get(currentEntryType), packageReservationData);
		}
		packageReservationData.setVouchersSize(getVoucherSize(packageReservationData));
		listPackageReservationData.add(packageReservationData);
		packageReservationDataList.setPackageReservationDatas(listPackageReservationData);

		modifyOrderNotificationData.setEventType("OrderModification_" + abstractOrderModel.getBookingJourneyType().getCode());
		return modifyOrderNotificationData;
	}

	private int getVoucherSize(final PackageReservationData packageReservationData)
	{
		int totalFerries = 0;
		int totalHotels = 0;
		int totalActivities = 0;

		if (CollectionUtils.isNotEmpty(packageReservationData.getFerryJourneyInfoList()))
		{
			totalFerries = (int) packageReservationData.getFerryJourneyInfoList().stream()
					.filter(ferryJourneyInfo -> CollectionUtils.isNotEmpty(ferryJourneyInfo.getFerryInfoList()))
					.flatMap(ferryJourneyInfo -> ferryJourneyInfo.getFerryInfoList().stream()).count();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getHotelInfoList()))
		{
			totalHotels = packageReservationData.getHotelInfoList().size();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getActivityInfoList()))
		{
			totalActivities = packageReservationData.getActivityInfoList().size();
		}

		return totalFerries + totalHotels + totalActivities;
	}

	protected OrderFinancialDataPipelineManager getModifyOrderFinancialDataPipelineManager()
	{
		return modifyOrderFinancialDataPipelineManager;
	}

	public Map<String, VacationOrderCreateNotificationHandler> getCreateOrderNotificationHandlersMap()
	{
		return createOrderNotificationHandlersMap;
	}

	public void setCreateOrderNotificationHandlersMap(
			final Map<String, VacationOrderCreateNotificationHandler> createOrderNotificationHandlersMap)
	{
		this.createOrderNotificationHandlersMap = createOrderNotificationHandlersMap;
	}

	@Required
	public void setModifyOrderFinancialDataPipelineManager(
			final OrderFinancialDataPipelineManager modifyOrderFinancialDataPipelineManager)
	{
		this.modifyOrderFinancialDataPipelineManager = modifyOrderFinancialDataPipelineManager;
	}
}
