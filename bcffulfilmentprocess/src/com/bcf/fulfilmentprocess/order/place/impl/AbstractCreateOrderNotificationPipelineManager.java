/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.order.AbstractOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public abstract class AbstractCreateOrderNotificationPipelineManager extends AbstractOrderNotificationPipelineManager
{

	private List<CreateOrderNotificationGlobalHandler> globalHandlers;

	protected void populateGlobalData(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData orderPlaceNotificationData)
			throws IOException, WriterException
	{
		for(CreateOrderNotificationGlobalHandler handler : getGlobalHandlers())
		{
			handler.handle(abstractOrderModel, orderPlaceNotificationData);
		}
	}

	protected List<CreateOrderNotificationGlobalHandler> getGlobalHandlers()
	{
		return globalHandlers;
	}

	@Required
	public void setGlobalHandlers(
			final List<CreateOrderNotificationGlobalHandler> globalHandlers)
	{
		this.globalHandlers = globalHandlers;
	}

}
