/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.notification.request.order.financial.FinancialData;


public class DefaultOrderFinancialDataPipelineManager implements OrderFinancialDataPipelineManager
{
	private Converter<AbstractOrderModel,FinancialData> financialDataConverter;

	@Override
	public FinancialData executePipeline(final AbstractOrderModel abstractOrderModel)
	{
		final FinancialData financialData =  getFinancialDataConverter().convert(abstractOrderModel);
		return financialData;
	}

	protected Converter<AbstractOrderModel, FinancialData> getFinancialDataConverter()
	{
		return financialDataConverter;
	}

	@Required
	public void setFinancialDataConverter(
			final Converter<AbstractOrderModel, FinancialData> financialDataConverter)
	{
		this.financialDataConverter = financialDataConverter;
	}
}
