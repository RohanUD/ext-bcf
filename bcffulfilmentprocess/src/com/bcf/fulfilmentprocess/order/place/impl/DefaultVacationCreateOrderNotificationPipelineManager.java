/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.BCFNonRefundableCostTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;
import com.bcf.core.service.BcfVacationTermsAndConditionsService;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.PackageReservationDataList;
import com.google.zxing.WriterException;


public class DefaultVacationCreateOrderNotificationPipelineManager extends AbstractCreateOrderNotificationPipelineManager
		implements CreateOrderNotificationPipelineManager
{
	private Map<String, VacationOrderCreateNotificationHandler> createOrderNotificationHandlersMap;
	private OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager;

	@Resource(name = "bcfVacationTermsAndConditionsService")
	private BcfVacationTermsAndConditionsService bcfVacationTermsAndConditionsService;

	@Resource
	private Converter<VacationPolicyTAndCData, com.bcf.notification.request.order.createorder.VacationPolicyTAndCData> notificationVacationPolicyTAndCDataConverter;

	@Resource
	Converter<BCFNonRefundableCostTAndCData, com.bcf.notification.request.order.createorder.BCFNonRefundableCostTAndCData> notificationNonRefundableCostTAndCDataConverter;

	@Override
	public CreateOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel)
			throws IOException, WriterException
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}
		final CreateOrderNotificationData createOrderNotificationData = new CreateOrderNotificationData();
		createOrderNotificationData.setCode(abstractOrderModel.getCode());
		createOrderNotificationData.setOrderType(abstractOrderModel.getBookingJourneyType().getCode());
		final Map<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByType = abstractOrderModel.getEntries().stream()
				.filter(e -> !OrderEntryType.FEE.equals(e.getType()))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));

		final PackageReservationDataList packageReservationDataList = new PackageReservationDataList();
		final List<PackageReservationData> listPackageReservationData = new ArrayList<>();
		final PackageReservationData packageReservationData = new PackageReservationData();

		for (final OrderEntryType currentEntryType : dealEntriesByType.keySet())
		{
			getCreateOrderNotificationHandlersMap().get(currentEntryType.getCode())
					.handle(abstractOrderModel, dealEntriesByType.get(currentEntryType), packageReservationData);
		}

		packageReservationData.setVouchersSize(getVoucherSize(packageReservationData));
		listPackageReservationData.add(packageReservationData);
		packageReservationDataList.setPackageReservationDatas(listPackageReservationData);
		createOrderNotificationData.setPackageReservations(packageReservationDataList);

		populateGlobalData(abstractOrderModel, createOrderNotificationData);

		setPackageTermsAndCondition(createOrderNotificationData, abstractOrderModel);

		createOrderNotificationData.setEventType("vacationConfirmation");
		return createOrderNotificationData;
	}

	@Override
	public CreateOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel,
			List<AbstractOrderEntryModel> orderEntries, List<OrderEntryType> orderEntryTypes)
			throws IOException, WriterException
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}
		final CreateOrderNotificationData createOrderNotificationData = new CreateOrderNotificationData();
		createOrderNotificationData.setCode(abstractOrderModel.getCode());
		createOrderNotificationData.setOrderType(abstractOrderModel.getBookingJourneyType().getCode());

		List<AbstractOrderEntryModel> entriesToBeWorkedUpon = orderEntries;
		if (CollectionUtils.isEmpty(entriesToBeWorkedUpon))
		{
			entriesToBeWorkedUpon = abstractOrderModel.getEntries();
		}

		final Map<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByType;
		if (CollectionUtils.isEmpty(orderEntryTypes))
		{
			dealEntriesByType = StreamUtil.safeStream(entriesToBeWorkedUpon).filter(e -> !OrderEntryType.FEE.equals(e.getType()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
		}
		else
		{
			dealEntriesByType = StreamUtil.safeStream(entriesToBeWorkedUpon)
					.filter(e -> orderEntryTypes.contains(e.getType()))
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
		}

		final PackageReservationDataList packageReservationDataList = new PackageReservationDataList();
		final List<PackageReservationData> listPackageReservationData = new ArrayList<>();
		final PackageReservationData packageReservationData = new PackageReservationData();

		for (final OrderEntryType currentEntryType : dealEntriesByType.keySet())
		{
			getCreateOrderNotificationHandlersMap().get(currentEntryType.getCode())
					.handle(abstractOrderModel, dealEntriesByType.get(currentEntryType), packageReservationData);
		}

		packageReservationData.setVouchersSize(getVoucherSize(packageReservationData));
		listPackageReservationData.add(packageReservationData);
		packageReservationDataList.setPackageReservationDatas(listPackageReservationData);
		createOrderNotificationData.setPackageReservations(packageReservationDataList);

		populateGlobalData(abstractOrderModel, createOrderNotificationData);

		setPackageTermsAndCondition(createOrderNotificationData, abstractOrderModel);

		createOrderNotificationData.setEventType("vacationConfirmation");
		return createOrderNotificationData;
	}

	private int getVoucherSize(final PackageReservationData packageReservationData)
	{
		int totalFerries = 0;
		int totalHotels = 0;
		int totalActivities = 0;

		if (CollectionUtils.isNotEmpty(packageReservationData.getFerryJourneyInfoList()))
		{
			totalFerries = (int) packageReservationData.getFerryJourneyInfoList().stream()
					.filter(ferryJourneyInfo -> CollectionUtils.isNotEmpty(ferryJourneyInfo.getFerryInfoList()))
					.flatMap(ferryJourneyInfo -> ferryJourneyInfo.getFerryInfoList().stream()).count();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getHotelInfoList()))
		{
			totalHotels = packageReservationData.getHotelInfoList().size();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getActivityInfoList()))
		{
			totalActivities = packageReservationData.getActivityInfoList().size();
		}

		return totalFerries + totalHotels + totalActivities;
	}

	private void setPackageTermsAndCondition(final CreateOrderNotificationData createOrderNotificationData,
			final AbstractOrderModel abstractOrderModel)
	{
		if (Objects.nonNull(createOrderNotificationData.getPackageReservations()))
		{
			VacationPolicyTAndCData vacationPolicyTermsAndConditions = bcfVacationTermsAndConditionsService
					.getVacationPolicyTermsAndConditionsForEmails(createOrderNotificationData.getCode(), abstractOrderModel);

			com.bcf.notification.request.order.createorder.VacationPolicyTAndCData vacationPolicyTAndCData = notificationVacationPolicyTAndCDataConverter
					.convert(vacationPolicyTermsAndConditions);

			createOrderNotificationData.getPackageReservations().setVacationPolicyTAndCData(vacationPolicyTAndCData);

			List<BCFNonRefundableCostTAndCData> bcfNonRefundableCostTermsAndConditions = bcfVacationTermsAndConditionsService
					.getBCFNonRefundableCostTermsAndConditionsForEmails(createOrderNotificationData.getCode());

			List<com.bcf.notification.request.order.createorder.BCFNonRefundableCostTAndCData> bcfNonRefundableCostTAndCDataList;
			bcfNonRefundableCostTAndCDataList = notificationNonRefundableCostTAndCDataConverter
					.convertAll(CollectionUtils.isNotEmpty(bcfNonRefundableCostTermsAndConditions) ?
							bcfNonRefundableCostTermsAndConditions :
							null);

			createOrderNotificationData.getPackageReservations()
					.setBcfNonRefundableCostTAndCDatas(bcfNonRefundableCostTAndCDataList);
		}
	}

	protected Map<String, VacationOrderCreateNotificationHandler> getCreateOrderNotificationHandlersMap()
	{
		return createOrderNotificationHandlersMap;
	}

	@Required
	public void setCreateOrderNotificationHandlersMap(
			final Map<String, VacationOrderCreateNotificationHandler> createOrderNotificationHandlersMap)
	{
		this.createOrderNotificationHandlersMap = createOrderNotificationHandlersMap;
	}

	protected OrderFinancialDataPipelineManager getCreateOrderFinancialDataPipelineManager()
	{
		return createOrderFinancialDataPipelineManager;
	}

	@Required
	public void setCreateOrderFinancialDataPipelineManager(
			final OrderFinancialDataPipelineManager createOrderFinancialDataPipelineManager)
	{
		this.createOrderFinancialDataPipelineManager = createOrderFinancialDataPipelineManager;
	}
}
