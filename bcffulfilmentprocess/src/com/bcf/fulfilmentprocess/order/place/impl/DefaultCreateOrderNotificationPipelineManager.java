/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.create.order.CreateOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationDataList;
import com.google.zxing.WriterException;


public class DefaultCreateOrderNotificationPipelineManager extends AbstractCreateOrderNotificationPipelineManager
		implements CreateOrderNotificationPipelineManager
{
	private List<ReservationDataNotificationHandler> handlers;

	@Override
	public CreateOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel) throws IOException,
			WriterException
	{
		final CreateOrderNotificationData orderPlaceNotificationData = new CreateOrderNotificationData();
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservationDatas = new ArrayList<>();
		final Map<Integer, List<AbstractOrderEntryModel>> entriesByJourneyReference = abstractOrderModel.getEntries().stream()
				.filter(entry -> Objects.isNull(entry.getEntryGroup())
						&& OrderEntryType.TRANSPORT.equals(entry.getType())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo()))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		for (final Integer journeyRefNumber : entriesByJourneyReference.keySet()) //NOSONAR
		{
			final ReservationData reservationData = new ReservationData();
			reservationData.setJourneyReferenceNumber(journeyRefNumber);
			final List<AbstractOrderEntryModel> entriesPerJourney = entriesByJourneyReference.get(journeyRefNumber);
			for (final ReservationDataNotificationHandler handler : getHandlers())
			{
				handler.handle(abstractOrderModel, entriesPerJourney, reservationData);
			}
			reservationDatas.add(reservationData);
		}

		reservationDataList.setReservationData(reservationDatas);
		orderPlaceNotificationData.setTransportReservation(reservationDataList);

		populateGlobalData(abstractOrderModel, orderPlaceNotificationData);

		if (abstractOrderModel.isFerryOptionBooking())
		{
			orderPlaceNotificationData.setEventType("createOrderKeyAccounts");
			orderPlaceNotificationData.setOptionBooking(true);
		}
		else
		{
			orderPlaceNotificationData.setEventType("ordercreate");
		}
		return orderPlaceNotificationData;
	}

	@Override
	public CreateOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> orderEntries, final List<OrderEntryType> orderEntryTypes)
			throws IOException, WriterException
	{
		//deliberately kept empty, this scenario would not arise where we need to send specific transport order entries
		return null;
	}

	protected List<ReservationDataNotificationHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ReservationDataNotificationHandler> handlers)
	{
		this.handlers = handlers;
	}
}
