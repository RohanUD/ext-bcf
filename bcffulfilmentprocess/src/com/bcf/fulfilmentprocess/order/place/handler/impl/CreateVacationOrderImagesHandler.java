/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.BookingConfirmationQrCodeHelper;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.google.zxing.WriterException;


public class CreateVacationOrderImagesHandler implements CreateOrderNotificationGlobalHandler
{
	private BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
			throws IOException, WriterException
	{
		populateEncodedImages(abstractOrderModel, createOrderNotificationData);
	}

	protected void populateEncodedImages(final AbstractOrderModel abstractOrderModel,
			final CreateOrderNotificationData orderPlaceNotificationData) throws IOException, WriterException
	{
		final Map<String, String> qrCodeImageMap = getBookingConfirmationQrCodeHelper().getQRImageMap(abstractOrderModel);
		orderPlaceNotificationData.setInlineImageMap(qrCodeImageMap);
	}

	protected BookingConfirmationQrCodeHelper getBookingConfirmationQrCodeHelper()
	{
		return bookingConfirmationQrCodeHelper;
	}

	@Required
	public void setBookingConfirmationQrCodeHelper(final BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper)
	{
		this.bookingConfirmationQrCodeHelper = bookingConfirmationQrCodeHelper;
	}

}
