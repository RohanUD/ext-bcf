/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import static com.bcf.core.constants.BcfCoreConstants.UNDERSCORE;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.notification.request.order.createorder.ActivityInfo;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.PassengerInfo;
import com.bcf.notification.request.order.createorder.VendorInfo;


public class VacationActivityInfoHandler implements VacationOrderCreateNotificationHandler
{
	private BCFPassengerTypeService passengerTypeService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> abstractOrderEntries,
			final PackageReservationData packageReservationData)
	{
		packageReservationData.setActivityInfoList(populateActivityInfoList(abstractOrderEntries));
	}

	private List<ActivityInfo> populateActivityInfoList(final List<AbstractOrderEntryModel> transportOrActivitiesEntries)
	{
		final List<ActivityInfo> activityInfoList = new ArrayList<>();

		final Map<String, List<AbstractOrderEntryModel>> entriesByUniqueActivities = StreamUtil.safeStream(transportOrActivitiesEntries)
				.collect(Collectors.groupingBy(e -> getUniqueActivityGroupingCode(e)));

		for (final Map.Entry<String, List<AbstractOrderEntryModel>> mapEntry : entriesByUniqueActivities.entrySet())
		{
			final ActivityInfo activityInfo = new ActivityInfo();
			final ActivityProductModel activityProduct = (ActivityProductModel) mapEntry.getValue().get(0).getProduct();
			activityInfo.setActivityName(activityProduct.getName());
			activityInfo.setActivityDescription(activityProduct.getDescription());
			final ActivityOrderEntryInfoModel activityOrderEntryInfo = mapEntry.getValue().get(0).getActivityOrderEntryInfo();
			activityInfo.setActivityDate(activityOrderEntryInfo.getActivityDate());
			activityInfo.setScheduleTime(activityOrderEntryInfo.getActivityTime());
			activityInfo.setGuestCount(mapEntry.getValue().stream().mapToInt(v -> v.getQuantity().intValue()).sum());
			if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(mapEntry.getValue().get(0).getComments())){
				activityInfo
						.setNotes(mapEntry.getValue().get(0).getComments().stream().map(comment -> comment.getText()).collect(
						Collectors.toList()));
			}
			populateVendorInfo(activityProduct, activityInfo);
			populateGuestCount(mapEntry.getValue(), activityInfo);
			populatePassengerInfo(activityOrderEntryInfo, activityInfo);
			activityInfoList.add(activityInfo);
		}
		return activityInfoList;
	}

	private String getUniqueActivityGroupingCode(final AbstractOrderEntryModel orderEntryModel)
	{
		return orderEntryModel.getProduct().getCode() + UNDERSCORE + orderEntryModel.getActivityOrderEntryInfo().getActivityDate()
				.toString() + UNDERSCORE + orderEntryModel
				.getActivityOrderEntryInfo().getActivityTime();
	}

	protected void populateGuestCount(final List<AbstractOrderEntryModel> transportOrActivitiesEntries,
			final ActivityInfo activityInfo)
	{
		final Map<String, Integer> countPerGuest = new HashMap<>();
		transportOrActivitiesEntries.forEach(entry -> {
			final PassengerTypeModel passengerType = getPassengerTypeService()
					.getPassengerType(entry.getActivityOrderEntryInfo().getGuestType().getCode().toLowerCase());
			String passengerTypeCode = StringUtils.EMPTY;
			if (passengerType instanceof SpecializedPassengerTypeModel)
			{
				passengerTypeCode = ((SpecializedPassengerTypeModel) passengerType).getBasePassengerType().getCode();
			}
			else
			{
				passengerTypeCode = passengerType.getCode();
			}
			countPerGuest.put(passengerTypeCode, entry.getQuantity().intValue());
		});
		activityInfo.setCountPerGuest(countPerGuest);
	}

	private void populateVendorInfo(final ActivityProductModel activityProduct, final ActivityInfo activityInfo)
	{
		if (CollectionUtils.isNotEmpty(activityProduct.getVendors()))
		{
			final VendorModel activityVendor = activityProduct.getVendors().iterator().next();
			final VendorInfo vendorInfo = new VendorInfo();
			vendorInfo.setName(activityVendor.getName());
			if (CollectionUtils.isNotEmpty(activityVendor.getEmails()))
			{
				vendorInfo.setVendorEmails(new ArrayList<>(activityVendor.getEmails()));
			}
			activityInfo.setVendorInfo(vendorInfo);
		}
	}

	private void populatePassengerInfo(final ActivityOrderEntryInfoModel orderEntryInfoModel, final ActivityInfo activityInfo)
	{
		final List<PassengerInfo> passengerInfoList = new ArrayList<>();
		final PassengerInfo passengerInfo = new PassengerInfo();
		passengerInfo.setFirstName(orderEntryInfoModel.getLeadPaxFirstName());
		passengerInfo.setLastName(orderEntryInfoModel.getLeadPaxLastName());
		passengerInfo.setLeadTraveller(true);
		passengerInfoList.add(passengerInfo);

		activityInfo.setPassengerInfoList(passengerInfoList);
	}

	protected BCFPassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}
}
