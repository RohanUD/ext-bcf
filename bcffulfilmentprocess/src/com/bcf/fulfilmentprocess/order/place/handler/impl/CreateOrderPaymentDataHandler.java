/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CardPaymentData;
import com.bcf.notification.request.order.createorder.CashPaymentData;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.createorder.GiftCardPaymentData;
import com.bcf.notification.request.order.createorder.PaymentData;


public class CreateOrderPaymentDataHandler implements CreateOrderNotificationGlobalHandler
{
	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
	{
		final Map<BCFPaymentMethodType, List<PaymentTransactionModel>> paymentTransactionsByType = abstractOrderModel
				.getPaymentTransactions()
				.stream().filter(transaction -> CollectionUtils.isNotEmpty(transaction.getEntries()))
				.collect(Collectors.groupingBy(paymentTransactionEntry -> paymentTransactionEntry.getEntries().get(0).getTransactionType()));
		final PaymentData paymentData = new PaymentData();

		final Double purchaseAmount = StreamUtil.safeStream(paymentTransactionsByType.values()).filter(Objects::nonNull)
				.flatMap(paymentTransactionModels -> paymentTransactionModels.stream())
				.flatMap(transactionModel -> transactionModel.getEntries().stream())
				.mapToDouble(entry -> entry.getAmount().doubleValue())
				.sum();
		paymentData.setPurchaseAmount(purchaseAmount);

		for (final BCFPaymentMethodType bcfPaymentMethodType : paymentTransactionsByType.keySet())
		{
			final List<PaymentTransactionModel> transactions = paymentTransactionsByType.get(bcfPaymentMethodType);
			if (BCFPaymentMethodType.CARD.equals(bcfPaymentMethodType))
			{
				final CardPaymentData cardPaymentData = new CardPaymentData();
				final PaymentTransactionModel cardPaymentTransaciton = transactions.get(0);
				final PaymentTransactionEntryModel cardPaymentEntry = cardPaymentTransaciton.getEntries().get(0);
				cardPaymentData.setApprovalCode(cardPaymentEntry.getApprovalNumber());
				cardPaymentData.setTransactionDate(cardPaymentEntry.getTime());
				final CreditCardPaymentInfoModel creditCardInfo = (CreditCardPaymentInfoModel) cardPaymentTransaciton.getInfo();
				cardPaymentData.setCardType(creditCardInfo.getType().getCode());
				cardPaymentData.setCardNumber(creditCardInfo.getNumber());
				paymentData.setCardPaymentData(cardPaymentData);
			}
			else if (BCFPaymentMethodType.GIFTCARD.equals(bcfPaymentMethodType))
			{
				final List<PaymentTransactionModel> giftCardTransactions = paymentTransactionsByType.get(bcfPaymentMethodType);
				final List<GiftCardPaymentData> giftCardPaymentDataList = new ArrayList<>();
				for (final PaymentTransactionModel giftCardTransaction : giftCardTransactions)
				{
					final GiftCardPaymentData giftCardPaymentData = new GiftCardPaymentData();
					final GiftCardPaymentInfoModel giftCardPaymentInfoModel = (GiftCardPaymentInfoModel) giftCardTransaction.getInfo();
					giftCardPaymentData.setGiftCard(giftCardPaymentInfoModel.getGiftCardNumber());
					giftCardPaymentData.setAmount(giftCardTransaction.getEntries().get(0).getAmount().doubleValue());
					giftCardPaymentDataList.add(giftCardPaymentData);
				}
				paymentData.setGiftPaymentData(giftCardPaymentDataList);
			}
			else if (BCFPaymentMethodType.CASH.equals(bcfPaymentMethodType))
			{
				final PaymentTransactionModel cashPaymentTransaction = transactions.get(0);
				final CashPaymentInfoModel cashPaymentInfoModel = (CashPaymentInfoModel) cashPaymentTransaction.getInfo();
				final CashPaymentData cashPaymentData = new CashPaymentData();
				cashPaymentData.setReceiptNo(cashPaymentInfoModel.getReceiptNumber());
				cashPaymentData.setAmount(cashPaymentTransaction.getEntries().get(0).getAmount().doubleValue());
				paymentData.setCashPaymentData(cashPaymentData);
			}
		}
		createOrderNotificationData.setPaymentData(paymentData);
	}
}
