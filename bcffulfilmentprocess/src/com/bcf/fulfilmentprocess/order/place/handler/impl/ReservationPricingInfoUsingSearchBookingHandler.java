/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.travelservices.enums.TravellerType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.dao.BCFPassengerTypeDao;
import com.bcf.core.util.StreamUtil;
import com.bcf.core.vehicletype.dao.VehicleTypeDao;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataUsingSearchBookingNotificationHandler;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.integration.common.data.ProductFare;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.order.createorder.ItineraryPricingInfoData;
import com.bcf.notification.request.order.createorder.PTCFareBreakdownData;
import com.bcf.notification.request.order.createorder.PassengerFareData;
import com.bcf.notification.request.order.createorder.PassengerTypeData;
import com.bcf.notification.request.order.createorder.PassengerTypeQuantityData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.bcf.notification.request.order.createorder.ReservationPricingInfoData;
import com.bcf.notification.request.order.createorder.VehicleFareBreakdownData;
import com.bcf.notification.request.order.createorder.VehicleTypeData;
import com.bcf.notification.request.order.createorder.VehicleTypeQuantityData;


public class ReservationPricingInfoUsingSearchBookingHandler implements ReservationDataUsingSearchBookingNotificationHandler
{
	private NotificationUtils notificationUtils;
	private BCFPassengerTypeDao bcfPassengerTypeDao;
	private VehicleTypeDao vehicleTypeDao;

	@Override
	public void handle(final Itinerary itinerary, final ReservationData reservationData)
	{
		final List<ReservationItemData> reservationItems = reservationData.getReservationItem();

		final ReservationItemData reservationItemData = reservationItems.get(0);
		final ReservationPricingInfoData reservationPricingInfo = new ReservationPricingInfoData();
		reservationPricingInfo.setItineraryPricingInfo(createItineraryPricingInfo(itinerary));
		reservationItemData.setReservationPricingInfo(reservationPricingInfo);
	}

	protected ItineraryPricingInfoData createItineraryPricingInfo(final Itinerary itinerary)
	{
		final ItineraryPricingInfoData itineraryPricingInfoData = new ItineraryPricingInfoData();

		itineraryPricingInfoData.setPtcFareBreakdownData(createPtcFareBreakdownList(itinerary));
		itineraryPricingInfoData.setVehicleFareBreakdownDatas(createVehicleFareBreakdownList(itinerary));
		itineraryPricingInfoData.setFareType(
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get().getSailingPrices())
						.findFirst().get().getTariffType());
		return itineraryPricingInfoData;
	}

	protected List<VehicleFareBreakdownData> createVehicleFareBreakdownList(final Itinerary itinerary)
	{
		final List<VehicleFareBreakdownData> vehicleFareBreakdownDatas = new ArrayList<>();
		StreamUtil.safeStream(
				StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get().getSailingPrices())
						.findFirst().get().getProductFares())
				.filter(productFare -> TravellerType.VEHICLE.getCode().equals(productFare.getProduct().getProductCategory()))
				.forEach(productFare -> vehicleFareBreakdownDatas.add(createVehicleFareBreakdownData(productFare)));
		return vehicleFareBreakdownDatas;
	}

	protected VehicleFareBreakdownData createVehicleFareBreakdownData(final ProductFare productFare)
	{
		final VehicleFareBreakdownData vehicleFareBreakdown = new VehicleFareBreakdownData();
		vehicleFareBreakdown.setVehicleTypeQuantity(createVehicleTypeQuantity(productFare));
		vehicleFareBreakdown.setVehicleFare(createProductFare(Collections.singletonList(productFare)));
		return vehicleFareBreakdown;
	}

	protected VehicleTypeQuantityData createVehicleTypeQuantity(final ProductFare productFare)
	{
		final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
		vehicleTypeQuantityData.setQuantity((int) productFare.getProduct().getProductNumber());

		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeData.setName(getVehicleTypeDao().getVehicleByCode(productFare.getProduct().getProductId()).getName());

		vehicleTypeQuantityData.setVehicleType(vehicleTypeData);

		return vehicleTypeQuantityData;
	}

	protected List<PTCFareBreakdownData> createPtcFareBreakdownList(final Itinerary itinerary)
	{
		final List<PTCFareBreakdownData> ptcFareBreakdownDatas = new ArrayList<>();
		final Map<String, List<ProductFare>> paxGroupedById = StreamUtil
				.safeStream(StreamUtil.safeStream(StreamUtil.safeStream(itinerary.getSailing().getLine()).findFirst().get()
						.getSailingPrices()).findFirst().get().getProductFares())
				.filter(productFare -> TravellerType.PASSENGER.getCode().equals(productFare.getProduct().getProductCategory()))
				.collect(Collectors.groupingBy(productFare -> productFare.getProduct().getProductId()));
		paxGroupedById.entrySet().stream().forEach(stringListEntry ->
			ptcFareBreakdownDatas.add(createPTCFareBreakdown(stringListEntry.getValue())));
		return ptcFareBreakdownDatas;
	}

	protected PTCFareBreakdownData createPTCFareBreakdown(final List<ProductFare> productFares)
	{
		final PTCFareBreakdownData ptcFareBreakdownData = new PTCFareBreakdownData();
		ptcFareBreakdownData.setPassengerFare(createProductFare(productFares));
		ptcFareBreakdownData.setPassengerTypeQuantity(createPassengerTypeQuantity(productFares));

		return ptcFareBreakdownData;
	}

	protected PassengerFareData createProductFare(final List<ProductFare> productFares)
	{
		final PassengerFareData passengerFareData = new PassengerFareData();
		final long paxFaresInCents = productFares.stream()
				.mapToLong(productFare -> productFare.getFareDetail().stream().findFirst().get().getGrossAmountInCents()).sum();
		passengerFareData.setTotalFare((double) paxFaresInCents);
		return passengerFareData;
	}

	protected PassengerTypeQuantityData createPassengerTypeQuantity(final List<ProductFare> productFares)
	{
		final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
		final int quantity = productFares.stream().mapToInt(productFare -> (int) productFare.getProduct().getProductNumber()).sum();
		passengerTypeQuantityData.setQuantity(quantity);
		passengerTypeQuantityData.setPassengerType(createPassengerType(productFares.stream().findFirst().get()));
		return passengerTypeQuantityData;
	}

	protected PassengerTypeData createPassengerType(final ProductFare productFare)
	{
		final PassengerTypeData passengerTypeData = new PassengerTypeData();
		passengerTypeData.setCode(productFare.getProduct().getProductId());
		passengerTypeData.setName(
				getBcfPassengerTypeDao().findPassengerModelForEBookingCode(productFare.getProduct().getProductId()).getName());
		return passengerTypeData;
	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}

	protected BCFPassengerTypeDao getBcfPassengerTypeDao()
	{
		return bcfPassengerTypeDao;
	}

	@Required
	public void setBcfPassengerTypeDao(final BCFPassengerTypeDao bcfPassengerTypeDao)
	{
		this.bcfPassengerTypeDao = bcfPassengerTypeDao;
	}

	protected VehicleTypeDao getVehicleTypeDao()
	{
		return vehicleTypeDao;
	}

	@Required
	public void setVehicleTypeDao(final VehicleTypeDao vehicleTypeDao)
	{
		this.vehicleTypeDao = vehicleTypeDao;
	}
}
