/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;


public class CreateOrderCurrencyDataHandler implements CreateOrderNotificationGlobalHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
	{
		createOrderNotificationData.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		createOrderNotificationData.setCurrencySymbol(abstractOrderModel.getCurrency().getSymbol());
	}

}
