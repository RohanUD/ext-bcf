/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.createorder.ItineraryData;
import com.bcf.notification.request.order.createorder.LocationData;
import com.bcf.notification.request.order.createorder.OriginDestinationOptionData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.bcf.notification.request.order.createorder.TransportFacilityData;
import com.bcf.notification.request.order.createorder.TransportOfferingData;
import com.bcf.notification.request.order.createorder.TransportVehicleData;
import com.bcf.notification.request.order.createorder.TravelRouteData;
import com.bcf.notification.request.order.createorder.TravelSectorData;


public class ReservationItineraryDataHandler implements ReservationDataNotificationHandler
{
	private NotificationUtils notificationUtils;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> cartEntriesByJourneyReference,
			final ReservationData reservationData)
	{
			final List<ReservationItemData> reservationItemData = reservationData.getReservationItem();
			if (CollectionUtils.isNotEmpty(reservationItemData))
			{
				final Map<Integer, List<AbstractOrderEntryModel>> entriesByOdRefNumber = cartEntriesByJourneyReference.stream()
						.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
				reservationItemData.forEach(reservationItem -> {
					List<AbstractOrderEntryModel> transportEntries = entriesByOdRefNumber
							.get(reservationItem.getOriginDestinationRefNumber());
					reservationItem.setReservationItinerary(populateItineraryData(transportEntries));
				});

			}
	}

	private ItineraryData populateItineraryData(final List<AbstractOrderEntryModel> orderEntriesByODReference)
	{
		final ItineraryData itineraryData = new ItineraryData();
		populateRoute(orderEntriesByODReference, itineraryData);
		populateOriginDestinationOptions(orderEntriesByODReference, itineraryData);
		itineraryData.setDuration(NotificationUtils.calculateJourneyDuration(
				itineraryData.getOriginDestinationOptions().get(0).getTransportOfferings()));
		return itineraryData;
	}


	private void populateOriginDestinationOptions(final List<AbstractOrderEntryModel> transportEntries,
			final ItineraryData itineraryData)
	{
		final List<OriginDestinationOptionData> originDestinationOptions = new ArrayList<>();
		final OriginDestinationOptionData originDestinationOption = new OriginDestinationOptionData();
		populateTransportOffering(transportEntries, originDestinationOption);
		originDestinationOptions.add(originDestinationOption);
		itineraryData.setOriginDestinationOptions(originDestinationOptions);
	}

	private void populateTransportOffering(final List<AbstractOrderEntryModel> transportEntries,
			final OriginDestinationOptionData originDestinationOption)
	{
		final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();
		final List<TransportOfferingModel> transportOfferingModel = (List<TransportOfferingModel>) transportEntries.get(0)
				.getTravelOrderEntryInfo().getTransportOfferings();
		transportOfferingModel.forEach(source -> {
			final TransportOfferingData transportOfferingData = new TransportOfferingData();
			transportOfferingData.setCode(source.getCode());
			transportOfferingData.setDepartureTime(source.getDepartureTime());
			transportOfferingData.setArrivalTime(source.getArrivalTime());
			transportOfferingData.setDuration(
					source.getDuration() != null ? NotificationUtils.getDurationMap(source.getDuration()) : null);

			if (source.getTravelSector() != null)
			{
				transportOfferingData.setSector(populateSector(source.getTravelSector()));
			}

			transportOfferingData.setTransportVehicle(populateTransportVehicle(source.getTransportVehicle()));
			transportOfferingDataList.add(transportOfferingData);
		});
		originDestinationOption.setTransportOfferings(transportOfferingDataList);
	}

	private void populateRoute(final List<AbstractOrderEntryModel> transportEntries, final ItineraryData itineraryData)
	{
		final TravelRouteData travelRouteData = new TravelRouteData();
		travelRouteData.setCode(transportEntries.get(0).getTravelOrderEntryInfo().getTravelRoute().getCode());
		travelRouteData.setName(transportEntries.get(0).getTravelOrderEntryInfo().getTravelRoute().getName());
		itineraryData.setRoute(travelRouteData);


	}

	private TravelSectorData populateSector(final TravelSectorModel travelSector)
	{
		final TravelSectorData travelSectorData = new TravelSectorData();
		final TransportFacilityData transportFacilityOrigin = new TransportFacilityData();
		final TransportFacilityData transportFacilityDestination = new TransportFacilityData();

		final LocationData locationOrigin = new LocationData();
		final LocationData locationDestination = new LocationData();
		travelSectorData.setCode(travelSector.getCode());
		if (travelSector.getOrigin() != null)

		{
			transportFacilityOrigin.setCode(travelSector.getOrigin().getCode());
			transportFacilityOrigin.setName(travelSector.getOrigin().getName());
			if (travelSector.getOrigin().getLocation() != null)
			{
				locationOrigin.setCode(travelSector.getOrigin().getLocation().getCode());
				locationOrigin.setName(travelSector.getOrigin().getLocation().getName());

			}
			transportFacilityOrigin.setLocation(locationOrigin);
			travelSectorData.setOrigin(transportFacilityOrigin);
		}
		if (travelSector.getDestination() != null)

		{
			transportFacilityDestination.setCode(travelSector.getDestination().getCode());
			transportFacilityDestination.setName(travelSector.getDestination().getName());
			if (travelSector.getDestination().getLocation() != null)
			{
				locationDestination.setCode(travelSector.getDestination().getLocation().getCode());
				locationDestination.setName(travelSector.getDestination().getLocation().getName());
			}
			transportFacilityDestination.setLocation(locationDestination);
			travelSectorData.setDestination(transportFacilityDestination);
		}
		return travelSectorData;
	}

	private TransportVehicleData populateTransportVehicle(final TransportVehicleModel transportVehicle)
	{
		final TransportVehicleData transportVehicleData = new TransportVehicleData();
		transportVehicleData.setCode(transportVehicle.getCode());
		if(Objects.nonNull(transportVehicle.getTransportVehicleInfo()))
		{
			transportVehicleData.setName(transportVehicle.getTransportVehicleInfo().getName());
		}
		return transportVehicleData;

	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}
}
