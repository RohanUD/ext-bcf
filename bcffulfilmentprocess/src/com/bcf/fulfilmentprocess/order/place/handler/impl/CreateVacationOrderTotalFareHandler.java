/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.TotalFareData;


public class CreateVacationOrderTotalFareHandler implements CreateOrderNotificationGlobalHandler
{
	private NotificationUtils notificationUtils;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
	{
		populatePriceData(abstractOrderModel, createOrderNotificationData.getPackageReservations().getPackageReservationDatas().get(0));
	}

	private void populatePriceData(final AbstractOrderModel abstractOrderModel, final PackageReservationData packageReservationData)
	{
		final TotalFareData totalFareData = new TotalFareData();

		totalFareData.setTotal(abstractOrderModel.getTotalPrice());
		totalFareData.setTax(abstractOrderModel.getTotalTax());
		packageReservationData.setTotalFare(totalFareData);
	}

	protected NotificationUtils getNotificationUtils() {
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils) {
		this.notificationUtils = notificationUtils;
	}

}
