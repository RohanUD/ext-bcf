/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;


import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;


public class ReservationItemDataHandler implements ReservationDataNotificationHandler
{

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> cartEntriesByJourneyReference,
			final ReservationData reservationData)
	{
		final Map<Integer, List<AbstractOrderEntryModel>> entriesByOdRefNumber = cartEntriesByJourneyReference.stream()
				.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));

		List<ReservationItemData> reservationItemDataList = new ArrayList<>();
		for (final Integer odRefNumber : entriesByOdRefNumber.keySet()) //NOSONAR
		{
			final ReservationItemData reservationItemData = new ReservationItemData();
			reservationItemData.setOriginDestinationRefNumber(odRefNumber);
			final List<AbstractOrderEntryModel> entriesPerOdRefNumber = entriesByOdRefNumber.get(odRefNumber);
			populateEbookingReference(entriesPerOdRefNumber, reservationItemData);
			populateTransferSailingIdentifier(entriesPerOdRefNumber, reservationItemData);
			populateCarryingDangerousGoods(entriesPerOdRefNumber, reservationItemData);
			populateStopOverLocation(entriesPerOdRefNumber, reservationItemData);
			reservationItemDataList.add(reservationItemData);
		}
		reservationData.setReservationItem(reservationItemDataList);
	}

	private void populateStopOverLocation(final List<AbstractOrderEntryModel> entries,
			final ReservationItemData reservationItemData)
	{
		if(CollectionUtils.isNotEmpty(entries.get(0).getTravelOrderEntryInfo().getTransportOfferings()) &&
			CollectionUtils.size(entries.get(0).getTravelOrderEntryInfo().getTransportOfferings()) > 1)
		{
			final List<String> stopOverLocations = entries.get(0).getTravelOrderEntryInfo()
					.getTravelRoute().getTravelSector().stream().map(TravelSectorModel::getDestination)
					.map(TransportFacilityModel::getCode).collect(Collectors.toList());
			reservationItemData.setStopOverAndTransferLocations(stopOverLocations);
		}
	}

	protected void populateCarryingDangerousGoods(final List<AbstractOrderEntryModel> orderEntries,
			final ReservationItemData reservationItemData)
	{
		if ((orderEntries.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods()))
		{
			reservationItemData.setCarryingDangerousGood(orderEntries.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods());
		}
	}

	protected void populateTransferSailingIdentifier(final List<AbstractOrderEntryModel> orderEntries,
			final ReservationItemData reservationItemData)
	{
		if (StringUtils.isNotEmpty(orderEntries.get(0).getTravelOrderEntryInfo().getTransferSailingIdentifier()))
		{
			reservationItemData.setTransferSailing(orderEntries.get(0).getTravelOrderEntryInfo().getTransferSailingIdentifier());
		}
	}

	protected void populateEbookingReference(final List<AbstractOrderEntryModel> orderEntries,
			ReservationItemData reservationItemData)
	{
		if (StringUtils.isNotEmpty(orderEntries.get(0).getBookingReference()))
		{
			reservationItemData.seteBookingReference(orderEntries.get(0).getBookingReference());
		}
	}


}
