/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.OrderModel;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;
import com.bcf.notification.request.order.financial.FinancialData;


public class AbstractOrderFinancialDataHandler
{
	private Converter<OrderModel,FinancialData> financialDataConverter;

	protected FinancialData populateFinancialData(final OrderModel orderModel)
	{
		return getFinancialDataConverter().convert(orderModel);

	}

	protected Converter<OrderModel, FinancialData> getFinancialDataConverter()
	{
		return financialDataConverter;
	}
	@Required
	public void setFinancialDataConverter(
			final Converter<OrderModel, FinancialData> financialDataConverter)
	{
		this.financialDataConverter = financialDataConverter;
	}
}
