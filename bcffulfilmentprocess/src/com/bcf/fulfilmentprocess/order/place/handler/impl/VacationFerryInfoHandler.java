/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.service.BcfTermsAndConditionsService;
import com.bcf.core.services.BCFTravellerService;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.notification.request.order.createorder.FerryInfo;
import com.bcf.notification.request.order.createorder.FerryJourneyInfo;
import com.bcf.notification.request.order.createorder.LocationData;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.PassengerTypeData;
import com.bcf.notification.request.order.createorder.PassengerTypeQuantityData;
import com.bcf.notification.request.order.createorder.TermsAndConditionsData;
import com.bcf.notification.request.order.createorder.TransportFacilityData;
import com.bcf.notification.request.order.createorder.TransportVehicleData;
import com.bcf.notification.request.order.createorder.TravelRouteData;
import com.bcf.notification.request.order.createorder.TravelSectorData;
import com.bcf.notification.request.order.createorder.TravelersData;
import com.bcf.notification.request.order.createorder.VehicleTypeData;
import com.bcf.notification.request.order.createorder.VehicleTypeQuantityData;


public class VacationFerryInfoHandler implements VacationOrderCreateNotificationHandler
{
	private BCFTravellerService travellerService;
	private BcfTermsAndConditionsService bcfTermsAndConditionsService;
	private ConfigurationService configurationService;


	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> abstractOrderEntries,
			final PackageReservationData packageReservationData)
	{
		final Map<String, TermsAndConditionsData> termsAndConditionsDataMap = new HashMap<>();
		packageReservationData.setFerryJourneyInfoList(
				populateFerryInfo(abstractOrderEntries,
						termsAndConditionsDataMap));
		packageReservationData.setTermsAndConditionMap(termsAndConditionsDataMap);
	}

	private List<FerryJourneyInfo> populateFerryInfo(final List<AbstractOrderEntryModel> cartEntriesByTypeTransport,
			final Map<String, TermsAndConditionsData> termsAndConditionsDataMap)
	{
		final List<FerryJourneyInfo> ferryJourneyInfoList = new ArrayList<>();
		final List<FerryInfo> listOfFerryInfo = new ArrayList<>();


		Map<Integer, List<AbstractOrderEntryModel>> entriesByJourney = StreamUtil.safeStream(cartEntriesByTypeTransport)
				.collect(Collectors.groupingBy(e -> e.getJourneyReferenceNumber()));


		for (Map.Entry<Integer, List<AbstractOrderEntryModel>> journeyMapEntry : entriesByJourney.entrySet())
		{
			FerryJourneyInfo ferryJourneyInfo = new FerryJourneyInfo();
			final Map<Integer, List<AbstractOrderEntryModel>> entriesByODRefNumber = StreamUtil
					.safeStream(journeyMapEntry.getValue())
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));

			for (final Integer odRefNumber : entriesByODRefNumber.keySet()) //NOSONAR
			{
				final FerryInfo ferryInfo = new FerryInfo();
				final Optional<AbstractOrderEntryModel> orderEntryModelOptional = entriesByODRefNumber.get(odRefNumber).stream()
						.findFirst();
				if (orderEntryModelOptional.isPresent())
				{
					final AbstractOrderEntryModel orderEntryModel = orderEntryModelOptional.get();
					final List<TransportOfferingModel> transportOfferingList = new ArrayList<>(
							orderEntryModel.getTravelOrderEntryInfo()
									.getTransportOfferings());
					ferryInfo.setDepartureSailing(transportOfferingList.get(0).getDepartureTime());
					ferryInfo.seteBookingReferenceNumber(orderEntryModel.getBookingReference());
					populateRouteData(ferryInfo, orderEntryModel, transportOfferingList);
					populateVehilceData(ferryInfo, orderEntryModel);
					populateTermsAndConditions(ferryInfo, orderEntryModel, termsAndConditionsDataMap);
					ferryInfo.setRoute(createRouteString(ferryInfo.getTravelRoute()));
					ferryInfo.setOpenTicket(orderEntryModel.getTravelOrderEntryInfo().isOpenTicket());
					ferryInfo.setOriginDestinationRefNumber(orderEntryModel.getTravelOrderEntryInfo().getOriginDestinationRefNumber());
				}
				populateTravelers(ferryInfo, entriesByODRefNumber.get(odRefNumber));
				listOfFerryInfo.add(ferryInfo);
			}
			ferryJourneyInfo.setJourneyRefNumber(journeyMapEntry.getKey());
			ferryJourneyInfo.setFerryInfoList(listOfFerryInfo);
			ferryJourneyInfoList.add(ferryJourneyInfo);
		}
		return ferryJourneyInfoList;

	}

	protected void populateTermsAndConditions(final FerryInfo ferryInfo, final AbstractOrderEntryModel orderEntryModel,
			final Map<String, TermsAndConditionsData> termsAndConditionsDataMap)
	{
		final Optional<AmountToPayInfoModel> amountToPayInfoModelOptional = orderEntryModel.getOrder().getAmountToPayInfos()
				.stream().filter(amountToPayInfoModel -> Objects.nonNull(amountToPayInfoModel.getBookingReference()))
				.filter(amountToPayInfoModel -> amountToPayInfoModel.getBookingReference()
						.equals(ferryInfo.geteBookingReferenceNumber())).findFirst();

		if (amountToPayInfoModelOptional.isPresent())
		{
			final AmountToPayInfoModel amountToPayInfoModel = amountToPayInfoModelOptional.get();
			final String termsAndConditionsCode = amountToPayInfoModel.getTermsAndConditionsCode();
			FerryTandCModel ferryTandCModel = getBcfTermsAndConditionsService()
					.getTermsAndConditionsModel(termsAndConditionsCode);
			if (!isAlreadyPresent(ferryTandCModel, termsAndConditionsDataMap))
			{
				TermsAndConditionsData termsAndConditionsData = new TermsAndConditionsData();
				termsAndConditionsData.setAdditionalcheckInText(ferryTandCModel.getAdditionalCheckInText());
				termsAndConditionsData.setCheckInText(ferryTandCModel.getCheckInText());
				termsAndConditionsData.setAdditionalcheckInTitle(ferryTandCModel.getAdditionalCheckInTitle());
				if (Objects.nonNull(ferryTandCModel.getImage()))
				{
					termsAndConditionsData.setImage(ferryTandCModel.getImage().getURL());
				}
				termsAndConditionsData.setCode(ferryTandCModel.getCode());
				ferryInfo.setTermsAndConditions(termsAndConditionsData);
				termsAndConditionsDataMap.put(ferryInfo.geteBookingReferenceNumber(), termsAndConditionsData);
			}
		}
	}

	private boolean isAlreadyPresent(final FerryTandCModel ferryTandCModel,
			final Map<String, TermsAndConditionsData> termsAndConditionsDataMap)
	{
		if (MapUtils.isEmpty(termsAndConditionsDataMap))
		{
			return Boolean.FALSE;
		}

		final Set<Map.Entry<String, TermsAndConditionsData>> termsAndConditionsEntrySet = termsAndConditionsDataMap
				.entrySet();

		for (Map.Entry<String, TermsAndConditionsData> termsAndConditionsDataEntry : termsAndConditionsEntrySet)
		{
			if (ferryTandCModel.getCode()
					.equals(termsAndConditionsDataEntry.getValue().getCode()))
			{
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	protected void populateTravelers(final FerryInfo ferryInfo, final List<AbstractOrderEntryModel> orderEntries)
	{
		final List<TravellerModel> travellingPassengersAndVehicles = getTravellerService().getTravellers(orderEntries);
		final List<TravelersData> travellers = new ArrayList<>();
		travellingPassengersAndVehicles.forEach(traveller -> {
			if (traveller.getInfo() instanceof PassengerInformationModel)
			{
				final TravelersData ferryTraveller = new TravelersData();
				final PassengerInformationModel passengerInformationModel = (PassengerInformationModel) traveller.getInfo();
				populatePassengerTypes(passengerInformationModel, ferryInfo);
				ferryTraveller.setFirstName(passengerInformationModel.getFirstName());
				ferryTraveller.setSurname(passengerInformationModel.getSurname());
				travellers.add(ferryTraveller);
				ferryInfo.setTravellers(travellers);
			}
			else if (traveller.getInfo() instanceof BCFVehicleInformationModel)
			{
				final BCFVehicleInformationModel bcfVehicleInformationModel = (BCFVehicleInformationModel) traveller.getInfo();
				final VehicleTypeQuantityData vehicleData = new VehicleTypeQuantityData();
				vehicleData.setLength(bcfVehicleInformationModel.getLength());
				vehicleData.setHeight(bcfVehicleInformationModel.getHeight());
				vehicleData.setWidth(bcfVehicleInformationModel.getWidth());
				final VehicleTypeData vehicleTypeData = new VehicleTypeData();
				vehicleTypeData.setCode(bcfVehicleInformationModel.getVehicleType().getType().getCode());
				vehicleTypeData.setName(bcfVehicleInformationModel.getVehicleType().getName());
				vehicleData.setVehicleType(vehicleTypeData);
				ferryInfo.setVehicleData(vehicleData);
			}
		});
	}

	protected void populatePassengerTypes(final PassengerInformationModel passengerInformationModel, final FerryInfo ferryInfo)
	{
		if (CollectionUtils.isNotEmpty(ferryInfo.getPassengerTypes()) && ferryInfo.getPassengerTypes()
				.stream().anyMatch(passenger -> passengerInformationModel.getPassengerType().getEBookingCode()
						.equals(passenger.getPassengerType().getCode())))
		{
			PassengerTypeQuantityData passengerTypeQtyData = ferryInfo.getPassengerTypes().stream()
					.filter(passenger -> passengerInformationModel.getPassengerType().getEBookingCode()
							.equals(passenger.getPassengerType().getCode())).findFirst().get();
			passengerTypeQtyData.setQuantity(passengerTypeQtyData.getQuantity() + 1);
		}
		if (CollectionUtils.isEmpty(ferryInfo.getPassengerTypes()))
		{
			final List<PassengerTypeQuantityData> passengerTypeQuantityDataList = new ArrayList<>();
			ferryInfo.setPassengerTypes(passengerTypeQuantityDataList);
			ferryInfo.getPassengerTypes().add(createPassengerTypeQuantityData(passengerInformationModel));
		}
	}

	protected PassengerTypeQuantityData createPassengerTypeQuantityData(final PassengerInformationModel passengerInformationModel)
	{
		final PassengerTypeQuantityData PassengerTypeQuantityData = new PassengerTypeQuantityData();
		final PassengerTypeData passengerTypeData = new PassengerTypeData();
		final PassengerTypeModel passengerTypeModel = passengerInformationModel.getPassengerType();
		passengerTypeData.setCode(passengerTypeModel.getEBookingCode());
		passengerTypeData.setName(passengerTypeModel.getName());
		PassengerTypeQuantityData.setPassengerType(passengerTypeData);
		PassengerTypeQuantityData.setQuantity(1);
		return PassengerTypeQuantityData;
	}

	protected String createRouteString(final TravelRouteData travelRoute)
	{
		return CollectionUtils.size(travelRoute.getSectors()) > 1 ?
				String.join("- ", travelRoute.getSectors().stream().map(sector -> sector.getDestination().getName()).collect(
						Collectors.toList())) :
				StringUtils.EMPTY;
	}

	protected void populateVehilceData(final FerryInfo ferryInfo, final AbstractOrderEntryModel orderEntryModel)
	{
		final List<TransportVehicleData> vehicles = new ArrayList<>();
		ferryInfo.getTravelRoute().getSectors().forEach(sector -> {
			final Optional<TransportOfferingModel> toOptional = orderEntryModel.getTravelOrderEntryInfo().getTransportOfferings()
					.stream().filter(to -> sector.getCode().equals(to.getTravelSector().getCode())).findFirst();
			if (toOptional.isPresent())
			{
				final TransportOfferingModel transportOffering = toOptional.get();
				final TransportVehicleModel transportVehicle = transportOffering.getTransportVehicle();
				final TransportVehicleData vehicleData = new TransportVehicleData();
				vehicleData.setCode(transportVehicle.getTransportVehicleInfo().getCode());
				vehicleData.setName(transportVehicle.getTransportVehicleInfo().getName());
				vehicles.add(vehicleData);

			}
		});

		ferryInfo.setVehicles(vehicles);
	}

	protected void populateRouteData(final FerryInfo ferryInfo, final AbstractOrderEntryModel orderEntryModel,
			final List<TransportOfferingModel> transportOfferingList)
	{
		final TravelRouteModel travelRoute = orderEntryModel.getTravelOrderEntryInfo().getTravelRoute();
		final TravelRouteData route = new TravelRouteData();

		final TransportFacilityData origin = new TransportFacilityData();
		final TransportFacilityData destination = new TransportFacilityData();

		route.setCode(travelRoute.getCode());
		route.setName(travelRoute.getName());

		final TransportFacilityModel originTransportFacility = travelRoute.getOrigin();
		final TransportFacilityModel destinationTransportFacility = travelRoute.getDestination();

		origin.setCode(originTransportFacility.getCode());
		origin.setName(originTransportFacility.getName());

		final LocationData routeOriginLocaltion = new LocationData();
		final LocationModel originLocation = getGeographicalLocation(originTransportFacility.getLocation());
		routeOriginLocaltion.setCode(originLocation.getCode());
		routeOriginLocaltion.setName(originLocation.getName());
		origin.setLocation(routeOriginLocaltion);

		destination.setCode(destinationTransportFacility.getCode());
		destination.setName(destinationTransportFacility.getName());

		final LocationData routeDestinationLocation = new LocationData();
		final LocationModel destinationLocation = getGeographicalLocation(destinationTransportFacility.getLocation());
		routeDestinationLocation.setCode(destinationLocation.getCode());
		routeDestinationLocation.setName(destinationLocation.getName());
		destination.setLocation(routeDestinationLocation);

		route.setOrigin(origin);
		route.setDestination(destination);

		popuateSectors(route, transportOfferingList);

		ferryInfo.setTravelRoute(route);
	}

	protected void popuateSectors(final TravelRouteData route, final List<TransportOfferingModel> transportOfferingList)
	{
		final List<TravelSectorData> sectors = new ArrayList<>();
		transportOfferingList.forEach(transportOffering -> {
			final TravelSectorModel travelSectorModel = transportOffering.getTravelSector();
			TravelSectorData sector = new TravelSectorData();
			sector.setCode(travelSectorModel.getCode());
			final TransportFacilityModel originFacility = travelSectorModel.getOrigin();
			final TransportFacilityModel destinationFacility = travelSectorModel.getDestination();

			TransportFacilityData origin = new TransportFacilityData();
			origin.setCode(originFacility.getCode());
			origin.setName(originFacility.getName());

			TransportFacilityData destination = new TransportFacilityData();
			destination.setCode(destinationFacility.getCode());
			destination.setName(destinationFacility.getName());
			sector.setOrigin(origin);
			sector.setDestination(destination);
			sectors.add(sector);
		});
		route.setSectors(sectors);
	}

	protected LocationModel getGeographicalLocation(final LocationModel location)
	{
		if (LocationType.GEOGRAPHICAL_AREA.equals(location.getLocationType()))
		{
			return location;
		}

		if (CollectionUtils.isNotEmpty(location.getSuperlocations()))
		{
			return getGeographicalLocation(location.getSuperlocations().get(0));
		}

		return null;
	}

	protected BCFTravellerService getTravellerService()
	{
		return travellerService;
	}

	@Required
	public void setTravellerService(final BCFTravellerService travellerService)
	{
		this.travellerService = travellerService;
	}

	public BcfTermsAndConditionsService getBcfTermsAndConditionsService()
	{
		return bcfTermsAndConditionsService;
	}

	@Required
	public void setBcfTermsAndConditionsService(final BcfTermsAndConditionsService bcfTermsAndConditionsService)
	{
		this.bcfTermsAndConditionsService = bcfTermsAndConditionsService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
