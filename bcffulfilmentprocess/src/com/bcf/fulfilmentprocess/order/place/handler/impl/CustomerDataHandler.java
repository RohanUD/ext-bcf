/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.Objects;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.fulfilmentprocess.order.util.CustomerUtils;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;


public class CustomerDataHandler implements CreateOrderNotificationGlobalHandler
{
	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
	{
		if (Objects.nonNull(abstractOrderModel.getUser()) && abstractOrderModel.getUser() instanceof CustomerModel)
		{

			final CustomerData customerData = new CustomerData();
			final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
			CustomerUtils.populateCustomerData(abstractOrderModel,customerModel,customerData);
			createOrderNotificationData.setCustomerData(customerData);
		}
	}
}
