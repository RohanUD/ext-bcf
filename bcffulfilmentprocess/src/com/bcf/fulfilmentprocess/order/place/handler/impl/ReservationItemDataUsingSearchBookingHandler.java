/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;


import java.util.ArrayList;
import java.util.List;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataUsingSearchBookingNotificationHandler;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;


public class ReservationItemDataUsingSearchBookingHandler implements ReservationDataUsingSearchBookingNotificationHandler
{
	@Override
	public void handle(final Itinerary itinerary, final ReservationData reservationData)
	{
		final List<ReservationItemData> reservationItems = new ArrayList<>();
		final ReservationItemData reservationItemData = new ReservationItemData();

		reservationItemData.seteBookingReference(itinerary.getBooking().getBookingReference().getBookingReference());
		reservationItems.add(reservationItemData);
		reservationData.setReservationItem(reservationItems);
	}

	protected void populateStopOverLocation(
			final ReservationItemData reservationItemData)
	{
		//No implementation
	}

	protected void populateCarryingDangerousGoods(
			final ReservationItemData reservationItemData)
	{
		//No implementation
	}

	protected void populateTransferSailingIdentifier(
			final ReservationItemData reservationItemData)
	{
		//No implementation
	}

	protected void populateEbookingReference(
			ReservationItemData reservationItemData)
	{
		//No implementation
	}

}
