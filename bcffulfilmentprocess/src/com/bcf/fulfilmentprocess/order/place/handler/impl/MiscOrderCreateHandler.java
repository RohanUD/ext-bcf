/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;


public class MiscOrderCreateHandler implements CreateOrderNotificationGlobalHandler
{
	private static final String MANAGE_BOOKING_URL = "manage-booking/guest-booking-details/?guestBookingIdentifier=";

	private ConfigurationService configurationService;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;



	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
	{
		createOrderNotificationData.setCode(abstractOrderModel.getCode());
		createOrderNotificationData.setCreationDate(abstractOrderModel.getCreationtime());
		if (Objects.nonNull(abstractOrderModel.getGuid()))
		{
			createOrderNotificationData.setOrderGuid(
					configurationService.getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL) + MANAGE_BOOKING_URL
							+ abstractOrderModel.getGuid());
		}

		setSalesApplication(abstractOrderModel, createOrderNotificationData);
	}

	private void setSalesApplication(final AbstractOrderModel abstractOrderModel,
			final CreateOrderNotificationData createOrderNotificationData)
	{
		SalesApplication salesApplication;
		if (abstractOrderModel instanceof OrderModel)
		{
			salesApplication = ((OrderModel) abstractOrderModel).getSalesApplication();
		}
		else
		{
			salesApplication = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		}
		createOrderNotificationData.setSalesApplication(salesApplication.getCode());
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}
}
