/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.core.service.BcfTermsAndConditionsService;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.bcf.notification.request.order.createorder.TermsAndConditionsData;


public class TermsAndConditionDataHandler implements ReservationDataNotificationHandler
{
	private BcfTermsAndConditionsService bcfTermsAndConditionsService;
	private ConfigurationService configurationService;


	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> cartEntriesPerJourneyReference,
			final ReservationData reservationData)
	{
		final Map<Integer, Map<String, TermsAndConditionsData>> termsAndConditionsDataMapByJourneyReference = new HashMap<>();
		final Map<String, TermsAndConditionsData> termsAndConditionsDataMap = new HashMap<>();
		final List<ReservationItemData> reservationItemData = reservationData.getReservationItem();
		if (CollectionUtils.isNotEmpty(reservationItemData))
		{
			reservationItemData.forEach(reservationItem -> {
				getTermsAndCondition(reservationItem, abstractOrderModel, termsAndConditionsDataMap,
						termsAndConditionsDataMapByJourneyReference,reservationData);
			});

		}
		reservationData.setTermsAndConditionMap(termsAndConditionsDataMapByJourneyReference);
	}

	private void getTermsAndCondition(final ReservationItemData reservationItemData, final AbstractOrderModel abstractOrderModel,
			final Map<String, TermsAndConditionsData> termsAndConditionsDataMap,
			final Map<Integer, Map<String, TermsAndConditionsData>> termsAndConditionsDataMapByJourneyReference,final ReservationData reservationData)
	{
		final Optional<AmountToPayInfoModel> amountToPayInfoModelOptional = abstractOrderModel.getAmountToPayInfos().stream()
				.filter(amountToPayInfoModel -> reservationItemData.geteBookingReference()
						.equals(amountToPayInfoModel.getBookingReference())).findFirst();
		if (amountToPayInfoModelOptional.isPresent())
		{
			final AmountToPayInfoModel amountToPayInfoModel = amountToPayInfoModelOptional.get();
			final String termsAndConditionsCode = amountToPayInfoModel.getTermsAndConditionsCode();
			FerryTandCModel ferryTandCModel = getBcfTermsAndConditionsService()
					.getTermsAndConditionsModel(termsAndConditionsCode);
			if (!isAlreadyPresent(ferryTandCModel,termsAndConditionsDataMapByJourneyReference,reservationData))
			{
				TermsAndConditionsData termsAndConditionsData = new TermsAndConditionsData();
				termsAndConditionsData.setCheckInTitle(ferryTandCModel.getCheckInTitle());
				termsAndConditionsData.setCheckInText(ferryTandCModel.getCheckInText());
				termsAndConditionsData.setAdditionalcheckInTitle(ferryTandCModel.getAdditionalCheckInTitle());
				termsAndConditionsData.setAdditionalcheckInText(ferryTandCModel.getAdditionalCheckInText());
				if(Objects.nonNull(ferryTandCModel.getImage()))
				{
					termsAndConditionsData.setImage(ferryTandCModel.getImage().getURL().substring(1));
				}
				termsAndConditionsData.setCode(ferryTandCModel.getCode());
				reservationItemData.setFerryTandCData(termsAndConditionsData);
				termsAndConditionsDataMap.put(reservationItemData.geteBookingReference(), termsAndConditionsData);
				termsAndConditionsDataMapByJourneyReference.put(reservationData.getJourneyReferenceNumber(),termsAndConditionsDataMap);

			}
		}
	}


	protected boolean isAlreadyPresent(final FerryTandCModel ferryTandCModel,
			final Map<Integer, Map<String, TermsAndConditionsData>> termsAndConditionsDataMapByJourneyReference,final ReservationData reservationData)
	{
		Map<String, TermsAndConditionsData> termsAndConditionsDataMap = termsAndConditionsDataMapByJourneyReference.get(reservationData.getJourneyReferenceNumber());

		if (MapUtils.isEmpty(termsAndConditionsDataMap))
		{
			return Boolean.FALSE;
		}

		final Set<Map.Entry<String, TermsAndConditionsData>> termsAndConditionsEntrySet = termsAndConditionsDataMap
				.entrySet();

		for (Map.Entry<String, TermsAndConditionsData> termsAndConditionsDataEntry : termsAndConditionsEntrySet)
		{
			if (ferryTandCModel.getCode()
					.equals(termsAndConditionsDataEntry.getValue().getCode()))
			{
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	protected BcfTermsAndConditionsService getBcfTermsAndConditionsService()
	{
		return bcfTermsAndConditionsService;
	}

	@Required
	public void setBcfTermsAndConditionsService(final BcfTermsAndConditionsService bcfTermsAndConditionsService)
	{
		this.bcfTermsAndConditionsService = bcfTermsAndConditionsService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
