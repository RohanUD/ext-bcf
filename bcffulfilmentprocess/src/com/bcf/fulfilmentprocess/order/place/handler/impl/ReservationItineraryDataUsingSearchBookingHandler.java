/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.travel.TravelSectorModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.services.BcfTransportOfferingService;
import com.bcf.core.travelroute.service.BCFTravelRouteService;
import com.bcf.core.travelsector.service.TravelSectorService;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataUsingSearchBookingNotificationHandler;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.integration.common.data.SailingLine;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.order.createorder.ItineraryData;
import com.bcf.notification.request.order.createorder.LocationData;
import com.bcf.notification.request.order.createorder.OriginDestinationOptionData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.bcf.notification.request.order.createorder.TransportFacilityData;
import com.bcf.notification.request.order.createorder.TransportOfferingData;
import com.bcf.notification.request.order.createorder.TransportVehicleData;
import com.bcf.notification.request.order.createorder.TravelRouteData;
import com.bcf.notification.request.order.createorder.TravelSectorData;


public class ReservationItineraryDataUsingSearchBookingHandler implements ReservationDataUsingSearchBookingNotificationHandler
{
	private NotificationUtils notificationUtils;
	private BCFTravelRouteService travelRouteService;
	private BcfTransportOfferingService transportOfferingService;
	private TravelSectorService travelSectorService;

	@Override
	public void handle(final Itinerary itinerary, final ReservationData reservationData)
	{
		final List<ReservationItemData> reservationItems = reservationData.getReservationItem();

		final ReservationItemData reservationItemData = reservationItems.get(0);
		final ItineraryData reservationItinerary = new ItineraryData();
		final TravelRouteModel travelRoute = fetchTravelRoute(itinerary);
		final TravelRouteData routeData = new TravelRouteData();
		routeData.setCode(travelRoute.getCode());
		routeData.setName(travelRoute.getName());
		reservationItinerary.setRoute(routeData);

		final List<OriginDestinationOptionData> originDestinationOptions = new ArrayList<>();
		final OriginDestinationOptionData originDestinationOptionData = new OriginDestinationOptionData();
		final List<TransportOfferingModel> transportOfferingModels = fetchTransportOfferings(itinerary);
		populateOdOptionWithTransportOfferings(transportOfferingModels, originDestinationOptionData);
		originDestinationOptions.add(originDestinationOptionData);
		reservationItinerary.setOriginDestinationOptions(originDestinationOptions);

		reservationItemData.setReservationItinerary(reservationItinerary);
	}

	private TravelSectorData populateSector(final TravelSectorModel travelSector)
	{
		final TravelSectorData travelSectorData = new TravelSectorData();
		final TransportFacilityData transportFacilityOrigin = new TransportFacilityData();
		final TransportFacilityData transportFacilityDestination = new TransportFacilityData();

		final LocationData locationOrigin = new LocationData();
		final LocationData locationDestination = new LocationData();
		travelSectorData.setCode(travelSector.getCode());
		if (travelSector.getOrigin() != null)

		{
			transportFacilityOrigin.setCode(travelSector.getOrigin().getCode());
			transportFacilityOrigin.setName(travelSector.getOrigin().getName());
			if (travelSector.getOrigin().getLocation() != null)
			{
				locationOrigin.setCode(travelSector.getOrigin().getLocation().getCode());
				locationOrigin.setName(travelSector.getOrigin().getLocation().getName());

			}
			transportFacilityOrigin.setLocation(locationOrigin);
			travelSectorData.setOrigin(transportFacilityOrigin);
		}
		if (travelSector.getDestination() != null)

		{
			transportFacilityDestination.setCode(travelSector.getDestination().getCode());
			transportFacilityDestination.setName(travelSector.getDestination().getName());
			if (travelSector.getDestination().getLocation() != null)
			{
				locationDestination.setCode(travelSector.getDestination().getLocation().getCode());
				locationDestination.setName(travelSector.getDestination().getLocation().getName());
			}
			transportFacilityDestination.setLocation(locationDestination);
			travelSectorData.setDestination(transportFacilityDestination);
		}
		return travelSectorData;
	}

	protected TravelRouteModel fetchTravelRoute(final Itinerary itinerary)
	{
		final List<SailingLine> lines = itinerary.getSailing().getLine();
		final String originCode = lines.stream().findFirst().get().getLineInfo().getDeparturePortCode();
		final String destinationCode = lines.stream().reduce((a, b) -> b).get().getLineInfo().getArrivalPortCode();
		final List<TravelRouteModel> travelRoutes = getTravelRouteService().getTravelRoutes(originCode, destinationCode);
		return travelRoutes.stream().findFirst().orElse(null);
	}

	protected List<TransportOfferingModel> fetchTransportOfferings(final Itinerary itinerary)
	{
		final List<SailingLine> lines = itinerary.getSailing().getLine();
		final List<TransportOfferingModel> transportOfferings = new ArrayList<>();
		lines.forEach(line -> {
			final String origin = line.getLineInfo().getDeparturePortCode();
			final String destination = line.getLineInfo().getArrivalPortCode();
			final List<TravelSectorModel> travelSectors = getTravelSectorService().getTravelSectors(origin, destination);
			final Date departureDate = TravelDateUtils
					.getDate(line.getLineInfo().getDepartureDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
			final Date arrivalDate = TravelDateUtils
					.getDate(line.getLineInfo().getArrivalDateTime(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
			if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(travelSectors))
			{
				TransportOfferingModel transportOffering = getTransportOfferingService()
						.getTransportOfferings(departureDate, arrivalDate, travelSectors.get(0));
				if (Objects.isNull(transportOffering))
				{
					transportOffering = getTransportOfferingService()
							.getDefaultTransportOfferingForSector(travelSectors.get(0).getCode());
				}
				transportOfferings.add(transportOffering);
			}
		});
		return transportOfferings;
	}

	protected void populateOdOptionWithTransportOfferings(final List<TransportOfferingModel> transportOfferingModels,
			final OriginDestinationOptionData originDestinationOption)
	{
		final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();
		transportOfferingModels.forEach(source -> {
			final TransportOfferingData transportOfferingData = new TransportOfferingData();
			transportOfferingData.setCode(source.getCode());
			transportOfferingData.setDepartureTime(source.getDepartureTime());
			transportOfferingData.setArrivalTime(source.getArrivalTime());

			transportOfferingData.setDuration(
					source.getDuration() != null ? NotificationUtils.getDurationMap(source.getDuration()) : null);

			if (source.getTravelSector() != null)
			{
				transportOfferingData.setSector(populateSector(source.getTravelSector()));
			}

			transportOfferingData.setTransportVehicle(populateTransportVehicle(source.getTransportVehicle()));
			transportOfferingDataList.add(transportOfferingData);
		});
		originDestinationOption.setTransportOfferings(transportOfferingDataList);
	}

	private TransportVehicleData populateTransportVehicle(final TransportVehicleModel transportVehicle)
	{
		final TransportVehicleData transportVehicleData = new TransportVehicleData();
		transportVehicleData.setCode(transportVehicle.getCode());
		transportVehicleData.setName(transportVehicle.getTransportVehicleInfo().getName());
		return transportVehicleData;

	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}

	protected BCFTravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final BCFTravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected BcfTransportOfferingService getTransportOfferingService()
	{
		return transportOfferingService;
	}

	@Required
	public void setTransportOfferingService(final BcfTransportOfferingService transportOfferingService)
	{
		this.transportOfferingService = transportOfferingService;
	}

	protected TravelSectorService getTravelSectorService()
	{
		return travelSectorService;
	}

	@Required
	public void setTravelSectorService(final TravelSectorService travelSectorService)
	{
		this.travelSectorService = travelSectorService;
	}
}
