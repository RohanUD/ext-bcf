/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import static com.bcf.core.services.impl.DefaultBcfCalculationService.CENTS_TO_DOLLAR_CONVERSION;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.ProductType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.services.TravellerService;
import de.hybris.platform.util.DiscountValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.FareDetailModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.createorder.FerryOptionData;
import com.bcf.notification.request.order.createorder.ItineraryPricingInfoData;
import com.bcf.notification.request.order.createorder.OrderProductData;
import com.bcf.notification.request.order.createorder.PTCFareBreakdownData;
import com.bcf.notification.request.order.createorder.PassengerFareData;
import com.bcf.notification.request.order.createorder.PassengerTypeData;
import com.bcf.notification.request.order.createorder.PassengerTypeQuantityData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.bcf.notification.request.order.createorder.ReservationPricingInfoData;
import com.bcf.notification.request.order.createorder.TotalFareData;
import com.bcf.notification.request.order.createorder.TravelersData;
import com.bcf.notification.request.order.createorder.VehicleFareBreakdownData;
import com.bcf.notification.request.order.createorder.VehicleTypeData;
import com.bcf.notification.request.order.createorder.VehicleTypeQuantityData;


public class ReservationPricingInfoHandler implements ReservationDataNotificationHandler
{
	private static final String PORT_AUTHORITY_FEE_FOR_ADULT_CODE = "NFA";
	private static final String PORT_AUTHORITY_FEE_FOR_CHILD_CODE = "NFC";
	private TravellerService travellerService;
	private NotificationUtils notificationUtils;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> cartEntriesByJourneyReference,
			final ReservationData reservationData)
	{
		List<ReservationItemData> reservationItemData = reservationData.getReservationItem();
		if (CollectionUtils.isNotEmpty(reservationItemData))
		{
			final Map<Integer, List<AbstractOrderEntryModel>> entriesByOdRefNumber = cartEntriesByJourneyReference.stream()
					.collect(Collectors.groupingBy(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()));
			reservationItemData.forEach(reservationItemDatas -> {
				List<AbstractOrderEntryModel> transportEntries = entriesByOdRefNumber
						.get(reservationItemDatas.getOriginDestinationRefNumber());
				reservationItemDatas
						.setReservationPricingInfo(populateReservationPricingInfoData(transportEntries, abstractOrderModel));

			});
		}
	}

	private ReservationPricingInfoData populateReservationPricingInfoData(
			final List<AbstractOrderEntryModel> orderEntriesByODReference, AbstractOrderModel abstractOrderModel)
	{
		final ReservationPricingInfoData reservationPricingInfoData = new ReservationPricingInfoData();
		final List<AncillaryProductModel> ancillaryProducts = orderEntriesByODReference.stream()
				.filter(entry -> AncillaryProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.map(entry -> (AncillaryProductModel) entry.getProduct()).collect(Collectors.toList());
		final List<AbstractOrderEntryModel> ancillaryProductsEntries = orderEntriesByODReference.stream()
				.filter(entry -> entry.getProduct() instanceof AncillaryProductModel).collect(
						Collectors.toList());

		for (final AncillaryProductModel ancillaryProduct : ancillaryProducts)
		{
			reservationPricingInfoData.setFerryOption(populateFerryOption(ancillaryProduct));
			populateFerryOptionWithFare(ancillaryProductsEntries, reservationPricingInfoData);

		}
		reservationPricingInfoData
				.setTotalFare(populateTotalFareForAncillary(orderEntriesByODReference, abstractOrderModel));
		reservationPricingInfoData
				.setItineraryPricingInfo(populateItineraryPricingInfoData(orderEntriesByODReference));

		return reservationPricingInfoData;
	}



	private ItineraryPricingInfoData populateItineraryPricingInfoData(
			final List<AbstractOrderEntryModel> orderEntriesByODReference)
	{
		final ItineraryPricingInfoData itineraryPricingInfoData = new ItineraryPricingInfoData();
		final List<TravellerModel> travellingPassengersAndVehicles = getTravellerService()
				.getTravellers(orderEntriesByODReference);

		for (final TravellerModel travellingPassengersAndVehicle : travellingPassengersAndVehicles)
		{
			if (travellingPassengersAndVehicle.getInfo() instanceof PassengerInformationModel)
			{
				createOrUpdatePTCBreakdowns(itineraryPricingInfoData, travellingPassengersAndVehicle);
				updatePTCBreakdownWithFare(itineraryPricingInfoData, orderEntriesByODReference);
			}
			else if (travellingPassengersAndVehicle.getInfo() instanceof BCFVehicleInformationModel)
			{
				createOrUpdateVehicleBreakdowns(itineraryPricingInfoData, travellingPassengersAndVehicle);
				updateVehicleBreakdownWithFare(itineraryPricingInfoData, orderEntriesByODReference);
			}
		}
		return itineraryPricingInfoData;
	}

	protected void updateVehicleBreakdownWithFare(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<AbstractOrderEntryModel> orderEntriesByODReference)
	{
		for (final VehicleFareBreakdownData vehicleFareBreakdownData : itineraryPricingInfoData.getVehicleFareBreakdownDatas())
		{
			final String vehicleCode = vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode();
			final Double totalFare = orderEntriesByODReference.stream().filter(entry ->
					entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get()
							.getInfo() instanceof BCFVehicleInformationModel
							&& vehicleCode.equals(
							((BCFVehicleInformationModel) entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get()
									.getInfo())
									.getVehicleType().getType().getCode()))
					.mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();

			vehicleFareBreakdownData.setVehicleFare(new PassengerFareData(totalFare));
		}
	}

	protected void updatePTCBreakdownWithFare(final ItineraryPricingInfoData itineraryPricingInfoData,
			final List<AbstractOrderEntryModel> orderEntriesByODReference)
	{
		for (final PTCFareBreakdownData ptcFareBreakdownData : itineraryPricingInfoData.getPtcFareBreakdownData())
		{
			final String passengerType = ptcFareBreakdownData.getPassengerTypeQuantity().getPassengerType().getCode();

			List<AbstractOrderEntryModel> entriesByPaxType = orderEntriesByODReference.stream()
					.filter(entry -> !ProductType.FEE.equals(entry.getProduct().getProductType())).filter(entry ->
							entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get()
									.getInfo() instanceof PassengerInformationModel
									&& passengerType.equals(
									((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get()
											.getInfo())
											.getPassengerType().getEBookingCode())).collect(Collectors.toList());

			double totalFare = entriesByPaxType.stream()
					.mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();

			double portAuthorityFees = getPortAuthorityFeesTotal(entriesByPaxType);
			totalFare = totalFare - portAuthorityFees;

			ptcFareBreakdownData.setPassengerFare(new PassengerFareData(totalFare, 0.0d, portAuthorityFees, 0.0d));
		}
	}

	private double getPortAuthorityFeesTotal(final List<AbstractOrderEntryModel> entriesByPaxType)
	{
		double portAuthorityFeeValues;
		portAuthorityFeeValues = entriesByPaxType.stream().flatMap(entry -> entry.getFareDetailList().stream()).
				filter(fareDetailModel ->
						StringUtils.equalsIgnoreCase(PORT_AUTHORITY_FEE_FOR_ADULT_CODE, fareDetailModel.getCode()) || StringUtils
								.equalsIgnoreCase(PORT_AUTHORITY_FEE_FOR_CHILD_CODE, fareDetailModel.getCode())).mapToDouble(
				FareDetailModel::getAmount).sum();
		return portAuthorityFeeValues / 100d;
	}

	protected void createOrUpdatePTCBreakdowns(final ItineraryPricingInfoData itineraryPricingInfoData,
			final TravellerModel travellingPassengersAndVehicle)
	{
		final PassengerInformationModel passengerInformationModel = (PassengerInformationModel) travellingPassengersAndVehicle
				.getInfo();
		final String eBookingPassengerTypeCode = passengerInformationModel.getPassengerType().getEBookingCode();
		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getPtcFareBreakdownData()) && itineraryPricingInfoData
				.getPtcFareBreakdownData().stream()
				.anyMatch(ptcFareBreakdownData -> eBookingPassengerTypeCode
						.equals(ptcFareBreakdownData.getPassengerTypeQuantity().getPassengerType().getCode())))
		{
			updatePTCBreakdown(itineraryPricingInfoData, eBookingPassengerTypeCode, passengerInformationModel);
		}
		else
		{
			if (CollectionUtils.isEmpty(itineraryPricingInfoData.getPtcFareBreakdownData()))
			{
				itineraryPricingInfoData.setPtcFareBreakdownData(new ArrayList<>());
			}
			itineraryPricingInfoData.getPtcFareBreakdownData().add(createPTCBreakdown(passengerInformationModel));
		}
	}

	protected void updatePTCBreakdown(final ItineraryPricingInfoData itineraryPricingInfoData,
			final String eBookingPassengerTypeCode,
			final PassengerInformationModel passengerInformationModel)
	{
		final PTCFareBreakdownData ptcFareBreakdown = itineraryPricingInfoData.getPtcFareBreakdownData().stream()
				.filter(ptcFareBreakdownData -> eBookingPassengerTypeCode
						.equals(ptcFareBreakdownData.getPassengerTypeQuantity().getPassengerType().getCode()))
				.findFirst().get();
		ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getPassengerTypeQuantity().getQuantity() + 1);

		addTraveller(ptcFareBreakdown, passengerInformationModel);
	}

	protected PTCFareBreakdownData createPTCBreakdown(final PassengerInformationModel passengerInformationModel)
	{
		final PTCFareBreakdownData ptcFareBreakdownData = new PTCFareBreakdownData();
		final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
		passengerTypeQuantityData.setQuantity(1);
		passengerTypeQuantityData.setPassengerType(getPassengerTypeData(passengerInformationModel.getPassengerType()));
		ptcFareBreakdownData.setPassengerTypeQuantity(passengerTypeQuantityData);

		addTraveller(ptcFareBreakdownData, passengerInformationModel);
		return ptcFareBreakdownData;
	}

	private void addTraveller(final PTCFareBreakdownData ptcFareBreakdownData,
			final PassengerInformationModel passengerInformationModel)
	{
		String firstName = passengerInformationModel.getFirstName();
		String lastName = passengerInformationModel.getSurname();

		if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName))
		{
			return;
		}

		List<TravelersData> travelersDataList = ptcFareBreakdownData.getTravllers();
		if (CollectionUtils.isEmpty(travelersDataList))
		{
			travelersDataList = new ArrayList<>();
		}


		if (!StreamUtil.safeStream(travelersDataList).anyMatch(
				travelersData -> firstName.equals(travelersData.getFirstName()) && lastName.equals(travelersData.getSurname())))
		{
			final TravelersData travelersData = new TravelersData();
			travelersData.setFirstName(passengerInformationModel.getFirstName());
			travelersData.setSurname(passengerInformationModel.getSurname());
			travelersDataList.add(travelersData);
		}
		ptcFareBreakdownData.setTravllers(travelersDataList);
	}

	protected void createOrUpdateVehicleBreakdowns(final ItineraryPricingInfoData itineraryPricingInfoData,
			final TravellerModel travellingPassengersAndVehicle)
	{
		final BCFVehicleInformationModel bcfVehicleInformationModel = (BCFVehicleInformationModel) travellingPassengersAndVehicle
				.getInfo();
		final String eBookingCodeVehicleType = bcfVehicleInformationModel.getVehicleType().getType().getCode();
		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()) && itineraryPricingInfoData
				.getVehicleFareBreakdownDatas().stream().anyMatch(
						vehicleFareBreakdownData -> eBookingCodeVehicleType
								.equals(vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode())))
		{
			updateVehicleBreakdown(itineraryPricingInfoData, eBookingCodeVehicleType);
		}
		else
		{
			if (CollectionUtils.isEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()))
			{
				itineraryPricingInfoData.setVehicleFareBreakdownDatas(new ArrayList<>());
			}
			itineraryPricingInfoData.getVehicleFareBreakdownDatas().add(createVehicleBreakdown(bcfVehicleInformationModel));
		}
	}

	protected VehicleFareBreakdownData createVehicleBreakdown(final BCFVehicleInformationModel bcfVehicleInformationModel)
	{
		final VehicleFareBreakdownData vehicleFareBreakdownData = new VehicleFareBreakdownData();
		final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeData.setCode(bcfVehicleInformationModel.getVehicleType().getType().getCode());
		vehicleTypeData.setName(bcfVehicleInformationModel.getVehicleType().getName());
		vehicleTypeQuantityData.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityData.setQuantity(1);
		vehicleTypeQuantityData.setWidth(bcfVehicleInformationModel.getWidth());
		vehicleTypeQuantityData.setHeight(bcfVehicleInformationModel.getHeight());
		vehicleTypeQuantityData.setLength(bcfVehicleInformationModel.getLength());
		if (bcfVehicleInformationModel.getWeight() > 0.0d)
		{
			vehicleTypeQuantityData.setWeight(bcfVehicleInformationModel.getWeight());
		}
		if (bcfVehicleInformationModel.getNumberOfAxis() > 0)
		{
			vehicleTypeQuantityData.setNumberOfAxis(bcfVehicleInformationModel.getNumberOfAxis());
		}
		if (bcfVehicleInformationModel.getGroundClearance() > 0)
		{
			vehicleTypeQuantityData.setGroundClearance(bcfVehicleInformationModel.getGroundClearance());
		}
		if (bcfVehicleInformationModel.getAdjustable())
		{
			vehicleTypeQuantityData.setAdjustable(bcfVehicleInformationModel.getAdjustable());
		}
		if (StringUtils.isNotEmpty(bcfVehicleInformationModel.getLicensePlateNumber()))
		{
			vehicleTypeQuantityData.setLicensePlateNumber(bcfVehicleInformationModel.getLicensePlateNumber());
		}
		if (StringUtils.isNotEmpty(bcfVehicleInformationModel.getLicensePlateProvince()))
		{
			vehicleTypeQuantityData.setLicensePlateProvince(bcfVehicleInformationModel.getLicensePlateProvince());
		}
		if (StringUtils.isNotEmpty(bcfVehicleInformationModel.getLicensePlateCountry()))
		{
			vehicleTypeQuantityData.setLicensePlateProvince(bcfVehicleInformationModel.getLicensePlateCountry());
		}
		vehicleFareBreakdownData.setVehicleTypeQuantity(vehicleTypeQuantityData);
		return vehicleFareBreakdownData;
	}

	private void updateVehicleBreakdown(final ItineraryPricingInfoData itineraryPricingInfoData,
			final String eBookingCodeVehicleType)
	{
		final VehicleFareBreakdownData vehicleFareBreakdown = itineraryPricingInfoData.getVehicleFareBreakdownDatas().stream()
				.filter(vehicleFareBreakdownData -> eBookingCodeVehicleType
						.equals(vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType().getCode())).findFirst().get();
		vehicleFareBreakdown.getVehicleTypeQuantity().setQuantity(vehicleFareBreakdown.getVehicleTypeQuantity().getQuantity() + 1);
	}

	private PassengerTypeData getPassengerTypeData(final PassengerTypeModel passengerTypeModel)
	{
		final PassengerTypeData passengerTypeData = new PassengerTypeData();
		passengerTypeData.setCode(passengerTypeModel.getEBookingCode());
		passengerTypeData.setName(passengerTypeModel.getName());
		return passengerTypeData;

	}

	private List<FerryOptionData> populateFerryOption(final AncillaryProductModel product)
	{

		final List<FerryOptionData> ferryOptionList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(ferryOptionList) && ferryOptionList.stream()
				.filter(option -> option.getProduct().getCode().equals(product.getCode())).findAny().isPresent())
		{
			final FerryOptionData ferryOption = ferryOptionList.stream()
					.filter(option -> option.getProduct().getCode().equals(product.getCode())).findFirst().get();
			ferryOption.getProduct().setQty(ferryOption.getProduct().getQty() + 1);
		}
		else
		{
			final FerryOptionData ferryOption = new FerryOptionData();
			final OrderProductData ferryOptionProduct = new OrderProductData();
			ferryOptionProduct.setQty(1);
			ferryOptionProduct.setCode(product.getCode());
			ferryOptionProduct.setName(product.getName());
			ferryOption.setProduct(ferryOptionProduct);
			ferryOptionList.add(ferryOption);
		}
		return ferryOptionList;
	}

	private void populateFerryOptionWithFare(final List<AbstractOrderEntryModel> ancillaryProductsEntries,
			final ReservationPricingInfoData reservationPricingInfoData)
	{
		for (final FerryOptionData ferryOptionData : reservationPricingInfoData.getFerryOption())
		{
			String ancillaryProductCode = ferryOptionData.getProduct().getCode();
			final Double totalFare = ancillaryProductsEntries.stream().filter(
					entry -> entry.getProduct() instanceof AncillaryProductModel && StringUtils
							.equals(ancillaryProductCode, (entry.getProduct().getCode())))
					.mapToDouble(AbstractOrderEntryModel::getTotalPrice).sum();

			ferryOptionData.getProduct().setTotalFare(new TotalFareData(totalFare));
		}

	}

	private TotalFareData populateTotalFareForAncillary(
			final List<AbstractOrderEntryModel> orderEntriesByJourneyReference, final AbstractOrderModel abstractOrderModel)
	{
		double totalPrice = 0d;
		double discount = 0d;
		double fees;
		double payAtTerminal;
		double alreadyPaid;

		for (final AbstractOrderEntryModel entry : orderEntriesByJourneyReference)
		{
			totalPrice += entry.getTotalPrice();
			discount += entry.getDiscountValues().stream().mapToDouble(DiscountValue::getValue).sum();
		}

		fees = StreamUtil.safeStream(orderEntriesByJourneyReference).filter(Objects::nonNull)
				.filter(entry -> ProductType.FEE.equals(entry.getProduct().getProductType()))
				.mapToDouble(entry -> entry.getTotalPrice()).sum();


		String bookingReference = StreamUtil.safeStream(orderEntriesByJourneyReference).filter(Objects::nonNull).findFirst().get()
				.getBookingReference();
		Optional<AmountToPayInfoModel> matchedAmountToPayInfo = StreamUtil.safeStream(abstractOrderModel.getAmountToPayInfos())
				.filter(Objects::nonNull).filter(amountToPayInfo -> amountToPayInfo.getBookingReference().equals(bookingReference))
				.findFirst();

		if (matchedAmountToPayInfo.isPresent())
		{
			AmountToPayInfoModel amountToPayInfo = matchedAmountToPayInfo.get();
			payAtTerminal = amountToPayInfo.getPayAtTerminal() / CENTS_TO_DOLLAR_CONVERSION;
			alreadyPaid = amountToPayInfo.getPreviouslyPaid() / CENTS_TO_DOLLAR_CONVERSION;

			return new TotalFareData(fees, discount, totalPrice, alreadyPaid, payAtTerminal);
		}
		return new TotalFareData(fees, discount, totalPrice);
	}

	public TravellerService getTravellerService()
	{
		return travellerService;
	}

	@Required
	public void setTravellerService(final TravellerService travellerService)
	{
		this.travellerService = travellerService;
	}

	public NotificationUtils getNotificationUtils()
	{
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils)
	{
		this.notificationUtils = notificationUtils;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
