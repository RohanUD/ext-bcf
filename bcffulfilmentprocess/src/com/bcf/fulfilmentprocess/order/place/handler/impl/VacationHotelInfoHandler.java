/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.orderentrygroup.OrderEntryGroupStrategy;
import com.bcf.core.service.BcfAsmCommentsService;
import com.bcf.core.util.StreamUtil;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.notification.request.order.createorder.HotelInfo;
import com.bcf.notification.request.order.createorder.PackageReservationData;


public class VacationHotelInfoHandler implements VacationOrderCreateNotificationHandler
{
	private Map<BookingJourneyType, OrderEntryGroupStrategy> entryGroupStrategyMap;

	private BcfAsmCommentsService bcfAsmCommentsService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final List<AbstractOrderEntryModel> abstractOrderEntries,
			final PackageReservationData packageReservationData)
	{
		final OrderEntryGroupStrategy orderEntryGroupStrategy = getEntryGroupStrategyMap()
				.get(abstractOrderModel.getBookingJourneyType());

		Map<AccommodationOfferingModel, List<AbstractOrderEntryModel>> entriesByHotel = StreamUtil.safeStream(abstractOrderEntries)
				.collect(Collectors.groupingBy(orderEntryGroupStrategy::getAccommodationOffering));

		List<HotelInfo> hotelInfoList = new ArrayList<>();
		for (Map.Entry<AccommodationOfferingModel, List<AbstractOrderEntryModel>> mapEntry : entriesByHotel.entrySet())
		{
			List<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroups = orderEntryGroupStrategy
					.getAccommodationEntryGroup(mapEntry.getValue());

			final HotelInfo hotelInfo = orderEntryGroupStrategy
					.populateHotelInfo(accommodationOrderEntryGroups, mapEntry.getValue().get(0));

			addComments(hotelInfo, mapEntry.getKey().getCode(), abstractOrderModel);
			hotelInfoList.add(hotelInfo);
		}
		packageReservationData.setHotelInfoList(hotelInfoList);
	}

	private void addComments(final HotelInfo hotelInfo, final String code,
			final AbstractOrderModel abstractOrderModel)
	{
		List<CommentModel> asmAccommodationSupplierComments = getBcfAsmCommentsService()
				.getAsmAccommodationSupplierComments(code, abstractOrderModel);

		if (CollectionUtils.isNotEmpty(asmAccommodationSupplierComments))
		{
			List<String> comments = new ArrayList<>();
			StreamUtil.safeStream(asmAccommodationSupplierComments).forEach(c -> comments.add(c.getText()));
			hotelInfo.setNotes(comments);
		}
	}

	protected Map<BookingJourneyType, OrderEntryGroupStrategy> getEntryGroupStrategyMap()
	{
		return entryGroupStrategyMap;
	}

	@Required
	public void setEntryGroupStrategyMap(
			final Map<BookingJourneyType, OrderEntryGroupStrategy> entryGroupStrategyMap)
	{
		this.entryGroupStrategyMap = entryGroupStrategyMap;
	}

	public BcfAsmCommentsService getBcfAsmCommentsService()
	{
		return bcfAsmCommentsService;
	}

	@Required
	public void setBcfAsmCommentsService(final BcfAsmCommentsService bcfAsmCommentsService)
	{
		this.bcfAsmCommentsService = bcfAsmCommentsService;
	}
}
