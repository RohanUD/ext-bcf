/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.place.handler.impl;

import static com.bcf.core.constants.BcfCoreConstants.CATALOG_VERSION;
import static com.bcf.core.constants.BcfCoreConstants.SITE_LOGO;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.BookingConfirmationQrCodeHelper;
import com.bcf.fulfilmentprocess.order.place.handler.CreateOrderNotificationGlobalHandler;
import com.bcf.notification.request.order.createorder.CreateOrderNotificationData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationItemData;
import com.google.zxing.WriterException;


public class CreateFerryOrderImagesHandler implements CreateOrderNotificationGlobalHandler
{
	private static final String TERMS_AND_CONDITION_KEY = "tAndCKey";
	private static final String LOGO_KEY = "logo";

	private BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper;
	private ConfigurationService configurationService;
	private CatalogVersionService catalogVersionService;
	private MediaService mediaService;
	private BcfBookingService bcfBookingService;

	@Override
	public void handle(final AbstractOrderModel abstractOrderModel, final CreateOrderNotificationData createOrderNotificationData)
			throws IOException, WriterException
	{
		if(bcfBookingService.checkIfAnyOrderEntryByType(abstractOrderModel, OrderEntryType.TRANSPORT))
		{
			populateEncodedImages(abstractOrderModel, createOrderNotificationData);
			populateServerImageMap(createOrderNotificationData);
		}
	}

	protected void populateEncodedImages(final AbstractOrderModel abstractOrderModel,
			final CreateOrderNotificationData orderPlaceNotificationData) throws IOException, WriterException
	{
		final Map<String, String> qrCodeImageMap = getBookingConfirmationQrCodeHelper().getQRImageMap(abstractOrderModel);
		orderPlaceNotificationData.setInlineImageMap(qrCodeImageMap);
	}

	protected void populateServerImageMap(final CreateOrderNotificationData createOrderNotificationData)
	{
		Map<String, String> serverImagesMap = new HashMap<>();
		for (ReservationData reservationData : createOrderNotificationData.getTransportReservation().getReservationData())
		{

			for (ReservationItemData reservationItemData : reservationData.getReservationItem())
			{

				if (Objects.nonNull(reservationItemData.getFerryTandCData()) && StringUtils
						.isNotEmpty(reservationItemData.getFerryTandCData().getImage()))
				{
					serverImagesMap.put(TERMS_AND_CONDITION_KEY, reservationItemData.getFerryTandCData().getImage());
				}
			}
		}
		try
		{
			MediaModel logo = mediaService
					.getMedia(catalogVersionService.getCatalogVersion(BcfCoreConstants.MASTER_CATALOG_ID, CATALOG_VERSION), SITE_LOGO);
			if (Objects.nonNull(logo))
			{
				serverImagesMap.put(LOGO_KEY, configurationService.getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL) + logo.getURL().substring(1));
			}
		}
		catch (Exception e)
		{
			//do nothing
		}
		createOrderNotificationData.setServerImagesMap(serverImagesMap);
	}

	protected BookingConfirmationQrCodeHelper getBookingConfirmationQrCodeHelper()
	{
		return bookingConfirmationQrCodeHelper;
	}

	@Required
	public void setBookingConfirmationQrCodeHelper(final BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper)
	{
		this.bookingConfirmationQrCodeHelper = bookingConfirmationQrCodeHelper;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}
}
