/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.order.BcfOrderService;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.financial.Discounts;
import com.bcf.notification.request.order.financial.FinancialData;
import com.bcf.notification.request.order.financial.FinancialPriceData;
import com.bcf.notification.request.order.financial.Taxes;


public class NotificationFinancialPriceDataPopulator implements Populator<OrderModel, FinancialData>
{
	private BcfOrderService orderService;

	@Override
	public void populate(final OrderModel orderModel, final FinancialData financialData) throws ConversionException
	{
		FinancialPriceData financialPriceData = financialData.getPriceData();
		if (Objects.isNull(financialPriceData))
		{
			financialPriceData = new FinancialPriceData();
		}

		populateDepositData(orderModel, financialPriceData);

		financialPriceData.setTotalPackageDiscount(NotificationUtils.formatDouble(orderModel.getTotalDiscounts()));
		financialPriceData.setTotalPriceIncludingGST(NotificationUtils.formatDouble(orderModel.getTotalPrice()));

		if (orderService.isAmendOrderProcess(orderModel))
		{
			for (final PaymentTransactionModel paymentTransactionModel : orderModel.getPaymentTransactions())
			{
				if (paymentTransactionModel.isExisting() && CollectionUtils.isNotEmpty(paymentTransactionModel.getEntries())
						&& orderService.isRefundOrder(paymentTransactionModel.getEntries()))
				{
					financialPriceData.setTotalPriceIncludingGST(paymentTransactionModel.getEntries().stream().filter(
							paymentTransactionEntryModel -> !paymentTransactionEntryModel.isExisting()).findFirst().get().getAmount()
							.doubleValue());
				}
				else
				{
					if (!paymentTransactionModel.isExisting())
					{
						final Double totalPaid = paymentTransactionModel.getEntries().stream()
								.filter(paymentTransactionEntryModel -> !paymentTransactionEntryModel.isExisting()).
										findFirst().get().getAmount().doubleValue();
						financialPriceData.setTotalPriceIncludingGST(NotificationUtils.formatDouble(totalPaid));
					}
				}
			}
		}

		financialPriceData.setDiscounts(populateDiscounts(orderModel));

		final List<Taxes> taxes = new ArrayList<Taxes>();
		if (CollectionUtils.isNotEmpty(orderModel.getTotalTaxValues()))
		{
			orderModel.getTotalTaxValues().stream().forEach(
					taxValue -> {
						final Taxes tax = new Taxes();
						tax.setCode(taxValue.getCode());
						tax.setAmount(NotificationUtils.formatDouble(taxValue.getValue()));
						taxes.add(tax);
					}
			);
		}

		financialPriceData.setTaxes(taxes);

		Double totalMargin = 0.0;
		if (CollectionUtils.isNotEmpty(orderModel.getEntries()))
		{
			for (final AbstractOrderEntryModel abstractOrderEntryModel : orderModel.getEntries())
			{
				if (Objects.nonNull(abstractOrderEntryModel.getMargin()))
				{
					totalMargin = totalMargin + abstractOrderEntryModel.getMargin();
				}
			}
		}

		financialPriceData.setMargin(NotificationUtils.formatDouble(totalMargin));

		financialData.setPriceData(financialPriceData);
	}

	private void populateDepositData(final OrderModel orderModel, final FinancialPriceData priceData)
	{
		for (final PaymentTransactionModel paymentTransactionModel : orderModel.getPaymentTransactions())
		{

			if (!CollectionUtils.isEmpty(paymentTransactionModel.getEntries()))
			{
				final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransactionModel.getEntries().stream()
						.filter(paymentTransactionEntry -> paymentTransactionEntry.isDepositPayment()).collect(
								Collectors.toList());

				if (CollectionUtils.isNotEmpty(paymentTransactionEntries))
				{

					Double depositPaid = paymentTransactionEntries.stream().
							map(paymentTransactionEntry -> paymentTransactionEntry.getAmount())
							.reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();


					final PaymentTransactionEntryModel depositPaymentTransactionEntryModel =
							paymentTransactionModel.getEntries().stream().findFirst().get();

					final Double balanceDue = orderModel.getAmountToPay();
					if (Objects.nonNull(balanceDue))
					{
						if (balanceDue == 0)
						{
							depositPaid = -depositPaid;
						}
					}

					priceData.setDepositAmount(NotificationUtils.formatDouble(depositPaid));
					priceData.setDepositPaidTimestamp(paymentTransactionEntries.get(0).getCreationtime());
					priceData.setBalanceDueAmount(NotificationUtils.formatDouble(orderModel.getAmountToPay()));
					priceData.setBalanceDueDate(orderModel.getDepositPayDate());
				}

			}
		}
	}

	private List<Discounts> populateDiscounts(final OrderModel orderModel)
	{
		final List<Discounts> discountList = new ArrayList<>();
		final List<DiscountModel> discountModels = orderModel.getDiscounts().stream().collect(Collectors.toList());
		discountModels.forEach(discounts -> {
			final Discounts discount = new Discounts();
			discount.setAmount(NotificationUtils.formatDouble(discounts.getValue()));
			discount.setCode(discounts.getCode());
			discountList.add(discount);
		});

		return discountList;
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}
}
