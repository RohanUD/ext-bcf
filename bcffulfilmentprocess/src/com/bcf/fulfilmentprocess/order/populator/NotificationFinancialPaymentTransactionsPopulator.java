/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.order.BcfOrderService;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.financial.AgentData;
import com.bcf.notification.request.order.financial.FinancialData;
import com.bcf.notification.request.order.financial.FinancialPriceData;
import com.bcf.notification.request.order.financial.PaymentInfoData;
import com.bcf.notification.request.order.financial.PaymentTransactionData;
import com.bcf.notification.request.order.financial.TransactionData;


public class NotificationFinancialPaymentTransactionsPopulator implements Populator<OrderModel, FinancialData>
{
	private BcfOrderService orderService;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param orderModel    the source object
	 * @param financialData the target to fill
	 * @throws ConversionException if an error occurs
	 */
	@Override
	public void populate(final OrderModel orderModel, final FinancialData financialData) throws ConversionException
	{
		final List<PaymentTransactionData> paymentTransactionDataList = new ArrayList<>(
				CollectionUtils.size(orderModel.getPaymentTransactions()));
		for (final PaymentTransactionModel paymentTransactionModel : orderModel.getPaymentTransactions())
		{

			if (!CollectionUtils.isEmpty(paymentTransactionModel.getEntries()))
			{
				final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransactionModel.getEntries().stream()
						.filter(paymentTransactionEntry -> paymentTransactionEntry.isDepositPayment()).collect(
								Collectors.toList());
				if (CollectionUtils.isNotEmpty(paymentTransactionEntries))
				{

					Double depositPaid = paymentTransactionEntries.stream().
							map(paymentTransactionEntry -> paymentTransactionEntry.getAmount())
							.reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();

					final FinancialPriceData priceData = financialData.getPriceData();
					final Double balanceDue = orderModel.getAmountToPay();
					if (Objects.nonNull(balanceDue))
					{
						if (balanceDue == 0)
						{
							depositPaid = -depositPaid;
						}
					}

					priceData.setDepositAmount(NotificationUtils.formatDouble(depositPaid));
					priceData.setDepositPaidTimestamp(paymentTransactionEntries.get(0).getCreationtime());
					priceData.setBalanceDueAmount(NotificationUtils.formatDouble(orderModel.getAmountToPay()));
					priceData.setBalanceDueDate(orderModel.getDepositPayDate());
					financialData.setPriceData(priceData);
				}

			}

			if (CollectionUtils.isEmpty(paymentTransactionModel.getEntries()) || paymentTransactionModel.isExisting())
			{
				if (!getOrderService().isRefundOrder(paymentTransactionModel.getEntries()))
					continue;
			}
			final PaymentTransactionData paymentTransactionData = new PaymentTransactionData();

			final BCFPaymentMethodType bcfPaymentMethodType = paymentTransactionModel.getEntries().get(0).getTransactionType();

			populatePaymentInfo(paymentTransactionModel, paymentTransactionData, bcfPaymentMethodType);

			final List<TransactionData> transactions = new ArrayList<>(CollectionUtils.size(paymentTransactionModel.getEntries()));
			populatePaymentTransactionEntries(paymentTransactionModel, transactions);
			paymentTransactionData.setTransaction(transactions);
			paymentTransactionDataList.add(paymentTransactionData);
		}
		financialData.setPaymentTransactions(paymentTransactionDataList);
	}

	private void populatePaymentInfo(final PaymentTransactionModel paymentTransactionModel,
			final PaymentTransactionData paymentTransactionData, final BCFPaymentMethodType bcfPaymentMethodType)
	{
		if (BCFPaymentMethodType.CARD.equals(bcfPaymentMethodType))
		{
			final PaymentInfoData paymentInfoData = new PaymentInfoData();
			final CreditCardPaymentInfoModel paymentInfo = ((CreditCardPaymentInfoModel) paymentTransactionModel.getInfo());
			paymentInfoData.setValidityMonth(paymentInfo.getValidToMonth());
			paymentInfoData.setValidityYear(paymentInfo.getValidToYear());
			paymentInfoData.setType(paymentInfo.getType().getCode());
			paymentInfoData.setNumber(paymentInfo.getNumber());
			paymentTransactionData.setPaymentInfo(paymentInfoData);
		}

		if (BCFPaymentMethodType.CASH.equals(bcfPaymentMethodType))
		{
			final PaymentInfoData paymentInfoData = new PaymentInfoData();
			final CashPaymentInfoModel paymentInfo = ((CashPaymentInfoModel) paymentTransactionModel.getInfo());
			paymentInfoData.setType(BCFPaymentMethodType.CASH.getCode());
			paymentInfoData.setNumber(paymentInfo.getReceiptNumber());
			paymentTransactionData.setPaymentInfo(paymentInfoData);
		}

		if (BCFPaymentMethodType.GIFTCARD.equals(bcfPaymentMethodType))
		{
			final PaymentInfoData paymentInfoData = new PaymentInfoData();
			final GiftCardPaymentInfoModel paymentInfo = ((GiftCardPaymentInfoModel) paymentTransactionModel.getInfo());
			paymentInfoData.setType(BCFPaymentMethodType.GIFTCARD.getCode());
			paymentInfoData.setNumber(paymentInfo.getGiftCardNumber());
			paymentTransactionData.setPaymentInfo(paymentInfoData);
		}
	}

	private void populatePaymentTransactionEntries(final PaymentTransactionModel paymentTransactionModel,
			final List<TransactionData> transactions)
	{
		for (final PaymentTransactionEntryModel transactionEntry : paymentTransactionModel.getEntries())
		{
			if (transactionEntry.isExisting())
			{
				continue;
			}
			final TransactionData transactionData = new TransactionData();
			transactionData.setCode(transactionEntry.getCode());
			transactionData.setAmount(NotificationUtils.formatDouble(transactionEntry.getAmount().doubleValue()));

			if (BCFPaymentMethodType.GIFTCARD.equals(transactionEntry.getTransactionType()))
			{
				final GiftCardPaymentInfoModel giftCardPaymentInfoModel = (GiftCardPaymentInfoModel) paymentTransactionModel
						.getInfo();
				transactionData.setType(giftCardPaymentInfoModel.getGiftCardType().getCode());
			}
			else
			{
				transactionData.setType(transactionEntry.getCardType());
			}

			transactionData.setDate(transactionEntry.getCreationtime());
			transactionData.setTokenType(transactionEntry.getTokenType());
			transactionData.setTokenValue(transactionEntry.getTokenValue());
			if (Objects.nonNull(transactionEntry.getAgent()))
			{
				final AgentData agentData = NotificationUtils.setAgentData(transactionEntry.getAgent());
				transactionData.setAgent(agentData);
			}
			transactionData.setApprovalNumber(transactionEntry.getApprovalNumber());
			transactionData.setStatusMessage(transactionEntry.getApprovalOrDeclineMessage());
			transactionData.setBcfReferenceRecord(transactionEntry.getBcfRefRecord());
			transactionData.setBcfReferenceRecord(transactionEntry.getBcfResponseDescription());
			transactionData.setCardEntryMethod(transactionEntry.getCardEntryMethod());
			transactionData.setCardHolderLanguage(transactionEntry.getCardHolderLanguage());
			transactionData.setCustomerDisplayMessage(transactionEntry.getCustomerDisplayMessage());
			transactionData.setCmvIndivator(transactionEntry.getCvmIndicator());
			transactionData.setDebitAccountType(transactionEntry.getDebitAccountType());
			transactionData.setEmvAid(transactionEntry.getEmvAid());
			transactionData.setEmvAppLabel(transactionEntry.getEmvApplicationLabel());
			transactionData.setEmvAppPreferredName(transactionEntry.getEmvApplicationLabel());
			transactionData.setEmvPinEntryMessage(transactionEntry.getEmvPinEntryMessage());
			transactionData.setEmvTsi(transactionEntry.getEmvTsi());
			transactionData.setEmvTvr(transactionEntry.getEmvTvr());
			transactionData.setFormFactor(transactionEntry.getFormFactor());
			transactionData.setIsoResponse(transactionEntry.getIsoResponseCode());
			transactionData.setMotoIndicator(transactionEntry.getMotoIndicator());
			transactionData.setOperatorDisplay(transactionEntry.getOperatorDisplayMessage());
			transactionData.setSafIndicator(transactionEntry.getSafIndicator());
			if (Objects.nonNull(transactionEntry.getSalesApplication()))
			{
				transactionData.setSalesApplication(transactionEntry.getSalesApplication().getCode());
			}
			transactionData.setPaymentTerminalID(transactionEntry.getServiceProviderPaymentTerminalId());
			transactionData.setServiceProviderResponseCode(transactionEntry.getServiceProviderResponseCode());
			transactionData.setServiceProviderResponseDesc(transactionEntry.getServiceProviderResponseDescription());
			transactionData.setSignatureRequired(transactionEntry.isSignatureRequiredFlag());
			transactionData.setSupplementaryMessage(transactionEntry.getSupplementalMessage());
			transactionData.setTransactionRefNumber(transactionEntry.getTransactionRefNumber());
			transactionData.setApprovalOrDeclineMessage(transactionEntry.getApprovalOrDeclineMessage());
			transactionData.setTransactionTypeToPrint(transactionEntry.getPaymentTransactionType());
			transactions.add(transactionData);
		}
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}
}
