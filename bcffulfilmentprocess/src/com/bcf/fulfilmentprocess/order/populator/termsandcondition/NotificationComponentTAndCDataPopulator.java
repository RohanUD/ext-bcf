/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator.termsandcondition;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.accommodation.data.ComponentTAndCData;


public class NotificationComponentTAndCDataPopulator
		implements Populator<ComponentTAndCData, com.bcf.notification.request.order.createorder.ComponentTAndCData>
{

	@Override
	public void populate(final ComponentTAndCData source,
			final com.bcf.notification.request.order.createorder.ComponentTAndCData target) throws ConversionException
	{
		target.setAccommodationOfferings(source.getAccommodationOfferings());
		target.setActivityProducts(source.getActivityProducts());
		target.setTermsAndConditions(source.getTermsAndConditions());
	}
}
