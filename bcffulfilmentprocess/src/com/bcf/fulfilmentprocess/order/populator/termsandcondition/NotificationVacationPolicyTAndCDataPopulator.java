/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator.termsandcondition;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.data.ComponentTAndCData;
import com.bcf.core.accommodation.data.VacationPolicyTAndCData;


public class NotificationVacationPolicyTAndCDataPopulator
		implements Populator<VacationPolicyTAndCData, com.bcf.notification.request.order.createorder.VacationPolicyTAndCData>
{

	private Converter<ComponentTAndCData, com.bcf.notification.request.order.createorder.ComponentTAndCData> notificationComponentTAndCConverter;

	@Override
	public void populate(final VacationPolicyTAndCData source,
			final com.bcf.notification.request.order.createorder.VacationPolicyTAndCData target)
			throws ConversionException
	{
		target.setPackageTermsAndConditions(source.getPackageTermsAndConditions());

		if (CollectionUtils.isNotEmpty(source.getComponentTermsAndConditions()))
		{
			target.setComponentTermsAndConditions(
					notificationComponentTAndCConverter.convertAll(source.getComponentTermsAndConditions()));
		}
	}

	@Required
	public void setNotificationComponentTAndCConverter(
			final Converter<ComponentTAndCData, com.bcf.notification.request.order.createorder.ComponentTAndCData> notificationComponentTAndCConverter)
	{
		this.notificationComponentTAndCConverter = notificationComponentTAndCConverter;
	}
}
