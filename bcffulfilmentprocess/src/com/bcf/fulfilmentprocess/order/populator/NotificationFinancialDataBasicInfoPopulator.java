/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.model.GoodWillRefundOrderInfoModel;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.financial.AgentData;
import com.bcf.notification.request.order.financial.FinancialData;
import com.bcf.notification.request.order.financial.GoodWillData;


public class NotificationFinancialDataBasicInfoPopulator implements Populator<OrderModel, FinancialData>
{
	@Override
	public void populate(final OrderModel orderModel, final FinancialData financialData) throws ConversionException
	{

		financialData.setOrderNumber(orderModel.getCode());
		financialData.setCreationTime(orderModel.getCreationtime());
		financialData.setModifiedTime(orderModel.getModifiedtime());
		financialData.setVersion(orderModel.getEBookingVersion());
		financialData.setCurrency(orderModel.getCurrency().getIsocode());
		if (Objects.nonNull(orderModel.getPaymentStatus()))
		{
			financialData.setPaymentStatus(orderModel.getPaymentStatus().getCode());
		}
		financialData.setBookingStatus(orderModel.getStatus().getCode());
		if (Objects.isNull(orderModel.getSalesApplication()))
		{
			financialData.setSalesApplication(orderModel.getSalesApplication().getCode());
		}

		if (CollectionUtils.isNotEmpty(orderModel.getGoodwillRefundOrderInfos()))
		{
			final List<GoodWillData> goodWillDataList = new ArrayList<>();
			for (final GoodWillRefundOrderInfoModel goodwillRefundOrderInfo : orderModel.getGoodwillRefundOrderInfos())
			{
				final GoodWillData goodWillData = new GoodWillData();
				goodWillData.setReasoncode(goodwillRefundOrderInfo.getReasonCode());
				goodWillData.setAmount(NotificationUtils.formatDouble(goodwillRefundOrderInfo.getAmount()));
				goodWillData.setDate(goodwillRefundOrderInfo.getCreationtime());
				if (Objects.nonNull(goodwillRefundOrderInfo.getAgent()))
				{
					final AgentData agentData = NotificationUtils.setAgentData(goodwillRefundOrderInfo.getAgent());
					goodWillData.setAgentData(agentData);
				}
				goodWillDataList.add(goodWillData);
			}
			financialData.setGoodWillData(goodWillDataList);
		}

	}
}
