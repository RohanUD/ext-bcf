/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.AbstractPromotionActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleInfoModel;
import de.hybris.platform.travelservices.model.travel.TransportVehicleModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.SpecialRequestDetailModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.VehicleTypeCode;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.orderentrygroup.OrderEntryGroupStrategy;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.notification.request.order.financial.Acitivity;
import com.bcf.notification.request.order.financial.AgentData;
import com.bcf.notification.request.order.financial.CostAdjustment;
import com.bcf.notification.request.order.financial.Discounts;
import com.bcf.notification.request.order.financial.Ferry;
import com.bcf.notification.request.order.financial.FinancialData;
import com.bcf.notification.request.order.financial.FinancialPriceData;
import com.bcf.notification.request.order.financial.Guest;
import com.bcf.notification.request.order.financial.Hotel;
import com.bcf.notification.request.order.financial.LineItems;
import com.bcf.notification.request.order.financial.ProductInfo;
import com.bcf.notification.request.order.financial.Taxes;


public class NotificationFinancialLineItemDataPopulator implements Populator<OrderModel, FinancialData>
{
	private BcfOrderService orderService;
	private Map<BookingJourneyType, OrderEntryGroupStrategy> entryGroupStrategyMap;
	private Map<String, String> lineStatuses;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BCFTravelCartService bcfTravelCartService;
	private EnumerationService enumerationService;
	private static final DecimalFormat df2 = new DecimalFormat("#.##");

	@Override
	public void populate(final OrderModel orderModel, final FinancialData financialData) throws ConversionException
	{
		final OrderEntryGroupStrategy orderEntryGroupStrategy = getEntryGroupStrategyMap().get(orderModel.getBookingJourneyType());

		final Map<OrderEntryType, List<AbstractOrderEntryModel>> entriesByType;

		if (getOrderService().isAmendOrderProcess(orderModel))
		{
			entriesByType = getOrderService().getDeltaOrderEntries(orderModel);
		}
		else
		{
			entriesByType = bcfTravelCartService.splitCartEntriesByType(orderModel);
		}

		final List<LineItems> lineItemsList = new ArrayList<>();

		for (final OrderEntryType entryType : entriesByType.keySet())
		{
			final List<AbstractOrderEntryModel> entriesPerType = entriesByType.get(entryType);
			entriesPerType.forEach(entry -> {
				final LineItems lineItems = new LineItems();
				lineItems.setStatus(lineStatuses.get(entry.getAmendStatus().getCode()));

				if (OrderStatus.CANCELLED.equals(orderModel.getStatus()))
				{
					lineItems.setStatus(OrderStatus.CANCELLED.getCode());
				}

				if (OrderEntryType.ACCOMMODATION.equals(entryType))
				{
					createAccomodationProductInfo(lineItems, entry, orderEntryGroupStrategy);
				}
				else if (OrderEntryType.TRANSPORT.equals(entryType))
				{
					createFerryProductInfo(lineItems, entry);
				}
				else if (OrderEntryType.ACTIVITY.equals(entryType))
				{
					createActivityProductInfo(lineItems, entry);
				}
				else if (OrderEntryType.FEE.equals(entryType))
				{
					createFeeProductInfo(lineItems, entry);
				}
				updateProductInfo(lineItems, entry);
				setPriceData(lineItems, entry);
				populateCostAdjustment(lineItems, entry);

				lineItemsList.add(lineItems);
			});
		}

		financialData.setLinItems(lineItemsList);
	}

	protected void populateCostAdjustment(final LineItems lineItems, final AbstractOrderEntryModel entry)
	{
		final CostAdjustment costAdjustment = new CostAdjustment();
		costAdjustment.setAmount(entry.getAdjustedCost());
		if (Objects.nonNull(entry.getCostAdjustmentAgent()))
		{
			final AgentData agentData = NotificationUtils.setAgentData(entry.getCostAdjustmentAgent());
			costAdjustment.setAgent(agentData);
		}
		lineItems.setCostAdjustment(costAdjustment);
	}

	private void updateProductInfo(final LineItems lineItems, final AbstractOrderEntryModel entry)
	{
		final ProductInfo productInfo = lineItems.getProductInfo();
		productInfo.setQuantity(entry.getQuantity().intValue());
		productInfo.setGiveAway(entry.getGiveAway());
		productInfo.setBookingReference(entry.getBookingReference());
	}

	private void setPriceData(final LineItems lineItems, final AbstractOrderEntryModel entry)
	{
		final FinancialPriceData priceData = new FinancialPriceData();
		priceData.setTotalBaseCost(NotificationUtils.formatDouble(entry.getBasePrice()));
		priceData.setCostToBCF(NotificationUtils.formatDouble(entry.getCostToBCF()));
		priceData.setTotalBasePrice(NotificationUtils.formatDouble(entry.getTotalPrice()));
		priceData.setTotalPriceIncludingGST(NotificationUtils.formatDouble(entry.getTotalPrice()));

		if (OrderEntryType.ACCOMMODATION.equals(entry.getType()) || OrderEntryType.ACTIVITY.equals(entry.getType()))
		{
			if (Objects.nonNull(entry.getTaxValues()) && !entry.getTaxValues().isEmpty())
			{
				final List<Taxes> taxes = new ArrayList<Taxes>();
				if (CollectionUtils.isNotEmpty(entry.getTaxValues()))
				{
					entry.getTaxValues().stream().filter(taxValue -> !BcfCoreConstants.GST.equals(taxValue.getCode())).forEach(
							taxValue -> {
								final Taxes tax = new Taxes();
								tax.setCode(taxValue.getCode());
								tax.setAmount(NotificationUtils.formatDouble(taxValue.getValue()));
								taxes.add(tax);
							}
					);
				}

				priceData.setTaxes(taxes);

				final TaxValue gstTaxValue = entry.getTaxValues().stream()
						.filter(taxValue -> BcfCoreConstants.GST.equals(taxValue.getCode())).findFirst().orElse(null);
				if (Objects.nonNull(gstTaxValue))
				{
					final Double totalBasePriceWitoutGST = entry.getTotalPrice() - gstTaxValue.getValue();
					priceData.setTotalBasePrice(NotificationUtils.formatDouble(totalBasePriceWitoutGST));
					priceData.setTotalGST(NotificationUtils.formatDouble(gstTaxValue.getValue()));
				}
			}

			final String travellerName = getLeadTravellerName(entry);
			lineItems.setLeadTravelGuestName(travellerName);
		}

		if (CollectionUtils.isNotEmpty(entry.getDiscountValues()))
		{
			final Double discount = entry.getDiscountValues().stream().map(DiscountValue::getValue).reduce(Double::sum).get();
			priceData.setTotalPackageDiscount(discount);
			final List<Discounts> discountList = new ArrayList<>(entry.getDiscountValues().size());
			entry.getDiscountValues().forEach(dv -> {
				final Discounts discountValue = new Discounts();

				if (Objects.nonNull(entry.getOrder().getAllPromotionResults()))
				{
					final String promotionName = getPromotionName(entry.getOrder().getAllPromotionResults(), dv.getCode());
					discountValue.setCode(promotionName);
				}

				discountValue.setAmount(NotificationUtils.formatDouble(dv.getValue()));
				discountList.add(discountValue);
			});
			priceData.setDiscounts(discountList);
		}

		priceData.setMargin(NotificationUtils.formatDouble(entry.getMargin()));

		lineItems.setPriceData(priceData);
	}

	private String getPromotionName(final Set<PromotionResultModel> allPromotionResults, final String actionId)
	{
		String promoName = StringUtils.EMPTY;

		for (final PromotionResultModel promotionResultModel : allPromotionResults)
		{
			for (final AbstractPromotionActionModel promotionActionModel : promotionResultModel.getAllPromotionActions())
			{
				if (promotionActionModel.getGuid().equals(actionId)
						&& promotionActionModel instanceof RuleBasedOrderEntryAdjustActionModel)
				{
					final RuleBasedOrderEntryAdjustActionModel ruleBasedOrderEntryAdjustActionModel = (RuleBasedOrderEntryAdjustActionModel) promotionActionModel;
					promoName = ruleBasedOrderEntryAdjustActionModel.getRule().getPromotion().getCode();
				}
			}
		}
		return promoName;
	}

	private String getLeadTravellerName(final AbstractOrderEntryModel entry)
	{
		if (Objects.nonNull(entry) && Objects.nonNull(entry.getEntryGroup()))
		{
			if (entry.getEntryGroup() instanceof DealOrderEntryGroupModel)
			{
				final DealOrderEntryGroupModel dealOrderEntryGroupModel = (DealOrderEntryGroupModel) entry
						.getEntryGroup();
				if (Objects.nonNull(dealOrderEntryGroupModel))
				{
					final Optional<AccommodationOrderEntryGroupModel> refObj = dealOrderEntryGroupModel
							.getAccommodationEntryGroups().stream().findFirst();
					if (refObj.isPresent())
					{
						final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = refObj.get();
						return accommodationOrderEntryGroupModel.getContactName();
					}
				}
			}
		}
		return StringUtils.EMPTY;
	}

	private void createFeeProductInfo(final LineItems lineItems, final AbstractOrderEntryModel entry)
	{
		final ProductInfo productInfo = new ProductInfo();
		productInfo.setCode(entry.getProduct().getCode());
		productInfo.setName(entry.getProduct().getName());
		productInfo
				.setSupplierName(Objects.nonNull(entry.getChosenVendor()) ? entry.getChosenVendor().getName() : StringUtils.EMPTY);
		productInfo.setSupplierEmail(
				Objects.nonNull(entry.getChosenVendor()) && CollectionUtils.isNotEmpty(entry.getChosenVendor().getEmails())
						? entry.getChosenVendor().getEmails().stream().findFirst().get() : StringUtils.EMPTY);
		productInfo.setProductType(OrderEntryType.FEE.getCode());
		lineItems.setProductInfo(productInfo);
	}

	private void createAccomodationProductInfo(final LineItems lineItems, final AbstractOrderEntryModel entry,
			final OrderEntryGroupStrategy orderEntryGroupStrategy)
	{
		final ProductInfo productInfo = new ProductInfo();
		final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel = orderEntryGroupStrategy
				.getAccommodationEntryGroup(entry);
		productInfo.setCode(accommodationOrderEntryGroupModel.getAccommodationOffering().getName());
		productInfo.setName(accommodationOrderEntryGroupModel.getAccommodation().getName());
		productInfo.setProductType(OrderEntryType.ACCOMMODATION.getCode());
		productInfo.setBookingReference(entry.getBookingReference());
		productInfo.setGiveAway(entry.getGiveAway());

		String availabilityStatus = AvailabilityStatus.ON_REQUEST.toString();
		if (entry.isStockAllocated())
		{
			availabilityStatus = AvailabilityStatus.AVAILABLE.toString();
		}
		productInfo.setInventoryType(availabilityStatus);

		final VendorModel vendor = accommodationOrderEntryGroupModel.getAccommodationOffering().getVendor();
		if (Objects.nonNull(vendor))
		{
			productInfo.setSupplierName(vendor.getName());
		}

		final AccommodationOfferingModel accommodationOfferingModel = accommodationOrderEntryGroupModel.getAccommodationOffering();
		if (Objects.nonNull(accommodationOfferingModel.getLocation()))
		{
			final LocationModel locationModel = accommodationOfferingModel.getLocation();
			if (Objects.nonNull(locationModel.getPointOfService()) &&
					!locationModel.getPointOfService().isEmpty())
			{
				final PointOfServiceModel pointOfServiceModel = accommodationOrderEntryGroupModel.getAccommodationOffering()
						.getLocation()
						.getPointOfService().stream().findFirst().get();
				final AddressModel posAddress = pointOfServiceModel.getAddress();
				if (Objects.nonNull(posAddress))
				{
					productInfo.setSupplierEmail(posAddress.getEmail());
					productInfo.setSupplierPhone(posAddress.getPhone1());
				}
			}
		}

		productInfo.setHotel(createHotel(entry, accommodationOrderEntryGroupModel));
		createGuestCountData(lineItems, accommodationOrderEntryGroupModel);
		lineItems.setProductInfo(productInfo);
	}

	private void createGuestCountData(final LineItems lineItems,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{
		final List<Guest> guestList = new ArrayList<>();
		final List<GuestCountModel> guestCounts = accommodationOrderEntryGroupModel.getGuestCounts();
		guestCounts.forEach(guestCount -> {
			if (!containsGuest(guestCount, guestList))
			{
				final Guest guest = new Guest();
				guest.setType(guestCount.getPassengerType().getCode());
				guest.setCount(guestCount.getQuantity());
				guestList.add(guest);
			}
			else
			{
				final Optional<Guest> guestOptional = guestList.stream()
						.filter(guest -> guestCount.getPassengerType().getCode().equals(guest.getType())).findFirst();
				if (guestOptional.isPresent())
				{
					final Guest exisingGuest = guestOptional.get();
					exisingGuest.setCount(exisingGuest.getCount() + 1);
				}
			}
		});
		lineItems.setGuestData(guestList);
	}

	private boolean containsGuest(final GuestCountModel guestCount, final List<Guest> guestList)
	{
		return guestList.stream().anyMatch(guest -> guestCount.getPassengerType().getCode().equals(guest.getType()));
	}

	private void createFerryProductInfo(final LineItems lineItems, final AbstractOrderEntryModel entry)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo = entry.getTravelOrderEntryInfo();
		final TransportOfferingModel transportOffering = travelOrderEntryInfo
				.getTransportOfferings().stream().collect(Collectors.toList()).get(0);
		final Ferry ferry = new Ferry();
		ferry.setRouteCode(transportOffering.getTravelSector().getCode());
		ferry.setSailingCode(transportOffering.getSailingCode());

		final TransportVehicleModel transportVehicleModel = transportOffering.getTransportVehicle();
		if (Objects.nonNull(transportVehicleModel))
		{
			final TransportVehicleInfoModel transportVehicleInfoModel = transportVehicleModel.getTransportVehicleInfo();
			if (Objects.nonNull(transportVehicleInfoModel))
			{
				ferry.setVesselName(transportVehicleInfoModel.getName());
			}

			final SpecialRequestDetailModel specialRequestDetailModel = travelOrderEntryInfo.getSpecialRequestDetail();
			if (Objects.nonNull(specialRequestDetailModel)
					&& CollectionUtils.isNotEmpty(specialRequestDetailModel.getSpecialServiceRequest()))
			{
				final String specialRequests = specialRequestDetailModel.getSpecialServiceRequest().stream().map(
						SpecialServiceRequestModel::getName).collect(Collectors.joining(", "));
				ferry.seteBookingSpecialInstructions(specialRequests);
			}
		}

		ferry.setDepartureTime(transportOffering.getDepartureTime());
		if (Objects.nonNull(transportOffering.getOriginTransportFacility()))
		{
			ferry.setDepartureTerminal(transportOffering.getOriginTransportFacility().getCode());
		}

		ferry.setArrivalTime(transportOffering.getArrivalTime());
		if (Objects.nonNull(transportOffering.getDestinationTransportFacility()))
		{
			ferry.setArrivalTerminal(transportOffering.getDestinationTransportFacility().getCode());
		}

		ferry.setOpenTicketFlag(transportOffering.getDefault());

		final ProductInfo productInfo = new ProductInfo();

		String productCode = null;
		String productName = null;
		if (Objects.equals(entry.getProduct().getItemtype(), FareProductModel._TYPECODE))
		{
			final TravellerModel traveller = entry.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get();
			if (traveller.getInfo() instanceof PassengerInformationModel)
			{
				final PassengerTypeModel passengerType = ((PassengerInformationModel) traveller.getInfo()).getPassengerType();
				productCode = passengerType.getEBookingCode();
				productName = passengerType.getCode();
			}
			else if (traveller.getInfo() instanceof BCFVehicleInformationModel)
			{
				final VehicleTypeCode vehicleTypeCode = ((BCFVehicleInformationModel) traveller.getInfo()).getVehicleType().getType();
				productCode = vehicleTypeCode.getCode();
				productName = getEnumerationService().getEnumerationName(vehicleTypeCode);
			}
		}

		productInfo.setCode(productCode);
		productInfo.setName(productName);
		productInfo.setSupplierName("BC-Ferries");
		productInfo.setFerry(ferry);
		productInfo.setProductType(OrderEntryType.TRANSPORT.getCode());
		lineItems.setProductInfo(productInfo);
	}

	private Hotel createHotel(final AbstractOrderEntryModel entry,
			final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{
		final Hotel hotel = new Hotel();
		hotel.setCode(accommodationOrderEntryGroupModel.getAccommodationOffering().getCode());
		hotel.setName(accommodationOrderEntryGroupModel.getAccommodationOffering().getName());
		hotel.setCheckInDate(accommodationOrderEntryGroupModel.getStartingDate());
		hotel.setCheckOutDate(accommodationOrderEntryGroupModel.getEndingDate());
		return hotel;
	}

	private void createActivityProductInfo(final LineItems lineItems, final AbstractOrderEntryModel activityEntry)
	{
		final Acitivity activity = new Acitivity();
		final ActivityOrderEntryInfoModel activityInfo = activityEntry.getActivityOrderEntryInfo();
		activity.setDate(activityInfo.getActivityDate());
		final ProductInfo productInfo = new ProductInfo();

		final ProductModel product = activityEntry.getProduct();

		productInfo.setCode(product.getCode());
		productInfo.setName(product.getName());
		productInfo.setActivity(activity);
		productInfo.setProductType(OrderEntryType.ACTIVITY.getCode());

		if (Objects.nonNull(product.getVendors()) && CollectionUtils.isNotEmpty(product.getVendors()))
		{
			final Optional<VendorModel> objRef = product.getVendors().stream().findFirst();
			if (objRef.isPresent())
			{
				final VendorModel vendor = objRef.get();
				productInfo
						.setSupplierName(Objects.nonNull(vendor.getName()) ? vendor.getName() : StringUtils.EMPTY);
				productInfo.setSupplierEmail(CollectionUtils.isNotEmpty(vendor.getEmails())
						? vendor.getEmails().stream().findFirst().get() : StringUtils.EMPTY);
			}
		}

		String availabilityStatus = AvailabilityStatus.ON_REQUEST.toString();
		if (activityEntry.isStockAllocated())
		{
			availabilityStatus = AvailabilityStatus.AVAILABLE.toString();
		}
		productInfo.setInventoryType(availabilityStatus);

		final List<Guest> guestList = new ArrayList<>();
		final Guest guest = new Guest();
		guest.setType(activityInfo.getGuestType().getCode());
		guest.setCount(activityEntry.getQuantity().intValue());
		guestList.add(guest);
		lineItems.setGuestData(guestList);

		lineItems.setProductInfo(productInfo);
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	protected Map<BookingJourneyType, OrderEntryGroupStrategy> getEntryGroupStrategyMap()
	{
		return entryGroupStrategyMap;
	}

	@Required
	public void setEntryGroupStrategyMap(
			final Map<BookingJourneyType, OrderEntryGroupStrategy> entryGroupStrategyMap)
	{
		this.entryGroupStrategyMap = entryGroupStrategyMap;
	}

	public Map<String, String> getLineStatuses()
	{
		return lineStatuses;
	}

	public void setLineStatuses(final Map<String, String> lineStatuses)
	{
		this.lineStatuses = lineStatuses;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
