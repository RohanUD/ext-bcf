/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 09/08/19, 2:47 AM
 */

package com.bcf.fulfilmentprocess.order.util;

import static com.bcf.core.constants.BcfCoreConstants.CRM_SOURCE_SYSTEM;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.notification.email.AddressData;
import com.bcf.notification.email.CustomerData;


public class CustomerUtils
{
	public static void populateCustomerData(AbstractOrderModel abstractOrder, CustomerModel customerModel,
			CustomerData customerData)
	{
		if (Objects.nonNull(customerModel))
		{
			if (StringUtils.isNotEmpty(customerModel.getContactEmail()))
			{
				customerData.setEmail(customerModel.getContactEmail());
			}
			if (StringUtils.isNotEmpty(customerModel.getFirstName()) && StringUtils.isNotEmpty(customerModel.getLastName()))
			{
				customerData.setName(customerModel.getFirstName() + " " + customerModel.getLastName());
			}
			if (Objects.nonNull(customerModel.getType()))
			{
				customerData.setType(customerModel.getType().getCode());
			}
			if (StringUtils.isNotEmpty(customerModel.getFirstName()))
			{
				customerData.setFirstName(customerModel.getFirstName());
			}
			if (StringUtils.isNotEmpty(customerModel.getLastName()))
			{
				customerData.setLastName(customerModel.getLastName());
			}
			if (Objects.nonNull(customerModel.getTitle()) && StringUtils
					.isNotEmpty(customerModel.getTitle().getCode()))
			{
				customerData.setTitle(customerModel.getTitle().getCode());
			}
			if (StringUtils.isNotEmpty(customerModel.getPhoneNo()))
			{
				customerData.setContactNumber(customerModel.getPhoneNo());
			}
			if (Objects.nonNull(customerModel.getGroups()))
			{
				//Considering first group as main Group
				Optional<PrincipalGroupModel> principalGroupModel = customerModel.getGroups().stream().findFirst();
				principalGroupModel.ifPresent(groupModel -> customerData.setGroup(groupModel.getUid()));
			}
			if (MapUtils.isNotEmpty(customerModel.getSystemIdentifiers()) && StringUtils
					.isNotBlank(customerModel.getSystemIdentifiers().get(CRM_SOURCE_SYSTEM)))
			{
				customerData.setCrmId(customerModel.getSystemIdentifiers().get(CRM_SOURCE_SYSTEM));
			}
			if (Objects.nonNull(abstractOrder) && Objects.nonNull(abstractOrder.getPaymentAddress()))
			{
				populateAddress(abstractOrder, customerData);
			}
		}
	}

	public static void populateAddress(final AbstractOrderModel abstractOrderModel, final CustomerData customerData)
	{
		final AddressModel addressModel = abstractOrderModel.getPaymentAddress();
		if (Objects.nonNull(addressModel))
		{
			final AddressData addressData = new AddressData();
			addressData.setStreetName(addressModel.getStreetname());
			addressData.setStreetNumber(addressModel.getLine2());
			addressData.setCity(addressModel.getTown());
			addressData.setCountry(addressModel.getCountry().getName());
			addressData.setPostalCode(addressModel.getPostalcode());
			addressData.setPhone(addressModel.getPhone1());
			customerData.setAddress(addressData);
		}
	}
}
