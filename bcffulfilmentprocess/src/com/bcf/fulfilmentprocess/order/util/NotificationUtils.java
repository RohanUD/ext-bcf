/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.util;

import static com.bcf.fulfilmentprocess.constants.BcfFulfilmentProcessConstants.AMERICA_VANCOUVER;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;
import com.bcf.notification.request.order.createorder.TransportOfferingData;
import com.bcf.notification.request.order.financial.AgentData;


public class NotificationUtils
{

	private static final String TRANSPORT_OFFERING_STATUS_RESULT_MINUTES = "minutes";
	private static final String TRANSPORT_OFFERING_STATUS_RESULT_HOURS = "hours";
	private static final String TRANSPORT_OFFERING_STATUS_RESULT_DAYS = "days";
	private CommonI18NService commonI18NService;
	private CommerceCommonI18NService commerceCommonI18NService;
	private I18NService i18NService;

	public static Map<String, Integer> getDurationMap(final Long duration)
	{
		final Long durationInMinutes = duration / 60000L;
		final Map<String, Integer> durationMap = new LinkedHashMap();
		final int days = (int) (durationInMinutes / 60L) / 24;
		if (days > 0)
		{
			durationMap.put(TRANSPORT_OFFERING_STATUS_RESULT_DAYS, days);
		}

		final int hours = (int) (durationInMinutes / 60L) % 24;
		if (hours > 0)
		{
			durationMap.put(TRANSPORT_OFFERING_STATUS_RESULT_HOURS, hours);
		}

		durationMap.put(TRANSPORT_OFFERING_STATUS_RESULT_MINUTES, (int) (durationInMinutes % 60L));
		return durationMap;
	}

	public static Map<String, Integer> calculateJourneyDuration(final List<TransportOfferingData> transportOfferings)
	{
		final int numberOfTranportOfferings = CollectionUtils.size(transportOfferings);
		if (numberOfTranportOfferings > 1)
		{
			Long summedDuration = 0L;

			for (int i = 0; i < numberOfTranportOfferings; ++i)
			{
				summedDuration = summedDuration + calculateDuration(transportOfferings.get(i)).toMillis();
				if (i > 0)
				{
					final ZonedDateTime departureUtcTime = TravelDateUtils
							.getUtcZonedDateTime(transportOfferings.get(i).getDepartureTime(),
									ZoneId.of(AMERICA_VANCOUVER));
					final ZonedDateTime arrivalUtcTime = TravelDateUtils
							.getUtcZonedDateTime(transportOfferings.get(i - 1).getArrivalTime(),
									ZoneId.of(AMERICA_VANCOUVER));
					summedDuration = summedDuration + Duration.between(arrivalUtcTime, departureUtcTime).toMillis();
				}
			}

			return getDurationMap(summedDuration);
		}
		else
		{
			return numberOfTranportOfferings == 1 ?
					transportOfferings.get(0).getDuration() :
					null;
		}
	}

	private static Duration calculateDuration(final TransportOfferingData transportOfferingData)
	{

		final ZonedDateTime originUtcDateTime = TravelDateUtils
				.getUtcZonedDateTime(transportOfferingData.getDepartureTime(), ZoneId.of(AMERICA_VANCOUVER));
		final ZonedDateTime destinationUtcDateTime = TravelDateUtils
				.getUtcZonedDateTime(transportOfferingData.getArrivalTime(), ZoneId.of(AMERICA_VANCOUVER));

		return Duration.between(originUtcDateTime, destinationUtcDateTime);
	}

	public String formatPrice(final Double value, final CurrencyModel currency)
	{ //NOSONAR
		final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
		Locale locale = getCommerceCommonI18NService().getLocaleForLanguage(currentLanguage);
		if (locale == null)
		{
			// Fallback to session locale
			locale = new Locale("en", "CA");
		}
		return NumberFormat.getCurrencyInstance(locale).format(value);
	}

	public PriceData createPriceData(final PriceDataType priceType, final BigDecimal value, final String currencyIso)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currencyIso, "Parameter currencyIso cannot be null.");

		final CurrencyModel currency = getCommonI18NService().getCurrency(currencyIso);
		return create(priceType, value, currency);
	}

	public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currency, "Parameter currency cannot be null.");
		final PriceData priceData = new PriceData();
		priceData.setPriceType(priceType);
		priceData.setValue(value);
		priceData.setCurrencyIso(currency.getIsocode());
		priceData.setFormattedValue(formatPrice(value.doubleValue(), currency));

		return priceData;
	}

	public static String getTransportOfferingStatusResultMinutes()
	{
		return TRANSPORT_OFFERING_STATUS_RESULT_MINUTES;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	public static AgentData setAgentData(final UserModel agent)
	{
		final AgentData agentData = new AgentData();
		if (Objects.nonNull(agent))
		{
			agentData.setLogin(agent.getLdaplogin());
			agentData.setName(agent.getName());
		}
		return agentData;
	}

	public static Double formatDouble(final Double price)
	{
		if (Objects.nonNull(price))
		{
			final BigDecimal priceBD = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP);
			return priceBD.doubleValue();
		}
		return price;
	}
}
