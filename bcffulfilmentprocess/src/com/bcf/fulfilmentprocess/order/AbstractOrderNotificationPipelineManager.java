/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order;

import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.BookingConfirmationQrCodeHelper;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;


public abstract class AbstractOrderNotificationPipelineManager
{

	private NotificationUtils notificationUtils;
	private BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	protected NotificationUtils getNotificationUtils() {
		return notificationUtils;
	}

	@Required
	public void setNotificationUtils(final NotificationUtils notificationUtils) {
		this.notificationUtils = notificationUtils;
	}

	protected BookingConfirmationQrCodeHelper getBookingConfirmationQrCodeHelper()
	{
		return bookingConfirmationQrCodeHelper;
	}

	@Required
	public void setBookingConfirmationQrCodeHelper(final BookingConfirmationQrCodeHelper bookingConfirmationQrCodeHelper)
	{
		this.bookingConfirmationQrCodeHelper = bookingConfirmationQrCodeHelper;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

}
