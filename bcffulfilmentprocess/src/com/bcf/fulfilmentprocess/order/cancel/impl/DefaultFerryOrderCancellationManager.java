/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.cancel.impl;

import static com.bcf.core.services.impl.DefaultBcfCalculationService.CENTS_TO_DOLLAR_CONVERSION;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.cancel.order.CancelOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.cancel.handler.CancelOrderNotificationGlobalHandler;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataNotificationHandler;
import com.bcf.fulfilmentprocess.order.place.handler.ReservationDataUsingSearchBookingNotificationHandler;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.bcf.notification.request.order.createorder.ReservationData;
import com.bcf.notification.request.order.createorder.ReservationDataList;
import com.google.zxing.WriterException;


public class DefaultFerryOrderCancellationManager extends AbstractCancelOrderNotificationManager implements
		CancelOrderNotificationPipelineManager
{
	private List<ReservationDataNotificationHandler> handlers;
	private List<ReservationDataUsingSearchBookingNotificationHandler> reservationSearchBookingHandlers;
	private List<CancelOrderNotificationGlobalHandler> cancelOrderUsingSearchBookingHandlers;

	@Override
	public CancelOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel)
			throws IOException, WriterException
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}

		final CancelOrderNotificationData cancelOrderNotificationData = new CancelOrderNotificationData();

		final PriceData refundAmount = getRefundAmount(abstractOrderModel.getCode(), abstractOrderModel.getStore());
		populateOrderCancellationData(abstractOrderModel, cancelOrderNotificationData, refundAmount);
		populateCustomerData(abstractOrderModel, cancelOrderNotificationData);
		populateOptionBookingData(abstractOrderModel, cancelOrderNotificationData);

		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservations = new ArrayList<>();
		final Map<Integer, List<AbstractOrderEntryModel>> entriesByJourneyRefNumber = abstractOrderModel.getEntries().stream()
				.filter(entry -> Objects
						.nonNull(entry.getTravelOrderEntryInfo()))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber));

		for (final Integer journeyRefNumber : entriesByJourneyRefNumber.keySet()) //NOSONAR
		{
			final ReservationData reservationData = new ReservationData();
			reservationData.setJourneyReferenceNumber(journeyRefNumber);
			final List<AbstractOrderEntryModel> entriesPerJourney = entriesByJourneyRefNumber.get(journeyRefNumber);
			for (final ReservationDataNotificationHandler handler : getHandlers())
			{
				handler.handle(abstractOrderModel, entriesPerJourney, reservationData);
			}
			reservations.add(reservationData);
		}
		reservationDataList.setReservationData(reservations);
		cancelOrderNotificationData.setReservationDataList(reservationDataList);

		cancelOrderNotificationData.setAmountPaid(abstractOrderModel.getAmountPaid());
		cancelOrderNotificationData.setAmountDue(abstractOrderModel.getAmountToPay());
		populateMiscDetails(abstractOrderModel, cancelOrderNotificationData);
		return cancelOrderNotificationData;
	}

	@Override
	public CancelOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> entries) throws IOException, WriterException
	{
		if (CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}
		final CancelOrderNotificationData cancelOrderNotificationData = new CancelOrderNotificationData();

		cancelOrderNotificationData.setCode(abstractOrderModel.getCode());
		populateCustomerData(abstractOrderModel, cancelOrderNotificationData);

		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservations = new ArrayList<>();
		final ReservationData reservationData = new ReservationData();

		for (final ReservationDataNotificationHandler handler : getHandlers())
		{
			handler.handle(abstractOrderModel, entries, reservationData);
		}
		reservations.add(reservationData);
		reservationDataList.setReservationData(reservations);
		cancelOrderNotificationData.setReservationDataList(reservationDataList);

		Double amountPaidInCents = abstractOrderModel.getAmountToPayInfos().stream()
				.filter(atpi -> entries.stream().map(AbstractOrderEntryModel::getBookingReference).anyMatch(bookingRefFromEntry ->
						bookingRefFromEntry.equals(atpi.getBookingReference()))).mapToDouble(atpi -> atpi.getPreviouslyPaid()).sum();
		Double amountPaid = amountPaidInCents / CENTS_TO_DOLLAR_CONVERSION;
		cancelOrderNotificationData.setAmountPaid(amountPaid);
		cancelOrderNotificationData.setRefundAmount(amountPaid);

		Double amountDueInCents = abstractOrderModel.getAmountToPayInfos().stream()
				.filter(atpi -> entries.stream().map(AbstractOrderEntryModel::getBookingReference).anyMatch(bookingRefFromEntry ->
						bookingRefFromEntry.equals(atpi.getBookingReference()))).mapToDouble(atpi -> atpi.getPayAtTerminal()).sum();
		Double amountDue = amountDueInCents / CENTS_TO_DOLLAR_CONVERSION;
		cancelOrderNotificationData.setAmountDue(amountDue);

		populateMiscDetails(abstractOrderModel, cancelOrderNotificationData);
		return cancelOrderNotificationData;
	}

	@Override
	public CancelOrderNotificationData executePipeline(final SearchBookingResponseDTO searchBookingResponse)
			throws IOException, WriterException, ParseException
	{
		final Itinerary itinerary = searchBookingResponse.getSearchResult().get(0);
		final CancelOrderNotificationData cancelOrderNotificationData = new CancelOrderNotificationData();
		final ReservationDataList reservationDataList = new ReservationDataList();
		final List<ReservationData> reservations = new ArrayList<>();
		final ReservationData reservationData = new ReservationData();

		for (final ReservationDataUsingSearchBookingNotificationHandler handler : getReservationSearchBookingHandlers())
		{
			handler.handle(itinerary, reservationData);
		}

		reservations.add(reservationData);
		reservationDataList.setReservationData(reservations);
		cancelOrderNotificationData.setReservationDataList(reservationDataList);

		for (final CancelOrderNotificationGlobalHandler handler : getCancelOrderUsingSearchBookingHandlers())
		{
			handler.handle(itinerary, cancelOrderNotificationData);
		}

		return cancelOrderNotificationData;
	}

	protected void populateMiscDetails(final AbstractOrderModel abstractOrderModel,
			final CancelOrderNotificationData cancelOrderNotificationData) throws IOException, WriterException
	{
		cancelOrderNotificationData.setDateIssued(abstractOrderModel.getCreationtime());
		final SalesApplication salesApplication = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		final String getBookedBy = SalesApplication.CALLCENTER.equals(salesApplication) ?
				"BCF CUSTOMER CARE" : cancelOrderNotificationData.getCustomerData().getCrmId();
		final String bookedBy = SalesApplication.TRAVELCENTRE.equals(salesApplication) ?
				"BCF Vacations" : getBookedBy;
		cancelOrderNotificationData.setBookedBy(bookedBy);
		cancelOrderNotificationData.setSalesApplication(getBcfSalesApplicationResolverService().getCurrentSalesChannel().getCode());
		cancelOrderNotificationData.setEventType("OrderCancelation_" + abstractOrderModel.getBookingJourneyType().getCode());
		cancelOrderNotificationData.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		cancelOrderNotificationData.setCurrencySymbol(abstractOrderModel.getCurrency().getSymbol());
		populateEncodedImages(abstractOrderModel, cancelOrderNotificationData);
	}

	protected List<ReservationDataNotificationHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ReservationDataNotificationHandler> handlers)
	{
		this.handlers = handlers;
	}

	protected List<ReservationDataUsingSearchBookingNotificationHandler> getReservationSearchBookingHandlers()
	{
		return reservationSearchBookingHandlers;
	}

	@Required
	public void setReservationSearchBookingHandlers(
			final List<ReservationDataUsingSearchBookingNotificationHandler> reservationSearchBookingHandlers)
	{
		this.reservationSearchBookingHandlers = reservationSearchBookingHandlers;
	}

	public List<CancelOrderNotificationGlobalHandler> getCancelOrderUsingSearchBookingHandlers()
	{
		return cancelOrderUsingSearchBookingHandlers;
	}

	@Required
	public void setCancelOrderUsingSearchBookingHandlers(
			final List<CancelOrderNotificationGlobalHandler> cancelOrderUsingSearchBookingHandlers)
	{
		this.cancelOrderUsingSearchBookingHandlers = cancelOrderUsingSearchBookingHandlers;
	}

}
