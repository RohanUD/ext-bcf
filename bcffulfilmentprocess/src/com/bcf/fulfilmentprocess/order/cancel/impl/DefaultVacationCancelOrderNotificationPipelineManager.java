/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.cancel.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.fulfilmentprocess.cancel.order.CancelOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.place.OrderFinancialDataPipelineManager;
import com.bcf.fulfilmentprocess.order.place.handler.VacationOrderCreateNotificationHandler;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.bcf.notification.request.order.createorder.PackageReservationData;
import com.bcf.notification.request.order.createorder.PackageReservationDataList;


public class DefaultVacationCancelOrderNotificationPipelineManager extends AbstractCancelOrderNotificationManager implements
		CancelOrderNotificationPipelineManager
{
	private Map<String, VacationOrderCreateNotificationHandler> cancelOrderNotificationHandlersMap;
	private OrderFinancialDataPipelineManager cancelOrderFinancialDataPipelineManager;

	@Override
	public CancelOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel)
	{
		return executePipeline(abstractOrderModel, abstractOrderModel.getEntries());
	}

	@Override
	public CancelOrderNotificationData executePipeline(final AbstractOrderModel abstractOrderModel,
			final List<AbstractOrderEntryModel> entries)
	{
		if (CollectionUtils.isEmpty(entries))
		{
			return null;
		}
		final CancelOrderNotificationData cancelOrderNotificationData = new CancelOrderNotificationData();
		cancelOrderNotificationData.setCode(abstractOrderModel.getCode());

		PriceData refundAmount = null;
		if (OptionBookingUtil.isOptionBooking(abstractOrderModel))
		{
			refundAmount = getEmptyPriceData(abstractOrderModel);
		}
		else
		{
			refundAmount = getRefundAmount(abstractOrderModel.getCode(), abstractOrderModel.getStore());
		}
		populateOrderCancellationData(abstractOrderModel, cancelOrderNotificationData, refundAmount);
		populateCustomerData(abstractOrderModel, cancelOrderNotificationData);
		populateOptionBookingData(abstractOrderModel, cancelOrderNotificationData);

		final Map<OrderEntryType, List<AbstractOrderEntryModel>> dealEntriesByType = entries.stream()
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));

		final PackageReservationDataList packageReservationDataList = new PackageReservationDataList();
		final List<PackageReservationData> listPackageReservationData = new ArrayList<>();
		final PackageReservationData packageReservationData = new PackageReservationData();

		for (final OrderEntryType currentEntryType : dealEntriesByType.keySet())
		{
			VacationOrderCreateNotificationHandler vacationOrderCreateNotificationHandler = getCancelOrderNotificationHandlersMap()
					.get(currentEntryType.getCode());

			if (Objects.nonNull(vacationOrderCreateNotificationHandler))
			{
				vacationOrderCreateNotificationHandler
						.handle(abstractOrderModel, dealEntriesByType.get(currentEntryType), packageReservationData);
			}
		}
		packageReservationData.setVouchersSize(getVoucherSize(packageReservationData));
		listPackageReservationData.add(packageReservationData);
		packageReservationDataList.setPackageReservationDatas(listPackageReservationData);
		cancelOrderNotificationData.setPackageReservations(packageReservationDataList);

		cancelOrderNotificationData.setEventType("vacationCancellation");
		return cancelOrderNotificationData;
	}

	@Override
	public CancelOrderNotificationData executePipeline(final SearchBookingResponseDTO searchBookingResponse)
	{
		return null;
	}

	private int getVoucherSize(final PackageReservationData packageReservationData)
	{
		int totalFerries = 0;
		int totalHotels = 0;
		int totalActivities = 0;

		if (CollectionUtils.isNotEmpty(packageReservationData.getFerryJourneyInfoList()))
		{
			totalFerries = (int) packageReservationData.getFerryJourneyInfoList().stream()
					.filter(ferryJourneyInfo -> CollectionUtils.isNotEmpty(ferryJourneyInfo.getFerryInfoList()))
					.flatMap(ferryJourneyInfo -> ferryJourneyInfo.getFerryInfoList().stream()).count();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getHotelInfoList()))
		{
			totalHotels = packageReservationData.getHotelInfoList().size();
		}
		if (CollectionUtils.isNotEmpty(packageReservationData.getActivityInfoList()))
		{
			totalActivities = packageReservationData.getActivityInfoList().size();
		}

		return totalFerries + totalHotels + totalActivities;
	}

	protected OrderFinancialDataPipelineManager getCancelOrderFinancialDataPipelineManager()
	{
		return cancelOrderFinancialDataPipelineManager;
	}

	@Required
	public void setCancelOrderFinancialDataPipelineManager(
			final OrderFinancialDataPipelineManager cancelOrderFinancialDataPipelineManager)
	{
		this.cancelOrderFinancialDataPipelineManager = cancelOrderFinancialDataPipelineManager;
	}

	protected Map<String, VacationOrderCreateNotificationHandler> getCancelOrderNotificationHandlersMap()
	{
		return cancelOrderNotificationHandlersMap;
	}

	@Required
	public void setCancelOrderNotificationHandlersMap(
			final Map<String, VacationOrderCreateNotificationHandler> cancelOrderNotificationHandlersMap)
	{
		this.cancelOrderNotificationHandlersMap = cancelOrderNotificationHandlersMap;
	}
}
