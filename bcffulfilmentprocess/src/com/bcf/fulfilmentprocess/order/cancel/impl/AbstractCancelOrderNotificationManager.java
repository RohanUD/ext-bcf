/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.fulfilmentprocess.order.cancel.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.travelservices.customer.TravelCustomerAccountService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.fulfilmentprocess.order.AbstractOrderNotificationPipelineManager;
import com.bcf.fulfilmentprocess.order.util.CustomerUtils;
import com.bcf.notification.email.CustomerData;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.google.zxing.WriterException;


public abstract class AbstractCancelOrderNotificationManager extends AbstractOrderNotificationPipelineManager
{
	private TravelCustomerAccountService customerAccountService;
	private BcfBookingService bcfBookingService;

	protected void populateOrderCancellationData(final AbstractOrderModel order,
			final CancelOrderNotificationData cancelOrderContext, final PriceData priceData)
	{
		cancelOrderContext.setRefundAmount(priceData.getValue().doubleValue());
		cancelOrderContext.setCode(order.getCode());
	}

	protected PriceData getRefundAmount(final String orderCode, final BaseStoreModel storeModel)
	{
		final OrderModel order = getCustomerAccountService().getOrderForCode(orderCode, storeModel);
		final BigDecimal refundTotal = getBcfBookingService().getRefundAmount(order);
		return getNotificationUtils().createPriceData(PriceDataType.BUY, refundTotal, order.getCurrency().getIsocode());
	}

	protected PriceData getEmptyPriceData(final AbstractOrderModel abstractOrderModel)
	{
		return getNotificationUtils().createPriceData(PriceDataType.BUY, new BigDecimal(0), abstractOrderModel.getCurrency().getIsocode());
	}

	protected void populateOptionBookingData(final AbstractOrderModel abstractOrderModel, final CancelOrderNotificationData cancelOrderContext)
	{
		if(OptionBookingUtil.isOptionBooking(abstractOrderModel)){
			cancelOrderContext.setOptionBooking(true);
		}
		if(OptionBookingUtil.wasOptionBookingAtAnyTime(abstractOrderModel)){
			cancelOrderContext.setCreatedFromOptionBooking(((CartModel)abstractOrderModel).isAlreadyCreatedFromOptionBooking());
		}
	}

	protected void populateCustomerData(final AbstractOrderModel abstractOrderModel, final CancelOrderNotificationData cancelOrderContext)
	{
		final CustomerData customerData = new CustomerData();
		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
		CustomerUtils.populateCustomerData(abstractOrderModel, customerModel, customerData);
		cancelOrderContext.setCustomerData(customerData);
	}

	protected void populateEncodedImages(final AbstractOrderModel abstractOrderModel,
			final CancelOrderNotificationData cancelOrderContext) throws IOException, WriterException
	{
		final Map<String, String> qrCodeImageMap = getBookingConfirmationQrCodeHelper().getQRImageMap(abstractOrderModel);
		cancelOrderContext.setInlineImageMap(qrCodeImageMap);
	}

	protected TravelCustomerAccountService getCustomerAccountService() {
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final TravelCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected BcfBookingService getBcfBookingService() {
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService) {
		this.bcfBookingService = bcfBookingService;
	}
}
