/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.fulfilmentprocess.order.cancel.handler.impl;

import static com.bcf.core.services.impl.DefaultBcfCalculationService.CENTS_TO_DOLLAR_CONVERSION;
import static com.bcf.fulfilmentprocess.constants.BcfFulfilmentProcessConstants.DEFAULT_CURRENCY_CODE;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.fulfilmentprocess.order.cancel.handler.CancelOrderNotificationGlobalHandler;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;
import com.bcf.notification.request.order.createorder.TotalFareData;


public class DefaultCancelOrderSearchBookingPaymentDataHandler implements CancelOrderNotificationGlobalHandler
{
	private CommonI18NService commonI18NService;

	@Override
	public void handle(final Itinerary itinerary, final CancelOrderNotificationData cancelOrderNotificationData)
	{
		Double amountDueInCents = new Double(itinerary.getBooking().getOutstandingToPayInCents());
		Double amountDue = amountDueInCents / CENTS_TO_DOLLAR_CONVERSION;

		Double amountPaidInCents = itinerary.getBooking().getPaidFaresInCents().doubleValue();
		Double amountPaid = amountPaidInCents / CENTS_TO_DOLLAR_CONVERSION;

		cancelOrderNotificationData.setAmountDue(amountDue);
		cancelOrderNotificationData.setAmountPaid(amountPaid);
		cancelOrderNotificationData.setRefundAmount(amountPaid);

		CurrencyModel currencyUSD = commonI18NService.getCurrency(DEFAULT_CURRENCY_CODE);
		cancelOrderNotificationData.setCurrency(currencyUSD.getIsocode());
		cancelOrderNotificationData.setCurrencySymbol(currencyUSD.getSymbol());

		Double totalAmountInCents = new Double(itinerary.getBooking().getGrossAmountInCents().doubleValue());
		Double totalAmount = totalAmountInCents / CENTS_TO_DOLLAR_CONVERSION;

		TotalFareData totalFareData = new TotalFareData(0.0d, 0.0d, totalAmount, amountPaid, amountDue);
		cancelOrderNotificationData.getReservationDataList().getReservationData().stream().findFirst().get().getReservationItem()
				.stream().findFirst().get().getReservationPricingInfo().setTotalFare(totalFareData);
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
