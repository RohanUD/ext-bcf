/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.fulfilmentprocess.order.cancel.handler.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.annotation.Resource;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.fulfilmentprocess.order.cancel.handler.CancelOrderNotificationGlobalHandler;
import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;


public class DefaultCancelOrderSearchBookingGlobalHandler implements CancelOrderNotificationGlobalHandler
{
	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Override
	public void handle(final Itinerary itinerary, final CancelOrderNotificationData cancelOrderNotificationData)
	{
		cancelOrderNotificationData.setCode(itinerary.getBooking().getBookingReference().getBookingReference());
		cancelOrderNotificationData.setDateIssued(getDateIssued(itinerary));

		final SalesApplication salesApplication = bcfSalesApplicationResolverService.getCurrentSalesChannel();
		final String bookedBy = getBookedBy(cancelOrderNotificationData, salesApplication);
		cancelOrderNotificationData.setBookedBy(bookedBy);
		cancelOrderNotificationData.setSalesApplication(salesApplication.getCode());
		cancelOrderNotificationData.setEventType("OrderCancelation_" + BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());
	}

	private String getBookedBy(final CancelOrderNotificationData cancelOrderNotificationData,
			final SalesApplication salesApplication)
	{
		final String getBookiedBy = SalesApplication.CALLCENTER.equals(salesApplication) ?
				"BCF CUSTOMER CARE" :
				Objects.nonNull(cancelOrderNotificationData.getCustomerData()) ?
						cancelOrderNotificationData.getCustomerData().getCrmId():
						"";
		return SalesApplication.TRAVELCENTRE.equals(salesApplication) ?
				"BCF Vacations" : getBookiedBy;
	}

	private Date getDateIssued(final Itinerary itinerary)
	{
		Date dateIssued = null;
		try
		{
			final DateFormat df = new SimpleDateFormat(BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
			dateIssued = df.parse(itinerary.getBooking().getBookedDateTime());
		}
		catch (ParseException e)
		{
			//do nothing
		}
		return dateIssued;
	}

}
