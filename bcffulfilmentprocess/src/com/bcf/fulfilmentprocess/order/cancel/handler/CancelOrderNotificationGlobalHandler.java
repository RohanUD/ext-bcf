/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.fulfilmentprocess.order.cancel.handler;

import com.bcf.integrations.search.booking.response.Itinerary;
import com.bcf.notification.request.cancel.order.CancelOrderNotificationData;


public interface CancelOrderNotificationGlobalHandler
{
	void handle(final Itinerary itinerary, final CancelOrderNotificationData cancelOrderNotificationData);
}
