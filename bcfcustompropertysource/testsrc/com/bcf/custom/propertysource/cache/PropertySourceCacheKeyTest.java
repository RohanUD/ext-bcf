/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.cache;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Locale;
import org.junit.Before;
import org.junit.Test;


/**
 * Unit Test for @PropertySourceCacheKey
 */
public class PropertySourceCacheKeyTest
{
	private static final String CODE = "test.code";
	private static final Locale LOCALE = Locale.ENGLISH;
	private static final String SITE = "testSite";

	private PropertySourceCacheKey initial;

	@Before
	public void setUp()
	{
		initial = new PropertySourceCacheKey(CODE, LOCALE, SITE);
	}

	@Test
	public void propertySourceCacheKeyTest()
	{
		final PropertySourceCacheKey nullKey = null;
		assertFalse(initial.equals(nullKey));
		assertFalse(initial.equals(new PropertySourceCacheKey(CODE, Locale.CHINESE, SITE)));
		assertFalse(initial.equals(new PropertySourceCacheKey("other.code", LOCALE, SITE)));
		assertFalse(initial.equals(new PropertySourceCacheKey("other", CODE, LOCALE, SITE)));

		final PropertySourceCacheKey pointer = initial;
		assertTrue(initial.equals(pointer));
		assertTrue(initial.equals(new PropertySourceCacheKey(CODE, LOCALE, SITE)));
	}
}
