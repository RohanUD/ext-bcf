/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.facade;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Locale;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;


/**
 * Integration tests for the @DefaultPropertySourceFacade class.
 */
public class DefaultPropertySourceFacadeIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String NON_EXISTING_PROPERTY_SOURCE_CODE = "NON_EXISTING_PROPERTY_SOURCE_CODE";
	private static final String TEST_PROPERTY_SOURCE_CODE = "TEST.CODE";
	private static final String TEST_PROPERTY_SOURCE_VALUE = "TEST.VALUE";
	private static final MessageCategory TEST_PROPERTY_SOURCE_MESSAGECATEGORY = MessageCategory.ERROR;
	private static final String TEST_PROPERTY_SOURCE_FORMNAME = "TEST.FORNAME";

	@Resource
	private PropertySourceFacade propertySourceFacade;

	@Resource
	private ModelService modelService;

	@Test
	public void findPropertySourceForCodeFacadeTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceData existingPropertySourceData = propertySourceFacade.getPropertySource(TEST_PROPERTY_SOURCE_CODE);
		assertNotNull(existingPropertySourceData);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceData.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceData.getValue());
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceData.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceData.getFormName());

	}

	@Test
	public void findPropertySourceForMessageCategoryFacadeTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceData existingPropertySourceData = propertySourceFacade
				.getPropertySourceByMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY.toString()).get(0);
		assertNotNull(existingPropertySourceData);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceData.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceData.getValue());
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceData.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceData.getFormName());

	}

	@Test
	public void findPropertySourceForFormNameFacadeTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceData existingPropertySourceData = propertySourceFacade
				.getPropertySourceByFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		assertNotNull(existingPropertySourceData);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceData.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceData.getValue());
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceData.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceData.getFormName());

	}


	@Test
	public void nonExistingPropertySourceFacadeSearchTest()
	{
		final PropertySourceData nonExistingPropertySourceData = propertySourceFacade
				.getPropertySource(NON_EXISTING_PROPERTY_SOURCE_CODE);
		assertNull(nonExistingPropertySourceData);
	}

	@Test
	public void propertySourceNullCodeServiceTest()
	{
		assertNull(propertySourceFacade.getPropertySource(null));
	}
}
