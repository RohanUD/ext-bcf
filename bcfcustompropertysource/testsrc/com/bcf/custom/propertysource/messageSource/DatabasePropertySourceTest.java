/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.messageSource;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import java.text.MessageFormat;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import com.bcf.custom.propertysource.DatabasePropertySource;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


/**
 * Unit tests for the @DatabasePropertySource class.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DatabasePropertySourceTest
{
	private static final Locale PROPERTY_SOURCE_LOCALE = Locale.ENGLISH;
	private static final String PROPERTY_SOURCE_CODE = "test.property.source.code";
	private static final String PROPERTY_SOURCE_DATABASE = "Test Database Property Source Value";
	private static final String PROPERTY_SOURCE_PROPERTIES = "Test Property File Property Source Value";

	@InjectMocks
	private final DatabasePropertySource databasePropertySource = new DatabasePropertySource();

	@Mock
	private PropertySourceFacade propertySourceFacade;

	@Mock
	private MessageSource propertiesMessageSource;

	@Before
	public void setUp()
	{
		databasePropertySource.setParentMessageSource(propertiesMessageSource);
	}

	@Test
	public void testResolveCodeWithDbPropertySource()
	{
		final PropertySourceData propertySourceData = new PropertySourceData();
		propertySourceData.setCode(PROPERTY_SOURCE_CODE);
		propertySourceData.setValue(PROPERTY_SOURCE_DATABASE);

		when(propertySourceFacade.getPropertySource(PROPERTY_SOURCE_CODE)).thenReturn(propertySourceData);

		final MessageFormat messageFormat = databasePropertySource.resolveCode(PROPERTY_SOURCE_CODE, PROPERTY_SOURCE_LOCALE);

		assertEquals(PROPERTY_SOURCE_DATABASE, messageFormat.toPattern());
		assertEquals(PROPERTY_SOURCE_LOCALE, messageFormat.getLocale());
	}

	@Test
	public void testResolveCodeWithPropertiesMessage()
	{
		when(propertySourceFacade.getPropertySource(PROPERTY_SOURCE_CODE)).thenReturn(null);

		when(propertiesMessageSource.getMessage(PROPERTY_SOURCE_CODE, null, PROPERTY_SOURCE_LOCALE)).thenReturn(
				PROPERTY_SOURCE_PROPERTIES);

		final MessageFormat messageFormat = databasePropertySource.resolveCode(PROPERTY_SOURCE_CODE, PROPERTY_SOURCE_LOCALE);

		assertEquals(PROPERTY_SOURCE_PROPERTIES, messageFormat.toPattern());
		assertEquals(PROPERTY_SOURCE_LOCALE, messageFormat.getLocale());
	}

	@Test
	public void testResolveCodeWithMissingPropertySourceWithFallback()
	{
		when(propertySourceFacade.getPropertySource(PROPERTY_SOURCE_CODE)).thenReturn(null);

		when(propertiesMessageSource.getMessage(PROPERTY_SOURCE_CODE, null, PROPERTY_SOURCE_LOCALE)).thenThrow(
				new NoSuchMessageException("Property Source Not found!"));

		databasePropertySource.setUseCodeAsDefaultMessage(true);

		final MessageFormat messageFormat = databasePropertySource.resolveCode(PROPERTY_SOURCE_CODE, PROPERTY_SOURCE_LOCALE);

		assertEquals(PROPERTY_SOURCE_CODE, messageFormat.toPattern());
		assertEquals(PROPERTY_SOURCE_LOCALE, messageFormat.getLocale());
	}

	@Test(expected = NoSuchMessageException.class)
	public void testResolveCodeWithMissingPropertySourceNoFallback()
	{
		when(propertySourceFacade.getPropertySource(PROPERTY_SOURCE_CODE)).thenReturn(null);

		when(propertiesMessageSource.getMessage(PROPERTY_SOURCE_CODE, null, PROPERTY_SOURCE_LOCALE)).thenThrow(
				new NoSuchMessageException("Property Source Not found!"));

		databasePropertySource.setUseCodeAsDefaultMessage(false);

		databasePropertySource.resolveCode(PROPERTY_SOURCE_CODE, PROPERTY_SOURCE_LOCALE);
	}
}
