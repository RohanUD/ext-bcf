/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.service;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Locale;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;


/**
 * Integration tests for the @DefaultPropertySourceService class.
 */
public class DefaultPropertySourceServiceIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String NON_EXISTING_PROPERTY_SOURCE_CODE = "NON_EXISTING_PROPERTY_SOURCE_CODE";
	private static final String TEST_PROPERTY_SOURCE_CODE = "TEST.CODE";
	private static final String TEST_PROPERTY_SOURCE_VALUE = "TEST.VALUE";
	private static final MessageCategory TEST_PROPERTY_SOURCE_MESSAGECATEGORY = MessageCategory.ERROR;
	private static final String TEST_PROPERTY_SOURCE_FORMNAME = "TEST.FORNAME";

	@Resource
	private PropertySourceService propertySourceService;

	@Resource
	private ModelService modelService;

	@Test
	public void findPropertySourceForCodeServiceTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceModel existingPropertySourceModel = propertySourceService
				.getPropertySourceForCode(TEST_PROPERTY_SOURCE_CODE);
		assertNotNull(existingPropertySourceModel);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceModel.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceModel.getValue(Locale.ENGLISH));
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceModel.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceModel.getFormName());

	}

	@Test
	public void findPropertySourceForFormNameServiceTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceModel existingPropertySourceModel = propertySourceService
				.getPropertySourceForFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		assertNotNull(existingPropertySourceModel);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceModel.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceModel.getValue(Locale.ENGLISH));
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceModel.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceModel.getFormName());

	}

	@Test
	public void findPropertySourceForMessageCatgeoryServiceTest()
	{
		final PropertySourceModel propertySourceModel = new PropertySourceModel();
		propertySourceModel.setCode(TEST_PROPERTY_SOURCE_CODE);
		propertySourceModel.setValue(TEST_PROPERTY_SOURCE_VALUE, Locale.ENGLISH);
		propertySourceModel.setMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY);
		propertySourceModel.setFormName(TEST_PROPERTY_SOURCE_FORMNAME);
		modelService.save(propertySourceModel);

		final PropertySourceModel existingPropertySourceModel = propertySourceService
				.getPropertySourceForMessageCategory(TEST_PROPERTY_SOURCE_MESSAGECATEGORY.toString()).get(0);
		assertNotNull(existingPropertySourceModel);
		assertEquals(TEST_PROPERTY_SOURCE_CODE, existingPropertySourceModel.getCode());
		assertEquals(TEST_PROPERTY_SOURCE_VALUE, existingPropertySourceModel.getValue(Locale.ENGLISH));
		assertEquals(TEST_PROPERTY_SOURCE_MESSAGECATEGORY, existingPropertySourceModel.getMessageCategory());
		assertEquals(TEST_PROPERTY_SOURCE_FORMNAME, existingPropertySourceModel.getFormName());

	}

	@Test
	public void nonExistingPropertySourceServiceSearchTest()
	{
		final PropertySourceModel nonExistingPropertySourceModel = propertySourceService
				.getPropertySourceForCode(NON_EXISTING_PROPERTY_SOURCE_CODE);
		assertNull(nonExistingPropertySourceModel);
	}

	@Test
	public void propertySourceNullCodeServiceTest()
	{
		assertNull(propertySourceService.getPropertySourceForCode(null));
	}
}
