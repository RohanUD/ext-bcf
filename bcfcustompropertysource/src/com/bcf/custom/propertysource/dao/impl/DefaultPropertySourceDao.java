/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.dao.impl;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.bcf.custom.propertysource.dao.PropertySourceDao;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;


public class DefaultPropertySourceDao extends DefaultGenericDao<PropertySourceModel> implements PropertySourceDao
{

	private static final String CODE_PARAMETER = "code";
	private static final String FROM = "} FROM {";

	private static final String MESSAGE_CATEGORY_PARAMETER = "MessageCategory";

	private static final String PROPERTY_SOURCE_CODE_IS_NOT_UNIQUE = "PropertySource code %s is not unique.";

	public static final String SELECT = "Select {";
	public static final String WHERE = "} WHERE {";
	private static final String PROPERTY_SOURCE_LOOKUP_QUERY = SELECT + PropertySourceModel.PK + FROM
			+ PropertySourceModel._TYPECODE + WHERE + PropertySourceModel.CODE + "}=?code";

	private static final String PROPERTY_SOURCE_LOOKUP_QUERY_BY_FORMNAME = SELECT + PropertySourceModel.PK + FROM
			+ PropertySourceModel._TYPECODE + WHERE + PropertySourceModel.FORMNAME + "}=?" + PropertySourceModel.FORMNAME;

	private static final String PROPERTY_SOURCE_LOOKUP_QUERY_BY_FORMNAME_AND_MESSAGE_CATEGORY =
			SELECT + PropertySourceModel.PK + FROM
					+ PropertySourceModel._TYPECODE + WHERE + PropertySourceModel.FORMNAME + "}=?"
					+ PropertySourceModel.FORMNAME
					+ " AND {" + PropertySourceModel.MESSAGECATEGORY + "}=?"
					+ PropertySourceModel.MESSAGECATEGORY;

	private static final String PROPERTY_SOURCE_LOOKUP_QUERY_BY_CATEGORY = "Select {ps." + PropertySourceModel.PK + FROM
			+ PropertySourceModel._TYPECODE + " AS ps JOIN " + MESSAGE_CATEGORY_PARAMETER + " AS mc ON {ps."
			+ MESSAGE_CATEGORY_PARAMETER + "}={mc.pk}} WHERE {mc." + CODE_PARAMETER + "}=?" + CODE_PARAMETER;

	public DefaultPropertySourceDao()
	{
		super(PropertySourceModel._TYPECODE);
	}

	public DefaultPropertySourceDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public PropertySourceModel findPropertySourceByCode(final String code)
	{
		if (StringUtils.isEmpty(code))
		{
			return null;
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(PROPERTY_SOURCE_LOOKUP_QUERY);
		query.addQueryParameter(CODE_PARAMETER, code);
		final SearchResult<PropertySourceModel> resList = getFlexibleSearchService().search(query);

		final int resCount = resList.getTotalCount();
		if (resCount == 0)
		{
			return null;
		}
		else if (resCount > 1)
		{
			throw new AmbiguousIdentifierException(String.format(PROPERTY_SOURCE_CODE_IS_NOT_UNIQUE, code));
		}
		return resList.getResult().get(0);
	}

	@Override
	public PropertySourceModel findPropertySourceByFormName(final String formName)
	{
		if (StringUtils.isEmpty(formName))
		{
			return null;
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(PROPERTY_SOURCE_LOOKUP_QUERY_BY_FORMNAME);
		query.addQueryParameter(PropertySourceModel.FORMNAME, formName);
		final SearchResult<PropertySourceModel> resList = getFlexibleSearchService().search(query);

		final int resCount = resList.getTotalCount();
		if (resCount == 0)
		{
			return null;
		}

		return resList.getResult().get(0);

	}

	@Override
	public List<PropertySourceModel> findPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory)
	{
		if (StringUtils.isEmpty(formName))
		{
			return Collections.emptyList();
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(PROPERTY_SOURCE_LOOKUP_QUERY_BY_FORMNAME_AND_MESSAGE_CATEGORY);
		query.addQueryParameter(PropertySourceModel.FORMNAME, formName);
		query.addQueryParameter(PropertySourceModel.MESSAGECATEGORY, messageCategory);
		final SearchResult<PropertySourceModel> resList = getFlexibleSearchService().search(query);
		return resList.getResult();
	}

	@Override
	public List<PropertySourceModel> findPropertySourceByMessageCategory(final String messageCategory)
	{

		if (StringUtils.isEmpty(messageCategory))
		{
			return Collections.emptyList();
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(PROPERTY_SOURCE_LOOKUP_QUERY_BY_CATEGORY);
		query.addQueryParameter(CODE_PARAMETER, messageCategory);
		final SearchResult<PropertySourceModel> resList = getFlexibleSearchService().search(query);

		final int resCount = resList.getTotalCount();
		if (resCount == 0)
		{
			return Collections.emptyList();
		}

		return resList.getResult();
	}

}
