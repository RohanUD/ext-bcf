/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.dao;

import java.util.List;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;


/**
 * Provides data access to @PropertySource
 */
public interface PropertySourceDao
{
	/**
	 * Returns the @PropertySourceModel for the given code. If no @PropertySourceModel is found @Null object is returned.
	 */
	PropertySourceModel findPropertySourceByCode(final String code);

	/**
	 * Returns the @PropertySourceModel for the given formName. If no @PropertySourceModel is found @Null object is
	 * returned.
	 */
	PropertySourceModel findPropertySourceByFormName(final String formName);

	/**
	 * Returns the List of @PropertySourceModel for the given formName category and message. If no @PropertySourceModel is found @Null object is
	 * returned.
	 */
	List<PropertySourceModel> findPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory);

	/**
	 * Returns the List of @PropertySourceModel for the given messageCategory. If no @PropertySourceModel is found @Null
	 * object is returned.
	 */
	List<PropertySourceModel> findPropertySourceByMessageCategory(final String messageCategory);

}
