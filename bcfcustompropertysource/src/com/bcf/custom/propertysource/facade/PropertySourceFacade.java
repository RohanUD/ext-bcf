/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.facade;

import java.util.List;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.enums.MessageCategory;


/**
 * Facade retrieves a @PropertySourceData objects
 */
public interface PropertySourceFacade
{
	/**
	 * Retrieves @PropertySourceData for the specific code. If @PropertySourceData is not found a @Null object is
	 * returned.
	 *
	 * @param code unique code
	 * @return the propertySource
	 */
	PropertySourceData getPropertySource(final String code);

	/**
	 * Retrieves @PropertySourceData for the specific formName. If @PropertySourceData is not found a @Null object is
	 * returned.
	 *
	 * @param formName
	 * @return the propertySource
	 */
	PropertySourceData getPropertySourceByFormName(final String formName);

	/**
	 * Retrieves List of @PropertySourceData for the specific formName and messageCategory. If @PropertySourceData is not found a @Null object is
	 * returned.
	 *
	 * @param formName
	 * @param messageCategory
	 * @return the propertySource
	 */
	List<PropertySourceData> getPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory);


	/**
	 * Retrieves @PropertySourceData for the specific messageCategory. If @PropertySourceData is not found a @Null object
	 * is returned.
	 *
	 * @param messageCategory
	 * @return the List of propertySource
	 */
	List<PropertySourceData> getPropertySourceByMessageCategory(final String messageCategory);


	/**
	 * Retrieves PropertyValue for the specific code. If PropertyValue is not found then null will returned.
	 *
	 * @param code unique code
	 * @return the PropertyValue
	 **/
	String getPropertySourceValue(String code);

	/**
	 * Retrieves PropertyValue for the specific code. If PropertyValue is not found then optionalValue will returned.
	 *
	 * @param code          unique code
	 * @param optionalValue If PropertyValue is not found then optionalValue will return
	 * @return the PropertyValue
	 **/
	String getPropertySourceValue(String code, String optionalValue);

	/**
	 * Retrieves PropertyValue for the specific code. If PropertyValue is not found then null will returned.
	 *
	 * @param code             unique code
	 * @param messageArguments arguments to set in message
	 * @return the PropertyValue
	 **/
	String getPropertySourceValueMessage(String code, Object[] messageArguments);

	/**
	 * Retrieves PropertyValue for the specific code. If PropertyValue is not found then optionalValue will returned.
	 *
	 * @param code             unique code
	 * @param optionalValue    If PropertyValue is not found then optionalValue will return
	 * @param messageArguments arguments to set in message
	 * @return the PropertyValue
	 **/
	String getPropertySourceValueMessage(String code, String optionalValue, Object[] messageArguments);

	PropertySourceData getAdditionalInfoSource(final String code);


}
