/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.facade.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringEscapeUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.custom.propertysource.model.PropertySourceModel;
import com.bcf.custom.propertysource.service.PropertySourceService;


/**
 * Default Implementation of PropertySourceFacade interface.
 */
public class DefaultPropertySourceFacade implements PropertySourceFacade
{
	private PropertySourceService propertySourceService;

	@Override
	public PropertySourceData getPropertySource(final String code)
	{
		if (code == null)
		{
			return null;
		}

		final PropertySourceModel propertySourceModel = propertySourceService.getPropertySourceForCode(code);

		if (propertySourceModel == null)
		{
			return null;
		}

		return createpPropertySourceData(propertySourceModel);
	}


	@Override
	public PropertySourceData getPropertySourceByFormName(final String formName)
	{

		if (formName == null)
		{
			return null;
		}

		final PropertySourceModel propertySourceModel = propertySourceService.getPropertySourceForFormName(formName);

		if (propertySourceModel == null)
		{
			return null;
		}

		return createpPropertySourceData(propertySourceModel);
	}

	@Override
	public List<PropertySourceData> getPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory)
	{
		return StreamUtil.safeStream(propertySourceService
				.getPropertySourceByFormNameAndMessageCategory(formName, messageCategory)).map(this::createpPropertySourceData)
				.collect(
						Collectors.toList());
	}

	@Override
	public List<PropertySourceData> getPropertySourceByMessageCategory(final String messageCategory)
	{

		if (messageCategory == null)
		{
			return Collections.emptyList();
		}

		final List<PropertySourceModel> propertySourceModels = propertySourceService
				.getPropertySourceForMessageCategory(messageCategory);

		if (propertySourceModels.isEmpty())
		{
			return Collections.emptyList();
		}
		final List<PropertySourceData> propertySourceDatas = new ArrayList<>();

		propertySourceModels.forEach(property -> propertySourceDatas.add(createpPropertySourceData(property)));

		return propertySourceDatas;
	}


	@Override
	public String getPropertySourceValue(final String code)
	{
		return getPropertySourceValue(code, null);
	}

	@Override
	public String getPropertySourceValueMessage(final String code, final Object[] messageArguments)
	{
		return getPropertySourceValueMessage(code, null, messageArguments);
	}

	@Override
	public String getPropertySourceValue(final String code, final String optionalValue)
	{
		final PropertySourceData propertySourceData = getPropertySource(code);
		if (propertySourceData != null)
		{
			return propertySourceData.getValue();
		}
		return optionalValue;
	}

	@Override
	public String getPropertySourceValueMessage(final String code, final String optionalValue, final Object[] messageArguments)
	{
		final PropertySourceData propertySourceData = getPropertySource(code);
		if (propertySourceData != null)
		{
			propertySourceData.getValue();
			return getMessageWithValues(propertySourceData.getValue(), messageArguments);
		}
		return getMessageWithValues(optionalValue, messageArguments);
	}

	@Override
	public PropertySourceData getAdditionalInfoSource(final String code)
	{
		if (code == null)
		{
			return null;
		}
		final PropertySourceModel propertySourceModel = propertySourceService.getPropertySourceForCode(code);

		if (propertySourceModel == null)
		{
			return null;
		}

		return createAdditionInfoData(propertySourceModel);
	}

	private PropertySourceData createAdditionInfoData(final PropertySourceModel propertySourceModel)
	{
		final PropertySourceData propertySourceData = new PropertySourceData();
		propertySourceData.setCode(propertySourceModel.getCode());
		propertySourceData.setTitle(StringEscapeUtils.escapeHtml4(propertySourceModel.getInfoTitle()));
		propertySourceData.setDescription(StringEscapeUtils.escapeHtml4(propertySourceModel.getInfoDescription()));
		return propertySourceData;
	}

	private String getMessageWithValues(final String value, final Object[] messageArguments)
	{
		if (value != null && messageArguments != null && messageArguments.length != 0)
		{
			final Locale locale = Locale.ENGLISH;
			final MessageFormat messageFormat = new MessageFormat(value, locale);
			return messageFormat.format(messageArguments, new StringBuffer(), null).toString();
		}
		return value;
	}

	private PropertySourceData createpPropertySourceData(final PropertySourceModel propertySourceModel)
	{
		final PropertySourceData propertySourceData = new PropertySourceData();
		propertySourceData.setCode(propertySourceModel.getCode());
		propertySourceData.setValue(propertySourceModel.getValue());
		propertySourceData.setMessageCategory(propertySourceModel.getMessageCategory());
		propertySourceData.setFormName(propertySourceModel.getFormName());
		return propertySourceData;
	}



	/**
	 * @return the propertySourceService
	 */
	public PropertySourceService getPropertySourceService()
	{
		return propertySourceService;
	}

	/**
	 * @param propertySourceService the propertySourceService to set
	 */
	public void setPropertySourceService(final PropertySourceService propertySourceService)
	{
		this.propertySourceService = propertySourceService;
	}

}
