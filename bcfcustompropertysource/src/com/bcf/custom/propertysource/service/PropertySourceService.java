/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.service;

import java.util.List;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;


/**
 * Service retrieves @PropertySourceModel
 */
public interface PropertySourceService
{
	/**
	 * Retrieves a @PropertySourceModel for the specific code. If @PropertySourceModel is not found a @Null object is
	 * returned.
	 *
	 * @param code unique code
	 * @return the PropertySource
	 */
	PropertySourceModel getPropertySourceForCode(final String code);

	/**
	 * Retrieves a @PropertySourceModel for the specific formName. If @PropertySourceModel is not found a @Null object is
	 * returned.
	 *
	 * @param formName
	 * @return the PropertySource
	 */
	PropertySourceModel getPropertySourceForFormName(final String formName);

	/**
	 * Retrieves a list of @PropertySourceModel for the specific formName and messageCategory. If @PropertySourceModel is not found a @Null object is
	 * returned.
	 *
	 * @param formName
	 * @param messageCategory
	 * @return the PropertySource
	 */
	List<PropertySourceModel> getPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory);


	/**
	 * Retrieves all @PropertySourceModel for the specific messageCategory. If @PropertySourceModel is not found a @Null
	 * object is returned.
	 *
	 * @param messageCategory
	 * @return the List of PropertySource
	 */
	List<PropertySourceModel> getPropertySourceForMessageCategory(final String messageCategory);
}
