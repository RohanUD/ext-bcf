/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.service.impl;

import java.util.List;
import com.bcf.custom.propertysource.dao.PropertySourceDao;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.model.PropertySourceModel;
import com.bcf.custom.propertysource.service.PropertySourceService;


/**
 * Default Implementation of PropertySourceService interface.
 */
public class DefaultPropertySourceService implements PropertySourceService
{
	private PropertySourceDao propertySourceDao;

	@Override
	public PropertySourceModel getPropertySourceForCode(final String code)
	{
		return getPropertySourceDao().findPropertySourceByCode(code);
	}

	@Override
	public PropertySourceModel getPropertySourceForFormName(final String formName)
	{

		return getPropertySourceDao().findPropertySourceByFormName(formName);
	}

	@Override
	public List<PropertySourceModel> getPropertySourceByFormNameAndMessageCategory(final String formName,
			final MessageCategory messageCategory)
	{
		return getPropertySourceDao().findPropertySourceByFormNameAndMessageCategory(formName, messageCategory);
	}

	@Override
	public List<PropertySourceModel> getPropertySourceForMessageCategory(final String messageCategory)
	{

		return getPropertySourceDao().findPropertySourceByMessageCategory(messageCategory);
	}

	/**
	 * @return the propertySourceDao
	 */
	public PropertySourceDao getPropertySourceDao()
	{
		return propertySourceDao;
	}

	/**
	 * @param propertySourceDao the propertySourceDao to set
	 */
	public void setPropertySourceDao(final PropertySourceDao propertySourceDao)
	{
		this.propertySourceDao = propertySourceDao;
	}



}
