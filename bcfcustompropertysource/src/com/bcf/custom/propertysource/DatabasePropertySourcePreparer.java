/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource;

import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.support.MethodReplacer;
import org.springframework.context.MessageSource;
import org.springframework.util.Assert;


public class DatabasePropertySourcePreparer implements MethodReplacer
{
	private static final Logger LOG = Logger.getLogger(DatabasePropertySourcePreparer.class);

	private static final String THE_REPLACEMENT_METHOD_REQUIRES_ONE_ARGUMENT = "The replacement method requires one argument";

	private static final String THE_REPLACEMENT_METHOD_REQUIRES_AN_ARGUMENT = "The replacement method requires an argument";

	private static final String THE_REPLACEMENT_METHOD_PARAMETER_MUST_BE = "The replacement method parameter must be ";

	private static final String THE_REPLACEMENT_METHOD_RETURN_TYPE_MUST_BE = "The replacement method return type must be ";

	private static final String THE_REPLACEMENT_METHOD_NAME_MUST_BE = "The replacement method name must be ";

	private static final String THE_REPLACEMENT_METHOD_CANNOT_BE_NULL = "The replacement method cannot be null";

	private static final String METHOD_NAME = "prepareMessageSource";

	private DatabasePropertySource databasePropertySource;

	@Override
	public Object reimplement(final Object target, final Method method, final Object[] args)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Attempting to re-implement method " + METHOD_NAME + " with " + method);
		}

		validateMethod(method, args);
		getDatabasePropertySource().setParentMessageSource((MessageSource) args[0]);
		return getDatabasePropertySource();
	}

	protected void validateMethod(final Method method, final Object[] args)
	{
		Assert.notNull(method, THE_REPLACEMENT_METHOD_CANNOT_BE_NULL);
		Assert.isTrue(METHOD_NAME.equals(method.getName()), THE_REPLACEMENT_METHOD_NAME_MUST_BE + METHOD_NAME);
		Assert.isTrue(method.getReturnType().equals(MessageSource.class), THE_REPLACEMENT_METHOD_RETURN_TYPE_MUST_BE
				+ MessageSource.class.getSimpleName());
		Assert.isTrue(method.getParameterTypes()[0].equals(MessageSource.class), THE_REPLACEMENT_METHOD_PARAMETER_MUST_BE
				+ MessageSource.class.getSimpleName());

		Assert.notNull(args, THE_REPLACEMENT_METHOD_REQUIRES_AN_ARGUMENT);
		Assert.isTrue(args.length == 1, THE_REPLACEMENT_METHOD_REQUIRES_ONE_ARGUMENT);
	}

	/**
	 * @return the databasePropertySource
	 */
	public DatabasePropertySource getDatabasePropertySource()
	{
		return databasePropertySource;
	}

	/**
	 * @param databasePropertySource the databasePropertySource to set
	 */
	@Required
	public void setDatabasePropertySource(final DatabasePropertySource databasePropertySource)
	{
		this.databasePropertySource = databasePropertySource;
	}

}
