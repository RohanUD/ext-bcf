/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package com.bcf.custom.propertysource.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.custom.propertysource.constants.BcfcustompropertysourceConstants;


@SuppressWarnings("PMD")
public class BcfcustompropertysourceManager extends GeneratedBcfcustompropertysourceManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(BcfcustompropertysourceManager.class.getName());

	public static final BcfcustompropertysourceManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfcustompropertysourceManager) em.getExtension(BcfcustompropertysourceConstants.EXTENSIONNAME);
	}

}
