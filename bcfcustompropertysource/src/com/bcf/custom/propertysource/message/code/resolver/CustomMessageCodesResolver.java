/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource.message.code.resolver;

import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.util.StringUtils;
import org.springframework.validation.DefaultMessageCodesResolver;


/**
 * @author marcinmazgaj
 */
public class CustomMessageCodesResolver extends DefaultMessageCodesResolver
{

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.MessageCodesResolver#resolveMessageCodes(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.Class)
	 */
	@Override
	public String[] resolveMessageCodes(final String paramString1, final String paramString2, final String paramString3,
			final Class<?> paramClass)
	{
		final Set codeList = new LinkedHashSet();
		codeList.add(paramString1);
		return StringUtils.toStringArray(codeList);
	}

}

