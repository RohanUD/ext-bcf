/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.propertysource;


import de.hybris.platform.site.BaseSiteService;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Objects;
import javax.annotation.Resource;
import org.springframework.context.support.AbstractMessageSource;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class DatabasePropertySource extends AbstractMessageSource
{

	@Resource
	private PropertySourceFacade propertySourceFacade;

	@Resource
	private BaseSiteService baseSiteService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MessageFormat resolveCode(final String code, final Locale locale)
	{
		final String message = getMessage(code);

		if (Objects.isNull(message))
		{
			return null;
		}
		else
		{
			return createMessageFormat(message, locale);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String resolveCodeWithoutArguments(final String code, final Locale locale)
	{
		return getMessage(code);

	}

	private String getMessage(final String code)
	{
		return propertySourceFacade.getPropertySourceValue(code, code);
	}

}
