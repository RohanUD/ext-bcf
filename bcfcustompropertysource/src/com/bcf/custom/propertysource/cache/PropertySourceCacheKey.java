/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.bcf.custom.propertysource.cache;

import de.hybris.platform.regioncache.key.AbstractCacheKey;
import java.util.Locale;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;


public class PropertySourceCacheKey extends AbstractCacheKey
{
	private static final String DEFAULT_TENANT = "master";
	private static final String TYPE_CODE = "PropertySource";

	private final String key;

	/**
	 * Creates a message bundle.
	 *
	 * @param code   The code
	 * @param locale The locale
	 * @param site   The site
	 */
	public PropertySourceCacheKey(final String code, final Locale locale, final String site)
	{
		this(DEFAULT_TENANT, code, locale, site);
	}

	/**
	 * Creates a message bundle with specific tenant.
	 *
	 * @param tenant The tenant
	 * @param code   The code
	 * @param locale The locale
	 * @param site   The site
	 */
	protected PropertySourceCacheKey(final String tenant, final String code, final Locale locale, final String site)
	{
		super(TYPE_CODE, tenant);
		key = typeCode.toString() + "-" + code + "-" + locale.toString() + "-" + site;
	}

	@Override
	public int hashCode()
	{
		return key.hashCode();
	}

	@Override
	public String toString()
	{
		return "[" + getClass().getSimpleName() + ":" + getTenantId() + ":" + getKey() + "]";
	}

	protected String getKey()
	{
		return key;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null)
		{
			return false;
		}

		if (getClass() != obj.getClass())
		{
			return false;
		}

		final PropertySourceCacheKey other = (PropertySourceCacheKey) obj;
		return StringUtils.equals(tenantId, other.getTenantId()) && Objects.equals(typeCode, other.getTypeCode())
				&& StringUtils.equals(key, other.getKey());
	}
}
