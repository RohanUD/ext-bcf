/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import java.io.UnsupportedEncodingException;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.integration.crm.service.CRMSearchCustomerService;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


@IntegrationTest
public class DefaultCRMIntegrationServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private CRMUpdateCustomerService crmUpdateCustomerService;
	@Resource
	private CRMSearchCustomerService crmSearchCustomerService;
	@Resource
	private CommonI18NService commonI18NService;

	@Test
	public void shouldReturnRolesForTestUser() throws Exception
	{
		// given
		final CustomerModel customerModel = modelService.create(CustomerModel.class);
		customerModel.setName("FirstName LastName");
		final TitleModel titleModel = modelService.create(TitleModel.class);
		titleModel.setCode("mr");
		customerModel.setTitle(titleModel);
		customerModel.setSessionLanguage(commonI18NService.getCurrentLanguage());
		customerModel.setSessionCurrency(commonI18NService.getCurrentCurrency());
		customerModel.setAccountValidationKey("testKey");
		customerModel.setLdapaccount(Boolean.TRUE);
		customerModel.setLdaplogin("test");

		// when
		final UpdateCRMCustomerResponseDTO response = crmUpdateCustomerService.updateCustomer(customerModel);

		// then
		assertThat(response.getBCF_Customer().getSystemIdentifier().get(0).getID()).isNotNull();
	}

	@Test
	public void shouldReturnCRMCustomerForSearchTerm() throws IntegrationException, UnsupportedEncodingException
	{
		// given
		final String userName = "test";

		// when
		final CRMGetCustomerResponseDTO response = crmSearchCustomerService.getCustomer(userName,false);

		// then
		assertThat(response).isNotNull();
		assertThat(response.getBCF_Customer()).isNotNull();
	}
}
