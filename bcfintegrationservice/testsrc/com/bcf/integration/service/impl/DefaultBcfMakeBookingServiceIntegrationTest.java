/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.service.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.fest.assertions.Assertions;
import org.junit.Test;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.ebooking.service.MakeBookingService;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


@IntegrationTest
public class DefaultBcfMakeBookingServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource
	private MakeBookingService makeBookingService;
	@Resource
	private UserService userService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private SessionService sessionService;

	@Test
	public void testNumberOfProductsWithoutProduct()
	{
		sessionService.getCurrentSession().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());
		final AddBundleToCartRequestData requestData = new AddBundleToCartRequestData();
		final AddBundleToCartRequestData data = prepareTestRequestDatawithOutProducts(requestData);
		final BCFMakeBookingResponse response;
		try
		{
			response = makeBookingService.getMakeBookingResponse(data);
			Assertions.assertThat(response).isNotNull();
		}
		catch (IntegrationException e)
		{
			//
		}
	}

	@Test
	public void testProductWhichDoesNotExist()
	{
		sessionService.getCurrentSession().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());
		final AddBundleToCartRequestData requestData = new AddBundleToCartRequestData();
		final AddBundleToCartRequestData data = prepareRequestDataForProductWhichDoesNotExist(requestData);
		try
		{
			final BCFMakeBookingResponse response = makeBookingService.getMakeBookingResponse(data);
			Assertions.assertThat(response).isNotNull();
		}
		catch (IntegrationException e)
		{
			//
		}
	}

	@Test
	public void testNumberOfProductsWithThreeProducts()
	{
		sessionService.getCurrentSession().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());
		final AddBundleToCartRequestData requestData = new AddBundleToCartRequestData();
		final AddBundleToCartRequestData data = prepareRequestDataForThreeProducts(requestData);
		final BCFMakeBookingResponse response;
		try
		{
			response = makeBookingService.getMakeBookingResponse(data);
			assertEquals(3,
					response.getItinerary().getSailing().getLine().stream().findFirst().get().getSailingPrices().stream().findFirst()
							.get().getProductFares()
							.size());
		}
		catch (IntegrationException e)
		{
			//
		}

	}

	private AddBundleToCartRequestData prepareRequestDataForThreeProducts(AddBundleToCartRequestData requestData)
	{
		final List<PassengerTypeQuantityData> passengerTypesList = new ArrayList<>();
		final List<VehicleTypeQuantityData> vehicleTypesList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypes = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleData = new VehicleTypeData();
		final PassengerTypeQuantityData passengerTypes = new PassengerTypeQuantityData();
		final PassengerTypeData passengerData = new PassengerTypeData();
		passengerData.setBcfCode("AD");
		passengerData.setCode("adult");
		passengerTypes.setQuantity(2);
		passengerTypes.setPassengerType(passengerData);
		passengerTypesList.add(passengerTypes);

		final PassengerTypeQuantityData passengerTypes2 = new PassengerTypeQuantityData();
		final PassengerTypeData passengerData2 = new PassengerTypeData();
		passengerData2.setBcfCode("CH");
		passengerData2.setCode("child");
		passengerTypes2.setQuantity(1);
		passengerTypes2.setPassengerType(passengerData2);
		passengerTypesList.add(passengerTypes2);
		requestData.setPassengerTypes(passengerTypesList);

		requestData.setSelectedJourneyRefNumber(150000);
		requestData.setSelectedOdRefNumber(15001);
		requestData.setSalesApplication(SalesApplication.WEB);

		vehicleData.setCode("UH");
		vehicleTypes.setVehicleType(vehicleData);
		vehicleTypes.setQty(1);
		vehicleTypes.setLength(2000);
		vehicleTypesList.add(vehicleTypes);
		requestData.setVehicleTypes(vehicleTypesList);

		requestData = createBundleDataForRequest(requestData);
		return requestData;
	}

	public AddBundleToCartRequestData prepareTestRequestDatawithOutProducts(AddBundleToCartRequestData requestData)
	{
		requestData = createBundleDataForRequest(requestData);
		requestData.setPassengerTypes(Collections.emptyList());
		requestData.setVehicleTypes(Collections.emptyList());
		requestData.setSelectedJourneyRefNumber(150000);
		requestData.setSelectedOdRefNumber(15001);
		requestData.setSalesApplication(SalesApplication.WEB);
		return requestData;
	}

	private AddBundleToCartRequestData prepareRequestDataForProductWhichDoesNotExist(AddBundleToCartRequestData requestData)
	{
		requestData = createBundleDataForRequest(requestData);
		requestData = createProductDataForRequest(requestData, "ADD", "UHH");
		return requestData;
	}

	public AddBundleToCartRequestData createProductDataForRequest(final AddBundleToCartRequestData requestData,
			final String VehicleCode, final String passengertCode)
	{
		final List<PassengerTypeQuantityData> passengerTypesList = new ArrayList<>();
		final List<VehicleTypeQuantityData> vehicleTypesList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypes = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleData = new VehicleTypeData();
		final PassengerTypeQuantityData passengerTypes = new PassengerTypeQuantityData();
		final PassengerTypeData passengerData = new PassengerTypeData();
		passengerData.setBcfCode(passengertCode);
		passengerData.setCode("adult");
		passengerTypes.setQuantity(2);
		passengerTypes.setPassengerType(passengerData);
		passengerTypesList.add(passengerTypes);
		requestData.setPassengerTypes(passengerTypesList);

		requestData.setSelectedJourneyRefNumber(150000);
		requestData.setSelectedOdRefNumber(15001);
		requestData.setSalesApplication(SalesApplication.WEB);

		vehicleData.setCode(VehicleCode);
		vehicleTypes.setVehicleType(vehicleData);
		vehicleTypes.setQty(1);
		vehicleTypes.setLength(2000);
		vehicleTypesList.add(vehicleTypes);
		requestData.setVehicleTypes(vehicleTypesList);
		return requestData;
	}

	public AddBundleToCartRequestData createBundleDataForRequest(final AddBundleToCartRequestData requestData)
	{
		final BcfAddBundleToCartData bundleData = new BcfAddBundleToCartData();
		final List<AddBundleToCartData> cartDataList = new ArrayList<>();
		final List<TransportOfferingData> transportOfferingDatas = new ArrayList<>();
		final TransportOfferingData offeringData = new TransportOfferingData();

		Calendar departureDate = Calendar.getInstance();
		departureDate.add(Calendar.WEEK_OF_YEAR,1);
		offeringData.setDepartureTime(departureDate.getTime());
		transportOfferingDatas.add(offeringData);
		bundleData.setTransportOfferingDatas(transportOfferingDatas);
		bundleData.setTravelRouteCode("TSA-SWB");
		cartDataList.add(bundleData);
		requestData.setAddBundleToCartData(cartDataList);
		return requestData;
	}
}
