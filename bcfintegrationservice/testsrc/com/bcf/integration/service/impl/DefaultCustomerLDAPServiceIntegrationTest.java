/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 15/12/18 15:57
 */

package com.bcf.integration.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.integration.activedirectory.service.BCFCustomerLDAPService;
import com.bcf.integration.data.CreateCustomerResponseDTO;
import com.bcf.integration.data.VerifyCustomerResponseDTO;


@IntegrationTest
public class DefaultCustomerLDAPServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource(name = "customerLDAPService")
	private BCFCustomerLDAPService customerLDAPService;

	@Test
	public void shouldReturnAccountValidationKeyForTestUser() throws Exception
	{
		// given
		final RegisterData registerData = new RegisterData();
		registerData.setFirstName("FirstName");
		registerData.setLastName("LastName");
		registerData.setLogin("test@test.com");
		registerData.setPassword("password");
		registerData.setTitleCode("mr");

		// when
		final CreateCustomerResponseDTO userResponse = customerLDAPService.registerAtLDAP(registerData);

		// then
		assertThat(userResponse).isNotNull();
		assertThat(userResponse.getCustomer()).isNotNull();
	}

	@Test
	public void shouldReturnTrueForValidationKey() throws Exception
	{
		// given
		final String accountValidationKey = "8701dc00-3dee-4bcd-85d1-46940fa9b8de";
		final String uid = "test@test.com";

		// when
		final VerifyCustomerResponseDTO response = customerLDAPService.verifyAtLDAP(accountValidationKey, uid);

		// then
		assertThat(response).isNotNull();
	}


}
