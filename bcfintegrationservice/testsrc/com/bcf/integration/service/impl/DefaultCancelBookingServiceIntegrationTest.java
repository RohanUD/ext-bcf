/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.core.exception.IntegrationException;


@IntegrationTest
public class DefaultCancelBookingServiceIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String TEST_BASESITE_UID = "testSite";
	private static final String TEST_BASESTORE_UID = "testStore";

	@Resource
	private CancelBookingService cancelBookingService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private SessionService sessionService;

	@BeforeClass
	public static void beforeClass()
	{
		Registry.setCurrentTenantByID("junit");
	}

	@Before
	public void setUp() throws Exception
	{
		importCsv("/test/testCommerceCart.csv", "utf-8");
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(TEST_BASESITE_UID), false);
	}

	@Test
	public void testCancelBooking()
	{
		final UserModel ahertz = userService.getUserForUID("ahertz");
		final Collection<CartModel> cartModels = ahertz.getCarts();
		final BaseStoreModel store = baseStoreService.getBaseStoreForUid(TEST_BASESTORE_UID);
		store.setNet(true);
		modelService.save(store);
		Assert.assertEquals(cartModels.size(), 1);
		final CartModel cart = cartModels.iterator().next();
		sessionService.getCurrentSession().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());

		cart.getEntries().forEach(entry -> entry.setTravelOrderEntryInfo(createTravelOrderEntryInfoModel(0)));
		cart.getEntries().forEach(entry -> entry.setBookingReference("INTBOOK-0001"));
		final CancelBookingResponseForCart response;
		try
		{
			response = cancelBookingService.cancelBooking(cart,
					filterEntries(cart, true, null));
			Assertions.assertThat(response).isNotNull();
		}
		catch (IntegrationException e)
		{
			//
		}

	}

	protected TravelOrderEntryInfoModel createTravelOrderEntryInfoModel(final int originDestinationRefNumber)
	{
		final TravelOrderEntryInfoModel travelOrderEntryInfo = new TravelOrderEntryInfoModel();
		travelOrderEntryInfo.setOriginDestinationRefNumber(originDestinationRefNumber);
		return travelOrderEntryInfo;
	}

	protected List<AbstractOrderEntryModel> filterEntries(final AbstractOrderModel order, final boolean emptyCart,
			final List<CartFilterParamsDto> cartFilterParamsDtos)
	{
		return order.getEntries().stream().filter(entry -> entry.getActive()
			&& entry.getProduct().getItemtype().equals(ProductModel._TYPECODE)).
					collect(Collectors.toList());
	}
}
