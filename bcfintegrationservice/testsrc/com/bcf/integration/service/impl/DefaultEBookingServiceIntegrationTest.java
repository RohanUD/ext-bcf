/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/05/19 12:28
 */

package com.bcf.integration.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integration.ebooking.service.SearchBookingService;


@IntegrationTest
public class DefaultEBookingServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource
	private SearchBookingService searchBookingService;

	@Resource
	private ModelService modelService;

	@Test
	public void shouldReturnBookingsForTestUser() throws Exception
	{
		// given
		final CustomerModel customerModel = modelService.create(CustomerModel.class);
		customerModel.setName("FirstName LastName");
		customerModel.setAccountValidationKey("testKey");
		final Map<String, String> systemIdentifiersMap = new HashMap<>();
		systemIdentifiersMap.put(BcfCoreConstants.MYBCF_SOURCE_SYSTEM, "123456");
		customerModel.setSystemIdentifiers(systemIdentifiersMap);
		customerModel.setUid("test@test.com");

		// when
		final SearchBookingResponseDTO response = searchBookingService.searchBookings(customerModel, 0, true, false);

		// then
		assertThat(response.getSearchResult()).isNotNull();
		assertThat(response.getCurrentPageNumber()).isNotNull();
		assertThat(response.getTotalPageNumber()).isNotNull();
		assertThat(response.getTotalRecordNumber()).isNotNull();
	}

	@Test
	public void shouldReturnBookingForReferences() throws Exception
	{
		// given
		final CustomerModel customerModel = modelService.create(CustomerModel.class);
		customerModel.setName("FirstName LastName");
		customerModel.setAccountValidationKey("testKey");
		final Map<String, String> systemIdentifiersMap = new HashMap<>();
		systemIdentifiersMap.put(BcfCoreConstants.MYBCF_SOURCE_SYSTEM, "123456");
		customerModel.setSystemIdentifiers(systemIdentifiersMap);
		customerModel.setUid("test@test.com");
		final List<String> bookingReferencesList = new ArrayList<>();
		bookingReferencesList.add("B150000034");
		bookingReferencesList.add("B150000035");

		// when
		final SearchBookingResponseDTO response = searchBookingService.searchBookings(customerModel, bookingReferencesList, 0);

		// then
		assertThat(response.getSearchResult()).isNotNull();
		assertThat(response.getCurrentPageNumber()).isNotNull();
		assertThat(response.getTotalPageNumber()).isNotNull();
		assertThat(response.getTotalRecordNumber()).isNotNull();
	}
}
