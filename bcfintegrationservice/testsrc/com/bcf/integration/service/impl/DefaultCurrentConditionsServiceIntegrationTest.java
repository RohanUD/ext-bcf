/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.service.impl;


import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import com.bcf.integration.currentconditions.service.CurrentConditionsIntegrationService;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;


@IntegrationTest
public class DefaultCurrentConditionsServiceIntegrationTest extends ServicelayerBaseTest
{

	@Resource
	private CurrentConditionsIntegrationService currentConditionsIntegrationService;

	@Test
	public void testServiceWithNonNullTerminalCode() throws Exception
	{
		final String terminalCode = "tsa";
		final CurrentConditionsResponseData currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForTerminal(terminalCode);
		assertTrue(Objects.nonNull(currentConditionsResponseData));
	}

	@Test
	public void testServiceWithNullTerminalCode() throws Exception
	{
		final String terminalCode = null;
		final CurrentConditionsResponseData currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForTerminal(terminalCode);
		assertTrue(Objects.isNull(currentConditionsResponseData));
	}

	@Test
	public void testServiceWithNonNullTerminalCodeAndRoute() throws Exception
	{
		final String terminalCode = "tsa";
		final String route = "route09";
		final CurrentConditionsResponseData currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForRoute(terminalCode, route);
		assertTrue(Objects.nonNull(currentConditionsResponseData));
	}

	@Test
	public void testServiceWithNullTerminalCodeAndRoute() throws Exception
	{
		final String terminalCode = null;
		final String route = null;
		final CurrentConditionsResponseData currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForRoute(terminalCode, route);
		assertTrue(Objects.isNull(currentConditionsResponseData));
	}

	@Test
	public void testServiceWithNonNullTerminalCodes() throws Exception
	{
		final List<String> terminalCodes = new ArrayList<>();
		terminalCodes.add("tsa");
		terminalCodes.add("swb");
		final CurrentConditionsResponseData[] currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForTerminals(terminalCodes);
		assertTrue(!ArrayUtils.isEmpty(currentConditionsResponseData));
	}

	@Test
	public void testServiceWithNullTerminalCodes() throws Exception
	{
		final List<String> terminalCodes = null;
		final CurrentConditionsResponseData[] currentConditionsResponseData = currentConditionsIntegrationService
				.getCurrentConditionsForTerminals(terminalCodes);
		assertTrue(ArrayUtils.isEmpty(currentConditionsResponseData));
	}

}
