/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.integration.activedirectory.service.ADUserService;


@IntegrationTest
public class DefaultADUserServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource
	private ADUserService adUserService;

	@Test
	public void shouldReturnRolesForTestUser() throws Exception
	{
		// given
		final String userId = "asagent";
		final String password = "123456";

		// when
		final UserModel user = adUserService.authenticateAndUpdateADUser(userId, password);

		// then
		assertThat(user).isNotNull();
		assertThat(user.getGroups()).isNotEmpty();
		assertThat(((List) user.getGroups()).get(0)).isEqualTo("asagentsalesmanagergroup");
		assertThat(((List) user.getGroups()).get(1)).isEqualTo("backofficeadmingroup");
		assertThat(((List) user.getGroups()).get(2)).isEqualTo("admingroup");
	}

}
