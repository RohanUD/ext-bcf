/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.service.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import java.util.Objects;
import javax.annotation.Resource;
import org.junit.Test;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;


@IntegrationTest
public class DefaultTripAdvisorServiceIntegrationTest extends ServicelayerBaseTest
{
	@Resource
	private TripAdvisorService tripAdvisorService;

	@Test
	public void testServiceWithNonNullLocationId() throws Exception
	{

		final String locationId = "188401";
		final TripAdvisorResponseData tripAdvisorResponseData = tripAdvisorService.getReviewInformation(locationId);
		assertTrue(Objects.nonNull(tripAdvisorResponseData));
	}

	@Test
	public void testServiceWithNullLocationId() throws Exception
	{

		final String locationId = null;
		final TripAdvisorResponseData tripAdvisorResponseData = tripAdvisorService.getReviewInformation(locationId);
		assertTrue(Objects.isNull(tripAdvisorResponseData));

	}

}
