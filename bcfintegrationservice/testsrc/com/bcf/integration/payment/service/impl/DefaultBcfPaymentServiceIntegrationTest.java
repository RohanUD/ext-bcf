/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.order.TravelCartService;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.TokenRequestDTO;
import com.bcf.integration.data.TokenResponseDTO;
import com.bcf.integration.payment.service.BcfPaymentIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.CardDate;
import com.bcf.processpayment.request.data.CardIdentifier;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;
import com.bcf.processpayment.request.data.TokenRef;
import com.bcf.processpayment.request.data.TransactionInfoRequest;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


@IntegrationTest
public class DefaultBcfPaymentServiceIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String CARD_EXPIRATION_YEAR = "card_expirationYear";
	private static final String TOKEN_TYPE = "token_type";
	private static final String ESELECTPLUS = "ESELECTPLUS";
	private static final String GET_TOKEN_REQUEST = "GetTokenRequest";

	@Resource
	private BcfPaymentIntegrationService bcfPaymentIntegrationService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource
	private TravelCartService travelCartService;

	@Resource
	private SessionService sessionService;

	private final Map<String, String> requestParams = new HashMap<>();

	@BeforeClass
	public static void beforeClass()
	{
		Registry.setCurrentTenantByID("junit");
	}

	@Before
	public void setUp() throws Exception
	{
		requestParams.put("card_expirationMonth", "9");
		requestParams.put(CARD_EXPIRATION_YEAR, "2022");
		requestParams.put("card_accountNumber", "ot-hTIilcGXCOpYwyCEkjwGeuVN2");
		requestParams.put("token", requestParams.get("card_accountNumber"));
		sessionService.getCurrentSession().setAttribute("agentId", "1000");
		importCsv("/test/testRefundOrder.csv", "utf-8");
	}

	@Test
	public void testPaymentWebUser() throws IntegrationException
	{
		requestParams.put(TOKEN_TYPE,
				configurationService.getConfiguration().getString(BcfintegrationserviceConstants.TOKEN_TYPE_TEMPORARY));
		final CartModel cartModel = travelCartService.getSessionCart();
		cartModel.setAmountToPay(10d);
		final PaymentRequestDTO paymentRequestDTO = createPaymentRequestDTO(requestParams, cartModel);
		final PaymentResponseDTO paymentResponseDTO = bcfPaymentIntegrationService.makePayment(
				BcfintegrationserviceConstants.PAYMENT_SERVICE_URL,
				paymentRequestDTO);

		Assert.assertNotNull(paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus());
	}

	@Test
	public void testPaymentCallCentreUser() throws IntegrationException
	{
		requestParams.put(TOKEN_TYPE, ESELECTPLUS);
		final CartModel cartModel = travelCartService.getSessionCart();
		cartModel.setAmountToPay(10d);
		final PaymentRequestDTO paymentRequestDTO = createPaymentRequestDTO(requestParams, cartModel);
		final PaymentResponseDTO paymentResponseDTO = bcfPaymentIntegrationService.makePayment(
				BcfintegrationserviceConstants.PAYMENT_SERVICE_URL,
				paymentRequestDTO);

		Assert.assertNotNull(paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus());
	}

	@Test
	public void testGetTokenCallCentreUser() throws IntegrationException
	{
		final TokenRequestDTO tokenRequestDTO = new TokenRequestDTO();
		populatePermanentTokenRequest(tokenRequestDTO);
		final TokenResponseDTO tokenResponseDTO = bcfPaymentIntegrationService.getPermanentToken(tokenRequestDTO);
		assertThat(tokenResponseDTO).isNotNull();
	}

	protected PaymentRequestDTO createPaymentRequestDTO(final Map<String, String> requestParams, final CartModel cartModel)
	{
		final PaymentRequestDTO paymentRequestDTO = new PaymentRequestDTO();
		paymentRequestDTO.setSourceSystemInfo(createSourceSystemInfo());
		paymentRequestDTO.setCardInfoRequest(createCardInfoRequest(requestParams));
		paymentRequestDTO.setTransactionInfoRequest(createTransactionInfo(cartModel));
		return paymentRequestDTO;
	}

	protected PaymentSourceSystemInfo createSourceSystemInfo()
	{
		final PaymentSourceSystemInfo sourceSystemInfo = new PaymentSourceSystemInfo();
		sourceSystemInfo
				.setAppliId("MYBCF");
		sourceSystemInfo
				.setLocationId(configurationService.getConfiguration().getString(BcfintegrationserviceConstants.LOCATION_ID));
		sourceSystemInfo.setStationId(configurationService.getConfiguration().getString(BcfintegrationserviceConstants.STATION_ID));
		sourceSystemInfo.setPassword("password123!");
		return sourceSystemInfo;
	}

	protected CardInfoRequest createCardInfoRequest(final Map<String, String> requestParams)
	{
		final CardInfoRequest cardInfoRequest = new CardInfoRequest();
		final CardIdentifier cardIdentifier = new CardIdentifier();
		cardIdentifier.setCardIdentifierType(
				configurationService.getConfiguration().getString(BcfintegrationserviceConstants.CARD_IDENTIFIER_TYPE));
		final TokenRef tokenRef = new TokenRef();
		tokenRef.setTokenValue(requestParams.get("token"));
		tokenRef.setTokenType(requestParams.get(TOKEN_TYPE));
		cardIdentifier.setTokenRef(tokenRef);
		cardInfoRequest.setCardIdentifier(cardIdentifier);
		cardInfoRequest.setExpectedCardType(
				configurationService.getConfiguration().getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
		final CardDate cardExpiryDate = new CardDate();
		cardExpiryDate.setMonth(requestParams.get("card_expirationMonth"));
		cardExpiryDate.setYear(requestParams.get(CARD_EXPIRATION_YEAR));
		cardInfoRequest.setCardExpiryDate(cardExpiryDate);
		return cardInfoRequest;
	}

	protected TransactionInfoRequest createTransactionInfo(final CartModel cartModel) throws ConversionException
	{
		final TransactionInfoRequest transactionInfoRequest = new TransactionInfoRequest();
		transactionInfoRequest.setTransactionType(
				configurationService.getConfiguration().getString(BcfintegrationserviceConstants.TRANSACTION_TYPE_PURCHASE));
		transactionInfoRequest.setAmountInCents(Math.round(100 * cartModel.getAmountToPay()));
		return transactionInfoRequest;
	}

	private void populatePermanentTokenRequest(final TokenRequestDTO tokenRequestDTO)
	{
		tokenRequestDTO.setType(GET_TOKEN_REQUEST);
		final PaymentSourceSystemInfo sourceSystemInfo = createSourceSystemInfo();
		sourceSystemInfo.setAgentId(sessionService.getCurrentSession().getAttribute("agentId"));
		tokenRequestDTO.setSourceSystemInfo(sourceSystemInfo);
	}
}
