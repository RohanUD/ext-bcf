/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 13/05/19 12:34
 */

package com.bcf.integration.payment.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Collection;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


@IntegrationTest
public class DefaultBcfRefundServiceIntegrationTest extends ServicelayerTransactionalTest
{
	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private BcfRefundIntegrationService bcfRefundIntegrationService;

	@BeforeClass
	public static void beforeClass()
	{
		Registry.setCurrentTenantByID("junit");
	}

	@Before
	public void setUp() throws Exception
	{
		importCsv("/test/testRefundOrder.csv", "utf-8");
	}

	@Test
	public void testRefundOrder() throws IntegrationException
	{
		final UserModel user = userService.getUserForUID("ahertz");
		final Collection<OrderModel> orderModels = user.getOrders();
		Assert.assertEquals(orderModels.size(), 1);
		final OrderModel order = orderModels.iterator().next();
		order.setBookingJourneyType(BookingJourneyType.BOOKING_TRANSPORT_ONLY);

		final PaymentResponseDTO paymentResponseDTO = bcfRefundIntegrationService.refundOrder(order);
		assertThat(paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()).isIn(true,false);

		Assert.assertEquals("REFUND",
				paymentResponseDTO.getTransactionInfoResponse().getTransactionType());
	}
}
