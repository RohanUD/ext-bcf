/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.strategies.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.bcf.integration.payment.strategy.impl.DefaultBCFCreditCardPaymentInfoCreateStrategy;


@UnitTest
public class DefaultBCFCreditCardPaymentInfoCreateStrategyTest
{
	@Mock
	private ModelService mockModelService;

	private DefaultBCFCreditCardPaymentInfoCreateStrategy defaultBCFCreditCardPaymentInfoCreateStrategy;

	private SubscriptionInfoData subscriptionInfo;

	private PaymentInfoData paymentInfo;

	private AddressModel billingAddress;

	private CustomerModel customerModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		defaultBCFCreditCardPaymentInfoCreateStrategy = new DefaultBCFCreditCardPaymentInfoCreateStrategy();
		subscriptionInfo = new SubscriptionInfoData();
		paymentInfo = new PaymentInfoData();
		paymentInfo.setCardAccountNumber("cardNumber");
		paymentInfo.setCardCardType("001");
		paymentInfo.setCardExpirationMonth(02);
		paymentInfo.setCardExpirationYear(2020);
		paymentInfo.setCardCvNumber("cvv");
		billingAddress = new AddressModel();
		customerModel = new CustomerModel();
		defaultBCFCreditCardPaymentInfoCreateStrategy.setModelService(mockModelService);

	}

	@Test
	public void testcreateCreditCardPaymentInfo()
	{
		final CreditCardPaymentInfoModel cardPaymentInfoModel = new CreditCardPaymentInfoModel();
		given(mockModelService.create(CreditCardPaymentInfoModel.class)).willReturn(cardPaymentInfoModel);
		defaultBCFCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(subscriptionInfo, paymentInfo, billingAddress,
				customerModel, false);
		Assert.assertEquals("cardNumber", cardPaymentInfoModel.getToken());
	}
}
