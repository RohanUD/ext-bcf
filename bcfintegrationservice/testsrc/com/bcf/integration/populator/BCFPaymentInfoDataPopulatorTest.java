/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.populator;

import static org.mockito.Mockito.mock;

import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integration.ebooking.populators.BCFPaymentInfoDataPopulator;


public class BCFPaymentInfoDataPopulatorTest
{

	@Mock
	private SessionService sessionService;

	final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
	private final BCFPaymentInfoDataPopulator bcfPaymentInfoDataPopulator = new BCFPaymentInfoDataPopulator();
	private AbstractPopulatingConverter<AbstractOrderModel, ConfirmBookingsRequest> confirmBookingsRequestConverter;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		confirmBookingsRequestConverter = new ConverterFactory<AbstractOrderModel, ConfirmBookingsRequest, BCFPaymentInfoDataPopulator>()
				.create(ConfirmBookingsRequest.class, bcfPaymentInfoDataPopulator);

		bcfPaymentInfoDataPopulator.setSessionService(sessionService);
		final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(entry.getTotalPrice()).thenReturn((double) 13);
		final List<AbstractOrderEntryModel> list = new ArrayList<AbstractOrderEntryModel>();
		list.add(entry);
		Mockito.when(abstractOrderModel.getEntries()).thenReturn(list);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = new CreditCardPaymentInfoModel();
		ccPaymentInfoModel.setCcOwner("cardName");
		ccPaymentInfoModel.setNumber("number");
		ccPaymentInfoModel.setValidToMonth("02");
		ccPaymentInfoModel.setValidToYear("2020");
		Mockito.when(abstractOrderModel.getTotalPrice()).thenReturn((double) 13);
		Mockito.when(abstractOrderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
		Mockito.when(sessionService.getAttribute(BcfCoreConstants.CREDIT_CARD_INFO_SECURITY_CODE)).thenReturn("cvv");

	}

	@Test
	public void testConvert()
	{

		final ConfirmBookingsRequest confirmBookingsRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);
		final Double totalPrice = (double) 13;
		Assert.assertEquals(totalPrice, confirmBookingsRequest.getPaymentInfo().getTotalAmountInCents());
		Assert.assertEquals("cardName",
				confirmBookingsRequest.getPaymentInfo().getPaymentProcessed().getProofOfPaymentStub().get(0).getCardInfo()
						.getCardName());
		Assert.assertEquals("number",
				confirmBookingsRequest.getPaymentInfo().getPaymentProcessed().getProofOfPaymentStub().get(0).getCardInfo()
						.getTokenIdentifier());
		Assert.assertEquals("022020",
				confirmBookingsRequest.getPaymentInfo().getPaymentProcessed().getProofOfPaymentStub().get(0).getCardInfo()
						.getCardExpiry());
		Assert.assertEquals("TEST_CACHING_KEY",
				confirmBookingsRequest.getPaymentInfo().getPaymentProcessed().getPaymentDistribution().get(0).getBookingReference()
						.getCachingKey());
		Assert.assertEquals(totalPrice,
				confirmBookingsRequest.getPaymentInfo().getPaymentProcessed().getPaymentDistribution().get(0).getAmountInCents());
	}
}
