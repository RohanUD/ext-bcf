/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integration.ebooking.populators.BCFBasicConfirmBookingPopulator;


@UnitTest
public class BCFBasicConfirmBookingPopulatorTest
{
	private final String CONFIRMBOOKING_CLIENT_CODE = "PROXY_BOOKINGCLIENT_MYBCF";
	private final String CONFIRMBOOKING_BOOKINGTYPE_ANONYMOUS = "QUOTATION";
	private final String CONFIRMBOOKING_BOOKINGTYPE_SIGNEDIN = "OPTION";
	private final String CONFIRMBOOKING_BOOKINGTYPE_AMENDED = "MODIFICATION_QUOTATION";
	final String CACHING_KEY = "458900b316dae41a:2e4b8f5b:14ec124749d:-7e4d";

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private BCFTravelCartService cartService;

	private final BCFBasicConfirmBookingPopulator bcfBasicConfirmBookingPopulator = new BCFBasicConfirmBookingPopulator();
	private AbstractPopulatingConverter<AbstractOrderModel, ConfirmBookingsRequest> confirmBookingsRequestConverter;
	final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
	final Configuration configuration = Mockito.mock(HybrisConfiguration.class);


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		bcfBasicConfirmBookingPopulator.setConfigurationService(configurationService);
		bcfBasicConfirmBookingPopulator.setCartService(cartService);
		final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
		final List<AbstractOrderEntryModel> list = new ArrayList<AbstractOrderEntryModel>();
		list.add(entry);
		Mockito.when(abstractOrderModel.getEntries()).thenReturn(list);
		confirmBookingsRequestConverter = new ConverterFactory<AbstractOrderModel, ConfirmBookingsRequest, BCFBasicConfirmBookingPopulator>()
				.create(ConfirmBookingsRequest.class, bcfBasicConfirmBookingPopulator);
		given(getConfigurationService().getConfiguration()).willReturn(configuration);
		given(configuration.getString(BcfintegrationserviceConstants.CONFIRMBOOKING_CLIENT_CODE))
				.willReturn(CONFIRMBOOKING_CLIENT_CODE);
		given(abstractOrderModel.getUser()).willReturn(mock(CustomerModel.class));
	}

	@Test
	public void testGuestUserConvert()
	{
		final CustomerType customerType = CustomerType.GUEST;
		given(((CustomerModel) abstractOrderModel.getUser()).getType()).willReturn(customerType);
		given(configuration.getString(BcfintegrationserviceConstants.CONFIRMBOOKING_BOOKINGTYPE_ANONYMOUS))
				.willReturn(CONFIRMBOOKING_BOOKINGTYPE_ANONYMOUS);

		final ConfirmBookingsRequest confirmBookingsRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals(CONFIRMBOOKING_CLIENT_CODE, confirmBookingsRequest.getBookingClientSystem().getClientCode());
		Assert.assertEquals(CONFIRMBOOKING_BOOKINGTYPE_ANONYMOUS, confirmBookingsRequest.getBookingType().getBookingType());
		Assert.assertNotNull(confirmBookingsRequest.getTempBookingReferences().get(0).getCachingKey());
		Assert.assertNull(confirmBookingsRequest.isIsAllowChangesAtPOS());
	}

	@Test
	public void testSignedInUserConvert()
	{
		final CustomerType customerType = CustomerType.REGISTERED;
		given(((CustomerModel) abstractOrderModel.getUser()).getType()).willReturn(customerType);
		given(configuration.getString(BcfintegrationserviceConstants.CONFIRMBOOKING_BOOKINGTYPE_SIGNEDIN))
				.willReturn(CONFIRMBOOKING_BOOKINGTYPE_SIGNEDIN);

		final ConfirmBookingsRequest confirmBookingsProxyRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals(CONFIRMBOOKING_CLIENT_CODE, confirmBookingsProxyRequest.getBookingClientSystem().getClientCode());
		Assert.assertEquals(CONFIRMBOOKING_BOOKINGTYPE_SIGNEDIN, confirmBookingsProxyRequest.getBookingType().getBookingType());
		Assert.assertNotNull(confirmBookingsProxyRequest.getTempBookingReferences().get(0).getCachingKey());
		Assert.assertNull(confirmBookingsProxyRequest.isIsAllowChangesAtPOS());
	}

	@Test
	public void testAmendedCartConvert()
	{
		given(bcfBasicConfirmBookingPopulator.getCartService().isAmendmentCart(any())).willReturn(true);
		given(configuration.getString(BcfintegrationserviceConstants.CONFIRMBOOKING_BOOKINGTYPE_AMENDED))
				.willReturn(CONFIRMBOOKING_BOOKINGTYPE_AMENDED);

		final ConfirmBookingsRequest confirmBookingsProxyRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals(CONFIRMBOOKING_CLIENT_CODE, confirmBookingsProxyRequest.getBookingClientSystem().getClientCode());
		Assert.assertEquals(CONFIRMBOOKING_BOOKINGTYPE_AMENDED, confirmBookingsProxyRequest.getBookingType().getBookingType());
		Assert.assertNotNull(confirmBookingsProxyRequest.getTempBookingReferences().get(0).getCachingKey());
		Assert.assertNotNull(confirmBookingsProxyRequest.isIsAllowChangesAtPOS());
		Assert.assertEquals(false, confirmBookingsProxyRequest.isIsAllowChangesAtPOS());
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
