/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:15
 */

package com.bcf.integration.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integration.ebooking.populators.BCFCustomerInfoDataPopulator;


@UnitTest
public class BCFCustomerInfoDataPopulatorTest
{
	private static final String CUSTOMER_NUMBER = "CustomerNumber";
	private final BCFCustomerInfoDataPopulator bcfCustomerInfoDataPopulator = new BCFCustomerInfoDataPopulator();
	private AbstractPopulatingConverter<AbstractOrderModel, ConfirmBookingsRequest> confirmBookingsRequestConverter;
	final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		confirmBookingsRequestConverter = new ConverterFactory<AbstractOrderModel, ConfirmBookingsRequest, BCFCustomerInfoDataPopulator>()
				.create(ConfirmBookingsRequest.class, bcfCustomerInfoDataPopulator);
	}

	@Test
	public void testGuestUserConvert()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setName("firstName lastName");
		customerModel.setFirstName("firstName");
		customerModel.setLastName("lastName");
		customerModel.setPhoneNo("01156745678");
		customerModel.setCustomerID(CUSTOMER_NUMBER);
		final CustomerType customerType = CustomerType.GUEST;
		customerModel.setType(customerType);
		given(abstractOrderModel.getUser()).willReturn(customerModel);

		final ConfirmBookingsRequest confirmBookingsRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals("firstName", confirmBookingsRequest.getBookingHolderInfo().getFirstName());
		Assert.assertEquals("lastName", confirmBookingsRequest.getBookingHolderInfo().getLastName());
		Assert.assertEquals("01156745678", confirmBookingsRequest.getBookingHolderInfo().getPhone());
	}

	@Test
	public void testSignedInUserConvert()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_NUMBER);
		final CustomerType customerType = CustomerType.REGISTERED;
		customerModel.setType(customerType);
		given(abstractOrderModel.getUser()).willReturn(customerModel);

		final ConfirmBookingsRequest confirmBookingsRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals(CUSTOMER_NUMBER, confirmBookingsRequest.getBookingHolderInfo().getCustomerNumber());
	}

	@Test
	public void testAmendedCartConvert()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_NUMBER);
		final CustomerType customerType = CustomerType.valueOf("Amended");
		customerModel.setType(customerType);
		given(abstractOrderModel.getUser()).willReturn(customerModel);

		final ConfirmBookingsRequest confirmBookingsRequest = confirmBookingsRequestConverter
				.convert(abstractOrderModel);

		Assert.assertEquals(CUSTOMER_NUMBER, confirmBookingsRequest.getBookingHolderInfo().getCustomerNumber());
	}
}
