/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import com.bcf.integration.ebooking.populators.BCFPaymentInfoResultPopulator;


@UnitTest
public class BCFPaymentInfoResultPopulatorTest
{

	private static final String TOKEN = "token";
	private final Map<String, String> source = new HashMap<>();

	private final BCFPaymentInfoResultPopulator bcfPaymentInfoResultPopulator = new BCFPaymentInfoResultPopulator();
	private AbstractPopulatingConverter<Map<String, String>, CreateSubscriptionResult> abstractPopulatingConverter;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		abstractPopulatingConverter = new ConverterFactory<Map<String, String>, CreateSubscriptionResult, BCFPaymentInfoResultPopulator>()
				.create(CreateSubscriptionResult.class, bcfPaymentInfoResultPopulator);

		source.put("card_accountNumber", "card_accountNumber");
		source.put("card_cardType", "card_cardType");
		source.put("card_nameOnCard", "card_nameOnCard");
		source.put("card_expirationMonth", "02");
		source.put("card_expirationYear", "2020");
		source.put("card_startMonth", "card_startMonth");
		source.put("card_startYear", "card_startYear");
		source.put("paymentOption", "paymentOption");
		source.put(TOKEN, TOKEN);
	}

	@Test
	public void testConvert()
	{
		final CreateSubscriptionResult createSubscriptionResult = abstractPopulatingConverter.convert(source);
		Assert.assertEquals(TOKEN, createSubscriptionResult.getPaymentInfoData().getToken());
	}
}
