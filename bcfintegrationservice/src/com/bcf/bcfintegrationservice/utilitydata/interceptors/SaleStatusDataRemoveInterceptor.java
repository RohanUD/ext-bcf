/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfintegrationservice.utilitydata.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.SaleStatusDataModel;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationOfferingCreateUtility;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility;


public class SaleStatusDataRemoveInterceptor implements RemoveInterceptor<SaleStatusDataModel>
{

	private static final Logger LOG = Logger.getLogger(AbstractDataUploadUtility.class);
	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;
	private AccommodationOfferingCreateUtility accommodationOfferingCreateUtility;

	@Override
	public void onRemove(final SaleStatusDataModel saleStatusDataModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		try
		{
			if (isSaleStatusAssociatedToAccommodation(saleStatusDataModel))
			{
				AccommodationDataModel accommodationDataModel = saleStatusDataModel.getAccommodationData();
				accommodationDataModel.setSaleStatuses(
						accommodationDataModel.getSaleStatuses().stream().filter(s -> !s.equals(saleStatusDataModel)).collect(
								Collectors.toList()));
				accommodationsDataUploadUtility.createOrUpdateSaleStatuses(accommodationDataModel, true);
			}
			else
			{
				AccommodationOfferingDataModel accommodationOfferingDataModel = saleStatusDataModel.getAccommodationOfferingData();
				accommodationOfferingDataModel.setSaleStatuses(
						accommodationOfferingDataModel.getSaleStatuses().stream().filter(s -> !s.equals(saleStatusDataModel)).collect(
								Collectors.toList()));
				accommodationOfferingCreateUtility.createOrUpdateSaleStatuses(accommodationOfferingDataModel);
			}
		}
		catch (final Exception e)
		{
			LOG.error(e);
			throw new InterceptorException(e.getMessage());
		}
	}

	private boolean isSaleStatusAssociatedToAccommodation(
			final SaleStatusDataModel saleStatusData)
	{
		return Objects.nonNull(saleStatusData.getAccommodationData());
	}

	@Required
	public void setAccommodationsDataUploadUtility(
			final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}

	@Required
	public void setAccommodationOfferingCreateUtility(
			final AccommodationOfferingCreateUtility accommodationOfferingCreateUtility)
	{
		this.accommodationOfferingCreateUtility = accommodationOfferingCreateUtility;
	}
}
