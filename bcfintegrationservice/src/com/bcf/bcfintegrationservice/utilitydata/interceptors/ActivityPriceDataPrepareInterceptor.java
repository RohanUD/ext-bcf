/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfintegrationservice.utilitydata.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.bcfintegrationservice.utilitydata.strategy.ActivityPriceDataCodeGenerateStrategy;
import com.bcf.integration.dataupload.service.activities.ActivitiesDataService;


public class ActivityPriceDataPrepareInterceptor implements PrepareInterceptor<ActivityPriceDataModel>
{
	ActivityPriceDataCodeGenerateStrategy activityPriceDataCodeGenerateStrategy;
	ActivitiesDataService activitiesDataService;

	@Override
	public void onPrepare(final ActivityPriceDataModel model, final InterceptorContext ctx)
	{
		if (StringUtils.isEmpty(model.getCode()) && Objects.nonNull(model.getActivitiesData()))
		{
			List<ActivityPriceDataModel> existingModels = activitiesDataService.getExistingActivityPricesData(model);
			if (CollectionUtils.isNotEmpty(existingModels))
			{
				//delete all existing models with the matching code
				existingModels.forEach(existedModel -> ctx.registerElementFor(existedModel, PersistenceOperation.DELETE));
			}
			model.setCode(activityPriceDataCodeGenerateStrategy.generateCode(model));
		}
	}

	@Required
	public void setActivityPriceDataCodeGenerateStrategy(
			final ActivityPriceDataCodeGenerateStrategy activityPriceDataCodeGenerateStrategy)
	{
		this.activityPriceDataCodeGenerateStrategy = activityPriceDataCodeGenerateStrategy;
	}

	@Required
	public void setActivitiesDataService(final ActivitiesDataService activitiesDataService)
	{
		this.activitiesDataService = activitiesDataService;
	}
}
