/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfintegrationservice.utilitydata.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.bcfintegrationservice.utilitydata.strategy.RatePlanCodeGenerateStrategy;


public class RatePlanDataPrepareInterceptor implements PrepareInterceptor<RatePlanDataModel>
{
	private RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy;

	@Override
	public void onPrepare(final RatePlanDataModel ratePlanData, final InterceptorContext intercetptorContext)
	{
		if (StringUtils.isEmpty(ratePlanData.getCode()))
		{
			ratePlanData.setCode(getRatePlanCodeGenerateStrategy().getCode(ratePlanData));
		}
	}

	/**
	 * @return the ratePlanCodeGenerateStrategy
	 */
	protected RatePlanCodeGenerateStrategy getRatePlanCodeGenerateStrategy()
	{
		return ratePlanCodeGenerateStrategy;
	}

	/**
	 * @param ratePlanCodeGenerateStrategy the ratePlanCodeGenerateStrategy to set
	 */
	@Required
	public void setRatePlanCodeGenerateStrategy(final RatePlanCodeGenerateStrategy ratePlanCodeGenerateStrategy)
	{
		this.ratePlanCodeGenerateStrategy = ratePlanCodeGenerateStrategy;
	}
}
