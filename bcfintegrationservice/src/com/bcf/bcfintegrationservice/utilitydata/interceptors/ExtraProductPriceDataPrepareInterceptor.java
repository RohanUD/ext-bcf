/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfintegrationservice.utilitydata.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.integration.dataupload.service.accommodations.ExtraProductDataService;


public class ExtraProductPriceDataPrepareInterceptor implements PrepareInterceptor<ExtraProductPriceDataModel>
{

	ExtraProductDataService extraProductDataService;

	PersistentKeyGenerator extraProductPriceCodeGenerator;

	ModelService modelService;

	@Override
	public void onPrepare(final ExtraProductPriceDataModel model, final InterceptorContext ctx)
	{
		if(StringUtils.isEmpty(model.getCode())){
			model.setCode(extraProductPriceCodeGenerator.generate().toString());
		}

		if(modelService .isNew(model) && model.getExtraProductData()!=null && !modelService.isNew(model.getExtraProductData()))
		{
			List<ExtraProductPriceDataModel> existingModels = extraProductDataService.getExistingExtraProductPricesData(model);
			if (CollectionUtils.isNotEmpty(existingModels))
			{
				//delete all existing models with the matching code
				existingModels.forEach(existedModel -> ctx.registerElementFor(existedModel, PersistenceOperation.DELETE));
			}
		}

	}

	public ExtraProductDataService getExtraProductDataService()
	{
		return extraProductDataService;
	}

	public void setExtraProductDataService(
			final ExtraProductDataService extraProductDataService)
	{
		this.extraProductDataService = extraProductDataService;
	}

	public PersistentKeyGenerator getExtraProductPriceCodeGenerator()
	{
		return extraProductPriceCodeGenerator;
	}

	public void setExtraProductPriceCodeGenerator(
			final PersistentKeyGenerator extraProductPriceCodeGenerator)
	{
		this.extraProductPriceCodeGenerator = extraProductPriceCodeGenerator;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
