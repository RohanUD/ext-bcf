/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfintegrationservice.utilitydata.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.model.vacation.BcfVacationLocationModel;


public class BcfVacationLocationPrepareInterceptor implements PrepareInterceptor<BcfVacationLocationModel>
{
	private BcfTravelLocationService bcfTravelLocationService;

	@Override
	public void onPrepare(final BcfVacationLocationModel model, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (Objects.isNull(model) || StringUtils.isBlank(model.getCode()) || Objects
				.isNull(model.getLocationType()))
		{
			throw new InterceptorException("Required fields not provided to create vacation location");
		}
		LocationModel originalLocationModel = bcfTravelLocationService.getOrCreateLocationFromVacationLocation(model);
		model.setOriginalLocation(originalLocationModel);
	}

	public BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}
}
