/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfintegrationservice.utilitydata.strategy;

import de.hybris.platform.cronjob.enums.DayOfWeek;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;


public class RatePlanCodeGenerateStrategy
{
	private static final String RP = "RP";
	private static final String UNDERSCORE = "_";

	/**
	 * Gets the code.
	 *
	 * @param ratePlanData
	 *           the rate plan data
	 * @return the code
	 */
	public String getCode(final RatePlanDataModel ratePlanData)
	{
		final Date startDate = ratePlanData.getRatePlanStartDate();
		final Date endDate = ratePlanData.getRatePlanEndDate();
		final String strDateFormat = "ddMMyyyy";
		final DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
		final String startDateStr = dateFormat.format(startDate);
		final String endDateStr = dateFormat.format(endDate);
		final String name = StringUtils.deleteWhitespace(ratePlanData.getName());
		String daysCode = StringUtils.EMPTY;

		final List<DayOfWeek> daysOfWeeks = ratePlanData.getDaysOfWeek();
		if (CollectionUtils.isNotEmpty(daysOfWeeks))
		{
			daysCode = daysOfWeeks.stream().map(dayOfWeek -> String.valueOf(dayOfWeek.getCode().charAt(0)))
					.collect(Collectors.joining());
			daysCode = daysCode + UNDERSCORE;
		}

		final Calendar cal = Calendar.getInstance();
		final long timeMills = cal.getTimeInMillis();
		return RP + UNDERSCORE + name + UNDERSCORE + startDateStr + UNDERSCORE + endDateStr
				+ UNDERSCORE + daysCode + timeMills;
	}
}
