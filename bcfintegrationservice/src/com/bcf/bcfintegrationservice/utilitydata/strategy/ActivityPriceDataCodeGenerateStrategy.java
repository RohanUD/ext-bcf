/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfintegrationservice.utilitydata.strategy;

import static com.bcf.core.constants.BcfCoreConstants.SIMPLE_DATE_FORMAT;

import de.hybris.platform.cronjob.enums.DayOfWeek;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.core.util.BCFDateUtils;


public class ActivityPriceDataCodeGenerateStrategy
{

	private static final String ACTIVITY_PRICE_DATA_PREFIX_KEY = "APD";
	private static final String UNDERSCORE = "_";

	public String generateCode(final ActivityPriceDataModel activityPriceData)
	{
		Date startDate = activityPriceData.getStartDate();
		Date endDate = activityPriceData.getEndDate();

		String activitiesDataCode = activityPriceData.getActivitiesData().getCode();
		String startDateStr = BCFDateUtils.convertDateToString(startDate, SIMPLE_DATE_FORMAT);
		String endDateStr = BCFDateUtils.convertDateToString(endDate, SIMPLE_DATE_FORMAT);
		String paxType = activityPriceData.getPassengerType().getCode();

		String daysCode = StringUtils.EMPTY;
		final List<DayOfWeek> daysOfWeek = activityPriceData.getDaysOfWeek();
		if (CollectionUtils.isNotEmpty(daysOfWeek))
		{
			daysCode = daysOfWeek.stream().sorted().map(dayOfWeek -> String.valueOf(dayOfWeek.ordinal()))
					.collect(Collectors.joining());
		}

		return String.join(UNDERSCORE, ACTIVITY_PRICE_DATA_PREFIX_KEY, activitiesDataCode, startDateStr, endDateStr, paxType, daysCode);
	}

	public String generateCode(final String activitiesDataCode, final String paxType,
			final Date startDate, final Date endDate, final List<DayOfWeek> daysOfWeek)
	{
		String startDateStr = BCFDateUtils.convertDateToString(startDate, SIMPLE_DATE_FORMAT);
		String endDateStr = BCFDateUtils.convertDateToString(endDate, SIMPLE_DATE_FORMAT);

		String daysCode = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(daysOfWeek))
		{
			daysCode = daysOfWeek.stream().sorted().map(dayOfWeek -> String.valueOf(dayOfWeek.ordinal()))
					.collect(Collectors.joining());
		}

		return String.join(UNDERSCORE, ACTIVITY_PRICE_DATA_PREFIX_KEY, activitiesDataCode, startDateStr, endDateStr, paxType, daysCode);
	}
}
