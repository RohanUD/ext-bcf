/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfintegrationservice.integration.utility;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.RegistrationSource;
import com.bcf.core.util.BcfB2bUtil;
import com.bcf.integration.common.data.CustomerInfo;


public class IntegrationUtility
{
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private SessionService sessionService;
	private UserService userService;
	private Map<String, Map<String, String>> sessionBookingJourneyMap;

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;


	/**
	 * returns client code to be sent in a request
	 */
	public String getClientCode(final SalesApplication salesApplication)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		return getSessionBookingJourneyMap().get(sessionBookingJourney).get(salesApplication.getCode());
	}

	public String getClientCode(final SalesApplication salesApplication, final OrderModel order)
	{
		String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isBlank(sessionBookingJourney))
		{
			sessionBookingJourney = order.getBookingJourneyType().getCode();
		}
		if (StringUtils.isBlank(sessionBookingJourney))
		{
			return StringUtils.EMPTY;
		}
		return getSessionBookingJourneyMap().get(sessionBookingJourney).get(salesApplication.getCode());
	}

	public String getClientCode(final SalesApplication salesApplication, final BookingJourneyType bookingJourney)
	{
		return getSessionBookingJourneyMap().get(bookingJourney.getCode()).get(salesApplication.getCode());
	}

	/**
	 * returns customer information to be sent in a request
	 */
	public CustomerInfo createCustomerInfo(final CustomerModel customermodel)
	{
		final CustomerInfo customerInfo = new CustomerInfo();

		if (getUserService().isAnonymousUser(customermodel) || CustomerType.GUEST
				.equals(customermodel.getType()))
		{
			if (MapUtils.isNotEmpty(customermodel.getSystemIdentifiers()) && customermodel.getSystemIdentifiers()
					.containsKey(BcfCoreConstants.EBOOKING))
			{
				customerInfo.setCustomerNumber(customermodel.getSystemIdentifiers().get(BcfCoreConstants.EBOOKING));
				customerInfo.setSourceSystemName(BcfCoreConstants.EBOOKING);
			}

		}
		final boolean isRegisteredUser = Objects.equals(CustomerType.REGISTERED, customermodel.getType());

		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();

		if (Objects.nonNull(b2bUnitInContext))
		{
			customerInfo.setCustomerNumber(customermodel.getSystemIdentifiers().get(BcfCoreConstants.CRM_SOURCE_SYSTEM));
			customerInfo
					.setAccountId(Arrays.asList(b2bUnitInContext.getSystemIdentifiers().get(BcfCoreConstants.CRM_SOURCE_SYSTEM)));
			customerInfo.setSourceSystemName(RegistrationSource.CRM.getCode());
		}
		else if (isRegisteredUser)
		{
			customerInfo.setCustomerNumber(customermodel.getSystemIdentifiers().get(BcfCoreConstants.CRM_SOURCE_SYSTEM));
			customerInfo.setSourceSystemName(RegistrationSource.CRM.getCode());
		}
		customerInfo.setFirstName(customermodel.getFirstName());
		customerInfo.setLastName(customermodel.getLastName());
		customerInfo.setEmailAddress(customermodel.getContactEmail());
		customerInfo.setPhone(customermodel.getPhoneNo());
		return customerInfo;
	}

	/**
	 * returns booking type to be sent in a request
	 */
	public String getBookingType(final boolean isAnonymousUser, final boolean isAmendmentCart, final boolean isAlcarteFlow)
	{
		if (isAlcarteFlow)
		{

			if (isAnonymousUser)
			{
				return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_GUEST_CUSTOMER;
			}
			else
			{
				return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER;
			}

		}
		else if (isAmendmentCart)
		{
			return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER_FOR_MODIFICATION;
		}
		else if (isAnonymousUser || AccountType.SUBSCRIPTION_ONLY
				.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()))
		{
			return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_GUEST_CUSTOMER;

		}
		else
		{
			return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER;
		}
	}

	public String getModifyBookingType(final boolean isAnonymousUser, final boolean isAmendmentCart, final boolean isAlacarteFlow)
	{
		if (isAlacarteFlow && !isAnonymousUser)
		{
			return BcfintegrationcoreConstants.MODIFY_BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER;
		}
		else if (isAmendmentCart)
		{
			return BcfintegrationcoreConstants.BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER_FOR_MODIFICATION;
		}
		else
		{
			return BcfintegrationcoreConstants.MODIFY_BOOKING_TYPE_FOR_SIGNED_IN_CUSTOMER;
		}
	}

	public String getSystemIdentifierForKey(final String key, final Map<String, String> systemIdentifierMap)
	{
		if (MapUtils.isNotEmpty(systemIdentifierMap) && systemIdentifierMap.containsKey(key))
		{
			return systemIdentifierMap.get(key);
		}

		return null;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected Map<String, Map<String, String>> getSessionBookingJourneyMap()
	{
		return sessionBookingJourneyMap;
	}

	@Required
	public void setSessionBookingJourneyMap(
			final Map<String, Map<String, String>> sessionBookingJourneyMap)
	{
		this.sessionBookingJourneyMap = sessionBookingJourneyMap;
	}
}
