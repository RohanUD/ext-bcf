/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfintegrationservice.job.financial.redemption;

import reactor.util.CollectionUtils;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.model.cronjob.financial.RevenueRedemptionCronJobModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.PackageCostBreakupCustomer;
import com.bcf.notification.request.payment.RevenueRecognitionCustomer;


/**
 * This job will publish the "price of goods sold to the customer" and
 * includes any margins and GST on accommodations and activities to Kafka for BCF to create a report.
 * The report will also incllude all service or non-refundable fee product prices
 */
public class RevenueRedemptionJob extends AbstractJobPerformable<RevenueRedemptionCronJobModel>
{

	private static final Logger LOG = Logger.getLogger(RevenueRedemptionJob.class);
	private static final String REDEMPTION_TYPE_CUSTOMER = "customer";

	private BcfOrderService orderService;
	private ConfigurationService configurationService;
	private Map<OrderEntryType, String> revenueRecognitionEntryMap;
	private NotificationEngineRestService notificationEngineRestService;

	@Resource(name = "changeAndCancellationFeeCalculationHelper")
	private ChangeAndCancellationFeeCalculationHelper changeAndCancellationFeeCalculationHelper;

	@Override
	public PerformResult perform(final RevenueRedemptionCronJobModel cronJobModel)
	{
		String configuredDate = getConfigurationService().getConfiguration().getString("revenue.recognition.date");
		final DateFormat dateFormat = new SimpleDateFormat(BcfintegrationserviceConstants.DATE_FORMAT);
		Date specificDate;
		try
		{
			if (StringUtils.isNotBlank(configuredDate))
			{
				specificDate = dateFormat.parse(configuredDate);
			}
			else
			{
				final Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				configuredDate = dateFormat.format(cal.getTime());
				specificDate = dateFormat.parse(configuredDate);
			}

			Calendar startDate = new GregorianCalendar();
			startDate.setTime(specificDate);
			startDate.set(11, 0);
			startDate.set(12, 0);
			startDate.set(13, 0);
			Calendar endDate = new GregorianCalendar();
			endDate.setTime(specificDate);
			endDate.set(11, 23);
			endDate.set(12, 59);
			endDate.set(13, 59);

			final Date rangeStartDate = startDate.getTime();
			final Date rangeEndDate = endDate.getTime();

			final List<OrderModel> orders = getOrderService().findOrdersForSpecificSailingDate(rangeStartDate, rangeEndDate);
			final RevenueRecognitionCustomer revenueRecognitionCustomer = getRevenueRecognitionPayLoad(orders,
					cronJobModel.getRedemptionType());
			revenueRecognitionCustomer.setSessionStart(rangeStartDate);
			revenueRecognitionCustomer.setSessionEnd(rangeEndDate);

			getNotificationEngineRestService()
					.sendRestRequest(revenueRecognitionCustomer, BaseResponseDTO.class, HttpMethod.POST,
							BcfintegrationserviceConstants.REVENUE_RECOGNITION_CUSTOMER_URL);

			if (CollectionUtils.isEmpty(orders))
			{
				LOG.info("No Orders for the specified date");
			}

		}
		catch (final ParseException e)
		{
			LOG.error("unable to parse the specified date", e);
		}
		catch (IntegrationException e)
		{
			LOG.error("unable to publish Data", e);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	protected RevenueRecognitionCustomer getRevenueRecognitionPayLoad(final List<OrderModel> orders, String redemptionType)
	{
		final RevenueRecognitionCustomer revenueRecognitionCustomer = new RevenueRecognitionCustomer();
		revenueRecognitionCustomer
				.setBookingChannel(REDEMPTION_TYPE_CUSTOMER.equals(redemptionType) ? "Revenue Recognition" : "Cost of Goods Sold");
		final List<PackageCostBreakupCustomer> packagesRedeemed = new ArrayList<>();
		orders.forEach(order -> {
			PackageCostBreakupCustomer packageCostBreakupCustomer = new PackageCostBreakupCustomer();
			Date firstTravelDate = null;
			if (Objects.nonNull(order.getVacationDepartureDate()))
			{
				firstTravelDate = order.getVacationDepartureDate();
			}
			else
			{
				firstTravelDate = changeAndCancellationFeeCalculationHelper.getFirstTravelDate(order.getEntries(), null);
			}
			String firstTravelDateFormatted = new SimpleDateFormat(BcfintegrationserviceConstants.DATE_FORMAT)
					.format(firstTravelDate);
			packageCostBreakupCustomer.setFirstTravelDate(firstTravelDateFormatted);
			packageCostBreakupCustomer.setBookingReference(order.getCode());
			packageCostBreakupCustomer.setCreationTime(order.getCreationtime());
			packageCostBreakupCustomer.setModifiedTime(order.getModifiedtime());
			final Map<OrderEntryType, List<AbstractOrderEntryModel>> orderEntriesByType = order.getEntries().stream()
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getType));
			final Set<OrderEntryType> orderEntryTypeSet = orderEntriesByType.keySet();
			final Map<String, BigDecimal> lineItems = new HashMap<>();
			for (final OrderEntryType type : orderEntryTypeSet)
			{
				updateLineItems(redemptionType, orderEntriesByType, lineItems, type);
			}
			packageCostBreakupCustomer.setLineItems(lineItems);
			BigDecimal total = lineItems.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);
			final BigDecimal priceBD = total.setScale(2, RoundingMode.HALF_UP);
			packageCostBreakupCustomer.setTotal(priceBD);
			packagesRedeemed.add(packageCostBreakupCustomer);

		});

		revenueRecognitionCustomer.setPackagesRedeemed(packagesRedeemed);

		Map<String, BigDecimal> totalRedeemed = new HashMap<>();
		List<Map.Entry<String, BigDecimal>> allLineItems = packagesRedeemed.stream()
				.flatMap(redeemedPackage -> redeemedPackage.getLineItems().entrySet().stream()).collect(Collectors.toList());
		allLineItems.forEach(lineItem ->
				totalRedeemed.merge(lineItem.getKey(), lineItem.getValue(), BigDecimal::add));

		revenueRecognitionCustomer.setTotalRedeemed(totalRedeemed);

		return revenueRecognitionCustomer;
	}

	private void updateLineItems(final String redemptionType,
			final Map<OrderEntryType, List<AbstractOrderEntryModel>> orderEntriesByType, final Map<String, BigDecimal> lineItems,
			final OrderEntryType type)
	{
		if (OrderEntryType.ACCOMMODATION.equals(type) || OrderEntryType.ACTIVITY.equals(type))
		{
			final List<AbstractOrderEntryModel> entries = orderEntriesByType.get(type);
			Double productsSum;
			if (REDEMPTION_TYPE_CUSTOMER.equals(redemptionType))
			{
				productsSum = entries.stream().map(entry -> entry.getTotalPrice()).reduce(Double::sum).get();
			}
			else
			{
				productsSum = entries.stream().map(entry -> entry.getCostToBCF()).reduce(Double::sum).get();
			}
			final BigDecimal productsSumBD = new BigDecimal(productsSum).setScale(2, RoundingMode.HALF_UP);
			lineItems.put(getRevenueRecognitionEntryMap().get(type), productsSumBD);
		}
		if (OrderEntryType.FEE.equals(type) && REDEMPTION_TYPE_CUSTOMER.equals(redemptionType))
		{
			final List<AbstractOrderEntryModel> entries = orderEntriesByType.get(type);
			final Map<String, List<AbstractOrderEntryModel>> orderEntriesByProduct = entries.stream().collect(
					Collectors.groupingBy(entry -> entry.getProduct().getCode()));
			final Set<String> productCodeSet = orderEntriesByProduct.keySet();
			for (final String productCode : productCodeSet)
			{
				final List<AbstractOrderEntryModel> entriesPerProduct = orderEntriesByProduct.get(productCode);
				final Double productsSumForFeeEntryType = entriesPerProduct.stream().map(entry -> entry.getTotalPrice())
						.reduce(Double::sum).get();
				final BigDecimal productsSumForFeeEntryTypeBD = new BigDecimal(productsSumForFeeEntryType)
						.setScale(2, RoundingMode.HALF_UP);
				lineItems.put(getRevenueRecognitionEntryMap().get(productCode), productsSumForFeeEntryTypeBD);
			}
		}
	}

	protected BcfOrderService getOrderService()
	{
		return orderService;
	}

	@Required
	public void setOrderService(final BcfOrderService orderService)
	{
		this.orderService = orderService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected Map<OrderEntryType, String> getRevenueRecognitionEntryMap()
	{
		return revenueRecognitionEntryMap;
	}

	@Required
	public void setRevenueRecognitionEntryMap(
			final Map<OrderEntryType, String> revenueRecognitionEntryMap)
	{
		this.revenueRecognitionEntryMap = revenueRecognitionEntryMap;
	}

	protected NotificationEngineRestService getNotificationEngineRestService()
	{
		return notificationEngineRestService;
	}

	@Required
	public void setNotificationEngineRestService(final NotificationEngineRestService notificationEngineRestService)
	{
		this.notificationEngineRestService = notificationEngineRestService;
	}
}
