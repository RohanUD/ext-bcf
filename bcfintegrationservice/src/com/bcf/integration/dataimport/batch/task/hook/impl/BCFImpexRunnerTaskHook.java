/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import java.io.FileNotFoundException;
import com.bcf.integration.dataimport.batch.task.hook.ImpexRunnerTaskHook;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;


public class BCFImpexRunnerTaskHook extends AbstractImpexRunnerTask implements ImpexRunnerTaskHook
{

	private ImpexRunnerTaskHookStrategy impexRunnerTaskHookStrategy;

	@Override
	public BatchHeader execute(final BatchHeader header) throws FileNotFoundException
	{
		super.execute(header);

		//perform post actions
		afterImpexRunnerTask(header);

		return header;
	}

	@Override
	public BatchHeader afterImpexRunnerTask(final BatchHeader header)
	{
		//Call the respective strategy
		impexRunnerTaskHookStrategy.afterImpexRunnerTask(header);

		return header;
	}

	@Override
	public ImportConfig getImportConfig()
	{
		return null;
	}

	public ImpexRunnerTaskHookStrategy getImpexRunnerTaskHookStrategy()
	{
		return impexRunnerTaskHookStrategy;
	}

	public void setImpexRunnerTaskHookStrategy(final ImpexRunnerTaskHookStrategy impexRunnerTaskHookStrategy)
	{
		this.impexRunnerTaskHookStrategy = impexRunnerTaskHookStrategy;
	}
}
