/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;


public class TerminalCacheIncImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{

	private CMSSiteService cmsSiteService;
	private ModelService modelService;
	private BaseSiteService baseSiteService;


	@Override
	public void afterImpexRunnerTask(final BatchHeader header)
	{
		//Incrementing terminal cache version number by 1
		CMSSiteModel cmsSite = getCmsSiteService().getCurrentSite();
		if (cmsSite == null)
		{
			cmsSite = (CMSSiteModel) baseSiteService.getBaseSiteForUID(BcfCoreConstants.DEFAULT_SITE_ID);
		}
		cmsSite.setTerminalsCacheVersion(cmsSite.getTerminalsCacheVersion().intValue() + 1);

		getModelService().save(cmsSite);
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}



}
