/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.catalog.synchronization.CatalogSynchronizationService;
import de.hybris.platform.catalog.synchronization.SyncConfig;
import de.hybris.platform.catalog.synchronization.SyncResult;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Objects;
import org.apache.log4j.Logger;
import com.bcf.integration.catalogsync.service.CatalogVersionSyncJobService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;


public class AmenitiesImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{
	private static final Logger LOG = Logger.getLogger(AmenitiesImpexRunnerTaskHookStrategy.class);

	private static final String CATALOGSYNC_JOB_CODE = "bcfintegrationservice.catalog.sync.job.code";

	private CatalogSynchronizationService catalogSynchronizationService;
	private CatalogVersionSyncJobService catalogVersionSyncJobService;
	private ConfigurationService configurationService;
	private SyncConfig syncConfig;

	@Override
	public void afterImpexRunnerTask(final BatchHeader header)
	{
		//sync product catalog
		synchronizeProductCatalog(header);
	}

	/**
	 * Synchronize product catalog.
	 */
	private void synchronizeProductCatalog(final BatchHeader header)
	{
		final String fileName = header.getFile().getName();
		LOG.info(String.format("Begin synchronizing Product Catalog [%s] after importing hotfolder file [%s]",
				BcfintegrationserviceConstants.PRODUCT_CATALOG_ID, fileName));
		final CatalogVersionSyncJobModel catalogVersionSyncJob = getCatalogVersionSyncJobService()
				.getCatalogVersionSyncJob(getConfigurationService().getConfiguration().getString(CATALOGSYNC_JOB_CODE,
						"sync bcfProductCatalog:Staged->Online"));
		final SyncResult syncResult = getCatalogSynchronizationService().synchronize(catalogVersionSyncJob, syncConfig);
		if (Objects.nonNull(syncResult) && (getCatalogSynchronizationService().isInProgress(catalogVersionSyncJob)
				|| (syncResult.isFinished() && syncResult.isSuccessful())))
		{
			LOG.info(
					String.format("Product Catalog [%s] sync finished successfully after importing hotfolder file [%s], cronjob [%s]",
							BcfintegrationserviceConstants.PRODUCT_CATALOG_ID, fileName));
		}
		else
		{
			LOG.error(String.format("Product Catalog [%s] sync has failed after importing hotfolder file [%s]",
					BcfintegrationserviceConstants.PRODUCT_CATALOG_ID, fileName));
		}

		LOG.info(String.format("End synchronizing Product Catalog [%s] after importing hotfolder file [%s]",
				BcfintegrationserviceConstants.PRODUCT_CATALOG_ID, fileName));
	}

	/**
	 * @return the catalogSynchronizationService
	 */
	public CatalogSynchronizationService getCatalogSynchronizationService()
	{
		return catalogSynchronizationService;
	}

	/**
	 * @param catalogSynchronizationService the catalogSynchronizationService to set
	 */
	public void setCatalogSynchronizationService(final CatalogSynchronizationService catalogSynchronizationService)
	{
		this.catalogSynchronizationService = catalogSynchronizationService;
	}

	/**
	 * @return the catalogVersionSyncJobService
	 */
	public CatalogVersionSyncJobService getCatalogVersionSyncJobService()
	{
		return catalogVersionSyncJobService;
	}

	/**
	 * @param catalogVersionSyncJobService the catalogVersionSyncJobService to set
	 */
	public void setCatalogVersionSyncJobService(final CatalogVersionSyncJobService catalogVersionSyncJobService)
	{
		this.catalogVersionSyncJobService = catalogVersionSyncJobService;
	}

	/**
	 * @return the syncConfig
	 */
	public SyncConfig getSyncConfig()
	{
		return syncConfig;
	}

	/**
	 * @param syncConfig the syncConfig to set
	 */
	public void setSyncConfig(final SyncConfig syncConfig)
	{
		this.syncConfig = syncConfig;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}


}
