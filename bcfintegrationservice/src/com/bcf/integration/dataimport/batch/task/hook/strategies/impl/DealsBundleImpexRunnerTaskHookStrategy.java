/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;
import com.bcf.integration.dataupload.utility.deals.DealsBundleDataUploadUtility;


public class DealsBundleImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{

	private DealsBundleDataUploadUtility dealsBundleDataUploadUtility;

	@Override
	public void afterImpexRunnerTask(final BatchHeader header)
	{
		getDealsBundleDataUploadUtility().uploadDealsBundleData();
	}

	/**
	 * @return the dealsBundleDataUploadUtility
	 */
	public DealsBundleDataUploadUtility getDealsBundleDataUploadUtility()
	{
		return dealsBundleDataUploadUtility;
	}

	/**
	 * @param dealsBundleDataUploadUtility the dealsBundleDataUploadUtility to set
	 */
	public void setDealsBundleDataUploadUtility(final DealsBundleDataUploadUtility dealsBundleDataUploadUtility)
	{
		this.dealsBundleDataUploadUtility = dealsBundleDataUploadUtility;
	}

}
