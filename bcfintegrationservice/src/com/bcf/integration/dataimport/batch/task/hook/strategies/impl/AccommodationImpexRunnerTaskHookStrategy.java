/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;
import com.bcf.integration.dataupload.utility.accommodations.AccommodationsDataUploadUtility;


public class AccommodationImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{

	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;


	@Override
	public void afterImpexRunnerTask(final BatchHeader header)
	{
		getAccommodationsDataUploadUtility().uploadAccommodationsData();
	}


	/**
	 * @return the accommodationsDataUploadUtility
	 */
	public AccommodationsDataUploadUtility getAccommodationsDataUploadUtility()
	{
		return accommodationsDataUploadUtility;
	}


	/**
	 * @param accommodationsDataUploadUtility the accommodationsDataUploadUtility to set
	 */
	public void setAccommodationsDataUploadUtility(final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}

}
