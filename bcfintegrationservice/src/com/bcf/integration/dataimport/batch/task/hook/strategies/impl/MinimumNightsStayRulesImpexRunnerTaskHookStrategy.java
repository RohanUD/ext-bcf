/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;
import com.bcf.integration.dataupload.utility.accommodations.MinimumNightsStayRulesDataUploadUtility;


public class MinimumNightsStayRulesImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{

	private MinimumNightsStayRulesDataUploadUtility minimumNightsStayRulesDataUploadUtility;


	@Override
	public void afterImpexRunnerTask(final BatchHeader header)
	{
		minimumNightsStayRulesDataUploadUtility.uploadMinimumNightsStayRulesDatas();
	}

	public MinimumNightsStayRulesDataUploadUtility getMinimumNightsStayRulesDataUploadUtility()
	{
		return minimumNightsStayRulesDataUploadUtility;
	}

	public void setMinimumNightsStayRulesDataUploadUtility(
			final MinimumNightsStayRulesDataUploadUtility minimumNightsStayRulesDataUploadUtility)
	{
		this.minimumNightsStayRulesDataUploadUtility = minimumNightsStayRulesDataUploadUtility;
	}
}
