/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataimport.batch.task.hook.strategies.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import org.apache.log4j.Logger;
import com.bcf.integration.dataimport.batch.task.hook.strategies.ImpexRunnerTaskHookStrategy;


public class TransportOfferingImpexRunnerTaskHookStrategy implements ImpexRunnerTaskHookStrategy
{

	private static final Logger LOG = Logger.getLogger(TransportOfferingImpexRunnerTaskHookStrategy.class);

	private static final String BCFERRIES_INDEX = "bcferriesIndex";

	private SetupSolrIndexerService setupSolrIndexerService;

	@Override
	public void afterImpexRunnerTask(BatchHeader header)
	{
		try
		{
			//Perform Update indexing
			getSetupSolrIndexerService().executeSolrIndexerCronJob(BCFERRIES_INDEX, false);
		}
		catch (final Exception e)
		{
			LOG.error("Error while doing index update after importing hot folder file: " + header.getFile().getName());
		}
	}

	public SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	public void setSetupSolrIndexerService(SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}


}
