/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.crm.populators.CRMCustomerRequestPopulator;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CRMCustomerEmailUpdateResponseDTO;
import com.bcf.integration.data.CRMGetCustomerAccountResponseDTO;
import com.bcf.integration.data.UpdateCRMCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCRMUpdateCustomerService implements CRMUpdateCustomerService
{
	private BaseRestService crmRestService;
	private CRMCustomerRequestPopulator crmCustomerRequestPopulator;
	private IntegrationUtility integrationUtils;

	@Override
	public UpdateCRMCustomerResponseDTO updateCustomer(final CustomerModel customerModel) throws IntegrationException
	{
		return (UpdateCRMCustomerResponseDTO) getCrmRestService().sendRestRequest(
				getCrmCustomerRequestPopulator().getRequestBody(customerModel), UpdateCRMCustomerResponseDTO.class,
				BcfintegrationserviceConstants.CRM_CUSTOMER_UPDATE_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public UpdateCRMCustomerResponseDTO updateAccount(final CustomerModel customerModel, final RegisterData registerData) throws IntegrationException
	{
		return (UpdateCRMCustomerResponseDTO) getCrmRestService().sendRestRequest(
				getCrmCustomerRequestPopulator().getRequestBody(customerModel), UpdateCRMCustomerResponseDTO.class,
				BcfintegrationserviceConstants.CRM_CUSTOMER_UPDATE_ACCOUNT_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public CRMCustomerEmailUpdateResponseDTO updateEmail(final CustomerModel customerModel, final String newEmail)
			throws IntegrationException
	{
		return (CRMCustomerEmailUpdateResponseDTO) getCrmRestService().sendRestRequest(
				getCrmCustomerRequestPopulator().getRequestBodyForUpdateEmail(customerModel, newEmail),
				CRMCustomerEmailUpdateResponseDTO.class,
				BcfintegrationserviceConstants.CRM_CUSTOMER_UPDATE_EMAIL_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public CRMGetCustomerAccountResponseDTO getCustomerAccount(final B2BUnitModel b2BUnitModel) throws IntegrationException
	{
		return (CRMGetCustomerAccountResponseDTO) getCrmRestService().sendRestRequest(
				null, CRMGetCustomerAccountResponseDTO.class,
				BcfintegrationserviceConstants.CRM_GET_CUSTOMER_ACCOUNT_SERVICE_URL, HttpMethod.GET,getParams(b2BUnitModel));
	}

	private Map<String, Object> getParams(final B2BUnitModel b2BUnitModel)
	{
		final Map<String, Object> params = new HashMap<>();

		final Map<String, String> systemIdentifiers = b2BUnitModel.getSystemIdentifiers();
		if (MapUtils.isNotEmpty(systemIdentifiers))
		{
			if (systemIdentifiers.containsKey(BcfCoreConstants.CRM_SOURCE_SYSTEM))
			{
				params.put(BcfintegrationserviceConstants.SOURCE_SYSTEM_KEY, BcfCoreConstants.CRM_SOURCE_SYSTEM);
				params.put(BcfintegrationserviceConstants.ID_KEY, systemIdentifiers.get(BcfCoreConstants.CRM_SOURCE_SYSTEM));
			}
			else if (systemIdentifiers.containsKey(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM))
			{
				params.put(BcfintegrationserviceConstants.SOURCE_SYSTEM_KEY, BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
				params.put(BcfintegrationserviceConstants.ID_KEY, systemIdentifiers.get(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM));
			}
		}
		return params;
	}

	protected BaseRestService getCrmRestService()
	{
		return crmRestService;
	}

	@Required
	public void setCrmRestService(final BaseRestService crmRestService)
	{
		this.crmRestService = crmRestService;
	}

	protected CRMCustomerRequestPopulator getCrmCustomerRequestPopulator()
	{
		return crmCustomerRequestPopulator;
	}

	@Required
	public void setCrmCustomerRequestPopulator(final CRMCustomerRequestPopulator crmCustomerRequestPopulator)
	{
		this.crmCustomerRequestPopulator = crmCustomerRequestPopulator;
	}

	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}


}
