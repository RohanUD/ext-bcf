/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service.impl;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.SUBSCRIPTION_INDEX_PARAM_KEY;

import de.hybris.platform.core.model.user.CustomerModel;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.subscription.dao.BcfSubscriptionsMasterDetailsDao;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.crm.populators.CustomerSubscriptionRequestPopulator;
import com.bcf.integration.crm.service.CRMCustomerSubscriptionService;
import com.bcf.integration.data.CustomerSubscriptionRequestDTO;
import com.bcf.integration.data.CustomerSubscriptionResponseDTO;
import com.bcf.integration.data.SubscriberListForRouteResponseDTO;
import com.bcf.integration.data.SubscriptionCountsByRouteResponseDTO;
import com.bcf.integration.data.SubscriptionCustomerDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCRMCustomerSubscriptionService implements CRMCustomerSubscriptionService
{
	private static final Logger LOG = Logger.getLogger(DefaultCRMCustomerSubscriptionService.class);

	private static final String EMAIL_ADDRESS = "emailAddress";

	private static final int DEFAULT_INITIAL_INDEX = 0;

	private BaseRestService crmRestService;
	private CustomerSubscriptionRequestPopulator customerSubscriptionRequestPopulator;

	private BcfSubscriptionsMasterDetailsDao bcfSubscriptionsMasterDetailsDao;

	@Override
	public CustomerSubscriptionResponseDTO createOrUpdateCustomerSubscriptions(final CustomerModel customerModel)
			throws IntegrationException
	{
		final CustomerSubscriptionRequestDTO requestBody = customerSubscriptionRequestPopulator.populate(customerModel);
		return (CustomerSubscriptionResponseDTO) getCrmRestService().sendRestRequest(requestBody,
				CustomerSubscriptionResponseDTO.class,
				BcfintegrationserviceConstants.CRM_CREATE_OR_UPDATE_CUSTOMER_SUBSCRIPTION_SERVICE_URL,
				HttpMethod.POST);
	}

	@Override
	public CustomerSubscriptionResponseDTO getCustomerSubscriptionsFromCRM(final CustomerModel customerModel)
			throws IntegrationException
	{
		final Map<String, Object> params = this.getParams(customerModel);
		if (MapUtils.isEmpty(params))
		{
			throw new IntegrationException("System Identifiers not found for the customer " + customerModel.getUid());
		}
		return (CustomerSubscriptionResponseDTO) getCrmRestService().sendRestRequest(null, CustomerSubscriptionResponseDTO.class,
				BcfintegrationserviceConstants.CRM_GET_CUSTOMER_SUBSCRIPTION_SERVICE_URL,
				HttpMethod.GET, params);
	}

	@Override
	public CustomerSubscriptionResponseDTO unsubscribeAll(final CustomerModel customerModel)
			throws IntegrationException, UnsupportedEncodingException
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(EMAIL_ADDRESS, URLEncoder.encode(customerModel.getUid(), "UTF-8"));

		return (CustomerSubscriptionResponseDTO) getCrmRestService().sendRestRequest(null, CustomerSubscriptionResponseDTO.class,
				BcfintegrationserviceConstants.CRM_UNSUBSCRIBE_ALL_SUBSCRIPTION_SERVICE_URL,
				HttpMethod.DELETE, params);
	}

	@Override
	public List<SubscriptionCustomerDTO> getSubscribersForRouteFromCRM(final String subscriptionCode) throws IntegrationException
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfintegrationserviceConstants.SUBSCRIPTION_CODE_PARAM_KEY, subscriptionCode);


		final List<SubscriberListForRouteResponseDTO> responseDTOs = new ArrayList<>();
		SubscriberListForRouteResponseDTO response;
		boolean isLastIndex = false;
		Integer nextIndex = DEFAULT_INITIAL_INDEX;
		do
		{
			params.put(SUBSCRIPTION_INDEX_PARAM_KEY, String.valueOf(nextIndex));
			response = (SubscriberListForRouteResponseDTO) getCrmRestService()
					.sendRestRequest(null, SubscriberListForRouteResponseDTO.class,
							BcfintegrationserviceConstants.CRM_SUBSCRIBERS_LIST_SERVICE_URL,
							HttpMethod.GET, params);
			responseDTOs.add(response);
			isLastIndex = response.getSubscriptionContacts().isLastPageInd();
			nextIndex = response.getSubscriptionContacts().getCurrentIndex();
		}
		while (!isLastIndex && nextIndex != null);

		return responseDTOs.stream().flatMap(r->r.getSubscriptionContacts().getCustomer().stream()).collect(Collectors.toList());
	}

	private Map<String, Object> getParams(final CustomerModel customerModel)
	{
		final Map<String, Object> params = new HashMap<>();
		final Map<String, String> systemIdentifiers = customerModel.getSystemIdentifiers();
		if (MapUtils.isNotEmpty(systemIdentifiers))
		{
			if (systemIdentifiers.containsKey(BcfCoreConstants.CRM_SOURCE_SYSTEM))
			{
				params.put(BcfintegrationserviceConstants.SOURCE_SYSTEM_KEY, BcfCoreConstants.CRM_SOURCE_SYSTEM);
				params.put(BcfintegrationserviceConstants.ID_KEY, systemIdentifiers.get(BcfCoreConstants.CRM_SOURCE_SYSTEM));
			}
			else if (systemIdentifiers.containsKey(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM))
			{
				params.put(BcfintegrationserviceConstants.SOURCE_SYSTEM_KEY, BcfCoreConstants.HYBRIS_SOURCE_SYSTEM);
				params.put(BcfintegrationserviceConstants.ID_KEY, systemIdentifiers.get(BcfCoreConstants.HYBRIS_SOURCE_SYSTEM));
			}
		}
		return params;
	}

	@Override
	public List<CRMSubscriptionMasterDetailModel> getSubscriptionMasterDetailsForServiceNotice(
			final ServiceNoticeModel serviceNotice)
	{
		List<CRMSubscriptionMasterDetailModel> selectedSubscriptionMasterDetails = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(serviceNotice.getSubscriptionGroups()))
		{
			selectedSubscriptionMasterDetails = bcfSubscriptionsMasterDetailsDao
					.findSubscriptionsForGroups(
							serviceNotice.getSubscriptionGroups().stream().distinct().collect(Collectors.toList()));
		}

		if (CollectionUtils.isNotEmpty(serviceNotice.getSubscriptionMasterDetails()))
		{
			selectedSubscriptionMasterDetails = serviceNotice.getSubscriptionMasterDetails().stream().distinct()
					.collect(Collectors.toList());
		}

		return selectedSubscriptionMasterDetails;
	}

	protected BaseRestService getCrmRestService()
	{
		return crmRestService;
	}

	@Required
	public void setCrmRestService(final BaseRestService crmRestService)
	{
		this.crmRestService = crmRestService;
	}

	protected CustomerSubscriptionRequestPopulator getCustomerSubscriptionRequestPopulator()
	{
		return customerSubscriptionRequestPopulator;
	}

	@Required
	public void setBcfSubscriptionsMasterDetailsDao(
			final BcfSubscriptionsMasterDetailsDao bcfSubscriptionsMasterDetailsDao)
	{
		this.bcfSubscriptionsMasterDetailsDao = bcfSubscriptionsMasterDetailsDao;
	}

	@Required
	public void setCustomerSubscriptionRequestPopulator(
			final CustomerSubscriptionRequestPopulator customerSubscriptionRequestPopulator)
	{
		this.customerSubscriptionRequestPopulator = customerSubscriptionRequestPopulator;
	}
}
