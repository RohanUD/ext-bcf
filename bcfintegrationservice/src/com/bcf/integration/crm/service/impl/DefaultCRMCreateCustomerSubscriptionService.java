/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.crm.populators.CustomerSubscriptionRequestPopulator;
import com.bcf.integration.crm.service.CRMCreateCustomerSubscriptionService;
import com.bcf.integration.data.CustomerSubscriptionRequestDTO;
import com.bcf.integration.data.CustomerSubscriptionResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCRMCreateCustomerSubscriptionService implements CRMCreateCustomerSubscriptionService
{
	private BaseRestService crmRestService;
	private CustomerSubscriptionRequestPopulator customerSubscriptionRequestPopulator;

	@Override
	public CustomerSubscriptionResponseDTO createCustomerSubscription(final CustomerModel customer)
			throws IntegrationException
	{
		final CustomerSubscriptionRequestDTO requestBody = getCustomerSubscriptionRequestPopulator()
				.populate(customer);

		return (CustomerSubscriptionResponseDTO) getCrmRestService().sendRestRequest(requestBody,
				CustomerSubscriptionResponseDTO.class,
				BcfintegrationserviceConstants.CRM_CREATE_OR_UPDATE_CUSTOMER_SUBSCRIPTION_SERVICE_URL,
				HttpMethod.POST);
	}

	protected BaseRestService getCrmRestService()
	{
		return crmRestService;
	}

	@Required
	public void setCrmRestService(final BaseRestService crmRestService)
	{
		this.crmRestService = crmRestService;
	}

	protected CustomerSubscriptionRequestPopulator getCustomerSubscriptionRequestPopulator()
	{
		return customerSubscriptionRequestPopulator;
	}

	@Required
	public void setCustomerSubscriptionRequestPopulator(
			final CustomerSubscriptionRequestPopulator customerSubscriptionRequestPopulator)
	{
		this.customerSubscriptionRequestPopulator = customerSubscriptionRequestPopulator;
	}
}
