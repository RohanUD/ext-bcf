/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.facades.customer.search.SearchParams;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.crm.service.CRMSearchCustomerService;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.CRMGetSearchCustomerResponseDTO;
import com.bcf.integration.data.CustomerSearchRequestDTO;
import com.bcf.integration.data.EmailAddress;
import com.bcf.integration.data.EmploymentInfo;
import com.bcf.integration.data.PersonName;
import com.bcf.integration.data.PhoneNumber;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCRMSearchCustomerService implements CRMSearchCustomerService
{
	private BaseRestService crmRestService;

	private static final Logger LOG = Logger.getLogger(DefaultCRMSearchCustomerService.class);

	@Override
	public CRMGetCustomerResponseDTO getCustomer(final String userName, final boolean guestind)
			throws IntegrationException, UnsupportedEncodingException
	{
		return (CRMGetCustomerResponseDTO) getCrmRestService().sendRestRequest(null, CRMGetCustomerResponseDTO.class,
				BcfintegrationserviceConstants.GET_CRM_CUSTOMER_SERVICE_URL, HttpMethod.GET, getParams(userName, guestind));
	}

	@Override
	public CRMGetSearchCustomerResponseDTO searchCustomer(final SearchParams searchParams)
			throws IntegrationException
	{
		return (CRMGetSearchCustomerResponseDTO) getCrmRestService()
				.sendRestRequest(getCustomerRequestBody(searchParams),
						CRMGetSearchCustomerResponseDTO.class,
						BcfintegrationserviceConstants.SEARCH_CRM_CUSTOMERS_SERVICE_URL, HttpMethod.POST, Collections.emptyMap());
	}


	private Map<String, Object> getParams(final String userName, final boolean guestind) throws UnsupportedEncodingException
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfintegrationserviceConstants.OPERATION_KEY, BcfintegrationserviceConstants.OPERATION_VALUE);
		params.put(BcfintegrationserviceConstants.EMAIL, URLEncoder.encode(userName, StandardCharsets.UTF_8.toString()));
		params.put(BcfintegrationserviceConstants.GUESTIND, String.valueOf(guestind));
		return params;
	}

	private CustomerSearchRequestDTO getCustomerRequestBody(final SearchParams searchParams)
	{
		final CustomerSearchRequestDTO customerSearchRequestDTO = new CustomerSearchRequestDTO();

		final PersonName personName = new PersonName();
		personName.setGivenName(searchParams.getFirstName());
		personName.setSurname(searchParams.getLastName());
		customerSearchRequestDTO.setPersonName(personName);

		final EmailAddress emailAddress = new EmailAddress();
		emailAddress.setEmailAddress(searchParams.getEmailAddress());
		customerSearchRequestDTO.setEmailAddress(emailAddress);

		final PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setPhoneNumber(searchParams.getMobileNumber());
		customerSearchRequestDTO.setPhoneNumber(phoneNumber);

		final EmploymentInfo employmentInfo = new EmploymentInfo();
		employmentInfo.setCompanyName(searchParams.getCompanyName());
		customerSearchRequestDTO.setEmploymentInfo(employmentInfo);

		customerSearchRequestDTO.setCurrentIndex(searchParams.getLastIndex());
		customerSearchRequestDTO.setLastPageInd(searchParams.isLastPageInd());

		return customerSearchRequestDTO;
	}

	protected BaseRestService getCrmRestService()
	{
		return crmRestService;
	}

	@Required
	public void setCrmRestService(final BaseRestService crmRestService)
	{
		this.crmRestService = crmRestService;
	}
}
