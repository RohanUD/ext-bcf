/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.util.BcfB2bUtil;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.crm.populators.CRMAddPaymentCardRequestPopulator;
import com.bcf.integration.crm.service.CRMPaymentCardsService;
import com.bcf.integration.data.CRMAddOrUpdatePaymentCardsRequestDTO;
import com.bcf.integration.data.CRMAddOrUpdatePaymentCardsResponseDTO;
import com.bcf.integration.data.CRMGetPaymentCardsResponseDTO;
import com.bcf.integration.data.CRMRemovePaymentCardResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCRMPaymentCardsService implements CRMPaymentCardsService
{
	private static final String CRM_ID = "id";
	private static final String CRM_CUSTOMER_ID = "customerId";
	private static final String CARD_NUMBER = "cardNumber";

	private CRMAddPaymentCardRequestPopulator crmAddPaymentCardRequestPopulator;
	private BaseRestService crmRestService;

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;

	@Resource (name = "integrationUtils")
	private IntegrationUtility integrationUtils;

	@Override
	public CRMGetPaymentCardsResponseDTO getPaymentsCards(final CustomerModel customerModel) throws IntegrationException
	{
		final String url = getPaymentsCardsUrl();
		return (CRMGetPaymentCardsResponseDTO) getCrmRestService().sendRestRequest(null, CRMGetPaymentCardsResponseDTO.class, url, HttpMethod.GET, getParams(customerModel));
	}

	private String getPaymentsCardsUrl()
	{
		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (Objects.nonNull(b2bUnitInContext))
		{
			return BcfintegrationserviceConstants.CRM_GET_ACCOUNT_PAYMENT_CARDS_SERVICE_URL;
		}
		return BcfintegrationserviceConstants.CRM_GET_PAYMENT_CARDS_SERVICE_URL;
	}

	@Override
	public CRMAddOrUpdatePaymentCardsResponseDTO updatePaymentCards(final CreditCardPaymentInfoModel creditCardPaymentInfoModel)
			throws IntegrationException
	{
		final CRMAddOrUpdatePaymentCardsRequestDTO requestBody = getCrmAddPaymentCardRequestPopulator()
				.getRequestBody(creditCardPaymentInfoModel);
		return (CRMAddOrUpdatePaymentCardsResponseDTO) getCrmRestService()
				.sendRestRequest(requestBody, CRMAddOrUpdatePaymentCardsResponseDTO.class,
						getCrmUpdatePaymentCardServiceUrl(), HttpMethod.POST);
	}

	private String getCrmUpdatePaymentCardServiceUrl()
	{

		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (Objects.nonNull(b2bUnitInContext))
		{
			return BcfintegrationserviceConstants.CRM_ACCOUNT_UPDATE_PAYMENT_CARD_SERVICE_URL;
		}
		return BcfintegrationserviceConstants.CRM_UPDATE_PAYMENT_CARD_SERVICE_URL;
	}

	private String getCrmRemovePaymentCardServiceUrl()
	{

		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (Objects.nonNull(b2bUnitInContext))
		{
			return BcfintegrationserviceConstants.CRM_ACCOUNT_REMOVE_PAYMENT_CARD_SERVICE_URL;
		}
		return BcfintegrationserviceConstants.CRM_REMOVE_PAYMENT_CARD_SERVICE_URL;
	}

	@Override
	public CRMAddOrUpdatePaymentCardsResponseDTO updateAccountPaymentCard(final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfo)
			throws IntegrationException
	{
		final CRMAddOrUpdatePaymentCardsRequestDTO requestBody = getCrmAddPaymentCardRequestPopulator()
				.getRequestBody(ctcTcCardPaymentInfo);
		return (CRMAddOrUpdatePaymentCardsResponseDTO) getCrmRestService()
				.sendRestRequest(requestBody, CRMAddOrUpdatePaymentCardsResponseDTO.class,
						BcfintegrationserviceConstants.CRM_ACCOUNT_UPDATE_PAYMENT_CARD_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public CRMRemovePaymentCardResponseDTO removePaymentCard(final CustomerModel customerModel, final String paymentInfoId)
			throws IntegrationException
	{

		return (CRMRemovePaymentCardResponseDTO) getCrmRestService()
				.sendRestRequest(null, CRMRemovePaymentCardResponseDTO.class,
						getCrmRemovePaymentCardServiceUrl(), HttpMethod.DELETE,
						getRemoveParams(customerModel, paymentInfoId));
	}

	private Map<String, Object> getRemoveParams(final CustomerModel customerModel, final String paymentInfoId)
	{
		final Map<String, Object> params = this.getParams(customerModel);
		params.put(CARD_NUMBER, paymentInfoId);
		return params;
	}

	private Map<String, Object> getParams(final CustomerModel customerModel)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (Objects.nonNull(b2bUnitInContext))
		{
			keyMap.put(CRM_ID, integrationUtils
					.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, b2bUnitInContext.getSystemIdentifiers()));
			keyMap.put(CRM_CUSTOMER_ID, integrationUtils
					.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, customerModel.getSystemIdentifiers()));
		}
		else
		{
			keyMap.put(CRM_CUSTOMER_ID, integrationUtils
					.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, customerModel.getSystemIdentifiers()));
		}
		return keyMap;
	}

	protected BaseRestService getCrmRestService()
	{
		return crmRestService;
	}

	@Required
	public void setCrmRestService(final BaseRestService crmRestService)
	{
		this.crmRestService = crmRestService;
	}

	protected CRMAddPaymentCardRequestPopulator getCrmAddPaymentCardRequestPopulator()
	{
		return crmAddPaymentCardRequestPopulator;
	}

	@Required
	public void setCrmAddPaymentCardRequestPopulator(
			final CRMAddPaymentCardRequestPopulator crmAddPaymentCardRequestPopulator)
	{
		this.crmAddPaymentCardRequestPopulator = crmAddPaymentCardRequestPopulator;
	}

}
