/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import java.util.Objects;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.util.BcfB2bUtil;
import com.bcf.integration.data.Address;
import com.bcf.integration.data.CRMAddOrUpdatePaymentCardsRequestDTO;
import com.bcf.integration.data.PaymentProfileInfo;


public class CRMAddPaymentCardRequestPopulator
{

	private static final String CREDIT_CARD = "Credit Card";
	private IntegrationUtility integrationUtils;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;

	public CRMAddOrUpdatePaymentCardsRequestDTO getRequestBody(final PaymentInfoModel paymentInfoModel)
	{
		final CRMAddOrUpdatePaymentCardsRequestDTO requestBody = new CRMAddOrUpdatePaymentCardsRequestDTO();
		populateCustomerId(paymentInfoModel, requestBody);
		requestBody.setPaymentProfileInfo(createCardPaymentInfo(paymentInfoModel));
		return requestBody;
	}

	private void populateCustomerId(final PaymentInfoModel paymentInfoModel,
			final CRMAddOrUpdatePaymentCardsRequestDTO requestBody)
	{

		if (Objects.nonNull(paymentInfoModel.getB2bUnit()))
		{
			final String accountCustomerId = getIntegrationUtils().getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, paymentInfoModel.getB2bUnit().getSystemIdentifiers());
			requestBody.setCustomerId(accountCustomerId);

			final String contactId = getIntegrationUtils()
					.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM,
							((CustomerModel) paymentInfoModel.getUser()).getSystemIdentifiers());
			requestBody.setContactId(contactId);
		}
		else
		{
			requestBody.setCustomerId(getCustomerId((CustomerModel) paymentInfoModel.getUser()));
		}
	}

	private PaymentProfileInfo createCardPaymentInfo(final PaymentInfoModel paymentInfoModel)
	{
		final PaymentProfileInfo paymentProfileInfo = new PaymentProfileInfo();
		if(paymentInfoModel instanceof CreditCardPaymentInfoModel) {
			final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) paymentInfoModel;
			paymentProfileInfo.setAddress(getBillingAddress(paymentInfoModel.getBillingAddress()));
			paymentProfileInfo.setNumber(creditCardPaymentInfoModel.getToken());
			paymentProfileInfo.setCreditCardHolderName(creditCardPaymentInfoModel.getCcOwner());
			paymentProfileInfo.setType(CREDIT_CARD);
			final String creditCardType = enumerationService.getEnumerationName(creditCardPaymentInfoModel.getType());
			final String firstChar = creditCardType.substring(0, 1);
			paymentProfileInfo.setCreditCardType(creditCardType.replaceFirst(firstChar, firstChar.toUpperCase()));
			paymentProfileInfo.setCreditCardNumber(creditCardPaymentInfoModel.getNumber());
			paymentProfileInfo
					.setCardExpiry(creditCardPaymentInfoModel.getValidToMonth() + creditCardPaymentInfoModel.getValidToYear());
		}
		else if (paymentInfoModel instanceof CTCTCCardPaymentInfoModel)
		{
			paymentProfileInfo.setNumber(paymentInfoModel.getCode());
			paymentProfileInfo.setType(BcfCoreConstants.BCF_INTERNAL_CARD);
		}

		return paymentProfileInfo;
	}

	private Address getBillingAddress(final AddressModel billingAddress)
	{
		if (Objects.isNull(billingAddress))
		{
			return null;
		}

		final Address address = new Address();
		address.setAddressLine1(billingAddress.getLine1());
		address.setAddressLine2(billingAddress.getLine2());
		address.setCity(billingAddress.getTown());
		address.setPostalCode(billingAddress.getPostalcode());
		if (Objects.nonNull(billingAddress.getCountry()))
		{
			address.setCountryCode(billingAddress.getCountry().getIsocode());
		}
		if (Objects.nonNull(billingAddress.getRegion()))
		{
			address.setProvinceStateCode(billingAddress.getRegion().getIsocodeShort());
		}
		return address;
	}

	public String getCustomerId(final CustomerModel customerModel)
	{
		final B2BUnitModel b2bUnitInContext = bcfB2bUtil.getB2bUnitInContext();
		if (Objects.nonNull(b2bUnitInContext))
		{
			return b2bUnitInContext.getSystemIdentifiers().get(BcfCoreConstants.CRM_SOURCE_SYSTEM);
		}
		else
		{
			return getIntegrationUtils()
					.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, customerModel.getSystemIdentifiers());
		}
	}

	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}
}
