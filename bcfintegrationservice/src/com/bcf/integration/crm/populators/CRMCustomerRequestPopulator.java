/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.populators;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.integration.data.Address;
import com.bcf.integration.data.BCF_Customer;
import com.bcf.integration.data.CRMCustomerEmailChangeRequestDTO;
import com.bcf.integration.data.CustomerStatus;
import com.bcf.integration.data.EmailAddress;
import com.bcf.integration.data.PersonName;
import com.bcf.integration.data.PhoneNumber;
import com.bcf.integration.data.SystemIdentifier;
import com.bcf.integration.data.UpdateCRMCustomerRequestDTO;


public class CRMCustomerRequestPopulator
{

	private IntegrationUtility integrationUtils;

	private static final String CUSTOMER_STATUS = "Active";
	private static final String PHONE_USE = "Personal";
	private static final String PHONE_TECHNOLOGY = "PHONE";
	private static final String EMAIL_USE = "Personal";
	private static final String ADDRESS_USE_MAILING = "Mailing";

	public UpdateCRMCustomerRequestDTO getRequestBody(final CustomerModel customerModel)
	{
		final UpdateCRMCustomerRequestDTO requestBody = new UpdateCRMCustomerRequestDTO();
		final BCF_Customer bcfCustomer = new BCF_Customer();
		requestBody.setBCF_Customer(bcfCustomer);
		setSystemIdentifier(bcfCustomer, customerModel);
		setPersonaldetails(bcfCustomer, customerModel);
		setAddress(bcfCustomer, customerModel.getAddresses());// Need to check and send
		setEmailAddress(bcfCustomer, customerModel);
		setPhoneNumber(bcfCustomer, customerModel);// Need to check and send
		setCustomerStatus(bcfCustomer);

		bcfCustomer.setGuestInd(customerModel.isConsumer());
		bcfCustomer.setWebEnabledInd(customerModel.isWebEnabled());
		bcfCustomer.setSubscriberInd(this.checkOptedInSubscriber(customerModel));
		return requestBody;
	}

	private Boolean checkOptedInSubscriber(final CustomerModel customerModel)
	{

		if(CollectionUtils.isEmpty(customerModel.getSubscriptionList())) {
			return false;
		}

		final List<CustomerSubscriptionModel> customerSubscriptionModels = customerModel.getSubscriptionList().stream()
				.filter(CustomerSubscriptionModel::getIsSubscribed).collect(Collectors.toList());

		return CollectionUtils.isEmpty(customerSubscriptionModels) ? false : true;
	}

	private void setSystemIdentifier(final BCF_Customer bcfCustomer, final CustomerModel customerModel)
	{
		final List<SystemIdentifier> identifierList = new ArrayList<SystemIdentifier>();

		bcfCustomer.setSystemIdentifier(identifierList);

		if (MapUtils.isNotEmpty(customerModel.getSystemIdentifiers()))
		{
			customerModel.getSystemIdentifiers().forEach((k, v) -> {
				final SystemIdentifier identifier = new SystemIdentifier();
				identifier.setSourceSystem(k);
				identifier.setID(v);
				identifierList.add(identifier);
			});
		}
	}

	private void setCustomerStatus(final BCF_Customer bcfCustomer)
	{
		final CustomerStatus status = new CustomerStatus();
		status.setStatus(CUSTOMER_STATUS);// Need to check and set
		bcfCustomer.setCustomerStatus(status);
	}

	private void setPhoneNumber(final BCF_Customer bcfCustomer, final CustomerModel customerModel)
	{
		if (customerModel == null || StringUtils.isEmpty(customerModel.getPhoneNo()))
		{
			return;
		}
		final List<PhoneNumber> phoneList = new ArrayList<PhoneNumber>();
		final PhoneNumber phone = new PhoneNumber();
		phone.setPrimaryInd(true);// Need to check and set
		phone.setPhoneUse(PHONE_USE);// Need to check and set
		phone.setPhoneTechnology(PHONE_TECHNOLOGY);// Need to check and set
		phone.setPhoneNumber(customerModel.getPhoneNo());
		phone.setPhoneType(customerModel.getPhoneType());
		phoneList.add(phone);
		bcfCustomer.setPhoneNumber(phoneList);
	}

	private void setEmailAddress(final BCF_Customer bcfCustomer, final CustomerModel customerModel)
	{
		final List<EmailAddress> emailList = new ArrayList<EmailAddress>();
		final EmailAddress emailAddress = new EmailAddress();

		emailAddress.setEmailUse(EMAIL_USE);// Need to check and set
		emailAddress.setPrimaryInd(true);// Need to check and set
		emailAddress.setEmailAddress(customerModel.getUid());

		emailList.add(emailAddress);
		bcfCustomer.setEmailAddress(emailList);
	}

	private void setAddress(final BCF_Customer bcfCustomer, final Collection<AddressModel> addressModelList)
	{
		if (addressModelList == null || addressModelList.isEmpty())
		{
			return;
		}

		final AddressModel addressModel = addressModelList.stream()
				.filter(address -> Objects.nonNull(address.getPrimaryInd()) && Objects.equals(address.getPrimaryInd(), Boolean.TRUE))
				.findFirst().orElse(null);
		if(Objects.isNull(addressModel))
		{
			return;
		}

		final List<Address> addressList = new ArrayList<Address>();

		final Address address = new Address();
		address.setAddressUse(ADDRESS_USE_MAILING);
		address.setAddressLine1(addressModel.getLine1());
		address.setAddressLine2(addressModel.getLine2());
		address.setCity(addressModel.getTown());
		if (Objects.nonNull(addressModel.getRegion()))
		{
			address.setProvinceStateCode(addressModel.getRegion().getIsocodeShort());
		}
		address.setCountryCode(addressModel.getCountry().getIsocode());
		address.setPostalCode(addressModel.getPostalcode());
		address.setPrimaryInd(true);// Need to check and set
		addressList.add(address);

		bcfCustomer.setAddress(addressList);
	}

	private void setPersonaldetails(final BCF_Customer bcfCustomer, final CustomerModel customerModel)
	{
		final PersonName person = new PersonName();
		person.setGivenName(customerModel.getFirstName());
		person.setSurname(customerModel.getLastName());
		bcfCustomer.setPersonName(person);
	}

	public CRMCustomerEmailChangeRequestDTO getRequestBodyForUpdateEmail(final CustomerModel customerModel, final String newEmail)
	{
		final CRMCustomerEmailChangeRequestDTO crmCustomerEmailChangeRequestDTO = new CRMCustomerEmailChangeRequestDTO();
		final SystemIdentifier systemIdentifier = new SystemIdentifier();
		systemIdentifier.setID(getIntegrationUtils()
				.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, customerModel.getSystemIdentifiers()));
		systemIdentifier.setSourceSystem(BcfCoreConstants.CRM_SOURCE_SYSTEM);
		crmCustomerEmailChangeRequestDTO.setSystemIdentifier(systemIdentifier);
		crmCustomerEmailChangeRequestDTO.setEmailAddress(newEmail);
		return crmCustomerEmailChangeRequestDTO;
	}

	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}
}
