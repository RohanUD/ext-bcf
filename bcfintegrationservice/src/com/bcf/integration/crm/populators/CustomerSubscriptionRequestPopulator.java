/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.crm.populators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.BusinessOpportunitiesModel;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.CustomerSubscriptionModel;
import com.bcf.integration.data.BusinessOpportunity;
import com.bcf.integration.data.BusinessOpportunityList;
import com.bcf.integration.data.CustomerSubscriptionRequestDTO;
import com.bcf.integration.data.CustomerSubscriptions;
import com.bcf.integration.data.Subscription;
import com.bcf.integration.data.SubscriptionList;
import com.bcf.integration.data.SubscriptionOptInDetails;
import com.bcf.integration.data.SubscriptionOptOutDetails;
import com.bcf.integration.data.SystemIdentifier;


public class CustomerSubscriptionRequestPopulator
{
	@Resource
	private EnumerationService enumerationService;

	@Resource(name = "integrationUtils")
	private IntegrationUtility integrationUtils;

	public CustomerSubscriptionRequestDTO populate(final CustomerModel customerModel)
	{
		final CustomerSubscriptionRequestDTO customerSubscriptionRequestDTO = new CustomerSubscriptionRequestDTO();
		final CustomerSubscriptions customerSubscriptions = new CustomerSubscriptions();
		customerSubscriptionRequestDTO.setCustomerSubscriptions(customerSubscriptions);

		customerSubscriptions.setSystemIdentifier(
				this.getSystemIdentifiers(customerModel.getSystemIdentifiers()));

		if (CollectionUtils.isNotEmpty(customerModel.getSubscriptionList()))
		{
			final SubscriptionList subscriptionList = new SubscriptionList();
			customerSubscriptions.setSubscriptionList(subscriptionList);

			final List<Subscription> subscriptionDatas = new ArrayList<>();

			final Collection<CustomerSubscriptionModel> customerSubscriptionModels = customerModel.getSubscriptionList();
			customerSubscriptionModels.stream().forEach(customerSubscriptionModel -> {
				subscriptionDatas.add(this.getSubscription(customerSubscriptionModel));
			});
			subscriptionList.setSubscription(subscriptionDatas);
		}

		return customerSubscriptionRequestDTO;
	}

	private Subscription getSubscription(final CustomerSubscriptionModel customerSubscriptionModel)
	{
		final Subscription subscription = new Subscription();
		final CRMSubscriptionMasterDetailModel subscriptionMasterData = customerSubscriptionModel.getSubscriptionMasterData();
		subscription.setType(enumerationService.getEnumerationName(subscriptionMasterData.getSubscriptionType()));
		subscription.setCode(subscriptionMasterData.getSubscriptionCode());
		subscription.setName(subscriptionMasterData.getName());
		subscription.setDescription(subscriptionMasterData.getDescription());
		if (Objects.nonNull(subscriptionMasterData.getSubscriptionGroup()))
		{
			subscription.setNoticeGroup(subscriptionMasterData.getSubscriptionGroup().getCode());
		}
		if (customerSubscriptionModel.getIsSubscribed() && Objects.nonNull(customerSubscriptionModel.getSubscribedDate()))
		{
			final SubscriptionOptInDetails subscriptionOptInDetails = new SubscriptionOptInDetails();
			subscriptionOptInDetails.setSource(customerSubscriptionModel.getSource());
			subscriptionOptInDetails
					.setDateTime(TravelDateUtils.convertDateToStringDate(customerSubscriptionModel.getSubscribedDate(),
							BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			subscription.setOptInDetail(subscriptionOptInDetails);
		} else if (Objects.nonNull(customerSubscriptionModel.getUnSubscribedDate()))
		{
			final SubscriptionOptOutDetails subscriptionOptInOutDetails = new SubscriptionOptOutDetails();
			subscriptionOptInOutDetails.setSource(customerSubscriptionModel.getSource());
			subscriptionOptInOutDetails
					.setDateTime(TravelDateUtils.convertDateToStringDate(customerSubscriptionModel.getUnSubscribedDate(),
							BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			subscription.setOptOutDetail(subscriptionOptInOutDetails);
		}
		if (customerSubscriptionModel.getIsSubscribed() && Objects.nonNull(customerSubscriptionModel.getBusinessOpportunities()))
		{
			subscription
					.setBusinessOpportunityList(getBusinessOpportunitiesData(customerSubscriptionModel.getBusinessOpportunities()));
		}
		return subscription;
	}

	private BusinessOpportunityList getBusinessOpportunitiesData(final BusinessOpportunitiesModel businessOpportunitiesModel)
	{
		final BusinessOpportunityList businessOpportunityList = new BusinessOpportunityList();
		//		businessOpportunityList.setCompanyName(businessOpportunitiesModel.getCompanyName());
		//		businessOpportunityList.setJobTitle(businessOpportunitiesModel.getJobTitle());
		final List<BusinessOpportunity> businessOpportunities = new ArrayList<BusinessOpportunity>();
		businessOpportunitiesModel.getServiceSupplies().stream().forEach(service_supplies -> {
			final BusinessOpportunity businessOpportunity = new BusinessOpportunity();
			businessOpportunity.setName(service_supplies.getCode());
			businessOpportunity.setEnabledInd(true);
			businessOpportunities.add(businessOpportunity);
		});

		businessOpportunityList.setBusinessOpportunitiy(businessOpportunities);
		return businessOpportunityList;
	}

	private SystemIdentifier getSystemIdentifiers(final Map<String, String> customerSubscriptions)
	{
		final SystemIdentifier systemIdentifier = new SystemIdentifier();
		systemIdentifier.setSourceSystem(BcfCoreConstants.CRM_SOURCE_SYSTEM);
		systemIdentifier
				.setID(integrationUtils.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, customerSubscriptions));
		return systemIdentifier;
	}
}
