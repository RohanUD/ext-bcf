/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.constants;

@SuppressWarnings("PMD")
public class BcfintegrationserviceConstants extends GeneratedBcfintegrationserviceConstants
{
	public static final String EXTENSIONNAME = "bcfintegrationservice";

	private BcfintegrationserviceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "bcfintegrationservicePlatformLogo";
	public static final String CONFIRMBOOKING_CLIENT_CODE = "confirmbooking.client.code";
	public static final String CONFIRMBOOKING_BOOKINGTYPE_AMENDED = "confirmbooking.bookingtype.amended";
	public static final String CONFIRMBOOKING_BOOKINGTYPE_ANONYMOUS = "confirmbooking.bookingtype.anonymous";
	public static final String CONFIRMBOOKING_BOOKINGTYPE_SIGNEDIN = "confirmbooking.bookingtype.signedin";


	//Service URL Map keys
	public static final String LDAP_CUSTOMER_CREATE_SERVICE_URL = "LDAPCustomerCreateServiceUrl";
	public static final String LDAP_CUSTOMER_VERIFY_SERVICE_URL = "LDAPCustomerVerifyServiceUrl";
	public static final String LDAP_CUSTOMER_VERIFY_EMAIL_CHANGE_SERVICE_URL = "LDAPCustomerVerifyEmailChangeServiceUrl";
	public static final String LDAP_CUSTOMER_LOGIN_SERVICE_URL = "LDAPCustomerLoginServiceUrl";
	public static final String LDAP_CUSTOMER_PASSWORDRESET_SERVICE_URL = "LDAPCustomerPasswordResetServiceUrl";
	public static final String LDAP_CUSTOMER_VERIFYPASSWORDRESET_SERVICE_URL = "LDAPCustomerVerifyPasswordResetServiceUrl";
	public static final String LDAP_CUSTOMER_CHANGEPASSWORD_SERVICE_URL = "LDAPCustomerChangePasswordServiceUrl";
	public static final String LDAP_CUSTOMER_CHANGEEMAIL_SERVICE_URL = "LDAPCustomerChangeEmailServiceUrl";
	public static final String ACTIVE_DIRECTORY_AUTHENTICATE_SERVICE_URL = "ActiveDirectoryAuthenticateServiceUrl";
	public static final String CRM_CUSTOMER_UPDATE_SERVICE_URL = "CRMCustomerUpdateServiceUrl";
	public static final String CRM_CUSTOMER_UPDATE_ACCOUNT_SERVICE_URL = "CRMCustomerUpdateAccountServiceUrl";
	public static final String CRM_GET_CUSTOMER_ACCOUNT_SERVICE_URL = "CRMGetCustomerAccountServiceUrl";
	public static final String CRM_CUSTOMER_UPDATE_EMAIL_SERVICE_URL = "CRMCustomerUpdateEmailServiceUrl";
	public static final String CRM_GET_CUSTOMER_SUBSCRIPTION_SERVICE_URL = "CRMGetCustomerSubscriptionServiceUrl";
	public static final String CRM_UNSUBSCRIBE_ALL_SUBSCRIPTION_SERVICE_URL = "CRMUnsubscribeAllCustomerSubscriptionServiceUrl";
	public static final String CRM_CREATE_OR_UPDATE_CUSTOMER_SUBSCRIPTION_SERVICE_URL = "CRMCreateOrUpdateCustomerSubscriptionServiceUrl";
	public static final String CRM_SUBSCRIBERS_LIST_SERVICE_URL = "CRMGetSubscribersListServiceUrl";
	public static final String GET_CRM_CUSTOMER_SERVICE_URL = "GetCRMCustomerServiceUrl";
	public static final String CRM_GET_PAYMENT_CARDS_SERVICE_URL = "CRMGetPaymentCardsServiceUrl";
	public static final String CRM_GET_ACCOUNT_PAYMENT_CARDS_SERVICE_URL = "CRMGetAccountPaymentCardsServiceUrl";
	public static final String CRM_UPDATE_PAYMENT_CARD_SERVICE_URL = "CRMUpdatePaymentCardsServiceUrl";
	public static final String CRM_ACCOUNT_UPDATE_PAYMENT_CARD_SERVICE_URL = "CRMAccountUpdatePaymentCardsServiceUrl";
	public static final String CRM_REMOVE_PAYMENT_CARD_SERVICE_URL = "CRMRemovePaymentCardsServiceUrl";
	public static final String CRM_ACCOUNT_REMOVE_PAYMENT_CARD_SERVICE_URL = "CRMAccountRemovePaymentCardsServiceUrl";
	public static final String SEARCH_CRM_CUSTOMERS_SERVICE_URL = "SearchCRMCustomersServiceUrl";
	public static final String EBOOKING_SAILING_SERVICE_URL = "EBookingSailingServiceUrl";
	public static final String EBOOKING_SAILING_V2_SERVICE_URL = "EBookingSailingV2ServiceUrl";
	public static final String EBOOKING_MAKE_BOOKING_SERVICE_URL = "EBookingMakeBookingServiceUrl";
	public static final String EBOOKING_MODIFY_BOOKING_SERVICE_URL = "EBookingModifyBookingServiceUrl";
	public static final String EBOOKING_CONFIRM_BOOKING_SERVICE_URL = "EBookingConfirmBookingServiceUrl";
	public static final String EBOOKING_SEARCH_BOOKINGS_SERVICE_URL = "EBookingSearchBookingsServiceUrl";
	public static final String EBOOKING_CANCEL_BOOKING_SERVICE_URL = "EBookingCancelBookingServiceUrl";
	public static final String TRIPADVISOR_LOCATION_DETAILS_URL = "TripAdvisorLocationDetailsUrl";
	public static final String SEARCH_MOVEMENTS_URL = "SearchMovementsUrl";
	public static final String TRAILER_MOVEMENT_LOCATIONS_URL = "TrailerMovementLocationsUrl";
	public static final String EVENT_TYPES_URL = "EventTypesUrl";
	public static final String ON_REQUEST_BOOKING_URL = "onRequestBookingUrl";
	public static final String ORDER_CREATE_NOTIFICATION_URL = "orderCreateServiceUrl";
	public static final String CUSTOMER_REGISTRATION_SERVICE_URL = "customerRegistrationServiceUrl";
	public static final String FORGOT_PASSWORD_SERVICE_URL = "forgotPasswordServiceUrl";
	public static final String API_GATEWAY_TOKEN_SERVICE_URL = "ApiGatewayTokenServiceUrl";
	public static final String API_GATEWAY_TOKEN_CACHE_SERVICE_ENABLE = "apigateway.token.cache.service.enable";
	public static final String ORDER_CANCEL_NOTIFICATION_URL = "cancelOrderServiceUrl";
	public static final String ORDER_MODIFY_NOTIFICATION_URL = "modifyOrderServiceUrl";
	public static final String ORDER_MODIFY_NOTIFY_VENDOR_NOTIFICATION_URL = "modifyOrderNotifyVendorServiceUrl";
	public static final String SHARE_ITINERARY_SERVICE_URL = "shareItineraryServiceUrl";
	public static final String SERVICE_NOTICE_PUBLISH_SERVICE_URL = "serviceNoticePublishServiceUrl";
	public static final String SERVICE_NOTICE_PUBLISH_NEWS_URL = "serviceNoticePublishNewsUrl";
	public static final String SERVICE_NOTICE_PUBLISH_COMPETITION_URL = "serviceNoticePublishCompetitionUrl";
	public static final String SERVICE_SUBSTANTIAL_PERFORMANCE_NOTICE_PUBLISH_COMPETITION_URL = "serviceNoticePublishSubstantialPerformanceNoticeUrl";
	public static final String SERVICE_NOTICE_PUBLISH_BUSINESS_OPPORTUNITY_URL = "serviceNoticePublishBusinessOpportunityUrl";
	public static final String SERVICE_NOTICE_PUBLISH_ELECTRONIC_NEWS_LETTER_URL = "serviceNoticePublishElectronicNewsLetterUrl";
	public static final String TRIPADVISOR_API_REQUEST_HEADER_NAME = "X-TripAdvisor-API-Key";
	public static final String CURRENT_CONDITIONS_TERMINAL_URL = "currentConditionsTerminalUrl";
	public static final String CURRENT_CONDITIONS_ROUTE_URL = "currentConditionsRouteUrl";
	public static final String CURRENT_CONDITIONS_NEXT_SAILINGS_ROUTE_URL = "currentConditionsNextSailingsTerminalUrl";
	public static final String CURRENT_CONDITIONS_NEXT_SAILINGS_MULTI_TERMINAL_URL = "currentConditionsNextSailingsMultiTerminalUrl";
	public static final String CURRENT_CONDITIONS_MULTI_TERMINAL_URL = "currentConditionsMultiTerminalUrl";
	public static final String SEASONAL_SCHEDULES_URL = "seasonalSchedulesUrl";
	public static final String CATALOGVERSION_STAGED = "Staged";
	public static final String BUFFER_TIME_IN_SEC_BEFORE_TOKEN_EXPIRY = "bufferTimeInSec";
	public static final String END_OF_THE_DAY_REPORT_URL = "endOfTheDayReportUrl";
	public static final String REVENUE_RECOGNITION_CUSTOMER_URL = "revenueRecognitionCustomerUrl";


	public static final String DATE_FORMAT = "yyyy-MM-dd";



	public static final String PRODUCT_CATALOG_ID = "bcfProductCatalog";

	public static final String CURRENT_CMS_SITE_ID = "bcf.cms.site.id";
	public static final String CALL_CENTER_AGENT_ID = "payment.callcenter.agent.id";
	public static final String LOCATION_ID = "paymentLocationId";
	public static final String STATION_ID = "paymentStationId";
	public static final String TRANSACTION_TYPE_PURCHASE = "payment.transaction.type.purchase";
	public static final String TRANSACTION_TYPE_REFUND = "transaction.type.refund";
	public static final String CARD_IDENTIFIER_TYPE = "payment.card.identifier.type";
	public static final String CTC_CARD_IDENTIFIER_TYPE = "payment.ctc.card.identifier.type";
	public static final String CTC_CARD_VALUE_TYPE = "payment.ctc.card.value.type";
	public static final String TOKEN_TYPE_TEMPORARY = "payment.token.type.temporary";
	public static final String EXPECTED_CARD_TYPE_CREDITDEBIT = "payment.expected.card.type.creditDebit";
	public static final String PAYMENT_PROCESSING_METHOD = "payment.processing.method";
	public static final String PAYMENT_SERVICE_URL = "PaymentServiceUrl";
	public static final String CTC_CARD_PAYMENT_SERVICE_URL = "CTCCardPaymentServiceUrl";
	public static final String PAYMENT_VAULT_SERVICE_URL = "PaymentVaultServiceUrl";
	public static final String PAYMENT_TOKEN_SERVICE_URL = "PaymentTokenServiceUrl";
	public static final String RGISTER_CARD_TO_VAULT_SERVICE_URL = "PaymentTokenServiceUrl";

	public static final String REFUND_WITH_TOKEN_REQUEST_SERVICE_URL = "RefundWithTokenRequestServiceUrl";
	public static final String TRANS_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	public static final String NOTE_DELIMITER = "|||";
	public static final String CANCELLING_BOOKING_NOTE = " cancelled this booking";
	public static final String PASSENGER_TYPE_ADULT = "adult";
	public static final String PASSENGER_TYPE_CHILD = "child";
	public static final String BCFERRIES_INDEX = "bcferriesIndex";
	public static final String BCVACATION_INDEX = "bcVacationIndex";
	public static final String UNDERSCORE = "_";
	public static final String SESSION_PINPAD_ID = "pinPadID";
	public static final String SESSION_ICEBAR_ID = "iceBarID";

	public static final String GRANT_TYPE_KEY = "grant_type";
	public static final String GRANT_TYPE_VALUE = "client_credentials";
	public static final String CONSUMER_KEY = "apigateway.token.service.consumer.key";
	public static final String CONSUMER_SECRET = "apigateway.token.service.consumer.secret";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BASIC = "Basic ";
	public static final String SCOPE = "scope";
	public static final String SCOPE_PREFIX = "device_";
	public static final String LIST_SAILING_CALENDER_DAYS_BEFORE_SR = "short.route.list.sailing.calendar.days.before";
	public static final String LIST_SAILING_CALENDER_DAYS_AFTER_SR = "short.route.list.sailing.calendar.days.after";
	public static final String LIST_SAILING_CALENDER_DAYS_BEFORE_NR = "long.route.list.sailing.calendar.days.before";
	public static final String LIST_SAILING_CALENDER_DAYS_AFTER_NR = "long.route.list.sailing.calendar.days.after";
	public static final String QUOTATION_BOOKING = "QUOTATION";

	public static final String SUBSCRIPTION_CODE_PARAM_KEY = "code";
	public static final String SUBSCRIPTION_INDEX_PARAM_KEY = "index";

	public static final String ID_KEY = "ID";
	public static final String FIRST_NAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String COMPANY_NAME = "company";
	public static final String MOBILE_NUMBER = "phone";
	public static final String EMAIL = "email";
	public static final String SOURCE_SYSTEM_KEY = "SourceSystem";
	public static final String GUESTIND = "guestind";
	public static final String DEFAULT_ENCODING = "UTF-8";
	public static final String SOURCE_SYSTEM_VALUE = "EMAIL";
	public static final String OPERATION_KEY = "operation";
	public static final String OPERATION_VALUE = "RetrieveContact";
	public static final String ID = "id";
	public static final String SOURCE = "source";

	public static final String CARRYING_LIVESTOCK_SUFFIX = "L";
	public static final String UPDATE_NOTIFICATION_EMAIL_EVENT = "update";
	public static final String CUSTOMER_PROFILE_UPDATE_URL = "updateProfileServiceUrl";
	public static final String SAVE_QUOTE_NOTIFICATION_URL = "saveQuoteServiceUrl";
	public static final String FINANCIAL_DATA_NOTIFICATION_URL = "financialDataServiceUrl";
	public static final String YFORMS_NOTIFICATION_URL = "yFormsNotificationServiceUrl";

}
