/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.helper;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.email.service.BcfCMSEmailPageService;
import com.bcf.core.helper.VelocityTemplateHelper;
import com.bcf.notification.request.order.common.BaseContextRequestData;


public class NotificationEngineRestServiceHelper
{
	@Resource
	VelocityTemplateHelper velocityTemplateHelper;

	@Resource
	BcfCMSEmailPageService bcfCMSEmailPageService;

	/**
	 * updates the version of email template to be sent to notification engine
	 *
	 * @param requestBody
	 */
	public void updateEmailTemplateVersionInRequestBody(final BaseContextRequestData requestBody)
	{
		String templateId = velocityTemplateHelper.getEmailTemplateId(requestBody.getEventType());

		if (StringUtils.isNotBlank(templateId))
		{
			EmailPageTemplateModel emailTemplate = bcfCMSEmailPageService.getLatestEmailPageTemplateForTemplateId(templateId);
			if (Objects.nonNull(emailTemplate))
			{
				int version = emailTemplate.getVersion();
				if (version <= 0)
				{
					version = 1;
				}
				requestBody.setEmailTemplateVersion(version);
			}
		}
	}
}
