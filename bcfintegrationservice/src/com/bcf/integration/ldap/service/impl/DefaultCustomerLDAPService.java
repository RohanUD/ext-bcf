/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ldap.service.impl;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.service.BcfCustomerService;
import com.bcf.integration.activedirectory.service.BCFCustomerLDAPService;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.ChangeEmailRequestDTO;
import com.bcf.integration.data.ChangeEmailResponseDTO;
import com.bcf.integration.data.ChangePasswordRequestDTO;
import com.bcf.integration.data.CreateCustomerResponseDTO;
import com.bcf.integration.data.CreateCustumerRequestDTO;
import com.bcf.integration.data.PasswordChangeResponseDTO;
import com.bcf.integration.data.PasswordResetRequestDTO;
import com.bcf.integration.data.PasswordResetResponseDTO;
import com.bcf.integration.data.ValidateCustomerCredentialsResponseDTO;
import com.bcf.integration.data.ValidateCustomerRequestDTO;
import com.bcf.integration.data.VerifyCustomerResponseDTO;
import com.bcf.integration.data.VerifyCustumerEmailChangeRequestDTO;
import com.bcf.integration.data.VerifyCustumerRequestDTO;
import com.bcf.integration.data.VerifyPasswordResetRequestDTO;
import com.bcf.integration.data.VerifyPasswordResetResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCustomerLDAPService implements BCFCustomerLDAPService
{
	private BaseRestService ldapRestService;
	private BcfCustomerService bcfCustomerService;

	@Override
	public CreateCustomerResponseDTO registerAtLDAP(final RegisterData registerData) throws IntegrationException
	{
		return (CreateCustomerResponseDTO) getLdapRestService().sendRestRequest(
				createCustomerRequest(registerData), CreateCustomerResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_CREATE_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public VerifyCustomerResponseDTO verifyAtLDAP(final String accountValidationKey, final String email)
			throws IntegrationException
	{
		return (VerifyCustomerResponseDTO) getLdapRestService().sendRestRequest(
				createVerifyCustomerRequest(accountValidationKey, email), VerifyCustomerResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_VERIFY_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public VerifyCustomerResponseDTO verifyEmailChangeAtLDAP(final String accountValidationKey, final String oldEmail,
			final String newEmail)
			throws IntegrationException
	{
		return (VerifyCustomerResponseDTO) getLdapRestService().sendRestRequest(
				createVerifyCustomerEmailChangeRequest(accountValidationKey, oldEmail, newEmail), VerifyCustomerResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_VERIFY_EMAIL_CHANGE_SERVICE_URL, HttpMethod.POST);
	}

	private BaseRequestDTO createVerifyCustomerEmailChangeRequest(final String accountValidationKey, final String oldEmail,
			final String newEmail)
	{
		final VerifyCustumerEmailChangeRequestDTO requestBody = new VerifyCustumerEmailChangeRequestDTO();
		requestBody.setExistingEmail(oldEmail);
		requestBody.setNewEmail(newEmail);
		requestBody.setAccountValidationKey(accountValidationKey);
		return requestBody;
	}

	@Override
	public ValidateCustomerCredentialsResponseDTO validateCredentials(final String username, final Object credentials)
			throws IntegrationException
	{
		return (ValidateCustomerCredentialsResponseDTO) getLdapRestService()
				.sendRestRequest(createValidateCustomerRequest(username, credentials), ValidateCustomerCredentialsResponseDTO.class,
						BcfintegrationserviceConstants.LDAP_CUSTOMER_LOGIN_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public PasswordResetResponseDTO resetPassword(final String email) throws IntegrationException
	{
		return (PasswordResetResponseDTO) getLdapRestService().sendRestRequest(
				createResetPasswordRequest(email), PasswordResetResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_PASSWORDRESET_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public VerifyPasswordResetResponseDTO verifyPasswordReset(final String key, final String newPassword)
			throws IntegrationException
	{
		final CustomerModel customer = getBcfCustomerService().getCustomerForPasswordRecoveryKey(key);
		if (Objects.isNull(customer))
		{
			throw new UnknownIdentifierException("No customer found with the given password recovery key " + key);
		}

		return (VerifyPasswordResetResponseDTO) getLdapRestService().sendRestRequest(
				createVerifyPasswordResetRequest(customer.getUid(), key, newPassword), VerifyPasswordResetResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_VERIFYPASSWORDRESET_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public PasswordChangeResponseDTO changePassword(final String uid, final String newPassword) throws IntegrationException
	{
		return (PasswordChangeResponseDTO) getLdapRestService().sendRestRequest(
				createChangePasswordResetRequest(uid, newPassword), PasswordChangeResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_CHANGEPASSWORD_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public ChangeEmailResponseDTO changeEmail(final String oldEmail, final String newEmail) throws IntegrationException
	{
		return (ChangeEmailResponseDTO) getLdapRestService().sendRestRequest(
				createChangeEmailResetRequest(oldEmail, newEmail), ChangeEmailResponseDTO.class,
				BcfintegrationserviceConstants.LDAP_CUSTOMER_CHANGEEMAIL_SERVICE_URL, HttpMethod.POST);
	}

	private BaseRequestDTO createChangePasswordResetRequest(final String uid, final String newPassword)
	{
		final ChangePasswordRequestDTO changePasswordRequestDTO = new ChangePasswordRequestDTO();
		changePasswordRequestDTO.setEmail(uid);
		changePasswordRequestDTO.setNewPassword(newPassword);
		return changePasswordRequestDTO;
	}

	private BaseRequestDTO createChangeEmailResetRequest(final String oldEmail, final String newEmail)
	{
		final ChangeEmailRequestDTO changeEmailRequestDTO = new ChangeEmailRequestDTO();
		changeEmailRequestDTO.setExistingEmail(oldEmail);
		changeEmailRequestDTO.setNewEmail(newEmail);
		return changeEmailRequestDTO;
	}

	private VerifyCustumerRequestDTO createVerifyCustomerRequest(final String accountValidationKey, final String email)
	{
		final VerifyCustumerRequestDTO requestBody = new VerifyCustumerRequestDTO();
		requestBody.setAccountValidationKey(accountValidationKey);
		requestBody.setEmail(email);
		return requestBody;
	}

	private CreateCustumerRequestDTO createCustomerRequest(final RegisterData registerData)
	{
		final CreateCustumerRequestDTO body = new CreateCustumerRequestDTO();
		body.setEmail(registerData.getLogin());
		body.setPassword(registerData.getPassword());
		return body;
	}

	private ValidateCustomerRequestDTO createValidateCustomerRequest(final String username, final Object credentials)
	{
		final ValidateCustomerRequestDTO body = new ValidateCustomerRequestDTO();
		body.setEmail(username);
		body.setPassword(String.valueOf(credentials));
		return body;
	}

	private PasswordResetRequestDTO createResetPasswordRequest(final String email)
	{
		final PasswordResetRequestDTO body = new PasswordResetRequestDTO();
		body.setEmail(email);
		return body;
	}

	private VerifyPasswordResetRequestDTO createVerifyPasswordResetRequest(final String uid, final String key,
			final String newPasword)
	{
		final VerifyPasswordResetRequestDTO body = new VerifyPasswordResetRequestDTO();
		body.setEmail(uid);
		body.setPasswordRecoveryKey(key);
		body.setNewPassword(newPasword);
		return body;
	}
	public BaseRestService getLdapRestService()
	{
		return ldapRestService;
	}

	@Required
	public void setLdapRestService(final BaseRestService ldapRestService)
	{
		this.ldapRestService = ldapRestService;
	}

	public BcfCustomerService getBcfCustomerService()
	{
		return bcfCustomerService;
	}

	@Required
	public void setBcfCustomerService(final BcfCustomerService bcfCustomerService)
	{
		this.bcfCustomerService = bcfCustomerService;
	}
}
