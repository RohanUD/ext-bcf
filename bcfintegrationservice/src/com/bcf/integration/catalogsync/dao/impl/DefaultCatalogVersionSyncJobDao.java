/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.catalogsync.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.integration.catalogsync.dao.CatalogVersionSyncJobDao;


public class DefaultCatalogVersionSyncJobDao extends DefaultGenericDao<CatalogVersionSyncJobModel>
		implements CatalogVersionSyncJobDao
{
	public DefaultCatalogVersionSyncJobDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public CatalogVersionSyncJobModel findCatalogVersionSyncJob(final String jobCode)
	{
		validateParameterNotNull(jobCode, "jobCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(CatalogVersionSyncJobModel.CODE, jobCode);
		final List<CatalogVersionSyncJobModel> catalogVersionSyncJobModels = find(params);
		if (CollectionUtils.isNotEmpty(catalogVersionSyncJobModels))
		{
			return catalogVersionSyncJobModels.stream().findFirst().orElse(null);
		}

		return null;
	}

}
