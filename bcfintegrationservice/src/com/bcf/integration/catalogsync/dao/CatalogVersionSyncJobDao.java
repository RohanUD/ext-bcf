/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.catalogsync.dao;

import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;


public interface CatalogVersionSyncJobDao
{

	/**
	 * Find catalog version sync job.
	 *
	 * @param jobCode the job code
	 * @return the catalog version sync job model
	 */
	CatalogVersionSyncJobModel findCatalogVersionSyncJob(String jobCode);
}
