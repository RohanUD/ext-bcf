/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.catalogsync.service.impl;

import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.catalogsync.dao.CatalogVersionSyncJobDao;
import com.bcf.integration.catalogsync.service.CatalogVersionSyncJobService;


public class DefaultCatalogVersionSyncJobService implements CatalogVersionSyncJobService
{
	private CatalogVersionSyncJobDao catalogVersionSyncJobDao;

	@Override
	public CatalogVersionSyncJobModel getCatalogVersionSyncJob(final String jobCode)
	{
		return getCatalogVersionSyncJobDao().findCatalogVersionSyncJob(jobCode);
	}

	/**
	 * @return the catalogVersionSyncJobDao
	 */
	protected CatalogVersionSyncJobDao getCatalogVersionSyncJobDao()
	{
		return catalogVersionSyncJobDao;
	}

	/**
	 * @param catalogVersionSyncJobDao the catalogVersionSyncJobDao to set
	 */
	@Required
	public void setCatalogVersionSyncJobDao(final CatalogVersionSyncJobDao catalogVersionSyncJobDao)
	{
		this.catalogVersionSyncJobDao = catalogVersionSyncJobDao;
	}

}
