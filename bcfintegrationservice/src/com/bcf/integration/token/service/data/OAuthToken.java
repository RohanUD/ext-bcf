/*
 * BCF - British Columbia Ferries.
 *
 * Copyright (c) 2019.  All rights reserved.
 *
 * This software is the confidential and proprietary information of BCF
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with BCF.
 */
package com.bcf.integration.token.service.data;

import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;
import com.bcf.integration.constants.BcfintegrationserviceConstants;


/**
 * This is specialized class, responsible to hold the
 * static data regarding the token fetched by the
 * external party.
 *
 * @author nbilal
 */
public class OAuthToken
{

	private static String tokenValue;

	private static long expiryTime;

	private static long BUFFER_TIME_IN_SEC_BEFORE_TOKEN_EXPIRY = Config
			.getLong(BcfintegrationserviceConstants.BUFFER_TIME_IN_SEC_BEFORE_TOKEN_EXPIRY, 500);

	private static String systemIdentifier;

	private OAuthToken()
	{
		//Utility classes, which are a collection of static members, are not meant to be instantiated
	}

	private static final Logger LOG = Logger.getLogger(OAuthToken.class.getName());

	public static String getTokenValue()
	{
		return tokenValue;
	}

	public static void setTokenValue(final String tokenValue)
	{
		OAuthToken.tokenValue = tokenValue;
	}

	public static long getExpiryTime()
	{
		return expiryTime;
	}

	public static void setExpiryTime(final long expiryTime)
	{
		OAuthToken.expiryTime = expiryTime;
	}

	/**
	 * In the multithreaded environment, we will eagerly renew the token.
	 *
	 * @param tokenExpiresInSeconds
	 * @return
	 */
	public static long getExpiryOffsetTimeMillis(final long tokenExpiresInSeconds)
	{
		final long secondsBeforeRenew = tokenExpiresInSeconds - BUFFER_TIME_IN_SEC_BEFORE_TOKEN_EXPIRY;
		final long milliSecondsBeforeRenew = 1000 * secondsBeforeRenew;
		return System.currentTimeMillis() + milliSecondsBeforeRenew;
	}

	public static String getValidToken()
	{
		// Both times are subjected to change so snapshot in the local vars.
		final long currentTimeMillis = System.currentTimeMillis();
		final long expiryTime = OAuthToken.getExpiryTime();

		if (expiryTime > currentTimeMillis)
		{
			LOG.debug("The token has not expired yet using this token, " + OAuthToken.getTokenValue());
			return OAuthToken.getTokenValue();
		}
		else
		{
			LOG.debug("The token has nearly expired (ms) ," + expiryTime + " and current time is (ms) ," + currentTimeMillis
					+ " going to fetch the new one");
		}

		return null;
	}

	public static String getSystemIdentifier()
	{
		return systemIdentifier;
	}

	public static void setSystemIdentifier(final String systemIdentifier)
	{
		OAuthToken.systemIdentifier = systemIdentifier;
	}

}
