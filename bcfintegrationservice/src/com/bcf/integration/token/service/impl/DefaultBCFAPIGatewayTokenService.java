/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.token.service.impl;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.API_GATEWAY_TOKEN_CACHE_SERVICE_ENABLE;
import static com.bcf.integration.constants.BcfintegrationserviceConstants.SCOPE_PREFIX;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.GetTokenResponseDTO;
import com.bcf.integration.token.service.BCFAPIGatewayTokenService;
import com.bcf.integration.token.service.data.OAuthToken;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBCFAPIGatewayTokenService implements BCFAPIGatewayTokenService
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(DefaultBCFAPIGatewayTokenService.class.getName());

	private BaseRestService tokenRestService;
	private ModelService modelService;
	private ConfigurationService configurationService;

	@Override
	public String getOAuth2Token() throws IntegrationException
	{
		return getOrFetchToken();
	}

	protected String getOrFetchToken() throws IntegrationException
	{

		final String validToken = OAuthToken.getValidToken();

		if (validToken == null)
		{
			final Boolean cacheServiceEnable = getConfigurationService().getConfiguration()
					.getBoolean(API_GATEWAY_TOKEN_CACHE_SERVICE_ENABLE);

			// After the first network call which will keep the lock for slightly
			// longer period of time, then rest lock release will be relatively quick.
			synchronized (this)
			{
				//Check if token caching required.
				if (cacheServiceEnable)
				{
					return getTokenFromRemoteWithCaching();
				}
				else
				{
					return getTokenFromRemote();
				}
			}
		}
		else
		{
			return validToken;
		}
	}

	protected String getTokenFromRemote() throws IntegrationException
	{
		// There is a possibility that, in a concurrent situation first thread called the token service
		// and obtained the token, however the subsequent threads try to perform the same action but
		// getValidToken() call will act as a safety for them, as after the first network token call,
		// getValidToken() will not be null.
		LOG.debug("Entered to get the new token");
		final Map<String, Object> requestParams = new HashMap<>();
		requestParams.put(BcfintegrationserviceConstants.GRANT_TYPE_KEY, BcfintegrationserviceConstants.GRANT_TYPE_VALUE);
		final GetTokenResponseDTO tokenResponseDTO = (GetTokenResponseDTO) getTokenRestService().sendRestRequest(
				null, GetTokenResponseDTO.class, HttpMethod.POST,
				BcfintegrationserviceConstants.API_GATEWAY_TOKEN_SERVICE_URL, getHeaders(), requestParams);

		LOG.debug("Exiting with the new token ," + OAuthToken.getTokenValue() + " and offset expiry time (ms) ," + OAuthToken
				.getExpiryTime());

		return tokenResponseDTO.getAccess_token();
	}

	protected String getTokenFromRemoteWithCaching() throws IntegrationException
	{
		final String validToken = OAuthToken.getValidToken();

		// There is a possibility that, in a concurrent situation first thread called the token service
		// and obtained the token, however the subsequent threads try to perform the same action but
		// getValidToken() call will act as a safety for them, as after the first network token call,
		// getValidToken() will not be null.
		if (validToken == null)
		{
			synchronized (this)
			{
				LOG.debug("Entered to get the new token");
				final Map<String, Object> requestParams = new HashMap<>();
				requestParams.put(BcfintegrationserviceConstants.GRANT_TYPE_KEY, BcfintegrationserviceConstants.GRANT_TYPE_VALUE);
				requestParams.put(BcfintegrationserviceConstants.SCOPE, getScopeValue());
				final GetTokenResponseDTO tokenResponseDTO = (GetTokenResponseDTO) getTokenRestService().sendRestRequest(
						null, GetTokenResponseDTO.class, HttpMethod.POST,
						BcfintegrationserviceConstants.API_GATEWAY_TOKEN_SERVICE_URL, getHeaders(), requestParams);

				OAuthToken.setTokenValue(tokenResponseDTO.getAccess_token());
				OAuthToken.setExpiryTime(OAuthToken.getExpiryOffsetTimeMillis(tokenResponseDTO.getExpires_in()));

				//To validate the request scope and respons token. Will be changed to debug mode once tested/checked
				LOG.info("Exiting with the new token ," + OAuthToken.getTokenValue() + " and offset expiry time (ms) ," + OAuthToken
						.getExpiryTime() + "for Scope: " + getScopeValue());

				// Wake up all the waiting threads as fresh token just arrived.
				this.notifyAll();
			}
		}

		return OAuthToken.getTokenValue();
	}

	private String getScopeValue()
	{
		if (Objects.isNull(OAuthToken.getSystemIdentifier()))
		{
			OAuthToken.setSystemIdentifier(SCOPE_PREFIX + UUID.randomUUID().toString());
		}
		return OAuthToken.getSystemIdentifier();
	}


	private HttpHeaders getHeaders()
	{
		final HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		final String consumerKey = getConfigurationService().getConfiguration()
				.getString(BcfintegrationserviceConstants.CONSUMER_KEY);
		final String consumerSecret = getConfigurationService().getConfiguration()
				.getString(BcfintegrationserviceConstants.CONSUMER_SECRET);
		final String encodedConsumerDetails = Base64.getEncoder()
				.encodeToString((consumerKey + ":" + consumerSecret).getBytes(StandardCharsets.UTF_8));
		requestHeaders
				.set(BcfintegrationserviceConstants.AUTHORIZATION, BcfintegrationserviceConstants.BASIC + encodedConsumerDetails);
		return requestHeaders;
	}

	protected BaseRestService getTokenRestService()
	{
		return tokenRestService;
	}

	@Required
	public void setTokenRestService(final BaseRestService tokenRestService)
	{
		this.tokenRestService = tokenRestService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
