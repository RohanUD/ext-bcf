/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.url;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.servicelayer.i18n.I18NService;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;


public class DefaultDealBundleDataModelUrlResolver extends AbstractUrlResolver<DealsBundleDataModel>
{
	public static final String BUNDLE_ID = "{bundle-id}";
	public static final String BUNDLE_NAME = "{bundle-name}";
	private I18NService i18NService;
	private String defaultPattern;

	@Override
	protected String resolveInternal(final DealsBundleDataModel source)
	{

		String url = getDefaultPattern();

		if (url.contains(BUNDLE_ID))
		{
			url = url.replace(BUNDLE_ID, urlSafe(source.getBundleId()));
		}
		if (url.contains(BUNDLE_NAME))
		{
			url = url.replace(BUNDLE_NAME, urlEncode(source.getBundleName(getI18NService().getCurrentLocale())));
		}
		return url;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	public String getDefaultPattern()
	{
		return defaultPattern;
	}

	public void setDefaultPattern(final String defaultPattern)
	{
		this.defaultPattern = defaultPattern;
	}

}
