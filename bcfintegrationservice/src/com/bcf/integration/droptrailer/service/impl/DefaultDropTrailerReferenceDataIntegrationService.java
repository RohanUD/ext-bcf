/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.droptrailer.service.impl;

import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.droptrailer.service.DropTrailerReferenceDataIntegrationService;
import com.bcf.integration.droptrailer.trailermovement.data.EventTypesResponseData;
import com.bcf.integration.droptrailer.trailermovement.data.TrailerMovementLocationsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultDropTrailerReferenceDataIntegrationService implements DropTrailerReferenceDataIntegrationService
{

	private BaseRestService dropTrailerRestService;

	@Override
	public EventTypesResponseData getEventTypes() throws IntegrationException
	{
		EventTypesResponseData eventTypesResponseData = null;

		eventTypesResponseData = (EventTypesResponseData) getDropTrailerRestService()
				.sendRestRequest(null, EventTypesResponseData.class,
						BcfintegrationserviceConstants.EVENT_TYPES_URL,
						HttpMethod.GET);

		return eventTypesResponseData;
	}

	@Override
	public TrailerMovementLocationsResponseData getTrailerMovementLocations() throws IntegrationException
	{

		TrailerMovementLocationsResponseData trailerMovementLocationsResponseData = null;

		trailerMovementLocationsResponseData = (TrailerMovementLocationsResponseData) getDropTrailerRestService()
				.sendRestRequest(null, TrailerMovementLocationsResponseData.class,
						BcfintegrationserviceConstants.TRAILER_MOVEMENT_LOCATIONS_URL,
						HttpMethod.GET);

		return trailerMovementLocationsResponseData;
	}


	public BaseRestService getDropTrailerRestService()
	{
		return dropTrailerRestService;
	}

	public void setDropTrailerRestService(final BaseRestService dropTrailerRestService)
	{
		this.dropTrailerRestService = dropTrailerRestService;
	}
}
