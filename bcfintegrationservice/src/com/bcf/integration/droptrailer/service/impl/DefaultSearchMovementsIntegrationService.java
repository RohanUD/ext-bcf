/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.droptrailer.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.droptrailer.search.data.SearchMovementsResponseData;
import com.bcf.integration.droptrailer.service.SearchMovementsIntegrationService;
import com.bcf.integration.droptrailer.trailermovement.data.SearchMovementsRequestData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultSearchMovementsIntegrationService implements SearchMovementsIntegrationService
{

	private static final String CUSTOMER_ID = "customerID";
	private static final String DEPARTURE_TERM = "deptTerm";
	private static final String DESTINATION_TERM = "destTerm";
	private static final String EVENT_TYPE = "eventType";
	private static final String FROM_SAILING_DATE = "fromSailingDate";
	private static final String TO_SAILING_DATE = "toSailingDate";
	private static final String INCLUDE_OLD_SHIPMENTS = "includeOldShipments";
	private static final String TRAILER_ID = "trailerID";


	private BaseRestService dropTrailerRestService;

	@Override
	public SearchMovementsResponseData searchMovements(final SearchMovementsRequestData searchMovementsRequestData)
			throws IntegrationException
	{
		SearchMovementsResponseData searchMovementsResponseData = null;

		if (Objects.nonNull(searchMovementsRequestData.getCustomerId()))
		{

			searchMovementsResponseData = (SearchMovementsResponseData) getDropTrailerRestService()
					.sendRestRequest(null, SearchMovementsResponseData.class,
							BcfintegrationserviceConstants.SEARCH_MOVEMENTS_URL,
							HttpMethod.GET, getParamMap(searchMovementsRequestData));
		}
		return searchMovementsResponseData;

	}

	private Map<String, Object> getParamMap(final SearchMovementsRequestData searchMovementsRequestData)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		keyMap.computeIfAbsent(CUSTOMER_ID, val -> searchMovementsRequestData.getCustomerId());
		keyMap.computeIfAbsent(DEPARTURE_TERM, val -> searchMovementsRequestData.getDeptTerm());
		keyMap.computeIfAbsent(DESTINATION_TERM, val -> searchMovementsRequestData.getDestTerm());
		keyMap.computeIfAbsent(EVENT_TYPE, val -> searchMovementsRequestData.getEventType());
		keyMap.computeIfAbsent(FROM_SAILING_DATE, val -> Objects.nonNull(searchMovementsRequestData.getFromSailingDate()) ?
				searchMovementsRequestData.getFromSailingDate().toString() : null);
		keyMap.computeIfAbsent(TO_SAILING_DATE, val -> Objects.nonNull(searchMovementsRequestData.getFromSailingDate()) ?
				searchMovementsRequestData.getFromSailingDate().toString() : null);
		keyMap.computeIfAbsent(INCLUDE_OLD_SHIPMENTS, val -> searchMovementsRequestData.isIncludeOldShipments() ?
				String.valueOf(searchMovementsRequestData.isIncludeOldShipments()) : null);
		keyMap.computeIfAbsent(TRAILER_ID, val -> searchMovementsRequestData.getTrailerID());
		return keyMap;
	}

	public BaseRestService getDropTrailerRestService()
	{
		return dropTrailerRestService;
	}

	public void setDropTrailerRestService(final BaseRestService dropTrailerRestService)
	{
		this.dropTrailerRestService = dropTrailerRestService;
	}
}
