/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.tripadvisor.service.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.tripadvisor.service.TripAdvisorService;
import com.bcf.integration.tripadvisorResponse.data.TripAdvisorResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultTripAdvisorService implements TripAdvisorService
{
	public static final String TRIPADVISOR_CACHE_KEY_STRING = "cache-key";
	public static final String TRIPADVISOR_LOCATION_ID = "locationId";
	public static final String TRIPADVISOR_LANGUAGE = "lang";

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(DefaultTripAdvisorService.class);

	private BaseRestService tripAdvisorRestService;
	private ConfigurationService configurationService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private String tripAdvisorApiKey;
	private CommerceCommonI18NService commerceCommonI18NService;
	private Map<String, String> tripAdvisorlangMap;


	@Override
	public TripAdvisorResponseData getReviewInformation(final String locationId) throws IntegrationException
	{
		TripAdvisorResponseData tripAdvisorResponseData = null;

		if (Objects.nonNull(locationId) && Boolean
				.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue("enableTripAdvisor")))
		{
			final HttpHeaders requestHeader = new HttpHeaders();
			requestHeader.add(BcfintegrationserviceConstants.TRIPADVISOR_API_REQUEST_HEADER_NAME,
					getTripAdvisorApiKey());
			tripAdvisorResponseData = (TripAdvisorResponseData) getTripAdvisorRestService()
					.sendRestRequest(null, TripAdvisorResponseData.class, HttpMethod.GET,
							BcfintegrationserviceConstants.TRIPADVISOR_LOCATION_DETAILS_URL
							, requestHeader, getParamMap(locationId));
		}
		return tripAdvisorResponseData;
	}

	private Map<String, Object> getParamMap(final String locationId)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		keyMap.put(TRIPADVISOR_CACHE_KEY_STRING, getCacheKey());
		keyMap.put(TRIPADVISOR_LOCATION_ID, locationId);
		keyMap.put(TRIPADVISOR_LANGUAGE,
				getTripAdvisorlangMap().get(getCommerceCommonI18NService().getCurrentLanguage().getIsocode()));
		return keyMap;
	}

	private String getCacheKey()
	{
		final String key = getTripAdvisorApiKey();
		final MessageDigest messageDigest;
		try
		{
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(key.getBytes());
			final byte[] digest = messageDigest.digest();
			final StringBuilder stringBuilder = new StringBuilder();
			for (final byte byteVar : digest)
			{
				stringBuilder.append(String.format("%02X", byteVar));
			}
			return stringBuilder.toString();

		}
		catch (final NoSuchAlgorithmException e)
		{
			LOG.error(e);
		}
		return null;
	}

	public BaseRestService getTripAdvisorRestService()
	{
		return tripAdvisorRestService;
	}

	@Required
	public void setTripAdvisorRestService(final BaseRestService tripAdvisorRestService)
	{
		this.tripAdvisorRestService = tripAdvisorRestService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public String getTripAdvisorApiKey()
	{
		return tripAdvisorApiKey;
	}

	@Required
	public void setTripAdvisorApiKey(final String tripAdvisorApiKey)
	{
		this.tripAdvisorApiKey = tripAdvisorApiKey;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public Map<String, String> getTripAdvisorlangMap()
	{
		return tripAdvisorlangMap;
	}

	@Required
	public void setTripAdvisorlangMap(final Map<String, String> tripAdvisorlangMap)
	{
		this.tripAdvisorlangMap = tripAdvisorlangMap;
	}

}
