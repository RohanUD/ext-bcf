/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.common.BookingReferences;


public class BcfBookingReferencesCancelBookingPopulator implements Populator<OrderModel, CancelBookingRequestForOrder>
{
	@Override
	public void populate(final OrderModel source, final CancelBookingRequestForOrder target) throws ConversionException
	{
		final List<BookingReferences> bookingReferences = new ArrayList<>();
		source.getEntries().stream().filter(entry -> entry.getActive() && Objects.equals(entry.getType(), OrderEntryType.TRANSPORT))
				.map(AbstractOrderEntryModel::getBookingReference).distinct().forEach(entryBookingReference -> {
			final BookingReferences bookingReference = new BookingReferences();
			bookingReference.setBookingReference(entryBookingReference);
			bookingReferences.add(bookingReference);
		});

		target.setBookingReferences(bookingReferences);
	}

}
