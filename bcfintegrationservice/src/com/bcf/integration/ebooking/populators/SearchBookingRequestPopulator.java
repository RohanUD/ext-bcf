/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.integration.data.SearchBookingRequestDTO;
import com.bcf.integration.data.SearchCriteria;
import com.bcf.integrations.booking.common.BookingReferences;
import com.bcf.integrations.booking.request.SailingEvent;
import com.bcf.integrations.common.BookingClientSystem;


public class SearchBookingRequestPopulator
{
	private IntegrationUtility integrationUtils;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	public SearchBookingRequestDTO getRequestBody(final CustomerModel customerModel, final Integer currentPageNumber,
			final Boolean isUpcomingBooking)
	{
		final SearchBookingRequestDTO request = new SearchBookingRequestDTO();
		request.setBookingClientSystem(createBookingClient());
		request.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo(customerModel));
		request.setSearchCriteria(createSearchCriteria(currentPageNumber, isUpcomingBooking));
		request.setOverrideHidePricing(true);
		return request;
	}

	public SearchBookingRequestDTO getRequestBody(final CustomerModel customerModel,
			final List<String> bookingReferencesList, final Integer currentPageNumber, final Boolean isUpcomingBooking)
	{
		final SearchBookingRequestDTO request = new SearchBookingRequestDTO();
		request.setBookingClientSystem(createBookingClient());
		request.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo(customerModel));
		request.setSearchCriteria(createSearchCriteria(currentPageNumber, isUpcomingBooking));
		request.getSearchCriteria().setBookingReference(createBookingReferences(bookingReferencesList));
		request.setOverrideHidePricing(true);
		return request;
	}

	public SearchBookingRequestDTO getAdvanceSearchRequestBody(final CustomerModel customerModel, final Integer currentPageNumber,
			final BookingsAdvanceSearchData bookingsAdvanceSearchData)
	{
		final SearchBookingRequestDTO request = new SearchBookingRequestDTO();
		request.setBookingClientSystem(createBookingClient());
		request.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo(customerModel));
		request.setSearchCriteria(createBookingsAdvanceSearchCriteria(currentPageNumber, bookingsAdvanceSearchData));
		request.setOverrideHidePricing(true);
		return request;
	}

	private List<BookingReferences> createBookingReferences(final List<String> bookingReferencesList)
	{
		if (Objects.isNull(bookingReferencesList) || bookingReferencesList.isEmpty())
		{
			return Collections.emptyList();
		}

		final List<BookingReferences> bookingReferences = new ArrayList<>();

		for (final String bookingRef : bookingReferencesList)
		{
			final BookingReferences newReference = new BookingReferences();
			newReference.setBookingReference(bookingRef);
			bookingReferences.add(newReference);
		}
		return bookingReferences;
	}

	private BookingClientSystem createBookingClient()
	{
		final BookingClientSystem bookingClient = new BookingClientSystem();
		bookingClient
				.setClientCode(getIntegrationUtils().getClientCode(getBcfSalesApplicationResolverService().getCurrentSalesChannel(),
						BookingJourneyType.BOOKING_TRANSPORT_ONLY));
		return bookingClient;
	}

	private SearchCriteria createSearchCriteria(final Integer currentPageNumber, final Boolean isUpcomingBooking)
	{
		final SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setCurrentPageNumber(currentPageNumber);

		if (Objects.nonNull(isUpcomingBooking))
		{
			if (isUpcomingBooking)
			{
				searchCriteria.setDepartureDateTimeFrom(
						TravelDateUtils.getTimeForDate(new Date(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			}
			else
			{
				searchCriteria.setDepartureDateTimeTo(
						TravelDateUtils.getTimeForDate(new Date(), BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			}
		}
		return searchCriteria;
	}

	private SearchCriteria createBookingsAdvanceSearchCriteria(final Integer currentPageNumber,
			final BookingsAdvanceSearchData bookingsAdvanceSearchData)
	{
		final SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setCurrentPageNumber(currentPageNumber);

		if (StringUtils.isNotEmpty(bookingsAdvanceSearchData.getBookingReference()))
		{
			final BookingReferences bookingReferences = new BookingReferences();
			bookingReferences.setBookingReference(bookingsAdvanceSearchData.getBookingReference());
			searchCriteria.setBookingReference(Arrays.asList(bookingReferences));
		}
		if (StringUtils.isNotEmpty(bookingsAdvanceSearchData.getDeparturePortCode()) || StringUtils
				.isNotEmpty(bookingsAdvanceSearchData.getArrivalPortCode()))
		{
			final SailingEvent sailingEvent = new SailingEvent();
			sailingEvent.setDeparturePortCode(bookingsAdvanceSearchData.getDeparturePortCode());
			sailingEvent.setArrivalPortCode(bookingsAdvanceSearchData.getArrivalPortCode());
			searchCriteria.setRoutes(Arrays.asList(sailingEvent));
		}
		final Date departureDateTimeFrom = getAdvanceSearchDate(bookingsAdvanceSearchData.getDepartureDateTimeFrom());
		final Date departureDateTimeTo = getAdvanceSearchDate(bookingsAdvanceSearchData.getDepartureDateTimeTo());

		if (Objects.nonNull(departureDateTimeFrom) && Objects.nonNull(departureDateTimeTo))
		{
			searchCriteria.setDepartureDateTimeFrom(
					TravelDateUtils.getTimeForDate(departureDateTimeFrom, BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
			searchCriteria.setDepartureDateTimeTo(
					TravelDateUtils.getTimeForDate(departureDateTimeTo, BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN));
		}
		return searchCriteria;
	}

	private Date getAdvanceSearchDate(final String date)
	{
		try
		{
			return new SimpleDateFormat(BcfintegrationcoreConstants.BOOKINGS_ADVANCE_SEARCH_DATE_FORMAT).parse(date);
		}
		catch (final ParseException var4)
		{
			return null;
		}
	}

	public IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}
}
