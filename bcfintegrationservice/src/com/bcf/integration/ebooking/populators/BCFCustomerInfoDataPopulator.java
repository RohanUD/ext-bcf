/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integration.data.ConfirmBookingsRequest;



public class BCFCustomerInfoDataPopulator implements Populator<AbstractOrderModel, ConfirmBookingsRequest>
{
	private IntegrationUtility integrationUtils;

	@Override
	public void populate(final AbstractOrderModel source, final ConfirmBookingsRequest target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final CustomerModel customermodel = (CustomerModel) source.getUser();
			target.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo(customermodel));

		}
	}

	public IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}
}
