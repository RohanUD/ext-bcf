/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;


public class CustomerInfoFullCancelBookingPopulator implements Populator<OrderModel, CancelBookingRequestForOrder>
{
	private BCFTravelCartService bcfTravelCartService;
	private UserService userService;
	private IntegrationUtility integrationUtility;

	@Override
	public void populate(final OrderModel source, final CancelBookingRequestForOrder target) throws ConversionException
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
		target.setCustomerInfo(getIntegrationUtility().createCustomerInfo(customerModel));
		target.setNotes(BcfintegrationserviceConstants.NOTE_DELIMITER + customerModel.getName() + BcfintegrationserviceConstants.
				NOTE_DELIMITER + BcfintegrationserviceConstants.CANCELLING_BOOKING_NOTE);
	}
	/**
	 * @return the bcfTravelCartService
	 */
	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public IntegrationUtility getIntegrationUtility()
	{
		return integrationUtility;
	}

	public void setIntegrationUtility(final IntegrationUtility integrationUtility)
	{
		this.integrationUtility = integrationUtility;
	}
}
