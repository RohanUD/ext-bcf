/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.PaymentInfoResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Map;


public class BCFPaymentInfoResultPopulator extends PaymentInfoResultPopulator
{
	@Override
	public void populate(final Map<String, String> source, final CreateSubscriptionResult target) throws ConversionException
	{
		super.populate(source, target);
	}
}
