/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators.listsailing.response;

import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.ArrayList;
import java.util.List;
import com.bcf.integration.listSailingResponse.data.LineData;
import com.bcf.integration.listSailingResponse.data.ListSailingsResponseData;


public class FareSelectionBasicDataPopulator implements Populator<ListSailingsResponseData, FareSelectionData>
{

	@Override
	public void populate(final ListSailingsResponseData source, final FareSelectionData target) throws ConversionException
	{

		final List<PricedItineraryData> pricedItineraryDataList = new ArrayList<>();
		source.getSailing().forEach(sailing -> {

			final PricedItineraryData pricedItineraryData = new PricedItineraryData();
			pricedItineraryData.setAvailable(sailing.getLine().stream().allMatch(LineData::getIsBookable));

			pricedItineraryDataList.add(pricedItineraryData);
			target.setPricedItineraries(pricedItineraryDataList);


		});
	}


}
