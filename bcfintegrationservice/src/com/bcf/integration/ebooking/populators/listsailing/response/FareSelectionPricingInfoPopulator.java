/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators.listsailing.response;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.integration.listSailingResponse.data.ListSailingPriceData;
import com.bcf.integration.listSailingResponse.data.ListSailingsResponseData;
import com.bcf.integration.listSailingResponse.data.SailingData;


public class FareSelectionPricingInfoPopulator implements Populator<ListSailingsResponseData, FareSelectionData>
{
	private String currencyIsoCode;

	@Override
	public void populate(final ListSailingsResponseData source, final FareSelectionData target) throws ConversionException
	{
		final List<SailingData> sailings = source.getSailing();

		for (int i = 0; i < sailings.size(); i++)
		{
			final List<ItineraryPricingInfoData> itineraryPricingInfoDataList = new ArrayList<>();
			if (Objects.nonNull(sailings.get(i).getPrices()))
			{
				final List<ListSailingPriceData> prices = sailings.get(i).getPrices();
				prices.forEach(price -> populateItineraryPricingInfos(price, itineraryPricingInfoDataList));
				target.getPricedItineraries().get(i).setItineraryPricingInfos(itineraryPricingInfoDataList);
			}
		}

	}

	private void populateItineraryPricingInfos(final ListSailingPriceData price,
			final List<ItineraryPricingInfoData> itineraryPricingInfoDataList)
	{
		final ItineraryPricingInfoData itineraryPricingInfo = new ItineraryPricingInfoData();
		final List<TravelBundleTemplateData> bundleTempateList = new ArrayList<>();
		final TravelBundleTemplateData bundleTemplate = new TravelBundleTemplateData();
		bundleTemplate.setBundleType(getBundleType(price));
		bundleTempateList.add(bundleTemplate);

		itineraryPricingInfo.setTotalFare(getTotalFare(price));
		//check availability by identifying whether a total fare is there or not
		itineraryPricingInfo.setAvailable(Objects.nonNull(itineraryPricingInfo.getTotalFare()));
		itineraryPricingInfo.setBundleTemplates(bundleTempateList);

		itineraryPricingInfoDataList.add(itineraryPricingInfo);
	}

	private String getBundleType(final ListSailingPriceData price)
	{
		return price.getType();
	}

	private TotalFareData getTotalFare(final ListSailingPriceData price)
	{
		final TotalFareData totalFare = new TotalFareData();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso(currencyIsoCode);
		priceData.setValue(BigDecimal.valueOf(price.getAmount()));
		totalFare.setTotalPrice(priceData);
		return totalFare;
	}


	public String getCurrencyIsoCode()
	{
		return currencyIsoCode;
	}

	public void setCurrencyIsoCode(final String currencyIsoCode)
	{
		this.currencyIsoCode = currencyIsoCode;
	}

}
