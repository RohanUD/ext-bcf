/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators.listsailing.response;

import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.List;
import com.bcf.integration.listSailingResponse.data.LineData;
import com.bcf.integration.listSailingResponse.data.ListSailingsResponseData;
import com.bcf.integration.listSailingResponse.data.SailingData;


public class FareSelectionItineraryDataPopulator implements Populator<ListSailingsResponseData, FareSelectionData>
{

	@Override
	public void populate(final ListSailingsResponseData source, final FareSelectionData target) throws ConversionException
	{

		final List<SailingData> sailings = source.getSailing();

		for (int i = 0; i < sailings.size(); i++)
		{
			final List<LineData> lineList = sailings.get(i).getLine();
			final ItineraryData itineraryData = new ItineraryData();
			final TravelRouteData travelRouteData = new TravelRouteData();
			final TransportFacilityData origin = new TransportFacilityData();
			origin.setCode(lineList.get(0).getDeparturePortCode());

			final TransportFacilityData destination = new TransportFacilityData();
			destination.setCode(lineList.get(lineList.size() - 1).getArrivalPortCode());

			travelRouteData.setDestination(destination);
			travelRouteData.setOrigin(origin);

			itineraryData.setRoute(travelRouteData);
			target.getPricedItineraries().get(i).setItinerary(itineraryData);
		}

	}

}
