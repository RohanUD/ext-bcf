/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators.listsailing.response;

import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.OriginDestinationOptionData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TransportVehicleData;
import de.hybris.platform.commercefacades.travel.TransportVehicleInfoData;
import de.hybris.platform.commercefacades.travel.TravelSectorData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.integration.listSailingResponse.data.LineData;
import com.bcf.integration.listSailingResponse.data.ListSailingsResponseData;
import com.bcf.integration.listSailingResponse.data.SailingData;


public class FareSelectionOriginDestinationPopulator implements Populator<ListSailingsResponseData, FareSelectionData>
{
	private static final Logger LOG = Logger.getLogger(FareSelectionOriginDestinationPopulator.class);

	@Override
	public void populate(final ListSailingsResponseData source, final FareSelectionData target) throws ConversionException
	{

		final List<SailingData> sailings = source.getSailing();

		for (int i = 0; i < sailings.size(); i++)
		{
			final List<LineData> lineList = sailings.get(i).getLine();

			final List<OriginDestinationOptionData> originDestinationOptionDataList = new ArrayList<>();

			lineList.forEach(line -> {
				final OriginDestinationOptionData originDestinationOptionData = new OriginDestinationOptionData();
				final List<TransportOfferingData> transportOfferingDataList = new ArrayList<>();

				final TransportOfferingData transportOfferingData = new TransportOfferingData();
				final TravelSectorData travelSectorData = new TravelSectorData();
				final TransportFacilityData origin = new TransportFacilityData();
				origin.setCode(line.getDeparturePortCode());
				final TransportFacilityData destination = new TransportFacilityData();
				destination.setCode(line.getArrivalPortCode());
				travelSectorData.setOrigin(origin);
				travelSectorData.setDestination(destination);
				transportOfferingData.setSector(travelSectorData);

				final TransportVehicleInfoData transportVehicleInfoData = new TransportVehicleInfoData();
				transportVehicleInfoData.setName(line.getVesselCode());

				final TransportVehicleData transportVehicleData = new TransportVehicleData();

				transportVehicleData.setVehicleInfo(transportVehicleInfoData);
				transportOfferingData.setTransportVehicle(transportVehicleData);

				final SimpleDateFormat dateFormat = new SimpleDateFormat(BcfintegrationcoreConstants.BCF_DATE_TIME_PATTERN);
				try
				{
					transportOfferingData.setArrivalTime(dateFormat.parse(line.getArrivalDateTime()));
					transportOfferingData.setDepartureTime(dateFormat.parse(line.getDepartureDateTime()));

					final long diff = transportOfferingData.getArrivalTime().getTime()
							- transportOfferingData.getDepartureTime().getTime();
					transportOfferingData.setDurationValue(diff);
				}
				catch (final ParseException e)
				{
					LOG.error("Parse exception " + e);
				}

				transportOfferingDataList.add(transportOfferingData);
				originDestinationOptionData.setTransportOfferings(transportOfferingDataList);
				originDestinationOptionDataList.add(originDestinationOptionData);
			});
			target.getPricedItineraries().get(i).getItinerary().setOriginDestinationOptions(originDestinationOptionDataList);
		}

	}

}
