/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators.listsailing.request;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.integration.listSailingRequest.data.ListSailingsRequestData;
import com.bcf.integration.listSailingRequest.data.SailingEventData;


public class OriginDestinationRequestPopulator implements Populator<FareSearchRequestData, ListSailingsRequestData>
{

	@Override
	public void populate(final FareSearchRequestData source, final ListSailingsRequestData target) throws ConversionException
	{
		final DateFormat dateFormat = new SimpleDateFormat(BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN);
		final SailingEventData sailingEventData = new SailingEventData();
		sailingEventData.setArrivalPortCode(source.getOriginDestinationInfo().get(0).getArrivalLocation());
		sailingEventData.setDepartureDateTime(dateFormat.format(source.getOriginDestinationInfo().get(0).getDepartureTime()));
		sailingEventData.setDeparturePortCode(source.getOriginDestinationInfo().get(0).getDepartureLocation());
		target.setSailingEvent(sailingEventData);
	}

}
