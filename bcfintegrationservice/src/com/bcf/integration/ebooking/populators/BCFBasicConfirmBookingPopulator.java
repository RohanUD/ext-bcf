/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseSubCategoriesPopulator;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integrations.common.BookingClientSystem;
import com.bcf.integrations.common.BookingReferences;
import com.bcf.integrations.common.BookingType;



public class BCFBasicConfirmBookingPopulator implements Populator<AbstractOrderModel, ConfirmBookingsRequest>
{

	private ConfigurationService configurationService;
	private BCFTravelCartService bcfTravelCartService;
	private UserService userService;
	private BCFTravelCartService cartService;
	private IntegrationUtility integrationUtils;

	@Override
	public void populate(final AbstractOrderModel source, final ConfirmBookingsRequest target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final BookingClientSystem bookingClientSystem = new BookingClientSystem();

			bookingClientSystem.setClientCode(getIntegrationUtils().getClientCode(((CartModel) source).getSalesApplication()));
			target.setBookingClientSystem(bookingClientSystem);
			final BookingType bookingType = new BookingType();

			final UserModel user = getUserService().getCurrentUser();
			final boolean isAnonymous = getUserService().isAnonymousUser(user) || bcfTravelCartService
					.isGuestUserCart(bcfTravelCartService.getSessionCart());
			final boolean isAmendmentCart = getCartService().isAmendmentCart(source);
			bookingType.setBookingType(
					integrationUtils.getBookingType(isAnonymous, isAmendmentCart, bcfTravelCartService.isAlacateFlow()));
			target.setBookingType(bookingType);
			final List<BookingReferences> tempBookingReference = createTempBookingReferences(source);
			target.setTempBookingReferences(tempBookingReference.isEmpty() ? null : tempBookingReference);
			target.setIsAllowChangesAtPOS(source.isIsAllowChangesAtPOS());
			target.setSendNotification(false);
		}
	}

	protected List<BookingReferences> createTempBookingReferences(final AbstractOrderModel source)
	{
		if (CollectionUtils.isEmpty(source.getEntries()))
		{
			return Collections.emptyList();
		}
		final List<BookingReferences> tempBookingReferences = new ArrayList<BookingReferences>();

		final Map<String, List<AbstractOrderEntryModel>> paymentTransactionEntries = bcfTravelCartService
				.groupEntriesByCacheKeyOrBookingRef(source);

		if (MapUtils.isNotEmpty(paymentTransactionEntries))
		{
			for (String cacheKeyOrBookingRef : paymentTransactionEntries.keySet())
			{
				final BookingReferences tempBookingReference = new BookingReferences();
				if (cartService.useCacheKey(source))
				{
					tempBookingReference.setCachingKey(cacheKeyOrBookingRef);
				}
				else
				{
					tempBookingReference.setBookingReference(cacheKeyOrBookingRef);
				}
				tempBookingReferences.add(tempBookingReference);
			}
		}
		Collections.sort(tempBookingReferences, BookingReferenceComparator.INSTANCE);
		return tempBookingReferences;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public BCFTravelCartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final BCFTravelCartService cartService)
	{
		this.cartService = cartService;
	}

	public IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}


	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public static class BookingReferenceComparator extends AbstractComparator<BookingReferences>
	{
		public static final BookingReferenceComparator INSTANCE = new BookingReferenceComparator();

		@Override
		protected int compareInstances(final BookingReferences bookingReferences1, final BookingReferences bookingReferences2)
		{
			if (Objects.nonNull(bookingReferences1.getBookingReference()))
			{
				return compareValues(bookingReferences1.getBookingReference(), bookingReferences2.getBookingReference(), false);
			}

			if (Objects.nonNull(bookingReferences1.getCachingKey()))
			{
				return compareValues(bookingReferences1.getCachingKey(), bookingReferences2.getCachingKey(), false);
			}

			return 0;
		}
	}
}
