/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CashPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.GiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integration.data.confirm.booking.request.CardInfo;
import com.bcf.integration.data.confirm.booking.request.PaymentInfo;
import com.bcf.integration.data.confirm.booking.request.PaymentProcessed;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentCard;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentMessages;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentReceipt;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentStatus;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentStub;
import com.bcf.integration.data.confirm.booking.request.ProofOfPaymentTransaction;
import com.bcf.integration.ebooking.helper.BcfRemarksHelper;
import com.bcf.integrations.common.BookingReferences;
import com.bcf.integrations.common.PaymentDistribution;
import com.bcf.integrations.common.PaymentTransaction;


public class BCFPaymentInfoDataPopulator implements Populator<AbstractOrderModel, ConfirmBookingsRequest>
{
	private static final String VACATION_CC_CARD_NAME = "vacation.cc.card.name";
	private static final String VACATION_CC_CARD_NUMBER = "vacation.cc.card.number";
	private SessionService sessionService;
	private BCFTravelCartService bcfTravelCartService;
	private CommonI18NService commerceI18NService;
	private ConfigurationService configurationService;
	private UserService userService;
	private BcfRemarksHelper bcfRemarksHelper;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private Map<String, String> journeyBasedPaymentTypeMap;
	private Map<String, String> channelBasedPaymentTypeMap;
	private Map<String, String> journeyBasedPaymentMethodMap;
	private Map<String, String> channelBasedPaymentMethodMap;

	@Override
	public void populate(final AbstractOrderModel source, final ConfirmBookingsRequest target) throws ConversionException
	{
		final Integer cardIndex = 1;
		final List<PaymentDistribution> paymentDistributions = new ArrayList<>();
		final PaymentInfo paymentInfo = new PaymentInfo();
		if (source.getBookingJourneyType() != null && StringUtils
				.equalsIgnoreCase(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), source.getBookingJourneyType().getCode())
				&& CollectionUtils.isNotEmpty(source.getAmountToPayInfos()))
		{
			final Double totalPrice = getTotalAmountToPayNow(source);
			final List<AmountToPayInfoModel> amountToPayInfos = getAmountToPayInfos(source);
			for (final AmountToPayInfoModel amountToPayInfoModel : amountToPayInfos)
			{
				final int amountToPayInCentsPerSailing = getAmountToPayNowPerSailing(source, amountToPayInfoModel);
				paymentDistributions
						.add(createPaymentDistributions(cardIndex, amountToPayInCentsPerSailing, amountToPayInfoModel.getCacheKey(),
								amountToPayInfoModel.getBookingReference(), source));
			}
			paymentInfo.setTotalAmountInCents(totalPrice.intValue());
		}
		else
		{
			final Map<String, List<AbstractOrderEntryModel>> paymentTransactionEntries = bcfTravelCartService
					.groupEntriesByCacheKeyOrBookingRef(source);
			final AtomicInteger totalOrderValue = new AtomicInteger(0);
			paymentTransactionEntries.values().forEach(paymentTransactionEntry -> {

				final Double totalValue = paymentTransactionEntry.stream().mapToDouble(entry -> entry.getTotalPrice() * 100).sum();
				totalOrderValue.getAndAdd(totalValue.intValue());
				paymentDistributions
						.add(createPaymentDistributions(cardIndex, totalValue.intValue(), paymentTransactionEntry.get(0).getCacheKey(),
								paymentTransactionEntry.get(0).getBookingReference(), source));
			});
			paymentInfo.setTotalAmountInCents(totalOrderValue.get());
		}

		final List<ProofOfPaymentStub> proofOfPaymentStubList = new ArrayList<>();
		final List<CardInfo> cardInfoList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(source.getPaymentTransactions()))
		{
			final PaymentTransactionModel paymentTransactionModel = source.getPaymentTransactions().stream()
					.filter(transaction -> !transaction.isExisting()).findFirst().orElse(null);

			if (paymentTransactionModel != null)
			{
				if (source.getBookingJourneyType().equals(BookingJourneyType.BOOKING_PACKAGE) || source.getBookingJourneyType()
						.equals(BookingJourneyType.BOOKING_TRANSPORT_ACCOMMODATION) || source.getBookingJourneyType()
						.equals(BookingJourneyType.BOOKING_ALACARTE))
				{
					cardInfoList.add(createCardInfoStub(cardIndex));
				}
				paymentTransactionModel.getEntries().stream().filter(
						entry -> BCFPaymentMethodType.CARD.equals(entry.getTransactionType()) || BCFPaymentMethodType.CTCCARD
								.equals(entry.getTransactionType()) || BCFPaymentMethodType.CASH.equals(entry.getTransactionType()))
						.forEach(paymentTransactionEntryModel ->
								proofOfPaymentStubList.add(createProofOfPaymentStub(cardIndex, paymentTransactionEntryModel,
										paymentInfo.getTotalAmountInCents(), source.getBookingJourneyType(),
										getBcfSalesApplicationResolverService().getCurrentSalesChannel())));

				paymentTransactionModel.getEntries().stream()
						.filter(entry -> BCFPaymentMethodType.CTCCARD.equals(entry.getTransactionType()))
						.forEach(paymentTransactionEntryModel -> {
							cardInfoList.add(createCardInfoStub(cardIndex, paymentTransactionEntryModel));
						});

			}
		}
		final PaymentProcessed paymentProcessed = new PaymentProcessed();
		paymentProcessed.setPaymentDistribution(paymentDistributions);
		paymentProcessed.setProofOfPaymentStub(proofOfPaymentStubList);

		paymentInfo.setPaymentProcessed(paymentProcessed);
		paymentInfo.setCardInfo(cardInfoList);
		target.setPaymentInfo(paymentInfo);
		if (StringUtils.isNotBlank(source.getAgentComment()))
		{
			target.setNotes(String.format("%s %s", getBcfRemarksHelper().getRemarks(source), source.getAgentComment()));
		}
		else
		{
			target.setNotes(getBcfRemarksHelper().getRemarks(source));
		}
		target.setExternalReferenceId(getExternalReference(source));
		final String email = ((CustomerModel) source.getUser()).getContactEmail();
		final StringBuilder emailBuilder = new StringBuilder(email);
		emailBuilder.append("<mailto:").append(email).append(">");
		target.setExtraEmailAddress(emailBuilder.toString());
		final PaymentTransaction paymentTransaction = new PaymentTransaction();
		paymentTransaction.setIsDeferredPayment(source.isDeferPayment());
		final List<PaymentTransaction> paymentTransactions = new ArrayList<>();
		paymentTransactions.add(paymentTransaction);
		target.setPaymentTransactions(paymentTransactions);
	}

	protected PaymentDistribution createPaymentDistributions(final Integer cardIndex, final Integer amountInCents,
			final String cachingKey, final String bookingRef, final AbstractOrderModel cartModel)
	{
		final PaymentDistribution paymentDistribution = new PaymentDistribution();
		final BookingReferences bookingReference = new BookingReferences();
		if (bcfTravelCartService.useCacheKey(cartModel))
		{
			bookingReference.setCachingKey(cachingKey);
		}
		else
		{
			bookingReference.setBookingReference(bookingRef);
		}
		paymentDistribution.setBookingReference(bookingReference);
		paymentDistribution.setAmountInCents(amountInCents);
		paymentDistribution.setStubIndex(cardIndex);
		return paymentDistribution;
	}

	private ProofOfPaymentStub createProofOfPaymentStub(final Integer cardIndex, final PaymentTransactionEntryModel payTxnEntry,
			final Integer totalAmountInCents, final BookingJourneyType journeyType, final SalesApplication salesApplication)
	{
		final ProofOfPaymentStub proofOfPaymentStub = new ProofOfPaymentStub();
		proofOfPaymentStub.setIndex(cardIndex);
		proofOfPaymentStub.setIsDeferredPayment(payTxnEntry.getPaymentTransaction().getOrder().isDeferPayment());
		proofOfPaymentStub.setPaymentType(getPaymentType(payTxnEntry, journeyType, salesApplication));
		proofOfPaymentStub.setPaymentMethod(getPaymentMethod(payTxnEntry, journeyType, salesApplication));
		proofOfPaymentStub.setStatusInfo(createProofOfPaymentStatus(payTxnEntry));
		proofOfPaymentStub.setTransactionInfo(createProofOfPaymentTransaction(payTxnEntry, totalAmountInCents));
		if (!BCFPaymentMethodType.CASH.equals(payTxnEntry.getTransactionType()))
		{
			proofOfPaymentStub.setCardInfo(createProofOfPaymentCard(payTxnEntry));
		}
		proofOfPaymentStub.setDisplayInfo(createProofOfPaymentMessages(payTxnEntry));
		proofOfPaymentStub.setReceiptInfo(createProofOfPaymentReceipt(payTxnEntry));
		return proofOfPaymentStub;
	}

	private CardInfo createCardInfoStub(final Integer cardIndex, final PaymentTransactionEntryModel paymentTransactionEntryModel)
	{
		final PaymentInfoModel paymentInfo = paymentTransactionEntryModel.getPaymentTransaction().getInfo();
		if (paymentInfo instanceof CTCTCCardPaymentInfoModel)
		{
			final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = (CTCTCCardPaymentInfoModel) paymentInfo;
			final CardInfo cardInfo = new CardInfo();
			cardInfo.setIndex(cardIndex);
			cardInfo.setCardName(paymentTransactionEntryModel.getCardType());
			cardInfo.setCardNumber(ctcTcCardPaymentInfoModel.getCardNumber());
			cardInfo.setCardExpiry("mm20yy");
			return cardInfo;
		}
		if (paymentInfo instanceof CreditCardPaymentInfoModel)
		{
			final CardInfo cardInfo = new CardInfo();
			cardInfo.setIndex(cardIndex);
			cardInfo.setCardName(getConfigurationService().getConfiguration().getString(VACATION_CC_CARD_NAME, "CARDTYPE"));
			cardInfo.setCardNumber(getConfigurationService().getConfiguration().getString(VACATION_CC_CARD_NUMBER, "1234567"));
			cardInfo.setCardExpiry("mm20yy");
			return cardInfo;
		}
		return null;
	}

	private CardInfo createCardInfoStub(final Integer cardIndex)
	{
		final CardInfo cardInfo = new CardInfo();
		cardInfo.setIndex(cardIndex);
		cardInfo.setCardName(getConfigurationService().getConfiguration().getString(VACATION_CC_CARD_NAME, "CARDTYPE"));
		cardInfo.setCardNumber(getConfigurationService().getConfiguration().getString(VACATION_CC_CARD_NUMBER, "1234567"));
		cardInfo.setCardExpiry("mm20yy");
		return cardInfo;
	}

	private ProofOfPaymentStatus createProofOfPaymentStatus(final PaymentTransactionEntryModel paymentTransactionEntryModel)
	{
		final ProofOfPaymentStatus proofOfPaymentStatus = new ProofOfPaymentStatus();
		proofOfPaymentStatus.setInternalResponseCode(paymentTransactionEntryModel.getBcfResponseCode());
		proofOfPaymentStatus.setInternalResponseDescription(paymentTransactionEntryModel.getBcfResponseDescription());
		proofOfPaymentStatus.setProviderResponseCode(paymentTransactionEntryModel.getServiceProviderResponseCode());
		proofOfPaymentStatus.setIsoResponseCode(paymentTransactionEntryModel.getIsoResponseCode());
		return proofOfPaymentStatus;
	}

	private ProofOfPaymentTransaction createProofOfPaymentTransaction(
			final PaymentTransactionEntryModel paymentTransactionEntryModel, final Integer totalAmountInCents)
	{
		final ProofOfPaymentTransaction proofOfPaymentTransaction = new ProofOfPaymentTransaction();
		proofOfPaymentTransaction.setTransactionAmountInCents(totalAmountInCents);
		proofOfPaymentTransaction.setInvoiceNumber(paymentTransactionEntryModel.getInvoiceNumber());
		proofOfPaymentTransaction.setTransactionReference(paymentTransactionEntryModel.getTransactionRefNumber());
		proofOfPaymentTransaction.setAuthorizationCode(paymentTransactionEntryModel.getApprovalNumber());
		final DateFormat dateFormat = new SimpleDateFormat(BcfintegrationserviceConstants.TRANS_TIMESTAMP);
		proofOfPaymentTransaction
				.setTransactionDateTime(dateFormat.format(
						Objects.nonNull(paymentTransactionEntryModel.getTime()) ? paymentTransactionEntryModel.getTime() : new Date()));
		proofOfPaymentTransaction.setInternalRefRecord(paymentTransactionEntryModel.getBcfRefRecord());
		return proofOfPaymentTransaction;
	}

	private ProofOfPaymentCard createProofOfPaymentCard(final PaymentTransactionEntryModel payTxnEntry)
	{
		ProofOfPaymentCard proofOfPaymentCard = new ProofOfPaymentCard();
		final PaymentInfoModel paymentInfo = payTxnEntry.getPaymentTransaction().getInfo();

		if (paymentInfo instanceof CashPaymentInfoModel)
		{
			proofOfPaymentCard = createCardInfo(BCFPaymentMethodType.CASH.getCode());
		}
		else if (paymentInfo instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel ccPaymentInfoModel = (CreditCardPaymentInfoModel) paymentInfo;
			proofOfPaymentCard = createCardInfo(ccPaymentInfoModel, payTxnEntry);
			proofOfPaymentCard.setTokenType(ccPaymentInfoModel.getTokenType());
		}
		else if (paymentInfo instanceof CTCTCCardPaymentInfoModel)
		{
			final CTCTCCardPaymentInfoModel ctctcCardPaymentInfoModel = (CTCTCCardPaymentInfoModel) paymentInfo;
			proofOfPaymentCard = createCardInfo(ctctcCardPaymentInfoModel, payTxnEntry);
		}
		else if (paymentInfo instanceof GiftCardPaymentInfoModel)
		{
			proofOfPaymentCard = createCardInfo(BCFPaymentMethodType.GIFTCARD.getCode());
		}
		return proofOfPaymentCard;
	}

	private ProofOfPaymentMessages createProofOfPaymentMessages(final PaymentTransactionEntryModel paymentTransactionEntryModel)
	{
		final ProofOfPaymentMessages proofOfPaymentMessages = new ProofOfPaymentMessages();
		proofOfPaymentMessages.setMessageToOperator(paymentTransactionEntryModel.getOperatorDisplayMessage());
		proofOfPaymentMessages.setMessageToCustomer(paymentTransactionEntryModel.getCustomerDisplayMessage());
		return proofOfPaymentMessages;
	}

	private ProofOfPaymentReceipt createProofOfPaymentReceipt(final PaymentTransactionEntryModel paymentTransactionEntryModel)
	{
		final ProofOfPaymentReceipt proofOfPaymentReceipt = new ProofOfPaymentReceipt();
		proofOfPaymentReceipt.setTransactionType(paymentTransactionEntryModel.getType().getCode());
		paymentTransactionEntryModel.setApprovalOrDeclineMessage(paymentTransactionEntryModel.getApprovalOrDeclineMessage());
		return proofOfPaymentReceipt;
	}

	protected ProofOfPaymentCard createCardInfo(final CreditCardPaymentInfoModel ccPaymentInfoModel,
			final PaymentTransactionEntryModel payTxnEntry)
	{
		final ProofOfPaymentCard cardInfo = new ProofOfPaymentCard();
		cardInfo.setCardName(payTxnEntry.getCardType());
		cardInfo.setCardIdentifier(ccPaymentInfoModel.getNumber());
		cardInfo.setCardIdentifierDisplayable(payTxnEntry.getCardNumberToPrint());
		cardInfo.setTokenIdentifier(ccPaymentInfoModel.getToken());
		cardInfo.setCardExpiry(ccPaymentInfoModel.getValidToMonth().concat(ccPaymentInfoModel.getValidToYear()));
		return cardInfo;
	}

	protected ProofOfPaymentCard createCardInfo(final CTCTCCardPaymentInfoModel ctctcPaymentInfoModel,
			final PaymentTransactionEntryModel payTxnEntry)
	{
		final ProofOfPaymentCard cardInfo = new ProofOfPaymentCard();
		cardInfo.setCardName(payTxnEntry.getCardType());
		cardInfo.setCardIdentifier(ctctcPaymentInfoModel.getCardNumber());
		cardInfo.setCardIdentifierDisplayable(payTxnEntry.getCardNumberToPrint());
		cardInfo.setCardExpiry("mm20yy");
		return cardInfo;
	}

	protected ProofOfPaymentCard createCardInfo(final String paymentTypeCode)
	{
		final ProofOfPaymentCard cardInfo = new ProofOfPaymentCard();
		cardInfo.setCardName(paymentTypeCode);
		return cardInfo;
	}

	protected String getExternalReference(final AbstractOrderModel source)
	{
		if (getBcfTravelCartService().isAmendmentCart(source) && Objects.nonNull(source.getOriginalOrder()))
		{
			return source.getOriginalOrder().getCartId();
		}
		return source.getCode();
	}

	private Double getTotalAmountToPayNow(final AbstractOrderModel source)
	{
		final CurrencyModel currency = source.getCurrency();
		final int digits = currency.getDigits().intValue();
		if (getBcfTravelCartService().isAmendmentCart(source) && StringUtils
				.equalsIgnoreCase(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), source.getBookingJourneyType().getCode()))
		{
			return getCommerceI18NService().roundCurrency((long) Math.abs(source.getAmountToPay() * 100), digits);
		}
		return getCommerceI18NService().roundCurrency(source.getAmountToPayInfos().stream()
				.mapToDouble(amountToPayInfoModel -> source.isDeferPayment()
						? Double.sum(amountToPayInfoModel.getAmountToPay(), amountToPayInfoModel.getPayAtTerminal())
						: amountToPayInfoModel.getAmountToPay()).sum(), digits);
	}

	private int getAmountToPayNowPerSailing(final AbstractOrderModel source, final AmountToPayInfoModel amountToPayInfo)
	{
		if (getBcfTravelCartService().isAmendmentCart(source) && StringUtils
				.equalsIgnoreCase(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), source.getBookingJourneyType().getCode()))
		{
			return ((int) Math.abs(source.getAmountToPay()) * 100);
		}
		return Double.valueOf(source.isDeferPayment()
				? Double.sum(amountToPayInfo.getAmountToPay(), amountToPayInfo.getPayAtTerminal())
				: amountToPayInfo.getAmountToPay()).intValue();
	}

	private String getPaymentType(final PaymentTransactionEntryModel payTxnEntry, final BookingJourneyType journeyType,
			final SalesApplication salesApplication)
	{
		if (SalesApplication.WEB.equals(salesApplication))
		{
			return getJourneyBasedPaymentTypeMap().get(String
					.format("%s-%s", journeyType.getCode().toUpperCase(), payTxnEntry.getTransactionType().getCode().toUpperCase()));
		}
		else
		{
			return getChannelBasedPaymentTypeMap()
					.get(String.format("%s-%s", salesApplication.getCode().toUpperCase(),
							payTxnEntry.getTransactionType().getCode().toUpperCase()));
		}
	}

	private String getPaymentMethod(final PaymentTransactionEntryModel payTxnEntry, final BookingJourneyType journeyType,
			final SalesApplication salesApplication)
	{
		if (SalesApplication.WEB.equals(salesApplication))
		{
			return getJourneyBasedPaymentMethodMap().get(String
					.format("%s-%s", journeyType.getCode().toUpperCase(), payTxnEntry.getTransactionType().getCode().toUpperCase()));
		}
		else
		{
			return getChannelBasedPaymentMethodMap()
					.get(String.format("%s-%s", salesApplication.getCode().toUpperCase(),
							payTxnEntry.getTransactionType().getCode().toUpperCase()));
		}
	}

	private List<AmountToPayInfoModel> getAmountToPayInfos(final AbstractOrderModel source)
	{
		if (CollectionUtils.isNotEmpty(source.getAmountToPayInfos()))
		{
			if (getBcfTravelCartService().isAmendmentCart(source))
			{
				return StreamUtil.safeStream(source.getAmountToPayInfos())
						.filter(amountToPayInfo -> StringUtils.isNotBlank(amountToPayInfo.getCacheKey())).collect(Collectors.toList());
			}
			return StreamUtil.safeStream(source.getAmountToPayInfos()).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public CommonI18NService getCommerceI18NService()
	{
		return commerceI18NService;
	}

	public void setCommerceI18NService(final CommonI18NService commerceI18NService)
	{
		this.commerceI18NService = commerceI18NService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected BcfRemarksHelper getBcfRemarksHelper()
	{
		return bcfRemarksHelper;
	}

	@Required
	public void setBcfRemarksHelper(final BcfRemarksHelper bcfRemarksHelper)
	{
		this.bcfRemarksHelper = bcfRemarksHelper;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	protected Map<String, String> getJourneyBasedPaymentTypeMap()
	{
		return journeyBasedPaymentTypeMap;
	}

	@Required
	public void setJourneyBasedPaymentTypeMap(final Map<String, String> journeyBasedPaymentTypeMap)
	{
		this.journeyBasedPaymentTypeMap = journeyBasedPaymentTypeMap;
	}

	protected Map<String, String> getChannelBasedPaymentTypeMap()
	{
		return channelBasedPaymentTypeMap;
	}

	@Required
	public void setChannelBasedPaymentTypeMap(final Map<String, String> channelBasedPaymentTypeMap)
	{
		this.channelBasedPaymentTypeMap = channelBasedPaymentTypeMap;
	}

	protected Map<String, String> getJourneyBasedPaymentMethodMap()
	{
		return journeyBasedPaymentMethodMap;
	}

	@Required
	public void setJourneyBasedPaymentMethodMap(final Map<String, String> journeyBasedPaymentMethodMap)
	{
		this.journeyBasedPaymentMethodMap = journeyBasedPaymentMethodMap;
	}

	protected Map<String, String> getChannelBasedPaymentMethodMap()
	{
		return channelBasedPaymentMethodMap;
	}

	@Required
	public void setChannelBasedPaymentMethodMap(final Map<String, String> channelBasedPaymentMethodMap)
	{
		this.channelBasedPaymentMethodMap = channelBasedPaymentMethodMap;
	}
}
