/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.common.BookingClientSystem;



public class BcfBasicCancelBookingPopulator implements Populator<OrderModel, CancelBookingRequestForOrder>
{
	private IntegrationUtility integrationUtils;

	@Override
	public void populate(final OrderModel source, final CancelBookingRequestForOrder target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final BookingClientSystem bookingClientSystem = new BookingClientSystem();
			bookingClientSystem.setClientCode(getIntegrationUtils().getClientCode(source.getSalesApplication(), source));
			target.setBookingClientSystem(bookingClientSystem);
			target.setSendNotification(false);
		}
	}

	/**
	 * @return the integrationUtils
	 */
	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	/**
	 * @param integrationUtils the integrationUtils to set
	 */
	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}
}
