/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.helper;

import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.integration.data.MakeBookingRequestData;


public class BcfSpecialInstructionsHelper extends BcfAdditionalDetailsHelper
{
	private static final String DANGEROUS_GOODS = "ferry.option.booking.special.instruction.dangerous.goods";
	private static final String OWN_WHEELCHAIR = "ferry.option.booking.special.instruction.own.wheelchair";
	private static final String HEARING_IMPAIRED = "ferry.option.booking.special.instruction.hearing.impaired";
	private static final String VISUALLY_IMPAIRED = "ferry.option.booking.special.instruction.visually.impaired";

	private static final String PRODUCT_CODE_OWN_WHEELCHAIR = "WCHR";
	private static final String PRODUCT_CODE_HEARING_IMPAIRED = "DEAF";
	private static final String PRODUCT_CODE_VISUALLY_IMPAIRED = "BLND";

	public String getSpecialInstructions(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final List<String> specialInstructionsLineItems = getSpecialInstructionsFromRequestData(addBundleToCartRequestData);
		return StreamUtil.safeStream(specialInstructionsLineItems).collect(Collectors.joining(ITEM_SEPARATOR));
	}

	public String getSpecialInstructions(final MakeBookingRequestData makeBookingRequestData)
	{
		Integer journeyRef=0;
		Integer odRef=0;
		if(CollectionUtils.isNotEmpty(makeBookingRequestData.getAddProductToCartRequestDatas())){

			journeyRef=makeBookingRequestData.getAddProductToCartRequestDatas().get(0).getJourneyRefNumber();
			odRef=makeBookingRequestData.getAddProductToCartRequestDatas().get(0).getOriginDestinationRefNumber();
		}else if(CollectionUtils.isNotEmpty(makeBookingRequestData.getSailingEntries())){

			journeyRef=makeBookingRequestData.getSailingEntries().get(0).getJourneyReferenceNumber();
			odRef=makeBookingRequestData.getSailingEntries().get(0).getTravelOrderEntryInfo().getOriginDestinationRefNumber();
		}else if(makeBookingRequestData.getAddBundleToCartRequestData()!=null && CollectionUtils.isNotEmpty(makeBookingRequestData.getAddBundleToCartRequestData().getAddBundleToCartData())){

			journeyRef=((BcfAddBundleToCartData)makeBookingRequestData.getAddBundleToCartRequestData().getAddBundleToCartData().get(0)).getJourneyRefNumber();
			odRef=makeBookingRequestData.getAddBundleToCartRequestData().getAddBundleToCartData().get(0).getOriginDestinationRefNumber();
		}
		final List<String> specialInstructionsLineItems = getSpecialInstructionsFromCart(
				journeyRef,
				odRef);
		return StreamUtil.safeStream(specialInstructionsLineItems).collect(Collectors.joining(ITEM_SEPARATOR));
	}

	private List<String> getSpecialInstructionsFromRequestData(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final List<String> specialInstructionsLineItems = new ArrayList<>();
		final AddBundleToCartData addBundleToCartData = addBundleToCartRequestData.getAddBundleToCartData().get(0);
		if (BcfAddBundleToCartData.class.isInstance(addBundleToCartData))
		{
			final BcfAddBundleToCartData bcfAddBundleToCartData = BcfAddBundleToCartData.class.cast(addBundleToCartData);
			if (bcfAddBundleToCartData.isCarryingDangerousGoodsInOutbound())
			{
				specialInstructionsLineItems.add(getConfigurationService().getConfiguration().getString(DANGEROUS_GOODS));
			}
		}
		if (CollectionUtils.isNotEmpty(addBundleToCartRequestData.getSpecialServicesData()))
		{
			final List<String> instructionsPerPAX = getInstructionsBySpecialServices(
					addBundleToCartRequestData.getSpecialServicesData());
			if (CollectionUtils.isNotEmpty(instructionsPerPAX))
			{
				specialInstructionsLineItems.addAll(instructionsPerPAX);
			}
		}
		return specialInstructionsLineItems;
	}

	private List<String> getSpecialInstructionsFromCart(final int journeyRef, final int odRef)
	{
		final List<String> specialInstructionsLineItems = new ArrayList<>();
		final List<AbstractOrderEntryModel> transportEntries = getTransportEntries(journeyRef, odRef);
		if (CollectionUtils.isNotEmpty(transportEntries))
		{
			if (transportEntries.stream().findFirst().get().getTravelOrderEntryInfo().isCarryingDangerousGoods())
			{
				specialInstructionsLineItems.add(getConfigurationService().getConfiguration().getString(DANGEROUS_GOODS));
			}
			final List<TravellerModel> passengers = getPassengers(transportEntries);
			if (CollectionUtils.isNotEmpty(passengers))
			{
				final List<String> instructionsPerPAX = getInstructionsByPassenger(passengers);
				if (CollectionUtils.isNotEmpty(instructionsPerPAX))
				{
					specialInstructionsLineItems.addAll(instructionsPerPAX);
				}
			}
		}
		return specialInstructionsLineItems;
	}

	private List<AbstractOrderEntryModel> getTransportEntries(final int journeyRef, final int odRef)
	{
		final CartModel sessionCart = getBcfTravelCartService().getSessionCart();
		if (Objects.nonNull(sessionCart))
		{
			return StreamUtil.safeStream(sessionCart.getEntries())
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
							&& entry.getAmendStatus().equals(AmendStatus.NEW)
							&& entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& journeyRef == entry.getJourneyReferenceNumber() && Objects.nonNull(entry.getTravelOrderEntryInfo())
							&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
							&& odRef == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private List<String> getInstructionsBySpecialServices(final List<SpecialServicesData> specialServicesDataList)
	{
		final List<String> wheelchairs = new ArrayList<>();
		final List<String> hearingImpairs = new ArrayList<>();
		final List<String> visuallyImpairs = new ArrayList<>();
		StreamUtil.safeStream(specialServicesDataList).filter(specialServicesData -> !StringUtils
				.equals(BcfCoreConstants.OTHER_ACCESSIBILITY_REQUIREMENT_CODE, specialServicesData.getCode()))
				.collect(Collectors.groupingBy(var -> var.getPassengerType()))
				.forEach((yPAXCode, specialServicesData) -> {
					if (CollectionUtils.isNotEmpty(specialServicesData))
					{
						final String paxCode = getPassengerTypeService().getPassengerTypesForCode(yPAXCode).getEBookingCode();
						final Map<Integer, List<SpecialServicesData>> serviceGroupedByIndex = specialServicesData.stream()
								.collect(Collectors.groupingBy(data -> data.getIndex()));
						final Iterator<Map.Entry<Integer, List<SpecialServicesData>>> iterator = serviceGroupedByIndex.entrySet()
								.iterator();
						IntStream.range(0, serviceGroupedByIndex.entrySet().size())
								.forEach(index ->
										iterator.next().getValue().stream().forEach(data -> {
											updateListsWithInstructions(wheelchairs, hearingImpairs, visuallyImpairs,
													data.getCode(), paxCode, index);
										})
								);
					}
				});
		return StreamUtil.concatenate(wheelchairs, hearingImpairs, visuallyImpairs);
	}

	private List<String> getInstructionsByPassenger(final List<TravellerModel> passengers)
	{
		final List<String> wheelchairs = new ArrayList<>();
		final List<String> hearingImpairs = new ArrayList<>();
		final List<String> visuallyImpairs = new ArrayList<>();
		StreamUtil.safeStream(passengers)
				.collect(Collectors.groupingBy(o -> ((PassengerInformationModel) o.getInfo()).getPassengerType().getEBookingCode()))
				.forEach((paxCode, travellerModels) -> {
					for (int i = 0; i < travellerModels.size(); i++)
					{
						if (Objects.nonNull(travellerModels.get(i).getSpecialRequestDetail()))
						{
							for (final SpecialServiceRequestModel specialService : travellerModels.get(i).getSpecialRequestDetail()
									.getSpecialServiceRequest())
							{
								updateListsWithInstructions(wheelchairs, hearingImpairs, visuallyImpairs, specialService.getCode(),
										paxCode,
										i);
							}
						}
					}
				});
		return StreamUtil.concatenate(wheelchairs, hearingImpairs, visuallyImpairs);
	}

	void updateListsWithInstructions(final List<String> wheelchairs, final List<String> hearingImpairs,
			final List<String> visuallyImpairs, final String specialServiceCode, final String paxCode, final int index)
	{
		switch (specialServiceCode)
		{
			case PRODUCT_CODE_OWN_WHEELCHAIR:
				wheelchairs.add(String
						.format("%s%s%s%s%s", getConfigurationService().getConfiguration().getString(OWN_WHEELCHAIR), DELIMITER,
								paxCode, SPACE, (index + 1)));
				break;
			case PRODUCT_CODE_HEARING_IMPAIRED:
				hearingImpairs.add(String
						.format("%s%s%s%s%s", getConfigurationService().getConfiguration().getString(HEARING_IMPAIRED), DELIMITER,
								paxCode, SPACE, (index + 1)));
				break;
			case PRODUCT_CODE_VISUALLY_IMPAIRED:
				visuallyImpairs.add(String
						.format("%s%s%s%s%s", getConfigurationService().getConfiguration().getString(VISUALLY_IMPAIRED), DELIMITER,
								paxCode, SPACE, (index + 1)));
				break;
		}
	}
}
