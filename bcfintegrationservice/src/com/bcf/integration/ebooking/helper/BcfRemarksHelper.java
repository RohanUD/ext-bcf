/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.helper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.util.StreamUtil;


public class BcfRemarksHelper extends BcfAdditionalDetailsHelper
{
	private static final String NOT_AVAILABLE = "NA";
	private static final String OTHER_ACCESSIBILITY_PREFIX = "ferry.option.booking.remark.other.accessibility.prefix";
	private static final String WEIGHT = "ferry.option.booking.remark.vehicle.weight";
	private static final String AXLES = "ferry.option.booking.remark.vehicle.number.of.axles";
	private static final String GROUND_CLEARANCE = "ferry.option.booking.remark.vehicle.ground.clearance";
	private static final String ADJUSTABLE = "ferry.option.booking.remark.vehicle.adjustable";
	private static final String LENGTH = "ferry.option.booking.remark.vehicle.length";
	private static final String HEIGHT = "ferry.option.booking.remark.vehicle.height";
	private static final String WIDTH = "ferry.option.booking.remark.vehicle.width";
	private static final String TRUE = "ferry.option.booking.remark.vehicle.adjustable.yes";
	private static final String FALSE = "ferry.option.booking.remark.vehicle.adjustable.no";

	public String getRemarks(final AbstractOrderModel cart)
	{
		String remarks = null;
		final List<String> remarksByBooking = new ArrayList<>();
		final List<List<AbstractOrderEntryModel>> entriesByJourneyAndOD = getEntriesByJourneyAndOD(cart);
		if (CollectionUtils.isNotEmpty(entriesByJourneyAndOD))
		{
			for (final List<AbstractOrderEntryModel> entries : entriesByJourneyAndOD)
			{
				final List<String> remarksPerJourneyAndOD = getRemarksForLegEntries(entries, entriesByJourneyAndOD.size() > 1);
				remarksByBooking.add(StreamUtil.safeStream(remarksPerJourneyAndOD).collect(Collectors.joining(ITEM_SEPARATOR)));
			}
			remarks = StreamUtil.safeStream(remarksByBooking).collect(Collectors.joining(ITEM_SEPARATOR));
		}
		return StringUtils.isNotBlank(remarks) ? remarks : NOT_AVAILABLE;
	}

	private List<String> getRemarksForLegEntries(final List<AbstractOrderEntryModel> transportEntries,
			final boolean appendRouteAndTime)
	{
		List<String> remarksLineItems = new ArrayList<>();
		final List<String> otherAccessibilityDetails = getOtherAccessibilityDetails(transportEntries);
		if (CollectionUtils.isNotEmpty(otherAccessibilityDetails))
		{
			remarksLineItems.addAll(otherAccessibilityDetails);
		}
		final List<String> vehicleDimensionRemarks = getVehicleDimensionRemarks(transportEntries);
		if (CollectionUtils.isNotEmpty(vehicleDimensionRemarks))
		{
			remarksLineItems.addAll(vehicleDimensionRemarks);
		}
		if (CollectionUtils.isNotEmpty(remarksLineItems) && appendRouteAndTime)
		{
			final String routeCode = getRouteCode(transportEntries);
			final String departDateTimeString = getDepartDateTimeString(transportEntries);
			final List<String> newRemarksLineItems = new ArrayList<>();
			StreamUtil.safeStream(remarksLineItems).forEach(remark -> newRemarksLineItems
					.add(String.format("%s%s%s%s%s", remark, DELIMITER, routeCode, DELIMITER, departDateTimeString)));
			remarksLineItems = newRemarksLineItems;
		}
		return remarksLineItems;
	}

	private List<String> getOtherAccessibilityDetails(final List<AbstractOrderEntryModel> entries)
	{
		List<String> otherAccessibilityDetails = new ArrayList<>();
		final List<TravellerModel> passengers = getPassengers(entries);
		if (CollectionUtils.isNotEmpty(passengers))
		{
			otherAccessibilityDetails = getOtherAccessibilityByPassenger(passengers);
		}
		return otherAccessibilityDetails;
	}

	private List<String> getOtherAccessibilityByPassenger(final List<TravellerModel> passengers)
	{
		final List<String> otherAccessibilityDetails = new ArrayList<>();
		StreamUtil.safeStream(passengers)
				.collect(Collectors.groupingBy(o -> ((PassengerInformationModel) o.getInfo()).getPassengerType().getEBookingCode()))
				.forEach((paxCode, travellerModels) -> {
					for (int i = 0; i < travellerModels.size(); i++)
					{
						if (Objects.nonNull(travellerModels.get(i).getSpecialRequestDetail()))
						{
							for (final SpecialServiceRequestModel specialService : travellerModels.get(i).getSpecialRequestDetail()
									.getSpecialServiceRequest())
							{
								final String description = travellerModels.get(i).getSpecialRequestDetail().getDescription();
								if (StringUtils.equals(BcfCoreConstants.OTHER_ACCESSIBILITY_REQUIREMENT_CODE, specialService.getCode())
										&& StringUtils.isNotBlank(description))
								{
									otherAccessibilityDetails.add(String.format(
											"%s%s%s%s%s%s",
											getConfigurationService().getConfiguration().getString(OTHER_ACCESSIBILITY_PREFIX), description,
											DELIMITER, paxCode, SPACE, (i + 1)));
								}
							}
						}
					}
				});
		return otherAccessibilityDetails;
	}

	private List<String> getVehicleDimensionRemarks(final List<AbstractOrderEntryModel> transportEntries)
	{
		final List<String> remarksLineItems = new ArrayList<>();
		final AbstractOrderEntryModel vehicleEntry = getVehicleEntry(transportEntries);
		if (Objects.nonNull(vehicleEntry))
		{
			StreamUtil.safeStream(vehicleEntry.getTravelOrderEntryInfo().getTravellers()).forEach(travellerModel -> {
				if (travellerModel.getInfo() instanceof BCFVehicleInformationModel)
				{
					updateListForVehicleRemarks(remarksLineItems, BCFVehicleInformationModel.class.cast(travellerModel.getInfo()));
				}
			});
		}
		return remarksLineItems;
	}

	private void updateListForVehicleRemarks(final List<String> remarksLineItems, final BCFVehicleInformationModel vehicleInfo)
	{
		if (Objects.nonNull(vehicleInfo.getWeight()) && vehicleInfo.getWeight() > 0d)
		{
			remarksLineItems
					.add(String.format(getConfigurationService().getConfiguration().getString(WEIGHT), vehicleInfo.getWeight()));
		}
		if (Objects.nonNull(vehicleInfo.getNumberOfAxis()) && vehicleInfo.getNumberOfAxis() > 0)
		{
			remarksLineItems
					.add(String.format(getConfigurationService().getConfiguration().getString(AXLES), vehicleInfo.getNumberOfAxis()));
		}
		if (Objects.nonNull(vehicleInfo.getGroundClearance()) && vehicleInfo.getGroundClearance() > 0)
		{
			remarksLineItems.add(String.format(getConfigurationService().getConfiguration().getString(GROUND_CLEARANCE),
					vehicleInfo.getGroundClearance()));
		}
		if (Objects.nonNull(vehicleInfo.getAdjustable()) && vehicleInfo.getGroundClearance() > 0)
		{
			remarksLineItems.add(String.format(getConfigurationService().getConfiguration().getString(ADJUSTABLE),
					vehicleInfo.getAdjustable() ?
							getConfigurationService().getConfiguration().getString(TRUE) :
							getConfigurationService().getConfiguration().getString(FALSE)));
		}
		if (Objects.nonNull(vehicleInfo.getLength()) && vehicleInfo.getLength() > 0d)
		{
			remarksLineItems
					.add(String.format(getConfigurationService().getConfiguration().getString(LENGTH), vehicleInfo.getLength()));
		}
		if (Objects.nonNull(vehicleInfo.getHeight()) && vehicleInfo.getHeight() > 0d)
		{
			remarksLineItems
					.add(String.format(getConfigurationService().getConfiguration().getString(HEIGHT), vehicleInfo.getHeight()));
		}
		if (Objects.nonNull(vehicleInfo.getWidth()) && vehicleInfo.getWidth() > 0d)
		{
			remarksLineItems
					.add(String.format(getConfigurationService().getConfiguration().getString(WIDTH), vehicleInfo.getWidth()));
		}
	}

	private List<List<AbstractOrderEntryModel>> getEntriesByJourneyAndOD(final AbstractOrderModel cart)
	{
		final List<List<AbstractOrderEntryModel>> entriesByJourneyAndOD = new ArrayList<>();
		StreamUtil.safeStream(getTransportEntries(cart))
				.collect(Collectors.groupingBy(AbstractOrderEntryModel::getJourneyReferenceNumber))
				.forEach((journeyRef, entriesByJourney) -> StreamUtil.safeStream(entriesByJourney).collect(Collectors.groupingBy(
						entryByJourney -> entryByJourney.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
						.forEach((odRef, entriesByOD) -> entriesByJourneyAndOD.add(entriesByOD)));
		return entriesByJourneyAndOD;
	}

	private List<AbstractOrderEntryModel> getTransportEntries(final AbstractOrderModel cart)
	{
		return StreamUtil.safeStream(cart.getEntries())
				.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
						&& entry.getAmendStatus().equals(AmendStatus.NEW)
						&& entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
						&& Objects.nonNull(entry.getTravelOrderEntryInfo())
						&& Objects.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()))
				.collect(Collectors.toList());
	}

	private AbstractOrderEntryModel getVehicleEntry(final List<AbstractOrderEntryModel> transportEntries)
	{
		final List<AbstractOrderEntryModel> vehicleEntries = new ArrayList<>();
		StreamUtil.safeStream(transportEntries)
				.forEach(entry -> entry.getTravelOrderEntryInfo().getTravellers().stream().forEach(travellerModel -> {
					if (travellerModel.getType().equals(TravellerType.VEHICLE))
					{
						vehicleEntries.add(entry);
					}
				}));
		return CollectionUtils.isNotEmpty(vehicleEntries) ? vehicleEntries.get(0) : null;
	}

	private String getRouteCode(final List<AbstractOrderEntryModel> transportEntries)
	{
		return StreamUtil.safeStream(transportEntries).findFirst().get().getTravelOrderEntryInfo().getTravelRoute().getCode();
	}

	private String getDepartDateTimeString(final List<AbstractOrderEntryModel> transportEntries)
	{
		return TravelDateUtils.convertDateToStringDate(
				StreamUtil.safeStream(transportEntries).findFirst().get().getTravelOrderEntryInfo().getTransportOfferings().stream()
						.findFirst().get().getDepartureTime(), BcfintegrationcoreConstants.BCF_DATE_TIME_PATTERN);
	}
}
