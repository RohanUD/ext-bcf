/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.helper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFPassengerTypeService;
import com.bcf.core.service.BCFTravelCartService;


public abstract class BcfAdditionalDetailsHelper
{
	protected static final String SPACE = " ";
	protected static final String ITEM_SEPARATOR = ", ";
	protected static final String DELIMITER = " - ";

	private BCFTravelCartService bcfTravelCartService;
	private BCFPassengerTypeService passengerTypeService;
	private ConfigurationService configurationService;

	protected List<TravellerModel> getPassengers(final List<AbstractOrderEntryModel> transportEntries)
	{
		return transportEntries.stream().map(e -> e.getTravelOrderEntryInfo().getTravellers()).flatMap(t -> t.stream())
				.filter(travellerModel -> TravellerType.PASSENGER.equals(travellerModel.getType())).distinct()
				.collect(Collectors.toList());
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected BCFPassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final BCFPassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
