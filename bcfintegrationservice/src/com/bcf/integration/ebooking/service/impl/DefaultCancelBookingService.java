/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import static com.bcf.core.constants.BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.pipelinemanager.CancelBookingPipelineManager;
import com.bcf.integration.ebooking.pipelinemanager.CancelSailingPipelineManager;
import com.bcf.integration.ebooking.service.CancelBookingService;
import com.bcf.integrations.cancelbooking.cart.request.CancelBookingRequestForCart;
import com.bcf.integrations.cancelbooking.cart.response.CancelBookingResponseForCart;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.cancelbooking.order.response.CancelBookingResponseForOrder;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCancelBookingService implements CancelBookingService
{
	private static final Logger LOG = Logger.getLogger(DefaultCancelBookingService.class);
	private BaseRestService eBookingRestService;
	private CancelBookingPipelineManager cancelBookingPipelineManager;
	private Converter<OrderModel, CancelBookingRequestForOrder> cancelBookingsRequestConverter;
	private CancelSailingPipelineManager cancelSailingPipelineManager;
	private ModelService modelService;
	private CalculationService calculationService;

	@Override
	public CancelBookingResponseForCart cancelBooking(final CartModel cart, final List<AbstractOrderEntryModel> removeEntries)
			throws IntegrationException
	{
		final CancelBookingRequestForCart request = getCancelBookingPipelineManager().createRequest(cart, removeEntries);
		return (CancelBookingResponseForCart) geteBookingRestService().sendRestRequest(request, CancelBookingResponseForCart.class,
				BcfintegrationserviceConstants.EBOOKING_CANCEL_BOOKING_SERVICE_URL,
				HttpMethod.POST);
	}

	@Override
	public CancelBookingResponseForOrder cancelBookings(final OrderModel order)
			throws IntegrationException
	{
		if (Objects.isNull(order))
		{
			return null;
		}

		final CancelBookingRequestForOrder cancelBookingsRequest = getCancelBookingsRequestConverter().convert(order);
		return (CancelBookingResponseForOrder) geteBookingRestService().sendRestRequest(cancelBookingsRequest,
				CancelBookingResponseForOrder.class, BcfintegrationserviceConstants.EBOOKING_CANCEL_BOOKING_SERVICE_URL,
				HttpMethod.POST);
	}

	@Override
	public CancelBookingResponseForOrder cancelBookings(final OrderModel order,
			final List<AbstractOrderEntryModel> removeOrderEntries) throws IntegrationException
	{
		if (Objects.isNull(order))
		{
			return null;
		}

		final CancelBookingRequestForOrder cancelBookingsRequest = getCancelSailingPipelineManager().createRequest(order,
				removeOrderEntries);
		return (CancelBookingResponseForOrder) geteBookingRestService().sendRestRequest(cancelBookingsRequest,
				CancelBookingResponseForOrder.class, BcfintegrationserviceConstants.EBOOKING_CANCEL_BOOKING_SERVICE_URL,
				HttpMethod.POST);
	}

	@Override
	public void updateCancelOrder(final List<OrderEntryModel> orderEntryModels)
	{
		if (!Objects.isNull(orderEntryModels))
		{
			final List<TravelOrderEntryInfoModel> travelInfoToSave = new ArrayList<>();
			final OrderModel orderModel = orderEntryModels.get(0).getOrder();
			final List<AbstractOrderEntryModel> inboundOrderEntryForSameJourneyRef = orderModel.getEntries().stream()
					.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType()) && Objects
							.nonNull(entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
							&& entry.getJourneyReferenceNumber() == orderEntryModels.get(0).getJourneyReferenceNumber() && entry
							.getTravelOrderEntryInfo().getOriginDestinationRefNumber()
							== TravelservicesConstants.INBOUND_REFERENCE_NUMBER).collect(Collectors.toList());
			final Boolean returnJourney = !inboundOrderEntryForSameJourneyRef.isEmpty();
			if (returnJourney && orderEntryModels.get(0).getTravelOrderEntryInfo().getOriginDestinationRefNumber()
					== OUTBOUND_REFERENCE_NUMBER)
			{
				inboundOrderEntryForSameJourneyRef.stream().forEach(
						convertToOutboundLegEntry -> {
							convertToOutboundLegEntry.getTravelOrderEntryInfo()
									.setOriginDestinationRefNumber(OUTBOUND_REFERENCE_NUMBER);
							travelInfoToSave.add(convertToOutboundLegEntry.getTravelOrderEntryInfo());
						});
			}

			orderEntryModels.forEach(entry -> {
				entry.setActive(Boolean.FALSE);
				entry.setAmendStatus(AmendStatus.CHANGED);
			});
			final Optional<AbstractOrderEntryModel> anyActiveOrderEntry = orderModel.getEntries()
					.stream()
					.filter(entry -> entry.getActive() == Boolean.TRUE).findAny();
			if (!anyActiveOrderEntry.isPresent())
			{
				orderModel.setStatus(OrderStatus.CANCELLED);
			}
			getModelService().saveAll(travelInfoToSave);
			getModelService().saveAll(orderEntryModels);
			getModelService().save(orderModel);
			try
			{
				calculationService.calculate(orderModel);
			}
			catch (final CalculationException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
	}


	/**
	 * @return the eBookingRestService
	 */
	protected BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	/**
	 * @param eBookingRestService the eBookingRestService to set
	 */
	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

	/**
	 * @return the cancelBookingPipelineManager
	 */
	protected CancelBookingPipelineManager getCancelBookingPipelineManager()
	{
		return cancelBookingPipelineManager;
	}

	/**
	 * @param cancelBookingPipelineManager the cancelBookingPipelineManager to set
	 */
	@Required
	public void setCancelBookingPipelineManager(final CancelBookingPipelineManager cancelBookingPipelineManager)
	{
		this.cancelBookingPipelineManager = cancelBookingPipelineManager;
	}

	/**
	 * @return the cancelBookingsRequestConverter
	 */
	protected Converter<OrderModel, CancelBookingRequestForOrder> getCancelBookingsRequestConverter()
	{
		return cancelBookingsRequestConverter;
	}

	/**
	 * @param cancelBookingsRequestConverter the cancelBookingsRequestConverter to set
	 */
	@Required
	public void setCancelBookingsRequestConverter(
			final Converter<OrderModel, CancelBookingRequestForOrder> cancelBookingsRequestConverter)
	{
		this.cancelBookingsRequestConverter = cancelBookingsRequestConverter;
	}

	/**
	 * @return the cancelSailingPipelineManager
	 */
	protected CancelSailingPipelineManager getCancelSailingPipelineManager()
	{
		return cancelSailingPipelineManager;
	}

	/**
	 * @param cancelSailingPipelineManager the cancelSailingPipelineManager to set
	 */
	@Required
	public void setCancelSailingPipelineManager(final CancelSailingPipelineManager cancelSailingPipelineManager)
	{
		this.cancelSailingPipelineManager = cancelSailingPipelineManager;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public CalculationService getCalculationService()
	{
		return calculationService;
	}

	@Required
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}
}
