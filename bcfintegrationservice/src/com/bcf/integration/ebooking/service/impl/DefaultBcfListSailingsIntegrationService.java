/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.service.BcfListSailingsIntegrationService;
import com.bcf.integration.listSailingRequest.data.BcfListSailingsRequestData;
import com.bcf.integration.listSailingResponse.data.BcfListSailingsResponseData;
import com.bcf.integration.listsailings.request.manager.ListSailingsRequestPipelineManager;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBcfListSailingsIntegrationService implements BcfListSailingsIntegrationService
{
	private BaseRestService eBookingRestService;
	private ListSailingsRequestPipelineManager listSailingsRequestPipelineManager;

	@Override
	public List<BcfListSailingsResponseData> getSailingsList(final FareSearchRequestData fareSearchRequestData)
			throws IntegrationException
	{
		final List<BcfListSailingsResponseData> completeSailings = new ArrayList<>();

		for (final OriginDestinationInfoData originDestinationOption : fareSearchRequestData.getOriginDestinationInfo())
		{
			final BcfListSailingsResponseData currentSailingsResponse = (BcfListSailingsResponseData) geteBookingRestService()
					.sendRestRequest(createListSailingRequest(fareSearchRequestData, originDestinationOption),
							BcfListSailingsResponseData.class, BcfintegrationserviceConstants.EBOOKING_SAILING_V2_SERVICE_URL,
							HttpMethod.POST);
			if (Objects.nonNull(currentSailingsResponse))
			{
				completeSailings.add(currentSailingsResponse);
			}
		}

		return completeSailings;
	}

	public BcfListSailingsRequestData createListSailingRequest(final FareSearchRequestData fareSearchRequestData,
			final OriginDestinationInfoData originDestinationOption)
	{
		final BcfListSailingsRequestData listSailingRequest = new BcfListSailingsRequestData();
		getListSailingsRequestPipelineManager().executePipeline(listSailingRequest, originDestinationOption,
				fareSearchRequestData);
		return listSailingRequest;
	}

	protected BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

	protected ListSailingsRequestPipelineManager getListSailingsRequestPipelineManager()
	{
		return listSailingsRequestPipelineManager;
	}

	@Required
	public void setListSailingsRequestPipelineManager(
			final ListSailingsRequestPipelineManager listSailingsRequestPipelineManager)
	{
		this.listSailingsRequestPipelineManager = listSailingsRequestPipelineManager;
	}

}
