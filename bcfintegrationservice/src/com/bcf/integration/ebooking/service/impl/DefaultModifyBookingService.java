/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.ModifyBookingPipelineManager;
import com.bcf.integration.ebooking.service.ModifyBookingService;
import com.bcf.integrations.booking.request.BCFModifyBookingRequest;
import com.bcf.integrations.booking.response.BCFModifyBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultModifyBookingService implements ModifyBookingService
{
	private BaseRestService eBookingRestService;
	private ModifyBookingPipelineManager modifyBookingPipelineManager;

	@Override
	public BCFModifyBookingResponse getModifyBookingResponse(final MakeBookingRequestData makeBookingRequestData,
			final String requestType)
			throws IntegrationException
	{

		final BCFModifyBookingRequest request = modifyBookingPipelineManager.processRequest(makeBookingRequestData, requestType);
		final BCFModifyBookingResponse bcfModifyBookingResponse = (BCFModifyBookingResponse) geteBookingRestService()
				.sendRestRequest(request, BCFModifyBookingResponse.class,
				BcfintegrationserviceConstants.EBOOKING_MODIFY_BOOKING_SERVICE_URL, HttpMethod.POST);
		if (StringUtils.equals(requestType, BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_REQUEST))
		{
			addCacheKeyInResponse(request, bcfModifyBookingResponse);
		}
		return bcfModifyBookingResponse;
	}


	@Override
	public BCFModifyBookingResponse getModifyBookingResponse(final AddBundleToCartRequestData requestData,
			final String requestType) throws IntegrationException
	{
		final BCFModifyBookingRequest request = modifyBookingPipelineManager.processRequest(requestData, requestType);
		return (BCFModifyBookingResponse) geteBookingRestService().sendRestRequest(request, BCFModifyBookingResponse.class,
				BcfintegrationserviceConstants.EBOOKING_MODIFY_BOOKING_SERVICE_URL, HttpMethod.POST);
	}

	private void addCacheKeyInResponse(final BCFModifyBookingRequest request,
			final BCFModifyBookingResponse bcfModifyBookingResponse)
	{
		final List<String> cacheKeys = new ArrayList();
		StreamUtil.safeStream(request.getConvertQuotationToOptionRequest().getTempBookingReferences())
				.forEach(bookingReferences -> cacheKeys.add(bookingReferences.getCachingKey()));
		IntStream.range(0, cacheKeys.size()).forEach(i ->
				bcfModifyBookingResponse.getConvertQuotationToOptionResponse().getBookingReference().get(i)
						.setCachingKey(cacheKeys.get(i)));
	}

	public BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

	public ModifyBookingPipelineManager getModifyBookingPipelineManager()
	{
		return modifyBookingPipelineManager;
	}

	public void setModifyBookingPipelineManager(
			final ModifyBookingPipelineManager modifyBookingPipelineManager)
	{
		this.modifyBookingPipelineManager = modifyBookingPipelineManager;
	}
}
