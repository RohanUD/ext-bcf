/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.SearchBookingResponseDTO;
import com.bcf.integration.ebooking.populators.SearchBookingRequestPopulator;
import com.bcf.integration.ebooking.service.SearchBookingService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.integrations.search.booking.response.Itinerary;


public class DefaultSearchBookingService implements SearchBookingService
{
	private BaseRestService eBookingRestService;
	private SearchBookingRequestPopulator searchBookingRequestPopulator;

	@Override
	public SearchBookingResponseDTO searchBookings(final CustomerModel customerModel, final Integer currentPageNumber,
			final Boolean isUpcomingBooking, final boolean fetchAll)
			throws IntegrationException
	{
		final SearchBookingResponseDTO searchResponse = (SearchBookingResponseDTO) geteBookingRestService().sendRestRequest(
				getSearchBookingRequestPopulator().getRequestBody(customerModel, currentPageNumber, isUpcomingBooking),
				SearchBookingResponseDTO.class,
				BcfintegrationserviceConstants.EBOOKING_SEARCH_BOOKINGS_SERVICE_URL, HttpMethod.POST);
		if (fetchAll && searchResponse.getTotalPageNumber() > 1)
		{
			for (int currentPage = 2; currentPage <= searchResponse.getTotalPageNumber(); currentPage++)
			{
				searchResponse.getSearchResult().addAll(((SearchBookingResponseDTO) geteBookingRestService().sendRestRequest(
						getSearchBookingRequestPopulator().getRequestBody(customerModel, currentPage, isUpcomingBooking),
						SearchBookingResponseDTO.class,
						BcfintegrationserviceConstants.EBOOKING_SEARCH_BOOKINGS_SERVICE_URL, HttpMethod.POST)).getSearchResult());
			}
			searchResponse.setCurrentPageNumber(1);
			searchResponse.setTotalPageNumber(1);
			searchResponse.setTotalRecordNumber(searchResponse.getSearchResult().size());
		}
		return searchResponse;
	}

	@Override
	public SearchBookingResponseDTO advanceSearchBookings(final CustomerModel customerModel, final Integer currentPageNumber,
			final BookingsAdvanceSearchData bookingsAdvanceSearchData)
			throws IntegrationException
	{
		return (SearchBookingResponseDTO) geteBookingRestService().sendRestRequest(getSearchBookingRequestPopulator()
						.getAdvanceSearchRequestBody(customerModel, currentPageNumber, bookingsAdvanceSearchData),
				SearchBookingResponseDTO.class, BcfintegrationserviceConstants.EBOOKING_SEARCH_BOOKINGS_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public SearchBookingResponseDTO searchBookings(final CustomerModel customerModel,
			final List<String> eBookingReferencesList, final Integer currentPageNumber) throws IntegrationException
	{
		final SearchBookingResponseDTO response = new SearchBookingResponseDTO();
		final List<Itinerary> searchResults = new ArrayList<>();

		/*
		TODO - for loop need to be removed once the service can serve search on multiple booking reference at once.
		The response can be returned directly no other action required.
		*/
		for (final String bookingReference : eBookingReferencesList)
		{
			if (StringUtils.isNotBlank(bookingReference))
			{
				final SearchBookingResponseDTO searchResponse = (SearchBookingResponseDTO) geteBookingRestService().sendRestRequest(
						getSearchBookingRequestPopulator()
								.getRequestBody(customerModel, Arrays.asList(bookingReference), currentPageNumber, null),
						SearchBookingResponseDTO.class, BcfintegrationserviceConstants.EBOOKING_SEARCH_BOOKINGS_SERVICE_URL,
						HttpMethod.POST);
				if (Objects.nonNull(searchResponse) && CollectionUtils.isNotEmpty(searchResponse.getSearchResult()))
				{
					searchResults.addAll(searchResponse.getSearchResult());
				}
			}
		}
		response.setSearchResult(searchResults);
		response.setCurrentPageNumber(1);
		response.setTotalPageNumber(1);
		response.setTotalRecordNumber(searchResults.size());
		return response;
	}

	protected SearchBookingRequestPopulator getSearchBookingRequestPopulator()
	{
		return searchBookingRequestPopulator;
	}

	@Required
	public void setSearchBookingRequestPopulator(final SearchBookingRequestPopulator searchBookingRequestPopulator)
	{
		this.searchBookingRequestPopulator = searchBookingRequestPopulator;
	}

	protected BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}
}
