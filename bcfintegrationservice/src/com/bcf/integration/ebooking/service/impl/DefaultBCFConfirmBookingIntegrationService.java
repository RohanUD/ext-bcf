/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.ebooking.ConfirmBookingServiceSuccessData;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.ConfirmBookingsRequest;
import com.bcf.integration.data.ConfirmBookingsResponse;
import com.bcf.integration.ebooking.service.BCFConfirmBookingIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBCFConfirmBookingIntegrationService implements BCFConfirmBookingIntegrationService
{

	private BCFTravelCartService bcfTravelCartService;
	private Converter<AbstractOrderModel, ConfirmBookingsRequest> confirmBookingsRequestConverter;
	private ModelService modelService;
	private BaseRestService eBookingRestService;

	private static final Logger LOG = Logger.getLogger(DefaultBCFConfirmBookingIntegrationService.class);


	@Override
	public ConfirmBookingServiceSuccessData makeConfirmBookingRequest(final AbstractOrderModel abstractOrderModel) throws IntegrationException
	{
		final ConfirmBookingsRequest request = getConfirmBookingsRequestConverter()
				.convert(abstractOrderModel);
		final ConfirmBookingsResponse response = (ConfirmBookingsResponse) geteBookingRestService().sendRestRequest(
				request, ConfirmBookingsResponse.class,
				BcfintegrationserviceConstants.EBOOKING_CONFIRM_BOOKING_SERVICE_URL, HttpMethod.POST);

		return createEBookingServiceSucessData(abstractOrderModel, response, request);
	}

	protected ConfirmBookingServiceSuccessData createEBookingServiceSucessData(AbstractOrderModel abstractOrderModel,
			final ConfirmBookingsResponse response, final ConfirmBookingsRequest request)
	{
		final ConfirmBookingServiceSuccessData confirmBookingServiceSuccessData = new ConfirmBookingServiceSuccessData();

		if (Objects.isNull(response))
		{
			LOG.error("response is null");
			return null;
		}
		if (!StringUtils.equals("SUCCESS", response.getConfirmResult().get(0).getResult()))
		{
			confirmBookingServiceSuccessData.setErrorCode(response.getConfirmResult().get(0).getError().getErrorCode());
			confirmBookingServiceSuccessData.setErrorDetails(response.getConfirmResult().get(0).getError().getErrorDetail());
			confirmBookingServiceSuccessData.setErrorSummary(response.getConfirmResult().get(0).getError().getErrorSummary());
			return confirmBookingServiceSuccessData;
		}
		if (StringUtils.equals(BcfintegrationserviceConstants.QUOTATION_BOOKING, request.getBookingType().getBookingType()))
		{
			final Map<String, String> bookingReferenceMap = new HashMap<>(
					CollectionUtils.size(response.getConfirmResult()));
			response.getConfirmResult().forEach(result -> {
				bookingReferenceMap
						.put(result.getBookingReference().getCachingKey(), result.getBookingReference().getBookingReference());
			});

			IntStream.range(0, request.getTempBookingReferences().size())
					.forEach(i -> bookingReferenceMap
							.put(request.getTempBookingReferences().get(i).getCachingKey(),
									response.getConfirmResult().get(i).getBookingReference().getBookingReference()));
			confirmBookingServiceSuccessData.setBookingReferenceMap(bookingReferenceMap);
		}
		confirmBookingServiceSuccessData.setConfirmBookingResponse(response);
		return confirmBookingServiceSuccessData;
	}



	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	/**
	 * @return the confirmBookingsRequestConverter
	 */
	protected Converter<AbstractOrderModel, ConfirmBookingsRequest> getConfirmBookingsRequestConverter()
	{
		return confirmBookingsRequestConverter;
	}

	/**
	 * @param confirmBookingsRequestConverter the confirmBookingsProxyRequestConverter to set
	 */
	@Required
	public void setConfirmBookingsRequestConverter(
			final Converter<AbstractOrderModel, ConfirmBookingsRequest> confirmBookingsRequestConverter)
	{
		this.confirmBookingsRequestConverter = confirmBookingsRequestConverter;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

}
