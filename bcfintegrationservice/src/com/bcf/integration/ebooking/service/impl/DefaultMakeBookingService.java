/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.service.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.MakeBookingPipelineManager;
import com.bcf.integration.ebooking.service.MakeBookingService;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;
import com.bcf.integrations.booking.response.BCFMakeBookingResponse;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultMakeBookingService implements MakeBookingService
{
	private BaseRestService eBookingRestService;
	private MakeBookingPipelineManager makeBookingPipelineManager;

	@Override
	public BCFMakeBookingResponse getMakeBookingResponse(final AddBundleToCartRequestData requestData) throws IntegrationException
	{
		final BCFMakeBookingRequest request = getMakeBookingPipelineManager().processRequest(requestData);
		return (BCFMakeBookingResponse) geteBookingRestService().sendRestRequest(request, BCFMakeBookingResponse.class,
				BcfintegrationserviceConstants.EBOOKING_MAKE_BOOKING_SERVICE_URL, HttpMethod.POST);
	}

	@Override
	public BCFMakeBookingResponse getMakeBookingResponse(final MakeBookingRequestData makeBookingRequestData)
			throws IntegrationException
	{

		final BCFMakeBookingRequest request = getMakeBookingPipelineManager().processRequest(makeBookingRequestData);
		return (BCFMakeBookingResponse) geteBookingRestService().sendRestRequest(request, BCFMakeBookingResponse.class,
				BcfintegrationserviceConstants.EBOOKING_MAKE_BOOKING_SERVICE_URL, HttpMethod.POST);
	}

	public BaseRestService geteBookingRestService()
	{
		return eBookingRestService;
	}

	@Required
	public void seteBookingRestService(final BaseRestService eBookingRestService)
	{
		this.eBookingRestService = eBookingRestService;
	}

	protected MakeBookingPipelineManager getMakeBookingPipelineManager()
	{
		return makeBookingPipelineManager;
	}

	@Required
	public void setMakeBookingPipelineManager(final MakeBookingPipelineManager makeBookingPipelineManager)
	{
		this.makeBookingPipelineManager = makeBookingPipelineManager;
	}


}
