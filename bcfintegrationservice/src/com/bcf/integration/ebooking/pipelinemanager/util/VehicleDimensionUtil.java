/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.util;

import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.data.VehicleDimension;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.common.data.VehicleInfo;


public class VehicleDimensionUtil
{
	private static final int CONVERSION_MULTIPLIER = 100;

	private VehicleDimensionUtil()
	{
		//Utility classes, which are a collection of static members, are not meant to be instantiated
	}

	public static void setVehicleDimensions(final VehicleInfo vehicleInfo, final VehicleDimension vehicleDimension,
			final String vehicleCode)
	{
		setVehicleDimensions(vehicleInfo, vehicleCode, vehicleDimension.getProductLength(), vehicleDimension.getProductWeight(),
				vehicleDimension.getProductWidth(), vehicleDimension.getProductHeight());
	}

	public static void setVehicleDimensions(final VehicleInfo vehicleInfo, final VehicleTypeQuantityData vehicleInfoData)
	{
		setVehicleDimensions(vehicleInfo, vehicleInfoData.getVehicleType().getCode(), vehicleInfoData.getLength(),
				vehicleInfoData.getWeight(), vehicleInfoData.getWidth(), vehicleInfoData.getHeight());
	}

	private static void setVehicleDimensions(final VehicleInfo vehicleInfo, final String vehicleCode, final double length,
			final double weight, final double width, final double height)
	{
		switch (vehicleCode)
		{
			case BcfCoreConstants.MOTORCYCLE_STANDARD:
				setMotorcycleDimensions(vehicleInfo);
				break;
			case BcfCoreConstants.MOTORCYCLE_WITH_TRAILER:
				setMotorcycleDimensions(vehicleInfo);
				break;
			case BcfCoreConstants.UNDER_HEIGHT:
			case BcfCoreConstants.UNDER_HEIGHT_WITH_LIVESTOCK:
				setUHDimensions(vehicleInfo, length);
				break;
			case BcfCoreConstants.OVERSIZE:
			case BcfCoreConstants.OVERSIZE_WITH_LIVESTOCK:
				setOSDimensions(vehicleInfo, length, height);
				break;
			default:
				setOver5500Dimensions(vehicleInfo, length, weight, width, height);
		}
	}

	private static void setUHDimensions(final VehicleInfo vehicleInfo, final double length)
	{
		vehicleInfo.setVehicleLength(convertDimensions(length > 0d ? length : BcfCoreConstants.DEFAULT_MIN_UH_OS_LENGTH));
		vehicleInfo.setVehicleHeight(convertDimensions(BcfCoreConstants.DEFAULT_MIN_UH_OS_HEIGHT));
	}

	private static void setOSDimensions(final VehicleInfo vehicleInfo, final double length, final double height)
	{
		vehicleInfo.setVehicleLength(convertDimensions(length > 0d ? length : BcfCoreConstants.DEFAULT_MIN_UH_OS_LENGTH));
		vehicleInfo.setVehicleHeight(convertDimensions(height > 0d ? height : BcfCoreConstants.DEFAULT_MIN_UH_OS_HEIGHT));
	}

	private static void setMotorcycleDimensions(final VehicleInfo vehicleInfo)
	{
		vehicleInfo.setVehicleLength(convertDimensions(BcfCoreConstants.DEFAULT_MC_MST_LENGTH));
		vehicleInfo.setVehicleHeight(convertDimensions(BcfCoreConstants.DEFAULT_MC_MST_HEIGHT));
	}

	private static void setOver5500Dimensions(final VehicleInfo vehicleInfo, final double length, final double weight,
			final double width, final double height)
	{
		vehicleInfo.setVehicleLength(convertDimensions(length));
		vehicleInfo.setVehicleHeight(convertDimensions(height));
		if (weight > 0d)
		{
			vehicleInfo.setVehicleWeight(convertDimensions(weight));
		}
		if (width > BcfCoreConstants.OVER5500KG_MIN_WIDTH)
		{
			vehicleInfo.setVehicleWidth(convertDimensions(width));
		}
	}

	private static long convertDimensions(final double dimensionValue)
	{
		return ((long) Math.ceil(dimensionValue)) * CONVERSION_MULTIPLIER;
	}

}
