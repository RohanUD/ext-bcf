/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.ModifyBookingPipelineManager;
import com.bcf.integration.ebooking.pipelinemanager.handler.MakeBookingRequestHandler;
import com.bcf.integration.ebooking.pipelinemanager.handler.ModifyBookingRequestHandler;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;
import com.bcf.integrations.booking.request.BCFModifyBookingRequest;
import com.bcf.integrations.booking.request.ConvertQuotationToOptionRequest;

public class DefaultModifyBookingPipelineManager implements ModifyBookingPipelineManager
{
	private Map<String, List<MakeBookingRequestHandler>> handlersMap;
	private List<ModifyBookingRequestHandler> cartConversionHandler;

	@Override
	public BCFModifyBookingRequest processRequest(final MakeBookingRequestData makeBookingRequestData,
			final String requestType)
	{

		final BCFModifyBookingRequest bcfModifyBookingRequest = new BCFModifyBookingRequest();

		if (StringUtils.equals(requestType, BcfCoreConstants.MAKE_BOOKING_REQUEST))
		{
			final BCFMakeBookingRequest request = new BCFMakeBookingRequest();
			getHandlersMap().get("UPDATE_CART").forEach(handler -> handler.createRequest(makeBookingRequestData, request));

			bcfModifyBookingRequest.setMakeBookingRequest(request);
		}
		if (StringUtils.equals(requestType, BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_REQUEST))
		{
			final ConvertQuotationToOptionRequest cartConversionRequest = new ConvertQuotationToOptionRequest();
			getCartConversionHandler().forEach(handler -> handler.createRequest(makeBookingRequestData, cartConversionRequest));
			bcfModifyBookingRequest.setConvertQuotationToOptionRequest(cartConversionRequest);
		}

		bcfModifyBookingRequest.setRequestType(requestType);

		return bcfModifyBookingRequest;
	}


	@Override
	public BCFModifyBookingRequest processRequest(final AddBundleToCartRequestData addBundleToCartRequestData,
			final String requestType)
	{
		final BCFModifyBookingRequest bcfModifyBookingRequest = new BCFModifyBookingRequest();
		final BCFMakeBookingRequest request = new BCFMakeBookingRequest();

		getHandlersMap().get("ADD_BUNDLE").forEach(handler -> handler.createRequest(addBundleToCartRequestData, request));
		bcfModifyBookingRequest.setMakeBookingRequest(request);

		bcfModifyBookingRequest.setRequestType(requestType);

		return bcfModifyBookingRequest;
	}


	/**
	 * @return the handlersMap
	 */
	protected Map<String, List<MakeBookingRequestHandler>> getHandlersMap()
	{
		return handlersMap;
	}

	/**
	 * @param handlersMap the handlersMap to set
	 */
	@Required
	public void setHandlersMap(final Map<String, List<MakeBookingRequestHandler>> handlersMap)
	{
		this.handlersMap = handlersMap;
	}

	protected List<ModifyBookingRequestHandler> getCartConversionHandler()
	{
		return cartConversionHandler;
	}

	@Required
	public void setCartConversionHandler(
			final List<ModifyBookingRequestHandler> cartConversionHandler)
	{
		this.cartConversionHandler = cartConversionHandler;
	}

}
