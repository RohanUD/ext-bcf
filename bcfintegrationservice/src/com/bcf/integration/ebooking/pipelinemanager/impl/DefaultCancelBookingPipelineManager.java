/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.pipelinemanager.CancelBookingPipelineManager;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForCartRequestHandler;
import com.bcf.integrations.cancelbooking.cart.request.CancelBookingRequestForCart;
import com.bcf.integrations.common.BookingClientSystem;



public class DefaultCancelBookingPipelineManager implements CancelBookingPipelineManager
{
	private List<CancelBookingForCartRequestHandler> handlers;

	private IntegrationUtility integrationUtils;

	@Override
	public CancelBookingRequestForCart createRequest(final CartModel cartModel,
			final List<AbstractOrderEntryModel> removeEntries)
	{
		final CancelBookingRequestForCart cancelBookingRequest = new CancelBookingRequestForCart();

		final SalesApplication salesApplication = cartModel.getSalesApplication();
		final BookingClientSystem bookingClientSystem = new BookingClientSystem();
		bookingClientSystem.setClientCode(getIntegrationUtils().getClientCode(salesApplication));
		cancelBookingRequest.setBookingClientSystem(bookingClientSystem);
		cancelBookingRequest.setNotes(
				BcfintegrationserviceConstants.NOTE_DELIMITER + cartModel.getUser().getName() + BcfintegrationserviceConstants.
						NOTE_DELIMITER + BcfintegrationserviceConstants.CANCELLING_BOOKING_NOTE);
		getHandlers().forEach(handler -> handler.populateRequest(cartModel, removeEntries, cancelBookingRequest));
		return cancelBookingRequest;
	}


	/**
	 * @return the handlers
	 */
	protected List<CancelBookingForCartRequestHandler> getHandlers()
	{
		return handlers;
	}

	/**
	 * @param handlers the handlers to set
	 */
	@Required
	public void setHandlers(final List<CancelBookingForCartRequestHandler> handlers)
	{
		this.handlers = handlers;
	}

	/**
	 * @return the integrationUtils
	 */
	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	/**
	 * @param integrationUtils the integrationUtils to set
	 */
	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}

}
