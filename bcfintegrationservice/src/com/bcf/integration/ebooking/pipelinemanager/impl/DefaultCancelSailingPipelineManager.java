/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.ebooking.pipelinemanager.CancelSailingPipelineManager;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForOrderRequestHandler;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;


public class DefaultCancelSailingPipelineManager implements CancelSailingPipelineManager
{
	private List<CancelBookingForOrderRequestHandler> handlers;

	@Override
	public CancelBookingRequestForOrder createRequest(final OrderModel order, final List<AbstractOrderEntryModel> removeEntries)
	{
		final CancelBookingRequestForOrder cancelBookingRequest = new CancelBookingRequestForOrder();
		getHandlers().forEach(handler -> handler.handle(order, removeEntries, cancelBookingRequest));
		return cancelBookingRequest;
	}

	/**
	 * @return the handlers
	 */
	protected List<CancelBookingForOrderRequestHandler> getHandlers()
	{
		return handlers;
	}

	/**
	 * @param handlers the handlers to set
	 */
	@Required
	public void setHandlers(final List<CancelBookingForOrderRequestHandler> handlers)
	{
		this.handlers = handlers;
	}
}
