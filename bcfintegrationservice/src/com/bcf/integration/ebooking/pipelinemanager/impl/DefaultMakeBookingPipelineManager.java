/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.MakeBookingPipelineManager;
import com.bcf.integration.ebooking.pipelinemanager.handler.MakeBookingRequestHandler;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public class DefaultMakeBookingPipelineManager implements MakeBookingPipelineManager
{
	private Map<String, List<MakeBookingRequestHandler>> handlersMap;


	@Override
	public BCFMakeBookingRequest processRequest(final MakeBookingRequestData makeBookingRequestData)
	{
		final BCFMakeBookingRequest request = new BCFMakeBookingRequest();

		getHandlersMap().get("UPDATE_CART").forEach(handler -> handler.createRequest(makeBookingRequestData, request));

		return request;
	}

	@Override
	public BCFMakeBookingRequest processRequest(final AddBundleToCartRequestData addBundleToCartRequestData)
	{
		final BCFMakeBookingRequest request = new BCFMakeBookingRequest();
		getHandlersMap().get("ADD_BUNDLE").forEach(handler -> handler.createRequest(addBundleToCartRequestData, request));
		return request;
	}

	/**
	 * @return the handlersMap
	 */
	protected Map<String, List<MakeBookingRequestHandler>> getHandlersMap()
	{
		return handlersMap;
	}

	/**
	 * @param handlersMap the handlersMap to set
	 */
	@Required
	public void setHandlersMap(final Map<String, List<MakeBookingRequestHandler>> handlersMap)
	{
		this.handlersMap = handlersMap;
	}

}
