/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.handler.MakeBookingRequestHandler;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public class MakeBookingProductRequestHandler extends AbstractMakeBookingRequestHandler
{
	private Map<String, MakeBookingRequestHandler> makeBookingProductHandlerMap;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		if (CollectionUtils.isNotEmpty(makeBookingRequestData.getSailingEntries()))
		{
			final Map<String, List<AbstractOrderEntryModel>> sailEntries = makeBookingRequestData.getSailingEntries().stream()
					.collect(Collectors.groupingBy(entry -> entry.getProduct().getItemtype()));
			if (MapUtils.isNotEmpty(sailEntries))
			{
				sailEntries.keySet().forEach(
						key -> getMakeBookingProductHandlerMap().get(key).createRequest(makeBookingRequestData, request));
			}
		}
		else
		{
			final AddBundleToCartRequestData data = makeBookingRequestData.getAddBundleToCartRequestData();
			getMakeBookingProductHandlerMap().get(FareProductModel._TYPECODE).createRequest(data, request);
		}
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		getMakeBookingProductHandlerMap().get(FareProductModel._TYPECODE).createRequest(makeBookingRequestData, request);
	}

	protected Map<String, MakeBookingRequestHandler> getMakeBookingProductHandlerMap()
	{
		return makeBookingProductHandlerMap;
	}

	@Required
	public void setMakeBookingProductHandlerMap(final Map<String, MakeBookingRequestHandler> makeBookingProductHandlerMap)
	{
		this.makeBookingProductHandlerMap = makeBookingProductHandlerMap;
	}

}
