/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public class MakeBookingAncillaryProductRequestHandler extends AbstractMakeBookingProductRequestHandler
{
	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		final Map<String, List<AbstractOrderEntryModel>> ancillaries;
		if (CollectionUtils.isNotEmpty(makeBookingRequestData.getSailingEntries()))
		{
			ancillaries = makeBookingRequestData.getSailingEntries().stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE))
					.collect(Collectors.groupingBy(entry -> entry.getProduct().getCode()));

		}
		else
		{
			final CartModel cart = makeBookingRequestData.getCartModel();
			ancillaries = cart.getEntries().stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE))
					.collect(Collectors.groupingBy(entry -> entry.getProduct().getCode()));
		}

		if (Objects.isNull(request.getProducts()))
		{
			request.setProducts(createEmptyProducts());
		}

		ancillaries.keySet().forEach(ancillaryCode -> {
			final List<AbstractOrderEntryModel> ancillaryEntries = ancillaries.get(ancillaryCode);
			int quantity = ancillaryEntries.stream().mapToInt(ancillaryEntry -> ancillaryEntry.getQuantity().intValue()).sum();
			if (StreamUtil.safeStream(makeBookingRequestData.getAddProductToCartRequestDatas()).anyMatch(addProductToCartRequestData
					-> StringUtils.equalsIgnoreCase(addProductToCartRequestData.getProductCode(), ancillaryCode)))
			{
				quantity = (int) makeBookingRequestData.getAddProductToCartRequestDatas().stream().filter(addProductToCartRequestData
						-> StringUtils.equalsIgnoreCase(addProductToCartRequestData.getProductCode(), ancillaryCode)).findFirst().get()
						.getQty();
			}
			request.getProducts().getProduct().add(createAncillaryProduct(ancillaryCode, quantity, null));
		});
	}
}
