/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.integration.common.data.Product;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public class MakeBookingUpdateProductListRequestHandler extends AbstractMakeBookingProductRequestHandler
{
	private static final String AMENITY = "AMENITY";

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		createProductList(makeBookingRequestData.getAddProductToCartRequestDatas(), request);
		updateProductList(request);
	}

	private void createProductList(final List<AddProductToCartRequestData> addProductToCartRequestDatas,
			final BCFMakeBookingRequest request)
	{
		if (CollectionUtils.isNotEmpty(addProductToCartRequestDatas)
				&& Objects.nonNull(request.getProducts()) && CollectionUtils.isNotEmpty(request.getProducts().getProduct()))
		{
			final Map<String, List<AddProductToCartRequestData>> stringListMap = addProductToCartRequestDatas.stream()
					.collect(Collectors.groupingBy(AddProductToCartRequestData::getProductCode));
			stringListMap.entrySet().forEach(stringListEntry -> {
				final Optional<Product> requestProduct = request.getProducts().getProduct().stream()
						.filter(product -> product.getProductId().equals(stringListEntry.getKey())).findAny();
				if (requestProduct.isPresent())
				{
					final int quantity = (int) stringListEntry.getValue().stream().mapToLong(value -> value.getQty()).sum();
					if (quantity > 0)
					{
						requestProduct.get().setProductNumber(quantity);
					}
					else if (quantity <= 0)
					{
						request.getProducts().getProduct().remove(requestProduct.get());
					}
				}
				else
				{
					createProduct(stringListEntry.getValue().get(0), request,
							(int) stringListEntry.getValue().stream().mapToLong(value -> value.getQty()).sum());
				}
			});
		}
	}

	private void updateProductList(final BCFMakeBookingRequest request)
	{
		if (Objects.nonNull(request.getProducts()) && CollectionUtils.isNotEmpty(request.getProducts().getProduct()))
		{
			final List<Product> products = new ArrayList<>();
			request.getProducts().getProduct().stream().forEach(product -> {
				if (StringUtils.equals(AMENITY, product.getProductCategory()) && product.getProductNumber() > 1)
				{
					for (long count = 1; count < product.getProductNumber(); count++)
					{
						products.add(createAncillaryProduct(product.getProductId(), 1, null));
					}
					product.setProductNumber(1);
				}
			});
			request.getProducts().getProduct().addAll(products);
			request.getProducts().getProduct().removeIf(s -> s.getProductNumber() < 1);
		}
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{

		createProductList(makeBookingRequestData.getAddProductToCartRequestDatas(), request);
	}

	private void createProduct(final AddProductToCartRequestData addProductToCartData, final BCFMakeBookingRequest request,
			final int qty)
	{
		if (AncillaryProductModel._TYPECODE.equals(addProductToCartData.getProductType()))
		{
			request.getProducts().getProduct()
					.add(createAncillaryProduct(addProductToCartData.getProductCode(), qty, null));
		}
	}
}
