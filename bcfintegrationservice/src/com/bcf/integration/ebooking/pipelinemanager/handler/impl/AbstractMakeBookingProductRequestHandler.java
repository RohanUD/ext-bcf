/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.data.VehicleDimension;
import com.bcf.facades.cart.PassengerRequestData;
import com.bcf.facades.cart.VehicleRequestData;
import com.bcf.integration.common.data.AmenityInfo;
import com.bcf.integration.common.data.CustomerInfo;
import com.bcf.integration.common.data.VehicleInfo;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.pipelinemanager.util.VehicleDimensionUtil;
import com.bcf.integrations.common.Product;


public abstract class AbstractMakeBookingProductRequestHandler extends AbstractMakeBookingRequestHandler
{
	protected void createAncillaryProduct(final List<PassengerTypeQuantityData> passengerTypeQuantityDataList)
	{
		if (CollectionUtils.size(passengerTypeQuantityDataList) > 0)
		{
			for (final PassengerTypeQuantityData passengerTypes : passengerTypeQuantityDataList)
			{
				if (passengerTypes.getQuantity() > 0)
				{
					createPassengerProduct(passengerTypes.getPassengerType().getBcfCode(), passengerTypes.getQuantity(),
							Collections.emptyList());
				}
			}
		}
	}

	protected com.bcf.integration.common.data.Products createEmptyProducts()
	{
		final com.bcf.integration.common.data.Products products = new com.bcf.integration.common.data.Products();
		products.setProduct(new ArrayList<>());
		return products;
	}

	protected com.bcf.integration.common.data.Product createAncillaryProduct(final String ebookingCode, final int quantity,
			String amenityType)
	{
		if (StringUtils.isBlank(amenityType))
		{
			final ProductModel product = getProductService().getProductForCode(ebookingCode);
			if (product instanceof AncillaryProductModel)
			{
				final AncillaryProductModel ancillaryProductModel = (AncillaryProductModel) product;
				if (Objects.nonNull(ancillaryProductModel.getType()))
				{
					amenityType = ancillaryProductModel.getType().getCode();
				}
			}
		}
		final com.bcf.integration.common.data.Product product = new com.bcf.integration.common.data.Product();
		product.setProductId(ebookingCode);
		product.setProductNumber(quantity);
		product.setProductCategory(BcfCoreConstants.CATEGORY_AMENITY);
		final AmenityInfo amenityInfo = new AmenityInfo();
		amenityInfo.setAmenityType(amenityType);
		product.setAmenityInfo(Arrays.asList(amenityInfo));
		return product;
	}

	protected Product createFeeProduct(final String ebookingCode, final int quantity)
	{
		final Product product = new Product();
		product.setProductId(ebookingCode);
		product.setProductNumber(quantity);
		product.setProductCategory(BcfCoreConstants.CATEGORY_OTHER);
		return product;
	}

	protected com.bcf.integration.common.data.Product createPassengerProduct(final String ebookingCode, final int quantity,
			final List<PassengerRequestData> filteredPassengerRequestData)
	{
		final com.bcf.integration.common.data.Product product = new com.bcf.integration.common.data.Product();
		final List<CustomerInfo> passengerInfoList = new ArrayList<>();
		product.setProductId(ebookingCode);
		product.setProductNumber(quantity);
		product.setProductCategory(BcfCoreConstants.CATEGORY_PASSENGER);
		if (CollectionUtils.isNotEmpty(filteredPassengerRequestData))
		{
			filteredPassengerRequestData.forEach(passengerRequestData -> {
				final CustomerInfo passengerInfo = new CustomerInfo();
				passengerInfo.setCustomerNumber(passengerRequestData.getCustomerNumber());
				passengerInfo.setPassengerCode(passengerRequestData.getPassengerCode());
				passengerInfo.setFirstName(passengerRequestData.getFirstName());
				passengerInfo.setLastName(passengerRequestData.getLastName());
				passengerInfo.setPhone(passengerRequestData.getPhone());
				if (StringUtils.isNotBlank(passengerRequestData.getEmailAddress()))
				{
					passengerInfo.setEmailAddress(passengerRequestData.getEmailAddress());
				}
				passengerInfoList.add(passengerInfo);
			});
			product.setPassengerInfo(passengerInfoList);
		}
		return product;
	}

	protected com.bcf.integration.common.data.Product createVehicleProduct(final String ebookingCode, final int quantity,
			final VehicleDimension vehicleDimension, final VehicleRequestData vehicleRequestData, final boolean isCarryingLivestock)
	{
		String newEbookingCode = ebookingCode;
		final com.bcf.integration.common.data.Product product = new com.bcf.integration.common.data.Product();
		if (isCarryingLivestock && (BcfCoreConstants.UNDER_HEIGHT
				.equalsIgnoreCase(ebookingCode) || BcfCoreConstants.OVERSIZE
				.equalsIgnoreCase(ebookingCode)))
		{
			newEbookingCode = newEbookingCode + BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX;
		}
		product.setProductId(newEbookingCode);
		final VehicleInfo vehicleInfo = new VehicleInfo();
		VehicleDimensionUtil.setVehicleDimensions(vehicleInfo, vehicleDimension, ebookingCode);
		if (Objects.nonNull(vehicleRequestData) && StringUtils.isNotBlank(vehicleRequestData.getVehicleLicense()))
		{
			vehicleInfo.setVehicleLicense(vehicleRequestData.getVehicleLicense());
			vehicleInfo.setVehicleNationality(vehicleRequestData.getVehicleNationality());
			vehicleInfo.setVehicleProvince(vehicleRequestData.getVehicleProvince());
		}
		product.setProductNumber(quantity);
		product.setProductCategory(BcfCoreConstants.CATEGORY_VEHICLE);
		product.setVehicleInfo(vehicleInfo);
		return product;
	}

	protected VehicleDimension populateVehicleDimensions(final double length, final double width, final double height,
			final double weight)
	{
		final VehicleDimension vehicleDimension = new VehicleDimension();
		vehicleDimension.setProductHeight(height);
		vehicleDimension.setProductLength(length);
		vehicleDimension.setProductWeight(weight);
		vehicleDimension.setProductWidth(width);
		return vehicleDimension;
	}
}
