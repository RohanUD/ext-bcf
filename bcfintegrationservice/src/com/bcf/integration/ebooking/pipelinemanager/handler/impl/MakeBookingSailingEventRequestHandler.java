/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.services.TravelRouteService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.integration.common.data.BCfSailingEventData;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;
import com.bcf.integrations.booking.response.OpenTicket;


public class MakeBookingSailingEventRequestHandler extends AbstractMakeBookingRequestHandler
{
	private BCFTravelCartService bcfTravelCartService;
	private TravelRouteService travelRouteService;
	private UserService userService;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		/**
		 * add bundle to cart data needs to be populated for amending travelers as well properly so that the below code
		 * can be used for amend travelers
		 */
		if (bcfTravelCartService.isAmendmentCart(null) && Objects.nonNull(makeBookingRequestData.getAddBundleToCartRequestData()))
		{
			createRequest(makeBookingRequestData.getAddBundleToCartRequestData(), request);
		}

		if (CollectionUtils.isNotEmpty(makeBookingRequestData.getSailingEntries()))
		{
			final int originDestinationRefNumber;
			final int journeyRefNumber;
			if (CollectionUtils.isNotEmpty(makeBookingRequestData.getAddProductToCartRequestDatas()))
			{
				final AddProductToCartRequestData addProductToCartRequestData = makeBookingRequestData
						.getAddProductToCartRequestDatas()
						.get(0);
				originDestinationRefNumber = addProductToCartRequestData.getOriginDestinationRefNumber();
				journeyRefNumber = addProductToCartRequestData.getJourneyRefNumber();
			}
			else
			{
				originDestinationRefNumber = makeBookingRequestData.getSailingEntries().get(0).getTravelOrderEntryInfo()
						.getOriginDestinationRefNumber();
				journeyRefNumber = makeBookingRequestData.getSailingEntries().get(0).getJourneyReferenceNumber();
			}

			final Optional<AbstractOrderEntryModel> optionalOrderEntry = makeBookingRequestData.getSailingEntries().stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == originDestinationRefNumber
							&& entry.getJourneyReferenceNumber() == journeyRefNumber)
					.findFirst();
			if (optionalOrderEntry.isPresent() && Objects.nonNull(optionalOrderEntry.get().getTravelOrderEntryInfo()))
			{
				final TravelOrderEntryInfoModel travelOrderEntryInfo = optionalOrderEntry.get().getTravelOrderEntryInfo();
				final BCfSailingEventData event = new BCfSailingEventData();
				event.setDeparturePortCode(travelOrderEntryInfo.getTravelRoute().getOrigin().getCode());
				event.setArrivalPortCode(travelOrderEntryInfo.getTravelRoute().getDestination().getCode());

				final Date departureDate = travelOrderEntryInfo.getTransportOfferings().iterator().next().getDepartureTime();
				if (makeBookingRequestData.isOpenTicket())
				{
					setOpenTicket(request, departureDate);
				}

				event.setDepartureDateTime(
						TravelDateUtils.convertDateToStringDate(departureDate, BcfintegrationcoreConstants.BCF_DATE_TIME_PATTERN));

				request.setSailingEvent(event);

				final String bundleType = optionalOrderEntry.get().getBundleTemplate().getParentTemplate().getType().getCode();
				setSailingPrice(bundleType, request);


			}
		}

	}

	protected void setSailingPrice(final String bundleType, final BCFMakeBookingRequest request)
	{
		final SailingPrices sailingPrices = new SailingPrices();
		sailingPrices.setTariffType(bundleType);
		request.setSailingPrice(sailingPrices);
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		super.createRequest(makeBookingRequestData, request);

		final String travelRouteCode = makeBookingRequestData.getAddBundleToCartData().get(0).getTravelRouteCode();
		final TravelRouteModel travelRouteModel = getTravelRouteService().getTravelRoute(travelRouteCode);
		final String origin = travelRouteModel.getOrigin().getCode();
		final String destination = travelRouteModel.getDestination().getCode();

		final BCfSailingEventData event = new BCfSailingEventData();
		event.setDeparturePortCode(origin);
		event.setArrivalPortCode(destination);
		request.setSailingEvent(event);

		final Date departureDate = ((BcfAddBundleToCartData) (makeBookingRequestData.getAddBundleToCartData().get(0)))
				.getTransportOfferingDatas().get(0).getDepartureTime();
		event.setDepartureDateTime(
				TravelDateUtils.convertDateToStringDate(departureDate, BcfintegrationcoreConstants.BCF_DATE_TIME_PATTERN));

		if (makeBookingRequestData.isOpenTicket())
		{
			setOpenTicket(request, departureDate);
		}

		final CartModel cart = getBcfTravelCartService().getSessionCart();
		List<String> cachingKeys = Collections.emptyList();
		if (makeBookingRequestData.isAddCachingKeys())
		{
			final boolean anonymousUser = getUserService().isAnonymousUser(getUserService().getCurrentUser()) || bcfTravelCartService
					.isGuestUserCart(bcfTravelCartService.getSessionCart());
			if (!anonymousUser)
			{
				cachingKeys = getBcfTravelCartService().getOrderCachingKeys(cart);
			}

		}
		if (CollectionUtils.isNotEmpty(cachingKeys))
		{
			final List<com.bcf.integration.common.data.BookingReferences> tempBookingRefs = getTempBookingReferences(cachingKeys);
			if (CollectionUtils.isNotEmpty(tempBookingRefs))
			{
				request.setTempBookingReferences(getTempBookingReferences(cachingKeys));
			}
		}
	}

	private void setOpenTicket(final BCFMakeBookingRequest request, final Date openTicketDepartureDate)
	{
		final OpenTicket openTicket = new OpenTicket();
		openTicket.setIsOpenTicket(true);
		openTicket.setOpenTicketExpiryDate(TravelDateUtils
				.convertDateToStringDate(openTicketDepartureDate, BcfintegrationcoreConstants.DATE_PATTERN_YYYY_MM_DD));
		request.setOpenTicket(openTicket);
	}

	protected List<com.bcf.integration.common.data.BookingReferences> getTempBookingReferences(final List<String> cartCachingKeys)
	{
		if (CollectionUtils.isNotEmpty(cartCachingKeys))
		{
			final List<com.bcf.integration.common.data.BookingReferences> tempBookingReferences = new ArrayList<>(
					cartCachingKeys.size());
			cartCachingKeys.forEach(cachingKey -> {
				if (cachingKey != null)
				{
					final com.bcf.integration.common.data.BookingReferences bookingReference = new com.bcf.integration.common.data.BookingReferences();
					bookingReference.setCachingKey(cachingKey);
					tempBookingReferences.add(bookingReference);
				}
			});
			return tempBookingReferences;
		}

		return Collections.emptyList();
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	@Required
	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
