/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForOrderRequestHandler;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.common.BookingClientSystem;



public class CancelSailingBookingClientHandler implements CancelBookingForOrderRequestHandler
{
	private IntegrationUtility integrationUtils;

	@Override
	public void handle(final OrderModel order, final List<AbstractOrderEntryModel> removeEntries,
			final CancelBookingRequestForOrder cancelBookingRequest)
	{
		if (Objects.nonNull(order))
		{
			final BookingClientSystem bookingClientSystem = new BookingClientSystem();
			bookingClientSystem.setClientCode(getIntegrationUtils().getClientCode(order.getSalesApplication()));
			cancelBookingRequest.setBookingClientSystem(bookingClientSystem);
		}
	}

	/**
	 * @return the integrationUtils
	 */
	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	/**
	 * @param integrationUtils the integrationUtils to set
	 */
	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}
}
