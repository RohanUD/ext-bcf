/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.user.TravellerModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.PassengerRequestData;
import com.bcf.facades.cart.VehicleRequestData;
import com.bcf.facades.ferry.AccessibilitytData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.common.data.Product;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public class MakeBookingFareProductRequestHandler extends AbstractMakeBookingProductRequestHandler
{
	PassengerTypeService passengerTypeService;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		final Map<String, com.bcf.integration.common.data.Product> individualSailMap = new HashMap<>();
		if (makeBookingRequestData.getSailingEntries() != null)
		{
			makeBookingRequestData.getSailingEntries().stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE))
					.forEach(sailEntry -> {
						final TravellerModel traveller = sailEntry.getTravelOrderEntryInfo().getTravellers().iterator().next();
						String ebookingProductCode = Strings.EMPTY;
						if (traveller.getType().equals(TravellerType.PASSENGER))
						{
							ebookingProductCode = populatePassengerProduct(traveller, individualSailMap);

						}
						else if (traveller.getType().equals(TravellerType.VEHICLE))
						{
							ebookingProductCode = populateVehicleProduct(traveller, individualSailMap);
						}

						final List<AbstractOrderEntryModel> products = Objects
								.nonNull(makeBookingRequestData.getProductCodeEntryMap().get(ebookingProductCode))
								? makeBookingRequestData.getProductCodeEntryMap().get(ebookingProductCode)
								: new ArrayList<>();
						products.add(sailEntry);
						makeBookingRequestData.getProductCodeEntryMap().put(ebookingProductCode, products);
					});
		}
		else
		{
			final AddBundleToCartRequestData data = makeBookingRequestData.getAddBundleToCartRequestData();
			createRequest(data, request);
		}
		if (Objects.isNull(request.getProducts()))
		{
			request.setProducts(createEmptyProducts());
		}
		request.getProducts().getProduct().addAll(individualSailMap.values());
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData requestData, final BCFMakeBookingRequest request)
	{
		final com.bcf.integration.common.data.Products products1 = new com.bcf.integration.common.data.Products();
		final List<com.bcf.integration.common.data.Product> product1List = new ArrayList<>();
		populatePassengerProduct(requestData.getPassengerTypes(), product1List, requestData.getPassengerTravellers());
		populateVehicleProduct(requestData.getVehicleTypes(), product1List, requestData.getVehicleTraveller());
		populateAncillaryProduct(requestData.getAccessibilityDatas(), product1List);
		populateLargeItemsProduct(requestData.getLargeItems(), product1List);
		products1.setProduct(product1List);
		request.setProducts(products1);
	}

	protected void populateAncillaryProduct(final List<AccessibilitytData> accessibilitytDataList,
			final List<com.bcf.integration.common.data.Product> productList)
	{
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(accessibilitytDataList))
		{
			final Map<String, List<AccessibilitytData>> accessibilityDataCodeMap = accessibilitytDataList
					.stream()
					.collect(Collectors.groupingBy(accessibility -> accessibility.getCode()));

			for (final Map.Entry<String, List<AccessibilitytData>> mapEntry : accessibilityDataCodeMap.entrySet())
			{
				productList.add(createAncillaryProduct(mapEntry.getKey(), mapEntry.getValue().size(),
						mapEntry.getValue().stream().findFirst().get().getType()));
			}
		}
	}

	protected void populateLargeItemsProduct(final List<LargeItemData> largeItemDataList,
			final List<Product> productList)
	{
		StreamUtil.safeStream(largeItemDataList).forEach(largeItemData -> {
			if (largeItemData.getQuantity() > 0)
			{
				final Product product = new Product();
				product.setProductNumber(largeItemData.getQuantity());
				product.setProductId(largeItemData.getCode());
				product.setProductCategory(BcfCoreConstants.CATEGORY_STOWAGE);
				productList.add(product);
			}
		});
	}

	protected void populatePassengerProduct(final List<PassengerTypeQuantityData> passengerTypeQuantityDatas,
			final List<com.bcf.integration.common.data.Product> productList,
			final List<PassengerRequestData> passengerTravellerRequestDataList)
	{
		final List<PassengerTypeModel> passengerTypeModels = getPassengerTypeService().getPassengerTypes();
		if (CollectionUtils.size(passengerTypeQuantityDatas) > 0)
		{
			for (final PassengerTypeQuantityData passengerType : passengerTypeQuantityDatas)
			{
				if (passengerType.getQuantity() > 0)
				{
					final PassengerTypeModel passengerTypeModel = passengerTypeModels.stream()
							.filter(passenger -> passengerType.getPassengerType().getCode().equals(passenger.getCode())).findFirst()
							.get();
					final List<PassengerRequestData> filteredPassengerRequestData = CollectionUtils
							.isNotEmpty(passengerTravellerRequestDataList) ? passengerTravellerRequestDataList.stream()
							.filter(passengerRequestData -> passengerRequestData.getPassengerCode()
									.equals(passengerTypeModel.getEBookingCode())).collect(Collectors.toList()) :
							Collections.emptyList();
					productList
							.add(createPassengerProduct(passengerTypeModel.getEBookingCode(), passengerType.getQuantity(),
									filteredPassengerRequestData));
				}
			}
		}
	}

	protected void populateVehicleProduct(final List<VehicleTypeQuantityData> vehicleTypeQuantityDatas,
			final List<com.bcf.integration.common.data.Product> productList,
			final VehicleRequestData vehicleRequestData)
	{
		if (CollectionUtils.size(vehicleTypeQuantityDatas) > 0)
		{
			for (final VehicleTypeQuantityData vehicleTypes : vehicleTypeQuantityDatas)
			{
				if (vehicleTypes.getQty() > 0)
				{
					productList.add(createVehicleProduct(vehicleTypes.getVehicleType().getCode(), vehicleTypes.getQty(),
							populateVehicleDimensions(vehicleTypes.getLength(), vehicleTypes.getWidth(),
									vehicleTypes.getHeight(), vehicleTypes.getWeight()),
							Objects.nonNull(vehicleRequestData) ? vehicleRequestData : null, vehicleTypes.isCarryingLivestock()));
				}
			}
		}
	}


	protected String populatePassengerProduct(final TravellerModel traveller,
			final Map<String, com.bcf.integration.common.data.Product> individualSailMap)
	{
		final String ebookingProductCode = ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
				.getEBookingCode();
		com.bcf.integration.common.data.Product product =
				MapUtils.isNotEmpty(individualSailMap) && individualSailMap.containsKey(ebookingProductCode)
						? individualSailMap.get(ebookingProductCode)
						: null;
		if (product == null)
		{
			product = createPassengerProduct(ebookingProductCode, 1, Collections.emptyList());
		}
		else
		{
			product.setProductNumber(product.getProductNumber() + 1);
		}

		individualSailMap.put(ebookingProductCode, product);

		return ebookingProductCode;
	}

	protected String populateVehicleProduct(final TravellerModel traveller,
			final Map<String, com.bcf.integration.common.data.Product> individualSailMap)
	{
		final VehicleTypeModel vehicleModel = ((BCFVehicleInformationModel) traveller.getInfo()).getVehicleType();
		final String ebookingProductCode = vehicleModel.getType().getCode();
		com.bcf.integration.common.data.Product product =
				MapUtils.isNotEmpty(individualSailMap) && individualSailMap.containsKey(ebookingProductCode)
						? individualSailMap.get(ebookingProductCode)
						: null;
		if (product == null)
		{
			final BCFVehicleInformationModel vehicleInfo = ((BCFVehicleInformationModel) traveller.getInfo());
			product =
					createVehicleProduct(ebookingProductCode, 1,
							populateVehicleDimensions(vehicleInfo.getLength(), vehicleInfo.getWidth(), vehicleInfo.getHeight(),
									vehicleInfo.getWeight()), null, vehicleInfo.getCarryingLivestock());
		}
		else
		{
			product.setProductNumber(product.getProductNumber() + 1);
		}
		individualSailMap.put(ebookingProductCode, product);

		return ebookingProductCode;
	}

	public PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

}
