/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForCartRequestHandler;
import com.bcf.integrations.cancelbooking.cart.request.CancelBookingRequestForCart;
import com.bcf.integrations.common.BookingReferences;


public class CancelBookingForCartBookingReferenceHandler implements CancelBookingForCartRequestHandler
{
	@Override
	public void populateRequest(final CartModel cartModel, final List<AbstractOrderEntryModel> removeEntries,
			final CancelBookingRequestForCart cancelBookingRequest)
	{
		final List<BookingReferences> bookingReferences = new ArrayList<>();
		final Set<String> uniqueBookingReferences = new HashSet<>();
		StreamUtil.safeStream(removeEntries).forEach(entry -> {
			if (StringUtils.isNotBlank(entry.getBookingReference()))
			{
				uniqueBookingReferences.add(entry.getBookingReference());
			}
		});
		StreamUtil.safeStream(uniqueBookingReferences).forEach(bookingReference ->
				bookingReferences.add(createBookingReference(bookingReference))
		);
		cancelBookingRequest.setBookingReferences(bookingReferences);
		cancelBookingRequest.setSendNotification(false);
	}

	protected BookingReferences createBookingReference(final String eBookingReference)
	{
		final BookingReferences bookingReference = new BookingReferences();
		bookingReference.setBookingReference(eBookingReference);
		return bookingReference;
	}
}
