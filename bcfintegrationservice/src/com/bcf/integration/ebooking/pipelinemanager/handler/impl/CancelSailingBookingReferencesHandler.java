/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.ArrayList;
import java.util.List;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForOrderRequestHandler;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;
import com.bcf.integrations.common.BookingReferences;


public class CancelSailingBookingReferencesHandler implements CancelBookingForOrderRequestHandler
{
	@Override
	public void handle(final OrderModel order, final List<AbstractOrderEntryModel> removeEntries,
			final CancelBookingRequestForOrder cancelBookingRequest)
	{
		final List<BookingReferences> bookingReferences = new ArrayList<>();
		removeEntries.stream().map(AbstractOrderEntryModel::getBookingReference).distinct().forEach(entryBookingReference -> {

			final BookingReferences bookingReference = new BookingReferences();
			bookingReference.setBookingReference(entryBookingReference);
			bookingReferences.add(bookingReference);
		});

		cancelBookingRequest.setBookingReferences(bookingReferences);
	}
}
