/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.helper.BcfSpecialInstructionsHelper;
import com.bcf.integration.ebooking.pipelinemanager.handler.MakeBookingRequestHandler;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public abstract class AbstractMakeBookingRequestHandler implements MakeBookingRequestHandler
{
	private ConfigurationService configurationService;
	private ProductService productService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private BcfSpecialInstructionsHelper bcfSpecialInstructionsHelper;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		request.setOverrideHidePricing(true);
		request.setSendNotification(false);
		request.setExpirationSeconds(makeBookingRequestData.getHoldTimeInSeconds());
		final String specialInstructions = getBcfSpecialInstructionsHelper().getSpecialInstructions(makeBookingRequestData);
		if (StringUtils.isNotBlank(specialInstructions))
		{
			request.setSpecialInstructions(specialInstructions);
		}
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		request.setOverrideHidePricing(true);
		request.setSendNotification(false);
		request.setExpirationSeconds(makeBookingRequestData.getHoldTimeInSeconds());
		final String specialInstructions = getBcfSpecialInstructionsHelper().getSpecialInstructions(makeBookingRequestData);
		if (StringUtils.isNotBlank(specialInstructions))
		{
			request.setSpecialInstructions(specialInstructions);
		}
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected BcfSpecialInstructionsHelper getBcfSpecialInstructionsHelper()
	{
		return bcfSpecialInstructionsHelper;
	}

	@Required
	public void setBcfSpecialInstructionsHelper(
			final BcfSpecialInstructionsHelper bcfSpecialInstructionsHelper)
	{
		this.bcfSpecialInstructionsHelper = bcfSpecialInstructionsHelper;
	}
}
