/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.ebooking.pipelinemanager.handler.CancelBookingForCartRequestHandler;
import com.bcf.integrations.cancelbooking.cart.request.CancelBookingRequestForCart;


public class CancelBookingForCartCustomerInfoRequestHandler implements CancelBookingForCartRequestHandler
{

	private BCFTravelCartService bcfTravelCartService;

	private UserService userService;

	private IntegrationUtility integrationUtility;

	@Override
	public void populateRequest(final CartModel cartModel, final List<AbstractOrderEntryModel> removeEntries,
			final CancelBookingRequestForCart cancelBookingRequest)
	{
		final UserModel user = getUserService().getCurrentUser();
		if (!getUserService().isAnonymousUser(user))
		{
			cancelBookingRequest.setCustomerInfo(getIntegrationUtility().createCustomerInfo((CustomerModel) user));
		}

	}

	/**
	 * @return the bcfTravelCartService
	 */
	public BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Required
	public IntegrationUtility getIntegrationUtility()
	{
		return integrationUtility;
	}

	public void setIntegrationUtility(final IntegrationUtility integrationUtility)
	{
		this.integrationUtility = integrationUtility;
	}
}
