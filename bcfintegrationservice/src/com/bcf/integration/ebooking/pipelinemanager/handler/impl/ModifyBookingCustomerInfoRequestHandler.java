/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.handler.ModifyBookingRequestHandler;
import com.bcf.integrations.booking.request.ConvertQuotationToOptionRequest;


public class ModifyBookingCustomerInfoRequestHandler implements ModifyBookingRequestHandler
{
	private IntegrationUtility integrationUtils;
	private UserService userService;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final ConvertQuotationToOptionRequest request)
	{
		final UserModel user = getUserService().getCurrentUser();
		request.setCustomerInfo(getIntegrationUtils().createCustomerInfo((CustomerModel) user));
	}


	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
