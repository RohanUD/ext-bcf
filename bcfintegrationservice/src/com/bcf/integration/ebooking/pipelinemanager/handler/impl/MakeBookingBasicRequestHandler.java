/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.core.enums.AccountType;
import com.bcf.core.model.AmountToPayInfoModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.integration.common.data.BookingReferences;
import com.bcf.integration.common.data.SailingPrices;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;
import com.bcf.integrations.booking.response.OpenTicket;
import com.bcf.integrations.common.BookingClientSystem;
import com.bcf.integrations.common.BookingType;


public class MakeBookingBasicRequestHandler extends AbstractMakeBookingRequestHandler
{
	private IntegrationUtility integrationUtils;
	private UserService userService;
	private BCFTravelCartService bcfTravelCartService;
	private static final String MODIFICATION_JOURNEY = "modify";

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		super.createRequest(makeBookingRequestData, request);
		final BookingClientSystem bokingClient = new BookingClientSystem();
		bokingClient
				.setClientCode(getIntegrationUtils().getClientCode(makeBookingRequestData.getCartModel().getSalesApplication()));
		request.setBookingClientSystem(bokingClient);

		final UserModel user = getUserService().getCurrentUser();
		final boolean isAnonymous =
				getUserService().isAnonymousUser(user) || bcfTravelCartService.isGuestUserCart(bcfTravelCartService.getSessionCart());

		final boolean isAmendmentCart = getBcfTravelCartService().isAmendmentCart(null);

		String journeyType = null;

		if (isAmendmentCart)
		{
			journeyType = MODIFICATION_JOURNEY;
		}


		request.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo((CustomerModel) user));


		if (makeBookingRequestData.isBookingRefUpdate())
		{
			if (CollectionUtils.isNotEmpty(makeBookingRequestData.getCartModel().getAmountToPayInfos()))
			{
				final List<AbstractOrderEntryModel> originalEntries = makeBookingRequestData.getSailingEntries();

				if (CollectionUtils.isNotEmpty(makeBookingRequestData.getSailingEntries()))
				{
					if (isAmendmentCart)
					{
						request.setOriginalBookingReference(getOriginalBookingReference(originalEntries));
					}
					final List<BookingReferences> tempBookingReferences = new ArrayList<>();

					final BookingReferences bookingReference = new BookingReferences();
					bookingReference.setCachingKey(getCachekey(originalEntries));
					bookingReference.setBookingReference(getOriginalBookingReference(originalEntries));
					tempBookingReferences.add(bookingReference);
					request.setRouteReference(getReferenceCode(originalEntries));
					final Integer journeyRef = StreamUtil.safeStream(originalEntries).findAny().get().getJourneyReferenceNumber();
					final Integer odRef = StreamUtil.safeStream(originalEntries).findAny().get().getTravelOrderEntryInfo()
							.getOriginDestinationRefNumber();
					setBookingType(request, isAnonymous, isAmendmentCart, journeyType, journeyRef, odRef);
					setOriginalBookingRefAndType(makeBookingRequestData.getCartModel(), journeyRef, odRef, journeyRef, odRef,
							request, isAnonymous, isAmendmentCart, journeyType);
				}
			}
		}
		else if (Objects.nonNull(makeBookingRequestData.getAddBundleToCartRequestData()))
		{
			BcfAddBundleToCartData bcfAddBundleToCartData = null;
			final List<AddBundleToCartData> addBundleToCartDatas = makeBookingRequestData.getAddBundleToCartRequestData()
					.getAddBundleToCartData();
			if (CollectionUtils.isNotEmpty(addBundleToCartDatas))
			{
				bcfAddBundleToCartData = (BcfAddBundleToCartData) addBundleToCartDatas.get(0);

			}
			setOriginalBookingRefAndType(makeBookingRequestData.getCartModel(),
					makeBookingRequestData.getAddBundleToCartRequestData().getSelectedJourneyRefNumber(),
					makeBookingRequestData.getAddBundleToCartRequestData().getSelectedOdRefNumber(),
					bcfAddBundleToCartData != null ? bcfAddBundleToCartData.getJourneyRefNumber() : null,
					bcfAddBundleToCartData != null ? bcfAddBundleToCartData.getOriginDestinationRefNumber() : null, request,
					isAnonymous, isAmendmentCart, journeyType);
		}
		else if (CollectionUtils.isNotEmpty(makeBookingRequestData.getAddProductToCartRequestDatas()))
		{
			final AddProductToCartRequestData addProductToCartRequestData = makeBookingRequestData.getAddProductToCartRequestDatas()
					.get(0);

			setOriginalBookingRefAndType(makeBookingRequestData.getCartModel(), addProductToCartRequestData.getJourneyRefNumber(),
					addProductToCartRequestData.getOriginDestinationRefNumber(),
					addProductToCartRequestData.getJourneyRefNumber(), addProductToCartRequestData.getOriginDestinationRefNumber(),
					request, isAnonymous, isAmendmentCart, journeyType);
		}
		else
		{
			setBookingType(request, isAnonymous, isAmendmentCart, journeyType, null, null);
		}
	}

	private void setOriginalBookingRefAndType(final AbstractOrderModel cart, final Integer selectedJourneyRefNumber,
			final Integer selectedOdRefNumber, final Integer addBundleToCartJourneyRefNumber,
			final Integer addBundleToCartOdRefNumber, final BCFMakeBookingRequest request,
			final boolean isAnonymous, final boolean isAmendmentCart, final String journeyType)
	{
		final List<AbstractOrderEntryModel> originalEntries = getBcfTravelCartService()
				.getCartEntriesForRefNo(selectedJourneyRefNumber,
						selectedOdRefNumber);
		if (CollectionUtils.isNotEmpty(originalEntries))
		{
			if (isAmendmentCart)
			{
				request.setOriginalBookingReference(getOriginalBookingReference(originalEntries));
			}
			final List<BookingReferences> tempBookingReferences = new ArrayList<>();

			if (isAnonymous || AccountType.SUBSCRIPTION_ONLY
					.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()))
			{
				final BookingReferences bookingReference = new BookingReferences();
				bookingReference.setCachingKey(getCachekey(originalEntries));
				bookingReference.setBookingReference(getOriginalBookingReference(originalEntries));
				tempBookingReferences.add(bookingReference);
				request.setRouteReference(getReferenceCode(originalEntries));
				request.setTempBookingReferences(tempBookingReferences);
			}
			else
			{
				request.setOriginalBookingReference(getOriginalBookingReference(originalEntries));
			}
		}

		setBookingType(request, isAnonymous, isAmendmentCart, journeyType, selectedJourneyRefNumber, selectedOdRefNumber);

		if (isAmendmentCart && StringUtils.isEmpty(request.getOriginalBookingReference()) && addBundleToCartOdRefNumber != null
				&& addBundleToCartJourneyRefNumber != null)
		{

			final List<AbstractOrderEntryModel> entries = cart.getEntries().stream()
					.filter(entry -> entry.getJourneyReferenceNumber() == addBundleToCartJourneyRefNumber
							&& entry.getTravelOrderEntryInfo() != null
							&& entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() != null
							&& addBundleToCartOdRefNumber == entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(entries))
			{
				final AbstractOrderEntryModel abstractOrderEntryModel = entries.stream()
						.filter(entry -> entry.getBookingReference() != null).findAny().orElse(null);
				if (abstractOrderEntryModel != null)
				{
					request.setOriginalBookingReference(abstractOrderEntryModel.getBookingReference());
				}
				request.setRouteReference(getReferenceCode(originalEntries));
				final List<BookingReferences> tempBookingReferences = new ArrayList<>();
				final BookingReferences bookingReference = new BookingReferences();
				bookingReference.setCachingKey(getCachekey(entries));
				bookingReference.setBookingReference(getOriginalBookingReference(entries));
				tempBookingReferences.add(bookingReference);
				request.setTempBookingReferences(tempBookingReferences);
			}
		}
	}

	protected void setBookingType(final BCFMakeBookingRequest request, final boolean isAnonymous, final boolean isAmendmentCart,
			final String journeyType, final Integer journeyRef, final Integer odRef)
	{
		final BookingType bookingType = new BookingType();
		final boolean isAlacarteFlow = bcfTravelCartService.isAlacateFlow();
		if (StringUtils.equals(MODIFICATION_JOURNEY, journeyType) || (hasExistingEntriesForJourneyAndOD(journeyRef, odRef)
				&& !isAnonymous && !AccountType.SUBSCRIPTION_ONLY
				.equals(((CustomerModel) bcfTravelCartService.getSessionCart().getUser()).getAccountType())))
		{
			bookingType.setBookingType(getIntegrationUtils().getModifyBookingType(isAnonymous, isAmendmentCart, isAlacarteFlow));
		}
		else
		{
			bookingType.setBookingType(getIntegrationUtils().getBookingType(isAnonymous, isAmendmentCart, isAlacarteFlow));
		}
		request.setBookingType(bookingType);
	}

	@Override
	public void createRequest(final AddBundleToCartRequestData makeBookingRequestData, final BCFMakeBookingRequest request)
	{
		final UserModel user = getUserService().getCurrentUser();
		final boolean isAnonymous =
				getUserService().isAnonymousUser(user) || bcfTravelCartService.isGuestUserCart(bcfTravelCartService.getSessionCart());
		final boolean isAmendmentCart = getBcfTravelCartService().isAmendmentCart(null);
		final BookingClientSystem bokingClient = new BookingClientSystem();

		if (makeBookingRequestData.isOpenTicket())
		{
			final OpenTicket openTicket = new OpenTicket();
			openTicket.setIsOpenTicket(true);
			request.setOpenTicket(openTicket);
		}

		final CartModel cart = bcfTravelCartService.getSessionCart();
		setOriginalBookingRefAndType(cart, makeBookingRequestData.getSelectedJourneyRefNumber(),
				makeBookingRequestData.getSelectedOdRefNumber(),
				makeBookingRequestData.getSelectedJourneyRefNumber(), makeBookingRequestData.getSelectedOdRefNumber(), request,
				isAnonymous, isAmendmentCart, makeBookingRequestData.getJourneyType());

		bokingClient.setClientCode(getIntegrationUtils()
				.getClientCode(makeBookingRequestData.getSalesApplication(), cart.getBookingJourneyType()));
		request.setBookingClientSystem(bokingClient);

		setSailingPrice(makeBookingRequestData.getBundleType(), request);

		request.setBookingHolderInfo(getIntegrationUtils().createCustomerInfo((CustomerModel) user));

		if (StringUtils.isNotEmpty(makeBookingRequestData.getTransferSailingIdentifier()))
		{
			request.setTransferSailingIdentifier(makeBookingRequestData.getTransferSailingIdentifier());
		}
	}

	protected void setSailingPrice(final String bundleType, final BCFMakeBookingRequest request)
	{
		final SailingPrices sailingPrices = new SailingPrices();
		sailingPrices.setTariffType(bundleType);
		request.setSailingPrice(sailingPrices);
	}

	protected String getOriginalBookingReference(final List<AbstractOrderEntryModel> originalEntries)
	{

		if (CollectionUtils.isNotEmpty(originalEntries))
		{

			final AbstractOrderEntryModel originalEntry = originalEntries.stream()
					.filter(entry -> entry.getBookingReference() != null).findAny().orElse(null);
			if (originalEntry != null)
			{
				return originalEntry.getBookingReference();
			}
		}
		return null;

	}

	protected String getReferenceCode(final List<AbstractOrderEntryModel> originalEntries)
	{
		if (CollectionUtils.isNotEmpty(originalEntries))
		{
			final AbstractOrderEntryModel originalEntry = originalEntries.stream()
					.filter(entry -> entry.getBookingReference() != null).findAny().orElse(null);
			if (originalEntry != null)
			{
				final Collection<AmountToPayInfoModel> amountToPayInfos = originalEntries.get(0).getOrder().getAmountToPayInfos();

				final List<AmountToPayInfoModel> amountToPayInfoModel = amountToPayInfos.stream()
						.filter(entry -> originalEntry.getBookingReference().equalsIgnoreCase(originalEntry.getBookingReference()))
						.collect(Collectors.toList());

				if (CollectionUtils.isNotEmpty(amountToPayInfoModel))
				{
					return amountToPayInfoModel.get(0).getReferenceCode();
				}
			}
		}
		return null;

	}

	protected String getCachekey(final List<AbstractOrderEntryModel> originalEntries)
	{

		if (CollectionUtils.isNotEmpty(originalEntries))
		{

			final AbstractOrderEntryModel originalEntry = originalEntries.stream().filter(entry -> entry.getCacheKey() != null)
					.findAny().orElse(null);
			if (originalEntry != null)
			{
				return originalEntry.getCacheKey();
			}
		}
		return null;

	}

	private boolean hasExistingEntriesForJourneyAndOD(final Integer journeyRef, final Integer odRef)
	{
		if (Objects.nonNull(journeyRef) && Objects.nonNull(odRef))
		{
			return getBcfTravelCartService().getCartEntriesForRefNo(journeyRef, odRef).size() > 0;
		}
		return false;
	}

	/**
	 * @return the integrationUtils
	 */
	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	/**
	 * @param integrationUtils the integrationUtils to set
	 */
	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService the userService to set
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the bcfTravelCartService
	 */
	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	/**
	 * @param bcfTravelCartService the bcfTravelCartService to set
	 */
	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
