/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager.handler.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.common.data.BookingReferences;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.pipelinemanager.handler.ModifyBookingRequestHandler;
import com.bcf.integrations.booking.request.ConvertQuotationToOptionRequest;


public class ModifyBookingTempBookingReferencesRequestHandler implements ModifyBookingRequestHandler
{
	private BCFTravelCartService bcfTravelCartService;

	@Override
	public void createRequest(final MakeBookingRequestData makeBookingRequestData, final ConvertQuotationToOptionRequest request)
	{
		if (Objects.nonNull(getBcfTravelCartService().getSessionCart()) && CollectionUtils
				.isNotEmpty(getBcfTravelCartService().getSessionCart().getEntries()))
		{
			final Map<String, List<AbstractOrderEntryModel>> cartEntriesByCachingKey = StreamUtil
					.safeStream(getBcfTravelCartService()
							.getSessionCart().getEntries()).filter(entry -> StringUtils.isNotBlank(entry.getCacheKey()))
					.collect(Collectors.groupingBy(entry -> entry.getCacheKey()));
			final List<BookingReferences> tempBookingReferences = new ArrayList<>();
			for (final Map.Entry<String, List<AbstractOrderEntryModel>> entry : cartEntriesByCachingKey.entrySet())
			{
				final BookingReferences bookingReference = new BookingReferences();
				bookingReference.setCachingKey(getCachekey(entry.getValue()));
				tempBookingReferences.add(bookingReference);
			}
			request.setTempBookingReferences(tempBookingReferences);
		}
	}

	protected String getCachekey(final List<AbstractOrderEntryModel> originalEntries)
	{

		if (CollectionUtils.isNotEmpty(originalEntries))
		{

			final AbstractOrderEntryModel originalEntry = originalEntries.stream().filter(entry -> entry.getCacheKey() != null)
					.findAny().orElse(null);
			if (originalEntry != null)
			{
				return originalEntry.getCacheKey();
			}
		}
		return null;

	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
