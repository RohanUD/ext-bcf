/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import com.bcf.integrations.cancelbooking.order.request.CancelBookingRequestForOrder;


public interface CancelSailingPipelineManager
{

	/**
	 * Creates the request.
	 *
	 * @param order         the order
	 * @param removeEntries the remove entries
	 * @return the cancel booking request for order
	 */
	CancelBookingRequestForOrder createRequest(OrderModel order, List<AbstractOrderEntryModel> removeEntries);
}
