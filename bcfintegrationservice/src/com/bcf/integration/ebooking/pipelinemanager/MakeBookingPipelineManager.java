/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.ebooking.pipelinemanager;

import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integrations.booking.request.BCFMakeBookingRequest;


public interface MakeBookingPipelineManager
{
	BCFMakeBookingRequest processRequest(MakeBookingRequestData makeBookingRequestData);

	BCFMakeBookingRequest processRequest(AddBundleToCartRequestData addBundleToCartRequestData);

}
