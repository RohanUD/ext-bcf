/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.releasecenter.service.impl;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.SERVICE_NOTICE_PUBLISH_SERVICE_URL;

import com.bcf.integration.releasecenter.strategy.AbstractReleaseCenterStrategy;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;
import com.bcf.notification.request.releasecenter.ReleaseCenterServiceNotificationData;


public class ReleaseCenterTypeServiceStrategy extends AbstractReleaseCenterStrategy
{

	@Override
	protected String getServiceNoticePublishURL()
	{
		return SERVICE_NOTICE_PUBLISH_SERVICE_URL;
	}

	@Override
	protected Class getServiceNoticeNotificationDataClass()
	{
		return ReleaseCenterServiceNotificationData.class;
	}

	@Override
	protected ReleaseCenterNotificationData getServiceNoticeTargetInstance(){
		return new ReleaseCenterServiceNotificationData();
	}

}
