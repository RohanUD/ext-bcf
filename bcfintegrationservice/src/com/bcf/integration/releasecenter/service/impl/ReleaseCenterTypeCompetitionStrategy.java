/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.releasecenter.service.impl;

import static com.bcf.integration.constants.BcfintegrationserviceConstants.SERVICE_NOTICE_PUBLISH_COMPETITION_URL;

import com.bcf.integration.releasecenter.strategy.AbstractReleaseCenterStrategy;
import com.bcf.notification.request.releasecenter.ReleaseCenterCompetitionNotificationData;
import com.bcf.notification.request.releasecenter.ReleaseCenterNewsNotificationData;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class ReleaseCenterTypeCompetitionStrategy extends AbstractReleaseCenterStrategy
{

	@Override
	protected String getServiceNoticePublishURL()
	{
		return SERVICE_NOTICE_PUBLISH_COMPETITION_URL;
	}

	@Override
	protected Class getServiceNoticeNotificationDataClass()
	{
		return ReleaseCenterCompetitionNotificationData.class;
	}

	@Override
	protected ReleaseCenterNotificationData getServiceNoticeTargetInstance()
	{
		return new ReleaseCenterCompetitionNotificationData();
	}
}
