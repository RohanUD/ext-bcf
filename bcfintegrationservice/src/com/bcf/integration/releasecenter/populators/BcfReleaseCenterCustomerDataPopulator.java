/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.releasecenter.populators;

import static com.bcf.core.constants.BcfCoreConstants.CRM_SOURCE_SYSTEM;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.integration.data.SubscriptionCustomerDTO;
import com.bcf.integration.data.SystemIdentifier;
import com.bcf.notification.email.CustomerData;


public class BcfReleaseCenterCustomerDataPopulator implements Populator<SubscriptionCustomerDTO, CustomerData>
{
	@Override
	public void populate(final SubscriptionCustomerDTO source, final CustomerData target) throws ConversionException
	{
		if (CollectionUtils.isNotEmpty(source.getEmailAddress()))
		{
			String emailAddress = source.getEmailAddress().stream().filter(e -> e.isPrimaryInd()).findFirst()
					.orElse(source.getEmailAddress().get(0)).getEmailAddress();
			target.setEmail(emailAddress);
		}
		if (Objects.nonNull(source.getPersonName()))
		{
			target.setFirstName(source.getPersonName().getGivenName());
			target.setLastName(source.getPersonName().getSurname());
		}
		if (CollectionUtils.isNotEmpty(source.getSystemIdentifier()))
		{
			Optional<SystemIdentifier> customerIdOptional = source.getSystemIdentifier().stream()
					.filter(sysIdentifier -> CRM_SOURCE_SYSTEM.equalsIgnoreCase(sysIdentifier.getSourceSystem())).findFirst();

			if (customerIdOptional.isPresent())
			{
				target.setCrmId(customerIdOptional.get().getID());
			}
		}
	}
}
