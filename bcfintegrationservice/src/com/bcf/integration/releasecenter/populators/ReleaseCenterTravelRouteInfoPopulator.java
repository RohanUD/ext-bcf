/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class ReleaseCenterTravelRouteInfoPopulator
		implements Populator<TravelRouteInfo, com.bcf.notification.request.releasecenter.TravelRouteInfo>
{
	@Override
	public void populate(final TravelRouteInfo source,
			final com.bcf.notification.request.releasecenter.TravelRouteInfo target) throws ConversionException
	{
		target.setDestinationLocationName(source.getDestinationLocationName());
		target.setDestinationTerminalCode(source.getDestinationTerminalCode());
		target.setDestinationTerminalName(source.getDestinationTerminalName());
		target.setRouteCode(source.getRouteCode());
		target.setRouteRegion(source.getRouteRegion());
		target.setSourceLocationName(source.getSourceLocationName());
		target.setSourceTerminalCode(source.getSourceTerminalCode());
		target.setSourceTerminalName(source.getSourceTerminalName());
		target.setFerryTrackingLink(source.getFerryTrackingLink());
	}
}
