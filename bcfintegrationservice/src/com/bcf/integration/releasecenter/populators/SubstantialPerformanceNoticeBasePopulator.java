/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;
import com.bcf.notification.request.releasecenter.ReleaseCenterSubstantialPerformanceNoticeNotificationData;


public class SubstantialPerformanceNoticeBasePopulator extends ReleaseCenterBasePopulator implements ReleaseCenterPopulator
{
	@Override
	public void populate(final ServiceNoticeModel source, final ReleaseCenterNotificationData target)
			throws ConversionException
	{
		if (source instanceof SubstantialPerformanceNoticeModel
				&& target instanceof ReleaseCenterSubstantialPerformanceNoticeNotificationData)
		{
			super.populate(source, target);
			final SubstantialPerformanceNoticeModel substantialPerformanceNoticeModel = SubstantialPerformanceNoticeModel.class
					.cast(source);
			final ReleaseCenterSubstantialPerformanceNoticeNotificationData releaseCenterSubstantialPerformanceNoticeNotificationData = ReleaseCenterSubstantialPerformanceNoticeNotificationData.class
					.cast(target);
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setProductOwner(substantialPerformanceNoticeModel.getProductOwner());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setProductOwnerAddress(substantialPerformanceNoticeModel.getProductOwnerAddress());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setContractor(substantialPerformanceNoticeModel.getContractor());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setConsultant(substantialPerformanceNoticeModel.getConsultant());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setSubstantialDate(substantialPerformanceNoticeModel.getSubstantialDate());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setAdditionalNote(substantialPerformanceNoticeModel.getAdditionalNote());
			releaseCenterSubstantialPerformanceNoticeNotificationData
					.setPrimaryContact(substantialPerformanceNoticeModel.getPrimaryContact());
		}
	}
}
