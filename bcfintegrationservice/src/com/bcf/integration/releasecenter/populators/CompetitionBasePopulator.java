/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.notification.request.releasecenter.ReleaseCenterCompetitionNotificationData;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class CompetitionBasePopulator extends ReleaseCenterBasePopulator implements ReleaseCenterPopulator
{
	@Override
	public void populate(final ServiceNoticeModel source, final ReleaseCenterNotificationData target)
			throws ConversionException
	{
		if (source instanceof CompetitionModel && target instanceof ReleaseCenterCompetitionNotificationData)
		{
			super.populate(source, target);
			final CompetitionModel competitionModel = CompetitionModel.class.cast(source);
			final ReleaseCenterCompetitionNotificationData releaseCenterCompetitionNotificationData = ReleaseCenterCompetitionNotificationData.class
					.cast(target);
			releaseCenterCompetitionNotificationData.setName(competitionModel.getName());
			releaseCenterCompetitionNotificationData.setMainContact(competitionModel.getMainContact());
			releaseCenterCompetitionNotificationData.setSecondaryContact(competitionModel.getSecondaryContact());
		}
	}
}
