/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ServiceModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.util.StreamUtil;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class ServiceBasePopulator extends ReleaseCenterBasePopulator implements ReleaseCenterPopulator
{
	@Autowired
	private EnumerationService enumerationService;

	@Override
	public void populate(ServiceNoticeModel source, ReleaseCenterNotificationData target) throws ConversionException
	{
		if (source instanceof ServiceModel)
		{
			super.populate(source, target);

			if (CollectionUtils.isNotEmpty(source.getSubscriptionMasterDetails()))
			{
				List<String> subscriptionDetailsNameList = collectSubscriptionMasterCodes(new ArrayList(source.getSubscriptionMasterDetails()));
				target.setSubscriptionMasterDetails(subscriptionDetailsNameList);
			}
			else if (CollectionUtils.isNotEmpty(source.getSubscriptionGroups()))
			{
				List<String> subscriptionGroupsNameList = StreamUtil.safeStream(source.getSubscriptionGroups())
						.map(s -> getEnumerationService().getEnumerationName(s))
						.collect(Collectors.toList());
				target.setSubscriptionGroups(subscriptionGroupsNameList);

				final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetailModelList = getSubscriptionsMasterDetailsService()
						.getSubscriptionMasterDetailsForSubscriptionGroups(new ArrayList(source.getSubscriptionGroups()));

				List<String> subscriptionDetailsNameList = collectSubscriptionMasterCodes(crmSubscriptionMasterDetailModelList);

				 target.setSubscriptionMasterDetails(subscriptionDetailsNameList);
			}
		}
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
