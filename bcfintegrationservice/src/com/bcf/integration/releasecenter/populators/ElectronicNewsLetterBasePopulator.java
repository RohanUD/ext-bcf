/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.List;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ElectronicNewsLetterModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class ElectronicNewsLetterBasePopulator extends ReleaseCenterBasePopulator implements ReleaseCenterPopulator
{

	@Override
	public void populate(ServiceNoticeModel source, ReleaseCenterNotificationData target) throws ConversionException
	{
		if (source instanceof ElectronicNewsLetterModel)
		{
			super.populate(source, target);

			final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetails = getSubscriptionsMasterDetailsService()
					.getSubscriptionsMasterDetailsForType(getSubscriptionType(source.getClass()));

			List<String> subscriptionDetailsNameList = collectSubscriptionMasterCodes(crmSubscriptionMasterDetails);
			target.setSubscriptionMasterDetails(subscriptionDetailsNameList);
		}
	}
}
