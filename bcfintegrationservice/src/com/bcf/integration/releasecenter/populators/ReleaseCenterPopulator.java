/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.releasecenter.populators;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.model.BusinessOpportunityModel;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.ElectronicNewsLetterModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.util.StreamUtil;


public interface ReleaseCenterPopulator
{

	default CustomerSubscriptionType getSubscriptionType(final Class clazz)
	{
		if (NewsModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.NEWS_RELEASE;
		}
		else if (BusinessOpportunityModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.BUSINESS_OPPORTUNITY;
		}
		else if (ElectronicNewsLetterModel.class.equals(clazz))
		{
			return CustomerSubscriptionType.ELECTRONIC_NEWS_LETTER;
		}
		return null;
	}

	default List<String> collectSubscriptionMasterCodes(
			final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetailModelList)
	{
		if (CollectionUtils.isEmpty(crmSubscriptionMasterDetailModelList))
		{
			return Collections.emptyList();
		}
		return StreamUtil.safeStream(crmSubscriptionMasterDetailModelList)
				.map(CRMSubscriptionMasterDetailModel::getSubscriptionCode)
				.collect(Collectors.toList());
	}
}
