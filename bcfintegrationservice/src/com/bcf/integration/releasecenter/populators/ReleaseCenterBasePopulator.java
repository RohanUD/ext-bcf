/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.core.subscription.service.BcfSubscriptionsMasterDetailsService;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class ReleaseCenterBasePopulator implements Populator<ServiceNoticeModel, ReleaseCenterNotificationData>
{

	Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeConverter;
	Converter<TravelRouteInfo, com.bcf.notification.request.releasecenter.TravelRouteInfo> releaseCenterTravelRouteInfoConverter;

	@Autowired
	protected BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService;

	@Autowired
	private ConfigurationService configurationService;

	@Override
	public void populate(final ServiceNoticeModel source, final ReleaseCenterNotificationData target) throws ConversionException
	{
		final ServiceNoticeData data = serviceNoticeConverter.convert(source);

		target.setCode(data.getCode());
		target.setBodyContent(data.getBodyContent());
		target.setSummary(source.getSummary());
		target.setAffectedRoutes(releaseCenterTravelRouteInfoConverter.convertAll(data.getTravelRoutes()));
		target.setTitle(data.getTitle());
		if (Objects.nonNull(source.getAuthor()))
		{
			target.setAuthor(source.getAuthor().getName());
		}
		target.setEventType(data.getNoticeType().name().toLowerCase());
		target.setWebsiteURL(configurationService.getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL));
	}

	@Required
	public void setServiceNoticeConverter(final Converter<ServiceNoticeModel, ServiceNoticeData> serviceNoticeConverter)
	{
		this.serviceNoticeConverter = serviceNoticeConverter;
	}

	protected Converter<TravelRouteInfo, com.bcf.notification.request.releasecenter.TravelRouteInfo> getReleaseCenterTravelRouteInfoConverter()
	{
		return releaseCenterTravelRouteInfoConverter;
	}

	@Required
	public void setReleaseCenterTravelRouteInfoConverter(
			final Converter<TravelRouteInfo, com.bcf.notification.request.releasecenter.TravelRouteInfo> releaseCenterTravelRouteInfoConverter)
	{
		this.releaseCenterTravelRouteInfoConverter = releaseCenterTravelRouteInfoConverter;
	}

	public BcfSubscriptionsMasterDetailsService getSubscriptionsMasterDetailsService()
	{
		return subscriptionsMasterDetailsService;
	}

	public void setSubscriptionsMasterDetailsService(
			final BcfSubscriptionsMasterDetailsService subscriptionsMasterDetailsService)
	{
		this.subscriptionsMasterDetailsService = subscriptionsMasterDetailsService;
	}

}
