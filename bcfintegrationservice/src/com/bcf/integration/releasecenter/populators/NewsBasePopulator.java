/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.releasecenter.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.List;
import com.bcf.core.model.CRMSubscriptionMasterDetailModel;
import com.bcf.core.model.NewsModel;
import com.bcf.core.model.ServiceNoticeModel;
import com.bcf.notification.request.releasecenter.ReleaseCenterNotificationData;


public class NewsBasePopulator extends ReleaseCenterBasePopulator implements ReleaseCenterPopulator
{

	@Override
	public void populate(final ServiceNoticeModel source, final ReleaseCenterNotificationData target) throws ConversionException
	{
		if (source instanceof NewsModel)
		{
			super.populate(source, target);
			final NewsModel newsModel = NewsModel.class.cast(source);
			target.setPageUrl(newsModel.getPageUrl());
			final List<CRMSubscriptionMasterDetailModel> crmSubscriptionMasterDetails = getSubscriptionsMasterDetailsService()
					.getSubscriptionsMasterDetailsForType(getSubscriptionType(source.getClass()));

			final List<String> subscriptionDetailsNameList = collectSubscriptionMasterCodes(crmSubscriptionMasterDetails);
			target.setSubscriptionMasterDetails(subscriptionDetailsNameList);
		}
	}
}
