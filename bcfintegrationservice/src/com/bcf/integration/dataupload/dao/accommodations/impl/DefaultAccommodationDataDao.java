/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationDataDao extends DefaultGenericDao<AccommodationDataModel> implements AccommodationDataDao
{

	public static final String PROCESS_STATUS_MUST_NOT_BE_NULL = "processStatus must not be null!";

	public DefaultAccommodationDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public AccommodationDataModel getBcfAccommodationData(final String accommodationCode)
	{
		validateParameterNotNull(accommodationCode, "accommodation code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationDataModel.CODE, accommodationCode);
		final List<AccommodationDataModel> accommodationDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationDataModels))
		{
			return accommodationDataModels.stream().findFirst().orElse(null);
		}

		return null;
	}

	@Override
	public List<AccommodationDataModel> findAccommodationsData(final AccommodationOfferingDataModel accommodationOffering,
			final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, PROCESS_STATUS_MUST_NOT_BE_NULL);
		validateParameterNotNull(accommodationOffering, "accommodationOfferingCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationDataModel.ACCOMMODATIONOFFERING, accommodationOffering);
		params.put(AccommodationDataModel.STATUS, processStatus);
		final List<AccommodationDataModel> accommodationsDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationsDataModels))
		{
			return accommodationsDataModels;
		}

		return Collections.emptyList();
	}

	@Override
	public AccommodationDataModel findAccommodationByAccommodationCode(final String accommodationCode,
			final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, PROCESS_STATUS_MUST_NOT_BE_NULL);
		validateParameterNotNull(accommodationCode, "accommodationCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationDataModel.CODE, accommodationCode);
		params.put(AccommodationDataModel.STATUS, processStatus);
		final List<AccommodationDataModel> accommodationsDataModels = find(params);

		return accommodationsDataModels.stream().findFirst().orElse(null);
	}

	@Override
	public List<AccommodationDataModel> findAccommodationsData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, PROCESS_STATUS_MUST_NOT_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationDataModel.STATUS, processStatus);
		final List<AccommodationDataModel> accommodationsDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationsDataModels))
		{
			return accommodationsDataModels;
		}

		return Collections.emptyList();
	}
}
