/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations;

import java.util.Date;
import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface ExtraProductPricesDataDao
{
	List<ExtraProductPriceDataModel> findExtraProductPricesData(final DataImportProcessStatus processStatus);

	List<ExtraProductPriceDataModel> findExtraProductPricesData(ExtraProductDataModel extraProduct, final Date startDate,
			final Date endDate);


}
