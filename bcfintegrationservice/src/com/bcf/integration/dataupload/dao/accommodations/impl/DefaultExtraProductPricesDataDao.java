/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.integration.dataupload.dao.accommodations.ExtraProductPricesDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultExtraProductPricesDataDao extends DefaultGenericDao<ExtraProductPriceDataModel> implements
		ExtraProductPricesDataDao
{
	public DefaultExtraProductPricesDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ExtraProductPriceDataModel> findExtraProductPricesData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ExtraProductPriceDataModel.STATUS, processStatus);
		final List<ExtraProductPriceDataModel> extraProductPriceDataModels = find(params);
		if (CollectionUtils.isNotEmpty(extraProductPriceDataModels))
		{
			return extraProductPriceDataModels;
		}
		return Collections.emptyList();
	}



	@Override
	public List<ExtraProductPriceDataModel> findExtraProductPricesData(ExtraProductDataModel extraProduct,  final Date startDate, final Date endDate)
	{
		validateParameterNotNull(extraProduct, "extraProduct must not be null!");
		validateParameterNotNull(startDate, "startDate must not be null!");
		validateParameterNotNull(endDate, "endDate must not be null!");

		Map<String, Object> params = new HashMap<>();
		params.put(ExtraProductPriceDataModel.EXTRAPRODUCTDATA, extraProduct);
		params.put(ExtraProductPriceDataModel.STARTDATE, startDate);
		params.put(ExtraProductPriceDataModel.ENDDATE, endDate);

		final List<ExtraProductPriceDataModel> extraProductPriceDataModels = find(params);
		if (CollectionUtils.isNotEmpty(extraProductPriceDataModels))
		{
			return extraProductPriceDataModels;
		}
		return Collections.emptyList();
	}
}

