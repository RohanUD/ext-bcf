/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface AccommodationDataDao
{

	/**
	 * Gets the bcf accommodation data.
	 *
	 * @param accommodationCode the accommodation code
	 * @return the bcf accommodation data
	 */
	AccommodationDataModel getBcfAccommodationData(final String accommodationCode);

	/**
	 * Find accommodations data.
	 *
	 * @param accommodationOffering the accommodation offering
	 * @param processStatus         the process status
	 * @return the list
	 */
	List<AccommodationDataModel> findAccommodationsData(AccommodationOfferingDataModel accommodationOffering,
			DataImportProcessStatus processStatus);

	List<AccommodationDataModel> findAccommodationsData(DataImportProcessStatus processStatus);

	/**
	 * Find accommodation by accommodation code.
	 *
	 * @param accommodationCode the accommodation code
	 * @param processStatus     the process status
	 * @return the accommodation data model
	 */
	AccommodationDataModel findAccommodationByAccommodationCode(String accommodationCode, DataImportProcessStatus processStatus);
}
