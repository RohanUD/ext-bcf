/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.integration.dataupload.dao.accommodations.MinimumNightsStayRulesDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultMinimumNightsStayRulesDataDao extends DefaultGenericDao<MinimumNightsStayRulesDataModel> implements
		MinimumNightsStayRulesDataDao
{
	public DefaultMinimumNightsStayRulesDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<MinimumNightsStayRulesDataModel> findMinimumNightsStayRulesData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(MinimumNightsStayRulesDataModel.STATUS, processStatus);
		final List<MinimumNightsStayRulesDataModel> minimumNightsStayRulesDataModels = find(params);
		if (CollectionUtils.isNotEmpty(minimumNightsStayRulesDataModels))
		{
			return minimumNightsStayRulesDataModels;
		}
		return Collections.emptyList();
	}
}
