/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationsOfferingDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationsOfferingDataDao extends DefaultGenericDao<AccommodationOfferingDataModel>
		implements AccommodationsOfferingDataDao
{

	public DefaultAccommodationsOfferingDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<AccommodationOfferingDataModel> findAccommodationsOfferingData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationOfferingDataModel.STATUS, processStatus);
		final List<AccommodationOfferingDataModel> accommodationsOfferingDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationsOfferingDataModels))
		{
			return accommodationsOfferingDataModels;
		}
		return Collections.emptyList();
	}

	@Override
	public AccommodationOfferingDataModel findAccommodationsOfferingData(final String code)
	{
		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationOfferingDataModel.CODE, code);
		final List<AccommodationOfferingDataModel> accommodationsOfferingDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationsOfferingDataModels)
				&& CollectionUtils.size(accommodationsOfferingDataModels) > 1)
		{
			throw new AmbiguousIdentifierException("Multiple entries with same code found");
		}
		if (CollectionUtils.isEmpty(accommodationsOfferingDataModels))
		{
			return null;
		}
		return accommodationsOfferingDataModels.get(0);
	}
}
