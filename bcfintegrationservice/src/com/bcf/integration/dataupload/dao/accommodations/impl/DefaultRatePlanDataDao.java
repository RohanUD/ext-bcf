/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.integration.dataupload.dao.accommodations.RatePlanDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultRatePlanDataDao extends DefaultGenericDao<RatePlanDataModel> implements RatePlanDataDao
{

	public DefaultRatePlanDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<RatePlanDataModel>
	findRatePlanData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(RatePlanDataModel.STATUS, processStatus);
		final List<RatePlanDataModel> ratePlanDataModels = find(params);
		if (CollectionUtils.isNotEmpty(ratePlanDataModels))
		{
			return ratePlanDataModels;
		}
		return Collections.emptyList();
	}

	@Override
	public RatePlanDataModel findRatePlanData(final String ratePlanCode)
	{
		validateParameterNotNull(ratePlanCode, "ratePlanCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(RatePlanDataModel.CODE, ratePlanCode);
		final List<RatePlanDataModel> ratePlanDataModels = find(params);
		if (CollectionUtils.isNotEmpty(ratePlanDataModels) && CollectionUtils.size(ratePlanDataModels) > 1)
		{
			throw new AmbiguousIdentifierException("Multiple Entries found");
		}
		if (CollectionUtils.isEmpty(ratePlanDataModels))
		{
			return null;
		}

		return ratePlanDataModels.get(0);
	}
}
