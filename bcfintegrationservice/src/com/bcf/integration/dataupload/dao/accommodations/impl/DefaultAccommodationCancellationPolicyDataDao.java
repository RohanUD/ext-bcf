/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.accommodations.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationCancellationPolicyDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationCancellationPolicyDataDao extends DefaultGenericDao<AccommodationCancellationPolicyDataModel>
		implements AccommodationCancellationPolicyDataDao
{
	private static final String FIND_BY_ACCOMMODATION_CANCELLATION_POLICY_BY_CODE =
			"SELECT {acc.pk} FROM {AccommodationCancellationPolicyData as acc} WHERE {acc.code} = ?"
					+ AccommodationCancellationPolicyDataModel.CODE;

	public DefaultAccommodationCancellationPolicyDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<AccommodationCancellationPolicyDataModel> findAccommodationCancellationPolicyData(
			final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationCancellationPolicyDataModel.STATUS, processStatus);
		final List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDataModels = find(params);
		if (CollectionUtils.isNotEmpty(accommodationCancellationPolicyDataModels))
		{
			return accommodationCancellationPolicyDataModels;
		}

		return Collections.emptyList();
	}

	@Override
	public AccommodationCancellationPolicyDataModel getAccommodationCancellationPolicyDataByCode(
			final String accommodationCancellationPolicyCode)
	{
		validateParameterNotNull(accommodationCancellationPolicyCode, "accommodationCancellationPolicyCode must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(AccommodationCancellationPolicyDataModel.CODE, accommodationCancellationPolicyCode);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(FIND_BY_ACCOMMODATION_CANCELLATION_POLICY_BY_CODE, params);
		final SearchResult<AccommodationCancellationPolicyDataModel> searchResult = this.getFlexibleSearchService().search(fsq);

		if (CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return null;
		}
		else if (searchResult.getResult().size() > 1)
		{
			throw new AmbiguousIdentifierException("Found " + searchResult.getResult().size() + " results for the given query");
		}
		return searchResult.getResult().stream().findFirst().get();
	}
}
