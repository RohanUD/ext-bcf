/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.activities.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivitiesDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivitiesDataDao extends DefaultGenericDao<ActivitiesDataModel> implements ActivitiesDataDao
{
	public DefaultActivitiesDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ActivitiesDataModel> findActivitiesData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivitiesDataModel.STATUS, processStatus);
		final List<ActivitiesDataModel> activitiesDataModels = find(params);
		if (CollectionUtils.isNotEmpty(activitiesDataModels))
		{
			return activitiesDataModels;
		}

		return Collections.emptyList();
	}

	@Override
	public ActivitiesDataModel findActivitiesData(final String code)
	{
		validateParameterNotNull(code, "code must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivitiesDataModel.CODE, code);
		final List<ActivitiesDataModel> activitiesDataModels = find(params);
		if (!activitiesDataModels.isEmpty())
		{
			return activitiesDataModels.get(0);
		}
		return null;
	}
}
