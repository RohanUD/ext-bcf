/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.activities;

import de.hybris.platform.cronjob.enums.DayOfWeek;
import java.util.Date;
import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface ActivityPricesDataDao
{
	List<ActivityPriceDataModel> findActivityPricesData(final DataImportProcessStatus processStatus);

	List<ActivityPriceDataModel> findActivityPricesData(final String activitiesDataCode, final String paxType,
			final Date startDate, final Date endDate, final List<DayOfWeek> daysOfWeek);
}
