/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.activities.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.bcfintegrationservice.utilitydata.strategy.ActivityPriceDataCodeGenerateStrategy;
import com.bcf.integration.dataupload.dao.activities.ActivityPricesDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivityPricesDataDao extends DefaultGenericDao<ActivityPriceDataModel> implements ActivityPricesDataDao
{
	ActivityPriceDataCodeGenerateStrategy activityPriceDataCodeGenerateStrategy;

	public DefaultActivityPricesDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ActivityPriceDataModel> findActivityPricesData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(ActivityPriceDataModel.STATUS, processStatus);
		final List<ActivityPriceDataModel> activityPriceDataModels = find(params);
		if (CollectionUtils.isNotEmpty(activityPriceDataModels))
		{
			return activityPriceDataModels;
		}
		return Collections.emptyList();
	}

	@Override
	public List<ActivityPriceDataModel> findActivityPricesData(final String activitiesDataCode, final String paxType,
			final Date startDate, final Date endDate, final List<DayOfWeek> daysOfWeek)
	{
		validateParameterNotNull(activitiesDataCode, "activitiesDataCode must not be null!");
		validateParameterNotNull(paxType, "paxType must not be null!");
		validateParameterNotNull(startDate, "startDate must not be null!");
		validateParameterNotNull(endDate, "endDate must not be null!");
		validateParameterNotNull(daysOfWeek, "daysOfWeek must not be null!");

		String code = activityPriceDataCodeGenerateStrategy
				.generateCode(activitiesDataCode, paxType, startDate, endDate, daysOfWeek);

		Map<String, Object> params = new HashMap<>();
		params.put(ActivityPriceDataModel.CODE, code);

		SortParameters sortParameters = new SortParameters();
		sortParameters.addSortParameter(ActivityPriceDataModel.MODIFIEDTIME, SortParameters.SortOrder.DESCENDING);

		final List<ActivityPriceDataModel> activityPriceDataModels = find(params, sortParameters);
		if (CollectionUtils.isNotEmpty(activityPriceDataModels))
		{
			return activityPriceDataModels;
		}
		return Collections.emptyList();
	}

	@Required
	public void setActivityPriceDataCodeGenerateStrategy(
			final ActivityPriceDataCodeGenerateStrategy activityPriceDataCodeGenerateStrategy)
	{
		this.activityPriceDataCodeGenerateStrategy = activityPriceDataCodeGenerateStrategy;
	}
}
