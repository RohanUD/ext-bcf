/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.dao.deals.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.integration.dataupload.dao.deals.DealsBundleDataDao;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultDealsBundleDataDao extends DefaultGenericDao<DealsBundleDataModel> implements DealsBundleDataDao
{
	public DefaultDealsBundleDataDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<DealsBundleDataModel> findDealsBundleData(final DataImportProcessStatus processStatus)
	{
		validateParameterNotNull(processStatus, "processStatus must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(DealsBundleDataModel.STATUS, processStatus);
		final List<DealsBundleDataModel> dealsBundleDataModels = find(params);
		if (CollectionUtils.isNotEmpty(dealsBundleDataModels))
		{
			return dealsBundleDataModels;
		}

		return Collections.emptyList();
	}

	@Override
	public DealsBundleDataModel findDealsBundleDataForSeoUrl(final String seoUrl, final CatalogVersionModel catalogVersionModel)
	{
		validateParameterNotNull(seoUrl, "seoUrl must not be null!");
		final Map<String, Object> params = new HashMap<>();
		params.put(DealsBundleDataModel.SEOURL, seoUrl);
		params.put(DealsBundleDataModel.STATUS, DataImportProcessStatus.SUCCESS);
		params.put(DealsBundleDataModel.CATALOGVERSION,catalogVersionModel);
		final List<DealsBundleDataModel> dealsBundleDataModels = find(params);
		if (!dealsBundleDataModels.isEmpty())
		{
			return dealsBundleDataModels.get(0);
		}
		return null;
	}

}
