/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.dataupload.service.accommodations;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface MinimumNightsStayRulesDataService
{

	/**
	 * Gets the Minimum Nights Stay Rules Data data.
	 *
	 * @param processStatus the process status
	 * @return the Minimum Nights Stay Rules Data
	 */
	List<MinimumNightsStayRulesDataModel> getMinimumNightsStayRulesData(DataImportProcessStatus processStatus);

}
