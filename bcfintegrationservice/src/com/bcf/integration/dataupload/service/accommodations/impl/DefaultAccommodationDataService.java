/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationDataDao;
import com.bcf.integration.dataupload.service.accommodations.AccommodationDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationDataService implements AccommodationDataService
{
	private AccommodationDataDao accommodationDataDao;

	@Override
	public AccommodationDataModel getAccommodationData(final String accommodationCode)
	{
		return getAccommodationDataDao().getBcfAccommodationData(accommodationCode);
	}

	@Override
	public List<AccommodationDataModel> getAccommodationsData(final AccommodationOfferingDataModel accommodationOffering,
			final DataImportProcessStatus processStatus)
	{
		return getAccommodationDataDao().findAccommodationsData(accommodationOffering, processStatus);
	}

	@Override
	public AccommodationDataModel getAccommodationByAccommodationCode(final String accommodationCode,
			final DataImportProcessStatus processStatus)
	{
		return getAccommodationDataDao().findAccommodationByAccommodationCode(accommodationCode, processStatus);
	}

	@Override
	public List<AccommodationDataModel> getAccommodation(final DataImportProcessStatus processStatus)
	{
		return getAccommodationDataDao().findAccommodationsData(processStatus);
	}

	protected AccommodationDataDao getAccommodationDataDao()
	{
		return accommodationDataDao;
	}

	@Required
	public void setAccommodationDataDao(final AccommodationDataDao accommodationDataDao)
	{
		this.accommodationDataDao = accommodationDataDao;
	}
}
