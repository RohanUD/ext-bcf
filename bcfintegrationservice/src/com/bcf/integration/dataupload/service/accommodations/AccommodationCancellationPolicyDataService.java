/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface AccommodationCancellationPolicyDataService
{

	/**
	 * Gets the accommodation cancellation policy data.
	 *
	 * @param processStatus the process status
	 * @return the list
	 */
	List<AccommodationCancellationPolicyDataModel> getAccommodationCancellationPolicyData(DataImportProcessStatus processStatus);

	AccommodationCancellationPolicyDataModel getAccommodationCancellationPolicyDataByCode(
			final String accommodationCancellationPolicyDataModelCode);
}
