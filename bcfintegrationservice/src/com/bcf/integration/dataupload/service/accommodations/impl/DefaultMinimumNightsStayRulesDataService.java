/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.integration.dataupload.dao.accommodations.MinimumNightsStayRulesDataDao;
import com.bcf.integration.dataupload.service.accommodations.MinimumNightsStayRulesDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultMinimumNightsStayRulesDataService implements MinimumNightsStayRulesDataService
{
	private MinimumNightsStayRulesDataDao minimumNightsStayRulesDataDao;

	@Override
	public List<MinimumNightsStayRulesDataModel> getMinimumNightsStayRulesData(final DataImportProcessStatus processStatus)
	{
		return minimumNightsStayRulesDataDao.findMinimumNightsStayRulesData(processStatus);
	}

	public MinimumNightsStayRulesDataDao getMinimumNightsStayRulesDataDao()
	{
		return minimumNightsStayRulesDataDao;
	}

	public void setMinimumNightsStayRulesDataDao(
			final MinimumNightsStayRulesDataDao minimumNightsStayRulesDataDao)
	{
		this.minimumNightsStayRulesDataDao = minimumNightsStayRulesDataDao;
	}
}
