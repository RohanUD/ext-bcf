/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.integration.dataupload.dao.accommodations.RatePlanDataDao;
import com.bcf.integration.dataupload.service.accommodations.RatePlanDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultRatePlanDataService implements RatePlanDataService
{
	private RatePlanDataDao ratePlanDataDao;

	@Override
	public List<RatePlanDataModel> getRatePlanData(final DataImportProcessStatus processStatus)
	{
		return getRatePlanDataDao().findRatePlanData(processStatus);
	}

	@Override
	public RatePlanDataModel getRatePlanData(final String ratePlanCode)
	{
		return getRatePlanDataDao().findRatePlanData(ratePlanCode);
	}

	/**
	 * Gets the rate plan data dao.
	 *
	 * @return the rate plan data dao
	 */
	protected RatePlanDataDao getRatePlanDataDao()
	{
		return ratePlanDataDao;
	}

	/**
	 * Sets the rate plan data dao.
	 *
	 * @param ratePlanDataDao the new rate plan data dao
	 */
	@Required
	public void setRatePlanDataDao(final RatePlanDataDao ratePlanDataDao)
	{
		this.ratePlanDataDao = ratePlanDataDao;
	}
}
