/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.integration.dataupload.service.accommodations.UpdateStockLevelsToAccommodationOfferingService;


public class DefaultUpdateStockLevelsToAccommodationOfferingService implements UpdateStockLevelsToAccommodationOfferingService
{
	public static final String DATE_PATTERN = "MMM dd yyyy";
	private static final Logger LOG = Logger.getLogger(DefaultUpdateStockLevelsToAccommodationOfferingService.class);
	private static final String CATALOG_NAME = "bcfProductCatalog";
	private static final String COMMA = ",";
	private ProductService productService;
	private CatalogVersionService catalogVersionService;
	private ModelService modelService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;
	private EnumerationService enumerationService;

	/**
	 * Update stock levels to accommodation offering.
	 *
	 * @param accommodationStockLevelDatasMap the Map of AccommodationOfferingModel and List<AccommodationStockLevelDataModel>
	 */
	@Override
	public void updateBcfStockLevelsToAccommodationOffering(
			final Map<AccommodationOfferingModel, List<AccommodationStockLevelDataModel>> accommodationStockLevelDatasMap)
	{
		for (final Map.Entry<AccommodationOfferingModel, List<AccommodationStockLevelDataModel>> accommodationStockLevelDataMap : accommodationStockLevelDatasMap
				.entrySet())
		{
			final AccommodationOfferingModel accommodationOffering = accommodationStockLevelDataMap.getKey();
			final List<AccommodationStockLevelDataModel> accommodationStockLevelDataList = accommodationStockLevelDataMap.getValue();
			final Map<AccommodationDataModel, List<AccommodationStockLevelDataModel>> accommodationDataMap = accommodationStockLevelDataList
					.stream().collect(Collectors.groupingBy(AccommodationStockLevelDataModel::getAccommodationData));
			for (final Map.Entry<AccommodationDataModel, List<AccommodationStockLevelDataModel>> accommodationDataEntry : accommodationDataMap
					.entrySet())
			{
				final List<AccommodationStockLevelDataModel> groupedAccommodationStockLevelData = accommodationDataEntry.getValue();
				updateBcfStockLevelForAccommodation(accommodationOffering, groupedAccommodationStockLevelData);
			}
		}
	}

	/**
	 * Update stock level for accommodation.
	 *
	 * @param accommodationOffering              the accommodation offering
	 * @param groupedAccommodationStockLevelData the accommodationStockLevelDatasByCode
	 */
	private void updateBcfStockLevelForAccommodation(final AccommodationOfferingModel accommodationOffering,
			final List<AccommodationStockLevelDataModel> groupedAccommodationStockLevelData)
	{
		final String accommodationCodeKey = groupedAccommodationStockLevelData.stream().findFirst().get().getAccommodationData()
				.getCode();
		final String productCode = accommodationOffering.getCode() + "_" + accommodationCodeKey;
		final AccommodationModel accommodation = (AccommodationModel) getProductService().getProductForCode(
				getCatalogVersionService().getCatalogVersion(CATALOG_NAME, "Staged"), productCode);
		for (final AccommodationStockLevelDataModel accommodationStockLevelData : groupedAccommodationStockLevelData)
		{
			final Date truncatedStartDate = DateUtils.truncate(accommodationStockLevelData.getStartDate(), Calendar.DATE);
			final Date startDate = truncatedStartDate;
			Date truncatedEndDate = null;
			if (Objects.nonNull(accommodationStockLevelData.getEndDate()))
			{
				truncatedEndDate = DateUtils.truncate(accommodationStockLevelData.getEndDate(), Calendar.DATE);
			}
			final Date endDate = Objects.nonNull(truncatedEndDate) ? truncatedEndDate : startDate;
			if(StringUtils.isBlank(accommodationStockLevelData.getDaysOfWeekStr()))
			{
				if (CollectionUtils.isEmpty(accommodationStockLevelData.getDaysOfWeek()))
				{
					accommodationStockLevelData.setDaysOfWeek(Arrays.asList(DayOfWeek.values()));
				}
			}
			else
			{
				final List<String> dayOfWeekValues = Arrays
						.asList(StringUtils.split(accommodationStockLevelData.getDaysOfWeekStr(), COMMA));
				try
				{
					final List<DayOfWeek> daysOfWeekList = dayOfWeekValues.stream()
							.map(dayOfWeekValue -> DayOfWeek.valueOf(StringUtils.trim(dayOfWeekValue))).collect(Collectors.toList());
					accommodationStockLevelData.setDaysOfWeek(daysOfWeekList);
				}
				catch (final IllegalArgumentException e)
				{
					LOG.error("No enum constant DayOfWeek", e);
				}
				accommodationStockLevelData.setDaysOfWeekStr(StringUtils.EMPTY);
			}
			final List<DayOfWeek> daysOfWeek = accommodationStockLevelData.getDaysOfWeek();
			setStockLevelForAccommodation(accommodationOffering, accommodation, accommodationStockLevelData, startDate, endDate,
					daysOfWeek);
		}
	}

	private void setStockLevelForAccommodation(final AccommodationOfferingModel accommodationOffering,
			final AccommodationModel accommodation, final AccommodationStockLevelDataModel accommodationStockLevelData,
			final Date startDate, final Date endDate, final List<DayOfWeek> daysOfWeek)
	{
		for (Date date = startDate; ((date.before(endDate) || date.equals(endDate))
				&& isValidForDayOfWeek(date, daysOfWeek)); date = TravelDateUtils.addDays(date, 1))
		{
			setBcfStockLevelForAccommodation(accommodationStockLevelData, accommodationOffering, accommodation, date);
		}
	}

	/**
	 * Checks if is valid for day of week.
	 *
	 * @param date       the date
	 * @param daysOfWeek the days of week
	 * @return true, if is valid for day of week
	 */
	protected boolean isValidForDayOfWeek(final Date date, final List<DayOfWeek> daysOfWeek)
	{
		final LocalDate localDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		final DayOfWeek dayOfWeek = getEnumerationService().getEnumerationValue(DayOfWeek.class,
				localDate.getDayOfWeek().toString());
		return daysOfWeek.contains(dayOfWeek);
	}

	/**
	 * If a mapping file is provided stock levels are populated according to its content otherwise their availability is
	 * simulated by a populating algorithm
	 *
	 * @param accommodationStockLevelData the accommodationStockLevelData.
	 * @param accommodationOffering       the accommodationOffering
	 * @param accommodation               the accommodation
	 * @param date                        the date
	 */
	private void setBcfStockLevelForAccommodation(final AccommodationStockLevelDataModel accommodationStockLevelData,
			final AccommodationOfferingModel accommodationOffering, final AccommodationModel accommodation, final Date date)
	{
		final Collection<WarehouseModel> warehouses = Collections.singletonList(accommodationOffering);
		final StockLevelModel stockLevel = getBcfTravelCommerceStockService()
				.getStockLevelModelForDate(accommodation, date, warehouses);
		if (Objects.isNull(stockLevel))
		{
			createStock(accommodationOffering, accommodation, accommodationStockLevelData, date,
					accommodationStockLevelData.getBlockReleaseDays());
		}
		else if (Objects.equals(StockLevelType.STANDARD, stockLevel.getStockLevelType()))
		{
			updateStock(stockLevel, accommodation, accommodationStockLevelData, date);
		}
	}

	private void updateStock(final StockLevelModel stockLevel, final AccommodationModel accommodation,
			final AccommodationStockLevelDataModel accommodationStockLevelData, final Date date)
	{
		final Integer stockAvailability = accommodationStockLevelData.getStockLevel();
		final Integer overSellingQuantity = accommodationStockLevelData.getOverSellingQuantity();
		stockLevel.setAvailable(stockAvailability);
		stockLevel.setProduct(accommodation);
		final Integer overSelling = Objects.isNull(overSellingQuantity) ? 0 : overSellingQuantity;
		stockLevel.setOverSelling(overSelling);
		stockLevel.setReleaseBlockDay(TravelDateUtils.addDays(date, -accommodationStockLevelData.getBlockReleaseDays()));
		getModelService().save(stockLevel);
	}

	private void createStock(final AccommodationOfferingModel accommodationOffering, final AccommodationModel accommodation,
			final AccommodationStockLevelDataModel accommodationStockLevelData, final Date date, final int releaseBlockDays)
	{
		final Integer stockAvailability = accommodationStockLevelData.getStockLevel();
		final Integer overSellingQuantity = accommodationStockLevelData.getOverSellingQuantity();
		final StockLevelModel stockLevel = getModelService().create(StockLevelModel.class);
		stockLevel.setWarehouse(accommodationOffering);
		stockLevel.setProductCode(accommodation.getCode());
		stockLevel.setAvailable(stockAvailability);
		stockLevel.setReserved(0);
		stockLevel.setProduct(accommodation);
		stockLevel.setDate(date);
		stockLevel.setReleaseBlockDay(TravelDateUtils.addDays(date, -releaseBlockDays));
		stockLevel.setStockLevelType(StockLevelType.STANDARD);
		final Integer overSelling = Objects.isNull(overSellingQuantity) ? 0 : overSellingQuantity;
		stockLevel.setOverSelling(overSelling);
		getModelService().save(stockLevel);
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}
	
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
