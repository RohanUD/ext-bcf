/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationsOfferingDataDao;
import com.bcf.integration.dataupload.service.accommodations.AccommodationsOfferingDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationsOfferingDataService implements AccommodationsOfferingDataService
{
	private AccommodationsOfferingDataDao accommodationsOfferingDataDao;

	@Override
	public List<AccommodationOfferingDataModel> getAccommodationsOfferingData(final DataImportProcessStatus processStatus)
	{
		return getAccommodationsOfferingDataDao().findAccommodationsOfferingData(processStatus);
	}

	@Override
	public AccommodationOfferingDataModel getAccommodationsOfferingData(final String code)
	{
		return getAccommodationsOfferingDataDao().findAccommodationsOfferingData(code);
	}

	/**
	 * Gets the accommodations offering data dao.
	 *
	 * @return the accommodations offering data dao
	 */
	protected AccommodationsOfferingDataDao getAccommodationsOfferingDataDao()
	{
		return accommodationsOfferingDataDao;
	}

	/**
	 * Sets the accommodations offering data dao.
	 *
	 * @param accommodationsOfferingDataDao the new accommodations offering data dao
	 */
	@Required
	public void setAccommodationsOfferingDataDao(final AccommodationsOfferingDataDao accommodationsOfferingDataDao)
	{
		this.accommodationsOfferingDataDao = accommodationsOfferingDataDao;
	}
}
