/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationCancellationPolicyDataDao;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationPolicyDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationCancellationPolicyDataService implements AccommodationCancellationPolicyDataService
{
	private AccommodationCancellationPolicyDataDao accommodationCancellationPolicyDataDao;

	@Override
	public List<AccommodationCancellationPolicyDataModel> getAccommodationCancellationPolicyData(
			final DataImportProcessStatus processStatus)
	{
		return getAccommodationCancellationPolicyDataDao().findAccommodationCancellationPolicyData(processStatus);
	}

	@Override
	public AccommodationCancellationPolicyDataModel getAccommodationCancellationPolicyDataByCode(
			final String accommodationCancellationPolicyDataModelCode)
	{
		return getAccommodationCancellationPolicyDataDao()
				.getAccommodationCancellationPolicyDataByCode(accommodationCancellationPolicyDataModelCode);
	}

	protected AccommodationCancellationPolicyDataDao getAccommodationCancellationPolicyDataDao()
	{
		return accommodationCancellationPolicyDataDao;
	}

	@Required
	public void setAccommodationCancellationPolicyDataDao(
			final AccommodationCancellationPolicyDataDao accommodationCancellationPolicyDataDao)
	{
		this.accommodationCancellationPolicyDataDao = accommodationCancellationPolicyDataDao;
	}
}
