/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface ExtraProductDataService
{

	/**
	 * Gets the Extra Product data.
	 *
	 * @param processStatus the process status
	 * @return the list of Extra Product data
	 */
	List<ExtraProductDataModel> getExtraProductData(DataImportProcessStatus processStatus);

	List<ExtraProductPriceDataModel> getExtraProductPricesData(DataImportProcessStatus processStatus);

	List<ExtraProductPriceDataModel> getExistingExtraProductPricesData(final ExtraProductPriceDataModel model);

}
