/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationCancellationFeesDataDao;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationFeesDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationCancellationFeesDataService implements AccommodationCancellationFeesDataService
{
	private AccommodationCancellationFeesDataDao accommodationCancellationFeesDataDao;

	@Override
	public List<AccommodationCancellationFeesDataModel> getAccommodationCancellationFeesData(
			final DataImportProcessStatus processStatus)
	{
		return getAccommodationCancellationFeesDataDao().findAccommodationCancellationFeesData(processStatus);
	}

	protected AccommodationCancellationFeesDataDao getAccommodationCancellationFeesDataDao()
	{
		return accommodationCancellationFeesDataDao;
	}

	@Required
	public void setAccommodationCancellationFeesDataDao(
			final AccommodationCancellationFeesDataDao accommodationCancellationFeesDataDao)
	{
		this.accommodationCancellationFeesDataDao = accommodationCancellationFeesDataDao;
	}
}
