/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.integration.dataupload.dao.accommodations.ExtraProductDataDao;
import com.bcf.integration.dataupload.dao.accommodations.ExtraProductPricesDataDao;
import com.bcf.integration.dataupload.service.accommodations.ExtraProductDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultExtraProductDataService implements ExtraProductDataService
{
	private ExtraProductDataDao extraProductDataDao;

	private ExtraProductPricesDataDao extraProductPricesDataDao;

	@Override
	public List<ExtraProductDataModel> getExtraProductData(final DataImportProcessStatus processStatus)
	{
		return getExtraProductDataDao().findExtraProductData(processStatus);
	}

	@Override
	public List<ExtraProductPriceDataModel> getExtraProductPricesData(final DataImportProcessStatus processStatus)
	{
		return extraProductPricesDataDao.findExtraProductPricesData(processStatus);
	}

	public List<ExtraProductPriceDataModel> getExistingExtraProductPricesData(final ExtraProductPriceDataModel model)
	{
		return extraProductPricesDataDao
				.findExtraProductPricesData(model.getExtraProductData(), model.getStartDate(),
						model.getEndDate());
	}



	protected ExtraProductDataDao getExtraProductDataDao()
	{
		return extraProductDataDao;
	}

	@Required
	public void setExtraProductDataDao(final ExtraProductDataDao extraProductDataDao)
	{
		this.extraProductDataDao = extraProductDataDao;
	}


	public ExtraProductPricesDataDao getExtraProductPricesDataDao()
	{
		return extraProductPricesDataDao;
	}

	public void setExtraProductPricesDataDao(
			final ExtraProductPricesDataDao extraProductPricesDataDao)
	{
		this.extraProductPricesDataDao = extraProductPricesDataDao;
	}
}
