/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.integration.dataupload.dao.accommodations.AccommodationStockLevelDataDao;
import com.bcf.integration.dataupload.service.accommodations.AccommodationStockLevelDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultAccommodationStockLevelDataService implements AccommodationStockLevelDataService
{
	private AccommodationStockLevelDataDao accommodationStockLevelDataDao;

	@Override
	public List<AccommodationStockLevelDataModel> getAccommodationStockLevelData(final DataImportProcessStatus processStatus)
	{
		return getAccommodationStockLevelDataDao().findAccommodationStockLevelData(processStatus);
	}

	protected AccommodationStockLevelDataDao getAccommodationStockLevelDataDao()
	{
		return accommodationStockLevelDataDao;
	}

	@Required
	public void setAccommodationStockLevelDataDao(final AccommodationStockLevelDataDao accommodationStockLevelDataDao)
	{
		this.accommodationStockLevelDataDao = accommodationStockLevelDataDao;
	}
}
