/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.service.accommodations;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface AccommodationDataService
{
	/**
	 * Gets the bcf accommodation data.
	 *
	 * @param accommodationCode the accommodation code
	 * @return the bcf accommodation data
	 */
	AccommodationDataModel getAccommodationData(String accommodationCode);

	/**
	 * Gets the accommodations data.
	 *
	 * @param accommodationOffering the accommodation Offering Code
	 * @param processStatus         the process status
	 * @return the accommodations data
	 */
	List<AccommodationDataModel> getAccommodationsData(final AccommodationOfferingDataModel accommodationOffering, final
	DataImportProcessStatus processStatus);

	/**
	 * Gets the accommodations data.
	 *
	 * @param accommodationCode the accommodation Code
	 * @param processStatus     the process status
	 * @return the accommodations data
	 */
	AccommodationDataModel getAccommodationByAccommodationCode(final String accommodationCode,
			DataImportProcessStatus processStatus);

	List<AccommodationDataModel> getAccommodation(DataImportProcessStatus processStatus);
}
