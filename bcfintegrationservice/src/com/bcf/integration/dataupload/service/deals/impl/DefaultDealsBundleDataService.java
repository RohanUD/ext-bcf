/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.deals.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.integration.dataupload.dao.deals.DealsBundleDataDao;
import com.bcf.integration.dataupload.service.deals.DealsBundleDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultDealsBundleDataService implements DealsBundleDataService
{
	private DealsBundleDataDao dealsBundleDataDao;

	@Override
	public List<DealsBundleDataModel> getDealsBundleData(final DataImportProcessStatus processStatus)
	{
		return getDealsBundleDataDao().findDealsBundleData(processStatus);
	}

	@Override
	public DealsBundleDataModel getDealsBundleDataForSeoUrl(final String seoUrl , final CatalogVersionModel catalogVersionModel)
	{
		return getDealsBundleDataDao().findDealsBundleDataForSeoUrl(seoUrl,catalogVersionModel);
	}

	/**
	 * @return the dealsBundleDataDao
	 */
	protected DealsBundleDataDao getDealsBundleDataDao()
	{
		return dealsBundleDataDao;
	}

	/**
	 * @param dealsBundleDataDao the dealsBundleDataDao to set
	 */
	@Required
	public void setDealsBundleDataDao(final DealsBundleDataDao dealsBundleDataDao)
	{
		this.dealsBundleDataDao = dealsBundleDataDao;
	}

}
