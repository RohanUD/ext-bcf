/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.deals;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface DealsBundleDataService
{

	/**
	 * Gets the deals bundle data.
	 *
	 * @param processStatus the process status
	 * @return the deals bundle data
	 */
	List<DealsBundleDataModel> getDealsBundleData(DataImportProcessStatus processStatus);
	/**
	 * Find deals bundle data.
	 *
	 * @param seoUrl The SEO URL
	 * @return DealsBundleDataModel
	 */
	DealsBundleDataModel getDealsBundleDataForSeoUrl(String seoUrl, CatalogVersionModel catalogVersionModel);
}
