/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivityScheduleDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivityScheduleDataDao;
import com.bcf.integration.dataupload.service.activities.ActivityScheduleDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivityScheduleDataService implements ActivityScheduleDataService
{

	private ActivityScheduleDataDao activityScheduleDataDao;

	@Override
	public List<ActivityScheduleDataModel> getActivityScheduleData(final DataImportProcessStatus processStatus)
	{
		return getActivityScheduleDataDao().findActivityScheduleData(processStatus);
	}

	public ActivityScheduleDataDao getActivityScheduleDataDao()
	{
		return activityScheduleDataDao;
	}

	public void setActivityScheduleDataDao(final ActivityScheduleDataDao activityScheduleDataDao)
	{
		this.activityScheduleDataDao = activityScheduleDataDao;
	}
}
