/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.integration.enums.DataImportProcessStatus;


public interface ActivitiesDataService
{

	/**
	 * Gets the activities data.
	 *
	 * @param processStatus the process status
	 * @return the activities data
	 */
	List<ActivitiesDataModel> getActivitiesData(DataImportProcessStatus processStatus);

	ActivitiesDataModel getActivitiesData(String code);

	List<ActivityPriceDataModel> getActivityPricesData(DataImportProcessStatus processStatus);

	List<ActivityPriceDataModel> getExistingActivityPricesData(final ActivityPriceDataModel model);
}
