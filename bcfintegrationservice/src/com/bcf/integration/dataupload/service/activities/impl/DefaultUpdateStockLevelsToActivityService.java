/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import static com.bcf.core.constants.BcfCoreConstants.CATALOG_VERSION;
import static com.bcf.core.constants.BcfCoreConstants.PRODUCTCATALOG;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.core.enums.StockLevelType;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.product.ActivityScheduleModel;
import com.bcf.core.service.BcfTravelCommerceStockService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.service.activities.UpdateStockLevelsToActivityService;


public class DefaultUpdateStockLevelsToActivityService implements UpdateStockLevelsToActivityService
{

	private static final Logger LOG = Logger.getLogger(DefaultUpdateStockLevelsToActivityService.class);
	private static final String DEFAULT = "default";

	private ProductService productService;
	private CatalogVersionService catalogVersionService;
	private ModelService modelService;
	private WarehouseService warehouseService;
	private BcfTravelCommerceStockService bcfTravelCommerceStockService;

	/**
	 * Update stock levels to activity.
	 *
	 * @param activityInventoryDatasMap the Map of ActivityProductModel and List<ActivityInventoryDataModel>
	 */
	@Override
	public void updateBcfStockLevelsToActivity(
			final Map<ActivityProductModel, List<ActivityInventoryDataModel>> activityInventoryDatasMap)
	{
		final List<StockLevelModel> stockLevelsToSave = new ArrayList<>();
		final WarehouseModel defaultWarehouse = getWarehouseService().getWarehouseForCode(DEFAULT);
		for (final Map.Entry<ActivityProductModel, List<ActivityInventoryDataModel>> activityInventoryDataMap : activityInventoryDatasMap
				.entrySet())
		{
			final ActivityProductModel activityProduct = activityInventoryDataMap.getKey();
			final List<ActivityInventoryDataModel> groupedActivityInventoryData = activityInventoryDataMap
					.getValue();
			updateBcfStockLevelForActivity(stockLevelsToSave, activityProduct, groupedActivityInventoryData,
					defaultWarehouse);
		}

		getBcfTravelCommerceStockService().batchSavingOfStockLevels(stockLevelsToSave);
	}

	@Override
	public void updateBcfStockByType(final ActivitiesDataModel activitiesDataModel)
	{
		/**
		 * if StockLevelType is updated/modified from Standard to 'ONREQUEST/FREE' .we need to remove all previous StockLevels
		 */
		if (Objects.equals(activitiesDataModel.getStockLevelType(), StockLevelType.ONREQUEST) || Objects
				.equals(activitiesDataModel.getStockLevelType(), StockLevelType.FREE))
		{
			final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
			ActivityProductModel activityProduct = (ActivityProductModel) getProductService()
					.getProductForCode(catalogVersion, activitiesDataModel.getCode());
			final int stockLevelSize = org.apache.commons.collections.CollectionUtils.size(activityProduct.getStockLevels());
			getModelService().removeAll(activityProduct.getStockLevels());
			LOG.info("Removed...previous Number of stock levels: " + stockLevelSize);
		}
	}

	/**
	 * Update stock level for accommodation.
	 *
	 * @param stockLevelsToSave            the max availability
	 * @param activityProduct              the activity product
	 * @param groupedActivityInventoryData the groupedActivityInventoryData
	 * @param defaultWarehouse             the default warehouse
	 */
	private void updateBcfStockLevelForActivity(
			final List<StockLevelModel> stockLevelsToSave, final ActivityProductModel activityProduct,
			final List<ActivityInventoryDataModel> groupedActivityInventoryData, final WarehouseModel defaultWarehouse)
	{
		final String activityProductCode = activityProduct.getCode();
		Set<StockLevelModel> stockLevels = new HashSet<>(activityProduct.getStockLevels());
		final SimpleDateFormat sm = new SimpleDateFormat(BcfintegrationserviceConstants.DATE_FORMAT);
		for (final ActivityInventoryDataModel activityInventoryData : groupedActivityInventoryData)
		{
			activityInventoryData.setStartDate(Objects.isNull(activityInventoryData.getStartDate()) ?
					activityInventoryData.getStartDate() :
					DateUtils.truncate(activityInventoryData.getStartDate(), Calendar.DATE));
			activityInventoryData.setEndDate(Objects.isNull(activityInventoryData.getEndDate()) ?
					activityInventoryData.getEndDate() :
					DateUtils.truncate(activityInventoryData.getEndDate(), Calendar.DATE));
			Date endDate = activityInventoryData.getEndDate();
			try
			{
				final Date startDate = sm.parse(sm.format(activityInventoryData.getStartDate()));
				final Calendar start = Calendar.getInstance();
				start.setTime(startDate);
				if (Objects.nonNull(endDate))
				{
					endDate = sm.parse(sm.format(activityInventoryData.getEndDate()));
					final Calendar end = Calendar.getInstance();
					end.setTime(endDate);

					stockLevelToRemove(stockLevels, activityInventoryData, startDate);
					updateStockLevelForActivity(stockLevelsToSave, activityProduct, defaultWarehouse, activityProductCode, stockLevels,
							activityInventoryData, start, end);
				}
				else
				{
					setBcfStockLevelForActivity(activityInventoryData, activityProductCode, stockLevels,
							activityProduct, startDate, stockLevelsToSave, defaultWarehouse);
				}
			}
			catch (final ParseException e)
			{
				LOG.error("Date parsing error");
			}
		}
	}

	private void updateStockLevelForActivity(final List<StockLevelModel> stockLevelsToSave,
			final ActivityProductModel activityProduct, final WarehouseModel defaultWarehouse, final String activityProductCode,
			final Set<StockLevelModel> stockLevels, final ActivityInventoryDataModel activityInventoryData, final Calendar start,
			final Calendar end)
	{
		for (Date date = start.getTime(); (start.before(end) || start.equals(end)); start
				.add(Calendar.DATE, 1), date = start
				.getTime())
		{
			if (activityInventoryData.getApplyForAllSchedule())
			{
				setBcfStockLevelForScheduledActivity(activityInventoryData, activityProductCode, stockLevels,
						activityProduct, date, stockLevelsToSave, defaultWarehouse);
			}
			else
			{
				setBcfStockLevelForActivity(activityInventoryData, activityProductCode, stockLevels,
						activityProduct, date, stockLevelsToSave, defaultWarehouse);
			}

		}
	}

	private void stockLevelToRemove(final Set<StockLevelModel> stockLevels, final ActivityInventoryDataModel activityInventoryData,
			final Date startDate)
	{
		if (CollectionUtils.isNotEmpty(stockLevels) && Objects.nonNull(activityInventoryData.getStartTime()))
		{
			List<StockLevelModel> stockLevelsToRemove = new ArrayList<>();
			stockLevels.forEach(stockLevelModel -> {
				if (stockLevelModel.getDate().before(startDate) && activityInventoryData.getStartTime()
						.equals(stockLevelModel.getTime()))
				{
					stockLevelsToRemove.add(stockLevelModel);
				}
			});
			stockLevels.removeAll(stockLevelsToRemove);
			getModelService().removeAll(stockLevelsToRemove);
		}
	}

	/**
	 * Stock Levels are updated or created on the basis of the activity schedule and the inventory data
	 *
	 * @param activityInventoryData the activityInventoryData.
	 * @param activityProductCode   the activityProductCode
	 * @param stockLevels           the stockLevels
	 * @param activityProduct       the activityProduct
	 * @param date                  the date
	 * @param stockLevelsToSave     the stockLevelsToSave
	 * @param defaultWarehouse      the default warehouse
	 */
	private void setBcfStockLevelForScheduledActivity(final ActivityInventoryDataModel activityInventoryData,
			final String activityProductCode, final Set<StockLevelModel> stockLevels, final ActivityProductModel activityProduct,
			final Date date, final List<StockLevelModel> stockLevelsToSave, final WarehouseModel defaultWarehouse)
	{
		final List<ActivityScheduleModel> activityScheduleList = activityProduct.getActivityScheduleList().stream().filter(
				schedule -> (schedule.getStartDate().equals(date) || (date.after(schedule.getStartDate()) && date
						.before(schedule.getEndDate()) || date.equals(schedule.getEndDate())))).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(activityScheduleList))
		{
			activityScheduleList.forEach(schedule -> {
				final boolean isStockCodePresent =
						CollectionUtils.isNotEmpty(stockLevels) && stockLevels.stream().anyMatch(stockLevel -> {
							if (stockLevel.getProductCode().equals(activityProductCode)
									&& TravelDateUtils.isSameDate(stockLevel.getDate(), date) && stockLevel.getTime()
									.equals(schedule.getStartTime()))
							{
								updateStockLevel(stockLevelsToSave, stockLevel, activityProduct, date, activityInventoryData);
								return true;
							}
							return false;
						});
				if (!isStockCodePresent)
				{
					createIfBcfStockCodeNotPresent(activityProduct, date, schedule.getStartTime(), stockLevelsToSave, defaultWarehouse,
							activityInventoryData);
				}
			});
		}
	}


	/**
	 * If a mapping file is provided stock levels are populated according to its content otherwise their availability is
	 * simulated by a populating algorithm
	 *
	 * @param activityInventoryData the activityInventoryData.
	 * @param activityProductCode   the activityProductCode
	 * @param stockLevels           the stockLevels
	 * @param activityProduct       the activityProduct
	 * @param date                  the date
	 * @param stockLevelsToSave     the stockLevelsToSave
	 * @param defaultWarehouse      the default warehouse
	 */
	private void setBcfStockLevelForActivity(final ActivityInventoryDataModel activityInventoryData,
			final String activityProductCode, final Set<StockLevelModel> stockLevels, final ActivityProductModel activityProduct,
			final Date date, final List<StockLevelModel> stockLevelsToSave, final WarehouseModel defaultWarehouse)
	{
		final boolean isStockCodePresent =
				CollectionUtils.isNotEmpty(stockLevels) && stockLevels.stream().anyMatch(stockLevel -> {
					if (stockLevel.getProductCode().equals(activityProductCode)
							&& TravelDateUtils.isSameDate(stockLevel.getDate(), date)
							&& ((Objects.isNull(stockLevel.getTime()) && (Objects.isNull(activityInventoryData.getStartTime())))
							|| (Objects.nonNull(stockLevel.getTime()) && Objects.nonNull(activityInventoryData.getStartTime())
							&& stockLevel.getTime().equals(activityInventoryData.getStartTime()))))
					{
						updateStockLevel(stockLevelsToSave, stockLevel, activityProduct, date, activityInventoryData);
						return true;
					}
					return false;
				});
		if (!isStockCodePresent)
		{
			createIfBcfStockCodeNotPresent(activityProduct, date,
					activityInventoryData.getStartTime(), stockLevelsToSave,
					defaultWarehouse, activityInventoryData);
		}
	}

	private void updateStockLevel(final List<StockLevelModel> stockLevelsToSave, final StockLevelModel stockLevel,
			final ActivityProductModel activityProduct,
			final Date date, final ActivityInventoryDataModel activityInventoryData)
	{
		stockLevel.setAvailable(activityInventoryData.getAvailableQuantity());
		stockLevel.setProduct(activityProduct);
		final Integer overSelling = Objects.isNull(activityInventoryData.getOverSellingQuantity()) ?
				0 :
				activityInventoryData.getOverSellingQuantity();
		stockLevel.setOverSelling(overSelling);
		stockLevel.setReleaseBlockDay(TravelDateUtils.addDays(date, -activityInventoryData.getBlockReleaseDays()));
		stockLevelsToSave.add(stockLevel);
	}

	private void createIfBcfStockCodeNotPresent(final ActivityProductModel activityProduct, final Date date,
			final String startTime,
			final List<StockLevelModel> stockLevelsToSave,
			final WarehouseModel defaultWarehouse,
			final ActivityInventoryDataModel activityInventoryData)
	{
		final StockLevelModel stockLevelModel = getModelService().create(StockLevelModel.class);
		stockLevelModel.setWarehouse(defaultWarehouse);
		stockLevelModel.setProductCode(activityProduct.getCode());
		stockLevelModel.setAvailable(activityInventoryData.getAvailableQuantity());
		stockLevelModel.setReserved(0);
		stockLevelModel.setProduct(activityProduct);
		stockLevelModel.setDate(date);
		stockLevelModel.setTime(startTime);
		stockLevelModel.setStockLevelType(activityProduct.getStockLevelType());
		stockLevelModel.setReleaseBlockDay(TravelDateUtils.addDays(date, -activityInventoryData.getBlockReleaseDays()));
		final Integer overSelling = Objects.isNull(activityInventoryData.getOverSellingQuantity()) ?
				0 :
				activityInventoryData.getOverSellingQuantity();
		stockLevelModel.setOverSelling(overSelling);
		stockLevelsToSave.add(stockLevelModel);
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Required
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	protected BcfTravelCommerceStockService getBcfTravelCommerceStockService()
	{
		return bcfTravelCommerceStockService;
	}

	@Required
	public void setBcfTravelCommerceStockService(final BcfTravelCommerceStockService bcfTravelCommerceStockService)
	{
		this.bcfTravelCommerceStockService = bcfTravelCommerceStockService;
	}
}
