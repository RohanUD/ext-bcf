/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivityInventoryDataDao;
import com.bcf.integration.dataupload.service.activities.ActivityInventoryDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivityInventoryDataService implements ActivityInventoryDataService
{
	private ActivityInventoryDataDao activityInventoryDataDao;

	@Override
	public List<ActivityInventoryDataModel> getActivityInventoryData(final DataImportProcessStatus processStatus)
	{
		return getActivityInventoryDataDao().findActivityInventoryData(processStatus);
	}

	protected ActivityInventoryDataDao getActivityInventoryDataDao()
	{
		return activityInventoryDataDao;
	}

	@Required
	public void setActivityInventoryDataDao(final ActivityInventoryDataDao activityInventoryDataDao)
	{
		this.activityInventoryDataDao = activityInventoryDataDao;
	}
}
