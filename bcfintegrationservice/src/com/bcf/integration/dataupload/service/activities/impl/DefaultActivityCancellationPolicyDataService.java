/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivityCancellationPolicyDataDao;
import com.bcf.integration.dataupload.service.activities.ActivityCancellationPolicyDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivityCancellationPolicyDataService implements ActivityCancellationPolicyDataService
{
	 private ActivityCancellationPolicyDataDao activityCancellationPolicyDataDao;

	@Override
	public List<ActivityCancellationPolicyDataModel> getCancellationPolicyData(final DataImportProcessStatus processStatus)
	{
		return getActivityCancellationPolicyDataDao().findCancellationPolicyData(processStatus);
	}

	@Override
	public ActivityCancellationPolicyDataModel getCancellationPolicyFromCode(final String code)
	{
		return getActivityCancellationPolicyDataDao().findCancellationPolicyData(code);
	}



	public ActivityCancellationPolicyDataDao getActivityCancellationPolicyDataDao()
	{
		return activityCancellationPolicyDataDao;
	}

	public void setActivityCancellationPolicyDataDao(
			final ActivityCancellationPolicyDataDao activityCancellationPolicyDataDao)
	{
		this.activityCancellationPolicyDataDao = activityCancellationPolicyDataDao;
	}
}
