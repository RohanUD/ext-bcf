/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivitiesDataDao;
import com.bcf.integration.dataupload.dao.activities.ActivityPricesDataDao;
import com.bcf.integration.dataupload.service.activities.ActivitiesDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivitiesDataService implements ActivitiesDataService
{
	private ActivitiesDataDao activitiesDataDao;
	private ActivityPricesDataDao activityPricesDataDao;

	@Override
	public List<ActivitiesDataModel> getActivitiesData(final DataImportProcessStatus processStatus)
	{
		return getActivitiesDataDao().findActivitiesData(processStatus);
	}

	@Override
	public ActivitiesDataModel getActivitiesData(final String code)
	{
		return getActivitiesDataDao().findActivitiesData(code);
	}

	@Override
	public List<ActivityPriceDataModel> getActivityPricesData(final DataImportProcessStatus processStatus)
	{
		return getActivityPricesDataDao().findActivityPricesData(processStatus);
	}

	public List<ActivityPriceDataModel> getExistingActivityPricesData(final ActivityPriceDataModel model)
	{
		return getActivityPricesDataDao()
				.findActivityPricesData(model.getActivitiesData().getCode(), model.getPassengerType().getCode(), model.getStartDate(),
						model.getEndDate(), model.getDaysOfWeek());
	}

	/**
	 * @return the activitiesDataDao
	 */
	protected ActivitiesDataDao getActivitiesDataDao()
	{
		return activitiesDataDao;
	}

	/**
	 * @param activitiesDataDao the activitiesDataDao to set
	 */
	@Required
	public void setActivitiesDataDao(final ActivitiesDataDao activitiesDataDao)
	{
		this.activitiesDataDao = activitiesDataDao;
	}

	protected ActivityPricesDataDao getActivityPricesDataDao()
	{
		return activityPricesDataDao;
	}

	@Required
	public void setActivityPricesDataDao(final ActivityPricesDataDao activityPricesDataDao)
	{
		this.activityPricesDataDao = activityPricesDataDao;
	}
}
