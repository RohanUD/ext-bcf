/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.service.activities.impl;

import java.util.List;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationFeesDataModel;
import com.bcf.integration.dataupload.dao.activities.ActivityCancellationFeesDataDao;
import com.bcf.integration.dataupload.service.activities.ActivityCancellationFeesDataService;
import com.bcf.integration.enums.DataImportProcessStatus;


public class DefaultActivityCancellationFeesDataService implements ActivityCancellationFeesDataService
{
	private ActivityCancellationFeesDataDao activityCancellationFeesDataDao;

	@Override
	public List<ActivityCancellationFeesDataModel> getCancellationFeesData(final DataImportProcessStatus processStatus)
	{
		return getActivityCancellationFeesDataDao().findCancellationFeesData(processStatus);
	}

	public ActivityCancellationFeesDataDao getActivityCancellationFeesDataDao()
	{
		return activityCancellationFeesDataDao;
	}

	public void setActivityCancellationFeesDataDao(final ActivityCancellationFeesDataDao activityCancellationFeesDataDao)
	{
		this.activityCancellationFeesDataDao = activityCancellationFeesDataDao;
	}
}
