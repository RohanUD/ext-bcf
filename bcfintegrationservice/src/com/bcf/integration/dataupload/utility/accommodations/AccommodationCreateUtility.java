/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.enums.AccommodationType;
import de.hybris.platform.travelservices.enums.RoomType;
import de.hybris.platform.travelservices.model.accommodation.GuestOccupancyModel;
import de.hybris.platform.travelservices.model.facility.AccommodationFacilityModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.core.accommodation.service.AccommodationFacilityService;
import com.bcf.core.accommodation.service.GuestOccupancyService;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.core.accommodation.service.RoomTypeMappingService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;


public class AccommodationCreateUtility extends AbstractDataUploadUtility
{
	private static final String ADULT = "adult";
	private static final String ACCOMMODATION = "ACCOMMODATION";

	private AccommodationFacilityService accommodationFacilityService;
	private GuestOccupancyService guestOccupancyService;
	private CategoryService categoryService;
	private RoomTypeMappingService roomTypeMappingService;
	private ProductTaxGroupMappingService productTaxGroupMappingService;
	private Map<Integer, RoomType> roomTypePersonMappingMap;

	/**
	 * Creates the new accommodation.
	 *
	 * @param accommodationData the accommodation data
	 * @return the accommodation model
	 */
	public AccommodationModel createOrUpdateAccommodation(final String accommodationCode,
			final AccommodationDataModel accommodationData, final List<ItemModel> items)
	{
		if (accommodationData.isDeleted())
		{
			return markDeleted(accommodationCode);
		}

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);

		AccommodationModel accommodation = null;
		try
		{
			accommodation = (AccommodationModel) getProductService().getProductForCode(catalogVersion, accommodationCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			accommodation = getModelService().create(AccommodationModel.class);
			accommodation.setCatalogVersion(catalogVersion);
			accommodation.setCode(accommodationCode);
		}
		populateAccommodationData(accommodation, accommodationData, catalogVersion);
		items.add(accommodation);
		return accommodation;
	}

	private AccommodationModel markDeleted(final String accommodationCode)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		AccommodationModel accommodation = null;
		try
		{
			accommodation = (AccommodationModel) getProductService().getProductForCode(catalogVersion, accommodationCode);
			accommodation.setDeleted(Boolean.TRUE);
			getModelService().save(accommodation);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			//do nothing
		}
		return accommodation;
	}

	protected void populateAccommodationData(final AccommodationModel accommodation,
			final AccommodationDataModel accommodationData, final CatalogVersionModel catalogVersion)
	{
		getLocaleList().forEach(locale -> accommodation.setName(accommodationData.getAccommodationName(locale), locale));
		getLocaleList()
				.forEach(locale -> accommodation.setDescription(accommodationData.getAccommodationDescription(locale), locale));
		accommodation.setSize(accommodationData.getAccommodationSize());
		accommodation.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		accommodation.setType(AccommodationType.ROOM);
		accommodation.setBaseOccupancyCount(accommodationData.getBaseOccupancyCount());
		accommodation.setMaxOccupancyCount(accommodationData.getMaxOccupancyCount());
		accommodation.setStockLevelType(accommodationData.getStockLevelType());
		accommodation.setDeleted(accommodationData.isDeleted());
		accommodation.setSaleStatuses(
				Sets.newHashSet(createOrGetSaleStatues(accommodationData.getSaleStatuses(), accommodation.getSaleStatuses())));

		final CategoryModel category = getCategoryService().getCategoryForCode(catalogVersion, ACCOMMODATION);
		accommodation.setSupercategories(Collections.singletonList(category));

		final Collection<AccommodationFacilityModel> facilities = getAccommodationFacilityService()
				.getAccommodationFacilities(accommodationData.getAccommodationFacilities());
		accommodation.setAccommodationFacility(facilities);

		List<GuestOccupancyModel> guestOccupancies = accommodationData.getGuestOccupancies().isEmpty() ? new ArrayList<>()
				: new ArrayList<>(accommodationData.getGuestOccupancies());
		if (guestOccupancies.isEmpty())
		{
			guestOccupancies = createNewGuestOccupancyModel(ADULT, accommodation.getBaseOccupancyCount(), 1);
		}
		accommodation.setGuestOccupancies(guestOccupancies);
		final Integer baseOccupancyCount = accommodationData.getBaseOccupancyCount();
		final RoomType roomType = getRoomTypePersonMappingMap().get(baseOccupancyCount);
		accommodation.setRoomType(roomType);
	}

	private List<GuestOccupancyModel> createNewGuestOccupancyModel(final String passengerType, final Integer quantityMax,
			final Integer quantityMin)
	{
		return getGuestOccupancyService().getGuestOccupancies(passengerType, quantityMax, quantityMin);
	}

	/**
	 * @return the accommodationFacilityService
	 */
	protected AccommodationFacilityService getAccommodationFacilityService()
	{
		return accommodationFacilityService;
	}

	/**
	 * @param accommodationFacilityService the accommodationFacilityService to set
	 */
	@Required
	public void setAccommodationFacilityService(final AccommodationFacilityService accommodationFacilityService)
	{
		this.accommodationFacilityService = accommodationFacilityService;
	}

	/**
	 * @return the guestOccupancyService
	 */
	protected GuestOccupancyService getGuestOccupancyService()
	{
		return guestOccupancyService;
	}

	/**
	 * @param guestOccupancyService the guestOccupancyService to set
	 */
	@Required
	public void setGuestOccupancyService(final GuestOccupancyService guestOccupancyService)
	{
		this.guestOccupancyService = guestOccupancyService;
	}

	/**
	 * @return the categoryService
	 */
	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	/**
	 * @param categoryService the categoryService to set
	 */
	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	/**
	 * @return the roomTypeMappingService
	 */
	protected RoomTypeMappingService getRoomTypeMappingService()
	{
		return roomTypeMappingService;
	}

	/**
	 * @param roomTypeMappingService the roomTypeMappingService to set
	 */
	@Required
	public void setRoomTypeMappingService(final RoomTypeMappingService roomTypeMappingService)
	{
		this.roomTypeMappingService = roomTypeMappingService;
	}

	/**
	 * @return the productTaxGroupMappingService
	 */
	protected ProductTaxGroupMappingService getProductTaxGroupMappingService()
	{
		return productTaxGroupMappingService;
	}

	/**
	 * @param productTaxGroupMappingService the productTaxGroupMappingService to set
	 */
	@Required
	public void setProductTaxGroupMappingService(final ProductTaxGroupMappingService productTaxGroupMappingService)
	{
		this.productTaxGroupMappingService = productTaxGroupMappingService;
	}

	protected Map<Integer, RoomType> getRoomTypePersonMappingMap()
	{
		return roomTypePersonMappingMap;
	}

	@Required
	public void setRoomTypePersonMappingMap(
			final Map<Integer, RoomType> roomTypePersonMappingMap)
	{
		this.roomTypePersonMappingMap = roomTypePersonMappingMap;
	}
}
