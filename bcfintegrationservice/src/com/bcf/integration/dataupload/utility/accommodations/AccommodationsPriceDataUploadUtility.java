/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.cronjob.MarketingRatePlanInfoCronjobModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.RatePlanService;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.cronjob.BcfUpdateStockLevelsToAccommodationOfferingJob;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.service.accommodations.RatePlanDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;


public class AccommodationsPriceDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(AccommodationsPriceDataUploadUtility.class);

	private static final String MARKETING_RATEPLANINFO_CRONJOB = "marketingRatePlanInfoCronjob";
	private static final String BCFERRIES_INDEX = "bcferriesIndex";

	private RatePlanDataService ratePlanDataService;
	private BcfAccommodationService bcfAccommodationService;
	private AccommodationOfferingService accommodationOfferingService;
	private RatePlanService ratePlanService;
	private RatePlanCreateUtility ratePlanCreateUtility;
	private BcfUpdateStockLevelsToAccommodationOfferingJob bcfUpdateStockLevelsToAccommodationOfferingJob;
	private CronJobService cronJobService;
	private SetupSolrIndexerService setupSolrIndexerService;
	private ExtraGuestOccupancyProductUploadUtility extraGuestOccupancyProductUploadUtility;

	/**
	 * Upload rate plan datas.
	 *
	 * @return the List of String
	 */
	public void uploadRatePlansData()
	{
		final List<RatePlanDataModel> ratePlansDatas = getRatePlanDataService().getRatePlanData(DataImportProcessStatus.PENDING);
		if (CollectionUtils.isEmpty(ratePlansDatas))
		{
			LOG.error("The Impex import was unsuccessfull. Please check the data being imported.");
			return;
		}
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();
			final List<ItemModel> itemModels = new ArrayList<>();
			uploadRatePlansData(new ArrayList<>(ratePlansDatas), itemModels);
			LOG.info("Transaction Commit");
			tx.commit();
			final List<ItemModel> items = collectItemsForSync(ratePlansDatas);
			if (CollectionUtils.isNotEmpty(items))
			{
				synchronizeProductCatalog(items);
				getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCVACATION_INDEX, false);
			}
		}
		catch (final ModelSavingException | IllegalArgumentException ex)
		{
			LOG.error("Transaction RollBack : " + ex);
			tx.rollback();
		}
	}

	private List<ItemModel> collectItemsForSync(final List<RatePlanDataModel> ratePlansDatas)
	{
		final List<ItemModel> items = new ArrayList<>();
		final Map<AccommodationOfferingDataModel, List<RatePlanDataModel>> ratePlanAccommodationsOfferingsMap = ratePlansDatas
				.stream().filter(ratePlan -> Objects.nonNull(ratePlan.getAccommodationOffering()))
				.collect(Collectors.groupingBy(RatePlanDataModel::getAccommodationOffering));
		for (final Map.Entry<AccommodationOfferingDataModel, List<RatePlanDataModel>> accommodationOfferingEntry : ratePlanAccommodationsOfferingsMap
				.entrySet())
		{
			final AccommodationOfferingDataModel accommodationOfferingData = accommodationOfferingEntry.getKey();
			collectAccommodationOfferingRelatedItems(accommodationOfferingData, items);
		}
		return items;
	}

	/**
	 * Upload rate plan data.
	 *
	 * @param ratePlansData the list of rate plan data
	 */
	public void uploadRatePlansData(final List<RatePlanDataModel> ratePlansData, final List<ItemModel> items)
	{
		if (CollectionUtils.isEmpty(ratePlansData))
		{
			return;
		}

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		ratePlansData.forEach(ratePlanData -> ratePlanData.setStatus(DataImportProcessStatus.PROCESSING));

		final List<AccommodationOfferingModel> accommodationOfferings = findAccommodationOffering(ratePlansData, catalogVersion,
				items);
		if (accommodationOfferings.isEmpty())
		{
			throw new IllegalArgumentException("Data validation failed");
		}
		updateStatus(ratePlansData, DataImportProcessStatus.PROCESSED, DataImportProcessStatus.SUCCESS);

		getModelService().saveAll(ratePlansData);

		updateRatePlan(accommodationOfferings, catalogVersion, items);

	}

	/**
	 * Find accommodation offering .
	 *
	 * @param ratePlansData  the list of rate plan data
	 * @param catalogVersion the catalog version
	 * @return the Accommodation Offering Model
	 */
	private List<AccommodationOfferingModel> findAccommodationOffering(
			final List<RatePlanDataModel> ratePlansData, final CatalogVersionModel catalogVersion, final List<ItemModel> items)
	{
		final boolean isAllRoomRateDataValid = ratePlansData.stream().allMatch(this::validateData);
		if (!isAllRoomRateDataValid)
		{
			return Collections.emptyList();
		}

		final List<AccommodationOfferingModel> accommodationOfferings = new ArrayList<>();
		final Map<AccommodationOfferingDataModel, List<RatePlanDataModel>> ratePlanAccommodationsOfferingsMap = ratePlansData
				.stream().filter(ratePlan -> Objects.nonNull(ratePlan.getAccommodationOffering()))
				.collect(Collectors.groupingBy(RatePlanDataModel::getAccommodationOffering));
		for (final Map.Entry<AccommodationOfferingDataModel, List<RatePlanDataModel>> accommodationOfferingEntry : ratePlanAccommodationsOfferingsMap
				.entrySet())
		{
			final List<RatePlanDataModel> groupedRatePlanAccommodationOfferings = accommodationOfferingEntry.getValue();
			final String accommodationOfferingCode = accommodationOfferingEntry.getKey().getCode();
			AccommodationOfferingModel accommodationOffering = null;
			try
			{
				accommodationOffering = getAccommodationOfferingService().getAccommodationOffering(accommodationOfferingCode);
			}
			catch (final ModelNotFoundException | AmbiguousIdentifierException ex)
			{
				//Deliberately Left Empty
			}
			if (Objects.isNull(accommodationOffering))
			{
				LOG.error("Could not found accommodationOffering corresponding to accommodationOfferingCode "
						+ accommodationOfferingCode);
				getModelService().removeAll(groupedRatePlanAccommodationOfferings);
				continue;
			}

			final boolean isSuccess = performAccommodationLinking(accommodationOffering, groupedRatePlanAccommodationOfferings,
					catalogVersion, items);
			if (isSuccess)
			{
				accommodationOfferings.add(accommodationOffering);
			}
		}

		return accommodationOfferings;
	}

	/**
	 * Find accommodation.
	 *
	 * @param accommodationOffering                 the accommodation offering
	 * @param groupedRatePlanAccommodationOfferings the list of rate plan by accommodation offering code
	 * @param catalogVersion                        the catalog version
	 * @return the Accommodation Model
	 */
	private boolean performAccommodationLinking(
			final AccommodationOfferingModel accommodationOffering,
			final List<RatePlanDataModel> groupedRatePlanAccommodationOfferings,
			final CatalogVersionModel catalogVersion, final List<ItemModel> items)
	{
		boolean isLinkingSuccess = false;
		final Map<AccommodationDataModel, List<RatePlanDataModel>> accommodationsMap = groupedRatePlanAccommodationOfferings
				.stream()
				.filter(accommodation -> Objects.nonNull(accommodation.getAccommodation()))
				.collect(Collectors.groupingBy(RatePlanDataModel::getAccommodation));

		for (final Map.Entry<AccommodationDataModel, List<RatePlanDataModel>> accommodationEntry : accommodationsMap.entrySet())
		{
			final List<RatePlanDataModel> groupedRatePlanAccommodations = accommodationEntry.getValue();
			final RatePlanDataModel ratePlanAccommodationData = groupedRatePlanAccommodations.get(0);
			final String accommodationCode =
					accommodationOffering.getCode() + "_" + ratePlanAccommodationData.getAccommodation().getCode();
			final AccommodationModel accommodationModel = findAccommodationModel(accommodationCode, catalogVersion);
			if (Objects.isNull(accommodationModel))
			{
				LOG.error("Could not found accommodation corresponding to accommodationCode "
						+ accommodationCode);

				groupedRatePlanAccommodationOfferings.removeAll(groupedRatePlanAccommodations);
				getModelService().removeAll(groupedRatePlanAccommodations);
				continue;
			}


			linkRatePlanDatasToAccommodation(accommodationOffering, accommodationModel, groupedRatePlanAccommodations, items);
			linkRatePlanDataToAccommodationData(groupedRatePlanAccommodations);
			final List<String> accommodations = Objects.isNull(accommodationOffering.getAccommodations()) ?
					new ArrayList<>() : new ArrayList<>(accommodationOffering.getAccommodations());
			if (!accommodations.contains(accommodationCode))
			{
				accommodations.add(accommodationCode);
			}
			accommodationOffering.setAccommodations(accommodations);
			isLinkingSuccess = true;
			getModelService().saveAll();
		}

		return isLinkingSuccess;
	}

	private AccommodationModel findAccommodationModel(final String accommodationCode,
			final CatalogVersionModel catalogVersion)
	{
		AccommodationModel accommodationModel = null;
		try
		{
			accommodationModel = getBcfAccommodationService().getAccommodation(accommodationCode, catalogVersion);
		}
		catch (final ModelNotFoundException | AmbiguousIdentifierException ex)
		{
			//Deliberately Left Empty.
		}
		return accommodationModel;
	}

	/**
	 * Upload rate plan data.
	 *
	 * @param accommodationOffering         the accommodation offering
	 * @param accommodation                 the accommodation
	 * @param groupedRatePlanAccommodations the list of rate plan by accommodation
	 * @return the Collection of rate plan model
	 */
	private void linkRatePlanDatasToAccommodation(
			final AccommodationOfferingModel accommodationOffering,
			final AccommodationModel accommodation, final List<RatePlanDataModel> groupedRatePlanAccommodations,
			final List<ItemModel> items)
	{
		final List<RatePlanModel> ratePlans = new ArrayList<>();
		final Map<String, List<RatePlanDataModel>> ratePlanCodesDataMap = groupedRatePlanAccommodations.stream()
				.collect(Collectors.groupingBy(RatePlanDataModel::getCode));
		for (final Map.Entry<String, List<RatePlanDataModel>> ratePlanCodeDataEntry : ratePlanCodesDataMap.entrySet())
		{
			final List<RatePlanDataModel> groupedRatePlanCodesData = ratePlanCodeDataEntry.getValue();

			final RatePlanDataModel ratePlanData = groupedRatePlanCodesData.get(0);

			final String ratePlanCode = ratePlanData.getCode();
			RatePlanModel ratePlan = null;
			try
			{
				ratePlan = getRatePlanService().getRatePlanForCode(ratePlanCode);
			}
			catch (final ModelNotFoundException ex)
			{
				LOG.error("RatePlanModel not found");
			}
			if (Objects.nonNull(ratePlan))
			{
				ratePlan = getRatePlanCreateUtility()
						.updateRatePlan(accommodationOffering, accommodation, groupedRatePlanCodesData, ratePlan, items);
			}
			else
			{
				ratePlan = getRatePlanCreateUtility()
						.createNewRatePlan(accommodationOffering, accommodation, groupedRatePlanCodesData, ratePlanCode, items);
				ratePlans.add(ratePlan);
			}
			if (Objects.isNull(accommodation.getRatePlan()))
			{
				accommodation.setRatePlan(new ArrayList<>());
			}
			if (!accommodation.getRatePlan().contains(ratePlan))
			{
				final List<RatePlanModel> accommodationRatePlans = new ArrayList<>(accommodation.getRatePlan());
				accommodationRatePlans.add(ratePlan);
				accommodation.setRatePlan(accommodationRatePlans);
			}
			if(accommodation.getRatePlan().contains(ratePlan) && getModelService().isModified(ratePlan))
			{
				getModelService().save(ratePlan);
			}
		}
	}

	protected void linkRatePlanDataToAccommodationData(final List<RatePlanDataModel> ratePlanDataModels)
	{

		final AccommodationDataModel accommodationDataModel = ratePlanDataModels.get(0).getAccommodation();
		final Collection<RatePlanDataModel> distinctRatePlanDataModel = ratePlanDataModels.stream().collect(Collectors
				.groupingBy(RatePlanDataModel::getCode,
						Collectors.collectingAndThen(Collectors.toList(), ratePlanModel -> ratePlanModel.stream().findFirst().get())))
				.values();
		if (CollectionUtils.isEmpty(accommodationDataModel.getRatePlanDatas()))
		{
			accommodationDataModel.setRatePlanDatas(distinctRatePlanDataModel);
			return;
		}
		if (Objects.isNull(accommodationDataModel.getRatePlanDatas()))
		{
			accommodationDataModel.setRatePlanDatas(new ArrayList<>());
		}
		if (accommodationDataModel.getRatePlanDatas().size() == 0)
		{
			accommodationDataModel.setRatePlanDatas(distinctRatePlanDataModel);
			return;
		}
		final List<RatePlanDataModel> oldRatePlans = accommodationDataModel.getRatePlanDatas().stream()
				.filter(ratePlan -> distinctRatePlanDataModel.stream()
						.anyMatch(newRatePlanData -> newRatePlanData.getCode().equals(ratePlan.getCode()))).collect(
						Collectors.toList());
		final List<RatePlanDataModel> newRatePlanDataModelList = new ArrayList<>(accommodationDataModel.getRatePlanDatas());
		newRatePlanDataModelList.removeAll(oldRatePlans);
		newRatePlanDataModelList.addAll(distinctRatePlanDataModel);
		accommodationDataModel.setRatePlanDatas(newRatePlanDataModelList);
	}

	/**
	 * Update stock and rate plan.
	 *
	 * @param accommodationOfferings the list of accommodation offering
	 * @param catalogVersion         the catalog version
	 */
	public void updateRatePlan(final List<AccommodationOfferingModel> accommodationOfferings,
			final CatalogVersionModel catalogVersion, final List<ItemModel> items)
	{
		final MarketingRatePlanInfoCronjobModel marketingRatePlanInfoCronjob = (MarketingRatePlanInfoCronjobModel) getCronJobService()
				.getCronJob(MARKETING_RATEPLANINFO_CRONJOB);
		marketingRatePlanInfoCronjob.setAccommodationOfferings(accommodationOfferings);
		getModelService().save(marketingRatePlanInfoCronjob);
		getCronJobService().performCronJob(marketingRatePlanInfoCronjob, true);

		while (marketingRatePlanInfoCronjob.getStatus().equals(CronJobStatus.RUNNING))
		{
			// Waiting for cronjob to finish.
		}

		final List<MarketingRatePlanInfoModel> marketingRatePlanInfos = accommodationOfferings.stream()
				.flatMap(accommodationOffering -> accommodationOffering.getMarketingRatePlanInfos().stream())
				.collect(Collectors.toList());

		items.addAll(marketingRatePlanInfos);
		marketingRatePlanInfos.stream().forEach(
				marketingRatePlanInfo -> items.addAll(marketingRatePlanInfo.getRatePlanConfig()));
	}

	private void updateStatus(final List<RatePlanDataModel> ratePlanDataList,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		ratePlanDataList.stream().filter(ratePlanData -> fromStatus.equals(ratePlanData.getStatus()))
				.forEach(ratePlanData -> ratePlanData.setStatus(toStatus));
	}

	/**
	 * Validate room rate data.
	 *
	 * @param ratePlanData the rate Plan data
	 * @return true, if successful
	 */
	protected boolean validateData(final RatePlanDataModel ratePlanData)
	{
		if (Objects.isNull(ratePlanData.getAccommodationOffering()))
		{
			LOG.error("accommodationOfferingCode is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.isNull(ratePlanData.getAccommodation()))
		{
			LOG.error("accommodationCode is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.isNull(ratePlanData.getCode()))
		{
			LOG.error("ratePlan is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.isNull(ratePlanData.getRatePlanStartDate()))
		{
			LOG.error("start date is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.isNull(ratePlanData.getRatePlanEndDate()))
		{
			LOG.error("end date is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.isNull(ratePlanData.getPrice()))
		{
			LOG.error("end date is empty for " + ratePlanData.getCode());
			return false;
		}
		if (Objects.nonNull(ratePlanData.getDiscount()) && Objects.isNull(ratePlanData.getDiscountType()))
		{
			LOG.error("discount type can not be empty if discount is provided for " + ratePlanData.getCode());
			return false;
		}

		return true;
	}

	protected RatePlanDataService getRatePlanDataService()
	{
		return ratePlanDataService;
	}

	@Required
	public void setRatePlanDataService(final RatePlanDataService ratePlanDataService)
	{
		this.ratePlanDataService = ratePlanDataService;
	}

	@Override
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Override
	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected BcfUpdateStockLevelsToAccommodationOfferingJob getBcfUpdateStockLevelsToAccommodationOfferingJob()
	{
		return bcfUpdateStockLevelsToAccommodationOfferingJob;
	}

	@Required
	public void setBcfUpdateStockLevelsToAccommodationOfferingJob(
			final BcfUpdateStockLevelsToAccommodationOfferingJob bcfUpdateStockLevelsToAccommodationOfferingJob)
	{
		this.bcfUpdateStockLevelsToAccommodationOfferingJob = bcfUpdateStockLevelsToAccommodationOfferingJob;
	}

	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}

	@Required
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}

	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}

	protected RatePlanCreateUtility getRatePlanCreateUtility()
	{
		return ratePlanCreateUtility;
	}

	@Required
	public void setRatePlanCreateUtility(final RatePlanCreateUtility ratePlanCreateUtility)
	{
		this.ratePlanCreateUtility = ratePlanCreateUtility;
	}

	protected ExtraGuestOccupancyProductUploadUtility getExtraGuestOccupancyProductUploadUtility()
	{
		return extraGuestOccupancyProductUploadUtility;
	}

	@Required
	public void setExtraGuestOccupancyProductUploadUtility(
			final ExtraGuestOccupancyProductUploadUtility extraGuestOccupancyProductUploadUtility)
	{
		this.extraGuestOccupancyProductUploadUtility = extraGuestOccupancyProductUploadUtility;
	}
}
