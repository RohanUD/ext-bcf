/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.dataupload.utility.accommodations;


import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.cronjob.BcfUpdateStockLevelsToAccommodationOfferingJob;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.service.accommodations.AccommodationDataService;
import com.bcf.integration.dataupload.service.accommodations.AccommodationsOfferingDataService;
import com.bcf.integration.dataupload.service.accommodations.RatePlanDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;


public class AccommodationsDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(AccommodationsDataUploadUtility.class);

	private static final String ACCOMMODATION_OFFERING_NOT_CREATED = "accommodation.offering.not.created";

	private AccommodationOfferingCreateUtility accommodationOfferingCreateUtility;
	private AccommodationCreateUtility accommodationCreateUtility;
	private RoomRatesCreateUtility roomRatesCreateUtility;

	private AccommodationDataService accommodationDataService;
	private AccommodationsOfferingDataService accommodationsOfferingDataService;
	private RatePlanDataService ratePlanDataService;
	private AccommodationOfferingService accommodationOfferingService;
	private ProductService productService;
	private BcfUpdateStockLevelsToAccommodationOfferingJob bcfUpdateStockLevelsToAccommodationOfferingJob;
	private CronJobService cronJobService;
	private SetupSolrIndexerService setupSolrIndexerService;
	private PropertySourceFacade propertySourceFacade;
	private BcfAccommodationService accommodationService;

	/**
	 * Upload accommodations data.
	 *
	 * @return the list
	 */
	public void uploadAccommodationsData()
	{
		final List<AccommodationOfferingDataModel> accommodationsOfferingData = getAccommodationsOfferingDataService()
				.getAccommodationsOfferingData(DataImportProcessStatus.PENDING);
		List<AccommodationOfferingDataModel> filterNonPartialSaveAndPublishAccommodationOfferings = null;
		if (CollectionUtils.isNotEmpty(accommodationsOfferingData))
		{
			filterNonPartialSaveAndPublishAccommodationOfferings = accommodationsOfferingData.stream()
					.filter(accommodationOfferingDataModel -> !accommodationOfferingDataModel.isPartialSave())
					.collect(Collectors.toList());
		}

		if (CollectionUtils.isEmpty(filterNonPartialSaveAndPublishAccommodationOfferings))
		{
			return;
		}
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();
			final List<ItemModel> items = new ArrayList<>();
			uploadAccommodationsData(filterNonPartialSaveAndPublishAccommodationOfferings, items);
			LOG.info("Transaction Commit");
			tx.commit();
		}
		catch (final ModelSavingException ex)
		{
			LOG.error("Transaction RollBack : " + ex, ex);
			tx.rollback();
		}
		catch(final Exception e){
			LOG.error("Transaction RollBack : ", e);
			tx.rollback();
		}
	}

	public List<AccommodationOfferingModel> uploadAccommodationsData(
			final List<AccommodationOfferingDataModel> accommodationsOfferingData, final List<ItemModel> items)
	{

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final Map<String, List<AccommodationOfferingDataModel>> accommodationsOfferingsDataMap = accommodationsOfferingData
				.stream().filter(accommodationOfferingData -> StringUtils.isNotEmpty(accommodationOfferingData.getCode()))
				.collect(Collectors.groupingBy(AccommodationOfferingDataModel::getCode));

		final List<AccommodationOfferingModel> accommodationOfferings = new ArrayList<>();
		for (final Entry<String, List<AccommodationOfferingDataModel>> accommodationsOfferingsDataEntry : accommodationsOfferingsDataMap
				.entrySet())
		{
			final String code = accommodationsOfferingsDataEntry.getKey();
			final List<AccommodationOfferingDataModel> groupedAccommodationOfferingsData = accommodationsOfferingsDataEntry
					.getValue();
			groupedAccommodationOfferingsData
					.forEach(accommodationOfferingData -> accommodationOfferingData.setStatus(DataImportProcessStatus.PROCESSING));

			final AccommodationOfferingModel accommodationOffering = uploadAccommodationsOfferingData(code,
					groupedAccommodationOfferingsData, items);
			if (Objects.isNull(accommodationOffering))
			{
				continue;
			}
			accommodationOfferings.add(accommodationOffering);
			updateStatusOfAccommodationOfferings(groupedAccommodationOfferingsData, DataImportProcessStatus.PROCESSING,
					DataImportProcessStatus.PROCESSED);
			getModelService().saveAll();
		}
		updateStatusOfAccommodationOfferings(accommodationsOfferingData, DataImportProcessStatus.PROCESSED,
				DataImportProcessStatus.SUCCESS);
		getModelService().saveAll();
		return accommodationOfferings;
	}

	public void markDeleted(final AccommodationOfferingDataModel accommodationOfferingDataModel)
	{
		final AccommodationOfferingModel accommodationOffering;
		final List<ItemModel> accommodationList = new ArrayList<>();

		try
		{
			accommodationOffering = getAccommodationOfferingService()
					.getAccommodationOffering(accommodationOfferingDataModel.getCode());
			accommodationOffering.setDeleted(Boolean.TRUE);
			accommodationOffering.getAccommodations().forEach(accommodation -> {
				final AccommodationModel stagedAccommodationModel = getAccommodationService().getAccommodation(accommodation,
						getCatalogVersionService()
								.getCatalogVersion("bcfProductCatalog", BcfintegrationserviceConstants.CATALOGVERSION_STAGED));
				if (Objects.nonNull(stagedAccommodationModel))
				{
					stagedAccommodationModel.setDeleted(Boolean.TRUE);
					getModelService().save(stagedAccommodationModel);
					accommodationList.add(stagedAccommodationModel);
				}
			});
			getModelService().save(accommodationOffering);
		}
		catch (final ModelNotFoundException e)
		{
			return;
		}
		if (accommodationOfferingDataModel.isPublish())
		{
			synchronizeProductCatalog(accommodationList);
			getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCVACATION_INDEX, false);
		}
	}

	private void updateStatusOfAccommodationOfferings(final List<AccommodationOfferingDataModel> accommodationOfferingDataList,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		accommodationOfferingDataList.stream()
				.filter(accommodationOfferingData -> fromStatus.equals(accommodationOfferingData.getStatus()))
				.forEach(accommodationOfferingData -> accommodationOfferingData.setStatus(toStatus));
		getModelService().saveAll(accommodationOfferingDataList);
	}

	private void updateStatusOfAccommodations(final List<AccommodationDataModel> accommodationDataList,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		accommodationDataList.stream().filter(accommodationData -> fromStatus.equals(accommodationData.getStatus()))
				.forEach(accommodationData -> accommodationData.setStatus(toStatus));
	}

	public AccommodationOfferingModel uploadAccommodationsOfferingData(final String code,
			final List<AccommodationOfferingDataModel> groupedAccommodationOfferingsData, final List<ItemModel> items)
	{
		AccommodationOfferingModel accommodationOffering;
		try
		{
			accommodationOffering = getAccommodationOfferingService().getAccommodationOffering(code);
			getAccommodationOfferingCreateUtility().updateAccommodationOffering(accommodationOffering, code,
					groupedAccommodationOfferingsData.get(0), items);
		}
		catch (final ModelNotFoundException e)
		{
			final AccommodationOfferingDataModel accommodationOfferingData = groupedAccommodationOfferingsData.get(0);
			accommodationOffering = getAccommodationOfferingCreateUtility()
					.createNewAccommodationOffering(accommodationOfferingData, items);
		}

		if (Objects.isNull(accommodationOffering))
		{
			final String message = getPropertySourceFacade().getPropertySourceValue(ACCOMMODATION_OFFERING_NOT_CREATED);
			LOG.error(message);
			groupedAccommodationOfferingsData.forEach(accommodationOfferingData ->
					accommodationOfferingData.setStatus(DataImportProcessStatus.FAILED));
			return null;
		}

		List<AccommodationDataModel> accommodationsData = new ArrayList<>();
		if (Objects.nonNull(groupedAccommodationOfferingsData.get(0).getPk()))
		{
			accommodationsData = getAccommodationDataService()
					.getAccommodationsData(groupedAccommodationOfferingsData.get(0), DataImportProcessStatus.PENDING);
		}
		if (CollectionUtils.isEmpty(accommodationsData))
		{

			final List<AccommodationDataModel> newAccommodationDatas = groupedAccommodationOfferingsData.stream()
					.filter(groupedAccommodationOfferingData -> CollectionUtils
							.isNotEmpty(groupedAccommodationOfferingData.getAccommodationDatas()) && groupedAccommodationOfferingData
							.getAccommodationDatas().stream()
							.anyMatch(accommodationData -> accommodationData.getStatus().equals(DataImportProcessStatus.PENDING)))
					.flatMap(accommodationOfferingDataModel -> accommodationOfferingDataModel.getAccommodationDatas().stream())
					.filter(accommodationDataModel -> accommodationDataModel.getStatus().equals(DataImportProcessStatus.PENDING))
					.collect(
							Collectors.toList());
			if (CollectionUtils.isEmpty(newAccommodationDatas))
			{
				return accommodationOffering;
			}
			accommodationsData = newAccommodationDatas;
		}

		uploadAccommodations(accommodationOffering, accommodationsData, items);

		for (final AccommodationOfferingDataModel accommodationOfferingDataModel : groupedAccommodationOfferingsData)
		{
			if (Objects.isNull(accommodationOfferingDataModel.getAccommodationDatas()))
			{
				accommodationOfferingDataModel.setAccommodationDatas(new ArrayList<>());
			}

			if (CollectionUtils.isEmpty(accommodationOfferingDataModel.getAccommodationDatas()))
			{
				accommodationOfferingDataModel.setAccommodationDatas(accommodationsData);
				continue;
			}
			final List<AccommodationDataModel> filteredAccommodationDataModels = accommodationsData.stream().filter(
					accommodationDataModel -> !accommodationOfferingDataModel.getAccommodationDatas().contains(accommodationDataModel))
					.collect(
							Collectors.toList());
			final List<AccommodationDataModel> newAccommodationDataModels = new ArrayList<>(
					accommodationOfferingDataModel.getAccommodationDatas());
			newAccommodationDataModels.addAll(filteredAccommodationDataModels);
			accommodationOfferingDataModel.setAccommodationDatas(newAccommodationDataModels);
		}

		updateStatusOfAccommodations(accommodationsData, DataImportProcessStatus.PROCESSED, DataImportProcessStatus.SUCCESS);

		return accommodationOffering;
	}

	/**
	 * Upload accommodations.
	 *
	 * @param accommodationOffering
	 * @param accommodationsData    the grouped accommodation offerings data
	 */
	protected void uploadAccommodations(final AccommodationOfferingModel accommodationOffering,
			final List<AccommodationDataModel> accommodationsData, final List<ItemModel> items)
	{
		final Map<String, List<AccommodationDataModel>> accommodationsDataMap = accommodationsData.stream()
				.filter(accommodationData -> StringUtils.isNotEmpty(accommodationData.getCode()))
				.collect(Collectors.groupingBy(AccommodationDataModel::getCode));

		for (final Entry<String, List<AccommodationDataModel>> accommodationDataEntry : accommodationsDataMap.entrySet())
		{
			final String code = accommodationOffering.getCode() + "_" + accommodationDataEntry.getKey();
			final List<AccommodationDataModel> groupedAccommodationsData = accommodationDataEntry.getValue();
			groupedAccommodationsData.forEach(accommodationData -> accommodationData.setStatus(DataImportProcessStatus.PROCESSING));

			getAccommodationCreateUtility().createOrUpdateAccommodation(code, groupedAccommodationsData.get(0), items);

			final List<String> accommodations = Objects.isNull(accommodationOffering.getAccommodations()) ? new ArrayList<>()
					: new ArrayList<>(accommodationOffering.getAccommodations());
			if (!accommodations.contains(code))
			{
				accommodations.add(code);
				accommodationOffering.setAccommodations(accommodations);
			}
		}
		updateStatusOfAccommodations(accommodationsData, DataImportProcessStatus.PROCESSING, DataImportProcessStatus.PROCESSED);
	}

	public void createOrUpdateSaleStatuses(final AccommodationDataModel accommodationData, final boolean toSyncItem)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		AccommodationModel accommodation = null;
		accommodation = (AccommodationModel) getProductService().getProductForCode(catalogVersion,
				accommodationData.getAccommodationOffering().getCode() + "_" + accommodationData.getCode());
		accommodation.setSaleStatuses(
				Sets.newHashSet(createOrGetSaleStatues(accommodationData.getSaleStatuses(), accommodation.getSaleStatuses())));
		getModelService().saveAll(accommodation, accommodationData);

		if (toSyncItem)
		{
			synchronizeProductCatalog(Collections.singletonList(accommodation));
		}
	}

	/**
	 * @return the accommodationCreateUtility
	 */
	protected AccommodationCreateUtility getAccommodationCreateUtility()
	{
		return accommodationCreateUtility;
	}

	/**
	 * @param accommodationCreateUtility the accommodationCreateUtility to set
	 */
	@Required
	public void setAccommodationCreateUtility(final AccommodationCreateUtility accommodationCreateUtility)
	{
		this.accommodationCreateUtility = accommodationCreateUtility;
	}

	/**
	 * @return the accommodationOfferingCreateUtility
	 */
	protected AccommodationOfferingCreateUtility getAccommodationOfferingCreateUtility()
	{
		return accommodationOfferingCreateUtility;
	}

	/**
	 * @param accommodationOfferingCreateUtility the accommodationOfferingCreateUtility to set
	 */
	@Required
	public void setAccommodationOfferingCreateUtility(final AccommodationOfferingCreateUtility accommodationOfferingCreateUtility)
	{
		this.accommodationOfferingCreateUtility = accommodationOfferingCreateUtility;
	}

	protected AccommodationDataService getAccommodationDataService()
	{
		return accommodationDataService;
	}

	@Required
	public void setAccommodationDataService(
			final AccommodationDataService accommodationDataService)
	{
		this.accommodationDataService = accommodationDataService;
	}

	/**
	 * @return the accommodationOfferingService
	 */
	@Override
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	/**
	 * @param accommodationOfferingService the accommodationOfferingService to set
	 */
	@Override
	@Required
	public void setAccommodationOfferingService(final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	/**
	 * @return the productService
	 */
	@Override
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService the productService to set
	 */
	@Override
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @return the roomRatesCreateUtility
	 */
	protected RoomRatesCreateUtility getRoomRatesCreateUtility()
	{
		return roomRatesCreateUtility;
	}

	/**
	 * @param roomRatesCreateUtility the roomRatesCreateUtility to set
	 */
	@Required
	public void setRoomRatesCreateUtility(final RoomRatesCreateUtility roomRatesCreateUtility)
	{
		this.roomRatesCreateUtility = roomRatesCreateUtility;
	}

	/**
	 * @return the bcfUpdateStockLevelsToAccommodationOfferingJob
	 */
	protected BcfUpdateStockLevelsToAccommodationOfferingJob getBcfUpdateStockLevelsToAccommodationOfferingJob()
	{
		return bcfUpdateStockLevelsToAccommodationOfferingJob;
	}

	/**
	 * @param bcfUpdateStockLevelsToAccommodationOfferingJob the bcfUpdateStockLevelsToAccommodationOfferingJob to set
	 */
	@Required
	public void setBcfUpdateStockLevelsToAccommodationOfferingJob(
			final BcfUpdateStockLevelsToAccommodationOfferingJob bcfUpdateStockLevelsToAccommodationOfferingJob)
	{
		this.bcfUpdateStockLevelsToAccommodationOfferingJob = bcfUpdateStockLevelsToAccommodationOfferingJob;
	}

	/**
	 * @return the cronJobService
	 */
	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}

	/**
	 * @param cronJobService the cronJobService to set
	 */
	@Required
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}

	/**
	 * @return the setupSolrIndexerService
	 */
	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	/**
	 * @param setupSolrIndexerService the setupSolrIndexerService to set
	 */
	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	/**
	 * @return the accommodationsOfferingDataService
	 */
	public AccommodationsOfferingDataService getAccommodationsOfferingDataService()
	{
		return accommodationsOfferingDataService;
	}

	/**
	 * @param accommodationsOfferingDataService the accommodationsOfferingDataService to set
	 */
	public void setAccommodationsOfferingDataService(final AccommodationsOfferingDataService accommodationsOfferingDataService)
	{
		this.accommodationsOfferingDataService = accommodationsOfferingDataService;
	}

	/**
	 * @return the ratePlanDataService
	 */
	public RatePlanDataService getRatePlanDataService()
	{
		return ratePlanDataService;
	}

	/**
	 * @param ratePlanDataService the ratePlanDataService to set
	 */
	public void setRatePlanDataService(final RatePlanDataService ratePlanDataService)
	{
		this.ratePlanDataService = ratePlanDataService;
	}

	public BcfAccommodationService getAccommodationService()
	{
		return accommodationService;
	}

	public void setAccommodationService(final BcfAccommodationService accommodationService)
	{
		this.accommodationService = accommodationService;
	}
}
