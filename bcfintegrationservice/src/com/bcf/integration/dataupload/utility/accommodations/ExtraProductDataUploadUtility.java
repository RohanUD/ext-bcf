/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.integration.dataupload.service.accommodations.AccommodationsOfferingDataService;
import com.bcf.integration.dataupload.service.accommodations.ExtraProductDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ExtraProductDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(ExtraProductDataUploadUtility.class);

	private static final String EXTRA_PRODUCT_DATA_SUCCESS = "Extra Product data created successfully";
	private static final String CA_TAX_GROUP = "ca-tax-group";

	private ExtraProductDataService extraProductDataService;
	private ExtraProductDataValidator extraProductDataValidator;
	private AccommodationOfferingService accommodationOfferingService;
	private AccommodationsOfferingDataService accommodationsOfferingDataService;
	private ProductTaxGroupMappingService productTaxGroupMappingService;


	public void uploadExtraProductDatas()
	{

		final List<ExtraProductDataModel> pendingExtraProductData = extraProductDataService
				.getExtraProductData(DataImportProcessStatus.PENDING);

		/* checking for invalid items */
		final List<ExtraProductDataModel> inValidExtraProductData = pendingExtraProductData.stream()
				.filter(extraProductData -> !extraProductDataValidator.validateExtraProductData(extraProductData).isEmpty())
				.collect(Collectors.toList());
		inValidExtraProductData.forEach(extraProductData -> extraProductData.setStatus(DataImportProcessStatus.FAILED));

		getModelService().saveAll(inValidExtraProductData);

		if(CollectionUtils.isNotEmpty(inValidExtraProductData)){

			pendingExtraProductData.removeAll(inValidExtraProductData);
		}

		/* processing for valid items */
		pendingExtraProductData.forEach(extraProductData -> extraProductData.setStatus(DataImportProcessStatus.PROCESSING));

		getModelService().saveAll(pendingExtraProductData);
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		final List<ItemModel> items = new ArrayList<>();
		final List<ItemModel> itemsToSave = new ArrayList<>();

		itemsToSave.addAll(pendingExtraProductData);
		uploadExtraProductData(pendingExtraProductData,catalogVersion,items,itemsToSave,true);
		getModelService().saveAll(itemsToSave);
		if (CollectionUtils.isNotEmpty(items))
		{
			synchronizeProductCatalog(items);
		}
		pendingExtraProductData.forEach(extraProductData -> extraProductData.setStatus(DataImportProcessStatus.SUCCESS));
		getModelService().saveAll(pendingExtraProductData);
	}


	public void markUnapproved(final ExtraProductDataModel extraProductDataModel, final CatalogVersionModel catalogVersion)
	{
		final ExtraProductModel extraProductModel;
		try
		{
			extraProductModel = (ExtraProductModel) getProductService().getProductForCode(catalogVersion, extraProductDataModel.getCode());
			extraProductModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
			getModelService().save(extraProductModel);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			//do nothing
		}
	}

	public boolean uploadExtraProductData(
			final List<ExtraProductDataModel> pendingExtraProductData, final CatalogVersionModel catalogVersion,final List<ItemModel> items, final List<ItemModel> itemsToSave ,
			final boolean publish)
	{

		for ( final ExtraProductDataModel extraProductDataModel : pendingExtraProductData)
		{
			 uploadData( extraProductDataModel,catalogVersion,items, itemsToSave, publish);
		}

		return Boolean.TRUE;

	}

	protected ExtraProductModel getExtraModelProduct(final ExtraProductDataModel extraProductData,
			final CatalogVersionModel catalogVersion)
	{
		final String extraProductCode = extraProductData.getCode();
		final ExtraProductModel extraProduct;
		try
		{
			extraProduct = (ExtraProductModel)getProductService().getProductForCode(catalogVersion, extraProductCode);
			return extraProduct;
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			final ExtraProductModel extraProductModel = getModelService().create(ExtraProductModel.class);
			extraProductModel.setCode(extraProductCode);
			return extraProductModel;
		}
	}

	private void updateExtraProduct(final ExtraProductModel extraProduct,
			final ExtraProductDataModel extraProductData,
			final CatalogVersionModel catalogVersion)
	{
		getLocaleList().forEach(locale -> extraProduct.setName(extraProductData.getName(locale), locale));
		getLocaleList().forEach(locale -> extraProduct.setDescription(extraProductData.getDescription(locale), locale));
		extraProduct.setCatalogVersion(catalogVersion);
		extraProduct.setApprovalStatus(ArticleApprovalStatus.APPROVED);

		/* TaxGroup */
		ProductTaxGroup taxGroup = getProductTaxGroupMappingService().getProductTaxGroupMapping(
				extraProductData.getApplyDMF(), extraProductData.getApplyMRDT(),
				extraProductData.getApplyGST(), extraProductData.getApplyPST());
		if (Objects.isNull(taxGroup))
		{
			taxGroup = ProductTaxGroup.valueOf(CA_TAX_GROUP);
		}
		extraProduct.setEurope1PriceFactory_PTG(taxGroup);
	}

	private void uploadData(

			final ExtraProductDataModel extraProductData, final CatalogVersionModel catalogVersion,
		final List<ItemModel> items, final List<ItemModel> itemsToSave, final boolean publish)
	{

		final ExtraProductModel extraProductModel= getExtraModelProduct(extraProductData, catalogVersion);
		updateExtraProduct(extraProductModel, extraProductData, catalogVersion);
		itemsToSave.add(extraProductModel);
		if(StringUtils.isNotEmpty(extraProductData.getAccommodationOfferingCode()))
		{
			final AccommodationOfferingDataModel accommodationOfferingData = findValidAccommodationOfferingData(
					extraProductData.getAccommodationOfferingCode());

			if (accommodationOfferingData != null)
			{
				final List<ExtraProductDataModel> extraProductDataList = Objects
						.isNull(accommodationOfferingData.getExtraProductDatas()) ?
						Collections.emptyList()
						:
						new ArrayList<>(accommodationOfferingData.getExtraProductDatas());
				final List<ExtraProductDataModel> extraProductMatchDataList = Optional
						.ofNullable(accommodationOfferingData.getExtraProductDatas())
						.orElseGet(Collections::emptyList).stream()
						.filter(extraProduct -> StringUtils.equals(extraProduct.getCode(), extraProductData.getCode()))
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(extraProductMatchDataList))
				{
					extraProductDataList.add(extraProductData);
					accommodationOfferingData.setExtraProductDatas(extraProductDataList);
					itemsToSave.add(accommodationOfferingData);

				}
			}


			final AccommodationOfferingModel accommodationOffering = findValidAccommodationOffering(
					extraProductData.getAccommodationOfferingCode());

			addItemsToSave(extraProductData, itemsToSave, accommodationOffering);
		}

		if(publish){
			items.add(extraProductModel);
		}


	}

	private void addItemsToSave(final ExtraProductDataModel extraProductData, final List<ItemModel> itemsToSave,
			final AccommodationOfferingModel accommodationOffering)
	{
		if (accommodationOffering != null)
		{
			final List<String> extraProductList = Objects.isNull(accommodationOffering.getExtras()) ? new ArrayList<>()
					: new ArrayList<>(accommodationOffering.getExtras());

			final List<String> extraProductMatchList = Optional.ofNullable(accommodationOffering.getExtras())
					.orElseGet(Collections::emptyList).stream()
					.filter(extraProduct -> extraProduct.equals(extraProductData.getCode()))
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(extraProductMatchList))
			{
				extraProductList.add(extraProductData.getCode());
			}
			accommodationOffering.setExtras(extraProductList);
			itemsToSave.add(accommodationOffering);

		}
	}

	private AccommodationOfferingDataModel findValidAccommodationOfferingData(final String accommodationOfferingCode)
	{
		LOG.debug("accommodationOfferingCode : " + accommodationOfferingCode);
		final AccommodationOfferingDataModel accommodationOfferingData;
		try
		{
			accommodationOfferingData = getAccommodationsOfferingDataService()
					.getAccommodationsOfferingData(accommodationOfferingCode);
			LOG.debug("AccommodationOfferingData found for accommodationOfferingCode : " + accommodationOfferingCode);
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error("AccommodationOfferingData not found for accommodationOfferingCode : " + accommodationOfferingCode);
			return null;
		}
		return accommodationOfferingData;
	}

	private AccommodationOfferingModel findValidAccommodationOffering(
			final String accommodationOfferingCode)
	{
		LOG.debug("accommodationOfferingCode : " + accommodationOfferingCode);
		final AccommodationOfferingModel accommodationOffering;
		try
		{
			accommodationOffering = getAccommodationOfferingService()
					.getAccommodationOffering(accommodationOfferingCode);
			LOG.debug("AccommodationOffering found for accommodationOfferingCode : " + accommodationOfferingCode);
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error("AccommodationOffering not found for accommodationOfferingCode : " + accommodationOfferingCode);
			return null;
		}
		return accommodationOffering;
	}


	public void handleSuccess(final ExtraProductDataModel extraProductData, final List<ItemModel> items)
	{
		if (CollectionUtils.isNotEmpty(items))
		{
			final Map<String,List<ItemModel>> itemMap=items.stream().collect(Collectors.groupingBy(item->item.getItemtype()));
			for(final Map.Entry<String,List<ItemModel>> itemMapEntry:itemMap.entrySet()){
				synchronizeProductCatalog(itemMapEntry.getValue());
			}

		}

		extraProductData.setStatus(DataImportProcessStatus.SUCCESS);
		extraProductData.setStatusDescription(EXTRA_PRODUCT_DATA_SUCCESS);

	}

	protected ExtraProductDataService getExtraProductDataService()
	{
		return extraProductDataService;
	}

	@Required
	public void setExtraProductDataService(
			final ExtraProductDataService extraProductDataService)
	{
		this.extraProductDataService = extraProductDataService;
	}

	protected ExtraProductDataValidator getExtraProductDataValidator()
	{
		return extraProductDataValidator;
	}

	@Required
	public void setExtraProductDataValidator(
			final ExtraProductDataValidator extraProductDataValidator)
	{
		this.extraProductDataValidator = extraProductDataValidator;
	}



	@Override
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Override
	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected AccommodationsOfferingDataService getAccommodationsOfferingDataService()
	{
		return accommodationsOfferingDataService;
	}

	@Required
	public void setAccommodationsOfferingDataService(
			final AccommodationsOfferingDataService accommodationsOfferingDataService)
	{
		this.accommodationsOfferingDataService = accommodationsOfferingDataService;
	}

	public ProductTaxGroupMappingService getProductTaxGroupMappingService()
	{
		return productTaxGroupMappingService;
	}

	public void setProductTaxGroupMappingService(
			final ProductTaxGroupMappingService productTaxGroupMappingService)
	{
		this.productTaxGroupMappingService = productTaxGroupMappingService;
	}
}
