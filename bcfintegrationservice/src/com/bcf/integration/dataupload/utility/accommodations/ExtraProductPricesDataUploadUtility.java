/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.product.ExtraProductModel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.integration.dataupload.service.accommodations.ExtraProductDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.accommodation.ExtraProductPriceDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ExtraProductPricesDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger log = Logger.getLogger(ExtraProductPricesDataUploadUtility.class);

	private static final String ACTIVITY_DATA_SUCCESS = "Activity Price data created successfully";

	private ExtraProductDataService extraProductDataService;
	private PriceRowService priceRowService;
	private UnitService unitService;
	private ExtraProductPriceDataValidator extraProductPriceDataValidator;

	public void uploadExtraProductPricesData()
	{
		final List<ItemModel> items = new ArrayList<>();
		final List<ExtraProductPriceDataModel> pendingExtraProductPriceData =extraProductDataService
				.getExtraProductPricesData(DataImportProcessStatus.PENDING);

		/* checking for invalid items */
		final List<ExtraProductPriceDataModel> invalidExtraProductPriceDatas = pendingExtraProductPriceData.stream()
				.filter(extraProductPriceData -> !extraProductPriceDataValidator.validateExtraProductPriceData(extraProductPriceData).isEmpty())
				.collect(Collectors.toList());

		invalidExtraProductPriceDatas.forEach(invalidExtraProductPriceData -> invalidExtraProductPriceData.setStatus(DataImportProcessStatus.FAILED));
		getModelService().saveAll(invalidExtraProductPriceDatas);

		if(CollectionUtils.isNotEmpty(invalidExtraProductPriceDatas)){
			pendingExtraProductPriceData.removeAll(invalidExtraProductPriceDatas);
		}


		pendingExtraProductPriceData.forEach(extraProductPriceData -> extraProductPriceData.setStatus(DataImportProcessStatus.PROCESSING));
		getModelService().saveAll(pendingExtraProductPriceData);

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		List<ExtraProductPriceDataModel> validExtraProductPriceDatas =new ArrayList<>();
		for(ExtraProductPriceDataModel  extraProductPriceDataModel:pendingExtraProductPriceData){
			if(uploadExtraProductPrices(extraProductPriceDataModel,catalogVersion, items)){
				validExtraProductPriceDatas.add(extraProductPriceDataModel);
			}
		}

		getModelService().saveAll();
		if (CollectionUtils.isNotEmpty(items))
		{
			synchronizeProductCatalog(items);
		}

		validExtraProductPriceDatas.forEach(this::handleSuccess);
		getModelService().saveAll(pendingExtraProductPriceData);
	}


	public Boolean uploadExtraProductPrices(final ExtraProductPriceDataModel extraProductPriceData, final CatalogVersionModel catalogVersion,
			final List<ItemModel> items)
	{

		final String extraProductCode = extraProductPriceData.getExtraProductData().getCode();
		final ExtraProductModel extraProductModel;
		try
		{
			extraProductModel = (ExtraProductModel) getProductService().getProductForCode(catalogVersion, extraProductCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			final String errorMessage = "ExtraProductModel not found for extraProductCode: " + extraProductCode;
			log.error(errorMessage);
			extraProductPriceData.setStatusDescription(errorMessage);
			return Boolean.FALSE;
		}

		updatePriceRow(extraProductModel, extraProductPriceData.getStartDate(),
				extraProductPriceData.getEndDate(), extraProductPriceData.getCostPrice(), extraProductPriceData.getSellPrice(),
				items);

		return Boolean.TRUE;
	}

	public void handleSuccess(final ExtraProductPriceDataModel extraProductPriceDataModel)
	{
		extraProductPriceDataModel.setStatus(DataImportProcessStatus.SUCCESS);
		extraProductPriceDataModel.setStatusDescription(ACTIVITY_DATA_SUCCESS);
	}

	public void truncateDates(final ExtraProductPriceDataModel extraProductPriceDataModel)
	{
		extraProductPriceDataModel.setStartDate(DateUtils.truncate(extraProductPriceDataModel.getStartDate(), Calendar.DATE));
		extraProductPriceDataModel.setEndDate(DateUtils.truncate(extraProductPriceDataModel.getEndDate(), Calendar.DATE));
	}

	/**
	 * Update price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @param costPrice     the cost price
	 * @param sellPrice     the base price
	 */
	protected void updatePriceRow(final ExtraProductModel product,  final Date startDate,
			final Date endDate, final Double costPrice ,final Double sellPrice,final List<ItemModel> items)
	{
		PriceRowModel price;

		price = getPriceRowService().getPriceRow(product, null,startDate, endDate);
		if (Objects.isNull(price) || (price.getPrice()!=null && price.getPrice()!=sellPrice && price.getCostPrice()!=null && price.getCostPrice()!=costPrice))
		{
			price = createPriceRow(product, startDate, endDate);
		}
		price.setCostPrice(costPrice);
		price.setPrice(sellPrice);
		items.add(price);
	}

	/**
	 * Creates the price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @return the price row model
	 */
	protected PriceRowModel createPriceRow(final ExtraProductModel product, final Date startDate,
			final Date endDate)
	{
		final PriceRowModel price = getModelService().create(PriceRowModel.class);

		price.setProduct(product);
		price.setCurrency(getCommonI18NService().getBaseCurrency());
		price.setMinqtd(1l);
		price.setNet(true);
		price.setUnit(getUnitService().getUnitForCode(UNIT));
		price.setCatalogVersion(product.getCatalogVersion());
		price.setStartTime(startDate);
		price.setEndTime(endDate);
		price.setProductMatchQualifier(Long.valueOf(product.getPk().getLongValue()));

		return price;
	}

	protected PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	@Required
	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}

	protected UnitService getUnitService()
	{
		return unitService;
	}

	@Required
	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}

	public ExtraProductDataService getExtraProductDataService()
	{
		return extraProductDataService;
	}

	public void setExtraProductDataService(
			final ExtraProductDataService extraProductDataService)
	{
		this.extraProductDataService = extraProductDataService;
	}

	public ExtraProductPriceDataValidator getExtraProductPriceDataValidator()
	{
		return extraProductPriceDataValidator;
	}

	public void setExtraProductPriceDataValidator(
			final ExtraProductPriceDataValidator extraProductPriceDataValidator)
	{
		this.extraProductPriceDataValidator = extraProductPriceDataValidator;
	}
}
