/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.PropertyFacilityType;
import de.hybris.platform.travelservices.model.accommodation.AccommodationProviderModel;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.facility.PropertyFacilityModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.PassengerTypeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.BcfCommentDataModel;
import com.bcf.core.accommodation.service.AccommodationProviderService;
import com.bcf.core.accommodation.service.BcfCommentService;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.accommodation.service.VendorService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.accommodation.BcfCommentModel;
import com.bcf.core.services.BcfPropertyFacilityService;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;


public class AccommodationOfferingCreateUtility extends AbstractDataUploadUtility
{
	private static final Logger log = Logger.getLogger(AccommodationOfferingCreateUtility.class);
	private static final String POS = "pos";
	private static final String VENDOR = "bcferries";

	private AccommodationProviderService accommodationProviderService;
	private BaseStoreService baseStoreService;
	private VendorService vendorService;
	private BcfTravelLocationService bcfTravelLocationService;
	private PassengerTypeService passengerTypeService;
	private TaxService taxService;
	private BcfCommentService bcfCommentService;
	private BcfPropertyFacilityService bcfPropertyFacilityService;

	/**
	 * Creates the new accommodation offering.
	 *
	 * @param accommodationOfferingData the accommodation offering data
	 * @return the accommodation offering model
	 */

	public AccommodationOfferingModel createNewAccommodationOffering(
			final AccommodationOfferingDataModel accommodationOfferingData, final List<ItemModel> items)
	{
		if (!validateAccommodationOfferingData(accommodationOfferingData))
		{
			getModelService().remove(accommodationOfferingData);
			return null;
		}

		final AccommodationOfferingModel accommodationOffering = getModelService().create(AccommodationOfferingModel.class);
		accommodationOffering.setCode(accommodationOfferingData.getCode());
		getLocaleList().forEach(locale -> accommodationOffering.setName(accommodationOfferingData.getPropertyName(locale), locale));
		final LocationModel location = createNewLocation(accommodationOfferingData);
		accommodationOffering.setLocation(location);
		final String providerName = accommodationOfferingData.getProvider();
		AccommodationProviderModel provider = getAccommodationProviderService().getAccommodationProvider(providerName);
		if (Objects.isNull(provider))
		{
			provider = createNewAccommodationProvider(providerName);
		}
		if (StringUtils.isNotEmpty(accommodationOfferingData.getProviderEmail()))
		{
			provider.setEmails(Arrays.asList(StringUtils.split(accommodationOfferingData.getProviderEmail(), ",")));
		}
		accommodationOffering.setProvider(provider);
		final VendorModel vendor = getVendorService().getVendor(VENDOR);
		accommodationOffering.setVendor(vendor);
		accommodationOffering.setDefault(true);
		accommodationOffering.setIsDealOfTheDay(accommodationOfferingData.getIsDealOfTheDay());
		if (BooleanUtils.isTrue(accommodationOfferingData.getIsDealOfTheDay()))
		{
			accommodationOffering.setDealOfTheDayDateRange(createNewDateRange(accommodationOfferingData.getDealOfTheDayStartDate(),
					accommodationOfferingData.getDealOfTheDayEndDate()));
		}
		if (Objects.nonNull(accommodationOfferingData.getExtraProductDatas()))
		{
			accommodationOffering.setExtras(accommodationOfferingData.getExtraProductDatas().stream()
					.map(extraProductData -> extraProductData.getCode()).collect(Collectors.toList()));
		}
		accommodationOffering.setReviewLocationId(accommodationOfferingData.getReviewLocationId());
		accommodationOffering.setPropertyType(accommodationOfferingData.getPropertyType());
		accommodationOffering.setPropertyCategoryTypes(accommodationOfferingData.getPropertyCategoryTypes());
		accommodationOffering.setPropertyFacility(getPropertyFacilities(accommodationOfferingData.getPropertyFacilityTypes()));
		createSpecialAges(accommodationOfferingData, accommodationOffering);
		getLocaleList().forEach(locale -> accommodationOffering
				.setPropertyInformation(accommodationOfferingData.getPropertyInformation(locale), locale));
		getLocaleList().forEach(
				locale -> accommodationOffering.setDescription(accommodationOfferingData.getPropertyDescription(locale), locale));
		getLocaleList().forEach(locale -> accommodationOffering
				.setTermsAndConditions(accommodationOfferingData.getTermsAndConditions(locale), locale));
		accommodationOffering.setBcfComments(createOrGetBcfComments(accommodationOfferingData.getBcfComments()));
		accommodationOffering.setSaleStatuses(Sets.newHashSet(
				createOrGetSaleStatues(accommodationOfferingData.getSaleStatuses(), accommodationOffering.getSaleStatuses())));
		items.add(accommodationOffering);
		return accommodationOffering;
	}

	private Collection<PropertyFacilityModel> getPropertyFacilities(final Collection<PropertyFacilityType> propertyFacilityTypes)
	{
		final Collection<PropertyFacilityModel> propertyFacilityList = new ArrayList<>();
		for (final PropertyFacilityType propertyFacilityType : propertyFacilityTypes)
		{
			final PropertyFacilityModel propertyFacility = getBcfPropertyFacilityService()
					.getPropertyFacilityFromType(propertyFacilityType);
			if (Objects.isNull(propertyFacility))
			{
				final PropertyFacilityModel newPropertyFacility = getModelService().create(PropertyFacilityModel.class);
				newPropertyFacility.setCode(propertyFacilityType.getCode());
				newPropertyFacility.setType(propertyFacilityType);
				propertyFacilityList.add(newPropertyFacility);
				continue;
			}
			propertyFacilityList.add(propertyFacility);
		}
		return propertyFacilityList;
	}

	/**
	 * Update accommodation offering model.
	 *
	 * @param accommodationOfferingModel the accommodation offering model
	 * @param accommodationOfferingData  the accommodation offering data
	 */

	public void updateAccommodationOffering(final AccommodationOfferingModel accommodationOfferingModel,
			final String accommodationOfferingCode, final AccommodationOfferingDataModel accommodationOfferingData,
			final List<ItemModel> items)
	{
		getLocaleList()
				.forEach(locale -> accommodationOfferingModel.setName(accommodationOfferingData.getPropertyName(locale), locale));
		Optional<PointOfServiceModel> firstPOS = StreamUtil.safeStream(accommodationOfferingModel.getLocation().getPointOfService())
				.findFirst();
		if (firstPOS.isPresent())
		{
			firstPOS.get().getAddress()
					.setPhone1(accommodationOfferingData.getPhone());
		}
		if (Objects.nonNull(accommodationOfferingData.getExtraProductDatas()))
		{
			accommodationOfferingModel.setExtras(accommodationOfferingData.getExtraProductDatas().stream()
					.map(extraProductData -> extraProductData.getCode()).collect(Collectors.toList()));
		}
		final String providerName = accommodationOfferingData.getProvider();
		AccommodationProviderModel provider = getAccommodationProviderService().getAccommodationProvider(providerName);
		if (Objects.isNull(provider))
		{
			provider = createNewAccommodationProvider(providerName);
		}
		if (StringUtils.isNotEmpty(accommodationOfferingData.getProviderEmail()))
		{
			provider.setEmails(Arrays.asList(StringUtils.split(accommodationOfferingData.getProviderEmail(), ",")));
		}
		accommodationOfferingModel.setProvider(provider);
		accommodationOfferingModel.setReviewLocationId(accommodationOfferingData.getReviewLocationId());
		accommodationOfferingModel.setPropertyType(accommodationOfferingData.getPropertyType());
		accommodationOfferingModel.setPropertyCategoryTypes(accommodationOfferingData.getPropertyCategoryTypes());
		accommodationOfferingModel.setPropertyFacility(getPropertyFacilities(accommodationOfferingData.getPropertyFacilityTypes()));
		accommodationOfferingModel.setIsDealOfTheDay(accommodationOfferingData.getIsDealOfTheDay());
		if (BooleanUtils.isTrue(accommodationOfferingData.getIsDealOfTheDay()))
		{
			accommodationOfferingModel
					.setDealOfTheDayDateRange(createNewDateRange(accommodationOfferingData.getDealOfTheDayStartDate(),
							accommodationOfferingData.getDealOfTheDayEndDate()));
		}
		accommodationOfferingModel.setDeleted(Boolean.FALSE);
		updateSpecializedPassengerType(accommodationOfferingData, accommodationOfferingModel);
		getLocaleList().forEach(locale -> accommodationOfferingModel
				.setPropertyInformation(accommodationOfferingData.getPropertyInformation(locale), locale));
		getLocaleList().forEach(locale -> accommodationOfferingModel
				.setDescription(accommodationOfferingData.getPropertyDescription(locale), locale));
		getLocaleList().forEach(locale -> accommodationOfferingModel
				.setTermsAndConditions(accommodationOfferingData.getTermsAndConditions(locale), locale));
		accommodationOfferingModel.setBcfComments(createOrGetBcfComments(accommodationOfferingData.getBcfComments()));
		accommodationOfferingModel
				.setSaleStatuses(Sets.newHashSet(createOrGetSaleStatues(accommodationOfferingData.getSaleStatuses(),
						accommodationOfferingModel.getSaleStatuses())));
		final String locationCode = accommodationOfferingCode;
		final LocationModel location = getBcfTravelLocationService().getLocation(locationCode);
		if (Objects.nonNull(location))
		{
			updateLocation(location, accommodationOfferingCode, accommodationOfferingData);
		}
		else
		{
			createNewLocation(accommodationOfferingData);
		}
	}

	public void createOrUpdateSaleStatuses(final AccommodationOfferingDataModel accommodationOfferingData)
	{
		final AccommodationOfferingModel accommodationOfferingModel;
		accommodationOfferingModel = getAccommodationOfferingService()
				.getAccommodationOffering(accommodationOfferingData.getCode());
		accommodationOfferingModel
				.setSaleStatuses(Sets.newHashSet(createOrGetSaleStatues(accommodationOfferingData.getSaleStatuses(),
						accommodationOfferingModel.getSaleStatuses())));

		getModelService().saveAll(accommodationOfferingModel, accommodationOfferingData);
	}

	/**
	 * Creates the or get bcf comments.
	 *
	 * @param bcfCommentDatas the bcf comments
	 * @return the collection
	 */
	protected List<BcfCommentModel> createOrGetBcfComments(final Collection<BcfCommentDataModel> bcfCommentDatas)
	{
		if (CollectionUtils.isEmpty(bcfCommentDatas))
		{
			return Collections.emptyList();
		}

		final List<BcfCommentModel> bcfComments = new ArrayList<>();
		for (final BcfCommentDataModel bcfCommentData : bcfCommentDatas)
		{
			BcfCommentModel bcfComment = getBcfCommentService().getBcfComment(bcfCommentData.getCode());
			if (Objects.isNull(bcfComment))
			{
				bcfComment = getModelService().create(BcfCommentModel.class);
				bcfComment.setCode(bcfCommentData.getCode());
			}
			bcfComment.setStartDate(Objects.isNull(bcfCommentData.getStartDate()) ?
					bcfCommentData.getStartDate() :
					DateUtils.truncate(bcfCommentData.getStartDate(), Calendar.DATE));
			bcfComment.setEndDate(Objects.isNull(bcfCommentData.getEndDate()) ?
					bcfCommentData.getEndDate() :
					DateUtils.truncate(bcfCommentData.getEndDate(), Calendar.DATE));
			for (final Locale locale : getLocaleList())
			{
				bcfComment.setText(bcfCommentData.getText(locale), locale);
			}
			bcfComments.add(bcfComment);
		}
		return bcfComments;
	}

	/**
	 * Creates a new date range.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @return the date range model
	 */
	protected DateRangeModel createNewDateRange(final Date startDate, final Date endDate)
	{
		final DateRangeModel dateRange = getModelService().create(DateRangeModel.class);
		if (Objects.nonNull(startDate))
		{
			final Date truncatedStartDate = DateUtils.truncate(startDate, Calendar.DATE);
			dateRange.setStartingDate(truncatedStartDate);
		}
		if (Objects.nonNull(endDate))
		{
			final Date truncatedEndDate = DateUtils.truncate(endDate, Calendar.DATE);
			dateRange.setEndingDate(truncatedEndDate);
		}
		return dateRange;
	}


	private void createSpecialAges(final AccommodationOfferingDataModel accommodationOfferingData,
			final AccommodationOfferingModel accommodationOffering)
	{
		createAdultSpecialAges(accommodationOfferingData, accommodationOffering);
		createChildSpecialAges(accommodationOfferingData, accommodationOffering);
	}

	private void createChildSpecialAges(final AccommodationOfferingDataModel accommodationOfferingData,
			final AccommodationOfferingModel accommodationOfferingModel)
	{
		final PassengerTypeModel passengerTypeChild = getPassengerTypeService()
				.getPassengerType(BcfintegrationserviceConstants.PASSENGER_TYPE_CHILD);

		if ((accommodationOfferingData.getChildMinAge() != null
				&& passengerTypeChild.getMinAge() != accommodationOfferingData.getChildMinAge())
				|| (accommodationOfferingData.getChildMaxAge() != null
				&& passengerTypeChild.getMaxAge() != accommodationOfferingData.getChildMaxAge()))
		{
			createSpecializedPassengerType(accommodationOfferingData.getChildMinAge(), accommodationOfferingData.getChildMaxAge(),
					accommodationOfferingModel, BcfintegrationserviceConstants.PASSENGER_TYPE_CHILD);
		}
	}

	private void createAdultSpecialAges(final AccommodationOfferingDataModel accommodationOfferingData,
			final AccommodationOfferingModel accommodationOfferingModel)
	{
		final PassengerTypeModel passengerTypeAdult = getPassengerTypeService()
				.getPassengerType(BcfintegrationserviceConstants.PASSENGER_TYPE_ADULT);

		if ((accommodationOfferingData.getAdultMinAge() != null
				&& passengerTypeAdult.getMinAge() != accommodationOfferingData.getAdultMinAge())
				|| (accommodationOfferingData.getAdultMaxAge() != null
				&& passengerTypeAdult.getMaxAge() != accommodationOfferingData.getAdultMaxAge()))
		{
			createSpecializedPassengerType(accommodationOfferingData.getAdultMinAge(), accommodationOfferingData.getAdultMaxAge(),
					accommodationOfferingModel, BcfintegrationserviceConstants.PASSENGER_TYPE_ADULT);
		}
	}

	private void createSpecializedPassengerType(final Integer minAge, final Integer maxAge,
			final AccommodationOfferingModel accommodationOffering, final String passengerTypeCode)
	{
		final SpecializedPassengerTypeModel specializedPassengerTypeModel = getModelService()
				.create(SpecializedPassengerTypeModel.class);
		specializedPassengerTypeModel
				.setCode(accommodationOffering.getCode() + BcfintegrationserviceConstants.UNDERSCORE + passengerTypeCode);
		specializedPassengerTypeModel.setBasePassengerType(getPassengerTypeService().getPassengerType(passengerTypeCode));
		specializedPassengerTypeModel.setMinAge(minAge);
		specializedPassengerTypeModel.setMaxAge(maxAge);
		final Collection<SpecializedPassengerTypeModel> newSpecializedPassengerTypeModelList = new ArrayList<>();
		if (Objects.nonNull(accommodationOffering.getSpecializedPassengerTypes()))
		{
			newSpecializedPassengerTypeModelList.addAll(accommodationOffering.getSpecializedPassengerTypes());
		}
		newSpecializedPassengerTypeModelList.add(specializedPassengerTypeModel);
		accommodationOffering.setSpecializedPassengerTypes(newSpecializedPassengerTypeModelList);
	}

	private void updateSpecializedPassengerType(final AccommodationOfferingDataModel accommodationOfferingData,
			final AccommodationOfferingModel accommodationOfferingModel)
	{
		if (Objects.isNull(accommodationOfferingModel.getSpecializedPassengerTypes())
				|| accommodationOfferingModel.getSpecializedPassengerTypes().isEmpty())
		{
			createSpecialAges(accommodationOfferingData, accommodationOfferingModel);
			return;
		}
		final List<SpecializedPassengerTypeModel> adultModelList = accommodationOfferingModel
				.getSpecializedPassengerTypes().stream().filter(specializedPassengerType -> specializedPassengerType
						.getBasePassengerType().getCode().equals(BcfintegrationserviceConstants.PASSENGER_TYPE_ADULT))
				.collect(Collectors.toList());
		if (Objects.isNull(adultModelList) || adultModelList.isEmpty())
		{
			createAdultSpecialAges(accommodationOfferingData, accommodationOfferingModel);
		}
		else
		{
			updateSpecialAges(accommodationOfferingData.getAdultMinAge(), accommodationOfferingData.getAdultMaxAge(),
					adultModelList.get(0));
		}

		final List<SpecializedPassengerTypeModel> childModelList = accommodationOfferingModel
				.getSpecializedPassengerTypes().stream().filter(specializedPassengerType -> specializedPassengerType
						.getBasePassengerType().getCode().equals(BcfintegrationserviceConstants.PASSENGER_TYPE_CHILD))
				.collect(Collectors.toList());
		if (Objects.isNull(childModelList) || childModelList.isEmpty())
		{
			createChildSpecialAges(accommodationOfferingData, accommodationOfferingModel);
		}
		else
		{
			updateSpecialAges(accommodationOfferingData.getChildMinAge(), accommodationOfferingData.getChildMaxAge(),
					childModelList.get(0));
		}
	}

	private void updateSpecialAges(final Integer minAge, final Integer maxAge,
			final SpecializedPassengerTypeModel specializedPassengerTypeModel)
	{
		specializedPassengerTypeModel.setMinAge(minAge);
		specializedPassengerTypeModel.setMaxAge(maxAge);
	}

	/**
	 * Creates the new accommodation provider.
	 *
	 * @param providerName the provider name
	 * @return the accommodation provider model
	 */
	protected AccommodationProviderModel createNewAccommodationProvider(final String providerName)
	{
		final AccommodationProviderModel accommodationProvider = getModelService().create(AccommodationProviderModel.class);
		accommodationProvider.setCode(providerName);
		accommodationProvider.setName(providerName);
		return accommodationProvider;
	}

	/**
	 * Creates the new location.
	 *
	 * @param accommodationOfferingData the accommodation offering data
	 * @return the location model
	 */
	protected LocationModel createNewLocation(final AccommodationOfferingDataModel accommodationOfferingData)
	{
		final LocationModel location = getModelService().create(LocationModel.class);
		final String locationCode = accommodationOfferingData.getCode();

		location.setCode(locationCode);
		location.setLocationType(LocationType.PROPERTY);
		location.setSuperlocations(Collections.singletonList(accommodationOfferingData.getTown().getOriginalLocation()));

		String posName = locationCode + UNDERSCORE + POS;
		PointOfServiceModel pointOfService = getBcfTravelLocationService()
				.getOrCreatePointOfServiceForName(posName);

		uploadPointOfService(locationCode, pointOfService, accommodationOfferingData);

		final AddressModel address = getModelService().create(AddressModel.class);
		uploadAddress(address, accommodationOfferingData);
		address.setOwner(pointOfService);
		pointOfService.setAddress(address);
		location.setPointOfService(Collections.singletonList(pointOfService));
		if (Objects.nonNull(accommodationOfferingData.getDmfRate()))
		{
			createOrUpdateDMFTaxRow(location, accommodationOfferingData.getDmfRate());
		}

		return location;
	}

	/**
	 * Updates the location.
	 *
	 * @param accommodationOfferingData the accommodation offering data
	 * @return the location model
	 */
	protected LocationModel updateLocation(final LocationModel location, final String accommodationOfferingCode,
			final AccommodationOfferingDataModel accommodationOfferingData)
	{
		location.setLocationType(LocationType.PROPERTY);
		location.setSuperlocations(Collections.singletonList(accommodationOfferingData.getTown().getOriginalLocation()));
		Optional<PointOfServiceModel> firstPOS = StreamUtil.safeStream(location.getPointOfService()).findFirst();

		PointOfServiceModel pointOfService;
		if (firstPOS.isPresent())
		{
			pointOfService = firstPOS.get();
		}
		else
		{
			String posName = accommodationOfferingData.getCode() + UNDERSCORE + POS;
			pointOfService = getBcfTravelLocationService()
					.getOrCreatePointOfServiceForName(posName);
		}
		uploadPointOfService(location.getCode(), pointOfService, accommodationOfferingData);
		AddressModel address = pointOfService.getAddress();
		if (Objects.isNull(address))
		{
			address = getModelService().create(AddressModel.class);
			address.setOwner(pointOfService);
		}
		uploadAddress(address, accommodationOfferingData);
		location.setPointOfService(Collections.singletonList(pointOfService));
		if (Objects.nonNull(accommodationOfferingData.getDmfRate()))
		{
			createOrUpdateDMFTaxRow(location, accommodationOfferingData.getDmfRate());
		}

		return location;
	}

	protected TaxRowModel createOrUpdateDMFTaxRow(final LocationModel location, final Double dmfRate)
	{
		TaxRowModel taxRow = null;
		final List<TaxRowModel> taxRows = getTaxRowService().getTaxRow(Collections.singletonList(BcfCoreConstants.DMF), location);
		if (CollectionUtils.isNotEmpty(taxRows))
		{
			taxRow = taxRows.stream().findFirst().orElse(null);
		}
		if (Objects.isNull(taxRow))
		{
			taxRow = getModelService().create(TaxRowModel.class);
			final TaxModel dmfTax = getTaxService().getTaxForCode(BcfCoreConstants.DMF);
			taxRow.setTax(dmfTax);
			taxRow.setLocation(location.getCode());
		}
		taxRow.setValue(dmfRate); //NOSONAR
		return taxRow;
	}

	public PointOfServiceModel uploadPointOfService(final String locationCode, final PointOfServiceModel pointOfService,
			final AccommodationOfferingDataModel accommodationOfferingData)
	{
		pointOfService.setType(PointOfServiceTypeEnum.STORE);
		pointOfService.setLatitude(accommodationOfferingData.getLatitude());
		pointOfService.setLongitude(accommodationOfferingData.getLongitude());
		pointOfService.setTimeZoneId(accommodationOfferingData.getTimeZoneId());
		pointOfService.setBaseStore(getBaseStoreService().getAllBaseStores().get(0));
		return pointOfService;
	}

	private AddressModel uploadAddress(final AddressModel address, final AccommodationOfferingDataModel accommodationOfferingData)
	{
		address.setStreetname(accommodationOfferingData.getStreetName());
		address.setStreetnumber(accommodationOfferingData.getStreetNumber());
		address.setPostalcode(accommodationOfferingData.getPostalCode());
		address.setTown(accommodationOfferingData.getTown().getName());
		address.setCountry(accommodationOfferingData.getCountry());
		address.setPhone1(accommodationOfferingData.getPhone());
		return address;
	}

	/**
	 * Validate accommodation offering data.
	 *
	 * @param accommodationOfferingData the accommodation offering data
	 */

	protected boolean validateAccommodationOfferingData(final AccommodationOfferingDataModel accommodationOfferingData)
	{
		if (StringUtils.isEmpty(accommodationOfferingData.getCode()))
		{
			log.error("Code is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}

		if (Objects.isNull(accommodationOfferingData.getPropertyName()))
		{
			log.error("PropertyName is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getProvider()))
		{
			log.error("provider is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}
		if (Objects.isNull(accommodationOfferingData.getTown()))
		{
			log.error("Town is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}
		if (Objects.isNull(accommodationOfferingData.getCountry()))
		{
			log.error("Country is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}
		if (StringUtils.isEmpty(accommodationOfferingData.getPhone()))
		{
			log.error("Phone is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}

		if (StringUtils.isEmpty(accommodationOfferingData.getTimeZoneId()))
		{
			log.error("TimeZoneId is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}
		if (Objects.isNull(accommodationOfferingData.getPropertyType()))
		{
			log.error("PropertyType is empty for " + accommodationOfferingData.getPropertyName());
			return false;
		}

		return true;
	}

	/**
	 * @return the accommodationProviderService
	 */
	protected AccommodationProviderService getAccommodationProviderService()
	{
		return accommodationProviderService;
	}

	/**
	 * @param accommodationProviderService the accommodationProviderService to set
	 */
	@Required
	public void setAccommodationProviderService(final AccommodationProviderService accommodationProviderService)
	{
		this.accommodationProviderService = accommodationProviderService;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService the baseStoreService to set
	 */
	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the vendorService
	 */
	protected VendorService getVendorService()
	{
		return vendorService;
	}

	/**
	 * @param vendorService the vendorService to set
	 */
	@Required
	public void setVendorService(final VendorService vendorService)
	{
		this.vendorService = vendorService;
	}

	/**
	 * @return the bcfTravelLocationService
	 */
	protected BcfTravelLocationService getBcfTravelLocationService()
	{
		return bcfTravelLocationService;
	}

	/**
	 * @param bcfTravelLocationService the bcfTravelLocationService to set
	 */
	@Required
	public void setBcfTravelLocationService(final BcfTravelLocationService bcfTravelLocationService)
	{
		this.bcfTravelLocationService = bcfTravelLocationService;
	}

	protected PassengerTypeService getPassengerTypeService()
	{
		return passengerTypeService;
	}

	@Required
	public void setPassengerTypeService(final PassengerTypeService passengerTypeService)
	{
		this.passengerTypeService = passengerTypeService;
	}

	protected TaxService getTaxService()
	{
		return taxService;
	}

	@Required
	public void setTaxService(final TaxService taxService)
	{
		this.taxService = taxService;
	}

	/**
	 * @return the bcfCommentService
	 */
	protected BcfCommentService getBcfCommentService()
	{
		return bcfCommentService;
	}

	/**
	 * @param bcfCommentService the bcfCommentService to set
	 */
	@Required
	public void setBcfCommentService(final BcfCommentService bcfCommentService)
	{
		this.bcfCommentService = bcfCommentService;
	}


	protected BcfPropertyFacilityService getBcfPropertyFacilityService()
	{
		return bcfPropertyFacilityService;
	}

	@Required
	public void setBcfPropertyFacilityService(final BcfPropertyFacilityService bcfPropertyFacilityService)
	{
		this.bcfPropertyFacilityService = bcfPropertyFacilityService;
	}
}
