/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.travelservices.model.accommodation.DateRangeModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.core.accommodation.service.DateRangeService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;


public class RoomRatesCreateUtility extends AbstractDataUploadUtility
{
	private DateRangeService dateRangeService;
	private UnitService unitService;

	/**
	 * Creates the new room rates.
	 *
	 * @param accommodationOffering          the accommodation offering
	 * @param accommodation                  the accommodation
	 * @param groupedRatePlanDaysOfWeekDatas the list of rate plan by DaysOfWeek Data
	 * @param roomRateProductCode            the room rate product code
	 * @return the room rate product model
	 */
	public RoomRateProductModel createNewRoomRates(final AccommodationOfferingModel accommodationOffering,
			final AccommodationModel accommodation, final List<RatePlanDataModel> groupedRatePlanDaysOfWeekDatas,
			final String roomRateProductCode, final List<ItemModel> items)
	{
		final RatePlanDataModel ratePlanDataModel = groupedRatePlanDaysOfWeekDatas.stream().findFirst().get();
		final List<DateRangeModel> dateRanges = new ArrayList<>();
		for (final RatePlanDataModel ratePlanData : groupedRatePlanDaysOfWeekDatas)
		{
			final Date truncatedStartDate = DateUtils.truncate(ratePlanData.getRatePlanStartDate(), Calendar.DATE);
			final Date truncatedEndDate = DateUtils.truncate(ratePlanData.getRatePlanEndDate(), Calendar.DATE);
			final Date startDate = truncatedStartDate;
			final Date endDate = truncatedEndDate;
			DateRangeModel dateRange = getDateRangeService().getDateRange(startDate, endDate);
			if (Objects.isNull(dateRange))
			{
				dateRange = createNewDateRange(startDate, endDate);
			}
			if (!dateRanges.contains(dateRange))
			{
				dateRanges.add(dateRange);
			}
		}
		return createNewRoomRateProduct(accommodation, roomRateProductCode, ratePlanDataModel, dateRanges, items);
	}

	/**
	 * Updates the room rates.
	 *
	 * @param roomRateProduct                the room rate product
	 * @param groupedRatePlanDaysOfWeekDatas the list of rate plan by DaysOfWeek Data
	 */
	public void updateRoomRates(final RoomRateProductModel roomRateProduct,
			final List<RatePlanDataModel> groupedRatePlanDaysOfWeekDatas)
	{
		final List<DateRangeModel> dateRanges = Objects.isNull(roomRateProduct.getDateRanges()) ? new ArrayList<>()
				: new ArrayList<>(roomRateProduct.getDateRanges());
		for (final RatePlanDataModel ratePlanData : groupedRatePlanDaysOfWeekDatas)
		{
			final Date truncatedStartDate = DateUtils.truncate(ratePlanData.getRatePlanStartDate(), Calendar.DATE);
			final Date truncatedendDate = DateUtils.truncate(ratePlanData.getRatePlanEndDate(), Calendar.DATE);
			final Date startDate = truncatedStartDate;
			final Date endDate = truncatedendDate;
			DateRangeModel dateRange = getDateRangeService().getDateRange(startDate, endDate);

			if (Objects.isNull(dateRange))
			{
				dateRange = createNewDateRange(startDate, endDate);
			}
			if (!dateRanges.contains(dateRange))
			{
				dateRanges.add(dateRange);
			}
		}
		roomRateProduct.setDateRanges(dateRanges);
	}

	/**
	 * Creates the new date range.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @return the date range model
	 */
	protected DateRangeModel createNewDateRange(final Date startDate, final Date endDate)
	{
		final DateRangeModel dateRange = getModelService().create(DateRangeModel.class);
		dateRange.setStartingDate(startDate);
		dateRange.setEndingDate(endDate);
		return dateRange;
	}

	/**
	 * Creates the new room rate product.
	 *
	 * @param accommodation       the accommodation
	 * @param roomRateProductCode the room rate product code
	 * @param dateRanges          the list of date range model
	 * @return the date range model
	 */
	protected RoomRateProductModel createNewRoomRateProduct(final AccommodationModel accommodation,
			final String roomRateProductCode, final RatePlanDataModel ratePlanData, List<DateRangeModel> dateRanges,
			final List<ItemModel> items)
	{
		final RoomRateProductModel roomRateProduct = getModelService().create(RoomRateProductModel.class);
		roomRateProduct.setCode(roomRateProductCode);
		List<DayOfWeek> daysOfWeek = ratePlanData.getDaysOfWeek();
		if (CollectionUtils.isEmpty(daysOfWeek))
		{
			daysOfWeek = Arrays.asList(DayOfWeek.values());
		}
		roomRateProduct.setDaysOfWeek(daysOfWeek);
		roomRateProduct.setCatalogVersion(accommodation.getCatalogVersion());
		roomRateProduct.setEurope1PriceFactory_PTG(accommodation.getEurope1PriceFactory_PTG());
		roomRateProduct.setApprovalStatus(accommodation.getApprovalStatus());
		if (dateRanges.isEmpty())
		{
			dateRanges = new ArrayList<>();
			roomRateProduct.setDateRanges(dateRanges);
		}
		roomRateProduct.setDateRanges(dateRanges);
		roomRateProduct.setUnit(getUnitService().getUnitForCode("pieces"));
		items.add(roomRateProduct);
		return roomRateProduct;
	}

	/**
	 * @return the dateRangeService
	 */
	protected DateRangeService getDateRangeService()
	{
		return dateRangeService;
	}

	/**
	 * @param dateRangeService the dateRangeService to set
	 */
	@Required
	public void setDateRangeService(final DateRangeService dateRangeService)
	{
		this.dateRangeService = dateRangeService;
	}

	protected UnitService getUnitService()
	{
		return unitService;
	}

	@Required
	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}
}
