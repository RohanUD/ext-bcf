/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import de.hybris.platform.travelservices.services.RatePlanService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.core.accommodation.service.AccommodationCancellationFeesService;
import com.bcf.core.accommodation.service.AccommodationCancellationPolicyService;
import com.bcf.core.model.accommodation.AccommodationCancellationFeesModel;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationFeesDataService;
import com.bcf.integration.dataupload.service.accommodations.AccommodationCancellationPolicyDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.accommodation.AccommodationCancellationFeesDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class AccommodationCancellationPolicyDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(AccommodationCancellationPolicyDataUploadUtility.class);

	private AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService;
	private AccommodationCancellationFeesDataService accommodationCancellationFeesDataService;
	private AccommodationCancellationFeesDataValidator accommodationCancellationFeesDataValidator;
	private AccommodationCancellationPolicyService accommodationCancellationPolicyService;
	private AccommodationCancellationFeesService accommodationCancellationFeesService;
	private AccommodationOfferingService accommodationOfferingService;
	private RatePlanService ratePlanService;


	public void uploadAccommodationCancellationPolicyDatas()
	{
		final List<AccommodationCancellationPolicyDataModel> pendingAccCancellationPolicyData = getAccommodationCancellationPolicyDataService()
				.getAccommodationCancellationPolicyData(DataImportProcessStatus.PENDING);
		LOG.info("pendingAccCancellationPolicyData size : " + pendingAccCancellationPolicyData.size());

		if (!pendingAccCancellationPolicyData.isEmpty())
		{
			final List<ItemModel> items = new ArrayList<>();
			uploadAccommodationCancellationPolicyData(pendingAccCancellationPolicyData, items);
			synchronizeProductCatalog(items);
		}
	}

	/**
	 * Upload Accommodation Cancellation Policy Data.
	 *
	 * @return the list
	 */
	public void uploadAccommodationCancellationPolicyData(
			final List<AccommodationCancellationPolicyDataModel> pendingAccCancellationPolicyData, final List<ItemModel> items)
	{
		updateStatusOfCancellationPolicyData(pendingAccCancellationPolicyData, DataImportProcessStatus.PENDING,
				DataImportProcessStatus.PROCESSING);

		final List<AccommodationCancellationPolicyDataModel> validAccCancellationPolicyData = new ArrayList<>();
		final Map<String, List<AccommodationCancellationFeesDataModel>> validAccCancellationFeesDataMap = new HashMap<>();
		for (final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData : pendingAccCancellationPolicyData)
		{
			final List<AccommodationCancellationFeesDataModel> pendingAccCancellationFeesData = accommodationCancellationPolicyData
					.getAccommodationCancellationFeesData().stream().filter(
							accommodationCancellationFeesData -> accommodationCancellationFeesData.getStatus()
									.equals(DataImportProcessStatus.PENDING)).collect(Collectors.toList());

			LOG.debug("pendingAccCancellationFeesData size : " + pendingAccCancellationFeesData.size());

			/* checking for invalid cancellation fees items */
			final List<AccommodationCancellationFeesDataModel> invalidAccCancellationFeesData = pendingAccCancellationFeesData
					.stream()
					.filter(accommodationCancellationFeesData -> !getAccommodationCancellationFeesDataValidator()
							.validateAccommodationCancellationFeesData(accommodationCancellationFeesData).isEmpty())
					.collect(Collectors.toList());
			List<AccommodationCancellationFeesDataModel> validAccCancellationFeesData = removeInvalidAccommodationCancellationFeesData(
					pendingAccCancellationFeesData, invalidAccCancellationFeesData);
			updateStatusOfCancellationFeesData(validAccCancellationFeesData, DataImportProcessStatus.PENDING,
					DataImportProcessStatus.PROCESSING);
			validAccCancellationFeesData = validAccCancellationFeesData.stream().filter(
					accommodationCancellationFeesData -> accommodationCancellationFeesData.getStatus()
							.equals(DataImportProcessStatus.PROCESSING)).collect(Collectors.toList());
			LOG.debug("validAccCancellationFeesData size : " + validAccCancellationFeesData.size());
			if (!validAccCancellationFeesData.isEmpty())
			{
				validAccCancellationFeesDataMap.put(accommodationCancellationPolicyData.getCode(), validAccCancellationFeesData);
			}

			/* checking for failed cancellation fees items in a cancellation policy item*/
			final AccommodationCancellationPolicyDataModel accCancellationPolicyData = findValidAccCancellationPolicyData(
					accommodationCancellationPolicyData);
			if (Objects.nonNull(accCancellationPolicyData))
			{
				validAccCancellationPolicyData.add(accCancellationPolicyData);
			}
		}
		LOG.debug("validAccCancellationFeesDataMap size : " + validAccCancellationFeesDataMap.size());

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		if (!validAccCancellationPolicyData.isEmpty())
		{
			uploadAccommodationCancellationPolicy(validAccCancellationPolicyData, validAccCancellationFeesDataMap, catalogVersion, items);
			updateStatusOfCancellationPolicyData(validAccCancellationPolicyData, DataImportProcessStatus.PROCESSED,
					DataImportProcessStatus.SUCCESS);

			for (final Map.Entry<String, List<AccommodationCancellationFeesDataModel>> validAccCancellationFeesData : validAccCancellationFeesDataMap
					.entrySet())
			{
				updateStatusOfCancellationFeesData(validAccCancellationFeesData.getValue(), DataImportProcessStatus.PROCESSED,
						DataImportProcessStatus.SUCCESS);
				validAccCancellationFeesData.getValue().stream().forEach(validAccCancellationFeeData -> items.add(validAccCancellationFeeData));
			}
		}
		getModelService().saveAll();
	}

	/**
	 * UploadAccommodation Cancellation Policy.
	 *
	 * @param validAccCancellationPolicyData    the list of AccommodationCancellationPolicyDataModel
	 * @param validAccCancellationPolicyDataMap the Map of accommodationCancellationPolicyCode and List<AccommodationCancellationFeesDataModel>
	 * @return
	 */
	public Boolean uploadAccommodationCancellationPolicy(
			final List<AccommodationCancellationPolicyDataModel> validAccCancellationPolicyData,
			final Map<String, List<AccommodationCancellationFeesDataModel>> validAccCancellationPolicyDataMap,
			final CatalogVersionModel catalogVersion, final List<ItemModel> items)
	{
		final Map<RatePlanDataModel, List<AccommodationCancellationPolicyDataModel>> ratePlanDataMap = validAccCancellationPolicyData
				.stream().filter(accommodationCancellationPolicyData -> Objects
						.nonNull(accommodationCancellationPolicyData.getRatePlanData()))
				.collect(Collectors.groupingBy(AccommodationCancellationPolicyDataModel::getRatePlanData));
		for (final Map.Entry<RatePlanDataModel, List<AccommodationCancellationPolicyDataModel>> ratePlanDataMapEntry : ratePlanDataMap
				.entrySet())
		{
			final AccommodationCancellationPolicyDataModel ratePlanDataEntry = ratePlanDataMapEntry.getValue().stream()
					.findFirst().get();

			//Get RatePlanModel
			final RatePlanModel ratePlan = findValidRatePlanEntries(ratePlanDataEntry.getRatePlanData(),
					validAccCancellationPolicyData);
			if (Objects.isNull(ratePlan))
			{
				continue;
			}

			final List<AccommodationCancellationPolicyModel> accommodationCancellationPolicyList = Objects.isNull(ratePlan
					.getAccommodationCancellationPolicy()) ? new ArrayList<>() :
					new ArrayList<>(ratePlan.getAccommodationCancellationPolicy());
			for (final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData : ratePlanDataMapEntry.getValue())
			{
				LOG.debug(String.format("Importing cancellationpolicyCode-%s for rateplan- %s (room- %s) ",
						accommodationCancellationPolicyData.getCode(), ratePlan.getCode(),
						ratePlan.getAccommodation().iterator().next().getCode()));

				final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesData = accommodationCancellationPolicyData
						.getAccommodationCancellationFeesData().stream().filter(
								accommodationCancellationFeeData -> accommodationCancellationFeeData
										.getStatus().equals(DataImportProcessStatus.PROCESSING)).collect(Collectors.toList());

				/* AccommodationCancellationPolicyModel &  AccommodationCancellationFeesModel */
				final AccommodationCancellationPolicyModel accommodationCancellationPolicy = uploadAccCancellationPolicy(
						accommodationCancellationPolicyData, accommodationCancellationFeesData, ratePlan, items);
				if (!accommodationCancellationPolicyList.contains(accommodationCancellationPolicy))
				{
					accommodationCancellationPolicyList.add(accommodationCancellationPolicy);
				}
				updateStatusOfCancellationFeesData(accommodationCancellationFeesData, DataImportProcessStatus.PROCESSING,
						DataImportProcessStatus.PROCESSED);
				accommodationCancellationPolicyData.setStatus(DataImportProcessStatus.PROCESSED);
				items.add(accommodationCancellationPolicyData);
			}
			ratePlan.setAccommodationCancellationPolicy(accommodationCancellationPolicyList);
			items.add(ratePlan);
		}
		return Boolean.TRUE;
	}

	public AccommodationCancellationPolicyModel uploadAccCancellationPolicy(
			final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData,
			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesData, final RatePlanModel ratePlan, final List<ItemModel> items)
	{
		final String accommodationCancellationPolicyDataCode = accommodationCancellationPolicyData.getCode();

		AccommodationCancellationPolicyModel accommodationCancellationPolicy = getAccommodationCancellationPolicyService()
				.getAccommodationCancellationPolicy(accommodationCancellationPolicyDataCode,accommodationCancellationPolicyData.getCatalogVersion());
		if (Objects.isNull(accommodationCancellationPolicy))
		{
			LOG.debug("Create new accommodationCancellationPolicy");
			accommodationCancellationPolicy = getModelService().create(AccommodationCancellationPolicyModel.class);
			accommodationCancellationPolicy.setCode(accommodationCancellationPolicyDataCode);
			accommodationCancellationPolicy.setCatalogVersion(accommodationCancellationPolicyData.getCatalogVersion());
		}
		updateAccommodationCancellationPolicy(accommodationCancellationPolicy, accommodationCancellationPolicyData);
		getModelService().save(accommodationCancellationPolicy);
		final List<AccommodationCancellationFeesModel> accCancellationFees = uploadAccommodationCancellationFees(
				accommodationCancellationPolicy, accommodationCancellationFeesData, items);
		accommodationCancellationPolicy.setAccommodationCancellationFees(accCancellationFees);
		accommodationCancellationPolicy.setRatePlan(ratePlan);
		items.add(accommodationCancellationPolicy);
		return accommodationCancellationPolicy;
	}

	private List<AccommodationCancellationFeesModel> uploadAccommodationCancellationFees(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy,
			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesData, final List<ItemModel> items)
	{
		final List<AccommodationCancellationFeesModel> accCancellationFees = Objects.isNull(accommodationCancellationPolicy
				.getAccommodationCancellationFees()) ? new ArrayList<>() :
				new ArrayList<>(accommodationCancellationPolicy.getAccommodationCancellationFees());

		for (final AccommodationCancellationFeesDataModel accommodationCancellationFeeData : accommodationCancellationFeesData)
		{
			AccommodationCancellationFeesModel accommodationCancellationFees = getAccommodationCancellationFeesService()
					.getAccommodationCancellationFees(accommodationCancellationPolicy,
							accommodationCancellationFeeData.getMinDays(),
							accommodationCancellationFeeData.getMaxDays());
			if (Objects.isNull(accommodationCancellationFees))
			{
				LOG.debug("Create new accommodationCancellationFee");
				accommodationCancellationFees = getModelService().create(AccommodationCancellationFeesModel.class);
			}
			createUpdateAccommodationCancellationFees(accommodationCancellationPolicy, accommodationCancellationFees,
					accommodationCancellationFeeData);
			if (!accCancellationFees.contains(accommodationCancellationFees))
			{
				items.add(accommodationCancellationFees);
				accCancellationFees.add(accommodationCancellationFees);
			}
		}
		return accCancellationFees;
	}

	private void updateAccommodationCancellationPolicy(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy,
			final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData)
	{
		accommodationCancellationPolicy.setStartDate(Objects.isNull(accommodationCancellationPolicyData.getStartDate()) ?
				accommodationCancellationPolicyData.getStartDate() :
				DateUtils.truncate(accommodationCancellationPolicyData.getStartDate(), Calendar.DATE));
		accommodationCancellationPolicy.setEndDate(Objects.isNull(accommodationCancellationPolicyData.getEndDate()) ?
				accommodationCancellationPolicyData.getEndDate() :
				DateUtils.truncate(accommodationCancellationPolicyData.getEndDate(), Calendar.DATE));
		getLocaleList().forEach(locale -> accommodationCancellationPolicy
				.setCancellationPolicyDescription(accommodationCancellationPolicyData.getCancellationPolicyDescription(locale),
						locale));
	}

	private void createUpdateAccommodationCancellationFees(
			final AccommodationCancellationPolicyModel accommodationCancellationPolicy,
			final AccommodationCancellationFeesModel accommodationCancellationFees,
			final AccommodationCancellationFeesDataModel accommodationCancellationFeeData)
	{
		accommodationCancellationFees.setMinDays(accommodationCancellationFeeData.getMinDays());
		accommodationCancellationFees.setMaxDays(accommodationCancellationFeeData.getMaxDays());
		accommodationCancellationFees.setCancellationFee(accommodationCancellationFeeData.getCancellationFee());
		accommodationCancellationFees.setCancellationFeeType(accommodationCancellationFeeData.getCancellationFeeType());
		accommodationCancellationFees.setChangeFee(accommodationCancellationFeeData.getChangeFee());
		accommodationCancellationFees.setChangeFeeType(accommodationCancellationFeeData.getChangeFeeType());
		accommodationCancellationFees.setAccommodationCancellationPolicy(accommodationCancellationPolicy);
	}

	private RatePlanModel findValidRatePlanEntries(final RatePlanDataModel ratePlanData,
			final List<AccommodationCancellationPolicyDataModel> validAccCancellationPolicyData)
	{
		final String ratePlanCode = ratePlanData.getCode();
		RatePlanModel ratePlan = null;
		try
		{
			ratePlan = getRatePlanService().getRatePlanForCode(ratePlanCode);
		}
		catch (final ModelNotFoundException ex)
		{
			// Deliberately left blank
		}
		if (Objects.isNull(ratePlan))
		{
			getValidAccommodationCancellationPolicyData(validAccCancellationPolicyData, ratePlanData);
			LOG.debug("ratePlan is null and validAccCancellationPolicyData size : "
					+ validAccCancellationPolicyData.size());
		}
		return ratePlan;
	}

	private AccommodationCancellationPolicyDataModel findValidAccCancellationPolicyData(
			final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData)
	{
		boolean flag = false;
		final Collection<AccommodationCancellationFeesDataModel> accCancellationFeesData = accommodationCancellationPolicyData
				.getAccommodationCancellationFeesData();
		for (final AccommodationCancellationFeesDataModel accCancellationFeeData : accCancellationFeesData)
		{
			if (!accCancellationFeeData.getStatus().equals(DataImportProcessStatus.FAILED))
			{
				flag = true;
				break;
			}
		}
		if (flag)
		{
			return accommodationCancellationPolicyData;
		}

		LOG.debug("accommodationCancellationPolicyData mark FAILED");
		accommodationCancellationPolicyData.setStatus(DataImportProcessStatus.FAILED);
		return null;
	}

	private List<AccommodationCancellationPolicyDataModel> getValidAccommodationCancellationPolicyData(
			List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDatas,
			final AccommodationOfferingDataModel accommodationOfferingData)
	{
		final List<AccommodationCancellationPolicyDataModel> invalidAccommodationCancellationPolicyData = accommodationCancellationPolicyDatas
				.stream()
				.filter(
						accCancellationPolicyData -> accCancellationPolicyData.getAccommodationOfferingData()
								.equals(accommodationOfferingData))
				.collect(Collectors.toList());
		LOG.debug("invalidAccommodationCancellationPolicy Data size : " + invalidAccommodationCancellationPolicyData.size());
		final List<AccommodationCancellationPolicyDataModel> validAccommodationCancellationPolicyData = removeInvalidAccommodationCancellationPolicyData(
				accommodationCancellationPolicyDatas, invalidAccommodationCancellationPolicyData);
		accommodationCancellationPolicyDatas = validAccommodationCancellationPolicyData;
		LOG.debug("accommodationCancellationPolicy Datas size : " + accommodationCancellationPolicyDatas.size());
		return accommodationCancellationPolicyDatas;
	}

	private List<AccommodationCancellationPolicyDataModel> getValidAccommodationCancellationPolicyData(
			List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDatas,
			final AccommodationDataModel accommodationData)
	{
		final List<AccommodationCancellationPolicyDataModel> invalidAccommodationCancellationPolicyData = accommodationCancellationPolicyDatas
				.stream()
				.filter(accCancellationPolicyData -> accCancellationPolicyData.getAccommodationData().equals(accommodationData))
				.collect(Collectors.toList());
		LOG.debug("invalid Accommodation Cancellation Policy Data size : " + invalidAccommodationCancellationPolicyData.size());
		final List<AccommodationCancellationPolicyDataModel> validAccommodationCancellationPolicyData = removeInvalidAccommodationCancellationPolicyData(
				accommodationCancellationPolicyDatas, invalidAccommodationCancellationPolicyData);
		accommodationCancellationPolicyDatas = validAccommodationCancellationPolicyData;
		LOG.debug("accommodation Cancellation Policy Datas size : " + accommodationCancellationPolicyDatas.size());
		return accommodationCancellationPolicyDatas;
	}

	private List<AccommodationCancellationPolicyDataModel> getValidAccommodationCancellationPolicyData(
			List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDatas,
			final RatePlanDataModel rateplanData)
	{
		final List<AccommodationCancellationPolicyDataModel> invalidAccommodationCancellationPolicyData = accommodationCancellationPolicyDatas
				.stream().filter(accCancellationPolicyData -> accCancellationPolicyData.getRatePlanData().equals(rateplanData))
				.collect(Collectors.toList());
		LOG.debug("invalid AccommodationCancellationPolicyData size : " + invalidAccommodationCancellationPolicyData.size());
		final List<AccommodationCancellationPolicyDataModel> validAccommodationCancellationPolicyData = removeInvalidAccommodationCancellationPolicyData(
				accommodationCancellationPolicyDatas, invalidAccommodationCancellationPolicyData);
		accommodationCancellationPolicyDatas = validAccommodationCancellationPolicyData;
		LOG.debug("accommodationCancellationPolicyData size : " + accommodationCancellationPolicyDatas.size());
		return accommodationCancellationPolicyDatas;
	}

	private List<AccommodationCancellationPolicyDataModel> removeInvalidAccommodationCancellationPolicyData(
			final List<AccommodationCancellationPolicyDataModel> accCancellationPolicyDatas,
			final List<AccommodationCancellationPolicyDataModel> invalidAccCancellationPolicyData)
	{
		invalidAccCancellationPolicyData.forEach(
				accommodationCancellationPolicyData -> accommodationCancellationPolicyData
						.setStatus(DataImportProcessStatus.FAILED));

		final List<AccommodationCancellationPolicyDataModel> validAccommodationCancellationPolicyData = accCancellationPolicyDatas
				.stream()
				.filter(validAccommodationCancellationPolicy -> !invalidAccCancellationPolicyData.contains(
						validAccommodationCancellationPolicy))
				.collect(Collectors.toList());
		LOG.debug("validAccommodationCancellationPolicyData size : " + validAccommodationCancellationPolicyData.size());
		return validAccommodationCancellationPolicyData;
	}

	private List<AccommodationCancellationFeesDataModel> removeInvalidAccommodationCancellationFeesData(
			final List<AccommodationCancellationFeesDataModel> accCancellationFeesDatas,
			final List<AccommodationCancellationFeesDataModel> invalidAccCancellationFeesData)
	{
		invalidAccCancellationFeesData.forEach(
				accommodationCancellationFeesData -> accommodationCancellationFeesData.setStatus(DataImportProcessStatus.FAILED));

		final List<AccommodationCancellationFeesDataModel> validAccommodationCancellationFeesData = accCancellationFeesDatas
				.stream()
				.filter(validAccommodationCancellationFees -> !invalidAccCancellationFeesData.contains(
						validAccommodationCancellationFees))
				.collect(Collectors.toList());
		LOG.debug("validAccommodationCancellationFeesData size : " + validAccommodationCancellationFeesData.size());
		return validAccommodationCancellationFeesData;
	}

	private void updateStatusOfCancellationPolicyData(
			final List<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDatas,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		accommodationCancellationPolicyDatas.stream()
				.filter(accommodationCancellationPolicyData -> fromStatus.equals(accommodationCancellationPolicyData.getStatus()))
				.forEach(accommodationCancellationPolicyData -> {
					accommodationCancellationPolicyData.setStatus(toStatus);
					getModelService().save(accommodationCancellationPolicyData);
				});
	}

	private void updateStatusOfCancellationFeesData(
			final List<AccommodationCancellationFeesDataModel> accommodationCancellationFeesDatas,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		accommodationCancellationFeesDatas.stream()
				.filter(accommodationCancellationFeesData -> fromStatus.equals(accommodationCancellationFeesData.getStatus()))
				.forEach(accommodationCancellationFeesData -> accommodationCancellationFeesData.setStatus(toStatus));
	}

	protected AccommodationCancellationPolicyDataService getAccommodationCancellationPolicyDataService()
	{
		return accommodationCancellationPolicyDataService;
	}

	@Required
	public void setAccommodationCancellationPolicyDataService(
			final AccommodationCancellationPolicyDataService accommodationCancellationPolicyDataService)
	{
		this.accommodationCancellationPolicyDataService = accommodationCancellationPolicyDataService;
	}

	protected AccommodationCancellationFeesDataService getAccommodationCancellationFeesDataService()
	{
		return accommodationCancellationFeesDataService;
	}

	@Required
	public void setAccommodationCancellationFeesDataService(
			final AccommodationCancellationFeesDataService accommodationCancellationFeesDataService)
	{
		this.accommodationCancellationFeesDataService = accommodationCancellationFeesDataService;
	}

	protected AccommodationCancellationFeesDataValidator getAccommodationCancellationFeesDataValidator()
	{
		return accommodationCancellationFeesDataValidator;
	}

	@Required
	public void setAccommodationCancellationFeesDataValidator(
			final AccommodationCancellationFeesDataValidator accommodationCancellationFeesDataValidator)
	{
		this.accommodationCancellationFeesDataValidator = accommodationCancellationFeesDataValidator;
	}

	protected AccommodationCancellationPolicyService getAccommodationCancellationPolicyService()
	{
		return accommodationCancellationPolicyService;
	}

	@Required
	public void setAccommodationCancellationPolicyService(
			final AccommodationCancellationPolicyService accommodationCancellationPolicyService)
	{
		this.accommodationCancellationPolicyService = accommodationCancellationPolicyService;
	}

	protected AccommodationCancellationFeesService getAccommodationCancellationFeesService()
	{
		return accommodationCancellationFeesService;
	}

	@Required
	public void setAccommodationCancellationFeesService(
			final AccommodationCancellationFeesService accommodationCancellationFeesService)
	{
		this.accommodationCancellationFeesService = accommodationCancellationFeesService;
	}

	@Override
	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Override
	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected RatePlanService getRatePlanService()
	{
		return ratePlanService;
	}

	@Required
	public void setRatePlanService(final RatePlanService ratePlanService)
	{
		this.ratePlanService = ratePlanService;
	}
}
