/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.ExtraGuestOccupancyProductDataModel;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.integration.dataupload.service.accommodations.ExtraGuestOccupancyProductDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class ExtraGuestOccupancyProductUploadUtility extends AbstractDataUploadUtility
{
	private static final String UNIT = "pieces";
	private static final String EXTRA_OCCUPANCY = "EGO";

	private UnitService unitService;
	private ExtraGuestOccupancyProductDataService extraGuestOccupancyProductDataService;
	private ProductTaxGroupMappingService productTaxGroupMappingService;
	private BcfAccommodationService bcfAccommodationService;

	public void uploadExtraGuestOccupancy()
	{
		final List<ItemModel> items = new ArrayList<>();
		final List<ExtraGuestOccupancyProductDataModel> extraGuestOccupancyProductDataModelList = getExtraGuestOccupancyProductDataService()
				.getExtraGuestOccupancyProductData(DataImportProcessStatus.PENDING);
		processExtraGuestOccupancyDatas(extraGuestOccupancyProductDataModelList, items);
	}

	public void processExtraGuestOccupancyDatas(
			List<ExtraGuestOccupancyProductDataModel> extraGuestOccupancyProductDataModelList,
			final List<ItemModel> items)
	{

		List<ExtraGuestOccupancyProductDataModel> invalidExtraGuestOccupancyProductDataModelList=extraGuestOccupancyProductDataModelList
				.stream().filter(extraGuestOccupancyProductDataModel->extraGuestOccupancyProductDataModel.getAccommodationData()==null).collect(Collectors.toList());

		extraGuestOccupancyProductDataModelList=extraGuestOccupancyProductDataModelList
				.stream().filter(extraGuestOccupancyProductDataModel->extraGuestOccupancyProductDataModel.getAccommodationData()!=null).collect(Collectors.toList());


		if (CollectionUtils.isNotEmpty(invalidExtraGuestOccupancyProductDataModelList))
		{
			invalidExtraGuestOccupancyProductDataModelList.forEach(extraGuestOccupancyProductDataModel -> extraGuestOccupancyProductDataModel
					.setStatus(DataImportProcessStatus.FAILED));
			getModelService().saveAll(invalidExtraGuestOccupancyProductDataModelList);
		}

		if(CollectionUtils.isNotEmpty(extraGuestOccupancyProductDataModelList))
		{
			final Map<AccommodationDataModel, List<ExtraGuestOccupancyProductDataModel>> accommodationExtraGuestOccupancyProductDatasMap = extraGuestOccupancyProductDataModelList
					.stream()
					.filter(extraGuestOccupancyProductDataModel -> extraGuestOccupancyProductDataModel.getAccommodationData() != null)
					.collect(
							Collectors.groupingBy(
									extraGuestOccupancyProductDataModel -> extraGuestOccupancyProductDataModel.getAccommodationData()));
			uploadExtraGuestOccupancy(accommodationExtraGuestOccupancyProductDatasMap, items);

		}
	}

	public void uploadExtraGuestOccupancy(
			final Map<AccommodationDataModel, List<ExtraGuestOccupancyProductDataModel>> accommodationExtraGuestOccupancyProductDatasMap,
			final List<ItemModel> items)
	{
		for (final Map.Entry<AccommodationDataModel, List<ExtraGuestOccupancyProductDataModel>> accommodationExtraOccupancyProductDatMapEntry : accommodationExtraGuestOccupancyProductDatasMap
				.entrySet())
		{
			final AccommodationDataModel accommodationDataModel = accommodationExtraOccupancyProductDatMapEntry.getKey();
			final Collection<ExtraGuestOccupancyProductDataModel> extraGuestOccupancyProductDatas = accommodationExtraOccupancyProductDatMapEntry
					.getValue();

			final AccommodationModel accommodationModel = getAccommodation(accommodationDataModel);
			if (Objects.isNull(accommodationModel))
			{
				extraGuestOccupancyProductDatas.forEach(extraGuestOccupancyProductDataModel -> extraGuestOccupancyProductDataModel
						.setStatus(DataImportProcessStatus.FAILED));
				getModelService().saveAll(extraGuestOccupancyProductDatas);
				continue;
			}

			final List<ExtraGuestOccupancyProductModel> ExtraGuestOccupancyProducts = extraGuestOccupancyProductDatas.stream()
					.map(extraGuestOccupancyProductDataModel -> getOrCreateExtraGuestOccupancyData(accommodationDataModel,
							extraGuestOccupancyProductDataModel, accommodationModel.getApprovalStatus(), items))
					.collect(Collectors.toList());

			accommodationModel.setExtraGuestProducts(ExtraGuestOccupancyProducts);
			getModelService().save(accommodationModel);

			extraGuestOccupancyProductDatas.forEach(extraGuestOccupancyProductDataModel -> extraGuestOccupancyProductDataModel
					.setStatus(DataImportProcessStatus.SUCCESS));
			getModelService().saveAll(extraGuestOccupancyProductDatas);
		}
	}

	protected AccommodationModel getAccommodation(final AccommodationDataModel accommodationDataModel)
	{
		AccommodationModel accommodation = null;
		try
		{
			accommodation = getBcfAccommodationService().getAccommodation(
					accommodationDataModel.getAccommodationOffering().getCode() + "_" + accommodationDataModel.getCode(),
					getCatalogVersionModel());
		}
		catch (final ModelNotFoundException ex)
		{
			return null;
		}

		return accommodation;
	}

	public ExtraGuestOccupancyProductModel getOrCreateExtraGuestOccupancyData(final AccommodationDataModel accommodationDataModel,
			final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel,
			final ArticleApprovalStatus articleApprovalStatus, final List<ItemModel> items)
	{
		final String extraGuestOccupancyProductCode =
				EXTRA_OCCUPANCY + UNDERSCORE + accommodationDataModel.getCode() + UNDERSCORE + extraGuestOccupancyProductDataModel
						.getPassengerType().getCode();
		ExtraGuestOccupancyProductModel extraGuestOccupancyProduct = null;
		try
		{
			final ProductModel product = getProductService().getProductForCode(getCatalogVersionModel(),
					extraGuestOccupancyProductCode);
			if (product instanceof ExtraGuestOccupancyProductModel)
			{
				extraGuestOccupancyProduct = (ExtraGuestOccupancyProductModel) product;
			}
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			extraGuestOccupancyProduct = createExtraGuestOccupancyProduct(extraGuestOccupancyProductCode, articleApprovalStatus);

		}
		updateExtraGuestOccupancyProduct(extraGuestOccupancyProduct, extraGuestOccupancyProductDataModel,
				accommodationDataModel, items);
		items.add(extraGuestOccupancyProduct);
		extraGuestOccupancyProductDataModel.setStatus(DataImportProcessStatus.PROCESSED);

		getModelService().saveAll();
		return extraGuestOccupancyProduct;
	}

	/**
	 * Creates the extra guest occupancy product.
	 */
	protected ExtraGuestOccupancyProductModel createExtraGuestOccupancyProduct(final String extraGuestOccupancyProductCode,
			final ArticleApprovalStatus articleApprovalStatus)
	{
		final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct = getModelService()
				.create(ExtraGuestOccupancyProductModel.class);
		extraGuestOccupancyProduct.setCode(extraGuestOccupancyProductCode);
		extraGuestOccupancyProduct.setCatalogVersion(getCatalogVersionModel());
		extraGuestOccupancyProduct.setApprovalStatus(articleApprovalStatus);
		return extraGuestOccupancyProduct;
	}

	/**
	 * Update extra guest occupancy product.
	 */
	protected ExtraGuestOccupancyProductModel updateExtraGuestOccupancyProduct(
			final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct,
			final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel,
			final AccommodationDataModel accommodationDataModel, final List<ItemModel> items)
	{
		createAndSetPriceRow(extraGuestOccupancyProduct, extraGuestOccupancyProductDataModel, items);
		updateProductTaxGroup(extraGuestOccupancyProduct, accommodationDataModel.getAccommodationOffering());
		items.add(extraGuestOccupancyProduct);
		return extraGuestOccupancyProduct;
	}

	/**
	 * Creates the and set price row.
	 */
	protected void createAndSetPriceRow(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct,
			final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel, final List<ItemModel> items)
	{
		PriceRowModel priceRow = null;
		if (!getModelService().isNew(extraGuestOccupancyProduct))
		{
			priceRow = getPriceRowService()
					.getPriceRow(extraGuestOccupancyProduct, extraGuestOccupancyProductDataModel.getPassengerType().getCode());
		}

		if (Objects.isNull(priceRow))
		{
			createNewPriceRow(extraGuestOccupancyProduct, extraGuestOccupancyProductDataModel, items);
			return;
		}

		priceRow.setPrice(extraGuestOccupancyProductDataModel.getPrice());//NOSONAR
	}

	/**
	 * Creates the new price row.
	 */
	private PriceRowModel createNewPriceRow(final ExtraGuestOccupancyProductModel extraGuestOccupancyProduct,
			final ExtraGuestOccupancyProductDataModel extraGuestOccupancyProductDataModel, final List<ItemModel> items)
	{
		final PriceRowModel price = getModelService().create(PriceRowModel.class);

		price.setProduct(extraGuestOccupancyProduct);
		price.setCurrency(getCommonI18NService().getBaseCurrency());
		price.setNet(true);
		price.setUnit(getUnitService().getUnitForCode(UNIT));
		price.setCatalogVersion(getCatalogVersionModel());
		price.setPassengerType(extraGuestOccupancyProductDataModel.getPassengerType().getCode());
		price.setPrice(extraGuestOccupancyProductDataModel.getPrice());
		items.add(price);
		return price;
	}

	/**
	 * update tax row.
	 */
	private void updateProductTaxGroup(final ExtraGuestOccupancyProductModel roomRateProduct,
			final AccommodationOfferingDataModel accommodationOfferingDataModel)
	{
		final Boolean applyDMF = accommodationOfferingDataModel.isApplyDMF();
		final Boolean applyMRDT = accommodationOfferingDataModel.isApplyMRDT();
		final Boolean applyGST = accommodationOfferingDataModel.isApplyGST();
		final Boolean applyPST = accommodationOfferingDataModel.isApplyPST();
		final ProductTaxGroup productTaxGroup = getProductTaxGroupMappingService().getProductTaxGroupMapping(applyDMF, applyMRDT,
				applyGST, applyPST);
		roomRateProduct.setEurope1PriceFactory_PTG(productTaxGroup);
	}

	protected UnitService getUnitService()
	{
		return unitService;
	}

	@Required
	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}

	protected ProductTaxGroupMappingService getProductTaxGroupMappingService()
	{
		return productTaxGroupMappingService;
	}

	@Required
	public void setProductTaxGroupMappingService(final ProductTaxGroupMappingService productTaxGroupMappingService)
	{
		this.productTaxGroupMappingService = productTaxGroupMappingService;
	}

	protected BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	@Required
	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	protected ExtraGuestOccupancyProductDataService getExtraGuestOccupancyProductDataService()
	{
		return extraGuestOccupancyProductDataService;
	}

	@Required
	public void setExtraGuestOccupancyProductDataService(
			final ExtraGuestOccupancyProductDataService extraGuestOccupancyProductDataService)
	{
		this.extraGuestOccupancyProductDataService = extraGuestOccupancyProductDataService;
	}
}
