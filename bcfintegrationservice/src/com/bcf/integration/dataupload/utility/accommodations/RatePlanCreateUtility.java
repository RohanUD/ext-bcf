/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationPolicyDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.RatePlanDataModel;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.core.model.accommodation.AccommodationCancellationPolicyModel;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;


public class RatePlanCreateUtility extends AbstractDataUploadUtility {
    private static final Logger LOG = Logger.getLogger(RatePlanCreateUtility.class);
    private static final String UNIT = "pieces";
    private RoomRatesCreateUtility roomRatesCreateUtility;
    private PriceRowService priceRowService;
    private UnitService unitService;
    private ProductTaxGroupMappingService productTaxGroupMappingService;
    private AccommodationCancellationPolicyDataUploadUtility accommodationCancellationPolicyDataUploadUtility;

    /**
     * Creates the new rate plan.
     *
     * @param accommodationOffering    the accommodation offering
     * @param accommodation            the accommodation
     * @param groupedRatePlanCodesData the list of rate plan by code Data
     * @return the rate plan model
     */

    public RatePlanModel createNewRatePlan(final AccommodationOfferingModel accommodationOffering,
                                           final AccommodationModel accommodation, final List<RatePlanDataModel> groupedRatePlanCodesData,
                                           final String ratePlanCode, final List<ItemModel> items) {
        final RatePlanModel ratePlan = getModelService().create(RatePlanModel.class);
        final RatePlanDataModel ratePlanData = groupedRatePlanCodesData.get(0);
        getLocaleList().forEach(locale -> ratePlan.setName(ratePlanData.getName(locale), locale));
        ratePlan.setCode(ratePlanCode);
        ratePlan.setCatalogVersion(accommodation.getCatalogVersion());
        final List<ProductModel> roomRates = uploadRoomRates(accommodationOffering, accommodation, groupedRatePlanCodesData,
                ratePlan, items);
        ratePlan.setProducts(roomRates);
        ratePlan.setName(ratePlanData.getName());
        ratePlan.setDiscount(ratePlanData.getDiscount());
        ratePlan.setDiscountType(ratePlanData.getDiscountType());
        ratePlan.setMarginRate(ratePlanData.getMarginRate());
        ratePlan.setAccommodationCancellationPolicy(
              getCancellationPolicy(ratePlanData.getAccommodationCancellationPolicyData(), ratePlan, items));
        getLocaleList().forEach(locale -> ratePlan.setTermsAndConditions(ratePlanData.getTermsAndConditions(locale), locale));
        items.add(ratePlan);
        return ratePlan;
    }

    /**
     * Update rate plan.
     *
     * @param accommodationOffering    the accommodation offering
     * @param accommodation            the accommodation
     * @param groupedRatePlanCodesData the list of rate plan by code Data
     * @return the rate plan model
     */

    public RatePlanModel updateRatePlan(final AccommodationOfferingModel accommodationOffering,
                                        final AccommodationModel accommodation, final List<RatePlanDataModel> groupedRatePlanCodesData,
                                        final RatePlanModel ratePlan, final List<ItemModel> items) {
        final List<ProductModel> roomRates = uploadRoomRates(accommodationOffering, accommodation, groupedRatePlanCodesData,
                ratePlan, items);
        ratePlan.setProducts(roomRates);
        final RatePlanDataModel ratePlanData = groupedRatePlanCodesData.get(0);
        ratePlan.setName(ratePlanData.getName());
        ratePlan.setDiscount(ratePlanData.getDiscount());
        ratePlan.setDiscountType(ratePlanData.getDiscountType());
        ratePlan.setMarginRate(ratePlanData.getMarginRate());
        ratePlan.setAccommodationCancellationPolicy(
              getCancellationPolicy(ratePlanData.getAccommodationCancellationPolicyData(), ratePlan, items));
        getLocaleList().forEach(locale -> ratePlan.setTermsAndConditions(ratePlanData.getTermsAndConditions(locale), locale));
        return ratePlan;
    }


    private Collection<AccommodationCancellationPolicyModel> getCancellationPolicy(
          final Collection<AccommodationCancellationPolicyDataModel> accommodationCancellationPolicyDatas,
          final RatePlanModel ratePlan, final List<ItemModel> items)
    {
        final List<AccommodationCancellationPolicyModel> accommodationCancellationPolicies = new ArrayList<>();

        for (final AccommodationCancellationPolicyDataModel accommodationCancellationPolicyData : accommodationCancellationPolicyDatas)
        {
            List<AccommodationCancellationFeesDataModel> cancellationFeesData = Collections.emptyList();
            if (CollectionUtils.isNotEmpty(accommodationCancellationPolicyData.getAccommodationCancellationFeesData())) {
                cancellationFeesData = new ArrayList<>(accommodationCancellationPolicyData.getAccommodationCancellationFeesData());
            }
            final AccommodationCancellationPolicyModel accommodationCancellationPolicyModel = getAccommodationCancellationPolicyDataUploadUtility()
                  .uploadAccCancellationPolicy(accommodationCancellationPolicyData, cancellationFeesData, ratePlan, items);
            accommodationCancellationPolicies.add(accommodationCancellationPolicyModel);
        }
        return accommodationCancellationPolicies;
    }


    /**
     * Upload room rates.
     *
     * @param accommodationOffering    the accommodation offering
     * @param accommodation            the accommodation
     * @param groupedRatePlanCodesData the list of rate plan by code Data
     * @param ratePlan                 the rate plan model
     * @return the list of product model
     */
    protected List<ProductModel> uploadRoomRates(final AccommodationOfferingModel accommodationOffering,
                                                 final AccommodationModel accommodation, final List<RatePlanDataModel> groupedRatePlanCodesData,
                                                 final RatePlanModel ratePlan, final List<ItemModel> items) {
        final boolean isAllRoomRateDataValid = groupedRatePlanCodesData.stream().allMatch(this::validateRoomRateData);
        if (!isAllRoomRateDataValid) {
            return Collections.emptyList();
        }

        final Map<String, List<RatePlanDataModel>> ratePlanDaysOfWeekDataMap = new HashMap<>();

        groupedRatePlanCodesData.forEach(ratePlanDataModel -> {
            final List<DayOfWeek> daysOfWeekList = Objects.isNull(ratePlanDataModel.getDaysOfWeek()) ? new ArrayList<>()
                    : new ArrayList<>(ratePlanDataModel.getDaysOfWeek());
            ratePlanDataModel.setDaysOfWeek(daysOfWeekList);
            Collections.sort(ratePlanDataModel.getDaysOfWeek(), Comparator.comparing(DayOfWeek::ordinal));

            final String daysCode = ratePlanDataModel.getDaysOfWeek().stream()
                    .map(dayOfWeek -> String.valueOf(dayOfWeek.getCode().charAt(0))).collect(Collectors.joining());


            if (ratePlanDaysOfWeekDataMap.containsKey(daysCode)) {
                ratePlanDaysOfWeekDataMap.get(daysCode).add(ratePlanDataModel);
            } else {
                final List<RatePlanDataModel> ratePlanDataModels = new ArrayList<>();
                ratePlanDataModels.add(ratePlanDataModel);
                ratePlanDaysOfWeekDataMap.put(daysCode, ratePlanDataModels);
            }
        });

        final RatePlanDataModel ratePlanData = groupedRatePlanCodesData.get(0);
        final List<ProductModel> roomRateProducts = Objects.isNull(ratePlan.getProducts()) ? new ArrayList<>()
                : new ArrayList<>(ratePlan.getProducts());
        for (final Map.Entry<String, List<RatePlanDataModel>> ratePlanDaysOfWeekDataEntry : ratePlanDaysOfWeekDataMap.entrySet()) {
            final List<RatePlanDataModel> groupedRatePlanDaysOfWeekDatas = ratePlanDaysOfWeekDataEntry.getValue();

            final String roomRateProductCode = ratePlanData.getCode();

            final ProductModel product;
            RoomRateProductModel roomRateProduct;
            try {
                product = getProductService().getProductForCode(ratePlan.getCatalogVersion(), roomRateProductCode);
                if (product instanceof RoomRateProductModel) {
                    roomRateProduct = (RoomRateProductModel) product;
                    getRoomRatesCreateUtility().updateRoomRates(roomRateProduct, groupedRatePlanDaysOfWeekDatas);
                } else {
                    throw new ModelNotFoundException("product is not instance of RoomRateProductModel");
                }
            } catch (final ModelNotFoundException | UnknownIdentifierException e) {
                roomRateProduct = getRoomRatesCreateUtility().createNewRoomRates(accommodationOffering, accommodation,
                        groupedRatePlanDaysOfWeekDatas, roomRateProductCode, items);
                roomRateProducts.add(roomRateProduct);
            }
            createAndSetPriceRow(roomRateProduct, ratePlanData, items);
            updateProductTaxGroup(roomRateProduct, ratePlanData);
            items.add(roomRateProduct);
        }

        return roomRateProducts;
    }

    /**
     * Creates the and set price row.
     *
     * @param roomRateProduct the room rate product
     * @param ratePlanData    the rate plan data
     */
    protected void createAndSetPriceRow(final RoomRateProductModel roomRateProduct, final RatePlanDataModel ratePlanData,
                                        final List<ItemModel> items) {
        PriceRowModel price = null;
        if (!getModelService().isNew(roomRateProduct)) {
            final Long minQtd = Long.valueOf(ratePlanData.getNumberOfNights());
            price = getPriceRowService().getPriceRow(roomRateProduct, minQtd);
        }
        if (price == null) {
            price = getModelService().create(PriceRowModel.class);

            price.setProduct(roomRateProduct);
            price.setCurrency(getCommonI18NService().getBaseCurrency());
            price.setMinqtd(Long.valueOf(ratePlanData.getNumberOfNights()));
            price.setNet(true);
            price.setUnit(getUnitService().getUnitForCode(UNIT));
            price.setCatalogVersion(roomRateProduct.getCatalogVersion());
        }
        price.setPrice(ratePlanData.getPrice());
        items.add(price);
    }

    /**
     * update tax row.
     *
     * @param roomRateProduct the room rate product
     * @param ratePlanData    the rate plan data
     */
    private void updateProductTaxGroup(final RoomRateProductModel roomRateProduct, final RatePlanDataModel ratePlanData) {
        final AccommodationOfferingDataModel accommodationOfferingDataModel = ratePlanData.getAccommodationOffering();
        final Boolean applyDMF = accommodationOfferingDataModel.isApplyDMF();
        final Boolean applyMRDT = accommodationOfferingDataModel.isApplyMRDT();
        final Boolean applyGST = accommodationOfferingDataModel.isApplyGST();
        final Boolean applyPST = accommodationOfferingDataModel.isApplyPST();
        final ProductTaxGroup productTaxGroup = getProductTaxGroupMappingService().getProductTaxGroupMapping(applyDMF, applyMRDT,
                applyGST, applyPST);
        roomRateProduct.setEurope1PriceFactory_PTG(productTaxGroup);
    }

    /**
     * Validate room rate data.
     *
     * @param ratePlanData the rate Plan data
     * @return true, if successful
     */
    protected boolean validateRoomRateData(final RatePlanDataModel ratePlanData) {
        if (Objects.isNull(ratePlanData.getRatePlanStartDate())) {
            LOG.error("startDate is empty for " + ratePlanData.getCode());
            return false;
        }
        if (Objects.isNull(ratePlanData.getRatePlanEndDate())) {
            LOG.error("endDate is empty for " + ratePlanData.getCode());
            return false;
        }
        if (ratePlanData.getRatePlanStartDate().compareTo(ratePlanData.getRatePlanEndDate()) > 0) {
            LOG.error("endDate is before startDate for " + ratePlanData.getCode());
            return false;
        }
        if (Objects.isNull(ratePlanData.getNumberOfNights())) {
            LOG.error("numberOfNights is empty for " + ratePlanData.getCode());
            return false;
        }
        if (Objects.isNull(ratePlanData.getPrice())) {
            LOG.error("price is empty for " + ratePlanData.getCode());
            return false;
        }
        if (Objects.nonNull(ratePlanData.getDiscount()) && Objects.isNull(ratePlanData.getDiscountType())) {
            LOG.error("discount type can not be empty if discount is provided for " + ratePlanData.getCode());
            return false;
        }
        if (Objects.isNull(ratePlanData.getMarginRate())) {
            LOG.error("marginRate is empty for " + ratePlanData.getCode());
            return false;
        }

        return true;
    }

    protected RoomRatesCreateUtility getRoomRatesCreateUtility() {
        return roomRatesCreateUtility;
    }

    @Required
    public void setRoomRatesCreateUtility(final RoomRatesCreateUtility roomRatesCreateUtility) {
        this.roomRatesCreateUtility = roomRatesCreateUtility;
    }

    @Override
    protected PriceRowService getPriceRowService() {
        return priceRowService;
    }

    @Override
    @Required
    public void setPriceRowService(final PriceRowService priceRowService) {
        this.priceRowService = priceRowService;
    }

    protected UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    protected ProductTaxGroupMappingService getProductTaxGroupMappingService() {
        return productTaxGroupMappingService;
    }

    @Required
    public void setProductTaxGroupMappingService(final ProductTaxGroupMappingService productTaxGroupMappingService) {
        this.productTaxGroupMappingService = productTaxGroupMappingService;
    }

    protected AccommodationCancellationPolicyDataUploadUtility getAccommodationCancellationPolicyDataUploadUtility() {
        return accommodationCancellationPolicyDataUploadUtility;
    }

    @Required
    public void setAccommodationCancellationPolicyDataUploadUtility(
          final AccommodationCancellationPolicyDataUploadUtility accommodationCancellationPolicyDataUploadUtility)
    {
        this.accommodationCancellationPolicyDataUploadUtility = accommodationCancellationPolicyDataUploadUtility;
    }
}
