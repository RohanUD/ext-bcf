/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.MinimumNightsStayRulesDataModel;
import com.bcf.core.accommodation.service.BcfAccommodationService;
import com.bcf.core.accommodation.service.MinimumNightsStayRulesService;
import com.bcf.core.model.accommodation.MinimumNightsStayRulesModel;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.service.accommodations.MinimumNightsStayRulesDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;


public class MinimumNightsStayRulesDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(MinimumNightsStayRulesDataUploadUtility.class);

	private MinimumNightsStayRulesDataService minimumNightsStayRulesDataService;
	private  MinimumNightsStayRulesService minimumNightsStayRulesService;
	private BcfAccommodationService bcfAccommodationService;
	private SetupSolrIndexerService setupSolrIndexerService;
	private AccommodationsDataUploadUtility accommodationsDataUploadUtility;

	public void uploadMinimumNightsStayRulesDatas()
	{
		final List<MinimumNightsStayRulesDataModel> minimumNightsStayRulesDatas =minimumNightsStayRulesDataService
				.getMinimumNightsStayRulesData(DataImportProcessStatus.PENDING);
		List<AccommodationModel> accommodationModels=null;
		if(CollectionUtils.isNotEmpty(minimumNightsStayRulesDatas)) {
			final Transaction tx = Transaction.current();
			try
			{
				tx.begin();
				 accommodationModels=uploadMinimumNightsStayRulesDatas(minimumNightsStayRulesDatas);

				LOG.info("Transaction Commit");
				tx.commit();

			if(CollectionUtils.isNotEmpty(accommodationModels)){

				final List<ItemModel> itemsToSynch = new ArrayList<>();
				itemsToSynch.addAll(accommodationModels);
				getAccommodationsDataUploadUtility().synchronizeProductCatalog(itemsToSynch);

				getSetupSolrIndexerService().executeSolrIndexerCronJob(BcfintegrationserviceConstants.BCVACATION_INDEX, false);
			}

			}
			catch (final ModelSavingException | IllegalArgumentException ex)
			{
				LOG.error("Transaction RollBack : " + ex);
				tx.rollback();
			}


		}
	}

	/**
	 * Upload accommodations stock level data.
	 *
	 * @return the List of String
	 */
	public List<AccommodationModel>  uploadMinimumNightsStayRulesDatas(final List<MinimumNightsStayRulesDataModel> minimumNightsStayRulesDatas)
	{
		LOG.debug("minimumNightsStayRulesDatas size : " + minimumNightsStayRulesDatas.size());
		final List<ItemModel> items = new ArrayList<>();
		minimumNightsStayRulesDatas
				.forEach(minimumNightsStayRulesData -> minimumNightsStayRulesData.setStatus(DataImportProcessStatus.PROCESSING));

		final List<MinimumNightsStayRulesDataModel> invalidMinimumNightsStayRulesDatas = minimumNightsStayRulesDatas.stream()
				.filter(minimumNightsStayRulesData -> !validateData(minimumNightsStayRulesData)).collect(Collectors.toList());
		LOG.debug("invalidMinimumNightsStayRulesDatas size : " + invalidMinimumNightsStayRulesDatas.size());

		 List<MinimumNightsStayRulesDataModel> validMinimumNightsStayRulesDatas = removeinvalidMinimumNightsStayRulesData(
				minimumNightsStayRulesDatas, invalidMinimumNightsStayRulesDatas);

		if(CollectionUtils.isNotEmpty(validMinimumNightsStayRulesDatas))
		{
			final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
			getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

			validMinimumNightsStayRulesDatas=validMinimumNightsStayRulesDatas.stream().filter(minimumNightsStayRulesDataModel->!minimumNightsStayRulesDataModel.getAccommodationData().getAccommodationOffering().isPartialSave()).collect(
					Collectors.toList());

			if(CollectionUtils.isNotEmpty(validMinimumNightsStayRulesDatas)){

				final Map<AccommodationDataModel, List<MinimumNightsStayRulesDataModel>> minimumNightsStayRulesDataMap = validMinimumNightsStayRulesDatas
						.stream().collect(Collectors.groupingBy(MinimumNightsStayRulesDataModel::getAccommodationData));

				processData(minimumNightsStayRulesDataMap,items);


				validMinimumNightsStayRulesDatas=validMinimumNightsStayRulesDatas.stream().filter(minimumNightsStayRulesDataModel->minimumNightsStayRulesDataModel.getAccommodationData().getAccommodationOffering().isPublish()).collect(
						Collectors.toList());

				if(CollectionUtils.isNotEmpty(validMinimumNightsStayRulesDatas)){

					List<AccommodationModel> accommodationModels=	validMinimumNightsStayRulesDatas.stream().map(minimumNightsStayRulesDataModel->getAccommodation(minimumNightsStayRulesDataModel.getAccommodationData())).collect(
							Collectors.toList());

					return accommodationModels;

				}

			}


		}

		return null;
	}

	public void processData( Map<AccommodationDataModel, List<MinimumNightsStayRulesDataModel>> minimumNightsStayRulesDataMap,List<ItemModel> items )
	{

		for (final Map.Entry<AccommodationDataModel, List<MinimumNightsStayRulesDataModel>> minimumNightsStayRulesDataMapEntry : minimumNightsStayRulesDataMap
				.entrySet())
		{
			final AccommodationDataModel accommodationData = minimumNightsStayRulesDataMapEntry.getKey();
			final List<MinimumNightsStayRulesDataModel> minimumNightsStayRulesDataModels = minimumNightsStayRulesDataMapEntry.getValue();
			LOG.debug("minimumNightsStayRulesDataModels size : " + minimumNightsStayRulesDataModels.size());
			accommodationData.setMinimumNightsStayRulesData(minimumNightsStayRulesDataModels);
			items.add(accommodationData);

			final AccommodationModel accommodationModel = getAccommodation(accommodationData);
			if (Objects.isNull(accommodationModel))
			{
				minimumNightsStayRulesDataModels.forEach(minimumNightsStayRulesDataModel -> minimumNightsStayRulesDataModel
						.setStatus(DataImportProcessStatus.FAILED));
				items.addAll(minimumNightsStayRulesDataModels);
				continue;
			}
			final List<MinimumNightsStayRulesModel> minimumNightsStayRulesModels = minimumNightsStayRulesDataModels.stream()
					.map(minimumNightsStayRulesDataModel -> getOrCreateMinimumNightsStayRules(accommodationModel,
							minimumNightsStayRulesDataModel, items))
					.collect(Collectors.toList());

			accommodationModel.setMinimumNightsStayRules(minimumNightsStayRulesModels);

			items.addAll(minimumNightsStayRulesModels);
			items.add(accommodationModel);

			minimumNightsStayRulesDataModels.forEach(minimumNightsStayRulesDataModel -> minimumNightsStayRulesDataModel
					.setStatus(DataImportProcessStatus.SUCCESS));
			items.addAll(minimumNightsStayRulesDataModels);



		}

		getModelService().saveAll(items);

	}

	public MinimumNightsStayRulesModel getOrCreateMinimumNightsStayRules(AccommodationModel accommodationModel ,
			final MinimumNightsStayRulesDataModel minimumNightsStayRulesDataModel,final List<ItemModel> items)
	{
		MinimumNightsStayRulesModel minimumNightsStayRulesModel = null;
		try
		{
				minimumNightsStayRulesModel=minimumNightsStayRulesService.getMinimumNightsStayRules(accommodationModel,minimumNightsStayRulesDataModel.getStartDate(),minimumNightsStayRulesDataModel.getEndDate());

			if(minimumNightsStayRulesModel==null || !checkSameDaysOfTheWeek(minimumNightsStayRulesDataModel.getDaysOfWeek(),minimumNightsStayRulesModel.getDaysOfWeek())){



				minimumNightsStayRulesModel = createMinimumNightsStayRules(accommodationModel, minimumNightsStayRulesDataModel);
			}

		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			minimumNightsStayRulesModel = createMinimumNightsStayRules(accommodationModel, minimumNightsStayRulesDataModel);

		}
		minimumNightsStayRulesModel.setMinimumNightsStay(minimumNightsStayRulesDataModel.getMinimumNightsStay());

		return minimumNightsStayRulesModel;
	}


	private boolean checkSameDaysOfTheWeek(List<DayOfWeek> daysOfTheWeek, List<DayOfWeek> existingDaysOfTheWeek){

		return existingDaysOfTheWeek != null && daysOfTheWeek != null && existingDaysOfTheWeek.containsAll(daysOfTheWeek)
				&& daysOfTheWeek.containsAll(existingDaysOfTheWeek);

	}



	/**
	 * Creates the extra guest occupancy product.
	 */
	protected MinimumNightsStayRulesModel createMinimumNightsStayRules(AccommodationModel accommodationModel ,
			final MinimumNightsStayRulesDataModel minimumNightsStayRulesDataModel)
	{
		final MinimumNightsStayRulesModel minimumNightsStayRulesModel = getModelService()
				.create(MinimumNightsStayRulesModel.class);
		minimumNightsStayRulesModel.setAccommodation(accommodationModel);
		minimumNightsStayRulesModel.setStartDate(minimumNightsStayRulesDataModel.getStartDate());
		minimumNightsStayRulesModel.setEndDate(minimumNightsStayRulesDataModel.getEndDate());
		minimumNightsStayRulesModel.setDaysOfWeek(minimumNightsStayRulesDataModel.getDaysOfWeek());
		return minimumNightsStayRulesModel;
	}


	protected AccommodationModel getAccommodation(final AccommodationDataModel accommodationDataModel)
	{
		AccommodationModel accommodation = null;
		try
		{
			accommodation = getBcfAccommodationService().getAccommodation(
					accommodationDataModel.getAccommodationOffering().getCode() + "_" + accommodationDataModel.getCode(),
					getCatalogVersionModel());
		}
		catch (final ModelNotFoundException ex)
		{
			return null;
		}

		return accommodation;
	}

	private List<MinimumNightsStayRulesDataModel> removeinvalidMinimumNightsStayRulesData(
			final List<MinimumNightsStayRulesDataModel> minimumNightsStayRulesDatas,
			final List<MinimumNightsStayRulesDataModel> invalidMinimumNightsStayRulesDatas)
	{
		invalidMinimumNightsStayRulesDatas
				.forEach(invalidMinimumNightsStayRulesData -> invalidMinimumNightsStayRulesData.setStatus(DataImportProcessStatus.FAILED));

		getModelService().saveAll(invalidMinimumNightsStayRulesDatas);

		final List<MinimumNightsStayRulesDataModel> validMinimumNightsStayRulesDatas = minimumNightsStayRulesDatas.stream()
				.filter(minimumNightsStayRulesData -> !invalidMinimumNightsStayRulesDatas.contains(minimumNightsStayRulesData))
				.collect(Collectors.toList());
		LOG.debug("validMinimumNightsStayRulesDatas size : " + validMinimumNightsStayRulesDatas.size());
		return validMinimumNightsStayRulesDatas;
	}

	/**
	 * Validate minimumNightsStayRulesDataModel.
	 *
	 * @param minimumNightsStayRulesDataModel
	 *           the minimumNightsStayRulesDataModel
	 * @return true, if successful
	 */
	protected boolean validateData(final MinimumNightsStayRulesDataModel minimumNightsStayRulesDataModel)
	{
		if (Objects.isNull(minimumNightsStayRulesDataModel.getAccommodationData()))
		{
			LOG.error("accommodationData is empty for minimumNightsStayRulesDataModel : Start Date "
					+ minimumNightsStayRulesDataModel.getStartDate() + "---End Date "
					+ minimumNightsStayRulesDataModel.getEndDate() + "----Minimum Night Stay " + minimumNightsStayRulesDataModel
					.getMinimumNightsStay());
			return false;
		}

		if (Objects.isNull(minimumNightsStayRulesDataModel.getStartDate()))
		{
			LOG.error("startDate is empty for minimumNightsStayRulesDataModel : End Date "
					+ minimumNightsStayRulesDataModel.getEndDate() + "----Minimum Night Stay " + minimumNightsStayRulesDataModel
					.getMinimumNightsStay()+ "----Minimum Night Stay accommodationData" + minimumNightsStayRulesDataModel
					.getAccommodationData());
			return false;
		}

		if (Objects.isNull(minimumNightsStayRulesDataModel.getEndDate()))
		{
			LOG.error("startDate is empty for minimumNightsStayRulesDataModel : Start Date "
					+ minimumNightsStayRulesDataModel.getStartDate() + "----Minimum Night Stay " + minimumNightsStayRulesDataModel
					.getMinimumNightsStay()+ "----Minimum Night Stay accommodationData" + minimumNightsStayRulesDataModel
					.getAccommodationData());
			return false;
		}

		return true;
	}

	public MinimumNightsStayRulesDataService getMinimumNightsStayRulesDataService()
	{
		return minimumNightsStayRulesDataService;
	}

	public void setMinimumNightsStayRulesDataService(
			final MinimumNightsStayRulesDataService minimumNightsStayRulesDataService)
	{
		this.minimumNightsStayRulesDataService = minimumNightsStayRulesDataService;
	}

	public MinimumNightsStayRulesService getMinimumNightsStayRulesService()
	{
		return minimumNightsStayRulesService;
	}

	public void setMinimumNightsStayRulesService(
			final MinimumNightsStayRulesService minimumNightsStayRulesService)
	{
		this.minimumNightsStayRulesService = minimumNightsStayRulesService;
	}

	public BcfAccommodationService getBcfAccommodationService()
	{
		return bcfAccommodationService;
	}

	public void setBcfAccommodationService(final BcfAccommodationService bcfAccommodationService)
	{
		this.bcfAccommodationService = bcfAccommodationService;
	}

	public SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	public AccommodationsDataUploadUtility getAccommodationsDataUploadUtility()
	{
		return accommodationsDataUploadUtility;
	}

	public void setAccommodationsDataUploadUtility(
			final AccommodationsDataUploadUtility accommodationsDataUploadUtility)
	{
		this.accommodationsDataUploadUtility = accommodationsDataUploadUtility;
	}
}
