/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.accommodations;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.tx.Transaction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationDataModel;
import com.bcf.bcfintegrationservice.model.utility.AccommodationStockLevelDataModel;
import com.bcf.core.enums.StockLevelType;
import com.bcf.integration.dataupload.service.accommodations.AccommodationStockLevelDataService;
import com.bcf.integration.dataupload.service.accommodations.UpdateStockLevelsToAccommodationOfferingService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;


public class AccommodationsStockLevelDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(AccommodationsStockLevelDataUploadUtility.class);

	private static final String START_DATE_MSG = "and startDate :";

	private AccommodationStockLevelDataService accommodationStockLevelDataService;
	private UpdateStockLevelsToAccommodationOfferingService updateStockLevelsToAccommodationOfferingService;

	public void uploadAccommodationStockLevelDatas()
	{
		final List<AccommodationStockLevelDataModel> accommodationStockLevelData = getAccommodationStockLevelDataService()
				.getAccommodationStockLevelData(DataImportProcessStatus.PENDING);
		final Transaction tx = Transaction.current();
		try
		{
			tx.begin();
			uploadAccommodationStockLevelData(accommodationStockLevelData);
			LOG.info("Transaction Commit");
			tx.commit();
		}
		catch (final ModelSavingException | IllegalArgumentException ex)
		{
			LOG.error("Transaction RollBack : " + ex);
			tx.rollback();
		}
	}

	/**
	 * Upload accommodations stock level data.
	 *
	 * @return the List of String
	 */
	public void uploadAccommodationStockLevelData(final List<AccommodationStockLevelDataModel> accommodationStockLevelData)
	{
		LOG.debug("accommodationStockLevelData size : " + accommodationStockLevelData.size());

		accommodationStockLevelData
				.forEach(validAccommodationStockLevel -> validAccommodationStockLevel.setStatus(DataImportProcessStatus.PROCESSING));

		final List<AccommodationStockLevelDataModel> invalidAccommodationStockLevelData = accommodationStockLevelData.stream()
				.filter(accommodationStockLevel -> !validateData(accommodationStockLevel)).collect(Collectors.toList());
		LOG.debug("invalidAccommodationStockLevelData size : " + invalidAccommodationStockLevelData.size());

		final List<AccommodationStockLevelDataModel> validAccommodationStockLevelData = removeinvalidAccommodationStockLevelData(
				accommodationStockLevelData, invalidAccommodationStockLevelData);

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		processData(validAccommodationStockLevelData);
	}

	private void processData(final List<AccommodationStockLevelDataModel> accommodationStockLevelDatas)
	{
		final Map<AccommodationDataModel, List<AccommodationStockLevelDataModel>> accommodationDataMap = accommodationStockLevelDatas
				.stream().collect(Collectors.groupingBy(AccommodationStockLevelDataModel::getAccommodationData));
		final Map<AccommodationOfferingModel, List<AccommodationStockLevelDataModel>> accommodationStockLevelDatasMap = new HashMap<>();
		for (final Map.Entry<AccommodationDataModel, List<AccommodationStockLevelDataModel>> accommodationDataEntry : accommodationDataMap
				.entrySet())
		{
			final AccommodationDataModel accommodationData = accommodationDataEntry.getKey();
			final List<AccommodationStockLevelDataModel> accommodationStockLevelDataEntries = accommodationDataEntry.getValue();
			LOG.debug("accommodationStockLevelDatas size : " + accommodationStockLevelDatas.size());
			final AccommodationOfferingModel accommodationOffering = findValidAccommodationOffering(accommodationData);

			if (Objects.isNull(accommodationOffering))
			{
				getValidAccommodationStockLevelData(accommodationStockLevelDatas, accommodationData);
				LOG.debug(
						"accommodationOffering is null and accommodationStockLevelDatas size : " + accommodationStockLevelDatas.size());
			}
			else
			{
				List<AccommodationStockLevelDataModel> validAccommodationStockLevelData = new ArrayList<>();
				if (accommodationStockLevelDatasMap.containsKey(accommodationOffering))
				{
					final List<AccommodationStockLevelDataModel> accommodationStockLevelDataList = accommodationStockLevelDatasMap
							.get(accommodationOffering);
					validAccommodationStockLevelData = Objects.isNull(accommodationStockLevelDataList) ? new ArrayList<>()
							: new ArrayList<>(accommodationStockLevelDataList);
				}

				LOG.debug("Adding to Map");
				LOG.debug("accommodationOfferingCode : " + accommodationOffering.getCode());

				validAccommodationStockLevelData.addAll(accommodationStockLevelDataEntries.stream().filter(
						accommodationStockLevelData -> !accommodationStockLevelData.getStatus().equals(DataImportProcessStatus.FAILED))
						.collect(Collectors.toList()));
				accommodationStockLevelDatasMap.put(accommodationOffering, validAccommodationStockLevelData);
				final List<AccommodationStockLevelDataModel> accommodationStockLevelDataModels = new ArrayList<>();
				accommodationStockLevelDataModels.addAll(accommodationStockLevelDataEntries);
				if (CollectionUtils.isNotEmpty(accommodationData.getAccommodationStockLevelDataList()))
				{
					accommodationStockLevelDataModels.addAll(accommodationData.getAccommodationStockLevelDataList().stream()
							.filter(accommodationStockLevelDataModel -> !isStockLevelDataModelAlreadyPresent(accommodationStockLevelDataModels, accommodationStockLevelDataModel))
							.collect(Collectors.toList()));
				}
				accommodationData.setAccommodationStockLevelDataList(accommodationStockLevelDataModels);
				getModelService().save(accommodationData);
			}
		}
		getUpdateStockLevelsToAccommodationOfferingService()
				.updateBcfStockLevelsToAccommodationOffering(accommodationStockLevelDatasMap);

		updateStatusOfAccommodationStockLevel(accommodationStockLevelDatas, DataImportProcessStatus.PROCESSING,
				DataImportProcessStatus.SUCCESS);
		getModelService().saveAll(accommodationStockLevelDatas);
	}

	private boolean isStockLevelDataModelAlreadyPresent(final List<AccommodationStockLevelDataModel> newAccommodationStockLevelDataModels,
			final AccommodationStockLevelDataModel existedStockLevelModel)
	{
		String dayOfWeekCodeForExistedStockLevelModel = existedStockLevelModel.getDaysOfWeek().stream()
				.map(w -> String.valueOf(w.ordinal())).sorted().collect(
						Collectors.joining(""));
		for (AccommodationStockLevelDataModel newStockLevelModel : newAccommodationStockLevelDataModels)
		{
			String daysOfWeekCode = getDaysOfWeekCode(newStockLevelModel);
			if (daysOfWeekCode.equals(dayOfWeekCodeForExistedStockLevelModel) && newStockLevelModel.getAccommodationData()
					.equals(existedStockLevelModel.getAccommodationData())
					&& isDatesEqual(newStockLevelModel, existedStockLevelModel))
			{
				return true;
			}
		}
		return false;
	}

	private boolean isDatesEqual(final AccommodationStockLevelDataModel newStockLevelModel,
			final AccommodationStockLevelDataModel existedStockLevelModel)
	{
		return newStockLevelModel.getStartDate().equals(existedStockLevelModel.getStartDate()) && newStockLevelModel.getEndDate()
				.equals(existedStockLevelModel.getEndDate());
	}

	private String getDaysOfWeekCode(final AccommodationStockLevelDataModel model)
	{
		String daysOfWeekCode;
		if (StringUtils.isBlank(model.getDaysOfWeekStr()))
		{
			daysOfWeekCode = Arrays.asList(DayOfWeek.values()).stream().map(dayOfWeek -> String.valueOf(dayOfWeek.ordinal()))
					.sorted().collect(
							Collectors.joining(""));
		}
		else
		{
			daysOfWeekCode = Arrays.asList(model.getDaysOfWeekStr().split(",")).stream().map(dayOfWeekValue -> DayOfWeek
					.valueOf(StringUtils.trim(dayOfWeekValue))).map(dayOfWeek -> String.valueOf(dayOfWeek.ordinal())).sorted().collect(
					Collectors.joining(""));
		}
		return daysOfWeekCode;
	}

	private AccommodationOfferingModel findValidAccommodationOffering(final AccommodationDataModel accommodationData)
	{
		final String accommodationOfferingCode = accommodationData.getAccommodationOffering().getCode();
		LOG.debug("accommodationOfferingCode : " + accommodationOfferingCode);
		final AccommodationOfferingModel accommodationOffering;
		try
		{
			accommodationOffering = getAccommodationOfferingService().getAccommodationOffering(accommodationOfferingCode);
			LOG.debug("AccommodationOffering found for accommodationCode : " + accommodationData.getCode()
					+ " and accommodationOfferingCode : " + accommodationOfferingCode);
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error("AccommodationOffering not found for accommodationCode : " + accommodationData.getCode()
					+ " and accommodationOfferingCode : " + accommodationOfferingCode);
			return null;
		}

		if (!accommodationData.getStockLevelType().equals(StockLevelType.STANDARD))
		{
			LOG.error("accommodation StockLevelType is not STANDARD for "+accommodationData.getCode());
			return null;
		}
		return accommodationOffering;
	}

	private List<AccommodationStockLevelDataModel> getValidAccommodationStockLevelData(
			List<AccommodationStockLevelDataModel> accommodationStockLevelDatas, final AccommodationDataModel accommodationData)
	{
		final List<AccommodationStockLevelDataModel> invalidAccommodationStockLevelData = accommodationStockLevelDatas.stream()
				.filter(accommodationStockLevel -> accommodationStockLevel.getAccommodationData().equals(accommodationData))
				.collect(Collectors.toList());
		LOG.debug("invalidAccommodationStockLevelData size : " + invalidAccommodationStockLevelData.size());
		final List<AccommodationStockLevelDataModel> validAccommodationStockLevelData = removeinvalidAccommodationStockLevelData(
				accommodationStockLevelDatas, invalidAccommodationStockLevelData);
		accommodationStockLevelDatas = validAccommodationStockLevelData;
		LOG.debug("accommodationStockLevelDatas size : " + accommodationStockLevelDatas.size());
		return accommodationStockLevelDatas;
	}

	private List<AccommodationStockLevelDataModel> removeinvalidAccommodationStockLevelData(
			final List<AccommodationStockLevelDataModel> accommodationStockLevelData,
			final List<AccommodationStockLevelDataModel> invalidAccommodationStockLevelData)
	{
		invalidAccommodationStockLevelData
				.forEach(invalidAccommodationStockLevel -> invalidAccommodationStockLevel.setStatus(DataImportProcessStatus.FAILED));
		final List<AccommodationStockLevelDataModel> validAccommodationStockLevelData = accommodationStockLevelData.stream()
				.filter(validAccommodationStockLevel -> !invalidAccommodationStockLevelData.contains(validAccommodationStockLevel))
				.collect(Collectors.toList());
		LOG.debug("validAccommodationStockLevelData size : " + validAccommodationStockLevelData.size());
		return validAccommodationStockLevelData;
	}

	/**
	 * Validate accommodations stock level data.
	 *
	 * @param accommodationStockLevelData
	 *           the accommodation stock level data
	 * @return true, if successful
	 */
	protected boolean validateData(final AccommodationStockLevelDataModel accommodationStockLevelData)
	{
		if (Objects.isNull(accommodationStockLevelData.getAccommodationData()))
		{
			LOG.error("accommodationData is empty for accommodationData : Start Date "
					+ accommodationStockLevelData.getStartDate() + "---End Date "
					+ accommodationStockLevelData.getEndDate() + "----Stock Level " + accommodationStockLevelData.getStockLevel());
			return false;
		}
		if (Objects.isNull(accommodationStockLevelData.getStockLevel()))
		{
			LOG.error("stockLevel is empty for accommodationCode : " + accommodationStockLevelData.getAccommodationData().getCode()
					+ START_DATE_MSG + accommodationStockLevelData.getStartDate());
			return false;
		}
		if (Objects.isNull(accommodationStockLevelData.getStartDate()))
		{
			LOG.error("startDate is empty for accommodationCode : " + accommodationStockLevelData.getAccommodationData().getCode()
					+ START_DATE_MSG + accommodationStockLevelData.getStartDate());
			return false;
		}

		return true;
	}

	private void updateStatusOfAccommodationStockLevel(
			final List<AccommodationStockLevelDataModel> accommodationStockLevelDataList, final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		accommodationStockLevelDataList.stream()
				.filter(accommodationStockLevelData -> fromStatus.equals(accommodationStockLevelData.getStatus()))
				.forEach(stockLevelData -> stockLevelData.setStatus(toStatus));
	}

	protected AccommodationStockLevelDataService getAccommodationStockLevelDataService()
	{
		return accommodationStockLevelDataService;
	}

	@Required
	public void setAccommodationStockLevelDataService(final AccommodationStockLevelDataService accommodationStockLevelDataService)
	{
		this.accommodationStockLevelDataService = accommodationStockLevelDataService;
	}

	protected UpdateStockLevelsToAccommodationOfferingService getUpdateStockLevelsToAccommodationOfferingService()
	{
		return updateStockLevelsToAccommodationOfferingService;
	}

	@Required
	public void setUpdateStockLevelsToAccommodationOfferingService(
			final UpdateStockLevelsToAccommodationOfferingService updateStockLevelsToAccommodationOfferingService)
	{
		this.updateStockLevelsToAccommodationOfferingService = updateStockLevelsToAccommodationOfferingService;
	}
}
