/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.deals.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.integration.dataupload.service.deals.DealsBundleDataService;


public class ValidateDealBundleDataSeoUrlInterceptor implements ValidateInterceptor<DealsBundleDataModel>
{

	private DealsBundleDataService dealsBundleDataService;


	@Override
	public void onValidate(final DealsBundleDataModel dealsBundleDataModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (Objects.nonNull(dealsBundleDataModel.getSeoUrl()))
		{
			final DealsBundleDataModel dealsBundleBySeoUrl = getDealsBundleDataService()
					.getDealsBundleDataForSeoUrl(dealsBundleDataModel.getSeoUrl(), dealsBundleDataModel.getCatalogVersion());
			if (Objects.nonNull(dealsBundleBySeoUrl) && !(dealsBundleDataModel.getPk().equals(dealsBundleBySeoUrl.getPk())))
			{
				throw new InterceptorException("SEO URL is not Unique");
			}
		}
	}

	protected DealsBundleDataService getDealsBundleDataService()
	{
		return dealsBundleDataService;
	}

	@Required
	public void setDealsBundleDataService(final DealsBundleDataService dealsBundleDataService)
	{
		this.dealsBundleDataService = dealsBundleDataService;
	}

}
