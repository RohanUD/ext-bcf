/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.dataupload.utility.deals;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.travelservices.enums.BundleType;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.deal.AccommodationBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.DealBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.RouteBundleTemplateModel;
import de.hybris.platform.travelservices.model.deal.TransportBundleTemplateModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.BundleTemplateTransportOfferingMappingModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.CabinClassService;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.DealsBundleDataModel;
import com.bcf.core.accommodation.service.BcfGuestCountService;
import com.bcf.core.accommodation.service.BcfTravelBundleTemplateService;
import com.bcf.core.enums.FareProductType;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.integration.dataupload.service.deals.DealsBundleDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.enums.DataImportProcessStatus;
import com.bcf.integration.url.DefaultDealBundleDataModelUrlResolver;
import com.google.common.collect.Sets;


public class DealsBundleDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger log = Logger.getLogger(DealsBundleDataUploadUtility.class);

	private static final String DEAL_VERSION = "1.0";
	private static final String TRANSPORT = "Transport";
	private static final String OUTBOUND = "Outbound";
	private static final String INBOUND = "Inbound";
	private static final String ROUTE = "Route";
	private static final String NOT_FOUND = " not found.";
	private static final String ACCOMMODATION = "Accommodation";
	private static final String CABIN_CODE = "M";
	private static final String TRAVEL_ROUTE_NOT_FOUND = "deal.bundle.travel.route.not.found";
	private static final String FARE_PRODUCT_NOT_FOUND = "deal.bundle.fare.product.not.found";
	private static final String ACCOMMODATION_SELECTION_CRITERIA_NOT_EXISTS = "deal.bundle.accommodation.selection.criteria.not.exists";
	private static final String BTTOM = "BTTOM";
	private static final String BCFERRIES_DEAL_INDEX = "bcferriesDealIndex";
	private static final String ACTIVITY = "Activity";
	private static final String PRODUCT = "Product";

	private DealsBundleDataService dealsBundleDataService;
	private BcfGuestCountService bcfGuestCountService;
	private TravelRouteService travelRouteService;
	private CabinClassService cabinClassService;
	private BcfTravelBundleTemplateService bcfTravelBundleTemplateService;
	private PropertySourceFacade propertySourceFacade;
	private SetupSolrIndexerService setupSolrIndexerService;
	private DefaultDealBundleDataModelUrlResolver dealBundleDataModelUrlResolver;

	/**
	 * Upload deals bundle data.
	 *
	 * @return the list
	 */
	public List<String> uploadDealsBundleData()
	{
		final List<ItemModel> items = new ArrayList<>();
		final List<DealsBundleDataModel> dealsBundleModelList = getDealsBundleDataService()
				.getDealsBundleData(DataImportProcessStatus.PENDING);

		if (CollectionUtils.isEmpty(dealsBundleModelList))
		{
			return Collections.emptyList();
		}

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		for (final DealsBundleDataModel dealBundleModel : dealsBundleModelList)
		{
			uploadDealBundleData(dealBundleModel, catalogVersion, items);
		}

		getModelService().saveAll();

		synchronizeProductCatalog(items);
		indexDeals();

		dealsBundleModelList.forEach(dealsBundleModel -> dealsBundleModel.setStatus(DataImportProcessStatus.SUCCESS));
		getModelService().saveAll(dealsBundleModelList);

		return Collections.emptyList();
	}

	/**
	 * Upload deal bundle data.
	 *
	 * @param dealBundleModel the deal bundle data
	 * @param catalogVersion  the catalog version
	 * @return the boolean
	 */
	public boolean uploadDealBundleData(final DealsBundleDataModel dealBundleModel, final CatalogVersionModel catalogVersion,
			final List<ItemModel> items)
	{
		dealBundleModel.setStatus(DataImportProcessStatus.PROCESSING);
		createAndSetSeoUrlIfEmpty(dealBundleModel);
		/* Deal Bundle */
		final DealBundleTemplateModel dealBundleTemplate = getDealBundleTemplate(dealBundleModel, catalogVersion);
		if (Objects.isNull(dealBundleTemplate))
		{
			return false;
		}

		/* Transport Bundle */
		final TransportBundleTemplateModel transportBundleTemplate = getTransportBundleTemplate(dealBundleModel, catalogVersion,
				dealBundleTemplate);

		/* Fare Product */
		final BundleType bundleType = dealBundleModel.getBundleType();
		final BundleTemplateModel bundleTemplateWithType = getBcfTravelBundleTemplateService().getBundleTemplate(bundleType,
				FareProductModel._TYPECODE, catalogVersion);
		if (Objects.isNull(bundleTemplateWithType) || CollectionUtils.isEmpty(bundleTemplateWithType.getProducts()))
		{
			final Object[] messageParams =
					{ bundleType };

			final String message = getPropertySourceFacade().getPropertySourceValueMessage(FARE_PRODUCT_NOT_FOUND, messageParams);
			log.error(message);
			dealBundleModel.setStatus(DataImportProcessStatus.FAILED);
			dealBundleModel.setStatusDescription(message);
			return false;
		}
		List<FareProductModel> fareProducts = bundleTemplateWithType.getProducts().stream()
				.filter(product -> StringUtils.equals(FareProductModel._TYPECODE, product.getItemtype()))
				.map(product -> (FareProductModel) product).collect(
						Collectors.toList());

		if (Objects.isNull(dealBundleModel.getVehicleType()))
		{
			fareProducts = fareProducts.stream()
					.filter(fareProduct -> FareProductType.PASSENGER.equals(fareProduct.getFareProductType())).collect(
							Collectors.toList());
		}

		/* Ancillary Products */
		final List<ProductModel> ancillaryProducts = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(dealBundleModel.getAncillaryProducts()))
		{
			ancillaryProducts.addAll(dealBundleModel.getAncillaryProducts());
			ancillaryProducts.stream().forEach(ancillaryProduct -> items.add(ancillaryProduct));
		}

		/* Route Bundle */
		final TravelRouteModel outboundTravelRoute = dealBundleModel.getTravelRoute();

		final String outBoundRouteBundleTemplateName = OUTBOUND + SPACE + ROUTE + SPACE
				+ dealBundleModel.getBundleName(Locale.ENGLISH);
		final RouteBundleTemplateModel outBoundRouteBundleTemplate = getRouteBundleTemplate(outBoundRouteBundleTemplateName,
				outboundTravelRoute, bundleType, 0, catalogVersion, dealBundleModel.getBundleTemplateStatus(),
				transportBundleTemplate);
		outBoundRouteBundleTemplate.setFareProducts(Sets.newHashSet(fareProducts));
		outBoundRouteBundleTemplate.setProducts(ancillaryProducts);
		if (Objects.nonNull(dealBundleModel.getVehicleType()))
		{
			outBoundRouteBundleTemplate.setVehicleType(dealBundleModel.getVehicleType());
		}
		items.add(bundleTemplateWithType);
		fareProducts.stream().forEach(fareProduct -> items.add(fareProduct));
		items.add(outBoundRouteBundleTemplate);
		final List<TravelRouteModel> travelRoutes = getTravelRouteService()
				.getTravelRoutes(outboundTravelRoute.getDestination().getCode(), outboundTravelRoute.getOrigin().getCode());
		if (CollectionUtils.isEmpty(travelRoutes))
		{
			final Object[] messageParams =
					{ outboundTravelRoute.getOrigin().getCode(), outboundTravelRoute.getDestination().getCode() };
			final String message = getPropertySourceFacade().getPropertySourceValueMessage(TRAVEL_ROUTE_NOT_FOUND, messageParams);
			log.error(message);
			dealBundleModel.setStatus(DataImportProcessStatus.FAILED);
			dealBundleModel.setStatusDescription(message);
			return false;
		}


		final TravelRouteModel inboundTravelRoute = travelRoutes.stream().findFirst().orElse(null);

		final String inBoundRouteBundleTemplateName = INBOUND + SPACE + ROUTE + SPACE
				+ dealBundleModel.getBundleName(Locale.ENGLISH);
		final RouteBundleTemplateModel inBoundRouteBundleTemplate = getRouteBundleTemplate(inBoundRouteBundleTemplateName,
				inboundTravelRoute, bundleType, 1, catalogVersion, dealBundleModel.getBundleTemplateStatus(),
				transportBundleTemplate);
		inBoundRouteBundleTemplate.setFareProducts(Sets.newHashSet(fareProducts));
		inBoundRouteBundleTemplate.setProducts(ancillaryProducts);

		if (Objects.nonNull(dealBundleModel.getVehicleType()))
		{
			inBoundRouteBundleTemplate.setVehicleType(dealBundleModel.getVehicleType());
		}

		if (CollectionUtils.isNotEmpty(dealBundleModel.getTransportOfferings()))
		{
			createOrUpdateBundleTemplateTransportOfferingMapping(dealBundleModel.getTransportOfferings(),
					outBoundRouteBundleTemplate, inBoundRouteBundleTemplate, catalogVersion, items);
		}
		items.add(transportBundleTemplate);
		items.add(inBoundRouteBundleTemplate);
		/* Accommodation Bundle */
		final AccommodationBundleTemplateModel accommodationBundleTemplate = getAccommodationBundleTemplate(dealBundleModel,
				catalogVersion, dealBundleTemplate, items);
		if (Objects.isNull(accommodationBundleTemplate))
		{
			final String message = getPropertySourceFacade().getPropertySourceValue(ACCOMMODATION_SELECTION_CRITERIA_NOT_EXISTS);
			log.error(message);
			dealBundleModel.setStatus(DataImportProcessStatus.FAILED);
			dealBundleModel.setStatusDescription(message);
			return false;
		}

		/* Activity Bundle */
		if (CollectionUtils.isNotEmpty(dealBundleModel.getActivityProducts()))
		{
			getActivityBundleTemplate(dealBundleModel, catalogVersion, dealBundleTemplate, items);
		}
		dealBundleModel.setStatus(DataImportProcessStatus.PROCESSED);
		items.add(dealBundleTemplate);
		items.add(dealBundleModel);
		return true;
	}

	/**
	 * Gets the route bundle template.
	 *
	 * @param routeBundleTemplateName    the route bundle template name
	 * @param travelRoute                the travel route
	 * @param bundleType
	 * @param originDestinationRefNumber
	 * @param catalogVersion             the catalog version
	 * @param bundleTemplateStatus
	 * @param transportBundleTemplate
	 * @return the route bundle template
	 */
	protected RouteBundleTemplateModel getRouteBundleTemplate(final String routeBundleTemplateName,
			final TravelRouteModel travelRoute, final BundleType bundleType, final int originDestinationRefNumber,
			final CatalogVersionModel catalogVersion, final BundleTemplateStatusModel bundleTemplateStatus,
			final TransportBundleTemplateModel transportBundleTemplate)
	{
		final String routeBundleTemplateId = routeBundleTemplateName.replaceAll(SPACE, StringUtils.EMPTY);

		RouteBundleTemplateModel routeBundleTemplate;
		try
		{
			final BundleTemplateModel bundleTemplateModel = getBcfTravelBundleTemplateService()
					.getBundleTemplateById(routeBundleTemplateId, catalogVersion);
			if (bundleTemplateModel instanceof RouteBundleTemplateModel)
			{
				routeBundleTemplate = (RouteBundleTemplateModel) bundleTemplateModel;
				routeBundleTemplate.setVersion(getBundleIncrementedVersion(routeBundleTemplate.getVersion()));
			}
			else
			{
				throw new ModelNotFoundException("routeBundleTemplate " + routeBundleTemplateId + NOT_FOUND);
			}
		}
		catch (final ModelNotFoundException e)
		{
			routeBundleTemplate = getModelService().create(RouteBundleTemplateModel.class);
			routeBundleTemplate.setVersion(DEAL_VERSION);
			routeBundleTemplate.setId(routeBundleTemplateId);
			routeBundleTemplate.setParentTemplate(transportBundleTemplate);
		}
		routeBundleTemplate.setTravelRoute(travelRoute);
		routeBundleTemplate.setType(bundleType);
		routeBundleTemplate.setName(routeBundleTemplateName);
		routeBundleTemplate.setCatalogVersion(catalogVersion);
		routeBundleTemplate.setStatus(bundleTemplateStatus);
		routeBundleTemplate.setOriginDestinationRefNumber(originDestinationRefNumber);
		routeBundleTemplate.setCabinClass(getCabinClassService().getCabinClass(CABIN_CODE));
		return routeBundleTemplate;
	}

	/**
	 * Gets the transport bundle template.
	 *
	 * @param dealBundleData     the deal bundle data
	 * @param catalogVersion
	 * @param dealBundleTemplate
	 * @return the transport bundle template
	 */
	protected TransportBundleTemplateModel getTransportBundleTemplate(final DealsBundleDataModel dealBundleData,
			final CatalogVersionModel catalogVersion, final DealBundleTemplateModel dealBundleTemplate)
	{
		final String transportBundleTemplateName = TRANSPORT + SPACE + dealBundleData.getBundleName(Locale.ENGLISH);
		final String transportBundleTemplateId = transportBundleTemplateName.replaceAll(SPACE, StringUtils.EMPTY);

		TransportBundleTemplateModel transportBundleTemplate;
		try
		{
			final BundleTemplateModel bundleTemplateModel = getBcfTravelBundleTemplateService()
					.getBundleTemplateById(transportBundleTemplateId, catalogVersion);
			if (bundleTemplateModel instanceof TransportBundleTemplateModel)
			{
				transportBundleTemplate = (TransportBundleTemplateModel) bundleTemplateModel;
				transportBundleTemplate.setVersion(getBundleIncrementedVersion(transportBundleTemplate.getVersion()));
			}
			else
			{
				throw new ModelNotFoundException("transportBundleTemplate " + transportBundleTemplateId + NOT_FOUND);
			}
		}
		catch (final ModelNotFoundException e)
		{
			transportBundleTemplate = getModelService().create(TransportBundleTemplateModel.class);
			transportBundleTemplate.setVersion(DEAL_VERSION);
			transportBundleTemplate.setId(transportBundleTemplateId);
			transportBundleTemplate.setParentTemplate(dealBundleTemplate);
		}
		transportBundleTemplate.setName(transportBundleTemplateName);
		transportBundleTemplate.setStatus(dealBundleData.getBundleTemplateStatus());
		transportBundleTemplate.setCatalogVersion(catalogVersion);

		return transportBundleTemplate;
	}

	/**
	 * Gets the accommodation bundle template.
	 *
	 * @param dealBundleData     the deal bundle data
	 * @param catalogVersion     the catalog version
	 * @param dealBundleTemplate
	 * @return the accommodation bundle template
	 */
	protected AccommodationBundleTemplateModel getAccommodationBundleTemplate(final DealsBundleDataModel dealBundleData,
			final CatalogVersionModel catalogVersion, final DealBundleTemplateModel dealBundleTemplate, final List<ItemModel> items)
	{
		final String accommodationBundleTemplateName = ACCOMMODATION + SPACE + dealBundleData.getBundleName(Locale.ENGLISH);
		final String accommodationBundleTemplateId = accommodationBundleTemplateName.replaceAll(SPACE, StringUtils.EMPTY);

		AccommodationBundleTemplateModel accommodationBundleTemplate;
		try
		{
			final BundleTemplateModel bundleTemplateModel = getBcfTravelBundleTemplateService()
					.getBundleTemplateById(accommodationBundleTemplateId, catalogVersion);
			if (bundleTemplateModel instanceof AccommodationBundleTemplateModel)
			{
				accommodationBundleTemplate = (AccommodationBundleTemplateModel) bundleTemplateModel;
				accommodationBundleTemplate.setVersion(getBundleIncrementedVersion(accommodationBundleTemplate.getVersion()));
			}
			else
			{
				throw new ModelNotFoundException("accommodationBundleTemplate " + accommodationBundleTemplateId + NOT_FOUND);
			}
			items.add(bundleTemplateModel);
		}
		catch (final ModelNotFoundException e)
		{
			accommodationBundleTemplate = getModelService().create(AccommodationBundleTemplateModel.class);
			accommodationBundleTemplate.setVersion(DEAL_VERSION);
			accommodationBundleTemplate.setId(accommodationBundleTemplateId);
			accommodationBundleTemplate.setParentTemplate(dealBundleTemplate);
		}

		accommodationBundleTemplate.setStatus(dealBundleData.getBundleTemplateStatus());
		accommodationBundleTemplate.setName(accommodationBundleTemplateName);
		accommodationBundleTemplate.setCatalogVersion(catalogVersion);
		accommodationBundleTemplate.setAccommodationOffering(dealBundleData.getAccommodationOffering());
		accommodationBundleTemplate.setAccommodation(dealBundleData.getAccommodation());

		final RatePlanModel ratePlan = dealBundleData.getRatePlan();
		accommodationBundleTemplate.setRatePlan(ratePlan);

		if (CollectionUtils.isEmpty(ratePlan.getProducts()))
		{
			log.error("No roomrate product is configured for rateplan " + dealBundleData.getRatePlan() + " and roomType "
					+ dealBundleData.getAccommodation().getRoomType());
			return null;
		}

		final RoomRateProductModel roomRateProduct = (RoomRateProductModel) ratePlan.getProducts().stream()
				.filter(product -> StringUtils.equals(RoomRateProductModel._TYPECODE, product.getItemtype())).findFirst().get();
		accommodationBundleTemplate.setRoomRateProduct(roomRateProduct);
		items.add(ratePlan);
		items.add(roomRateProduct);

		/* Extra Products */
		final List<ProductModel> extraProducts = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(dealBundleData.getExtraProducts()))
		{
			extraProducts.addAll(dealBundleData.getExtraProducts());
		}
		accommodationBundleTemplate.setProducts(extraProducts);
		extraProducts.stream().forEach(extraProduct -> items.add(extraProduct));
		items.add(accommodationBundleTemplate);
		return accommodationBundleTemplate;
	}

	/**
	 * Gets the deal bundle template.
	 *
	 * @param catalogVersion
	 * @param dealBundleData the deal bundle data
	 * @return the deal bundle template
	 */
	protected DealBundleTemplateModel getDealBundleTemplate(final DealsBundleDataModel dealBundleData,
			final CatalogVersionModel catalogVersion)
	{
		if (!validateDealsBundleData(dealBundleData))
		{
			return null;
		}

		final String dealBundleTemplateId = dealBundleData.getBundleId();
		DealBundleTemplateModel dealBundleTemplate;
		try
		{
			final BundleTemplateModel bundleTemplateModel = getBcfTravelBundleTemplateService()
					.getBundleTemplateById(dealBundleTemplateId, catalogVersion);
			if (bundleTemplateModel instanceof DealBundleTemplateModel)
			{
				dealBundleTemplate = (DealBundleTemplateModel) bundleTemplateModel;
				dealBundleTemplate.setVersion(getBundleIncrementedVersion(dealBundleTemplate.getVersion()));
				appendStatusDescription(dealBundleData,
						"Updating dealBundleTemplate with version " + dealBundleTemplate.getVersion());
			}
			else
			{
				throw new ModelNotFoundException("dealBundleTemplate " + dealBundleTemplateId + NOT_FOUND);
			}
		}
		catch (final ModelNotFoundException e)
		{
			dealBundleTemplate = getModelService().create(DealBundleTemplateModel.class);
			dealBundleTemplate.setVersion(DEAL_VERSION);
			dealBundleTemplate.setId(dealBundleTemplateId);
			appendStatusDescription(dealBundleData, "Creating new dealBundleTemplate.");
		}
		dealBundleTemplate.setName(dealBundleData.getBundleName(Locale.ENGLISH));
		dealBundleTemplate.setDescription(dealBundleData.getBundleDescription());
		dealBundleTemplate.setCatalogVersion(catalogVersion);
		dealBundleTemplate.setStatus(dealBundleData.getBundleTemplateStatus());
		dealBundleTemplate.setDateRanges(dealBundleData.getDateRanges());
		dealBundleTemplate.setDealTypes(dealBundleData.getDealTypes());
		dealBundleTemplate.setIsMultiCityDeal(false);
		dealBundleTemplate.setSoldIndividually(dealBundleData.getSoldIndividually());
		dealBundleTemplate.setStartingDatePattern(dealBundleData.getStartingDatePattern());
		dealBundleTemplate.setLength(dealBundleData.getLength());
		dealBundleTemplate.setGuestCounts(dealBundleData.getGuestCounts());
		dealBundleTemplate.setFromPrice(dealBundleData.getFromPrice());

		dealBundleTemplate.setSeoUrl(dealBundleData.getSeoUrl());
		dealBundleTemplate.setSoldFrom(Objects.isNull(dealBundleData.getSoldFrom()) ?
				dealBundleData.getSoldFrom() :
				DateUtils.truncate(dealBundleData.getSoldFrom(), Calendar.DATE));
		dealBundleTemplate.setSoldTo(Objects.isNull(dealBundleData.getSoldTo()) ?
				dealBundleData.getSoldTo() :
				DateUtils.truncate(dealBundleData.getSoldTo(), Calendar.DATE));

		setBundleNameLocales(dealBundleTemplate, dealBundleData);
		setBundleDescription(dealBundleTemplate, dealBundleData);

		return dealBundleTemplate;
	}

	private void createAndSetSeoUrlIfEmpty(final DealsBundleDataModel dealsBundleData)
	{
		if (StringUtils.isEmpty(dealsBundleData.getSeoUrl()))
		{
			dealsBundleData.setSeoUrl(getDealBundleDataModelUrlResolver().resolve(dealsBundleData));
		}
	}

	/**
	 * Sets the bundle name locales.
	 *
	 * @param dealBundleTemplate the deal bundle template
	 * @param dealBundleData     the deal bundle data
	 */
	protected void setBundleNameLocales(final DealBundleTemplateModel dealBundleTemplate,
			final DealsBundleDataModel dealBundleData)
	{
		getLocaleList().forEach(locale -> dealBundleTemplate.setName(dealBundleData.getBundleName(locale), locale));
	}

	/**
	 * Sets the bundle description.
	 *
	 * @param dealBundleTemplate the deal bundle template
	 * @param dealBundleData     the deal bundle data
	 */
	protected void setBundleDescription(final DealBundleTemplateModel dealBundleTemplate,
			final DealsBundleDataModel dealBundleData)
	{
		getLocaleList().forEach(locale -> dealBundleTemplate.setDescription(dealBundleData.getBundleDescription(locale), locale));
	}

	/**
	 * Creates the or update bundle template transport offering mapping.
	 *
	 * @param transportOfferings          the transport offerings
	 * @param outBoundRouteBundleTemplate the out bound route bundle template
	 * @param inBoundRouteBundleTemplate  the in bound route bundle template
	 * @param catalogVersion              the catalog version
	 */
	protected void createOrUpdateBundleTemplateTransportOfferingMapping(
			final Collection<TransportOfferingModel> transportOfferings, final RouteBundleTemplateModel outBoundRouteBundleTemplate,
			final RouteBundleTemplateModel inBoundRouteBundleTemplate, final CatalogVersionModel catalogVersion,
			final List<ItemModel> items)
	{
		//Out Bound TransportOffering mapping
		final TransportOfferingModel outBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute()
						.contains(outBoundRouteBundleTemplate.getTravelRoute()))
				.findFirst().orElse(null);

		final String outBoundMappingCode = outBoundRouteBundleTemplate.getId() + UNDERSCORE + BTTOM;
		BundleTemplateTransportOfferingMappingModel outBundleTemplateTransportOfferingMapping = getBcfTravelBundleTemplateService()
				.getBundleTemplateTransportOfferingMappingById(outBoundMappingCode, catalogVersion);
		if (Objects.isNull(outBundleTemplateTransportOfferingMapping))
		{
			outBundleTemplateTransportOfferingMapping = getModelService().create(BundleTemplateTransportOfferingMappingModel.class);
			outBundleTemplateTransportOfferingMapping.setCode(outBoundMappingCode);
		}
		outBundleTemplateTransportOfferingMapping.setBundleTemplate(outBoundRouteBundleTemplate);
		outBundleTemplateTransportOfferingMapping.setTransportOffering(outBoundTransportOffering);
		outBundleTemplateTransportOfferingMapping.setTravelRoute(outBoundRouteBundleTemplate.getTravelRoute());
		outBundleTemplateTransportOfferingMapping.setCabinClass(outBoundRouteBundleTemplate.getCabinClass());
		outBundleTemplateTransportOfferingMapping.setCatalogVersion(outBoundRouteBundleTemplate.getCatalogVersion());

		//In Bound TransportOffering mapping
		final TransportOfferingModel inBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute()
						.contains(inBoundRouteBundleTemplate.getTravelRoute()))
				.findFirst().orElse(null);

		final String inBoundMappingCode = inBoundRouteBundleTemplate.getId() + UNDERSCORE + BTTOM;
		BundleTemplateTransportOfferingMappingModel inBundleTemplateTransportOfferingMapping = getBcfTravelBundleTemplateService()
				.getBundleTemplateTransportOfferingMappingById(inBoundMappingCode, catalogVersion);
		if (Objects.isNull(inBundleTemplateTransportOfferingMapping))
		{
			inBundleTemplateTransportOfferingMapping = getModelService().create(BundleTemplateTransportOfferingMappingModel.class);
			inBundleTemplateTransportOfferingMapping.setCode(inBoundMappingCode);
		}

		inBundleTemplateTransportOfferingMapping.setBundleTemplate(inBoundRouteBundleTemplate);
		inBundleTemplateTransportOfferingMapping.setTransportOffering(inBoundTransportOffering);
		inBundleTemplateTransportOfferingMapping.setTravelRoute(inBoundRouteBundleTemplate.getTravelRoute());
		inBundleTemplateTransportOfferingMapping.setCabinClass(inBoundRouteBundleTemplate.getCabinClass());
		inBundleTemplateTransportOfferingMapping.setCatalogVersion(inBoundRouteBundleTemplate.getCatalogVersion());
		items.add(outBoundTransportOffering);
		items.add(outBundleTemplateTransportOfferingMapping);
		items.add(inBoundTransportOffering);
		items.add(inBundleTemplateTransportOfferingMapping);
	}

	/**
	 * Gets the activity bundle template.
	 *
	 * @param dealBundleData     the deal bundle data
	 * @param catalogVersion     the catalog version
	 * @param dealBundleTemplate the deal bundle template
	 * @return the activity bundle template
	 */
	protected BundleTemplateModel getActivityBundleTemplate(final DealsBundleDataModel dealBundleData,
			final CatalogVersionModel catalogVersion, final DealBundleTemplateModel dealBundleTemplate, final List<ItemModel> items)
	{
		final String activityBundleTemplateName = ACTIVITY + SPACE + PRODUCT + SPACE + dealBundleData.getBundleName(Locale.ENGLISH);
		final String activityBundleTemplateId = activityBundleTemplateName.replaceAll(SPACE, StringUtils.EMPTY);

		BundleTemplateModel bundleTemplate;
		try
		{
			bundleTemplate = getBcfTravelBundleTemplateService().getBundleTemplateById(activityBundleTemplateId, catalogVersion);
			bundleTemplate.setVersion(getBundleIncrementedVersion(bundleTemplate.getVersion()));
		}
		catch (final ModelNotFoundException e)
		{
			bundleTemplate = getModelService().create(BundleTemplateModel.class);
			bundleTemplate.setVersion(DEAL_VERSION);
			bundleTemplate.setId(activityBundleTemplateId);
			bundleTemplate.setParentTemplate(dealBundleTemplate);
		}

		bundleTemplate.setStatus(dealBundleData.getBundleTemplateStatus());
		bundleTemplate.setName(activityBundleTemplateName);
		bundleTemplate.setCatalogVersion(catalogVersion);

		/* Activity Products */
		final List<ProductModel> activityProducts = new ArrayList<>(dealBundleData.getActivityProducts());
		bundleTemplate.setProducts(activityProducts);
		activityProducts.stream().forEach(activityProduct -> items.add(activityProduct));
		items.add(bundleTemplate);
		return bundleTemplate;
	}

	/**
	 * Append status description.
	 *
	 * @param dealBundleData    the deal bundle data
	 * @param statusDescription
	 */
	protected void appendStatusDescription(final DealsBundleDataModel dealBundleData, final String statusDescription)
	{
		dealBundleData.setStatusDescription(
				dealBundleData.getStatusDescription() + System.getProperty("line.separator") + statusDescription);
	}

	/**
	 * Gets the bundle incremented version.
	 *
	 * @param version the version
	 * @return the bundle incremented version
	 */
	protected String getBundleIncrementedVersion(final String version)
	{
		final DecimalFormat decimalFormat = new DecimalFormat("#.#");
		return decimalFormat.format(Double.valueOf(version) + 1.0);
	}

	/**
	 * Index deals.
	 */
	public void indexDeals()
	{
		log.info("Begin solr indexing Product Catalog for deals");
		getSetupSolrIndexerService().executeSolrIndexerCronJob(BCFERRIES_DEAL_INDEX, false);
		log.info("End solr indexing Product Catalog for deals");
	}

	/**
	 * Validate deals bundle data.
	 *
	 * @param dealsBundleData the deals bundle data
	 * @return true, if successful
	 */
	protected boolean validateDealsBundleData(final DealsBundleDataModel dealsBundleData)
	{
		final String errorMsg;
		if (StringUtils.isBlank(dealsBundleData.getBundleName(Locale.ENGLISH)))
		{
			errorMsg = "BundleName is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (StringUtils.isEmpty(dealsBundleData.getStartingDatePattern()))
		{
			errorMsg = "Starting Date Pattern cron expression is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (dealsBundleData.getLength() <= 0)
		{
			errorMsg = "Length can't be -ve or zero for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (CollectionUtils.isEmpty(dealsBundleData.getGuestCounts()))
		{
			errorMsg = "GuestCounts is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (Objects.isNull(dealsBundleData.getTravelRoute()))
		{
			errorMsg = "TravelRoute is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (CollectionUtils.isNotEmpty(dealsBundleData.getTransportOfferings())
				&& !validateTransportOfferings(dealsBundleData.getTravelRoute(), dealsBundleData.getTransportOfferings(),
				dealsBundleData.getBundleId(), dealsBundleData))
		{
			return false;
		}
		if (Objects.isNull(dealsBundleData.getBundleType()))
		{
			errorMsg = "BundleType is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (Objects.isNull(dealsBundleData.getAccommodationOffering()))
		{
			errorMsg = "AccommodationOffering is empty or it is not found for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (Objects.isNull(dealsBundleData.getAccommodation()))
		{
			errorMsg = "Accommodation is empty or it is not found for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		if (Objects.isNull(dealsBundleData.getRatePlan()))
		{
			errorMsg = "RatePlan is empty for " + dealsBundleData.getBundleId();
			log.error(errorMsg);
			dealsBundleData.setStatusDescription(errorMsg);
			return false;
		}
		return true;
	}

	/**
	 * Validate transport offerings.
	 *
	 * @param outboundTravelRoute the outbound travel route
	 * @param transportOfferings  the transport offerings
	 * @param bundleName          the bundle name
	 * @return true, if successful
	 */
	protected boolean validateTransportOfferings(final TravelRouteModel outboundTravelRoute,
			final Collection<TransportOfferingModel> transportOfferings, final String bundleName,
			final DealsBundleDataModel dealsBundleDataModel)
	{
		String errorMsg;
		if (CollectionUtils.size(transportOfferings) != 2)
		{
			errorMsg = "Only two transport offerings must be linked in order to make " + bundleName + ", specific to sailings";
			log.error(errorMsg);
			dealsBundleDataModel.setStatusDescription(errorMsg);
			return false;
		}
		final TransportOfferingModel outBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute().contains(outboundTravelRoute))
				.findFirst().orElse(null);

		final List<TravelRouteModel> travelRoutes = getTravelRouteService()
				.getTravelRoutes(outboundTravelRoute.getDestination().getCode(), outboundTravelRoute.getOrigin().getCode());
		if (CollectionUtils.isEmpty(travelRoutes))
		{
			errorMsg = "Inbound travel route is not configured for the given travel route for " + bundleName;
			log.error(errorMsg);
			dealsBundleDataModel.setStatusDescription(errorMsg);
		}
		final TravelRouteModel inboundTravelRoute = travelRoutes.stream().findFirst().orElse(null);
		final TransportOfferingModel inBoundTransportOffering = transportOfferings.stream()
				.filter(transportOffering -> transportOffering.getTravelSector().getTravelRoute().contains(inboundTravelRoute))
				.findFirst().orElse(null);

		if (Objects.isNull(outBoundTransportOffering) || Objects.isNull(inBoundTransportOffering))
		{
			errorMsg = "Outbound or Inbound transport offering provided for " + bundleName
					+ " is not matching as per the given travel route.";
			log.error(errorMsg);
			dealsBundleDataModel.setStatusDescription(errorMsg);
		}
		return true;
	}

	/**
	 * @return the dealsBundleDataService
	 */
	protected DealsBundleDataService getDealsBundleDataService()
	{
		return dealsBundleDataService;
	}

	/**
	 * @param dealsBundleDataService the dealsBundleDataService to set
	 */
	@Required
	public void setDealsBundleDataService(final DealsBundleDataService dealsBundleDataService)
	{
		this.dealsBundleDataService = dealsBundleDataService;
	}

	/**
	 * @return the bcfGuestCountService
	 */
	protected BcfGuestCountService getBcfGuestCountService()
	{
		return bcfGuestCountService;
	}

	/**
	 * @param bcfGuestCountService the bcfGuestCountService to set
	 */
	@Required
	public void setBcfGuestCountService(final BcfGuestCountService bcfGuestCountService)
	{
		this.bcfGuestCountService = bcfGuestCountService;
	}

	/**
	 * @return the travelRouteService
	 */
	protected TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	/**
	 * @param travelRouteService the travelRouteService to set
	 */
	@Required
	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	/**
	 * @return the cabinClassService
	 */
	protected CabinClassService getCabinClassService()
	{
		return cabinClassService;
	}

	/**
	 * @param cabinClassService the cabinClassService to set
	 */
	@Required
	public void setCabinClassService(final CabinClassService cabinClassService)
	{
		this.cabinClassService = cabinClassService;
	}

	/**
	 * @return the bcfTravelBundleTemplateService
	 */
	protected BcfTravelBundleTemplateService getBcfTravelBundleTemplateService()
	{
		return bcfTravelBundleTemplateService;
	}

	/**
	 * @param bcfTravelBundleTemplateService the bcfTravelBundleTemplateService to set
	 */
	@Required
	public void setBcfTravelBundleTemplateService(final BcfTravelBundleTemplateService bcfTravelBundleTemplateService)
	{
		this.bcfTravelBundleTemplateService = bcfTravelBundleTemplateService;
	}

	/**
	 * Gets the property source facade.
	 *
	 * @return the property source facade
	 */
	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	/**
	 * Sets the property source facade.
	 *
	 * @param propertySourceFacade the new property source facade
	 */
	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	/**
	 * @return the setupSolrIndexerService
	 */
	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	/**
	 * @param setupSolrIndexerService the setupSolrIndexerService to set
	 */
	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	protected DefaultDealBundleDataModelUrlResolver getDealBundleDataModelUrlResolver()
	{
		return dealBundleDataModelUrlResolver;
	}

	@Required
	public void setDealBundleDataModelUrlResolver(
			final DefaultDealBundleDataModelUrlResolver dealBundleDataModelUrlResolver)
	{
		this.dealBundleDataModelUrlResolver = dealBundleDataModelUrlResolver;
	}
}
