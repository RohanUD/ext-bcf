/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.activities;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityScheduleDataModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.model.product.ActivityScheduleModel;
import com.bcf.integration.dataupload.service.activities.ActivityScheduleDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityScheduleDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivityScheduleDataUploadUtility extends AbstractDataUploadUtility
{
	protected static final Logger LOG = Logger.getLogger(ActivityScheduleDataUploadUtility.class);
	ActivityScheduleDataService activityScheduleDataService;
	private ActivityScheduleDataValidator activityScheduleDataValidator;

	public void uploadActivityScheduleData()
	{
		final List<ActivityScheduleDataModel> pendingActivityScheduleData = getActivityScheduleDataService()
				.getActivityScheduleData(DataImportProcessStatus.PENDING);
		/* checking for invalid items */
		final List<ActivityScheduleDataModel> inValidActivityScheduleData = pendingActivityScheduleData.stream()
				.filter(activityScheduleData -> !getActivityScheduleDataValidator().validateActivityScheduleData(activityScheduleData)
						.isEmpty()
				)
				.collect(Collectors.toList());
		inValidActivityScheduleData.forEach(activityScheduleData -> activityScheduleData.setStatus(DataImportProcessStatus.FAILED));
		getModelService().saveAll(inValidActivityScheduleData);

		/* processing for valid items */

		final List<ActivityScheduleDataModel> activityScheduleDataList = pendingActivityScheduleData.stream()
				.filter(activityScheduleData -> getActivityScheduleDataValidator()
						.validateActivityScheduleData(activityScheduleData).isEmpty())
				.collect(Collectors.toList());


		activityScheduleDataList
				.forEach(activityScheduleData -> activityScheduleData.setStatus(DataImportProcessStatus.PROCESSING));
		getModelService().saveAll(activityScheduleDataList);
		final List<ItemModel> items = new ArrayList<>();
		final Map<ActivitiesDataModel, List<ActivityScheduleDataModel>> activitySchedulesGroupByActivityData = activityScheduleDataList
				.stream().collect(Collectors.groupingBy(ActivityScheduleDataModel::getActivitiesData));
		activitySchedulesGroupByActivityData.forEach((activitiesDataModel, activityScheduleDatas) -> {
			uploadActivityScheduleList(activitiesDataModel, activityScheduleDatas, items);
		});
		getModelService().saveAll();
		synchronizeProductCatalog(items);
		getModelService().saveAll(activityScheduleDataList);
	}

	public void uploadActivityScheduleList(final ActivitiesDataModel activitiesDataModel,
			final Collection<ActivityScheduleDataModel> activityScheduleDataList, final List<ItemModel> items)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final ActivityProductModel activityProduct = getActivityProduct(activitiesDataModel, catalogVersion);
		if (Objects.isNull(activityProduct))
		{
			activityScheduleDataList
					.forEach(activityScheduleDataModel -> activityScheduleDataModel.setStatus(DataImportProcessStatus.FAILED));
			getModelService().saveAll(activityScheduleDataList);
			return;
		}

		removeInvalidSchedules(activityScheduleDataList, activityProduct);

		for (final ActivityScheduleDataModel activityScheduleData : activityScheduleDataList)
		{
			activityScheduleData.setStartDate(Objects.isNull(activityScheduleData.getStartDate()) ?
					activityScheduleData.getStartDate() :
					DateUtils.truncate(activityScheduleData.getStartDate(), Calendar.DATE));
			activityScheduleData.setEndDate(Objects.isNull(activityScheduleData.getEndDate()) ?
					activityScheduleData.getEndDate() :
					DateUtils.truncate(activityScheduleData.getEndDate(), Calendar.DATE));

			final Collection<ActivityScheduleModel> activitySchedules = getActivitySchedules(activitiesDataModel,
					activityScheduleData, activityProduct, items);
			activityProduct.setActivityScheduleList(activitySchedules);
			getModelService().saveAll(activitySchedules);
			getModelService().save(activityProduct);
			items.add(activityProduct);
			activityScheduleData.setStatus(DataImportProcessStatus.SUCCESS);
			getModelService().save(activityScheduleData);
			items.add(activityScheduleData);
			items.add(activitiesDataModel);
		}
	}

	private void removeInvalidSchedules(final Collection<ActivityScheduleDataModel> activityScheduleDataList,
			final ActivityProductModel activityProduct)
	{
		if (CollectionUtils.isEmpty(activityScheduleDataList) && CollectionUtils
				.isNotEmpty(activityProduct.getActivityScheduleList()))
		{
			getModelService().removeAll(activityProduct.getActivityScheduleList());
			getModelService().refresh(activityProduct);
		}

		Collection<ActivityScheduleModel> invalidSchedules = activityProduct.getActivityScheduleList().stream()
				.filter(scheduleModel -> !isScheduleAvailableInScheduleDataList(scheduleModel, activityScheduleDataList)).collect(
						Collectors.toList());

		if (CollectionUtils.isNotEmpty(invalidSchedules))
		{
			getModelService().removeAll(invalidSchedules);
			getModelService().refresh(activityProduct);
		}
	}

	private boolean isScheduleAvailableInScheduleDataList(final ActivityScheduleModel scheduleModel,
			final Collection<ActivityScheduleDataModel> activityScheduleDataList)
	{
		return activityScheduleDataList.stream().anyMatch(scheduleData -> isScheduleMatch(scheduleModel, scheduleData));
	}

	private boolean isScheduleMatch(final ActivityScheduleModel scheduleModel, final ActivityScheduleDataModel scheduleData)
	{
		return scheduleData.getStartDate().equals(scheduleModel.getStartDate())
				&& scheduleData.getEndDate().equals(scheduleModel.getEndDate())
				&& scheduleData.getStartTime().equals(scheduleModel.getStartTime())
				&& scheduleData.getCheckinTime().equals(scheduleModel.getCheckinTime());
	}

	protected Collection<ActivityScheduleModel> getActivitySchedules(final ActivitiesDataModel activitiesDataModel,
			final ActivityScheduleDataModel activityScheduleData, final ActivityProductModel activityProduct,
			final List<ItemModel> items)
	{
		final List<ActivityScheduleModel> activityScheduleList = CollectionUtils
				.isEmpty(activityProduct.getActivityScheduleList()) ?
				Collections.emptyList() :
				(List<ActivityScheduleModel>) activityProduct.getActivityScheduleList();
		final Map<String, ActivityScheduleModel> activityScheduleMap = getActivityScheduleMap(activitiesDataModel,
				activityScheduleList);
		final ActivityScheduleModel activityScheduleModel = getOrCreateActivityScheduleModel(
				activitiesDataModel, activityScheduleData, activityScheduleMap, items);
		activityScheduleMap.put(activityProduct.getCode()
				.concat(String.valueOf(activityScheduleModel.getStartDate().getTime()))
				.concat(String.valueOf(activityScheduleModel.getEndDate().getTime()))
				.concat(String.valueOf(activityScheduleModel.getStartTime())), activityScheduleModel);

		return activityScheduleMap.values();
	}

	private ActivityScheduleModel getOrCreateActivityScheduleModel(final ActivitiesDataModel activitiesDataModel,
			final ActivityScheduleDataModel activityScheduleData,
			final Map<String, ActivityScheduleModel> activityScheduleMap, final List<ItemModel> items)
	{
		final String key = activitiesDataModel.getCode()
				.concat(String.valueOf(activityScheduleData.getStartDate().getTime()))
				.concat(String.valueOf(activityScheduleData.getEndDate().getTime()))
				.concat(String.valueOf(activityScheduleData.getStartTime()));
		final ActivityScheduleModel activityScheduleModel = activityScheduleMap.containsKey(key) ?
				activityScheduleMap.get(key) : getModelService().create(ActivityScheduleModel.class);
		activityScheduleModel.setStartDate(activityScheduleData.getStartDate());
		activityScheduleModel.setEndDate(activityScheduleData.getEndDate());
		activityScheduleModel.setStartTime(activityScheduleData.getStartTime());
		activityScheduleModel.setDuration(activityScheduleData.getDuration());
		activityScheduleModel.setCheckinTime(activityScheduleData.getCheckinTime());
		items.add(activityScheduleModel);
		return activityScheduleModel;
	}

	private Map<String, ActivityScheduleModel> getActivityScheduleMap(final ActivitiesDataModel activitiesDataModel,
			final List<ActivityScheduleModel> activityScheduleList)
	{
		final Map<String, ActivityScheduleModel> activityScheduleModelMap = new HashMap<>();
		activityScheduleList
				.forEach(activitySchedule -> activityScheduleModelMap
						.put(activitiesDataModel.getCode()
								.concat(String.valueOf(activitySchedule.getStartDate().getTime()))
								.concat(String.valueOf(activitySchedule.getEndDate().getTime()))
								.concat(String.valueOf(activitySchedule.getStartTime())), activitySchedule));
		return activityScheduleModelMap;
	}


	private ActivityProductModel getActivityProduct(final ActivitiesDataModel activitiesData,
			final CatalogVersionModel catalogVersion)
	{
		final String activityCode = activitiesData.getCode();
		ActivityProductModel activity = null;
		try
		{
			activity = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			LOG.error("No Activity Product found for the activity code", e);
		}
		return activity;
	}

	public ActivityScheduleDataValidator getActivityScheduleDataValidator()
	{
		return activityScheduleDataValidator;
	}

	public void setActivityScheduleDataValidator(
			final ActivityScheduleDataValidator activityScheduleDataValidator)
	{
		this.activityScheduleDataValidator = activityScheduleDataValidator;
	}

	public ActivityScheduleDataService getActivityScheduleDataService()
	{
		return activityScheduleDataService;
	}

	public void setActivityScheduleDataService(
			final ActivityScheduleDataService activityScheduleDataService)
	{
		this.activityScheduleDataService = activityScheduleDataService;
	}
}
