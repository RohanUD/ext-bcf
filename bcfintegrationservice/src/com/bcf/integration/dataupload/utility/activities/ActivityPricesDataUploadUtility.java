/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.activities;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelservices.model.user.PassengerTypeModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityPriceDataModel;
import com.bcf.core.model.SpecializedPassengerTypeModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.dataupload.service.activities.ActivitiesDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityPriceDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivityPricesDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(ActivityPricesDataUploadUtility.class);

	private static final String ACTIVITY_DATA_SUCCESS = "Activity Price data created successfully";

	private ActivitiesDataService activitiesDataService;
	private UnitService unitService;
	private ActivityPriceDataValidator activityPriceDataValidator;

	public void uploadActivityPriceData()
	{
		final List<ItemModel> items = new ArrayList<>();
		final List<ActivityPriceDataModel> pendingActivityPriceData = getActivitiesDataService()
				.getActivityPricesData(DataImportProcessStatus.PENDING);
		/* checking for invalid items */
		final List<ActivityPriceDataModel> invalidActivityPriceData = pendingActivityPriceData.stream()
				.filter(activityPriceData -> !getActivityPriceDataValidator().validateActivityPriceData(activityPriceData).isEmpty())
				.collect(Collectors.toList());
		invalidActivityPriceData.forEach(activityData -> activityData.setStatus(DataImportProcessStatus.FAILED));
		getModelService().saveAll(invalidActivityPriceData);

		/* processing for valid items */
		final List<ActivityPriceDataModel> activityPriceDataList = pendingActivityPriceData.stream()
				.filter(activityPriceData -> getActivityPriceDataValidator().validateActivityPriceData(activityPriceData).isEmpty())
				.collect(Collectors.toList());
		activityPriceDataList.forEach(activityPriceData -> activityPriceData.setStatus(DataImportProcessStatus.PROCESSING));
		getModelService().saveAll(activityPriceDataList);

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		activityPriceDataList.stream().forEach(activityPriceData -> uploadActivityPrices(activityPriceData, catalogVersion, items));

		getModelService().saveAll();
		if (CollectionUtils.isNotEmpty(items))
		{
			synchronizeProductCatalog(items);
		}

		activityPriceDataList.forEach(this::handleSuccess);
		getModelService().saveAll(activityPriceDataList);
	}


	public Boolean uploadActivityPrices(final ActivityPriceDataModel activityPriceData, final CatalogVersionModel catalogVersion,
			final List<ItemModel> items)
	{

		final String activityCode = activityPriceData.getActivitiesData().getCode();
		final ActivityProductModel activity;
		try
		{
			activity = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			final String errorMessage = "ActivityProductModel not found for activityCode: " + activityCode;
			LOG.error(errorMessage);
			activityPriceData.setStatusDescription(errorMessage);
			return Boolean.FALSE;
		}

		String passengerCode = Strings.EMPTY;
		if (Objects.nonNull(activityPriceData.getPassengerType()))
		{
			if (activityPriceData.getMinAge() != null && activityPriceData.getMaxAge() != null)
			{
				createUpdateSpecializedPassengerType(activityPriceData, activity);
			}
			passengerCode = activityPriceData.getPassengerType().getCode();

		}
		handleOldOverLappingPriceRows(activity, passengerCode,
				activityPriceData, items);

		updatePriceRow(activity, passengerCode, activityPriceData.getStartDate(),
				activityPriceData.getEndDate(), activityPriceData.getPrice(), activityPriceData.getMarginRate(),
				activityPriceData.getActivitiesData().isPublish(),
				items, activityPriceData.getDaysOfWeek());
		return Boolean.TRUE;
	}

	private void handleOldOverLappingPriceRows(final ActivityProductModel activity, final String passengerCode,
			final ActivityPriceDataModel activityPriceData, final List<ItemModel> items)
	{
		String daysOfWeekCode = BCFDateUtils.createDaysOfWeekCode(activityPriceData.getDaysOfWeek());
		Date startDate = activityPriceData.getStartDate();
		Date endDate = activityPriceData.getEndDate();

		Collection<PriceRowModel> ownEurope1Prices = activity.getOwnEurope1Prices();
		List<PriceRowModel> overlappingOldPriceRows = StreamUtil.safeStream(ownEurope1Prices)
				.filter(p -> BCFDateUtils.checkIfDatesOverlapped(startDate, endDate, p.getStartTime(),
						p.getEndTime()))
				.filter(p -> StringUtils.equals(passengerCode, p.getPassengerType()))
				.filter(p -> StringUtils.equals(daysOfWeekCode, p.getDaysOfWeekCode())).collect(
						Collectors.toList());

		// code only for Logging starts
		String removedPriceRowsDatesStr = "";
		for (PriceRowModel price : overlappingOldPriceRows)
		{
			removedPriceRowsDatesStr += new StringBuilder().append("[").append(price.getStartTime()).append(" - ")
					.append(price.getEndTime()).append("] ").toString();
		}

		String newDate = new StringBuilder().append("[").append(startDate).append(" - ")
				.append(endDate).append("] ").toString();

		LOG.info(String.format("Deleted %d old overlapped price rows for product %s, old dates %s, new date %s",
				overlappingOldPriceRows.size(),
				activity.getCode(), removedPriceRowsDatesStr, newDate));
		// code only for Logging ends

		if (CollectionUtils.isNotEmpty(overlappingOldPriceRows))
		{
			getModelService().removeAll(overlappingOldPriceRows);
			items.add(activity);
		}
	}

	public void handleSuccess(final ActivityPriceDataModel activityPriceData)
	{
		activityPriceData.setStatus(DataImportProcessStatus.SUCCESS);
		activityPriceData.setStatusDescription(ACTIVITY_DATA_SUCCESS);
	}

	public void truncateDates(final ActivityPriceDataModel activityPriceDataModel)
	{
		activityPriceDataModel.setStartDate(DateUtils.truncate(activityPriceDataModel.getStartDate(), Calendar.DATE));
		activityPriceDataModel.setEndDate(DateUtils.truncate(activityPriceDataModel.getEndDate(), Calendar.DATE));
	}

	private void createUpdateSpecializedPassengerType(final ActivityPriceDataModel activityPriceData,
			final ActivityProductModel activity)
	{
		if (Objects.isNull(activity.getSpecializedPassengerTypes()) || activity.getSpecializedPassengerTypes().isEmpty())
		{
			createSpecializedPassengerType(activityPriceData, activity);
			return;
		}

		final List<SpecializedPassengerTypeModel> specializedPassengerTypeModelList = activity.getSpecializedPassengerTypes()
				.stream()
				.filter(specializedPassengerType -> specializedPassengerType.getBasePassengerType()
						.equals(activityPriceData.getPassengerType())).collect(Collectors.toList());

		if (Objects.isNull(specializedPassengerTypeModelList) || specializedPassengerTypeModelList.isEmpty())
		{
			createSpecializedPassengerType(activityPriceData, activity);
			return;
		}
		updateSpecializedPassengerType(activityPriceData, specializedPassengerTypeModelList.get(0));
	}

	private void createSpecializedPassengerType(final ActivityPriceDataModel activityPriceData,
			final ActivityProductModel activity)
	{
		final SpecializedPassengerTypeModel specializedPassengerTypeModel = getModelService()
				.create(SpecializedPassengerTypeModel.class);
		specializedPassengerTypeModel.setCode(activity.getCode() + activityPriceData.getPassengerType().getCode());
		specializedPassengerTypeModel
				.setCode(activity.getCode() + BcfintegrationserviceConstants.UNDERSCORE + activityPriceData.getPassengerType()
						.getCode());
		specializedPassengerTypeModel.setBasePassengerType(activityPriceData.getPassengerType());
		specializedPassengerTypeModel.setMinAge(activityPriceData.getMinAge());
		specializedPassengerTypeModel.setMaxAge(activityPriceData.getMaxAge());

		final Collection<SpecializedPassengerTypeModel> newSpecializedPassengerTypeModelList = new ArrayList<>();
		if (Objects.nonNull(activity.getSpecializedPassengerTypes()))
		{
			newSpecializedPassengerTypeModelList.addAll(activity.getSpecializedPassengerTypes());
		}
		newSpecializedPassengerTypeModelList.add(specializedPassengerTypeModel);

		activity.setSpecializedPassengerTypes(newSpecializedPassengerTypeModelList);
	}

	private void updateSpecializedPassengerType(final ActivityPriceDataModel activityPriceData,
			final SpecializedPassengerTypeModel specializedPassengerTypeModel)
	{
		specializedPassengerTypeModel.setMinAge(activityPriceData.getMinAge());
		specializedPassengerTypeModel.setMaxAge(activityPriceData.getMaxAge());
	}

	/**
	 * Update price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @param basePrice     the base price
	 * @param marginRate    the margin rate
	 * @param publish       the publish
	 * @param items         the items
	 * @param daysOfWeek    the days of week
	 */
	protected void updatePriceRow(final ActivityProductModel product, final String passengerType, final Date startDate,
			final Date endDate, final Double basePrice, final Double marginRate, final boolean publish, final List<ItemModel> items,
			List<DayOfWeek> daysOfWeek)
	{
		PriceRowModel price;

		if (CollectionUtils.isEmpty(daysOfWeek))
		{
			daysOfWeek = Arrays.asList(DayOfWeek.values());
		}
		price = getPriceRowService().getPriceRow(product, passengerType, startDate, endDate, daysOfWeek);
		if (Objects.isNull(price))
		{
			price = createPriceRow(product, passengerType, startDate, endDate, daysOfWeek);
		}
		price.setPrice(basePrice);
		price.setActivityMarginRate(marginRate);
		if (publish)
		{
			items.add(price);
		}
	}

	/**
	 * Creates the price row.
	 *
	 * @param product       the product
	 * @param passengerType the passenger type
	 * @param startDate     the start date
	 * @param endDate       the end date
	 * @return the price row model
	 */
	protected PriceRowModel createPriceRow(final ActivityProductModel product, final String passengerType, final Date startDate,
			final Date endDate, final List<DayOfWeek> daysOfWeek)
	{
		final PriceRowModel price = getModelService().create(PriceRowModel.class);

		price.setProduct(product);
		price.setCurrency(getCommonI18NService().getBaseCurrency());
		price.setMinqtd(1l);
		price.setNet(true);
		price.setUnit(getUnitService().getUnitForCode(UNIT));
		price.setCatalogVersion(product.getCatalogVersion());
		price.setPassengerType(passengerType);
		price.setStartTime(startDate);
		price.setEndTime(endDate);
		price.setDaysOfWeekCode(BCFDateUtils.createDaysOfWeekCode(daysOfWeek));

		return price;
	}

	protected UnitService getUnitService()
	{
		return unitService;
	}

	@Required
	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}

	protected ActivitiesDataService getActivitiesDataService()
	{
		return activitiesDataService;
	}

	@Required
	public void setActivitiesDataService(final ActivitiesDataService activitiesDataService)
	{
		this.activitiesDataService = activitiesDataService;
	}


	protected ActivityPriceDataValidator getActivityPriceDataValidator()
	{
		return activityPriceDataValidator;
	}

	@Required
	public void setActivityPriceDataValidator(
			final ActivityPriceDataValidator activityPriceDataValidator)
	{
		this.activityPriceDataValidator = activityPriceDataValidator;
	}
}
