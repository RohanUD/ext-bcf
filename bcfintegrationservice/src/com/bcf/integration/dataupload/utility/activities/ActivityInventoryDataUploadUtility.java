/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.activities;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.tx.Transaction;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.integration.dataupload.service.activities.ActivitiesDataService;
import com.bcf.integration.dataupload.service.activities.ActivityInventoryDataService;
import com.bcf.integration.dataupload.service.activities.UpdateStockLevelsToActivityService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityInventoryDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivityInventoryDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(ActivityInventoryDataUploadUtility.class);

	private static final String ACTIVITY_DATA_SUCCESS = "Activity Inventory data created successfully";

	private ActivityInventoryDataService activityInventoryDataService;
	private UpdateStockLevelsToActivityService updateStockLevelsToActivityService;
	private ActivitiesDataService activitiesDataService;
	private ActivityInventoryDataValidator activityInventoryDataValidator;

	public void uploadActivityInventoryDatas()
	{
		final List<ActivityInventoryDataModel> pendingActivityInventoryData = getActivityInventoryDataService()
				.getActivityInventoryData(DataImportProcessStatus.PENDING);
		final Transaction tx = Transaction.current();
		boolean result = false;
		try
		{
			tx.begin();
			uploadActivityInventoryData(pendingActivityInventoryData);
			getModelService().saveAll();
			LOG.info("Transaction Commit");
			tx.commit();
			result = true;
		}
		catch (final ModelSavingException | IllegalArgumentException ex)
		{
			LOG.error("Transaction RollBack : " + ex);
			tx.rollback();
		}
		finally
		{
			if (!result)
			{
				tx.rollback();
			}
		}
	}

	/**
	 * Upload activity inventory data.
	 */
	public void uploadActivityInventoryData(final List<ActivityInventoryDataModel> pendingActivityInventoryData)
	{
		LOG.debug("pendingActivityInventoryData size : " + pendingActivityInventoryData.size());

		pendingActivityInventoryData
				.forEach(activityInventoryData -> activityInventoryData.setStatus(DataImportProcessStatus.PROCESSING));
		final List<ActivityInventoryDataModel> invalidActivityInventoryData = pendingActivityInventoryData
				.stream().filter(activityInventoryData -> !getActivityInventoryDataValidator()
						.validateActivityInventoryData(activityInventoryData).isEmpty())
				.collect(Collectors.toList());

		LOG.debug("invalidActivityInventoryData size : " + invalidActivityInventoryData.size());

		final List<ActivityInventoryDataModel> validActivityInventoryData = removeInvalidActivityInventoryData(
				pendingActivityInventoryData, invalidActivityInventoryData);

		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		processData(validActivityInventoryData, catalogVersion);
	}

	private void processData(
			final List<ActivityInventoryDataModel> activityInventoryDatas, final CatalogVersionModel catalogVersion)
	{
		final Map<String, List<ActivityInventoryDataModel>> activityInventoryDatasMapByActivityCode = activityInventoryDatas
				.stream().collect(
						Collectors.groupingBy(activityInventoryDataModel -> activityInventoryDataModel.getActivitiesData().getCode()));
		final Map<ActivityProductModel, List<ActivityInventoryDataModel>> activityInventoryDatasMap = new HashMap<>();
		for (final Map.Entry<String, List<ActivityInventoryDataModel>> activityInventoryDatasEntryMapByActivityCode : activityInventoryDatasMapByActivityCode
				.entrySet())
		{
			final String activityCode = activityInventoryDatasEntryMapByActivityCode.getKey();
			final List<ActivityInventoryDataModel> groupedActivityInventoryDataEntryByActivity = activityInventoryDatasEntryMapByActivityCode
					.getValue();

			final ActivityProductModel activityProduct = findValidActivityProduct(activityCode, catalogVersion);
			if (Objects.isNull(activityProduct))
			{
				LOG.error("marking failed to linked activity inventories for activityCode : " + activityCode);
				groupedActivityInventoryDataEntryByActivity
						.forEach(invalidActivityInventory -> invalidActivityInventory.setStatus(DataImportProcessStatus.FAILED));
			}
			else
			{
				activityInventoryDatasMap.put(activityProduct, groupedActivityInventoryDataEntryByActivity);
			}
		}
		getUpdateStockLevelsToActivityService().updateBcfStockLevelsToActivity(activityInventoryDatasMap);
		updateStatusOfActivityInventory(activityInventoryDatas, DataImportProcessStatus.PROCESSING,
				DataImportProcessStatus.SUCCESS);
	}

	public void uploadActivityStockLevel(final ActivitiesDataModel activitiesDataModel)
	{
		getUpdateStockLevelsToActivityService().updateBcfStockByType(activitiesDataModel);
	}

	private ActivityProductModel findValidActivityProduct(final String activityCode, final CatalogVersionModel catalogVersion)
	{
		ActivityProductModel activityProduct=null;
		try
		{
			activityProduct = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			LOG.error("activityProductModel not found for activityCode : " + activityCode);
		}
		return activityProduct;
	}

	private List<ActivityInventoryDataModel> removeInvalidActivityInventoryData(
			final List<ActivityInventoryDataModel> pendingActivityInventoryData,
			final List<ActivityInventoryDataModel> invalidActivityInventoryData)
	{
		invalidActivityInventoryData
				.forEach(invalidActivityInventory -> invalidActivityInventory.setStatus(DataImportProcessStatus.FAILED));
		final List<ActivityInventoryDataModel> validActivityInventoryData = pendingActivityInventoryData.stream()
				.filter(validActivityInventory -> !invalidActivityInventoryData.contains(
						validActivityInventory))
				.collect(Collectors.toList());
		LOG.debug("validActivityInventoryData size : " + validActivityInventoryData.size());
		return validActivityInventoryData;
	}

	private void updateStatusOfActivityInventory(
			final List<ActivityInventoryDataModel> activityInventoryDataList,
			final DataImportProcessStatus fromStatus,
			final DataImportProcessStatus toStatus)
	{
		activityInventoryDataList.stream()
				.filter(accommodationStockLevelData -> fromStatus.equals(accommodationStockLevelData.getStatus()))
				.forEach(stockLevelData -> stockLevelData.setStatus(toStatus));
	}

	public void handleSuccess(final ActivityInventoryDataModel activityInventoryData)
	{
		activityInventoryData.setStatus(DataImportProcessStatus.SUCCESS);
		activityInventoryData.setStatusDescription(ACTIVITY_DATA_SUCCESS);
	}

	protected ActivityInventoryDataService getActivityInventoryDataService()
	{
		return activityInventoryDataService;
	}

	@Required
	public void setActivityInventoryDataService(
			final ActivityInventoryDataService activityInventoryDataService)
	{
		this.activityInventoryDataService = activityInventoryDataService;
	}

	protected UpdateStockLevelsToActivityService getUpdateStockLevelsToActivityService()
	{
		return updateStockLevelsToActivityService;
	}

	@Required
	public void setUpdateStockLevelsToActivityService(
			final UpdateStockLevelsToActivityService updateStockLevelsToActivityService)
	{
		this.updateStockLevelsToActivityService = updateStockLevelsToActivityService;
	}

	protected ActivitiesDataService getActivitiesDataService()
	{
		return activitiesDataService;
	}

	@Required
	public void setActivitiesDataService(final ActivitiesDataService activitiesDataService)
	{
		this.activitiesDataService = activitiesDataService;
	}

	protected ActivityInventoryDataValidator getActivityInventoryDataValidator()
	{
		return activityInventoryDataValidator;
	}

	@Required
	public void setActivityInventoryDataValidator(
			final ActivityInventoryDataValidator activityInventoryDataValidator)
	{
		this.activityInventoryDataValidator = activityInventoryDataValidator;
	}
}
