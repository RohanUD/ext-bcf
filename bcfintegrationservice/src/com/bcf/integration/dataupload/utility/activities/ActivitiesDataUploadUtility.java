/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.activities;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivitySaleStatusDataModel;
import com.bcf.core.accommodation.service.ProductTaxGroupMappingService;
import com.bcf.core.accommodation.service.VendorService;
import com.bcf.core.model.ActivitySaleStatusModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.integration.dataupload.service.activities.ActivitiesDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivitiesDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivitiesDataUploadUtility extends AbstractDataUploadUtility
{
	private static final Logger LOG = Logger.getLogger(ActivitiesDataUploadUtility.class);
	private static final String CA_TAX_GROUP = "ca-tax-group";
	private static final String ACTIVITY_DATA_SUCCESS = "Activity data created successfully";
	private static final String BCFERRIES_DEAL_INDEX = "bcferriesDealIndex";

	private ActivitiesDataService activitiesDataService;
	private VendorService vendorService;
	private UnitService unitService;
	private ActivitiesDataValidator activitiesDataValidator;
	private ProductTaxGroupMappingService productTaxGroupMappingService;
	private SetupSolrIndexerService setupSolrIndexerService;

	private Converter<ActivitySaleStatusDataModel, ActivitySaleStatusModel> activitySaleStatusReverseConverter;

	/**
	 * Upload activities data.
	 *
	 * @return the list
	 */
	public List<String> uploadActivitiesData()
	{
		final List<ActivitiesDataModel> pendingActivitiesData = getActivitiesDataService()
				.getActivitiesData(DataImportProcessStatus.PENDING);

		/* checking for invalid items */
		final List<ActivitiesDataModel> inValidActivitiesData = pendingActivitiesData.stream()
				.filter(activityData -> !getActivitiesDataValidator().validateActivityData(activityData).isEmpty())
				.collect(Collectors.toList());
		inValidActivitiesData.forEach(activityData -> activityData.setStatus(DataImportProcessStatus.FAILED));
		getModelService().saveAll(inValidActivitiesData);

		/* processing for valid items */
		final List<ActivitiesDataModel> activitiesData = pendingActivitiesData.stream()
				.filter(activityData -> getActivitiesDataValidator().validateActivityData(activityData).isEmpty())
				.collect(Collectors.toList());
		activitiesData.forEach(activityData -> activityData.setStatus(DataImportProcessStatus.PROCESSING));

		getModelService().saveAll(activitiesData);
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		final List<ItemModel> items = new ArrayList<>();
		uploadActivities(activitiesData, catalogVersion, items);
		getModelService().saveAll();
		if (CollectionUtils.isNotEmpty(items))
		{
			synchronizeProductCatalog(items);
			indexDeals();
		}
		activitiesData.forEach(activityData -> activityData.setStatus(DataImportProcessStatus.SUCCESS));
		getModelService().saveAll(activitiesData);
		return Collections.emptyList();
	}

	public void markUnapproved(final ActivitiesDataModel activityData, final CatalogVersionModel catalogVersion)
	{
		final ActivityProductModel activity;
		try
		{
			activity = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityData.getCode());
			activity.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
			getModelService().save(activity);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			//do nothing
		}
	}

	public void setCode(final ActivitiesDataModel activitiesDataModel)
	{
		activitiesDataModel.setCode(getCodeFromName(
				getCodeFromName(StringUtils.EMPTY, activitiesDataModel.getVendorName(Locale.ENGLISH), StringUtils.EMPTY)
						+ UNDERSCORE, activitiesDataModel.getActivityName(Locale.ENGLISH), StringUtils.EMPTY));
	}

	protected String getCodeFromName(final String prefix, final String name, final String suffix)
	{
		return prefix + name.replaceAll(SPACE, UNDERSCORE).toUpperCase() + suffix;
	}

	/**
	 * Upload activities.
	 *
	 * @param activitiesData the activities data
	 * @param catalogVersion the catalog version
	 * @return
	 */
	public Boolean uploadActivities(final List<ActivitiesDataModel> activitiesData, final CatalogVersionModel catalogVersion,
			final List<ItemModel> items)
	{
		final Map<String, List<ActivitiesDataModel>> activitiesDataMapByVendor = activitiesData.stream()
				.filter(activityData -> StringUtils.isNotEmpty(activityData.getVendorName()))
				.collect(Collectors.groupingBy(ActivitiesDataModel::getVendorName));

		for (final Entry<String, List<ActivitiesDataModel>> activityDataEntryByVendor : activitiesDataMapByVendor.entrySet())
		{
			final String vendorName = activityDataEntryByVendor.getKey();
			final List<ActivitiesDataModel> groupedActivitiesDataByVendor = activityDataEntryByVendor.getValue();

			/* Vendor */
			final VendorModel vendor = getOrCreateVendor(vendorName);

			final Map<String, List<ActivitiesDataModel>> activitiesDataMapByActivity = groupedActivitiesDataByVendor.stream()
					.filter(activityData -> StringUtils.isNotEmpty(activityData.getActivityName()))
					.collect(Collectors.groupingBy(ActivitiesDataModel::getCode));
			/* Activity Products */
			uploadActivityProducts(activitiesDataMapByActivity, vendor, catalogVersion, items);
			groupedActivitiesDataByVendor.forEach(activityData -> activityData.setStatus(DataImportProcessStatus.PROCESSED));
		}

		return Boolean.TRUE;
	}

	/**
	 * Upload activity products.
	 *
	 * @param activitiesDataMapByActivity the activities data map by activity
	 * @param vendor                      the vendor
	 * @param catalogVersion
	 */
	protected void uploadActivityProducts(final Map<String, List<ActivitiesDataModel>> activitiesDataMapByActivity,
			final VendorModel vendor, final CatalogVersionModel catalogVersion, final List<ItemModel> items)
	{
		for (final Entry<String, List<ActivitiesDataModel>> activityDataEntryByActivity : activitiesDataMapByActivity.entrySet())
		{
			final List<ActivitiesDataModel> groupedActivitiesDataByActivity = activityDataEntryByActivity.getValue();

			/* Activity */
			final ActivitiesDataModel activityDataForActivity = groupedActivitiesDataByActivity.stream().findFirst().get();
			final ActivityProductModel activity = getActivityProduct(activityDataForActivity, vendor, catalogVersion);
			updateActivity(activity, activityDataForActivity, vendor, catalogVersion);
			if (activityDataForActivity.isPublish())
			{
				items.add(activity);
			}
		}
	}

	private void updateActivity(final ActivityProductModel activity,
			final ActivitiesDataModel activityDataForActivity, final VendorModel vendor,
			final CatalogVersionModel catalogVersion)
	{
		getLocaleList().forEach(locale -> activity.setName(activityDataForActivity.getActivityName(locale), locale));
		getLocaleList().forEach(locale -> activity.setDescription(activityDataForActivity.getActivityDescription(locale), locale));
		getLocaleList()
				.forEach(locale -> activity.setTermsAndConditions(activityDataForActivity.getTermsAndConditions(locale), locale));
		activity.setCatalogVersion(catalogVersion);
		final Set<VendorModel> vendors = new HashSet<>();
		vendors.add(vendor);
		activity.setVendors(vendors);
		final List<ActivitySaleStatusModel> saleStatusModels = activitySaleStatusReverseConverter
				.convertAll(activityDataForActivity.getSaleStatuses());
		activity.setSaleStatuses(saleStatusModels);
		activity.setActivityCategoryTypes(activityDataForActivity.getActivityCategoryTypes());
		activity.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		activity.setDestination(activityDataForActivity.getDestination());
		activity.setSellableOnOwn(activityDataForActivity.isSellableOnOwn());
		activity.setStockLevelType(activityDataForActivity.getStockLevelType());
		activity.setActivityProviderEmail(activityDataForActivity.getActivityProviderEmail());

		/* TaxGroup */
		ProductTaxGroup taxGroup = getProductTaxGroupMappingService().getProductTaxGroupMapping(
				activityDataForActivity.getApplyDMF(), activityDataForActivity.getApplyMRDT(),
				activityDataForActivity.getApplyGST(), activityDataForActivity.getApplyPST());
		if (Objects.isNull(taxGroup))
		{
			taxGroup = ProductTaxGroup.valueOf(CA_TAX_GROUP);
		}
		activity.setEurope1PriceFactory_PTG(taxGroup);
	}

	/**
	 * Gets the activity product.
	 *
	 * @param activityData   the activity data
	 * @param vendor
	 * @param catalogVersion
	 * @return the activity product
	 */
	protected ActivityProductModel getActivityProduct(final ActivitiesDataModel activityData, final VendorModel vendor,
			final CatalogVersionModel catalogVersion)
	{
		final String activityCode = activityData.getCode();
		ActivityProductModel activity;
		try
		{
			activity = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			final ActivityProductModel finalActivity = getModelService().create(ActivityProductModel.class);
			finalActivity.setCode(activityCode);
			activity = finalActivity;
		}
		return activity;
	}

	/**
	 * Gets the or create vendor.
	 *
	 * @param vendorName the vendor name
	 * @return the vendor
	 */
	protected VendorModel getOrCreateVendor(final String vendorName)
	{
		final String vendorCode = getCodeFromName(StringUtils.EMPTY, vendorName, StringUtils.EMPTY);
		VendorModel vendor = getVendorService().getVendor(vendorCode);
		if (Objects.isNull(vendor))
		{
			vendor = getModelService().create(VendorModel.class);
			vendor.setCode(vendorCode);
			vendor.setName(vendorName);
		}

		return vendor;
	}

	public void handleSuccess(final ActivitiesDataModel activityDataModel, final List<ItemModel> items)
	{
		if (CollectionUtils.isNotEmpty(items))
		{
			synchronizeProductCatalog(items);
			indexDeals();
		}
		activityDataModel.setStatus(DataImportProcessStatus.SUCCESS);
		activityDataModel.setStatusDescription(ACTIVITY_DATA_SUCCESS);
	}

	/**
	 * Index deals.
	 */
	public void indexDeals()
	{
		LOG.info("Begin solr indexing Product Catalog for deals");
		getSetupSolrIndexerService().executeSolrIndexerCronJob(BCFERRIES_DEAL_INDEX, false);
		LOG.info("End solr indexing Product Catalog for deals");
	}

	/**
	 * @return the activitiesDataService
	 */
	protected ActivitiesDataService getActivitiesDataService()
	{
		return activitiesDataService;
	}

	/**
	 * @param activitiesDataService the activitiesDataService to set
	 */
	@Required
	public void setActivitiesDataService(final ActivitiesDataService activitiesDataService)
	{
		this.activitiesDataService = activitiesDataService;
	}

	/**
	 * @return the vendorService
	 */
	protected VendorService getVendorService()
	{
		return vendorService;
	}

	/**
	 * @param vendorService the vendorService to set
	 */
	@Required
	public void setVendorService(final VendorService vendorService)
	{
		this.vendorService = vendorService;
	}

	/**
	 * @return the unitService
	 */
	protected UnitService getUnitService()
	{
		return unitService;
	}


	/**
	 * @param unitService the unitService to set
	 */
	@Required
	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}

	/**
	 * @return the productTaxGroupMappingService
	 */
	protected ProductTaxGroupMappingService getProductTaxGroupMappingService()
	{
		return productTaxGroupMappingService;
	}

	/**
	 * @param productTaxGroupMappingService the productTaxGroupMappingService to set
	 */
	@Required
	public void setProductTaxGroupMappingService(final ProductTaxGroupMappingService productTaxGroupMappingService)
	{
		this.productTaxGroupMappingService = productTaxGroupMappingService;
	}

	protected ActivitiesDataValidator getActivitiesDataValidator()
	{
		return activitiesDataValidator;
	}

	@Required
	public void setActivitiesDataValidator(final ActivitiesDataValidator activitiesDataValidator)
	{
		this.activitiesDataValidator = activitiesDataValidator;
	}

	protected SetupSolrIndexerService getSetupSolrIndexerService()
	{
		return setupSolrIndexerService;
	}

	@Required
	public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService)
	{
		this.setupSolrIndexerService = setupSolrIndexerService;
	}

	@Required
	public void setActivitySaleStatusReverseConverter(
			final Converter<ActivitySaleStatusDataModel, ActivitySaleStatusModel> activitySaleStatusReverseConverter)
	{
		this.activitySaleStatusReverseConverter = activitySaleStatusReverseConverter;
	}
}
