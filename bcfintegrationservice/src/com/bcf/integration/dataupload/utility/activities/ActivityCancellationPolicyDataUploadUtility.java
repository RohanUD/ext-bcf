/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.integration.dataupload.utility.activities;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationFeesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.core.activity.service.ActivityCancellationPolicyService;
import com.bcf.core.model.ActivityCancellationFeesModel;
import com.bcf.core.model.ActivityCancellationPolicyModel;
import com.bcf.core.model.product.ActivityProductModel;
import com.bcf.integration.dataupload.service.activities.ActivityCancellationPolicyDataService;
import com.bcf.integration.dataupload.utility.AbstractDataUploadUtility;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityCancellationFeesDataValidator;
import com.bcf.integration.dataupload.utility.validator.activity.ActivityCancellationPolicyDataValidator;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivityCancellationPolicyDataUploadUtility extends AbstractDataUploadUtility
{
	private ActivityCancellationPolicyDataService activityCancellationPolicyDataService;
	private ActivityCancellationPolicyDataValidator activityCancellationPolicyDataValidator;
	private ActivityCancellationPolicyService activityCancellationPolicyService;
	private ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator;

	protected static final Logger LOG = Logger.getLogger(ActivityCancellationPolicyDataUploadUtility.class);

	public void uploadCancellationPolicyData()
	{

		final List<ActivityCancellationPolicyDataModel> pendingCancellationPolicyData = getActivityCancellationPolicyDataService()
				.getCancellationPolicyData(DataImportProcessStatus.PENDING);
		/* checking for invalid items */
		final List<ActivityCancellationPolicyDataModel> inValidCancellationPolicyDataList = pendingCancellationPolicyData.stream()
				.filter(cancellationPolicyData -> !validateActivityCancellationPolicy(cancellationPolicyData).isEmpty()
				)
				.collect(Collectors.toList());
		inValidCancellationPolicyDataList.forEach(cancellationPolicyData -> {
			cancellationPolicyData.setStatus(DataImportProcessStatus.FAILED);
			cancellationPolicyData.getActivityCancellationFeesData()
					.forEach(cancellationFees -> cancellationFees.setStatus(DataImportProcessStatus.FAILED));
		});
		getModelService().saveAll(inValidCancellationPolicyDataList);

		/* processing for valid items */

		final List<ActivityCancellationPolicyDataModel> activityCancellationPolicyList = pendingCancellationPolicyData.stream()
				.filter(cancellationPolicyData -> validateActivityCancellationPolicy(cancellationPolicyData).isEmpty())
				.collect(Collectors.toList());


		activityCancellationPolicyList.forEach(cancellationPolicyData -> {
			cancellationPolicyData.setStatus(DataImportProcessStatus.PROCESSING);
			cancellationPolicyData.getActivityCancellationFeesData()
					.forEach(cancellationFees -> cancellationFees.setStatus(DataImportProcessStatus.PROCESSING));

		});
		getModelService().saveAll(activityCancellationPolicyList);


		final List<ItemModel> items = new ArrayList<>();
		uploadCancellationPolicy(activityCancellationPolicyList, items);
		getModelService().saveAll();
		synchronizeProductCatalog(items);
		getModelService().saveAll(activityCancellationPolicyList);
	}

	private List<String> validateActivityCancellationPolicy(final ActivityCancellationPolicyDataModel cancellationPolicyData)
	{
		final List<String> invalidFieldList = getActivityCancellationPolicyDataValidator()
				.validateCancellationPolicyData(cancellationPolicyData);
		for (final ActivityCancellationFeesDataModel cancellationFeesData : cancellationPolicyData
				.getActivityCancellationFeesData())
		{
			invalidFieldList.addAll(getActivityCancellationFeesDataValidator().validateCancellationFeesData(cancellationFeesData));
		}
		return invalidFieldList;
	}


	public void uploadCancellationPolicy(final List<ActivityCancellationPolicyDataModel> activityCancellationPolicyDataList,
			final List<ItemModel> items)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		for (final ActivityCancellationPolicyDataModel cancellationPolicyData : activityCancellationPolicyDataList)
		{
			final ActivityProductModel activityProduct = getActivityProduct(cancellationPolicyData.getActivitiesData(),
					catalogVersion);
			final List<ActivityCancellationFeesDataModel> cancellationFeesDataForPolicyList = (List<ActivityCancellationFeesDataModel>) cancellationPolicyData
					.getActivityCancellationFeesData();
			if (Objects.nonNull(activityProduct))
			{
				final List<ActivityCancellationPolicyModel> activityCancellationPolicyList = CollectionUtils
						.isEmpty(activityProduct.getCancellationPolicies()) ?
						Collections.emptyList() :
						(List<ActivityCancellationPolicyModel>) activityProduct.getCancellationPolicies();
				final Map<String, ActivityCancellationPolicyModel> activityCancellationPolicyMap = getCancellationPolicyMap(
						activityCancellationPolicyList);
				final ActivityCancellationPolicyModel activityCancellationPolicyModel = getOrCreateCancellationPolicyModel(
						cancellationPolicyData, activityCancellationPolicyMap);
				activityCancellationPolicyModel.setCode(cancellationPolicyData.getCode());
				activityCancellationPolicyModel.setCatalogVersion(cancellationPolicyData.getCatalogVersion());
				getLocaleList().forEach(locale -> activityCancellationPolicyModel
						.setCancellationPolicyDescription(cancellationPolicyData.getCancellationPolicyDescription(locale), locale));
				activityCancellationPolicyModel.setStartDate(Objects.isNull(cancellationPolicyData.getStartDate()) ?
						cancellationPolicyData.getStartDate() :
						DateUtils.truncate(cancellationPolicyData.getStartDate(), Calendar.DATE));
				activityCancellationPolicyModel.setEndDate(Objects.isNull(cancellationPolicyData.getEndDate()) ?
						cancellationPolicyData.getEndDate() :
						DateUtils.truncate(cancellationPolicyData.getEndDate(), Calendar.DATE));
				activityCancellationPolicyModel.setActivityProduct(activityProduct);
				handleCancellationFees(cancellationFeesDataForPolicyList, activityCancellationPolicyModel, items);
				activityCancellationPolicyMap.put(cancellationPolicyData.getCode(), activityCancellationPolicyModel);
				items.add(activityCancellationPolicyModel);
				activityProduct.setCancellationPolicies(activityCancellationPolicyMap.values());
				cancellationPolicyData.setStatus(DataImportProcessStatus.SUCCESS);
				cancellationFeesDataForPolicyList
						.forEach(cancellationFeesData -> cancellationFeesData.setStatus(DataImportProcessStatus.SUCCESS));
				checkAndRemoveInvalidCancellationPolicy(cancellationPolicyData, activityProduct);
				items.add(cancellationPolicyData.getActivitiesData());
				items.add(activityProduct);
				getModelService().saveAll();
				getModelService().saveAll(cancellationPolicyData.getActivityCancellationFeesData());
				items.add(cancellationPolicyData);
				getModelService().save(cancellationPolicyData);
			}
			else
			{
				cancellationPolicyData.setStatus(DataImportProcessStatus.FAILED);
				cancellationFeesDataForPolicyList
						.forEach(cancellationFees -> cancellationFees.setStatus(DataImportProcessStatus.FAILED));
				getModelService().saveAll(cancellationPolicyData, cancellationFeesDataForPolicyList);
				items.add(cancellationPolicyData);
				cancellationFeesDataForPolicyList.stream()
						.forEach(activityCancellationFeesData -> items.add(activityCancellationFeesData));
			}
		}
	}


	private void checkAndRemoveInvalidCancellationPolicy(final ActivityCancellationPolicyDataModel cancellationPolicyData,
			final ActivityProductModel activityProduct)
	{
		final ActivityCancellationPolicyModel activityCancellationPolicyModel = getActivityCancellationPolicyService()
				.getActivityCancellationPolicyModel(cancellationPolicyData.getCode());
		if (Objects.nonNull(activityCancellationPolicyModel) && !activityCancellationPolicyModel.getActivityProduct().getCode()
				.equals(activityProduct.getCode()))
		{
			getModelService().remove(activityCancellationPolicyModel);
		}
	}

	private Map<String, ActivityCancellationPolicyModel> getCancellationPolicyMap(
			final List<ActivityCancellationPolicyModel> activityCancellationPolicyList)
	{
		final Map<String, ActivityCancellationPolicyModel> activityCancellationPolicyMap = new HashMap<>();
		activityCancellationPolicyList
				.forEach(cancellationPolicy -> activityCancellationPolicyMap.put(cancellationPolicy.getCode(), cancellationPolicy));
		return activityCancellationPolicyMap;
	}

	private void handleCancellationFees(final List<ActivityCancellationFeesDataModel> cancellationFeesDataForPolicyList,
			final ActivityCancellationPolicyModel cancellationPolicyModel, final List<ItemModel> items)
	{
		final List<ActivityCancellationFeesModel> activityCancellationFeesModelList = CollectionUtils
				.isEmpty(cancellationPolicyModel.getActivityCancellationFees()) ?
				Collections.emptyList() :
				(List<ActivityCancellationFeesModel>) cancellationPolicyModel.getActivityCancellationFees();
		final Map<String, ActivityCancellationFeesModel> activityCancellationFeesModelMap = getActivityCancellationFeesModelMap(
				activityCancellationFeesModelList);
		for (final ActivityCancellationFeesDataModel cancellationFeesData : cancellationFeesDataForPolicyList)
		{
			final ActivityCancellationFeesModel activityCancellationFeesModel = getOrCreateCancellationFeeModels(
					cancellationPolicyModel, cancellationFeesData, activityCancellationFeesModelMap);
			activityCancellationFeesModel.setCancellationFee(cancellationFeesData.getCancellationFee());
			activityCancellationFeesModel.setCancellationPolicy(cancellationPolicyModel);
			activityCancellationFeesModel.setCancellationFeeType(cancellationFeesData.getCancellationFeeType());
			activityCancellationFeesModel.setChangeFee(cancellationFeesData.getChangeFee());
			activityCancellationFeesModel.setChangeFeeType(cancellationFeesData.getChangeFeeType());
			activityCancellationFeesModel.setMaxDays(cancellationFeesData.getMaxDays());
			activityCancellationFeesModel.setMinDays(cancellationFeesData.getMinDays());
			activityCancellationFeesModelMap
					.put(cancellationPolicyModel.getCode().concat(String.valueOf(cancellationFeesData.getMinDays())).
							concat(String.valueOf(cancellationFeesData.getMaxDays())), activityCancellationFeesModel);
			cancellationPolicyModel.setActivityCancellationFees(activityCancellationFeesModelMap.values());
			cancellationFeesData.setStatus(DataImportProcessStatus.SUCCESS);
			items.add(activityCancellationFeesModel);
			items.add(cancellationFeesData);
		}
	}

	public void removeInvalidCancellationFees(final ActivitiesDataModel activitiesDataModel)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
		getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final String activityCode = activitiesDataModel.getCode();
		try
		{
			final ActivityProductModel activityProductModel = (ActivityProductModel) getProductService()
					.getProductForCode(catalogVersion, activityCode);
			final Map<String, List<String>> cancellationDataMap = createCancellationDataMap(activitiesDataModel);
			for (final ActivityCancellationPolicyModel cancellationPolicy : activityProductModel.getCancellationPolicies())
			{
				final List<String> cancellationFeesList = cancellationDataMap.get(cancellationPolicy.getCode());
				cancellationPolicy.getActivityCancellationFees().forEach(fees -> {
					final boolean result = cancellationFeesList
							.contains(fees.getCancellationPolicy().getCode().concat(fees.getMinDays().toString())
									.concat(String.valueOf(fees.getMaxDays())));
					if (!result)
					{
						getModelService().remove(fees);
					}
				});
			}
			getModelService().saveAll();
			getModelService().saveAll(activityProductModel.getCancellationPolicies());
			getModelService().save(activityProductModel);

		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			final String errorMessage = "ActivityProductModel not found for activityCode: " + activityCode;
			LOG.error(errorMessage);
			activitiesDataModel.setStatusDescription(errorMessage);
		}
	}

	private Map<String, List<String>> createCancellationDataMap(final ActivitiesDataModel activitiesDataModel)
	{
		final Map<String, List<String>> cancellationDataMap = new HashMap<>();
		for (final ActivityCancellationPolicyDataModel activityCancellationPolicyData : activitiesDataModel
				.getActivityCancellationPolicyData())
		{
			final List<String> cancellationFeesList = new ArrayList<>();
			for (final ActivityCancellationFeesDataModel cancellationFees : activityCancellationPolicyData
					.getActivityCancellationFeesData())
			{
				cancellationFeesList.add(cancellationFees.getActivityCancellationPolicyData().getCode()
						.concat(cancellationFees.getMinDays().toString())
						.concat(String.valueOf(cancellationFees.getMaxDays())));
			}
			cancellationDataMap.put(activityCancellationPolicyData.getCode(), cancellationFeesList);
		}
		return cancellationDataMap;
	}


	private Map<String, ActivityCancellationFeesModel> getActivityCancellationFeesModelMap(
			final List<ActivityCancellationFeesModel> activityCancellationFeesModelList)
	{
		final Map<String, ActivityCancellationFeesModel> activityCancellationFeesModelMap = new HashMap<>();
		activityCancellationFeesModelList.forEach(cancellationFees -> activityCancellationFeesModelMap.put(
				cancellationFees.getCancellationPolicy().getCode().concat(cancellationFees.getMinDays().toString())
						.concat(String.valueOf(cancellationFees.getMaxDays())), cancellationFees));
		return activityCancellationFeesModelMap;
	}

	private ActivityCancellationFeesModel getOrCreateCancellationFeeModels(
			final ActivityCancellationPolicyModel cancellationPolicyModel,
			final ActivityCancellationFeesDataModel cancellationFeesData,
			final Map<String, ActivityCancellationFeesModel> activityCancellationFeesModelMap)
	{

		final String mapKey = cancellationPolicyModel.getCode().concat(String.valueOf(cancellationFeesData.getMinDays()))
				.concat(String.valueOf(cancellationFeesData.getMaxDays()));
		return activityCancellationFeesModelMap.containsKey(mapKey) ?
				activityCancellationFeesModelMap.get(mapKey) :
				getModelService().create(ActivityCancellationFeesModel.class);

	}

	private ActivityCancellationPolicyModel getOrCreateCancellationPolicyModel(
			final ActivityCancellationPolicyDataModel cancellationPolicyData,
			final Map<String, ActivityCancellationPolicyModel> activityCancellationPolicyMap)
	{

		return activityCancellationPolicyMap.containsKey(cancellationPolicyData.getCode()) ?
				activityCancellationPolicyMap.get(cancellationPolicyData.getCode()) :
				getModelService().create(ActivityCancellationPolicyModel.class);

	}


	private ActivityProductModel getActivityProduct(final ActivitiesDataModel activitiesData,
			final CatalogVersionModel catalogVersion)
	{
		final String activityCode = activitiesData.getCode();
		ActivityProductModel activity = null;
		try
		{
			activity = (ActivityProductModel) getProductService().getProductForCode(catalogVersion, activityCode);
		}
		catch (final ModelNotFoundException | UnknownIdentifierException e)
		{
			LOG.error("No Activity Product found for the activity code", e);
		}
		return activity;
	}

	public ActivityCancellationPolicyDataService getActivityCancellationPolicyDataService()
	{
		return activityCancellationPolicyDataService;
	}

	public void setActivityCancellationPolicyDataService(
			final ActivityCancellationPolicyDataService activityCancellationPolicyDataService)
	{
		this.activityCancellationPolicyDataService = activityCancellationPolicyDataService;
	}

	public ActivityCancellationPolicyDataValidator getActivityCancellationPolicyDataValidator()
	{
		return activityCancellationPolicyDataValidator;
	}

	public void setActivityCancellationPolicyDataValidator(
			final ActivityCancellationPolicyDataValidator activityCancellationPolicyDataValidator)
	{
		this.activityCancellationPolicyDataValidator = activityCancellationPolicyDataValidator;
	}

	public ActivityCancellationFeesDataValidator getActivityCancellationFeesDataValidator()
	{
		return activityCancellationFeesDataValidator;
	}

	public void setActivityCancellationFeesDataValidator(
			final ActivityCancellationFeesDataValidator activityCancellationFeesDataValidator)
	{
		this.activityCancellationFeesDataValidator = activityCancellationFeesDataValidator;
	}

	public ActivityCancellationPolicyService getActivityCancellationPolicyService()
	{
		return activityCancellationPolicyService;
	}

	public void setActivityCancellationPolicyService(
			final ActivityCancellationPolicyService activityCancellationPolicyService)
	{
		this.activityCancellationPolicyService = activityCancellationPolicyService;
	}
}

