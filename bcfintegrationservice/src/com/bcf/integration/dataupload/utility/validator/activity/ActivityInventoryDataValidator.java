/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.bcfintegrationservice.model.utility.ActivityInventoryDataModel;
import com.bcf.core.enums.StockLevelType;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.integration.enums.DataImportProcessStatus;


public class ActivityInventoryDataValidator
{
	private static final String SPACE = " ";
	private static final String STOCK_LEVEL_TYPE_ERROR = "stockleveltype.error";

	private PropertySourceFacade propertySourceFacade;

	public List<String> validateActivityInventoryData(final ActivityInventoryDataModel activityInventoryData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (Objects.isNull(activityInventoryData.getAvailableQuantity()))
		{
			validationMessage.append("Available Quantity Code is empty. ");
			invalidFieldList.add(ActivityInventoryDataModel.AVAILABLEQUANTITY);
		}
		if (Objects.isNull(activityInventoryData.getStartDate()))
		{
			validationMessage.append("Start Date is empty. ");
			invalidFieldList.add(ActivityInventoryDataModel.STARTDATE);
		}
		if (!StockLevelType.STANDARD.equals(activityInventoryData.getActivitiesData().getStockLevelType()))
		{
			activityInventoryData.setActivitiesData(null);
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(STOCK_LEVEL_TYPE_ERROR) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.STOCKLEVELTYPE);
		}

		activityInventoryData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	public List<String> validateInventoryForStockTypeData(final ActivityInventoryDataModel activityInventoryData,
			final ActivitiesDataModel activityDataModel)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (!activityDataModel.getStockLevelType().equals(StockLevelType.STANDARD) && activityDataModel
				.getActivityInventoryDataList().stream()
				.filter(activityInventory -> activityInventory.getStatus().equals(DataImportProcessStatus.PENDING))
				.anyMatch(activityInventoryDataModel -> CollectionUtils
						.isNotEmpty(activityDataModel.getActivityInventoryDataList())))
		{
			final String message = getPropertySourceFacade().getPropertySourceValueMessage(STOCK_LEVEL_TYPE_ERROR, null);
			validationMessage.append(message);
			invalidFieldList.add(ActivityInventoryDataModel._TYPECODE);
		}

		activityInventoryData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
