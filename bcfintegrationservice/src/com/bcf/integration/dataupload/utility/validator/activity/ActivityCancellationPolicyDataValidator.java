/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityCancellationPolicyDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class ActivityCancellationPolicyDataValidator
{

	private static final Logger log = Logger.getLogger(ActivityPriceDataValidator.class);
	private static final String FOR = "for ";
	private static final String SPACE = " ";

	private static final String CANCELLATION_POLICY_DESCRIPTION_EMPTY = "cancellation.policy.description.empty";
	private static final String CODE_EMPTY = "code.empty";
	private static final String START_DATE_EMPTY = "start.date.empty";
	private static final String END_DATE_EMPTY = "end.date.empty";



	private PropertySourceFacade propertySourceFacade;

	public List<String> validateCancellationPolicyData(final ActivityCancellationPolicyDataModel cancellationPolicyData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (Objects.isNull(cancellationPolicyData.getCode()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CODE_EMPTY) + SPACE);
			invalidFieldList.add(ActivityCancellationPolicyDataModel.CODE);
		}
		if (Objects.isNull(cancellationPolicyData.getCancellationPolicyDescription(Locale.ENGLISH)))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CANCELLATION_POLICY_DESCRIPTION_EMPTY) + SPACE);
			invalidFieldList.add(ActivityCancellationPolicyDataModel.CANCELLATIONPOLICYDESCRIPTION);
		}
		if (Objects.isNull(cancellationPolicyData.getStartDate()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(START_DATE_EMPTY) + SPACE);
			invalidFieldList.add(ActivityCancellationPolicyDataModel.STARTDATE);
		}
		if (Objects.isNull(cancellationPolicyData.getEndDate()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(END_DATE_EMPTY) + SPACE);
			invalidFieldList.add(ActivityCancellationPolicyDataModel.ENDDATE);
		}
		if (validationMessage.length() > 0)
		{
			log.error(validationMessage + FOR + cancellationPolicyData.getCode());
		}

		cancellationPolicyData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

}
