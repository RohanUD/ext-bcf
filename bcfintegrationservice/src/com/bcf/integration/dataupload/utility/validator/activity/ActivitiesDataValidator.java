/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivitiesDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class ActivitiesDataValidator
{
	private static final Logger log = Logger.getLogger(ActivitiesDataValidator.class);
	private static final String AND = " and ";
	private static final String FOR = "for ";
	private static final String SPACE = " ";
	private static final String VENDOR_NAME_EMPTY = "vendor.name.empty";
	private static final String ACTIVITY_NAME_EMPTY = "activity.name.empty";
	private static final String ACTIVITY_TYPE_EMPTY = "activity.type.empty";
	private static final String STOCKLEVEL_TYPE_EMPTY = "stocklevel.type.empty";
	private static final String APPLY_DMF_EMPTY = "apply.dmf.empty";
	private static final String APPLY_MRDT_EMPTY = "apply.mrdt.empty";
	private static final String APPLY_GST_EMPTY = "apply.gst.empty";
	private static final String APPLY_PST_EMPTY = "apply.pst.empty";
	private static final String SELLABLE_ON_OWN_EMPTY = "sellable.on.own.empty";
	private static final String DESTINATION_EMPTY = "destination.empty";
	private static final String INVALID_EMAIL = "activityProviderEmail.invalid";
	private static final String EMAIL_EMPTY = "activityProviderEmail.empty";

	private static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

	private PropertySourceFacade propertySourceFacade;

	public List<String> validateActivityData(final ActivitiesDataModel activityData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (Objects.isNull(activityData.getVendorName(Locale.ENGLISH)))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(VENDOR_NAME_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.VENDORNAME);
		}
		if (Objects.isNull(activityData.getActivityName(Locale.ENGLISH)))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(ACTIVITY_NAME_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.ACTIVITYNAME);
		}
		if (Objects.isNull(activityData.getActivityCategoryTypes()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(ACTIVITY_TYPE_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.ACTIVITYCATEGORYTYPES);
		}
		if (Objects.isNull(activityData.getStockLevelType()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(STOCKLEVEL_TYPE_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.STOCKLEVELTYPE);
		}
		if (Objects.isNull(activityData.getApplyDMF()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_DMF_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.APPLYDMF);
		}
		if (Objects.isNull(activityData.getApplyMRDT()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_MRDT_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.APPLYMRDT);
		}
		if (Objects.isNull(activityData.getApplyGST()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_GST_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.APPLYGST);
		}
		if (Objects.isNull(activityData.getApplyPST()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_PST_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.APPLYPST);
		}
		if (Objects.isNull(activityData.isSellableOnOwn()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(SELLABLE_ON_OWN_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.SELLABLEONOWN);
		}
		if (Objects.isNull(activityData.getDestination()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(DESTINATION_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.DESTINATION);
		}
		if (StringUtils.isBlank(activityData.getActivityProviderEmail()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(EMAIL_EMPTY) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.ACTIVITYPROVIDEREMAIL);
		}
		if (StringUtils.isNotBlank(activityData.getActivityProviderEmail()) && !validateEmailAddress(
				activityData.getActivityProviderEmail()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(INVALID_EMAIL) + SPACE);
			invalidFieldList.add(ActivitiesDataModel.ACTIVITYPROVIDEREMAIL);
		}

		if (validationMessage.length() > 0)
		{
			log.error(validationMessage + FOR + activityData.getVendorName() + AND + activityData.getActivityName());
		}

		activityData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	public boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
