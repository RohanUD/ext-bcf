/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.accommodation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductPriceDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class ExtraProductPriceDataValidator
{
	private static final String SPACE = " ";
	private static final String PRICE_EMPTY = "price.empty";
	private static final String START_DATE_EMPTY = "start.date.empty";
	private static final String END_DATE_EMPTY = "end.date.empty";
	private static final String END_DATE_INVALID = "end.date.invalid";

	private PropertySourceFacade propertySourceFacade;

	public List<String> validateExtraProductPriceData(final ExtraProductPriceDataModel extraProductPriceDataModel)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (Objects.isNull(extraProductPriceDataModel.getCostPrice()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(PRICE_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductPriceDataModel.COSTPRICE);
		}

		if (Objects.isNull(extraProductPriceDataModel.getSellPrice()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(PRICE_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductPriceDataModel.SELLPRICE);
		}
		if (Objects.isNull(extraProductPriceDataModel.getStartDate()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(START_DATE_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductPriceDataModel.STARTDATE);
		}
		if (Objects.isNull(extraProductPriceDataModel.getEndDate()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(END_DATE_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductPriceDataModel.ENDDATE);
		}

		if ((!Objects.isNull(extraProductPriceDataModel.getStartDate()) && !Objects.isNull(extraProductPriceDataModel.getEndDate()))
				&& (extraProductPriceDataModel.getStartDate().compareTo(extraProductPriceDataModel.getEndDate()) > 0))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(END_DATE_INVALID) + SPACE);
			invalidFieldList.add(ExtraProductPriceDataModel.ENDDATE);
		}



		extraProductPriceDataModel.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
