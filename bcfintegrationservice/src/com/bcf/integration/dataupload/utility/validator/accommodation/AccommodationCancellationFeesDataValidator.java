/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.accommodation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationCancellationFeesDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class AccommodationCancellationFeesDataValidator
{
	private static final Logger log = Logger.getLogger(AccommodationCancellationFeesDataValidator.class);
	private static final String FOR = "for ";
	private static final String SPACE = " ";
	private static final String MIN_DAYS_EMPTY = "min.days.empty";
	private static final String MAX_DAYS_EMPTY = "max.days.empty";
	private static final String CANCELLATION_FEE_EMPTY = "cancellation.fee.empty";
	private static final String CANCELLATION_FEE_TYPE_EMPTY = "cancellation.fee.type.empty";
	private static final String CHANGE_FEE_EMPTY = "change.fee.empty";
	private static final String CHANGE_FEE_TYPE_EMPTY = "change.fee.type.empty";

	private PropertySourceFacade propertySourceFacade;

	public List<String> validateAccommodationCancellationFeesData(
			final AccommodationCancellationFeesDataModel accCancellationFeesData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();

		if (Objects.isNull(accCancellationFeesData.getMinDays()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(MIN_DAYS_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.MINDAYS);
		}
		if (Objects.isNull(accCancellationFeesData.getMaxDays()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(MAX_DAYS_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.MAXDAYS);
		}

		if (Objects.isNull(accCancellationFeesData.getCancellationFee()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CANCELLATION_FEE_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.CANCELLATIONFEE);
		}
		if (Objects.isNull(accCancellationFeesData.getCancellationFeeType()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CANCELLATION_FEE_TYPE_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.CANCELLATIONFEETYPE);
		}
		if (Objects.isNull(accCancellationFeesData.getChangeFee()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CHANGE_FEE_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.CHANGEFEE);
		}
		if (Objects.isNull(accCancellationFeesData.getChangeFeeType()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CHANGE_FEE_TYPE_EMPTY) + SPACE);
			invalidFieldList.add(AccommodationCancellationFeesDataModel.CHANGEFEETYPE);
		}
		if (validationMessage.length() > 0)
		{
			log.error(validationMessage + FOR + accCancellationFeesData.getAccommodationCancellationPolicyData().getCode());
		}

		accCancellationFeesData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
