/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility.validator.activity;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ActivityScheduleDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class ActivityScheduleDataValidator
{
	private static final Logger log = Logger.getLogger(ActivityScheduleDataValidator.class);
	private static final String AND = " and ";
	private static final String FOR = "for ";
	private static final String TIME_FORMAT = "HH:mm";

	private PropertySourceFacade propertySourceFacade;

	public List<String> validateActivityScheduleData(final ActivityScheduleDataModel activityScheduleData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();


		if (Objects.isNull(activityScheduleData.getActivitiesData()))
		{
			validationMessage.append("Activity Data is empty. ");
			invalidFieldList.add(ActivityScheduleDataModel.ACTIVITIESDATA);
		}
		if (Objects.isNull(activityScheduleData.getStartDate()))
		{
			validationMessage.append("Start Date is empty. ");
			invalidFieldList.add(ActivityScheduleDataModel.STARTDATE);
		}
		if (Objects.isNull(activityScheduleData.getEndDate()))
		{
			validationMessage.append("End date is empty. ");
			invalidFieldList.add(ActivityScheduleDataModel.ENDDATE);
		}

		validateStartTime(activityScheduleData.getStartTime(), validationMessage, invalidFieldList);

		if (Objects.isNull(activityScheduleData.getDuration()))
		{
			validationMessage.append("Duration is empty. ");
			invalidFieldList.add(ActivityScheduleDataModel.DURATION);
		}

		if (validationMessage.length() > 0)
		{
			log.error(
					validationMessage + FOR + activityScheduleData.getActivitiesData().getCode() + AND + activityScheduleData
							.getStartDate()
							+ AND + activityScheduleData.getEndDate() + AND + activityScheduleData.getStartTime());
		}

		activityScheduleData.setStatusDescription(validationMessage.toString());
		return invalidFieldList;
	}

	protected void validateStartTime(final String startTime, final StringBuilder validationMessage, final List<String> invalidFieldList)
	{
		if (Objects.isNull(startTime))
		{
			validationMessage.append("Start Time is empty. ");
			invalidFieldList.add(ActivityScheduleDataModel.STARTTIME);
			return;
		}

		try
		{
			final DateTimeFormatter strictTimeFormatter = DateTimeFormatter.ofPattern(TIME_FORMAT)
					.withResolverStyle(ResolverStyle.STRICT);
			LocalTime.parse(startTime, strictTimeFormatter);
		}
		catch (DateTimeParseException e)
		{
			validationMessage.append("Invalid time format ");
			invalidFieldList.add(ActivityScheduleDataModel.STARTTIME);
			validationMessage.append("Accepted time format is: " + TIME_FORMAT + " ");
			return;
		}
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
