/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.dataupload.utility.validator.accommodation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.ExtraProductDataModel;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class ExtraProductDataValidator
{
	private static final Logger log = Logger.getLogger(ExtraProductDataValidator.class);
	private static final String FOR = "for ";
	private static final String CODE_EMPTY = "code.empty";
	private static final String APPLY_DMF_EMPTY = "apply.dmf.empty";
	private static final String APPLY_MRDT_EMPTY = "apply.mrdt.empty";
	private static final String APPLY_GST_EMPTY = "apply.gst.empty";
	private static final String APPLY_PST_EMPTY = "apply.pst.empty";
	private static final String SPACE = " ";
	private PropertySourceFacade propertySourceFacade;

	public List<String> validateExtraProductData(final ExtraProductDataModel extraProductData)
	{
		final StringBuilder validationMessage = new StringBuilder();
		final List<String> invalidFieldList = new ArrayList<>();


		if (Objects.isNull(extraProductData.getCode()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(CODE_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductDataModel.CODE);
		}
		if (Objects.isNull(extraProductData.getApplyDMF()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_DMF_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductDataModel.APPLYDMF);
		}
		if (Objects.isNull(extraProductData.getApplyMRDT()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_MRDT_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductDataModel.APPLYMRDT);
		}
		if (Objects.isNull(extraProductData.getApplyGST()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_GST_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductDataModel.APPLYGST);
		}
		if (Objects.isNull(extraProductData.getApplyPST()))
		{
			validationMessage.append(getPropertySourceFacade().getPropertySourceValue(APPLY_PST_EMPTY) + SPACE);
			invalidFieldList.add(ExtraProductDataModel.APPLYPST);
		}
		if (validationMessage.length() > 0)
		{
			log.error(validationMessage + FOR + extraProductData.getCode());
		}

		return invalidFieldList;
	}

	protected PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
