/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.dataupload.utility;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.catalog.synchronization.CatalogSynchronizationService;
import de.hybris.platform.catalog.synchronization.SyncConfig;
import de.hybris.platform.catalog.synchronization.SyncResult;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.JobLogLevel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.travelservices.model.accommodation.MarketingRatePlanInfoModel;
import de.hybris.platform.travelservices.model.accommodation.RatePlanModel;
import de.hybris.platform.travelservices.model.accommodation.RoomRateProductModel;
import de.hybris.platform.travelservices.model.product.AccommodationModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.services.AccommodationOfferingService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.model.utility.AccommodationOfferingDataModel;
import com.bcf.bcfintegrationservice.model.utility.SaleStatusDataModel;
import com.bcf.core.accommodation.service.PriceRowService;
import com.bcf.core.model.SaleStatusModel;
import com.bcf.core.service.TaxRowService;
import com.bcf.integration.catalogsync.service.CatalogVersionSyncJobService;
import com.bcf.model.product.ExtraGuestOccupancyProductModel;


public class AbstractDataUploadUtility
{
	private static final Logger log = Logger.getLogger(AbstractDataUploadUtility.class);
	public static final String SPACE = " ";
	public static final String UNDERSCORE = "_";
	public static final String S = "S";
	public static final String P = "P";
	public static final String PRODUCTCATALOG = "bcfProductCatalog";
	public static final String CATALOG_VERSION = CatalogManager.OFFLINE_VERSION;
	private static final String CATALOGSYNC_JOB_CODE = "sync " + PRODUCTCATALOG + ":Staged->Online";
	public static final String UNIT = "pieces";

	private ModelService modelService;
	private ProductService productService;
	private CommonI18NService commonI18NService;
	private CatalogVersionService catalogVersionService;
	private CatalogVersionSyncJobService catalogVersionSyncJobService;
	private CatalogSynchronizationService catalogSynchronizationService;
	private List<Locale> localeList;
	private AccommodationOfferingService accommodationOfferingService;
	private TaxRowService taxRowService;
	private PriceRowService priceRowService;

	/**
	 * Synchronize product catalog.
	 *
	 * @return true, if successful
	 */
	public boolean synchronizeProductCatalog(final List<ItemModel> items)
	{
		log.info(String.format("Begin synchronizing Product Catalog [%s]", PRODUCTCATALOG));
		final CatalogVersionSyncJobModel catalogVersionSyncJob = getCatalogVersionSyncJobService()
				.getCatalogVersionSyncJob(CATALOGSYNC_JOB_CODE);

		final SyncConfig syncConfig = new SyncConfig();
		syncConfig.setCreateSavedValues(Boolean.TRUE);
		syncConfig.setForceUpdate(Boolean.TRUE);
		syncConfig.setLogLevelDatabase(JobLogLevel.WARNING);
		syncConfig.setLogLevelFile(JobLogLevel.WARNING);
		syncConfig.setLogToFile(Boolean.TRUE);
		syncConfig.setLogToDatabase(Boolean.FALSE);
		syncConfig.setSynchronous(Boolean.FALSE);

		final SyncResult syncResult = getCatalogSynchronizationService()
				.performSynchronization(items, catalogVersionSyncJob, syncConfig);
		if (Objects.nonNull(syncResult) && (syncResult.isFinished() && syncResult.isSuccessful()))
		{
			log.info(String.format("Product Catalog [%s] sync finished successfully.", PRODUCTCATALOG));
			return true;
		}

		log.error(String.format("Product Catalog [%s] sync has issues.", PRODUCTCATALOG));
		log.info(String.format("End synchronizing Product Catalog [%s]", PRODUCTCATALOG));
		return false;
	}

	protected List<SaleStatusModel> createOrGetSaleStatues(final Collection<SaleStatusDataModel> saleStatusDatas,
			final Collection<SaleStatusModel> existingSaleStatuses)
	{
		if (CollectionUtils.isEmpty(saleStatusDatas))
		{
			return Collections.emptyList();
		}

		final List<SaleStatusModel> saleStatuses = new ArrayList<>();
		final List<SaleStatusModel> saleStatusesToSave = new ArrayList<>();
		for (final SaleStatusDataModel saleStatusData : saleStatusDatas)
		{
			SaleStatusModel saleStatus = getSaleStatus(saleStatusData, existingSaleStatuses);
			if (Objects.isNull(saleStatus))
			{
				saleStatus = getModelService().create(SaleStatusModel.class);
				saleStatus.setSaleStatusType(saleStatusData.getSaleStatusType());
				saleStatus.setStartDate(saleStatusData.getStartDate());
				saleStatus.setEndDate(saleStatusData.getEndDate());
				saleStatusesToSave.add(saleStatus);
			}
			saleStatuses.add(saleStatus);
		}
		getModelService().saveAll(saleStatusesToSave);

		return saleStatuses;
	}

	private SaleStatusModel getSaleStatus(final SaleStatusDataModel saleStatusData,
			final Collection<SaleStatusModel> existingSaleStatuses)
	{
		final SaleStatusModel saleStatus = null;
		if (CollectionUtils.isNotEmpty(existingSaleStatuses))
		{
			existingSaleStatuses.stream().filter(
					saleStatusModel -> Objects.equals(saleStatusModel.getSaleStatusType(), saleStatusData.getSaleStatusType())
							&& DateUtils.isSameDay(saleStatusData.getStartDate(), saleStatusModel.getStartDate()) && DateUtils
							.isSameDay(saleStatusData.getEndDate(), saleStatusModel.getEndDate())).findAny()
					.orElse(null);
		}
		return saleStatus;

	}

	protected void collectAccommodationOfferingRelatedItems(final AccommodationOfferingDataModel accommodationOfferingData,
			final List<ItemModel> items)
	{
		if (accommodationOfferingData.isPublish())
		{
			final AccommodationOfferingModel accommodationOffering = getAccommodationOfferingService()
					.getAccommodationOffering(accommodationOfferingData.getCode());
			if (Objects.nonNull(accommodationOffering))
			{
				final List<MarketingRatePlanInfoModel> marketingRatePlanInfos = (List) accommodationOffering
						.getMarketingRatePlanInfos();
				final List<MarketingRatePlanInfoModel> filteredMarketingRatePlanInfos = marketingRatePlanInfos.stream().filter(
						marketingRatePlanInfo -> CatalogManager.OFFLINE_VERSION
								.equals(marketingRatePlanInfo.getCatalogVersion().getVersion()))
						.collect(Collectors.toList());
				filteredMarketingRatePlanInfos.stream().forEach(marketingRatePlanInfo -> items.add(marketingRatePlanInfo));
				final Collection<TaxRowModel> taxRows = getTaxRowService().getTaxRow(accommodationOffering.getLocation());
				taxRows.stream().forEach(taxRow -> items.add(taxRow));
				collectAccommodationRelatedItems(accommodationOffering, items);
			}
		}
	}

	private void collectAccommodationRelatedItems(final AccommodationOfferingModel accommodationOffering,
			final List<ItemModel> items)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionModel();
		final List<String> accommodationCodes = accommodationOffering.getAccommodations();
		for (final String accommodationCode : accommodationCodes)
		{
			final AccommodationModel accommodation = (AccommodationModel) getProductService()
					.getProductForCode(catalogVersion, accommodationCode);
			if (Objects.nonNull(accommodation))
			{
				items.add(accommodation);
				items.addAll(accommodation.getRatePlanConfigs());
				collectRatePlanRelatedItems(accommodation, items);
			}
			final Collection<ExtraGuestOccupancyProductModel> extraGuestOccupancyProducts = accommodation.getExtraGuestProducts();
			if (CollectionUtils.isNotEmpty(extraGuestOccupancyProducts))
			{
				for (final ExtraGuestOccupancyProductModel s : extraGuestOccupancyProducts)
				{
					items.add(s);
					getPriceRowService().getPriceRow(s).stream()
							.forEach(priceRow -> items.add(priceRow));
				}
			}
		}
	}

	private void collectRatePlanRelatedItems(final AccommodationModel accommodation, final List<ItemModel> items)
	{
		if (Objects.nonNull(accommodation.getRatePlan()))
		{
			final List<RatePlanModel> ratePlans = (List) accommodation.getRatePlan();
			for (final RatePlanModel ratePlan : ratePlans)
			{
				items.add(ratePlan);
				collectProductRelatedItems(ratePlan, items);
			}
		}
	}

	private void collectProductRelatedItems(final RatePlanModel ratePlan, final List<ItemModel> items)
	{
		if (Objects.nonNull(ratePlan.getProducts()))
		{
			final List<ProductModel> products = ratePlan.getProducts();
			for (final ProductModel product : products)
			{
				if (product instanceof RoomRateProductModel)
				{
					final RoomRateProductModel roomRateProduct = (RoomRateProductModel) product;
					items.add(roomRateProduct);
					getPriceRowService().getPriceRow(roomRateProduct).stream()
							.forEach(priceRow -> items.add(priceRow));
				}
			}
		}
	}

	public CatalogVersionModel getCatalogVersionModel()
	{
		CatalogVersionModel catalogVersionModel = getCatalogVersionService().getSessionCatalogVersionForCatalog(PRODUCTCATALOG);
		if (Objects.isNull(catalogVersionModel))
		{
			catalogVersionModel = getCatalogVersionService().getCatalogVersion(PRODUCTCATALOG, CATALOG_VERSION);
			getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catalogVersionModel));
		}
		return catalogVersionModel;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the catalogSynchronizationService
	 */
	protected CatalogSynchronizationService getCatalogSynchronizationService()
	{
		return catalogSynchronizationService;
	}

	/**
	 * @param catalogSynchronizationService the catalogSynchronizationService to set
	 */
	@Required
	public void setCatalogSynchronizationService(final CatalogSynchronizationService catalogSynchronizationService)
	{
		this.catalogSynchronizationService = catalogSynchronizationService;
	}

	/**
	 * @return the catalogVersionSyncJobService
	 */
	protected CatalogVersionSyncJobService getCatalogVersionSyncJobService()
	{
		return catalogVersionSyncJobService;
	}

	/**
	 * @param catalogVersionSyncJobService the catalogVersionSyncJobService to set
	 */
	@Required
	public void setCatalogVersionSyncJobService(final CatalogVersionSyncJobService catalogVersionSyncJobService)
	{
		this.catalogVersionSyncJobService = catalogVersionSyncJobService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected List<Locale> getLocaleList()
	{
		return localeList;
	}

	@Required
	public void setLocaleList(final List<Locale> localeList)
	{
		this.localeList = localeList;
	}

	protected AccommodationOfferingService getAccommodationOfferingService()
	{
		return accommodationOfferingService;
	}

	@Required
	public void setAccommodationOfferingService(
			final AccommodationOfferingService accommodationOfferingService)
	{
		this.accommodationOfferingService = accommodationOfferingService;
	}

	protected TaxRowService getTaxRowService()
	{
		return taxRowService;
	}

	@Required
	public void setTaxRowService(final TaxRowService taxRowService)
	{
		this.taxRowService = taxRowService;
	}

	protected PriceRowService getPriceRowService()
	{
		return priceRowService;
	}

	@Required
	public void setPriceRowService(final PriceRowService priceRowService)
	{
		this.priceRowService = priceRowService;
	}
}
