/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.currentconditions.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.CurrentConditionRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.currentconditions.service.CurrentConditionsIntegrationService;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCurrentConditionsIntegrationService implements CurrentConditionsIntegrationService
{

	private static final String CURRENT_CONDITIONS_BY_TERMINAL_CODE = "terminalCode";
	private static final String CURRENT_CONDITIONS_MULTI_TERMINAL_CODE = "term";
	private static final String CURRENT_CONDITIONS_ROUTE = "route";
	private CurrentConditionRestService currentConditionsRestService;

	@Override
	public CurrentConditionsResponseData getCurrentConditionsForTerminal(final String terminalCode) throws IntegrationException
	{
		CurrentConditionsResponseData currentConditionsResponseData = null;

		if (Objects.nonNull(terminalCode))
		{
			currentConditionsResponseData = (CurrentConditionsResponseData) getCurrentConditionsRestService()
					.sendRestRequest(null, CurrentConditionsResponseData.class,
							BcfintegrationserviceConstants.CURRENT_CONDITIONS_TERMINAL_URL, HttpMethod.GET
							, getParamMapForTermnal(terminalCode));
		}
		return currentConditionsResponseData;
	}

	@Override
	public CurrentConditionsResponseData getCurrentConditionsForRoute(final String terminalCode, final String route)
			throws IntegrationException
	{
		CurrentConditionsResponseData currentConditionsResponseData = null;

		if (Objects.nonNull(terminalCode))
		{
			currentConditionsResponseData = (CurrentConditionsResponseData) getCurrentConditionsRestService()
					.sendRestRequest(null, CurrentConditionsResponseData.class,
							BcfintegrationserviceConstants.CURRENT_CONDITIONS_ROUTE_URL, HttpMethod.GET
							, getParamMapForTerminalAndRoute(terminalCode, route));
		}
		return currentConditionsResponseData;
	}

	@Override
	public CurrentConditionsResponseData[] getCurrentConditionsForTerminals(final List<String> terminals)
			throws IntegrationException
	{
		CurrentConditionsResponseData[] currentConditionsResponseData = null;

		if (!CollectionUtils.isEmpty(terminals))
		{
			currentConditionsResponseData = (CurrentConditionsResponseData[]) getCurrentConditionsRestService()
					.sendRestRequestWithArrayResponse(null, CurrentConditionsResponseData[].class,
							BcfintegrationserviceConstants.CURRENT_CONDITIONS_MULTI_TERMINAL_URL, HttpMethod.GET
							, getParamMapForTerminals(terminals));
		}
		return currentConditionsResponseData;
	}

	private Map<String, Object> getParamMapForTermnal(final String terminalCode)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		keyMap.put(CURRENT_CONDITIONS_BY_TERMINAL_CODE, terminalCode);
		return keyMap;
	}

	private Map<String, Object> getParamMapForTerminalAndRoute(final String terminalCode, final String route)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		keyMap.put(CURRENT_CONDITIONS_MULTI_TERMINAL_CODE, terminalCode);
		keyMap.put(CURRENT_CONDITIONS_ROUTE, route);
		return keyMap;
	}

	private Map<String, List<String>> getParamMapForTerminals(final List<String> terminalCodes)
	{
		final Map<String, List<String>> keyMap = new HashMap<>();
		keyMap.put(CURRENT_CONDITIONS_MULTI_TERMINAL_CODE, terminalCodes);
		return keyMap;
	}

	public CurrentConditionRestService getCurrentConditionsRestService()
	{
		return currentConditionsRestService;
	}

	public void setCurrentConditionsRestService(final CurrentConditionRestService currentConditionsRestService)
	{
		this.currentConditionsRestService = currentConditionsRestService;
	}
}
