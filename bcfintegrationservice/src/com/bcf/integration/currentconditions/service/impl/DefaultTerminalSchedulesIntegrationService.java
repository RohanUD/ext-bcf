package com.bcf.integration.currentconditions.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.CurrentConditionRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.currentconditions.service.TerminalSchedulesIntegrationService;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultTerminalSchedulesIntegrationService implements TerminalSchedulesIntegrationService
{
	private static final String ROUTE = "route";
	private static final String CODE = "code";
	private static final String SCHEDULED_DATE = "scheduledDate";
	private static final String LIMIT = "limit";
	private CurrentConditionRestService terminalSchedulesRestService;

	@Override
	public ScheduleArrivalDepartureData[] getCurrentConditionsNextSailingsForRoute(final String terminalCode,
			final String route,
			final String scheuledDate, final String limit) throws IntegrationException
	{
		ScheduleArrivalDepartureData[] currentConditionsNextSailingsResponseData = null;

		if (StringUtils.isNotEmpty(terminalCode) && StringUtils.isNotEmpty(route))
		{
			currentConditionsNextSailingsResponseData = (ScheduleArrivalDepartureData[]) getTerminalSchedulesRestService()
					.sendRestRequestForNextSailingsWithArrayResponse(null, ScheduleArrivalDepartureData[].class, HttpMethod.GET,
							BcfintegrationserviceConstants.CURRENT_CONDITIONS_NEXT_SAILINGS_ROUTE_URL, new HttpHeaders()
							, getParamMapForNextSailingsForRoute(terminalCode, route, scheuledDate, limit));
		}
		return currentConditionsNextSailingsResponseData;
	}

	@Override
	public Map<String, Map<String, List<TerminalSchedulesRouteData>>> getScheduleForTerminals(final List<String> terminalCode,
			final String scheuledDate, final String limit) throws IntegrationException
	{
		Map<String, Map<String, List<TerminalSchedulesRouteData>>> terminalsSchedule = new HashMap<String, Map<String, List<TerminalSchedulesRouteData>>>();

		if (CollectionUtils.isNotEmpty(terminalCode))
		{
			terminalsSchedule = getTerminalSchedulesRestService()
					.sendRestRequestForTerminalsWithMapResponse(null, terminalsSchedule.getClass(), HttpMethod.GET,
							BcfintegrationserviceConstants.CURRENT_CONDITIONS_NEXT_SAILINGS_MULTI_TERMINAL_URL, new HttpHeaders()
							, getParamMapForTerminals(terminalCode, scheuledDate, limit));
		}
		return terminalsSchedule;
	}

	private Map<String, Object> getParamMapForNextSailingsForRoute(final String terminalCode, final String route,
			final String scheduledDate, final String limit)
	{
		final Map<String, Object> keyMap = new HashMap<>();
		keyMap.put(CODE, terminalCode);
		keyMap.put(ROUTE, route);
		keyMap.put(SCHEDULED_DATE, scheduledDate);
		keyMap.put(LIMIT, limit);
		return keyMap;
	}

	private Map<String, List<String>> getParamMapForTerminals(final List<String> terminalCode, final String scheduledDate,
			final String limit)
	{
		final Map<String, List<String>> keyMap = new HashMap<>();
		keyMap.put(CODE, terminalCode);
		keyMap.put(SCHEDULED_DATE, Collections.singletonList(scheduledDate));
		keyMap.put(LIMIT, Collections.singletonList(limit));
		return keyMap;
	}

	public CurrentConditionRestService getTerminalSchedulesRestService()
	{
		return terminalSchedulesRestService;
	}

	public void setTerminalSchedulesRestService(final CurrentConditionRestService terminalSchedulesRestService)
	{
		this.terminalSchedulesRestService = terminalSchedulesRestService;
	}
}
