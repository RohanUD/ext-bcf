/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.currentconditions.service;

import java.util.List;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface CurrentConditionsIntegrationService
{
	CurrentConditionsResponseData getCurrentConditionsForTerminal(String terminalCode) throws IntegrationException;

	CurrentConditionsResponseData getCurrentConditionsForRoute(String terminalCode, String route)
			throws IntegrationException;

	CurrentConditionsResponseData[] getCurrentConditionsForTerminals(List<String> terminals) throws IntegrationException;
}
