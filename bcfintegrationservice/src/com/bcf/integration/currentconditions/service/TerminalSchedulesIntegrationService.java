package com.bcf.integration.currentconditions.service;

import java.util.List;
import java.util.Map;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integrations.core.exception.IntegrationException;


public interface TerminalSchedulesIntegrationService
{
	ScheduleArrivalDepartureData[] getCurrentConditionsNextSailingsForRoute(String terminalCode, String route,
			String scheuledDate, String limit) throws IntegrationException;

	Map<String, Map<String, List<TerminalSchedulesRouteData>>> getScheduleForTerminals(List<String> terminalCode,
			String scheuledDate, String limit) throws IntegrationException;
}
