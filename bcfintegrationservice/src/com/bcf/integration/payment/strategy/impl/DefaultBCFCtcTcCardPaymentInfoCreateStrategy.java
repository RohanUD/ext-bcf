/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.strategy.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.user.CustomerModel;
import java.util.Map;
import javax.annotation.Resource;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.core.services.strategies.BcfCtcTcCardPaymentInfoCreateStrategy;
import com.bcf.core.user.BcfUserService;
import com.bcf.travelb2bfacades.checkout.b2bcontext.B2BCheckoutContextData;


public class DefaultBCFCtcTcCardPaymentInfoCreateStrategy extends DefaultBCFCreditCardPaymentInfoCreateStrategy implements
		BcfCtcTcCardPaymentInfoCreateStrategy
{

	@Resource(name = "bcfDefaultUserService")
	private BcfUserService bcfUserService;

	@Override
	public CTCTCCardPaymentInfoModel saveAccountPaymentDetails(final CustomerModel customerModel,
			final Map<String, String> resultMap)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = createCtcTcCardPaymentInfo(resultMap, customerModel);
		return ctcTcCardPaymentInfoModel;
	}

	private CTCTCCardPaymentInfoModel createCtcTcCardPaymentInfo(final Map<String, String> resultMap,
			final CustomerModel customerModel)
	{
		final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = getModelService().create(CTCTCCardPaymentInfoModel.class);
		ctcTcCardPaymentInfoModel.setCardNumber(resultMap.get("cardNumber"));
		ctcTcCardPaymentInfoModel.setCode(resultMap.get("ctcCardNo"));
		ctcTcCardPaymentInfoModel.setUser(customerModel);
		ctcTcCardPaymentInfoModel.setSaved(true);

		if (bcfUserService.isB2BCustomer())
		{
			final B2BCheckoutContextData b2bCheckoutContextData = getSessionService()
					.getAttribute(BcfCoreConstants.B2B_CHECKOUT_CONTEXT_DATA);
			ctcTcCardPaymentInfoModel.setB2bUnit(b2bCheckoutContextData.getB2bUnit());
		}
		return ctcTcCardPaymentInfoModel;
	}
}
