/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultPaymentTransactionStrategy;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.services.strategies.BcfCTCCardPaymentInfoCreateStrategy;
import com.bcf.core.services.strategies.BcfCashPaymentInfoCreateStrategy;
import com.bcf.core.services.strategies.BcfCreditCardPaymentInfoCreateStrategy;
import com.bcf.core.services.strategies.BcfGiftCardPaymentInfoCreateStrategy;
import com.bcf.core.services.strategies.BcfPaymentTransactionStrategy;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;


public class DefaultBcfPaymentTransactionStrategy extends DefaultPaymentTransactionStrategy
		implements BcfPaymentTransactionStrategy
{
	public static final String AMERICA_VANCOUVER = "America/Vancouver";
	private static final Logger LOG = Logger.getLogger(DefaultBcfPaymentTransactionStrategy.class);
	private static final String ASM_SESSION_PARAMETER = "ASM";
	public static final String PAYMENT_TYPE = "paymentType";
	private static final String DEPOSIT_AMOUNT = "depositAmount";

	private BcfCashPaymentInfoCreateStrategy bcfCashPaymentInfoCreateStrategy;
	private BcfCreditCardPaymentInfoCreateStrategy bcfCreditCardPaymentInfoCreateStrategy;
	private BcfGiftCardPaymentInfoCreateStrategy bcfGiftCardPaymentInfoCreateStrategy;
	private BcfCTCCardPaymentInfoCreateStrategy bcfCTCCardPaymentInfoCreateStrategy;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private SessionService sessionService;

	@Override
	public boolean createPaymentTransactions(final CustomerModel customerModel, final TransactionDetails transactionDetails,
			final PaymentTransactionType paymentTransactionType, final BCFPaymentMethodType bcfPaymentMethodType,
			final AbstractOrderModel abstractOrderModel,
			final Map<String, String> resultMap, final String refundReason, final boolean newTransaction)
	{
		final PaymentTransactionModel transaction = getOrCreatePaymentTransaction(customerModel, paymentTransactionType,
				abstractOrderModel, bcfPaymentMethodType, newTransaction);
		return createPaymentTransactionEntry(customerModel, transactionDetails, paymentTransactionType, bcfPaymentMethodType,
				abstractOrderModel, resultMap, transaction, refundReason);
	}

	@Override
	public boolean createPaymentTransactionEntry(final CustomerModel customerModel, final TransactionDetails transactionDetails,
			final PaymentTransactionType paymentTransactionType, final BCFPaymentMethodType bcfPaymentMethodType,
			final AbstractOrderModel abstractOrderModel,
			final Map<String, String> resultMap, final PaymentTransactionModel transaction, final String refundReason)
	{
		final List<ItemModel> itemsToSave = new ArrayList<>();
		final Set<PaymentTransactionModel> transactions = CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions())
				? new HashSet<>()
				: new HashSet<>(abstractOrderModel.getPaymentTransactions());

		PaymentTransactionEntryModel transactionEntry = null;
		if (!StringUtils.equals(resultMap.get(BcfCoreConstants.ON_REQUEST_ORDER), BcfCoreConstants.PACKAGE_ON_REQUEST))
		{
			transactionEntry = createPaymentTransactionEntry(paymentTransactionType,
					transactionDetails, bcfPaymentMethodType);

			transactionEntry.setCode(getPaymentService().getNewPaymentTransactionEntryCode(transaction, paymentTransactionType));
			transactionEntry.setRefundReason(refundReason);
			final List<PaymentTransactionEntryModel> transactionEntries = CollectionUtils.isEmpty(transaction.getEntries())
					? new ArrayList<>()
					: new ArrayList<>(transaction.getEntries());
			transactionEntries.add(transactionEntry);
			transaction.setEntries(transactionEntries);
		}

		if (MapUtils.isNotEmpty(resultMap))
		{
			final String paymentType = resultMap.get(PAYMENT_TYPE);
			if (StringUtils.isBlank(paymentType))
			{
				resultMap.put(PAYMENT_TYPE, bcfPaymentMethodType.getCode());
			}
			updateTransactionWithPaymentInfo(customerModel, abstractOrderModel, transaction, resultMap);
		}

		if (StringUtils.isNotBlank(resultMap.get(DEPOSIT_AMOUNT)))
		{
			transactionEntry.setDepositPayment(true);
		}

		transactions.add(transaction);
		itemsToSave.add(transaction);
		if (transactionEntry != null)
		{
			itemsToSave.add(transactionEntry);
		}
		abstractOrderModel.setPaymentTransactions(transactions.stream().collect(Collectors.toList()));
		itemsToSave.add(abstractOrderModel);
		getModelService().saveAll(itemsToSave);

		return true;
	}



	protected PaymentTransactionModel getOrCreatePaymentTransaction(final CustomerModel customerModel,
			final PaymentTransactionType paymentTransactionType,
			final AbstractOrderModel abstractOrderModel, final BCFPaymentMethodType bcfPaymentMethodType,
			final boolean newTransaction)
	{
		/**
		 * TODO the below block needs to be verified as it is wrtitten only for credit card payment info
		 */
		if (newTransaction)
		{
			return createPaymentTransaction(customerModel, abstractOrderModel);
		}

		if (CollectionUtils.isNotEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			final PaymentTransactionEntryModel transactionEntry = abstractOrderModel.getPaymentTransactions().stream()
					.flatMap(transaction -> transaction.getEntries().stream()).filter(entry -> bcfPaymentMethodType.equals(
							entry.getTransactionType())).
							findAny().orElse(null);
			if (Objects.nonNull(transactionEntry) && !transactionEntry.isExisting())
			{
				return transactionEntry.getPaymentTransaction();
			}
		}
		return createPaymentTransaction(customerModel, abstractOrderModel);
	}

	private Boolean isRefundTransaction(final AbstractOrderModel abstractOrderModel)
	{
		return abstractOrderModel.getPaymentTransactions().stream().
				anyMatch(transactionModel -> transactionModel.getEntries().stream().
						anyMatch(transactionEntryModel ->
								transactionEntryModel.getType().equals(PaymentTransactionType.REFUND)));
	}

	private PaymentTransactionModel createPaymentTransaction(final CustomerModel customerModel,
			final AbstractOrderModel abstractOrderModel)
	{
		final PaymentTransactionModel transaction = getModelService().create(PaymentTransactionModel.class);
		transaction.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
		transaction.setPaymentProvider(getCommerceCheckoutService().getPaymentProvider());
		transaction.setPlannedAmount(new BigDecimal(0));
		transaction.setInfo(abstractOrderModel.getPaymentInfo());
		return transaction;
	}

	protected PaymentTransactionEntryModel createPaymentTransactionEntry(final PaymentTransactionType paymentTransactionType,
			final TransactionDetails transactionDetails, final BCFPaymentMethodType bcfPaymentMethodType)
	{
		final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
		final SalesApplication salesApplication = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		if ((SalesApplication.TRAVELCENTRE.equals(salesApplication)) || (SalesApplication.CALLCENTER.equals(salesApplication)))
		{
			final AssistedServiceSession asmSession = getSessionService().getAttribute(
					ASM_SESSION_PARAMETER);
			if (asmSession.getAgent() != null)
			{
				entry.setAgent(asmSession.getAgent());
			}
		}
		entry.setSalesApplication(salesApplication);
		if (StringUtils.isNotBlank(transactionDetails.getTransactionTimeStamp()))
		{
			final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BcfintegrationserviceConstants.TRANS_TIMESTAMP);
			try
			{
				entry.setTime(simpleDateFormat.parse(transactionDetails.getTransactionTimeStamp()));
			}
			catch (final ParseException e)
			{
				LOG.error("Unable to parse date, setting current time as transaction timestamp");
				entry.setTime(new Date());
			}
		}

		entry.setTransactionType(BCFPaymentMethodType.CARD);
		entry.setType(paymentTransactionType);
		if (BCFPaymentMethodType.GIFTCARD.equals(bcfPaymentMethodType) || BCFPaymentMethodType.CASH
				.equals(bcfPaymentMethodType)
				|| BCFPaymentMethodType.CTCCARD.equals(bcfPaymentMethodType))
		{
			entry.setType(PaymentTransactionType.PURCHASE);
			entry.setTransactionType(bcfPaymentMethodType);
		}

		entry.setBcfPaymentResponseBooleanStatus(transactionDetails.isBcfPaymentResponseBooleanStatus());
		entry.setBcfResponseCode(transactionDetails.getBcfResponseCode());
		entry.setBcfResponseDescription(transactionDetails.getBcfResponseDescription());
		entry.setServiceProviderResponseCode(transactionDetails.getServiceProviderResponseCode());
		entry.setServiceProviderResponseDescription(transactionDetails.getServiceProviderResponseDescription());
		entry.setIsoResponseCode(transactionDetails.getIsoResponseCode());
		if (Objects.nonNull(transactionDetails.getTransactionAmountInCents()))
		{
			entry.setAmount(new BigDecimal(transactionDetails.getTransactionAmountInCents() / 100d));
		}
		entry.setApprovalNumber(transactionDetails.getApprovalNumber());
		entry.setTransactionRefNumber(transactionDetails.getTransactionRefNumber());
		entry.setInvoiceNumber(transactionDetails.getInvoiceNumber());
		entry.setServiceProviderPaymentTerminalId(transactionDetails.getServiceProviderPaymentTerminalId());
		entry.setOperatorDisplayMessage(transactionDetails.getOperatorDisplayMessage());
		entry.setCustomerDisplayMessage(transactionDetails.getOperatorDisplayMessage());
		entry.setApprovalOrDeclineMessage(transactionDetails.getApprovalOrDeclineMessage());
		entry.setCardEntryMethod(transactionDetails.getCardEntryMethod());
		entry.setSafIndicator(transactionDetails.getSafIndicator());
		entry.setCvmIndicator(transactionDetails.getCvmIndicator());
		entry.setMotoIndicator(transactionDetails.getMotoIndicator());
		entry.setEmvTvr(transactionDetails.getEmvTvr());
		entry.setEmvTsi(transactionDetails.getEmvTsi());
		entry.setTokenValue(transactionDetails.getTokenValue());
		entry.setTokenType(transactionDetails.getTokenType());
		entry.setDebitAccountType(transactionDetails.getDebitAccountType());
		entry.setCardHolderLanguage(transactionDetails.getCardHolderLanguage());
		entry.setEmvApplicationLabel(transactionDetails.getEmvApplicationLabel());
		entry.setEmvAppPreferredName(transactionDetails.getEmvAppPreferredName());
		entry.setEmvAid(transactionDetails.getEmvAid());
		entry.setEmvPinEntryMessage(transactionDetails.getEmvPinEntryMessage());
		entry.setSignatureRequiredFlag(transactionDetails.isSignatureRequiredFlag());
		entry.setFormFactor(transactionDetails.getFormFactor());
		entry.setSupplementalMessage(transactionDetails.getSupplementalMessage());
		entry.setPaymentProcessingMethod(transactionDetails.getPaymentProcessingMethod());
		entry.setCardType(transactionDetails.getCardType());
		entry.setCardNumberToPrint(transactionDetails.getCardNumberToPrint());
		entry.setPaymentTransactionType(transactionDetails.getTransactionType());

		if (Objects.nonNull(transactionDetails.getBcfRefRecord()))
		{
			entry.setBcfRefRecord(transactionDetails.getBcfRefRecord());
		}
		return entry;
	}

	@Override
	public boolean createPaymentTransactions(final CustomerModel customerModel, final Double amountToPay,
			final PaymentTransactionType paymentTransactionType,
			final AbstractOrderModel abstractOrderModel, final BCFPaymentMethodType bcfPaymentMethodType,
			final Map<String, String> resultMap)
	{
		final PaymentTransactionModel transaction = getOrCreatePaymentTransaction(customerModel, paymentTransactionType,
				abstractOrderModel, bcfPaymentMethodType, false);

		createPaymentTransactionEntry(transaction, customerModel, amountToPay, paymentTransactionType, abstractOrderModel,
				resultMap, null);
		return true;
	}


	@Override
	public void createPaymentTransactionEntry(final PaymentTransactionModel transaction, final CustomerModel customerModel,
			final Double amountToPay,
			final PaymentTransactionType paymentTransactionType, final AbstractOrderModel abstractOrderModel,
			final Map<String, String> resultMap, final String refundReason)
	{
		final BCFPaymentMethodType bcfPaymentMethodType = getPaymentMethodType(transaction, resultMap);

		final List<ItemModel> itemsToSave = new ArrayList<>();
		final PaymentTransactionEntryModel transactionEntry = createPaymentTransactionEntry(paymentTransactionType, amountToPay,
				resultMap, bcfPaymentMethodType);

		transactionEntry.setCode(getPaymentService().getNewPaymentTransactionEntryCode(transaction, paymentTransactionType));
		transactionEntry.setRefundReason(refundReason);

		if (StringUtils.isNotBlank(resultMap.get(DEPOSIT_AMOUNT)))
		{
			transactionEntry.setDepositPayment(true);
		}
		final HashSet<PaymentTransactionModel> transactions = CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions())
				? new HashSet<>()
				: new HashSet<>(abstractOrderModel.getPaymentTransactions());
		final List<PaymentTransactionEntryModel> transactionEntries = CollectionUtils.isEmpty(transaction.getEntries())
				? new ArrayList<>()
				: new ArrayList<>(transaction.getEntries());
		transactionEntries.add(transactionEntry);
		transaction.setEntries(transactionEntries);

		if (!paymentTransactionType.equals(PaymentTransactionType.REFUND))
		{
			updateTransactionWithPaymentInfo(customerModel, abstractOrderModel, transaction, resultMap);
		}

		transactions.add(transaction);
		itemsToSave.add(transaction);
		itemsToSave.add(transactionEntry);
		abstractOrderModel.setPaymentTransactions(transactions.stream().collect(Collectors.toList()));
		itemsToSave.add(abstractOrderModel);
		getModelService().saveAll(itemsToSave);
	}

	/**
	 * @param customerModel
	 * @param abstractOrderModel
	 * @param transaction
	 */
	protected void updateTransactionWithPaymentInfo(final CustomerModel customerModel, final AbstractOrderModel abstractOrderModel,
			final PaymentTransactionModel transaction,
			final Map<String, String> resultMap)
	{
		final BCFPaymentMethodType bcfPaymentMethodType = getPaymentMethodType(transaction, resultMap);

		final PaymentInfoModel paymentInfoModel;
		if (BCFPaymentMethodType.CASH.equals(bcfPaymentMethodType))
		{
			paymentInfoModel = getBcfCashPaymentInfoCreateStrategy().savePaymentInfo(customerModel, resultMap, abstractOrderModel);
		}
		else if (BCFPaymentMethodType.GIFTCARD.equals(bcfPaymentMethodType))
		{
			paymentInfoModel = getBcfGiftCardPaymentInfoCreateStrategy()
					.savePaymentInfo(customerModel, resultMap, abstractOrderModel);
		}
		else if (BCFPaymentMethodType.CTCCARD.equals(bcfPaymentMethodType))
		{
			paymentInfoModel = getBcfCTCCardPaymentInfoCreateStrategy()
					.savePaymentInfo(customerModel, resultMap, abstractOrderModel);
		}
		else
		{
			paymentInfoModel = getBcfCreditCardPaymentInfoCreateStrategy()
					.savePaymentDetails(customerModel, resultMap, abstractOrderModel);
		}
		transaction.setInfo(paymentInfoModel);
	}

	protected PaymentTransactionEntryModel createPaymentTransactionEntry(final PaymentTransactionType paymentTransactionType,
			final Double amountToPay, final Map<String, String> resultMap, final BCFPaymentMethodType bcfPaymentMethodType)
	{
		final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);

		final SalesApplication salesApplication = getBcfSalesApplicationResolverService().getCurrentSalesChannel();
		if ((SalesApplication.TRAVELCENTRE.equals(salesApplication)) || (SalesApplication.CALLCENTER.equals(salesApplication)))
		{
			final AssistedServiceSession asmSession = getSessionService().getAttribute(
					ASM_SESSION_PARAMETER);
			if (asmSession.getAgent() != null)
			{
				entry.setAgent(asmSession.getAgent());
			}
		}
		entry.setSalesApplication(salesApplication);

		final LocalDateTime currentTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
		entry.setTime(Date.from(currentTime.atZone(ZoneId.of(AMERICA_VANCOUVER)).toInstant()));

		entry.setType(paymentTransactionType);
		entry.setTransactionType(bcfPaymentMethodType);

		entry.setAmount(BigDecimal.valueOf(amountToPay));
		entry.setInvoiceNumber(resultMap.get("receiptNo"));
		return entry;
	}

	@Override
	public BCFPaymentMethodType getPaymentMethodType(final PaymentTransactionModel transaction,
			final Map<String, String> resultMap)
	{
		final String paymentType = resultMap.get(PAYMENT_TYPE);
		if (StringUtils.isNotBlank(paymentType))
		{
			if (StringUtils.equalsIgnoreCase("savedCard", paymentType))
			{
				return BCFPaymentMethodType.CARD;
			}
			return BCFPaymentMethodType.valueOf(resultMap.get(PAYMENT_TYPE).toUpperCase());
		}

		PaymentTransactionEntryModel paymentTransactionEntryModel = null;
		if (CollectionUtils.isNotEmpty(transaction.getEntries()))
		{
			paymentTransactionEntryModel = transaction.getEntries().stream().findFirst().get();

			return paymentTransactionEntryModel.getTransactionType();
		}

		return null;
	}

	protected BcfCashPaymentInfoCreateStrategy getBcfCashPaymentInfoCreateStrategy()
	{
		return bcfCashPaymentInfoCreateStrategy;
	}

	@Required
	public void setBcfCashPaymentInfoCreateStrategy(
			final BcfCashPaymentInfoCreateStrategy bcfCashPaymentInfoCreateStrategy)
	{
		this.bcfCashPaymentInfoCreateStrategy = bcfCashPaymentInfoCreateStrategy;
	}

	protected BcfCreditCardPaymentInfoCreateStrategy getBcfCreditCardPaymentInfoCreateStrategy()
	{
		return bcfCreditCardPaymentInfoCreateStrategy;
	}

	@Required
	public void setBcfCreditCardPaymentInfoCreateStrategy(
			final BcfCreditCardPaymentInfoCreateStrategy bcfCreditCardPaymentInfoCreateStrategy)
	{
		this.bcfCreditCardPaymentInfoCreateStrategy = bcfCreditCardPaymentInfoCreateStrategy;
	}

	protected BcfGiftCardPaymentInfoCreateStrategy getBcfGiftCardPaymentInfoCreateStrategy()
	{
		return bcfGiftCardPaymentInfoCreateStrategy;
	}

	@Required
	public void setBcfGiftCardPaymentInfoCreateStrategy(
			final BcfGiftCardPaymentInfoCreateStrategy bcfGiftCardPaymentInfoCreateStrategy)
	{
		this.bcfGiftCardPaymentInfoCreateStrategy = bcfGiftCardPaymentInfoCreateStrategy;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfCTCCardPaymentInfoCreateStrategy getBcfCTCCardPaymentInfoCreateStrategy()
	{
		return bcfCTCCardPaymentInfoCreateStrategy;
	}

	public void setBcfCTCCardPaymentInfoCreateStrategy(
			final BcfCTCCardPaymentInfoCreateStrategy bcfCTCCardPaymentInfoCreateStrategy)
	{
		this.bcfCTCCardPaymentInfoCreateStrategy = bcfCTCCardPaymentInfoCreateStrategy;
	}
}
