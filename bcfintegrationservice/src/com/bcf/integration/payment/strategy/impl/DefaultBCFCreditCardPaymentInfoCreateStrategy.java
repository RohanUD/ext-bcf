/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.strategy.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.model.ModelCloningContext;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.core.services.strategies.BcfCreditCardPaymentInfoCreateStrategy;


public class DefaultBCFCreditCardPaymentInfoCreateStrategy extends DefaultCreditCardPaymentInfoCreateStrategy
		implements BcfCreditCardPaymentInfoCreateStrategy
{
	private static final String DEFAULT_CARD_TYPE = "OTHER";
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private SessionService sessionService;
	private static final String SAVED_CC_CARD_CODE = "savedCCCardCode";
	private BcfCustomerAccountService customerAccountService;
	private ModelCloningContext paymentInfoCloningContext;

	@Override
	public CreditCardPaymentInfoModel savePaymentDetails(final CustomerModel customerModel, final Map<String, String> resultMap)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		final CreditCardPaymentInfoModel cardPaymentInfoModel = createCreditCardPaymentInfo(resultMap, customerModel, true, null);
		cardPaymentInfoModel.setBillingAddress(createBillingAddress(customerModel, resultMap));
		return cardPaymentInfoModel;
	}

	protected AddressModel createBillingAddress(final CustomerModel customerModel, final Map<String, String> resultMap)
	{
		final AddressModel addressModel = getModelService().create(AddressModel.class);
		addressModel.setOwner(customerModel);
		addressModel.setBillingAddress(true);
		addressModel.setLine1(resultMap.get("billingAddress.line1"));
		addressModel.setLine2(resultMap.get("billingAddress.line2"));
		addressModel.setTown(resultMap.get("billingAddress.townCity"));
		addressModel.setPostalcode(resultMap.get("billingAddress.postcode"));
		addressModel.setPrimaryInd(true);
		addressModel.setVisibleInAddressBook(true);

		if (resultMap.containsKey("billingAddress.countryIso"))
		{
			final CountryModel country = getCommonI18NService().getCountry(resultMap.get("billingAddress.countryIso"));
			addressModel.setCountry(country);
			if (resultMap.containsKey("billingAddress.regionIso"))
			{
				final String regionIso = resultMap.get("billingAddress.regionIso");
				if (!StringUtils.equals(regionIso, "-1"))
				{
					addressModel.setRegion(getCommonI18NService().getRegion(country, country.getIsocode() + "-" + regionIso));
				}
			}
		}

		return addressModel;
	}

	@Override
	public CreditCardPaymentInfoModel savePaymentDetails(final CustomerModel customerModel, final Map<String, String> resultMap,
			final AbstractOrderModel orderModel)
	{
		final boolean savePaymentInfo = Boolean.valueOf(resultMap.get("savePaymentInfo"))
				|| getCheckoutCustomerStrategy().isAnonymousCheckout();
		return createCreditCardPaymentInfo(resultMap, customerModel, savePaymentInfo, orderModel);

	}

	private CreditCardPaymentInfoModel createCreditCardPaymentInfo(final Map<String, String> resultMap,
			final CustomerModel customerModel, final boolean saveInAccount, final AbstractOrderModel orderModel)
	{
		validateParameterNotNull(customerModel, "customerModel cannot be null");
		CreditCardPaymentInfoModel cardPaymentInfoModel = null;

		if (StringUtils.isNotBlank(resultMap.get(SAVED_CC_CARD_CODE)))
		{
			final CreditCardPaymentInfoModel paymentInfo = getCustomerAccountService()
					.getCreditCardPaymentInfoForCode(customerModel, resultMap.get(SAVED_CC_CARD_CODE));
			if (Objects.nonNull(paymentInfo))
			{
				orderModel.setPaymentAddress(getModelService().clone(paymentInfo.getBillingAddress()));
				getModelService().save(orderModel);
			}
			return clonePaymentInfoForOrder(paymentInfo, orderModel);
		}
		if (StringUtils.isNotBlank(resultMap.get("cardNumber")))
		{
			cardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
			cardPaymentInfoModel.setNumber(resultMap.get("cardNumber"));
			if (resultMap.get("token") == null)
			{
				cardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
			}
			else
			{
				cardPaymentInfoModel.setCode(resultMap.get("token"));
			}

			cardPaymentInfoModel.setToken(resultMap.get("token"));
			cardPaymentInfoModel.setTokenType(resultMap.get("token_type"));
			cardPaymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
			cardPaymentInfoModel.setUser(customerModel);
			cardPaymentInfoModel.setCcOwner(customerModel.getName());
			setCardValidity(resultMap, cardPaymentInfoModel);
			cardPaymentInfoModel.setSaved(saveInAccount);
			if (resultMap.get("cardType") != null)
			{
				cardPaymentInfoModel.setType(CreditCardType.valueOf(resultMap.get("cardType").toUpperCase()));
			}
			else
			{
				cardPaymentInfoModel.setType(CreditCardType.valueOf(DEFAULT_CARD_TYPE));
			}
		}

		return cardPaymentInfoModel;
	}

	private void setCardValidity(final Map<String, String> resultMap, final CreditCardPaymentInfoModel cardPaymentInfoModel)
	{
		final String cardExpiration = resultMap.get("cardExpiration");
		final String cardExpirationMonth = resultMap.get("cardExpirationMonth");
		final String cardExpirationYear = resultMap.get("cardExpirationYear");
		if (StringUtils.isNotBlank(cardExpiration))
		{
			final String cardExpiryMonth = cardExpiration.substring(0, 2);
			if (StringUtils.isNotBlank(cardExpiryMonth) && Integer.parseInt(cardExpiryMonth) > 0)
			{
				cardPaymentInfoModel.setValidToMonth(cardExpiryMonth);
			}
			final String cardExpiryYear = cardExpiration.substring(3);
			if (StringUtils.isNotBlank(cardExpiryYear) && Integer.parseInt(cardExpiryYear) > 0)
			{
				cardPaymentInfoModel.setValidToYear(cardExpiryYear);
			}
		}
		else if (StringUtils.isNotEmpty(cardExpirationMonth) && StringUtils.isNotEmpty(cardExpirationYear))
		{
			cardPaymentInfoModel.setValidToMonth(cardExpirationMonth);
			cardPaymentInfoModel.setValidToYear(cardExpirationYear);
		}
		else
		{
			//Default values since the pin pad response doesn't contain expiry date
			cardPaymentInfoModel.setValidToMonth(String.valueOf(1));
			cardPaymentInfoModel.setValidToYear(String.valueOf(1));
		}
	}

	private CreditCardPaymentInfoModel clonePaymentInfoForOrder(final CreditCardPaymentInfoModel paymentInfo,
			final AbstractOrderModel order)
	{
		validateParameterNotNullStandardMessage("order", order);
		validateParameterNotNullStandardMessage("paymentInfo", paymentInfo);
		final CreditCardPaymentInfoModel newPaymentInfo = getModelService().clone(paymentInfo, this.paymentInfoCloningContext);
		newPaymentInfo.setOwner(order);
		newPaymentInfo.setDuplicate(Boolean.TRUE);
		newPaymentInfo.setOriginal(paymentInfo);
		return newPaymentInfo;
	}

	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	public void setCustomerAccountService(final BcfCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}
}
