/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.common.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.processpayment.request.data.CardIdentifier;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.TokenRef;


public class BcfCardInfoRequestDataPopulator implements Populator<AbstractOrderModel, PaymentRequestDTO>
{

	private ConfigurationService configurationService;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{

			final CardInfoRequest cardInfoRequest = target.getCardInfoRequest();
			final CardIdentifier cardIdentifier = cardInfoRequest.getCardIdentifier();
			cardIdentifier.setCardIdentifierType(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.CARD_IDENTIFIER_TYPE));
			final TokenRef tokenRef = cardIdentifier.getTokenRef();
			if (source.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
			{
				final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) source.getPaymentInfo();
				tokenRef.setTokenValue(creditCardPaymentInfoModel.getToken());
				tokenRef.setTokenType(creditCardPaymentInfoModel.getTokenType());
			}

			cardInfoRequest.setExpectedCardType(getConfigurationService().getConfiguration()
					.getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
		}
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

}
