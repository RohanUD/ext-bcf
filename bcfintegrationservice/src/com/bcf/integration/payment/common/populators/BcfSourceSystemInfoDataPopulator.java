/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.common.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;


public class BcfSourceSystemInfoDataPopulator implements Populator<AbstractOrderModel, PaymentRequestDTO>
{
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;
	private Map<String, Map<String, String>> paymentApplicationIdMap;
	private Map<String, String> paymentClientCodePasswordMap;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
         String applicationId = paymentApplicationIdMap.get(source.getBookingJourneyType().getCode()).get(bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode());

			final PaymentSourceSystemInfo sourceSystemInfo = (PaymentSourceSystemInfo)target.getSourceSystemInfo();
			sourceSystemInfo
					.setAppliId(applicationId);
			sourceSystemInfo
					.setPassword(paymentClientCodePasswordMap.get(applicationId));
			sourceSystemInfo
					.setLocationId(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfintegrationserviceConstants.LOCATION_ID));

		}



	}

	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	@Required
	public void setPaymentApplicationIdMap(final Map<String, Map<String, String>> paymentApplicationIdMap)
	{
		this.paymentApplicationIdMap = paymentApplicationIdMap;
	}

	@Required
	public void setPaymentClientCodePasswordMap(final Map<String, String> paymentClientCodePasswordMap)
	{
		this.paymentClientCodePasswordMap = paymentClientCodePasswordMap;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
