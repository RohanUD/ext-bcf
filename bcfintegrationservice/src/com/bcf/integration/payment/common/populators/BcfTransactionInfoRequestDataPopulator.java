/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.common.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfBookingService;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.TransactionInfoRequest;


public class BcfTransactionInfoRequestDataPopulator implements Populator<AbstractOrderModel, PaymentRequestDTO>
{
	private BcfBookingService bcfBookingService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final TransactionInfoRequest transactionInfoRequest = target.getTransactionInfoRequest();
			Double amountToPay = source.getAmountToPay();
			if (source instanceof OrderModel)
			{
				final OrderModel order = (OrderModel) source;
				if (Objects.equals(OrderStatus.ACTIVE, order.getStatus()))
				{
					amountToPay = getBcfBookingService().getTotalPriceForRemovedTransportEntries(order);
				}
			}
			transactionInfoRequest.setAmountInCents(Math.round(100 * amountToPay));
		}
	}

	/**
	 * @return the bcfBookingService
	 */
	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	/**
	 * @param bcfBookingService the bcfBookingService to set
	 */
	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}
}
