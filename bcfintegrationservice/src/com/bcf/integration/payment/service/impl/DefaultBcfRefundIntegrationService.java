/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.BCFPaymentMethodType;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.services.strategies.BcfPaymentTransactionStrategy;
import com.bcf.core.util.BCFPaymentMethodUtil;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.data.TransactionDetails;
import com.bcf.integration.payment.exceptions.InvalidPaymentInfoException;
import com.bcf.integration.payment.refund.populators.BcfRefundTransactionCardInfoRequestDataPopulator;
import com.bcf.integration.payment.service.BcfRefundIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;
import com.bcf.processpayment.response.data.PaymentResponseDTO;


public class DefaultBcfRefundIntegrationService implements BcfRefundIntegrationService
{

	private static final Logger LOG = Logger.getLogger(DefaultBcfRefundIntegrationService.class);

	private Converter<AbstractOrderModel, PaymentRequestDTO> bcfProcessRefundRequestConverter;


	private Converter<AbstractOrderModel, PaymentRequestDTO> bcfGoodWillRefundRequestConverter;

	private Converter<AbstractOrderModel, PaymentRequestDTO> bcfTransactionRefundRequestConverter;


	private BaseRestService bcfPaymentRestService;

	private UserService userService;

	private BcfPaymentTransactionStrategy paymentTransactionStrategy;

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	private BcfRefundTransactionCardInfoRequestDataPopulator bcfRefundTransactionCardInfoRequestDataPopulator;

	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	private SessionService sessionService;

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	private Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter;

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public Converter<PaymentResponseDTO, TransactionDetails> getPaymentResponseDTOConverter()
	{
		return paymentResponseDTOConverter;
	}

	public void setPaymentResponseDTOConverter(
			final Converter<PaymentResponseDTO, TransactionDetails> paymentResponseDTOConverter)
	{
		this.paymentResponseDTOConverter = paymentResponseDTOConverter;
	}


	public BcfPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	public void setPaymentTransactionStrategy(final BcfPaymentTransactionStrategy paymentTransactionStrategy)
	{
		this.paymentTransactionStrategy = paymentTransactionStrategy;
	}

	@Override
	public PaymentResponseDTO refundOrder(final AbstractOrderModel abstractOrder)
			throws IntegrationException, InvalidPaymentInfoException
	{
		if (CollectionUtils.isNotEmpty(abstractOrder.getPaymentTransactions()))
		{
			final PaymentTransactionModel paymentTransactionModel = abstractOrder.getPaymentTransactions().stream().filter(
					transaction -> transaction.getInfo() instanceof CreditCardPaymentInfoModel).findAny().orElse(null);

			if (paymentTransactionModel == null)
			{
				throw new InvalidPaymentInfoException("processing refund failed, original card payment transaction does not exist");
			}
			final PaymentRequestDTO processPaymentRequest = getBcfProcessRefundRequestConverter().convert(abstractOrder);
			final PaymentResponseDTO paymentResponseDTO = (PaymentResponseDTO) getBcfPaymentRestService()
					.sendRestRequest(processPaymentRequest, PaymentResponseDTO.class,
							BcfintegrationserviceConstants.REFUND_WITH_TOKEN_REQUEST_SERVICE_URL, HttpMethod.POST);

			updateRefundTransaction(abstractOrder, paymentResponseDTO);
			return paymentResponseDTO;
		}
		else
		{
			throw new InvalidPaymentInfoException("processing refund failed, original card payment transaction does not exist");
		}
	}


	@Override
	public PaymentResponseDTO processRefund(final AbstractOrderModel abstractOrder, final double refundAmount)
			throws IntegrationException, InvalidPaymentInfoException
	{

		if (CollectionUtils.isNotEmpty(abstractOrder.getPaymentTransactions()))
		{
			final PaymentTransactionModel paymentTransactionModel = abstractOrder.getPaymentTransactions().stream().filter(
					transaction -> transaction
							.getInfo() instanceof de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel).findAny()
					.orElse(null);

			if (paymentTransactionModel == null)
			{
				throw new InvalidPaymentInfoException(
						"processing good will refund failed, original card payment transaction does not exist");
			}

			final PaymentResponseDTO paymentResponseDTO;
			final PaymentRequestDTO paymentRequestDTO = bcfGoodWillRefundRequestConverter.convert(abstractOrder);
			paymentRequestDTO.getTransactionInfoRequest().setAmountInCents(Math.round(100 * refundAmount));
			paymentResponseDTO = (PaymentResponseDTO) getBcfPaymentRestService()
					.sendRestRequest(paymentRequestDTO, PaymentResponseDTO.class,
							BcfintegrationserviceConstants.REFUND_WITH_TOKEN_REQUEST_SERVICE_URL, HttpMethod.POST);

			updateRefundTransaction(abstractOrder, paymentResponseDTO);
			return paymentResponseDTO;


		}
		else
		{
			throw new InvalidPaymentInfoException(
					"processing good will refund failed, original card payment transaction does not exist");
		}


	}

	private void updateRefundTransaction(final AbstractOrderModel orderModel, final PaymentResponseDTO paymentResponseDTO)
	{
		final BCFPaymentMethodType bcfPaymentMethodType = BCFPaymentMethodUtil.getBCFPaymentMethodType(orderModel);

		final TransactionDetails transactionDetails = populateTransactionDetails(paymentResponseDTO);
		getPaymentTransactionStrategy()
				.createPaymentTransactions((CustomerModel) getUserService().getCurrentUser(), transactionDetails,
						PaymentTransactionType.REFUND, bcfPaymentMethodType, orderModel, MapUtils.EMPTY_SORTED_MAP, null, false);
	}

	private TransactionDetails populateTransactionDetails(final PaymentResponseDTO paymentResponseDTO)
	{
		final Boolean bcfResponseStatus = paymentResponseDTO.getResponseStatus()
				.getBcfPaymentResponseBooleanStatus();
		final String bcfResponseCode = paymentResponseDTO.getResponseStatus().getBcfResponseCode();
		final String bcfResponseDesc = paymentResponseDTO.getResponseStatus()
				.getBcfResponseDescription();

		TransactionDetails transactionDetails = new TransactionDetails();
		if (isPaymentSuccessful(paymentResponseDTO))
		{
			transactionDetails = getPaymentResponseDTOConverter().convert(paymentResponseDTO);
			transactionDetails.setIsSuccessful(true);
			transactionDetails.setCallerMethod("refundWithToken");

		}
		else
		{

			transactionDetails.setIsSuccessful(false);
			LOG.error(
					String.format("Refund Request failed with status : [%s], code : [%s] and description : [%s] ", bcfResponseStatus,
							bcfResponseCode, bcfResponseDesc));
			transactionDetails.setCallerMethod("refundWithToken");
			transactionDetails.setBcfResponseCode(bcfResponseCode);
			transactionDetails.setBcfResponseDescription(bcfResponseDesc);

		}
		return transactionDetails;
	}


	@Override
	public List<PaymentResponseDTO> processRefund(final AbstractOrderModel abstractOrder,
			final List<RefundTransactionInfoData> refundTransactionInfoDatas)
			throws IntegrationException, InvalidPaymentInfoException
	{

		final List<PaymentResponseDTO> paymentResponseDTOs = new ArrayList<>();
		final String salesChannel = bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode();
		for (final RefundTransactionInfoData refundTransactionInfoData : refundTransactionInfoDatas)
		{

			if (refundTransactionInfoData.getCode().equals(BcfCoreConstants.CARD_REFUND_WITH_PIN_PAD))
			{
				final PaymentResponseDTO paymentResponseDTO;

				final PaymentRequestDTO paymentRequestDTO = bcfTransactionRefundRequestConverter.convert(abstractOrder);
				bcfRefundTransactionCardInfoRequestDataPopulator.populate(null, paymentRequestDTO);
				populateSourceSystemInfo(salesChannel, paymentRequestDTO);


				paymentRequestDTO.getTransactionInfoRequest()
						.setAmountInCents(Math.round(100 * refundTransactionInfoData.getAmount()));

				paymentResponseDTO = (PaymentResponseDTO) getBcfPaymentRestService()
						.sendRestRequest(paymentRequestDTO, PaymentResponseDTO.class,
								BcfintegrationserviceConstants.REFUND_WITH_TOKEN_REQUEST_SERVICE_URL, HttpMethod.POST);

				final TransactionDetails transactionDetails = populateTransactionDetails(paymentResponseDTO);
				paymentResponseDTOs.add(paymentResponseDTO);

				final BCFPaymentMethodType bcfPaymentMethodType = BCFPaymentMethodUtil.getBCFPaymentMethodType(abstractOrder);

				getPaymentTransactionStrategy()
						.createPaymentTransactions((CustomerModel) getUserService().getCurrentUser(), transactionDetails,
								PaymentTransactionType.REFUND, bcfPaymentMethodType, abstractOrder, MapUtils.EMPTY_SORTED_MAP,
								refundTransactionInfoData.getRefundReason(), true);
			}
			else
			{
				final PaymentTransactionModel paymentTransaction = abstractOrder.getPaymentTransactions().stream().filter(
						paymentTransactionModel -> refundTransactionInfoData.getCode().equals(paymentTransactionModel.getCode()))
						.findAny().orElse(null);

				if (paymentTransaction.getInfo() != null && paymentTransaction
						.getInfo() instanceof de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel)
				{


					final PaymentResponseDTO paymentResponseDTO;

					final PaymentRequestDTO paymentRequestDTO = bcfTransactionRefundRequestConverter.convert(abstractOrder);
					bcfRefundTransactionCardInfoRequestDataPopulator.populate(paymentTransaction, paymentRequestDTO);
					populateSourceSystemInfo(salesChannel, paymentRequestDTO);

					paymentRequestDTO.getTransactionInfoRequest()
							.setAmountInCents(Math.round(100 * refundTransactionInfoData.getAmount()));

					paymentResponseDTO = (PaymentResponseDTO) getBcfPaymentRestService()
							.sendRestRequest(paymentRequestDTO, PaymentResponseDTO.class,
									BcfintegrationserviceConstants.REFUND_WITH_TOKEN_REQUEST_SERVICE_URL, HttpMethod.POST);

					final TransactionDetails transactionDetails = populateTransactionDetails(paymentResponseDTO);
					paymentResponseDTOs.add(paymentResponseDTO);

					final BCFPaymentMethodType bcfPaymentMethodType = getPaymentTransactionStrategy()
							.getPaymentMethodType(paymentTransaction, new HashMap<>());

					getPaymentTransactionStrategy()
							.createPaymentTransactionEntry((CustomerModel) getUserService().getCurrentUser(), transactionDetails,
									PaymentTransactionType.REFUND, bcfPaymentMethodType, abstractOrder,
									MapUtils.EMPTY_SORTED_MAP, paymentTransaction, refundTransactionInfoData.getRefundReason());
				}
				else
				{
					getPaymentTransactionStrategy()
							.createPaymentTransactionEntry(paymentTransaction, (CustomerModel) getUserService().getCurrentUser(),
									refundTransactionInfoData.getAmount(), PaymentTransactionType.REFUND, abstractOrder,
									MapUtils.EMPTY_SORTED_MAP, refundTransactionInfoData.getRefundReason());
				}
			}

		}
		return paymentResponseDTOs;
	}

	private void populateSourceSystemInfo(final String salesChannel, final PaymentRequestDTO paymentRequestDTO)
	{
		if (salesChannel != null && StringUtils.equalsIgnoreCase(SalesApplication.TRAVELCENTRE.getCode(), salesChannel))
		{
			final PaymentSourceSystemInfo sourceSystemInfo = (PaymentSourceSystemInfo) paymentRequestDTO.getSourceSystemInfo();
			sourceSystemInfo.setAgentId(
					getSessionService().getCurrentSession().getAttribute(BcfintegrationserviceConstants.SESSION_ICEBAR_ID));

			sourceSystemInfo.setStationId(
					getSessionService().getCurrentSession().getAttribute(BcfintegrationserviceConstants.SESSION_PINPAD_ID));
		}
	}

	protected boolean isPaymentSuccessful(final PaymentResponseDTO paymentResponseDTO)
	{
		return paymentResponseDTO.getResponseStatus().getBcfPaymentResponseBooleanStatus()
				&& StringUtils.equals(paymentResponseDTO.getResponseStatus().getBcfResponseCode(),
				getBcfConfigurablePropertiesService().getBcfPropertyValue("paymentSuccessCode"));
	}

	public Converter<AbstractOrderModel, PaymentRequestDTO> getBcfGoodWillRefundRequestConverter()
	{
		return bcfGoodWillRefundRequestConverter;
	}

	public void setBcfGoodWillRefundRequestConverter(
			final Converter<AbstractOrderModel, PaymentRequestDTO> bcfGoodWillRefundRequestConverter)
	{
		this.bcfGoodWillRefundRequestConverter = bcfGoodWillRefundRequestConverter;
	}

	protected Converter<AbstractOrderModel, PaymentRequestDTO> getBcfProcessRefundRequestConverter()
	{
		return bcfProcessRefundRequestConverter;
	}

	@Required
	public void setBcfProcessRefundRequestConverter(
			final Converter<AbstractOrderModel, PaymentRequestDTO> bcfProcessRefundRequestConverter)
	{
		this.bcfProcessRefundRequestConverter = bcfProcessRefundRequestConverter;
	}

	protected BaseRestService getBcfPaymentRestService()
	{
		return bcfPaymentRestService;
	}

	@Required
	public void setBcfPaymentRestService(final BaseRestService bcfPaymentRestService)
	{
		this.bcfPaymentRestService = bcfPaymentRestService;
	}

	public BcfRefundTransactionCardInfoRequestDataPopulator getBcfRefundTransactionCardInfoRequestDataPopulator()
	{
		return bcfRefundTransactionCardInfoRequestDataPopulator;
	}

	public void setBcfRefundTransactionCardInfoRequestDataPopulator(
			final BcfRefundTransactionCardInfoRequestDataPopulator bcfRefundTransactionCardInfoRequestDataPopulator)
	{
		this.bcfRefundTransactionCardInfoRequestDataPopulator = bcfRefundTransactionCardInfoRequestDataPopulator;
	}

	public Converter<AbstractOrderModel, PaymentRequestDTO> getBcfTransactionRefundRequestConverter()
	{
		return bcfTransactionRefundRequestConverter;
	}

	public void setBcfTransactionRefundRequestConverter(
			final Converter<AbstractOrderModel, PaymentRequestDTO> bcfTransactionRefundRequestConverter)
	{
		this.bcfTransactionRefundRequestConverter = bcfTransactionRefundRequestConverter;
	}

	public BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	public void setBcfSalesApplicationResolverService(
			final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
