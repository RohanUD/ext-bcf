/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.service.impl;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.TokenRequestDTO;
import com.bcf.integration.data.TokenResponseDTO;
import com.bcf.integration.payment.service.BcfPaymentVaultIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.processpayment.request.data.PaymentVaultRequestDTO;
import com.bcf.processpayment.response.data.PaymentVaultResponseDTO;


public class DefaultBcfPaymentVaultIntegrationService implements BcfPaymentVaultIntegrationService
{
   private BaseRestService bcfPaymentRestService;

   @Override
   public PaymentVaultResponseDTO makeVaultPayment(final String paymentServiceEndpoint, final PaymentVaultRequestDTO paymentRequestDTO)
         throws IntegrationException
   {
      return (PaymentVaultResponseDTO) getBcfPaymentRestService().sendRestRequest(paymentRequestDTO, PaymentVaultResponseDTO.class,
            paymentServiceEndpoint, HttpMethod.POST);
   }

   @Override
   public TokenResponseDTO getPermanentToken(final TokenRequestDTO tokenRequestDTO) throws IntegrationException
   {
      return (TokenResponseDTO) getBcfPaymentRestService().sendRestRequest(tokenRequestDTO, TokenResponseDTO.class,
            BcfintegrationserviceConstants.PAYMENT_TOKEN_SERVICE_URL, HttpMethod.POST);
   }

   protected BaseRestService getBcfPaymentRestService()
   {
      return bcfPaymentRestService;
   }

   @Required
   public void setBcfPaymentRestService(final BaseRestService bcfPaymentRestService)
   {
      this.bcfPaymentRestService = bcfPaymentRestService;
   }
}
