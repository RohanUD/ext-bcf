/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.refund.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.payment.common.populators.BcfSourceSystemInfoDataPopulator;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.PaymentSourceSystemInfo;


public class BcfRefundSourceSystemInfoDataPopulator extends BcfSourceSystemInfoDataPopulator
		implements Populator<AbstractOrderModel, PaymentRequestDTO>
{
	private ConfigurationService configurationService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.isNull(source))
		{
			return;
		}
		final PaymentSourceSystemInfo sourceSystemInfo = new PaymentSourceSystemInfo();
		target.setSourceSystemInfo(sourceSystemInfo);
		super.populate(source, target);
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
