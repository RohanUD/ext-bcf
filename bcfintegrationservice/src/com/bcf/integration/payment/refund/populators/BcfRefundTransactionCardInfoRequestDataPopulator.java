/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.refund.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.common.populators.BcfTransactionCardInfoRequestDataPopulator;
import com.bcf.processpayment.request.data.CardIdentifier;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.RefundTokenRef;


public class BcfRefundTransactionCardInfoRequestDataPopulator extends BcfTransactionCardInfoRequestDataPopulator
		implements Populator<PaymentTransactionModel, PaymentRequestDTO>
{




	@Override
	public void populate(final PaymentTransactionModel paymentTransactionModel, final PaymentRequestDTO target) throws ConversionException
	{
		final CardInfoRequest cardInfoRequest = new CardInfoRequest();
		target.setCardInfoRequest(cardInfoRequest);
		if (Objects.nonNull(paymentTransactionModel))
		{

			final CardIdentifier cardIdentifier = new CardIdentifier();
			final RefundTokenRef tokenRef = new RefundTokenRef();
			cardIdentifier.setTokenRef(tokenRef);
			cardInfoRequest.setCardIdentifier(cardIdentifier);

			super.populate(paymentTransactionModel, target);


				final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) paymentTransactionModel.getInfo();
				tokenRef.setCardType(creditCardPaymentInfoModel.getType().getCode());
				tokenRef.setTokenType(creditCardPaymentInfoModel.getTokenType());
				tokenRef.setTokenValue(creditCardPaymentInfoModel.getToken());


		}
		cardInfoRequest.setExpectedCardType(getConfigurationService().getConfiguration()
				.getString(BcfintegrationserviceConstants.EXPECTED_CARD_TYPE_CREDITDEBIT));
	}



}
