/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.refund.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.integration.payment.common.populators.BcfCardInfoRequestDataPopulator;
import com.bcf.processpayment.request.data.CardIdentifier;
import com.bcf.processpayment.request.data.CardInfoRequest;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.RefundTokenRef;


public class BcfRefundCardInfoRequestDataPopulator extends BcfCardInfoRequestDataPopulator
		implements Populator<AbstractOrderModel, PaymentRequestDTO>
{

	private ConfigurationService configurationService;
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final CardInfoRequest cardInfoRequest = new CardInfoRequest();
			final CardIdentifier cardIdentifier = new CardIdentifier();
			final RefundTokenRef tokenRef = new RefundTokenRef();
			cardIdentifier.setTokenRef(tokenRef);
			cardInfoRequest.setCardIdentifier(cardIdentifier);
			target.setCardInfoRequest(cardInfoRequest);
			super.populate(source, target);
			PaymentTransactionModel paymentTransactionModel = source.getPaymentTransactions().stream().filter(
					transaction -> transaction
							.getInfo() instanceof de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel).findAny()
					.orElse(null);
			if(paymentTransactionModel!=null && paymentTransactionModel.getInfo() instanceof CreditCardPaymentInfoModel)
			{
				final CreditCardPaymentInfoModel creditCardPaymentInfoModel = (CreditCardPaymentInfoModel) paymentTransactionModel.getInfo();
				tokenRef.setCardType(creditCardPaymentInfoModel.getType().getCode());
				tokenRef.setTokenType(creditCardPaymentInfoModel.getTokenType());
				tokenRef.setTokenValue(creditCardPaymentInfoModel.getToken());
			}

		}
	}

	@Override
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Override
	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Override
	protected BcfSalesApplicationResolverService getBcfSalesApplicationResolverService()
	{
		return bcfSalesApplicationResolverService;
	}

	@Override
	@Required
	public void setBcfSalesApplicationResolverService(final BcfSalesApplicationResolverService bcfSalesApplicationResolverService)
	{
		this.bcfSalesApplicationResolverService = bcfSalesApplicationResolverService;
	}

}
