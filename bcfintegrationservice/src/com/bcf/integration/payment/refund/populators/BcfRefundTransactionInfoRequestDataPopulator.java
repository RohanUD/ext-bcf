/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.payment.refund.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.payment.common.populators.BcfTransactionInfoRequestDataPopulator;
import com.bcf.processpayment.request.data.PaymentRequestDTO;
import com.bcf.processpayment.request.data.TransactionInfoRequest;


public class BcfRefundTransactionInfoRequestDataPopulator extends BcfTransactionInfoRequestDataPopulator
		implements Populator<AbstractOrderModel, PaymentRequestDTO>
{
	private ConfigurationService configurationService;
	private BCFTravelCartService travelCartService;

	@Override
	public void populate(final AbstractOrderModel source, final PaymentRequestDTO target) throws ConversionException
	{
		if (Objects.nonNull(source))
		{
			final TransactionInfoRequest transactionInfoRequest = new TransactionInfoRequest();

			if (getTravelCartService().isAmendmentCart(source) && StringUtils
					.equalsIgnoreCase(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), source.getBookingJourneyType().getCode()))
			{
				transactionInfoRequest.setAmountInCents((long) Math.abs(source.getAmountToPay() * 100));
			}
			else
			{
				transactionInfoRequest.setAmountInCents(getTransactionAmountInCents(source));
			}
			target.setTransactionInfoRequest(transactionInfoRequest);
			transactionInfoRequest.setTransactionType(
					getConfigurationService().getConfiguration().getString(BcfintegrationserviceConstants.TRANSACTION_TYPE_REFUND));
		}
	}

	private long getTransactionAmountInCents(final AbstractOrderModel source)
	{
		if (CollectionUtils.isNotEmpty(source.getPaymentTransactions()))
		{
			final Optional<PaymentTransactionModel> optionalPaymentTxn = source.getPaymentTransactions().stream()
					.filter(transaction -> !transaction.isExisting() && transaction.getInfo() instanceof CreditCardPaymentInfoModel)
					.findAny();
			if (optionalPaymentTxn.isPresent())
			{
				final Optional<PaymentTransactionEntryModel> optionalEntry = optionalPaymentTxn.get().getEntries().stream()
						.filter(entry -> PaymentTransactionType.PURCHASE.equals(entry.getType())).findFirst();
				if (optionalEntry.isPresent() && BigDecimal.ZERO.compareTo(optionalEntry.get().getAmount()) < 0)
				{
					return Math.round(100 * optionalEntry.get().getAmount().doubleValue());
				}
			}
		}
		return 0;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected BCFTravelCartService getTravelCartService()
	{
		return travelCartService;
	}

	@Required
	public void setTravelCartService(final BCFTravelCartService travelCartService)
	{
		this.travelCartService = travelCartService;
	}
}
