/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.listsailings.request.manager.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.listSailingRequest.data.BcfListSailingsRequestData;
import com.bcf.integration.listsailings.request.handler.BcfListSailingsRequestHandler;
import com.bcf.integration.listsailings.request.manager.ListSailingsRequestPipelineManager;


public class DefaultListSailingsRequestPipelineManager implements ListSailingsRequestPipelineManager
{
	private List<BcfListSailingsRequestHandler> handlers;

	@Override
	public void executePipeline(final BcfListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{

		for (final BcfListSailingsRequestHandler handler : getHandlers())
		{
			handler.handle(listSailingsRequestData, originDestinationOption, fareSearchRequestData);
		}

	}

	protected List<BcfListSailingsRequestHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<BcfListSailingsRequestHandler> handlers)
	{
		this.handlers = handlers;
	}
}
