/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.listsailings.request.manager.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.integration.listSailingRequest.data.ListSailingsRequestData;
import com.bcf.integration.listsailings.request.handler.ListSailingsRequestHandler;
import com.bcf.integration.listsailings.request.manager.BCFListSailingsRequestPipelineManager;


public class DefaultBCFListSailingsRequestPipelineManager implements BCFListSailingsRequestPipelineManager
{

	private List<ListSailingsRequestHandler> handlers;

	@Override
	public void executePipeline(final ListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{

		for (final ListSailingsRequestHandler handler : getHandlers())
		{
			handler.handle(listSailingsRequestData, originDestinationOption, fareSearchRequestData);
		}

	}

	protected List<ListSailingsRequestHandler> getHandlers()
	{
		return handlers;
	}

	@Required
	public void setHandlers(final List<ListSailingsRequestHandler> handlers)
	{
		this.handlers = handlers;
	}



}
