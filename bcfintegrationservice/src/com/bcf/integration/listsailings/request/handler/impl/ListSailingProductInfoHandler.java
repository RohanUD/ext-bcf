/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.listsailings.request.handler.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.ferry.AccessibilitytData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.common.data.AmenityInfo;
import com.bcf.integration.common.data.CustomerInfo;
import com.bcf.integration.common.data.Product;
import com.bcf.integration.common.data.Products;
import com.bcf.integration.common.data.VehicleInfo;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.ebooking.pipelinemanager.util.VehicleDimensionUtil;
import com.bcf.integration.listSailingRequest.data.BcfListSailingsRequestData;
import com.bcf.integration.listsailings.request.handler.BcfListSailingsRequestHandler;


public class ListSailingProductInfoHandler implements BcfListSailingsRequestHandler
{
	private TravelRouteService travelRouteService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void handle(final BcfListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{
		final List<Product> productList = new ArrayList<>();
		final Products products = new Products();
		for (final PassengerTypeQuantityData passengerTypes : fareSearchRequestData.getPassengerTypes())
		{
			final Product product = new Product();
			if (passengerTypes.getQuantity() > 0)
			{
				final CustomerInfo customerInfo = new CustomerInfo();
				final List<CustomerInfo> customerInfos = new ArrayList<>();

				product.setProductId(passengerTypes.getPassengerType().getBcfCode());
				product.setProductNumber(passengerTypes.getQuantity());
				product.setProductCategory(BcfCoreConstants.CATEGORY_PASSENGER);
				customerInfo.setPassengerCode(passengerTypes.getPassengerType().getBcfCode());
				customerInfos.add(customerInfo);
				product.setPassengerInfo(customerInfos);
				productList.add(product);
			}
		}

		if (CollectionUtils.isNotEmpty(fareSearchRequestData.getVehicleInfoData()))
		{
			final VehicleTypeQuantityData vehicleInfoData = fareSearchRequestData.getVehicleInfoData()
					.get(0);
			if (Objects.nonNull(vehicleInfoData) && vehicleInfoData.getQty() > 0)
			{
				final Product product = new Product();
				final VehicleInfo vehicleInfo = new VehicleInfo();
				product.setProductId(getVehicleCode(vehicleInfoData, fareSearchRequestData.getRouteType()));
				product.setProductNumber(1);
				product.setProductCategory(BcfCoreConstants.CATEGORY_VEHICLE);
				VehicleDimensionUtil.setVehicleDimensions(vehicleInfo, vehicleInfoData);
				product.setVehicleInfo(vehicleInfo);
				productList.add(product);
			}
		}

		if (CollectionUtils.isNotEmpty(fareSearchRequestData.getAccessibilities()))
		{
			final Map<String, List<AccessibilitytData>> accessibilityDataCodeMap = fareSearchRequestData.getAccessibilities()
					.stream()
					.collect(Collectors.groupingBy(accessibility -> accessibility.getCode()));

			for (final Map.Entry<String, List<AccessibilitytData>> mapEntry : accessibilityDataCodeMap.entrySet())
			{
				final Product product = new Product();
				final AmenityInfo amenityInfo = new AmenityInfo();
				product.setProductId(mapEntry.getKey());
				product.setProductCategory(BcfCoreConstants.CATEGORY_AMENITY);
				product.setProductNumber(mapEntry.getValue().size());
				amenityInfo.setAmenityType(mapEntry.getValue().stream().findFirst().get().getType());
				product.setAmenityInfo(Arrays.asList(amenityInfo));
				productList.add(product);

			}
		}

		if (CollectionUtils.isNotEmpty(fareSearchRequestData.getLargeItems()))
		{
			StreamUtil.safeStream(fareSearchRequestData.getLargeItems()).forEach(largeItemData -> {
				final Product product = new Product();
				product.setProductId(largeItemData.getCode());
				product.setProductCategory(BcfCoreConstants.CATEGORY_STOWAGE);
				product.setProductNumber(largeItemData.getQuantity());
				productList.add(product);
			});
		}

		products.setProduct(productList);
		listSailingsRequestData.setProducts(products);
	}

	private String getVehicleCode(final VehicleTypeQuantityData vehicleInfoData, final String routeType)
	{
		String vehicleCode = vehicleInfoData.getVehicleType().getCode();
		if (vehicleInfoData.isCarryingLivestock() && (StringUtils.equals(BcfCoreConstants.UNDER_HEIGHT, vehicleCode) || StringUtils
				.equals(BcfCoreConstants.OVERSIZE, vehicleCode)))
		{
			vehicleCode = vehicleCode + BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX;
		}
		final List<String> commercialVehicleCodes = BCFPropertiesUtils
				.convertToList(getBcfConfigurablePropertiesService().getBcfPropertyValue(BcfCoreConstants.COMMERCIAL_VEHICLE_CODES));
		if (commercialVehicleCodes.contains(vehicleCode) && vehicleInfoData.getWidth() > BcfCoreConstants.OVER5500KG_MIN_WIDTH)
		{
			if (RouteType.LONG.getCode().equalsIgnoreCase(routeType))
			{
				vehicleCode = BcfCoreConstants.C_TYPE_VEHICLE_PREFIX + (((int) Math.ceil(vehicleInfoData.getWidth())) - 1);
			}
			else
			{
				vehicleCode = BcfCoreConstants.S_TYPE_VEHICLE_PREFIX + (((int) Math.ceil(vehicleInfoData.getWidth())) - 1);
			}
		}
		return vehicleCode;
	}

	public TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

}
