/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.listsailings.request.handler.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.travelservices.services.TravelRouteService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.listSailingRequest.data.ListSailingsRequestData;
import com.bcf.integration.listSailingRequest.data.ProductData;
import com.bcf.integration.listSailingRequest.data.ProductListData;
import com.bcf.integration.listsailings.request.handler.ListSailingsRequestHandler;


public class ProductInfoHandler implements ListSailingsRequestHandler
{

	private TravelRouteService travelRouteService;

	@Override
	public void handle(final ListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{
		final List<ProductData> productList = new ArrayList<>();
		final ProductListData products = new ProductListData();

		for (final PassengerTypeQuantityData passengerTypes : fareSearchRequestData.getPassengerTypes())
		{
			final ProductData product = new ProductData();
			if (passengerTypes.getQuantity() > 0)
			{
				product.setProductId(passengerTypes.getPassengerType().getBcfCode());
				product.setProductNumber(passengerTypes.getQuantity());
				product.setProductCategory(BcfCoreConstants.CATEGORY_PASSENGER);
				productList.add(product);
			}
		}

		if (Objects.nonNull(fareSearchRequestData.getVehicleInfoData()))
		{
			final VehicleTypeQuantityData vehicleInfoData = fareSearchRequestData.getVehicleInfoData()
					.get(originDestinationOption.getReferenceNumber());
			if (Objects.nonNull(vehicleInfoData) && vehicleInfoData.getQty() > 0)
			{
				final ProductData product = new ProductData();
				product.setProductId(vehicleInfoData.getVehicleType().getCode());
				product.setProductNumber(1);
				product.setProductCategory(BcfCoreConstants.CATEGORY_VEHICLE);
				product.setProductLength((long) vehicleInfoData.getLength());
				product.setProductHeight((long) vehicleInfoData.getHeight());
				product.setProductWidth((long) vehicleInfoData.getWidth());
				product.setProductWeight(1);
				productList.add(product);
			}

		}
		products.setProduct(productList);

		listSailingsRequestData.setProducts(products);
	}

	public TravelRouteService getTravelRouteService()
	{
		return travelRouteService;
	}

	public void setTravelRouteService(final TravelRouteService travelRouteService)
	{
		this.travelRouteService = travelRouteService;
	}



}
