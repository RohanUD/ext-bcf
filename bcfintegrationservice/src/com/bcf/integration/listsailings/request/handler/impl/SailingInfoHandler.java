/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.integration.listsailings.request.handler.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.integration.listSailingRequest.data.ListSailingsRequestData;
import com.bcf.integration.listSailingRequest.data.SailingEventData;
import com.bcf.integration.listsailings.request.handler.ListSailingsRequestHandler;


public class SailingInfoHandler implements ListSailingsRequestHandler
{

	private ConfigurationService configurationService;

	@Override
	public void handle(final ListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{
		final SailingEventData sailingEventData = new SailingEventData();
		sailingEventData.setArrivalPortCode(originDestinationOption.getArrivalLocation());

		final boolean isMockEnable = getConfigurationService().getConfiguration()
				.getBoolean("ebooking.sailing.list.service.url.mock.enable", Boolean.FALSE);
		String datepattern = BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN;
		if (isMockEnable)
		{
			datepattern = BcfintegrationcoreConstants.BCF_SAILING_DATE_TIME_PATTERN;
		}
		sailingEventData.setDepartureDateTime(
				TravelDateUtils.convertDateToStringDate(originDestinationOption.getDepartureTime(), datepattern));
		sailingEventData.setDeparturePortCode(originDestinationOption.getDepartureLocation());
		listSailingsRequestData.setSailingEvent(sailingEventData);

	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}


}
