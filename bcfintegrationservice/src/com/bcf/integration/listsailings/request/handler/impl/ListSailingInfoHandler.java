/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.listsailings.request.handler.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.RouteType;
import com.bcf.integration.common.data.BCfSailingEventData;
import com.bcf.integration.common.data.DateRange;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.listSailingRequest.data.BcfListSailingsRequestData;
import com.bcf.integration.listsailings.request.handler.BcfListSailingsRequestHandler;


public class ListSailingInfoHandler implements BcfListSailingsRequestHandler
{
	private ConfigurationService configurationService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void handle(final BcfListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{
		final BCfSailingEventData sailingEventData = new BCfSailingEventData();
		sailingEventData.setArrivalPortCode(originDestinationOption.getArrivalLocation());

		final boolean isMockEnable = getConfigurationService().getConfiguration()
				.getBoolean("ebooking.sailing.list.V2.service.url.mock.enable", Boolean.FALSE);
		String datepattern = BcfintegrationcoreConstants.INTEGRATION_DATE_PATTERN;
		if (isMockEnable)
		{
			datepattern = BcfintegrationcoreConstants.BCF_SAILING_DATE_TIME_PATTERN;
		}
		sailingEventData.setDepartureDateTime(
				TravelDateUtils.convertDateToStringDate(originDestinationOption.getDepartureTime(), datepattern));
		sailingEventData.setDeparturePortCode(originDestinationOption.getDepartureLocation());
		sailingEventData.setDepartureDateRange(getDateRanges(fareSearchRequestData, originDestinationOption, datepattern));
		listSailingsRequestData.setSailingEvent(sailingEventData);
	}

	private DateRange getDateRanges(final FareSearchRequestData fareSearchRequestData,
			final OriginDestinationInfoData originDestinationOption, final String datePattern)
	{
		final DateRange dateRange = new DateRange();
		if (fareSearchRequestData.isShowCalenderView())
		{
			final String fromDate;
			final String toDate;
			if (StringUtils.equalsIgnoreCase(RouteType.LONG.getCode(), fareSearchRequestData.getRouteType()))
			{
				fromDate = TravelDateUtils
						.convertDateToStringDate(getPastDate(originDestinationOption.getDepartureTime(), Integer.parseInt(
								getBcfConfigurablePropertiesService()
										.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_BEFORE_NR))),
								datePattern);
				toDate = TravelDateUtils
						.convertDateToStringDate(getFutureDate(originDestinationOption.getDepartureTime(), Integer.parseInt(
								getBcfConfigurablePropertiesService()
										.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_AFTER_NR))),
								datePattern);
			}
			else
			{
				fromDate = TravelDateUtils
						.convertDateToStringDate(getPastDate(originDestinationOption.getDepartureTime(), Integer.parseInt(
								getBcfConfigurablePropertiesService()
										.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_BEFORE_SR))),
								datePattern);
				toDate = TravelDateUtils
						.convertDateToStringDate(getFutureDate(originDestinationOption.getDepartureTime(), Integer.parseInt(
								getBcfConfigurablePropertiesService()
										.getBcfPropertyValue(BcfintegrationserviceConstants.LIST_SAILING_CALENDER_DAYS_AFTER_SR))),
								datePattern);
			}
			dateRange.setDateFrom(fromDate);
			dateRange.setDateTo(toDate);
			return dateRange;
		}
		dateRange.setDateFrom(TravelDateUtils.convertDateToStringDate(originDestinationOption.getDepartureTime(), datePattern));
		dateRange.setDateTo(TravelDateUtils.convertDateToStringDate(originDestinationOption.getDepartureTime(), datePattern));
		return dateRange;
	}
	
	protected Date getPastDate(final Date date, final int daysToSubtract)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -daysToSubtract);
		return cal.getTime();
	}

	protected Date getFutureDate(final Date date, final int daysToAdd)
	{
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, daysToAdd);
		return cal.getTime();
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
