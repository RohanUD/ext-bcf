/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.listsailings.request.handler.impl;

import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.integration.listSailingRequest.data.ListSailingsRequestData;
import com.bcf.integration.listsailings.request.handler.ListSailingsRequestHandler;
import com.bcf.integrations.common.BookingClientSystem;


public class BookingClientSystemInfoHandler implements ListSailingsRequestHandler
{
	private IntegrationUtility integrationUtils;


	@Override
	public void handle(final ListSailingsRequestData listSailingsRequestData,
			final OriginDestinationInfoData originDestinationOption, final FareSearchRequestData fareSearchRequestData)
	{
		final BookingClientSystem bookingClientSystem = new BookingClientSystem();
		bookingClientSystem.setClientCode(getIntegrationUtils().getClientCode(fareSearchRequestData.getSalesApplication()));
		listSailingsRequestData.setBookingClientSystem(bookingClientSystem);
	}

	protected IntegrationUtility getIntegrationUtils()
	{
		return integrationUtils;
	}

	@Required
	public void setIntegrationUtils(final IntegrationUtility integrationUtils)
	{
		this.integrationUtils = integrationUtils;
	}

}
