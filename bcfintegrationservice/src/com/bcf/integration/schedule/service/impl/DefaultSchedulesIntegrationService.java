/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.schedule.service.impl;

import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.schedule.SeasonalSchedulesData;
import com.bcf.integration.schedule.service.SchedulesIntegrationService;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultSchedulesIntegrationService implements SchedulesIntegrationService
{
	private BaseRestService schedulesRestService;

	@Override
	public SeasonalSchedulesData getSeasonalSchedules(final Map<String, Object> params) throws IntegrationException
	{
		if (!MapUtils.isEmpty(params))
		{
			return (SeasonalSchedulesData) getSchedulesRestService().sendRestRequest(
					null, SeasonalSchedulesData.class,
							BcfintegrationserviceConstants.SEASONAL_SCHEDULES_URL, HttpMethod.GET, params);
		}

		return null;
	}

	protected BaseRestService getSchedulesRestService()
	{
		return schedulesRestService;
	}

	@Required
	public void setSchedulesRestService(final BaseRestService schedulesRestService)
	{
		this.schedulesRestService = schedulesRestService;
	}
}
