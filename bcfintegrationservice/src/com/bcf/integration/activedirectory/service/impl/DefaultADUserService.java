/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.activedirectory.service.impl;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.integration.activedirectory.service.ADUserService;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integration.data.ADUserLoginRequestDTO;
import com.bcf.integration.data.ADUserLoginResponseDTO;
import com.bcf.integrations.core.exception.ADServiceException;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultADUserService implements ADUserService
{
	private static final Logger LOG = Logger.getLogger(DefaultADUserService.class);
	private static final String CLIENT_SYSTEM = "adServiceClientSystem";
	private static final String CLIENT_KEY = "adServiceClientKey";

	// Default parent group id for all AS agents
	public static final String AS_AGENT_GROUP_UID = "asagentgroup";

	private Map<String, String> adUserRoleMap;
	private BaseRestService adRestService;
	private UserService userService;
	private ModelService modelService;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public UserModel authenticateAndUpdateADUser(final String username, final String password) throws IntegrationException

	{
		final ADUserLoginResponseDTO userResponse = authenticateActiveDirectoryUser(username, password);
		return getUserForUpdate(username, userResponse);
	}

	@Override
	public ADUserLoginResponseDTO authenticateActiveDirectoryUser(final String username, final String password)
			throws IntegrationException

	{
		ADUserLoginResponseDTO userResponse = null;

		userResponse = (ADUserLoginResponseDTO) getAdRestService().sendRestRequest(
				getBody(username, password), ADUserLoginResponseDTO.class,
				BcfintegrationserviceConstants.ACTIVE_DIRECTORY_AUTHENTICATE_SERVICE_URL, HttpMethod.POST);
		validateResponse(userResponse, username);

		return userResponse;
	}

	private UserModel getUserForUpdate(final String username, final ADUserLoginResponseDTO userResponse)
	{
		EmployeeModel agent = null;

		if (getUserService().isUserExisting(username))
		{
			try
			{
				agent = getUserService().getUserForUID(username, EmployeeModel.class);
				updateUserRoles(agent, userResponse, getAdUserRoleMap());
			}
			catch (final ClassMismatchException var3)
			{
				throw new BadCredentialsException("Bad credentials", var3);
			}
			catch (final UnknownIdentifierException | IllegalArgumentException var4)
			{
				throw new UsernameNotFoundException("Username not found", var4);
			}
		}
		else
		{
			agent = createUser(username, userResponse, getAdUserRoleMap());
		}
		return agent;
	}

	private ADUserLoginRequestDTO getBody(final String username, final String password)
	{
		final ADUserLoginRequestDTO body = new ADUserLoginRequestDTO();
		body.setNetworkId(username);
		body.setPassword(password);
		body.setClientSystem(getBcfConfigurablePropertiesService().getBcfPropertyValue(CLIENT_SYSTEM));
		body.setClientKey(getBcfConfigurablePropertiesService().getBcfPropertyValue(CLIENT_KEY));

		return body;
	}

	private void validateResponse(final ADUserLoginResponseDTO userResponse, final String username) throws ADServiceException
	{
		if (userResponse == null)
		{
			LOG.error(String.format("Null response received for Agent: [%s]", username));
			throw new ADServiceException(BcfintegrationcoreConstants.SERVICE_UNAVAILABLE);
		}
		else if (userResponse.getRoles().isEmpty())
		{
			LOG.info(String.format("No user roles returned for Agent: [%s]", username));
			throw new ADServiceException(BcfintegrationcoreConstants.NOT_AUTHORISED);
		}
	}

	@Override
	public EmployeeModel createUser(final String username, final ADUserLoginResponseDTO userResponse,
			final Map<String, String> adUserRoleMap)
	{
		final EmployeeModel agent = getModelService().create(EmployeeModel.class);
		agent.setUid(username);
		agent.setName(username);

		final Set<PrincipalGroupModel> groupSet = new HashSet<>();
		for (final String role : userResponse.getRoles())
		{
			if (adUserRoleMap.get(role) == null)
			{
				continue;
			}
			final PrincipalGroupModel group = getUserService().getUserGroupForUID(adUserRoleMap.get(role));
			groupSet.add(group);
		}
		agent.setGroups(groupSet);
		getModelService().save(agent);
		return agent;
	}

	@Override
	public void updateUserRoles(final EmployeeModel employee, final ADUserLoginResponseDTO userResponse,
			final Map<String, String> adUserRoleMap)
	{
		final Set<PrincipalGroupModel> groupSet = new HashSet<>();
		for (final String role : userResponse.getRoles())
		{
			if (adUserRoleMap.get(role) == null)
			{
				continue;
			}
			final PrincipalGroupModel group = getUserService().getUserGroupForUID(adUserRoleMap.get(role));
			groupSet.add(group);
		}
		final PrincipalGroupModel asAgentGroup = getUserService().getUserGroupForUID(AS_AGENT_GROUP_UID);
		groupSet.add(asAgentGroup);
		employee.setGroups(groupSet);
		getModelService().save(employee);
	}


	protected BaseRestService getAdRestService()
	{
		return adRestService;
	}

	@Required
	public void setAdRestService(final BaseRestService adRestService)
	{
		this.adRestService = adRestService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public Map<String, String> getAdUserRoleMap()
	{
		return adUserRoleMap;
	}

	@Required
	public void setAdUserRoleMap(final Map<String, String> adUserRoleMap)
	{
		this.adUserRoleMap = adUserRoleMap;
	}

}
