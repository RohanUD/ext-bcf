/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.logger;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Utility class to print Request and Response of WebService
 */
public class WSMessageLogger
{

	private static final Logger LOG = Logger.getLogger(WSMessageLogger.class);

	private WSMessageLogger()
	{
		throw new IllegalAccessError("Utility class WSMessageLogger cannot be instantiated.");
	}

	public static void logMessage(final Object value, String type, final String url, String header)
	{
		final ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);

		String jsonInString = StringUtils.EMPTY;
		try
		{
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value);
		}
		catch (final IOException e)
		{
			LOG.error(e);
		}

		LOG.debug("######################### " + type + " :::START:::###########################");
		LOG.debug("URL :" + url);
		LOG.debug("Header :" + header);
		LOG.debug(jsonInString);
		LOG.debug("######################### " + type + " :::END:::###########################");

	}
}
