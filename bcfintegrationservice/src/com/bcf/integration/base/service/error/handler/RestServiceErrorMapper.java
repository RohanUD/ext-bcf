/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public interface RestServiceErrorMapper
{
	ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException, IntegrationException;

	ErrorListDTO getDefaultError();

	String getErrorMessage(final String errorCode);
}
