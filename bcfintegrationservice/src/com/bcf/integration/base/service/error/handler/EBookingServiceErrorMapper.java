/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.bcf.integration.data.BaseErrorResponseDTO;
import com.bcf.integration.error.EbookingErrorDTO;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integration.error.FaultNotification;


public class EBookingServiceErrorMapper extends IntegrationServiceErrorMapper
{
	@Override
	public ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException
	{
		final BaseErrorResponseDTO errorResponse = (BaseErrorResponseDTO) getObjectMapper().readValue(responseString,
				Class.forName(getRestServiceErrorResponseBeanMap().get(serviceURLKey)));
		return createErrorListDTO(errorResponse);
	}

	private ErrorListDTO createErrorListDTO(final BaseErrorResponseDTO errorResponse)
	{
		if (errorResponse instanceof EbookingErrorDTO)
		{
			return populateErrorListDTO((EbookingErrorDTO) errorResponse);
		}
		return null;
	}

	private ErrorListDTO populateErrorListDTO(final EbookingErrorDTO errorResponse)
	{
		final ErrorListDTO errorListDTO = new ErrorListDTO();
		final List<ErrorDTO> errorList = new ArrayList<>();
		errorListDTO.setErrorDto(errorList);
		errorResponse.getFaultNotification().stream().forEach(faultNotification -> setErrorDTO(errorList, faultNotification));
		return errorListDTO;
	}

	private void setErrorDTO(final List<ErrorDTO> errorList, final FaultNotification faultNotification)
	{
		final ErrorDTO error = new ErrorDTO();
		error.setErrorCode(faultNotification.getFaultCode());
		error.setErrorDetail(StringUtils.isNotEmpty(getErrorMessage(faultNotification.getFaultCode())) ?
				getErrorMessage(faultNotification.getFaultCode()) :
				faultNotification.getFaultText());
		errorList.add(error);
	}
}
