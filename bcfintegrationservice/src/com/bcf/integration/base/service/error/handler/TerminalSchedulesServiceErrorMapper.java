package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.bcf.integration.data.BaseErrorResponseDTO;
import com.bcf.integration.error.CurrentConditionsErrorDTO;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integration.error.ErrorListDTO;


public class TerminalSchedulesServiceErrorMapper extends IntegrationServiceErrorMapper
{
	@Override
	public ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException
	{
		final BaseErrorResponseDTO errorResponse = (BaseErrorResponseDTO) getObjectMapper().readValue(responseString,
				Class.forName(getRestServiceErrorResponseBeanMap().get(serviceURLKey)));
		return createErrorListDTO(errorResponse);
	}

	private ErrorListDTO createErrorListDTO(final BaseErrorResponseDTO errorResponse)
	{
		if (errorResponse instanceof CurrentConditionsErrorDTO)
		{
			return populateErrorListDTO((CurrentConditionsErrorDTO) errorResponse);
		}
		return null;
	}

	private ErrorListDTO populateErrorListDTO(final CurrentConditionsErrorDTO errorResponse)
	{
		final ErrorListDTO errorListDTO = new ErrorListDTO();
		final List<ErrorDTO> errorList = new ArrayList<>();
		errorListDTO.setErrorDto(errorList);
		errorList.add(setErrorDTO(errorResponse));
		return errorListDTO;
	}

	private ErrorDTO setErrorDTO(final CurrentConditionsErrorDTO errorResponse)
	{
		final ErrorDTO error = new ErrorDTO();
		error.setErrorCode(errorResponse.getCode());
		error.setErrorDetail(errorResponse.getMessage());
		return error;
	}
}
