/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.custom.propertysource.model.PropertySourceModel;
import com.bcf.custom.propertysource.service.PropertySourceService;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class IntegrationServiceErrorMapper implements RestServiceErrorMapper
{
	private PropertySourceService propertySourceService;
	private Map<String, String> restServiceErrorResponseBeanMap;
	private ObjectMapper objectMapper;

	private static final String DEFAULT_ERROR_CODE = "integration.systemerror";

	@Override
	public ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException, IntegrationException
	{
		return getDefaultError();
	}

	@Override
	public ErrorListDTO getDefaultError()
	{
		final ErrorListDTO errorListDTO = new ErrorListDTO();
		final List<ErrorDTO> errorList = new ArrayList<>();
		errorListDTO.setErrorDto(errorList);
		final ErrorDTO error = new ErrorDTO();
		error.setErrorCode(DEFAULT_ERROR_CODE);

		final PropertySourceModel propertySourceModel = getPropertySourceService().getPropertySourceForCode(DEFAULT_ERROR_CODE);

		if (propertySourceModel != null)
		{
			error.setErrorDetail(propertySourceModel.getValue());
		}
		errorList.add(error);

		return errorListDTO;
	}

	@Override
	public String getErrorMessage(final String errorCode)
	{
		final PropertySourceModel propertySourceModel = getPropertySourceService()
				.getPropertySourceForCode(errorCode);

		if (propertySourceModel != null)
		{
			return propertySourceModel.getValue();
		}
		return null;
	}

	protected Map<String, String> getRestServiceErrorResponseBeanMap()
	{
		return restServiceErrorResponseBeanMap;
	}

	@Required
	public void setRestServiceErrorResponseBeanMap(final Map<String, String> restServiceErrorResponseBeanMap)
	{
		this.restServiceErrorResponseBeanMap = restServiceErrorResponseBeanMap;
	}

	protected ObjectMapper getObjectMapper()
	{
		return objectMapper;
	}

	@Required
	public void setObjectMapper(final ObjectMapper objectMapper)
	{
		this.objectMapper = objectMapper;
	}

	protected PropertySourceService getPropertySourceService()
	{
		return propertySourceService;
	}

	@Required
	public void setPropertySourceService(final PropertySourceService propertySourceService)
	{
		this.propertySourceService = propertySourceService;
	}
}
