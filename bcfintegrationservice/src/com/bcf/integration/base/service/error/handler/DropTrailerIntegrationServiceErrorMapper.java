/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import com.bcf.integration.data.BaseErrorResponseDTO;
import com.bcf.integration.error.DropTrailerErrorDTO;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integration.error.ErrorListDTO;


public class DropTrailerIntegrationServiceErrorMapper extends IntegrationServiceErrorMapper
{
	@Override
	public ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException
	{
		final BaseErrorResponseDTO errorResponse = (BaseErrorResponseDTO) getObjectMapper().readValue(responseString,
				Class.forName(getRestServiceErrorResponseBeanMap().get(serviceURLKey)));
		return createErrorListDTO(errorResponse);
	}

	private ErrorListDTO createErrorListDTO(final BaseErrorResponseDTO errorResponse)
	{
		if (errorResponse instanceof DropTrailerErrorDTO)
		{
			return populateErrorListDTO((DropTrailerErrorDTO) errorResponse);
		}
		return null;
	}

	private ErrorListDTO populateErrorListDTO(final DropTrailerErrorDTO errorResponse)
	{
		final ErrorListDTO errorListDTO = new ErrorListDTO();
		errorListDTO.setErrorDto(Arrays.asList(createErrorDTO(errorResponse)));
		return errorListDTO;
	}

	private ErrorDTO createErrorDTO(final DropTrailerErrorDTO errorResponse)
	{
		final ErrorDTO error = new ErrorDTO();
		error.setErrorCode(errorResponse.getCode());
		error.setErrorDetail(StringUtils.isNotEmpty(getErrorMessage(errorResponse.getCode())) ?
				getErrorMessage(errorResponse.getCode()) :
				errorResponse.getMessage());
		return error;
	}
}
