/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;


public class IntegrationServiceErrorHandler extends DefaultResponseErrorHandler implements ResponseErrorHandler
{

	@Override
	public boolean hasError(final ClientHttpResponse response) throws IOException
	{
		return super.hasError(response);
	}

	@Override
	public void handleError(final ClientHttpResponse paramClientHttpResponse)
	{
		// deliberately left empty
	}

}
