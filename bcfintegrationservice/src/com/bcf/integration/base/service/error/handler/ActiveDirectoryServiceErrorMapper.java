/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.error.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.bcf.integration.data.ADUserLoginResponseDTO;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class ActiveDirectoryServiceErrorMapper extends IntegrationServiceErrorMapper
{
	@Override
	public ErrorListDTO mapErrorResponse(final String responseString, final String serviceURLKey)
			throws IOException, ClassNotFoundException, IntegrationException
	{
		final String className = getRestServiceErrorResponseBeanMap().get(serviceURLKey);
		if(StringUtils.isEmpty(className)) {
			throw new IntegrationException(responseString);
		}
		final ADUserLoginResponseDTO errorResponse = (ADUserLoginResponseDTO) getObjectMapper().readValue(responseString,
				Class.forName(className));
		return createErrorListDTO(errorResponse);
	}

	private ErrorListDTO createErrorListDTO(final ADUserLoginResponseDTO errorResponse)
	{
		final ErrorListDTO errorListDTO = new ErrorListDTO();
		final List<ErrorDTO> errorList = new ArrayList<>();
		errorList.add(createErrorDTO(errorResponse));
		errorListDTO.setErrorDto(errorList);
		return errorListDTO;
	}

	private ErrorDTO createErrorDTO(final ADUserLoginResponseDTO errorResponse)
	{
		final ErrorDTO error = new ErrorDTO();
		error.setErrorCode(errorResponse.getErrorType());
		error.setErrorDetail(StringUtils.isNotEmpty(getErrorMessage(errorResponse.getErrorType())) ?
				getErrorMessage(errorResponse.getErrorType()) :
				errorResponse.getErrorMessage());
		return error;
	}
}
