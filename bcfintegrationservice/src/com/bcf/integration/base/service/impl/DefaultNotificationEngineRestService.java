/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.impl;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import com.bcf.integration.base.service.NotificationEngineRestService;
import com.bcf.integration.base.service.logger.WSMessageLogger;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integration.helper.NotificationEngineRestServiceHelper;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.order.common.BaseContextRequestData;


public class DefaultNotificationEngineRestService extends AbstractRestService implements NotificationEngineRestService
{
	private static final Logger LOG = Logger.getLogger(DefaultBaseRestService.class);

	@Resource
	NotificationEngineRestServiceHelper notificationEngineRestServiceHelper;

	@Override
	public void sendRestRequest(final BaseContextRequestData body, final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey) throws IntegrationException
	{
		sendRestRequest(body, responseClass, httpMethod, serviceURLKey, new HttpHeaders());
	}

	@Override
	public void sendRestRequest(final BaseContextRequestData requestBody, final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders)
			throws IntegrationException
	{
		if (Objects.nonNull(requestBody))
		{

			notificationEngineRestServiceHelper.updateEmailTemplateVersionInRequestBody(requestBody);

			final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
			final String serviceUrl = makeServiceUrl(serviceURLKey, new HashMap<>());
			// Request Print in logs
			WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
					String.format("Request Headers: %s", requestHeaders.toString()));
			try
			{
				final ResponseEntity<?> responseEntity = getRestTemplate().exchange(URI.create(serviceUrl), httpMethod,
						requestEntity, String.class);
				final String responseString = (String) responseEntity.getBody();
				LOG.error("Notification Response-------" + responseString);
				LOG.error("Notification Response Status-------" + responseEntity.getStatusCode());
				if (isError(responseEntity.getStatusCode()))
				{

					final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString,
							serviceURLKey);

					// Response Print in logs
					WSMessageLogger.logMessage(error, "Response", serviceUrl,
							String.format("Response Headers: %s",
									responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString()
											: StringUtils.EMPTY));
					throw new IntegrationException("error returned", error);
				}

			}
			catch (final IOException | RestClientException | ClassNotFoundException e)
			{
				LOG.error(e);
				final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
				throw new IntegrationException("Exception occured", error);
			}
		}
	}

	protected boolean isError(final HttpStatus statusCode)
	{
		return statusCode.is4xxClientError() || statusCode.is5xxServerError();
	}




}
