/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 7/24/19 12:11 PM
 */

package com.bcf.integration.base.service.impl;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.base.service.logger.WSMessageLogger;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultTripAdvisorRestService extends DefaultBaseRestService implements BaseRestService
{
	private static final Logger LOG = Logger.getLogger(DefaultTripAdvisorRestService.class);

	private String proxyHost;

	private String proxyPort;

	private String proxyUser;

	private String proxyPassword;

	private boolean enableProxy;

	private RestTemplate tripAdvisorRestTemplate;

	@PostConstruct
	public void init()
	{
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			tripAdvisorRestTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			tripAdvisorRestTemplate = getRestTemplate();
		}

		if (enableProxy)
		{
			final int proxyPortNum = Integer.parseInt(proxyPort);
			final CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider
					.setCredentials(new AuthScope(proxyHost, proxyPortNum), new UsernamePasswordCredentials(proxyUser, proxyPassword));

			final HttpClientBuilder clientBuilder = HttpClientBuilder.create();
			clientBuilder.useSystemProperties();
			clientBuilder.setProxy(new HttpHost(proxyHost, proxyPortNum));
			clientBuilder.setDefaultCredentialsProvider(credsProvider);
			clientBuilder.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy());
			final CloseableHttpClient client = clientBuilder.disableAutomaticRetries().build();

			final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setHttpClient(client);

			this.tripAdvisorRestTemplate.setRequestFactory(factory);
		}
	}

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, Object> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}
		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrl(serviceURLKey, params);


		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		// Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request %s", requestHeaders.toString()));

		try
		{
			final ResponseEntity<?> responseEntity = tripAdvisorRestTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				// Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl,
						String.format("Response %s",
								responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString()
										: StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final BaseResponseDTO responseBody = (BaseResponseDTO) getObjectMapper().readValue(responseString,
					responseClass);

			// Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took for " + serviceURLKey + " " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}

	public String getProxyHost()
	{
		return proxyHost;
	}

	public void setProxyHost(final String proxyHost)
	{
		this.proxyHost = proxyHost;
	}

	public String getProxyPort()
	{
		return proxyPort;
	}

	public void setProxyPort(final String proxyPort)
	{
		this.proxyPort = proxyPort;
	}

	public String getProxyUser()
	{
		return proxyUser;
	}

	public void setProxyUser(final String proxyUser)
	{
		this.proxyUser = proxyUser;
	}

	public String getProxyPassword()
	{
		return proxyPassword;
	}

	public void setProxyPassword(final String proxyPassword)
	{
		this.proxyPassword = proxyPassword;
	}

	public boolean isEnableProxy()
	{
		return enableProxy;
	}

	public void setEnableProxy(final boolean enableProxy)
	{
		this.enableProxy = enableProxy;
	}

	public RestTemplate getTripAdvisorRestTemplate()
	{
		return tripAdvisorRestTemplate;
	}

	public void setTripAdvisorRestTemplate(final RestTemplate tripAdvisorRestTemplate)
	{
		this.tripAdvisorRestTemplate = tripAdvisorRestTemplate;
	}
}
