/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 7/6/19 7:48 AM
 */

package com.bcf.integration.base.service.impl;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.bcf.integration.base.service.CurrentConditionRestService;
import com.bcf.integration.base.service.logger.WSMessageLogger;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.TerminalSchedulesRouteData;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultCurrentConditionRestService extends DefaultBaseRestService implements CurrentConditionRestService
{
	private static final Logger LOG = Logger.getLogger(DefaultCurrentConditionRestService.class);

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final String serviceURLKey, final HttpMethod httpMethod, final Map<String, Object> params)
			throws IntegrationException
	{
		return sendRestRequest(requestBody, responseClass, httpMethod, serviceURLKey, new HttpHeaders(), params);
	}

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, Object> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}
		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrl(serviceURLKey, params);

		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		// Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request %s", requestHeaders.toString()));

		final RestTemplate restTemplate;
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			restTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			restTemplate = getRestTemplate();
		}
		try
		{
			final ResponseEntity<?> responseEntity = restTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				// Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl,
						String.format("Response %s",
								responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString()
										: StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final BaseResponseDTO responseBody = (BaseResponseDTO) getObjectMapper().readValue(responseString,
					responseClass);

			// Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}

	@Override
	public Map<String, Map<String, List<TerminalSchedulesRouteData>>> sendRestRequestForTerminalsWithMapResponse(
			final BaseRequestDTO requestBody,
			final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, List<String>> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}

		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrlWithMultiKeys(serviceURLKey, params);

		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		//Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request Headers: %s", requestHeaders.toString()));

		final RestTemplate restTemplate;
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			restTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			restTemplate = getRestTemplate();
		}

		try
		{
			final ResponseEntity<?> responseEntity = restTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				//Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
						responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final Map<String, Map<String, List<TerminalSchedulesRouteData>>> responseBody = (Map<String, Map<String, List<TerminalSchedulesRouteData>>>) getObjectMapper()
					.readValue(responseString, responseClass);

			//Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}

	@Override
	public BaseResponseDTO[] sendRestRequestForNextSailingsWithArrayResponse(final BaseRequestDTO requestBody,
			final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, Object> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}

		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrl(serviceURLKey, params);

		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		//Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request Headers: %s", requestHeaders.toString()));

		final RestTemplate restTemplate;
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			restTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			restTemplate = getRestTemplate();
		}

		try
		{
			final ResponseEntity<?> responseEntity = restTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				//Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
						responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final ScheduleArrivalDepartureData[] responseBody = (ScheduleArrivalDepartureData[]) getObjectMapper()
					.readValue(responseString, responseClass);

			//Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}

	@Override
	public BaseResponseDTO[] sendRestRequestWithArrayResponse(final BaseRequestDTO requestBody,
			final Class<?> responseClass,
			final String serviceURLKey, final HttpMethod httpMethod, final Map<String, List<String>> params)
			throws IntegrationException
	{
		return sendRestRequestWithArrayResponse(requestBody, responseClass, httpMethod, serviceURLKey, new HttpHeaders(),
				params);
	}

	@Override
	public BaseResponseDTO[] sendRestRequestWithArrayResponse(final BaseRequestDTO requestBody,
			final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, List<String>> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}

		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrlWithMultiKeys(serviceURLKey, params);

		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		//Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request Headers: %s", requestHeaders.toString()));

		final RestTemplate restTemplate;
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			restTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			restTemplate = getRestTemplate();
		}

		try
		{
			final ResponseEntity<?> responseEntity = restTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				//Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
						responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final CurrentConditionsResponseData[] responseBody = (CurrentConditionsResponseData[]) getObjectMapper()
					.readValue(responseString, responseClass);

			//Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}
}
