/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriTemplate;
import com.bcf.integration.base.service.error.handler.RestServiceErrorMapper;
import com.bcf.integration.token.service.BCFAPIGatewayTokenService;
import com.fasterxml.jackson.databind.ObjectMapper;


public abstract class AbstractRestService
{

	private static final Logger LOG = Logger.getLogger(AbstractRestService.class);

	private ConfigurationService configurationService;
	private Map<String, String> restServiceUrlMap;
	private RestTemplate restTemplate;
	private ObjectMapper objectMapper;
	private RestServiceErrorMapper restServiceErrorMapper;
	private BCFAPIGatewayTokenService bcfAPIGatewayTokenService;


	protected HttpHeaders getRequestHeaders(final HttpHeaders requestHeaders)
	{
		requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		if (Objects.isNull(requestHeaders.getContentType()))
		{
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		}
		return requestHeaders;
	}

	protected String makeServiceUrl(final String serviceUrlKey, final Map<String, Object> params)
	{
		LOG.info("Service URL Key---------" + serviceUrlKey);
		final String serviceUrlValue = getRestServiceUrlMap().get(serviceUrlKey);
		final boolean isMockEnable = getConfigurationService().getConfiguration()
				.getBoolean(serviceUrlValue + ".mock.enable", Boolean.FALSE);
		LOG.info("Mock Enabled ---------" + isMockEnable);
		if (isMockEnable)
		{
			return manageUrlParams(getConfigurationService().getConfiguration().getString("mock." + serviceUrlValue),
					params);
		}
		return manageUrlParams(
				getConfigurationService().getConfiguration().getString(serviceUrlValue, StringUtils.EMPTY), params);
	}

	protected String makeServiceUrlWithMultiKeys(final String serviceUrlKey, final Map<String, List<String>> params)
	{
		LOG.info("Service URL Key---------" + serviceUrlKey);
		final String serviceUrlValue = getRestServiceUrlMap().get(serviceUrlKey);
		final boolean isMockEnable = getConfigurationService().getConfiguration()
				.getBoolean(serviceUrlValue + ".mock.enable", Boolean.FALSE);
		LOG.info("Mock Enabled ---------" + isMockEnable);
		if (isMockEnable)
		{
			return manageUrlParamsWithMultiKeys(getConfigurationService().getConfiguration().getString("mock." + serviceUrlValue),
					params);
		}
		return manageUrlParamsWithMultiKeys(
				getConfigurationService().getConfiguration().getString(serviceUrlValue, StringUtils.EMPTY), params);
	}

	protected String manageUrlParams(final String url, final Map<String, Object> params)
	{
		LOG.info("Service URL ---------" + url);
		if (StringUtils.isEmpty(url))
		{
			return "";
		}
		final UriTemplate template = new UriTemplate(url);
		final Map<String, String> pathParamMap = new HashMap<>();
		template.getVariableNames().forEach(pathParam -> {
			pathParamMap.put(pathParam, (String)params.get(pathParam));
			params.remove(pathParam);
		});



		final UriBuilder builders = UriBuilder.fromPath(url);
		final URI uri = builders.buildFromEncodedMap(pathParamMap);
		final UriComponentsBuilder builder = UriComponentsBuilder.fromUri(uri);
		for (final Map.Entry<String, Object> entry : params.entrySet())
		{
			builder.queryParam(entry.getKey(), entry.getValue());
		}
		LOG.info("Service URL with param---------" + builder.build(true).toUri().toString());
		return builder.build(true).toUri().toString();
	}

	protected String manageUrlParamsWithMultiKeys(final String url, final Map<String, List<String>> params)
	{
		final URI uri = UriComponentsBuilder.fromHttpUrl(url).build().toUri();
		final UriComponentsBuilder builder = UriComponentsBuilder.fromUri(uri);
		for (final Map.Entry<String, List<String>> entry : params.entrySet())
		{
			entry.getValue().forEach(s -> builder.queryParam(entry.getKey(), s));

		}

		return builder.build(true).toUri().toString();
	}

	protected RestTemplate getRestTemplateByPassingHostNameVerification()
	{
		final TrustStrategy acceptingTrustStrategy = (X509Certificate[] x509Certificates, String s) -> true;
		final SSLContext sslContext;
		try
		{
			sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
					.build();
		}
		catch (final NoSuchAlgorithmException | KeyManagementException | KeyStoreException e)
		{
			LOG.error("Exception encountered while setting custom SSL context", e);
			LOG.debug("Falling back to default RestTemplate");
			return getRestTemplate();
		}
		final SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
		final CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).disableAutomaticRetries().build();
		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		restTemplate.setRequestFactory(requestFactory);
		return restTemplate;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected Map<String, String> getRestServiceUrlMap()
	{
		return restServiceUrlMap;
	}

	public void setRestServiceUrlMap(final Map<String, String> restServiceUrlMap)
	{
		this.restServiceUrlMap = restServiceUrlMap;
	}

	protected RestTemplate getRestTemplate()
	{
		return restTemplate;
	}

	public void setRestTemplate(final RestTemplate restTemplate)
	{
		this.restTemplate = restTemplate;
	}

	protected ObjectMapper getObjectMapper()
	{
		return objectMapper;
	}

	public void setObjectMapper(final ObjectMapper objectMapper)
	{
		this.objectMapper = objectMapper;
	}

	protected RestServiceErrorMapper getRestServiceErrorMapper()
	{
		return restServiceErrorMapper;
	}

	public void setRestServiceErrorMapper(final RestServiceErrorMapper restServiceErrorMapper)
	{
		this.restServiceErrorMapper = restServiceErrorMapper;
	}

	protected BCFAPIGatewayTokenService getBcfAPIGatewayTokenService()
	{
		return bcfAPIGatewayTokenService;
	}

	public void setBcfAPIGatewayTokenService(final BCFAPIGatewayTokenService bcfAPIGatewayTokenService)
	{
		this.bcfAPIGatewayTokenService = bcfAPIGatewayTokenService;
	}


}
