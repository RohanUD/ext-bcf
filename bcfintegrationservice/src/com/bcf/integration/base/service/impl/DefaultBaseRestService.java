/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.integration.base.service.impl;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.bcf.integration.base.service.BaseRestService;
import com.bcf.integration.base.service.logger.WSMessageLogger;
import com.bcf.integration.data.BaseRequestDTO;
import com.bcf.integration.data.BaseResponseDTO;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


public class DefaultBaseRestService extends AbstractRestService implements BaseRestService
{

	private static final Logger LOG = Logger.getLogger(DefaultBaseRestService.class);

	protected static final String TRUST_ALL_SSL_CERTS = "trust.all.ssl.certs.external.api.call";
	protected static final String RESPONSE = "Response";
	protected static final String RESPONSE_HEADERS = "Response Headers: %s";

	private boolean addTokenHeader;

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final String serviceURLKey, final HttpMethod httpMethod) throws IntegrationException
	{
		return sendRestRequest(requestBody, responseClass, httpMethod, serviceURLKey, new HttpHeaders(),
				new HashMap<>());
	}

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final String serviceURLKey, final HttpMethod httpMethod, final Map<String, Object> params)
			throws IntegrationException
	{
		return sendRestRequest(requestBody, responseClass, httpMethod, serviceURLKey, new HttpHeaders(), params);
	}

	@Override
	public BaseResponseDTO sendRestRequest(final BaseRequestDTO requestBody, final Class<?> responseClass,
			final HttpMethod httpMethod, final String serviceURLKey, final HttpHeaders requestHeaders,
			final Map<String, Object> params) throws IntegrationException
	{
		if (getAddTokenHeader())
		{
			requestHeaders.set("Authorization", "Bearer " + getBcfAPIGatewayTokenService().getOAuth2Token());
		}
		final HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, getRequestHeaders(requestHeaders));
		final String serviceUrl = makeServiceUrl(serviceURLKey, params);


		final Date startDate = new Date();
		LOG.info("Start of invoking " + serviceUrl + ", at " + startDate.toString());

		// Request Print in logs
		WSMessageLogger.logMessage(requestBody, "Request", serviceUrl,
				String.format("Request %s", requestHeaders.toString()));

		final RestTemplate restTemplate;
		if (getConfigurationService().getConfiguration().getBoolean(TRUST_ALL_SSL_CERTS))
		{
			restTemplate = getRestTemplateByPassingHostNameVerification();
		}
		else
		{
			restTemplate = getRestTemplate();
		}
		try
		{
			final ResponseEntity<?> responseEntity = restTemplate
					.exchange(URI.create(serviceUrl), httpMethod, requestEntity, String.class);
			final String responseString = (String) responseEntity.getBody();

			if (isError(responseEntity.getStatusCode()))
			{
				final ErrorListDTO error = getRestServiceErrorMapper().mapErrorResponse(responseString, serviceURLKey);

				// Response Print in logs
				WSMessageLogger.logMessage(error, RESPONSE, serviceUrl,
						String.format("Response %s",
								responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString()
										: StringUtils.EMPTY));
				throw new IntegrationException("error returned", error);
			}
			final BaseResponseDTO responseBody = (BaseResponseDTO) getObjectMapper().readValue(responseString,
					responseClass);

			// Response Print in logs
			WSMessageLogger.logMessage(responseBody, RESPONSE, serviceUrl, String.format(RESPONSE_HEADERS,
					responseEntity.getHeaders() != null ? responseEntity.getHeaders().toString() : StringUtils.EMPTY));

			final Date endDate = new Date();
			LOG.info("End of invoking " + serviceUrl + ", at " + endDate.toString());
			final long diff = endDate.getTime() - startDate.getTime();
			LOG.info("Time took for " + serviceURLKey + " " + TimeUnit.MILLISECONDS.toSeconds(diff) + " seconds.");

			return responseBody;
		}
		catch (final IOException | RestClientException | ClassNotFoundException e)
		{
			LOG.error(e);
			final ErrorListDTO error = getRestServiceErrorMapper().getDefaultError();
			throw new IntegrationException("Exception occured", error);
		}
	}

	protected boolean isError(final HttpStatus statusCode)
	{
		return statusCode.is4xxClientError() || statusCode.is5xxServerError();
	}

	protected boolean getAddTokenHeader()
	{
		return addTokenHeader;
	}

	public void setAddTokenHeader(final boolean addTokenHeader)
	{
		this.addTokenHeader = addTokenHeader;
	}

}
