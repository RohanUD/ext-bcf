/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.custom.integrations.soap;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.support.channel.BeanFactoryChannelResolver;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.core.DestinationResolver;
import org.springframework.messaging.support.MessageBuilder;



public class SoapDemoFahToCal
{

	public void testSoap()
	{


		final ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("custom-spring-soap-integration.xml");
		final DestinationResolver<MessageChannel> channelResolver = new BeanFactoryChannelResolver(context);

		final String requestXml = "<GetCitiesByCountry xmlns=\"http://www.webserviceX.NET\"><CountryName>india</CountryName></GetCitiesByCountry>";

		// Create the Message object
		final Message<String> message = MessageBuilder.withPayload(requestXml).build();

		// Send the Message to the handler's input channel
		final MessageChannel channel = channelResolver.resolveDestination("wiremockRequestChannel");

		channel.send(message);

	}

}
