/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.initialdata.constants;

/**
 * Global class for all BcfInitialData constants.
 */
public final class BcfInitialDataConstants extends GeneratedBcfInitialDataConstants
{
	public static final String EXTENSIONNAME = "bcfinitialdata";

	private BcfInitialDataConstants()
	{
		//empty
	}
}
