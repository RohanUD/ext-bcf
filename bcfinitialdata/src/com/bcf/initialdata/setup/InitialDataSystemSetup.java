/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.ruleengineservices.RuleEngineServiceException;
import de.hybris.platform.ruleengineservices.maintenance.RuleMaintenanceService;
import de.hybris.platform.ruleengineservices.model.SourceRuleModel;
import de.hybris.platform.ruleengineservices.rule.services.RuleService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.initialdata.constants.BcfInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = BcfInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String IMPORT_CONTENT_CATALOGS_SAMPLE_DATA = "importContentCatalogsSampleData";

	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private static final String VACATIONS = "bcvacation";
	private static final String FERRIES = "bcferries";

	private static final String NONE = "none";

	private static final String SITE_SELECTION_PARAM = "siteSelectionParam";

	private static final String FERRIES_SAMPLE_DATA = "ferriesSampleData";
	private static final String MINIMAL_FERRIES_DATA = "minimalFerriesData";

	private static final String VACATIONS_SAMPLE_DATA = "vacationsSampleData";
	private static final String MINIMAL_VACATIONS_DATA = "minimalVacationsData";

	private static final String BCF_INITIAL_DATA = "bcfinitialdata";

	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;
	private ConfigurationService configurationService;

	@Resource(name = "defaultCronJobService")
	private CronJobService cronjobService;

	@Resource
	private RuleService ruleService;

	@Resource
	private RuleMaintenanceService ruleMaintenanceService;

	@SystemSetupParameterMethod
	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();

		final boolean importSampleData = getConfigurationService().getConfiguration().getBoolean("import.sample.data");
		final boolean importContentCatalogsSampleData = getConfigurationService().getConfiguration()
				.getBoolean("import.sample.contentcatalogs.data");

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", importSampleData));
		params.add(createBooleanSystemSetupParameter(IMPORT_CONTENT_CATALOGS_SAMPLE_DATA, "Import Content Catalogs Sample Data",
				importContentCatalogsSampleData));

		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));
		//setup Site selection
		params.add(createSiteSelectionSystemSetupParameter());
		//setup Ferries Data
		params.add(createFerriesDataSystemSetupParameter());
		//setup Vacations Data
		params.add(createVacationsDataSystemSetupParameter());
		return params;
	}

	protected SystemSetupParameter createSiteSelectionSystemSetupParameter()
	{
		//Determining which site to initialize based on configuration in local.properties
		String configuredSite = getConfigurationService().getConfiguration().getString("travelacc.sites");
		if (StringUtils.isEmpty(configuredSite) || !Arrays.asList(FERRIES, VACATIONS).contains(configuredSite))
		{
			//fallback to default configuration if travelacc.sites is empty or incorrect
			configuredSite = VACATIONS;
		}

		final SystemSetupParameter param = new SystemSetupParameter(SITE_SELECTION_PARAM);
		param.setLabel("Select sites:");
		param.addValue(FERRIES, StringUtils.equalsIgnoreCase(FERRIES, configuredSite));
		param.addValue(VACATIONS, StringUtils.equalsIgnoreCase(VACATIONS, configuredSite));
		return param;
	}

	protected SystemSetupParameter createFerriesDataSystemSetupParameter()
	{
		final SystemSetupParameter param = new SystemSetupParameter(FERRIES_SAMPLE_DATA);
		param.setLabel("Import Ferries Data");
		param.addValue(NONE);
		param.addValue(MINIMAL_FERRIES_DATA, true);
		return param;
	}

	protected SystemSetupParameter createVacationsDataSystemSetupParameter()
	{
		final SystemSetupParameter param = new SystemSetupParameter(VACATIONS_SAMPLE_DATA);
		param.setLabel("Import Vacations Data");
		param.addValue(NONE);
		param.addValue(MINIMAL_VACATIONS_DATA, true);
		return param;
	}

	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/bcfinitialdata/import/coredata/common/essential-data.impex");
		importImpexFile(context, "/bcfinitialdata/import/coredata/common/vacation-reports.impex");
		importImpexFile(context, "/bcfinitialdata/import/coredata/common/yforms.impex");
		if (getConfigurationService().getConfiguration().getBoolean("run.delta.impex", Boolean.TRUE))
		{
			importDeltaImpexes(context);
		}
	}

	/**
	 * This method will get the paths of the impexes (comma separated values) from local.property variable and import
	 * the respective impexes.
	 *
	 * @param context
	 */
	private void importDeltaImpexes(final SystemSetupContext context)
	{
		if (StringUtils.isNotEmpty(
				configurationService.getConfiguration().getString("delta.impexes", StringUtils.EMPTY))
				&& StringUtils.isNotEmpty(configurationService.getConfiguration()
				.getString("releaseVersion", StringUtils.EMPTY)))
		{
			final String[] deltaImpexNamesArray = StringUtils
					.split(configurationService.getConfiguration().getString("delta.impexes"), ',');
			final List<String> deltaImpexes = Arrays.asList(deltaImpexNamesArray);
			final StringBuilder path = new StringBuilder("/releases/")
					.append(configurationService.getConfiguration().getString("releaseVersion")).append("/");
			deltaImpexes.forEach(deltaImpex -> importImpexFile(context, path.toString() + deltaImpex.trim()));
		}
	}

	/*
	 * This method will be called during the system initialization.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<>();

		final List<String> sites = determineImportSites(context);
		final List<String> catalogNames = new ArrayList<>();
		final String[] catalogPrefixes =
				{ "master", "transactional", "commercial", "corporate", "vacations" };
		Collections.addAll(catalogNames, catalogPrefixes);

		final ImportData travelImportData = new ImportData();
		travelImportData.setProductCatalogName("bcf");
		travelImportData.setContentCatalogNames(catalogNames);
		/**
		 * this has to be same as the store you are importing using store.impex
		 */
		travelImportData.setStoreNames(Arrays.asList(FERRIES));
		importData.add(travelImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		importStoreData(context);

		importCommonDataset(context, BCF_INITIAL_DATA);

		importFerriesStoreData(context);
		if (sites.contains(VACATIONS))
		{
			importVacationsStoreData(context);
		}

		getSampleDataImportService().execute(this, context, importData);
		performActivityProductCronJob();
		performAccommodationOfferingCronJob();


		importCmsCockpitUsers(context, catalogNames);
		importTravelRulesData(context);
		performTransportOfferingCronJob();

		if (getBooleanSystemSetupParameter(context, ACTIVATE_SOLR_CRON_JOBS))
		{
			final List<String> indexNames = new ArrayList<>(sites);
			indexNames.add("bcferriesDeal");
			indexNames.forEach(indexName -> {
				logInfo(context, String.format("Activating solr index for [%s]", indexName));
				getSampleDataImportService().runSolrIndex(context.getExtensionName(), indexName);
			});
		}
		publicAllActivePromotions(context);
	}

	protected void publicAllActivePromotions(final SystemSetupContext context)
	{
		//Publish all active promotion rules
		final List<SourceRuleModel> promoRulesToPublish = ruleService.getAllActiveRulesForType(PromotionSourceRuleModel.class)
				.stream().map(SourceRuleModel.class::cast).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(promoRulesToPublish))
		{
			try
			{
				ruleMaintenanceService.compileAndPublishRules(promoRulesToPublish, "promotions-module", false);
			}
			catch (final RuleEngineServiceException ruleEngineServiceException)
			{
				logError(context, "Publishing active promotion rules failed", ruleEngineServiceException);
			}
			logInfo(context, "Publishing active promotion rules completed");
		}

	}

	private void performTransportOfferingCronJob()
	{
		final CronJobModel updateStockLevelsCronJob = cronjobService.getCronJob("updateStockLevelsToTransportOfferingCronJob");

		//Preform cronjob, update stockLevels to transportOfferings synchronously
		cronjobService.performCronJob(updateStockLevelsCronJob, true);

		final CronJobModel travelGeocodeAddressesCronJob = cronjobService.getCronJob("travelGeocodeAddressesCronJob");
		/*
		 * Preform cronjob, set latitude/longitude of all POS so that "duration(dynamic attribute of transport offering)"
		 * value can be set during indexing
		 */
		cronjobService.performCronJob(travelGeocodeAddressesCronJob, true);
	}

	private void performAccommodationOfferingCronJob()
	{
		final CronJobModel updateStockLevelsForAccommodationCronJob = cronjobService
				.getCronJob("updateStockLevelsToAccommodationOfferingCronJob");
		cronjobService.performCronJob(updateStockLevelsForAccommodationCronJob, true);

		final CronJobModel marketingRatePlanInfoCronjob = cronjobService.getCronJob("marketingRatePlanInfoCronjob");
		cronjobService.performCronJob(marketingRatePlanInfoCronjob, true);
	}

	private void performActivityProductCronJob()
	{
		final CronJobModel updateStockLevelsForActivityCronJob = cronjobService
				.getCronJob("updateStockLevelsToActivityProductCronJob");
		cronjobService.performCronJob(updateStockLevelsForActivityCronJob, true);
	}

	private void importCommonDataset(final SystemSetupContext context, final String baseImportDirectory)
	{
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/productcockpit/productcockpit-users.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/productcockpit/productcockpit-access-rights.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/productcockpit/productcockpit-constraints.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/cscockpit/cscockpit-users.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/cscockpit/cscockpit-access-rights.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/reportcockpit/reportcockpit-users.impex");
		importImpexFile(context, "/" + baseImportDirectory + "/import/cockpits/reportcockpit/reportcockpit-access-rights.impex");
	}

	private void importCmsCockpitUsers(final SystemSetupContext context, final List<String> sites)
	{
		sites.forEach(site -> importImpexFile(context,
				"/bcfinitialdata/import/sampledata/contentCatalogs/" + site + "ContentCatalog/cmscockpit-users.impex", false));
	}

	private void importFerriesStoreData(final SystemSetupContext context)
	{
		importImpexData(context, BCF_INITIAL_DATA, FERRIES, getFerriesStoreFileNames());
	}

	private void importVacationsStoreData(final SystemSetupContext context)
	{
		importImpexData(context, BCF_INITIAL_DATA, VACATIONS, getVacationsStoreFileNames());
	}

	private List<String> determineImportSites(final SystemSetupContext context)
	{
		final String parameterValue = context.getParameter(context.getExtensionName() + "_" + SITE_SELECTION_PARAM);
		if (StringUtils.isNotBlank(parameterValue))
		{
			return Collections.singletonList(parameterValue);
		}
		else
		{
			final String configuredSite = getConfigurationService().getConfiguration().getString("travelacc.sites");
			if (StringUtils.isEmpty(configuredSite) || Arrays.asList(VACATIONS).contains(configuredSite))
			{
				//fallback to default configuration if travelacc.sites is empty or incorrect
				logInfo(context,
						"Missing setup parameter for key [" + SITE_SELECTION_PARAM + "]. Using default value: [" + VACATIONS + "]");
				return Arrays.asList(FERRIES, VACATIONS);
			}

			return Collections.singletonList(configuredSite);
		}
	}

	/**
	 * This method will be called during the system initialization. Imports travel data common to both minimal and
	 * complete set.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	private void importStoreData(final SystemSetupContext context)
	{
		importImpexFile(context, "/bcfinitialdata/import/sampledata/productCatalogs/bcfProductCatalog/catalog-sync.impex", false);
		importImpexFile(context, "/bcfinitialdata/import/coredata/common/searchrestriction.impex", false);
		importImpexFile(context, "/bcfinitialdata/import/coredata/common/user-groups.impex", false);
	}

	/**
	 * Imports travel rules common to both minimal and complete set.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	private void importTravelRulesData(final SystemSetupContext context)
	{
		importImpexFile(context, "/travelrulesengine/import/TravelRulesSetup.impex", false);
		importImpexFile(context, "/travelrulesengine/import/essentialdata-definitions.impex", false);
		importImpexFile(context, "/travelrulesengine/import/travelrules.impex", false);
		importImpexFile(context, "/travelrulesengine/import/travelrules_en.impex", false);
	}

	private void importImpexData(final SystemSetupContext context, final String baseImportDirectory, final String storeName,
			final List<String> fileNames)
	{
		logInfo(context, "Begin importing dataset for [" + storeName + "]");
		final String importRoot = "/" + baseImportDirectory + "/import/coredata/datasets/";

		fileNames.forEach(file -> importImpexFile(context, importRoot + storeName + "/" + file, false));
	}

	private List<String> getFerriesStoreFileNames()
	{
		final List<String> fileNames = new ArrayList<>(30);
		fileNames.add("taxdata.impex");
		fileNames.add("categories.impex");
		fileNames.add("fareproductsdata.impex");
		fileNames.add("travelrestrictions.impex");
		fileNames.add("amendbookingproductsdata.impex");
		fileNames.add("bundlesdata.impex");
		fileNames.add("bundletemplatetransportofferingmapping.impex");
		fileNames.add("bundletemplates-selectioncriteria.impex");
		fileNames.add("accommodationmap.impex");
		fileNames.add("specialservicerequest.impex");
		fileNames.add("bcfconfigurableproperties.impex");
		fileNames.add("baseproperties.impex");
		fileNames.add("serviceNoticeWorkflow.impex");
		fileNames.add("serviceNoticeWorkflowUserRights.impex");
		fileNames.add("travelAdvisoryWorkflow.impex");
		fileNames.add("travelAdvisoryWorkflowUserRights.impex");
		fileNames.add("locations.impex");
		fileNames.add("subscriptionMasterDetails.impex");
		return fileNames;
	}

	private List<String> getVacationsStoreFileNames()
	{
		final List<String> fileNames = new ArrayList<>(40);
		fileNames.add("propertyfacilitytypes.impex");
		fileNames.add("propertyfacilities.impex");
		fileNames.add("accommodationfacilitytypes.impex");
		fileNames.add("accommodationfacilities.impex");
		fileNames.add("taxdata.impex");
		fileNames.add("guestoccupancies.impex");
		fileNames.add("guestOccupancyCountRoomTypeMapping.impex");
		fileNames.add("cancelpenalties.impex");
		fileNames.add("mealtypes.impex");
		fileNames.add("transportfacility.impex");
		fileNames.add("locations.impex");
		fileNames.add("rateplaninclusions.impex");
		fileNames.add("roomrateproduct.impex");
		fileNames.add("accommodationrestrictions.impex");
		fileNames.add("serviceproducts.impex");
		fileNames.add("roompreference.impex");
		fileNames.add("baseproperties.impex");
		return fileNames;
	}


	/**
	 * @return the coreDataImportService
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * @param coreDataImportService the coreDataImportService to set
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	/**
	 * @return the sampleDataImportService
	 */
	protected SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	/**
	 * @param sampleDataImportService the sampleDataImportService to set
	 */
	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
