/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;


public class CustomCoreDataImportService extends CoreDataImportService
{
	private static final String STORES_PATH = "/%s/import/coredata/stores/%s/";
	private static final String CONTENT_CATALOG_PATH = "/%s/import/coredata/contentCatalogs/%sContentCatalog/";

	@Override
	protected void importSolrIndex(final String extensionName, final String storeName)
	{
		importContent(extensionName, storeName, STORES_PATH + "solr.impex");
		importContent(extensionName, storeName, STORES_PATH + "solr_packages.impex");
		importContent(extensionName, storeName, STORES_PATH + "solr_accommodation_offering.impex");
		importContent(extensionName, storeName, STORES_PATH + "solr_server.impex");
		getSetupSolrIndexerService().createSolrIndexerCronJobs(String.format("%sIndex", storeName));
		importContent(extensionName, storeName, STORES_PATH + "solrtrigger.impex");
	}

	@Override
	protected void importContentCatalog(final String extensionName, final String contentCatalogName)
	{
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "catalog.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "pageTemplates/page-templates.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "pageTemplates/page-templates-online.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "components/components.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlots/content-slots.impex");
		importContent(extensionName, contentCatalogName,
				CONTENT_CATALOG_PATH + "contentSlotsForTemplate/content-slots-for-template.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlotNames/content-slots-names.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentPages/content-pages.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlotsForPage/content-slots-for-page.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "email-content.impex");
	}

	private void importContent(final String extensionName, final String contentCatalogName, final String importFileName)
	{
		getSetupImpexService().importImpexFile(String.format(importFileName, extensionName, contentCatalogName), false);
	}
}
