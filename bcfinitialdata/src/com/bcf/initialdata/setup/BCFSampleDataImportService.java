/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.util.Config;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;


public class BCFSampleDataImportService extends SampleDataImportService
{
	private static final Logger LOG = Logger.getLogger(BCFSampleDataImportService.class);
	private static final String CONTENT_CATALOG_PATH = "/%s/import/sampledata/contentCatalogs/%sContentCatalog/";
	private static final String IMPORT_CONTENT_CATALOGS_SAMPLE_DATA = "importContentCatalogsSampleData";

	@Override
	public void execute(final AbstractSystemSetup systemSetup, final SystemSetupContext context, final List<ImportData> importData)
	{
		super.execute(systemSetup, context, importData);
		final boolean importContentCatalogsSampleData = systemSetup
				.getBooleanSystemSetupParameter(context, IMPORT_CONTENT_CATALOGS_SAMPLE_DATA);

		if (importContentCatalogsSampleData)
		{
			importCommonData(context.getExtensionName());
			importData.forEach(data -> {
				data.getContentCatalogNames().forEach(contentCatalogName -> {
					importContentCatalog(context.getExtensionName(), contentCatalogName);
					synchronizeContentCatalog(systemSetup, context, contentCatalogName, true);
				});
			});
		}
	}

	@Override
	protected void importStore(final String extensionName, final String storeName, final String productCatalogName)
	{
		super.importStore(extensionName, storeName, productCatalogName);

		// ferries data import
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/locations.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/transportfacility.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/shipfacilities.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/shipinfo.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/shipinfofacilitiesmapping.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/transportvehicle.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/travelsector.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/travelroute.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/ancillaryproducts.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcferries/pricesdata.impex", extensionName), false);




		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/transportvehicleroutesmapping.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcferries/transportvehicleproductmapping.impex", extensionName), false);
		importImpexForTransportOffering(
				String.format("/%s/import/sampledata/stores/bcferries/transportoffering.impex", extensionName));


		// Vacation Data import
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/users.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodationproviders.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/goodwillrefunds.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/locations.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/locations-media.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/points-of-service.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/extraguestoccupancy.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodationofferings.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodationofferings-media.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodations.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodations-media.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/rateplans.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/rateplaninclusions.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/pricerows.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/rateplanaccommodationmapping.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/accommodationofferingcustomerreview.impex", extensionName),
				false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/productstaxes.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format(
				"/%s/import/sampledata/stores/bcvacation/accommodationofferingroomsextrasmapping.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/bundlesdata.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/bcvacation/bundletemplates-selectioncriteria.impex", extensionName),
				false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/regions.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/stores/bcvacation/vacationpolicy.impex", extensionName), false);



	}

	@Override
	protected void importCommonData(final String extensionName)
	{
		super.importCommonData(extensionName);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/common/sample-data.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/common/promotions.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/common/activitiesproduct.impex", extensionName), false);
	}

	/**
	 * Method iterates through a range of dates and imports schedule data for each day.
	 *
	 * @param impexFilePath the complete filename with path
	 */
	private void importImpexForTransportOffering(final String impexFilePath)
	{
		int numberOfDaysToAdd = 30;
		try
		{
			numberOfDaysToAdd = Integer.parseInt(Config.getParameter("transportoffering.schedule.days.to.add"));
		}
		catch (final NumberFormatException exec)
		{
			LOG.warn("Error reading the property 'transportoffering.schedule.days.to.add' Using default value as 30 days");
		}
		final LocalDate startDate = LocalDate.now();
		final LocalDate endDate = startDate.plusDays(numberOfDaysToAdd);
		final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		Stream.iterate(startDate, date -> date.plusDays(1)).limit(ChronoUnit.DAYS.between(startDate, endDate) + 1).forEach(date -> {
			updateTransportOfferingsData(dateFormat.format(date), impexFilePath);
			getSetupImpexService().importImpexFile(impexFilePath, false);
		});
		updateTransportOfferingsData("yyyy/MM/dd", impexFilePath);
	}

	/**
	 * Updates the impex file with the current date. The date is latter used by the bean shell to set the schedule data.
	 *
	 * @param currentDate  Current date - see documentation on java.time.LocalDate
	 * @param fileToModify
	 */
	public void updateTransportOfferingsData(final String currentDate, final String fileToModify)
	{
		final URL url = InitialDataSystemSetup.class.getResource(fileToModify);
		final File file = new File(url.getPath());
		try
		{
			String fileContext = FileUtils.readFileToString(file);
			final String pattern = "((.*)(travelDate=)(\")(.*?)(\"))";
			final Pattern patternRegx = Pattern.compile(pattern);
			final Matcher matcher = patternRegx.matcher(fileContext);
			if (matcher.find())
			{
				// Building the new travelDate macro for current date
				final String currentTravelDate = "$travelDate=\"" + currentDate + "\"";
				fileContext = fileContext.replace(matcher.group(), currentTravelDate);
			}

			FileUtils.writeStringToFile(file, fileContext);
		}
		catch (final IOException e)
		{
			LOG.warn("IOException when loading data for TransportOffering", e);
		}
	}

	@Override
	protected void importContentCatalog(final String extensionName, final String contentCatalogName)
	{
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "components/components.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlots/content-slots.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlotNames/content-slots-names.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentPages/content-pages.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "contentSlotsForPage/content-slots-for-page.impex");
		importContent(extensionName, contentCatalogName, CONTENT_CATALOG_PATH + "email-content.impex");
	}

	private void importContent(final String extensionName, final String contentCatalogName, final String importFileName)
	{
		getSetupImpexService().importImpexFile(String.format(importFileName, extensionName, contentCatalogName), false);
	}
}
