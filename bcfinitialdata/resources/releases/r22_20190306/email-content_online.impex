#
# Copyright (c) 2019 British Columbia Ferry Services Inc
# All rights reserved.
#
# This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
# You shall not disclose such Confidential Information and shall use it only in accordance with the
# terms of the license agreement you entered into with British Columbia Ferry Services Inc.
#
# Last Modified: 03/04/19 14:54
#

#
# Import the CMS content for the BCferries site emails
#
$contentCatalog = transactionalContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Online])[default = $contentCatalog:Online]

$bcferriesEmailResource = jar:com.bcf.initialdata.setup.InitialDataSystemSetup&/bcfinitialdata/import/sampledata/contentCatalogs/$contentCatalog/emails

# Import config properties into impex macros for modulegen
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$emailResource = $config-emailResourceValue

# Language
$lang = en

# CMS components and Email velocity templates
INSERT_UPDATE RendererTemplate; code[unique = true]                   ; description[lang = $lang]            ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Customer_Booking_Confirmation_Body    ; "Booking Confirmation Email Body"    ; $bcferriesEmailResource/email-bookingConfirmationBody.vm
                              ; Customer_Booking_Confirmation_Subject ; "Booking Confirmation Email Subject" ; $bcferriesEmailResource/email-bookingConfirmationSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]             ; name                              ; active; frontendTemplateName   ; subject(code)                         ; htmlTemplate(code)                 ; restrictedPageTypes(code)
                               ;                          ; OrderConfirmationEmailTemplate ; Order Confirmation Email Template ; true  ; orderConfirmationEmail ; Customer_Booking_Confirmation_Subject ; Customer_Booking_Confirmation_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]     ; name                     ; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; OrderConfirmationEmail ; Order Confirmation Email ; OrderConfirmationEmailTemplate ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"

####################customer registration###############################

INSERT_UPDATE RendererTemplate; code[unique = true]          ; description[lang = $lang]                     ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; CustomerRegistration_Body    ; "Booking Customer Registration Email Body"    ; $bcferriesEmailResource/email-customerRegistrationNotificationBody.vm
                              ; CustomerRegistration_Subject ; "Booking Customer Registration Email Subject" ; $bcferriesEmailResource/email-customerRegistrationNotificationSubject.vm


INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                ; name                                 ; active; frontendTemplateName      ; subject(code)                ; htmlTemplate(code)        ; restrictedPageTypes(code)
                               ;                          ; CustomerRegistrationEmailTemplate ; Customer Registration Email Template ; true  ; customerRegistrationEmail ; CustomerRegistration_Subject ; CustomerRegistration_Body ; EmailPage


INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]        ; name                        ; masterTemplate(uid, $contentCV)   ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = de]        ; fromName[lang = de]
                       ;                          ; CustomerRegistrationEmail ; Customer Registration Email ; CustomerRegistrationEmailTemplate ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team

####################Forgot Password###############################

INSERT_UPDATE RendererTemplate; code[unique = true]    ; description[lang = $lang]      ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; ForgotPassword_Body    ; "Forgot Password Email Body"   ; $bcferriesEmailResource/email-forgotPasswordNotificationBody.vm
                              ; ForgotPassword_Subject ; "ForgotPassword Email Subject" ; $bcferriesEmailResource/email-forgotPasswordNotificationSubject.vm

INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]             ; name                              ; active; frontendTemplateName   ; subject(code)          ; htmlTemplate(code)  ; restrictedPageTypes(code)
                               ;                          ; ForgottenPasswordEmailTemplate ; Forgotten Password Email Template ; true  ; forgottenPasswordEmail ; ForgotPassword_Subject ; ForgotPassword_Body ; EmailPage

INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]     ; name                  ; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = de]        ; fromName[lang = de]
                       ;                          ; forgottenPasswordEmail ; Forgot Password Email ; ForgottenPasswordEmailTemplate ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team

####################share itinerary###############################
# CMS components and Email velocity templates
INSERT_UPDATE RendererTemplate; code[unique = true]     ; description[lang = $lang]            ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Share_Itinerary_Body    ; "Booking Confirmation Email Body"    ; $bcferriesEmailResource/email-shareItineraryBody.vm
                              ; Share_Itinerary_Subject ; "Booking Confirmation Email Subject" ; $bcferriesEmailResource/email-shareItinerarySubject.vm

# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]          ; name                         ; active; frontendTemplateName; subject(code)           ; htmlTemplate(code)   ; restrictedPageTypes(code)
                               ;                          ; ShareItineraryEmailTemplate ; Share ItineraryEmailTemplate ; true  ; ShareItineraryEmail ; Share_Itinerary_Subject ; Share_Itinerary_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]  ; name                  ; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; ShareItineraryEmail ; Share Itenerary Email ; ShareItineraryEmailTemplate    ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"

####################Cancel Order###############################
INSERT_UPDATE RendererTemplate; code[unique = true]                  ; description[lang = $lang]           ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Customer_Booking_Cancelation_Body    ; "Booking Cancelation Email Body"    ; $bcferriesEmailResource/email-bookingCancelationBody.vm
                              ; Customer_Booking_Cancelation_Subject ; "Booking Cancelation Email Subject" ; $bcferriesEmailResource/email-bookingCancelationSubject.vm

# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]            ; name                             ; active; frontendTemplateName  ; subject(code)                        ; htmlTemplate(code)                ; restrictedPageTypes(code)
                               ;                          ; OrderCancelationEmailTemplate ; Order Cancelation Email Template ; true  ; orderCancelationEmail ; Customer_Booking_Cancelation_Subject ; Customer_Booking_Cancelation_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]    ; name                    ; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; orderCancelationEmail ; Order Cancelation Email ; OrderCancelationEmailTemplate  ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"

####################################VaccationConfirmation##############################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Vaccation_Confirmation_Body    ; "Vaccation Confirmation Email Body"    ; $bcferriesEmailResource/email-VacationbookingConfirmationBody.vm
                              ; Vaccation_Confirmation_Subject ; "Vaccation Confirmation Email Subject" ; $bcferriesEmailResource/email-VacationConfirmationSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                 ; name                                  ; active; frontendTemplateName       ; subject(code)                  ; htmlTemplate(code)          ; restrictedPageTypes(code)
                               ;                          ; VaccationConfirmationEmailTemplate ; Vaccation Confirmation Email Template ; true  ; VaccationConfirmationEmail ; Vaccation_Confirmation_Subject ; Vaccation_Confirmation_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]         ; name                         ; masterTemplate(uid, $contentCV)    ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; VaccationConfirmationEmail ; Vaccation Confirmation Email ; VaccationConfirmationEmailTemplate ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"


####################################Vacation Confirmation Hotel Vendor##############################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Vacation_Confirmation_Hotel_Vendor_Body    ; "Vaccation Confirmation Hotel Vendor Email Body"    ; $bcferriesEmailResource/email-VacationConfirmationHotelVendorBody.vm
                              ; Vacation_Confirmation_Hotel_Vendor_Subject ; "Vaccation Confirmation Hotel Vendor Email Subject" ; $bcferriesEmailResource/email-VacationConfirmationHotelVendorSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; VacationConfirmationHotelVendorEmailTemplate ; Vaccation Confirmation Hotel Vendor Email Template ; true   ; VaccationConfirmationHotelVendorEmail   ; Vacation_Confirmation_Hotel_Vendor_Subject    ; Vacation_Confirmation_Hotel_Vendor_Body   ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
                       ;                   ; VacationConfirmationHotelVendorEmail ; Vaccation Confirmation Hotel Vendor Email ; VacationConfirmationHotelVendorEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"


####################################Activity Confirmation Hotel Vendor##############################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Vacation_Confirmation_Activity_Vendor_Body    ; "Vaccation Confirmation Activity Vendor Email Body"    ; $bcferriesEmailResource/email-VacationConfirmationActivityVendorBody.vm
                              ; Vacation_Confirmation_Activity_Vendor_Subject ; "Vaccation Confirmation Activity Vendor Email Subject" ; $bcferriesEmailResource/email-VacationConfirmationActivityVendorSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; VacationConfirmationActivityVendorEmailTemplate ; Vaccation Confirmation Activity Vendor Email Template ; true   ; VaccationConfirmationActivityVendorEmail   ; Vacation_Confirmation_Activity_Vendor_Subject    ; Vacation_Confirmation_Activity_Vendor_Body   ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
                       ;                   ; VacationConfirmationActivityVendorEmail ; Vaccation Confirmation Activity Vendor Email ; VacationConfirmationActivityVendorEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"


#####################################Service Notice##################################################

INSERT_UPDATE RendererTemplate; code[unique = true]                ; description[lang = $lang]           ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; email_Service_Notice_Body          ; "Booking Service Notice Email Body" ; $bcferriesEmailResource/email-serviceNoticeNotificationBody.vm
                              ; email_Service_Notice_Subject       ; "Booking Service Notice Subject"    ; $bcferriesEmailResource/email-serviceNoticeNotificationSubject.vm
                              ; email_News_Release_Body            ; "NewsRelease Email Body"            ; $bcferriesEmailResource/email-newsReleaseNotificationBody.vm
                              ; email_News_Release_Subject         ; "NewsRelease Subject"               ; $bcferriesEmailResource/email-newsReleaseNotificationSubject.vm
                              ; email_Business_Opportunity_Body    ; "BusinessOpportunity Email Body"    ; $bcferriesEmailResource/email-businessOpportunityNotificationBody.vm
                              ; email_Business_Opportunity_Subject ; "BusinessOpportunity Subject"       ; $bcferriesEmailResource/email-businessOpportunityNotificationSubject.vm


INSERT_UPDATE ServiceNoticeEmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                ; name                                  ; active; frontendTemplateName; subject(code)                ; htmlTemplate(code)        ; restrictedPageTypes(code)
                                            ;                          ; defaultServiceNoticeEmailTemplate ; Default Service Notice Email Template ; true  ; serviceNoticeEmail  ; email_Service_Notice_Subject ; email_Service_Notice_Body ; EmailPage

INSERT_UPDATE NewsReleaseEmailPageTemplate; $contentCV[unique = true]; uid[unique = true]              ; name                               ; active; frontendTemplateName; subject(code)                ; htmlTemplate(code)        ; restrictedPageTypes(code)
                                          ;                          ; defaultNewsReleaseEmailTemplate ; Default NewsRelease Email Template ; true  ; serviceNoticeEmail  ; email_Service_Notice_Subject ; email_Service_Notice_Body ; EmailPage

INSERT_UPDATE BusinessOpportunityEmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                      ; name                                       ; active; frontendTemplateName; subject(code)                ; htmlTemplate(code)        ; restrictedPageTypes(code)
                                                  ;                          ; defaultBusinessOpportunityEmailTemplate ; Default BusinessOpportunity Email Template ; true  ; serviceNoticeEmail  ; email_Service_Notice_Subject ; email_Service_Notice_Body ; EmailPage

INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]       ; name                      ; masterTemplate(uid, $contentCV)         ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]     ; fromName[lang = $lang]
                       ;                          ; serviceNoticeEmail       ; Service Notice Email      ; defaultServiceNoticeEmailTemplate       ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team
                       ;                          ; newsReleaseEmail         ; NewsRelease Email         ; defaultNewsReleaseEmailTemplate         ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team
                       ;                          ; businessOpportunityEmail ; BusinessOpportunity Email ; defaultBusinessOpportunityEmailTemplate ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team

################################################Customer Email Change##########################################

INSERT_UPDATE RendererTemplate; code[unique = true]         ; description[lang = $lang]       ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; CustomerEmailChange_Body    ; "Customer Email Change Body"    ; $bcferriesEmailResource/email-customerEmailChangeNotificationBody.vm
                              ; CustomerEmailChange_Subject ; "Customer Email Change Subject" ; $bcferriesEmailResource/email-customerEmailChangeNotificationSubject.vm

INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]          ; name                           ; active; frontendTemplateName; subject(code)               ; htmlTemplate(code)       ; restrictedPageTypes(code)
                               ;                          ; CustomerEmailChangeTemplate ; Customer Email Change Template ; true  ; customerEmailChange ; CustomerEmailChange_Subject ; CustomerEmailChange_Body ; EmailPage

INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]  ; name                  ; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = de]        ; fromName[lang = de]
                       ;                          ; customerEmailChange ; Customer Email Change ; CustomerEmailChangeTemplate    ; true       ;                                           ; customerservices@hybris.com ; Customer Services Team


#####################################Vacation Cancellation##################################################

INSERT_UPDATE RendererTemplate; code[unique = true]           ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; vacation_Cancellation_Body    ; "Vaccation Cancellation Email Body"    ; $bcferriesEmailResource/email-vacationBookingCancellationBody.vm
                              ; vacation_Cancellation_Subject ; "Vaccation Cancellation Email Subject" ; $bcferriesEmailResource/email-vacationBookingCancellationSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                ; name                                 ; active; frontendTemplateName      ; subject(code)                 ; htmlTemplate(code)         ; restrictedPageTypes(code)
                               ;                          ; vacationCancellationEmailTemplate ; Vacation Cancellation Email Template ; true  ; VacationCancellationEmail ; vacation_Cancellation_Subject ; vacation_Cancellation_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]        ; name                        ; masterTemplate(uid, $contentCV)   ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; VacationCancellationEmail ; Vacation Cancellation Email ; vacationCancellationEmailTemplate ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"

#####################################Vacation Cancellation Hotel Vendor##################################################

INSERT_UPDATE RendererTemplate; code[unique = true]                        ; description[lang = $lang]                           ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; vacation_Cancellation_Hotel_Vendor_Body    ; "Vaccation Cancellation Hotel Vendor Email Body"    ; $bcferriesEmailResource/email-vacationCancellationHotelVendorBody.vm
                              ; vacation_Cancellation_Hotel_Vendor_Subject ; "Vaccation Cancellation Hotel Vendor Email Subject" ; $bcferriesEmailResource/email-vacationCancellationHotelVendorSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                 ; name                                  ; active; frontendTemplateName       ; subject(code)                  ; htmlTemplate(code)          ; restrictedPageTypes(code)
                               ;                          ; vacationCancellationHotelVendorEmailTemplate ; Vacation Cancellation Hotel Vendor Email Template ; true  ; VacationCancellationHotelVendorEmail ; vacation_Cancellation_Hotel_Vendor_Subject ; vacation_Cancellation_Hotel_Vendor_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]         ; name                         ; masterTemplate(uid, $contentCV)    ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; VacationCancellationHotelVendorEmail ; Vacation Cancellation Hotel Vendor Email ; vacationCancellationHotelVendorEmailTemplate ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"

#############################################Customer Profile Update Email########################################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Customer_Profile_Update_Body    ; "Customer Profile Update Email Body"    ; $bcferriesEmailResource/email-CustomerProfileUpdateBody.vm
                              ; Customer_Profile_Update_Subject ; "Customer Profile Update Email Subject" ; $bcferriesEmailResource/email-CustomerProfileUpdateSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; CustomerProfileUpdateEmailTemplate ; Customer Profile Update Email Template                                     ; true   ; CustomerProfileUpdateEmail              ; Customer_Profile_Update_Subject                  ; Customer_Profile_Update_Body                    ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
                       ;                   ; CustomerProfileUpdateEmail ; Customer Profile Update Email ; CustomerProfileUpdateEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"




#####################################Vacation Cancellation Activity Vendor##################################################

INSERT_UPDATE RendererTemplate; code[unique = true]                        ; description[lang = $lang]                           ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']
                              ; vacation_Cancellation_Activity_Vendor_Body    ; "Vaccation Cancellation Activity Vendor Email Body"    ; $bcferriesEmailResource/email-vacationCancellationActivityVendorBody.vm
                              ; vacation_Cancellation_Activity_Vendor_Subject ; "Vaccation Cancellation Activity Vendor Email Subject" ; $bcferriesEmailResource/email-vacationCancellationActivityVendorSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                 ; name                                  ; active; frontendTemplateName       ; subject(code)                  ; htmlTemplate(code)          ; restrictedPageTypes(code)
                               ;                          ; vacationCancellationActivityVendorEmailTemplate ; Vacation Cancellation Activity Vendor Email Template ; true  ; VacationCancellationActivityVendorEmail ; vacation_Cancellation_Activity_Vendor_Subject ; vacation_Cancellation_Activity_Vendor_Body ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]         ; name                         ; masterTemplate(uid, $contentCV)    ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]        ; fromName[lang = $lang]
                       ;                          ; VacationCancellationActivityVendorEmail ; Vacation Cancellation Activity Vendor Email ; vacationCancellationActivityVendorEmailTemplate ; true       ;                                           ; "codereview@ubiquedigital.com" ; "BC Ferries Customer Services Team"


#############################################Customer Subscription Update Email########################################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Customer_Subscription_Update_Body    ; "Customer Subscription Update Email Body"    ; $bcferriesEmailResource/email-CustomerSubscriptionUpdateBody.vm
                              ; Customer_Subscription_Update_Subject ; "Customer Subscription Update Email Subject" ; $bcferriesEmailResource/email-CustomerSubscriptionUpdateSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; CustomerSubscriptionUpdateEmailTemplate ; Customer Subscription Update Email Template                                     ; true   ; CustomerSubscriptionUpdateEmail              ; Customer_Subscription_Update_Subject                  ; Customer_Subscription_Update_Body                    ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
                       ;                   ; CustomerSubscriptionUpdateEmail ; Customer Subscription Update Email ; CustomerSubscriptionUpdateEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"


#############################################Save Quote Email########################################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; Save_Quote_Body    ; "Save Quote Email Body"    ; $bcferriesEmailResource/email-SaveQuoteBody.vm
                              ; Save_Quote_Subject ; "Save Quote Email Subject" ; $bcferriesEmailResource/email-SaveQuoteSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; SaveQuoteEmailTemplate ; Save Quote Email Template                                     ; true   ; SaveQuoteEmail              ; Save_Quote_Subject                 ; Save_Quote_Body                     ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
                       ;                   ; SaveQuoteEmail ; Save Quote Email ; SaveQuoteEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"


#############################################YForms Notification Email########################################

INSERT_UPDATE RendererTemplate; code[unique = true]            ; description[lang = $lang]              ; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]; rendererType(code)[default = 'velocity']

                              ; YForms_Email_Body    ; "YForm Email Body"    ; $bcferriesEmailResource/email-yFormsBody.vm
                              ; YForms_Email_Subject ; "YForm Email Subject" ; $bcferriesEmailResource/email-yFormsSubject.vm


# Email page Template
INSERT_UPDATE EmailPageTemplate; $contentCV[unique = true]; uid[unique = true]                                  ; name                                               ; active ; frontendTemplateName                    ; subject(code)                                     ; htmlTemplate(code)                            ; restrictedPageTypes(code)
                               ;                   ; YFormsEmailTemplate ; YForms Email Template                                     ; true   ; YFormsEmail              ; YForms_Email_Subject                 ; YForms_Email_Body                     ; EmailPage

# Email Pages
INSERT_UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]                          ; name                                      ; masterTemplate(uid, $contentCV)                               ; defaultPage; approvalStatus(code)[default = 'approved']; fromEmail[lang = $lang]                  ; fromName[lang = $lang]
;                   ; YFormsEmail ; YForm  Email ; YFormsEmailTemplate   ; true       ;                             ; "codereview@ubiquedigital.com"    ; "BC Ferries Customer Services Team"
