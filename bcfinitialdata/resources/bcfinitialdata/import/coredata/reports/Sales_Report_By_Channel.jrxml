<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2019 British Columbia Ferry Services Inc
  ~ All rights reserved.
  ~
  ~ This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
  ~ You shall not disclose such Confidential Information and shall use it only in accordance with the
  ~ terms of the license agreement you entered into with British Columbia Ferry Services Inc.
  ~
  ~ Last Modified: 02/07/19 14:01
  -->

<!-- Created with Jaspersoft Studio version 6.6.0.final using JasperReports Library version 6.6.0  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			  xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd"
			  name="Sales_Report_By_Channel" language="groovy" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="20"
			  rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="DataAdapter.xml"/>
	<parameter name="Channels" class="java.lang.String">
		<property name="com.jaspersoft.studio.js.ic.type" value="MULTI_LOV" />
	</parameter>
	<parameter name="FROM" class="java.util.Date" />
	<parameter name="TO" class="java.util.Date" />
	<queryString language="SQL">
		<![CDATA[SELECT NVL(enum.code,' ') as Channel, count( * ) as "Total bookings",ROUND(SUM(p_totalprice),2) AS "Gross Sales", ROUND(AVG(p_totalprice),2) as "Average Revenue"
		FROM orders o JOIN enumerationvalues enum ON enum.PK=o.p_salesApplication where (o.createdTS>=to_date($P{FROM}) AND o.createdTs<to_date($P{TO})) AND (enum.code=COALESCE($P{Channels},enum.code))
		 GROUP BY enum.code]]>
	</queryString>
	<field name="Channel" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="Channel"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="enumerationvalues"/>
	</field>
	<field name="Total bookings" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="Total bookings"/>
	</field>
	<field name="Gross Sales" class="java.math.BigDecimal">
		<property name="com.jaspersoft.studio.field.label" value="Gross Sales"/>
	</field>
	<field name="Average Revenue" class="java.math.BigDecimal">
		<property name="com.jaspersoft.studio.field.label" value="Average Revenue"/>
	</field>
	<group name="Group1">
		<groupExpression><![CDATA[$F{Channel}]]></groupExpression>
		<groupHeader>
			<band height="33">
				<staticText>
					<reportElement mode="Opaque" x="0" y="0" width="100" height="32" forecolor="#666666" backcolor="#E6E6E6" />
					<textElement>
						<font size="18" isBold="true"/>
					</textElement>
					<text><![CDATA[Channel]]></text>
				</staticText>
				<textField>
					<reportElement mode="Opaque" x="100" y="0" width="455" height="32" forecolor="#006699" backcolor="#E6E6E6" />
					<textElement>
						<font size="18" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Channel}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="-20" y="32" width="595" height="1" forecolor="#666666" />
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="72">
			<frame>
				<reportElement mode="Opaque" x="-20" y="-20" width="615" height="92" backcolor="#006699" />
				<staticText>
					<reportElement x="15" y="20" width="255" height="43" forecolor="#FFFFFF" />
					<textElement>
						<font size="24" isBold="true"/>
					</textElement>
					<text><![CDATA[Sale By Channel]]></text>
				</staticText>
				<staticText>
					<reportElement x="395" y="43" width="180" height="20" forecolor="#FFFFFF" />
					<textElement textAlignment="Right">
						<font size="14" isBold="false"/>
					</textElement>
					<text><![CDATA[Sales Report by channel]]></text>
				</staticText>
			</frame>
		</band>
	</title>
	<pageHeader>
		<band height="13"/>
	</pageHeader>
	<columnHeader>
		<band height="21">
			<line>
				<reportElement x="-20" y="20" width="595" height="1" forecolor="#666666" />
			</line>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="185" height="20" forecolor="#006699" backcolor="#E6E6E6">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="37baa742-1b1d-4ed1-b942-fcc4bda3a703"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Total bookings]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="185" y="0" width="185" height="20" forecolor="#006699" backcolor="#E6E6E6">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="b545f0f2-c116-4859-bdbe-8202adad85a9"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Gross Sales]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="370" y="0" width="185" height="20" forecolor="#006699" backcolor="#E6E6E6">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="2416c867-5ed3-4bb9-bd8e-ee36f95c18ab"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Average Revenue]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="19" width="555" height="1" />
			</line>
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="0" width="185" height="20">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="37baa742-1b1d-4ed1-b942-fcc4bda3a703"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total bookings}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="185" y="0" width="185" height="20">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="b545f0f2-c116-4859-bdbe-8202adad85a9"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Gross Sales}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="370" y="0" width="185" height="20">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="2416c867-5ed3-4bb9-bd8e-ee36f95c18ab"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Average Revenue}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band/>
	</columnFooter>
	<pageFooter>
		<band height="17">
			<textField>
				<reportElement mode="Opaque" x="0" y="4" width="515" height="13" backcolor="#E6E6E6" />
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement mode="Opaque" x="515" y="4" width="40" height="13" backcolor="#E6E6E6" />
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement x="0" y="4" width="100" height="13" />
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
