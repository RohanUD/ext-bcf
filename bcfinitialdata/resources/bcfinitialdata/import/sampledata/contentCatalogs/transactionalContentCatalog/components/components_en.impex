$contentCatalog = transactionalContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]
$siteResource = jar:com.bcf.initialdata.constants.BcfInitialDataConstants&/bcfinitialdata/import/sampledata/contentCatalogs/$contentCatalog
$paragraphs_en = $siteResource/paragraphs/en
$banner_messages_en = $siteResource/images/banners/messages/en
$picture = media(code, $contentCV)

# Language
$lang = en

# Site Logo Component
INSERT_UPDATE Media; code[unique = true]; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename; altText; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; logo.svg           ; $siteResource/images/theme/logo.svg                                                            ; logo.svg    ; "Home"


UPDATE Media; code[unique = true]; mime; $contentCV[unique = true]; folder(qualifier)[default = images];
            ; logo.svg           ; image/svg+xml

#Accordion Component PDF Icon
INSERT_UPDATE Media; code[unique = true]; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename ; altText; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; Pdf_icon.png       ; $siteResource/images/banners/Pdf_icon.png                                                      ; Pdf_icon.png ; "View"

UPDATE Media; code[unique = true]; mime; $contentCV[unique = true]; folder(qualifier)[default = images];
            ; Pdf_icon.png       ; image/png

UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; $picture
                            ;                          ; SiteLogoComponent ; logo.svg

# CMS Link Components
UPDATE CMSLinkComponent; $contentCV[unique = true]; uid[unique = true]         ; linkName[lang = $lang]
                       ;                          ; FAQLink                    ; "FAQ's"
                       ;                          ; PrivacyPolicyLink          ; "Privacy Policy"
                       ;                          ; TermsConditionsLink        ; "Terms & Conditions"
                       ;                          ; ContactUsLink              ; "Contact Us"
                       ;                          ; FerrystatusLink            ; "Ferry Status"
                       ;                          ; ManagebookingLink          ; "Manage Booking"
                       ;                          ; AlertsLink                 ; "Alerts"
                       ;                          ; GiftCertificatesLink       ; "Gift Certificates"
                       ;                          ; OurCompanyLink             ; "Our Company"
                       ;                          ; BusinesscustomersLink      ; "Business Customers"
                       ;                          ; CareersLink                ; "Careers"
                       ;                          ; HelpLink                   ; "Help"
                       ;                          ; SubscribeLink              ; "Subscribe"
                       ;                          ; ManageCardsLink            ; "Manage Cards"
                       ;                          ; AccountProfileLink         ; "My profile"
                       ;                          ; HomepageLink               ; "Plan & Book"
                       ;                          ; TravelInformationLink      ; "Before You Sail"
                       ;                          ; BusinessUsersLink          ; "On the Ferry"
                       ;                          ; TripFinderLink             ; "Explore Vacations"
                       ;                          ; DealsOffersLink            ; "Deals & Offers"
                       ;                          ; BusinessLoyaltyLink        ; "Using your loyalty points"
                       ;                          ; BusinessOffersLink         ; "Offers and News"
                       ;                          ; BusinessJoinLink           ; "Join the club"
                       ;                          ; BusinessContactLink        ; "Contact Us"
                       ;                          ; MyBookingsLink             ; "My bookings"
                       ;                          ; MyVacationsLink             ; "My vacations"
                       ;                          ; SwitchAccountLink          ; "Use different account"
                       ;                          ; SavedPaymentInfoLink       ; "My Saved Payments"
                       ;                          ; SavedPassengersLink        ; "My Saved Passengers"
                       ;                          ; PreferencesLink            ; "Preferences"
                       ;                          ; OrderApprovalDashboardLink ; "Order Approval Dashboard"
                       ;                          ; SavedAddressInfoLink       ; "My Saved Address"
                       ;                          ; SavedQuotesLink            ; "My quotes"


#########################
### Side Banner template
#########################

INSERT_UPDATE Media; code[unique = true]              ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename                     ; altText; mime[default = 'image/png']; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; PromoSide_boat.jpg               ; $siteResource/images/banners/PromoSide_boat.jpg                                                ; PromoSide_boat.jpg               ; "View" ;
                   ; PromoSide_rocks.jpg              ; $siteResource/images/banners/PromoSide_rocks.jpg                                               ; PromoSide_rocks.jpg              ; "View" ;
                   ; PromoSide_green.jpg              ; $siteResource/images/banners/PromoSide_green.jpg                                               ; PromoSide_green.jpg              ; "View" ;
                   ; DealPromo_01BeachDestination.jpg ; $siteResource/images/banners/DealPromo_01BeachDestination.jpg                                  ; DealPromo_01BeachDestination.jpg ; "View" ;


#########################
### NORMAL CONTENT PAGES
#########################

######
### Home Page
######

INSERT_UPDATE Media; code[unique = true]        ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename               ; altText; mime[default = 'image/jpeg']; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; HomeCarousel_burano.jpg    ; $siteResource/images/banners/HomeCarousel_burano.jpg                                           ; HomeCarousel_burano.jpg    ; "View" ;
                   ; HomeCarousel_maldives.jpg  ; $siteResource/images/banners/HomeCarousel_maldives.jpg                                         ; HomeCarousel_maldives.jpg  ; "View" ;
                   ; HomeCarousel_NYC.jpg       ; $siteResource/images/banners/HomeCarousel_NYC.jpg                                              ; HomeCarousel_NYC.jpg       ; "View" ;
                   ; HomePromo_01statue.jpg     ; $siteResource/images/banners/HomePromo_01statue.jpg                                            ; HomePromo_01statue.jpg     ; "View" ;
                   ; HomePromo_02oceanrocks.jpg ; $siteResource/images/banners/HomePromo_02oceanrocks.jpg                                        ; HomePromo_02oceanrocks.jpg ; "View" ;
                   ; HomePromo_03porthole.jpg   ; $siteResource/images/banners/HomePromo_03porthole.jpg                                          ; HomePromo_03porthole.jpg   ; "View" ;
                   ; HomePromo_04gondola.jpg    ; $siteResource/images/banners/HomePromo_04gondola.jpg                                           ; HomePromo_04gondola.jpg    ; "View" ;

######
### Deal Listing page
######

INSERT_UPDATE Media; code[unique = true]                    ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename                           ; altText; mime[default = 'image/jpeg']; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; DealListing_BeachDestinationBanner.jpg ; $siteResource/images/banners/DealListing_BeachDestinationBanner.jpg                            ; DealListing_BeachDestinationBanner.jpg ; "View" ;

UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]                   ; $picture[lang = $lang]
                            ;                          ; BeachDestinationDealsBannerComponent ; DealListing_BeachDestinationBanner.jpg

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]                       ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; BeachDestinationDealsHeaderCMSParagraph  ; $paragraphs_en/beachDestinationDealsHeaderCMSParagraph.html
                            ;                          ; BeachDestinationDealsContentCMSParagraph ; $paragraphs_en/beachDestinationDealsContentCMSParagraph.html
                            ;                          ; BeachDestinationDealsBottomCMSParagraph  ; $paragraphs_en/beachDestinationDealsBottomCMSParagraph.html

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]           ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; DealDetailsAboutCMSParagraph ; $paragraphs_en/dealDetailsAboutCMSParagraph.html

# Multiple banners will be used when styling and js for carousel is applied

UPDATE BannerComponent; $contentCV[unique = true]; uid[unique = true]           ; $picture                  ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                      ;                          ; HomeCarousel1BannerComponent ; HomeCarousel_burano.jpg   ; $banner_messages_en/homeCarouselBanner1PromoMessage.html
                      ;                          ; HomeCarousel2BannerComponent ; HomeCarousel_NYC.jpg      ; $banner_messages_en/homeCarouselBanner2PromoMessage.html
                      ;                          ; HomeCarousel3BannerComponent ; HomeCarousel_maldives.jpg ; $banner_messages_en/homeCarouselBanner3PromoMessage.html

UPDATE ResponsiveBackgroundBannerComponent; $contentCV[unique = true]; uid[unique = true]        ; $picture                         ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                                          ;                          ; HomePromoABannerComponent ; DealPromo_01BeachDestination.jpg ; $banner_messages_en/dealPromoBannerAMessage.html

######
### Privacy Policy Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]               ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; PrivacyPolicyHeaderCMSParagraph  ; $paragraphs_en/privacyPolicyHeaderCMSParagraph.html
                            ;                          ; PrivacyPolicyContentCMSParagraph ; $paragraphs_en/privacyPolicyContentCMSParagraph.html

######
### FAQ Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]  ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; FAQMenuCMSParagraph ; $paragraphs_en/faqMenuCMSParagraph.html
                            ;                          ; FAQTextCMSParagraph ; $paragraphs_en/faqTextCMSParagraph.html

######
### Terms & Conditions 
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]              ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; TermsConditionsMenuCMSParagraph ; $paragraphs_en/termsConditionsMenuCMSParagraph.html
                            ;                          ; TermsConditionsTextCMSParagraph ; $paragraphs_en/termsConditionsTextCMSParagraph.html
######
### Contact Us Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]           ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; ContactUsHeaderCMSParagraph  ; $paragraphs_en/contactUsHeaderCMSParagraph.html
                            ;                          ; ContactUsContentCMSParagraph ; $paragraphs_en/contactUsContentCMSParagraph.html

######
### Help Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]      ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; HelpHeaderCMSParagraph  ; $paragraphs_en/helpHeaderCMSParagraph.html
                            ;                          ; HelpContentCMSParagraph ; $paragraphs_en/helpContentCMSParagraph.html

######
### Travel Information Page
######

UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]        ; $picture
                            ;                          ; TravelInfoBannerComponent ; TripFinder_maldives.jpg

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]            ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; TravelInfoContentCMSParagraph ; $paragraphs_en/travelInfoContentCMSParagraph.html

######							
### Trip Finder Activities page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]                     ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; TripFinderActivitiesHeaderCMSParagraph ; $paragraphs_en/tripFinderActivitiesHeaderCMSParagraph.html


INSERT_UPDATE Media; code[unique = true]     ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename            ; altText; mime[default = 'image/jpeg']; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; Activity_beach.jpg      ; $siteResource/images/banners/Activity_beach.jpg                                                ; Activity_beach.jpg      ; "View" ;
                   ; Activity_city.jpg       ; $siteResource/images/banners/Activity_city.jpg                                                 ; Activity_city.jpg       ; "View" ;
                   ; Activity_culture.jpg    ; $siteResource/images/banners/Activity_culture.jpg                                              ; Activity_culture.jpg    ; "View" ;
                   ; Activity_family.jpg     ; $siteResource/images/banners/Activity_family.jpg                                               ; Activity_family.jpg     ; "View" ;
                   ; Activity_sport.jpg      ; $siteResource/images/banners/Activity_sport.jpg                                                ; Activity_sport.jpg      ; "View" ;
                   ; Activity_winter.jpg     ; $siteResource/images/banners/Activity_winter.jpg                                               ; Activity_winter.jpg     ; "View" ;
                   ; HomePageBannerMedia.jpg ; $siteResource/images/banners/home-banner.jpg                                                   ; HomePageBannerMedia.jpg ; "View" ;
                   ; HomeVacationMedia.jpg   ; $siteResource/images/banners/home-banner.jpg                                                   ; HomeVacationMedia.jpg   ; "View" ;
                   ; DealBannerMedia.png     ; $siteResource/images/banners/dealbanner.png                                                    ; DealBannerMedia.png     ; "View" ;


UPDATE ActivityBannerComponent; $contentCV[unique = true]; uid[unique = true]                   ; headline[lang = $lang]; $picture[lang = $lang]
                              ;                          ; BeachHolidayActivityBannerComponent  ; Beach Holiday         ; Activity_beach.jpg
                              ;                          ; CityBreakActivityBannerComponent     ; City Break            ; Activity_city.jpg
                              ;                          ; WinterSunActivityBannerComponent     ; Winter Sun            ; Activity_winter.jpg
                              ;                          ; CultureBreakActivityBannerComponent  ; Culture Break         ; Activity_culture.jpg
                              ;                          ; FamilyFriendsActivityBannerComponent ; Family Friends        ; Activity_family.jpg
                              ;                          ; SportsTripsActivityBannerComponent   ; Sports Trips          ; Activity_sport.jpg

#############################
### FUNCTIONAL CONTENT PAGES
#############################

######
### Fare Selection page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]             ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; FareSelectionAboutCMSParagraph ; $paragraphs_en/fareSelectionAboutCMSParagraph.html

######
### My Profile Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]                    ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; UpdateProfileConfirmationCMSParagraph ; $paragraphs_en/updateProfileConfirmationCMSParagraph.html

######
### Trip Finder page
######

INSERT_UPDATE Media; code[unique = true]     ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; realfilename            ; altText; mime[default = 'image/jpeg']; $contentCV[unique = true]; folder(qualifier)[default = images];
                   ; TripFinder_maldives.jpg ; $siteResource/images/banners/TripFinder_maldives.jpg                                           ; TripFinder_maldives.jpg ; "View" ;

UPDATE TripFinderBannerComponent; $contentCV[unique = true]; uid[unique = true]               ; $picture[lang = $lang]  ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                                ;                          ; TripFinderTopWideBannerComponent ; TripFinder_maldives.jpg ; $banner_messages_en/tripFinderWideBannerMessage.html

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]            ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; TripFinderCityCMSParagraphPAR ; $paragraphs_en/tripFinderCityCMSParagraphPARIS.html
                            ;                          ; TripFinderCityCMSParagraphATH ; $paragraphs_en/tripFinderCityCMSParagraphATH.html
                            ;                          ; TripFinderCityCMSParagraphVCE ; $paragraphs_en/tripFinderCityCMSParagraphVCE.html
                            ;                          ; TripFinderCityCMSParagraphNYC ; $paragraphs_en/tripFinderCityCMSParagraphNYC.html
                            ;                          ; TripFinderCityCMSParagraphNCE ; $paragraphs_en/tripFinderCityCMSParagraphNCE.html

######
### Check In Success Page
######

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]         ; content[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                            ;                          ; CheckInSuccessCMSParagraph ; $paragraphs_en/checkInSuccessCMSParagraph.html

# CMS Paragraph components 
UPDATE CMSAlertParagraphComponent; $contentCV[unique = true]; uid[unique = true]; title[lang = en]; content[lang = en]
                                 ;                          ; alertMessage      ; Alert           ; "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. sunt in culpa qui officia deserunt mollit anim id est laborum."


#########################
### FOOTER
#########################

UPDATE FooterComponent; $contentCV[unique = true]; uid[unique = true]      ; notice[lang = $lang]
                      ;                          ; FooterComponent         ; "Copyright © 2016"
                      ;                          ; FooterCheckoutComponent ; "Copyright © 2016"

#########################
### HEADER LINKS
#########################

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]      ; content[lang = $lang]
                            ;                          ; ContactInfoCMSParagraph ; "Contact Us: 02034995253"
###################
###	Accessibilty Declaration
###################
UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]                ; content[lang = $lang]
                            ;                          ; AccessibilityDeclarationComponent ; "Accommodation of accessibility requests has limitations on any given sailing. Requests are satisfied at the terminal on the day of sailing on a “first come, first served” basis after travelers identify themselves at the ticket booth. We recommend that you arrive 30 (Confirm) minutes earlier than the normal recommended arrival time for this sailing, if possible. There is a slight risk of rescheduling you to a later sailing if your needs cannot be adequately accommodated due to volume."

#########################
##BCFContactUsComponent##
UPDATE BCFContactUsComponent; $contentCV[unique = true]; uid[unique = true]     ; title[lang = $lang]; topText[lang = $lang]         ; description[lang = $lang]
                            ;                          ; BCFContactUsComponent3 ; Talk to us         ; 1-888-BC FERRY (223-3779)     ; 6:00 am - 10:00pm (Monday-Friday)
                            ;                          ; BCFContactUsComponent4 ; Email us           ; customerservice@bcferries.com ; We will respond as quickly as we can. Our response times are currently within 7-10 days.
                            ;                          ; BCFContactUsComponent5 ; Tweet us           ; @BCFerries                    ; 6:00 am - 10:00pm (Monday-Friday)
#########################
##BCFContactUsComponent##
UPDATE BCFContactUsComponent; $contentCV[unique = true]; uid[unique = true]       ; title[lang = $lang]; topText[lang = $lang]; description[lang = $lang]
                            ;                          ; BCFLostAndFoundComponent ;                    ; Lost & found         ; Call 1-888-223-3779 if you're lookingfor an item within 15 days of travel(30 days of wallets and purses) or visit the customer services deskat the terminal.
                            ;                          ; BCMakeAClaimComponent    ;                    ; Make a claim         ; Call 1-888-BC FERRIES (223-3779) if you're looking for an item within 15 days of travel(30 days of wallets and purses) or visit the customer services deskat the terminal.
                            ;                          ; BCFScamAlertsComponent   ;                    ; Scam alerts          ; Be on the lookout for fraudsters posing as legitimate BC Ferries representatives. Learn how to avoid email, phone and online scams.

UPDATE BCFContactUsComponent; $contentCV[unique = true]; uid[unique = true]    ; title[lang = $lang]; topText[lang = $lang]    ; description[lang = $lang]
                            ;                          ; BCFLocationComponent  ; Location:          ; The Fairmont Pacific Rim ; 1010 Canada Place Vancouver, British Columbia V6C0B9
                            ;                          ; BCFInPersonComponent  ; In person:         ; Hours of operation       ; "<p><strong>MONDAY TO FRIDAY</strong><br>from 9:00 am - 6:00 pm</p> <p><strong>SATURDAY AND SUNDAY</strong><br>from 10:00 am- 5:30 pm</p>"
                            ;                          ; BCFTelephoneComponent ; Telephone:         ; 1-888-223-3779 Ext. 3    ; "<p><strong>MONDAY TO FRIDAY</strong><br>from 8:00 am - 8:00 pm</p> <p><strong>SATURDAY AND SUNDAY</strong><br>from 9:00 am- 6:00 pm</p>"

UPDATE BCFLeftBodyContentComponent; $contentCV[unique = true]; uid[unique = true]                 ; content[lang = $lang]
                                  ;                          ; BCFAboutUsLeftBodyContentComponent ; "<h2>About BC Ferries</h2> <p>For almost 60 years, we have been dedicated to providing safe and efficient travel to our customers. Operations began in 1960 with just two ferriesand one route. Today we operate 24 routes with 35 ferries providing access to over 1,600km of beautiful BC coastline. As an independent operator of one of the largest, most complex ferry systems in the world, we are proudto be able to deliver 470 sailings, 60,000 people and 23,000 vehicles safely to their destinations every single day of the year.</p>"
