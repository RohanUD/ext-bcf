/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.cms2.model.restrictions.OrderStatusRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Collection;
import org.apache.commons.collections.CollectionUtils;
import com.bcf.core.service.BcfBookingService;


/**
 * Evaluates an ASM agent through session.
 * <p/>
 */
public class OrderStatusRestrictionEvaluator implements
		CMSRestrictionEvaluator<OrderStatusRestrictionModel>
{
	private SessionService sessionService;

	private BcfBookingService bcfBookingService;

	@Override
	public boolean evaluate(final OrderStatusRestrictionModel orderStatusRestrictionModel, final RestrictionData context)
	{
		Collection<OrderStatus> notAllowedOrderStatuses=orderStatusRestrictionModel.getNotAllowedOrderStatuses();

		if(CollectionUtils.isEmpty(notAllowedOrderStatuses)){
			return true;
		}
		final String bookingReference = getSessionService().getAttribute("bookingReference");
		final OrderModel orderModel = getBcfBookingService().getOrderModelFromStore(bookingReference);

		return !notAllowedOrderStatuses.contains(orderModel.getStatus());
	}


	public BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
