/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.model.restrictions.ASMUserRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;


/**
 * Evaluates an ASM agent through session.
 * <p/>
 */
public class ASMUserRestrictionEvaluator implements
		CMSRestrictionEvaluator<ASMUserRestrictionModel>
{
	private AssistedServiceFacade assistedServiceFacade;

	@Override
	public boolean evaluate(final ASMUserRestrictionModel asmUserRestrictionModel,
			final RestrictionData context)
	{
		return assistedServiceFacade.isAssistedServiceAgentLoggedIn();
	}

	public AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}
}
