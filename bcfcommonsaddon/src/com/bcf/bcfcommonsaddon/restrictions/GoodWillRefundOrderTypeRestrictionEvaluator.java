/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Arrays;
import com.bcf.bcfcommonsaddon.model.restrictions.GoodWillRefundOrderTypeRestrictionModel;
import com.bcf.facades.bcffacades.BcfBookingFacade;


/**
 * Evaluates an ASM agent through session.
 * <p/>
 */
public class GoodWillRefundOrderTypeRestrictionEvaluator implements
		CMSRestrictionEvaluator<GoodWillRefundOrderTypeRestrictionModel>
{

	private SessionService sessionService;

	private BcfBookingFacade bookingFacade;

	private AssistedServiceFacade assistedServiceFacade;

	public AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public BcfBookingFacade getBookingFacade()
	{
		return bookingFacade;
	}

	public void setBookingFacade(final BcfBookingFacade bookingFacade)
	{
		this.bookingFacade = bookingFacade;
	}

	@Override
	public boolean evaluate(final GoodWillRefundOrderTypeRestrictionModel goodWillRefundOrderTypeRestrictionModel, final RestrictionData context)
	{
		final String bookingReference = getSessionService().getAttribute("bookingReference");

		return
				assistedServiceFacade.isAssistedServiceAgentLoggedIn() && getBookingFacade().checkBookingJourneyType(bookingReference,
						Arrays.asList(BookingJourneyType.BOOKING_PACKAGE,BookingJourneyType.BOOKING_TRANSPORT_ACCOMMODATION,BookingJourneyType.BOOKING_ALACARTE));

	}

}
