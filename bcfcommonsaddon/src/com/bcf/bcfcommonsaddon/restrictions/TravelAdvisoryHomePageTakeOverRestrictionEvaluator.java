package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.cms2.model.restrictions.TravelAdvisoryHomePageTakeOverRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.traveladvisory.TravelAdvisoryFacade;


public class TravelAdvisoryHomePageTakeOverRestrictionEvaluator
		implements CMSRestrictionEvaluator<TravelAdvisoryHomePageTakeOverRestrictionModel>
{
	private TravelAdvisoryFacade travelAdvisoryFacade;

	@Override
	public boolean evaluate(final TravelAdvisoryHomePageTakeOverRestrictionModel travelAdvisoryHomePageTakeOverRestrictionModel,
			final RestrictionData restrictionData)
	{
		return getTravelAdvisoryFacade().findHomePageTakeOverAdvisory().isPresent()
				== travelAdvisoryHomePageTakeOverRestrictionModel.isDisplayOnTakeOver();
	}

	public TravelAdvisoryFacade getTravelAdvisoryFacade()
	{
		return travelAdvisoryFacade;
	}

	@Required
	public void setTravelAdvisoryFacade(final TravelAdvisoryFacade travelAdvisoryFacade)
	{
		this.travelAdvisoryFacade = travelAdvisoryFacade;
	}
}
