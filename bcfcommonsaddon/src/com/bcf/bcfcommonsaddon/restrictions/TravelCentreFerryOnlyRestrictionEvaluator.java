/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.model.restrictions.TravelCentreFerryOnlyRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;


public class TravelCentreFerryOnlyRestrictionEvaluator implements
		CMSRestrictionEvaluator<TravelCentreFerryOnlyRestrictionModel>
{
	private AssistedServiceFacade assistedServiceFacade;
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Override
	public boolean evaluate(final TravelCentreFerryOnlyRestrictionModel travelCentreFerryOnlyRestriction,
			final RestrictionData context)
	{
		if (getAssistedServiceFacade().isAssistedServiceAgentLoggedIn())
		{
			final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
			return !StringUtils.equals(channel, SalesApplication.TRAVELCENTREFERRYONLY.getCode());
		}
		return true;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	@Required
	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	protected AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	@Required
	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}
}

