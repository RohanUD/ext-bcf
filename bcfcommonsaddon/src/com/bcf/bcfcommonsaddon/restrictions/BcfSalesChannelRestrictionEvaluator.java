/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfcommonsaddon.restrictions;

import de.hybris.platform.cms2.model.restrictions.BcfSalesChannelRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.servicelayer.session.SessionService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * Evaluates an Bcf Sales Channel restriction
 * <p/>
 */
public class BcfSalesChannelRestrictionEvaluator implements
		CMSRestrictionEvaluator<BcfSalesChannelRestrictionModel>
{
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;
	private SessionService sessionService;

	@Override
	public boolean evaluate(final BcfSalesChannelRestrictionModel bcfSalesChannelRestrictionModel,
			final RestrictionData context)
	{

		final Object sessionJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		return bcfSalesChannelRestrictionModel.getSalesChannels()
				.contains(bcfSalesApplicationResolverFacade.getCurrentSalesChannel()) && (sessionJourney == null
				|| !BcfFacadesConstants.BOOKING_PACKAGE.equals(sessionJourney));
	}

	public BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	public void setBcfSalesApplicationResolverFacade(
			final BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade)
	{
		this.bcfSalesApplicationResolverFacade = bcfSalesApplicationResolverFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
