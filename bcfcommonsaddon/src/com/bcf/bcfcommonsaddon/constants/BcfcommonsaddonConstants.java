/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.constants;

/**
 * Global class for all Bcfcommonsaddon constants. You can add global constants for your extension into this class.
 */
public final class BcfcommonsaddonConstants extends GeneratedBcfcommonsaddonConstants
{
	public static final String EXTENSIONNAME = "bcfcommonsaddon";
	public static final String VALIDATE_QUERY_PARAM_REGEX = "^(:?[\\p{L}\\p{N}\\-\\_\\.\\+\\/\\s\\p{Sc}\\|]*)*$";

	private BcfcommonsaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
	public static final String VEHICLE_CATEGORY_STANDARD = "standard";
	public static final String VEHICLE_CODE_UH = "UH";
	public static final int VEHICLE_HEIGHT = 0;
	public static final int VEHICLE_LENGTH = 0;
	public static final int VEHICLE_WIDTH = 0;
	public static final String CHANGE_BOOKING_REQUEST_TITLE = "Change Booking Request";
	public static final String CHANGE_BOOKING_ERROR_FLAG = "changeBookingErrorFlag";
	public static final String BOOKING_REFERENCE = "bookingReference";
	public static final String GUEST_BOOKING_IDENTIFIER = "guestBookingIdentifier";
	public static final String CHANGE_BOOKING_FROM = "changeBookingForm";
	public static final String CHANGE_BOOKING_BINDING_RESULT = "org.springframework.validation.BindingResult.changeBookingForm";
	public static final String CHANGE_BOOKING_SUCCESS_MESSAGE = "changebooking.form.success";
}
