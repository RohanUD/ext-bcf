/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonConstants;


@SuppressWarnings("PMD")
public class BcfcommonsaddonManager extends GeneratedBcfcommonsaddonManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(BcfcommonsaddonManager.class.getName());

	public static final BcfcommonsaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfcommonsaddonManager) em.getExtension(BcfcommonsaddonConstants.EXTENSIONNAME);
	}

}
