/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.ticket.enums.CsTicketState;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.facades.bcffacades.BCFChangeBookingRequestFacade;
import com.bcf.facades.change.booking.CsTicketData;


/**
 * Controller class to handle change booking requests for asm user
 */
@Controller
@RequestMapping("/manage-booking/change-booking-requests")
public class ChangeBookingRequestsPageController extends BcfAbstractPageController
{

	private static final Logger LOGGER = Logger.getLogger(ChangeBookingRequestsPageController.class);
	private static final String CHANGE_BOOKING_REQUESTS_CMS_PAGE = "changeBookingRequests";
	public static final String CHANGE_BOOKING_REQUESTS_PAGE = "/manage-booking/change-booking-requests";
	public static final String CS_TICKETS = "csTicketDatas";

	@Resource(name = "bcfChangeBookingRequestFacade")
	private BCFChangeBookingRequestFacade bcfChangeBookingRequestFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getPage(final Model model)
	{
		try
		{

			List<CsTicketData> tickets= bcfChangeBookingRequestFacade.getAllOpenTickets();

			model.addAttribute(CS_TICKETS, tickets);
			storeCmsPageInModel(model, getContentPageForLabelOrId(CHANGE_BOOKING_REQUESTS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHANGE_BOOKING_REQUESTS_CMS_PAGE));
			return getViewForPage(model);
		}catch (final CMSItemNotFoundException e)
			{
				LOGGER.error("Could not find page with UID "+ CHANGE_BOOKING_REQUESTS_CMS_PAGE);
				throw new AbstractPageController.HttpNotFoundException(e);
			}
	}

	@RequestMapping(value = "/close-ticket", method = RequestMethod.POST)
	public String closeTicket(@RequestParam final String ticketId, final Model model)
	{
		bcfChangeBookingRequestFacade.changeTicketStatus(ticketId, CsTicketState.CLOSED);
		return REDIRECT_PREFIX+CHANGE_BOOKING_REQUESTS_PAGE;
	}

	@RequestMapping(value = "/mark-as-inprogress", method = RequestMethod.POST)
	public String markAsInProgress(@RequestParam final String ticketId, final Model model)
	{
		bcfChangeBookingRequestFacade.changeTicketStatus(ticketId, CsTicketState.INPROGRESS);
		return REDIRECT_PREFIX+CHANGE_BOOKING_REQUESTS_PAGE;
	}



}
