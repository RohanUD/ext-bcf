/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;


public class TravelFinderForm
{
	private FareFinderForm fareFinderForm;
	private AccommodationFinderForm accommodationFinderForm;
	private String travelRoute;
	private String travelRouteName;
	private boolean reservable;
	private boolean onlyAccommodationChange;
	private String availabilityStatus;
	private String referer;

	public boolean isOnlyAccommodationChange()
	{
		return onlyAccommodationChange;
	}

	public void setOnlyAccommodationChange(final boolean onlyAccommodationChange)
	{
		this.onlyAccommodationChange = onlyAccommodationChange;
	}

	/**
	 * @return the fareFinderForm
	 */
	public FareFinderForm getFareFinderForm()
	{
		return fareFinderForm;
	}

	/**
	 * @param fareFinderForm the fareFinderForm to set
	 */
	public void setFareFinderForm(final FareFinderForm fareFinderForm)
	{
		this.fareFinderForm = fareFinderForm;
	}

	/**
	 * @return the accommodationFinderForm
	 */
	public AccommodationFinderForm getAccommodationFinderForm()
	{
		return accommodationFinderForm;
	}

	/**
	 * @param accommodationFinderForm the accommodationFinderForm to set
	 */
	public void setAccommodationFinderForm(final AccommodationFinderForm accommodationFinderForm)
	{
		this.accommodationFinderForm = accommodationFinderForm;
	}

	public String getTravelRoute()
	{
		return travelRoute;
	}

	public void setTravelRoute(final String travelRoute)
	{
		this.travelRoute = travelRoute;
	}

	public String getTravelRouteName()
	{
		return travelRouteName;
	}

	public void setTravelRouteName(final String travelRouteName)
	{
		this.travelRouteName = travelRouteName;
	}

	public boolean isReservable()
	{
		return reservable;
	}

	public void setReservable(final boolean reservable)
	{
		this.reservable = reservable;
	}

	public String getAvailabilityStatus()
	{
		return availabilityStatus;
	}

	public void setAvailabilityStatus(final String availabilityStatus)
	{
		this.availabilityStatus = availabilityStatus;
	}

	public String getReferer()
	{
		return referer;
	}

	public void setReferer(final String referer)
	{
		this.referer = referer;
	}
}
