/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

public class DealFinderForm
{
	private String arrivalLocation;
	private String departureLocation;
	private String arrivalDealLocationName;
	private String departureDealLocationName;
	private int starRating;
	private int duration;
	private String dealType;
	private String departureDate;
	private String location;
	private String destinationLocation;
	private String destinationLocationName;
	private String latitude;
	private String longitude;
	private String suggestionType;

	public String getArrivalLocation()
	{
		return arrivalLocation;
	}

	public void setArrivalLocation(final String arrivalLocation)
	{
		this.arrivalLocation = arrivalLocation;
	}

	public String getDepartureLocation()
	{
		return departureLocation;
	}

	public void setDepartureLocation(final String departureLocation)
	{
		this.departureLocation = departureLocation;
	}

	public String getDepartureDealLocationName()
	{
		return departureDealLocationName;
	}

	public void setDepartureDealLocationName(final String departureDealLocationName)
	{
		this.departureDealLocationName = departureDealLocationName;
	}

	public String getArrivalDealLocationName()
	{
		return arrivalDealLocationName;
	}

	public void setArrivalDealLocationName(final String arrivalDealLocationName)
	{
		this.arrivalDealLocationName = arrivalDealLocationName;
	}

	public int getStarRating()
	{
		return starRating;
	}

	public void setStarRating(final int starRating)
	{
		this.starRating = starRating;
	}

	public int getDuration()
	{
		return duration;
	}

	public void setDuration(final int duration)
	{
		this.duration = duration;
	}

	public String getDealType()
	{
		return dealType;
	}

	public void setDealType(final String dealType)
	{
		this.dealType = dealType;
	}

	public String getDepartureDate()
	{
		return departureDate;
	}

	public void setDepartureDate(final String departureDate)
	{
		this.departureDate = departureDate;
	}
	public String getLocation()
	{
		return location;
	}

	public void setLocation(final String location)
	{
		this.location = location;
	}

	public String getDestinationLocation()
	{
		return destinationLocation;
	}

	public void setDestinationLocation(final String destinationLocation)
	{
		this.destinationLocation = destinationLocation;
	}

	public String getDestinationLocationName()
	{
		return destinationLocationName;
	}

	public void setDestinationLocationName(final String destinationLocationName)
	{
		this.destinationLocationName = destinationLocationName;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(final String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(final String longitude)
	{
		this.longitude = longitude;
	}

	public String getSuggestionType()
	{
		return suggestionType;
	}

	public void setSuggestionType(final String suggestionType)
	{
		this.suggestionType = suggestionType;
	}



}
