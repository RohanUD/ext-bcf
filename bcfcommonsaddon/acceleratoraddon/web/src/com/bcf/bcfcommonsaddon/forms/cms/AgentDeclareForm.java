/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfcommonsaddon.forms.cms;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import org.springframework.format.annotation.DateTimeFormat;


public class AgentDeclareForm
{
	private String portLocation;

	private String userId;

	private String userName;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date sessionStart;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date declareEnd;

	private String declareBy;

	private BigDecimal cashDeclaredAmount;

	private String bankDepositNumber;

	private Boolean auditRequestFlag;

	private String auditComment;

	private Map<String, BigDecimal> giftCardDeclaredTypes;

	private Map<String, BigDecimal> creditCardDeclaredTypes;

	public String getPortLocation()
	{
		return portLocation;
	}

	public void setPortLocation(final String portLocation)
	{
		this.portLocation = portLocation;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(final String userName)
	{
		this.userName = userName;
	}

	public Date getSessionStart()
	{
		return sessionStart;
	}

	public void setSessionStart(final Date sessionStart)
	{
		this.sessionStart = sessionStart;
	}

	public Date getDeclareEnd()
	{
		return declareEnd;
	}

	public void setDeclareEnd(final Date declareEnd)
	{
		this.declareEnd = declareEnd;
	}

	public String getDeclareBy()
	{
		return declareBy;
	}

	public void setDeclareBy(final String declareBy)
	{
		this.declareBy = declareBy;
	}

	public BigDecimal getCashDeclaredAmount()
	{
		return cashDeclaredAmount;
	}

	public void setCashDeclaredAmount(final BigDecimal cashDeclaredAmount)
	{
		this.cashDeclaredAmount = cashDeclaredAmount;
	}

	public String getBankDepositNumber()
	{
		return bankDepositNumber;
	}

	public void setBankDepositNumber(final String bankDepositNumber)
	{
		this.bankDepositNumber = bankDepositNumber;
	}

	public Map<String, BigDecimal> getCreditCardDeclaredTypes()
	{
		return creditCardDeclaredTypes;
	}

	public void setCreditCardDeclaredTypes(final Map<String, BigDecimal> creditCardDeclaredTypes)
	{
		this.creditCardDeclaredTypes = creditCardDeclaredTypes;
	}

	public Map<String, BigDecimal> getGiftCardDeclaredTypes()
	{
		return giftCardDeclaredTypes;
	}

	public void setGiftCardDeclaredTypes(final Map<String, BigDecimal> giftCardDeclaredTypes)
	{
		this.giftCardDeclaredTypes = giftCardDeclaredTypes;
	}

	public Boolean getAuditRequestFlag()
	{
		return auditRequestFlag;
	}

	public void setAuditRequestFlag(final Boolean auditRequestFlag)
	{
		this.auditRequestFlag = auditRequestFlag;
	}

	public String getAuditComment()
	{
		return auditComment;
	}

	public void setAuditComment(final String auditComment)
	{
		this.auditComment = auditComment;
	}
}
