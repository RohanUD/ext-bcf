/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

public class ScheduleFinderForm
{
	private String fromLocation;
	private String toLocation;
	private String scheduleDate;
	private String arrivalLocation;
	private String departureLocation;

	public String getFromLocation()
	{
		return fromLocation;
	}

	public void setFromLocation(final String fromLocation)
	{
		this.fromLocation = fromLocation;
	}

	public String getToLocation()
	{
		return toLocation;
	}

	public void setToLocation(final String toLocation)
	{
		this.toLocation = toLocation;
	}

	public String getScheduleDate()
	{
		return scheduleDate;
	}

	public void setScheduleDate(final String scheduleDate)
	{
		this.scheduleDate = scheduleDate;
	}

	public String getArrivalLocation()
	{
		return arrivalLocation;
	}

	public void setArrivalLocation(final String arrivalLocation)
	{
		this.arrivalLocation = arrivalLocation;
	}

	public String getDepartureLocation()
	{
		return departureLocation;
	}

	public void setDepartureLocation(final String departureLocation)
	{
		this.departureLocation = departureLocation;
	}

}
