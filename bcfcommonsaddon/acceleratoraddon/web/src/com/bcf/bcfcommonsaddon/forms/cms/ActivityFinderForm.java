/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

import java.util.List;
import com.bcf.facades.vacations.GuestData;


public class ActivityFinderForm
{
	private String destination;
	private String date;
	private String activity;
	private String activityCategoryTypes;
	private String query;
	private String sort;
	private int currentPage;
	private List<GuestData> guestData;
	private boolean changeActivity;
	private String changeActivityCode;
	private String changeActivityDate;
	private String changeActivityTime;
	private String changeActivityRef;
	private int maxGuestCountPerPaxType;

	public String getChangeActivityRef()
	{
		return changeActivityRef;
	}

	public void setChangeActivityRef(final String changeActivityRef)
	{
		this.changeActivityRef = changeActivityRef;
	}

	/**
	 * @return the destination
	 */
	public String getDestination()
	{
		return destination;
	}

	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * @return the activity
	 */
	public String getActivity()
	{
		return activity;
	}

	/**
	 * @return the activityCategoryTypes
	 */
	public String getActivityCategoryTypes()
	{
		return activityCategoryTypes;
	}

	/**
	 * @return the query
	 */
	public String getQuery()
	{
		return query;
	}

	/**
	 * @return the sort
	 */
	public String getSort()
	{
		return sort;
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage()
	{
		return currentPage;
	}

	/**
	 * @return the guestData
	 */
	public List<GuestData> getGuestData()
	{
		return guestData;
	}

	/**
	 * @param destination
	 *           the destination to set
	 */
	public void setDestination(final String destination)
	{
		this.destination = destination;
	}

	/**
	 * @param date
	 *           the date to set
	 */
	public void setDate(final String date)
	{
		this.date = date;
	}

	/**
	 * @param activity
	 *           the activity to set
	 */
	public void setActivity(final String activity)
	{
		this.activity = activity;
	}

	/**
	 * @param activityCategoryTypes
	 *           the activityCategoryTypes to set
	 */
	public void setActivityCategoryTypes(final String activityCategoryTypes)
	{
		this.activityCategoryTypes = activityCategoryTypes;
	}

	/**
	 * @param query
	 *           the query to set
	 */
	public void setQuery(final String query)
	{
		this.query = query;
	}

	/**
	 * @param sort
	 *           the sort to set
	 */
	public void setSort(final String sort)
	{
		this.sort = sort;
	}

	/**
	 * @param currentPage
	 *           the currentPage to set
	 */
	public void setCurrentPage(final int currentPage)
	{
		this.currentPage = currentPage;
	}

	/**
	 * @param guestData
	 *           the guestData to set
	 */
	public void setGuestData(final List<GuestData> guestData)
	{
		this.guestData = guestData;
	}

	public boolean isChangeActivity()
	{
		return changeActivity;
	}

	public void setChangeActivity(final boolean changeActivity)
	{
		this.changeActivity = changeActivity;
	}

	public String getChangeActivityCode()
	{
		return changeActivityCode;
	}

	public void setChangeActivityCode(final String changeActivityCode)
	{
		this.changeActivityCode = changeActivityCode;
	}

	public String getChangeActivityDate()
	{
		return changeActivityDate;
	}

	public void setChangeActivityDate(final String changeActivityDate)
	{
		this.changeActivityDate = changeActivityDate;
	}

	public String getChangeActivityTime()
	{
		return changeActivityTime;
	}

	public void setChangeActivityTime(final String changeActivityTime)
	{
		this.changeActivityTime = changeActivityTime;
	}

	public int getMaxGuestCountPerPaxType()
	{
		return maxGuestCountPerPaxType;
	}

	public void setMaxGuestCountPerPaxType(final int maxGuestCountPerPaxType)
	{
		this.maxGuestCountPerPaxType = maxGuestCountPerPaxType;
	}
}
