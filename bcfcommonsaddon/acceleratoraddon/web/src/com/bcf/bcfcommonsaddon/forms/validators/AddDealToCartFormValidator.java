/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.validators;

import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcommonsaddon.forms.cms.AddDealToCartForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;


@Component("addDealToCartFormValidator")
public class AddDealToCartFormValidator extends AbstractTravelValidator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return AddDealToCartFormValidator.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddDealToCartForm form = (AddDealToCartForm) object;
		validateBlankField("bundleTemplateID", form.getBundleTemplateID(), errors);
		validateDates(form.getStartingDate(), TravelacceleratorstorefrontValidationConstants.STARTING_DATE, form.getEndingDate(),
				TravelacceleratorstorefrontValidationConstants.ENDING_DATE, errors);

		validateEmptyField("itineraryPricingInfos", form.getItineraryPricingInfos(), errors);
		validateEmptyField("passengerTypes", form.getPassengerTypes(), errors);
	}

}
