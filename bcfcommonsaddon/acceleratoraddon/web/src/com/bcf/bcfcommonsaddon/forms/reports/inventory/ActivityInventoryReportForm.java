/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.reports.inventory;

import javax.validation.constraints.NotNull;


public class ActivityInventoryReportForm
{
	@NotNull
	private String startDate;
	@NotNull
	private String endDate;
	private String[] daysOfWeek;
	private String region;
	private String city;
	private String activity;

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(final String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(final String endDate)
	{
		this.endDate = endDate;
	}

	public String[] getDaysOfWeek()
	{
		return daysOfWeek;
	}

	public void setDaysOfWeek(final String[] daysOfWeek)
	{
		this.daysOfWeek = daysOfWeek;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getActivity()
	{
		return activity;
	}

	public void setActivity(final String activity)
	{
		this.activity = activity;
	}
}
