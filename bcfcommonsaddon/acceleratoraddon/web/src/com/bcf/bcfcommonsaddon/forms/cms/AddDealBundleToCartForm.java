/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import java.io.Serializable;
import java.util.List;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;


public class AddDealBundleToCartForm implements Serializable
{
	/**
	 * Default serialVersionUID value.
	 */

	private static final long serialVersionUID = 1L;
	private List<AddBundleToCartForm> addBundleToCartForms;
	private List<PassengerTypeQuantityData> passengerTypeQuantityList;
	private String parentDealId;
	private String startingDate;
	private String endingDate;
	private String checkInDate;
	private String checkOutDate;
	private String isValid;
	private List<String> errors;
	public List<AddBundleToCartForm> getAddBundleToCartForms()
	{
		return addBundleToCartForms;
	}

	public void setAddBundleToCartForms(final List<AddBundleToCartForm> addBundleToCartForms)
	{
		this.addBundleToCartForms = addBundleToCartForms;
	}

	public String getParentDealId()
	{
		return parentDealId;
	}

	public void setParentDealId(final String parentDealId)
	{
		this.parentDealId = parentDealId;
	}

	public List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		return passengerTypeQuantityList;
	}

	/**
	 * @param passengerTypeQuantityList the passengerTypeQuantityList to set
	 */
	public void setPassengerTypeQuantityList(
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		this.passengerTypeQuantityList = passengerTypeQuantityList;
	}

	public String getStartingDate()
	{
		return startingDate;
	}

	public void setStartingDate(final String startingDate)
	{
		this.startingDate = startingDate;
	}

	public String getEndingDate()
	{
		return endingDate;
	}

	public void setEndingDate(final String endingDate)
	{
		this.endingDate = endingDate;
	}

	public String getIsValid()
	{
		return isValid;
	}

	public void setIsValid(final String isValid)
	{
		this.isValid = isValid;
	}

	public List<String> getErrors()
	{
		return errors;
	}

	public void setErrors(final List<String> errors)
	{
		this.errors = errors;
	}

	public String getCheckInDate()
	{
		return checkInDate;
	}

	public void setCheckInDate(final String checkInDate)
	{
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate()
	{
		return checkOutDate;
	}

	public void setCheckOutDate(final String checkOutDate)
	{
		this.checkOutDate = checkOutDate;
	}
}
