/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.forms.cms;

import java.util.List;
import javax.validation.Valid;
import com.bcf.bcfaccommodationsaddon.forms.LeadGuestDetailsForm;
import com.bcf.bcfvoyageaddon.forms.TravellerForm;
import com.bcf.facades.travel.data.BCFTravellersData;


public class PersonalDetailsForm
{
	@Valid
	private List<TravellerForm> travellerForms;

	@Valid
	private List<LeadGuestDetailsForm> leadForms;

	@Valid
	private BCFTravellersData bcfTravellersData;

	public BCFTravellersData getBcfTravellersData()
	{
		return bcfTravellersData;
	}

	public void setBcfTravellersData(final BCFTravellersData bcfTravellersData)
	{
		this.bcfTravellersData = bcfTravellersData;
	}

	private boolean useDiffLeadDetails;

	/**
	 * @return the travellerForms
	 */
	public List<TravellerForm> getTravellerForms()
	{
		return travellerForms;
	}

	/**
	 * @return the leadForms
	 */
	public List<LeadGuestDetailsForm> getLeadForms()
	{
		return leadForms;
	}

	/**
	 * @param travellerForms the travellerForms to set
	 */
	public void setTravellerForms(final List<TravellerForm> travellerForms)
	{
		this.travellerForms = travellerForms;
	}

	/**
	 * @param leadForms the leadForms to set
	 */
	public void setLeadForms(final List<LeadGuestDetailsForm> leadForms)
	{
		this.leadForms = leadForms;
	}

	/**
	 * @return the useDiffLeadDetails
	 */
	public boolean isUseDiffLeadDetails()
	{
		return useDiffLeadDetails;
	}

	/**
	 * @param useDiffLeadDetails the useDiffLeadDetails to set
	 */
	public void setUseDiffLeadDetails(final boolean useDiffLeadDetails)
	{
		this.useDiffLeadDetails = useDiffLeadDetails;
	}

}
