/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfcommonsaddon.validators;

import java.math.BigDecimal;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfcommonsaddon.forms.cms.RefundReasonCodeForm;
import com.bcf.core.service.BCFGoodWillRefundService;

@Component("bcfGoodWillRefundValidator")
public class BCFGoodWillRefundValidator implements Validator
{
	@Resource(name = "bcfGoodWillRefundService")
	private BCFGoodWillRefundService bcfGoodWillRefundService;

	protected static final String GOODWILL_REFUND_AMOUNT_ERROR = "text.goodwill.refund.amount.error";
	protected static final String GOODWILL_REFUND_THRESHOLD_ERROR = "text.goodwill.refund.threshold.error";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RefundReasonCodeForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RefundReasonCodeForm refundReasonCodeForm = (RefundReasonCodeForm) object;

		Double  thresholdValue = bcfGoodWillRefundService.getGoodWillRefund(refundReasonCodeForm.getReasonCode()).getThresholdValue();

		if(thresholdValue!=null){

		String refundAmount=refundReasonCodeForm.getAmount();

		if(StringUtils.isEmpty(refundAmount) ||  ! isDouble(refundAmount)){
			errors.rejectValue("amount", GOODWILL_REFUND_AMOUNT_ERROR);
			return;
		}

		BigDecimal inputThresholdValue = BigDecimal.valueOf(Double.valueOf(refundReasonCodeForm.getAmount()));

		if (inputThresholdValue.compareTo(BigDecimal
				.valueOf(thresholdValue)) > 0)
		{
			errors.rejectValue("amount", GOODWILL_REFUND_THRESHOLD_ERROR);
		}
		}
	}

	private boolean isDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}



}
