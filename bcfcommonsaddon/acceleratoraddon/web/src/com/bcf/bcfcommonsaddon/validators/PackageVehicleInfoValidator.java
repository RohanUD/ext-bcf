/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.validators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfvoyageaddon.validators.VehicleInfoValidator;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


/**
 * Custom validation to validate Package Vehicle Finder Form attributes which can't be validated using JSR303
 */

@Component("packageVehicleInfoValidator")
public class PackageVehicleInfoValidator extends VehicleInfoValidator
{
	@Resource(name = "bcfStandardVehiclePackageValidator")
	private BcfStandardVehiclePackageValidator bcfStandardVehiclePackageValidator;

	private static final String ERROR_VEHICLE_NOT_SELECTED = "vehicleNotSelected";
	private static final String TRAVELLING_WITH_VEHICLE = "fareFinderForm.travellingWithVehicle";
	private static final String CODE = "code";


	@Override
	public boolean supports(final Class<?> clazz)
	{
		return TravelFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final TravelFinderForm travelFinderForm = (TravelFinderForm) object;

		final String departingDate = travelFinderForm.getFareFinderForm().getDepartingDateTime();
		validateForCurrentDate(departingDate, errors, TRAVELLING_WITH_VEHICLE);

		validateVehicleData(travelFinderForm, errors);
	}

	private boolean validateVehicleCode(final String code, final Errors errors, final int i)
	{
		if (StringUtils.isNotEmpty(code))
		{
			return true;
		}
		rejectValue(errors, FARE_FINDER_VEHICLE_INFO + i + "].vehicleType." + CODE, ERROR_VEHICLE_NOT_SELECTED);
		return false;
	}

	/**
	 * Method responsible for validating Vehicles
	 *
	 * @param travelFinderForm
	 */
	private void validateVehicleData(final TravelFinderForm travelFinderForm, final Errors errors)
	{
		boolean validCode = false;
		for (int i = 0; i < travelFinderForm.getFareFinderForm().getVehicleInfo().size(); i++)
		{
			final String code = travelFinderForm.getFareFinderForm().getVehicleInfo().get(i).getVehicleType().getCode();
			validCode = validateVehicleCode(code, errors, i);
			if (!validCode)
			{
				return;
			}
		}
		if (!validCode)
		{
			return;
		}

		final Map<String, List<VehicleTypeQuantityData>> vehiclesGroupedByCategory = new HashMap<>();
		travelFinderForm.getFareFinderForm().getVehicleInfo().forEach(vehicleTypeQuantityData -> {
			List<VehicleTypeQuantityData> vehicleTypeQuantityDatas = vehiclesGroupedByCategory
					.get(vehicleTypeQuantityData.getVehicleType().getCategory());
			if (CollectionUtils.isEmpty(vehicleTypeQuantityDatas))
			{
				vehicleTypeQuantityDatas = new ArrayList<>();
				vehiclesGroupedByCategory.put(vehicleTypeQuantityData.getVehicleType().getCategory(), vehicleTypeQuantityDatas);
			}
			vehicleTypeQuantityDatas.add(vehicleTypeQuantityData);
		});

		vehiclesGroupedByCategory.forEach((category, vehicleTypeQuantityDatas) -> {
			if (StringUtils.isNotEmpty(category))
			{
				bcfStandardVehiclePackageValidator.validate(vehicleTypeQuantityDatas, errors);
			}
		});
	}
}
