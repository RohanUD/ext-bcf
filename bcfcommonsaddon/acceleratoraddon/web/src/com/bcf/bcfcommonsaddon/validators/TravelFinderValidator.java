/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/09/19, 1:36 PM
 */

package com.bcf.bcfcommonsaddon.validators;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;


@Component("travelFinderValidator")
public class TravelFinderValidator extends AbstractTravelValidator
{
	private static final Logger LOG = Logger.getLogger(TravelFinderValidator.class);

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return TravelFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final TravelFinderForm travelFinderForm = (TravelFinderForm) object;
		validateTravelRoute(travelFinderForm, errors);
	}

	protected void validateTravelRoute(final TravelFinderForm travelFinderForm, final Errors errors)
	{
		validateBlankField(BcfcommonsaddonWebConstants.TRAVEL_ROUTE_NAME,
				travelFinderForm.getTravelRoute(), errors);
	}
}
