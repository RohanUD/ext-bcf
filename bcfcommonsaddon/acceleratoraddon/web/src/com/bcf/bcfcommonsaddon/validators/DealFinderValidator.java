/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.validators;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcommonsaddon.forms.cms.DealFinderForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;


@Component("dealFinderValidator")
public class DealFinderValidator extends AbstractTravelValidator
{
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	private static final String FIELD_DEPARTING_DATE_TIME = "departureDate";
	private static final String ERROR_TYPE_NOT_POPULATED = "NotPopulated";

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return com.bcf.bcfcommonsaddon.forms.cms.DealFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final DealFinderForm dealFinderForm = (DealFinderForm) object;
		setTargetForm(BcfvoyageaddonWebConstants.DEAL_FINDER_FORM);
		// first check if there is a valid data set
		validateDuration(dealFinderForm, errors);
		validateDate(dealFinderForm, errors);
		validateLocation(dealFinderForm, errors);
	}

	protected void validateLocation(final DealFinderForm dealFinderForm, final Errors errors)
	{
		if ((StringUtils.isBlank(dealFinderForm.getArrivalLocation()) || StringUtils.isBlank(dealFinderForm.getDepartureLocation()))
				&& StringUtils.isBlank(dealFinderForm.getLocation()))
		{
			rejectValue(errors, BcfvoyageaddonWebConstants.FIELD_ARRIVAL_LOCATION,
					TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
			rejectValue(errors, BcfvoyageaddonWebConstants.FIELD_DEPARTURE_LOCATION,
					TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
			rejectValue(errors, BcfvoyageaddonWebConstants.FIELD_LOCATION,
					TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
		}
	}


	protected Boolean validateDuration(final DealFinderForm dealFinderForm, final Errors errors)
	{
		if (dealFinderForm.getDuration() > 0)
		{
			return Boolean.TRUE;
		}
		rejectValue(errors, BcfvoyageaddonWebConstants.DURATION, BcfvoyageaddonWebConstants.ERROR_TYPE_EMPTY_DURATION);
		return Boolean.FALSE;
	}

	protected Boolean validateFieldFormat(final String fieldName, final String fieldValue, final String regex, final Errors errors)
	{
		if (!Pattern.matches(regex, fieldValue))
		{
			rejectValue(errors, fieldName, BcfvoyageaddonWebConstants.ERROR_TYPE_INVALID_FIELD_FORMAT);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	protected void validateDate(final DealFinderForm dealFinderForm, final Errors errors)
	{
		final boolean isDepartingDatePopulated = validateDateIsPopulated(dealFinderForm.getDepartureDate(), errors,
				FIELD_DEPARTING_DATE_TIME);

		if (isDepartingDatePopulated)
		{
			validateDateFormatFareFinder(dealFinderForm.getDepartureDate(), errors,
					FIELD_DEPARTING_DATE_TIME);
		}
	}

	protected boolean validateDateIsPopulated(final String date, final Errors errors, final String field)
	{
		if (StringUtils.isNotEmpty(date))
		{
			return true;
		}

		rejectValue(errors, field, ERROR_TYPE_NOT_POPULATED);

		return false;
	}

}
