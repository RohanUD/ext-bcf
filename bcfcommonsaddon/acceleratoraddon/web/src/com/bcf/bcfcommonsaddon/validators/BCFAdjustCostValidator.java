/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfcommonsaddon.validators;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfaccommodationsaddon.forms.cms.AdjustCostForm;
import com.bcf.facades.managebooking.data.AdjustCostData;


@Component("bcfAdjustCostValidator")
public class BCFAdjustCostValidator implements Validator
{
	protected static final String ADJUST_COST_AMOUNT_ERROR = "text.adjustcost.amount.error";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AdjustCostForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AdjustCostForm adjustCostForm = (AdjustCostForm) object;

		if(CollectionUtils.isNotEmpty(adjustCostForm.getAdjustAccommodationCostDatas())){
			for(int i =0;i<adjustCostForm.getAdjustAccommodationCostDatas().size();i++){
				AdjustCostData adjustCostData=adjustCostForm.getAdjustAccommodationCostDatas().get(i);
				if(StringUtils.isEmpty(adjustCostData.getCost()) ||  ! isDouble(adjustCostData.getCost())){
					errors.rejectValue("adjustAccommodationCostDatas[" + i + "].cost", ADJUST_COST_AMOUNT_ERROR);
					return;
				}
			}
		}

		if(CollectionUtils.isNotEmpty(adjustCostForm.getAdjustActivityCostDatas())){
			for(int i =0;i<adjustCostForm.getAdjustActivityCostDatas().size();i++){
				AdjustCostData adjustCostData=adjustCostForm.getAdjustActivityCostDatas().get(i);
				if(StringUtils.isEmpty(adjustCostData.getCost()) ||  ! isDouble(adjustCostData.getCost())){
					errors.rejectValue("adjustActivityCostDatas[" + i + "].cost", ADJUST_COST_AMOUNT_ERROR);
					return;
				}
			}
		}

	}

	private boolean isDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
