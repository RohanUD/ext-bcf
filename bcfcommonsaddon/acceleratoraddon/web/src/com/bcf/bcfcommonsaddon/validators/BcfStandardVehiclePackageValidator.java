/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.validators;

import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfvoyageaddon.validators.BcfStandardVehicleFareFinderValidator;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


@Component("bcfStandardVehiclePackageValidator")
public class BcfStandardVehiclePackageValidator extends BcfStandardVehicleFareFinderValidator implements Validator
{
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityData = (List<VehicleTypeQuantityData>) object;

		for (int i = 0; i < vehicleTypeQuantityData.size(); i++)
		{
			if (Objects.nonNull(vehicleTypeQuantityData.get(i).getVehicleType()) &&
					StringUtils.isNotEmpty(vehicleTypeQuantityData.get(i).getVehicleType().getCategory()))
			{
				final TravelRouteModel travelRouteModel = getTravelRoute(vehicleTypeQuantityData.get(i).getTravelRouteCode());
				final TransportFacilityModel originTransportFacilityData = getTransportFacility(
						travelRouteModel.getOrigin().getCode());
				final TransportFacilityModel destinationTransportFacilityData = getTransportFacility(
						travelRouteModel.getDestination().getCode());

				if (Objects.nonNull(originTransportFacilityData) && Objects.nonNull(destinationTransportFacilityData))
				{
					validateStandardVehicle(originTransportFacilityData, destinationTransportFacilityData, vehicleTypeQuantityData, i,
							FARE_FINDER_VEHICLE_INFO, errors);
				}
			}
		}
	}
}
