/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.validators;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.forms.GetAQuoteForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


@Component("getAQuoteValidator")
public class GetAQuoteValidator extends AbstractTravelValidator
{
	final String FIRST_NAME = "firstName";
	final String LAST_NAME = "lastName";
	final String EMAIL = "email";
	final String CONFIRM_EMAIL = "confirmEmail";
	final String PHONE_NUMBER = "phoneNumber";
	final String COMMENTS = "comments";
	final String NUMBER_OF_ROOMS = "numberOfRooms";
	final String POSTAL_CODE = "postalCode";

	public static final Pattern PHONENO_REGEX = Pattern.compile("(\\+?\\d+\\s?\\-?\\(?\\d+\\)?\\s?\\-?\\d+\\s?-?\\d+)");

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return GetAQuoteForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final GetAQuoteForm getAQuoteForm = (GetAQuoteForm) object;
		setTargetForm(BcfvoyageaddonWebConstants.GET_A_QUOTE_FORM);
		final String firstName = getAQuoteForm.getFirstName();
		final String lastName = getAQuoteForm.getLastName();
		final String email = getAQuoteForm.getEmail();
		final String confirmEmail = getAQuoteForm.getConfirmEmail();
		final String phoneNumber = getAQuoteForm.getPhoneNumber();
		final String comments = getAQuoteForm.getComments();
		final int numberOfRooms = getAQuoteForm.getNumberOfRooms();
		final String checkInDateTime = getAQuoteForm.getCheckInDateTime();
		final String postalCode = getAQuoteForm.getPostalCode();

		if (StringUtils.isBlank(firstName))
		{
			validateBlankField(FIRST_NAME, firstName, errors);
		}
		else if (!validateName(firstName))
		{

			errors.rejectValue(FIRST_NAME, "invalidFirstName");
		}

		if (StringUtils.isBlank(lastName))
		{
			validateBlankField(LAST_NAME, lastName, errors);
		}
		else if (!validateName(lastName))
		{

			errors.rejectValue(LAST_NAME, "invalidLastName");
		}

		if (StringUtils.isBlank(phoneNumber))
		{
			validateBlankField(PHONE_NUMBER, phoneNumber, errors);
		}
		else if (!validatePhoneNumber(phoneNumber))
		{

			errors.rejectValue(PHONE_NUMBER, "invalidPhoneNumber");
		}

		if (StringUtils.isBlank(email))
		{
			validateBlankField(EMAIL, email, errors);
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{

			errors.rejectValue(EMAIL, "invalidEmail");
		}

		compareEmails(errors, email, confirmEmail);
		validatePhoneNo(phoneNumber);
		validateComments(errors, comments);
		validateNumberOfRooms(errors, numberOfRooms);
		validateDates(errors, checkInDateTime, TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
		validatePostalCode(errors, postalCode);
		validateGuestsQuantity(getAQuoteForm.getNumberOfRooms(),
				getAQuoteForm.getRoomStayCandidates(), errors);
	}

	private boolean validateName(final String name)
	{
		return name.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES_SPACES_FIRST_LETTER);

	}

	private boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = BcfFacadesConstants.EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

	private void compareEmails(final Errors errors, final String email, final String confirmEmail)
	{
		if (StringUtils.isNotEmpty(email) && StringUtils.isNotEmpty(confirmEmail) && !StringUtils.equals(email, confirmEmail))
		{
			errors.rejectValue(CONFIRM_EMAIL, "invalidconfirmemail");
		}
		else
		{
			if (StringUtils.isEmpty(confirmEmail))
			{
				errors.rejectValue(CONFIRM_EMAIL, "emptyconfirmemail");
			}
		}
	}

	private boolean validatePhoneNumber(final String contactNumber)
	{
		return contactNumber.matches(TravelacceleratorstorefrontValidationConstants.REGEX_AT_LEAST_ONE_NUMBER)
				|| contactNumber.matches(TravelacceleratorstorefrontValidationConstants.REGEX_NUMBER_AND_SPECIAL_CHARS);
	}

	private boolean validatePhoneNo(final String phoneNo)
	{
		final Matcher matcher = PHONENO_REGEX.matcher(phoneNo);
		return matcher.matches();
	}


	private void validateComments(final Errors errors, final String comments)
	{
		if (StringUtils.isEmpty(comments) || StringUtils.length(comments) > configurationService.getConfiguration()
				.getInt(BcfFacadesConstants.MAX_COMMENTS_LENGTH))
		{
			errors.rejectValue(COMMENTS, "invalidcomments");
		}
	}

	private void validateNumberOfRooms(final Errors errors, final int numberOfRooms)
	{
		if (numberOfRooms == 0)
		{
			errors.rejectValue(NUMBER_OF_ROOMS, "invalidnumberofrooms");
		}
	}

	private void validateDates(final Errors errors, final String startDate, final String startDateFieldName)
	{
		final boolean isNotBlank = validateBlankField(startDateFieldName, startDate, errors);

		boolean isCheckInDateCorrectlyFormatted = false;

		if (isNotBlank)
		{
			isCheckInDateCorrectlyFormatted = validateDateFormat(startDate, errors, startDateFieldName);

			if (isCheckInDateCorrectlyFormatted)
			{
				validateForPastDate(startDate, errors, startDateFieldName);
			}
		}
	}

	private void validatePostalCode(final Errors errors, final String postalCode)
	{
		if (StringUtils.isBlank(postalCode))
		{
			errors.rejectValue(POSTAL_CODE, "emptyPostalCode");

		}
	}
}
