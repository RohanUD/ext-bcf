/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.validators;

import reactor.util.CollectionUtils;

import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcommonsaddon.forms.cms.AddDealBundleToCartForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;


@Component("addDealBundleToCartFormValidator")
public class AddDealBundleToCartFormValidator extends AbstractTravelValidator
{
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddDealBundleToCartForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object form, final Errors errors)
	{
		final AddDealBundleToCartForm addDealBundleToCartForm = (AddDealBundleToCartForm) form;
		setTargetForm("addDealBundleToCartForm");
		if (CollectionUtils.isEmpty(addDealBundleToCartForm.getAddBundleToCartForms()))
		{
			rejectValue(errors, "addBundleToCartForms",
					TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
		}

		final Map<String, List<AddBundleToCartForm>> addBundleToCartForms = addDealBundleToCartForm.getAddBundleToCartForms()
				.stream()
				.collect(
						Collectors.groupingBy(AddBundleToCartForm::getDealId));
		for (final Map.Entry<String, List<AddBundleToCartForm>> entry : addBundleToCartForms.entrySet())
		{
			final long count = entry.getValue().stream()
					.filter(addBundleToCartForm -> addBundleToCartForm.getItineraryPricingInfo().isSelected()).count();
			if (!(count == 0 || count == 2))
			{
				rejectValue(errors, "addBundleToCartForms",
						"error.deal.transportoffering.selection.single.error");
			}
		}

		validateDateFormat(addDealBundleToCartForm.getStartingDate(), errors, "startingDate");
	}
}
