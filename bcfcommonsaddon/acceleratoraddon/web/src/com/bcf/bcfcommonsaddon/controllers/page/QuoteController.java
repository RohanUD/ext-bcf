/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteDiscountForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.VoucherForm;
import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CommerceCartMetadata;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.request.TransportPackageRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commercefacades.util.CommerceCartMetadataUtils;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceQuoteAssignmentException;
import de.hybris.platform.commerceservices.order.CommerceQuoteExpirationTimeException;
import de.hybris.platform.commerceservices.order.exceptions.IllegalQuoteStateException;
import de.hybris.platform.commerceservices.order.exceptions.IllegalQuoteSubmitException;
import de.hybris.platform.commerceservices.order.exceptions.QuoteUnderThresholdException;
import de.hybris.platform.commerceservices.util.QuoteExpirationTimeUtils;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator.CannotCloneException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.controllers.util.BcfAccommodationControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.services.order.BCFTravelCommerceCartService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.assistedservicefacades.BCFAssistedServiceFacade;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleFareBreakdownData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.packages.BcfPackageFacade;
import com.bcf.facades.quote.BcfQuoteFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Quotes
 */
@Controller
@RequestMapping(value = "/quote")
public class QuoteController extends AbstractPackagePageController
{
	private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";
	private static final String REDIRECT_QUOTE_LIST_URL = REDIRECT_PREFIX + "/my-account/my-quotes/";
	private static final String REDIRECT_QUOTE_EDIT_URL = REDIRECT_PREFIX + "/quote/%s/edit/";
	private static final String REDIRECT_QUOTE_DETAILS_URL = REDIRECT_PREFIX + "/my-account/my-quotes/%s/";
	private static final String VOUCHER_FORM = "voucherForm";
	private static final String ALLOWED_ACTIONS = "allowedActions";
	private static final String DATE_FORMAT_KEY = "text.quote.dateformat";

	// localization properties
	private static final String PAGINATION_NUMBER_OF_COMMENTS = "quote.pagination.numberofcomments";
	private static final String QUOTE_EMPTY_CART_ERROR = "quote.cart.empty.error";
	private static final String QUOTE_CREATE_ERROR = "quote.create.error";
	private static final String QUOTE_REQUOTE_ERROR = "quote.requote.error";
	private static final String QUOTE_EDIT_LOCKED_ERROR = "quote.edit.locked";
	private static final String QUOTE_TEXT_CANCEL_SUCCESS = "text.quote.cancel.success";
	private static final String QUOTE_TEXT_NOT_CANCELABLE = "text.quote.not.cancelable";
	private static final String QUOTE_NOT_SUBMITABLE_ERROR = "text.quote.not.submitable";
	private static final String QUOTE_NEWCART_ERROR = "quote.newcart.error";
	private static final String QUOTE_NEWCART_SUCCESS = "quote.newcart.success";
	private static final String QUOTE_SAVE_CART_ERROR = "quote.save.cart.error";
	private static final String QUOTE_SUBMIT_ERROR = "quote.submit.error";
	private static final String QUOTE_SUBMIT_SUCCESS = "quote.submit.success";
	private static final String QUOTE_EXPIRED = "quote.state.expired";
	private static final String QUOTE_REJECT_INITIATION_REQUEST = "quote.reject.initiate.request";
	private static final String MY_QUOTES_CMS_PAGE = "my-quotes";
	private static final String QUOTES_DETAILS_PAGE = "quoteDetailsPage";

	private static final Logger LOG = Logger.getLogger(QuoteController.class);
	private static final String SESSION_CART_PARAMETER_NAME = "cart";
	private static final String GLOBAL_RESERVATION_DATA = "globalReservationDataList";
	private static final String CART_DEFAULT_CMS_PAGE = "basketSummaryPage";
	private static final String ALACARTE_REVIEW_PAGE = "alacarteReviewPage";
	private static final String ANCILLARY_PAGE_URL = "/ancillary";
	private static final String HOME_PAGE_PATH = "/";
	private static final String CREATE_QUOTE_FAILED = "text.page.quote.create.failed";
	private static final String CREATE_QUOTE_MODIFIED = "text.page.quote.create.modified";
	private static final String CREATE_QUOTE_SAILINGS_REMOVED = "text.page.quote.create.sailing.removed";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "bcfQuoteFacade")
	private BcfQuoteFacade quoteFacade;

	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	@Resource(name = "saveCartFacade")
	private SaveCartFacade saveCartFacade;

	@Resource(name = "validator")
	private SmartValidator smartValidator;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "packageFacade")
	private PackageFacade packageFacade;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "assistedServiceFacade")
	private BCFAssistedServiceFacade assistedServiceFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@Resource(name = "bcfAccommodationControllerUtil")
	private BcfAccommodationControllerUtil bcfAccommodationControllerUtil;

	@Resource(name = "bcfTravelCommerceCartService")
	private BCFTravelCommerceCartService bcfTravelCommerceCartService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfPackageFacade")
	private BcfPackageFacade bcfPackageFacade;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;


	@Resource(name = "bcfDealCartFacade")
	private BcfDealCartFacade bcfDealCartFacade;

	/**
	 * Creates a new quote based on session cart.
	 *
	 * @param redirectModel
	 * @return Mapping to quote page.
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createQuote(final RedirectAttributes redirectModel, final Model model,
			@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "emailAddress", required = false) final String emailAddress)
	{
		try
		{
			if (packageFacade.isPackageInCart() || bcfTravelCartFacadeHelper.isAlacateFlow())
			{
				if (!bcfTravelCartFacade.hasEntries())
				{
					// No session cart or empty session cart. Bounce back to the cart page.
					LOG.debug("Missing or empty cart");
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_EMPTY_CART_ERROR, null);
					model.addAttribute("quoteMessage", false);
					return BcfcommonsaddonControllerConstants.Views.Pages.Booking.quoteMessageResponse;
				}
				quoteFacade.createQuote(emailAddress);
			}
			model.addAttribute("quoteMessage", true);
			return BcfcommonsaddonControllerConstants.Views.Pages.Booking.quoteMessageResponse;
		}
		catch (final IllegalArgumentException | CannotCloneException | ModelSavingException e)
		{
			LOG.error("Unable to create quote", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_CREATE_ERROR, null);
			model.addAttribute("quoteMessage", false);
			return BcfcommonsaddonControllerConstants.Views.Pages.Booking.quoteMessageResponse;
		}
	}

	/**
	 * Removes all coupons from the client cart. To be updated in a future release.
	 *
	 * @param redirectModel
	 */
	protected void removeCoupons(final RedirectAttributes redirectModel)
	{
		final List<VoucherData> vouchers = voucherFacade.getVouchersForCart();

		for (final VoucherData voucher : vouchers)
		{
			try
			{
				voucherFacade.releaseVoucher(voucher.getCode());
			}
			catch (final VoucherOperationException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.voucher.release.error",
						new Object[]
								{ voucher.getCode() });
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * Adds discount to an existing quote.
	 *
	 * @param quoteCode     Quote to have discounts applied.
	 * @param form          Discount info.
	 * @param redirectModel
	 * @return Mapping redirect to quote page.
	 */
	@RequestMapping(value = "{quoteCode}/discount/apply", method = RequestMethod.POST)
	@RequireHardLogIn
	public String applyDiscountAction(@PathVariable("quoteCode") final String quoteCode, @Valid final QuoteDiscountForm form,
			final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().applyQuoteDiscount(form.getDiscountRate(), form.getDiscountType());
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(String.format("Error applying discount for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.argument.invalid.error", null);
		}
		catch (final SystemException e)
		{
			LOG.error(String.format("Failed to calculate session cart when applying the discount for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.calculation.error", null);
			return REDIRECT_CART_URL;
		}

		return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
	}

	@RequestMapping(value = "/{quoteCode}", method = RequestMethod.GET)
	public String quote(final Model model, final RedirectAttributes redirectModel,
			@PathVariable("quoteCode") final String cartCode,@RequestParam("guestBookingIdentifier") final String guestBookingIdentifier) throws CMSItemNotFoundException
	{

		try
		{
			final BcfGlobalReservationData bcfGlobalReservationData = getQuoteFacade().retrieveCart(cartCode);
			if (Objects.isNull(bcfGlobalReservationData))
			{
				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}

			if(!bcfTravelCartFacadeHelper.isCartGuestIdentifierMatching(cartCode,guestBookingIdentifier)){

				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}

			model.addAttribute("bcfGlobalReservationData", bcfGlobalReservationData);

			final Boolean isASMSession = assistedServiceFacade.getAsmSession() != null;

			final Date quoteExpiryDate = quoteFacade.getQuoteExpiryDate(cartCode);
			model.addAttribute("quoteExpiryDate", quoteExpiryDate);
			model.addAttribute("quoteID", cartCode);
			if (!isASMSession)
			{
				model.addAttribute("isQuoteExpired", quoteFacade.isQuoteExpired(quoteExpiryDate));
			}
			model.addAttribute("isASMSession", isASMSession);

			final CartModel cart = getSessionService().getAttribute(SESSION_CART_PARAMETER_NAME);
			if(Objects.nonNull(cart))
			{
				model.addAttribute("sessionCartCode", cart.getCode());
			}
		}
		catch (final CalculationException e)
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(QUOTES_DETAILS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(QUOTES_DETAILS_PAGE));

		return getViewForPage(model);
	}

	/**
	 * Loads quote edit page.
	 *
	 * @param model
	 * @param quoteCode
	 * @return Mapping to edit page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/{quoteCode}/edit", method = RequestMethod.GET)
	public String showQuoteEdit(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam("isSaveCartCurrentSession") final boolean isSaveCartCurrentSession,final HttpServletRequest request,final RedirectAttributes redirectAttributes, final Model model) throws CMSItemNotFoundException,
			IntegrationException,CommerceCartModificationException
	{

		final CartModel cartModel;
		if (!isSaveCartCurrentSession)
		{
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
			cartModel = assistedServiceFacade.getCartByCode(quoteCode, getSessionService().getAttribute("user"));
			getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, cartModel);
		}

		try
		{
			getQuoteFacade().attachCartToSession(quoteCode);
			final BcfGlobalReservationData bcfGlobalReservationData = getQuoteFacade().retrieveCart(quoteCode);
			final boolean packageJourney = BookingJourneyType.BOOKING_PACKAGE.getCode()
					.equals(bcfGlobalReservationData.getBookingJourneyType());

			final boolean accommodationAndActivityStockReserved=getQuoteFacade().reserveAccommodationAndActivityStocksForQuote(quoteCode);

			if(!accommodationAndActivityStockReserved){

				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, CREATE_QUOTE_FAILED);
				if(packageJourney)
				{
					return REDIRECT_PREFIX + HOME_PAGE_PATH + "?vacation=true";
				}else{
					return REDIRECT_PREFIX + HOME_PAGE_PATH;
				}

			}else
			{

				final boolean sailingStockReserved = getQuoteFacade().reserveSailingStocksForQuote(quoteCode);
				if (packageJourney)
				{
					final GlobalTravelReservationData globalTravelReservationData = bcfGlobalReservationData.getPackageReservations()
							.getPackageReservationDatas().stream().findFirst().get();
					final int journeyRef = Integer
							.valueOf(globalTravelReservationData.getAccommodationReservationData().getJourneyRef());

					final CartModel cart = bcfTravelCommerceCartService
							.getCartForCodeAndSite(quoteCode, baseSiteService.getCurrentBaseSite());
					final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);

					if (CollectionUtils.isEmpty(outBoundEntries))
					{
						bcfTravelCartService.removeSessionCart();
						GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
								CREATE_QUOTE_SAILINGS_REMOVED);
						return REDIRECT_PREFIX + HOME_PAGE_PATH + "?vacation=true";
					}
					final List<AbstractOrderEntryModel> inBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 1);

					final TravelFinderForm travelFinderForm = populateFormFromCart(cart, journeyRef, outBoundEntries, inBoundEntries);
					redirectAttributes.addFlashAttribute("travelFinderForm", travelFinderForm);

					if (!sailingStockReserved)
					{

						final PropertyData accommodationReferenceData = new PropertyData();

						final String accommodationOfferingCode = bcfAccommodationControllerUtil
								.getAccommodationOfferingCode(cart, journeyRef);
						accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);

						final FareFinderForm fareFinderForm = travelFinderForm.getFareFinderForm();
						final AccommodationFinderForm accommodationFinderForm = travelFinderForm.getAccommodationFinderForm();
						final PackageRequestData packageRequestData = populateTransportPackageRequestData(fareFinderForm, request);
						populateAccommodationPackageRequestData(packageRequestData, accommodationFinderForm.getCheckInDateTime(),
								accommodationFinderForm.getCheckOutDateTime(), accommodationFinderForm.getRoomStayCandidates(),
								accommodationOfferingCode,
								Boolean.FALSE, request, accommodationReferenceData, null, null);

						final boolean isOutBoundOpenTicket = outBoundEntries.get(0).getTravelOrderEntryInfo().isOpenTicket();
						final boolean isInBoundOpenTicket = inBoundEntries.get(0).getTravelOrderEntryInfo().isOpenTicket();
						final PackageResponseData packageResponseData = bcfPackageFacade.getPackageResponse(packageRequestData);
						final FareSelectionData fareSelectionData = packageResponseData.getTransportPackageResponse()
								.getFareSearchResponse();
						getQuoteFacade().removeSailingEntries(quoteCode);
						if (Objects.nonNull(fareSelectionData) && CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
						{

							bcfTravelCartFacade.addTransportBundleToCart(
									getAddBundleToCartRequestData(getPricedItinerary(fareSelectionData, true), travelFinderForm,
											isOutBoundOpenTicket), true);
							bcfTravelCartFacade.addTransportBundleToCart(
									getAddBundleToCartRequestData(getPricedItinerary(fareSelectionData, false), travelFinderForm,
											isInBoundOpenTicket), true);
							GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
									CREATE_QUOTE_SAILINGS_REMOVED);
							return REDIRECT_PREFIX + "/package-select-ferry";
						}


					}else{

						return REDIRECT_PREFIX + "/cart/summary";
					}
				}else
					{
						if (!sailingStockReserved)
						{
							GlobalMessages
									.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, CREATE_QUOTE_MODIFIED);
						}
						return REDIRECT_PREFIX + "/alacarte-review";
					}
			}
		}
		catch (final CalculationException e)
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;

		}


		return getViewForPage(model);

	}

	private TravelFinderForm populateFormFromCart(final CartModel cart, final int journeyRef,final List<AbstractOrderEntryModel> outBoundEntries,
			final List<AbstractOrderEntryModel> inBoundEntries){

		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();
		bcfAccommodationControllerUtil.initializeAccommodationFinderForm(accommodationFinderForm, cart, journeyRef);

		final FareFinderForm fareFinderForm = new FareFinderForm();
		bcfAccommodationControllerUtil.initializeFareFinderForm(fareFinderForm, accommodationFinderForm, outBoundEntries,
				inBoundEntries, journeyRef);
		final Date departingDate = TravelDateUtils
				.convertStringDateToDate(accommodationFinderForm.getCheckInDateTime(), BcfcommonsaddonWebConstants.DATE_FORMAT);
		fareFinderForm.setDepartingDateTime(TravelDateUtils.convertDateToStringDate(departingDate,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));

		final Date returnDate = TravelDateUtils
				.convertStringDateToDate(accommodationFinderForm.getCheckOutDateTime(), BcfcommonsaddonWebConstants.DATE_FORMAT);
		fareFinderForm.setReturnDateTime(TravelDateUtils.convertDateToStringDate(returnDate,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));

		final TravelRouteModel outBoundtravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		travelFinderForm.setAccommodationFinderForm(accommodationFinderForm);
		travelFinderForm.setFareFinderForm(fareFinderForm);
		travelFinderForm.setTravelRoute(outBoundtravelRoute.getCode());
		getSessionService().setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, journeyRef);
		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		return travelFinderForm;
	}

	private AddBundleToCartRequestData getAddBundleToCartRequestData(final Pair<PricedItineraryData, ItineraryPricingInfoData> pricedItineraryPair,
			final TravelFinderForm travelFinderForm, final boolean isOpenTicket)
	{
		final PricedItineraryData pricedItineraryData=pricedItineraryPair.getLeft();
		final ItineraryPricingInfoData itineraryPricingInfoData=pricedItineraryPair.getRight();
		final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
		addBundleToCartRequestData.setTripType(TripType.RETURN);

		final List<PassengerTypeQuantityData> passengerTypeQuantityDatas=new ArrayList<>();
		for(final PTCFareBreakdownData pTCFareBreakdownData:itineraryPricingInfoData.getPtcFareBreakdownDatas()){

			final PassengerTypeQuantityData passengerTypeQuantityData=new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(pTCFareBreakdownData.getPassengerTypeQuantity().getPassengerType());
			passengerTypeQuantityData.setQuantity(pTCFareBreakdownData.getPassengerTypeQuantity().getQuantity());
			passengerTypeQuantityDatas.add(passengerTypeQuantityData);

		}
		addBundleToCartRequestData.setPassengerTypes(passengerTypeQuantityDatas);

		final List<VehicleTypeQuantityData> vehicleTypeQuantityDatas = new ArrayList<>();

		for (final VehicleFareBreakdownData vehicleFareBreakdownData : itineraryPricingInfoData.getVehicleFareBreakdownDatas())
		{

			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			vehicleTypeQuantityData.setVehicleType(vehicleFareBreakdownData.getVehicleTypeQuantity().getVehicleType());
			vehicleTypeQuantityData.setWeight(vehicleFareBreakdownData.getVehicleTypeQuantity().getWeight());
			vehicleTypeQuantityData.setLength(vehicleFareBreakdownData.getVehicleTypeQuantity().getLength());
			vehicleTypeQuantityData.setHeight(vehicleFareBreakdownData.getVehicleTypeQuantity().getHeight());
			vehicleTypeQuantityData.setCarryingLivestock(vehicleFareBreakdownData.getVehicleTypeQuantity().isCarryingLivestock());
			vehicleTypeQuantityData
					.setVehicleWithSidecarOrTrailer(vehicleFareBreakdownData.getVehicleTypeQuantity().isVehicleWithSidecarOrTrailer());
			vehicleTypeQuantityData.setQty(vehicleFareBreakdownData.getVehicleTypeQuantity().getQty());
			if (vehicleFareBreakdownData.getVehicleTypeQuantity().isCarryingLivestock() && (BcfCoreConstants.UNDER_HEIGHT
					.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode()) || BcfCoreConstants.OVERSIZE
					.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode())))
			{
				vehicleTypeQuantityData.getVehicleType().setCode(
						vehicleTypeQuantityData.getVehicleType().getCode() + BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX);
			}
			vehicleTypeQuantityDatas.add(vehicleTypeQuantityData);
		}
		addBundleToCartRequestData.setVehicleTypes(vehicleTypeQuantityDatas);
		addBundleToCartRequestData
				.setReturnWithDifferentVehicle(travelFinderForm.getFareFinderForm().getReturnWithDifferentVehicle());
		addBundleToCartRequestData.setBundleType(itineraryPricingInfoData.getBundleType());
		if (CollectionUtils.isNotEmpty(travelFinderForm.getFareFinderForm().getAccessibilityRequestDataList()))
		{
			addBundleToCartRequestData.setSpecialServicesData(bcfvoyageaddonComponentControllerUtil
					.getSpecialServices(travelFinderForm.getFareFinderForm().getAccessibilityRequestDataList()));

		}

		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getLargeItemsBreakdownDataList()))
		{
			addBundleToCartRequestData.setLargeItems(itineraryPricingInfoData.getLargeItemsBreakdownDataList());

		}
		addBundleToCartRequestData.setOpenTicket(isOpenTicket);

		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getOtherCharges()))
		{
			addBundleToCartRequestData.setOtherChargeDatas(itineraryPricingInfoData.getOtherCharges());
		}

		if (StringUtils.isNotEmpty(pricedItineraryData.getTransferSailingIdentifier()))
		{
			addBundleToCartRequestData.setTransferSailingIdentifier(pricedItineraryData.getTransferSailingIdentifier());
		}
		final String travelRouteCode = pricedItineraryData.getItinerary().getRoute().getCode();
		final Integer originDestinationRefNumber;
		try
		{
			originDestinationRefNumber =  pricedItineraryData.getOriginDestinationRefNumber();
		}
		catch (final NumberFormatException ex)
		{
			LOG.error("Cannot parse Origin Destination Ref Number to Integer", ex);
			return null;
		}


		final List<AddBundleToCartData> addBundleToCartDatas = new ArrayList<>();
		for (final TravelBundleTemplateData bundleData : itineraryPricingInfoData.getBundleTemplates())
		{
			final BcfAddBundleToCartData addBundleToCart = new BcfAddBundleToCartData();
			bcfDealCartFacade
					.populateAddBundleToCart(bundleData, addBundleToCart, travelRouteCode, true);
			addBundleToCart.setBundleTemplateId(bundleData.getFareProductBundleTemplateId());
			addBundleToCart.setOriginDestinationRefNumber(originDestinationRefNumber);
			addBundleToCart.setTravelRouteCode(travelRouteCode);
			addBundleToCart.setJourneyRefNumber(0);
			if(BooleanUtils.isTrue(travelFinderForm.getFareFinderForm().getCarryingDangerousGoodsInOutbound())){
				addBundleToCart.setCarryingDangerousGoodsInOutbound(true);
			}else if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()))

			{
				addBundleToCart.setCarryingDangerousGoodsInOutbound(itineraryPricingInfoData.getVehicleFareBreakdownDatas().stream().map(vehicleFareBreakdownData->vehicleFareBreakdownData.getVehicleTypeQuantity()).findFirst().get()
							.isCarryingDangerousGoods());
			}
			addBundleToCart.setCarryingDangerousGoodsInReturn(hasDangerousGoodsInbound(travelFinderForm.getFareFinderForm(),itineraryPricingInfoData));
			addBundleToCartDatas.add(addBundleToCart);
		}

		addBundleToCartRequestData.setAddBundleToCartData(addBundleToCartDatas);
		addBundleToCartRequestData.setSalesApplication(bcfSalesApplicationResolverService.getCurrentSalesChannel());
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			addBundleToCartRequestData.setAddCachingKeys(true);
			addBundleToCartRequestData.setAmendmentCart(true);
		}
		else
		{
			addBundleToCartRequestData.setAddCachingKeys(false);
		}

		addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.PLAN);
		addBundleToCartRequestData.setSelectedJourneyRefNumber(0);
		addBundleToCartRequestData.setSelectedOdRefNumber(
				addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber());
		addBundleToCartRequestData.setHoldTimeInSeconds(bcfTravelCartFacadeHelper.getHoldTimeInSeconds(addBundleToCartRequestData.getSalesApplication()));
		return addBundleToCartRequestData;
	}


	private boolean hasDangerousGoodsInbound(final FareFinderForm fareFinderForm, final ItineraryPricingInfoData itineraryPricingInfoData)
	{
		if (BooleanUtils.isTrue(fareFinderForm.getCarryingDangerousGoodsInReturn()))
		{
			return true;
		}
		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()) && !fareFinderForm
				.getReturnWithDifferentVehicle())
		{
			return StreamUtil.safeStream(itineraryPricingInfoData.getVehicleFareBreakdownDatas()).map(vehicleFareBreakdownData->vehicleFareBreakdownData.getVehicleTypeQuantity()).findFirst().get()
					.isCarryingDangerousGoods();
		}
		if (CollectionUtils.isNotEmpty(itineraryPricingInfoData.getVehicleFareBreakdownDatas()) && fareFinderForm
				.getReturnWithDifferentVehicle())
		{
			return StreamUtil.safeStream(itineraryPricingInfoData.getVehicleFareBreakdownDatas()).map(vehicleFareBreakdownData->vehicleFareBreakdownData.getVehicleTypeQuantity()).reduce((a, b) -> b).get()
					.isCarryingDangerousGoods();
		}
		return false;
	}

	private Pair<PricedItineraryData, ItineraryPricingInfoData> getPricedItinerary(final FareSelectionData fareSelectionData, final boolean outbound)
	{
		for(final PricedItineraryData pricedItineraryData:fareSelectionData.getPricedItineraries()){
			if(outbound){
				if(pricedItineraryData.getOriginDestinationRefNumber()!=0){
					continue;
				}
			}else{

				if(pricedItineraryData.getOriginDestinationRefNumber()!=1){
					continue;
				}
			}
			if(CollectionUtils.isNotEmpty(pricedItineraryData.getItineraryPricingInfos())){
				final ItineraryPricingInfoData itineraryPricing=pricedItineraryData.getItineraryPricingInfos().stream().filter(itineraryPricingInfo->itineraryPricingInfo.isSelected()).findAny().orElse(null);

				if(itineraryPricing!=null)
				{
					return Pair.of(pricedItineraryData, itineraryPricing);
				}
			}
		}
		return null;
	}


	protected PackageRequestData populateTransportPackageRequestData(
			final FareFinderForm fareFinderForm, final HttpServletRequest request)
	{
		final PackageRequestData packageRequestData = new PackageRequestData();
		final TransportPackageRequestData transportPackageRequestData = new TransportPackageRequestData();
		transportPackageRequestData.setFareSearchRequest(prepareFareSearchRequestData(fareFinderForm, request));
		packageRequestData.setTransportPackageRequest(transportPackageRequestData);
		return packageRequestData;
	}


	protected String determineNexUrl(final HttpServletRequest request)
	{
		if (travelCheckoutFacade
				.containsLongRoute() && !travelCheckoutFacade.containsDefaultSailing())
		{
			return request.getContextPath() + ANCILLARY_PAGE_URL;
		}
		return request.getContextPath() + "/checkout/multi/payment-method/select-flow";
	}


	protected void fillQuoteForm(final Model model, final AbstractOrderData data)
	{
		if (!model.containsAttribute("quoteForm"))
		{
			final Locale currentLocale = getI18nService().getCurrentLocale();
			final String expirationTimePattern = getMessageSource().getMessage(DATE_FORMAT_KEY, null, currentLocale);

			final QuoteForm quoteForm = new QuoteForm();
			quoteForm.setName(data.getName());
			quoteForm.setDescription(data.getDescription());
			quoteForm.setExpirationTime(
					BCFDateUtils.convertDateToString(data.getExpirationTime(), expirationTimePattern, currentLocale));
			model.addAttribute("quoteForm", quoteForm);
		}
		model.addAttribute("quoteDiscountForm", new QuoteDiscountForm());
	}

	protected void fillVouchers(final Model model)
	{
		model.addAttribute("appliedVouchers", getVoucherFacade().getVouchersForCart());
		if (!model.containsAttribute(VOUCHER_FORM))
		{
			model.addAttribute(VOUCHER_FORM, new VoucherForm());
		}
	}

	protected void setUpdatable(final Model model, final CartData cartData, final boolean updatable)
	{
		for (final OrderEntryData entry : cartData.getEntries())
		{
			entry.setUpdateable(updatable);
		}

		model.addAttribute("disableUpdate", Boolean.valueOf(!updatable));
	}

	protected void setExpirationTimeAttributes(final Model model)
	{
		model.addAttribute("defaultOfferValidityPeriodDays",
				Integer.valueOf(QuoteExpirationTimeUtils.getDefaultOfferValidityPeriodDays()));
		model.addAttribute("minOfferValidityPeriodDays",
				Integer.valueOf(QuoteExpirationTimeUtils.getMinOfferValidityPeriodInDays()));
	}

	protected void prepareQuotePageElements(final Model model, final CartData cartData, final boolean updatable)
	{
		fillQuoteForm(model, cartData);
		fillVouchers(model);
		setUpdatable(model, cartData, updatable);
		loadCommentsShown(model);

		model.addAttribute("savedCartCount", saveCartFacade.getSavedCartsCountForCurrentUser());
		model.addAttribute("quoteCount", quoteFacade.getQuotesCountForCurrentUser());

		setExpirationTimeAttributes(model);
	}

	@RequestMapping(value = "/{quoteCode}/cancel", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			quoteFacade.cancelQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_TEXT_CANCEL_SUCCESS,
					new Object[]
							{ quoteCode });

		}
		catch (final UnknownIdentifierException uie)
		{
			LOG.warn("Attempted to cancel a quote that does not exist or is not visible: " + quoteCode, uie);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_TEXT_NOT_CANCELABLE,
					new Object[]
							{ quoteCode });
		}
		catch (final CommerceQuoteAssignmentException e)
		{
			LOG.warn("Attempted to cancel a quote that is assigned to another user: " + quoteCode, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_EDIT_LOCKED_ERROR, new Object[]
					{ quoteCode, e.getAssignedUser() });
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}

		return REDIRECT_QUOTE_LIST_URL;
	}

	/**
	 * Submit quote to next responsible in the workflow (e.g. from buyer to seller, from sales representative to sales
	 * approver).
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/{quoteCode}/submit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String submitQuote(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam(value = "editMode", defaultValue = "false") final boolean editMode, final QuoteForm quoteForm,
			final BindingResult bindingResult, final RedirectAttributes redirectModel)
	{
		if (validateCart(redirectModel))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SUBMIT_ERROR, null);
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}

		try
		{
			if (editMode)
			{
				final Optional<String> optionalUrl = handleEditModeSubmitQuote(quoteCode, quoteForm, bindingResult, redirectModel);
				if (optionalUrl.isPresent())
				{
					return optionalUrl.get();
				}
			}
			removeCoupons(redirectModel);
			getQuoteFacade().submitQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_SUBMIT_SUCCESS, null);
		}
		catch (final CommerceQuoteAssignmentException cqae)
		{
			LOG.warn("Attempted to submit a quote that is assigned to another user: " + quoteCode, cqae);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_EDIT_LOCKED_ERROR, new Object[]
					{ quoteCode, cqae.getAssignedUser() });
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final IllegalQuoteSubmitException e)
		{
			LOG.warn("Attempt to submit a quote that is not allowed.", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NOT_SUBMITABLE_ERROR);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final QuoteUnderThresholdException e)
		{
			final double quoteThreshold = getQuoteFacade().getQuoteRequestThreshold(quoteCode);
			LOG.warn(String.format("Quote %s under threshold", quoteCode));
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_REJECT_INITIATION_REQUEST,
					new Object[]
							{ getFormattedPriceValue(quoteThreshold) });
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to submit quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SUBMIT_ERROR, null);
			return REDIRECT_PREFIX + ROOT;
		}
		return REDIRECT_QUOTE_LIST_URL;
	}

	/**
	 * Approve a quote from the sales representative
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/{quoteCode}/approve", method = RequestMethod.POST)
	@RequireHardLogIn
	public String approveQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().approveQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "quote.approve.success", null);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to approve quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "quote.approve.error", null);
			return REDIRECT_PREFIX + ROOT;
		}
		return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
	}

	/**
	 * Reject a quote from the sales representative
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/{quoteCode}/reject", method = RequestMethod.POST)
	@RequireHardLogIn
	public String rejectQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().rejectQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "quote.reject.success", null);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to reject quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "quote.reject.error", null);
			return REDIRECT_PREFIX + ROOT;
		}

		return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
	}

	@RequestMapping(value = "/{quoteCode}/requote", method = RequestMethod.POST)
	@RequireHardLogIn
	public String requote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{

		try
		{
			removeCoupons(redirectModel);
			final QuoteData quoteData = getQuoteFacade().requote(quoteCode);

			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteData.getCode()));
		}
		catch (final IllegalQuoteStateException | CannotCloneException | IllegalArgumentException | ModelSavingException e)
		{
			LOG.error("Unable to requote", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_REQUOTE_ERROR, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
	}

	protected Optional<String> handleEditModeSubmitQuote(final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult, final RedirectAttributes redirectModel)
	{
		final boolean isSeller = Functions.isQuoteUserSalesRep();
		final Object validationGroup = isSeller ? QuoteForm.Seller.class : QuoteForm.Buyer.class;

		smartValidator.validate(quoteForm, bindingResult, validationGroup);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					isSeller ? "text.quote.expiration.time.invalid" : "text.quote.name.description.invalid", null);
			return Optional.of(String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode)));
		}

		try
		{
			final CommerceCartMetadata cartMetadata;
			if (isSeller)
			{
				final Optional<Date> expirationTime = Optional.ofNullable(getExpirationDateFromString(quoteForm.getExpirationTime()));
				cartMetadata = CommerceCartMetadataUtils.metadataBuilder().expirationTime(expirationTime)
						.removeExpirationTime(!expirationTime.isPresent()).build();
			}
			else
			{
				cartMetadata = CommerceCartMetadataUtils.metadataBuilder().name(Optional.ofNullable(quoteForm.getName()))
						.description(Optional.ofNullable(quoteForm.getDescription())).build();
			}

			bcfTravelCartFacade.updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid metadata input field(s) for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					isSeller ? "text.quote.expiration.time.invalid" : "text.quote.name.description.invalid", null);
			return Optional.of(String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode)));
		}

		return Optional.empty();
	}

	@RequestMapping(value = "/{quoteCode}/newcart", method = RequestMethod.GET)
	@RequireHardLogIn
	public String newCart(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			removeCoupons(redirectModel);
			final QuoteData quoteData = quoteFacade.newCart();
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_NEWCART_SUCCESS, new Object[]
					{ quoteData.getCode() });
			return REDIRECT_CART_URL;
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error("Unable to sync cart into quote. Illegal argument used to invoke a method", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NEWCART_ERROR, null);
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}
		catch (final SystemException e)
		{
			LOG.error("Unable to save quote while trying to close quote edit mode", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NEWCART_ERROR, null);
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}
	}

	/**
	 * Place an order for the given quote.
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the checkout page.
	 */
	@RequestMapping(value = "/{quoteCode}/checkout", method = RequestMethod.POST)
	@RequireHardLogIn
	public String placeOrder(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().acceptAndPrepareCheckout(quoteCode);
		}
		catch (final CommerceQuoteExpirationTimeException e)
		{
			LOG.warn(String.format("Quote has Expired. Quote Code : [%s]", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_EXPIRED, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn(String.format("Attempted to place order with a quote that does not exist or is not visible: %s", quoteCode), e);
			return REDIRECT_QUOTE_LIST_URL;
		}
		catch (final SystemException e)
		{
			LOG.warn("There was error saving the session cart", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SAVE_CART_ERROR, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		return getCheckoutRedirectUrl();
	}

	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/expiration", method = RequestMethod.POST)
	@RequireHardLogIn
	public ResponseEntity<String> setQuoteExpiration(@PathVariable("quoteCode") final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult)
	{
		smartValidator.validate(quoteForm, bindingResult, QuoteForm.Seller.class);

		if (bindingResult.hasErrors())
		{
			final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
					getI18nService().getCurrentLocale());
			return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
		}

		try
		{
			final Optional<Date> expirationTime = Optional.ofNullable(getExpirationDateFromString(quoteForm.getExpirationTime()));
			final CommerceCartMetadata cartMetadata = CommerceCartMetadataUtils.metadataBuilder().expirationTime(expirationTime)
					.removeExpirationTime(!expirationTime.isPresent()).build();

			bcfTravelCartFacade.updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid expiration time input for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		catch (final IllegalStateException | IllegalQuoteStateException | UnknownIdentifierException | ModelSavingException e)
		{
			LOG.error(String.format("Failed to update expiration time for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Update quote name and description
	 *
	 * @param quoteCode
	 * @param quoteForm
	 * @param bindingResult
	 * @return response entity
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/metadata", method = RequestMethod.POST)
	@RequireHardLogIn
	public ResponseEntity<String> setQuoteMetadata(@PathVariable("quoteCode") final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult)
	{
		smartValidator.validate(quoteForm, bindingResult, QuoteForm.Buyer.class);

		if (bindingResult.hasErrors())
		{
			final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
					getI18nService().getCurrentLocale());
			return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
		}

		try
		{
			final Optional<String> quoteName = Optional.ofNullable(quoteForm.getName());
			final Optional<String> quoteDescription = Optional.ofNullable(quoteForm.getDescription());

			final CommerceCartMetadata cartMetadata = CommerceCartMetadataUtils.metadataBuilder().name(quoteName)
					.description(quoteDescription).build();

			bcfTravelCartFacade.updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid metadata input for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException e)
		{
			LOG.error(String.format("Failed to update metadata for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	protected Date getExpirationDateFromString(final String expirationTime)
	{
		final Locale currentLocale = getI18nService().getCurrentLocale();
		final String expirationTimePattern = getMessageSource().getMessage(DATE_FORMAT_KEY, null, currentLocale);

		return BCFDateUtils.convertStringToDate(expirationTime, expirationTimePattern, currentLocale);
	}

	/**
	 * Add a quote comment to a given quote.
	 *
	 * @param comment
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	@RequireHardLogIn
	public ResponseEntity<String> addQuoteComment(@RequestParam("comment") final String comment,
			final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().addComment(comment);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.confirmation.quote.comment.added");
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn("Attempted to add a comment that is invalid");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/entry/comment", method = RequestMethod.POST)
	@RequireHardLogIn
	public ResponseEntity<String> addQuoteEntryComment(@RequestParam("entryNumber") final long entryNumber,
			@RequestParam("comment") final String comment, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().addEntryComment(entryNumber, comment);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.confirmation.quote.comment.added");
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn("Attempted to add an entry comment that is invalid");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	protected void sortComments(final CartData cartData)
	{
		if (cartData != null)
		{
			setCartDataComments(cartData);

			if (CollectionUtils.isNotEmpty(cartData.getEntries()))
			{
				for (final OrderEntryData orderEntry : cartData.getEntries())
				{
					setOrderEntryComments(orderEntry);
				}
			}
		}
	}

	private void setOrderEntryComments(final OrderEntryData orderEntry)
	{
		if (CollectionUtils.isNotEmpty(orderEntry.getComments()))
		{
			final List<CommentData> sortedEntryComments = orderEntry.getComments().stream()
					.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
					.collect(Collectors.toList());

			orderEntry.setComments(sortedEntryComments);
		}
		else if (orderEntry.getProduct() != null && orderEntry.getProduct().getMultidimensional() != null
				&& Boolean.TRUE.equals(orderEntry.getProduct().getMultidimensional())
				&& CollectionUtils.isNotEmpty(orderEntry.getEntries()))
		{
			for (final OrderEntryData multiDOrderEntry : orderEntry.getEntries())
			{
				if (CollectionUtils.isNotEmpty(multiDOrderEntry.getComments()))
				{
					final List<CommentData> sortedMultiDOrderEntryComments = multiDOrderEntry.getComments().stream()
							.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
							.collect(Collectors.toList());

					multiDOrderEntry.setComments(sortedMultiDOrderEntryComments);
				}
			}
		}
	}

	private void setCartDataComments(final CartData cartData)
	{
		if (CollectionUtils.isNotEmpty(cartData.getComments()))
		{
			final List<CommentData> sortedComments = cartData.getComments().stream()
					.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
					.collect(Collectors.toList());
			cartData.setComments(sortedComments);
		}
	}

	protected void loadCommentsShown(final Model model)
	{
		final int commentsShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_COMMENTS, 5);
		model.addAttribute("commentsShown", Integer.valueOf(commentsShown));
	}

	/**
	 * Set allowed actions for a given quote on model.
	 *
	 * @param model     the MVC model
	 * @param quoteCode the quote to be checked.
	 */
	protected void setAllowedActions(final Model model, final String quoteCode)
	{
		final Set<QuoteAction> actionSet = getQuoteFacade().getAllowedActions(quoteCode);

		if (actionSet != null)
		{
			final Map<String, Boolean> actionsMap = actionSet.stream()
					.collect(Collectors.toMap(QuoteAction::getCode, v -> Boolean.TRUE));
			model.addAttribute(ALLOWED_ACTIONS, actionsMap);
		}
	}

	@ExceptionHandler(IllegalQuoteStateException.class)
	public String handleIllegalQuoteStateException(final IllegalQuoteStateException exception, final HttpServletRequest request)
	{
		final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);

		LOG.warn("Invalid quote state for performed action.", exception);

		final String statusMessageKey = String.format("text.account.quote.status.display.%s", exception.getQuoteState());
		final String actionMessageKey = String.format("text.account.quote.action.display.%s", exception.getQuoteAction());

		GlobalMessages.addFlashMessage(currentFlashScope, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.quote.illegal.state.error",
				new Object[]
						{ getMessageSource().getMessage(actionMessageKey, null, getI18nService().getCurrentLocale()),
								exception.getQuoteCode(),
								getMessageSource().getMessage(statusMessageKey, null, getI18nService().getCurrentLocale()) });

		return REDIRECT_QUOTE_LIST_URL;
	}

	protected boolean validateCart(final RedirectAttributes redirectModel)
	{
		//Validate the cart
		List<CartModificationData> modifications = new ArrayList<>();
		try
		{
			modifications = bcfTravelCartFacade.validateCartData();
		}
		catch (final CommerceCartModificationException e)
		{

		}
		if (!modifications.isEmpty())
		{
			redirectModel.addFlashAttribute("validationData", modifications);

			// Invalid cart. Bounce back to the cart page.
			return true;
		}
		return false;
	}

	/**
	 * Get formatted monetary value with currency symbol
	 *
	 * @param value the value to be formatted
	 * @return formatted threshold string
	 */
	protected String getFormattedPriceValue(final double value)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(value), getCurrentCurrency().getIsocode())
				.getFormattedValue();
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	protected BcfQuoteFacade getQuoteFacade()
	{
		return quoteFacade;
	}

	protected VoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	public BCFAssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}
}
