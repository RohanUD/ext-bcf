/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonConstants;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.controllers.util.BcfAccommodationControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.impl.DefaultBcfAccommodationFacadeHelper;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.travel.data.BcfTerminalLocationData;
import com.bcf.facades.travel.data.BcfTerminalLocationInfoData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Package Finder Component Controller
 */
@Controller("PackageFinderComponentController")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.PackageFinderComponent)
public class PackageFinderComponentController extends AbstractTravelFinderComponentController
{
	protected static final String SEARCH = "/search";
	private static final Logger LOG = Logger.getLogger(PackageFinderComponentController.class);
	private static final String ARRIVAL_LOCATION = "arrivalLocation";
	private static final String DEPARTURE_LOCATION = "departureLocation";
	private static final String DEPARTING_DATE_TIME = "departingDateTime";
	private static final String RETURN_DATE_TIME = "returnDateTime";
	private static final String TRIP_TYPE = "tripType";
	private static final String CABIN_CLASS = "cabinClass";
	private static final String ROOM = "r";
	private static final String SHOW_CHILD_AGE = "showChildAge";
	private static final String BCF_VACATION_LOCATIONS = "bcfVacationLocations";
	private static final String TRAVEL_ROUTE_DATA = "travelRouteData";
	private static final String DESTINATION_CODE = "destinationCode";
	private static final String DESTINATION_NAME = "destinationName";
	private static final String PACKAGE_HOTEL_URL = "packageHotelUrl";


	@Resource(name = "bcfAccommodationControllerUtil")
	private BcfAccommodationControllerUtil bcfAccommodationControllerUtil;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;
	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "transportFacilityFacade")
	private TransportFacilityFacade transportFacilityFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{
		TravelFinderForm travelFinderForm = null;

		if (model.containsAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM))
		{
			travelFinderForm = (TravelFinderForm) model.asMap().get(BcfFacadesConstants.TRAVEL_FINDER_FORM);
			travelFinderForm.setOnlyAccommodationChange(false);

		}

		if (travelFinderForm == null && (request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM) != null
				&& request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) != null))
		{
			travelFinderForm = new TravelFinderForm();
			travelFinderForm.setOnlyAccommodationChange(false);

			travelFinderForm.setFareFinderForm((FareFinderForm) request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM));
			travelFinderForm.setAccommodationFinderForm(
					(AccommodationFinderForm) request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM));
			verifyAndUpdateDates(travelFinderForm);
			request.setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}

		if (travelFinderForm == null)
		{
			travelFinderForm = initializeTravelFinderForm(model);
			model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}
		else
		{
			if (!model.containsAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM)
					&& request.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) != null)
			{
				model.addAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT,
						request.getAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT));

				travelFinderForm = (TravelFinderForm) request.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);

				model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
			}
		}

		if (getSessionService().getAttribute(PACKAGE_HOTEL_URL) != null)
		{
			travelFinderForm.setReferer(getSessionService().getAttribute(PACKAGE_HOTEL_URL));
			getSessionService().removeAttribute(PACKAGE_HOTEL_URL);
		}

		final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> bcfVacationLocations = getBcfVacationLocations();

		model.addAttribute(BCF_VACATION_LOCATIONS, bcfVacationLocations);

		final String destinationCode = request.getParameter(DESTINATION_CODE);

		if (StringUtils.isNotBlank(destinationCode))
		{

			final List<TravelRouteData> travelRouteData = bcfTravelRouteFacade
					.getTravelRoutesForAccommodationLocation(destinationCode);
			final String destinationName = getDestinationName(bcfVacationLocations, destinationCode);

			model.addAttribute(TRAVEL_ROUTE_DATA, travelRouteData);
			model.addAttribute(DESTINATION_CODE, destinationCode);
			model.addAttribute(DESTINATION_NAME, destinationName);
			model.addAttribute(HIDE_FINDER_TITLE, Boolean.TRUE);

			travelFinderForm.getAccommodationFinderForm().setDestinationLocation(destinationCode);
			travelFinderForm.getAccommodationFinderForm().setSuggestionType("AIRPORTGROUP");


			getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}

		model.addAttribute(BcfstorefrontaddonWebConstants.MODIFY, Boolean.valueOf(request.getParameter(BcfstorefrontaddonWebConstants.MODIFY)));
		model.addAttribute(SHOW_COMPONENT, Boolean.TRUE);
		model.addAttribute(SHOW_CHILD_AGE, Boolean.TRUE);
		model.addAttribute(BCF_VACATION_LOCATIONS, getBcfVacationLocations());
		getSessionService().removeAttribute(BcfFacadesConstants.DEPARTURE_LOCATION);
		getSessionService().removeAttribute(BcfFacadesConstants.ARRIVAL_LOCATION);
	}

	private String getDestinationName(final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> bcfVacationLocations,
			final String destinationCode)
	{
		String destinationName = "";

		for (final Map.Entry<BcfTerminalLocationData, List<BcfTerminalLocationData>> entries : bcfVacationLocations.entrySet())
		{
			final Optional<BcfTerminalLocationData> matchedLocation = entries.getValue().stream()
					.filter(depLocation -> depLocation != null && depLocation.getCode().equalsIgnoreCase(destinationCode)).findAny();

			if (matchedLocation.isPresent())
			{
				destinationName = matchedLocation.get().getName();
				break;
			}
		}
		return destinationName;
	}


	/**
	 * Gets the bcf vacation locations.
	 *
	 * @return the bcf vacation locations
	 */
	protected Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> getBcfVacationLocations()
	{
		final List<BcfTerminalLocationInfoData> bcfVacationLocations = bcfTravelLocationFacade.getBcfVacationLocations();
		final Map<BcfTerminalLocationData, List<BcfTerminalLocationData>> geoAreaVacationLocationsMap = new HashMap<>();
		for (final BcfTerminalLocationInfoData bcfVacationLocation : bcfVacationLocations)
		{
			final BcfTerminalLocationData cityData = bcfVacationLocation.getCity();
			final BcfTerminalLocationData geoAreaData = bcfVacationLocation.getGeographicalArea();
			if (geoAreaData != null)
			{
				final Optional<BcfTerminalLocationData> optionalGeoAreaData = geoAreaVacationLocationsMap.keySet().stream()
						.filter(geoArea -> StringUtils.equals(geoAreaData.getCode(), geoArea.getCode())).findFirst();
				final BcfTerminalLocationData uniqueGeoAreaData = optionalGeoAreaData.isPresent() ? optionalGeoAreaData.get()
						: geoAreaData;

				final List<BcfTerminalLocationData> cityDatas = Objects.nonNull(geoAreaVacationLocationsMap.get(uniqueGeoAreaData))
						? geoAreaVacationLocationsMap.get(uniqueGeoAreaData)
						: new ArrayList<>();
				cityDatas.add(cityData);
				geoAreaVacationLocationsMap.put(uniqueGeoAreaData, cityDatas);
			}

		}

		return bcfTravelLocationFacade.getSortedVacationLocationMap(geoAreaVacationLocationsMap);
	}


	@ResponseBody
	@RequestMapping(value =
			{ "/travelroutes-location" }, method =
			{ RequestMethod.POST, RequestMethod.GET }, produces =
			{ "application/json" })
	public List<TravelRouteData> getTravelRoutesForLocation(@RequestParam(value = "locationCode") final String locationCode)
	{
		return bcfTravelRouteFacade.getTravelRoutesForAccommodationLocation(locationCode);
	}

	@RequestMapping(value = SEARCH, method = RequestMethod.POST)
	public String performSearch(
			@Valid @ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult, final RedirectAttributes redirectModel, final Model model)
	{
		setJourneyTypeInSession(travelFinderForm);
		initializeDefaultVehicle(travelFinderForm);
		bcfAccommodationControllerUtil.validateForms(travelFinderForm, bindingResult);

		final boolean hasErrorFlag = bindingResult.hasErrors();

		if (hasErrorFlag)
		{
			redirectModel.addFlashAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
			redirectModel.addFlashAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + "/?vacation=true";
		}
		initializeForms(travelFinderForm);

		model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);

		if (!travelFinderForm.getAccommodationFinderForm().isModify() && !travelCartFacade.isAmendmentCart())
		{
			travelCartFacade.deleteCurrentCart();
		}

		if (travelCartFacade.isAmendmentCart())
		{
			final int journeyRef = sessionService.getAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM);
			final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);
			final TravelRouteModel outBoundTravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
			final AccommodationFinderForm existingAccommodationFinderForm = new AccommodationFinderForm();
			bcfAccommodationControllerUtil
					.initializeAccommodationFinderForm(existingAccommodationFinderForm, bcfTravelCartService.getSessionCart(),
							journeyRef);
			travelFinderForm.setOnlyAccommodationChange(bcfAccommodationControllerUtil
					.isOnlyAccommodationChange(travelFinderForm.getAccommodationFinderForm(), existingAccommodationFinderForm,
							outBoundTravelRoute.getCode(), travelFinderForm.getTravelRoute()));
		}

		final String urlParameters = buildUrlParameters(travelFinderForm.getFareFinderForm(),
				travelFinderForm.getAccommodationFinderForm());

		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);

		final String redirectUrl = REDIRECT_PREFIX + BcfcommonsaddonWebConstants.PACKAGE_LISTING_PATH;

		return urlParameters.isEmpty() ? redirectUrl : redirectUrl + urlParameters;
	}

	private void initializeDefaultVehicle(final TravelFinderForm travelFinderForm)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypeQuantityDataOutBound = new VehicleTypeQuantityData();
		final VehicleTypeQuantityData vehicleTypeQuantityDataInBound = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeData.setCategory(StringUtils.EMPTY);
		vehicleTypeQuantityDataOutBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityDataInBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataOutBound);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataInBound);
		travelFinderForm.getFareFinderForm().setReturnWithDifferentVehicle(false);
		travelFinderForm.getFareFinderForm().setVehicleInfo(vehicleTypeQuantityList);
		travelFinderForm.getFareFinderForm().getVehicleInfo().forEach(vehicleInformation -> {
			vehicleInformation.getVehicleType().setCategory(BcfcommonsaddonConstants.VEHICLE_CATEGORY_STANDARD);
			vehicleInformation.getVehicleType().setCode(BcfcommonsaddonConstants.VEHICLE_CODE_UH);
			vehicleInformation.setHeight(BcfcommonsaddonConstants.VEHICLE_HEIGHT);
			vehicleInformation.setLength(BcfcommonsaddonConstants.VEHICLE_LENGTH);
			vehicleInformation.setWidth(BcfcommonsaddonConstants.VEHICLE_WIDTH);
			vehicleInformation.setQty(1);
			vehicleInformation.setTravelRouteCode(travelFinderForm.getTravelRoute());
		});
	}



	@Override
	protected void setJourneyTypeInSession(final TravelFinderForm travelFinderForm)
	{
		if (travelFinderForm.getAccommodationFinderForm().getCheckInDateTime()
				.equals(travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime()))
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
		}
		else
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
	}

	protected String buildUrlParameters(final FareFinderForm fareFinderForm, final AccommodationFinderForm accommodationFinderForm)
	{
		final Map<String, String> urlParameters = new HashMap<>();

		//--------FareFinderForm Fields---------//
		urlParameters.put(DEPARTURE_LOCATION, fareFinderForm.getDepartureLocation());
		urlParameters.put(ARRIVAL_LOCATION, fareFinderForm.getArrivalLocation());
		urlParameters.put(DEPARTING_DATE_TIME, fareFinderForm.getDepartingDateTime());
		urlParameters.put(TRIP_TYPE, fareFinderForm.getTripType());
		if (fareFinderForm.getTripType().equalsIgnoreCase(TripType.RETURN.toString()))
		{
			urlParameters.put(RETURN_DATE_TIME, fareFinderForm.getReturnDateTime());
		}
		urlParameters.put(CABIN_CLASS, fareFinderForm.getCabinClass());

		//--------AccommodationFinderForm Fields---------//
		urlParameters.put(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE,
				accommodationFinderForm.getCheckInDateTime());
		urlParameters.put(TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE,
				accommodationFinderForm.getCheckOutDateTime());
		urlParameters.put(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS,
				String.valueOf(accommodationFinderForm.getNumberOfRooms()));

		if (accommodationFinderForm.isModify())
		{
			urlParameters.put(DefaultBcfAccommodationFacadeHelper.MODIFY, "true");
		}

		try
		{
			final int numberOfRooms = accommodationFinderForm.getNumberOfRooms();
			for (int i = 0; i < numberOfRooms; i++)
			{
				final String result = getPassengersUrlParameters(accommodationFinderForm, i);
				urlParameters.put(ROOM + i, result);
			}
		}
		catch (final NumberFormatException e)
		{
			LOG.error("Cannot parse number of rooms string to integer" + e.getClass().getName() + " : " + e.getMessage());
			LOG.debug(e);

			return REDIRECT_PREFIX + "/";
		}

		return "?" + urlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "");
	}



	@Override
	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<String> codes = Arrays.asList(BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_ADULT,
				BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_CHILD);
		final List<PassengerTypeData> sortedPassengerTypes = getTravellerSortStrategy()
				.sortPassengerTypes(getPassengerTypeFacade().getPassengerTypesForCodes(codes));
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}
}
