/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.BookingActionRequestData;
import de.hybris.platform.commercefacades.travel.BookingActionResponseData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.facades.accommodation.impl.DefaultAccommodationOfferingCustomerReviewFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.services.DealBundleTemplateService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationAvailabilityForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationReviewForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AddRequestForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationAvailabilityValidator;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.RefundReasonCodeForm;
import com.bcf.bcfcommonsaddon.validators.BCFGoodWillRefundValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractBookingDetailsComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractBookingDetailsComponentModel;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFGoodWillRefundFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.impl.DefaultBcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.order.BcfOrderFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller("TravelBookingDetailsComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.TravelBookingDetailsComponent)
public class TravelBookingDetailsComponentController extends AbstractBookingDetailsComponentController
{
	private static final Logger LOG = Logger.getLogger(TravelBookingDetailsComponentController.class);
	private static final String GLOBAL_RESERVATION_DATA = "globalReservationData";
	private static final String ADD_REQUEST_FORM = "addRequestForm";
	private static final String ACCOMMODATION_QUANTITY = "accommodationsQuantity";
	private static final String GUEST_QUANTITY = "guestQuantity";
	private static final String ERROR_ADD_ROOM_ORDER_CODE = "error.page.bookingdetails.add.room.orderCode";
	private static final int DEFAULT_ACCOMMODATION_QUANTITY = 1;
	private static final String REFUND_REASON_CODE_DATA = "refundReasonCodeData";
	private static final String REFUND_REASON_CODE_FORM = "refundReasonCodeForm";
	private static final String GOODWILL_REFUND_PROCESSING_ERROR = "goodwill.refund.processing.error";
	private static final String MANAGE_BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String IS_WAITLISTED = "isWaitlisted";
	private static final String BALANCE_AMOUNT_TO_PAY = "balanceAmountToPay";
	private static final String BOOKING_MARKERS = "bookingMarkers";

	@Resource(name = "accommodationOfferingCustomerReviewFacade")
	private DefaultAccommodationOfferingCustomerReviewFacade accommodationOfferingCustomerReviewFacade;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "dealBundleTemplateService")
	private DealBundleTemplateService dealBundleTemplateService;

	@Resource(name = "accommodationAvailabilityValidator")
	private AccommodationAvailabilityValidator accommodationAvailabilityValidator;

	@Resource(name = "bcfGoodWillRefundFacade")
	private BCFGoodWillRefundFacade bcfGoodWillRefundFacade;

	@Resource(name = "bcfGoodWillRefundValidator")
	private BCFGoodWillRefundValidator bcfGoodWillRefundValidator;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "userFacade")
	private DefaultBcfUserFacade userFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AbstractBookingDetailsComponentModel component)
	{
		final String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);

		if (assistedServiceFacade.isAssistedServiceAgentLoggedIn() && Objects.nonNull(order))
		{
			final AsmOrderViewData asmOrderData = bcfTravelBookingFacade.getASMOrderData(order);
			model.addAttribute("asmOrderData", asmOrderData);
			model.addAttribute("asmLoggedIn", Boolean.TRUE);
			final Boolean northernRoute = ChangeAndCancellationFeeCalculationHelper.isNorthernRoute(order.getEntries());
			model.addAttribute("northernRoute", northernRoute);
			final Map<String, String> userRoles = userFacade.getUserRoles();
			model.addAttribute("userRoles", userRoles);
		}

		final List<String> bookingRefs = bcfOrderFacade.findBookingRefForOrder(order);

		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getbcfGlobalReservationDataList(order);

		model.addAttribute("bcfGlobalReservationData", getVacationBookingDetails(order, bcfGlobalReservationData, model));

		final boolean isPackageBookingJourney =
				getBookingFacade().checkBookingJourneyType(bookingReference, Arrays
						.asList(BookingJourneyType.BOOKING_PACKAGE, BookingJourneyType.BOOKING_TRANSPORT_ACCOMMODATION,
								BookingJourneyType.BOOKING_ALACARTE, BookingJourneyType.BOOKING_ACTIVITY_ONLY));
		if (isPackageBookingJourney)
		{
			try
			{
				final GlobalTravelReservationData globalReservationData = getBookingFacade()
						.getGlobalTravelReservationData(bookingReference);
				model.addAttribute(GLOBAL_RESERVATION_DATA, globalReservationData);

				handleDisruptedReservation(model, bookingReference, globalReservationData.getReservationData());
				// BookingActions
				final BookingActionRequestData transportBookingActionRequest = createTransportBookingActionRequest(bookingReference);
				final BookingActionRequestData accommodationBookingActionRequest = createAccommodationBookingActionRequest(
						bookingReference);
				final BookingActionRequestData globalBookingActionRequest = createGlobalBookingActionRequest(bookingReference);
				final BookingActionResponseData bookingActionResponse = getActionFacade().getTravelBookingAction(
						transportBookingActionRequest, accommodationBookingActionRequest, globalBookingActionRequest,
						globalReservationData);
				model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_ACTION_RESPONSE, bookingActionResponse);
				model.addAttribute(ADD_REQUEST_FORM, new AddRequestForm());
				model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_REVIEW_FORM, new AccommodationReviewForm());
				model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_ORDER,
						getPackageFacade().isPackageInOrder(bookingReference));
				model.addAttribute(BcfcommonsaddonWebConstants.IS_DEAL_IN_ORDER,
						dealBundleTemplateService.isDealBundleOrder(bookingReference));
				model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_BOOKING_JOURNEY,
						getBookingFacade()
								.checkBookingJourneyType(bookingReference, Arrays.asList(BookingJourneyType.BOOKING_PACKAGE)));
				if (Objects.nonNull(globalReservationData.getAccommodationReservationData()))
				{
					final PriceData amountPaid = getTravelCommercePriceFacade()
							.getPaidAmount(globalReservationData.getAccommodationReservationData());
					model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_PAID, amountPaid);
					final PriceData dueAmount = getTravelCommercePriceFacade()
							.getDueAmount(globalReservationData.getAccommodationReservationData(), amountPaid);
					if (Objects.nonNull(dueAmount))
					{
						model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_DUE, dueAmount);
					}
				}
				//TODO: review this to make sure the logic is correct. Should review logic be part of the booking action?
				if (globalReservationData.getAccommodationReservationData() != null
						&& globalReservationData.getAccommodationReservationData().getAccommodationReference() != null)
				{
					model.addAttribute(BcfaccommodationsaddonWebConstants.SUBMITTED_REVIEWS,
							accommodationOfferingCustomerReviewFacade.retrieveCustomerReviewByBooking(bookingReference,
									globalReservationData.getAccommodationReservationData().getAccommodationReference()
											.getAccommodationOfferingCode()));
				}
				model.addAttribute(REFUND_REASON_CODE_DATA, bcfGoodWillRefundFacade.getAllRefundReasonCodes());
				if (!model.containsAttribute(REFUND_REASON_CODE_FORM))
				{
					model.addAttribute(REFUND_REASON_CODE_FORM, new RefundReasonCodeForm());
				}

				model.addAttribute("isNonTransportBooking", bcfControllerUtil.isNonTransportBooking(bookingReference));
				model.addAttribute(BcfaccommodationsaddonWebConstants.ORDER_CODE, bookingReference);
				model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
						getBookingFacade().getBookingJourneyType(bookingReference));
				model.addAttribute(IS_WAITLISTED, bcfOrderFacade.isWaitlistedOrder(order));
				model.addAttribute(BALANCE_AMOUNT_TO_PAY, (order.getAmountToPay() > 0 ? true : false));
				final boolean isAgentLoggedIn = assistedServiceFacade.isAssistedServiceAgentLoggedIn();
				model.addAttribute("showSupplierComments", isAgentLoggedIn);
				model.addAttribute("showAvailabilityStatus", isAgentLoggedIn);
				model.addAttribute("isOnRequestBooking",
						BcfCoreConstants.PACKAGE_ON_REQUEST.equals(bookingFacade.getPackageAvailabilityStatus(bookingReference)));
				final AssistedServiceSession assistedServiceSession = assistedServiceFacade.getAsmSession();
				if (Objects.nonNull(assistedServiceSession) && Objects.nonNull(assistedServiceSession.getAgent()))
				{
					model.addAttribute(BOOKING_MARKERS, bcfOrderFacade.getEntryMarkers(order));
				}
			}
			catch (final JaloObjectNoLongerValidException ex)
			{
				model.addAttribute(PAGE_NOT_AVAILABLE, BcfstorefrontaddonWebConstants.PAGE_TEMPORARY_NOT_AVAILABLE);
			}
		}
		model.addAttribute("displayTravelBookingDetailsComponent", isPackageBookingJourney);
	}


	@RequestMapping(value = "/reset-booking-markers/{bookingRef}", method = RequestMethod.POST, produces = "application/json")
	public String resetMarkers(@PathVariable final String bookingRef, final Model model)
	{
		final AbstractOrderModel order = bookingFacade
				.getOrderFromBookingReferenceCode(bookingFacade.getOrderCodeForEBookingCode(bookingRef));
		model.addAttribute(BcfcommonsaddonWebConstants.RESET_BOOKING_MARKERS, bcfOrderFacade.updateEntryMarkers(order));
		return BcfcommonsaddonControllerConstants.Views.Pages.Booking.resetMarkersResponse;
	}

	//Getting booking details from eBooking for Vacations.
	private BcfGlobalReservationData getVacationBookingDetails(final AbstractOrderModel order,
			final BcfGlobalReservationData bcfGlobalReservationData, final Model model)
	{
		final List<String> bookingRefs = bcfOrderFacade.findBookingRefForOrder(order);
		try
		{
			final ReservationDataList reservationDataList = searchBookingFacade.getVacationBookingsFromEBooking(bookingRefs, order);
			if (bcfGlobalReservationData.getPackageReservations() != null)
			{
				for (final GlobalTravelReservationData globalTravelReservationData : bcfGlobalReservationData.getPackageReservations()
						.getPackageReservationDatas())
				{
					globalTravelReservationData.setReservationData(reservationDataList.getReservationDatas().get(0));
					model.addAttribute("isActivityPresent",
							Objects.nonNull(globalTravelReservationData.getActivityReservations()) ? true : false);
				}
			}
			else
			{
				bcfGlobalReservationData.setTransportReservations(reservationDataList);
			}
		}
		catch (final IntegrationException e)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, true);
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE,
					e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			LOG.error(e);
		}
		return bcfGlobalReservationData;
	}

	/**
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/add-room-options", method = RequestMethod.POST, produces =
			{ "application/json" })
	public String getAddRoomOptions(@ModelAttribute(value = "orderCode") final String orderCode, final Model model)
	{
		final AccommodationReservationData accommodationReservationData = reservationFacade
				.getAccommodationReservationSummary(orderCode);
		if (Objects.isNull(accommodationReservationData))
		{
			model.addAttribute(BcfcommonsaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			model.addAttribute(BcfcommonsaddonWebConstants.ERROR_MESSAGE, ERROR_ADD_ROOM_ORDER_CODE);
			return BcfcommonsaddonControllerConstants.Views.Pages.Booking.AddAccommodationRoomJsonResponse;
		}

		final AccommodationAvailabilityForm accommodationAvailabilityForm = initializeAccommodationAvailabilityForm(
				accommodationReservationData);
		model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM, accommodationAvailabilityForm);
		model.addAttribute(ACCOMMODATION_QUANTITY, populateAccommodationsQuantity(accommodationReservationData));
		model.addAttribute(GUEST_QUANTITY, populatePassengersQuantity());
		model.addAttribute(BcfaccommodationsaddonWebConstants.ORDER_CODE, orderCode);
		model.addAttribute(BcfcommonsaddonWebConstants.HAS_ERROR_FLAG, Boolean.FALSE);

		return BcfcommonsaddonControllerConstants.Views.Pages.Booking.AddAccommodationRoomJsonResponse;
	}


	@RequestMapping(value = "/validate-accommodation-availability-form/{orderCode}", method = RequestMethod.POST, produces =
			{ "application/json" })
	public String validateAccommodationAvailabilityForm(final AccommodationAvailabilityForm accommodationAvailabilityForm,
			@PathVariable final String orderCode, final BindingResult bindingResult, final Model model)
	{
		accommodationAvailabilityValidator.validateGuestsQuantity(accommodationAvailabilityForm.getNumberOfRooms(),
				accommodationAvailabilityForm.getRoomStayCandidates(), bindingResult);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);
		if (hasErrorFlag)
		{
			model.addAttribute(BcfcommonsaddonWebConstants.ERROR_MESSAGE, bindingResult.getFieldErrors().get(0).getCode());
		}
		return BcfcommonsaddonControllerConstants.Views.Pages.Booking.AddAccommodationRoomPackageFormValidationJsonResponse;
	}


	protected List<String> populatePassengersQuantity()
	{
		final List<String> guestsQuantity = new ArrayList<>();
		final int maxGuestQuantity = getConfigurationService().getConfiguration().getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
		for (int i = 0; i <= maxGuestQuantity; i++)
		{
			guestsQuantity.add(String.valueOf(i));
		}
		return guestsQuantity;
	}

	protected List<String> populateAccommodationsQuantity(final AccommodationReservationData accommodationReservationData)
	{
		final List<String> accommodationQuantity = new ArrayList<>();
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

		final int roomsBooked = CollectionUtils.size(accommodationReservationData.getRoomStays());


		for (int i = 1; i <= (maxAccommodationsQuantity - roomsBooked); i++)
		{
			accommodationQuantity.add(i + " ");

		}
		return accommodationQuantity;
	}

	protected AccommodationAvailabilityForm initializeAccommodationAvailabilityForm(
			final AccommodationReservationData accommodationReservationData)
	{
		final AccommodationAvailabilityForm accommodationAvailabilityForm = new AccommodationAvailabilityForm();
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

		final int roomsBooked = CollectionUtils.size(accommodationReservationData.getRoomStays());
		for (int i = roomsBooked; i < maxAccommodationsQuantity; i++)
		{
			final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
			roomStayCandidateData.setRoomStayCandidateRefNumber(i);
			roomStayCandidates.add(roomStayCandidateData);
		}
		accommodationAvailabilityForm.setRoomStayCandidates(roomStayCandidates);
		accommodationAvailabilityForm.setNumberOfRooms(DEFAULT_ACCOMMODATION_QUANTITY);
		return accommodationAvailabilityForm;
	}

	protected RoomStayCandidateData createRoomStayCandidatesData()
	{
		final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
		roomStayCandidateData.setPassengerTypeQuantityList(getPassengerTypeQuantityList());
		for (final PassengerTypeQuantityData passengeTypeQuantityData : roomStayCandidateData.getPassengerTypeQuantityList())
		{
			if (passengeTypeQuantityData.getPassengerType().getCode().equals(TravelfacadesConstants.PASSENGER_TYPE_CODE_ADULT))
			{
				passengeTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_ADULT_QUANTITY);
			}
		}
		return roomStayCandidateData;
	}

	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<PassengerTypeData> sortedPassengerTypes = travellerSortStrategy
				.sortPassengerTypes(passengerTypeFacade.getPassengerTypes());
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}


	protected BookingActionRequestData createGlobalBookingActionRequest(final String bookingReference)
	{
		final BookingActionRequestData bookingActionRequestData = new BookingActionRequestData();

		bookingActionRequestData.setBookingReference(bookingReference);

		final List<ActionTypeOption> requestActions = new ArrayList<>();
		requestActions.add(ActionTypeOption.AMEND_ALACARTE_BOOKING);
		requestActions.add(ActionTypeOption.APPLY_GOODWILL_REFUND);
		requestActions.add(ActionTypeOption.CANCEL_BOOKING);
		requestActions.add(ActionTypeOption.AMEND_STANDALONE_ACTIVITY);
		requestActions.add(ActionTypeOption.CANCEL_STANDALONE_ACTIVITY);
		bookingActionRequestData.setRequestActions(requestActions);

		final CustomerData currentCustomer = getCustomerFacade().getCurrentCustomer();
		bookingActionRequestData.setUserId(currentCustomer.getUid());

		return bookingActionRequestData;
	}

	@Override
	protected String getView(final AbstractBookingDetailsComponentModel component)
	{
		final AssistedServiceSession assistedServiceSession = assistedServiceFacade.getAsmSession();
		if (Objects.nonNull(assistedServiceSession) && Objects.nonNull(assistedServiceSession.getAgent()))
		{
			return "addon:" + "/" + getAddonUiExtensionName(component) + "/" + getCmsComponentFolder() + "/asm"
					+ getViewResourceName(component);
		}
		else
		{
			return super.getView(component);
		}
	}
}
