/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.core.constants.BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.ActivityFinderForm;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.ActivityProductFacade;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.activity.search.request.data.ActivitySearchCriteriaData;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.activity.search.response.data.ActivityResponseData;


@Controller
@RequestMapping("/vacations/activities")
public class ActivityListPageController extends TravelAbstractPageController
{
	private static final String ACTIVITY_LISTING_PAGE = "activityListingPage";
	private static final String ACTIVITIES_RESPONSE_DATALIST = "activitiesResponseDataList";
	private static final String ACTIVITY_FINDER_FORM = "activityFinderForm";
	private static final String COMMA = ",";

	@Resource(name = "activityProductSearchFacade")
	private ActivityProductSearchFacade activityProductSearchFacade;

	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getActivitiesList(final ActivityFinderForm activityFinderForm, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final ActivitySearchCriteriaData activitySearchCriteriaData = getActivitySearchCriteriaData(activityFinderForm);
		final ActivitiesResponseData activitiesResponseData = activityProductSearchFacade
				.doSearch(activitySearchCriteriaData, buildGlobalPaginationData(request, ACTIVITY_LISTING_PAGE));
		storePaginationResults(model, activitiesResponseData);
		final List<ActivityResponseData> withAdultPriceActivityResponseData = activitiesResponseData.getActivityResponseData()
				.stream()
				.filter(activityResponseData -> Objects.nonNull(activityResponseData.getAdultPriceValue()))
				.collect(Collectors.toList());
		activitiesResponseData.setActivityResponseData(withAdultPriceActivityResponseData);

		setIsActivityAvailable(activitiesResponseData);

		activitiesResponseData.getFacets().sort((lhs, rhs) -> {
			if (lhs.getCode() == "destination")
			{
				return 1;
			}
			else
			{
				return -1;
			}
		});
		model.addAttribute(ACTIVITIES_RESPONSE_DATALIST, activitiesResponseData);
		model.addAttribute(BcfcommonsaddonWebConstants.IS_DEAL_ACTIVITY, false);
		model.addAttribute(ACTIVITY_FINDER_FORM, activityFinderForm);
		populateChangeActivityRelatedFieldsInModel(activityFinderForm.isChangeActivity(),
				activityFinderForm.getChangeActivityCode(),
				activityFinderForm.getChangeActivityDate(), activityFinderForm.getChangeActivityTime(),
				activityFinderForm.getChangeActivityRef(), model);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ACTIVITY_LISTING_PAGE));
		return getViewForPage(model);
	}

	private ActivitySearchCriteriaData getActivitySearchCriteriaData(final ActivityFinderForm activityFinderForm)
	{
		if (Objects.isNull(activityFinderForm))
		{
			final ActivitySearchCriteriaData activitySearchCriteriaData = new ActivitySearchCriteriaData();
			activitySearchCriteriaData.setCurrentPage(1);
			return activitySearchCriteriaData;
		}

		return createActivitySearchCriteria(activityFinderForm.getSort(), activityFinderForm.getCurrentPage(),
				activityFinderForm.getDestination(), activityFinderForm.getActivityCategoryTypes());
	}

	private ActivitySearchCriteriaData createActivitySearchCriteria(final String sortParam, final int currentPage,
			final String destination, final String categories)
	{
		final ActivitySearchCriteriaData activitySearchCriteriaData = new ActivitySearchCriteriaData();
		activitySearchCriteriaData.setDestination(destination);
		activitySearchCriteriaData.setCurrentPage(currentPage);
		if (StringUtils.isNotBlank(categories))
		{
			activitySearchCriteriaData
					.setActivityCategoryTypes(Arrays.asList(StringUtils.split(categories, COMMA)));
		}
		activitySearchCriteriaData.setSort(sortParam);
		return activitySearchCriteriaData;
	}

	private void setIsActivityAvailable(final ActivitiesResponseData activitiesResponseData)
	{
		activityProductFacade.removeInvalidActivities(activitiesResponseData);

		final Date currentDate = new Date();
		final String currentDateStr = new SimpleDateFormat(BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN).format(currentDate);
		activityProductFacade.setAvailableStatusForActivitiesResponseData(activitiesResponseData,
				BCFDateUtils.convertStringToDate(currentDateStr, SOLR_INDEXER_DATE_PATTERN));
	}

}
