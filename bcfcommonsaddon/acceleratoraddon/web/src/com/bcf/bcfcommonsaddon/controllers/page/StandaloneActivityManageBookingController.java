/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.annotations.CheckRequireLoginForBookingChange;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.misc.TransportManageBookingController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
public class StandaloneActivityManageBookingController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(TransportManageBookingController.class);

	private static final String AMEND_ACTIVITY_FAILED = "text.page.managemybooking.amend.activity.failed";

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@RequestMapping(value = "/manage-booking/amend-activity-booking/{bookingReference}/{activityCode}/{date}", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String modifyBooking(@PathVariable("bookingReference") final String bookingReference,
			@PathVariable("activityCode") final String activityCode, @PathVariable("date") final String date,
			@RequestParam("time") final String time, final Model model,
			final RedirectAttributes redirectAttributes)
	{
		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, false);
		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACTIVITY_FAILED);
			return REDIRECT_PREFIX + "/manage-booking/booking-details/" + bookingReference;
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BcfstorefrontaddonWebConstants.BOOKING_ACTIVITY_ONLY);
		sessionService.setAttribute(BcfCoreConstants.IS_AMEND_ORDER, Boolean.TRUE);
		sessionService.setAttribute(BcfCoreConstants.AMEND_ORDER_CODE, bookingReference);

		return REDIRECT_PREFIX + "/vacations/activities/" + activityCode + "?changeActivity=true&changeActivityCode=" + activityCode
				+ "&date=" + date + "&time=" + time;
	}

	@RequestMapping(value = "/manage-booking/cancel-activity-booking/{bookingReference}/{activityCode}/{originDestRefNum}/{date}", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String cancelActivity(@PathVariable("bookingReference") final String bookingReference,
			@PathVariable("activityCode") final String activityCode, @PathVariable("originDestRefNum") final int originDestRefNum,
			@PathVariable("date") final String date, @RequestParam("time") final String time,
			final Model model,
			final RedirectAttributes redirectAttributes)
	{
		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, false);
		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACTIVITY_FAILED);
			return REDIRECT_PREFIX + "/manage-booking/booking-details/" + bookingReference;
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BcfstorefrontaddonWebConstants.BOOKING_ACTIVITY_ONLY);
		sessionService.setAttribute(BcfCoreConstants.IS_AMEND_ORDER, Boolean.TRUE);
		sessionService.setAttribute(BcfCoreConstants.AMEND_ORDER_CODE, bookingReference);

		return REDIRECT_PREFIX + "/vacations/activities/cancel-activity/" + activityCode + "/" + originDestRefNum + "/" + date
				+ "?time=" + time;
	}

}
