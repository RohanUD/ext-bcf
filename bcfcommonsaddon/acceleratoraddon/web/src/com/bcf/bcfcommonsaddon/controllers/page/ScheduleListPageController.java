/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.date.CalendarData;
import com.bcf.facades.schedules.DailySchedulesData;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.schedules.search.request.data.ScheduleSearchCriteriaData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/routes-fares/schedules/")
public class ScheduleListPageController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(ScheduleListPageController.class);

	private static final String ROUTE_NAMES_PATTERN = "{routeName}";
	private static final String ROUTE_CODE_PATTERN = "{routeCode}";
	private static final String DASH = "-";

	private static final String SCHEDULE_SELECTION_CMS_PAGE = "schedulesListing";
	private static final String DAILY_SCHEDULES = "dailySchedules";

	@Resource(name = "bcfTransportOfferingFacade")
	private BcfTransportOfferingFacade bcfTransportOfferingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;


	@RequestMapping(value = ROUTE_NAMES_PATTERN + "/" + ROUTE_CODE_PATTERN, method = RequestMethod.GET)
	public String getSchedulesList(@PathVariable final String routeName, @PathVariable final String routeCode,
			@RequestParam(value = "scheduleDate", required = false) String scheduleDate,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException, IntegrationException
	{
		final String origin = StringUtils.split(routeCode, DASH).length > 0 ? StringUtils.split(routeCode, DASH)[0] : null;
		final String destination = StringUtils.split(routeCode, DASH).length > 1 ? StringUtils.split(routeCode, DASH)[1] : null;
		String[] arguments = new String[] {};

		if (StringUtils.isNotBlank(origin) && StringUtils.isNotBlank(destination))
		{
			if (StringUtils.isEmpty(scheduleDate))
			{
				final LocalDate ldt = LocalDate.now();
				final DateTimeFormatter dayDateFormatter = DateTimeFormatter.ofPattern(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
				scheduleDate = dayDateFormatter.format(ldt);
			}
			final ScheduleSearchCriteriaData scheduleSearchCriteriaData = prepareScheduleSearchRequestData(origin, destination,
					scheduleDate);
			final DailySchedulesData dailySchedulesData = bcfTransportOfferingFacade
					.searchTransportOfferings(scheduleSearchCriteriaData);
			model.addAttribute(DAILY_SCHEDULES, dailySchedulesData);
			final Map<String, String> minMaxSailingDurations = bcfTransportOfferingFacade
					.getSailingDurations(dailySchedulesData.getScheduleRoutes());
			model.addAttribute("sailingDurations", minMaxSailingDurations);
			final List<Date> scheduleDates = getDatesForTabs(scheduleDate, dailySchedulesData);
			model.addAttribute(BcfvoyageaddonWebConstants.SCHEDULE_DATES, scheduleDates);
			final List<String> scheduleDatesTabLinks = getLinksForTabs(scheduleDates, origin, destination, routeName);
			model.addAttribute("scheduleDatesTabLinks", scheduleDatesTabLinks);
			final DestinationAndOriginForTravelRouteData routeInfo = dailySchedulesData.getTravelRouteInfo();

			final StringBuilder sb = new StringBuilder("/routes-fares/schedules/");
			sb.append(routeInfo.getArrivalRouteName()).append("-").append(routeInfo.getDepartureRouteName());
			sb.append("/").append(routeInfo.getArrivalTerminalCode()).append("-").append(routeInfo.getDepartureTerminalCode());
			sb.append("?scheduleDate=").append(scheduleDate);

			model.addAttribute("switchDirectionUrl", sb.toString());

			arguments = new String[] { routeInfo.getDepartureTerminalName() + "-" + routeInfo.getArrivalTerminalName() };
			sessionService.setAttribute(BcfFacadesConstants.DEPARTURE_LOCATION, origin);
			sessionService.setAttribute(BcfFacadesConstants.ARRIVAL_LOCATION, destination);
		}

		final ContentPageModel contentPageModel = getContentPageForLabelOrId(SCHEDULE_SELECTION_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel, arguments);
		setUpDynamicOGGraphData(model, contentPageModel, request, arguments);
		return getViewForPage(model);
	}

	protected ScheduleSearchCriteriaData prepareScheduleSearchRequestData(final String origin, final String destination,
			final String scheduleDate)
	{
		final ScheduleSearchCriteriaData scheduleSearchCriteriaData = new ScheduleSearchCriteriaData();

		// create OriginDestinationInfoData
		final List<OriginDestinationInfoData> originDestinationInfoData = new ArrayList<>();

		final OriginDestinationInfoData departureInfo = new OriginDestinationInfoData();

		departureInfo.setDepartureLocation(origin);
		departureInfo.setArrivalLocation(destination);
		departureInfo.setDepartureTime(
				TravelDateUtils
						.convertStringDateToDate(scheduleDate,
								BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));

		departureInfo.setReferenceNumber(0);

		originDestinationInfoData.add(departureInfo);

		// set scheduleSearchCriteriaData
		scheduleSearchCriteriaData.setOriginDestinationInfo(originDestinationInfoData);

		return scheduleSearchCriteriaData;
	}

	private List<Date> getDatesForTabs(final String scheduleDate, final DailySchedulesData dailySchedulesData)
	{
		if (Objects.nonNull(dailySchedulesData) && CollectionUtils.isNotEmpty(dailySchedulesData.getScheduleRoutes()))
		{
			final String routeType = dailySchedulesData.getScheduleRoutes().get(0).getRoute().getRouteType();
			if (RouteType.LONG.equals(RouteType.valueOf(routeType)))
			{
				return getDatesForNorthernRoute(scheduleDate, dailySchedulesData);
			}
			else
			{
				return getDatesForTabs(scheduleDate);
			}
		}
		return Collections.emptyList();
	}

	/*
	 *
	 * For northern route, it is possible that the next available and a previous available dates are not in continuation,
	 * so we need to traverse the schedules list to get one.
	 * <b>
	 * If there are no further schedules found, the dates list will simply be empty.
	 *
	 */
	private List<Date> getDatesForNorthernRoute(final String scheduleDateStr, final DailySchedulesData dailySchedulesData)
	{
		final Date selectedDate = BCFDateUtils.convertStringToDate(scheduleDateStr, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final List<CalendarData> scheduleDates = getSchedules(selectedDate, dailySchedulesData);

		String onePreviousDateStr = null;
		String oneNextDateStr = null;
		scheduleDates.sort(Comparator.comparing(CalendarData::getYear));
		scheduleDates.sort(Comparator.comparing(CalendarData::getMonth));
		for (final CalendarData calendarData : scheduleDates)
		{
			if (StringUtils.isNotBlank(onePreviousDateStr) && StringUtils.isNotBlank(oneNextDateStr))
			{
				break;
			}
			calendarData.getDates().sort(Integer::compareTo);
			final String formattedMonth =
					calendarData.getMonth() < 10 ? "0" + calendarData.getMonth() : "" + calendarData.getMonth();
			final int daysCountInMonth = calendarData.getDates().size();
			for (int currentDay = 0; currentDay < daysCountInMonth; currentDay++)
			{
				final int day = calendarData.getDates().get(currentDay);
				final String formattedDay = (day < 10) ? "0" + day : "" + day;
				final String dateStr = formattedMonth + "/" + formattedDay + "/" + calendarData.getYear();

				if (dateStr.equals(scheduleDateStr))
				{
					onePreviousDateStr = getOnePreviousDateStr(scheduleDates, calendarData, currentDay);
					oneNextDateStr = getOneNextDateStr(scheduleDates, calendarData, currentDay);
					break;
				}
			}
		}
		return getDateList(selectedDate, onePreviousDateStr, oneNextDateStr);
	}

	private List<CalendarData> getSchedules(final Date selectedDate, final DailySchedulesData dailySchedulesData)
	{
		final String origin = dailySchedulesData.getTravelRouteInfo().getDepartureTerminalCode();
		final String destination = dailySchedulesData.getTravelRouteInfo().getArrivalTerminalCode();

		final Calendar cal = new GregorianCalendar();
		cal.setTime(selectedDate);

		//to get previous 1 month schedules from the current selected date
		cal.add(Calendar.MONTH, -1);
		final Date startingDate = cal.getTime();

		//to get next 1 month schedules from the current selected date
		cal.add(Calendar.MONTH, 2);
		final Date endingDate = cal.getTime();

		final String startDate = TravelDateUtils.getTimeForDate(startingDate, BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN);
		final String endDate = TravelDateUtils.getTimeForDate(endingDate, BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN);

		final OriginDestinationInfoData originDestinationInfoData = createOrigniDestinationInfo(origin, destination);
		return bcfTransportOfferingFacade
				.getDepartureDates(originDestinationInfoData, startDate, endDate);
	}

	/*
	 * if current day is the first day of the month, get last day of the previous month.Else get previous day of the same month
	 */
	private String getOnePreviousDateStr(final List<CalendarData> departureDates,
			final CalendarData calendarData, final int i)
	{
		if (i == 0)
		{
			final Optional<CalendarData> previousMonth = departureDates.stream()
					.filter(d -> d.getMonth() == (calendarData.getMonth() - 1)).findFirst();
			if (previousMonth.isPresent())
			{
				final CalendarData previousMonthData = previousMonth.get();
				final int daysCount = previousMonthData.getDates().size();
				previousMonthData.getDates().sort(Integer::compareTo);
				final int prevDay = previousMonth.get().getDates().get(daysCount - 1);
				return previousMonthData.getMonth() + "/" + prevDay + "/" + calendarData.getYear();
			}
		}
		else
		{
			return calendarData.getMonth() + "/" + calendarData.getDates().get(i - 1) + "/" + calendarData.getYear();
		}
		return StringUtils.EMPTY;
	}

	/*
	 * if current day is last day of the month, get first day of the next month, else get next day of the current month
	 */
	private String getOneNextDateStr(final List<CalendarData> departureDates,
			final CalendarData calendarData, final int currentDay)
	{
		final int daysCountInMonth = calendarData.getDates().size();

		if (currentDay == (daysCountInMonth - 1))
		{
			final Optional<CalendarData> nextMonth = departureDates.stream()
					.filter(d -> d.getMonth() == (calendarData.getMonth() + 1)).findFirst();
			if (nextMonth.isPresent())
			{
				final CalendarData nextMonthData = nextMonth.get();
				nextMonthData.getDates().sort(Integer::compareTo);
				final int nextDay = nextMonthData.getDates().get(0);
				return nextMonthData.getMonth() + "/" + nextDay + "/" + calendarData.getYear();
			}
		}
		else
		{
			return calendarData.getMonth() + "/" + calendarData.getDates().get(currentDay + 1) + "/" + calendarData.getYear();
		}
		return StringUtils.EMPTY;
	}

	private List<Date> getDateList(final Date selectedDate, final String onePreviousDateStr, final String oneNextDateStr)
	{
		final List<Date> dates = new ArrayList<>();
		if (StringUtils.isNotBlank(onePreviousDateStr))
		{
			dates.add(0, BCFDateUtils.convertStringToDate(onePreviousDateStr, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		}
		dates.add(1, selectedDate);
		if (StringUtils.isNotBlank(oneNextDateStr))
		{
			dates.add(2, BCFDateUtils.convertStringToDate(oneNextDateStr, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		}
		return dates;
	}

	protected List<Date> getDatesForTabs(final String selectedDateString)
	{
		final List<Date> dates = new ArrayList<>();
		final SimpleDateFormat formatter = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

		Date selectedDate = null;
		final Date currentDate = new Date();
		currentDate.setHours(0);
		currentDate.setMinutes(0);
		currentDate.setSeconds(0);
		currentDate.setTime(currentDate.getTime() - currentDate.getTime() % 1000);

		try
		{
			selectedDate = formatter.parse(selectedDateString);
			final Calendar cal = new GregorianCalendar();
			cal.setTime(selectedDate);

			if (TravelDateUtils.isSameDate(selectedDate, currentDate))
			{
				dates.add(cal.getTime());
				dates.add(cal.getTime());
			}
			else if (selectedDate.after(currentDate))
			{
				cal.add(Calendar.DAY_OF_MONTH, -1);
				dates.add(cal.getTime());
				cal.add(Calendar.DAY_OF_MONTH, 1);
				dates.add(cal.getTime());
			}
			cal.add(Calendar.DAY_OF_MONTH, 1);
			dates.add(cal.getTime());
		}
		catch (final ParseException e)
		{
			LOG.error("Error while parsing dates for tabs: " + selectedDateString);
		}

		return dates;
	}

	protected OriginDestinationInfoData createOrigniDestinationInfo(final String origin, final String destination)
	{
		final OriginDestinationInfoData originDestinationInfoData = new OriginDestinationInfoData();
		originDestinationInfoData.setDepartureLocation(origin);
		originDestinationInfoData.setArrivalLocation(destination);
		originDestinationInfoData.setDepartureLocationType(LocationType.AIRPORTGROUP);
		originDestinationInfoData.setArrivalLocationType(LocationType.AIRPORTGROUP);
		return originDestinationInfoData;
	}

	protected List<String> getLinksForTabs(final List<Date> dates, final String origin, final String destination,
			final String routeName)
	{
		final List<String> linksForTabs = new ArrayList<>();
		for (final Date date : dates)
		{
			final String dateString = TravelDateUtils.convertDateToStringDate(date, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
			linksForTabs.add(buildRequestParameters(dateString, origin, destination, routeName));
		}
		return linksForTabs;
	}

	protected String buildRequestParameters(final String newDate, final String origin, final String destination,
			final String routeName)
	{
		final StringBuilder requestParams = new StringBuilder();
		requestParams.append("/").append(routeName);
		requestParams.append("/").append(origin).append("-").append(destination);
		requestParams.append("?scheduleDate=").append(newDate);
		return requestParams.toString();
	}

}
