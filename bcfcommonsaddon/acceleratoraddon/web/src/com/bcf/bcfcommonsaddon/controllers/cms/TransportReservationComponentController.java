/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import reactor.util.CollectionUtils;

import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcheckoutaddon.forms.BookingReferenceForm;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.enums.TransportReservationPage;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfcommonsaddon.model.components.TransportReservationComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.user.BcfUserService;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Controller for Transport Reservation Component
 */
@Controller("TransportReservationComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.TransportReservationComponent)
public class TransportReservationComponentController
		extends SubstitutingCMSAddOnComponentController<TransportReservationComponentModel>
{
	private static final Logger LOGGER = Logger.getLogger(TransportReservationComponentController.class);

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "sessionService")
	public SessionService sessionService;

	@Resource(name = "transportReservationPageViewMap")
	private Map<TransportReservationPage, String> transportReservationPageViewMap;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService cartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	@Resource(name = "userService")
	private BcfUserService userService;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Autowired
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final TransportReservationComponentModel component)
	{
		final TransportReservationComponentModel transportReservationComponentModel = component;
		final TravelFinderForm travelFinderForm = sessionService.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);
		try
		{
			switch (transportReservationComponentModel.getView())
			{
				case PACKAGETRANSPORTRESERVATION:
					packagePopulateModel(model, travelFinderForm);
					break;
				case FERRYTRANSPORTRESERVATION:
					ferryPopulateModel(model);
					break;
				default: // Do nothing
			}


		}
		catch (final Exception e)
		{
			LOGGER.error(e);
		}
	}

	/**
	 * This method is responsible for populating itinerary component after see full reservation button is clicked
	 *
	 * @param componentUid
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model,
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			final String view = handleGet(request, response, model);

			final CartTimerData cartTimerData = bcfCartTimerFacade.getCartStartTimerDetails();
			if(cartTimerData!=null){
				model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
				model.addAttribute("holdTimeMessage", cartTimerData.getHoldTimeMessage());
			}
			return view;


		}
		catch (final Exception e)
		{
			LOGGER.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * Populate model.
	 *
	 * @param model the model
	 */
	protected void packagePopulateModel(final Model model, final TravelFinderForm travelFinderForm)
	{
		final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
		final String travelRoute = travelFinderForm.getTravelRoute();
		final ReservationDataList reservationDataList = bcfReservationFacade
				.getCurrentReservationsByTravelRoute(travelRoute);
		if (Objects.isNull(reservationDataList) || CollectionUtils.isEmpty(reservationDataList.getReservationDatas()))
		{
			return;
		}
		final ReservationData reservationData = reservationDataList.getReservationDatas().stream().findFirst().get();
		final Map<String, List<TravellerData>> travellerDataCodeMap = bcfReservationFacade.getTravellerDataMap(reservationData);
		if (Objects.nonNull(travellerDataCodeMap))
		{
			model.addAttribute(BcfFacadesConstants.RESERVABLE, false);
			model.addAttribute(BcfvoyageaddonWebConstants.TRAVELLER_DATA_CODE_MAP, travellerDataCodeMap);
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.RESERVABLE, true);
		}
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, travelFinderForm.getAccommodationFinderForm());
		model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm.getFareFinderForm());
		model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
				sessionCart.getBookingJourneyType());
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_ACTIVITIES, BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.CONFIRM_AND_REVIEW, BcfvoyageaddonWebConstants.CONFIRM_AND_REVIEW_URL);
		model.addAttribute(BcfstorefrontaddonWebConstants.PACKAGE_AVAILABILITY_STATUS, travelFinderForm.getAvailabilityStatus());
		model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE, BcfvoyageaddonWebConstants.RETURN);
	}

	/**
	 * Populate model.
	 *
	 * @param model the model
	 */
	protected void ferryPopulateModel(final Model model)
	{
		final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
		final String travelRoute = sessionCart.getEntries().stream().filter(entry -> entry.getTravelOrderEntryInfo() != null)
				.iterator().next().getTravelOrderEntryInfo().getTravelRoute().getCode();
		final ReservationDataList reservationDataList = bcfReservationFacade
				.getCurrentReservationsByTravelRoute(travelRoute);
		if (Objects.isNull(reservationDataList) || CollectionUtils.isEmpty(reservationDataList.getReservationDatas()))
		{
			return;
		}
		final ReservationData reservationData = reservationDataList.getReservationDatas().stream().findFirst().get();
		final Map<String, List<TravellerData>> travellerDataCodeMap = bcfReservationFacade.getTravellerDataMap(reservationData);
		if (Objects.nonNull(travellerDataCodeMap))
		{
			model.addAttribute(BcfFacadesConstants.RESERVABLE, false);
			model.addAttribute(BcfvoyageaddonWebConstants.TRAVELLER_DATA_CODE_MAP, travellerDataCodeMap);
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.RESERVABLE, true);
		}
		model.addAttribute(BcfFacadesConstants.MAX_SAILING_ALLOWED,
				bcfTravelCheckoutFacade.isMaxSailingExceedsForUser(sessionCart));
		model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
				sessionCart.getBookingJourneyType());
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_ANOTHER_SAILING,
				BcfFacadesConstants.REMOVE_FORM_AND_REDIRECT_TO_HOME_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.LOGIN, BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH_ONLY);
		model.addAttribute(BcfvoyageaddonWebConstants.CONTINUE_AS_GUEST, getNextPageURL());
		model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE,
				reservationDataList.getReservationDatas().get(reservationDataList.getReservationDatas().size() - 1)
						.getReservationItems().stream().findFirst().get().getReservationItinerary().getTripType());
		model.addAttribute("hasLongRoute", bcfTravelCheckoutFacade.containsLongRoute());
		model.addAttribute("ancillaryPageURL", BcfvoyageaddonWebConstants.ANCILLARY_ROOT_URL);
		model.addAttribute("travellerDetailsPageURL", BcfvoyageaddonWebConstants.TRAVELLER_DETAILS_PATH);

		model.addAttribute(BcfCoreConstants.IS_B2B_CUSTOMER, userService.isB2BCustomer());
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		model.addAttribute("bookingReferenceForm", new BookingReferenceForm());
		model.addAttribute(BcfCoreConstants.IS_FERRY_OPTION_BOOKING, sessionCart.isFerryOptionBooking());
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
	}

	private String getNextPageURL()
	{
		if (BcfFacadesConstants.BOOKING_TRANSPORT_ONLY.equalsIgnoreCase(bcfTravelCheckoutFacade.getBookingJourney()))
		{
			if (bcfTravelCheckoutFacade.containsLongRoute())
			{
				return "/ancillary";
			}
			return "/checkout/multi/payment-method/select-flow";
		}
		else if (BookingJourneyType.BOOKING_ALACARTE.getCode().equalsIgnoreCase(bcfTravelCheckoutFacade.getBookingJourney()))
		{
			return BcfstorefrontaddonWebConstants.ALACARTE_REVIEW_PAGE_PATH;
		}
		else
		{
			return BcfvoyageaddonWebConstants.CONFIRM_AND_REVIEW_URL;
		}
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	protected String getView(final TransportReservationComponentModel transportReservationComponentModel)
	{
		return transportReservationPageViewMap.get(transportReservationComponentModel.getView());
	}

}
