/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfOptionBookingNotificationService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.core.util.OptionBookingUtil;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.vacations.GuestData;


@Controller
public class ActivityAddToCartController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(ActivityAddToCartController.class);

	private static final String ACTIVITY_DETAILS_PAGE_URL = "/vacations/activities/";
	private static final String ADD_ACTIVITY_TO_CART_RESPONSE = "addActivityToCartResponse";
	private static final String ADD_ACTIVITY_TO_CART_ERROR = "add.activity.to.cart.error";
	private static final String STOCK_NOT_AVAILBALE_FOR_ACTIVITY_TO_CART_ERROR = "activity.stock.not.available.error.message";

	@Resource(name = "bcfProductFacade")
	private BcfProductFacade bcfProductFacade;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "activityProductSearchFacade")
	private ActivityProductSearchFacade activityProductSearchFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "dealCartFacade")
	private BcfDealCartFacade dealCartFacade;

	@Resource(name = "bcfOptionBookingNotificationService")
	private BcfOptionBookingNotificationService bcfOptionBookingNotificationService;

	@Resource(name = "cartService")
	private CartService cartService;

	@RequestMapping(value = "/perform/add-activity-to-cart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String addActivityToCart(final AddActivityToCartData addActivityToCartData, final Model model,
			final RedirectAttributes redirectModel)
	{
		try
		{
			final Date activityDate = TravelDateUtils
					.convertStringDateToDate(addActivityToCartData.getActivityDate(), BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);
			final int paxQuantity = addActivityToCartData.getGuestData().stream().mapToInt(GuestData::getQuantity).sum();

			if (BCFDateUtils.isDateInPast(activityDate))
			{
				LOG.info(String.format("Activity [%s] for date [%s] is in past. Terminating AddToCart.",
						addActivityToCartData.getActivityCode(), activityDate));
				GlobalMessages
						.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								STOCK_NOT_AVAILBALE_FOR_ACTIVITY_TO_CART_ERROR);
				return REDIRECT_PREFIX + ACTIVITY_DETAILS_PAGE_URL + addActivityToCartData.getActivityCode();
			}

			final String availabilityStatus = bcfTravelCartFacade
					.getActivityAvailabilityStatus(addActivityToCartData.getActivityCode(), activityDate,
							addActivityToCartData.getStartTime(), paxQuantity);
			if (AvailabilityStatus.SOLD_OUT.name().equals(availabilityStatus))
			{
				LOG.info(String.format("Activity [%s] for date [%s - %s] has availability status [%s]. Terminating AddToCart.",
						addActivityToCartData.getActivityCode(), activityDate, addActivityToCartData.getStartTime(), availabilityStatus));
				GlobalMessages
						.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								STOCK_NOT_AVAILBALE_FOR_ACTIVITY_TO_CART_ERROR);
				return REDIRECT_PREFIX + ACTIVITY_DETAILS_PAGE_URL + addActivityToCartData.getActivityCode();
			}

			final Object bookingJorneyObj = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
			if (Objects.isNull(bookingJorneyObj))
			{
				sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
						BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode());
			}

			addActivityToCartData
					.setActivityDate(TravelDateUtils.convertDateToStringDate(activityDate, BcfFacadesConstants.ACTIVITY_DATE_PATTERN));
			LOG.info(String.format("Activity [%s] for date [%s - %s] has availability status [%s].",
					addActivityToCartData.getActivityCode(), activityDate, addActivityToCartData.getStartTime(), availabilityStatus));
			if (addActivityToCartData.isChangeActivity())
			{
				if (bcfTravelCartFacadeHelper.isAlacateFlow())
				{
					dealCartFacade.removeSelectedActivityEntries(addActivityToCartData.getChangeActivityRef());
				}
				else
				{
					dealCartFacade.removeSelectedActivityEntries(addActivityToCartData.getChangeActivityCode(),
							addActivityToCartData.getJourneyRefNumber(), addActivityToCartData.getChangeActivityDate(),
							addActivityToCartData.getChangeActivityTime());
				}
			}

			List<CartModificationData> cartModificationData = null;
			if (AvailabilityStatus.AVAILABLE.name().equals(availabilityStatus) || AvailabilityStatus.ON_REQUEST.name()
					.equals(availabilityStatus))
			{
				cartModificationData = bcfTravelCartFacade
						.createActivityOrderEntryInfo(addActivityToCartData.getActivityCode(), addActivityToCartData);
			}
			else
			{
				GlobalMessages
						.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								STOCK_NOT_AVAILBALE_FOR_ACTIVITY_TO_CART_ERROR);
				return REDIRECT_PREFIX + ACTIVITY_DETAILS_PAGE_URL + addActivityToCartData.getActivityCode();
			}


			final CartModel cart = cartService.getSessionCart();

			if (StringUtils
					.equals(CommerceCartModificationStatus.SUCCESS, cartModificationData.stream().findFirst().get().getStatusCode()))
			{
				if (OptionBookingUtil.isOptionBooking(cart))
				{
					bcfOptionBookingNotificationService.startOptionBookingEmailProcess(cart, cartModificationData);
				}

				bcfTravelCartFacadeHelper.updateAvailabilityStatus();
				bcfTravelCartFacadeHelper.calculateChangeFees();
				bcfTravelCartFacade.recalculateCart();
				return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						ADD_ACTIVITY_TO_CART_ERROR);
				return REDIRECT_PREFIX + ACTIVITY_DETAILS_PAGE_URL + addActivityToCartData.getActivityCode();
			}
		}
		catch (final CommerceCartModificationException e)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					ADD_ACTIVITY_TO_CART_ERROR);
			return REDIRECT_PREFIX + ACTIVITY_DETAILS_PAGE_URL + addActivityToCartData.getActivityCode();
		}

	}


	@ResponseBody
	@RequestMapping(value = "/findActivitySchedules", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int findActivitySchedules(@RequestParam("activityCode") final String activityCode,
			@RequestParam("dateSelected") final String dateSelected)
	{
		final Date dateSelectedDate = TravelDateUtils
				.convertStringDateToDate(dateSelected, BcfstorefrontaddonWebConstants.BCF_CALANDER_DATE_FORMAT);
		final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule = activityProductSearchFacade
				.createActivityPricePerPassengerAndSchedule(activityCode, dateSelectedDate, null);
		activityProductSearchFacade
				.getValidAvailableSchedules(activityPricePerPassengerWithSchedule, dateSelectedDate, dateSelectedDate);
		return activityPricePerPassengerWithSchedule.getAvailableSchedules().size();
	}

	@ResponseBody
	@RequestMapping(value = "/checkActivityAvailability", method = RequestMethod.POST)
	public String checkActivityAvailability(final AddActivityToCartData addActivityToCartData)
	{
		final Date activityDate = TravelDateUtils
				.convertStringDateToDate(addActivityToCartData.getActivityDate(), BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);
		final int paxQuantity = addActivityToCartData.getGuestData().stream().mapToInt(GuestData::getQuantity).sum();
		final String activitySelectedTime = addActivityToCartData.getStartTime();

		if (BCFDateUtils.isDateInPast(activityDate))
		{
			return AvailabilityStatus.SOLD_OUT.name();
		}
		return bcfTravelCartFacade
				.getActivityAvailabilityStatus(addActivityToCartData.getActivityCode(), activityDate,
						activitySelectedTime, paxQuantity);
	}

}
