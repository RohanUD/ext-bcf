/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import reactor.util.CollectionUtils;

import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.enums.TransportReservationPage;
import com.bcf.bcfcommonsaddon.model.components.AlacarteReservationComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.facades.assistedservicefacades.impl.DefaultBCFAssistedServiceFacade;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.bcffacades.impl.DefaultBcfUserFacade;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Controller for Transport Reservation Component
 */
@Controller("AlacarteReservationComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.AlacarteReservationComponent)
public class AlacarteReservationComponentController
		extends SubstitutingCMSAddOnComponentController<AlacarteReservationComponentModel>
{
	private static final Logger LOGGER = Logger.getLogger(AlacarteReservationComponentController.class);

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "sessionService")
	public SessionService sessionService;

	@Resource(name = "transportReservationPageViewMap")
	private Map<TransportReservationPage, String> transportReservationPageViewMap;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService cartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@Resource(name = "bcfAccommodationFacadeHelper")
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	@Autowired
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "assistedServiceFacade")
	DefaultBCFAssistedServiceFacade bcfAssistedServiceFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "userFacade")
	private DefaultBcfUserFacade userFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AlacarteReservationComponentModel component)
	{
		if (bcfAssistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			if (cartService.hasSessionCart())
			{
				final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
				final AsmOrderViewData asmOrderData = bcfTravelBookingFacade.getASMOrderData(sessionCart);
				model.addAttribute("asmOrderData", asmOrderData);
				model.addAttribute("asmLoggedIn", Boolean.TRUE);
				final Boolean northernRoute = ChangeAndCancellationFeeCalculationHelper.isNorthernRoute(sessionCart.getEntries());
				model.addAttribute("northernRoute", northernRoute);
				final Map<String, String> userRoles = userFacade.getUserRoles();
				userRoles.put("financestaffgroup", "financestaffgroup");
				userRoles.put("bcfvmanagergroup", "bcfvmanagergroup");
				model.addAttribute("userRoles", userRoles);
			}
		}

		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();
		ferryPopulateModel(model, bcfGlobalReservationData, request);
		accommodationPopulateModel(model, request, bcfGlobalReservationData);

		if (cartService.hasSessionCart())
		{
			if (bcfTravelCartFacadeHelper.isAlacateFlow())
			{
				bcfTravelCartFacadeHelper.setCartBookingType(BookingJourneyType.BOOKING_ALACARTE);
			}
			final boolean isAmendBooking = bcfTravelCartFacade.isAmendmentCart();
			model.addAttribute("isAmendment", isAmendBooking);

			final CartTimerData cartTimerData = bcfCartTimerFacade.getCartStartTimerDetails();
			if (cartTimerData != null)
			{
				model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
				model.addAttribute("holdTimeMessage", cartTimerData.getHoldTimeMessage());
			}

			model.addAttribute("showAvailabilityStatus", true);
			model.addAttribute("showSupplierComments", true);
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_ALACARTE);

	}

	/**
	 * This method is responsible for populating itinerary component after see full reservation button is clicked
	 *
	 * @param componentUid
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOGGER.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * Populate model.
	 *
	 * @param model the model
	 */
	protected void accommodationPopulateModel(final Model model, final HttpServletRequest request,
			final BcfGlobalReservationData bcfGlobalReservationData)
	{
		if (cartService.hasSessionCart())
		{

			if (bcfGlobalReservationData != null && bcfGlobalReservationData.getAccommodationReservations() != null
					&& !CollectionUtils
					.isEmpty(bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()))
			{
				model.addAttribute("accommodationMap",
						bcfAccommodationFacadeHelper.sortAccommodationReservation(bcfGlobalReservationData));

				model.addAttribute("urlParametersMap",
						bcfAccommodationFacadeHelper.buildAccommodationOfferingPageUrlParameters(
								bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()));
			}

			model.addAttribute(BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);
		}
	}


	/**
	 * Populate model.
	 *
	 * @param model the model
	 */
	protected void ferryPopulateModel(final Model model, final BcfGlobalReservationData bcfGlobalReservationData,
			final HttpServletRequest request)
	{
		ReservationDataList reservationDataList = null;
		CartModel sessionCart = null;
		if (cartService.hasSessionCart())
		{

			sessionCart = bcfTravelCheckoutFacade.getSessionCart();
			final AbstractOrderEntryModel abstractOrderEntryModel = sessionCart.getEntries().stream()
					.filter(entryModel -> entryModel.getTravelOrderEntryInfo() != null).findAny().orElse(null);

			if (abstractOrderEntryModel != null)
			{

				reservationDataList = bcfGlobalReservationData.getTransportReservations();
			}
			model.addAttribute(BcfFacadesConstants.MAX_SAILING_ALLOWED,
					bcfTravelCheckoutFacade.isMaxSailingExceedsForUser(sessionCart));
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					sessionCart.getBookingJourneyType());
		}
		if (reservationDataList != null && !CollectionUtils.isEmpty(reservationDataList.getReservationDatas()))
		{
			final ReservationData reservationData = reservationDataList.getReservationDatas().stream().findFirst().get();
			final Map<String, List<TravellerData>> travellerDataCodeMap = bcfReservationFacade.getTravellerDataMap(reservationData);
			if (Objects.nonNull(travellerDataCodeMap))
			{
				model.addAttribute(BcfFacadesConstants.RESERVABLE, false);
				model.addAttribute(BcfvoyageaddonWebConstants.TRAVELLER_DATA_CODE_MAP, travellerDataCodeMap);
			}
			else
			{
				model.addAttribute(BcfFacadesConstants.RESERVABLE, true);
			}
			model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
			final ReservationItemData reservationItemData = reservationDataList.getReservationDatas()
					.get(reservationDataList.getReservationDatas().size() - 1)
					.getReservationItems().stream().findFirst().orElse(null);

			if (reservationItemData != null)
			{
				model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE,
						reservationItemData.getReservationItinerary().getTripType());
			}


		}

		if (sessionCart != null)
		{

			model.addAttribute(BcfFacadesConstants.MAX_SAILING_ALLOWED,
					bcfTravelCheckoutFacade.isMaxSailingExceedsForUser(sessionCart));
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					sessionCart.getBookingJourneyType());
		}

		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));

		model.addAttribute(BcfvoyageaddonWebConstants.ADD_ANOTHER_SAILING,
				request.getContextPath() + "/");
		model.addAttribute(BcfvoyageaddonWebConstants.LOGIN, BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH);
		populateNextUrl(request, model);
	}

	protected void populateNextUrl(final HttpServletRequest request, final Model model)
	{
		model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL,
				request.getContextPath() + "/checkout/multi/payment-method/select-flow");
		if (travelCheckoutFacade
				.containsLongRoute() && !travelCheckoutFacade.containsDefaultSailing())
		{
			model.addAttribute(BcfvoyageaddonWebConstants.AMENITIES_URL, request.getContextPath() + "/ancillary");
			model.addAttribute(BcfvoyageaddonWebConstants.NORTHERN_ROUTE, true);
		}
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}



}
