/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityRequestData;
import de.hybris.platform.commercefacades.accommodation.AccommodationSearchRequestData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.ServiceData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.packages.request.AccommodationPackageRequestData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.SearchProcessingInfoData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TransportOfferingPreferencesData;
import de.hybris.platform.commercefacades.travel.TravelPreferencesData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.FareSelectionDisplayOrder;
import de.hybris.platform.commercefacades.travel.enums.TransportOfferingType;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.fare.sorting.strategies.AbstractResultSortingStrategy;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.ui.Model;
import com.bcf.bcfaccommodationsaddon.controllers.pages.AbstractAccommodationPageController;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonConstants;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.util.BcfCommonsaddonControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.facades.utils.BcfFacadeUtil;


/**
 * Abstract controller for Package related pages
 */
public abstract class AbstractPackagePageController extends AbstractAccommodationPageController
{
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "transportFacilityFacade")
	private TransportFacilityFacade transportFacilityFacade;

	@Resource(name = "fareSelectionSortingStrategyMap")
	private Map<FareSelectionDisplayOrder, AbstractResultSortingStrategy> fareSelectionSortingStrategyMap;

	@Resource(name = "bcfCommonsaddonControllerUtil")
	private BcfCommonsaddonControllerUtil bcfCommonsaddonControllerUtil;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade travelRouteFacade;

	protected void populateFareSearchResponseInModel(final FareSelectionData fareSearchResponse, final Model model)
	{
		if (fareSearchResponse != null)
		{
			final Optional<PricedItineraryData> outboundPricedItineraryOptional = fareSearchResponse.getPricedItineraries().stream()
					.filter(pricedItinerary -> TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER == pricedItinerary
							.getOriginDestinationRefNumber())
					.findAny();

			final Optional<PricedItineraryData> inboundPricedItineraryOptional = fareSearchResponse.getPricedItineraries().stream()
					.filter(pricedItinerary -> TravelfacadesConstants.INBOUND_REFERENCE_NUMBER == pricedItinerary
							.getOriginDestinationRefNumber())
					.findAny();

			final AddBundleToCartForm addBundleToCartForm = new AddBundleToCartForm();
			model.addAttribute(BcfvoyageaddonWebConstants.ADD_BUNDLE_TO_CART_FORM, addBundleToCartForm);
			model.addAttribute(BcfstorefrontaddonWebConstants.ADD_BUNDLE_TO_CART_URL, BcfcommonsaddonWebConstants.CHANGE_BUNDLE_URL);

			model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE,
					outboundPricedItineraryOptional.isPresent() && inboundPricedItineraryOptional.isPresent()
							? TripType.RETURN.toString()
							: TripType.SINGLE.toString());

			model.addAttribute(BcfvoyageaddonWebConstants.PI_DATE_FORMAT, BcfvoyageaddonWebConstants.PRICED_ITINERARY_DATE_FORMAT);
			model.addAttribute(BcfvoyageaddonWebConstants.OUTBOUND_REF_NUMBER, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER);
			model.addAttribute(BcfvoyageaddonWebConstants.INBOUND_REF_NUMBER, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);
			model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_OUTBOUND_OPTIONS,
					countJourneyOptions(fareSearchResponse, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER));
			model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_INBOUND_OPTIONS,
					countJourneyOptions(fareSearchResponse, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER));

			if (outboundPricedItineraryOptional.isPresent())
			{
				final TravelRouteData route = outboundPricedItineraryOptional.get().getItinerary().getRoute();
				model.addAttribute(BcfvoyageaddonWebConstants.ORIGIN,
						route.getOrigin().getName() + " (" + route.getOrigin().getCode() + ")");
				model.addAttribute(BcfvoyageaddonWebConstants.DESTINATION,
						route.getDestination().getName() + " (" + route.getDestination().getCode() + ")");
			}
		}
	}

	protected int countJourneyOptions(final FareSelectionData fareSelectionData, final int referenceNumber)
	{
		return (int) fareSelectionData.getPricedItineraries().stream()
				.filter(pricedItinerary -> pricedItinerary.getOriginDestinationRefNumber() == referenceNumber
						&& pricedItinerary.isAvailable())
				.count();
	}

	/**
	 * Method handles the preparation of a FareSearchRequestData object using the FareFinderForm.
	 *
	 * @param fareFinderForm
	 * @return
	 */
	protected FareSearchRequestData prepareFareSearchRequestData(final FareFinderForm fareFinderForm,
			final HttpServletRequest request)
	{
		final String channel = bcfSalesApplicationResolverFacade.getCurrentSalesChannel();
		final FareSearchRequestData fareSearchRequestData = new FareSearchRequestData();

		fareSearchRequestData.setTravelPreferences(createTravelPreferences(fareFinderForm));
		fareSearchRequestData.setPassengerTypes(fareFinderForm.getPassengerTypeQuantityList());
		fareSearchRequestData.setOriginDestinationInfo(createOriginDestinationInfos(fareFinderForm));
		fareSearchRequestData.setTripType(TripType.valueOf(fareFinderForm.getTripType()));
		fareSearchRequestData.setSearchProcessingInfo(createSearchProcessingInfo(request));
		fareSearchRequestData.setVehicleInfoData(createVehicleInfoData(fareFinderForm));
		fareSearchRequestData.setSalesApplication(SalesApplication.valueOf(channel));
		fareSearchRequestData.setRouteType(
				travelRouteFacade.getRouteType(fareFinderForm.getDepartureLocation(), fareFinderForm.getArrivalLocation()).getCode());
		return fareSearchRequestData;

	}

	protected List<VehicleTypeQuantityData> createVehicleInfoData(final FareFinderForm fareFinderForm)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();
		for (final VehicleTypeQuantityData vehicleInfo : fareFinderForm.getVehicleInfo())
		{
			bcfvoyageaddonComponentControllerUtil.setVehicleCodeAndCategory(vehicleInfo.getVehicleType());
			vehicleTypeQuantityDataList.add(vehicleInfo);
		}
		return vehicleTypeQuantityDataList;
	}

	protected SearchProcessingInfoData createSearchProcessingInfo(final HttpServletRequest request)
	{
		final SearchProcessingInfoData searchProcessingInfoData = new SearchProcessingInfoData();
		final String displayOrder = request.getParameter(BcfvoyageaddonWebConstants.DISPLAY_ORDER);
		if (StringUtils.isNotBlank(displayOrder))
		{
			searchProcessingInfoData.setDisplayOrder(displayOrder);
		}
		else
		{
			searchProcessingInfoData.setDisplayOrder(FareSelectionDisplayOrder.DEPARTURE_TIME.toString());
		}
		return searchProcessingInfoData;
	}

	protected List<OriginDestinationInfoData> createOriginDestinationInfos(final FareFinderForm fareFinderForm)
	{
		final List<OriginDestinationInfoData> originDestinationInfoData = new ArrayList<>();

		final OriginDestinationInfoData departureInfo = new OriginDestinationInfoData();

		departureInfo.setDepartureLocation(fareFinderForm.getDepartureLocation());
		departureInfo.setArrivalLocation(fareFinderForm.getArrivalLocation());
		departureInfo.setDepartureTime(TravelDateUtils.convertStringDateToDate(fareFinderForm.getDepartingDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		departureInfo.setReferenceNumber(0);

		originDestinationInfoData.add(departureInfo);

		if (StringUtils.equalsIgnoreCase(TripType.RETURN.name(), fareFinderForm.getTripType()))
		{
			final OriginDestinationInfoData returnInfo = new OriginDestinationInfoData();

			returnInfo.setDepartureLocation(fareFinderForm.getArrivalLocation());
			returnInfo.setArrivalLocation(fareFinderForm.getDepartureLocation());
			returnInfo.setDepartureTime(TravelDateUtils.convertStringDateToDate(fareFinderForm.getReturnDateTime(),
					BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			returnInfo.setReferenceNumber(1);
			originDestinationInfoData.add(returnInfo);
		}
		return originDestinationInfoData;
	}

	protected TravelPreferencesData createTravelPreferences(final FareFinderForm fareFinderForm)
	{
		final TravelPreferencesData travelPreferences = new TravelPreferencesData();
		travelPreferences.setCabinPreference(fareFinderForm.getCabinClass());
		travelPreferences.setTransportOfferingPreferences(createTransportOfferingPreferences());
		return travelPreferences;
	}

	protected TransportOfferingPreferencesData createTransportOfferingPreferences()
	{
		final TransportOfferingPreferencesData transportOfferingPreferencesData = new TransportOfferingPreferencesData();
		transportOfferingPreferencesData.setTransportOfferingType(TransportOfferingType.DIRECT);
		return transportOfferingPreferencesData;
	}

	/**
	 * Creates the passenger type quantity data.
	 *
	 * @param numberOfRooms
	 *           the accommodation finder form
	 * @param roomStayCandidates
	 *           roomStayCandidates
	 * @return the list
	 */
	protected List<PassengerTypeQuantityData> createPassengerTypeQuantityData(final int numberOfRooms,
			final List<RoomStayCandidateData> roomStayCandidates)
	{
		List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		if (CollectionUtils.size(roomStayCandidates) < numberOfRooms)
		{
			return passengerTypeQuantityList;
		}

		for (int i = 0; i < numberOfRooms; i++)
		{
			final List<PassengerTypeQuantityData> guestCounts = roomStayCandidates.get(i).getPassengerTypeQuantityList();
			if (CollectionUtils.isEmpty(guestCounts))
			{
				continue;
			}

			passengerTypeQuantityList = bcfCommonsaddonControllerUtil.getPassengerTypeQuantityList(guestCounts,
					passengerTypeQuantityList);
		}
		return passengerTypeQuantityList;
	}

	/**
	 * Gets the location name.
	 *
	 * @param locationCode
	 *           the location code
	 */
	protected String getLocationName(final String locationCode)
	{
		final TransportFacilityData transportFacilityData = transportFacilityFacade.getTransportFacility(locationCode);
		final LocationData location = transportFacilityFacade.getLocation(locationCode);
		String locationName = location.getName();
		if (Objects.nonNull(transportFacilityData))
		{
			locationName = locationName + " ( " + transportFacilityData.getName() + " )";
		}
		return locationName;
	}

	/**
	 * Method to sort the FareSelectionData based on the displayOrder. If displayOrder is null, empty or not a valid
	 * FareSelectionDisplayOrder enum, the default sorting by departureDate is applied.
	 *
	 * @param fareSelectionData
	 *           as the FareSelectionData to be sorted
	 * @param displayOrder
	 *           as the String corresponding to a sortingStrategy
	 */
	protected void sortFareSelectionData(final FareSelectionData fareSelectionData, final String displayOrder)
	{
		final FareSelectionDisplayOrder displayOrderOption = Arrays.asList(FareSelectionDisplayOrder.values()).stream()
				.filter(val -> val.toString().equals(displayOrder)).findAny().orElse(FareSelectionDisplayOrder.DEPARTURE_TIME);
		final AbstractResultSortingStrategy sortingStrategy = fareSelectionSortingStrategyMap.get(displayOrderOption);
		sortingStrategy.sortFareSelectionData(fareSelectionData);
	}

	protected void populateVehicles(final TravelFinderForm travelFinderForm) {
      travelFinderForm.getFareFinderForm().getVehicleInfo().forEach(vehicleInfo -> {
         populateVehicle(vehicleInfo);
         vehicleInfo.setTravelRouteCode(travelFinderForm.getTravelRoute());
      });
   }

	protected void initializeDefaultVehicle(final FareFinderForm fareFinderForm)
	{
		final VehicleTypeQuantityData vehicleInfo = new VehicleTypeQuantityData();
		vehicleInfo.setVehicleType(new VehicleTypeData());
      populateVehicle(vehicleInfo);
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();
		vehicleTypeQuantityDataList.add(vehicleInfo);
		fareFinderForm.setVehicleInfo(vehicleTypeQuantityDataList);
	}

	protected void populateVehicle(final VehicleTypeQuantityData vehicleInfo) {
      vehicleInfo.getVehicleType().setCategory(BcfcommonsaddonConstants.VEHICLE_CATEGORY_STANDARD);
      vehicleInfo.getVehicleType().setCode(BcfcommonsaddonConstants.VEHICLE_CODE_UH);
      vehicleInfo.setHeight(BcfcommonsaddonConstants.VEHICLE_HEIGHT);
      vehicleInfo.setLength(BcfcommonsaddonConstants.VEHICLE_LENGTH);
      vehicleInfo.setWidth(BcfcommonsaddonConstants.VEHICLE_WIDTH);
      vehicleInfo.setQty(1);
   }

	protected List<OriginDestinationInfoData> createOriginDestinationInfo(final HttpServletRequest request,
			final FareFinderForm fareFinderForm)
	{
		final List<OriginDestinationInfoData> originDestinationInfoData = new ArrayList<>();
		if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
				.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
		{
			final OriginDestinationInfoData returnInfo = new OriginDestinationInfoData();

			returnInfo.setDepartureLocation(fareFinderForm.getArrivalLocation());
			returnInfo.setArrivalLocation(fareFinderForm.getDepartureLocation());
			returnInfo.setDepartureTime(TravelDateUtils.convertStringDateToDate(fareFinderForm.getReturnDateTime(),
					BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			returnInfo.setReferenceNumber(1);
			originDestinationInfoData.add(returnInfo);
			return originDestinationInfoData;
		}

		final OriginDestinationInfoData departureInfo = new OriginDestinationInfoData();

		departureInfo.setDepartureLocation(fareFinderForm.getDepartureLocation());
		departureInfo.setArrivalLocation(fareFinderForm.getArrivalLocation());
		departureInfo.setDepartureTime(TravelDateUtils.convertStringDateToDate(fareFinderForm.getDepartingDateTime(),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		departureInfo.setReferenceNumber(0);
		originDestinationInfoData.add(departureInfo);
		return originDestinationInfoData;
	}

	protected void populateDynamicDataForPackageHotel(final Model model, final PackageResponseData packageResponseData,
			final ContentPageModel contentPageModel, final HttpServletRequest request)
	{
		String hotelName = "";
		String hotelCityName = "";
		String hotelCode = "";

		if (Objects.nonNull(packageResponseData.getAccommodationPackageResponse()) &&
				Objects.nonNull(packageResponseData.getAccommodationPackageResponse().getAccommodationAvailabilityResponse()) &&
				Objects.nonNull(packageResponseData.getAccommodationPackageResponse().getAccommodationAvailabilityResponse()
						.getAccommodationReference()))
		{
			final PropertyData accommodationReference = packageResponseData.getAccommodationPackageResponse()
					.getAccommodationAvailabilityResponse().getAccommodationReference();
			hotelName = accommodationReference.getAccommodationOfferingName();
			hotelCode = accommodationReference.getAccommodationOfferingCode();
			if (Objects.nonNull(accommodationReference.getAddress()))
			{
				hotelCityName = accommodationReference.getAddress().getTown();
			}
		}
		final String[] arguments = { hotelName, hotelCityName, hotelName };
		setUpMetaDataForContentPage(model, contentPageModel, arguments);
		setUpDynamicOGGraphData(model, contentPageModel, request, arguments);

		model.asMap().remove(BcfCoreConstants.CANONICAL_URL);
		final String[] pathParams = { hotelCityName.replace(" ", "-"), hotelName.replace(" ", "-"), hotelCode };

		final String relativeSiteUrl =
				getConfigurationService().getConfiguration().getString(BcfCoreConstants.BCF_HTTPS_URL) + BcfFacadeUtil
						.getRelativeSiteUrl(BcfFacadesConstants.PACKAGE_HOTEL_DETAILS_PAGE_PATH, Collections.emptyMap(), pathParams);
		model.addAttribute(BcfCoreConstants.CANONICAL_URL, StringUtils.replace(relativeSiteUrl, " ", "-"));
	}

	/**
	 * Populate the Package Request.
	 *
	 * @param packageRequestData
	 * @param checkInDateTime
	 * @param checkOutDateTime
	 * @param roomStayCandidates
	 * @param accommodationOfferingCode
	 * @param useOldReservedRoomStays
	 * @param request
	 */
	protected void populateAccommodationPackageRequestData(final PackageRequestData packageRequestData,
			final String checkInDateTime, final String checkOutDateTime, final List<RoomStayCandidateData> roomStayCandidates,
			final String accommodationOfferingCode,
			final boolean useOldReservedRoomStays, final HttpServletRequest request,
			final PropertyData accommodationReference, final ServiceData rentServiceData, final String promotionCode)
	{
		final AccommodationAvailabilityRequestData accommodationAvailabilityRequestData = new AccommodationAvailabilityRequestData();
		final CriterionData criterion = new CriterionData();
		final StayDateRangeData stayDateRange = new StayDateRangeData();
		stayDateRange.setStartTime(TravelDateUtils.convertStringDateToDate(checkInDateTime,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));
		stayDateRange.setEndTime(TravelDateUtils.convertStringDateToDate(checkOutDateTime,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));

		final long lengthOfStay = ChronoUnit.DAYS
				.between(stayDateRange.getStartTime().toInstant(), stayDateRange.getEndTime().toInstant());

		stayDateRange.setLengthOfStay((int) lengthOfStay);
		criterion.setStayDateRange(stayDateRange);


		if (rentServiceData != null)
		{
			roomStayCandidates.stream()
					.forEach(roomStayCandidate -> roomStayCandidate.setServices(Collections.singletonList(rentServiceData)));
		}

		criterion.setAccommodationReference(accommodationReference);
		criterion.setRoomStayCandidates(roomStayCandidates);
		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);
		accommodationAvailabilityRequestData.setCriterion(criterion);
		final List<ReservedRoomStayData> reservedRoomStays = new ArrayList<>();

		if (useOldReservedRoomStays)
		{
			final List<ReservedRoomStayData> oldreservedRoomStays = getBookingFacade().getOldReservedRoomStays();

			if (CollectionUtils.isNotEmpty(oldreservedRoomStays)
					&& (DateUtils.isSameDay(oldreservedRoomStays.get(0).getCheckInDate(), stayDateRange.getStartTime())
					&& DateUtils.isSameDay(oldreservedRoomStays.get(0).getCheckOutDate(), stayDateRange.getEndTime())))
			{
				reservedRoomStays.addAll(oldreservedRoomStays);
			}
		}

		final List<ReservedRoomStayData> newReservedRoomStays = getBookingFacade().getNewReservedRoomStays();
		if (CollectionUtils.isNotEmpty(newReservedRoomStays)
				&& (DateUtils.isSameDay(newReservedRoomStays.get(0).getCheckInDate(), stayDateRange.getStartTime())
				&& DateUtils.isSameDay(newReservedRoomStays.get(0).getCheckOutDate(), stayDateRange.getEndTime())))
		{
			reservedRoomStays.addAll(newReservedRoomStays);
		}
		accommodationAvailabilityRequestData.setReservedRoomStays(reservedRoomStays);
		final AccommodationSearchRequestData accommodationSearchRequestData = prepareAccommodationSearchRequestData(
				accommodationOfferingCode, checkInDateTime, checkOutDateTime, roomStayCandidates);

		final AccommodationPackageRequestData accommodationPackageRequestData = new AccommodationPackageRequestData();
		accommodationPackageRequestData.setAccommodationAvailabilityRequest(accommodationAvailabilityRequestData);
		accommodationPackageRequestData.setAccommodationSearchRequest(accommodationSearchRequestData);
		packageRequestData.setAccommodationPackageRequest(accommodationPackageRequestData);
		packageRequestData.setPromotionId(promotionCode);
	}
}
