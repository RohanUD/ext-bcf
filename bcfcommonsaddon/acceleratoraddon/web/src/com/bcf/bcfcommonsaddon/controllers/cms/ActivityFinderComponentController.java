/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.enumeration.EnumerationService;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.forms.cms.ActivityFinderForm;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.enums.ActivityCategoryType;
import com.bcf.core.enums.GuestType;
import com.bcf.facades.ActivityProductFacade;
import com.bcf.facades.DTO.EnumData;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.enumeration.EnumerationDTOFacade;
import com.bcf.facades.vacations.GuestData;


@Controller("ActivityFinderComponentController")
@RequestMapping(value = "/view/ActivityFinderComponentController")
public class ActivityFinderComponentController extends AbstractFinderComponentController
{
	private static final Logger LOG = Logger.getLogger(ActivityFinderComponentController.class);
	protected static final String SEARCH = "/search";

	@Resource
	private EnumerationService enumerationService;

	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@Resource(name = "enumerationDTOFacade")
	private EnumerationDTOFacade enumerationDTOFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{
		final List<EnumData> activityData = enumerationDTOFacade.getEnumData(ActivityCategoryType.SIMPLE_CLASSNAME);
		final List<ActivityDetailsData> activityProductDatas = activityProductFacade.findActivityProducts();
		model.addAttribute("activities", activityProductDatas);
		model.addAttribute("activityTypeData", activityData);
		model.addAttribute(BcfvoyageaddonWebConstants.ACTIVITY_FINDER_FORM, initializeActivityFinderForm(new ActivityFinderForm()));
	}

	public ActivityFinderForm initializeActivityFinderForm(final ActivityFinderForm activityFinderForm)
	{
		final List<GuestData> guestDataList = new ArrayList<>();
		final List<EnumData> enumData = enumerationDTOFacade.getEnumData(GuestType.SIMPLE_CLASSNAME);
		enumData.forEach(data -> {
			final GuestData guestData = new GuestData();
			guestData.setGuestType(data.getCode());
			guestData.setQuantity(0);
			guestDataList.add(guestData);
		});
		activityFinderForm.setGuestData(guestDataList);
		activityFinderForm.setMaxGuestCountPerPaxType(activityProductFacade.getMaxPaxAllowedPerPaxTypePerActivity());
		return activityFinderForm;
	}

	@RequestMapping(value = SEARCH, method = RequestMethod.POST)
	public String performSearch(@Valid final ActivityFinderForm activityFinderForm, final RedirectAttributes redirectModel,
			final BindingResult bindingResult)
	{
		redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.ACTIVITY_FINDER_FORM, activityFinderForm);

		final String urlParameters = buildUrlParameters(activityFinderForm);
		final String redirectUrl = REDIRECT_PREFIX + "/activity-listing";
		return urlParameters.isEmpty() ? redirectUrl : redirectUrl + urlParameters;
	}

	protected String buildUrlParameters(final ActivityFinderForm activityFinderForm)
	{
		final Map<String, String> urlParameters = new HashMap<>();
		final String activityselectionUrl;
		if (StringUtils.isNotBlank(activityFinderForm.getDestination()))
		{
			urlParameters.put(BcfvoyageaddonWebConstants.DESTINATION_LOCATION, activityFinderForm.getDestination());
		}
		else
		{
			urlParameters.put(BcfvoyageaddonWebConstants.DESTINATION_LOCATION, BcfFacadesConstants.Activity.ANY);
		}
		urlParameters.put(BcfvoyageaddonWebConstants.DATE, activityFinderForm.getDate());
		urlParameters.put(BcfvoyageaddonWebConstants.ACTIVITY_TYPE, activityFinderForm.getActivityCategoryTypes());
		try
		{
			final String encodedActivity = URLEncoder.encode(activityFinderForm.getActivity(), "UTF-8");
			urlParameters.put(BcfvoyageaddonWebConstants.ACTIVITY, encodedActivity);
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Cannot encode the URL", e);
		}

		for (final GuestData ptcData : activityFinderForm.getGuestData())
		{
			String guestAge = "_";
			if (ptcData.getGuestType() == null)
			{
				continue;
			}
			urlParameters.put(ptcData.getGuestType(), String.valueOf(ptcData.getQuantity()));
			if (Objects.nonNull(ptcData.getAge()))
			{
				for (final int age : ptcData.getAge())
				{
					guestAge = guestAge.concat(age + "_");
				}
				urlParameters.put(ptcData.getGuestType() + BcfFacadesConstants.Activity.AGE, guestAge);
			}
		}

		activityselectionUrl =
				BcfvoyageaddonWebConstants.QUESTION + urlParameters.toString().replace(", ", "&").replace("{", "")
						.replace("}", "");
		return activityselectionUrl;

	}

}
