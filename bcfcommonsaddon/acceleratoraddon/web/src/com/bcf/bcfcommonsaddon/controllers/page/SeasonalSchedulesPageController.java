/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.schedules.SeasonalScheduleData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.webservices.schedules.BcfSchedulesFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller("SeasonalSchedulesPageController")
@RequestMapping(value = "/seasonal-schedules")
public class SeasonalSchedulesPageController extends BcfAbstractPageController
{

	private static final Logger LOG = Logger.getLogger(SeasonalSchedulesPageController.class);

	@Resource(name = "bcfSchedulesFacade")
	private BcfSchedulesFacade bcfSchedulesFacade;

	@Resource(name = "bcfTransportFacilityFacade")
	private BCFTransportFacilityFacade bcfTransportFacilityFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@RequestMapping(method = RequestMethod.GET)
	public String getSchedulesLandingDetails(
			@RequestParam(value = "departurePort", required = true) final String departurePort,
			@RequestParam(value = "arrivalPort", required = true) final String arrivalPort,
			@RequestParam(value = "departureDate", required = false) final String departureDate,
			final HttpServletRequest request, final Model model)
			throws CMSItemNotFoundException, ParseException
	{

		model.addAttribute("schedules", bcfSchedulesFacade.getTravelRouteInfo());
		model.addAttribute("selectedDateSchedule", departureDate);

		final DestinationAndOriginForTravelRouteData routeInfo = bcfTransportFacilityFacade
				.getTravelRouteInfoForSourceAndDestination(departurePort, arrivalPort);
		model.addAttribute("routeInfo", routeInfo);

		try
		{
			final SeasonalScheduleData seasonalScheduleData = bcfSchedulesFacade
					.getSeasonalSchedules(populateParams(departurePort, arrivalPort, departureDate));
			final Map<String, String> minMaxSailingDurations = bcfSchedulesFacade
					.getSailingDurations(seasonalScheduleData.getSchedulesData());

			model.addAttribute("seasonalScheduleData", seasonalScheduleData);

			model.addAttribute("sailingDurations", minMaxSailingDurations);
		}
		catch (final IntegrationException intEx)
		{
			LOG.error(intEx.getMessage(), intEx);
			GlobalMessages.addErrorMessage(model, "text.service.notAvailable");
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(BcfcommonsaddonWebConstants.SEASONAL_SCHEDULE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(BcfcommonsaddonWebConstants.SEASONAL_SCHEDULE_CMS_PAGE));
		return getViewForPage(model);
	}

	private Map<String, Object> populateParams(final String departurePort, final String arrivalPort,
			final String departureDate)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_PORT, departurePort);
		params.put(BcfCoreConstants.SEASONAL_SCHEDULE_ARRIVAL_PORT, arrivalPort);
		if (StringUtils.isNotEmpty(departureDate))
		{
			final String[] dates = departureDate.split("-");
			params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_FROM, dates[0]);
			params.put(BcfCoreConstants.SEASONAL_SCHEDULE_DEPARTURE_DATE_TO, dates[1]);

		}
		return params;
	}
}
