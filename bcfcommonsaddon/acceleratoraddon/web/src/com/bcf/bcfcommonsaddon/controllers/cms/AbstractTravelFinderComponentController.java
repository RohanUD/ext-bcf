/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationSuggestionFacade;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationFinderValidator;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.util.BcfCommonsaddonControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.validators.FareFinderValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.VehicleType.VehicleTypeFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Abstract Travel Finder Component Controller
 */
public abstract class AbstractTravelFinderComponentController extends AbstractFinderComponentController
{

	private static final Logger LOG = Logger.getLogger(AbstractTravelFinderComponentController.class);

	private static final String ECONOMY_CABIN_CLASS_CODE = "M";
	private static final String SHOW_ACC_DESTINATION = "showAccommodationDestination";
	private static final String SHOW_CHECKIN_CHECKOUT = "showCheckInCheckOut";
	private static final int DEFAULT_ACCOMMODATION_QUANTITY = 1;
	private static final String ROOM = "r";
	public static final String VEHICLE_TO_SKIP = "vehicleToSkip";
	public static final String SKIP_VEHICLE_DIMENSIONS_PROPERTY = "vehicleTypesToSkipDimensionValidation";
	private static final String VEHICLES = "vehicles";

	@Resource(name = "fareFinderValidator")
	protected FareFinderValidator fareFinderValidator;

	@Resource(name = "accommodationFinderValidator")
	protected AccommodationFinderValidator accommodationFinderValidator;

	@Resource(name = "transportFacilityFacade")
	private TransportFacilityFacade transportFacilityFacade;

	@Resource(name = "accommodationSuggestionFacade")
	private AccommodationSuggestionFacade accommodationSuggestionFacade;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "bcfCommonsaddonControllerUtil")
	private BcfCommonsaddonControllerUtil bcfCommonsaddonControllerUtil;

	@Resource(name = "vehicleTypeFacade")
	private VehicleTypeFacade vehicleTypeFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{

		TravelFinderForm travelFinderForm = null;

		if (model.containsAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM))
		{
			travelFinderForm = (TravelFinderForm) model.asMap().get(BcfFacadesConstants.TRAVEL_FINDER_FORM);
			travelFinderForm.setOnlyAccommodationChange(false);
		}

		if (travelFinderForm == null && (request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM) != null
				&& request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) != null))
		{
			travelFinderForm = new TravelFinderForm();
			travelFinderForm.setFareFinderForm((FareFinderForm) request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM));
			travelFinderForm.setAccommodationFinderForm(
					(AccommodationFinderForm) request.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM));
			travelFinderForm.setOnlyAccommodationChange(false);
			verifyAndUpdateDates(travelFinderForm);
			request.setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}

		if (travelFinderForm == null)
		{
			travelFinderForm=initializeTravelFinderForm(model);
			model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}
		else
		{
			if (!model.containsAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM)
					&& request.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) != null)
			{
				model.addAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT,
						request.getAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT));

				travelFinderForm = (TravelFinderForm) request.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);
				travelFinderForm.setOnlyAccommodationChange(false);

				model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
			}
		}

	}

	protected void verifyAndUpdateDates(final TravelFinderForm travelFinderForm)
	{
		final String travelDepartureDate = travelFinderForm.getFareFinderForm().getDepartingDateTime();
		final String travelReturnDate = travelFinderForm.getFareFinderForm().getReturnDateTime();

		final String accommodationCheckInDate = travelFinderForm.getAccommodationFinderForm().getCheckInDateTime();
		final String accommodationCheckOutDate = travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime();

		boolean isSameDate = true;

		if (!StringUtils.equalsIgnoreCase(travelDepartureDate, accommodationCheckInDate))
		{
			travelFinderForm.getAccommodationFinderForm().setCheckInDateTime(travelDepartureDate);
			isSameDate = false;
		}

		if (!StringUtils.equalsIgnoreCase(travelReturnDate, accommodationCheckOutDate))
		{
			travelFinderForm.getAccommodationFinderForm().setCheckOutDateTime(travelReturnDate);
			isSameDate = false;
		}

		// if dates have been changed, we need to update accommodation finder and accommodation search url params that might be
		// currently in the session
		if (isSameDate)
		{
			return;
		}

		if (getSessionService().getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM) != null)
		{
			getSessionService().setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
					travelFinderForm.getAccommodationFinderForm());
		}

		if (getSessionService().getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_QUERY_STRING) != null)
		{
			getSessionService().setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_QUERY_STRING,
					buildUrlParameters(travelFinderForm.getAccommodationFinderForm()));
		}

	}

	protected TravelFinderForm initializeTravelFinderForm(final Model model)
	{
		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		travelFinderForm.setAccommodationFinderForm(initializeAccommodationFinderForm());
		travelFinderForm.setFareFinderForm(initializeFareFinderForm());
		model.addAttribute(VEHICLES, vehicleTypeFacade.getAllVehicleTypes());
		final String propertyValue = bcfConfigurablePropertiesService.getBcfPropertyValue(SKIP_VEHICLE_DIMENSIONS_PROPERTY);
		final List<String> vehicleToSkip = BCFPropertiesUtils.convertToList(propertyValue);
		model.addAttribute(VEHICLE_TO_SKIP, vehicleToSkip);
		model.addAttribute(SHOW_ACC_DESTINATION, Boolean.FALSE);
		model.addAttribute(SHOW_CHECKIN_CHECKOUT, Boolean.FALSE);
		travelFinderForm.setOnlyAccommodationChange(false);
		travelFinderForm.setReferer("/#tabs-2");
		return travelFinderForm;
	}

	protected AccommodationFinderForm initializeAccommodationFinderForm()
	{
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 0; i < maxAccommodationsQuantity; i++)
		{
			final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
			roomStayCandidateData.setRoomStayCandidateRefNumber(i);
			roomStayCandidates.add(roomStayCandidateData);
		}
		accommodationFinderForm.setRoomStayCandidates(roomStayCandidates);
		accommodationFinderForm.setNumberOfRooms(DEFAULT_ACCOMMODATION_QUANTITY);
		return accommodationFinderForm;
	}

	protected FareFinderForm initializeFareFinderForm()
	{
		final FareFinderForm fareFinderForm = new FareFinderForm();
		fareFinderForm.setTripType(bcfConfigurablePropertiesService.getBcfPropertyValue("defaultReturnTripType"));
		fareFinderForm.setTravellingWithChildren(false);
		fareFinderForm.setCarryingDangerousGoodsInReturn(false);
		// set default Cabin Class
		fareFinderForm.setCabinClass(ECONOMY_CABIN_CLASS_CODE);
		bcfCommonsaddonControllerUtil.setVehicleData(fareFinderForm);
		return fareFinderForm;
	}

	@RequestMapping(value = "/validate-travel-finder-form", method = RequestMethod.POST)
	public String validateTravelFinderForm(@Valid final TravelFinderForm travelFinderForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectModel)
	{
		setJourneyTypeInSession(travelFinderForm);
		initializeForms(travelFinderForm);
		validateForms(travelFinderForm, bindingResult);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfaccommodationsaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			model.addAttribute(BcfaccommodationsaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}
		redirectModel.addFlashAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		return BcfaccommodationsaddonControllerConstants.Views.Pages.FormErrors.FormErrorsResponse;
	}

	protected abstract void setJourneyTypeInSession(final TravelFinderForm travelFinderForm);

	protected void initializeForms(final TravelFinderForm travelFinderForm)
	{
		final AccommodationFinderForm accommodationFinderForm = travelFinderForm.getAccommodationFinderForm();
		final TravelRouteData travelRouteData = bcfTravelRouteFacade.getTravelRoute(travelFinderForm.getTravelRoute());
		final FareFinderForm fareFinderForm = travelFinderForm.getFareFinderForm();
		fareFinderForm.setArrivalLocation(travelRouteData.getDestination().getCode());
		fareFinderForm.setArrivalLocationName(travelRouteData.getDestination().getName());
		fareFinderForm.setDepartureLocation(travelRouteData.getOrigin().getCode());
		fareFinderForm.setDepartureLocationName(travelRouteData.getOrigin().getName());

		final Date departingDate = TravelDateUtils
				.convertStringDateToDate(accommodationFinderForm.getCheckInDateTime(), BcfcommonsaddonWebConstants.DATE_FORMAT);
		fareFinderForm.setDepartingDateTime(TravelDateUtils.convertDateToStringDate(departingDate,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));

		final Date returnDate = TravelDateUtils
				.convertStringDateToDate(accommodationFinderForm.getCheckOutDateTime(), BcfcommonsaddonWebConstants.DATE_FORMAT);
		fareFinderForm.setReturnDateTime(TravelDateUtils.convertDateToStringDate(returnDate,
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));

		if (StringUtils.isNotEmpty(fareFinderForm.getArrivalLocation()))
		{
			resolveDestinationLocation(accommodationFinderForm);
		}
		fareFinderForm.setPassengerTypeQuantityList(createPassengerTypeQuantityData(accommodationFinderForm));

	}

	/**
	 * This method resolves the destination to be used to build the query string for accommodation offering search. It
	 * tries to resolve the location from the transport facility, if no result is found we fall back to Google search and
	 * use geographic coordinates
	 *
	 * @param accommodationFinderForm
	 */
	protected void resolveDestinationLocation(final AccommodationFinderForm accommodationFinderForm)
	{
		final LocationData location = bcfTravelLocationFacade.getLocation(accommodationFinderForm.getDestinationLocation());

		if (Objects.nonNull(location))
		{
			accommodationFinderForm.setDestinationLocation(location.getCode());
			accommodationFinderForm.setDestinationLocationName(location.getName());
			accommodationFinderForm.setSuggestionType(SuggestionType.LOCATION.toString());
		}
	}

	protected GlobalSuggestionData getFirstValidResult(final LocationData location,
			final AccommodationFinderForm accommodationFinderForm, final String arrivalLocation)
	{
		GlobalSuggestionData firstValidResult = null;
		final List<GlobalSuggestionData> suggestionResults = Objects.nonNull(location)
				? accommodationSuggestionFacade.getLocationSuggestions(location.getName())
				: new ArrayList<>();
		if (CollectionUtils.isNotEmpty(suggestionResults))
		{
			firstValidResult = suggestionResults.stream().findFirst().get();
			accommodationFinderForm.setSuggestionType(SuggestionType.LOCATION.toString());
		}
		return firstValidResult;
	}

	protected void validateForms(final TravelFinderForm travelFinderForm, final BindingResult bindingResult)
	{
		validateFareFinderForm(fareFinderValidator, travelFinderForm.getFareFinderForm(), bindingResult,
				BcfvoyageaddonWebConstants.FARE_FINDER_FORM);

		if (!bindingResult.hasErrors())
		{
			validateAccommodationFinderForm(accommodationFinderValidator, travelFinderForm.getAccommodationFinderForm(),
					bindingResult, BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		}
	}

	protected void validateFareFinderForm(final AbstractTravelValidator fareFinderValidator, final FareFinderForm fareFinderForm,
			final BindingResult bindingResult, final String formName)
	{
		fareFinderValidator.setTargetForm(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		fareFinderValidator.setAttributePrefix(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
	}

	protected void validateAccommodationFinderForm(final AbstractTravelValidator accommodationFinderValidator,
			final AccommodationFinderForm accommodationFinderForm, final BindingResult bindingResult, final String formName)
	{
		accommodationFinderValidator.setTargetForm(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.setAttributePrefix(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.validate(accommodationFinderForm, bindingResult);
	}

	protected List<PassengerTypeQuantityData> createPassengerTypeQuantityData(
			final AccommodationFinderForm accommodationFinderForm)
	{
		List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final int numberOfRooms = accommodationFinderForm.getNumberOfRooms();
		if (accommodationFinderForm.getRoomStayCandidates().size() < numberOfRooms)
		{
			return passengerTypeQuantityList;
		}

		for (int i = 0; i < numberOfRooms; i++)
		{
			final List<PassengerTypeQuantityData> guestCounts = accommodationFinderForm.getRoomStayCandidates().get(i)
					.getPassengerTypeQuantityList();

			if (CollectionUtils.isEmpty(guestCounts))
			{
				continue;
			}

			passengerTypeQuantityList = bcfCommonsaddonControllerUtil.getPassengerTypeQuantityList(guestCounts,
					passengerTypeQuantityList);
		}
		return passengerTypeQuantityList;
	}

	protected String buildUrlParameters(final AccommodationFinderForm accommodationFinderForm)
	{
		final StringBuilder urlParameters = new StringBuilder();

		if (Arrays.asList(SuggestionType.LOCATION.toString(), StringUtils.EMPTY)
				.contains(accommodationFinderForm.getSuggestionType()))
		{
			urlParameters.append(BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			// We need to encode '|' character because it is not allowed in Spring Security 4
			String encodedDestinationLocation = accommodationFinderForm.getDestinationLocation();
			encodedDestinationLocation = encodedDestinationLocation.replaceAll("\\|", "%7C");
			urlParameters.append(encodedDestinationLocation);
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

			urlParameters.append(BcfaccommodationsaddonWebConstants.DESTINATION_LOCATION_NAME);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			urlParameters.append(accommodationFinderForm.getDestinationLocationName());
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

			urlParameters.append(BcfaccommodationsaddonWebConstants.SUGGESTION_TYPE);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			urlParameters.append(accommodationFinderForm.getSuggestionType());
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		}

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKIN_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getCheckInDateTime());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.CHECKOUT_DATE);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getCheckOutDateTime());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		urlParameters.append(TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(accommodationFinderForm.getNumberOfRooms());
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		try
		{
			final int numberOfRooms = accommodationFinderForm.getNumberOfRooms();
			for (int i = 0; i < numberOfRooms; i++)
			{
				final String result = getPassengersUrlParameters(accommodationFinderForm, i);
				urlParameters.append(ROOM + i);
				urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
				urlParameters.append(result);
				urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);

			}
		}
		catch (final NumberFormatException e)
		{
			LOG.error("Cannot parse number of rooms string to integer");
			LOG.error(e.getClass().getName() + " : " + e.getMessage());
			LOG.debug(e);

			return REDIRECT_PREFIX + "/";
		}
		String urlParametersString = urlParameters.toString();
		urlParametersString = urlParametersString.substring(0, urlParametersString.length() - 1);
		return "?" + urlParametersString;
	}

	protected String getPassengersUrlParameters(final AccommodationFinderForm accommodationFinderForm, final int i)
	{
		final StringBuilder guestsStringPerRoom = new StringBuilder();
		final List<PassengerTypeQuantityData> guestCounts = accommodationFinderForm.getRoomStayCandidates().get(i)
				.getPassengerTypeQuantityList();
		for (int j = 0; j < guestCounts.size(); j++)
		{
			final StringBuilder ages = new StringBuilder();
			if(CollectionUtils.isNotEmpty(guestCounts.get(j).getChildAges())){
				for (final int age : guestCounts.get(j).getChildAges())
				{
					ages.append(age).append("-");
				}
				ages.deleteCharAt(ages.length() - 1);

			}

			final String passengerType = guestCounts.get(j).getPassengerType().getCode();
			final int passengerQuantity = guestCounts.get(j).getQuantity();
			final StringBuilder guestParam = new StringBuilder(String.valueOf(passengerQuantity)).append("-").append(passengerType);
			if(ages.length()>0){
				guestParam.append("-").append(ages.toString());
			}
			guestsStringPerRoom.append(guestParam.toString());
			guestsStringPerRoom.append(",");
		}
		final String result = guestsStringPerRoom.toString();
		return result.substring(0, result.length() - 1);
	}
}
