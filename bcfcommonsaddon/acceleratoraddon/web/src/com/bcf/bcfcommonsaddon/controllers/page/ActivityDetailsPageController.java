/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.core.constants.BcfCoreConstants.AMEND_ORDER_CODE;
import static com.bcf.core.constants.BcfCoreConstants.HYPHEN;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.GuestType;
import com.bcf.core.model.order.ActivityOrderEntryInfoModel;
import com.bcf.core.order.BcfOrderService;
import com.bcf.core.util.StreamUtil;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.ActivityProductFacade;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.activityProduct.ActivityDetailsData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.packagedeal.search.impl.DefaultPackageSolrSearchFacade;
import com.bcf.facades.search.facetdata.PackageSearchPageData;
import com.bcf.facades.vacations.GuestData;


@Controller
@RequestMapping("/vacations/activities")
public class ActivityDetailsPageController extends TravelAbstractPageController
{
	private static final String ACTIVITY_DEATILS = "activityDetails";
	private static final String ACTIVITY_DEATILS_PAGE = "activityDetailsPage";
	private static final String ACTIVITY_AVAILIBILITY_START = "activityAvailibilityStart";
	private static final String ACTIVITY_AVAILIBILITY_END = "activityAvailibilityEnd";
	private static final String ADD_ACTIVITY_TO_CART_DATA = "addActivityToCartData";
	public static final String DESTINATION_PACKAGES = "destinationPackages";

	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@Resource(name = "dealCartFacade")
	private BcfDealCartFacade dealCartFacade;

	@Resource(name = "packageSolrSearchFacade")
	private DefaultPackageSolrSearchFacade packageSolrSearchFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "orderService")
	private BcfOrderService bcfOrderService;

	@RequestMapping(value = "/{activityCode}", method = RequestMethod.GET)
	public String getActivityDetails(@PathVariable("activityCode") final String activityCode,
			@RequestParam(value = "changeActivity", required = false) final boolean changeActivity,
			@RequestParam(value = "changeActivityCode", required = false) final String changeActivityCode,
			@RequestParam(value = "date", required = false) final String changeActivityDate,
			@RequestParam(value = "time", required = false) final String changeActivityTime,
			@RequestParam(value = "changeActivityRef", required = false) final String changeActivityRef,
			final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final ActivityDetailsData activityDetails = activityProductFacade.getActivityDetailsData(activityCode);
		model.addAttribute(ACTIVITY_DEATILS, activityDetails);

		final AddActivityToCartData addActivityToCartData = getAddActivityToCartData(activityDetails);

		if (CollectionUtils.isNotEmpty(activityDetails.getActivitySchedules()))
		{
			final Date minDate = activityDetails.getActivitySchedules().stream().map(ActivityScheduleData::getStartDate)
					.max(Date::compareTo).get();
			final Date maxDate = activityDetails.getActivitySchedules().stream().map(ActivityScheduleData::getEndDate)
					.max(Date::compareTo).get();

			model.addAttribute(ACTIVITY_AVAILIBILITY_START, minDate);
			model.addAttribute(ACTIVITY_AVAILIBILITY_END, maxDate);
		}

		if (activityCode != null)
		{
			final PackageSearchPageData packageSearchPageData = packageSolrSearchFacade.searchForActivity(activityCode);
			if (packageSearchPageData != null && packageSearchPageData.getResults() != null)
			{
				model.addAttribute(DESTINATION_PACKAGES, packageSearchPageData.getResults());
			}
		}
		populateChangeActivityRelatedFieldsInModel(changeActivity, changeActivityCode, changeActivityDate, changeActivityTime,
				changeActivityRef, model);
		populateAmendOrderRelatedFieldsInModel(addActivityToCartData, changeActivityDate, changeActivityTime, changeActivityRef,
				model);
		model.addAttribute(ADD_ACTIVITY_TO_CART_DATA, addActivityToCartData);
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACTIVITY_DEATILS_PAGE);
		storeCmsPageInModel(model, contentPageModel);

		final String activityName = activityDetails.getName();
		String activityCity = "";
		if (Objects.nonNull(activityDetails.getDestination()))
		{
			activityCity = activityDetails.getDestination().getName();
		}
		final String[] arguments = { activityName, activityCity };
		setUpMetaDataForContentPage(model, contentPageModel, arguments);
		setUpDynamicOGGraphData(model, contentPageModel, request, arguments);

		return getViewForPage(model);
	}

	private void populateAmendOrderRelatedFieldsInModel(final AddActivityToCartData addActivityToCartData, final String date,
			final String time, final String changeActivityRef, final Model model)
	{
		final Object isAmendOrder = getSessionService().getAttribute(BcfCoreConstants.IS_AMEND_ORDER);
		final Object orderCodeObj = getSessionService().getAttribute(BcfCoreConstants.AMEND_ORDER_CODE);
		if (Objects.nonNull(isAmendOrder) && (Boolean) isAmendOrder && Objects.nonNull(orderCodeObj) && (
				StringUtils.isNotBlank(date)
						|| StringUtils.isNotBlank(time)))
		{

			final String orderCode = String.valueOf(orderCodeObj);
			model.addAttribute(BcfCoreConstants.IS_AMEND_ORDER, Boolean.TRUE);
			model.addAttribute(AMEND_ORDER_CODE, orderCode);
			addActivityToCartData.setActivityDate(date);
			addActivityToCartData.setStartTime(time);

			//populating guest data from order
			final OrderModel order = bcfOrderService.getOrderByCode(orderCode);

			if (Objects.isNull(order))
			{
				return;
			}

			final List<AbstractOrderEntryModel> orderEntriesWithActivityProduct;
			if (StringUtils.isNotBlank(changeActivityRef))
			{
				Stream<String> changedActivityEntryNumbersStr = Stream.of(changeActivityRef.split(HYPHEN));
				List<Integer> changedActivityEntryNumbers
						= changedActivityEntryNumbersStr.map(Integer::valueOf).collect(Collectors.toList());
				orderEntriesWithActivityProduct = StreamUtil.safeStream(order.getEntries())
						.filter(e -> changedActivityEntryNumbers.contains(e.getEntryNumber())).collect(Collectors.toList());
			}
			else
			{
				orderEntriesWithActivityProduct = StreamUtil.safeStream(order.getEntries())
						.filter(e -> e.getProduct().getCode().equals(addActivityToCartData.getActivityCode()))
						.collect(Collectors.toList());
			}
			orderEntriesWithActivityProduct.forEach(entry -> {

				final ActivityOrderEntryInfoModel activityOrderEntryInfo = entry.getActivityOrderEntryInfo();
				if (dealCartFacade.isActivityDateTimeMatchesInActivityOrderEntryInfo(activityOrderEntryInfo, date, time))
				{
					final GuestType guestType = activityOrderEntryInfo.getGuestType();
					final Optional<GuestData> matchedGuestData = addActivityToCartData.getGuestData().stream()
							.filter(guestData -> guestData.getGuestType().equalsIgnoreCase(guestType.getCode())).findFirst();
					matchedGuestData.ifPresent(guestData -> guestData.setQuantity(entry.getQuantity().intValue()));
				}
			});

		}
	}

	private AddActivityToCartData getAddActivityToCartData(final ActivityDetailsData activityDetails)
	{
		final AddActivityToCartData addActivityToCartData = new AddActivityToCartData();
		addActivityToCartData.setActivityCode(activityDetails.getCode());
		addActivityToCartData.setJourneyRefNumber(0);

		final List<GuestData> guestDatas = activityDetails.getPassengerTypes().stream().map(passengerType -> {
			final GuestData guestData = new GuestData();
			guestData.setAgeRange(passengerType.getName());
			guestData.setGuestType(passengerType.getCode());
			return guestData;
		}).collect(Collectors.toList());
		addActivityToCartData.setGuestData(guestDatas);
		addActivityToCartData.setMaxGuestCountPerPaxType(activityProductFacade.getMaxPaxAllowedPerPaxTypePerActivity());

		return addActivityToCartData;
	}

	@RequestMapping(value = "/remove-activity/{activityProductCode}/{journeyRefNumber}/{date}", method = RequestMethod.GET)
	public String removeActivityForJourney(@PathVariable final String activityProductCode,
			@PathVariable final int journeyRefNumber, @PathVariable final String date, @RequestParam final String time,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		dealCartFacade.removeSelectedActivityEntries(activityProductCode, journeyRefNumber, date, time);
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACTIVITY_REMOVED, true);
		final String referer = request.getHeader("referer");
		return REDIRECT_PREFIX + referer;
	}

	@RequestMapping(value = "/cancel-activity/{activityProductCode}/{journeyRefNumber}/{date}", method = RequestMethod.GET)
	public String cancelActivity(@PathVariable final String activityProductCode,
			@PathVariable final int journeyRefNumber, @PathVariable final String date, @RequestParam final String time,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		dealCartFacade.removeSelectedActivityEntries(activityProductCode, journeyRefNumber, date, time);
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACTIVITY_REMOVED, true);
		return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();
	}

	@RequestMapping(value = "/remove-activity/{activityEntryNumbers}", method = RequestMethod.GET)
	public String removeActivity(@PathVariable final String activityEntryNumbers,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		dealCartFacade.removeSelectedActivityEntries(activityEntryNumbers);
		bcfTravelCartFacadeHelper.updateAvailabilityStatus();
		bcfTravelCartFacadeHelper.calculateChangeFees();
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACTIVITY_REMOVED, true);
		return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();
	}
}
