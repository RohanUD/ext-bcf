/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.controllers.pages.AbstractAccommodationPageController;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AdjustCostForm;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.util.BcfAccommodationControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfcommonsaddon.validators.BCFAdjustCostValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.managebooking.data.AdjustCostData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
public class PackageManageBookingController extends AbstractAccommodationPageController
{

	private static final Logger LOG = Logger.getLogger(PackageManageBookingController.class);

	private static final String AMEND_ACCOMMODATION_FAILED = "text.page.managemybooking.amend.accommodation.failed";
	protected static final String ADJUST_COST_INTEGRATION_EXCETION = "text.adjustcost.integration.exception";
	private static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String TRAVEL_ROUTE_DATA = "travelRouteData";
	private static final String QUEST_QUANTITY = "guestQuantity";
	private static final String JOURNEY_REF = "journeyRef";
	private static final String TRAVEL_FINDER_FORM = "travelFinderForm";
	private static final String BOOKING_REFERENCE = "bookingReference";
	private static final String ROOM_STAY_CANDIDATES = "roomStayCandidates";


	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "bcfTravelLocationService")
	private BcfTravelLocationService bcfTravelLocationService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "bcfAccommodationControllerUtil")
	private BcfAccommodationControllerUtil bcfAccommodationControllerUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfAdjustCostValidator")
	private BCFAdjustCostValidator bcfAdjustCostValidator;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@RequestMapping(value = "/manage-booking/amend-accommodation/{journeyRef}/{bookingReference}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String amendAccommodation(@PathVariable(value = "bookingReference") final String bookingReference,
			@PathVariable(value = "journeyRef") final Integer journeyRef,
			final RedirectAttributes redirectAttributes, final Model model)
	{

		try
		{
			sessionService.setAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE, bookingReference);
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, false);
		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACCOMMODATION_FAILED);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		final CartModel sessionAmendedCart = bcfTravelCartService.getSessionCart();
		final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);
		final List<AbstractOrderEntryModel> inBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 1);


		final TravelRouteModel outBoundtravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();

		bcfAccommodationControllerUtil.initializeAccommodationFinderForm(accommodationFinderForm, sessionAmendedCart, journeyRef);

		final FareFinderForm fareFinderForm = new FareFinderForm();
		bcfAccommodationControllerUtil
				.initializeFareFinderForm(fareFinderForm, accommodationFinderForm, outBoundEntries, inBoundEntries, journeyRef);
		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		travelFinderForm.setAccommodationFinderForm(accommodationFinderForm);
		travelFinderForm.setFareFinderForm(fareFinderForm);
		travelFinderForm.setTravelRoute(outBoundtravelRoute.getCode());

		final List<TravelRouteData> travelRouteData = bcfTravelRouteFacade
				.getTravelRoutesForAccommodationLocation(accommodationFinderForm.getDestinationLocation());
		redirectAttributes.addFlashAttribute(TRAVEL_ROUTE_DATA, travelRouteData);
		model.addAttribute(TRAVEL_ROUTE_DATA, travelRouteData);

		final List<RoomStayCandidateData> roomStayCandidates = bcfAccommodationControllerUtil.populateRoomStayCandidates();

		final List<String> passengerQuantity = bcfAccommodationControllerUtil.populatePassengersQuantity();
		final List<Integer> accommodationsQuantity = bcfAccommodationControllerUtil.populateAccommodationsQuantity();

		redirectAttributes.addFlashAttribute(TRAVEL_FINDER_FORM, travelFinderForm);

		redirectAttributes.addFlashAttribute(QUEST_QUANTITY, passengerQuantity);
		redirectAttributes.addFlashAttribute(JOURNEY_REF, journeyRef);
		redirectAttributes.addFlashAttribute(BOOKING_REFERENCE, bookingReference);
		redirectAttributes.addFlashAttribute(ROOM_STAY_CANDIDATES,  roomStayCandidates);
		redirectAttributes.addFlashAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY,accommodationsQuantity);
		redirectAttributes.addFlashAttribute("hasAmendAccommodation", true);
		model.addAttribute(TRAVEL_FINDER_FORM, travelFinderForm);

		model.addAttribute(QUEST_QUANTITY, passengerQuantity);
		model.addAttribute(JOURNEY_REF, journeyRef);
		model.addAttribute(BOOKING_REFERENCE, bookingReference);
		model.addAttribute(ROOM_STAY_CANDIDATES, roomStayCandidates);
		model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY, accommodationsQuantity);
		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		model.addAttribute("hasAmendAccommodation", true);

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;


	}

	@RequestMapping(value = "/manage-booking/amend-sailing/{journeyRef}/{bookingReference}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String amendSailing(@PathVariable(value = "bookingReference") final String bookingReference,
			@PathVariable(value = "journeyRef") final Integer journeyRef,
			final RedirectAttributes redirectAttributes, final Model model)
	{
		try
		{
			sessionService.setAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE, bookingReference);
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, true);

		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACCOMMODATION_FAILED);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		final CartModel sessionAmendedCart = bcfTravelCartService.getSessionCart();
		final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);
		final List<AbstractOrderEntryModel> inBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 1);

		final TravelRouteModel outBoundtravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();
		getSessionService().setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		bcfAccommodationControllerUtil.initializeAccommodationFinderForm(accommodationFinderForm, sessionAmendedCart, journeyRef);

		final FareFinderForm fareFinderForm = new FareFinderForm();
		bcfAccommodationControllerUtil.initializeFareFinderForm(fareFinderForm, accommodationFinderForm, outBoundEntries,
				inBoundEntries, journeyRef);

		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		travelFinderForm.setAccommodationFinderForm(accommodationFinderForm);
		travelFinderForm.setFareFinderForm(fareFinderForm);
		travelFinderForm.setTravelRoute(outBoundtravelRoute.getCode());


		fareFinderForm.setDepartingDateTime(travelFinderForm.getAccommodationFinderForm().getCheckInDateTime());
		fareFinderForm.setReturnDateTime(travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime());

		final List<TravelRouteData> travelRouteData = bcfTravelRouteFacade
				.getTravelRoutesForAccommodationLocation(accommodationFinderForm.getDestinationLocation());
		travelFinderForm.setTravelRoute(
				bcfAccommodationControllerUtil.getTravelRouteForForm(travelRouteData, travelFinderForm.getTravelRoute()));
		getSessionService().setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, journeyRef);
		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		model.addAttribute(TRAVEL_FINDER_FORM, travelFinderForm);
		model.addAttribute(TRAVEL_ROUTE_DATA, travelRouteData);
		model.addAttribute(QUEST_QUANTITY, bcfAccommodationControllerUtil.populatePassengersQuantity());
		model.addAttribute(JOURNEY_REF, journeyRef);
		model.addAttribute(BOOKING_REFERENCE, bookingReference);
		model.addAttribute(ROOM_STAY_CANDIDATES, bcfAccommodationControllerUtil.populateRoomStayCandidates());
		model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY,
				bcfAccommodationControllerUtil.populateAccommodationsQuantity());
		redirectAttributes.addFlashAttribute(TRAVEL_FINDER_FORM, travelFinderForm);

		return REDIRECT_PREFIX + "/package-ferry-passenger-info";


	}

	@RequestMapping(value = "/manage-booking/amend-alacarte-booking/{bookingReference}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String amendAlacarteBooking(@PathVariable(value = "bookingReference") final String bookingReference,
			final RedirectAttributes redirectAttributes, final Model model)
	{

		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, true);
			bcfTravelCartFacadeHelper.setCurrentJourneyRefNum();
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_ALACARTE);
			sessionService.setAttribute(BcfCoreConstants.IS_AMEND_ORDER, Boolean.TRUE);
			sessionService.setAttribute(BcfCoreConstants.AMEND_ORDER_CODE, bookingReference);
		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACCOMMODATION_FAILED);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}
		return REDIRECT_PREFIX + "/alacarte-review";
	}

	@RequestMapping(value = "/manage-booking/adjust-cost/{bookingReference}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String adjustCostForBooking(@PathVariable(value = "bookingReference") final String bookingReference,
			final RedirectAttributes redirectAttributes, final Model model)
	{

		final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);
		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getbcfGlobalReservationDataList(order);

		final List<AdjustCostData> accommodationAdjustCostDatas = bcfAccommodationControllerUtil
				.populateAccommodationAdjustCostData(
				bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas().stream().flatMap(
						packageReservationData -> StreamUtil
								.safeStream(packageReservationData.getAccommodationReservationData().getRoomStays())).collect(
						Collectors.toList()));
		List<AdjustCostData> activityAdjustCostDatas = new ArrayList<>();

		if (bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas().get(0).getActivityReservations() != null)
		{
			final List<ActivityReservationData> activityReservationDatas = StreamUtil
					.safeStream(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()).flatMap(
							packageReservationData -> StreamUtil
									.safeStream(packageReservationData.getActivityReservations().getActivityReservationDatas())).collect(
							Collectors.toList());
			activityAdjustCostDatas = bcfAccommodationControllerUtil.populateActivityAdjustCostData(activityReservationDatas);

		}
		bookingFacade.getAdjustedCostForBooking(accommodationAdjustCostDatas, activityAdjustCostDatas, bookingReference);
		final AdjustCostForm adjustCostForm = new AdjustCostForm();
		adjustCostForm.setAdjustAccommodationCostDatas(accommodationAdjustCostDatas);
		adjustCostForm.setAdjustActivityCostDatas(activityAdjustCostDatas);

		redirectAttributes.addFlashAttribute("adjustCostForm", adjustCostForm);
		redirectAttributes.addFlashAttribute(BOOKING_REFERENCE, bookingReference);
		redirectAttributes.addFlashAttribute("hasAdjustCost", true);

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;

	}

	@RequestMapping(value = "/manage-booking/adjust-cost", method = RequestMethod.POST)
	public String adjustCost(
			@ModelAttribute(value = "bookingReference") final String bookingReference,
			final AdjustCostForm adjustCostForm, final BindingResult bindingResult,
			final RedirectAttributes redirectModel, final Model model)
	{

		bcfAdjustCostValidator.validate(adjustCostForm, bindingResult);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		redirectModel.addFlashAttribute("adjustCostErrorFlag", hasErrorFlag);
		if (hasErrorFlag)
		{
			redirectModel.addFlashAttribute("hasAdjustCost", true);
			redirectModel
					.addFlashAttribute(BcfcommonsaddonWebConstants.ERROR_MESSAGE, bindingResult.getFieldErrors().get(0).getCode());
			redirectModel.addFlashAttribute("adjustCostForm", adjustCostForm);
			redirectModel.addFlashAttribute(BOOKING_REFERENCE, bookingReference);

		}
		else
		{
			try
			{
				bookingFacade.adjustCostForBooking(adjustCostForm.getAdjustAccommodationCostDatas(),
						adjustCostForm.getAdjustActivityCostDatas(), bookingReference);
			}
			catch (final IntegrationException e)
			{
				redirectModel.addFlashAttribute(BcfcommonsaddonWebConstants.ERROR_MESSAGE, ADJUST_COST_INTEGRATION_EXCETION);
				LOG.error("Integration exception occured while sending financial data to kafka", e);
			}
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"adjustcost.added.confirmation");
		}

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
	}

	@RequestMapping(value = "/manage-booking/amend-activity/{journeyRef}/{bookingReference}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String amendActivity(@PathVariable(value = "bookingReference") final String bookingReference,
			@PathVariable(value = "journeyRef") final Integer journeyRef,
			final RedirectAttributes redirectAttributes, final Model model)
	{
		try
		{
			sessionService.setAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE, bookingReference);
			bcfTravelCartFacade.createAndSetAmendableCart(bookingReference, true);
		}
		catch (final IntegrationException | BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ACCOMMODATION_FAILED);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}
		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		final CartModel sessionAmendedCart = bcfTravelCartService.getSessionCart();
		final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);
		final List<AbstractOrderEntryModel> inBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 1);

		final TravelRouteModel outBoundtravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();

		bcfAccommodationControllerUtil.initializeAccommodationFinderForm(accommodationFinderForm, sessionAmendedCart, journeyRef);

		final FareFinderForm fareFinderForm = new FareFinderForm();
		bcfAccommodationControllerUtil
				.initializeFareFinderForm(fareFinderForm, accommodationFinderForm, outBoundEntries, inBoundEntries, journeyRef);

		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		travelFinderForm.setAccommodationFinderForm(accommodationFinderForm);
		travelFinderForm.setFareFinderForm(fareFinderForm);
		travelFinderForm.setTravelRoute(outBoundtravelRoute.getCode());

		fareFinderForm.setDepartingDateTime(travelFinderForm.getAccommodationFinderForm().getCheckInDateTime());
		fareFinderForm.setReturnDateTime(travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime());

		final List<TravelRouteData> travelRouteData = bcfTravelRouteFacade
				.getTravelRoutesForAccommodationLocation(accommodationFinderForm.getDestinationLocation());

		getSessionService().setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, journeyRef);
		model.addAttribute(TRAVEL_FINDER_FORM, travelFinderForm);
		model.addAttribute(TRAVEL_ROUTE_DATA, travelRouteData);
		model.addAttribute(QUEST_QUANTITY, bcfAccommodationControllerUtil.populatePassengersQuantity());
		model.addAttribute(JOURNEY_REF, journeyRef);
		model.addAttribute(BOOKING_REFERENCE, bookingReference);
		model.addAttribute(ROOM_STAY_CANDIDATES, bcfAccommodationControllerUtil.populateRoomStayCandidates());
		model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY,
				bcfAccommodationControllerUtil.populateAccommodationsQuantity());
		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		redirectAttributes.addFlashAttribute(TRAVEL_FINDER_FORM, travelFinderForm);

		sessionService.setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
				travelFinderForm.getAccommodationFinderForm());

		return REDIRECT_PREFIX + "/deal-activity-listing";


	}

	@RequestMapping(value = "/manage-booking/amend-accommodation", method = RequestMethod.POST)
	@RequireHardLogIn
	public String amendAccommodation(@ModelAttribute("travelFinderForm") final TravelFinderForm travelFinderForm,
			@RequestParam(value = "bookingReference") final String bookingReference,
			@RequestParam("journeyRef") final int journeyRef, final RedirectAttributes redirectModel, final Model model,
			final BindingResult bindingResult)
	{
		final List<AbstractOrderEntryModel> outBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 0);
		final List<AbstractOrderEntryModel> inBoundEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRef, 1);

		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		final CartModel sessionAmendedCart = bcfTravelCartService.getSessionCart();

		final TravelRouteModel outBoundTravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();

		travelFinderForm.getAccommodationFinderForm().setRoomStayCandidates(bcfControllerUtil
				.cleanUpChildRoomStayCandidates(travelFinderForm.getAccommodationFinderForm().getNumberOfRooms(),
						travelFinderForm.getAccommodationFinderForm().getRoomStayCandidates()));
		final FareFinderForm fareFinderForm = new FareFinderForm();
		bcfAccommodationControllerUtil
				.initializeFareFinderForm(fareFinderForm, travelFinderForm.getAccommodationFinderForm(), outBoundEntries,
						inBoundEntries, journeyRef);
		travelFinderForm.setFareFinderForm(fareFinderForm);

		if (travelFinderForm.getTravelRoute() == null)
		{
			travelFinderForm.setTravelRoute(outBoundTravelRoute.getCode());
		}


		bcfAccommodationControllerUtil.validateForms(travelFinderForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(JOURNEY_REF, journeyRef);

			final List<TravelRouteData> travelRouteData = bcfTravelRouteFacade
					.getTravelRoutesForAccommodationLocation(travelFinderForm.getAccommodationFinderForm().getDestinationLocation());

			travelFinderForm.setTravelRoute(
					bcfAccommodationControllerUtil.getTravelRouteForForm(travelRouteData, travelFinderForm.getTravelRoute()));

			redirectModel.addFlashAttribute(TRAVEL_ROUTE_DATA, travelRouteData);
			redirectModel.addFlashAttribute(QUEST_QUANTITY, bcfAccommodationControllerUtil.populatePassengersQuantity());
			redirectModel.addFlashAttribute(ROOM_STAY_CANDIDATES, bcfAccommodationControllerUtil.populateRoomStayCandidates());
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY,
					bcfAccommodationControllerUtil.populateAccommodationsQuantity());
			redirectModel.addFlashAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, bindingResult);
			redirectModel.addFlashAttribute("hasAmendAccommodationError", true);
			redirectModel.addFlashAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}


		fareFinderForm.setDepartingDateTime(travelFinderForm.getAccommodationFinderForm().getCheckInDateTime());
		fareFinderForm.setReturnDateTime(travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime());
		getSessionService().setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, journeyRef);
		final AccommodationFinderForm existingAccommodationFinderForm = new AccommodationFinderForm();
		bcfAccommodationControllerUtil
				.initializeAccommodationFinderForm(existingAccommodationFinderForm, sessionAmendedCart, journeyRef);

		travelFinderForm.setOnlyAccommodationChange(bcfAccommodationControllerUtil
				.isOnlyAccommodationChange(travelFinderForm.getAccommodationFinderForm(), existingAccommodationFinderForm,
						outBoundTravelRoute.getCode(), travelFinderForm.getTravelRoute()));
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.TRAVEL_FINDER_FORM,
				travelFinderForm);
		return REDIRECT_PREFIX + "/package-listing";
	}


}
