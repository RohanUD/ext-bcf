/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_TOTAL_DATA;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Controller for  Package Accessibility Needs Component Controller
 */
@Controller("PackageFerryInfoPageController")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping("/package-ferry-passenger-info")
public class PackageFerryInfoPageController extends AbstractPackagePageController
{
	private static final String PACKAGE_FERRY_INFO_CMS_PAGE = "packageFerryInfoPage";

	@Resource
	private SessionService sessionService;

	@Resource(name = "bcfSpecialAssistanceFacade")
	private BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getPackageFerryInfoPage(
			@Valid @ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			@RequestParam(value = "availabilityStatus", required = false, defaultValue = "AVAILABLE") final String availabilityStatus,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			request.setAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, bindingResult);
		}
		final TotalFareData bcfGlobalReservationData = globalReservationFacade
				.getGlobalReservationForTotalPricewithTax(
						Arrays.asList(OrderEntryType.TRANSPORT, OrderEntryType.ACCOMMODATION));
		model.addAttribute(GLOBAL_RESERVATION_TOTAL_DATA, bcfGlobalReservationData);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, travelFinderForm.getAccommodationFinderForm());
		model.addAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, travelFinderForm.getFareFinderForm());
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_STATUS, availabilityStatus);
		travelFinderForm.setAvailabilityStatus(availabilityStatus);
		final String travelRoute = travelFinderForm.getTravelRoute();
		final TravelRouteData travelRouteData = bcfTravelRouteFacade.getTravelRoute(travelRoute);
		setVehicleDimensionThreshold(travelRouteData, travelFinderForm.getFareFinderForm());
		travelFinderForm.setReservable(travelRouteData.isReservable());
		if (travelRouteData.isReservable())
		{
			// Normal flow
			model.addAttribute(BcfcommonsaddonWebConstants.NEXT_URL,
					BcfstorefrontaddonWebConstants.PACKAGE_FERRY_SELECTION_PAGE_PATH);
		}
		else
		{
			// Default flow
			model.addAttribute(BcfFacadesConstants.NEXT_URL, BcfstorefrontaddonWebConstants.PACKAGE_FERRY_REVIEW_PAGE_PATH);
		}

		final ContentPageModel packageFerryInfoPage = getContentPageForLabelOrId(PACKAGE_FERRY_INFO_CMS_PAGE);

		storeCmsPageInModel(model, packageFerryInfoPage);
		setUpMetaDataForContentPage(model, packageFerryInfoPage);


		final CartTimerData cartTimerData = bcfCartTimerFacade.getCartStartTimerDetails();
		if (cartTimerData != null)
		{
			model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
			model.addAttribute("holdTimeMessage", cartTimerData.getHoldTimeMessage());
		}

		return getViewForPage(model);
	}

	@RequestMapping(value = "/retrieve-accessibility-needs", method = RequestMethod.POST)
	public String fetchAccessibilityPerPAXPackage(
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult,
			final Model model)
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList;
		final List<AccessibilityRequestData> accessibilityRequestDataList;

		passengerTypeQuantityList = bcfvoyageaddonComponentControllerUtil.getFilteredPassengerTypeQuantityDataList(
				travelFinderForm.getFareFinderForm().getPassengerTypeQuantityList());

		accessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil.getFilteredAccessibilityRequestDataList(
				travelFinderForm.getFareFinderForm().getAccessibilityRequestDataList());

		passengerTypeQuantityList.stream()
				.forEach(passengerTypeQuantityData -> IntStream.range(0, passengerTypeQuantityData.getQuantity()).forEach(value -> {
					final AccessibilityRequestData accessibilityRequestData = new AccessibilityRequestData();
					accessibilityRequestData.setPassengerType(passengerTypeQuantityData.getPassengerType());
					accessibilityRequestData.setSpecialServiceRequestDataList(
							bcfvoyageaddonComponentControllerUtil
									.getSpecialServiceDisplayList(bcfSpecialAssistanceFacade.getAllSpecialServiceRequest(),
											bcfvoyageaddonComponentControllerUtil.getSpecialServiceRequestProductsSequence()));
					accessibilityRequestData.setAncillaryRequestDataList(
							bcfvoyageaddonComponentControllerUtil
									.getProductListByDisplaySequence(bcfvoyageaddonComponentControllerUtil.getAccessibilityProductData(),
											bcfvoyageaddonComponentControllerUtil.getAccessibilityRequestProductsSequence()));
					accessibilityRequestDataList.add(accessibilityRequestData);
				}));
		travelFinderForm.getFareFinderForm().setAccessibilityRequestDataList(accessibilityRequestDataList);
		model.addAttribute(BcfvoyageaddonWebConstants.BOUNDFOR, "");
		model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		return BcfcommonsaddonControllerConstants.Views.CMS.accessibilityNeedsPerPAX;
	}

	public void setVehicleDimensionThreshold(final TravelRouteData travelRouteData, final FareFinderForm fareFinderForm)
	{
		if (Objects.nonNull(travelRouteData) && StringUtils.isNotBlank(travelRouteData.getCode()))
		{
			final TransportFacilityData originFacility = travelRouteData.getOrigin();
			final TransportFacilityData destinationFacility = travelRouteData.getDestination();
			if (Objects.nonNull(originFacility) && Objects.nonNull(destinationFacility))
			{
				fareFinderForm.setStandardVehicleHeightPortLimit(
						Math.min(originFacility.getStandardVehicleHeight(), destinationFacility.getStandardVehicleHeight()));
				fareFinderForm.setStandardVehicleLengthPortLimit(
						Math.min(originFacility.getStandardVehicleLength(), destinationFacility.getStandardVehicleLength()));
			}
		}
	}
}

