/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.GuestOccupancyData;
import de.hybris.platform.commercefacades.accommodation.ProfileData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;
import de.hybris.platform.travelfacades.order.AccommodationCartFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.LeadGuestDetailsForm;
import com.bcf.bcfaccommodationsaddon.validators.GuestDetailsValidator;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.PersonalDetailsForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.security.impl.B2BCheckOutAuthenticatonValidator;
import com.bcf.bcfvoyageaddon.forms.PassengerInformationForm;
import com.bcf.bcfvoyageaddon.forms.TravellerForm;
import com.bcf.bcfvoyageaddon.forms.validation.BCFTravellerFormValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.BCFTravellerFacade;


/**
 * Controller for Personal Details page
 */
@Controller
@SessionAttributes(BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_FORM)
@RequestMapping("/checkout/personal-details")
public class PersonalDetailsPageController extends TravelAbstractPageController
{
	private static final String ADULT_AGE_RANGE = "adultsAgeRange";
	private static final String CHILDREN_AGE_RANGE = "childrenAgeRange";
	private static final String INFANT_AGE_RANGE = "infantsAgeRange";
	private static final String TITLES = "titles";
	private static final String ADULT_TITLES = "adultsTitles";
	private static final String CHILDREN_TITLES = "childrenTitles";
	private static final String REASON_FOR_TRAVEL_OPTIONS = "reasonForTravelOptions";
	private static final String SAVED_TRAVELLERS = "savedTravellers";
	private static final String TRANSPORT_RESERVATION_DATA = "transportReservationData";
	private static final String TRAVELLERS_NAMES_MAP = "travellersNamesMap";

	private static final String HOME_PAGE_PATH = "/";

	private static final String PERSONAL_DETAILS_CMS_PAGE = "personalDetailsPage";
	private static final String HOURS = "hours";
	private static final String MINUTES = "minutes";
	private static final String PASSENGER_TYPE_QUANITY_MAP = "passengerTypeMaxQuantityMapPerRoom";
	private static final String DATE_PATTERN = "datePattern";

	private static final String DEFAULT_GUEST_CHECK_IN_TIME = " 12:00:00";
	private static final String SAVED_VEHICLES = "savedVehicleTravellers";
	private static final String RESERVATION_DATA = "reservationData";

	// Populated through properties files
	private String[] adultAgesRange;
	private String[] childrenAgeRange;
	private String[] infantAgeRange;
	private String[] adultTitles;
	private String[] childrenTitles;
	private Integer maxGuestQuantity;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "packageFacade")
	private PackageFacade packageFacade;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "bcfTravellerFormValidator")
	private BCFTravellerFormValidator travellerFormValidator;

	@Resource(name = "b2BCheckOutAuthenticatonValidator")
	private B2BCheckOutAuthenticatonValidator b2BCheckOutAuthenticatonValidator;

	@Resource(name = "leadGuestDetailsFormsValidator")
	private GuestDetailsValidator leadGuestDetailsFormsValidator;

	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Resource(name = "accommodationCartFacade")
	private AccommodationCartFacade accommodationCartFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;


	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getPersonalDetailsPage(final Model model, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		disableCachingForResponse(response);
		if (getTravelCustomerFacade().isCurrentUserB2bCustomer())
		{
			final ValidationResults validationResult = b2BCheckOutAuthenticatonValidator.validate(redirectAttributes);
			if (!ValidationResults.SUCCESS.getResult().equals(validationResult.getResult()))
			{
				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}
		}

		if (!travelCartFacade.isCurrentCartValid())
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}

		final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
		final List<String> routes = Stream.of(routeType.split(",")).collect(Collectors.toList());

		final CartFilterParamsDto cartFilterParams = bcfTravelCartFacade
				.initializeCartFilterParams(null, null, false, routes);

		final BCFTravellersData bcfTravellersData = travellerFacade.getTravellersGroupedByJourney(cartFilterParams);
		model.addAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, bcfTravellersData);

		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade
				.getGlobalReservationForEntryType(
						Arrays.asList(OrderEntryType.TRANSPORT, OrderEntryType.ACCOMMODATION, OrderEntryType.DEAL));

		final List<TravellerData> travellers = travellerFacade.getTravellersForCartEntries();

		model.addAttribute(TRAVELLERS_NAMES_MAP, travellerFacade.populateTravellersNamesMap(travellers));
		model.addAttribute(ADULT_AGE_RANGE, adultAgesRange);
		model.addAttribute(CHILDREN_AGE_RANGE, childrenAgeRange);
		model.addAttribute(INFANT_AGE_RANGE, infantAgeRange);
		model.addAttribute(TITLES, userFacade.getTitles());
		model.addAttribute(REASON_FOR_TRAVEL_OPTIONS, travellerFacade.getReasonForTravelTypes());

		final List<TravellerData> savedTravellers = travellerFacade.getSavedTravellersForCurrentUser();
		final List<TravellerData> savedPassengerTravellers = savedTravellers.stream()
				.filter(travellerData -> TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER.equals(travellerData.getTravellerType()))
				.collect(Collectors.toList());

		final List<TravellerData> savedVehicleTravellers = savedTravellers.stream()
				.filter(travellerData -> BcfFacadesConstants.TRAVELLER_TYPE_VEHICLE.equals(travellerData.getTravellerType()))
				.collect(Collectors.toList());

		model.addAttribute(SAVED_TRAVELLERS, savedPassengerTravellers);
		model.addAttribute(SAVED_VEHICLES, savedVehicleTravellers);

		final List<TitleData> titles = userFacade.getTitles();

		model.addAttribute(ADULT_TITLES, getTravellerTitle(adultTitles, titles));
		model.addAttribute(CHILDREN_TITLES, getTravellerTitle(childrenTitles, titles));

		final ReservationData reservationData = bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()
				.stream().findFirst().get().getReservationData();
		model.addAttribute(TRANSPORT_RESERVATION_DATA, reservationData);
		model.addAttribute(RESERVATION_DATA, reservationData);

		final AccommodationReservationData accommodationReservationData = bcfGlobalReservationData.getPackageReservations()
				.getPackageReservationDatas().stream().findFirst().get().getAccommodationReservationData();

		model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_RESERVATION_DATA, accommodationReservationData);

		model.addAttribute(BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_FORM,
				getPersonalDetailsForm(bcfTravellersData, accommodationReservationData.getRoomStays(), model));

		final Map<Integer, Map<String, Integer>> passengerTypeMaxQuantityMapPerRoom = getSessionService()
				.getAttribute(PASSENGER_TYPE_QUANITY_MAP);
		model.addAttribute(PASSENGER_TYPE_QUANITY_MAP, passengerTypeMaxQuantityMapPerRoom);
		model.addAttribute(DATE_PATTERN, TravelservicesConstants.DATE_PATTERN);
		model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, getTravelCartFacade().isAmendmentCart());
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(PERSONAL_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PERSONAL_DETAILS_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	protected void initializeGuestDetails(final PersonalDetailsForm personalDetailsForm,
			final List<ReservedRoomStayData> reservedRoomStays)
	{
		final List<LeadGuestDetailsForm> leadForms = new ArrayList<>();
		final Map<Integer, Map<String, Integer>> passengerTypeMaxQuantityMapPerRoom = new HashMap<>();
		reservedRoomStays.forEach(reservedRoomStayData -> {
			final LeadGuestDetailsForm leadGuestDetailsForm = new LeadGuestDetailsForm();
			leadGuestDetailsForm.setGuestData(CollectionUtils.isNotEmpty(reservedRoomStayData.getReservedGuests())
					? reservedRoomStayData.getReservedGuests().get(0) : null);
			leadGuestDetailsForm
					.setPassengerTypeQuantityData(getPassengerTypeQuantityData(reservedRoomStayData.getRoomStayRefNumber(),
							passengerTypeMaxQuantityMapPerRoom, reservedRoomStayData.getGuestCounts()));
			leadGuestDetailsForm.setArrivalTime(reservedRoomStayData.getArrivalTime());
			leadForms.add(leadGuestDetailsForm);
		});
		getSessionService().setAttribute(PASSENGER_TYPE_QUANITY_MAP, passengerTypeMaxQuantityMapPerRoom);
		personalDetailsForm.setLeadForms(leadForms);
	}

	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityData(final int roomRefNumber,
			final Map<Integer, Map<String, Integer>> passengerTypeMaxQuantityMapPerRoom,
			final List<PassengerTypeQuantityData> guestCounts)
	{

		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<GuestOccupancyData> guestOccupancies = bookingFacade.getGuestOccupanciesFromCart(roomRefNumber);
		final List<PassengerTypeData> passengerTypes = passengerTypeFacade.getPassengerTypes();
		final Map<String, Integer> passengerTypeMaxQuantityMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(guestOccupancies))
		{
			if (passengerTypes.size() != guestOccupancies.size())
			{
				final List<String> passengerCodes = new ArrayList<>();
				guestOccupancies.forEach(go -> passengerCodes.add(go.getPassengerType().getCode()));
				final List<PassengerTypeData> passengerTypeList = passengerTypes.stream()
						.filter(pt -> !passengerCodes.contains(pt.getCode())).collect(Collectors.toList());
				passengerTypeList.forEach(pt -> {
					final GuestOccupancyData guestOccupancyData = new GuestOccupancyData();
					guestOccupancyData.setPassengerType(pt);
					guestOccupancyData.setQuantityMin(
							StringUtils.equals(TravelacceleratorstorefrontValidationConstants.PASSENGER_TYPE_ADULT, pt.getCode())
									? TravelfacadesConstants.DEFAULT_QUANTITY_FOR_ADULTS
									: TravelfacadesConstants.DEFAULT_QUANTITY_FOR_NON_ADULTS);
					guestOccupancyData.setQuantityMax(getMaxGuestQuantity());
					guestOccupancies.add(guestOccupancyData);
					guestOccupancies.sort((p1, p2) -> p1.getPassengerType().getCode().compareTo(p2.getPassengerType().getCode()));
				});

			}
			for (final GuestOccupancyData guestOccupancy : guestOccupancies)
			{
				passengerTypeQuantityList.add(getPassengerTypeQuantityData(guestOccupancy, guestCounts));
				passengerTypeMaxQuantityMap.put(guestOccupancy.getPassengerType().getCode(), guestOccupancy.getQuantityMax());
			}
		}
		else
		{
			final List<PassengerTypeData> sortedPassengerTypes = travellerSortStrategy.sortPassengerTypes(passengerTypes);
			for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
			{
				final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
				passengerTypeQuantityData.setPassengerType(passengerTypeData);
				passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
				passengerTypeMaxQuantityMap.put(passengerTypeData.getCode(), getMaxGuestQuantity());
				passengerTypeQuantityList.add(passengerTypeQuantityData);
			}
		}
		passengerTypeMaxQuantityMapPerRoom.put(roomRefNumber, passengerTypeMaxQuantityMap);
		return passengerTypeQuantityList;
	}

	private PassengerTypeQuantityData getPassengerTypeQuantityData(final GuestOccupancyData guestOccupancy,
			final List<PassengerTypeQuantityData> guestCounts)
	{
		final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
		passengerTypeQuantityData.setPassengerType(guestOccupancy.getPassengerType());
		if (CollectionUtils.isNotEmpty(guestCounts))
		{
			passengerTypeQuantityData
					.setQuantity(guestCounts.stream().filter(guestCount -> StringUtils.equals(guestCount.getPassengerType().getCode(),
							guestOccupancy.getPassengerType().getCode())).findFirst().get().getQuantity());
		}
		else
		{
			passengerTypeQuantityData
					.setQuantity(StringUtils.equals(TravelacceleratorstorefrontValidationConstants.PASSENGER_TYPE_ADULT,
							guestOccupancy.getPassengerType().getCode()) ? guestOccupancy.getQuantityMax()
							: TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
		}
		return passengerTypeQuantityData;
	}


	protected void populateGuestDetails(final PersonalDetailsForm personalDetailsForm,
			final List<ReservedRoomStayData> reservedRoomStays)
	{
		final String checkInDate = TravelDateUtils.convertDateToStringDate(reservedRoomStays.get(0).getCheckInDate(),
				TravelservicesConstants.DATE_PATTERN) + DEFAULT_GUEST_CHECK_IN_TIME;
		final Map<Integer, Map<String, Integer>> passengerTypeMaxQuantityMapPerRoom = new HashMap<>();
		final List<LeadGuestDetailsForm> leadForms = new ArrayList<>();
		reservedRoomStays.forEach(reservedRoomStayData -> {
			final LeadGuestDetailsForm leadGuestDetailsForm = new LeadGuestDetailsForm();
			leadGuestDetailsForm.setGuestData(CollectionUtils.isNotEmpty(reservedRoomStayData.getReservedGuests())
					? reservedRoomStayData.getReservedGuests().get(0) : null);
			leadGuestDetailsForm.setRoomPreferenceCodes(CollectionUtils.isNotEmpty(reservedRoomStayData.getRoomPreferences())
					? Stream.of(reservedRoomStayData.getRoomPreferences().get(0).getCode()).collect(Collectors.toList()) : null);
			leadGuestDetailsForm
					.setPassengerTypeQuantityData(getPassengerTypeQuantityData(reservedRoomStayData.getRoomStayRefNumber(),
							passengerTypeMaxQuantityMapPerRoom, reservedRoomStayData.getGuestCounts()));
			leadGuestDetailsForm.getPassengerTypeQuantityData().forEach(passengerTypeQuantityData -> {
				final int reserveQuantity = passengerTypeMaxQuantityMapPerRoom.get(reservedRoomStayData.getRoomStayRefNumber())
						.get(passengerTypeQuantityData.getPassengerType().getCode());
				passengerTypeQuantityData.setQuantity(reserveQuantity == getMaxGuestQuantity() ? 0 : reserveQuantity);
			});

			leadGuestDetailsForm.setArrivalTime(checkInDate);
			leadGuestDetailsForm.setRoomStayRefNumber(reservedRoomStayData.getRoomStayRefNumber().toString());
			leadGuestDetailsForm.setFormId(Integer.valueOf(reservedRoomStays.indexOf(reservedRoomStayData)).toString());
			leadGuestDetailsForm.setNotRemoved(Boolean.FALSE);
			leadForms.add(leadGuestDetailsForm);
		});
		personalDetailsForm.setLeadForms(leadForms);
	}


	@RequestMapping(value = "/validate-personal-details-forms", method = RequestMethod.POST)
	public String validatePersonalDetailsForm(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_FORM) final PersonalDetailsForm personalDetailsForm,
			final BindingResult bindingResult, final Model model)
	{
		travellerFormValidator.validate(personalDetailsForm.getBcfTravellersData(), bindingResult);

		if (personalDetailsForm.isUseDiffLeadDetails())
		{
			leadGuestDetailsFormsValidator.validate(personalDetailsForm.getLeadForms(), bindingResult);
		}
		return getValidationErrorMessage(bindingResult, model);
	}

	protected String getValidationErrorMessage(final BindingResult bindingResult, final Model model)
	{
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfaccommodationsaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			model.addAttribute(BcfaccommodationsaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}
		return BcfcommonsaddonControllerConstants.Views.Pages.FormErrors.FormErrorsResponse;
	}


	/**
	 * @param travellerDetails
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String savePersonalDetails(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_FORM) final PersonalDetailsForm personalDetailsForms,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{

		travellerFormValidator.validate(personalDetailsForms.getBcfTravellersData(), bindingResult);

		if (bindingResult.hasErrors())
		{
			return getPersonalDetailsPage(model, redirectModel, request, response);
		}

		// TODO need to handle the leadGuest validation once we have wireframes changes

		personalDetailsForms.getBcfTravellersData().getTravellerDataPerJourney().forEach(
				travellerDataPerJourney -> travellerFacade
						.updateTravellerDetails(Stream.concat(travellerDataPerJourney.getOutboundTravellerData().stream(),
								travellerDataPerJourney.getInboundTravellerData().stream())
								.collect(Collectors.toList())));

		// TODO need to handle the update Accommodation guest details once we have wireframes changes


		return nextPage();
	}

	@ModelAttribute("countries")
	public List<CountryData> populateCountries()
	{
		final String propertyValue = bcfConfigurablePropertiesService.getBcfPropertyValue("vehicleCountries");
		final List<String> configuredCountries = Stream.of(propertyValue.split(",")).collect(Collectors.toList());
		final List<CountryData> countries = new ArrayList<>();
		configuredCountries.forEach(isoCode -> countries.add(i18NFacade.getCountryForIsocode(StringUtils.trim(isoCode))));
		return countries;
	}

	protected void useTravellerDetailsForAccommodationBooking(final PersonalDetailsForm personalDetailsForm)
	{
		final AccommodationReservationData accommodationReservationData = bookingFacade
				.getAccommodationReservationDataForGuestDetailsFromCart();
		populateGuestDetails(personalDetailsForm, accommodationReservationData.getRoomStays());
		for (int roomCount = 0; roomCount < CollectionUtils.size(personalDetailsForm.getLeadForms()); roomCount++)
		{
			final LeadGuestDetailsForm leadForm = personalDetailsForm.getLeadForms().get(roomCount);
			final PassengerInformationForm travellerDetails = personalDetailsForm.getTravellerForms().get(roomCount)
					.getPassengerInformation();
			final ProfileData guestProfileDetails = leadForm.getGuestData().getProfile();
			guestProfileDetails.setFirstName(travellerDetails.getFirstname());
			guestProfileDetails.setLastName(travellerDetails.getLastname());
			guestProfileDetails.setEmail(travellerDetails.getEmail());
			if (roomCount == 0)
			{
				guestProfileDetails.setContactNumber(travellerDetails.getContactNumber());
			}
		}
	}

	/**
	 * Redirects user to the next checkout page which is payment details
	 *
	 * @return payment details page or payment type page
	 */
	protected String nextPage()
	{
		if (getTravelCustomerFacade().isCurrentUserB2bCustomer())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PAYMENT_TYPE_PATH;
		}

		return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.SUMMARY_DETAILS_PATH;
	}


	protected PersonalDetailsForm getPersonalDetailsForm(final BCFTravellersData travellers,
			final List<ReservedRoomStayData> reservedRoomStays, final Model model)
	{
		final PersonalDetailsForm personalDetailsForm = new PersonalDetailsForm();
		personalDetailsForm.setBcfTravellersData(travellers);
		initializeGuestDetails(personalDetailsForm, reservedRoomStays);
		return personalDetailsForm;
	}

	protected void initializeTravellerDetails(final List<TravellerData> travellers, final PersonalDetailsForm personalDetailsForm)
	{
		final List<TravellerForm> travellerForms = new ArrayList<>();

		travellers.forEach(traveller -> {
			final PassengerInformationData passengerInformationData = (PassengerInformationData) traveller.getTravellerInfo();
			final PassengerInformationForm passengerInformationForm = new PassengerInformationForm();
			passengerInformationForm.setPassengerTypeCode(passengerInformationData.getPassengerType().getCode());
			passengerInformationForm.setPassengerTypeName(passengerInformationData.getPassengerType().getName());
			passengerInformationForm.setEmail(passengerInformationData.getEmail());
			passengerInformationForm.setFirstname(passengerInformationData.getFirstName());
			passengerInformationForm.setLastname(passengerInformationData.getSurname());
			passengerInformationForm.setGender(passengerInformationData.getGender());
			if (Objects.nonNull(passengerInformationData.getTitle()))
			{
				passengerInformationForm.setTitle(passengerInformationData.getTitle().getName());
			}
			passengerInformationForm.setReasonForTravel(passengerInformationData.getReasonForTravel());
			passengerInformationForm.setFrequentFlyerMembershipNumber(passengerInformationData.getMembershipNumber());
			passengerInformationForm.setFrequentFlyer(StringUtils.isNotEmpty(passengerInformationData.getMembershipNumber()));

			final TravellerForm travellerForm = new TravellerForm();
			travellerForm.setLabel(traveller.getLabel());
			travellerForm.setUid(traveller.getUid());
			travellerForm.setSelectedSavedTravellerUId(traveller.getSavedTravellerUid());
			travellerForm.setPassengerInformation(passengerInformationForm);
			travellerForm.setSpecialAssistance(Objects.nonNull(traveller.getSpecialRequestDetail()));
			travellerForm.setBooker(traveller.isBooker());
			travellerForms.add(travellerForm);
		});

		Collections.sort(travellerForms);
		final boolean isAddNewRoomPackage = travelCartFacade.isAmendmentCart() && accommodationCartFacade.isNewRoomInCart();
		if (isAddNewRoomPackage)
		{
			personalDetailsForm.setUseDiffLeadDetails(Boolean.TRUE);
			travellerForms.get(0).getPassengerInformation().setValidateContactNumber(Boolean.FALSE);
		}

		personalDetailsForm.setTravellerForms(travellerForms);
	}

	protected List<TitleData> getTravellerTitle(final String[] travellerTitles, final List<TitleData> titles)
	{
		final List<String> tt = new ArrayList<>();
		Collections.addAll(tt, travellerTitles);
		return titles.stream().filter(t -> tt.contains(t.getCode())).collect(Collectors.toList());
	}


	protected void disableCachingForResponse(final HttpServletResponse response)
	{
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	}

	@ModelAttribute(MINUTES)
	public List<String> populateMinutes()
	{
		final List<String> minutes = new ArrayList<>();
		for (int i = 0; i <= 59; i++)
		{

			minutes.add(i < 10 ? "0" + i : String.valueOf(i));

		}
		return minutes;
	}

	@ModelAttribute(HOURS)
	public List<String> populateHours()
	{
		final List<String> hours = new ArrayList<>();
		for (int i = 0; i <= 23; i++)
		{
			hours.add(i < 10 ? "0" + i : String.valueOf(i));

		}
		return hours;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getAdultAgesRange()
	{
		return adultAgesRange;
	}

	/**
	 * @param adultAgesRange
	 */
	@Required
	public void setAdultAgesRange(final String[] adultAgesRange)
	{
		this.adultAgesRange = adultAgesRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getChildrenAgeRange()
	{
		return childrenAgeRange;
	}

	/**
	 * @param childrenAgeRange
	 */
	@Required
	public void setChildrenAgeRange(final String[] childrenAgeRange)
	{
		this.childrenAgeRange = childrenAgeRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	public String[] getInfantAgeRange()
	{
		return infantAgeRange;
	}

	/**
	 * @param infantAgeRange
	 */
	@Required
	public void setInfantAgeRange(final String[] infantAgeRange)
	{
		this.infantAgeRange = infantAgeRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getAdultTitles()
	{
		return adultTitles;
	}

	/**
	 * @param adultTitles
	 */
	@Required
	public void setAdultTitles(final String[] adultTitles)
	{
		this.adultTitles = adultTitles;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getChildrenTitles()
	{
		return childrenTitles;
	}

	/**
	 * @param childrenTitles
	 */
	@Required
	public void setChildrenTitles(final String[] childrenTitles)
	{
		this.childrenTitles = childrenTitles;
	}


	/**
	 * @return the maxGuestQuantity
	 */
	protected Integer getMaxGuestQuantity()
	{
		return maxGuestQuantity;
	}


	/**
	 * @param maxGuestQuantity the maxGuestQuantity to set
	 */
	@Required
	public void setMaxGuestQuantity(final Integer maxGuestQuantity)
	{
		this.maxGuestQuantity = maxGuestQuantity;
	}

}
