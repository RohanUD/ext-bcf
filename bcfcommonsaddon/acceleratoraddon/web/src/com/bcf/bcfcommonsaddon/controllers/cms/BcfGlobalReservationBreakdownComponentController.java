/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.model.components.BcfGlobalReservationBreakdownComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.util.ReservationBreakdownDataHelper;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


@Controller("BcfGlobalReservationBreakdownComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.BcfGlobalReservationBreakdownComponent)
public class BcfGlobalReservationBreakdownComponentController
		extends SubstitutingCMSAddOnComponentController<BcfGlobalReservationBreakdownComponentModel>
{
	private static final Logger LOG = Logger.getLogger(BcfGlobalReservationBreakdownComponentModel.class);

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "reservationBreakdownDataHelper")
	private ReservationBreakdownDataHelper reservationBreakdownDataHelper;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BcfGlobalReservationBreakdownComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		if (StringUtils.isNotBlank(bookingReference))
		{
			final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);
			final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade
					.getbcfGlobalReservationDataList(order);
			Map<String, String> qrImageMap = new HashMap<>();
			try
			{
				qrImageMap = reservationBreakdownDataHelper.getQRImageMap(order);
			}
			catch (final Exception e)
			{
				LOG.error("Exception : ", e);
			}
			model.addAttribute(BcfcommonsaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);
			model.addAttribute(BcfvoyageaddonWebConstants.AMEND, bookingFacade.isAmendment(bookingReference));
			model.addAttribute(BcfvoyageaddonWebConstants.HIDE_BUTTON, "HIDE");
			model.addAttribute(BcfvoyageaddonWebConstants.DISABLE_CURRENCY_SELECTOR, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.DATE_FORMAT_LABEL, BcfvoyageaddonWebConstants.DATE_FORMAT_MONTH_DAY);
			model.addAttribute(BcfvoyageaddonWebConstants.TIME_FORMAT_LABEL, BcfvoyageaddonWebConstants.TIME_FORMAT_12HR);
			model.addAttribute("QRImageMap", qrImageMap);
		}
	}
}
