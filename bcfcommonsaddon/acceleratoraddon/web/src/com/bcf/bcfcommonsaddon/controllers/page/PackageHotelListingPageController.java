/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.packages.response.PackageSearchResponseData;
import de.hybris.platform.commercefacades.search.data.SearchFilterQueryData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfAccommodationOfferingFacade;
import com.bcf.facades.packagedeal.search.impl.DefaultPackageSolrSearchFacade;
import com.bcf.facades.packagedeal.search.response.data.PackageResponseData;
import com.bcf.facades.packages.BcfPackageFacade;
import com.bcf.facades.search.facetdata.PackageSearchPageData;


/**
 * Controller for Accommodation Search page
 */
@Controller
@RequestMapping("/vacations/promotions/{promotionName}/{promotionCode}")
public class PackageHotelListingPageController extends AbstractPackagePageController
{
	public static final String REDIRECT_PREFIX = "redirect:";
	public static final String PACKAGE_HOTEL_LISTING_PAGE = "packageHotelListingPage";
	private static final String FILTER_QUERY_KEY = "code";
	public static final String PROMOTION_DETAILS = "promotionDetails";

	@Resource(name = "accommodationOfferingFacade")
	private BcfAccommodationOfferingFacade accommodationOfferingFacade;

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Resource(name = "packageSolrSearchFacade")
	private DefaultPackageSolrSearchFacade packageSolrSearchFacade;

	@Resource(name = "bcfPackageFacade")
	private BcfPackageFacade packageFacade;

	/**
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getPackageListingPage(final Model model, @PathVariable("promotionCode") final String promotionCode)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(PACKAGE_HOTEL_LISTING_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PACKAGE_HOTEL_LISTING_PAGE));
		final PackageSearchResponseData packageSearchResponseData = new PackageSearchResponseData();
		packageSearchResponseData.setProperties(getPropertiesForPromotion(promotionCode));
		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES,
				packageSearchResponseData.getProperties());
		model.addAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES,
				packageSearchResponseData.getProperties());
		setSessionJourney(model, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		model.addAttribute(PROMOTION_DETAILS, packageFacade.getPromotionData(promotionCode));
		return getViewForPage(model);
	}

	private List<PropertyData> getPropertiesForPromotion(final String promotionCode)
	{

		final PackageSearchPageData packageSearchPageData = packageSolrSearchFacade
				.searchDeals(prepareDealSearchRequestData(promotionCode));
		final Optional<PackageResponseData> packageResponseData =
				StreamUtil.safeStream(packageSearchPageData.getResults()).findAny();
		if (packageResponseData.isPresent())
		{
			final String[] hotelCodes = StreamUtil.safeStream(packageResponseData.get().getHotels()).toArray(String[]::new);
			final String roomRateProductsParam = StreamUtil
					.safeStream(packageResponseData.get().getRoomRateProductCodes()).collect(Collectors.joining());
			return accommodationOfferingFacade.getPackageData(hotelCodes, promotionCode, roomRateProductsParam);
		}
		return Collections.emptyList();
	}

	protected void setSessionJourney(final Model model, final String bookingJourney)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourney);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, bookingJourney);
		}
	}

	private SearchStateData prepareDealSearchRequestData(final String promotionCode)
	{
		final SearchFilterQueryData searchFilterQueryData = new SearchFilterQueryData();
		final SearchStateData searchStateData = new SearchStateData();
		final SearchQueryData query = new SearchQueryData();
		if (Objects.nonNull(promotionCode))
		{
			searchFilterQueryData.setKey(FILTER_QUERY_KEY);
			searchFilterQueryData.setValues(Stream.of(promotionCode).collect(Collectors.toSet()));
			final List<SearchFilterQueryData> filterQueries = new ArrayList<>();
			filterQueries.add(searchFilterQueryData);
			query.setFilterQueries(filterQueries);
		}
		query.setValue("*:*");
		searchStateData.setQuery(query);
		return searchStateData;
	}
}
