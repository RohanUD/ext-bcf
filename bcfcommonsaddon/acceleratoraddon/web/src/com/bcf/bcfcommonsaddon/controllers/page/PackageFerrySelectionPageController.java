/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_TOTAL_DATA;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.packages.request.PackageSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.fest.util.Collections;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.custom.propertysource.message.code.resolver.CustomMessageCodesResolver;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.rits.cloning.Cloner;


/**
 * Controller for Fare Selection page
 */
@Controller("PackageFerrySelectionPageController")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping("/package-select-ferry")
public class PackageFerrySelectionPageController extends AbstractPackagePageController
{
	private static final String PACKAGE_FERRY_SELECTION_CMS_PAGE = "packageFerrySelectionPage";
	private static final String NO_SEARCH_RESULT = "search.result.not.found.fareFinderForm";
	private static final String INDEX = "index";
	private static final String CHANGE = "change";

	@Resource(name = "bcfFareSearchFacade")
	private BcfFareSearchFacade bcfFareSearchFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "fareFinderValidator")
	private AbstractTravelValidator fareFinderValidator;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "messageCodesResolver")
	private CustomMessageCodesResolver customMessageCodesResolver;

	@Resource(name = "packageVehicleInfoValidator")
	protected AbstractTravelValidator packageVehicleInfoValidator;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	/**
	 * Method responsible for handling GET request on Fare Selection page
	 *
	 * @param travelFinderForm
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return fare selection page
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
	public String getPackageFerrySelectionPage(
			@Valid @ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute(BcfstorefrontaddonWebConstants.CURRENT_PAGE,
				BcfstorefrontaddonWebConstants.PACKAGE_FERRY_INFO_PAGE_PATH);
		cloneVehicle(travelFinderForm);
		validateForm(packageVehicleInfoValidator, travelFinderForm, bindingResult, BcfFacadesConstants.TRAVEL_FINDER_FORM);
		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "true");
			redirectModel.addFlashAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PACKAGE_FERRY_INFO_PAGE_PATH;
		}
		final TotalFareData bcfGlobalReservationData = globalReservationFacade
				.getGlobalReservationForTotalPricewithTax(
						Arrays.asList(OrderEntryType.TRANSPORT, OrderEntryType.ACCOMMODATION));
		model.addAttribute(GLOBAL_RESERVATION_TOTAL_DATA, bcfGlobalReservationData);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, travelFinderForm.getAccommodationFinderForm());
		model.addAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, travelFinderForm.getFareFinderForm());
		model.addAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "false");
		final PackageSearchRequestData packageSearchRequestData = preparePackageSearchRequestData(travelFinderForm, request);
		try
		{
			final FareSelectionData fareSelectionData = bcfFareSearchFacade
					.performSearch(packageSearchRequestData.getFareSearchRequestData());
			if (Objects.isNull(fareSelectionData) || Collections.isEmpty(fareSelectionData.getPricedItineraries()))
			{
				final List<String> errorMessages = new ArrayList<>();
				errorMessages.add(propertySourceFacade.getPropertySourceValue(NO_SEARCH_RESULT));
				model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES, errorMessages);

				storeCmsPageInModel(model, getContentPageForLabelOrId(PACKAGE_FERRY_SELECTION_CMS_PAGE));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PACKAGE_FERRY_SELECTION_CMS_PAGE));
				model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
				return getViewForPage(model);
			}
			for (final PricedItineraryData pricedItineraryData : fareSelectionData.getPricedItineraries())
			{
				final Iterator<ItineraryPricingInfoData> i = pricedItineraryData.getItineraryPricingInfos().iterator();
				while (i.hasNext())
				{
					if (!i.next().getBundleType()
							.equals(bcfConfigurablePropertiesService
									.getBcfPropertyValue(BcfFacadesConstants.DEFAULT_FARE_BASIS_CODE_FOR_PASSENGER)))
					{
						i.remove();
					}
				}
			}
			updateNextURL(request,model);
			boolean returnJourney = false;
			if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
					.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
			{
				returnJourney = true;
				final String travelRoute = travelFinderForm.getTravelRoute();
				final ReservationDataList reservationDataList = bcfReservationFacade
						.getCurrentReservationsByTravelRoute(travelRoute);
				model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
				model.addAttribute(BcfFacadesConstants.IS_RETURN, "true");
			}
			else
			{
				model.addAttribute(BcfFacadesConstants.IS_RETURN, "false");
			}

			// sort fareSelectionData by displayOrder
			final String displayOrder = packageSearchRequestData.getFareSearchRequestData().getSearchProcessingInfo()
					.getDisplayOrder();
			bcfvoyageaddonComponentControllerUtil.sortFareSelectionData(fareSelectionData, displayOrder, model);

			populateModel(model, travelFinderForm.getFareFinderForm().getTripType(),
					travelFinderForm.getFareFinderForm().getDepartingDateTime(), fareSelectionData, returnJourney);
			model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_FERRY_SELECTION_REDIRECT_URL,
					BcfstorefrontaddonWebConstants.PACKAGE_FERRY_SELECTION_PAGE_PATH);
			model.addAttribute(BcfvoyageaddonWebConstants.DATE_NOW, new java.util.Date());
			model.addAttribute(BcfcommonsaddonWebConstants.FARE_PRODUCT_FARE_BASIS_CODE, bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfFacadesConstants.DEFAULT_FARE_BASIS_CODE_FOR_PASSENGER));
			model.addAttribute(BcfFacadesConstants.ACCESSIBILITIES,
					packageSearchRequestData.getFareSearchRequestData().getAccessibilities());
			model.addAttribute(BcfFacadesConstants.SPECIALSERVICES,
					packageSearchRequestData.getFareSearchRequestData().getSpecialServices());
			model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
					bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
			model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_STATUS, travelFinderForm.getAvailabilityStatus());
		}
		catch (final IntegrationException ex)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES,
					ex.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).collect(Collectors.toList()));
		}
		addAttributeForSessionBookingJourney(model);

		storeCmsPageInModel(model, getContentPageForLabelOrId(PACKAGE_FERRY_SELECTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PACKAGE_FERRY_SELECTION_CMS_PAGE));

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute("isAmendmentCart", bcfTravelCartFacade.isAmendmentCart());
		model.addAttribute("isToShowOpenTicket",
				bcfTravelRouteFacade.isOpenTicketAllowedForRoute(travelFinderForm.getTravelRoute()));

		final CartTimerData cartTimerData = bcfCartTimerFacade.getCartStartTimerDetails();
		if(cartTimerData!=null){
			model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
			model.addAttribute("holdTimeMessage",cartTimerData.getHoldTimeMessage());
		}
		getSessionService().setAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);

		return getViewForPage(model);
	}

	private void updateNextURL(final HttpServletRequest request, final Model model)
	{
		if (request.getParameterMap().containsKey(BcfstorefrontaddonWebConstants.MODIFY))
		{
			model.addAttribute(BcfFacadesConstants.NEXT_URL,
					BcfstorefrontaddonWebConstants.PACKAGE_FERRY_REVIEW_PAGE_PATH + "?modify=true");
		}
		else if (request.getParameterMap().containsKey(INDEX) || request.getParameterMap().containsKey(CHANGE))
		{
			model.addAttribute(BcfFacadesConstants.NEXT_URL, BcfstorefrontaddonWebConstants.PACKAGE_FERRY_REVIEW_PAGE_PATH);
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.NEXT_URL,
					BcfstorefrontaddonWebConstants.PACKAGE_FERRY_SELECTION_PAGE_PATH + "?" + BcfFacadesConstants.IS_RETURN
							+ "=true&index=0");
		}
	}

	private void validateForm(final AbstractTravelValidator validator, final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult, final String formName)
	{
		validator.setTargetForm(formName);
		validator.setAttributePrefix("");
		((BeanPropertyBindingResult) bindingResult).setMessageCodesResolver(customMessageCodesResolver);
		validator.validate(travelFinderForm, bindingResult);
	}

	protected void cloneVehicle(final TravelFinderForm travelFinderForm)
	{
		bcfvoyageaddonComponentControllerUtil.setVehicleCodeCategoryInfo(travelFinderForm.getFareFinderForm().getVehicleInfo());
		if (Objects.nonNull(travelFinderForm.getFareFinderForm().getVehicleInfo()))
		{
			final List<VehicleTypeQuantityData> vehicleInfo = travelFinderForm.getFareFinderForm().getVehicleInfo();
			final Cloner clone = new Cloner();
			final TravelRouteData travelRouteData = bcfTravelRouteFacade
					.getTravelRoutes(travelFinderForm.getFareFinderForm().getArrivalLocation(),
							travelFinderForm.getFareFinderForm().getDepartureLocation())
					.stream().findFirst().orElse(null);
			final VehicleTypeQuantityData returnVehicle = clone.deepClone(vehicleInfo.get(0));
			if (travelRouteData != null)
			{
				returnVehicle.setTravelRouteCode(travelRouteData.getCode());
			}
			vehicleInfo.set(1, returnVehicle);
		}
	}


	/**
	 * Method handles the preparation of a PackageSearchRequestData object using the TravelFinderForm.
	 *
	 * @param travelFinderForm
	 * @return
	 */
	protected PackageSearchRequestData preparePackageSearchRequestData(final TravelFinderForm travelFinderForm,
			final HttpServletRequest request)
	{

		final PackageSearchRequestData packageSearchRequestData = new PackageSearchRequestData();
		final FareSearchRequestData fareSearchRequestData = prepareFareSearchRequestData(travelFinderForm.getFareFinderForm(),
				request);

		fareSearchRequestData.setOriginDestinationInfo(createOriginDestinationInfo(request, travelFinderForm.getFareFinderForm()));
		// populate passenger details
		bcfvoyageaddonComponentControllerUtil
				.setPassengerDetails(fareSearchRequestData,
						travelFinderForm.getFareFinderForm().getPassengerTypeQuantityList(),
						travelFinderForm.getFareFinderForm().getAccessibilityRequestDataList());

		fareSearchRequestData.setSalesApplication(bcfSalesApplicationResolverService.getCurrentSalesChannel());

		packageSearchRequestData.setFareSearchRequestData(fareSearchRequestData);
		return packageSearchRequestData;
	}

	/**
	 * Method handles the preparation of the Model object
	 *
	 * @param tripType
	 * @param departureDateTime
	 * @param model
	 * @param fareSelectionData
	 */
	public void populateModel(final Model model, final String tripType, final String departureDateTime,
			final FareSelectionData fareSelectionData, final boolean returnJourney)
	{
		final String priceDisplayPassengerType = configurationService.getConfiguration()
				.getString(BcfvoyageaddonWebConstants.PRICE_DISPLAY_PASSENGER_TYPE);
		model.addAttribute(BcfvoyageaddonWebConstants.MODEL_PRICE_DISPLAY_PASSENGER_TYPE, priceDisplayPassengerType);

		model.addAttribute(BcfvoyageaddonWebConstants.FARE_SELECTION, fareSelectionData);
		model.addAttribute(BcfvoyageaddonWebConstants.FIRST_ITINERARY,
				bcfFerrySelectionUtil.getFirstItinerary(fareSelectionData, returnJourney ? 1 : 0));



		model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE, tripType);
		final AddBundleToCartForm addBundleToCartForm = new AddBundleToCartForm();
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_BUNDLE_TO_CART_FORM, addBundleToCartForm);
		model.addAttribute(BcfstorefrontaddonWebConstants.ADD_BUNDLE_TO_CART_URL,
				BcfvoyageaddonWebConstants.UPDATE_BUNDLE_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.PI_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
		model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
		model.addAttribute(BcfvoyageaddonWebConstants.OUTBOUND_REF_NUMBER, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.INBOUND_REF_NUMBER, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_OUTBOUND_OPTIONS,
				bcfvoyageaddonComponentControllerUtil
						.countJourneyOptions(fareSelectionData, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER));
		model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_INBOUND_OPTIONS,
				bcfvoyageaddonComponentControllerUtil
						.countJourneyOptions(fareSelectionData, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER));
	}

	private PricedItineraryData getFirstItinerary(final FareSelectionData fareSelectionData, final int refNumber)
	{
		if (CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
		{

			for (final PricedItineraryData pricedItineraryData : fareSelectionData.getPricedItineraries())
			{

				if (pricedItineraryData.isAvailable() && pricedItineraryData.getOriginDestinationRefNumber() == refNumber
						&& CollectionUtils.isNotEmpty(pricedItineraryData.getItineraryPricingInfos()))
				{
					return pricedItineraryData;
				}
			}
		}
		return null;
	}

	private void addAttributeForSessionBookingJourney(final Model model)
	{
		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
	}
}
