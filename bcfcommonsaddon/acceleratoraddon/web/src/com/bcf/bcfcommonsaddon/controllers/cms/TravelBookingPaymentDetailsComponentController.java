/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractBookingDetailsComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractBookingDetailsComponentModel;


@Controller("TravelBookingPaymentDetailsComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.TravelBookingPaymentDetailsComponent)
public class TravelBookingPaymentDetailsComponentController extends AbstractBookingDetailsComponentController
{
	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AbstractBookingDetailsComponentModel component)
	{
		final String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		final boolean isPackageInOrder = getPackageFacade().isPackageInOrder(bookingReference);
		model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_ORDER, isPackageInOrder);
		if (isPackageInOrder)
		{
			final PriceData totalPrice = getBookingFacade().getBookingTotal(bookingReference);
			final PriceData amountPaid = getBookingFacade().getOrderTotalPaid(bookingReference);
			model.addAttribute(BcfstorefrontaddonWebConstants.TOTAL, totalPrice);
			model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_PAID, amountPaid);
			model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_DUE,
					travelCartFacade.getBookingDueAmount(totalPrice, amountPaid));
			final PriceData notRefundableAmount = getBookingFacade().getNotRefundableAmount(bookingReference);
			model.addAttribute(BcfstorefrontaddonWebConstants.NOT_REFUNDABLE, notRefundableAmount);
			model.addAttribute(BcfcommonsaddonWebConstants.TRANSPORT_BOOKING_TOTAL_AMOUNT,
					getBookingFacade().getBookingTotalByOrderEntryType(bookingReference, OrderEntryType.TRANSPORT));
			model.addAttribute(BcfcommonsaddonWebConstants.ACCOMMODATION_BOOKING_TOTAL_AMOUNT,
					getBookingFacade().getBookingTotalByOrderEntryType(bookingReference, OrderEntryType.ACCOMMODATION));
		}
	}
}
