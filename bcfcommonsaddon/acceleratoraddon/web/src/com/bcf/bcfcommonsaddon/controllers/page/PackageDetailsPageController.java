/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.ServiceData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.packages.request.PackageRequestData;
import de.hybris.platform.commercefacades.packages.request.TransportPackageRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.fare.search.resolvers.FareSearchHashResolver;
import de.hybris.platform.travelfacades.order.AccommodationCartFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.controllers.BcfaccommodationsaddonControllerConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationAvailabilityForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.forms.cms.BcfAccommodationAddToCartForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationAvailabilityValidator;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.validators.FareFinderValidator;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.packages.BcfPackageFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


@Controller
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
public class PackageDetailsPageController extends AbstractPackagePageController
{
	private static final String PACKAGE_DETAILS_CMS_PAGE = "packageDetailsPage";
	private static final String AMEND_CART_ERROR = "accommodation.booking.details.page.request.cart.error";

	@Resource(name = "accommodationAvailabilityValidator")
	private AccommodationAvailabilityValidator accommodationAvailabilityValidator;

	@Resource(name = "fareFinderValidator")
	private FareFinderValidator fareFinderValidator;

	@Resource(name = "bcfPackageFacade")
	private BcfPackageFacade bcfPackageFacade;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "accommodationCartFacade")
	private AccommodationCartFacade accommodationCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "fareSearchHashResolver")
	private FareSearchHashResolver fareSearchHashResolver;

	@Resource(name = "activityProductSearchFacade")
	private ActivityProductSearchFacade activityProductSearchFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	private static final Logger LOG = LogManager.getLogger(PackageDetailsPageController.class);

	@RequestMapping(value = "/package-details/{accommodationOfferingCode}", method = RequestMethod.GET)
	public String getPackageDetailsPage(@PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
			//NOSONAR
			@Valid @ModelAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM) final AccommodationAvailabilityForm accommodationAvailabilityForm,
			final BindingResult accommodationBindingResult,
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult fareBindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel, final Model model) throws CMSItemNotFoundException
	{

		final FareFinderForm fareFinderForm = travelFinderForm.getFareFinderForm();

		//TODO further evaluation needed during amendments
		setSessionJourney(model, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);

		accommodationAvailabilityForm.setRoomStayCandidates(createRoomStayCandidates(request));

		if(travelFinderForm.getAccommodationFinderForm()!=null && CollectionUtils.isNotEmpty(travelFinderForm.getAccommodationFinderForm().getRoomStayCandidates())){

			travelFinderForm.getAccommodationFinderForm().setRoomStayCandidates(bcfControllerUtil
					.cleanUpChildRoomStayCandidates(travelFinderForm.getAccommodationFinderForm().getNumberOfRooms(),travelFinderForm.getAccommodationFinderForm().getRoomStayCandidates()));
		}
		accommodationAvailabilityValidator.validate(accommodationAvailabilityForm, accommodationBindingResult);

		if (accommodationBindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM_BINDING_RESULT,
					accommodationBindingResult);
			return REDIRECT_PREFIX + "/";
		}

		sessionService.setAttribute(TravelservicesConstants.SEARCH_SEED, fareSearchHashResolver.generateSeed());

		initializeFareFinderForm(fareFinderForm, accommodationAvailabilityForm);

		if (fareBindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM_BINDING_RESULT, fareBindingResult);
			return REDIRECT_PREFIX + "/";
		}

		if (!getTravelCartFacade().isAmendmentCart() && !"true".equals(request.getParameter(BcfstorefrontaddonWebConstants.MODIFY)))
		{
			getTravelCartFacade().deleteCurrentCart();
		}

		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);

		final PackageRequestData packageRequestData = new PackageRequestData();
		populateAccommodationPackageRequestData(packageRequestData, accommodationAvailabilityForm.getCheckInDateTime(),
				accommodationAvailabilityForm.getCheckOutDateTime(), accommodationAvailabilityForm.getRoomStayCandidates(),
				accommodationOfferingCode,
				Boolean.FALSE, request, accommodationReferenceData, null, null);
		populateTransportPackageRequestData(packageRequestData, fareFinderForm, request);

		try
		{
			final PackageResponseData packageResponseData = populatePackageRelatedData(
					model, packageRequestData);
			final int maxPassengersAllowed = getConfigurationService().getConfiguration()
					.getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
			final int maxChildrenAllowed = getConfigurationService().getConfiguration()
					.getInt(BcfFacadesConstants.MAX_CHILD_GUEST_QUANTITY);

			final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
					.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

			model.addAttribute(BcfvoyageaddonWebConstants.MAX_PASSENGER_QUANTITY, maxPassengersAllowed);
			model.addAttribute(BcfvoyageaddonWebConstants.MAX_CHILD_QUANTITY, maxChildrenAllowed);
			model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY, maxAccommodationsQuantity);
			model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_CART, bcfPackageFacade.isPackageInCart());
			model.addAttribute("isAmendment", bcfTravelCartService.isAmendmentCart(bcfTravelCartService.getSessionCart()));
			model.addAttribute("packageChangeAccommodationForm", new BcfAccommodationAddToCartForm());
			model.addAttribute(BcfstorefrontaddonWebConstants.MODIFY, Boolean.valueOf(request.getParameter(BcfstorefrontaddonWebConstants.MODIFY)));


			final ContentPageModel contentPageModel = getContentPageForLabelOrId(PACKAGE_DETAILS_CMS_PAGE);
			storeCmsPageInModel(model, contentPageModel);
			setUpMetaDataForContentPage(model, contentPageModel);

			populateDynamicDataForPackageHotel(model, packageResponseData, contentPageModel, request);

			return getViewForPage(model);
		}
		catch (final ModelNotFoundException ex)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"accommodation.offering.not.found.error.message");
			return REDIRECT_PREFIX + "/";
		}
	}

	@RequestMapping(value = "/package-details/edit/{accommodationOfferingCode}", method = RequestMethod.GET)
	public String getAmendedPackageDetailsPage(@PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		final AccommodationAvailabilityForm accommodationAvailabilityForm = new AccommodationAvailabilityForm();
		final List<RoomStayCandidateData> roomStayCandidateData = new ArrayList<>();
		for (int i = 0; i < travelFinderForm.getAccommodationFinderForm().getNumberOfRooms(); i++)
		{
			roomStayCandidateData.add(travelFinderForm.getAccommodationFinderForm().getRoomStayCandidates().get(i));
		}
		accommodationAvailabilityForm.setCheckInDateTime(travelFinderForm.getAccommodationFinderForm().getCheckInDateTime());
		accommodationAvailabilityForm.setCheckOutDateTime(travelFinderForm.getAccommodationFinderForm().getCheckOutDateTime());
		accommodationAvailabilityForm.setNumberOfRooms(travelFinderForm.getAccommodationFinderForm().getNumberOfRooms());

		accommodationAvailabilityForm.setRoomStayCandidates(roomStayCandidateData);

		accommodationAvailabilityValidator.validate(accommodationAvailabilityForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM_BINDING_RESULT,
					bindingResult);
			return REDIRECT_PREFIX + "/";
		}

		sessionService.setAttribute(TravelservicesConstants.SEARCH_SEED, fareSearchHashResolver.generateSeed());
		travelFinderForm.getFareFinderForm().setPassengerTypeQuantityList(createPassengerTypeQuantityData(
				accommodationAvailabilityForm.getNumberOfRooms(), accommodationAvailabilityForm.getRoomStayCandidates()));

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + "/";
		}
		if (getTravelCartFacade().isAmendmentCart())
		{
			getTravelCartFacade().deleteCurrentCart();
		}
		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);

		final PackageRequestData packageRequestData = new PackageRequestData();
		populateAccommodationPackageRequestData(packageRequestData, accommodationAvailabilityForm.getCheckInDateTime(),
				accommodationAvailabilityForm.getCheckOutDateTime(), accommodationAvailabilityForm.getRoomStayCandidates(),
				accommodationOfferingCode,
				Boolean.FALSE, request, accommodationReferenceData, null, null);
		populateTransportPackageRequestData(packageRequestData, travelFinderForm.getFareFinderForm(), request);

		try
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_OFFERING_CODE, accommodationOfferingCode);
			populatePackageRelatedData(
					model, packageRequestData);
			final int maxPassengersAllowed = getConfigurationService().getConfiguration()
					.getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
			final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
					.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

			model.addAttribute(BcfvoyageaddonWebConstants.MAX_PASSENGER_QUANTITY, maxPassengersAllowed);
			model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY, maxAccommodationsQuantity);

			model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_CART, bcfPackageFacade.isPackageInCart());
			model.addAttribute("packageChangeAccommodationForm", new BcfAccommodationAddToCartForm());

			return BcfcommonsaddonControllerConstants.Views.Pages.PackageSearch.packageDetailsJsonResponse;
		}
		catch (final ModelNotFoundException ex)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"accommodation.offering.not.found.error.message");
			return REDIRECT_PREFIX + "/";
		}
	}

	@RequestMapping(value = "/manage-booking/amend-package-details/{accommodationOfferingCode}", method = RequestMethod.GET)
	public String getAmendmentPackageDetailsPage(@PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
			@Valid @ModelAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_AVAILABILITY_FORM) final AccommodationAvailabilityForm accommodationAvailabilityForm,
			final BindingResult accommodationBindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel, final Model model) throws CMSItemNotFoundException
	{
		final boolean validateCart = validateAmendPackageInCart(accommodationOfferingCode, accommodationAvailabilityForm, request,
				accommodationBindingResult);
		if (!validateCart)
		{
			getTravelCartFacade().deleteCurrentCart();
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, AMEND_CART_ERROR);
			return REDIRECT_PREFIX + "/";
		}

		final List<Integer> newAccommodationOrderEntryGroupRefs = getBookingFacade().getNewAccommodationOrderEntryGroupRefs();
		boolean isRoomsAddedInCart = CollectionUtils.isNotEmpty(newAccommodationOrderEntryGroupRefs);
		if (isRoomsAddedInCart && CollectionUtils.size(newAccommodationOrderEntryGroupRefs) != CollectionUtils
				.size(accommodationAvailabilityForm.getRoomStayCandidates()))
		{
			newAccommodationOrderEntryGroupRefs.forEach(newAccommodationOrderEntryGroupRef -> accommodationCartFacade
					.removeAccommodationOrderEntryGroup(newAccommodationOrderEntryGroupRef));
			isRoomsAddedInCart = Boolean.FALSE;
		}

		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);

		final PackageRequestData packageRequestData = new PackageRequestData();
		populateAccommodationPackageRequestData(packageRequestData, accommodationAvailabilityForm.getCheckInDateTime(),
				accommodationAvailabilityForm.getCheckOutDateTime(), accommodationAvailabilityForm.getRoomStayCandidates(),
				accommodationOfferingCode,
				Boolean.TRUE, request, accommodationReferenceData, null, null);

		final PackageResponseData packageResponseData = bcfPackageFacade.getAmendPackageResponse(packageRequestData);

		populateModel(model, packageResponseData);

		model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_CART, isRoomsAddedInCart);

		setSessionJourney(model, BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY);
		storeCmsPageInModel(model, getContentPageForLabelOrId(PACKAGE_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PACKAGE_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	protected void populateModel(final Model model, final PackageResponseData packageResponseData)
	{
		model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_UNAVAILABLE,
				!packageResponseData.isAvailable() ? Boolean.TRUE : Boolean.FALSE);
		model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
		model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
		model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
				getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_DETAILS_CHANGED_FLAG, packageResponseData
				.getAccommodationPackageResponse().getAccommodationAvailabilityResponse().getConfigRoomsUnavailable());
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_RESPONSE, packageResponseData);
		final boolean isAmendJourney = getTravelCartFacade().isAmendmentCart();
		model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, isAmendJourney);
		model.addAttribute(BcfcommonsaddonWebConstants.ADD_ROOM_PACKAGE_URL,
				isAmendJourney ? BcfstorefrontaddonWebConstants.ADD_ROOM_PACKAGE_DETAILS_AMENDMENT_PAGE
						: BcfstorefrontaddonWebConstants.ADD_ROOM_PACKAGE_DETAILS_PAGE);
		model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, BcfstorefrontaddonWebConstants.PACKAGE_FERRY_INFO_PAGE_PATH);
	}

	protected void setSessionJourney(final Model model, final String bookingJourney)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourney);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, bookingJourney);
		}
	}

	protected boolean validateAmendPackageInCart(final String accommodationOfferingCode,
			final AccommodationAvailabilityForm accommodationAvailabilityForm, final HttpServletRequest request,
			final BindingResult accommodationBindingResult)
	{
		if (!getTravelCartFacade().isCurrentCartValid() || !getTravelCartFacade().isAmendmentCart()
				|| !bcfPackageFacade.isPackageInCart() || Objects.isNull(accommodationAvailabilityForm)
				|| StringUtils.isEmpty(accommodationOfferingCode))
		{
			return Boolean.FALSE;
		}

		final String originalOrderCode = getTravelCartFacade().getOriginalOrderCode();
		if (StringUtils.isEmpty(originalOrderCode))
		{
			return Boolean.FALSE;
		}

		final AccommodationReservationData accommodationReservationData = reservationFacade
				.getAccommodationReservationSummary(originalOrderCode);
		if (!StringUtils.equals(accommodationOfferingCode,
				accommodationReservationData.getAccommodationReference().getAccommodationOfferingCode()))
		{
			return Boolean.FALSE;
		}
		final String orderCheckInDate = TravelDateUtils.convertDateToStringDate(
				accommodationReservationData.getRoomStays().get(0).getCheckInDate(), TravelservicesConstants.DATE_PATTERN);
		final String orderCheckOutDate = TravelDateUtils.convertDateToStringDate(
				accommodationReservationData.getRoomStays().get(0).getCheckOutDate(), TravelservicesConstants.DATE_PATTERN);

		if (StringUtils.isEmpty(accommodationAvailabilityForm.getCheckInDateTime())
				|| StringUtils.isEmpty(accommodationAvailabilityForm.getCheckOutDateTime())
				|| !StringUtils.equals(accommodationAvailabilityForm.getCheckInDateTime(), orderCheckInDate)
				|| !StringUtils.equals(accommodationAvailabilityForm.getCheckOutDateTime(), orderCheckOutDate))
		{
			return Boolean.FALSE;
		}

		final int numberOfAccommodationOrderEntryGroups = CollectionUtils
				.size(getBookingFacade().getOldAccommodationOrderEntryGroupRefs());
		accommodationAvailabilityForm
				.setRoomStayCandidates(createRoomStayCandidates(request, numberOfAccommodationOrderEntryGroups));
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		if (maxAccommodationsQuantity < (numberOfAccommodationOrderEntryGroups
				+ CollectionUtils.size(accommodationAvailabilityForm.getRoomStayCandidates())))
		{
			return Boolean.FALSE;
		}
		accommodationAvailabilityValidator.validate(accommodationAvailabilityForm, accommodationBindingResult);
		return !accommodationBindingResult.hasErrors();

	}




	protected void populateTransportPackageRequestData(final PackageRequestData packageRequestData,
			final FareFinderForm fareFinderForm, final HttpServletRequest request)
	{
		final TransportPackageRequestData transportPackageRequestData = new TransportPackageRequestData();
		transportPackageRequestData.setFareSearchRequest(prepareFareSearchRequestData(fareFinderForm, request));
		packageRequestData.setTransportPackageRequest(transportPackageRequestData);
	}

	protected void initializeFareFinderForm(final FareFinderForm fareFinderForm,
			final AccommodationAvailabilityForm accommodationAvailabilityForm)
	{
		fareFinderForm.setDepartureLocationName(
				getLocationName(fareFinderForm.getDepartureLocation()));
		fareFinderForm.setArrivalLocationName(
				getLocationName(fareFinderForm.getArrivalLocation()));

		fareFinderForm.setPassengerTypeQuantityList(createPassengerTypeQuantityData(
				accommodationAvailabilityForm.getNumberOfRooms(), accommodationAvailabilityForm.getRoomStayCandidates()));
	}

	protected void initializeFareFinderForm(final TravelFinderForm travelFinderForm)
	{
		final String routeCode = travelFinderForm.getTravelRoute();

		final FareFinderForm fareFinderForm = travelFinderForm.getFareFinderForm();
		final AccommodationFinderForm accommodationFinderForm = travelFinderForm.getAccommodationFinderForm();

		// TODO: need to normalize this as different dataset causing this code split.
		if (routeCode.contains("-"))
		{
			final String[] routeCodes = routeCode.split("-");
			fareFinderForm.setDepartureLocationName(
					getLocationName(routeCodes[0]));
			fareFinderForm.setArrivalLocationName(
					getLocationName(routeCodes[1]));

			fareFinderForm.setDepartureLocation(routeCodes[0]);
			fareFinderForm.setArrivalLocation(routeCodes[1]);
		}
		else
		{
			final TravelRouteData travelRouteData = bcfTravelRouteFacade.getTravelRoute(routeCode);

			fareFinderForm.setDepartureLocationName(travelRouteData.getOrigin().getName());
			fareFinderForm.setArrivalLocationName(travelRouteData.getDestination().getName());

			fareFinderForm.setDepartureLocation(travelRouteData.getOrigin().getCode());
			fareFinderForm.setArrivalLocation(travelRouteData.getDestination().getCode());
		}

		fareFinderForm.setDepartingDateTime(accommodationFinderForm.getCheckInDateTime());
		fareFinderForm.setReturnDateTime(accommodationFinderForm.getCheckOutDateTime());

		fareFinderForm.setPassengerTypeQuantityList(createPassengerTypeQuantityData(
				accommodationFinderForm.getNumberOfRooms(), accommodationFinderForm.getRoomStayCandidates()));
		populateVehicles(travelFinderForm);
	}

	@RequestMapping(value = "/search-rooms/{accommodationOfferingCode}", method = RequestMethod.POST)
	public String performSearchRooms(@PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
			@RequestParam("roomRateProducts") final String roomRateProducts,
			@RequestParam(value = "promotionCode", required = false) final String promotionCode,
			@Valid @ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult accommodationBindingResult, final RedirectAttributes redirectModel, final HttpServletRequest request,
			final Model model)
	{
		final FareFinderForm fareFinderForm = travelFinderForm.getFareFinderForm();
		final AccommodationFinderForm accommodationFinderForm = travelFinderForm.getAccommodationFinderForm();

		if (StringUtils.isNotBlank(roomRateProducts))
		{
			accommodationFinderForm.setPromotionalRoomRateProducts(new HashSet<>(Arrays.asList(roomRateProducts.split(","))));
		}

		setSessionJourney(model, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);

		final String availabilityStatus = travelFinderForm.getAvailabilityStatus();

		sessionService.setAttribute(TravelservicesConstants.SEARCH_SEED, fareSearchHashResolver.generateSeed());

		initializeFareFinderForm(travelFinderForm);

		final PropertyData accommodationReferenceData = new PropertyData();
		accommodationReferenceData.setAccommodationOfferingCode(accommodationOfferingCode);
		accommodationReferenceData.setRatePlanConfigs(accommodationFinderForm.getPromotionalRoomRateProducts().stream().collect(
				Collectors.toList()));

		final ServiceData rentServiceData = new ServiceData();
		rentServiceData.setQuantity(travelFinderForm.getAccommodationFinderForm().getNumberOfRooms());


		final PackageRequestData packageRequestData = new PackageRequestData();
		populateAccommodationPackageRequestData(packageRequestData, accommodationFinderForm.getCheckInDateTime(),
				accommodationFinderForm.getCheckOutDateTime(),
				getAdjustedRoomStayCandidates(accommodationFinderForm),
				accommodationOfferingCode,
				Boolean.FALSE, request, accommodationReferenceData, rentServiceData, promotionCode);

		populateTransportPackageRequestData(packageRequestData, fareFinderForm, request);

		try
		{
			populatePackageRelatedData(model, packageRequestData);
			final int maxPassengersAllowed = getConfigurationService().getConfiguration()
					.getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
			final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
					.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

			model.addAttribute(BcfvoyageaddonWebConstants.MAX_PASSENGER_QUANTITY, maxPassengersAllowed);
			model.addAttribute(BcfvoyageaddonWebConstants.ACCOMMODATION_QUANTITY, maxAccommodationsQuantity);
			model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_CART, bcfPackageFacade.isPackageInCart());
			model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_STATUS, availabilityStatus);
			model.addAttribute(BcfcommonsaddonWebConstants.SHOW_PER_ROOM_PRICE, Boolean.TRUE);
			model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
		}
		catch (final Exception ex)
		{
			LOG.error("Error loading the packageResponseData {}", ex);
		}

		return BcfaccommodationsaddonControllerConstants.Views.Pages.Hotel.PackageDetailsResponse;
	}

	protected final List<RoomStayCandidateData> getAdjustedRoomStayCandidates(
			final AccommodationFinderForm accommodationFinderForm)
	{
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		if (!CollectionUtils.isEmpty(accommodationFinderForm.getRoomStayCandidates()))
		{
			IntStream.range(0, accommodationFinderForm.getNumberOfRooms()).forEach(index -> {
				final RoomStayCandidateData roomStayCandidateData = accommodationFinderForm.getRoomStayCandidates().get(index);
				roomStayCandidates.add(roomStayCandidateData);
			});
		}
		return roomStayCandidates;
	}

	protected PackageResponseData populatePackageRelatedData(final Model model, final PackageRequestData packageRequestData)
	{

		final PackageResponseData packageResponseData = bcfPackageFacade.getPackageResponse(packageRequestData);
		final FareSelectionData fareSelectionData = packageResponseData.getTransportPackageResponse().getFareSearchResponse();
		if (Objects.nonNull(fareSelectionData))
		{
			// sort fareSelectionData by displayOrder
			final FareSearchRequestData fareSearchRequestData = packageRequestData.getTransportPackageRequest()
					.getFareSearchRequest();
			final String displayOrder = fareSearchRequestData.getSearchProcessingInfo().getDisplayOrder();
			sortFareSelectionData(fareSelectionData, displayOrder);
			populateFareSearchResponseInModel(fareSelectionData, model);
		}
		populateModel(model, packageResponseData);
		return packageResponseData;
	}



}
