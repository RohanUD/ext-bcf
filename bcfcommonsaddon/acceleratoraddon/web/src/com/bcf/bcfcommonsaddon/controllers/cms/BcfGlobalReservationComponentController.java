/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfcommonsaddon.model.components.BcfGlobalReservationComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


@Controller("BcfGlobalReservationComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.BcfGlobalReservationComponent)
public class BcfGlobalReservationComponentController
		extends SubstitutingCMSAddOnComponentController<BcfGlobalReservationComponentModel>
{
	private static final Logger LOGGER = Logger.getLogger(BcfGlobalReservationComponentController.class);

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfAccommodationFacadeHelper")
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BcfGlobalReservationComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		final BcfGlobalReservationData bcfGlobalReservationData;
		boolean isAmendBooking=bcfTravelCartService.isAmendmentCart(bcfTravelCartService.getSessionCart());
		model.addAttribute("isAmendment",isAmendBooking);
		final TravelFinderForm travelFinderForm = sessionService.getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);
		if (!isAmendBooking && StringUtils.isNotEmpty(bookingReference))
		{
			final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);
			bcfGlobalReservationData = globalReservationFacade.getbcfGlobalReservationDataList(order);
			model.addAttribute("bookingReference", bookingReference);
		}
		else
		{
			model.addAttribute(BcfcommonsaddonWebConstants.VEHICLE_NAME,travelFinderForm.getFareFinderForm().getVehicleInfo().stream().findFirst().get().getVehicleType().getName());
			bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();
		}

		if(bcfGlobalReservationData.getAccommodationReservations()!=null && CollectionUtils.isNotEmpty(bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations())){
			bcfGlobalReservationData.getAccommodationReservations().setAccommodationReservations(bcfAccommodationFacadeHelper.sortAccommodationReservations(bcfGlobalReservationData.getAccommodationReservations().getAccommodationReservations()));

		}

		model.addAttribute(BcfcommonsaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);


		model.addAttribute("alacarteJourney", bcfTravelCartFacadeHelper.isAlacateFlow());
		model.addAttribute("isNonTransportBooking", bcfControllerUtil.isNonTransportBooking(bookingReference));
	}




	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOGGER.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}
}
