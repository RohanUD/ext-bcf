/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.accommodation.AvailableServiceData;
import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.RoomPreferenceData;
import de.hybris.platform.commercefacades.accommodation.RoomStayData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.TravelRestrictionFacade;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationExtrasFacade;
import de.hybris.platform.travelfacades.facades.accommodation.RoomPreferenceFacade;
import de.hybris.platform.travelfacades.facades.packages.DealCartFacade;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;
import de.hybris.platform.travelfacades.fare.search.UpgradeFareSearchFacade;
import de.hybris.platform.travelfacades.order.AccommodationCartFacade;
import de.hybris.platform.travelfacades.strategies.AncillaryOfferGroupDisplayStrategy;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.checkin.steps.CheckinStep;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.pages.steps.checkin.AbstractCheckinStepController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.offer.BCFOffersFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.reservation.data.OfferRequestDataList;
import com.bcf.facades.reservation.data.OfferResponseDataList;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.BCFTravellerFacade;


/**
 * Controller for Ancillary Extras page
 */
@Controller
@RequestMapping(
		{ "/ancillary-extras", "/manage-booking/ancillary-extras" })
public class AncillaryExtrasPageController extends AbstractCheckinStepController
{
	private static final String OFFER_GROUPS_VIEW_MAP = "offerGroupsViewMap";
	private static final String ANCILLARY_EXTRAS_DEFAULT_CMS_PAGE = "ancillaryExtrasPage";
	private static final String OFFER_RESPONSES_DATA = "offerResponsesData";
	private static final String ANCILLARY = "ancillary";
	private String defaultRoomBedPreferenceCode;

	@Resource(name = "offersFacade")
	private BCFOffersFacade offersFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "packageFacade")
	private PackageFacade packageFacade;

	@Resource(name = "travelRestrictionFacade")
	private TravelRestrictionFacade travelRestrictionFacade;


	@Resource(name = "roomPreferenceFacade")
	RoomPreferenceFacade roomPreferenceFacade;

	@Resource(name = "accommodationExtrasFacade")
	private AccommodationExtrasFacade accommodationExtrasFacade;

	@Resource(name = "ancillaryOfferGroupDisplayStrategy")
	private AncillaryOfferGroupDisplayStrategy ancillaryOfferGroupDisplayStrategy;

	@Resource(name = "offerGroupsViewMap")
	private Map<String, String> offerGroupsViewMap;

	@Resource(name = "dealCartFacade")
	private DealCartFacade dealCartFacade;

	@Resource(name = "upgradeFareSearchFacade")
	private UpgradeFareSearchFacade upgradeFareSearchFacade;

	@Resource(name = "accommodationCartFacade")
	private AccommodationCartFacade accommodationCartFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAncillaryPage(final Model model,
			@RequestParam(value = "roomStay", required = false) final Integer roomStayRefNumberToUpdate)
			throws CMSItemNotFoundException
	{

		if (!packageFacade.isPackageInCart())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.HOME_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ANCILLARY_EXTRAS_DEFAULT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ANCILLARY_EXTRAS_DEFAULT_CMS_PAGE));

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		bookingAccommodationOnlyDataSetup(sessionBookingJourney, model);
		bookingTransaportOnlyDataSetup(sessionBookingJourney, roomStayRefNumberToUpdate, model);

		model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, getTravelCartFacade().isAmendmentCart());
		model.addAttribute(BcfcommonsaddonWebConstants.IS_DEAL_IN_CART, dealCartFacade.isDealInCart());


		if (getTravelCartFacade().isAmendmentCart())
		{
			model.addAttribute(BcfvoyageaddonWebConstants.ORIGINAL_ORDER_CODE, getTravelCartFacade().getOriginalOrderCode());
		}

		if (StringUtils.isNotEmpty(sessionBookingJourney))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, determineNextUrl());

		return getViewForPage(model);
	}

	private void bookingAccommodationOnlyDataSetup(final String sessionBookingJourney, final Model model)
	{
		if (StringUtils.isEmpty(sessionBookingJourney) || (StringUtils.isNotEmpty(sessionBookingJourney)
				&& !StringUtils.equals(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY)))
		{
			final OfferRequestDataList offerRequestDataList = offersFacade.getOffersRequests();
			final OfferResponseDataList offerResponsesData = offersFacade.getOffers(offerRequestDataList);

			model.addAttribute(OFFER_RESPONSES_DATA, offerResponsesData);
			model.addAttribute(OFFER_GROUPS_VIEW_MAP, offerGroupsViewMap);
			final CartFilterParamsDto cartFilterParamsDto = bcfTravelCartFacade.initializeCartFilterParams(null, null, false,
					Collections.emptyList());
			final BCFTravellersData travellersGroupedByJourney = travellerFacade.getTravellersGroupedByJourney(cartFilterParamsDto);
			model.addAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, travellersGroupedByJourney);

		}
	}

	private void bookingTransaportOnlyDataSetup(final String sessionBookingJourney, final Integer roomStayRefNumberToUpdate,
			final Model model)
	{
		if (StringUtils.isEmpty(sessionBookingJourney) || (StringUtils.isNotEmpty(sessionBookingJourney)
				&& !StringUtils.equals(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY)))
		{
			final AccommodationReservationData accommodationReservationData = bookingFacade
					.getAccommodationReservationDataForGuestDetailsFromCart();

			if (Objects.nonNull(roomStayRefNumberToUpdate)
					|| getTravelCartFacade().isAmendmentCart() && accommodationCartFacade.isNewRoomInCart())
			{
				final List<Integer> roomStayRefNumbers = Objects.nonNull(roomStayRefNumberToUpdate)
						? Collections.singletonList(roomStayRefNumberToUpdate)
						: bookingFacade.getNewAccommodationOrderEntryGroupRefs();
				final List<ReservedRoomStayData> roomStaysToUpdate = accommodationReservationData.getRoomStays().stream()
						.filter(roomStay -> roomStayRefNumbers.contains(roomStay.getRoomStayRefNumber())).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(roomStaysToUpdate))
				{
					accommodationReservationData.setRoomStays(roomStaysToUpdate);
				}
			}

			if (CollectionUtils.isNotEmpty(accommodationReservationData.getRoomStays()))
			{
				final Comparator<ReservedRoomStayData> reservedRoomStayDataComparator = (b1, b2) -> b1.getRoomStayRefNumber()
						.compareTo(b2.getRoomStayRefNumber());
				accommodationReservationData.getRoomStays().sort(reservedRoomStayDataComparator);
			}

			final List<AvailableServiceData> availableServices = accommodationExtrasFacade
					.getAvailableServices(accommodationReservationData);

			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_RESERVATION_DATA, accommodationReservationData);
			model.addAttribute(BcfaccommodationsaddonWebConstants.AVAILABLE_SERVICES, availableServices);

			final Map<Integer, List<RoomPreferenceData>> accommodationRoomPreferenceMap = accommodationReservationData.getRoomStays()
					.stream().collect(
							Collectors.toMap(RoomStayData::getRoomStayRefNumber,
									roomStay -> roomPreferenceFacade.getRoomPreferencesForTypeAndAccommodation(
											BcfaccommodationsaddonWebConstants.ACCOMMODATION_ROOM_PREFERENCE_TYPE,
											roomStay.getRoomTypes())));
			model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_ROOM_PREFERENCE_MAP, accommodationRoomPreferenceMap);

			model.addAttribute(BcfstorefrontaddonWebConstants.DEFAULT_ROOM_BED_PREFERENCE_CODE, getDefaultRoomBedPreferenceCode());
		}
	}

	/**
	 * Redirects user to the next checkout page which is personal details
	 *
	 * @param redirectModel
	 * @return personal details page
	 */
	@RequestMapping(value = "/next", method = RequestMethod.GET)
	public String nextPage(final RedirectAttributes redirectModel)
	{
		final boolean isLongRoute = travelCheckoutFacade.containsLongRoute();

		if (bcfTravelCartFacade.isAmendmentCart())
		{
			if (!bcfTravelCartFacade.hasCartBeenAmended())
			{
				return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.BOOKING_DETAILS_URL
						+ bcfTravelCartFacade.getOriginalOrderCode();
			}

			if (accommodationCartFacade.isNewRoomInCart() && !isLongRoute)
			{
				return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_PATH;
			}

			if (getTravelCustomerFacade().isCurrentUserB2bCustomer())
			{
				return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PAYMENT_TYPE_PATH;
			}

			if (userFacade.isAnonymousUser())
			{
				getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
			}

			if (!isLongRoute)
			{
				return getCheckinStep().nextStep();
			}
		}

		if (userFacade.isAnonymousUser())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.CHECKOUT_LOGIN;
		}

		return isLongRoute ? REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_PATH : getCheckinStep().nextStep();
	}


	protected CheckinStep getCheckinStep()
	{
		return getCheckinStep(ANCILLARY);
	}

	/**
	 * This method determines the next url of checkout flow. If the context is purchase flow, then the next url is
	 * "/ancillary/next" else if the context is Amendments then the next url is "/manage-booking/ancillary/next".
	 *
	 * @return next url
	 */
	protected String determineNextUrl()
	{
		return (bcfTravelCartFacade.isAmendmentCart() ? BcfstorefrontaddonWebConstants.ANCILLARY_EXTRAS_AMENDMENT_PATH
				: BcfstorefrontaddonWebConstants.ANCILLARY_EXTRAS_PATH) + "/next";
	}

	/**
	 * @return the defaultRoomBedPreferenceCode
	 */
	protected String getDefaultRoomBedPreferenceCode()
	{
		return defaultRoomBedPreferenceCode;
	}

	/**
	 * @param defaultRoomBedPreferenceCode the defaultRoomBedPreferenceCode to set
	 */
	@Required
	public void setDefaultRoomBedPreferenceCode(final String defaultRoomBedPreferenceCode)
	{
		this.defaultRoomBedPreferenceCode = defaultRoomBedPreferenceCode;
	}
}
