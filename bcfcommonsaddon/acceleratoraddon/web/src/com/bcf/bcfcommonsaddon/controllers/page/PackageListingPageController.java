/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.accommodation.property.FacilityData;
import de.hybris.platform.commercefacades.accommodation.search.CriterionData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData;
import de.hybris.platform.commercefacades.accommodation.user.data.SearchAddressData;
import de.hybris.platform.commercefacades.packages.PackageData;
import de.hybris.platform.commercefacades.packages.request.PackageSearchRequestData;
import de.hybris.platform.commercefacades.packages.response.PackageSearchResponseData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.facades.TransportOfferingFacade;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationSuggestionFacade;
import de.hybris.platform.travelfacades.facades.packages.PackageSearchFacade;
import de.hybris.platform.travelfacades.facades.packages.strategies.EncodeSearchUrlToMapPackageStrategy;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationFinderValidator;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.validators.FareFinderValidator;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;


/**
 * Controller for Accommodation Search page
 */
@Controller
@RequestMapping("/package-listing")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
public class PackageListingPageController extends AbstractPackagePageController
{
	private static final Logger LOG = Logger.getLogger(PackageListingPageController.class);
	public static final String REDIRECT_PREFIX = "redirect:";

	private static final String PACKAGE_LISTING_CMS_PAGE = "packageListingPage";
	private static final String ROOM_QUERY_STRING_INDICATOR = "r";
	private static final String STARTING_NUMBER_OF_RESULTS = "startingNumberOfResults";
	private static final String TOTAL_NUMBER_OF_RESULTS = "totalNumberOfResults";
	private static final String TOTAL_SHOWN_RESULTS = "totalShownResults";
	private static final String PAGE_NUM = "pageNum";
	private static final String HAS_MORE_RESULTS = "hasMoreResults";
	private static final String PACKAGE_DETAILS_URL_PARAMS = "packageDetailsUrlParams";
	private static final String PROPERTY_NAME = "propertyName";
	private static final String QUERY = "q";
	private static final String HYPHEN_SEPERATOR = " - ";
	private static final String MIN_PACKAGE_PRICE = "minPackagePrice";
	private static final String MAX_PACKAGE_PRICE = "maxPackagePrice";
	private static final String PRICE_RANGE = "priceRange";
	private static final String REGEX_QUERY_PARAMETER = "^(:?[\\p{L}\\p{N}\\[\\]\\_\\-\\+\\[.*?\\]\\/\\s\\p{Sc}\\|]*)*$";
	private static final String PACKAGE_LISTING_FACETVALUES_SHOW = "package.listing.facetvalues.show.";
	private static final String SELECTED_FACET_VALUES = "selectedFacetValues_";

	@Resource(name = "encodeSearchUrlToMapPackageStrategy")
	private EncodeSearchUrlToMapPackageStrategy encodeSearchUrlToMapPackageStrategy;

	@Resource(name = "accommodationSuggestionFacade")
	private AccommodationSuggestionFacade accommodationSuggestionFacade;

	@Resource(name = "fareFinderValidator")
	protected FareFinderValidator fareFinderValidator;

	@Resource(name = "accommodationFinderValidator")
	private AccommodationFinderValidator accommodationFinderValidator;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "transportOfferingFacade")
	private TransportOfferingFacade transportOfferingFacade;

	@Resource(name = "packageSearchFacade")
	private PackageSearchFacade packageSearchFacade;

	@Resource(name = "transportFacilityFacade")
	private TransportFacilityFacade transportFacilityFacade;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	/**
	 * Gets accommodation search page.
	 *
	 * @param travelFinderForm the travel finder form
	 * @param propertyName     the property name
	 * @param query            the query
	 * @param sortCode         the sort code
	 * @param pageNumber       the page number
	 * @param bindingResult    the binding result
	 * @param model            the model
	 * @param request          the request
	 * @return the accommodation search page
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */

	@RequestMapping(method = RequestMethod.GET)
	public String getPackageListingPage( //NOSONAR
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult accommodationBindingResult,
			final BindingResult fareBindingResult, @RequestParam(value = "propertyName", required = false) final String propertyName,
			@RequestParam(value = QUERY, required = false) final String query,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			@RequestParam(value = PRICE_RANGE, required = false) final String priceRange, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(PACKAGE_LISTING_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataRobotsForContentPage(model, contentPageModel.getMetaIndexable(), contentPageModel.getMetaFollow());
		setUpMetaDataForContentPage(model, contentPageModel);

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
			model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
					travelFinderForm.getAccommodationFinderForm());
			model.addAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, travelFinderForm.getFareFinderForm());
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
		if (!bcfTravelCartFacade.isAmendmentCart())
		{
			initializeForms(travelFinderForm.getFareFinderForm(), travelFinderForm.getAccommodationFinderForm(), request);
		}

		validateFareFinderForm(fareFinderValidator, travelFinderForm.getFareFinderForm(), fareBindingResult,
				BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		if (fareBindingResult.hasErrors())
		{
			request.setAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, fareBindingResult);
		}
		else
		{
			validateAccommodationFinderForm(accommodationFinderValidator, travelFinderForm.getAccommodationFinderForm(),
					accommodationBindingResult,
					BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);

			if (accommodationBindingResult.hasErrors())
			{
				request.setAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, accommodationBindingResult);
			}
			else
			{
				final String encodedPropertyName;
				if (StringUtils.isNotBlank(propertyName) && !validateFieldPattern(propertyName,
						TravelacceleratorstorefrontValidationConstants.REGEX_SPECIAL_LETTERS_NUMBER_SPACES))
				{
					model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
							BcfaccommodationsaddonWebConstants.FILTER_PROPERTY_ERROR_MESSAGE);
					encodedPropertyName = StringUtils.EMPTY;
				}
				else
				{
					encodedPropertyName = StringUtils.trim(XSSFilterUtil.filter(propertyName));
				}
				model.addAttribute(BcfaccommodationsaddonWebConstants.FILTER_PROPERTY_NAME, encodedPropertyName);

				String validateSortCode = sortCode;
				if (StringUtils.isNotBlank(sortCode)
						&& !validateFieldPattern(sortCode, TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES))
				{
					model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
							BcfaccommodationsaddonWebConstants.SORT_CODE_ERROR_MESSAGE);
					validateSortCode = null;
				}

				/*
				 * Below block is to re-set the facets whenever there is a change in the currency
				 */
				if (!StringUtils.equals(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PREVIOUS_CURRENCY),
						storeSessionFacade.getCurrentCurrency().getIsocode()))
				{
					return getModifiedQueryString(request);
				}

				String validateQuery = query;
				if (StringUtils.isNotBlank(query)
						&& !validateFieldPattern(query, REGEX_QUERY_PARAMETER))
				{
					model.addAttribute(BcfaccommodationsaddonWebConstants.ACCOMMODATION_SEARCH_PARAMS_ERROR,
							BcfaccommodationsaddonWebConstants.FILTER_QUERY_ERROR_MESSAGE);
					validateQuery = null;
				}

				final PackageSearchRequestData packageSearchRequestData = new PackageSearchRequestData();
				packageSearchRequestData.setCriterion(
						getCriterionData(travelFinderForm.getAccommodationFinderForm(), request, encodedPropertyName, validateQuery,
								validateSortCode));
				final FareSearchRequestData fareSearchRequestData = prepareFareSearchRequestData(travelFinderForm.getFareFinderForm(),
						request);
				packageSearchRequestData.setFareSearchRequestData(fareSearchRequestData);

				if (StringUtils.isNotBlank(priceRange))
				{
					populatePriceRangeFilters(packageSearchRequestData, priceRange);
				}

				final PackageSearchResponseData packageSearchResponseData = packageSearchFacade.doSearch(packageSearchRequestData);
				populateModelWithPackagePriceRange(packageSearchResponseData, model, priceRange);

				setSelectedFacetDisplayValues(model, packageSearchResponseData);
				final String packageDetailsUrlParams = buildPackageDetailsPageUrlParameters(
						travelFinderForm.getAccommodationFinderForm(), travelFinderForm.getFareFinderForm());

				getSessionService().setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
						travelFinderForm.getAccommodationFinderForm());
				getSessionService()
						.setAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_FINDER_FORM, travelFinderForm.getFareFinderForm());
				getSessionService().setAttribute(BcfcommonsaddonWebConstants.PACKAGE_SEARCH_RESPONSE, packageSearchResponseData);
				getSessionService().setAttribute(PACKAGE_DETAILS_URL_PARAMS, packageDetailsUrlParams);
				getSessionService().setAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES,
						packageSearchResponseData.getProperties());

				populateCommonModelAttributes(0, pageNumber, resultsViewType, packageSearchResponseData, model);
				model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
						getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));
				model.addAttribute(BcfstorefrontaddonWebConstants.MODIFY,
						Boolean.valueOf(request.getParameter(BcfstorefrontaddonWebConstants.MODIFY)));

				model.addAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM, travelFinderForm);
				if (bcfTravelCartService.hasSessionCart())
				{
					final CartModel sessionCart = bcfTravelCartService.getSessionCart();
					model.addAttribute("isAmendment", bcfTravelCartService.isAmendmentCart(sessionCart));
				}
			}
		}
		return getViewForPage(model);

	}

	/**
	 * Initialize forms.
	 *
	 * @param fareFinderForm          the fare finder form
	 * @param accommodationFinderForm the accommodation finder form
	 * @param request                 the request
	 */
	protected void initializeForms(final FareFinderForm fareFinderForm, final AccommodationFinderForm accommodationFinderForm,
			final HttpServletRequest request)
	{
		if (StringUtils.isEmpty(fareFinderForm.getArrivalLocationName())
				|| StringUtils.isEmpty(fareFinderForm.getDepartureLocationName()))
		{
			resolveLocationFields(fareFinderForm);
		}
		if (Objects.isNull(accommodationFinderForm.getRoomStayCandidates()))
		{
			accommodationFinderForm.setRoomStayCandidates(createRoomStayCandidatesForSearchPage(request));
			fareFinderForm.setPassengerTypeQuantityList(createPassengerTypeQuantityData(accommodationFinderForm.getNumberOfRooms(),
					accommodationFinderForm.getRoomStayCandidates()));
		}
	}

	/**
	 * Resolve location name fields for fareFinderForm.
	 *
	 * @param fareFinderForm the fare finder form
	 */
	protected void resolveLocationFields(final FareFinderForm fareFinderForm)
	{
		fareFinderForm.setDepartureLocationName(
				getLocationName(fareFinderForm.getDepartureLocation()));
		fareFinderForm.setArrivalLocationName(
				getLocationName(fareFinderForm.getArrivalLocation()));
	}

	/**
	 * Gets the result view type.
	 *
	 * @param resultViewType the result view type
	 * @return the result view type
	 */
	protected String getResultViewType(final String resultViewType)
	{
		if (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_DEFAULT,
				resultViewType)
				|| StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_GRID,
				resultViewType)
				|| StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_MAP,
				resultViewType))
		{
			return resultViewType;
		}
		return BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_DEFAULT;
	}

	/**
	 * Prepare accommodation search request data accommodation search request data.
	 *
	 * @param accommodationFinderForm the accommodation finder form
	 * @param request                 the request
	 * @param propertyName            the property name
	 * @param query                   the query
	 * @param sortCode                the sort code
	 * @return the accommodation search request data
	 */
	protected CriterionData getCriterionData(final AccommodationFinderForm accommodationFinderForm,
			final HttpServletRequest request, final String propertyName, final String query, final String sortCode)
	{
		final CriterionData criterionData = new CriterionData();

		//set Address Data
		final SearchAddressData addressData = new SearchAddressData();
		addressData.setFormattedIndicator(false);
		addressData.setLine1(accommodationFinderForm.getDestinationLocation());
		addressData.setLine2(accommodationFinderForm.getDestinationLocationName());
		criterionData.setAddress(addressData);

		// set Facility Data
		final List<FacilityData> amenities = new ArrayList<>();
		criterionData.setAmenities(amenities);

		// set Stay Range Data
		final StayDateRangeData stayRangeData = new StayDateRangeData();
		stayRangeData.setStartTime(TravelDateUtils.convertStringDateToDate(accommodationFinderForm.getCheckInDateTime(),
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));
		stayRangeData.setEndTime(TravelDateUtils.convertStringDateToDate(accommodationFinderForm.getCheckOutDateTime(),
				BcfcommonsaddonWebConstants.DATE_PATTERN_MM_DD_YYYY));
		stayRangeData
				.setLengthOfStay((int) TravelDateUtils.getDaysBetweenDates(stayRangeData.getStartTime(), stayRangeData.getEndTime()));
		criterionData.setStayDateRange(stayRangeData);

		// set roomStayCandidates
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		if (!CollectionUtils.isEmpty(accommodationFinderForm.getRoomStayCandidates()))
		{
			IntStream.range(0, accommodationFinderForm.getNumberOfRooms()).forEach(index -> {
				final RoomStayCandidateData roomStayCandidateData = accommodationFinderForm.getRoomStayCandidates().get(index);
				roomStayCandidates.add(roomStayCandidateData);
			});
		}
		criterionData.setRoomStayCandidates(roomStayCandidates);

		criterionData.setPropertyFilterText(propertyName);
		criterionData.setQuery(query);
		criterionData.setSort(sortCode);
		criterionData.setSuggestionType(accommodationFinderForm.getSuggestionType());

		return criterionData;
	}

	/**
	 * Build package details page url parameters string.
	 *
	 * @param accommodationFinderForm accommodationFinderForm
	 * @param fareFinderForm          fareFinderForm
	 * @return the string
	 */
	protected String buildPackageDetailsPageUrlParameters(final AccommodationFinderForm accommodationFinderForm,
			final FareFinderForm fareFinderForm)
	{
		final StringBuilder urlParameters = new StringBuilder();

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION, fareFinderForm.getDepartureLocation());

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION, fareFinderForm.getArrivalLocation());

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.DEPARTING_DATE_TIME, fareFinderForm.getDepartingDateTime());

		if (StringUtils.equalsIgnoreCase(TripType.RETURN.toString(), fareFinderForm.getTripType()))
		{
			appendParameter(urlParameters, BcfstorefrontaddonWebConstants.RETURN_DATE_TIME, fareFinderForm.getReturnDateTime());
		}

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.TRIP_TYPE, fareFinderForm.getTripType());

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.CABIN_CLASS, fareFinderForm.getCabinClass());

		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.CHECKIN_DATE, accommodationFinderForm.getCheckInDateTime());
		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.CHECKOUT_DATE, accommodationFinderForm.getCheckOutDateTime());
		appendParameter(urlParameters, BcfstorefrontaddonWebConstants.NUMBER_OF_ROOMS,
				String.valueOf(accommodationFinderForm.getNumberOfRooms()));

		final int numberOfRooms = accommodationFinderForm.getNumberOfRooms();
		for (int i = 0; i < numberOfRooms; i++)
		{
			final StringBuilder guestsStringPerRoom = new StringBuilder();
			final List<PassengerTypeQuantityData> guestCounts = accommodationFinderForm.getRoomStayCandidates().get(i)
					.getPassengerTypeQuantityList();
			for (final PassengerTypeQuantityData guestCount : guestCounts)
			{
				final StringBuilder ages = new StringBuilder();
				if (CollectionUtils.isNotEmpty(guestCount.getChildAges()))
				{
					for (final int age : guestCount.getChildAges())
					{
						ages.append(age).append("-");
					}
					ages.deleteCharAt(ages.length() - 1);

				}

				final String passengerType = guestCount.getPassengerType().getCode();
				final int passengerQuantity = guestCount.getQuantity();

				final StringBuilder guestParam = new StringBuilder(String.valueOf(passengerQuantity)).append("-")
						.append(passengerType);
				if (ages.length() > 0)
				{
					guestParam.append("-").append(ages.toString());
				}
				guestsStringPerRoom.append(guestParam.toString());
				guestsStringPerRoom.append(",");
			}
			String result = guestsStringPerRoom.toString();
			result = result.substring(0, result.length() - 1);

			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
			urlParameters.append(ROOM_QUERY_STRING_INDICATOR).append(i);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			urlParameters.append(result);
		}

		return urlParameters.toString();
	}

	protected void appendParameter(final StringBuilder urlParameters, final String parameter, final String value)
	{
		urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		urlParameters.append(parameter);
		urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
		urlParameters.append(value);
	}

	/**
	 * Update criterion data for price range filter.
	 *
	 * @param packageSearchResponseData the package search response data
	 * @param model                     the model
	 * @param priceRange                the price range
	 */
	protected void populateModelWithPackagePriceRange(final PackageSearchResponseData packageSearchResponseData, final Model model,
			final String priceRange)
	{
		if (StringUtils.isNotBlank(priceRange))
		{
			model.addAttribute(PRICE_RANGE, priceRange);
		}

		final List<PropertyData> packageSearchResponseProperties = Objects.nonNull(packageSearchResponseData)
				? packageSearchResponseData.getProperties()
				: Collections.emptyList();

		/* to min total package price */
		long minPackagePrice = 0;
		final PackageData minPricedPackage = packageSearchFacade.getMinPricedPackage(packageSearchResponseProperties);
		if (Objects.nonNull(minPricedPackage))
		{
			minPackagePrice = Objects.nonNull(minPricedPackage.getTotalPackagePrice())
					? minPricedPackage.getTotalPackagePrice().getValue().longValue()
					: 0;
		}

		/* to max total package price */
		long maxPackagePrice = 0;
		final PackageData maxPricedPackage = packageSearchFacade.getMaxPricedPackage(packageSearchResponseProperties);
		if (Objects.nonNull(maxPricedPackage))
		{
			final double totalPackagePrice = Objects.nonNull(maxPricedPackage.getTotalPackagePrice())
					? maxPricedPackage.getTotalPackagePrice().getValue().doubleValue()
					: 0;
			maxPackagePrice = (long) Math.ceil(totalPackagePrice);
		}

		model.addAttribute(MIN_PACKAGE_PRICE, minPackagePrice);
		model.addAttribute(MAX_PACKAGE_PRICE, maxPackagePrice);
	}

	/**
	 * Display method to return the properties to mark on Map.
	 *
	 * @return the string
	 */
	@RequestMapping("/display-view")
	public String displayView(final Model model,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber,
			@RequestParam(value = PRICE_RANGE, required = false) final String priceRange)
	{
		final PackageSearchResponseData packageSearchResponseData = getPackageSearchWithAppliedPriceRange(priceRange);
		populateCommonModelAttributes(0, pageNumber, resultsViewType, packageSearchResponseData, model);
		return BcfcommonsaddonControllerConstants.Views.Pages.PackageSearch.packageResultsViewJsonResponse;
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param pageNumber the page number
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping("/show-more")
	public String showMore(@RequestParam(value = "pageNumber", required = false) final int pageNumber,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			@RequestParam(value = PRICE_RANGE, required = false) final String priceRange, final Model model)
	{
		final PackageSearchResponseData packageSearchResponseData = getPackageSearchWithAppliedPriceRange(priceRange);
		populateCommonModelAttributes(pageNumber - 1, pageNumber, resultsViewType, packageSearchResponseData, model);
		if (bcfTravelCartService.hasSessionCart())
		{
			final CartModel sessionCart = bcfTravelCartService.getSessionCart();
			model.addAttribute("isAmendment", bcfTravelCartService.isAmendmentCart(sessionCart));
		}
		return BcfcommonsaddonControllerConstants.Views.Pages.PackageSearch.packageListingJsonResponse;
	}

	/**
	 * Gets the package search results page size.
	 *
	 * @param resultsViewType the results view type
	 * @return the package search results page size
	 */
	protected int getPackageSearchResultsPageSize(final String resultsViewType)
	{
		if (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_MAP,
				resultsViewType))
		{
			return MAX_PAGE_LIMIT;
		}

		int packageSearchPageSize = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.ACCOMMODATION_SEARCH_RESULT_PAGE_SIZE);
		packageSearchPageSize = packageSearchPageSize > 0 ? packageSearchPageSize : MAX_PAGE_LIMIT;
		return packageSearchPageSize;
	}


	/**
	 * Populate common model attributes.
	 *
	 * @param pageNumber                the page number
	 * @param resultsViewType           the results view type
	 * @param packageSearchResponseData the package search response data
	 * @param model                     the model
	 */
	protected void populateCommonModelAttributes(final int startIndex, final int pageNumber, final String resultsViewType,
			final PackageSearchResponseData packageSearchResponseData, final Model model)
	{
		final List<PropertyData> packageSearchResponseProperties = packageSearchResponseData.getProperties().stream()
				.filter(propertyData -> (propertyData instanceof PackageData && ((PackageData) propertyData).getFiltered()))
				.collect(Collectors.toList());
		final int packageSearchPageSize = getPackageSearchResultsPageSize(resultsViewType);
		model.addAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES,
				packageSearchResponseProperties.subList(startIndex * packageSearchPageSize,
						Math.min(pageNumber * packageSearchPageSize, packageSearchResponseProperties.size())));
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_SEARCH_RESPONSE, packageSearchResponseData);
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_DETAILS_URL_PARAMETERS,
				getSessionService().getAttribute(PACKAGE_DETAILS_URL_PARAMS));
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE,
				getResultViewType(resultsViewType));
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(START_PAGE_NUMBER,
				getJsonPaginationStartPageNumber(packageSearchResponseProperties.size(), packageSearchPageSize, pageNumber));
		model.addAttribute(END_PAGE_NUMBER,
				getJsonPaginationEndPageNumber(packageSearchResponseProperties.size(), packageSearchPageSize, pageNumber));
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, packageSearchResponseProperties.size());
		model.addAttribute(STARTING_NUMBER_OF_RESULTS, (pageNumber - 1) * packageSearchPageSize + 1);
		final Boolean hasMoreResults = pageNumber * packageSearchPageSize < packageSearchResponseProperties.size();
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);
		model.addAttribute(TOTAL_SHOWN_RESULTS,
				hasMoreResults ? pageNumber * packageSearchPageSize : packageSearchResponseProperties.size());
	}

	/**
	 * Below method clears the q and propertyName request parameters from the query string as they are supposed to be
	 * cleared on currency change
	 *
	 * @param request
	 * @return
	 */
	protected String getModifiedQueryString(final HttpServletRequest request)
	{
		final StringBuilder urlParameters = new StringBuilder();
		final Map<String, String[]> map = request.getParameterMap();
		final Set<String> keys = map.keySet();
		for (final String key : keys)
		{
			if (StringUtils.equals(QUERY, key) || StringUtils.equals(PROPERTY_NAME, key) || StringUtils.equals(PRICE_RANGE, key))
			{
				continue;
			}
			urlParameters.append(key);
			urlParameters.append(BcfstorefrontaddonWebConstants.EQUALS);
			String value = map.get(key)[0];
			if (value.contains("|"))
			{
				value = value.replaceAll("\\|", "%7C");
			}
			urlParameters.append(value);
			urlParameters.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		}
		String urlParametersString = urlParameters.toString();
		if (StringUtils.isNotBlank(urlParametersString))
		{
			urlParametersString = urlParametersString.substring(0, urlParametersString.length() - 1);
		}

		return REDIRECT_PREFIX + BcfcommonsaddonWebConstants.PACKAGE_LISTING_PATH + "?" + urlParametersString;
	}

	/**
	 * Gets the price range filtered packages.
	 *
	 * @param priceRange      the price range
	 * @param resultsViewType the results view type
	 * @param pageNumber      the page number
	 * @param model           the model
	 * @return the price range filtered packages
	 */
	@RequestMapping("/filter-price-range")
	public String getPriceRangeFilteredPackages(@RequestParam(value = PRICE_RANGE, required = false) final String priceRange,
			@RequestParam(value = "resultsViewType", required = false, defaultValue = "listView") final String resultsViewType,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber, final Model model)
	{
		final PackageSearchResponseData packageSearchResponseData = getPackageSearchWithAppliedPriceRange(priceRange);
		populateCommonModelAttributes(0, pageNumber, resultsViewType, packageSearchResponseData, model);

		if (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_MAP,
				resultsViewType))
		{
			return BcfcommonsaddonControllerConstants.Views.Pages.PackageSearch.packageResultsViewJsonResponse;
		}

		return BcfcommonsaddonControllerConstants.Views.Pages.PackageSearch.packageListingJsonResponse;
	}

	/**
	 * Gets the package search with applied price range.
	 *
	 * @param priceRange the price range
	 * @return the package search with applied price range
	 */
	protected PackageSearchResponseData getPackageSearchWithAppliedPriceRange(final String priceRange)
	{
		final PackageSearchRequestData packageSearchRequestData = new PackageSearchRequestData();
		if (StringUtils.isNotBlank(priceRange))
		{
			populatePriceRangeFilters(packageSearchRequestData, priceRange);
		}
		return packageSearchFacade.getFilteredPackageResponseFilteredByPriceRange(packageSearchRequestData);
	}

	/**
	 * Populate price range filters.
	 *
	 * @param packageSearchRequestData the package search request data
	 * @param priceRange               the price range
	 */
	private void populatePriceRangeFilters(final PackageSearchRequestData packageSearchRequestData, final String priceRange)
	{
		final String[] rangeArr = priceRange.split(HYPHEN_SEPERATOR);
		long lowerPriceRange = 0;
		long upperPriceRange = 0;
		if (CollectionUtils.size(rangeArr) > 1)
		{
			try
			{
				lowerPriceRange = Long.valueOf(StringUtils.substring(rangeArr[0], 1));
				upperPriceRange = Long.valueOf(StringUtils.substring(rangeArr[1], 1));
				packageSearchRequestData.setMinPrice(lowerPriceRange);
				packageSearchRequestData.setMaxPrice(upperPriceRange);
			}
			catch (final NumberFormatException e)
			{
				LOG.error("Cannot parse price range limit string to integer" + e.getClass().getName() + " : " + e.getMessage());
			}
		}
	}

	/**
	 * Validate field pattern boolean.
	 *
	 * @param attribute the attribute
	 * @param pattern   the pattern
	 * @return the boolean
	 */
	protected Boolean validateFieldPattern(final String attribute, final String pattern)
	{
		if (!attribute.matches(pattern))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Validate accommodation finder form.
	 *
	 * @param accommodationFinderValidator the accommodation finder validator
	 * @param accommodationFinderForm      the accommodation finder form
	 * @param bindingResult                the binding result
	 * @param formName                     the form name
	 */
	protected void validateAccommodationFinderForm(final AbstractTravelValidator accommodationFinderValidator,
			final AccommodationFinderForm accommodationFinderForm, final BindingResult bindingResult, final String formName)
	{
		accommodationFinderValidator.setTargetForm(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.setAttributePrefix("");
		accommodationFinderValidator.validate(accommodationFinderForm, bindingResult);
	}


	/**
	 * Validate fare finder form.
	 *
	 * @param fareFinderValidator the fare finder validator
	 * @param fareFinderForm      the fare finder form
	 * @param bindingResult       the binding result
	 * @param formName            the form name
	 */
	protected void validateFareFinderForm(final AbstractTravelValidator fareFinderValidator, final FareFinderForm fareFinderForm,
			final BindingResult bindingResult, final String formName)
	{
		fareFinderValidator.setTargetForm(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		fareFinderValidator.setAttributePrefix("");
	}

	private void setSelectedFacetDisplayValues(final Model model, final PackageSearchResponseData packageSearchResponseData)
	{
		if (Objects.nonNull(packageSearchResponseData) && Objects.nonNull(packageSearchResponseData.getCriterion()))
		{
			StreamUtil.safeStream(packageSearchResponseData.getCriterion().getFacets())
					.map(FacetData::getCode)
					.forEach(facetCode -> setFilteredFacets(model, facetCode));
		}
	}

	private void setFilteredFacets(final Model model, final String facetCode)
	{
		model.addAttribute(SELECTED_FACET_VALUES + facetCode, getConfigurationService().getConfiguration()
				.getString(PACKAGE_LISTING_FACETVALUES_SHOW + facetCode, StringUtils.EMPTY));
	}

}
