/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_TOTAL_DATA;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.TotalFareData;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.Arrays;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Controller for Fare Selection page
 */
@Controller("PackageFerryReviewPageController")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping("/package-review-ferries")
public class PackageFerryReviewPageController extends AbstractPackagePageController
{
	private static final String PACKAGE_FERRY_REVIEW_CMS_PAGE = "packageFerryReviewPage";

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	/**
	 * Method responsible for handling GET request on Fare Selection page
	 *
	 * @param travelFinderForm
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return fare selection page
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
	public String getPackageFerryReviewPage(
			@Valid @ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "true");
			redirectModel.addFlashAttribute(BcfcommonsaddonWebConstants.TRAVEL_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PACKAGE_FERRY_SELECTION_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(PACKAGE_FERRY_REVIEW_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PACKAGE_FERRY_REVIEW_CMS_PAGE));
		final TotalFareData bcfGlobalReservationData = globalReservationFacade
				.getGlobalReservationForTotalPricewithTax(
						Arrays.asList(OrderEntryType.TRANSPORT, OrderEntryType.ACCOMMODATION));
		model.addAttribute(GLOBAL_RESERVATION_TOTAL_DATA, bcfGlobalReservationData);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, travelFinderForm.getAccommodationFinderForm());
		model.addAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, travelFinderForm.getFareFinderForm());
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_STATUS, travelFinderForm.getAvailabilityStatus());
		model.addAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "false");
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		if (request.getParameterMap().containsKey(BcfstorefrontaddonWebConstants.MODIFY)){
			model.addAttribute(BcfstorefrontaddonWebConstants.MODIFY, true);

		}
		final String travelRoute = travelFinderForm.getTravelRoute();
		model.addAttribute(BcfFacadesConstants.RESERVABLE, bcfTravelRouteFacade.getTravelRouteReservableValue(travelRoute));
		return getViewForPage(model);
	}
}
