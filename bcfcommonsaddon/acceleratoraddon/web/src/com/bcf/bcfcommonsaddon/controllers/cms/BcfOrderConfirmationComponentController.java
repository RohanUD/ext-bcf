/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.model.components.BcfOrderConfirmationComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.BcfTermsAndConditionsFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


@Controller("BcfOrderConfirmationComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.BcfOrderConfirmationComponent)
public class BcfOrderConfirmationComponentController
		extends SubstitutingCMSAddOnComponentController<BcfOrderConfirmationComponentModel>
{
	private static final String TERMS_AND_CONDITIONS = "termsAndConditions";
	private static final String SALES_CHANNEL_FOR_OPERATOR_DISPLAY_MESSAGE = "sales.channels.for.operator.display.message";
	private static final String SHOW_ADDITIONAL_MESSAGES = "showAdditionalMessages";

	protected static final Logger LOG = Logger.getLogger(BcfOrderConfirmationComponentController.class);

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfTermsAndConditionsFacade")
	private BcfTermsAndConditionsFacade bcfTermsAndConditionsFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BcfOrderConfirmationComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		if (StringUtils.isNotBlank(bookingReference))
		{
			final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);

			BcfGlobalReservationData bcfGlobalReservationData =null;
			BcfGlobalReservationData bcfFerryReservationData = globalReservationFacade.getbcfGlobalReservationDataListForFerry(order);

			if (!BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(order.getBookingJourneyType()))
			{
				bcfGlobalReservationData = globalReservationFacade.getbcfGlobalReservationDataList(order);
				if(bcfFerryReservationData!=null){
					bcfGlobalReservationData.setTransportReservations(bcfFerryReservationData.getTransportReservations());
				}

			}else{
				bcfGlobalReservationData=bcfFerryReservationData;
			}

			model.addAttribute(BcfcommonsaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);
			model.addAttribute(BcfcommonsaddonWebConstants.ORDER_NUMBER, order.getCode());
			final Map<String, String> propertyTermsConditionMap = bcfTermsAndConditionsFacade
					.getPropertyTermsConditionMap(bcfGlobalReservationData);
			if (bcfGlobalReservationData.getActivityReservations() != null)
			{
				final Map<String, String> activityTermsConditionMap = bcfTermsAndConditionsFacade
						.getActivityTermsConditionMap(bcfGlobalReservationData);
				if (MapUtils.isNotEmpty(activityTermsConditionMap))
				{
					model.addAttribute(BcfcommonsaddonWebConstants.ACTIVITY_TERMS_CONDITION_MAP, activityTermsConditionMap);
				}
			}
			if (MapUtils.isNotEmpty(propertyTermsConditionMap))
			{
				model.addAttribute(BcfcommonsaddonWebConstants.PROPERTY_TERMS_CONDITION_MAP, propertyTermsConditionMap);
			}
			if(BookingJourneyType.BOOKING_PACKAGE.equals(order.getBookingJourneyType()) || (bcfGlobalReservationData.getTransportReservations()!=null && CollectionUtils.isNotEmpty(bcfGlobalReservationData.getTransportReservations().getReservationDatas())))
			{
				model.addAttribute(TERMS_AND_CONDITIONS,
						bcfTermsAndConditionsFacade.getTermsAndConditionsForBooking(bookingReference));
			}
			if (BCFPropertiesUtils
					.convertToList(configurationService.getConfiguration().getString(SALES_CHANNEL_FOR_OPERATOR_DISPLAY_MESSAGE))
					.contains(bcfSalesApplicationResolverFacade.getCurrentSalesChannel()))
			{
				model.addAttribute(SHOW_ADDITIONAL_MESSAGES, true);
			}
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, order.getBookingJourneyType());
		}
	}

	@Override
	protected String getView(final BcfOrderConfirmationComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		if (StringUtils.isNotBlank(bookingReference))
		{
			final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);
			if (Objects.nonNull(order) && order.isFerryOptionBooking())
			{
				return BcfcommonsaddonControllerConstants.Views.CMS.tentativeOrderConfirmation;
			}
		}
		return super.getView(component);
	}
}


