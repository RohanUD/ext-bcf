/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonConstants;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.DealFinderForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.DealBundleSearchFacade;
import com.bcf.facades.deals.search.request.data.DealSearchCriteriaData;
import com.bcf.facades.deals.search.response.data.DealsResponseData;


@Controller
@RequestMapping("/deal-selection")
public class DealSelectionPageController extends AbstractPackagePageController
{
	private static final String DEAL_SELECTION_CMS_PAGE = "dealSelectionPage";

	@Resource(name = "dealBundleSearchFacade")
	private DealBundleSearchFacade dealBundleSearchFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "dealFinderValidator")
	private AbstractTravelValidator dealFinderValidator;

	@RequestMapping(method = RequestMethod.GET)
	public String getDealSelectionPage(
			@Valid @ModelAttribute("dealFinderForm") final DealFinderForm dealFinderForm,
			@RequestParam(value = "q", required = false) final String query,
			@RequestParam(value = "sort", required = false) final String sortCode,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		String validateQuery = query;
		if (StringUtils.isNotBlank(query)
				&& !dealFinderValidator
				.validateFieldPattern(query, BcfcommonsaddonConstants.VALIDATE_QUERY_PARAM_REGEX))
		{
			model.addAttribute(BcfcommonsaddonWebConstants.DEAL_SEARCH_PARAMS_ERROR,
					BcfcommonsaddonWebConstants.FILTER_QUERY_ERROR_MESSAGE);
			validateQuery = null;
		}

		String validateSortCode = sortCode;
		if (StringUtils.isNotBlank(sortCode)
				&& !dealFinderValidator
				.validateFieldPattern(sortCode, TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES))
		{
			model.addAttribute(BcfcommonsaddonWebConstants.DEAL_SEARCH_PARAMS_ERROR,
					BcfcommonsaddonWebConstants.SORT_CODE_ERROR_MESSAGE);
			validateSortCode = null;
		}

		final DealSearchCriteriaData dealSearchCriteriaData = prepareDealSearchRequestData(dealFinderForm, request, validateQuery,
				validateSortCode);
		model.addAttribute("dealsearchParams", encodeSearchData(dealSearchCriteriaData));
		model.addAttribute("dealSearchCriteriaData", dealSearchCriteriaData);
		model.addAttribute("dealSelectedDepartureDate",
				TravelDateUtils.convertDateToStringDate(TravelDateUtils.convertStringDateToDate(dealFinderForm.getDepartureDate(),
						BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY), TravelservicesConstants.DATE_PATTERN));
		final DealsResponseData dealsResponseDataList = dealBundleSearchFacade.doSearch(dealSearchCriteriaData);
		model.addAttribute("dealsResponseDataList", dealsResponseDataList);

		sessionService.setAttribute("dealsResponse", dealsResponseDataList);

		storeCmsPageInModel(model, getContentPageForLabelOrId(DEAL_SELECTION_CMS_PAGE));
		return getViewForPage(model);
	}

	protected Map<String, String> encodeSearchData(final DealSearchCriteriaData dealSearchCriteriaData)
	{
		final Map<String, String> paramMap = new HashMap();
		if (dealSearchCriteriaData.getDealType() != null)
		{
			paramMap.put("dealType", dealSearchCriteriaData.getDealType());
		}

		if (dealSearchCriteriaData.getDurationOfStay() != null)
		{
			paramMap.put("duration", String.valueOf(dealSearchCriteriaData.getDurationOfStay()));
		}

		if (dealSearchCriteriaData.getStarRating() != null)
		{
			paramMap.put("starRating", String.valueOf(dealSearchCriteriaData.getStarRating()));
		}

		if (StringUtils.isNotEmpty(dealSearchCriteriaData.getOriginLocation()))
		{
			paramMap.put("departureLocation", dealSearchCriteriaData.getOriginLocation());
		}

		if (StringUtils.isNotEmpty(dealSearchCriteriaData.getDestinationLocation()))
		{
			paramMap.put("arrivalLocation", dealSearchCriteriaData.getDestinationLocation());
		}

		if (StringUtils.isNotEmpty(dealSearchCriteriaData.getHotelLocation()))
		{
			paramMap.put("hotelLocation", dealSearchCriteriaData.getHotelLocation());
		}

		if (StringUtils.isNotEmpty(dealSearchCriteriaData.getDepartureDate()))
		{
			paramMap.put("departureDate", dealSearchCriteriaData.getDepartureDate());
		}


		return paramMap;
	}

	protected DealSearchCriteriaData prepareDealSearchRequestData(final DealFinderForm dealFinderForm,
			final HttpServletRequest request, final String validateQuery, final String validateSortCode)
	{
		final DealSearchCriteriaData dealSearchCriteriaData = new DealSearchCriteriaData();

		if (dealFinderForm.getStarRating() > 0)
		{
			dealSearchCriteriaData.setStarRating(dealFinderForm.getStarRating());
		}
		dealSearchCriteriaData.setDurationOfStay(dealFinderForm.getDuration());
		dealSearchCriteriaData.setHotelLocation(dealFinderForm.getLocation());
		dealSearchCriteriaData.setOriginLocation(dealFinderForm.getDepartureLocation());
		dealSearchCriteriaData.setDestinationLocation(dealFinderForm.getArrivalLocation());
		if (StringUtils.isNotBlank(dealFinderForm.getDealType()))
		{
			dealSearchCriteriaData.setDealType(dealFinderForm.getDealType());
		}
		dealSearchCriteriaData.setQuery(validateQuery);
		dealSearchCriteriaData.setSort(validateSortCode);

		if (StringUtils.equalsIgnoreCase(dealFinderForm.getSuggestionType(), SuggestionType.PROPERTY.toString()))
		{
			dealSearchCriteriaData.setHotelName(dealFinderForm.getLocation());
		}
		else if (StringUtils.equalsIgnoreCase(dealFinderForm.getSuggestionType(), SuggestionType.LOCATION.toString()))
		{
			dealSearchCriteriaData.setLocationCode(dealFinderForm.getLocation());
		}

		dealSearchCriteriaData.setDepartureDate(dealFinderForm.getDepartureDate());

		return dealSearchCriteriaData;
	}

}


