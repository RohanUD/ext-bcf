/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.forms.cms.AgentDeclareForm;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.GiftCardType;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.BcfUserGroupFacade;
import com.bcf.facades.bcffacades.BcfEndOfDayReportFacade;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.notification.request.payment.EndOfDayReportData;


@Controller
@RequestMapping("/endofday-report")
public class EndOfDayReportPageController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(EndOfDayReportPageController.class);
	private static final String ORDER_REPORT_CMS_PAGE = "endOfDayReportPage";
	private static final String CONFIRM_ORDER_REPORT_CMS_PAGE = "endOfDayConfirmReportPage";
	private static final String SUPERVISOR_DECLARE_REPORT_CMS_PAGE = "endOfDaySupervisorReportPage";
	private static final String ORDER_THANKYOU_REPORT_CMS_PAGE = "endOfDayThankyouReportPage";
	private static final String AGENT_DECLARE_FORM = "agentDeclareForm";
	private static final String PORT_LOCATION = "PortLocation";
	private static final String USER_ID = "UserId";
	private static final String USER_NAME = "UserName";
	private static final String SESSION_START = "SessionStart";
	private static final String DECLARE_END = "DeclareEnd";
	private static final String DECLARE_BY = "DeclareBy";
	private static final String CASH_DECLARED_AMOUNT = "CashDeclaredAmount";
	private static final String BANK_DEPOSIT_NUMBER = "BankDepositNumber";
	private static final String GIFT_CARD_DECLARED_TYPES = "GiftCardDeclaredTypes";
	private static final String CARD_DECLARED_TYPES = "CardDeclaredTypes";
	private static final String END_OF_DAY_REPORT_DATA = "endOfDayReportData";
	private static final String AGENT_DECLARE_DATA = "agentDeclareData";
	private static final String SESSION_END_OF_DAY_REPORT_DATA = "sessionEndOfDayReportData";
	private static final String AGENT_TO_DECLARE = "agents";
	public static final String SUPPORTED_CARD_TYPES = "supportedCardTypes";
	public static final String BOOLEAN_NO = "N";
	public static final String BOOLEAN_YES = "Y";

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "bcfEndOfDayReportFacade")
	private BcfEndOfDayReportFacade bcfEndOfDayReportFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Resource(name = "bcfUserGroupFacade")
	private BcfUserGroupFacade bcfUserGroupFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@RequestMapping(method = RequestMethod.GET)
	public String getOrderReport(final HttpServletRequest request, final HttpServletResponse response,
			final Model model, @RequestParam(value = "agentToDeclare") final String agentToDeclare) throws CMSItemNotFoundException
	{
		final AgentDeclareForm agentDeclareForm;

		if (Objects.nonNull(model.asMap().get(AGENT_DECLARE_FORM)))
		{
			agentDeclareForm = (AgentDeclareForm) model.asMap().get(AGENT_DECLARE_FORM);
		}
		else
		{
			agentDeclareForm = new AgentDeclareForm();
		}

		final Map<String, String> giftCardTypeMap = new TreeMap<String, String>();
		enumerationService.getEnumerationValues(GiftCardType.class).stream().forEach(
				giftCardType -> {
					giftCardTypeMap.put(giftCardType.getCode(), enumerationService.getEnumerationName(giftCardType));
				}
		);

		final List<String> allowedCardTypes = BCFPropertiesUtils
				.convertToList(
						bcfConfigurablePropertiesService.getBcfPropertyValue(SUPPORTED_CARD_TYPES));

		final Map<String, String> creditCardTypeMap = new TreeMap<String, String>();
		enumerationService.getEnumerationValues(CreditCardType.class).stream()
				.filter(creditCardType -> allowedCardTypes.contains(creditCardType.getCode())).forEach(
				creditCardType -> {
					creditCardTypeMap
							.put(creditCardType.getCode().toLowerCase(), enumerationService.getEnumerationName(creditCardType));
				}
		);

		if (assistedServiceFacade.getAsmSession() != null)
		{
			populateBasicAgentInfo(agentDeclareForm, agentToDeclare);
		}

		model.addAttribute("giftCardTypes", giftCardTypeMap);
		model.addAttribute("creditCardTypes", creditCardTypeMap);
		model.addAttribute(AGENT_DECLARE_FORM, agentDeclareForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_REPORT_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/supervisordeclare", method = RequestMethod.GET)
	public String getAgentsToReport(final HttpServletRequest request, final HttpServletResponse response,
			final Model model) throws CMSItemNotFoundException
	{
		final AgentDeclareForm agentDeclareForm;

		if (Objects.nonNull(model.asMap().get(AGENT_DECLARE_FORM)))
		{
			agentDeclareForm = (AgentDeclareForm) model.asMap().get(AGENT_DECLARE_FORM);
		}
		else
		{
			agentDeclareForm = new AgentDeclareForm();
		}

		if (assistedServiceFacade.getAsmSession() != null)
		{
			populateBasicAgentInfo(agentDeclareForm, null);
			final List<CustomerData> agentsList = bcfUserGroupFacade.getAgents();

			model.addAttribute(AGENT_TO_DECLARE, agentsList);
			model.addAttribute(AGENT_DECLARE_FORM, agentDeclareForm);
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(SUPERVISOR_DECLARE_REPORT_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/declare-endofday-report", method = RequestMethod.POST)
	public String declareReport(@ModelAttribute("agentDeclareForm") final AgentDeclareForm agentDeclareForm,
			final Model model,
			final BindingResult bindingResult, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (assistedServiceFacade.getAsmSession() != null)
		{
			final EndOfDayReportData endOfDayReportData = getSessionService().getCurrentSession()
					.getAttribute(SESSION_END_OF_DAY_REPORT_DATA);

			String auditRequested = BOOLEAN_NO;
			final Boolean auditRequestFlag = agentDeclareForm.getAuditRequestFlag();
			if (BooleanUtils.isNotFalse(auditRequestFlag))
			{
				auditRequested = BOOLEAN_YES;
			}

			endOfDayReportData.setAuditRequestFlag(auditRequested);
			endOfDayReportData.setAuditComment(agentDeclareForm.getAuditComment());
			endOfDayReportData.setBankDepositNumber(agentDeclareForm.getBankDepositNumber());
			endOfDayReportData.setSupervisorID(agentDeclareForm.getDeclareBy());
			try
			{
				bcfEndOfDayReportFacade.submitEndOfDayReport(endOfDayReportData);
			}
			catch (final IntegrationException e)
			{
				LOG.error("Integration error generation end of day report " + e.getMessage());
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.endofdayreport.integratoin.error", null);
			}

			getSessionService().removeAttribute(SESSION_END_OF_DAY_REPORT_DATA);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_THANKYOU_REPORT_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/confirm-endofday-report", method = RequestMethod.POST)
	public String confirmReport(@ModelAttribute("agentDeclareForm") final AgentDeclareForm agentDeclareForm,
			@RequestParam(value = "agentToDeclare") final String agentToDeclare,
			final Model model, final BindingResult bindingResult, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (assistedServiceFacade.getAsmSession() != null)
		{
			final AssistedServiceSession asmSession = assistedServiceFacade.getAsmSession();
			final Map<String, Object> agentDeclareDataMap = getAgentDeclareInfo(agentDeclareForm);

			final EndOfDayReportData endOfDayReportData = bcfEndOfDayReportFacade
					.getEndOfDayReportData(asmSession, agentToDeclare, agentDeclareDataMap);

			getSessionService().setAttribute(SESSION_END_OF_DAY_REPORT_DATA, endOfDayReportData);

			model.addAttribute(END_OF_DAY_REPORT_DATA, endOfDayReportData);
			model.addAttribute(AGENT_DECLARE_DATA, agentDeclareDataMap);
			model.addAttribute("currentCurrency", getCurrentCurrency());
			storeCmsPageInModel(model, getContentPageForLabelOrId(CONFIRM_ORDER_REPORT_CMS_PAGE));
		}
		return getViewForPage(model);
	}

	private Map<String, Object> getAgentDeclareInfo(final AgentDeclareForm agentDeclareForm)
	{
		final Map<String, Object> agentDeclareDataMap = new HashMap<String, Object>();
		agentDeclareDataMap.put(PORT_LOCATION, agentDeclareForm.getPortLocation());
		agentDeclareDataMap.put(USER_ID, agentDeclareForm.getUserId());
		agentDeclareDataMap.put(USER_NAME, agentDeclareForm.getUserName());
		agentDeclareDataMap.put(SESSION_START, agentDeclareForm.getSessionStart());
		agentDeclareDataMap.put(DECLARE_END, agentDeclareForm.getDeclareEnd());
		agentDeclareDataMap.put(DECLARE_BY, agentDeclareForm.getDeclareBy());
		agentDeclareDataMap.put(CASH_DECLARED_AMOUNT, agentDeclareForm.getCashDeclaredAmount());
		agentDeclareDataMap.put(BANK_DEPOSIT_NUMBER, agentDeclareForm.getBankDepositNumber());
		agentDeclareDataMap.put(GIFT_CARD_DECLARED_TYPES, agentDeclareForm.getGiftCardDeclaredTypes());
		agentDeclareDataMap.put(CARD_DECLARED_TYPES, agentDeclareForm.getCreditCardDeclaredTypes());
		return agentDeclareDataMap;
	}

	private void populateBasicAgentInfo(final AgentDeclareForm agentDeclareForm, final String agentToDeclare)
	{
		final AssistedServiceSession asmSession = assistedServiceFacade.getAsmSession();

		final EmployeeModel agent = (EmployeeModel) asmSession.getAgent();

		String userId = agent.getUid();
		String supervisorID = StringUtils.EMPTY;
		String name = agent.getName();

		Date agentDeclareTime = agent.getEndofdayDeclareTime();
		if (StringUtils.isNotBlank(agentToDeclare))
		{
			supervisorID = userId;
			final EmployeeModel agentDeclared = (EmployeeModel) userService.getUserForUID(agentToDeclare);
			userId = agentDeclared.getUid();
			name = agentDeclared.getName();
			agentDeclareTime = agentDeclared.getEndofdayDeclareTime();
		}

		Date declareStartTime = new Date();
		final Date declareEndTime = new Date();

		if (Objects.nonNull(agentDeclareTime))
		{
			declareStartTime = agentDeclareTime;
		}

		agentDeclareForm.setUserId(userId);
		agentDeclareForm.setUserName(name);
		agentDeclareForm.setDeclareBy(supervisorID);
		agentDeclareForm.setSessionStart(declareStartTime);
		agentDeclareForm.setDeclareEnd(declareEndTime);
		agentDeclareForm.setPortLocation(bcfSalesApplicationResolverService.getCurrentSalesChannel().getCode());
	}
}
