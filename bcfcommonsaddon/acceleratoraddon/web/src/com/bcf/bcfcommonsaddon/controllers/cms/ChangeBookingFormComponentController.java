/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.forms.cms.ChangeBookingForm;
import com.bcf.bcfaccommodationsaddon.validators.ChangeBookingRequestValidator;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.model.components.ChangeBookingFormComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BCFChangeBookingRequestFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;


@Controller("ChangeBookingFormComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.ChangeBookingFormComponent)
public class ChangeBookingFormComponentController extends SubstitutingCMSAddOnComponentController<ChangeBookingFormComponentModel>
{
	public static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";

	@Resource(name = "bcfChangeBookingRequestFacade")
	private BCFChangeBookingRequestFacade bcfChangeBookingRequestFacade;

	@Resource(name = "changeBookingRequestValidator")
	private ChangeBookingRequestValidator changeBookingRequestValidator;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ChangeBookingFormComponentModel component)
	{
		final String bookingReference =sessionService.getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);

		if (!model.containsAttribute(BcfcommonsaddonConstants.CHANGE_BOOKING_FROM))
		{
			model.addAttribute(BcfcommonsaddonConstants.CHANGE_BOOKING_FROM, new ChangeBookingForm());
		}
		model.addAttribute(BcfcommonsaddonConstants.BOOKING_REFERENCE, bookingReference);
		final String guestBookingIdentifier = bcfBookingFacade.getGuestIdentifierForOrderCode(bookingReference);
		if (StringUtils.isNotEmpty(guestBookingIdentifier))
		{
			model.addAttribute(BcfcommonsaddonConstants.GUEST_BOOKING_IDENTIFIER, guestBookingIdentifier);
		}
	}

	@RequestMapping(value = "/change-booking", method = RequestMethod.POST)
	public String validateAccommodationAvailabilityForm(
			@ModelAttribute(value = "orderCode") final String orderCode,
			@ModelAttribute(value = "guestBookingIdentifier") final String guestBookingIdentifier,
			final ChangeBookingForm changeBookingForm, final BindingResult bindingResult,
			final RedirectAttributes redirectModel, final Model model)
	{

		changeBookingRequestValidator.validate(changeBookingForm, bindingResult);

		final boolean hasErrorFlag = bindingResult.hasErrors();
		redirectModel.addFlashAttribute(BcfcommonsaddonConstants.CHANGE_BOOKING_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			redirectModel.addFlashAttribute(BcfcommonsaddonConstants.CHANGE_BOOKING_BINDING_RESULT, bindingResult);
			redirectModel.addFlashAttribute(BcfcommonsaddonConstants.CHANGE_BOOKING_FROM, changeBookingForm);

		}
		else
		{
			bcfChangeBookingRequestFacade.createTicketForChangeBooking(changeBookingForm.getFirstname()+" "+changeBookingForm.getLastname(),changeBookingForm.getEmail(),changeBookingForm.getContactnumber(),changeBookingForm.getDescription(),
					BcfcommonsaddonConstants.CHANGE_BOOKING_REQUEST_TITLE,orderCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					BcfcommonsaddonConstants.CHANGE_BOOKING_SUCCESS_MESSAGE);
		}

		return StringUtils.isEmpty(guestBookingIdentifier) ?
				REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode :
				REDIRECT_PREFIX + BcfCoreConstants.GUEST_BOOKING_DETAILS_PAGE + guestBookingIdentifier;
	}
}
