/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.forms.cms.ScheduleFinderForm;
import com.bcf.bcfcommonsaddon.model.components.ScheduleFinderComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;


@Controller("ScheduleFinderComponentController")
@RequestMapping(value = "/view/ScheduleFinderComponentController")
public class ScheduleFinderComponentController extends AbstractFinderComponentController
{
	protected static final String SEARCH = "/search";

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{
		model.addAttribute(BcfvoyageaddonWebConstants.SCHEDULE_FINDER_FORM, new ScheduleFinderForm());
		final ScheduleFinderComponentModel scheduleFinderComponent = (ScheduleFinderComponentModel) component;
		model.addAttribute("title", scheduleFinderComponent.getTitle());
		model.addAttribute("description", scheduleFinderComponent.getDescription());
	}
}
