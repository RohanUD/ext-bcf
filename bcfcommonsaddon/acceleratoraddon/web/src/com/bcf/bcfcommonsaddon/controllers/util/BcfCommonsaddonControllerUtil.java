/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfcommonsaddon.controllers.util;

import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.integration.constants.BcfintegrationserviceConstants;


@Component("bcfCommonsaddonControllerUtil")
public class BcfCommonsaddonControllerUtil
{
	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	/**
	 * Clone passenger type quantity data.
	 *
	 * @param gc the gc
	 * @return the passenger type quantity data
	 */
	public PassengerTypeQuantityData clonePassengerTypeQuantityData(final PassengerTypeQuantityData gc)
	{
		final PassengerTypeQuantityData ptqd = new PassengerTypeQuantityData();
		ptqd.setAge(gc.getAge());
		ptqd.setChildAges(gc.getChildAges());
		if(CollectionUtils.isNotEmpty(gc.getChildAges()))
		{
			ptqd.setChildAgesArray(gc.getChildAges().stream().map(integer -> integer.toString()).collect(Collectors.joining(",")));
		}

		ptqd.setQuantity(gc.getQuantity());

		final PassengerTypeData ptd = new PassengerTypeData();
		ptd.setCode(gc.getPassengerType().getCode());
		ptd.setIdentifier(gc.getPassengerType().getIdentifier());
		ptd.setName(gc.getPassengerType().getName());
		ptd.setPassengerType(gc.getPassengerType().getCode());
		ptd.setMinAge(gc.getPassengerType().getMinAge());
		ptd.setMaxAge(gc.getPassengerType().getMaxAge());
		ptd.setBcfCode(gc.getPassengerType().getBcfCode());

		ptqd.setPassengerType(ptd);
		return ptqd;
	}

	public List<PassengerTypeQuantityData> getPassengerTypeQuantityList(final List<PassengerTypeQuantityData> guestCounts,
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		final PassengerTypeQuantityData childGuestWithAge = guestCounts.stream().filter(
				gc -> StringUtils.equals(gc.getPassengerType().getCode(), BcfintegrationserviceConstants.PASSENGER_TYPE_CHILD)
						&& Objects.nonNull(gc.getChildAges()) && gc.getChildAges().size() > 0).findFirst().orElse(null);
		if (Objects.nonNull(childGuestWithAge))
		{
			final Integer adultMinAge = passengerTypeFacade
					.getMinAgeByPassengerTypeCode(BcfintegrationserviceConstants.PASSENGER_TYPE_ADULT);
			final List<Integer> childAgeList = childGuestWithAge.getChildAges().stream()
					.filter(childAge -> childAge > adultMinAge)
					.collect(Collectors.toList());
			if (childAgeList.size() > 0)
			{
				final PassengerTypeQuantityData adultGuestWithAge = guestCounts.stream().filter(
						gc -> StringUtils.equals(gc.getPassengerType().getCode(), BcfintegrationserviceConstants.PASSENGER_TYPE_ADULT))
						.findFirst().get();
				adultGuestWithAge.setQuantity(adultGuestWithAge.getQuantity() + childAgeList.size());
				childGuestWithAge.setQuantity(childGuestWithAge.getQuantity() - childAgeList.size());
			}
		}

		for (int j = 0; j < guestCounts.size(); j++)
		{
			final PassengerTypeQuantityData gc = guestCounts.get(j);
			if (CollectionUtils.isEmpty(passengerTypeQuantityList))
			{
				final PassengerTypeQuantityData ptqd = clonePassengerTypeQuantityData(gc);

				passengerTypeQuantityList.add(ptqd);
			}
			else
			{
				final Optional<PassengerTypeQuantityData> passengerTypeQuantityData = passengerTypeQuantityList.stream()
						.filter(pt -> StringUtils.equals(pt.getPassengerType().getCode(), gc.getPassengerType().getCode())).findFirst();
				if (passengerTypeQuantityData.isPresent())
				{
					passengerTypeQuantityData.get().setQuantity(passengerTypeQuantityData.get().getQuantity() + gc.getQuantity());
				}
				else
				{
					final PassengerTypeQuantityData ptqd = clonePassengerTypeQuantityData(gc);

					passengerTypeQuantityList.add(ptqd);
				}
			}
		}
		return passengerTypeQuantityList;
	}

	public void setVehicleData(final FareFinderForm fareFinderForm)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypeQuantityDataOutBound = new VehicleTypeQuantityData();
		final VehicleTypeQuantityData vehicleTypeQuantityDataInBound = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeData.setCategory(StringUtils.EMPTY);
		vehicleTypeQuantityDataOutBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityDataInBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataOutBound);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataInBound);
		fareFinderForm.setVehicleInfo(vehicleTypeQuantityList);
		fareFinderForm.setTravellingWithVehicle(
				Boolean.parseBoolean(bcfConfigurablePropertiesService.getBcfPropertyValue("defaultTravelingWithVehicle")));
		fareFinderForm.setReturnWithDifferentVehicle(
				Boolean.parseBoolean(bcfConfigurablePropertiesService.getBcfPropertyValue("defaultReturnWithDifferentVehicle")));
	}



}
