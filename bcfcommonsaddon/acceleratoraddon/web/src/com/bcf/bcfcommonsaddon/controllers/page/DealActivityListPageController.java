/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.COMMA;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.ActivityFinderForm;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.ActivityProductFacade;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.activity.data.ActivityScheduleData;
import com.bcf.facades.activity.search.ActivityProductSearchFacade;
import com.bcf.facades.activity.search.request.data.ActivitySearchCriteriaData;
import com.bcf.facades.activity.search.response.data.ActivitiesResponseData;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


@Controller
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping("/deal-activity-listing")
public class DealActivityListPageController extends TravelAbstractPageController
{
	private static final String DEAL_ACTIVITY_SELECTION_CMS_PAGE = "dealActivityListingPage";

	@Resource(name = "activityProductSearchFacade")
	private ActivityProductSearchFacade activityProductSearchFacade;

	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfDealCartFacade")
	private BcfDealCartFacade bcfDealCartFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfTravelLocationService")
	private BcfTravelLocationService bcfTravelLocationService;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "dealCartFacade")
	private BcfDealCartFacade dealCartFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@RequestMapping(method = RequestMethod.GET)
	public String getActivitiesList(final ActivityFinderForm activityFinderForm, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final TravelFinderForm travelFinderForm = getSessionService().getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);

		//Earlier on deal activity page we have a location facet, but this now has removed so the destination value is coming null,
		//thus setting the default destination from the accommodationFarefinder
		if (StringUtils.isBlank(activityFinderForm.getDestination()))
		{
			activityFinderForm.setDestination(travelFinderForm.getAccommodationFinderForm().getDestinationLocationName());
		}

		final ActivitySearchCriteriaData activitySearchCriteriaData = getActivitySearchCriteriaData(activityFinderForm);
		final ActivitiesResponseData activitiesResponseData = activityProductSearchFacade
				.doSearch(activitySearchCriteriaData, buildGlobalPaginationData(request, DEAL_ACTIVITY_SELECTION_CMS_PAGE));
		storePaginationResults(model, activitiesResponseData);
		activityProductFacade.removeInvalidActivities(activitiesResponseData);

		final Pair<Date, Date> checkInCheckOutDate = getCheckInAndCheckOutDate(travelFinderForm.getAccommodationFinderForm());
		final Date checkInDate = checkInCheckOutDate.getLeft();

		activityProductFacade.setAvailableStatusForActivitiesResponseData(activitiesResponseData, checkInDate);

		model.addAttribute(BcfcommonsaddonWebConstants.IS_DEAL_ACTIVITY, true);
		storeCmsPageInModel(model, getContentPageForLabelOrId(DEAL_ACTIVITY_SELECTION_CMS_PAGE));

		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();
		model.addAttribute(BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);

		return getViewForPage(model);
	}

	private ActivitySearchCriteriaData getActivitySearchCriteriaData(final ActivityFinderForm activityFinderForm)
	{
		if (Objects.isNull(activityFinderForm))
		{
			final ActivitySearchCriteriaData activitySearchCriteriaData = new ActivitySearchCriteriaData();
			activitySearchCriteriaData.setCurrentPage(1);
			return activitySearchCriteriaData;
		}

		return createActivitySearchCriteria(activityFinderForm.getSort(), activityFinderForm.getCurrentPage(),
				activityFinderForm.getDestination(), activityFinderForm.getActivityCategoryTypes());
	}

	private ActivitySearchCriteriaData createActivitySearchCriteria(final String sortParam, final int currentPage,
			final String destination, final String categories)
	{
		final ActivitySearchCriteriaData activitySearchCriteriaData = new ActivitySearchCriteriaData();
		if (StringUtils.isNotBlank(destination))
		{
			activitySearchCriteriaData.setDestination(
					activityProductSearchFacade.createActivityLocation(destination));
		}
		else
		{
			activitySearchCriteriaData.setDestination(destination);
		}
		activitySearchCriteriaData.setCurrentPage(currentPage);
		if (StringUtils.isNotBlank(categories))
		{
			activitySearchCriteriaData
					.setActivityCategoryTypes(Arrays.asList(StringUtils.split(categories, COMMA)));
		}
		activitySearchCriteriaData.setSort(sortParam);
		return activitySearchCriteriaData;
	}

	@RequestMapping(value = "/getActivityPricesAndSchedule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getActivityPricePerPassengerAndSchedules(
			@ModelAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM) final TravelFinderForm travelFinderForm,
			@RequestParam final String productCode,
			@RequestParam(value = "changeActivity", defaultValue = "false") final boolean changeActivity,
			@RequestParam(value = "changeActivityCode", required = false) final String changeActivityCode,
			final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		final AccommodationFinderForm accommodationFinderForm = travelFinderForm.getAccommodationFinderForm();

		final Pair<Date, Date> checkInCheckOutDate = getCheckInAndCheckOutDate(accommodationFinderForm);
		final Date checkInDate = checkInCheckOutDate.getLeft();
		final Date checkOutDate = checkInCheckOutDate.getRight();
		final String locationCode = activityProductSearchFacade
				.createActivityLocation(accommodationFinderForm.getDestinationLocation());

		final ActivityPricePerPassengerWithSchedule activityPricePerPassengerWithSchedule = activityProductSearchFacade
				.createActivityPricePerPassengerAndSchedule(productCode, checkInDate, locationCode);
		activityProductSearchFacade.getValidAvailableSchedules(activityPricePerPassengerWithSchedule, checkInDate, checkOutDate);

		final Map<ActivityScheduleData, List<Date>> datesGroupByScheduleMap = bcfDealCartFacade
				.getScheduleDates(checkInDate, checkOutDate, activityPricePerPassengerWithSchedule.getAvailableSchedules());

		activityPricePerPassengerWithSchedule.setProductCode(productCode);
		activityPricePerPassengerWithSchedule
				.setJourneyRefNumber(getSessionService().getAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM));
		activityPricePerPassengerWithSchedule.setChangeActivity(changeActivity);
		if (changeActivity)
		{
			activityPricePerPassengerWithSchedule.setChangeActivityCode(changeActivityCode);
		}
		model.addAttribute("activityPricePerPassengerWithSchedule", activityPricePerPassengerWithSchedule);
		model.addAttribute("checkOutDate", checkOutDate);
		model.addAttribute("datesGroupByScheduleMap", datesGroupByScheduleMap);
		model.addAttribute("addActivityToDealCartForm", activityPricePerPassengerWithSchedule);

		return BcfcommonsaddonControllerConstants.Views.Pages.Activity.ActivityPricesAndSchedulesResponse;
	}

	private Pair<Date, Date> getCheckInAndCheckOutDate(final AccommodationFinderForm accommodationFinderForm)
	{
		final String checkInDateTime = accommodationFinderForm.getCheckInDateTime();
		final Date checkInDate = TravelDateUtils
				.convertStringDateToDate(checkInDateTime, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final String checkOutDateTime = accommodationFinderForm.getCheckOutDateTime();
		final Date checkOutDate = TravelDateUtils
				.convertStringDateToDate(checkOutDateTime, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		return Pair.of(checkInDate, checkOutDate);
	}

	@RequestMapping(value = "/remove-activity/{activityProductCode}/{journeyRefNumber}/{date}", method = RequestMethod.GET)
	public String removeActivityForJourney(@PathVariable final String activityProductCode,
			@PathVariable final int journeyRefNumber, @PathVariable final String date, @RequestParam final String time,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		dealCartFacade.removeSelectedActivityEntries(activityProductCode, journeyRefNumber, date, time);
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.ACTIVITY_REMOVED, true);
		final String referer = request.getHeader("referer");
		return REDIRECT_PREFIX + referer;
	}
}
