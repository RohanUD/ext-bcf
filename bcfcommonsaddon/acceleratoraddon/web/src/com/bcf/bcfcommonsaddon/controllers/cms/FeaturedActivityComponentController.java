/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.enums.FeaturedActivityViewType;
import com.bcf.bcfcommonsaddon.model.components.FeaturedActivityComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.facades.ActivityProductFacade;


@Controller("FeaturedActivityComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.FeaturedActivityComponent)
public class FeaturedActivityComponentController extends SubstitutingCMSAddOnComponentController<FeaturedActivityComponentModel>
{
	@Resource(name = "featuredActivityViewTypeMap")
	private Map<FeaturedActivityViewType, String> featuredActivityViewTypeMap;

	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final FeaturedActivityComponentModel featuredActivityComponent)
	{
		if (Objects.equals(FeaturedActivityViewType.BANNER,featuredActivityComponent.getFeaturedActivityViewType()))
		{
				populateDataForBannerActivity(model, featuredActivityComponent.getProductCode());
		}
		else if (Objects.equals(FeaturedActivityViewType.FEATURED, featuredActivityComponent.getFeaturedActivityViewType()))
		{
			populateDataForFeaturedActivity(model, featuredActivityComponent.getProductCode());
		}
	}

	protected void populateDataForBannerActivity(final Model model, final String productCode)
	{
		model.addAttribute("activityProductDataForBanner",
				activityProductFacade.getActivityDetailsData(productCode));
	}

	protected void populateDataForFeaturedActivity(final Model model, final String productCode)
	{
		model.addAttribute("activityProductDataForFeatured",
				activityProductFacade.getActivityDetailsData(productCode));
	}

	@Override
	protected String getView(final FeaturedActivityComponentModel featuredActivityComponent)
	{
		return featuredActivityViewTypeMap.get(featuredActivityComponent.getFeaturedActivityViewType());
	}
}
