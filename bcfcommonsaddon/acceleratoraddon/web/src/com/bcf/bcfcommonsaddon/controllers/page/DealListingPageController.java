/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.facades.packagedeal.search.impl.DefaultPackageSolrSearchFacade;
import com.bcf.facades.search.facetdata.PackageSearchPageData;
import com.google.common.collect.Sets;


@Controller
@RequestMapping("/vacations/promotions")
public class DealListingPageController extends BcfAbstractPageController
{
	private static final String DEAL_LISTING_CMS_PAGE = "dealListingPage";
	private static final String FILTER_QUERY_KEY = "hotelCityCodes";
	private static final String HOTEL_LOCATION_NAME = "hotelLocationName";
	private static final String ALL = "ALL";

	@Resource(name = "packageSolrSearchFacade")
	private DefaultPackageSolrSearchFacade packageSolrSearchFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getDeals(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		final SearchStateData searchStateData = packageSolrSearchFacade
				.buildSearchStateData(FILTER_QUERY_KEY, getSearchFilterValues(request));
		final PackageSearchPageData packageSearchPageData = packageSolrSearchFacade
				.searchDeals(searchStateData, buildGlobalPaginationData(request, DEAL_LISTING_CMS_PAGE));
		storePaginationResults(model, packageSearchPageData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(DEAL_LISTING_CMS_PAGE));
		return getViewForPage(model);
	}

	private Set<String> getSearchFilterValues(final HttpServletRequest request)
	{
		final String hotelLocationName = request.getParameter(HOTEL_LOCATION_NAME);
		if (StringUtils.isNotEmpty(hotelLocationName) && !ALL.equals(hotelLocationName))
		{
			return Sets.newHashSet(hotelLocationName);
		}
		return null;
	}
}
