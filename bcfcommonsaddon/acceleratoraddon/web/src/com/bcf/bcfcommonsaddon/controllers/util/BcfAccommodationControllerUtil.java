/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfcommonsaddon.controllers.util;

import de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TripType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.GuestCountModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.warehouse.AccommodationOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfaccommodationsaddon.validators.AccommodationFinderValidator;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfcommonsaddon.validators.TravelFinderValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.validators.TravelFareFinderValidator;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.model.vacation.BcfVacationLocationModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.custom.propertysource.message.code.resolver.CustomMessageCodesResolver;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfAccommodationFacadeHelper;
import com.bcf.facades.managebooking.data.AdjustCostData;
import com.bcf.facades.reservation.data.ActivityReservationData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


@Component("bcfAccommodationControllerUtil")
public class BcfAccommodationControllerUtil
{

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "bcfTravelLocationService")
	private BcfTravelLocationService bcfTravelLocationService;

	@Resource(name = "transportFacilityFacade")
	private TransportFacilityFacade transportFacilityFacade;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "travelFareFinderValidator")
	protected TravelFareFinderValidator travelFareFinderValidator;

	@Resource(name = "accommodationFinderValidator")
	protected AccommodationFinderValidator accommodationFinderValidator;

	@Resource(name = "travelFinderValidator")
	protected TravelFinderValidator travelFinderValidator;

	@Resource(name = "messageCodesResolver")
	private CustomMessageCodesResolver customMessageCodesResolver;

	@Resource(name = "bcfAccommodationFacadeHelper")
	private BcfAccommodationFacadeHelper bcfAccommodationFacadeHelper;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	private static final String ECONOMY_CABIN_CLASS_CODE = "M";

	public void initializeAccommodationFinderForm(final AccommodationFinderForm accommodationFinderForm,
			final CartModel sessionAmendedCart,
			final int journeyRef)
	{

		final DealOrderEntryGroupModel dealOrderEntryGroupModel = bcfBookingService
				.getAccommodationDealOrderEntryGroup(sessionAmendedCart, journeyRef);

		Collection<AccommodationOrderEntryGroupModel> accommodationOrderEntryGroupModels = dealOrderEntryGroupModel
				.getAccommodationEntryGroups();
		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModels))
		{
			accommodationOrderEntryGroupModels = accommodationOrderEntryGroupModels.stream()
					.filter(
							accommodationOrderEntryGroupModel -> isActiveAccommodationEntriesPresent(accommodationOrderEntryGroupModel))
					.collect(
							Collectors.toList());
			final List<PassengerTypeData> passengerTypes = passengerTypeFacade.getPassengerTypes();
			final int numberOfRooms = accommodationOrderEntryGroupModels.size();
			accommodationFinderForm.setNumberOfRooms(numberOfRooms);
			accommodationFinderForm
					.setCheckInDateTime(getFormattedDate(accommodationOrderEntryGroupModels.iterator().next().getStartingDate()));
			accommodationFinderForm
					.setCheckOutDateTime(getFormattedDate(accommodationOrderEntryGroupModels.iterator().next().getEndingDate()));

			final AccommodationOfferingModel accommodationOfferingModel = accommodationOrderEntryGroupModels.iterator().next()
					.getAccommodationOffering();
			if (accommodationOfferingModel != null)
			{
				final LocationModel location = bcfTravelLocationService
						.getLocationWithLocationType(Collections.singletonList(accommodationOfferingModel.getLocation()),
								LocationType.CITY);
				final BcfVacationLocationModel bcfVacationLocation = bcfTravelLocationService
						.findBcfVacationLocationByOriginalLocation(location);

				accommodationFinderForm.setDestinationLocation(bcfVacationLocation.getCode());
				accommodationFinderForm.setDestinationLocationName(bcfVacationLocation.getName());
			}
			final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
			int i = 0;

			for (final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel : accommodationOrderEntryGroupModels)
			{
				final List<PassengerTypeQuantityData> passengerTypeQuantityDatas = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getGuestCounts()))
				{
					final Map<String, List<GuestCountModel>> guestCountMap = accommodationOrderEntryGroupModel.getGuestCounts()
							.stream()
							.collect(Collectors.groupingBy(guestCount -> guestCount.getPassengerType().getCode()));
					for (final Map.Entry<String, List<GuestCountModel>> guestCountMapEntry : guestCountMap.entrySet())
					{

						final PassengerTypeData passengerTypeData = passengerTypes.stream()
								.filter(pData -> pData.getCode().equals(guestCountMapEntry.getKey())).findFirst().get();
						final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
						passengerTypeQuantityData.setQuantity(
								guestCountMapEntry.getValue().stream().mapToInt(guestCount -> guestCount.getQuantity()).sum());
						passengerTypeQuantityData.setPassengerType(passengerTypeData);
						if(CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getChildAges()))
						{
							passengerTypeQuantityData.setChildAges(accommodationOrderEntryGroupModel.getChildAges().stream().map(childAge->Integer.valueOf(childAge)).collect(
									Collectors.toList()));
						}
						passengerTypeQuantityDatas.add(passengerTypeQuantityData);
					}
					if(passengerTypeQuantityDatas.size()==1){
						passengerTypeQuantityDatas.add(bcfAccommodationFacadeHelper.getChildPassengerTypeQuantity());
					}
					final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
					roomStayCandidateData.setRoomStayCandidateRefNumber(i);
					roomStayCandidateData.setFirstName(accommodationOrderEntryGroupModel.getFirstName());
					roomStayCandidateData.setLastName(accommodationOrderEntryGroupModel.getLastName());
					roomStayCandidateData.setPassengerTypeQuantityList(passengerTypeQuantityDatas);
					roomStayCandidates.add(roomStayCandidateData);
					i++;
				}




			}

			final int maxAccommodationsQuantity = configurationService.getConfiguration()
					.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);

			final int roomStayCandidatesSize=roomStayCandidates.size();
			if(maxAccommodationsQuantity>roomStayCandidatesSize){

				final int extraRoomStayCandidates=maxAccommodationsQuantity-roomStayCandidatesSize;
				for (int j = 0; j <extraRoomStayCandidates; j++)
				{
					final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
					roomStayCandidateData.setRoomStayCandidateRefNumber(j+roomStayCandidatesSize);
					roomStayCandidates.add(roomStayCandidateData);
				}
			}
			accommodationFinderForm.setRoomStayCandidates(roomStayCandidates);

		}
	}

	public boolean isActiveAccommodationEntriesPresent(final AccommodationOrderEntryGroupModel accommodationOrderEntryGroupModel)
	{

		if (CollectionUtils.isNotEmpty(accommodationOrderEntryGroupModel.getDealAccommodationEntries()))
		{
			return accommodationOrderEntryGroupModel.getDealAccommodationEntries().stream().anyMatch(entry -> entry.getActive());
		}
		else
		{
			return accommodationOrderEntryGroupModel.getEntries().stream().anyMatch(entry -> entry.getActive());
		}
	}

	public void initializeFareFinderForm(final FareFinderForm fareFinderForm,
			final AccommodationFinderForm accommodationFinderForm, final List<AbstractOrderEntryModel> outBoundEntries,
			final List<AbstractOrderEntryModel> inBoundEntries, final int journeyRef) //NOSONAR
	{
		final TravelRouteModel outBoundtravelRoute = outBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final TravelRouteModel inBoundtravelRoute = inBoundEntries.get(0).getTravelOrderEntryInfo().getTravelRoute();

		fareFinderForm.setDepartureLocation(outBoundtravelRoute.getOrigin().getCode());
		fareFinderForm.setDepartureLocationName(
				getLocationName(outBoundtravelRoute.getOrigin().getCode()));
		fareFinderForm.setArrivalLocation(outBoundtravelRoute.getDestination().getCode());
		fareFinderForm.setArrivalLocationName(
				getLocationName(outBoundtravelRoute.getDestination().getCode()));
		fareFinderForm.setPassengerTypeQuantityList(createPassengerTypeQuantityData(
				accommodationFinderForm.getNumberOfRooms(), accommodationFinderForm.getRoomStayCandidates()));
		fareFinderForm.setTripType(TripType.RETURN.getCode());
		fareFinderForm.setCabinClass(ECONOMY_CABIN_CLASS_CODE);

		initializeVehicle(fareFinderForm, outBoundEntries, inBoundEntries, outBoundtravelRoute, inBoundtravelRoute);

	}

	public String getAccommodationOfferingCode(final CartModel sessionAmendedCart,
			final int journeyRef){

		final DealOrderEntryGroupModel dealOrderEntryGroupModel = bcfBookingService
				.getAccommodationDealOrderEntryGroup(sessionAmendedCart, journeyRef);

		if(CollectionUtils.isNotEmpty(dealOrderEntryGroupModel.getAccommodationEntryGroups()))
		{
			return dealOrderEntryGroupModel.getAccommodationEntryGroups().iterator().next().getAccommodationOffering().getCode();
		}
		return null;
	}





	private void initializeVehicle(final FareFinderForm fareFinderForm,
			final List<AbstractOrderEntryModel> outBoundEntries, final List<AbstractOrderEntryModel> inBoundEntries,
			final TravelRouteModel outBoundtravelRoute, final TravelRouteModel inBoundtravelRoute)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();

		final List<AbstractOrderEntryModel> outboundvehicleEntryModels = outBoundEntries.stream().filter(
				entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
						&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
						FareProductType.VEHICLE)).collect(Collectors.toList());

		final List<AbstractOrderEntryModel> inboundvehicleEntryModels = inBoundEntries.stream().filter(
				entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
						&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
						FareProductType.VEHICLE)).collect(Collectors.toList());


		if (CollectionUtils.isNotEmpty(outboundvehicleEntryModels))
		{

			vehicleTypeQuantityDataList.add(populateVehicleTypeQtyData(outboundvehicleEntryModels,
					outBoundtravelRoute));
			fareFinderForm.setTravellingWithVehicle(true);
		}
		else
		{
			fareFinderForm.setTravellingWithVehicle(false);
		}

		if (CollectionUtils.isNotEmpty(inboundvehicleEntryModels))
		{

			vehicleTypeQuantityDataList.add(populateVehicleTypeQtyData(inboundvehicleEntryModels,
					inBoundtravelRoute));
		}

		fareFinderForm.setReturnWithDifferentVehicle(false);

		if (vehicleTypeQuantityDataList.size() == 2 && !vehicleTypeQuantityDataList.get(0).getVehicleType().getCode()
				.equalsIgnoreCase(vehicleTypeQuantityDataList.get(1).getVehicleType().getCode()))
		{
				fareFinderForm.setReturnWithDifferentVehicle(true);
		}


		fareFinderForm.setVehicleInfo(vehicleTypeQuantityDataList);
	}

	private VehicleTypeQuantityData populateVehicleTypeQtyData(final List<AbstractOrderEntryModel> outboundvehicleEntryModels,
			final TravelRouteModel outBoundtravelRoute)
	{
		final VehicleTypeQuantityData vehicleTypeQuantityDataOutBound = new VehicleTypeQuantityData();

		final BCFVehicleInformationModel vehicleInfoModel = (BCFVehicleInformationModel) outboundvehicleEntryModels.get(0)
				.getTravelOrderEntryInfo()
				.getTravellers().iterator().next().getInfo();

		vehicleTypeQuantityDataOutBound.setVehicleType(new VehicleTypeData());
		vehicleTypeQuantityDataOutBound.getVehicleType()
				.setCategory(vehicleInfoModel.getVehicleType().getVehicleCategoryType().getCode());
		vehicleTypeQuantityDataOutBound.getVehicleType().setCode(vehicleInfoModel.getVehicleType().getType().getCode());
		vehicleTypeQuantityDataOutBound.setHeight(vehicleInfoModel.getHeight() != null ? vehicleInfoModel.getHeight() : 0);
		vehicleTypeQuantityDataOutBound.setLength(vehicleInfoModel.getLength() != null ? vehicleInfoModel.getLength() : 0);
		vehicleTypeQuantityDataOutBound.setWidth(vehicleInfoModel.getWidth() != null ? vehicleInfoModel.getWidth() : 0);
		vehicleTypeQuantityDataOutBound.setQty(1);
		vehicleTypeQuantityDataOutBound.setTravelRouteCode(outBoundtravelRoute.getCode());
		vehicleTypeQuantityDataOutBound.setCarryingLivestock(BooleanUtils.toBoolean(vehicleInfoModel.getCarryingLivestock()));
		vehicleTypeQuantityDataOutBound
				.setNumberOfAxis(vehicleInfoModel.getNumberOfAxis() != null ? vehicleInfoModel.getNumberOfAxis() : 0);
		vehicleTypeQuantityDataOutBound
				.setVehicleWithSidecarOrTrailer(BooleanUtils.toBoolean(vehicleInfoModel.getVehicleWithSidecarOrTrailer()));
		return vehicleTypeQuantityDataOutBound;
	}

	private List<PassengerTypeQuantityData> createPassengerTypeQuantityData(final int numberOfRooms,
			final List<RoomStayCandidateData> roomStayCandidates)
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		if (CollectionUtils.size(roomStayCandidates) < numberOfRooms)
		{
			return passengerTypeQuantityList;
		}

		for (int i = 0; i < numberOfRooms; i++)
		{
			final List<PassengerTypeQuantityData> guestCounts = roomStayCandidates.get(i).getPassengerTypeQuantityList();
			if (CollectionUtils.isEmpty(guestCounts))
			{
				continue;
			}

			getPassengerTypeQuantityList(guestCounts,	passengerTypeQuantityList);
		}
		return passengerTypeQuantityList;
	}

	private List<PassengerTypeQuantityData> getPassengerTypeQuantityList(final List<PassengerTypeQuantityData> guestCounts,
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		for (int j = 0; j < guestCounts.size(); j++)
		{
			final PassengerTypeQuantityData gc = guestCounts.get(j);
			if (CollectionUtils.isEmpty(passengerTypeQuantityList))
			{
				final PassengerTypeQuantityData ptqd = clonePassengerTypeQuantityData(gc);

				passengerTypeQuantityList.add(ptqd);
			}
			else
			{
				final Optional<PassengerTypeQuantityData> passengerTypeQuantityData = passengerTypeQuantityList.stream()
						.filter(pt -> StringUtils.equals(pt.getPassengerType().getCode(), gc.getPassengerType().getCode())).findFirst();
				if (passengerTypeQuantityData.isPresent())
				{
					passengerTypeQuantityData.get().setQuantity(passengerTypeQuantityData.get().getQuantity() + gc.getQuantity());
				}
				else
				{
					final PassengerTypeQuantityData ptqd = clonePassengerTypeQuantityData(gc);

					passengerTypeQuantityList.add(ptqd);
				}
			}
		}
		return passengerTypeQuantityList;
	}

	private PassengerTypeQuantityData clonePassengerTypeQuantityData(final PassengerTypeQuantityData gc)
	{
		final PassengerTypeQuantityData ptqd = new PassengerTypeQuantityData();
		ptqd.setAge(gc.getAge());
		ptqd.setChildAges(gc.getChildAges());
		if(CollectionUtils.isNotEmpty(gc.getChildAges()))
		{
			ptqd.setChildAgesArray(gc.getChildAges().stream().map(integer -> integer.toString()).collect(Collectors.joining(",")));
		}
		ptqd.setQuantity(gc.getQuantity());

		final PassengerTypeData ptd = new PassengerTypeData();
		ptd.setCode(gc.getPassengerType().getCode());
		ptd.setIdentifier(gc.getPassengerType().getIdentifier());
		ptd.setName(gc.getPassengerType().getName());
		ptd.setPassengerType(gc.getPassengerType().getCode());
		ptd.setMinAge(gc.getPassengerType().getMinAge());
		ptd.setMaxAge(gc.getPassengerType().getMaxAge());
		ptd.setBcfCode(gc.getPassengerType().getBcfCode());

		ptqd.setPassengerType(ptd);
		return ptqd;
	}


	private List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<String> codes = Arrays.asList(BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_ADULT,
				BcfaccommodationsaddonWebConstants.PASSENGER_TYPE_CODE_CHILD);
		final List<PassengerTypeData> sortedPassengerTypes = travellerSortStrategy
				.sortPassengerTypes(passengerTypeFacade.getPassengerTypesForCodes(codes));
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}

	private String getLocationName(final String locationCode)
	{
		final TransportFacilityData transportFacilityData = transportFacilityFacade.getTransportFacility(locationCode);
		final LocationData location = transportFacilityFacade.getLocation(locationCode);
		String locationName = location.getName();
		if (Objects.nonNull(transportFacilityData))
		{
			locationName = locationName + " ( " + transportFacilityData.getName() + " )";
		}
		return locationName;
	}

	private String getFormattedDate(final java.util.Date date)
	{
		return TravelDateUtils
				.convertDateToStringDate(date, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
	}

	public List<RoomStayCandidateData> populateRoomStayCandidates()
	{
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		final int maxAccommodationsQuantity = configurationService.getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 0; i < maxAccommodationsQuantity; i++)
		{
			final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
			roomStayCandidateData.setRoomStayCandidateRefNumber(i);
			roomStayCandidates.add(roomStayCandidateData);
		}
		return roomStayCandidates;
	}

	private RoomStayCandidateData createRoomStayCandidatesData()
	{
		final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
		roomStayCandidateData.setPassengerTypeQuantityList(getPassengerTypeQuantityList());
		for (final PassengerTypeQuantityData passengeTypeQuantityData : roomStayCandidateData.getPassengerTypeQuantityList())
		{
			if (passengeTypeQuantityData.getPassengerType().getCode().equals(TravelfacadesConstants.PASSENGER_TYPE_CODE_ADULT))
			{
				passengeTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_ADULT_QUANTITY);
			}
		}
		return roomStayCandidateData;
	}


	public List<String> populatePassengersQuantity()
	{
		final List<String> guestsQuantity = new ArrayList<>();
		final String maxPersonalAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		final int maxGuestQuantity = StringUtils.isNotEmpty(maxPersonalAllowed) ? Integer.parseInt(maxPersonalAllowed) : 9;
		for (int i = 0; i <= maxGuestQuantity; i++)
		{
			guestsQuantity.add(String.valueOf(i));
		}
		return guestsQuantity;
	}

	public List<Integer> populateAccommodationsQuantity()
	{
		final List<Integer> accommodationQuantity = new ArrayList<>();
		final int maxAccommodationsQuantity = configurationService.getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 1; i <= maxAccommodationsQuantity; i++)
		{
			accommodationQuantity.add(i);
		}
		return accommodationQuantity;
	}

	public String getTravelRouteForForm(final List<TravelRouteData> travelRouteDatas, final String modifiedTravelRoute)
	{
		if (CollectionUtils.isNotEmpty(travelRouteDatas) && StringUtils.isNotBlank(modifiedTravelRoute))
		{
			final TravelRouteData modifiedTravelRouteData = bcfTravelRouteFacade.getTravelRoute(modifiedTravelRoute);
			final TravelRouteData selectedTravelRouteData = travelRouteDatas.stream().filter(
					travelRouteData -> travelRouteData.getOrigin().getCode().equals(modifiedTravelRouteData.getOrigin().getCode())
							&& travelRouteData.getDestination().getCode().equals(modifiedTravelRouteData.getDestination().getCode()))
					.findAny().orElse(null);
			if (selectedTravelRouteData != null)
			{
				return selectedTravelRouteData.getCode();
			}
		}
		return modifiedTravelRoute;
	}


	public Boolean isOnlyAccommodationChange(
			final AccommodationFinderForm modifiedAccommodationFinderForm,
			final AccommodationFinderForm existingAccommodationFinderForm, final String existingTravelRoute,
			final String modifiedTravelRoute)
	{
		if (!modifiedAccommodationFinderForm.getCheckInDateTime()
				.equalsIgnoreCase(existingAccommodationFinderForm.getCheckInDateTime()))
		{
			return false;
		}
		if (!modifiedAccommodationFinderForm.getCheckOutDateTime()
				.equalsIgnoreCase(existingAccommodationFinderForm.getCheckOutDateTime()))
		{
			return false;
		}
		final TravelRouteData modifiedTravelRouteData=bcfTravelRouteFacade.getTravelRoute(modifiedTravelRoute);
		final TravelRouteData existingTravelRouteData=bcfTravelRouteFacade.getTravelRoute(existingTravelRoute);
		if (!existingTravelRouteData.getOrigin().getCode().equalsIgnoreCase(modifiedTravelRouteData.getOrigin().getCode()) || !existingTravelRouteData.getDestination().getCode().equalsIgnoreCase(modifiedTravelRouteData.getDestination().getCode()))
		{
			return false;
		}

		return !checkIfPassengersChanged(modifiedAccommodationFinderForm, existingAccommodationFinderForm);
	}

	private boolean checkIfPassengersChanged(final AccommodationFinderForm modifiedAccommodationFinderForm,
			final AccommodationFinderForm existingAccommodationFinderForm)
	{
		final List<RoomStayCandidateData> modifiedRoomStayCandidateDatas = modifiedAccommodationFinderForm.getRoomStayCandidates()
				.stream().filter(roomStayCandidates -> CollectionUtils
						.isNotEmpty(roomStayCandidates.getPassengerTypeQuantityList()) && roomStayCandidates
						.getPassengerTypeQuantityList().stream().anyMatch(paxTypeAndQty -> paxTypeAndQty.getQuantity() > 0)).collect(
						Collectors.toList());

		final List<RoomStayCandidateData> existingRoomStayCandidateDatas = existingAccommodationFinderForm.getRoomStayCandidates()
				.stream().filter(roomStayCandidates -> CollectionUtils
						.isNotEmpty(roomStayCandidates.getPassengerTypeQuantityList()) && roomStayCandidates
						.getPassengerTypeQuantityList().stream().anyMatch(paxTypeAndQty -> paxTypeAndQty.getQuantity() > 0)).collect(
						Collectors.toList());

		if (existingRoomStayCandidateDatas.size() != modifiedRoomStayCandidateDatas.size())
		{
			return true;
		}

		for (final RoomStayCandidateData roomStayCandidateData : modifiedRoomStayCandidateDatas)
		{

			final Map<String, Integer> modifiedTravellerTypeAndQtyMap = roomStayCandidateData.getPassengerTypeQuantityList().stream()
					.collect(
							Collectors
									.groupingBy(paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode(), Collectors.summingInt(
											PassengerTypeQuantityData::getQuantity)));


			final Map<String, Integer> existingTravellerTypeAndQtyMap = existingAccommodationFinderForm.getRoomStayCandidates()
					.stream()
					.filter(roomStayCandidate -> roomStayCandidate.getRoomStayCandidateRefNumber()
							.equals(roomStayCandidateData.getRoomStayCandidateRefNumber()))
					.flatMap(roomStayCandidate -> roomStayCandidate.getPassengerTypeQuantityList().stream()).collect(Collectors
							.groupingBy(paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode(),
									Collectors.summingInt(PassengerTypeQuantityData::getQuantity)));



			final long count = modifiedTravellerTypeAndQtyMap.entrySet().stream().filter(
					modifiedTravellerTypeAndQtyMapVal -> existingTravellerTypeAndQtyMap.entrySet().stream().anyMatch(
							existingTravellerTypeAndQtyMapVal -> (
									existingTravellerTypeAndQtyMapVal.getKey().equals(modifiedTravellerTypeAndQtyMapVal.getKey())
											&& existingTravellerTypeAndQtyMapVal.getValue() == modifiedTravellerTypeAndQtyMapVal
											.getValue())))
					.count();
			if (modifiedTravellerTypeAndQtyMap.size() != count)
			{
				return true;
			}
			else
			{

				final List<Integer> modifiedChildAges = roomStayCandidateData.getPassengerTypeQuantityList().stream()

						.filter(
								paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode().equals("child")
										&& paxTypeAndQty.getQuantity() > 0 && CollectionUtils.isNotEmpty(paxTypeAndQty.getChildAges()))
						.flatMap(paxTypeAndQty -> paxTypeAndQty.getChildAges().stream()).collect(
								Collectors.toList());

				final List<Integer> existingChildAges = existingAccommodationFinderForm.getRoomStayCandidates()
						.stream()
						.filter(roomStayCandidate -> roomStayCandidate.getRoomStayCandidateRefNumber()
								.equals(roomStayCandidateData.getRoomStayCandidateRefNumber()))
						.flatMap(roomStayCandidate -> roomStayCandidate.getPassengerTypeQuantityList().stream())
						.filter(
								paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode().equals("child")
										&& paxTypeAndQty.getQuantity() > 0 && CollectionUtils.isNotEmpty(paxTypeAndQty.getChildAges()))
						.flatMap(paxTypeAndQty -> paxTypeAndQty.getChildAges().stream()).collect(
								Collectors.toList());

				if (!modifiedChildAges.containsAll(existingChildAges))
				{

					return true;
				}

			}

		}
		return false;

	}


	public void validateForms(final TravelFinderForm travelFinderForm, final BindingResult bindingResult)
	{
		((BeanPropertyBindingResult) bindingResult).setMessageCodesResolver(customMessageCodesResolver);
		validateFareFinderForm(travelFareFinderValidator);

		if (!bindingResult.hasErrors())
		{
			validateTravelFinderForm(travelFinderValidator, travelFinderForm,	bindingResult);
		}

		if (!bindingResult.hasErrors())
		{
			validateAccommodationFinderForm(accommodationFinderValidator, travelFinderForm.getAccommodationFinderForm(),
					bindingResult);
		}
	}

	protected void validateFareFinderForm(final AbstractTravelValidator fareFinderValidator)
	{
		fareFinderValidator.setTargetForm(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		fareFinderValidator.setAttributePrefix(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
	}

	protected void validateTravelFinderForm(final AbstractTravelValidator travelFinderValidator,
			final TravelFinderForm travelFinderForm, final BindingResult bindingResult)
	{
		travelFinderValidator.setTargetForm(BcfstorefrontaddonWebConstants.TRAVEL_FINDER_FORM);
		travelFinderValidator.validate(travelFinderForm, bindingResult);
	}

	protected void validateAccommodationFinderForm(final AbstractTravelValidator accommodationFinderValidator,
			final AccommodationFinderForm accommodationFinderForm, final BindingResult bindingResult)
	{
		accommodationFinderValidator.setTargetForm(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.setAttributePrefix(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		accommodationFinderValidator.validate(accommodationFinderForm, bindingResult);
	}

	public List<AdjustCostData> populateAccommodationAdjustCostData(final List<ReservedRoomStayData> reservedRoomStayDatas)
	{
		final List<AdjustCostData> adjustCostDatas=new ArrayList<>();
		for(final ReservedRoomStayData reservedRoomStayData:reservedRoomStayDatas){

			final AdjustCostData adjustCostData=new AdjustCostData();
			adjustCostData.setRefNumber(reservedRoomStayData.getRoomStayRefNumber());

			String description=reservedRoomStayData.getAccommodationOfferingName();
			if(CollectionUtils.isNotEmpty(reservedRoomStayData.getRoomTypes())){
				description=description+BcfstorefrontaddonWebConstants.HYPHEN+ reservedRoomStayData.getRoomTypes().get(0).getName();
			}
			adjustCostData.setRefDescription(description);
			adjustCostDatas.add(adjustCostData);

		}
		return adjustCostDatas;
	}

	public List<AdjustCostData> populateActivityAdjustCostData(final List<ActivityReservationData> activityReservationDatas)
	{
		final List<AdjustCostData> adjustCostDatas=new ArrayList<>();
		for(final ActivityReservationData activityReservationData:activityReservationDatas){

			final AdjustCostData adjustCostData=new AdjustCostData();
			adjustCostData.setRefNumber(activityReservationData.getEntryNumber());

			final String description=activityReservationData.getActivityDetails().getName()+BcfstorefrontaddonWebConstants.HYPHEN+activityReservationData.getActivityDate();
			adjustCostData.setRefDescription(description);
			adjustCostDatas.add(adjustCostData);

		}
		return adjustCostDatas;
	}


}
