/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;


import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.packages.response.PackageResponseData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfaccommodationsaddon.forms.cms.BcfAccommodationAddToCartForm;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.packages.BcfPackageFacade;


@Controller
@RequestMapping("/vacations/hotels")
public class PackageHotelDetailsPageController extends AbstractPackagePageController
{
   @Resource(name = "bcfPackageFacade")
   private BcfPackageFacade bcfPackageFacade;

   @Resource(name = "sessionService")
   private SessionService sessionService;

   private static final String PROMOTION_ROOM_RATE_PRODUCTS = "promotionRoomRateProducts";
   private static final String PROMOTION_ACCOMMODATION_OFFERING_CODE = "promotionAccommodationOfferingCode";
   private static final String PROMOTION_CODE = "promotionCode";
   private static final String HIDE_PRICE_AND_BOOKING = "hidePriceAndBooking";

   private static final String PACKAGE_HOTEL_DETAILS_PAGE = "packageHotelDetailsPage";

   @RequestMapping(value = "/{cityName}/{hotelName}/{accommodationOfferingCode}", method = RequestMethod.GET)
   public String getPackageDetailsPage(@PathVariable("cityName") final String cityName,
         @PathVariable("hotelName") final String hotelName,
         @PathVariable("accommodationOfferingCode") final String accommodationOfferingCode,
         @RequestParam(value = "roomRateProducts", required = false, defaultValue = StringUtils.EMPTY) final String roomRateProducts,
         @RequestParam(value = "packageDealCode", required = false) final String packageDealCode,
         final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
   {

      final PackageResponseData packageResponseData = bcfPackageFacade
            .getPackageDataByAccommodationOfferingCode(accommodationOfferingCode, roomRateProducts.split(","));

      populateModel(model, packageResponseData);

      model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_IN_CART, bcfPackageFacade.isPackageInCart());
      model.addAttribute(PROMOTION_ROOM_RATE_PRODUCTS, roomRateProducts);
      model.addAttribute(PROMOTION_ACCOMMODATION_OFFERING_CODE, accommodationOfferingCode);
      model.addAttribute(PROMOTION_CODE, packageDealCode);
      model.addAttribute(HIDE_PRICE_AND_BOOKING, Boolean.TRUE);
      model.addAttribute("packageChangeAccommodationForm", new BcfAccommodationAddToCartForm());
      sessionService.setAttribute("packageHotelUrl", request.getRequestURI()+"?"+request.getQueryString());

      final ContentPageModel contentPageModel = getContentPageForLabelOrId(PACKAGE_HOTEL_DETAILS_PAGE);
      storeCmsPageInModel(model, contentPageModel);
      setUpMetaDataForContentPage(model, contentPageModel);

      populateDynamicDataForPackageHotel(model, packageResponseData, contentPageModel, request);

      return getViewForPage(model);

   }

   protected void populateModel(final Model model, final PackageResponseData packageResponseData)
   {
      model.addAttribute(BcfcommonsaddonWebConstants.IS_PACKAGE_UNAVAILABLE,
            !packageResponseData.isAvailable() ? Boolean.TRUE : Boolean.FALSE);
      model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
      model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
      model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
            getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));
      model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_DETAILS_CHANGED_FLAG, packageResponseData
            .getAccommodationPackageResponse().getAccommodationAvailabilityResponse().getConfigRoomsUnavailable());
      model.addAttribute(BcfcommonsaddonWebConstants.PACKAGE_AVAILABILITY_RESPONSE, packageResponseData);
      final boolean isAmendJourney = getTravelCartFacade().isAmendmentCart();
      model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, isAmendJourney);
      model.addAttribute(BcfcommonsaddonWebConstants.ADD_ROOM_PACKAGE_URL,
            isAmendJourney ? BcfstorefrontaddonWebConstants.ADD_ROOM_PACKAGE_DETAILS_AMENDMENT_PAGE
                  : BcfstorefrontaddonWebConstants.ADD_ROOM_PACKAGE_DETAILS_PAGE);
      model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, BcfstorefrontaddonWebConstants.PACKAGE_FERRY_INFO_PAGE_PATH);
   }

}
