/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers;

import com.bcf.bcfcommonsaddon.model.components.AlacarteReservationComponentModel;
import com.bcf.bcfcommonsaddon.model.components.BcfGlobalReservationBreakdownComponentModel;
import com.bcf.bcfcommonsaddon.model.components.BcfGlobalReservationComponentModel;
import com.bcf.bcfcommonsaddon.model.components.BcfOrderConfirmationComponentModel;
import com.bcf.bcfcommonsaddon.model.components.ChangeBookingFormComponentModel;
import com.bcf.bcfcommonsaddon.model.components.ChangeBookingRequestsComponentModel;
import com.bcf.bcfcommonsaddon.model.components.DealComponentModel;
import com.bcf.bcfcommonsaddon.model.components.FeaturedActivityComponentModel;
import com.bcf.bcfcommonsaddon.model.components.PackageFinderComponentModel;
import com.bcf.bcfcommonsaddon.model.components.QuoteInfoComponentModel;
import com.bcf.bcfcommonsaddon.model.components.TransportReservationComponentModel;
import com.bcf.bcfcommonsaddon.model.components.TravelBookingDetailsComponentModel;
import com.bcf.bcfcommonsaddon.model.components.TravelBookingListComponentModel;
import com.bcf.bcfcommonsaddon.model.components.TravelBookingPaymentDetailsComponentModel;


/**
 *
 */
public interface BcfcommonsaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/bcfcommonsaddon/";

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Pages
		{
			interface PackageSearch
			{
				String packageResultsViewJsonResponse = ADDON_PREFIX + "pages/package/packageResultsViewJsonResponse";
				String packageListingJsonResponse = ADDON_PREFIX + "pages/package/packageListingJsonResponse";
				String packageDetailsJsonResponse = ADDON_PREFIX + "pages/package/packageDetailsJsonResponse";
			}

			interface Deal
			{
				String DealValidDatesJsonResponse = ADDON_PREFIX + "pages/deal/dealValidDatesJsonResponse";
				String DealDepartureDateValidationJsonResponse = ADDON_PREFIX + "pages/deal/dealDepartureDateValidationJsonResponse";
				String DealDetailsReturnDateJsonResponse = ADDON_PREFIX + "pages/deal/dealDetailsReturnDateJsonResponse";
				String AddDealToCartResponse = ADDON_PREFIX + "pages/deal/addDealToCartResponse";
			}

			interface Booking
			{
				String AddAccommodationRoomJsonResponse = ADDON_PREFIX + "pages/booking/addAccommodationRoomJsonResponse";
				String AddAccommodationRoomPackageFormValidationJsonResponse = ADDON_PREFIX
						+ "pages/booking/validateAddRoomForPackageJsonResponse";
				String resetMarkersResponse = ADDON_PREFIX + "pages/booking/resetMarkersJsonResponse";
				String quoteMessageResponse = ADDON_PREFIX + "pages/booking/quoteMessageJsonResponse";
			}

			interface Schedule
			{
				String ScheduleRouteMapJsonResponse = ADDON_PREFIX + "pages/schedules/scheduleRouteMapLandingPage";
			}

			interface FormErrors
			{
				String FormErrorsResponse = ADDON_PREFIX + "pages/form/formErrorsResponse";
			}

			interface Activity
			{
				String ActivityPricesAndSchedulesResponse = ADDON_PREFIX + "pages/activities/activityPricesAndSchedulesJSONData";
				String AddActivityToCartJsonResponse = ADDON_PREFIX + "pages/activities/addActivityToCartJsonResponse";
			}

			interface TravelBookingListing
			{
				String travelBookingListingPage = ADDON_PREFIX + "pages/travelbookinglisting/travelBookingListing";
				String vacationsBookingListing = ADDON_PREFIX + "pages/travelbookinglisting/vacationsBookingListing";
			}

			interface Inventory
			{
				String AccommodationInventoryReportJsonResponse =
						ADDON_PREFIX + "pages/inventory/accommodationInventoryReportJsonResponse";
				String ActivityInventoryReportJsonResponse = ADDON_PREFIX + "pages/inventory/activityInventoryReportJsonResponse";
			}
		}

		interface CMS
		{
			String accessibilityNeedsPerPAX = ADDON_PREFIX + "cms/accessibilityNeedsPerPAXPackage";
			String tentativeOrderConfirmation = ADDON_PREFIX + "cms/bcftentativeorderconfirmationcomponent";
		}
	}

	interface Actions
	{
		interface Cms
		{
			String _Prefix = "/view/";

			String _Suffix = "Controller";

			/**
			 * CMS components that have specific handlers
			 */
			String BcfGlobalReservationComponent = _Prefix + BcfGlobalReservationComponentModel._TYPECODE + _Suffix;
			String BcfGlobalReservationBreakdownComponent =
					_Prefix + BcfGlobalReservationBreakdownComponentModel._TYPECODE + _Suffix;
			String BcfOrderConfirmationComponent =
					_Prefix + BcfOrderConfirmationComponentModel._TYPECODE + _Suffix;
			String PackageFinderComponent = _Prefix + PackageFinderComponentModel._TYPECODE + _Suffix;
			String TravelBookingDetailsComponent = _Prefix + TravelBookingDetailsComponentModel._TYPECODE + _Suffix;
			String TravelBookingListComponent = _Prefix + TravelBookingListComponentModel._TYPECODE + _Suffix;
			String DealComponent = _Prefix + DealComponentModel._TYPECODE + _Suffix;
			String TravelBookingPaymentDetailsComponent = _Prefix + TravelBookingPaymentDetailsComponentModel._TYPECODE + _Suffix;
			String QuoteInfoComponent = _Prefix + QuoteInfoComponentModel._TYPECODE + _Suffix;
			String TransportReservationComponent = _Prefix + TransportReservationComponentModel._TYPECODE + _Suffix;
			String ChangeBookingFormComponent = _Prefix + ChangeBookingFormComponentModel._TYPECODE + _Suffix;
			String ChangeBookingRequestsComponent = _Prefix + ChangeBookingRequestsComponentModel._TYPECODE + _Suffix;
			String FeaturedActivityComponent = _Prefix + FeaturedActivityComponentModel._TYPECODE + _Suffix;
			String AlacarteReservationComponent = _Prefix + AlacarteReservationComponentModel._TYPECODE + _Suffix;
		}
	}
}
