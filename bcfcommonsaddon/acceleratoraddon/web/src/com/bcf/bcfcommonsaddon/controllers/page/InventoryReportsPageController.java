/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfaccommodationsaddon.controllers.pages.AbstractAccommodationPageController;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.reports.inventory.AccommodationInventoryReportForm;
import com.bcf.bcfcommonsaddon.forms.reports.inventory.ActivityInventoryReportForm;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.reports.inventory.BcfInventoryReportFacade;
import com.bcf.request.data.reports.inventory.AccommodationInventoryRequestData;
import com.bcf.request.data.reports.inventory.AccommodationOfferingInventoryResponseData;
import com.bcf.request.data.reports.inventory.ActivityInventoryRequestData;
import com.bcf.request.data.reports.inventory.ActivityInventoryResponseData;


@Controller
@RequestMapping("/inventory-reports")
public class InventoryReportsPageController extends AbstractAccommodationPageController
{
	private static final String ACCOMMODATION_INVENTORY_REPORT_FORM = "accommodationInventoryReportForm";
	private static final String ACCOMMODATION_LEVEL_DATAS = "accommodationLevelDatas";
	private static final String ACCOMMODATION_OFFERING_INVENTORY_RESPONSE_DATAS = "accommodationOfferingInventoryResponseDatas";

	private static final String ACTIVITY_LEVEL_DATAS = "activityLevelDatas";
	private static final String ACTIVITY_INVENTORY_REPORT_FORM = "activityInventoryReportForm";
	private static final String ACTIVITY_INVENTORY_RESPONSE_DATAS = "activityInventoryResponseDatas";
	private static final String VACATION_INVENTORY_REPORT_CMS_PAGE = "vacationInventoryReportPage";

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "bcfInventoryReportFacade")
	private BcfInventoryReportFacade bcfInventoryReportFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getInventoryReportsPage(final Model model) throws CMSItemNotFoundException
	{
		// for Accommodation Inventory Report
		final JSONArray accommodationLevelDatas = new JSONArray(bcfTravelLocationFacade.getAccommodationLevelDatas());
		model.addAttribute(ACCOMMODATION_LEVEL_DATAS, accommodationLevelDatas);
		final AccommodationInventoryReportForm accommodationInventoryReportForm = getAccommodationInventoryReportForm();
		model.addAttribute(ACCOMMODATION_INVENTORY_REPORT_FORM, accommodationInventoryReportForm);

		// for Activity Inventory Report
		final JSONArray activityLevelDatas = new JSONArray(bcfTravelLocationFacade.getActivityLevelDatas());
		model.addAttribute(ACTIVITY_LEVEL_DATAS, activityLevelDatas);
		final ActivityInventoryReportForm activityInventoryReportForm = getActivityInventoryReportForm();
		model.addAttribute(ACTIVITY_INVENTORY_REPORT_FORM, activityInventoryReportForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(VACATION_INVENTORY_REPORT_CMS_PAGE));
		return getViewForPage(model);
	}

	private AccommodationInventoryReportForm getAccommodationInventoryReportForm()
	{
		final AccommodationInventoryReportForm accommodationInventoryReportForm = new AccommodationInventoryReportForm();
		final String currentDate = TravelDateUtils.convertDateToStringDate(new Date(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);
		accommodationInventoryReportForm.setStartDate(currentDate);
		accommodationInventoryReportForm.setEndDate(currentDate);
		final String[] daysOfWeek = Arrays.stream(DayOfWeek.class.getEnumConstants()).map(Enum::name).toArray(String[]::new);
		accommodationInventoryReportForm.setDaysOfWeek(daysOfWeek);
		return accommodationInventoryReportForm;
	}

	private ActivityInventoryReportForm getActivityInventoryReportForm()
	{
		final ActivityInventoryReportForm activityInventoryReportForm = new ActivityInventoryReportForm();
		final String currentDate = TravelDateUtils.convertDateToStringDate(new Date(), BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);
		activityInventoryReportForm.setStartDate(currentDate);
		activityInventoryReportForm.setEndDate(currentDate);
		final String[] daysOfWeek = Arrays.stream(DayOfWeek.class.getEnumConstants()).map(Enum::name).toArray(String[]::new);
		activityInventoryReportForm.setDaysOfWeek(daysOfWeek);
		return activityInventoryReportForm;
	}

	@RequestMapping(value = "/accommodation", method = RequestMethod.POST, produces = "application/json")
	public String getAccommodationInventoryReportJsonResponse(
			@Valid final AccommodationInventoryReportForm accommodationInventoryReportForm,
			final Model model)
	{
		final Date startDate = TravelDateUtils.convertStringDateToDate(accommodationInventoryReportForm.getStartDate(),
				BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);
		final Date endDate = TravelDateUtils.convertStringDateToDate(accommodationInventoryReportForm.getEndDate(),
				BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);

		final AccommodationInventoryRequestData accommodationInventoryRequestData = new AccommodationInventoryRequestData();
		accommodationInventoryRequestData.setStartDate(startDate);
		accommodationInventoryRequestData.setEndDate(endDate);
		accommodationInventoryRequestData.setDaysOfWeek(Arrays.asList(accommodationInventoryReportForm.getDaysOfWeek()));
		accommodationInventoryRequestData.setGeoAreaCode(accommodationInventoryReportForm.getRegion());
		accommodationInventoryRequestData.setCityCode(accommodationInventoryReportForm.getCity());
		accommodationInventoryRequestData.setAccommodationOfferingCode(accommodationInventoryReportForm.getHotel());
		accommodationInventoryRequestData.setAccommodationCode(accommodationInventoryReportForm.getRoom());

		final List<AccommodationOfferingInventoryResponseData> accommodationOfferingInventoryResponseDatas = bcfInventoryReportFacade
				.getAccommodationInventoryResults(accommodationInventoryRequestData);
		model.addAttribute(ACCOMMODATION_OFFERING_INVENTORY_RESPONSE_DATAS, accommodationOfferingInventoryResponseDatas);
		return BcfcommonsaddonControllerConstants.Views.Pages.Inventory.AccommodationInventoryReportJsonResponse;
	}

	@RequestMapping(value = "/activity", method = RequestMethod.POST, produces = "application/json")
	public String getActivityInventoryReportJsonResponse(
			@Valid final ActivityInventoryReportForm activityInventoryReportForm,
			final Model model)
	{
		final Date startDate = TravelDateUtils.convertStringDateToDate(activityInventoryReportForm.getStartDate(),
				BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);
		final Date endDate = TravelDateUtils.convertStringDateToDate(activityInventoryReportForm.getEndDate(),
				BcfFacadesConstants.DATE_PATTERN_DD_MM_YYYY);

		final ActivityInventoryRequestData activityInventoryRequestData = new ActivityInventoryRequestData();
		activityInventoryRequestData.setStartDate(startDate);
		activityInventoryRequestData.setEndDate(endDate);
		activityInventoryRequestData.setDaysOfWeek(Arrays.asList(activityInventoryReportForm.getDaysOfWeek()));
		activityInventoryRequestData.setGeoAreaCode(activityInventoryReportForm.getRegion());
		activityInventoryRequestData.setCityCode(activityInventoryReportForm.getCity());
		activityInventoryRequestData.setActivityCode(activityInventoryReportForm.getActivity());

		final List<ActivityInventoryResponseData> activityInventoryResponseDatas = bcfInventoryReportFacade
				.getActivityInventoryResults(activityInventoryRequestData);
		model.addAttribute(ACTIVITY_INVENTORY_RESPONSE_DATAS, activityInventoryResponseDatas);

		return BcfcommonsaddonControllerConstants.Views.Pages.Inventory.ActivityInventoryReportJsonResponse;
	}

}
