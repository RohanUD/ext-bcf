/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.model.components.QuoteInfoComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.order.BcfTravelCartFacade;


@Controller("QuoteInfoComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.QuoteInfoComponent)
public class QuoteInfoComponentController extends SubstitutingCMSAddOnComponentController<QuoteInfoComponentModel>
{
	private static final Logger LOG = Logger.getLogger(QuoteInfoComponentController.class);

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final QuoteInfoComponentModel component)
	{
		model.addAttribute(BcfstorefrontaddonWebConstants.QUOTE_EXPIRATION_TIME, bcfTravelCartFacade.getQuotationExpirationTime());

		final List<TravelRouteModel> removedSailings = sessionService.getAttribute(BcfCoreConstants.SAILINGS_REMOVED_FROM_CART);
		if (CollectionUtils.isNotEmpty(removedSailings))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.REMOVED_SAILINGS_FROM_CART, removedSailings);
			sessionService.removeAttribute(BcfCoreConstants.SAILINGS_REMOVED_FROM_CART);
		}
	}

	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOG.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}
}
