/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.AddDealBundleToCartForm;
import com.bcf.bcfcommonsaddon.validators.AddDealBundleToCartFormValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.data.AddActivityToCartData;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.accommodation.enums.AvailabilityStatus;
import com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.packages.cart.AddBcfdDealToCartData;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


/**
 * Controller for Add to Cart functionalities for features belonging to travel site
 */
@Controller
public class TravelAddToCartController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(TravelAddToCartController.class);
	private static final String ADD_DEAL_TO_CART_ERROR = "add.deal.to.cart.error";

	private static final String ADD_DEAL_TO_CART_RESPONSE = "addDealToCartResponse";

	@Resource(name = "bcfDealCartFacade")
	private BcfDealCartFacade dealCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "addDealBundleToCartFormValidator")
	private AddDealBundleToCartFormValidator addDealBundleToCartFormValidator;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@RequestMapping(value = "/cart/addDeal", method = RequestMethod.POST, produces = "application/json")
	public String addDealToCart(final AddDealBundleToCartForm addDealBundleToCartForm, final BindingResult bindingResult,
			final Model model)
	{
		addDealBundleToCartFormValidator.validate(addDealBundleToCartForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			return getAddDealToCartError(model,
					bindingResult.getAllErrors().get(0).getCodes()[bindingResult.getAllErrors().get(0).getCodes().length - 1], false);
		}
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		final List<CartModificationData> cartModifications = dealCartFacade
				.addDealToCart(buildAddDealToCartData(addDealBundleToCartForm));
		if (cartModifications.stream().anyMatch(mod -> mod.getQuantityAdded() <= 0))
		{
			return getAddDealToCartError(model, ADD_DEAL_TO_CART_ERROR, false);
		}

		return getAddDealToCartError(model, null, true);

	}

	@RequestMapping(value = "/cart/validate-cart-type", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	boolean removeCurrentCart(final String journeyType)
	{
		return bcfTravelCartFacade.shallCartBeRemoved(journeyType);
	}

	@RequestMapping(value = "/cart/remove-current-cart", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	boolean removeCurrentCart()
	{
		return bcfTravelCartFacade.removeCurrentCart();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(BcfFacadesConstants.DEAL_OF_THE_DAY_DATE_PATTERN);
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

	@RequestMapping(value = "/cart/addDealActivity", method = RequestMethod.POST, produces = "application/json")
	public String addDealActivityToCart(final ActivityPricePerPassengerWithSchedule addActivityPriceSchedule,
			final BindingResult bindingResult, final RedirectAttributes redirectModel,
			final Model model)
	{
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		sessionService.setAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY, Boolean.FALSE);

		final int passengerQuantity = addActivityPriceSchedule.getActivityPricePerPassenger().stream()
				.mapToInt(activityPricePerPassenger -> activityPricePerPassenger.getPassengerType().getQuantity()).sum();


		String activitySelectedDate = null;
		final Date selectedDate;

		if (StringUtils.isBlank(addActivityPriceSchedule.getSelectedDate()) && Objects
				.isNull(addActivityPriceSchedule.getCheckInDate()))
		{
			LOG.error("Deal Activity Add to cart: Selected Date is empty.");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"activity.stock.not.available.error.message");
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL;
		}

		final AddActivityToCartData data = new AddActivityToCartData();
		if (Objects.nonNull(addActivityPriceSchedule.getSelectedDate()))
		{
			activitySelectedDate = addActivityPriceSchedule.getSelectedDate();
			selectedDate = TravelDateUtils
					.convertStringDateToDate(activitySelectedDate, BcfFacadesConstants.ACTIVITY_DATE_FORMAT);
			data.setActivityDate(TravelDateUtils.convertDateToStringDate(selectedDate, BcfFacadesConstants.ACTIVITY_DATE_PATTERN));
		}
		else
		{
			selectedDate = addActivityPriceSchedule.getCheckInDate();
			data.setActivityDate(TravelDateUtils
					.convertDateToStringDate(addActivityPriceSchedule.getCheckInDate(), BcfFacadesConstants.ACTIVITY_DATE_PATTERN));
			if (StringUtils.isNotBlank(addActivityPriceSchedule.getSelectedTime()))
			{
				data.setStartTime(addActivityPriceSchedule.getSelectedTime());
			}
		}

		if (BCFDateUtils.isDateInPast(selectedDate))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"activity.stock.not.available.error.message");
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL;
		}

		String availabilityStatus = bcfTravelCartFacade
				.getActivityAvailabilityStatus(addActivityPriceSchedule.getProductCode(), selectedDate,
						addActivityPriceSchedule.getSelectedTime(), passengerQuantity);

		try
		{
			if (AvailabilityStatus.AVAILABLE.name().equals(availabilityStatus) || AvailabilityStatus.ON_REQUEST.name()
					.equals(availabilityStatus))
			{
				final boolean success = dealCartFacade.addActivityToCart(addActivityPriceSchedule, data);

				bcfTravelCartFacadeHelper.updateAvailabilityStatus();
				bcfTravelCartFacadeHelper.calculateChangeFees();

				if (success)
				{
					final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade
							.getCurrentGlobalReservationData();
					redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);
					return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL;
				}
				else
				{
					LOG.error(String.format("Add deal activity [%s] to cart failed!", addActivityPriceSchedule.getProductCode()));
					return getAddDealToCartError(model, ADD_DEAL_TO_CART_ERROR, false);
				}
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"activity.stock.not.available.error.message");
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL;
			}
		}
		catch (final Exception e)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"activity.stock.not.available.error.message");
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ADD_ACTIVITIES_URL;
		}
	}

	protected String getAddDealToCartError(final Model model, final String message, final boolean isSuccess)
	{
		model.addAttribute(ADD_DEAL_TO_CART_RESPONSE,
				bcfTravelCartFacade.createAddToCartResponse(isSuccess, message, null, null));
		return BcfcommonsaddonControllerConstants.Views.Pages.Deal.AddDealToCartResponse;
	}

	protected AddBcfdDealToCartData buildAddDealToCartData(final AddDealBundleToCartForm addDealBundleToCartForm)
	{
		final AddBcfdDealToCartData addDealToCartData = new AddBcfdDealToCartData();
		addDealToCartData.setParentDealBundleId(addDealBundleToCartForm.getParentDealId());
		addDealToCartData.setPassengerTypes(
				addDealBundleToCartForm.getAddBundleToCartForms().stream().findFirst().get().getPassengerTypeQuantityList());
		addDealToCartData.setVehicleTypes(
				addDealBundleToCartForm.getAddBundleToCartForms().stream().findFirst().get().getVehicleTypeQuantityList());//NOSONAR
		addDealToCartData.setStartingDate(TravelDateUtils
				.convertStringDateToDate(addDealBundleToCartForm.getStartingDate(), BcfcommonsaddonWebConstants.DATE_FORMAT));
		if (CollectionUtils.isNotEmpty(addDealBundleToCartForm.getAddBundleToCartForms()))
		{
			addDealToCartData.setSelectedItineraryInfos(new HashMap<>());
			addDealBundleToCartForm.getAddBundleToCartForms().stream()
					.filter(addBundleToCartForm -> addBundleToCartForm.getItineraryPricingInfo().isSelected())
					.forEach(addBundleToCartForm -> populateSelectedItinerary(addDealToCartData
									.getSelectedItineraryInfos(), addBundleToCartForm.getDealId(),
							addBundleToCartForm.getItineraryPricingInfo())
					);
			final Map<String, List<AddBundleToCartForm>> addBundleToCartFormMap = addDealBundleToCartForm.getAddBundleToCartForms()
					.stream()
					.filter(addBundleToCartForm -> !addBundleToCartForm.getItineraryPricingInfo().isSelected())
					.collect(Collectors.groupingBy(AddBundleToCartForm::getDealId));
			for (final Map.Entry<String, List<AddBundleToCartForm>> entry : addBundleToCartFormMap.entrySet())
			{
				entry.getValue().stream()
						.collect(Collectors.groupingBy(AddBundleToCartForm::getOriginDestinationRefNumber,
								Collectors.collectingAndThen(Collectors.toList(), value -> value.stream().findFirst()))).values()
						.forEach(value -> {
							if (value.isPresent())
							{
								final AddBundleToCartForm addBundleToCartForm = value.get();
								populateSelectedItinerary(addDealToCartData
												.getSelectedItineraryInfos(), addBundleToCartForm.getDealId(),
										addBundleToCartForm.getItineraryPricingInfo());
							}
						});
			}
		}
		return addDealToCartData;
	}

	protected void populateSelectedItinerary(final Map<String, List<ItineraryPricingInfoData>> selectedItinerary,
			final String dealId,
			final ItineraryPricingInfoData itineraryPricingInfoData)
	{
		List<ItineraryPricingInfoData> itineraryPricingInfoDatas = selectedItinerary
				.get(dealId);
		if (CollectionUtils.isEmpty(itineraryPricingInfoDatas))
		{
			itineraryPricingInfoDatas = new ArrayList<>();
			itineraryPricingInfoDatas.add(itineraryPricingInfoData);
		}
		else if (itineraryPricingInfoDatas.stream()
				.noneMatch(itineraryPricingInfo -> StringUtils.isNotBlank(itineraryPricingInfo.getItineraryIdentifier())
						&& itineraryPricingInfo.getItineraryIdentifier()
						.equals(itineraryPricingInfoData.getItineraryIdentifier())))
		{
			//TODO  check above doesn't seem to be correct NOSONAR
			itineraryPricingInfoDatas.add(itineraryPricingInfoData);
		}
		selectedItinerary.put(dealId, itineraryPricingInfoDatas);
	}

	@ResponseBody
	@RequestMapping(value = "/checkDealActivityAvailability", method = RequestMethod.POST)
	public String checkDealActivityAvailability(final ActivityPricePerPassengerWithSchedule addActivityPriceSchedule)
	{
		Date activitySelectedDate = null;
		if (Objects.nonNull(addActivityPriceSchedule.getSelectedDate()))
		{
			activitySelectedDate = TravelDateUtils
					.convertStringDateToDate(addActivityPriceSchedule.getSelectedDate(), BcfFacadesConstants.ACTIVITY_DATE_FORMAT);
		}
		else
		{
			activitySelectedDate = addActivityPriceSchedule.getCheckInDate();
		}


		final int paxQuantity = addActivityPriceSchedule.getActivityPricePerPassenger().stream()
				.mapToInt(activityPricePerPassenger -> activityPricePerPassenger.getPassengerType().getQuantity()).sum();

		return bcfTravelCartFacade
				.getActivityAvailabilityStatus(addActivityPriceSchedule.getProductCode(), activitySelectedDate,
						addActivityPriceSchedule.getSelectedTime(), paxQuantity);
	}
}
