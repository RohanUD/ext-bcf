/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.fare.search.resolvers.FareSearchHashResolver;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.forms.cms.AddDealBundleToCartForm;
import com.bcf.bcfcommonsaddon.forms.cms.AddDealToCartForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfDealNotAvailableException;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.deals.search.DealBundleDetailsFacade;
import com.bcf.facades.packages.request.BcfPackageRequestData;
import com.bcf.facades.packages.response.BcfPackageResponseData;
import com.bcf.facades.packages.response.BcfPackagesResponseData;


@Controller
@RequestMapping(
		{ "/deal-details/d/{dealBundleTemplateName}/{dealBundleTemplateId}", "/deal-details" })
public class DealDetailsPageController extends AbstractPackagePageController
{
	private static final String DEAL_DETAILS_CMS_PAGE = "dealDetailsPage";
	private static final String HOME_PAGE_PATH = "/";

	@Resource(name = "dealSearchBundleTemplateFacade")
	private DealBundleDetailsFacade dealBundleDetailsFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "fareSearchHashResolver")
	private FareSearchHashResolver fareSearchHashResolver;

	@RequestMapping(method = RequestMethod.GET)
	public String getDealDetailsPage(
			@PathVariable(value = BcfcommonsaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID) final Optional<String> bundleTemplateId,
			@RequestParam(value = BcfcommonsaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID, required = false) String dealBundleTemplateId,
			@RequestParam(value = BcfcommonsaddonWebConstants.DEAL_SELECTED_DEPARTURE_DATE, required = false) final String dealSelectedDepartureDate,
			final Model model, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		disableCachingForResponse(response);
		if (bundleTemplateId.isPresent())
		{
			dealBundleTemplateId = bundleTemplateId.get();
		}
		if (StringUtils.isEmpty(dealBundleTemplateId) || StringUtils.isEmpty(dealSelectedDepartureDate))
		{
			return getChangeDealDateURLError(BcfcommonsaddonWebConstants.ERROR_DEAL_DETAILS_URL_INVALID, redirectModel);
		}
		if (!dealBundleDetailsFacade.isValidDealDepartureDate(dealSelectedDepartureDate, dealBundleTemplateId))
		{
			return getChangeDealDateURLError(BcfcommonsaddonWebConstants.ERROR_DEAL_DETAILS_URL_INVALID, redirectModel);
		}

		BcfPackageRequestData packageRequestData = null;
		try
		{
			packageRequestData = preparePackageRequestData(dealBundleTemplateId, dealSelectedDepartureDate);
		}
		catch (final InvalidParameterException ex)
		{
			return getChangeDealDateURLError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_INVALID_FORMAT, redirectModel);
		}
		catch (final BcfDealNotAvailableException ex)
		{
			return getChangeDealDateURLError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_NOT_AVAILABLE, redirectModel);
		}
		final BcfPackagesResponseData packagesResponseData = dealBundleDetailsFacade.getPackageResponseDetails(packageRequestData);

		if (Objects.isNull(packagesResponseData))
		{
			return getChangeDealDateURLError(BcfcommonsaddonWebConstants.ERROR_DEAL_DETAILS_URL_INVALID, redirectModel);

		}
		packageRequestData.getBcfPackageProductDatas().forEach(bcfPackageProductRequestData -> {
			final FareSearchRequestData fareSearchRequestData = bcfPackageProductRequestData.getTransportPackageRequest()
					.getFareSearchRequest();

			final BcfPackageResponseData bcfPackageResponseData = packagesResponseData.getPackageResponses().stream()
					.filter(BcfPackageResponseData.class::isInstance).map(BcfPackageResponseData.class::cast)
					.filter(packageResponseData -> packageResponseData.getPackageId()
							.equals(bcfPackageProductRequestData.getPackageId()))
					.findFirst().orElse(null);
			if (Objects.nonNull(bcfPackageResponseData) && Objects.nonNull(bcfPackageResponseData.getTransportPackageResponse())
					&& Objects.nonNull(bcfPackageResponseData.getTransportPackageResponse().getFareSearchResponse()))
			{
				final FareSelectionData fareSelectionData = bcfPackageResponseData.getTransportPackageResponse()
						.getFareSearchResponse();
				// sort fareSelectionData by displayOrder
				final String displayOrder = fareSearchRequestData.getSearchProcessingInfo().getDisplayOrder();
				sortFareSelectionData(fareSelectionData, displayOrder);
			}
		});

		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGES_RESPONSE_DATA, packagesResponseData);

		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_SELECTION_DATA);
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES);
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES);

		model.addAttribute(BcfcommonsaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID, dealBundleTemplateId);

		model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
		model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
		model.addAttribute(BcfstorefrontaddonWebConstants.GOOGLE_API_KEY,
				getConfigurationService().getConfiguration().getString(TravelfacadesConstants.GOOGLE_API_KEY));

		model.addAttribute(BcfvoyageaddonWebConstants.OUTBOUND_REF_NUMBER, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.INBOUND_REF_NUMBER, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.DEAPRTURE_DATE, dealSelectedDepartureDate);

		model.addAttribute(BcfcommonsaddonWebConstants.ADD_DEAL_TO_CART_FORM, initializeAddDealBundleToCartForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(DEAL_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(DEAL_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	protected AddDealBundleToCartForm initializeAddDealBundleToCartForm()
	{
		final AddDealBundleToCartForm addDealBundleToCartForm = new AddDealBundleToCartForm();
		addDealBundleToCartForm.setAddBundleToCartForms(new ArrayList<>());

		return addDealBundleToCartForm;
	}

	@RequestMapping(value = "/validate-departure-date", method = RequestMethod.GET)
	public String validateDealDepartureDate(
			@RequestParam(value = BcfcommonsaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID, required = true) final String dealBundleTemplateId,
			@RequestParam(value = BcfcommonsaddonWebConstants.DEAL_SELECTED_DEPARTURE_DATE, required = true) final String dealSelectedDepartureDate,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final Model model)
	{

		disableCachingForResponse(response);

		if (StringUtils.isEmpty(dealSelectedDepartureDate))
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_EMPTY, model);
		}

		final String startingDatePattern = dealBundleDetailsFacade.getDealValidCronJobExpressionById(dealBundleTemplateId);
		if (Objects.isNull(startingDatePattern))
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_BUNDLE_ID_INVALID, model);
		}

		final List<Date> validDates = dealBundleDetailsFacade.getDealValidDates(startingDatePattern, dealSelectedDepartureDate);
		final Date dealDepartureDate = TravelDateUtils.convertStringDateToDate(dealSelectedDepartureDate,
				TravelservicesConstants.DATE_PATTERN);

		if (Objects.isNull(dealDepartureDate))
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_INVALID_FORMAT, model);
		}
		else if (CollectionUtils.isEmpty(validDates) || !validDates.contains(dealDepartureDate))
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_NOT_AVAILABLE, model);
		}

		BcfPackageRequestData packageRequestData = null;
		try
		{
			packageRequestData = preparePackageRequestData(dealBundleTemplateId, dealSelectedDepartureDate);
		}
		catch (final InvalidParameterException ex)
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_INVALID_FORMAT, model);
		}
		catch (final BcfDealNotAvailableException ex)
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_NOT_AVAILABLE, model);
		}
		final BcfPackagesResponseData packagesResponseData = dealBundleDetailsFacade.getPackageResponseDetails(packageRequestData);
		if (Objects.isNull(packagesResponseData))
		{
			return getChangeDealDateError(BcfcommonsaddonWebConstants.ERROR_DEAL_DEPARTURE_DATE_NOT_AVAILABLE, model);
		}

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isNotEmpty(sessionBookingJourney))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);
		}
		model.addAttribute(BcfcommonsaddonWebConstants.PACKAGES_RESPONSE_DATA, packagesResponseData);
		model.addAttribute(BcfcommonsaddonWebConstants.ADD_DEAL_TO_CART_FORM, new AddDealToCartForm());
		model.addAttribute(BcfcommonsaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID, dealBundleTemplateId);
		return BcfcommonsaddonControllerConstants.Views.Pages.Deal.DealDetailsReturnDateJsonResponse;
	}

	protected String getChangeDealDateError(final String error, final Model model)
	{
		model.addAttribute(BcfcommonsaddonWebConstants.DEAL_CHANGE_DATE_VALIDATION_ERROR, error);

		return BcfcommonsaddonControllerConstants.Views.Pages.Deal.DealDetailsReturnDateJsonResponse;
	}

	protected String getChangeDealDateURLError(final String error, final RedirectAttributes redirectModel)
	{
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, error);
		return REDIRECT_PREFIX + HOME_PAGE_PATH;
	}

	/**
	 * This method populates and returns the PackageRequestData populated for the required bundleTemplate based on the
	 * given dealBundleTemplateId
	 *
	 * @param dealBundleTemplateId the dealBundleTemplateId
	 * @param departureDate        the departureDate
	 * @return the PackageRequestData
	 */
	protected BcfPackageRequestData preparePackageRequestData(final String dealBundleTemplateId, final String departureDate)
	{
		return dealBundleDetailsFacade.getBcfPackageRequestData(dealBundleTemplateId, departureDate);
	}

	protected void disableCachingForResponse(final HttpServletResponse response)
	{
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	}

	@ModelAttribute(BcfvoyageaddonWebConstants.PI_DATE_FORMAT)
	public String getPriceItineraryDateFormat()
	{
		return BcfvoyageaddonWebConstants.PRICED_ITINERARY_DATE_FORMAT;
	}


}
