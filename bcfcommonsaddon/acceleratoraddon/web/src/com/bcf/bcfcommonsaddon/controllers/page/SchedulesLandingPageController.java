/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.page;

import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.enumeration.EnumerationService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.bcfstorefrontaddon.forms.RegionFinderForm;
import com.bcf.core.enums.RouteRegion;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.webservices.schedules.BcfSchedulesFacade;


@Controller("SchedulesLandingPageController")
@RequestMapping(value = "/routes-fares/schedules")
public class SchedulesLandingPageController extends BcfAbstractPageController
{

	@Resource(name = "bcfSchedulesFacade")
	private BcfSchedulesFacade bcfSchedulesFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getSchedulesLandingDetails(final HttpServletRequest request, final Model model)
			throws CMSItemNotFoundException
	{
		final List<RouteRegion> routeRegionList = enumerationService.getEnumerationValues(RouteRegion._TYPECODE);
		model.addAttribute("schedules", bcfSchedulesFacade.getTravelRouteInfoWithRouteRegion(routeRegionList));
		final Map<String, String> routeRegions = new HashMap<>();
		routeRegionList.stream()
				.forEach(region -> routeRegions.put(region.getCode(), enumerationService.getEnumerationName(region)));
		model.addAttribute("routeRegions", routeRegions);
		model.addAttribute("regionFinderForm", new RegionFinderForm());
		final ContentPageModel contentPage = getContentPageForLabelOrId(BcfcommonsaddonWebConstants.SCHEDULE_LANDING_CMS_PAGE);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		return getViewForPage(model);
	}

	@Override
	protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		final List<MetaElementData> metadata = new LinkedList<>();
		metadata.add(createMetaElement("description", metaDescription));
		model.addAttribute("metatags", metadata);
	}

	@RequestMapping(value = "/schedule-routemap", method = RequestMethod.GET, produces =
			{ "application/json" })
	public String findrouteregions(@RequestParam(value = "routeRegions") final String routeRegions,
			final RegionFinderForm regionFinderForm,
			final Model model)
			throws CMSItemNotFoundException
	{
		final List<TravelRouteData> travelRoutelist = bcfTravelLocationFacade
				.getTravelRoutesByRegions(regionFinderForm.getRouteRegions());
		model.addAttribute("travelRoutelist", travelRoutelist);
		model.addAttribute("originToDestinationMapping", bcfTravelLocationFacade.createMapInfoWindowRouteMapping(travelRoutelist));
		return BcfcommonsaddonControllerConstants.Views.Pages.Schedule.ScheduleRouteMapJsonResponse;
	}
}
