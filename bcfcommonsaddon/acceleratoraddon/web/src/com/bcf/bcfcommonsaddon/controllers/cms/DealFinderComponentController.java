/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.enumeration.EnumerationService;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfaccommodationsaddon.constants.BcfaccommodationsaddonWebConstants;
import com.bcf.bcfcommonsaddon.forms.cms.DealFinderForm;
import com.bcf.bcfcommonsaddon.validators.DealFinderValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.DealType;


@Controller("DealFinderComponentController")
@RequestMapping(value = "/view/DealFinderComponentController")
public class DealFinderComponentController extends AbstractFinderComponentController
{
	protected static final String SEARCH = "/search";

	@Resource
	private EnumerationService enumerationService;

	@Resource(name = "dealFinderValidator")
	protected DealFinderValidator dealFinderValidator;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{

		final Map<String, String> dealTypeMap = enumerationService.getEnumerationValues(DealType.class).stream()
				.collect(Collectors.toMap(DealType::getCode, dealType -> enumerationService.getEnumerationName(dealType)));

		model.addAttribute("dealTypeMap", dealTypeMap);
		model.addAttribute(BcfvoyageaddonWebConstants.DEAL_FINDER_FORM, new DealFinderForm());
	}

	@RequestMapping(value = SEARCH, method = RequestMethod.POST)
	public String performSearch(@Valid final DealFinderForm dealFinderForm, final RedirectAttributes redirectModel,
			final BindingResult bindingResult)
	{
		dealFinderValidator.validate(dealFinderForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.DEAL_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + "/";
		}

		getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);

		redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.DEAL_FINDER_FORM, dealFinderForm);

		final String urlParameters = buildUrlParameters(dealFinderForm);
		final String redirectUrl = REDIRECT_PREFIX + "/deal-selection";
		return urlParameters.isEmpty() ? redirectUrl : redirectUrl + urlParameters;
	}

	@RequestMapping(value = "/validate-deal-finder-form", method = RequestMethod.POST)
	public String validateFareFinderForm(@Valid final DealFinderForm dealFinderForm, final BindingResult bindingResult,
			final Model model)
	{
		getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);

		dealFinderValidator.validate(dealFinderForm, bindingResult);

		model.addAttribute(BcfvoyageaddonWebConstants.DEAL_FINDER_FORM, dealFinderForm);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}

		return BcfvoyageaddonControllerConstants.Views.Pages.FormErrors.formErrorsResponse;
	}

	protected String buildUrlParameters(final DealFinderForm dealFinderForm)
	{
		final Map<String, String> urlParameters = new HashMap<>();
		final String dealselectionUrl;
		if (StringUtils.isNotBlank(dealFinderForm.getArrivalLocation()))
		{
			urlParameters.put(BcfvoyageaddonWebConstants.ARRIVAL_LOCATION, dealFinderForm.getArrivalLocation());
		}
		if (StringUtils.isNotBlank(dealFinderForm.getDepartureLocation()))
		{
			urlParameters.put(BcfvoyageaddonWebConstants.DEPARTURE_LOCATION, dealFinderForm.getDepartureLocation());
		}
		urlParameters.put(BcfvoyageaddonWebConstants.DEPARTURE_LOCATION_NAME, dealFinderForm.getDepartureDealLocationName());
		urlParameters.put(BcfvoyageaddonWebConstants.ARRIVAL_LOCATION_NAME, dealFinderForm.getArrivalDealLocationName());
		urlParameters.put(BcfvoyageaddonWebConstants.DEAPRTURE_DATE, dealFinderForm.getDepartureDate());

		urlParameters.put(BcfvoyageaddonWebConstants.DURATION, String.valueOf(dealFinderForm.getDuration()));
		urlParameters.put(BcfvoyageaddonWebConstants.STAR_RATING, String.valueOf(dealFinderForm.getStarRating()));
		if (StringUtils.isNotBlank(dealFinderForm.getDealType()))
		{
			urlParameters.put(BcfvoyageaddonWebConstants.DEAL_TYPE, dealFinderForm.getDealType());
		}
		// We need to encode '|' character because it is not allowed in Spring Security 4
		String encodedDestinationLocation = dealFinderForm.getDestinationLocation();
		encodedDestinationLocation = encodedDestinationLocation.replaceAll("\\|", "%7C");

		urlParameters.put(BcfaccommodationsaddonWebConstants.SUGGESTION_TYPE, dealFinderForm.getSuggestionType());
		urlParameters.put(BcfaccommodationsaddonWebConstants.FIELD_LOCATION, encodedDestinationLocation);
		dealselectionUrl =
				BcfvoyageaddonWebConstants.QUESTION + urlParameters.toString().replace(", ", "&").replace("{", "")
						.replace("}", "");
		return dealselectionUrl;

	}
}
