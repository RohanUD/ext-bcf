/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcommonsaddon.controllers.cms;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.ItineraryData;
import de.hybris.platform.commercefacades.travel.OriginDestinationOptionData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.facades.TravelImageFacade;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfcommonsaddon.constants.BcfcommonsaddonWebConstants;
import com.bcf.bcfcommonsaddon.controllers.BcfcommonsaddonControllerConstants;
import com.bcf.bcfcommonsaddon.model.components.TravelBookingListComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.booking.BcfBookingListFacade;
import com.bcf.facades.order.BcfOrderFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


/**
 * Travel Booking List Controller for handling requests for My Booking Section in My Account Page.
 */
@Controller("TravelBookingListComponentController")
@RequestMapping(value = BcfcommonsaddonControllerConstants.Actions.Cms.TravelBookingListComponent)
public class TravelBookingListComponentController extends SubstitutingCMSAddOnComponentController<TravelBookingListComponentModel>
{
	private static final String STARTING_NUMBER_OF_RESULTS = "startingNumberOfResults";
	private static final String TOTAL_NUMBER_OF_RESULTS = "totalNumberOfResults";
	private static final String TOTAL_SHOWN_RESULTS = "totalShownResults";
	private static final String PAGE_NUM = "pageNum";
	private static final String HAS_MORE_RESULTS = "hasMoreResults";

	@Resource(name = "bookingListFacade")
	private BcfBookingListFacade bookingListFacade;

	@Resource(name = "travelImageFacade")
	private TravelImageFacade travelImageFacade;

	@Resource(name = "timeService")
	private TimeService timeService;

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final TravelBookingListComponentModel component)
	{
		populateCommonModelAttributes(0, 1, bcfOrderFacade.findVacationsForCurrentUser(true), model);
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param pageNumber the page number
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping("/show-more")
	public String showMore(@RequestParam(value = "pageNumber", required = false) final int pageNumber,
			@RequestParam(value = "isUpcoming", required = false) final boolean isUpcoming, final Model model)
	{
		populateCommonModelAttributes(pageNumber - 1, pageNumber, bcfOrderFacade.findVacationsForCurrentUser(isUpcoming), model);
		return BcfcommonsaddonControllerConstants.Views.Pages.TravelBookingListing.travelBookingListingPage;
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param isUpcoming the Upcoming Orders
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping("/vacationslist")
	public String vacationsList(@RequestParam(value = "isUpcoming", required = false) final boolean isUpcoming, final Model model)
	{
		populateCommonModelAttributes(0, 1, bcfOrderFacade.findVacationsForCurrentUser(isUpcoming), model);
		return BcfcommonsaddonControllerConstants.Views.Pages.TravelBookingListing.vacationsBookingListing;
	}

	/**
	 * Populate common model attributes.
	 *
	 * @param pageNumber            the page number
	 * @param globalReservationData the Current User Orders Detail data
	 * @param model                 the model
	 */
	protected void populateCommonModelAttributes(final int startIndex, final int pageNumber,
			final List<BcfGlobalReservationData> globalReservationData, final Model model)
	{
		final int vacationsSearchPageSize = getConfigurationService().getConfiguration()
				.getInt(BcfcommonsaddonWebConstants.MY_BOOKINGS_PAGE_SIZE);
		if ((startIndex * vacationsSearchPageSize) <= Math.min(pageNumber * vacationsSearchPageSize, globalReservationData.size()))
		{
			model.addAttribute(BcfcommonsaddonWebConstants.MY_ACCOUNT_BOOKING,
					globalReservationData.subList(startIndex * vacationsSearchPageSize,
							Math.min(pageNumber * vacationsSearchPageSize, globalReservationData.size())));
		}
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, globalReservationData.size());
		model.addAttribute(STARTING_NUMBER_OF_RESULTS, (pageNumber - 1) * vacationsSearchPageSize + 1);
		final Boolean hasMoreResults = pageNumber * vacationsSearchPageSize < globalReservationData.size();
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);
		model.addAttribute(TOTAL_SHOWN_RESULTS,
				hasMoreResults ? pageNumber * vacationsSearchPageSize : globalReservationData.size());
		model.addAttribute(BcfcommonsaddonWebConstants.DATE_PATTERN, TravelservicesConstants.DATE_PATTERN);
	}

	protected Map<String, Boolean> getRemoveLinks(final List<GlobalTravelReservationData> myBookings)
	{
		final Map<String, Boolean> removeLinks = new HashMap<String, Boolean>();
		myBookings.forEach(myBooking -> {
			final String reservationNumber = getReservationNumber(myBooking);
			removeLinks.put(reservationNumber, Boolean.FALSE);
			if (canUnlinkBooking(myBooking))
			{
				removeLinks.put(reservationNumber, Boolean.TRUE);
			}
		});
		return removeLinks;
	}

	protected Boolean canUnlinkBooking(final GlobalTravelReservationData reservation)
	{
		if (reservation.getBookingStatusCode().equals(OrderStatus.PAST.getCode())
				|| reservation.getBookingStatusCode().equals(OrderStatus.CANCELLED.getCode()))
		{
			return true;
		}

		final List<Date> datesToCheck = new ArrayList<Date>();

		if (reservation.getAccommodationReservationData() != null)
		{
			datesToCheck.add(reservation.getAccommodationReservationData().getRoomStays().get(0).getCheckOutDate());
		}

		if (reservation.getReservationData() != null)
		{
			final List<ReservationItemData> reservationItems = reservation.getReservationData().getReservationItems();
			if (!CollectionUtils.isEmpty(reservationItems))
			{
				final ItineraryData reservationItinerary = reservationItems.get(reservationItems.size() - 1)
						.getReservationItinerary();
				if (reservationItinerary != null && CollectionUtils.isNotEmpty(reservationItinerary.getOriginDestinationOptions()))
				{
					//Get last transport offering in the journey
					final OriginDestinationOptionData lastOriginDestinationOption = reservationItinerary.getOriginDestinationOptions()
							.get(0);
					final TransportOfferingData lastTransportOffering = lastOriginDestinationOption.getTransportOfferings()
							.get(lastOriginDestinationOption.getTransportOfferings().size() - 1);

					datesToCheck.add(lastTransportOffering.getArrivalTime());
				}
			}
		}

		final Date now = timeService.getCurrentTime();

		for (final Date dateToCheck : datesToCheck)
		{
			if (dateToCheck.compareTo(now) >= 0)
			{
				return false;
			}
		}

		return true;
	}

	protected String getReservationNumber(final GlobalTravelReservationData myBooking)
	{
		if (myBooking.getReservationData() != null && StringUtils.isNotEmpty(myBooking.getReservationData().getCode()))
		{
			return myBooking.getReservationData().getCode();
		}

		if (myBooking.getAccommodationReservationData() != null
				&& StringUtils.isNotEmpty(myBooking.getAccommodationReservationData().getCode()))
		{
			return myBooking.getAccommodationReservationData().getCode();
		}

		return StringUtils.EMPTY;
	}

	protected Map<String, ImageData> getMediaForAccommodation(final List<GlobalTravelReservationData> globalTravelReservationDatas)
	{
		if (CollectionUtils.isEmpty(globalTravelReservationDatas))
		{
			return Collections.emptyMap();
		}
		final Map<String, ImageData> travelBookingsImagesMap = new HashMap<>();
		globalTravelReservationDatas.forEach(globalTravelReservationData -> {
			if (globalTravelReservationData.getReservationData() != null)
			{
				final ReservationData reservationData = globalTravelReservationData.getReservationData();
				reservationData.getReservationItems().stream()
						.filter(reservationItem -> reservationItem.getOriginDestinationRefNumber() == 0).forEach(reservationItem -> {
					travelBookingsImagesMap.put(reservationData.getCode(), travelImageFacade.getImageForArrivalTransportFacility(
							reservationItem.getReservationItinerary().getRoute().getDestination().getCode()));
				});
			}
			else
			{
				final AccommodationReservationData accommodationReservationData = globalTravelReservationData
						.getAccommodationReservationData();
				final String accommodationOfferingCode = accommodationReservationData.getAccommodationReference()
						.getAccommodationOfferingCode();

				travelBookingsImagesMap.put(accommodationReservationData.getCode(),
						travelImageFacade.getImageForAccommodationOfferingLocation(accommodationOfferingCode));
			}
		});
		return travelBookingsImagesMap;
	}

	protected Map<String, Map<String, Integer>> getAccommodationRoomNameMapping(
			final List<GlobalTravelReservationData> globalTravelReservationDatas)
	{
		if (CollectionUtils.isEmpty(globalTravelReservationDatas))
		{
			return Collections.emptyMap();
		}
		final Map<String, Map<String, Integer>> accommodationRoomStayMapping = new HashMap<>();
		globalTravelReservationDatas.stream()
				.filter(globalTravelReservationData -> globalTravelReservationData.getAccommodationReservationData() != null)
				.forEach(globalTravelReservationData -> {
					final AccommodationReservationData accommodationReservationData = globalTravelReservationData
							.getAccommodationReservationData();
					final Map<String, Integer> roomStayMapping = new HashMap<>();
					accommodationReservationData.getRoomStays().forEach(roomStay -> {
						final String roomTypeName = roomStay.getRoomTypes().get(0).getName();
						roomStayMapping.put(roomTypeName,
								roomStayMapping.get(roomTypeName) != null ? roomStayMapping.get(roomTypeName) + 1 : 1);
					});
					accommodationRoomStayMapping.put(accommodationReservationData.getCode(), roomStayMapping);
				});
		return accommodationRoomStayMapping;
	}
}
