ACC.optionbooking = {
    _autoloadTracc : [
    	     "bindOptionBookingDatePicker"
    	      ],
    bindOptionBookingDatePicker : function() {
        var startDate = new Date($("#creationDate").val());
        var endDate = new Date($("#departureDate").val());
        $(".optionbooking-datepicker").datepicker({minDate: startDate, maxDate: endDate});
    }

};
