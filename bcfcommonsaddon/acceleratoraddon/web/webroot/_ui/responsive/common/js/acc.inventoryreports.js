ACC.inventoryreports = {
    _autoloadTracc: ["initiate"],

    initiate : function() {
       if (!(typeof accommodationLevelDatasStr === "undefined")) {
            var accommodationLevelDatas = JSON.parse(accommodationLevelDatasStr);
            ACC.inventoryreports.populateRegionValues(accommodationLevelDatas,"accommodation");
            ACC.inventoryreports.bindElementEventCalls("accommodation");
            ACC.inventoryreports.bindInventoryReportDatePicker("accommodation");

            var activityLevelDatas=JSON.parse(activityLevelDatasStr);
            ACC.inventoryreports.populateRegionValues(activityLevelDatas,"activity");
            ACC.inventoryreports.bindElementEventCalls("activity");
            ACC.inventoryreports.bindInventoryReportDatePicker("activity");
        }
    },

    populateRegionValues : function(accommodationLevelDatas,selectorType) {
        var regionDatas = [];
        for ( var accommodationLevelDataIndex in accommodationLevelDatas) {
            var regionData = new Object();
            regionData.value = accommodationLevelDatas[accommodationLevelDataIndex].code;
            regionData.label = accommodationLevelDatas[accommodationLevelDataIndex].name;
            regionData.childLevelDatas = accommodationLevelDatas[accommodationLevelDataIndex].childLevelDatas;

            regionDatas.push(regionData);
        }
        $(".y_"+selectorType+"RegionSelector").autocomplete({
            source : regionDatas,
            minLength : 0,
            focus : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".y_"+selectorType+"Region").val(ui.item.value);
                ACC.inventoryreports.populateCityValues(ui.item.childLevelDatas,selectorType)
            }
        }).focus(function() {
            $(this).autocomplete('search', '')
        });
    },

    populateCityValues : function(accommodationLevelDatas,selectorType) {
        $(".y_"+selectorType+"CitySelector").val("");
        $(".y_"+selectorType+"City").val("");
        var cityDatas = [];
        for ( var accommodationLevelDataIndex in accommodationLevelDatas) {
            var cityData = new Object();
            cityData.value = accommodationLevelDatas[accommodationLevelDataIndex].code;
            cityData.label = accommodationLevelDatas[accommodationLevelDataIndex].name;
            cityData.childLevelDatas = accommodationLevelDatas[accommodationLevelDataIndex].childLevelDatas;

            cityDatas.push(cityData);
        }
        $(".y_"+selectorType+"CitySelector").autocomplete({
            source : cityDatas,
            minLength : 0,
            focus : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".y_"+selectorType+"City").val(ui.item.value);
                if("accommodation"==selectorType){
                    ACC.inventoryreports.populateHotelValues(ui.item.childLevelDatas,selectorType);
                } else if("activity"==selectorType){
                    ACC.inventoryreports.populateActivityValues(ui.item.childLevelDatas,selectorType)
                }
            }
        }).focus(function() {
            $(this).autocomplete('search', '')
        });
    },

    populateHotelValues : function(accommodationLevelDatas,selectorType) {
        $(".y_"+selectorType+"HotelSelector").val("");
        $(".y_"+selectorType+"Hotel").val("");
        var hotelDatas = [];
        for ( var accommodationLevelDataIndex in accommodationLevelDatas) {
            var hotelData = new Object();
            hotelData.value = accommodationLevelDatas[accommodationLevelDataIndex].code;
            hotelData.label = accommodationLevelDatas[accommodationLevelDataIndex].name;
            hotelData.childLevelDatas = accommodationLevelDatas[accommodationLevelDataIndex].childLevelDatas;

            hotelDatas.push(hotelData);
        }
        $(".y_"+selectorType+"HotelSelector").autocomplete({
            source : hotelDatas,
            minLength : 0,
            focus : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".y_"+selectorType+"Hotel").val(ui.item.value);
                ACC.inventoryreports.populateRoomValues(ui.item.childLevelDatas,selectorType)
            }
        }).focus(function() {
            $(this).autocomplete('search', '')
        });
    },

    populateRoomValues : function(accommodationLevelDatas,selectorType) {
        $(".y_"+selectorType+"RoomSelector").val("");
        $(".y_"+selectorType+"Room").val("");
        var roomDatas = [];
        for ( var accommodationLevelDataIndex in accommodationLevelDatas) {
            var roomData = new Object();
            roomData.value = accommodationLevelDatas[accommodationLevelDataIndex].code;
            roomData.label = accommodationLevelDatas[accommodationLevelDataIndex].name;
            roomData.childLevelDatas = accommodationLevelDatas[accommodationLevelDataIndex].childLevelDatas;

            roomDatas.push(roomData);
        }
        $(".y_"+selectorType+"RoomSelector").autocomplete({
            source : roomDatas,
            minLength : 0,
            focus : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".y_"+selectorType+"Room").val(ui.item.value);
            }
        }).focus(function() {
            $(this).autocomplete('search', '')
        });
    },

    populateActivityValues : function(accommodationLevelDatas,selectorType) {
        $(".y_"+selectorType+"Selector").val("");
        $(".y_"+selectorType).val("");
        var roomDatas = [];
        for ( var accommodationLevelDataIndex in accommodationLevelDatas) {
            var roomData = new Object();
            roomData.value = accommodationLevelDatas[accommodationLevelDataIndex].code;
            roomData.label = accommodationLevelDatas[accommodationLevelDataIndex].name;
            roomData.childLevelDatas = accommodationLevelDatas[accommodationLevelDataIndex].childLevelDatas;

            roomDatas.push(roomData);
        }
        $(".y_"+selectorType+"Selector").autocomplete({
            source : roomDatas,
            minLength : 0,
            focus : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select : function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".y_"+selectorType).val(ui.item.value);
            }
        }).focus(function() {
            $(this).autocomplete('search', '')
        });
    },

    bindElementEventCalls : function(selectorType) {
        $(".y_"+selectorType+"FindInventory").click(function(e) {
            e.preventDefault();
            $.when(ACC.inventoryreports.findVacationInventory($(".y_"+selectorType+"InventoryReportForm"))).then(
                function(response) {
                    var responseHtml=response[selectorType+"InventoryResponseDatas"];
                    $(".y_"+selectorType+"InventoryResponseHtml").html(responseHtml);
                });
        });

        $(".y_"+selectorType+"Reset").click(function(e) {
            e.preventDefault();
            $(".y_"+selectorType+"RegionSelector").val('');
            $(".y_"+selectorType+"CitySelector").val('');
            if(selectorType === 'accommodation') {
                $(".y_"+selectorType+"HotelSelector").val('');
                $(".y_"+selectorType+"RoomSelector").val('');
            }

            if(selectorType === 'activity') {
                $(".y_"+selectorType+"Selector").val('');
            }

            $(".y_"+selectorType+"InventoryResponseHtml").empty();
        });
    },

    bindInventoryReportDatePicker : function(selectorType) {
        $(".y_"+selectorType+"StartDate").datepicker({
            minDate : new Date(),
            dateFormat : 'dd/mm/yy',
            onSelect : function(selected) {
                $(".y_"+selectorType+"EndDate").val(selected);
                $(".y_"+selectorType+"EndDate").datepicker({
                    minDate : selected
                });
            }
        });
        $(".y_"+selectorType+"EndDate").datepicker({
            minDate : new Date(),
            dateFormat : 'dd/mm/yy'
        });
    },

    findVacationInventory : function(vacationInventoryReportForm) {
        return $.ajax({
            type: vacationInventoryReportForm.attr('method'),
            url: vacationInventoryReportForm.attr('action'),
            data: vacationInventoryReportForm.serialize()
        });
    }
}
