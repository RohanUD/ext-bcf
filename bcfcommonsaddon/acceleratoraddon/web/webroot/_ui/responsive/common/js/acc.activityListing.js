ACC.activityListing = {
    _autoloadTracc : [
            "displayPricePerPassengerAndSchedules",
            "bindActivitySearchCriteriaData",
            "bindPassengerSelector",
            "bindCustomAccordion",
            "bindTourTimeSelector",
            "bindActivityListingShowMore",
            "bindVacationActivityListingShowMore",
            "bindActivityDestinationFilter",
            "bindDealActivityAvailabilityChange"
        ],
    bindActivityDestinationFilter : function()
    {
        var Destinationfacet=$('#destination_list').find('.y_activitySearchCriteriaData:checkbox:checked').val();
        if(Destinationfacet != undefined && Destinationfacet != "undefined"){
        $('#destination').html(Destinationfacet+'<span class="fa fa-angle-down pull-right"></span>');
      }
    },
    bindTourTimeSelector : function()
    {
        $('body').on('change','.activity-selection-radio',function(){
            var parentObj=$(this).closest('.custom-accordion').siblings('.passenger-select-field');
            parentObj.removeClass('hidden');
            parentObj.find('.y_activitytime').html($(this).data('formatteddate'));
            $(this).closest('#addActivityToDealCartForm').find('input#selectedTime').val($(this).data('starttime'));
        });
    },
    bindPassengerSelector : function()
    {
        $('body').on('click','.activity-passenger-selector',function(){
        var accordionHeaderObj=$(this).closest('.custom-accordion').find('.custom-accordion-header');
        accordionHeaderObj.find('.custom-arrow').addClass('hidden');
        if(accordionHeaderObj.find('.fa-check').length==0)
        {
            accordionHeaderObj.append('<i class="fas fa-check pull-right"></i>');
            }
            var currentObj=$(this);
            setTimeout(function(){
                var changeIn=currentObj.parent().attr('data-controller');
                var currentValue=parseInt(currentObj.siblings('input').val());
                currentObj.closest('form').find('[data-id="'+changeIn+'"]').val(currentValue);

                var maxQuantityAllowed =  currentObj.closest('.input-btn-mp').find('.input-number').attr("max");
                var priceObj=currentObj.closest('form').find('[data-id="'+changeIn+'"]');
                var btnType = currentObj.data("type");

                //this is check for passenger selector on standalone activity deatils page
                if(priceObj.attr('data-price') != undefined){

                    var price=parseFloat(priceObj.attr('data-price').replace(/[^\d\.]/g, ''));
                    var currency='$';

                    var paxType=changeIn.split("_").pop().toLowerCase();
                    var displayText=changeIn.split("_").pop();
                    var totalPrice=price*currentValue;

                    if(/adult/i.test(displayText)){
                        displayText = "Adult";
                        paxType="adult";
                    }
                    else if(/child/i.test(displayText)){
                        displayText = "Child";
                        paxType="child"
                    }
                    else if(/youth/i.test(displayText)){
                        displayText = "Youth";
                        paxType="youth";
                    }
                    else if(/infant/i.test(displayText)){
                        displayText = "Infant";
                        paxType="infant";
                    }

                    var total=totalPrice;

                    //logic to add previous total as well based on the plus , minus buttom clicked
                    var previousTotalFieldValue = currentObj.closest('.main-div').find('.y_total').text();
                    if(previousTotalFieldValue != ""){
                        var total = parseFloat(currentObj.closest('.main-div').find('.y_total').text().replace(/[^\d\.]/g, ''));

                        if(btnType === "plus"){
                            total += price;
                        }else if(btnType === "minus"){
                            if(total>0){
                                total -= price;
                            }
                        }
                    }

                    currentObj.closest('.main-div').find('.passenger-select-field').removeClass('hidden');
                    currentObj.closest('.main-div').find('.y_'+paxType+'_selection').removeClass('hidden');
                    currentObj.closest('.main-div').find('.y_total').removeClass('hidden');
                    currentObj.closest('.main-div').find('.y_total').html('Total : '+currency+' '+total.toFixed(2));
                    currentObj.closest('.main-div').find('.y_'+paxType+'_selection').html(currentValue+' x '+displayText+' : '+currency+' '+totalPrice.toFixed(2));
                }

                if(btnType === "minus"){
                    if(currentValue==0){
                        currentObj.css("pointer-events", "none");
                    }else{
                        if(currentValue != maxQuantityAllowed){
                            currentObj.closest('.input-btn-mp').find('.activity-passenger-selector.bcf-icon-add').css("pointer-events", "auto");
                            currentObj.closest('.input-btn-mp').find('.activity-passenger-selector.bcf-icon-add').removeAttr("disabled");
                        }
                    }
                }
                if(btnType === "plus"){
                    if(currentValue == maxQuantityAllowed){
                        currentObj.css("pointer-events", "none");
                    }else{
                        if(currentValue != 0){
                            currentObj.closest('.input-btn-mp').find('.activity-passenger-selector.bcf-icon-remove').css("pointer-events", "auto");
                            currentObj.closest('.input-btn-mp').find('.activity-passenger-selector.bcf-icon-remove').removeAttr("disabled");
                        }
                    }
                }

            },100);
        });
    },
    bindCustomAccordion : function()
    {
        $('body').on('click','.manage-custom-accordion-header',function(){
            var contentObj=$(this).closest('.custom-accordion').find('.custom-accordion-content');
            if(contentObj.hasClass('hidden-obj'))
            {
		if($(this).find('.custom-arrow').length){
			$(this).find('.custom-arrow').find("i").addClass('fa-angle-up');
			$(this).find('.custom-arrow').find("i").removeClass('fa-angle-down');
		}
                $(this).addClass('active');
                contentObj.removeClass('hidden-obj');
                contentObj.slideDown();
            }
            else
            {
		if($(this).find('.custom-arrow').length){
			$(this).find('.custom-arrow').find("i").removeClass('fa-angle-up');
            $(this).find('.custom-arrow').find("i").addClass('fa-angle-down');
		}
                $(this).removeClass('active');
                contentObj.slideUp();
                contentObj.addClass('hidden-obj');
            }
        });
    },
    displayPricePerPassengerAndSchedules : function()
    {
      ACC.activityListing.datePickerActivityInit();
      ACC.activityListing.bindAddToCartForSelectedActivityForDeals();
      datepickerInit($(".datepicker5"));

        $('.y_price').on('click', function() {
            var productCode = $(this).find('.y_productCode').val();
            var changeActivity = $(this).find('.y_changeActivity').val();
            var changeActivityCode = $(this).find('.y_changeActivityCode').val();
            if(changeActivity == "" || changeActivity == 'undefined')
            {
                changeActivity = false;
            }
            if(changeActivityCode == "" || changeActivityCode == 'undefined')
            {
                changeActivityCode = "";
            }
            if($(this).hasClass('info-shown'))
            {
                $(this).find('button').removeClass('btn-outline-blue');
                $(this).find('button').addClass('btn-primary');
                $(this).find('button').html('<span class="btn-txt">View Details</span>');
                $(this).removeClass('info-shown');
                $('.y_pricePerPassenger'+'_'+productCode).addClass('hidden');
            }
            else
            {
                $(this).find('button').removeClass('btn-primary');
                $(this).find('button').addClass('btn-outline-blue');
                $(this).find('button').html('Close');
                $('.y_pricePerPassenger'+'_'+productCode).removeClass('hidden');
                $(this).addClass('info-shown');
                $.when(ACC.services.showPricesAndSchedules(productCode, changeActivity, changeActivityCode)).then(
                        function(response) {
                            $('.y_pricePerPassenger'+'_'+productCode).html(response.pricePerPassengerAndSchedules);
                            ACC.activityListing.datePickerActivityInit();
                            ACC.activityListing.bindAddToCartForSelectedActivityForDeals();
                            datepickerInit($(".datepicker5"));
                            var dt = new Date();
                            $(".current-year-custom").html(dt.getFullYear());
                            $(".current-year-custom").removeClass(".current-year-custom");
                            $('.y_pricePerPassenger'+'_'+productCode).find(".activity-tab").attr("href","#"+productCode)
                            $('.y_pricePerPassenger'+'_'+productCode).find(".activity-tab-content").attr("id",productCode)
                        });
                }
        });
    },

    bindAddToCartForSelectedActivityForDeals : function()
    {
        $('.add-activity-form button').on('click', function(){
           // var result = $(this).closest("form").click();

            $('.error-message').remove('');
                var errors=[];
                var formElement = $(this).closest("#addActivityToDealCartForm");
                if(!isPaxSelected(formElement,'adult') && !isPaxSelected(formElement,'youth') && !isPaxSelected(formElement,'child') && !isPaxSelected(formElement,'infant'))
                {
                    formElement.find('.manage-custom-accordion-header:first').after('<span class="error-message fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_passengers')+'</span>');
                    errors.push('y_spAdult');
                }

                if(formElement.find(".passenger-select-field .y_activitytime").html()=='')
                {
                    formElement.find('.manage-custom-accordion-header:last').after('<span class="error-message fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_checkin_date')+'</span>');
                    errors.push('y_activityCheckInDatePicker');
                }

                if(formElement.find(".datepicker-input").val()=='')
                {
                    formElement.find('.vacation-calen-box').after('<span class="error-message fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_checkin_date')+'</span>');
                    errors.push('y_activityCheckInDatePicker');
                }
                formElement.find('#'+errors[0]).focus();
                if(errors.length!=0) {
                    return false;
                }

                var proceedWithAddToCart = false;
                var addActivityToCartForm = $(this).closest("#addActivityToDealCartForm");
                $.when(ACC.services.checkDealActivityAvailability(addActivityToCartForm)).always(function(response, textStatus, jqXHR){
                    console.log("response:"+response)
                    if(response === "ON_REQUEST") {
                        $("#y_activityAvailabilityChangeModal").show();
                    } else {
                        proceedWithAddToCart=true;
                    }
                });

                //hack for the issue where class cboxElement got attched to the modal which causes an arbitrary button to appear on screen
                if($("#y_activityAvailabilityChangeModal").hasClass("cboxElement")) {
                    $("#y_activityAvailabilityChangeModal").removeClass("cboxElement");
                }

                if(!proceedWithAddToCart) {
                    return false;
                }
        });
    },

    datePickerActivityInit : function(){
        $(".y_activityCheckInDatePicker").datepicker({
        minDate: new Date(),
        dateFormat:'M dd yy'
        });

        if($('.y_activityCheckInDatePicker').length)
                {
                    $('.y_activityCheckInDatePicker').change(function()
                    {
                    $(this).attr('value', $('.y_activityCheckInDatePicker').val());
                    });
                }
    },

    bindActivitySearchCriteriaData: function() {
        $(".y_activitySearchCriteriaData.y_activitySearchSortSelect").on('change', function () {
            ACC.activityListing.submitForActivitiesSearch();
        });
        $(".y_activitySearchCriteriaData.y_activityFacetCheckbox").on('change', function () {
            var currentElement = $(this);
            $(".y_activitySearchCriteriaData.y_activityFacetCheckbox:checked").each(function(index,obj){
                if($(this).attr("id") != currentElement.attr("id")){
                    $(this).prop('checked', false);
                }
            });
            ACC.activityListing.submitForActivitiesSearch();
        });
    },

    submitForActivitiesSearch: function() {
        var sortBy=$(".y_activitySearchCriteriaData.y_activitySearchSortSelect").val();

        //always start from the first page if search facet has changed
        var pageNumber = 1;
        var activityCategoryTypes=[];
        $(".y_activitySearchCriteriaData.y_activityFacetCheckbox.activityCategoryTypes").each(function(index, checkbox) {
            if($(checkbox).is(":checked")) {
                activityCategoryTypes.push($(this).val());
            }
        });
        var destination="";
        $(".y_activitySearchCriteriaData.y_activityFacetCheckbox.destination").each(function(index, checkbox) {
            if($(checkbox).is(":checked")) {
                destination=$(this).val();
            }
        });

        var changeActivityParams = ACC.activityListing.getChangeActivityParameters();

        $(".y_activityFinderForm input[name=currentPage]").val(pageNumber);
        $(".y_activityFinderForm input[name=activityCategoryTypes]").val(activityCategoryTypes);
        $(".y_activityFinderForm input[name=sort]").val(sortBy);
        $(".y_activityFinderForm input[name=destination]").val(destination);
        $(".y_activityFinderForm input[name=changeActivity]").val(changeActivityParams.isChangeActivity);
        $(".y_activityFinderForm input[name=changeActivityCode]").val(changeActivityParams.changeActivityCode);
        $(".y_activityFinderForm").submit();
    },

        getChangeActivityParameters: function(){
            var pathName = window.location.href;
            var isChangeActivity = false,changeActivityCode="",changeActivityDate,changeActivityTime;
            if(pathName.indexOf("changeActivity") >= 0)
            {

                pathName.split("&").forEach(function(pair){
                     if (pair === "") return;
                     var parts = pair.split("=");
                     if(parts[0] === "changeActivity"){
                        isChangeActivity = decodeURIComponent(parts[1].replace(/\+/g, "")) === "true";
                     }
                     if(parts[0] === "changeActivityCode"){
                        changeActivityCode = decodeURIComponent(parts[1].replace(/\+/g, " "));
                     }
                     if(parts[0] === "date"){
                        changeActivityDate = decodeURIComponent(parts[1].replace(/\+/g, " "));
                     }
                     if(parts[0] === "time"){
                        changeActivityTime = decodeURIComponent(parts[1].replace(/\+/g, " "));
                     }
                })
            }
            return { isChangeActivity : isChangeActivity , changeActivityCode : changeActivityCode , changeActivityDate : changeActivityDate , changeActivityTime : changeActivityTime};
        },

        bindDealActivityAvailabilityChange : function() {

            $("#y_activityAvailabilityChangeModal .close,#y_activityAvailabilityChangeModal .btn-secondary").on("click",function(){
                 $('#y_activityAvailabilityChangeModal').modal('hide');
            })

            $("#y_activityAvailabilityChangeModal #y_proceedAddToCart").on("click",function(){
                 $('#y_activityAvailabilityChangeModal').modal('hide');
                 $("#addActivityToDealCartForm").submit();
            });
        }
}


function isPaxSelected(formElement,paxType){
    if(formElement.find('[id^=y_][id$='+paxType+']').length > 0){
       return (formElement.find('[id^=y_][id$='+paxType+']').val()>0)
    }
    return false;
}
