ACC.schedule = {
	_autoloadTracc: [
		"LabelMesagesInit",
		"calculateAndDisplayRoute(directionsService, map,originLatitude,originLongitude,destinationLatitute,destinationLongitude,originNameDisplay,destinationNameDisplay,allDestinationNames,listofOrigin,originCodeDisplay)",
		"mapSchedulesLanding",
		"bindSeasonalSchedules",
		"mapViewTabClick",
		"bindRouteRegionSelection"
	],
	activeWindow: null,
	displayedMap: null,
	mapStyle: [
		{
			"featureType": "administrative",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "administrative",
			"elementType": "labels.text",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "administrative.country",
			"elementType": "geometry.stroke",
			"stylers": [
				{
					"color": "#696969"
				}
			]
		},
		{
			"featureType": "administrative.country",
			"elementType": "labels.text",
			"stylers": [
				{
					"color": "#696969"
				}
			]
		},
		{
			"featureType": "administrative.province",
			"elementType": "geometry.stroke",
			"stylers": [
				{
					"color": "#696969"
				}
			]
		},
		{
			"featureType": "administrative.province",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#696969"
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "administrative.locality",
			"elementType": "labels.text",
			"stylers": [
				{
					"color": "#003366"
				}
			]
		},
		{
			"featureType": "administrative.locality",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#000000"
				}
			]
		},
		{
			"featureType": "administrative.locality",
			"elementType": "labels.icon",
			"stylers": [
				{
					"color": "#000000"
				},
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "administrative.neighborhood",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "administrative.neighborhood",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "administrative.land_parcel",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#96c6cd"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "labels.text",
			"stylers": [
				{
					"color": "#0f7aa7"
				},
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "landscape.natural",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#d82927"
				},
				{
					"saturation": "-80"
				},
				{
					"lightness": "20"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#d82927"
				},
				{
					"saturation": "-80"
				},
				{
					"lightness": "20"
				},
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#6bb2b9"
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "transit.line",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit.line",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#0079a6"
				},
				{
					"weight": 4
				}
			]
		},
		{
			"featureType": "transit.station.airport",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#c7e3ef"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "labels",
			"stylers": [
				{
					"color": "#666666"
				},
				{
					"visibility": "simplified"
				}
			]
		}
	],
	LabelMesagesInit : function(){
	      ACC.MapMessages = new validationMessages("LabelMesagesInit");
	      ACC.MapMessages.getMessages("error");
	      ACC.MapMessages.getMessages("label");
	    },
	addGoogleMapsApi: function (callback) {
		var callback = "ACC.schedule.loadGoogleMap";
		if (callback != undefined && $(".js-googleMapsApi").length == 0) {
			$('head').append('<script async defer class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + $('#showmap').data('googleapi') + '&callback=' + callback + '"></script>');
		} else if (callback != undefined) {
			eval(callback + "()");
		}
	},

	loadGoogleMap: function () {
		var map = new google.maps.Map(document.getElementById('showmap'), {
			mapTypeControl: false,
			streetViewControl: false,
			styles: ACC.schedule.mapStyle

		});
		var listOfTravelRouteCount = $('#listOfTravelRouteCount').attr('value');
		var processedFile = [];
		var checkDuplicateFile = [];
		var processedTerminal = [];
        var infoContentMap = {};
        var mapKmlFileMap = {};
        var mapDiv = $('.y_propertyMap');
        var url = mapDiv.data('schedulesurl')
        var originToDestinationMappingCount = $('#originToDestinationMappingCount').attr('value');
        for (var j = 0; j < originToDestinationMappingCount; j++) {

            var routeOriginKey ="#routeOrigin"+j;
            var routeOriginFilteredNameDisplay = $(routeOriginKey).data("originfilteredname");
            var routeOriginFilteredCodeDisplay = $(routeOriginKey).data("originfilteredcode");
            var routeOriginNameDisplay = $(routeOriginKey).data("originname");

            var destinationCountKey ="#destinationCount"+j;
            var destinationCountDisplay = $(destinationCountKey).data("destinationcount");

            var infoContent = ACC.viewScheduleFrom + "<br><b>" + routeOriginNameDisplay + "</b><br>" + ACC.viewScheduleTo +": " + "<br>";

            for (var k = 0; k < destinationCountDisplay; k++) {

                var routeDestinationKey ="#routeDestination"+j+k;
                var routeDestinationFilteredNameDisplay = $(routeDestinationKey).data("destinationfilteredname");
                var routeDestinationFilteredCodeDisplay = $(routeDestinationKey).data("destinationfilteredcode");
                var routeDestinationNameDisplay = $(routeDestinationKey).data("destinationname");

                var createURL = url + routeOriginFilteredNameDisplay + "-" + routeDestinationFilteredNameDisplay + "/" +
                routeOriginFilteredCodeDisplay + "-" + routeDestinationFilteredCodeDisplay;
                infoContent = infoContent + '<a href="' + createURL + '">' + routeDestinationNameDisplay + '</a>' + "<br>";

                var terminalCode = routeOriginFilteredCodeDisplay;
                infoContentMap[terminalCode] = infoContent;

            }
            console.log(terminalCode+" its infoContent " + infoContent);
        }
		for (var i = 0; i < listOfTravelRouteCount; i++) {
           var routeOriginDestinationKey ="#routeOriginDestination"+i;
           var mapkmlfile=$(routeOriginDestinationKey).data("mapkmlfile");
           var mapkmlInternalfileName=$(routeOriginDestinationKey).data("mapkmlinternalfilename");
           if(mapkmlfile != undefined)
           {
			        var mapkmlInternalfileNameSeparator = mapkmlInternalfileName.split("-");
                    var duplicateMapkmlInternalfileName = mapkmlInternalfileNameSeparator[1] + "-" + mapkmlInternalfileNameSeparator[0];
                    if(!checkDuplicateFile.includes(duplicateMapkmlInternalfileName))
                    {
                    checkDuplicateFile.push(mapkmlInternalfileName);
					var kmlFilePathName = mapkmlfile;
					var lineSymbol = {
						path: 'M 0,-0.1 0,0.5',
						strokeOpacity: 1,
						scale: 4
					};

                    // Comment the below line, if you are testing in local env
                    var absoluteFileName = mapkmlfile;

                    // And Uncomment the below line to work in local env.
                    //var absoluteFileName = mapkmlfile.substring(0, mapkmlfile.indexOf("?"));
					mapKmlFileMap[absoluteFileName] = mapkmlInternalfileName;

                    var listenerDebounce;
                    var bounds = new google.maps.LatLngBounds();
					var createMarker = function (placemark, doc) {
						var travelRoute = mapKmlFileMap[placemark.styleBaseUrl];
						var sameFileCount =1;
						if(!processedFile.includes(travelRoute)){
						    processedFile.push(travelRoute);
						}else{
						    sameFileCount =2;
						}
						var travelRouteOriginAndDestinationSeparator = travelRoute.split("-");
						var travelRouteOrigin = travelRouteOriginAndDestinationSeparator[0];
						var travelRouteDestination = travelRouteOriginAndDestinationSeparator[1];
						var isIncluded = true;
						var infoWindowContent;
						if (!processedTerminal.includes(travelRouteOrigin) && sameFileCount == 1) {
							processedTerminal.push(travelRouteOrigin);
							infoWindowContent = infoContentMap[travelRouteOrigin];
							isIncluded = false;
						}else if(!processedTerminal.includes(travelRouteDestination) && sameFileCount == 2){
						    processedTerminal.push(travelRouteDestination);
						    infoWindowContent = infoContentMap[travelRouteDestination];
                            isIncluded = false;
						}
						var marker=null;
						if (!isIncluded) {
							var markerOptions = geoXML3.combineOptions(geoXml.options.markerOptions, {
								map: geoXml.options.map,
								position: new google.maps.LatLng(placemark.Point.coordinates[0].lat, placemark.Point.coordinates[0].lng)
							});

							// Create the marker on the map
							marker = new google.maps.Marker(markerOptions);

							// Set up and create the infowindow if it is not suppressed
							if (!geoXml.options.suppressInfoWindows && !isIncluded) {
								var infoWindowOptions = geoXML3.combineOptions(geoXml.options.infoWindowOptions, {
									content: infoWindowContent
								});
								if (geoXml.options.infoWindow) {
									marker.infoWindow = geoXml.options.infoWindow;
								} else {
									marker.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
								}
								marker.infoWindowOptions = infoWindowOptions;
								bounds.extend(marker.position);
                                var listener = google.maps.event.addListener(map, "idle", function () {
                                    clearTimeout(listenerDebounce);
                                    listenerDebounce = setTimeout(map.fitBounds(bounds, 80), 250);
                                    google.maps.event.removeListener(listener);
                                });
								// Infowindow-opening event handler
								google.maps.event.addListener(marker, 'click', function () {
									//								this.infoWindow.close();
									marker.infoWindow.setOptions(this.infoWindowOptions);
									this.infoWindow.open(this.map, this);

								});
								google.maps.event.addListener(map, 'click', function (event) {
									marker.infoWindow.close();
								});
							}
						    placemark.marker = marker;
						}
						return marker;
					};

					var geoXml = new geoXML3.parser({
                        map: map,
						singleInfoWindow: true,
						createMarker: createMarker,
						markerOptions: {
							icon: {
								url: 'https://snazzy-maps-cdn.azureedge.net/assets/marker-c5ce3bc4-d268-4b8a-9941-285dc369ed54.png',
								scaledSize: new google.maps.Size(
									24,
									24),
								size: new google.maps.Size(
									24,
									24),
								anchor: new google.maps.Point(
									12,
									24)
							}
						},
						polylineOptions: {
							strokeOpacity: 0,
							strokeColor: '#0079a6',
							icons: [{
								icon: lineSymbol,
								offset: '0',
								repeat: '10px'
                            }],
                            clickable:false,
                            strokeWeight:4
						},
					});
					geoXml.parse(kmlFilePathName);

          }
          }
        }
	},


	mapSchedulesLanding: function () {
		$('#schedulesLandingMap').on("click", function () {
			$("#schedulesMap").show();
			$("#schedulesList").hide();
		});

		$('#schedulesLandingList').on("click", function () {
			$("#schedulesMap").hide();
			$("#schedulesList").show();
		});
	},
	mapViewTabClick: function () {
		$('#mapviewtab').on("click", function () {});
	},

	bindSeasonalSchedules: function () {
		$('#seasonalScheduleDate').on('change', function (e) {
			$("#seasonalSchedulesForm").submit();
		});

		$('#viewSeasonalSchedule').on("click", function () {
			var route = $("#seasonalScheduleSelectRoute").val();
			if (route != 'default') {
				var data = route.split("-");
				$("#departurePort").val(data[0]);
				$("#arrivalPort").val(data[1]);
				$("#seasonalSchedulesFomByRoute").submit();
			}
		});
	},
	bindRouteRegionSelection: function () {

		$('#y_routeRegionSelection').prepend('<option value="" disabled selected style="display: none;">'+ACC.MapMessages.message("text.choose.a.region.select")+'</option>');
		$('#y_routeRegionSelection').on("change", function () {
			var routeRegions = $("#y_routeRegionSelection option:selected").val();
			//            alert("Valid form submitted! "+routeRegions);
			$('.bg-image-map-text').html('');
			$.when(ACC.services.submitSelectedRouteRegionForm(routeRegions)).then(
				function (response) {
					$("#showMapResponse").replaceWith(response.routeRegionMap);
					ACC.schedule.addGoogleMapsApi();
				});
		});
	}
}
