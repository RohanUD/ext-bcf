ACC.packagedetails = {
    _autoloadTracc: [
        ["bindOutboundFerrySelectionButton", $(".y_packageFerrySelectionPage").length !== 0],
        ["bindInboundFerrySelectionButton", $(".y_packageFerrySelectionPage").length !== 0],
        "showNoAvailabilityModal",
        "bindDealDepartureDatePicker",
        "bindShowHideShowRoomOptions",
        "bindAccommodationChange",
        // "addPackageToCartOnLoad",
        // "amendPackageToCartOnLoad",
        "bindAddDealToCartButton",
        "bindRemoveDealFromCartButton",
        "bindAddPackageToCartButton",
        "bindAddAccommodationToCartButton",
        "editVacationAccordion",
        "datePickerInit",
        "roomQuantitySelector",
        "bindGuestQuanitySelector",
        "bindEditPackageButton",
        "collapseEditDiv",
        "bindBackToResultsButton",
        "bindRoomEditButton",
    ],

        datePickerInit : function(){
            $("#y_DatePickerCheckIn").datepicker({
                                                     minDate: new Date(),
                                                     maxDate: '+1y',
                                                     dateFormat: 'dd/mm/yy'
                                                 });
           $("#y_DatePickerCheckOut").datepicker({
                                                  minDate: new Date(),
                                                  maxDate: '+1y',
                                                  dateFormat: 'dd/mm/yy'
                                              });
        },
     editVacationAccordion: function () {
      $(function () {
         	$(".edit #accordion").accordion({ header: "h3", collapsible: true, active: false });
         });
     },

     collapseEditDiv : function(){
              $(".collapseEditClose").click(function(){
                  $(".checkVacationInAndCheckOutDiv").slideToggle("slow",function(){

                  });
                  $("#collapseEdit").toggle();
                  $("#collapseClose").toggle();
              });
     },
     bindRoomEditButton: function(){
        $(document).on("click",".roomNotAvailable",function(){
            var pageURL = window.location.href;
            if(pageURL.indexOf("/vacations/hotels") > 0){
                $(window).scrollTop($('.package-hotel-details-tabs').offset().top - 100);
                return;
            }

            //this is package booking flow
            if($('#y_travelFinderForm .package_edit').length > 0){
                $(window).scrollTop($('#y_travelFinderForm .package_edit').offset().top - 100);

                if(!$('.checkVacationInAndCheckOutDiv').is(':visible'))
                {
                    $(".checkVacationInAndCheckOutDiv").slideToggle("slow",function(){
                                  });
                    $("#collapseEdit").toggle();
                    $("#collapseClose").toggle();

                }
            }

            // this is for hotel only flow
            if($('#y_accommodationEditForm .package_edit').length > 0){
                $(window).scrollTop($('#y_accommodationEditForm .package_edit').offset().top - 100);

                if(!$('.checkVacationInAndCheckOutDiv').is(':visible'))
                {
                    $(".checkVacationInAndCheckOutDiv").slideToggle("slow",function(){
                                  });
                    $("#collapseEdit").toggle();
                    $("#collapseClose").toggle();

                }
            }
        });
     },
    addTransportBundleToCartResult: true,
    addAccommodationToCartResult: true,
    addDealToCartForm: null,

    bindEditPackageButton : function() {
    		$(document).on('click', '#editPackageButton', function() {
    		var editPackageForm = $(this).parents(".editPackageTab").find("#y_editPackageForm");
    		var editcheckIn =$("#editCheckIn").val();
    		var editcheckOut=$("#editCheckOut").val();
    		ACC.packagedetails.populateFormFields(editPackageForm,editcheckIn,editcheckOut);
    		$.when(ACC.services.editPackageAjax(editPackageForm)).then(
                	    function(data) {
                            $("#editPackageData").replaceWith(data.htmlContent);
                             ACC.packagedetails.bindAddDealToCartButton();
                             ACC.packagedetails.bindRemoveDealFromCartButton();
                             ACC.packagedetails.bindAddPackageToCartButton();
                             ACC.packagedetails.editVacationAccordion();
                             ACC.packagedetails.datePickerInit();
                             ACC.packagedetails.roomQuantitySelector();
                             ACC.packagedetails.bindGuestQuanitySelector();
                             ACC.packagedetails.collapseEditDiv();
                             $(".edit #accordion").accordion({ header: "h3", collapsible: true, active: false });
                	    });
    		});

    		},
    		
    		populateFormFields : function($form, editcheckIn, editcheckOut) {
    		    $form.find('#y_DatePickerCheckIn').val('');
    			$form.find('#y_DatePickerCheckIn').val(editcheckIn);
    			$form.find('#y_DatePickerCheckOut').val('');
    			$form.find('#y_DatePickerCheckOut').val(editcheckOut);
    		},


 roomQuantitySelector : function() {
			$('.y_roomQtySelectorPlus').click(
				function(e) {
					e.preventDefault();
					var input = $(this).parents(".roomSelector").find("#y_roomQuantity");
					var quantity = parseInt($(input).val());
					var maxAllowed =  parseInt($("#accommodationsQuantity").text());
					if (quantity < maxAllowed) {
						quantity = quantity + 1;
						$(input).val(quantity);
					}
				});

			$('.y_roomQtySelectorMinus').click(function(e) {
				e.preventDefault();
				var input = $(this).parents(".roomSelector").find("#y_roomQuantity");
				var quantity = parseInt($(input).val());
				if (quantity > 0) {
					quantity = quantity - 1;
					$(input).val(quantity);
				}
			});
	},

	 bindGuestQuanitySelector : function() {
    			$('.y_guestQtySelectorPlus').click(
    				function(e) {
    					e.preventDefault();
    					var input = $(this).parents(".y_guestQtySelector").find("#y_guestQuantity");
    					var quantity = parseInt($(input).val());
    					var currentCount =  ACC.packagedetails.currentGuestCount();
    					var maxAllowed =  parseInt($("#maxPassengerCount").text());
    					if (currentCount < maxAllowed) {
    						quantity = quantity + 1;
    						$(input).val(quantity);
    					}
    				});

    			$('.y_guestQtySelectorMinus').click(function(e) {
    				e.preventDefault();
    				var input = $(this).parents(".y_guestQtySelector").find("#y_guestQuantity");
    				var quantity = parseInt($(input).val());
    				if (quantity > 0) {
    					quantity = quantity - 1;
    					$(input).val(quantity);
    				}
    			});
    	},

    	currentGuestCount : function() {
        		var total=0;
        		$('[id=y_guestQuantity]').each(function() {
        			var quantity = parseInt($(this).val());
        	        if (!isNaN(quantity)) {
        	            total += quantity;
        	        }
        		});
        	return total;
        	},

    showNoAvailabilityModal: function() {
        var noPackageAvailability = $("#y_noPackageAvailability");
        if (noPackageAvailability) {
            var availabilityValue = noPackageAvailability.val();
            if (availabilityValue == "show") {
                $("#y_noPackageAvailabilityModal").modal();
            }
        }
    },

    bindAddDealToCartButton: function() {
        $(document).on('click', '#y_addDealToCart', function() {
            $.when(ACC.services.addDealBundleToCartAjax($("#addDealBundleToCartForm"))).then(function(response) {
                var result = response;
                if (!result.valid) {
                    var output = [];
                    if (result.errors) {
                        result.errors.forEach(function(error) {
                            output.push("<p>" + error + "</p>");
                        });
                    }
                    $("#y_addPackageToCartErrorModal").find(".y_addPackageToCartErrorBody").html(output.join(""));
                    $('#y_processingModal').modal("hide");
                    $("#y_addPackageToCartErrorModal").modal();
                } else {
                    $("#y_addPackageToCartSuccessModal").modal();
                }
                return;
            });
        });
    },

    bindRemoveDealFromCartButton: function() {
        $(document).on('click', '#y_removeDealFromCart', function() {
            $.when(ACC.services.removeDealBundleFromCartAjax($('#y_removeDealFromCart').attr('dealId'), $('#y_removeDealFromCart').attr('departureDate'))).then(function(response) {
                var result = response;
                if (!result.valid) {
                    var output = [];
                    if (result.errors) {
                        result.errors.forEach(function(error) {
                            output.push("<p>" + error + "</p>");
                        });
                    }
                    $("#y_addPackageToCartErrorModal").find(".y_addPackageToCartErrorBody").html(output.join(""));
                    $('#y_processingModal').modal("hide");
                    $("#y_addPackageToCartErrorModal").modal();
                } else {
                    $("#y_addPackageToCartSuccessModal").modal();
                }
                return;
            });
        });

    },

    bindDealDepartureDatePicker: function() {

        $('#dealDatePickerDeparting').on("keyup", function() {
            ACC.dealselection.dealValidDates = [];
        });
        $('#dealDatePickerDeparting').datepicker({
            minDate: new Date(),
            beforeShowDay: function(date) {
                if (ACC.dealselection.dealValidDates == '' && date.getDate() == 1) {
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    ACC.dealselection.getDealValidDates(month, year, this.getAttribute('data-dealstartdatepattern'));
                } else if (date.getDate() == 1 && !(ACC.dealselection.dealValidDates == '')) {
                    ACC.dealselection.dealValidDates = [];
                }

                var formattedDate = jQuery.datepicker.formatDate('dd/mm/yy', date);
                return [$.inArray(formattedDate, ACC.dealselection.dealValidDates) >= 0, ""];
            },

            onChangeMonthYear: function(year, month) {
                ACC.dealselection.dealValidDates = [];
            },

            onClose: function(selectedDate) {
                ACC.packagedetails.validateSelectedDepartureDate(selectedDate,
                    this.getAttribute('data-dealbundletemplateid'));
                ACC.dealselection.dealValidDates = [];
            }
        });

    },

    validateSelectedDepartureDate: function(dealSelectedDepartureDate, dealbundletemplateid) {
        $.when(ACC.services.validateSelectedDepartureDate(dealSelectedDepartureDate, dealbundletemplateid)).then(function(response) {
            if (response.dealChangeDateValidationError) {
                $('#y_dealDepartureError').html(response.dealChangeDateValidationError);
                $('#y_dealDepartureError').parent().show();
                $('#dealDatePickerReturning').val('');
                $('.y_dealUpdateDate').attr('disabled', 'disabled');
            } else {
                $('#y_dealUpdateDates').html(response.dealChangeDateHtmlContent);
                ACC.packagedetails.bindDealDepartureDatePicker();
            }
        });
    },

    bindShowHideShowRoomOptions: function() {

        var buttonParent = $('.y_roomOptionsCollapse');
        var roomOptionsCollapse = $('#roomOptionsCollapse');

        roomOptionsCollapse.on('shown.bs.collapse', function() {
            buttonParent.find('.show-text').addClass('hidden');
            buttonParent.find('.hide-text').removeClass('hidden');
        });

        roomOptionsCollapse.on('hidden.bs.collapse', function() {
            buttonParent.find('.show-text').removeClass('hidden');
            buttonParent.find('.hide-text').addClass('hidden');
        });
    },

    bindAccommodationChange: function() {
        var previouslySelectedOptionId;
        $(".y_changeAccommodationButton").on("focus", function() {
            previouslySelectedOptionId = $(document).find('.y_changeAccommodationButton:checked').prop("id");
        }).on("change", function() {
            $('#y_processingModal').modal({
                backdrop: 'static',
                keyboard: false
            });

            ACC.packagedetails.buildPackageAccommodationAddToCartForm(this, "/cart/accommodation/package-change");
        });
    },

    amendPackageToCartOnLoad: function() {
        if ($("#y_amendPackageDetailsPage").is('input') && $("#y_amendPackageDetailsPage").val() == 'true') {

            if ($("#y_packageAccommodationAddToCartForm").length !== 0) {
                // reveal spinner inside reservation component
                $('.y_spinner').removeClass('hidden');
                // trigger package build message
                $('#package-build-modal').modal();
                // disable buttons on the page
                ACC.packagedetails.disableButtons();

                setTimeout(function() {
                    // remove package build message
                    ACC.packagedetails.hidePackageBuildModal();
                }, 2500);
            }

            jQuery(window).load(function() {
                if ($("#y_packageAccommodationAddToCartForm").length === 0) {
                    ACC.packagedetails.enableButtons();
                    return false;
                }

                $.when(ACC.packagedetails.addAccommodationToCart()).then(function() {
                    if (!addAccommodationToCartResult) {
                        $('.y_spinner').addClass('hidden');
                        // hide spinner in reservation
                        $("#y_addPackageToCartErrorModal").modal();
                    }

                    ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
                    ACC.reservation.refreshAccommodationSummaryComponent($("#y_accommodationSummaryComponentId").val());

                    $('.y_spinner').addClass('hidden');
                    ACC.packagedetails.hidePackageBuildModal();
                    ACC.packagedetails.enableButtons();
                });
            });
        }
    },

    addPackageToCartOnLoad: function() {

            if ($(".y_packageAddBundleToCartForm").length !== 0 && $("#y_packageAccommodationAddToCartForm").length !== 0) {

                // reveal spinner inside reservation component
                $('.y_spinner').removeClass('hidden');
                // trigger package build message
                $('#package-build-modal').modal();
                // disable buttons on the page
                ACC.packagedetails.disableButtons();

                setTimeout(function() {
                    // remove package build message
                    ACC.packagedetails.hidePackageBuildModal();
                }, 2500);
            }

            $.when(ACC.packagedetails.addAccommodationToCart()).then(function() {

                if (!ACC.packagedetails.addAccommodationToCartResult) {
                    $('.y_spinner').addClass('hidden');
                    // hide spinner in reservation

                    $("#y_addPackageToCartErrorModal").modal();
                } else {
                            var changeVar=$("#changeVar").val();
                            ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
                            ACC.reservation.refreshAccommodationSummaryComponent($("#y_accommodationSummaryComponentId").val());
                            ACC.reservation.refreshTransportSummaryComponent($("#y_transportSummaryComponentId").val());

                            $('.y_spinner').addClass('hidden');
                            ACC.packagedetails.hidePackageBuildModal();
                            ACC.packagedetails.enableButtons();
                            if(changeVar=='true'){
                               window.location.href= ACC.config.contextPath+"/cart/summary";

                            }else{
                            var controller="/package-ferry-passenger-info";
                            if ($('#availabilityStatus').val() == "ON_REQUEST") {
                                controller = controller+"?availabilityStatus=ON_REQUEST"
                            }
                            }
                            window.location.href= ACC.config.contextPath+controller;



                }
            });
    },

    addAccommodationToCartOnLoad: function() {

                if ($("#y_packageAccommodationAddToCartForm").length !== 0) {
                    // reveal spinner inside reservation component
                    $('.y_spinner').removeClass('hidden');
                    // trigger package build message
                    $('#package-build-modal').modal();
                    // disable buttons on the page
                    ACC.packagedetails.disableButtons();

                    setTimeout(function() {
                        // remove package build message
                        ACC.packagedetails.hidePackageBuildModal();
                    }, 2500);
                }


                $.when(ACC.packagedetails.addAccommodationToCart()).then(function() {
                    if (!ACC.packagedetails.addAccommodationToCartResult) {
                        $('.y_spinner').addClass('hidden');
                        // hide spinner in reservation
                        $("#y_addPackageToCartErrorModal").modal();
                    } else {

                        var changeVar=$("#changeVar").val();
                        ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
                        ACC.reservation.refreshAccommodationSummaryComponent($("#y_accommodationSummaryComponentId").val());
                        ACC.reservation.refreshTransportSummaryComponent($("#y_transportSummaryComponentId").val());

                        $('.y_spinner').addClass('hidden');
                        ACC.packagedetails.hidePackageBuildModal();
                        ACC.packagedetails.enableButtons();
                        if(changeVar=='true'){
                            window.location.href= ACC.config.contextPath+"/cart/summary";
                        }else{

                            var controller="/package-ferry-passenger-info";
                            if ($('#availabilityStatus').val() == "ON_REQUEST") {
                                controller = controller+"?availabilityStatus=ON_REQUEST"
                            }
                            window.location.href= ACC.config.contextPath+controller;
                        }
                    }
                });


        },

    addTransportBundleToCart: function() {
        var dfd = $.Deferred();

        var packageAddBundleToCartFormList = $(".y_packageAddBundleToCartForm");
        ACC.packagedetails.addTransportBundleToCartResult = true;
        $.when(ACC.packagedetails.addTransportBundleEntry(packageAddBundleToCartFormList, 0)).then(function() {
            dfd.resolve();
        });
        return dfd.promise();
    },

    addTransportBundleEntry: function(formList, index) {
        if (index == (formList.length - 1)) {
            return $.when(ACC.services.addTransportBundleToCartForPackageAjax($(formList[index]))).then(function(response) {
                if (!response.valid) {
                    var output = [];
                    response.errors.forEach(function(error) {
                        output.push("<p>" + error + "</p>");
                    });
                    $("#y_addPackageToCartErrorModal").find(".y_addPackageToCartErrorBody").html(output.join(""));
                }
                ACC.packagedetails.addTransportBundleToCartResult = ACC.packagedetails.addTransportBundleToCartResult && response.valid;
            });
        } else {
            return $.when(ACC.services.addTransportBundleToCartForPackageAjax($(formList[index]))).then(function(response) {
                ACC.packagedetails.addTransportBundleToCartResult = ACC.packagedetails.addTransportBundleToCartResult && response.valid;
                if (!response.valid) {
                    var output = [];
                    response.errors.forEach(function (error) {
                        output.push('<p>${error}</p>');
                    });
                    $("#y_addPackageToCartErrorModal").find(".y_addPackageToCartErrorBody").html(output.join(""));
                    return;
                }
                return ACC.packagedetails.addTransportBundleEntry(formList, index + 1);
            });
        }
    },

    addAccommodationToCart: function() {
        var dfd = $.Deferred();
        var selectedRoomStays = $('.y_changeAccommodationButton:checked');
        ACC.packagedetails.addAccommodationToCartResult = true;
        $.when(ACC.packagedetails.addAccommodationToCartEntry(selectedRoomStays, 0)).then(function() {
            dfd.resolve();
        });
        return dfd.promise();
    },

    addAccommodationToCartEntry: function(selectedRoomStays, index) {
        var packageAccommodationAddToCartForm = ACC.packagedetails.buildPackageAccommodationAddToCartForm(selectedRoomStays[index], "/cart/accommodation/package-add");
    },

    buildPackageAccommodationAddToCartForm: function(element, formAction) {
        var ratePlanAttributesDiv = $(element).closest(".y_ratePlanAttributes");
        var roomStayContainerDiv = $(element).closest(".y_roomStayContainer");

        var checkInDate = roomStayContainerDiv.find("input[class=y_checkInDate]").val();
        var checkOutDate = roomStayContainerDiv.find("input[class=y_checkOutDate]").val();
        var roomStayRefNumber = roomStayContainerDiv.find("input[class=y_roomStayRefNumber]").val();
        var accommodationCode = roomStayContainerDiv.find("input[class=y_accommodationCode]").val();
        var ratePlanCode = ratePlanAttributesDiv.find("input[class=y_ratePlanCode]").val();
        var listOfRoomRateCodes = [];
        var listOfRoomRateDates = [];
        ratePlanAttributesDiv.find("input[class=y_roomRate]").each(function() {
            listOfRoomRateCodes.push($(this).attr('code'));
            listOfRoomRateDates.push($(this).val());
        });

        var numberOfRoomsFromForm = roomStayContainerDiv.find("input[class=y_numberOfRooms]").val();

        var packageChangeAccommodationForm = $("#packageChangeAccommodationForm");
        if (numberOfRoomsFromForm !== "undefined") {
            packageChangeAccommodationForm.find("#y_numberOfRooms").attr('value', numberOfRoomsFromForm);
        }

        packageChangeAccommodationForm.find("#y_checkInDate").attr('value', checkInDate);
        packageChangeAccommodationForm.find("#y_checkOutDate").attr('value', checkOutDate);
        packageChangeAccommodationForm.find("#y_accommodationCode").attr('value', accommodationCode);
        packageChangeAccommodationForm.find("#y_roomRateCodes").attr('value', listOfRoomRateCodes);
        packageChangeAccommodationForm.find("#y_roomRateDates").attr('value', listOfRoomRateDates);
        packageChangeAccommodationForm.find("#y_ratePlanCode").attr('value', ratePlanCode);
        packageChangeAccommodationForm.find("#y_roomStayRefNumber").attr('value', roomStayRefNumber);
        packageChangeAccommodationForm.find("#y_roomStayRefNumber").attr('value', roomStayRefNumber);
        packageChangeAccommodationForm.find("#y_changeVar").attr('value', $("#changeVar").val());
        packageChangeAccommodationForm.find("#y_onlyAccommodationChange").attr('value', $("#onlyAccommodationChange").val());

        var currentlySelectedOption = $(element);
        formAction = ACC.config.contextPath + formAction;
        $.when(ACC.services.addAccommodationToCartAjax(packageChangeAccommodationForm, formAction)).then(
            function(response) {
                var jsonData = JSON.parse(response);
                if (jsonData.valid) {

                } else {

                    var output = [];
                    ACC.packagedetails.addAccommodationToCartResult = false;
                    jsonData.errors.forEach(function(error) {
                        output.push("<p>" + error + "</p>");
                    });
                    $("#y_addPackageToCartErrorModal").find(".y_addPackageToCartErrorBody").html(output.join(""));
                    $('#y_processingModal').modal("hide");
                    $("#y_addPackageToCartErrorModal").modal();
                }
            }

        );
    },

    disableButtons: function() {
        $(".y_packageDetailsContinueButton").addClass('disabled');
        $(".y_roomOptionsCollapse").addClass('disabled');
        $(".y_flightOptionsCollapse").addClass('disabled');
    },

    enableButtons: function() {
        $(".y_packageDetailsContinueButton").removeClass('disabled');
        $(".y_roomOptionsCollapse").removeClass('disabled');
        $(".y_flightOptionsCollapse").removeClass('disabled');
    },

    hidePackageBuildModal: function() {
        if ($('#package-build-modal').is(":visible")) {
            $('#package-build-modal').modal('hide');
        }
    },

    bindOutboundFerrySelectionButton: function() {
        $(document).on('click', '#y_outbound .y_packageFerryResultSelect', function(event) {
            event.preventDefault();
            ACC.packagedetails.disableButtonsOnPage();
            ACC.appmodel.currentlySelectedItineraryId = $(this).attr("id");
            ACC.appmodel.trips.outbound.dateTime = new Date(parseInt($(this).val()));

            // Call to server to addBundleToCart
            var addBundleToCartForm = $(this).closest("li").find(".y_addBundleToCartForm");
            ACC.packagedetails.addBundleToCartSubmit(addBundleToCartForm, this);
        });
    },


    bindInboundFerrySelectionButton: function() {
        $(document).on('click', '#y_inbound .y_packageFerryResultSelect', function(event) {
            event.preventDefault();
            ACC.packagedetails.disableButtonsOnPage();
            ACC.appmodel.currentlySelectedItineraryId = $(this).attr("id");
            var inboundDepartureDate = new Date(parseInt($(this).val()));
            // Check if dateTimes are compatible, if not show modal and don't
            // call addToCart
            if (ACC.appmodel.trips.outbound.dateTime) {
                // if the inbound date is less than the outbound date, show
                // validation modal
                if (inboundDepartureDate < ACC.appmodel.trips.outbound.dateTime) {
                    ACC.packagedetails.enableButtonsOnPage();
                    $("#y_addBundleToCartValidationModal .y_addBundleToCartValidationBody").html(ACC.addons.bcfstorefrontaddon['fareselection.validation.time.error']);
                    $('#y_addBundleToCartValidationModal').modal('show');
                    return;
                }
            }

            // Call to server to addBundleToCart
            var addBundleToCartForm = $(this).closest("li").find(".y_addBundleToCartForm");
            ACC.packagedetails.addBundleToCartSubmit(addBundleToCartForm, this);
        });
    },

    disableButtonsOnPage: function() {
        $('.y_packageFerryResultSelect').attr('disabled', true);
    },

    enableButtonsOnPage: function() {
        $('.y_packageFerryResultSelect').attr('disabled', false);
    },

    addBundleToCartSubmit: function(addBundleToCartForm, btnClicked) {
        if ($('html').hasClass('y_isMobile')) {
            ACC.fareselection.contractExpandTravelSelection(true, $(btnClicked));
        }
        var addToCartResult;
        $.when(ACC.services.addBundleToCartAjax(addBundleToCartForm)).then(
            function(response) {

                addToCartResult = response.valid;

                if (!response.valid) {
                    if ($('html').hasClass('y_isMobile')) {
                        ACC.fareselection.contractExpandTravelSelection(false, $(btnClicked));
                    }
                    var output = [];
                    response.errors.forEach(function(error) {
                        output.push("<p>" + error + "</p>");
                    });
                    $("#y_addPackageToCartErrorModal .y_addPackageToCartErrorBody").html(output.join(""));
                    $("#y_addPackageToCartErrorModal").modal();

                    if (response.minOriginDestinationRefNumber == undefined) {
                        $("#" + ACC.appmodel.currentlySelectedItineraryId).prop('checked', false);
                        $("#" + ACC.appmodel.currentlySelectedItineraryId).parent().removeClass('selected');
                        if (ACC.appmodel.previouslySelectedItineraryId) {
                            $("#" + ACC.appmodel.previouslySelectedItineraryId).prop('checked', true);
                            $("#" + ACC.appmodel.previouslySelectedItineraryId).parent().addClass('selected');
                        }
                    }
                    $('#y_processingModal').modal("hide");
                    ACC.packagedetails.enableButtonsOnPage();
                } else {
                    // ACC.packagedetails.refreshPage("#flightOptionsCollapse");
                    location.href = $(btnClicked).attr("href");
                }
            });
        return addToCartResult;
    },

    refreshPage: function(elementID) {
        sessionStorage.setItem("elementToUncollapse", elementID);
        sessionStorage.setItem("scrollTop", $(this).scrollTop());
        location.reload(true);
    },

    changeButtonLabel: function(elementID) {
        if (elementID === "#flightOptionsCollapse") {
            $('#flightOptionsCollapse').prev('div').find('.show-text').addClass('hidden');
            $('#flightOptionsCollapse').prev('div').find('.hide-text').removeClass('hidden');
        } else if (elementID === "#roomOptionsCollapse") {
            $('.y_roomOptionsCollapse').find('.show-text').addClass('hidden');
            $('.y_roomOptionsCollapse').find('.hide-text').removeClass('hidden');
        }
    },

    bindAddPackageToCartButton: function() {
        $('.y_addPackageToCart').on('click', function() {
            ACC.packagedetails.uncheckAll(".y_changeAccommodationButton");
            $(this).parent().find(".y_changeAccommodationButton").prop("checked", true);
            ACC.packagedetails.addPackageToCartOnLoad();
        });
    },

     bindAddAccommodationToCartButton: function() {
            $('.y_addAccommodationToCart').on('click', function() {
                ACC.packagedetails.uncheckAll(".y_changeAccommodationButton");
                $(this).parent().find(".y_changeAccommodationButton").prop("checked", true);
                ACC.packagedetails.addAccommodationToCartOnLoad();
            });
        },


    uncheckAll: function(elementClass) {
        $(elementClass).each(function() {
            $(this).removeAttr('checked');
            $('input[type="radio"]').prop('checked', false);
        });
    },

    bindBackToResultsButton : function() {
        $(document).on('click', '.backToResults', function() {
            window.history.back();
        });
    }
};

$(document).ready(function() {
    if (sessionStorage.getItem("scrollTop") !== null) {
        var elementID = sessionStorage.getItem("elementToUncollapse");
        ACC.packagedetails.changeButtonLabel(elementID);
        $(elementID).addClass("in");
        $(window).scrollTop(sessionStorage.getItem("scrollTop"));
        sessionStorage.clear();
    }
});
