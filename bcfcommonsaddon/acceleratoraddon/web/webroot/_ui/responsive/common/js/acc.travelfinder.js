ACC.travelfinder = {

    _autoloadTracc: [
        "getAllTravelSectors",
        "init"
    ],

    allTravelSectors: [],
    componentParentSelector: '#y_travelFinderForm',
    minRelativeReturnDate: 0,
    terminalcacheversion: 0,

    init: function() {
        var $passengerQuantityArray = $(".y_travelFinderPassengerQuantity");
        $passengerQuantityArray.each(function() {
            $(this).attr('value', parseInt($(this).parent().find('.y_travelFinderPassengerQuantitySpan').text()));
        });
        ACC.travelfinder.validationMesagesInit();
        ACC.travelfinder.bindPackageChangeTripTypeButton();
        ACC.travelfinder.bindPackageTravelWithChildrenButton();
        ACC.travelfinder.bindPackageFareFinderLocationEvents();
        ACC.travelfinder.bindPackageTravelSectorsOriginAutoSuggest();
        ACC.travelfinder.bindPackageTravelSectorsDestinationAutoSuggest();
        ACC.travelfinder.addAccommodationLocation();
        ACC.travelfinder.bindRoomQuantity();
        ACC.travelfinder.bindCheckInAndCheckOutFieldsWithHotelPartStay();
        ACC.travelfinder.bindPartHotelStayWithTripType();
        ACC.travelfinder.bindValidationsOnCheckInAndCheckOut();
        ACC.travelfinder.calculateNumberOfNights();
        ACC.travelfinder.populateCheckInDateInOneWay();
        ACC.travelfinder.populateNumberOfNightsOnLoad();
        ACC.travelfinder.selectVehicleType();
        ACC.travelfinder.selectOversizeVehicleType();
        ACC.travelfinder.convertVehicleDimensionInMeters();
        ACC.travelfinder.dangerousGoodCheckBox();
        ACC.travelfinder.travellingWithVehicleCheckBox();
        ACC.travelfinder.returnwithdifferentvehicle();
        ACC.travelfinder.vehicleinboundcheckbox();
        ACC.travelfinder.sidecarcheckbox();
        ACC.travelfinder.bindDestinationAutosuggest();
        ACC.travelfinder.bindTravelFinderLocationEvents();
        ACC.travelfinder.bindVacationDestinationLocationChange();
        ACC.travelfinder.bindTravelRouteSelection();
        ACC.travelfinder.showChildDropDown();
        ACC.travelfinder.loadAccessibilityNeeds();
        ACC.travelfinder.loadOtherAccessibilityNeedTextBoxPackage()
        ACC.travelfinder.packageCheckAccessibilityNeed();
        ACC.travelfinder.showHideDivsPackage();
        ACC.travelfinder.setVehicleDimensionValues();
        ACC.travelfinder.showRefundReasonCodeForm();
        ACC.travelfinder.showChangeBookingForm();
        ACC.travelfinder.showInternalCommentsForm();
        ACC.travelfinder.addDatePickerForTravelFinder($("#datepicker"), $("#datepicker1"));
        ACC.travelfinder.setSelectedDates();
        ACC.travelfinder.renderRoomsUI();
        ACC.travelfinder.bindVacationEditRouteAccordionPopulation();

    },
    vacationValidation : function(journeyType){

        if (ACC.common.shallCartBeRemoved(journeyType) == false) {
            return false;
        }

      if($("#vacation-accordion1").find(".dropdown-text strong").text() == "Select a destination (city)"){
        ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.from.location"));
        return false;
      }


      if($("#vacation-accordion2").find(".dropdown-text strong").text() == "Select a route"){
        ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.to.location"));
        return false;
      }



      if($("#js-depart .bc-dropdown.depart").val()==""){
        ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.checkin.date"));
        return false;
      }


      if($("#js-return .bc-dropdown.arrive").val()==""){
        ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.checkout.date"));
        return false;
      }


      if($("#js-depart .bc-dropdown.depart").val()>$("#js-return .bc-dropdown.arrive").val()){
        ACC.travelfinder.showErrorMessage(ACC.validationMessages.travelfinder.message("error.farefinder.travelfinder.checkout.invaliddate"));
         return false;
      }

      ACC.travelfinder.showErrorMessage("","hide");
      return true;
    },
    renderRoomsUI : function(){

      //adding current years
      var dt = new Date();
      if($(".current-year-custom").length && $(".current-year-custom").text()==""){

            $(".current-year-custom").html(dt.getFullYear());
      }




      //init accordion
      $("#vacation-accordion2").accordion();
      $("#vacation-accordion2").accordion({
        header : "h3",
        collapsible : true,
        active : false,
        beforeActivate: function( event, ui ) {
            var wd = $(event.currentTarget).outerWidth();
            $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
        }
      });

      //render accordion


     $("#vacation-accordion1").find('.sub-link').click(function(){
       $(".y_accommodationDestinationLocation").val($(this).data("code"));
      });
      $("#vacation-accordion1").find('.sub-link').click(ACC.travelfinder.sourceClickHander);


      $("#vacation-accordion1").accordion({
          header: "h3",
          collapsible: true,
          active: false,
          heightStyle: "content",
          beforeActivate: function( event, ui ) {
            var wd = $(event.currentTarget).outerWidth();
            $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
          }
    });


      $("#vacation-accordion2").click(function(){
        $("#vacation-accordion1").closest(".ui-accordion").accordion({active:2});
        var destinationCode = document.getElementById('y_destinationCode').value;
        if(destinationCode.length != 0){
            ACC.travelfinder.sourceClickHander();
        }
      });

      //End

      $(".rooms-wrapper").find("input").change(function(){
        var prev = $(this).data('val');
        var current = $(this).val();
        $(this).data('val',current);
        if(prev==undefined){

          ACC.travelfinder.addRoomWidget();
        }else if(parseInt(prev) < parseInt(current)){
          ACC.travelfinder.addRoomWidget();
        }else{
          ACC.travelfinder.removeRoomWidget();
        }

      })

      $(document).on("change",".select-guest-custom .child-qty",function(){
        var prev = $(this).data('val');
        var current = $(this).val();
        var childBoxIndex = current - 1;
        $(this).data('val',current);

        if(prev==undefined){
          ACC.travelfinder.addChildBox($(this), childBoxIndex);
        }else if(parseInt(prev) < parseInt(current)){
          ACC.travelfinder.addChildBox($(this), childBoxIndex);
        }else{
          ACC.travelfinder.removeChildBox($(this));
        }
      })


      var noOfRoom = $("#hotelAccommodationQuantity").val();
      $(".rooms-wrapper input").val(noOfRoom);
      if(noOfRoom>0)
        $(".rooms-wrapper input").data("val",noOfRoom-1);
      //add room widget based on number of room on load
      var noOfRoom = $(".rooms-wrapper").find("input").val();
      for(var i = 0; i<noOfRoom ; i++){
        ACC.travelfinder.addRoomWidget(i+1);
      }

      setTimeout(function(){
          $("body").trigger("setSearchDropDown",$("#vacation-accordion1"));
          $("body").trigger("setSearchDropDown",$("#vacation-accordion2"));
      },500)

      $("#y_travelFinderForm").on("submit",function(){
          $('#y_buttonToClick').val("#bookAPackage");
          var y_travelFinderForm_data = ACC.travelfinder.vacationValidation('BOOKING_PACKAGE');
          ACC.carousel.adjustSearchHeight();
          return y_travelFinderForm_data;
      })

      $("#y_accommodationEditForm").on("submit",function(){
                $('#y_buttonToClick').val("#bookAccommodation");
                var y_AccommodationFinderForm_data = ACC.travelfinder.vacationValidation('BOOKING_ACCOMMODATION_ONLY');
                ACC.carousel.adjustSearchHeight();
                return y_AccommodationFinderForm_data;
            })


      $(".custom-guest-input").removeAttr("name");
      ACC.travelfinder.mapHiddenUiToUI();
      $(".vacation-flow-hide").remove();

      //outside clicked
      $(document).mouseup(function(e)
      {
          var container = $(".room-widget-wrapper");

          // if the target of the click isn't the container nor a descendant of the container
          if (!container.is(e.target) && container.has(e.target).length === 0)
          {
              $(".select-guest-accordion").accordion({
                    active:2,
                    beforeActivate: function( event, ui ) {
                        var wd = $(event.currentTarget).outerWidth();
                        $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
                    }
                });
          }
      });

    },
    destinationClickHandler : function(){
      ACC.farefinder.bindDropDownselection($(this));
      var travelRoute = $(this).data("code");
      var travelRouteName = $(this).text();
      var tripType = $(this).data("triptype");
      $(".y_travelFinderTravelRoute").val(travelRoute);
      $(".y_travelFinderTravelRouteName").val(travelRouteName);
      var long_route_calender_limit = "+18m";
      var short_route_calender_limit = "+12m";
      if($("#long-route-calender-limit").text() != ""){
        long_route_calender_limit = "+"+$("#long-route-calender-limit").text()+"m";
      }
      if($("#short-route-calender-limit").text() != ""){
        short_route_calender_limit = "+"+$("#short-route-calender-limit").text()+"m";
      }
      if(tripType == "LONG"){
        $("#datepicker").datepicker("option",{maxDate: long_route_calender_limit});
        $("#datepicker1").datepicker("option",{maxDate: long_route_calender_limit});
      }else{
        $("#datepicker").datepicker("option",{maxDate: short_route_calender_limit});
        $("#datepicker1").datepicker("option",{maxDate: short_route_calender_limit});
      }
    },
        sourceClickHander: function(){
      ACC.farefinder.bindDropDownselection($(this));
      var destinationCode = document.getElementById('y_destinationCode');
      if(destinationCode == null){
        destinationCode = $('.y_accommodationDestinationLocation').val();
        if(destinationCode == null){
            var vacationDestinationCode = $(this).data("code");
        }else{
            var vacationDestinationCode = destinationCode;
        }
      }else{
        var vacationDestinationCode = destinationCode.value;
      }

      if(vacationDestinationCode === undefined || vacationDestinationCode == "")
      {
          return;
      }
      $(".y_travelFinderLocationCode").val(vacationDestinationCode);
      $(".y_accommodationFinderLocationSuggestionType").val("AIRPORTGROUP");
      $.when(ACC.services.getTravelRoutesForLocationAjax(vacationDestinationCode)).then(
          function(data, textStatus, jqXHR) {
              var htmlEl = '<div id="vacation-accordion2" class="accordion-34 accordion-bg-white"><h3><span class="dropdown-text"><strong class="drop-text mt-0 destination-text">Select a route</strong> <span class="custom-arrow"></span> </span> </h3> <div> <div class="vacation-range-box p-0"> <div class="form-height-fix style-3">';
              var ulEL = '<ul><span class="second-span"></span>';
              $.each(data, function(i, item) {
                ulEL+='<li><a class="sub-link" data-code="'+item.code+'"  data-triptype="'+item.routeType+'" data-city="" data-terminal="">'+item.name+'</a></li>';
              });
              ulEL+="</ul>";
              htmlEl+= ulEL + "  </div></div></div></div>";

              $("#vacation-accordion2").replaceWith($(htmlEl));
              $("#vacation-accordion2").find('.sub-link').click(ACC.travelfinder.destinationClickHandler);
              $("#vacation-accordion2").accordion({
                  header: "h3",
                  collapsible: true,
                  active: false,
                  beforeActivate: function( event, ui ) {
                    var wd = $(event.currentTarget).outerWidth();
                    $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
                  }
                });
              $("#vacation-accordion2").closest(".bc-accordion").trigger("searchOnKeyPress");
              setTimeout(function(){
                $("body").trigger("setSearchDropDown",$("#vacation-accordion2"));
              },200);
          }
      );
    },
    mapHiddenUiToUI : function(){
      var i=0;
      $(".guest-types").each(function(){

        if(!$(this).hasClass("hidden")){

          var adult = $(this).find(".y_adultSelect").val();
          var child = $(this).find(".y_childSelect").val();

          var childAge = [];
          if(child>0){
            for (var j = 0; j < child; j++) {
              childAge.push(parseInt($("#room_"+i+"_child_"+j).val()));
            }
          }
          ACC.travelfinder.setDataToGuestWidget(i,adult,child,childAge);
        }
        i++;
      })
    },
    setDataToGuestWidget : function(i,adult,child,childAge){
      var sel = $(".select-guest-custom:eq("+i+")");
      sel.find(".adult-qty").val(adult);
      sel.find(".child-qty").val(child);

      if(parseInt(child) > 0)
      {
        sel.find(".child-qty").data("val",child);
      }
      _.forEach(childAge,function(val,key){
        ACC.travelfinder.addChildBox(sel.find(".child-qty"),key);
        if(val !== undefined)
          sel.find(".child-box:last select").val(val);
      })
    },
    showErrorMessage : function(data,action){
      if(action == "hide"){
        $(".custom-error-3").addClass("hidden");
      }else{
        $(".custom-error-3").find("span").text(data);
        $(".custom-error-3").removeClass("hidden");
      }

    },

    addChildBox : function(ele,index){

        if(index==null){
            var childNumber=parseInt($(ele).val())-1;
        }else{
            var childNumber=index;
        }
        var roomNo=parseInt(ele.parent().find(".room-number").text());

        if(ele.attr("name").indexOf('accommodationFinderForm')>=0){
            var childName = "accommodationFinderForm.roomStayCandidates["+(roomNo)+"].passengerTypeQuantityList[1].childAges["+(childNumber)+"]";

        }else{
            var childName = "roomStayCandidates["+(roomNo)+"].passengerTypeQuantityList[1].childAges["+(childNumber)+"]";
        }

        var childWrapper = ele.closest(".ui-accordion-content").find(".child-box-wrapper").first();
        var childNo = ele.closest(".ui-accordion-content").find(".child-qty").val();
        var childAge = $('#childAge'+roomNo+'1'+childNumber).text();

        var childBox = $(".snippets").find(".child-box").clone();

        childBox.find(".custom-select").attr("name",childName);

        if(index == undefined){
         childBox.find(".child-label-name-span").html(childNo);
        }
        else {
         childBox.find(".child-label-name-span").html(index+1);
        }


        var childAgeOption = "";
        for (var j = 0; j <= 18; j++) {
          if(j==childAge){
            var option = '<option value="' + j + '" selected> ' + j + ' </option>';
          }else{
          var option = '<option value="' + j + '"> ' + j + ' </option>';
        }


        childAgeOption = childAgeOption + option;
        }
        childBox.find(".custom-select").append(childAgeOption);

        $( document ).ready(function() {
            childWrapper.append(childBox);
        })
    },
    removeChildBox : function(ele){
      var childWrapper = ele.closest(".ui-accordion-content").find(".child-box-wrapper");
      childWrapper.find(".child-box:last").remove();

    },
    addRoomWidget : function(index){

      var roomNo = $(".rooms-wrapper").find("input").val();
      if(index !== undefined){
        roomNo = index;
      }
      var roomWidget = $(".snippets").find(".room-widget").clone();
      var adultName = "accommodationFinderForm.roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[0].quantity";
      var childName = "accommodationFinderForm.roomStayCandidates["+(roomNo-1)+"].passengerTypeQuantityList[1].quantity";
      roomWidget.find(".select-guest-accordion").attr("data-roomindex",(roomNo-1));
      roomWidget.find(".room-title span").html(roomNo);
      roomWidget.find(".room-number").html(roomNo-1);
      roomWidget.find(".adult-qty").attr("name",adultName);
      roomWidget.find(".adult-qty").siblings(".btn-number").each(function(){
        $(this).attr("data-field",adultName);
      })

      roomWidget.find(".child-qty").val("0");
      roomWidget.find(".child-qty").attr("name",childName);
      roomWidget.find(".child-qty").siblings(".btn-number").each(function(){
        $(this).attr("data-field",childName);
      })


      var roomWidgetWrapper = $(".room-widget-wrapper");
      roomWidgetWrapper.append(roomWidget);
      roomWidgetWrapper.find(".select-guest-accordion:last").accordion({
        header : "h3",
        collapsible : true,
        heightStyle: "content",
        active : false,
        activate: function(event, ui) {
          if(ui.newHeader.length === 0){
            ACC.travelfinder.renderAccordionState($(this),"close");
          }else{
            ACC.travelfinder.renderAccordionState($(this),"open");
          }
        }

      });
      setTimeout(function(){
        roomWidget.removeClass("hidden");
        $(window).trigger("resize");
      },500)
    },
    removeRoomWidget : function(){
      $(".room-widget-wrapper").find(".room-widget:last").remove();
      $(window).trigger("resize");
    },
    renderAccordionState : function(ele,state){
      var headerIcon = ele.find("h3 .ui-accordion-header-icon");
      var headerIconCustom = ele.find("h3 .custom-arrow");

      var msgWrapper = ele.closest(".select-guest-custom").find(".vacation-guest-gray");
      ACC.travelfinder.renderAccordionMessage(ele);
      if(state == "open"){
        headerIcon.removeClass("ui-icon");
        headerIconCustom.addClass("hidden");
        msgWrapper.addClass("hidden");
        if(headerIcon.find('i').length==0)
          headerIcon.append('<i class="fa fa-check" aria-hidden="true">');
      }else{
        msgWrapper.removeClass("hidden");
        headerIcon.addClass("ui-icon");
        headerIcon.find('i').remove();
        headerIconCustom.removeClass("hidden");
      }
      $(window).trigger("resize");

    },
    renderAccordionMessage : function(ele){
      var adultNo = ele.find(".adult-qty").val();
      var childNo = ele.find(".child-qty").val();
      var msgWrapper = ele.closest(".select-guest-custom").find(".vacation-guest-gray");
      msgWrapper.find(".adult-count").html(adultNo);
      msgWrapper.find(".child-count").html(childNo);
      if(ele.find(".infant-qty").length !== 0){
        msgWrapper.find(".infant-count").html(ele.find(".infant-qty").val());
      }
    },
    addDatePickerForTravelFinder : function($departureDateField, $returnDateField){
        var NewdateFormat = $(".routesfaresschedules").find(".vacation-ui-depart").find(".vacation-calen-year-txt").text().slice(0, -4);
        var mydate = new Date(NewdateFormat);
        var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
        var dayName = days[mydate.getDay()];
        var month = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
        $(".routesfaresschedules").find(".vacation-ui-depart").find(".vacation-calen-year-txt").text(dayName+', '+month +' '+mydate.getDate());
         var todayDate = new Date();
         $departureDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'mm/dd/yy',

                beforeShow: function (input) {

                    setTimeout(function() {
                        if($returnDateField.length){
                            $departureDateField.rules('remove')
                        }

                        }, 0);
                },
                onSelect: function (selectedDate) {
                    // add validation to departure date
                    var ele = $(this).siblings(".datepicker-input");
                    $(ele).val(selectedDate);
                    $(".vacation-ui-depart").find(".vacation-calen-year-txt").text(selectedDate);
                    $(".y_travelFinderDatePickerCheckOut").val(selectedDate);
                    //minimun date
                    $("#datepicker1").datepicker("option",{ minDate: selectedDate });

                    $(".vacation-calender").find("li.tab-depart").removeClass("active");

                    $(".vacation-calender-section").find(".depart-calendar").removeClass("active");

                }
            });
         if($returnDateField != undefined)
         {
        $returnDateField.datepicker({
            minDate: todayDate,
            maxDate: '+1y',
            dateFormat: 'mm/dd/yy',
            beforeShow: function (input) {

                setTimeout(function() {
                    $returnDateField.rules('remove')
                    }, 0);
            },
            onSelect: function (selectedDate) {
              var ele = $(this).siblings(".datepicker-input");
              $(ele).val(selectedDate);
              $(".vacation-ui-return").find(".vacation-calen-year-txt").text(selectedDate);
              $(".ui-datepicker a").removeAttr("href");
              $(ele).change();
              $(".vacation-calender").find("li.tab-return").removeClass("active");
              $(".vacation-calender-section").find(".return-calendar").removeClass("active");

            }
        });

          //binding input to datepicker
          $(".depart.datepicker-input").keyup(function(){
            $departureDateField.datepicker("setDate",$(this).val());
            var ele = $(this).closest(".tab-pane").attr("id");
            $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
            $(".ui-return").trigger("setDate",$(this).val());
          });

          $(".arrive.datepicker-input").keyup(function(){
            $returnDateField.datepicker("setDate",$(this).val());
            var ele = $(this).closest(".tab-pane").attr("id");
            $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
          })

         }

         $('#departingDateTime').keydown(function(e) {
             e.preventDefault();
             return false;
          });
           $('#returnDateTime').keydown(function(e) {
               e.preventDefault();
               return false;
            });

            $(document).mouseup(function(e)
            {

                var container = $(".vacation-calender-section");

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0)
                {
                    $(".vacation-calender-section").find(".tab-links.active").removeClass("active");
                    $(".vacation-calender-section").find(".tab-pane.active").removeClass("active");
                }
            });


    },
    validationMesagesInit : function(){
      ACC.validationMessages.travelfinder = new validationMessages("ferry-travelFinderForm");
      ACC.validationMessages.travelfinder.getMessages("error");
    },

    bindRoomQuantity: function() {
        ACC.accommodationfinder.bindRoomQuantity();
    },


    setSelectedDates: function() {
        if($(".vacation-ui-depart .current-year-custom-date").length){
           if($("#datepicker") != undefined){
            $("#datepicker").datepicker("setDate",$.trim($(".vacation-ui-depart .current-year-custom-date").text()));
            $(".depart.datepicker-input").val($.trim($(".vacation-ui-depart .current-year-custom-date").text()));
           }
      }
      if($(".vacation-ui-return .current-year-custom-date").length){
           if($("#datepicker1") != undefined){
            $("#datepicker1").datepicker("setDate",$.trim($(".vacation-ui-return .current-year-custom-date").text()));
            $(".arrive.datepicker-input").val($.trim($(".vacation-ui-return .current-year-custom-date").text()));
           }
      }
    },

    bindCheckInAndCheckOutFieldsWithHotelPartStay: function() {
        $('#y_hotel-part-stay').change(function() {
            $('#y_hotel-part-stay-value').val(this.checked);
            if (this.checked) {
                $(".checkInAndCheckOutDiv").show();
                $(".y_travelFinderDatePickerCheckIn").val("");
                $(".y_travelFinderDatePickerCheckOut").val("");
                var numberOfNightsValue = $('.nights-placeholder-text');
                $("#y_numberOfNights").text('0');
                numberOfNightsValue.last().addClass('hidden');
                numberOfNightsValue.first().removeClass('hidden');
            } else
                $(".checkInAndCheckOutDiv").hide();
        });

    },


    bindPartHotelStayWithTripType: function() {
        $('.radio-button-row').change(function() {
            if ($('.radio-button-row').find(':checked').val() == 'SINGLE') {
                ACC.farefinder.hideReturnField();
                $('#y_hotel-part-stay').prop('checked', true);
                $('#y_hotel-part-stay-value').val(true);
                $('#y_hotel-part-stay').attr('disabled', true);
                $(".checkInAndCheckOutDiv").show();



                var departingDate = $(".y_travelFinderDatePickerDeparting").val();
                if (departingDate) {
                    $(".y_travelFinderDatePickerCheckIn").val(departingDate);
                }
                $(".y_travelFinderDatePickerCheckOut").val('');
                $("#y_numberOfNights").text('0');
                var numberOfNightsValue = $('.nights-placeholder-text');
                numberOfNightsValue.last().addClass('hidden');
                numberOfNightsValue.first().removeClass('hidden');
                var todayDate = new Date();
                todayDate.setFullYear(todayDate.getFullYear() + 1);
                var currDate = ACC.travelcommon.convertToUSDate(departingDate);
                $(".y_travelFinderDatePickerCheckOut").datepicker("option", "minDate", ACC.travelcommon.addDays(currDate, 1));
                $(".y_travelFinderDatePickerCheckIn").datepicker("option", "maxDate", todayDate);
                if ($(".y_travelFinderDatePickerCheckIn").val()) {
                    $(".y_travelFinderDatePickerCheckOut").datepicker("option",
                        "maxDate", ACC.travelcommon.addDays(ACC.travelcommon.convertToUSDate($(".y_travelFinderDatePickerCheckIn").val()), 20));
                } else {
                    $(".y_travelFinderDatePickerCheckOut").datepicker("option", "maxDate", todayDate);
                }
            } else {
                ACC.farefinder.showReturnField();
                ACC.farefinder.addReturnFieldValidation($(".y_travelFinderDatePickerReturning"), $(".y_travelFinderDatePickerDeparting")
                    .val());
                $('#y_hotel-part-stay').prop('checked', false);
                $('#y_hotel-part-stay-value').val(false);
                $('#y_hotel-part-stay').attr('disabled', false);
                $(".checkInAndCheckOutDiv").hide();
                $(".y_travelFinderDatePickerCheckIn").rules("remove", "required");
                $(".y_travelFinderDatePickerCheckOut").rules("remove", "required");
            }
        });
    },

    bindValidationsOnCheckInAndCheckOut: function() {
        $("#y_travelDatePickerCheckIn, #y_travelDatePickerCheckOut").on("focusout", function() {

            if ($(this).hasClass('y_travelFinderDatePickerCheckIn')) {
                ACC.accommodationfinder.reInitializeCheckInDate($(this));
            }
            $('#y_hotel-part-stay-value').val($('#y_hotel-part-stay').prop("checked"));
            if ($('#y_hotel-part-stay').prop("checked") && $('.radio-button-row').find(':checked').val() == 'RETURN') {
                $(".y_travelFinderDatePickerCheckIn").rules("remove", "required");
                $(".y_travelFinderDatePickerCheckOut").rules("remove", "required");
            } else {
                if ($(this).is('input') && $(this).valid()) {
                    if ($(this).hasClass('y_travelFinderDatePickerCheckOut')) {
                        ACC.accommodationfinder.addCheckOutDateFieldValidation($(".y_travelFinderDatePickerCheckOut"), $(".y_travelFinderDatePickerCheckIn").val());
                    } else {
                        ACC.accommodationfinder.addCheckInDateFieldValidation($(this));
                    }
                    $(this).valid();
                }
            }

        });
    },

    populateCheckInDateInOneWay: function() {
        $(".y_travelFinderDatePickerDeparting").on("change keyup", function() {
            if ($('.radio-button-row').find(':checked').val() == 'SINGLE') {
                var departingDate = $(".y_travelFinderDatePickerDeparting").val();
                if (departingDate) {
                    $(".y_travelFinderDatePickerCheckIn").val(departingDate);
                    var $checkOutDateField = $(".y_travelFinderDatePickerCheckOut");
                    $checkOutDateField.val('');
                    var currDate = ACC.travelcommon.convertToUSDate(departingDate);
                    $checkOutDateField.datepicker("option", "minDate", ACC.travelcommon.addDays(currDate, 1));
                    $checkOutDateField.datepicker("option", "maxDate", ACC.travelcommon.addDays(currDate, 20));
                    ACC.travelfinder.updateNumberOfNights($(".y_travelFinderDatePickerCheckIn"), $(".y_travelFinderDatePickerCheckOut"));
                }
            }
        });
    },

    populateNumberOfNightsOnLoad: function() {
        if (!$(".y_travelFinderDatePickerCheckOut").val() && !$(".y_travelFinderDatePickerCheckIn").val()) {
            return;
        }
        ACC.accommodationfinder.populateNumberOfNightsOnListingPage($(".y_travelFinderDatePickerCheckIn"), $(".y_travelFinderDatePickerCheckOut"));
    },

    initializeDatePicker: function() {

        var $departureDateField = $(".y_travelFinderDatePickerDeparting");
        var $returnDateField = $(".y_travelFinderDatePickerReturning");
        var $checkInDateField = $(".y_travelFinderDatePickerCheckIn");
        var $checkOutDateField = $(".y_travelFinderDatePickerCheckOut");

        ACC.farefinder.addDatePickerForFareFinder($departureDateField, $returnDateField);
        ACC.accommodationfinder.addDatePickerForAccommodationFinder($checkInDateField, $checkOutDateField);
    },

    calculateNumberOfNights: function() {

        $(".y_travelFinderDatePickerCheckIn, .y_travelFinderDatePickerCheckOut").on("change keyup", function(event) {
            var code;
            if (!e) {
                var e = window.event;
            }
            if (e.keyCode) {
                code = e.keyCode;
            } else if (e.which) {
                code = e.which;
            }

            if (code == 8 || code == 46 || code == 37 || code == 39 || code == undefined) {
                if (!$(this).val()) {
                    var numberOfNightsValue = $('.nights-placeholder-text');
                    $("#y_numberOfNights").text('0');
                    numberOfNightsValue.last().addClass('hidden');
                    numberOfNightsValue.first().removeClass('hidden');
                }
                return false;
            }
            ACC.travelfinder.updateNumberOfNights($(".y_travelFinderDatePickerCheckIn"), $(".y_travelFinderDatePickerCheckOut"));
        });

    },

    updateNumberOfNights: function($checkInDateField, $checkOutDateField) {
        var checkOutDate = $checkOutDateField.datepicker('getDate');
        var checkInDate = $checkInDateField.datepicker('getDate');

        ACC.accommodationfinder.reInitializeCheckInDate($checkInDateField);

        if (jQuery.type(checkInDate) === 'date') {
            ACC.accommodationfinder.populateCheckOutField($checkInDateField, $checkOutDateField);
            ACC.accommodationfinder.calculateNumberOfNights($checkInDateField, $checkOutDateField);
        }
    },

    addCheckInCheckOutDatePickersForTravelFinder: function() {

        var $checkInDateField = $(".y_travelFinderDatePickerCheckIn");
        var $checkOutDateField = $(".y_travelFinderDatePickerCheckOut");

        var todayDate = new Date();
        $checkInDateField.datepicker({
            minDate: ACC.travelcommon.getTodayUKDate(),
            maxDate: '+1y',
            dateFormat: 'mm/dd/yy',
            beforeShow: function(input) {
                setTimeout(function() {
                    $checkInDateField.rules('remove')
                }, 0);
            },
            onClose: function(selectedDate) {
                if ($checkOutDateField.is(":visible")) {
                    if ($(this).valid()) {
                        if (selectedDate) {
                            var newDate = ACC.travelcommon.convertToUSDate(selectedDate);
                            $checkOutDateField.datepicker("option", "minDate", ACC.travelcommon.addDays(newDate, 1));
                            $checkOutDateField.datepicker("option", "maxDate", ACC.travelcommon.addDays(newDate, 20));
                        }
                    } else {
                        $checkOutDateField.datepicker({
                            minDate: ACC.travelcommon.getTodayUKDate(),
                            maxDate: '+1y',
                            beforeShow: function(input) {
                                setTimeout(function() {
                                    $checkOutDateField.rules('remove')
                                }, 0);
                            }

                        });
                    }
                }
            }
        });

        $checkOutDateField.datepicker({
            minDate: ACC.travelcommon.getTodayUKDate(),
            beforeShow: function(input) {
                if (!$checkInDateField.val()) {
                    $checkOutDateField.datepicker("option", "minDate", ACC.travelcommon.addDays(todayDate, 1));
                    $checkOutDateField.datepicker("option", "maxDate", ACC.travelcommon.addDays(todayDate, 20));
                }
                setTimeout(function() {
                    $checkOutDateField.rules('remove')
                }, 0);
            },
            onClose: function(selectedDate) {
                $(this).valid();
            }
        });
    },

    bindPackageChangeTripTypeButton: function() {
        $('#onewaytravel').on('click', function() {
            $("#y_oneWayRadbtntravel").prop("checked", true);
            $("#js-roundtrip #onewaytravel").addClass("color-tab-active");
            $("#js-roundtrip #onewaytravel").removeClass("color-tab-disble");
            $("#js-roundtrip #returntravel").removeClass("color-tab-active");
            $("#js-roundtrip #returntravel").addClass("color-tab-disble");
            ACC.travelfinder.hideReturnField();
        });
        $('#returntravel').on('click', function() {
            $("#y_roundTripRadbtntravel").prop("checked", true);
            $("#js-roundtrip #returntravel").addClass("color-tab-active");
            $("#js-roundtrip #returntravel").removeClass("color-tab-disble");
            $("#js-roundtrip #onewaytravel").removeClass("color-tab-active");
            $("#js-roundtrip #onewaytravel").addClass("color-tab-disble");
            ACC.travelfinder.showReturnField();
            ACC.travelfinder.addReturnFieldValidation($(".y_travelFinderDatePickerReturning"), $(".y_travelFinderDatePickerDeparting").val());
        });
    },

    addAccommodationLocation: function() {
        $('#fromtravelLocation').change(function() {
            var location = $(this).val();
            $('#accommodationtraLocation').val('tsa');
        });
    },

    bindPackageTravelWithChildrenButton: function() {
        // Event handler for 'travelWithChildrenButton'
        $(".y_travelFinderChildrenTrigger").click(function() {
            $(this).hide();
            $(ACC.travelfinder.componentParentSelector + " .y_travelFinderTravelingChildren").removeClass("hidden");
            $("#y_travelFinderTravelingWithChildren").val("true");
        });
    },

    setVehicleTravelRouteCode: function() {
        var tripType = $(".y_SelectTripTypetravel.color-tab-active").val();
        var fromLocation = $(".y_travelFinderOriginLocationCode").val();
        var toLocation = $(".y_travelFinderDestinationLocationCode").val();
        $('#travelvehicleTravelRouteCode_0').val(fromLocation + "-" + toLocation);
        if (tripType == "RETURN") {
            $('#travelvehicleTravelRouteCode_1').val(toLocation + "-" + fromLocation);
        }
    },

    travellingWithVehicleCheckBox: function() {
        $("#traveltravellingWithVehiclecheckbox").change(function() {
            if (this.checked) {
                $(".travelvehicleDetails").removeClass("hidden");
                $('#travely_standardRadbtn_0').prop('checked', true);
                $('#travely_standardRadbtn_0').trigger('click');
            } else {
                $(".travelvehicleDetails").addClass("hidden");
                $('#travely_standardRadbtn_0').prop('checked', false);
                if ($('#travelvehicleinboundcheckbox').is(":checked")) {
                    $("#travelvehicleinboundcheckbox").trigger('click');
                }
            }
        });
    },

    selectVehicleType: function() {
        $('.travely_fareFinderVehicleTypeBtn').on('click', function() {
            $('.subOversizeVehicle').addClass("hidden");
            var id = $(this).attr('id');
            var vehicleType = $(this).val();
            var buttonIndex = id.charAt(id.length - 1);
            ACC.travelfinder.refreshVehicles(vehicleType, buttonIndex);
        });
    },

    selectOversizeVehicleType: function() {
             $('#over9Ft_0').change(function() {
                if (this.checked && document.getElementById("userType").value=="guestUser") {
                   $('#fareFinderFindButton').attr('disabled', 'disabled');
                  }else{
                   $('#fareFinderFindButton').removeAttr('disabled');
                 }
             });
             var vehicleradios_0 = document.getElementsByName('vehicleInfoForm.vehicleInfo[0].vehicleType.code');
              $(vehicleradios_0).change(function() {
                if (this.checked &&  this.id=='otherOversize_0') {
                   $('#fareFinderFindButton').attr('disabled', 'disabled');
                   $('#vehiclewide_0').hide();
                   $('#other-input_0').show();

                  }else{
                   if(!$('#otherOversize_1').is(':checked')){
                    $('#fareFinderFindButton').removeAttr('disabled');
                   }
                   $('#vehiclewide_0').show();
                   $('#other-input_0').hide();
                }
             });
             $('#over9Ft_1').change(function() {
                if (this.checked && document.getElementById("userType").value=="guestUser") {
                    $('#fareFinderFindButton').attr('disabled', 'disabled');
                }else{
                    $('#fareFinderFindButton').removeAttr('disabled');
                }
             });
              $('#under9Ft_0').change(function() {
                if (this.checked) {
                  if(!$('#otherOversize_0').is(':checked') && !$('#otherOversize_1').is(':checked')){
                    $('#fareFinderFindButton').removeAttr('disabled');
                  }
                }else{
                    $('#fareFinderFindButton').attr('disabled', 'disabled');
                }
             });
             var vehicleradios_1 = document.getElementsByName('vehicleInfoForm.vehicleInfo[1].vehicleType.code');
              $(vehicleradios_1).change(function() {
                if (this.checked &&  this.id=='otherOversize_1') {
                   $('#fareFinderFindButton').attr('disabled', 'disabled');
                   $('#vehiclewide_1').hide();
                   $('#other-input_1').show();

                  }else{
                   if(!$('#otherOversize_0').is(':checked')){
                    $('#fareFinderFindButton').removeAttr('disabled');
                   }
                   $('#vehiclewide_1').show();
                   $('#other-input_1').hide();
                }
             });
             $('#under9Ft_1').change(function() {
                if (this.checked) {
                  if(!$('#otherOversize_0').is(':checked') && !$('#otherOversize_1').is(':checked')){
                    $('#fareFinderFindButton').removeAttr('disabled');
                  }
                }else{
                    $('#fareFinderFindButton').attr('disabled', 'disabled');
                }
             });
        },

    refreshVehicles: function(vehicleType, index) {
        $('#travely_vehicleSubTypeSelect_' + index + ' option').removeClass('hidden');
        $('#travely_vehicleSubTypeSelect_' + index).val('-1');
        $('#travely_vehicleSubTypeSelect_' + index + ' option[value=-1]').removeClass();
        $('#travely_vehicleSubTypeSelect_' + index + ' option[value=-1]').addClass(vehicleType);
        $('#travely_vehicleSubTypeSelect_' + index + ' option[value=-1]').attr('selected', 'selected');
        $('#travely_vehicleSubTypeSelect_' + index + ' option').each(function() {
            if (!$(this).hasClass(vehicleType)) {
                $(this).addClass('hidden');
            }
        })
    },

    convertVehicleDimensionInMeters: function() {
        $('.travellengthInput')
            .keyup(
                function(e) {
                    var lengthText = $.trim($(this).val());
                    if (lengthText.indexOf(".") != -1 || e.which < 48 || e.which > 57) {
                        $('#travellengthInMtrs_' + index)
                            .val(
                                ACC.validationMessages.travelfinder.message('error.farefinder.roundTo.nextFoot'));
                        var errorMsg = $('#travellengthInMtrs_' + index)
                            .val();
                        $('#travellengthInMtrs_' + index).html(errorMsg);
                    } else {
                        var id = $.trim($(this).attr('id'));
                        var index = id.charAt(id.length - 1);
                        var meters = lengthText / 3.2808;
                        var lengthInMtrs = meters.toFixed(2);
                        $('#travellengthInMtrs_' + index).html(
                            "(" + lengthInMtrs + " Mts)");
                    }
                })

        $('.travelheightInput')
            .keyup(
                function(e) {
                    var heightText = $.trim($(this).val());
                    if (heightText.indexOf(".") != -1 || e.which < 48 || e.which > 57) {
                        $('#travelheightInMtrs_' + index)
                            .val(
                                ACC.validationMessages.travelfinder.message('error.farefinder.roundTo.nextFoot'));
                        var errorMsg = $('#travelheightInMtrs_' + index)
                            .val();
                        $('#travelheightInMtrs_' + index).html(errorMsg);
                    } else {
                        var id = $.trim($(this).attr('id'));
                        var index = id.charAt(id.length - 1);
                        var meters = heightText / 3.2808;
                        var heightInMtrs = meters.toFixed(2);
                        $('#travelheightInMtrs_' + index).html(
                            "(" + heightInMtrs + " Mts)");
                    }
                })

        $('.travelwidthInput')
            .keyup(
                function(e) {
                    var widthText = $.trim($(this).val());
                    if (widthText.indexOf(".") != -1 || e.which < 48 || e.which > 57) {
                        $('#travelwidthInMtrs_' + index)
                            .val(
                                ACC.validationMessages.travelfinder.message('error.farefinder.roundTo.nextFoot'));
                        var errorMsg = $('#travelwidthInMtrs_' + index)
                            .val();
                        $('#travelwidthInMtrs_' + index).html(errorMsg);
                    } else {
                        var id = $.trim($(this).attr('id'));
                        var index = id.charAt(id.length - 1);
                        var meters = widthText / 3.2808;
                        var widthInMtrs = meters.toFixed(2);
                        $('#travelwidthInMtrs_' + index).html(
                            "(" + widthInMtrs + " Mts)");
                    }
                })

    },

    dangerousGoodCheckBox: function() {
        $("#traveldangerousgoodoutboundcheckbox, #traveldangerousgoodinboundcheckbox").change(function() {
            if (this.checked) {
                $(".dangerousgoodsDiv").removeClass("sr-only");
            } else {
                $(".dangerousgoodsDiv").addClass("sr-only");
            }

        });
    },

    returnwithdifferentvehicle: function() {
        $(".y_SelectTripTypetravel").click(function() {
            var id = $(this).attr('id');
            if (id == "returntravel") {
                $(".traveldifferentVehicleCheckBox").removeClass("hidden");
                $(".traveldangerousGoodReturnCheckBox").removeClass("hidden");
                $("#y_hotel-part-stay").prop('disabled', false);
            } else {
                if ($('#travelvehicleinboundcheckbox').is(":checked")) {
                    $("#travelvehicleinboundcheckbox").trigger('click');
                } // Unchecks 'Return with Different Vehicle' checkbox
                $(".traveldifferentVehicleCheckBox").addClass("hidden");
                $(".traveldangerousGoodReturnCheckBox").addClass("hidden");
                $(".travelvehicle_1").addClass("hidden");
                $("#y_hotel-part-stay").prop('disabled', true);

            }
        });
    },

    vehicleinboundcheckbox: function() {
        $("#travelvehicleinboundcheckbox").change(function() {
            if (this.checked) {
                $(".travelvehicle_1").removeClass("hidden");
                $('#travely_standardRadbtn_1').prop('checked', true);
                $('#travely_standardRadbtn_1').trigger('click');
            } else {
                $(".travelvehicle_1").addClass("hidden");
                $('#travely_standardRadbtn_1').prop('checked', false);
                $('#travely_commercialRadbtn_1').prop('checked', false);
                $('#travely_overSizeRadbtn_1').prop('checked', false);

            }

        });
    },

    sidecarcheckbox: function() {
        $(".travely_subVehicle").on('change', function() {
            var selected = $(this).find("option:selected").val();
            var vehicleToSkip = document.getElementById("travelskipVehicle").value;
            var id = $(this).attr('id');
            var index = id.charAt(id.length - 1);
            if (vehicleToSkip.includes(selected)) {
                $('.travelsidecarCheckBox_' + index).removeClass("hidden");
                $('.travelvehicleDimensions_' + index).find('input').val(0);
                $('.travelvehicleDimensions_' + index).addClass("hidden");
                $('#travellengthInMtrs_' + index).html("");
                $('#travelheightInMtrs_' + index).html("");
                $('#travelwidthInMtrs_' + index).html("");
            } else {
                if (!$('.travelsidecarCheckBox_' + index).hasClass("hidden")) {
                    $('#travelsidecarCheckBox_' + index).prop('checked', false);
                    $('.travelsidecarCheckBox_' + index).addClass("hidden");
                    $('.travelvehicleDimensions_' + index).removeClass("hidden");
                }
            }
        });
    },

  showChangeBookingForm:function() {
       $("#changeBookingFormButton").on('click',function(e){
            $('#y_changeBookingFormModal').modal('show');
        });
         if($("#changeBookingErrorFlag").val() =='true') {
            $('#y_changeBookingFormModal').modal('show');
         }
     },

    showInternalCommentsForm:function() {
        $("#internalCommentsButton").on('click',function(e) {
            $("#y_asmInternalCommentsModal").modal('show');
         });

        $(document).on("click",".y_addInternalComments",function() {
            var modal =$(this).closest("#y_asmInternalCommentsModal");
            var form = $(this).closest("#y_AddComments");
                var url = form.attr("action");
                var orderCode = form.find("#orderCode").val();
                var comment = form.find("#comment").val();
             $.when(ACC.services.addInternalComment(url, orderCode, comment)).then(function(response) {
                    if(response && response.content) {
                        modal.find(".y_asmCommentContent").replaceWith(response.content);
                        form.find("#comment").val("");
                    }
             });
        });

        $(".add-supplier-comments").on('click',function(e) {
            e.preventDefault();
            var addSupplierCommentURL = $(this).attr("href");
            var getSupplierCommentURL = "/view/AsmCommentsComponentController/get-supplier-comment";

            var modal =$("#y_asmSupplierCommentsModal");
            var code = $(this).data("code");
            var type = $(this).data("type");
            var ref = $(this).data("ref");

            var orderCode = modal.find("#orderCode").val();

            $.when(ACC.services.getSupplierComments(getSupplierCommentURL, orderCode, code, ref, type)).then(function(response) {
                if(response && response.content) {
                    modal.find(".y_asmCommentContent").replaceWith(response.content);

                }
                 $("#y_asmSupplierCommentsModal").modal('show');
            });
            $('#y_asmSupplierCommentsModal').find("#y_AddSupplierComments").attr("action", addSupplierCommentURL);
            $('#y_asmSupplierCommentsModal').find("#code").val(code);
            $('#y_asmSupplierCommentsModal').find("#ref").val(ref);
        });

        $(document).on("click",".y_addSupplierComments",function(e) {
            e.preventDefault();
            var modal = $(this).closest("#y_asmSupplierCommentsModal");
            var form = $(this).closest("#y_AddSupplierComments");
            var url = form.attr("action");
            var orderCode = form.find("#orderCode").val();
            var code = form.find("#code").val();
            var ref = form.find("#ref").val();
            var comment = form.find("#comment").val();
             $.when(ACC.services.addSupplierComment(url, code, ref ,orderCode, comment)).then(function(response) {
                    if(response && response.content) {
                        modal.find(".y_asmCommentContent").replaceWith(response.content);
                        form.find("#comment").val("");
                    }
             });
        });
     },

    bindPackageFareFinderLocationEvents: function() {
        // validation events bind for y_travelFinderOriginLocation field
        $(".y_travelFinderOriginLocation").keydown(function(e) {
            if ((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_travelFinderOriginLocationCode").val() == '') {
                if ((e.keyCode || e.which) == 13) {
                    e.preventDefault();
                }

                // select the first suggestion
                var suggestions = $(this).parent().find(".autocomplete-suggestions");
                if ($(this).val().length >= 3 && suggestions && suggestions.length != 0) {
                    var firstSuggestion = suggestions.find("a:eq( 1 )");
                    firstSuggestion.click();
                }
                $(this).valid();

            }
        });

        $(".y_travelFinderOriginLocation").autosuggestion({
            suggestionFieldChangedCallback: function() {
                $(".y_travelFinderOriginLocation").valid();
            }
        });

        $(".y_travelFinderOriginLocation").blur(function(e) {
            $(".y_travelFinderOriginLocation").valid();
        });

        $(".y_travelFinderOriginLocation").focus(function(e) {
            $(this).select();
        });

        // validation events bind for y_travelFinderDestinationLocation field
        $(".y_travelFinderDestinationLocation").keydown(function(e) {
            if ((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_travelFinderDestinationLocationCode").val() == '') {
                if ((e.keyCode || e.which) == 13) {
                    e.preventDefault();
                }

                // select the first suggestion
                var suggestions = $(this).parent().find(".autocomplete-suggestions");
                if ($(this).val().length >= 3 && suggestions && suggestions.length != 0) {
                    var firstSuggestion = suggestions.find("a:eq( 1 )");
                    firstSuggestion.click();
                }
                $(this).valid();

            }
        });

        $(".y_travelFinderDestinationLocation").autosuggestion({
            suggestionFieldChangedCallback: function() {
                if ($(".y_travelFinderDestinationLocation").length == 1) {
                    $(".y_travelFinderDestinationLocation").valid();
                }
            }
        });

        $(".y_travelFinderDestinationLocation").blur(function(e) {
            $(".y_travelFinderDestinationLocation").valid();
        });

        $(".y_travelFinderDestinationLocation").focus(function(e) {
            $(this).select();
        });
    },

    addDatePickerForFareFinder: function($departureDateField, $returnDateField) {

        var todayDate = new Date();
        $departureDateField.datepicker({
            minDate: todayDate,
            maxDate: '+1y',
            dateFormat: 'M dd yy',

            beforeShow: function(input) {

                setTimeout(function() {
                    if ($returnDateField.length) {
                        $departureDateField.rules('remove')
                    }

                }, 0);
            },
            onClose: function(selectedDate) {
                // add validation to departure date

                setTimeout(function() {
                    ACC.travelfinder.addDepartureFieldValidation($departureDateField)
                }, 0);
                // add validation to return date, only if it's visible and
                // validate the current field
                if ($returnDateField.is(":visible")) {
                    if ($(this).valid()) {
                        ACC.travelfinder.setReturnFieldMinDate($returnDateField, selectedDate, true);
                        // change the validation of the 'return date' to
                        // have a minDate of the 'from date'

                        setTimeout(function() {
                            ACC.travelfinder.addReturnFieldValidation($returnDateField, selectedDate)
                        }, 0);
                    } else {
                        ACC.travelfinder.setReturnFieldMinDate($returnDateField, todayDate, false);
                        // change the validation of the 'return date' to
                        // have a minDate of the 'from date'

                        setTimeout(function() {
                            ACC.travelfinder.addReturnFieldValidation($returnDateField, ACC.travelcommon.getTodayUKDateFareFinder())
                        }, 0);

                    }
                } else {
                    $(this).valid();
                }
            }
        });
        if ($returnDateField != undefined) {
            $returnDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'M dd yy',
                beforeShow: function(input) {

                    setTimeout(function() {
                        $returnDateField.rules('remove')
                    }, 0);
                },
                onClose: function(selectedDate) {
                    if ($departureDateField.valid()) {
                        ACC.travelfinder.setReturnFieldMinDate($returnDateField, $departureDateField.val(), true);
                        // change the validation of the 'return date' to have a
                        // minDate of the 'departure date'

                        setTimeout(function() {
                            ACC.travelfinder.addReturnFieldValidation($returnDateField, $departureDateField.val())
                        }, 0);

                    } else {
                        ACC.travelfinder.setReturnFieldMinDate($returnDateField, todayDate, false);
                        // change the validation of the 'return date' to have a
                        // minDate of the 'from date'

                        setTimeout(function() {
                            ACC.travelfinder.addReturnFieldValidation($returnDateField, ACC.travelcommon.getTodayUKDateFareFinder())
                        }, 0);

                    }
                    $(this).valid();
                }
            });
        }

        $('#departingDateTime').keydown(function(e) {
            e.preventDefault();
            return false;
        });
        $('#returnDateTime').keydown(function(e) {
            e.preventDefault();
            return false;
        });


    },

    addDepartureFieldValidation: function($departureDateField) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();

        $departureDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateUK,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.validationMessages.travelfinder.message('error.farefinder.departure.date')
            }
        });
    },

    setReturnFieldMinDate: function($returnDateField, selectedDate, isUKDate) {
        var selectedUSDate = new Date();
        if (selectedDate) {
            selectedUSDate = selectedDate;
            if (isUKDate) {
                selectedUSDate = ACC.travelcommon.convertToUSDateFareFinder(selectedDate);
            }
        }
        var minDateForReturnDate = ACC.travelcommon.addDaysToUsDate(selectedUSDate, ACC.travelfinder.minRelativeReturnDate);
        $returnDateField.datepicker("option", "minDate", minDateForReturnDate);
    },

    addReturnFieldValidation: function($returnDateField, selectedMinDate) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();
        if (selectedMinDate) {
            minDateUK = selectedMinDate;
        }

        if (!$returnDateField.length) {
            return;
        }

        var selectedUSDate = ACC.travelcommon.convertToUSDateFareFinder(minDateUK);
        var minDateForReturnDate = ACC.travelcommon.addDays(selectedUSDate, ACC.travelfinder.minRelativeReturnDate);

        $returnDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateForReturnDate,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.validationMessages.travelfinder.message('error.farefinder.arrival.date'),
                dateGreaterEqualTo: ACC.validationMessages.travelfinder.message('error.farefinder.arrival.dateGreaterEqualTo')
            }
        });
    },

    hideReturnField: function() {
        var $returnField = $('.y_travelFinderReturnField');
        $returnField.hide("fast");
        $returnField.find("input").rules("remove");
    },

    showReturnField: function() {
        var $returnField = $('.y_travelFinderReturnField');
        $returnField.show("fast");
    },


    /// auto suggestion //
    getTravelOriginMatches: function(travelSectors, locationText) {
        locationText = locationText.toLowerCase();
        var filterTravelSectors = new Object();

        if (!$.isEmptyObject(travelSectors)) {
            var matchingTravelSectors = [];
            for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                var travelSector = travelSectors[travelSectorIndex];
                if (travelSector.origin != null && travelSector.origin.code.toLowerCase() == locationText) {
                    matchingTravelSectors.push(travelSector);
                    break;
                }
            }


            if ($.isEmptyObject(matchingTravelSectors)) {
                for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                    var travelSector = travelSectors[travelSectorIndex];
                    if (travelSector.origin != null && travelSector.origin.name.toLowerCase().indexOf(locationText) != -1) {
                        matchingTravelSectors.push(travelSector);
                    }
                }
            }

            if ($.isEmptyObject(matchingTravelSectors)) {
                for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                    var travelSector = travelSectors[travelSectorIndex];
                    if (travelSector.origin != null && (travelSector.origin.location.code.toLowerCase() == locationText || travelSector.origin.location.name.toLowerCase().includes(locationText))) {
                        matchingTravelSectors.push(travelSector);
                    }
                }
            }

            for (var travelSectorIndex = 0; travelSectorIndex < matchingTravelSectors.length; travelSectorIndex++) {
                var travelSector = matchingTravelSectors[travelSectorIndex];
                var code = travelSector.origin.code;
                if (!filterTravelSectors[code]) {
                    filterTravelSectors[code] = travelSector;
                }
            }
        }

        return filterTravelSectors;
    },

    getTravelDestinationMatches: function(travelSectors, locationText) {
        locationText = locationText.toLowerCase();
        var filterTravelSectors = new Object();

        if (!$.isEmptyObject(travelSectors)) {
            var matchingTravelSectors = [];
            for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                var travelSector = travelSectors[travelSectorIndex];
                if (travelSector.destination != null && travelSector.destination.code.toLowerCase() == locationText) {
                    matchingTravelSectors.push(travelSector);
                }
            }

            if ($.isEmptyObject(matchingTravelSectors)) {
                for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                    var travelSector = travelSectors[travelSectorIndex];
                    if (travelSector.destination != null && travelSector.destination.name.toLowerCase().indexOf(locationText) != -1) {
                        matchingTravelSectors.push(travelSector);
                    }
                }
            }
            if ($.isEmptyObject(matchingTravelSectors)) {
                for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
                    var sector = travelSectors[travelSectorIdx];
                    if (sector.destination != null &&
                        (sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
                        matchingTravelSectors.push(sector);
                    }
                }
            }

            if (!$.isEmptyObject(matchingTravelSectors))
                filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0];
        }

        return filterTravelSectors;
    },

    getAllTravelSectors: function() {
        $(document).on("keyup", ".y_travelFinderOriginLocation", function(e) {
            // ignore the Enter key
            if ((e.keyCode || e.which) == 13) {
                return false;
            }

            var locationText = $.trim($(this).val());
            ACC.travelfinder.fireQueryToFetchAllTravelSectors(locationText);
        });
    },

    fireQueryToFetchAllTravelSectors: function(locationText) {
        if (locationText.length >= 3) {
            if ($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion")) {
                $.when(ACC.services.getTravelSectorsAjax()).then(function(data) {
                    ACC.travelfinder.allTravelSectors = data.travelRoute;
                    ACC.travelfinder.terminalcacheversion = data.terminalsCacheVersion;
                    localStorage.setItem("travelSector", JSON.stringify(ACC.travelfinder.allTravelSectors));
                    localStorage.setItem("terminalCacheVersion", ACC.travelfinder.terminalcacheversion);
                });
            } else {
                ACC.travelfinder.allTravelSectors = JSON.parse(localStorage.getItem("travelSector"));
            }
        }
    },

    // Get All Travel Sectors
    bindPackageTravelSectorsOriginAutoSuggest: function() {
        $(".y_travelFinderOriginLocation").autosuggestion({
            inputToClear: ".y_travelFinderDestinationLocation",
            autosuggestServiceHandler: function(locationText) {
                var suggestionSelect = "#y_travelFinderOriginLocationSuggestions";
                var travelSectors = ACC.travelfinder.allTravelSectors;
                var filterTravelSectors = ACC.travelfinder.getTravelOriginMatches(travelSectors, locationText);
                if (!$.isEmptyObject(filterTravelSectors)) {
                    var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                    var htmlStringEnd = '</ul> </div>';
                    var htmlStringBody = "";
                    var titleDisplayed = 0;
                    var htmlStringTitle = "";
                    $(suggestionSelect).html(htmlStringStart);

                    for (var code in filterTravelSectors) {
                        var filteredJson = filterTravelSectors[code];
                        if (titleDisplayed == 0) {
                            htmlStringTitle = ' <li class="parent"> <span class="title">' +
                                ' <a href="" class="autocomplete-suggestion inactiveLink" data-code= "' + filteredJson.origin.location.code +
                                '" data-suggestiontype="' + filteredJson.origin.location.locationType + '" >' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.location.superlocation[0].name + '</a> </span> </li>' + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
                                'data-code="' + filteredJson.origin.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.name + '(' + filteredJson.origin.code + ')</a> </li> ';
                            titleDisplayed = 1;
                        } else {
                            htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
                                'data-code="' + filteredJson.origin.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.name + '(' + filteredJson.origin.code + ')</a> </li> ';
                        }
                    }
                    var htmlContent = htmlStringStart + htmlStringTitle + htmlStringBody + htmlStringEnd;
                    $(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
                } else {
                    var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                    $(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
                }
            },
            suggestionFieldChangedCallback: function() {
                // clear the destination fields
                $(".y_travelFinderDestinationLocation").val("");
                $(".y_travelFinderDestinationLocationCode").val("");
            },
            attributes: ["Code", "SuggestionType"]
        });
    },

    // Suggestions/autocompletion for Destination location
    bindPackageTravelSectorsDestinationAutoSuggest: function() {
        $(".y_travelFinderDestinationLocation").autosuggestion({
            autosuggestServiceHandler: function(destinationText) {
                var suggestionSelect = "#y_travelFinderDestinationLocationSuggestions";
                var originCode = $(".y_travelFinderOriginLocationCode").val().toLowerCase();

                var travelSectors = ACC.travelfinder.allTravelSectors;
                if (travelSectors == null || travelSectors.length == 0) {
                    ACC.travelfinder.fireQueryToFetchAllTravelSectors(originCode);
                    travelSectors = ACC.travelfinder.allTravelSectors;
                }
                var matchedTravelSectors = ACC.travelfinder.getTravelSectorsWithOrigin(travelSectors, originCode);
                var filterTravelSectors = ACC.travelfinder.getTravelDestinationMatches(matchedTravelSectors, destinationText);

                if (!$.isEmptyObject(filterTravelSectors)) {
                    var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                    var htmlStringEnd = '</ul> </div>';
                    var htmlStringBody = "";
                    var titleDisplayed = 0;
                    $(suggestionSelect).html(htmlStringStart);

                    for (var code in filterTravelSectors) {
                        var filteredJson = filterTravelSectors[code];
                        if (titleDisplayed == 0) {
                            htmlStringBody = htmlStringBody + '<li class="parent"> <span class="title">' +
                                ' <a href="" class="autocomplete-suggestion inactiveLink" data-routetype="' + filteredJson.routeType + '" data-code= "' + filteredJson.destination.location.code +
                                '" data-suggestiontype="' + filteredJson.destination.location.locationType + '" >' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.location.superlocation[0].name + '</a> </span> </li>' + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
                                'data-code="' + filteredJson.destination.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.name + '(' + filteredJson.destination.code + ')</a> </li> ';
                            titleDisplayed = 1;
                        } else {
                            htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" date-routetype="' + filteredJson.routeType + '" ' +
                                'data-code="' + filteredJson.destination.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.name + '(' + filteredJson.destination.code + ')</a> </li> ';
                        }
                    }
                    var htmlContent = htmlStringStart + htmlStringBody + htmlStringEnd;
                    $(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
                } else {
                    var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                    $(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
                }
            },
            attributes: ["Code", "SuggestionType"]
        });
    },

    getTravelSectorsWithOrigin: function(travelSectors, originCode) {
        var matchingTravelSectors = [];
        if (!$.isEmptyObject(travelSectors)) {
            for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
                var travelSector = travelSectors[travelSectorIndex];
                if (travelSector.origin != null && (travelSector.origin.code.toLowerCase() == originCode || travelSector.origin.location.code.toLowerCase() == originCode)) {
                    matchingTravelSectors.push(travelSector);
                }
            }
        }
        return matchingTravelSectors;
    },

    // Suggestions/autocompletion for Origin location
    bindDestinationAutosuggest: function() {
        if (!$(".y_travelFinderLocation").is('input')) {
            return;
        }
        $(".y_travelFinderLocation").autosuggestion({
            //inputToClear: ".y_fareFinderDestinationLocation",
            autosuggestServiceHandler: function(locationText) {
                var suggestionSelect = "#y_travelFinderLocationSuggestions";
                // make AJAX call to get the suggestions
                $.when(ACC.services.getSuggestedAccommodationLocationsAjax(locationText)).then(
                    // success
                    function(data, textStatus, jqXHR) {
                        $(suggestionSelect).html(data.htmlContent);
                        if (data.htmlContent) {
                            $(suggestionSelect).removeClass("hidden");
                        }
                    }
                );
            },
            suggestionFieldChangedCallback: function() {
                $(".y_travelFinderLocation").valid();
            },
            attributes: ["Code", "SuggestionType", "Latitude", "Longitude", "Radius"]
        });
    },

    bindTravelFinderLocationEvents: function() {
        if (!$(".y_travelFinderLocation").is('input')) {
            return;
        }

        $(".y_travelFinderLocation").keydown(function(e) {
            if ((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_travelFinderLocationCode").val() == '') {
                if ((e.keyCode || e.which) == 13) {
                    e.preventDefault();
                }

                //select the first suggestion
                var suggestions = $(this).parent().find(".autocomplete-suggestions");
                if ($(this).val().length >= 3 && suggestions && suggestions.length != 0) {
                    var firstSuggestion = suggestions.find("a").first();
                    firstSuggestion.click();
                }
                $(this).valid();

            };
        });

        $(".y_travelFinderLocation").blur(function(e) {
            $(".y_travelFinderLocation").valid();
        });

        $(".y_travelFinderLocation").focus(function(e) {
            $(this).select();
            var locationString = $(".y_travelFinderLocationCode").val();
            var locationCode;
            if (locationString == "") {
                $('.y_travelRoute').empty()
                return;
            } else {
                if (locationString.indexOf('|') != -1) {
                    locationCode = locationString.split("|")[0];
                } else {
                    locationCode = locationString;
                }
            }
            $.when(ACC.services.getTravelRoutesForLocationAjax(locationCode)).then(
                function(data, textStatus, jqXHR) {
                    $('.y_travelRoute').empty()
                    $('.y_travelRoute').append($('<option value="" disabled selected>Select your option</option>>'));
                    $.each(data, function(i, item) {
                        $('.y_travelRoute').append($('<option>', {
                            value: item.code,
                            text: item.name
                        }));
                    });
                }
            );
        });

    },

    bindVacationDestinationLocationChange: function() {
        $(".y_vacationDestination").change(function() {
            var vacationDestinationCode = this.value;
            $(".y_travelFinderLocationCode").val(vacationDestinationCode);
            $(".y_accommodationFinderLocationSuggestionType").val("AIRPORTGROUP");
            $.when(ACC.services.getTravelRoutesForLocationAjax(vacationDestinationCode)).then(
                function(data, textStatus, jqXHR) {
                    $('.y_travelRoute').empty()
                    $('.y_travelRoute').append($('<option value="" disabled selected>Select your option</option>>'));
                    $.each(data, function(i, item) {
                        $('.y_travelRoute').append($('<option>', {
                            value: item.code,
                            text: item.name
                        }));
                    });
                }
            );
        });
    },

    bindVacationEditRouteAccordionPopulation: function() {
        var destinationCode = $('.y_accommodationDestinationLocation').val();
        if(destinationCode != null){
            ACC.travelfinder.sourceClickHander();

            var selectedTravelRouteCode = $(".y_travelFinderTravelRoute").val();
            if(selectedTravelRouteCode != undefined && selectedTravelRouteCode != "") {
                var selectedRouteName = $("#vacation-accordion2 .vacation-range-box").find("a[data-code="+selectedTravelRouteCode+"]");
                $("#vacation-accordion2 .destination-text").html(selectedRouteName.html());
            }
        }
    },

    bindTravelRouteSelection: function() {
      $(".y_travelRoute").change(function() {
          var travelRoute = this.value;
          var travelRouteName = $(".y_travelRoute option:selected").html();
          $(".y_travelFinderTravelRoute").val(travelRoute);
      })
    },

    showChildDropDown: function() {
        $(".y_childSelect").change(function() {

            var childQty = Number($(this).val());
            var childIndex = $('#childIndex').val();
            var path = $(this).parent();
            var pathParent = path;
            if(path.find(".y_roomStay").length == 0){
              path = path.parent();
              pathParent = $(this).closest(".ui-accordion-content");
            }
            pathParent.find("#childAgeDropdownPackage:first").empty();
            if(path.find(".y_roomStay").length){
            var pathName = path.find(".y_roomStay").html().replace(/(\r\n\t|\n|\r\t)/gm, "").trim();
            var childAgeOption = "";
            var childAgeSelect = "";

            for (var j = 0; j <= 18; j++) {
                var option = '<option value="' + j + '"> ' + j + ' </option>';
                childAgeOption = childAgeOption + option;
            }

            for (var i = 0; i < childQty; i++) {
                var j = i + 1;
                var label = '<div class="col-xs-6 col-sm-6"><label for="y_childSelectPackage' + j + '"> child-' + j + ' AGE </label>'
                var formSelect = '<select class="form-control" id="y_childSelectPackage' +
                    j +
                    '" name= "' + pathName + '.ages[' + i + ']" >' +
                    childAgeOption +
                    '</select></div>';
                var data = label + formSelect;
                pathParent.find("#childAgeDropdownPackage:first").append(data)
            }
            }
        });
    },


    loadAccessibilityNeeds: function() {
        if ($('input[id="y_packageCheckAccessibilityNeed"]:checked').length > 0) {
            ACC.travelfinder.showAccessibilityNeedsPackage();
        } else {
            $("#y_packageAccessibilityPlaceholderParent").addClass("sr-only");
        }
        $("div[id^='accordion-accessibility-']").accordion({
            header: "h3",
            collapsible: true,
            active: false
        });
    },

    loadOtherAccessibilityNeedTextBoxPackage : function () {
        $("input[id^='specialServiceRequest_OTHR_']").each(function(){
            var elem = $(this).parent().parent().find("div[id^='other_accessibility_need_']");
            if (this.checked) {
                elem.addClass('required-accessibility_need');
            elem.removeClass("hidden");
          } else {
                elem.removeClass('required-accessibility_need');
            elem.addClass("hidden");
          }
        });
    },

    packageCheckAccessibilityNeed: function() {
        $("#y_packageCheckAccessibilityNeed").change(function() {
            if (this.checked) {
                ACC.travelfinder.showAccessibilityNeedsPackage();
            } else {
                $("#y_packageAccessibilityPlaceholderParent").addClass("sr-only");
            }
            $("div[id^='accordion-accessibility-']").accordion({
                header: "h3",
                collapsible: true,
                active: false
            });
        });
    },
    showAccessibilityNeedsPackage: function() {
        $.when(ACC.services.fetchAccessibilityPerPAXPackage($('#y_accessibilityneeds').serialize())).then(
            function(data) {
                $("#y_packageAccessibilityPlaceholder").replaceWith(data.htmlContent);
            }
        );
        ACC.farefinder.bindAccessibilitySelectorButton();
        ACC.farefinder.bindClearSelectionButton();
        ACC.farefinder.bindOtherAccessibilityNeedTextBox();
        $("#y_packageAccessibilityPlaceholderParent").removeClass("sr-only");
    },

    showHideDivsPackage: function() {

        $("input[id^='over7HeightPackage_']").change(function() {
            var str = $(this).attr('id');
            var ret = str.split("_");
            if (document.getElementById("over7HeightPackage_" + ret[1]).checked) {
                $('#Over7heightPackageDiv_' + ret[1]).removeClass('d-none');
            }
        });


        $("input[id^='under7HeightPackage_']").change(function() {
            var str = $(this).attr('id');
            var ret = str.split("_");
            if (document.getElementById("under7HeightPackage_" + ret[1]).checked) {
                $('#Over7heightPackageDiv_' + ret[1]).addClass('d-none');
            }
        });

        $("input[id^='over20LengthPackage_']").change(function() {
            var str = $(this).attr('id');
            var ret = str.split("_");
            if (document.getElementById("over20LengthPackage_" + ret[1]).checked) {
                $('#Over20lengthDivPackage_' + ret[1]).removeClass('d-none');
            }
        });

        $("input[id^='under20LengthPackage_']").change(function() {
            var str = $(this).attr('id');
            var ret = str.split("_");
            if (document.getElementById("under20LengthPackage_" + ret[1]).checked) {
                $('#Over20lengthPackageDiv_' + ret[1]).addClass('d-none');
            }
        });

        var coll = document.getElementsByClassName("collapsible");
        coll.onclick = function() {
            var divValue = document.getElementsByClassName('collapsible').getAttribute('value');
            $(suggestionSelect).html(htmlStringStart);
        };
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }

    },

    showRefundReasonCodeForm: function() {
        $("#refundReasonCodeButton").on('click', function(e) {
            $('#y_showRefundReasonCodeModal').modal('show');
        });

        if ($("#hasErrorFlag").val() == 'true') {
            $('#y_showRefundReasonCodeModal').modal('show');
        }
    },

    setVehicleDimensionValues: function() {
        $("#fareFinderFindButton").on('click', function(e) {
            if ($("input[id^='over7HeightPackage_']").is(':checked')) {
                $("input[id^='over7HeightPackage_']").each(function() {
                    if ($(this).is(':checked')) {
                        var str = $(this).attr('id');
                        var ret = str.split("_");
                        var height = $("#Over7heightboxinft_" + ret[1]).val();
                        $("#height_" + ret[1]).val(height);
                        var heightLimit = $("#heightLimit").val();
                        if (height >= heightLimit) {
                            e.preventDefault();
                            $('#y_showLimitedSailingsModal').modal('show');
                        }
                    }

                });
            }

            if ($("input[id^='over20LengthPackage_']").is(':checked')) {
                $("input[id^='over20LengthPackage_']").each(function() {
                    if ($(this).is(':checked')) {
                        var str = $(this).attr('id');
                        var ret = str.split("_");
                        var length = $("#Over20lengthboxinft_" + ret[1]).val();
                        $("#length_" + ret[1]).val(length);
                        var lengthLimit = $("#lengthLimit").val();
                        if (length >= lengthLimit) {
                            e.preventDefault();
                            $('#y_showLimitedSailingsModal').modal('show');
                        }
                    }
                });
            }
        });
    }
};
