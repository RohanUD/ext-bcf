ACC.activitydetails = {
    isScheduleAvailable : $(".y_addActivityToCartData").find("#isActivityHasSchedules").val(),
	_autoloadTracc : [
	     "init",
	     "bindActivityDetailsDatePicker",
	     "bindAddActivity",
	     "bindActivityCalendar",
	     "bindActivityAvailabilityChange"
	      ],
    init: function() {
        //hide all tour time options on load
        $(window).load(function(){
            $(".activities-details-select .y_startTime .selectpicker").find("li").css("display","none");
        })
    },

    bindValidationForActivityDetails : function(){
        $('#addActivityToCartData').validate({
         errorElement: "span",
         errorClass: "fe-error",
         onfocusout: function(element) {
             $(element).valid();
         },
//         submitHandler: function (form) {
//           $(".custom-error-4").addClass("hidden");
//           if($("#currentcondition-accordion1").find(".dropdown-text strong").text() == "CITY"){
//             $(".custom-error-4 span").html(ACC.validationMessages.currentcondition.message("error.farefinder.currentcondition.from.location"));
//             $(".custom-error-4").removeClass("hidden");
//             return false;
//           }
//
//           if($("#currentcondition-accordion2").find(".dropdown-text strong").text() == "CITY"){
//             $(".custom-error-4 span").html(ACC.validationMessages.currentcondition.message("error.farefinder.currentcondition.to.location"));
//             $(".custom-error-4").removeClass("hidden");
//             return false;
//           }
//
//           form.submit();
//         },
         onkeyup: false,
         onclick: false
     });

     $("#activityDate").rules("add", {
                 required: true,
                 messages: {
                     required: "This is required"
                 }
             });


    },
	bindActivityDetailsDatePicker : function() {
		$(".y_activityDate").datepicker({
			minDate : new Date(),
			dateFormat : 'M dd yy'
		});
	},

    bindAddActivity : function() {
         $("#addActivityToCart").on('click', function(e) {

             $('.error-message').remove('');
             var errors=[];

              if (ACC.common.shallCartBeRemoved("BOOKING_ACTIVITY_ONLY") == false) {
                 return false;
              }

             var formElement = $(this).closest(".y_addActivityToCartData");
             if(!isPaxSelected(formElement,'adult') && !isPaxSelected(formElement,'youth') && !isPaxSelected(formElement,'child') && !isPaxSelected(formElement,'infant'))
             {
               formElement.find('.y_adultGuestTypeDiv').closest('.row').after('<div class="error-message fe-error" style="clear: left;">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_passengers')+'</div>');
               errors.push('y_adultSelect');
             }

             if(formElement.find(".datepicker-input").val() == '')
             {
                 formElement.find('.vacation-calen-box').after('<span class="error-message fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_checkin_date')+'</span>');
                 errors.push('vacation-calen-box');
             }

             if(formElement.find("#startTime").length != 0 && formElement.find(".activity-tour-time").is(':visible')){
                 if(formElement.find("#startTime").val() == null || formElement.find("#startTime").val() == '' || $(".activities-details-select .y_startTime.btn-group .filter-option").text() === 'Tour time')
                 {
                     formElement.find('.activities-details-select').after('<span class="error-message fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_time')+'</span>');
                     errors.push('startTime');
                 }
             }
             if(errors.length!=0)
             {
                 return false;
             }

            var proceedWithAddToCart = false;
                var addActivityToCartForm = $("#addActivityToCartData");
            $.when(ACC.services.checkActivityAvailability(addActivityToCartForm)).always(function(response, textStatus, jqXHR){
                console.log("response:"+response)
                if(response === "ON_REQUEST") {
                    $("#y_activityAvailabilityChangeModal").show();
                } else {
                    proceedWithAddToCart=true;
                }
            });

            //hack for the issue where class cboxElement got attched to the modal which causes an arbitrary button to appear on screen
            if($("#y_activityAvailabilityChangeModal").hasClass("cboxElement")) {
                $("#y_activityAvailabilityChangeModal").removeClass("cboxElement");
            }

            if(!proceedWithAddToCart) {
                return false;
            }

         });
    },

    bindActivityCalendar : function() {
        $('#datepicker-activitydetail').on("focusout", function() {
            $.when(ACC.services.findActivitySchedulesAjax($("#activityCode").val(), $(".vacation-calen-year-txt").prop("outerText"))).then(function(response) {
                if(response == 0){
                    $(".activity-tour-time").hide();
                } else {
                    $(".activity-tour-time").show();

                    var strDate = $(".vacation-calen-year-txt").prop("outerText");
                    var date = new Date(strDate);

                    //get the original options that contains schedule data
                    var allSchedules = $("#startTime").find("option");

                    //hide all tour time options first
                    $(".activities-details-select .y_startTime .selectpicker").find("li").css("display","none");

                    //set the text to "Tour time" whenever the date changes
                    $(".activities-details-select button.selectpicker").find(".filter-option").text('Tour time');
                    $(".activities-details-select .dropdown-menu.inner.selectpicker li").removeClass('selected');
                    $(".activities-details-select .dropdown-menu.inner.selectpicker li[rel=0]").addClass('selected');
                    $("#addActivityToCartData").find("#startTime").val(null);

                    //filter the list (this is dynamically generated by datepicker) that matches the schedule date and time
                    $(".activities-details-select .y_startTime .selectpicker").find("li").filter(function(index) {

                        //always shows the first option
                        if(index === 0) {
                            return true;
                        }

                        var matchedIndex = -1;

                        allSchedules.each(function(innerIndex) {
                            var startDate = new Date($(this).attr("data-startdate"));
                            var endDate = new Date($(this).attr("data-enddate"));

                            if((startDate.getTime() <= date.getTime() && endDate.getTime() >= date.getTime()) ) {
                                if(index == innerIndex){
                                   matchedIndex = innerIndex;
                                   return;
                                }
                            }
                        })
                        return (index === matchedIndex);
                    }).css("display","block");
                }
                $('#check-in').removeClass('active');
                $('.activities-details-calender .activity-tour-date').removeClass('active');
            });
        });
	},

	bindActivityAvailabilityChange : function() {

	    $("#y_activityAvailabilityChangeModal .close,#y_activityAvailabilityChangeModal .btn-secondary").on("click",function(){
            $("#y_activityAvailabilityChangeModal").hide();
	    })

	    $("#y_activityAvailabilityChangeModal #y_proceedAddToCart").on("click",function(){
            $("#y_activityAvailabilityChangeModal").hide();
            $("#addActivityToCartData").submit();
	    });
	}
}

function isPaxSelected(formElement,paxType){
    if(formElement.find('[id^=y_][id$='+paxType+']').length > 0){
       return (formElement.find('[id^=y_][id$='+paxType+']').val()>0)
    }
    return false;
}
