<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
	<c:choose>
		<c:when test="${not empty validationError}">
			<json:property name="validationError">
				<spring:theme code="${validationError}" />
			</json:property>
		</c:when>
		<c:otherwise>
			<json:property name="activityInventoryResponseDatas">
				<table border="1">
					<c:forEach var="activityInventoryResponseData" items="${activityInventoryResponseDatas}">
						<tr align="left">
							<th colspan="9">${activityInventoryResponseData.activityName}(${activityInventoryResponseData.stockLevelType})</th>
						</tr>
						<tr align="right">
							<th></th>
							<th><spring:theme code="label.vacation.inventory.report.result.date" text="Date" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.time" text="Time" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.block" text="Block" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.sold" text="Sold" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.available" text="Available" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.stopSell" text="Stop Sell" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.blockRelease" text="Block Release" /></th>
							<th><spring:theme code="label.vacation.inventory.report.result.stockType" text="Stock Type" /></th>
						</tr>
						<c:choose>
                            <c:when test="${activityInventoryResponseData.stockLevelType eq 'ONREQUEST'}">
                                <tr align="left">
                                    <td></td>
                                    <td colspan="8"> Stock level is of type ONREQUEST. No Inventory records.</td>
                                </tr>
                            </c:when>
                            <c:when test="${empty activityInventoryResponseData.stockLevelInventories}">
                                <tr align="left">
                                    <td></td>
                                    <td colspan="8"> No Inventory records.</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="stockLevelInventoryResponseData" items="${activityInventoryResponseData.stockLevelInventories}">
                                    <tr align="right">
                                        <td></td>
                                        <td>${stockLevelInventoryResponseData.date}</td>
                                        <td>${stockLevelInventoryResponseData.time}</td>
                                        <td>${stockLevelInventoryResponseData.initialAvailable}</td>
                                        <td>${stockLevelInventoryResponseData.reserved}</td>
                                        <td>${stockLevelInventoryResponseData.available}</td>
                                         <td>${fn:toUpperCase(stockLevelInventoryResponseData.stopSaleStatus)}</td>
                                        <td>${stockLevelInventoryResponseData.releaseDays}</td>
                                        <td>${stockLevelInventoryResponseData.stockType}</td>
                                    </tr>
                                </c:forEach>
						    </c:otherwise>
                        </c:choose>
					</c:forEach>
				</table>
			</json:property>
		</c:otherwise>
	</c:choose>
</json:object>
