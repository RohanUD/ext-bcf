<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="deallisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/deallisting"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
	<div class="container row-offcanvas row-offcanvas-left">
		<div class="col-xs-12">
			<h2 class="h2">
				<spring:theme code="dealsearch.title" text="Please choose your deal" />
			</h2>
		</div>


							<div class="col-xs-12 " id="y_displaySortSelection">
								<div class="row">
									<div class="col-xs-3 ">
										<deallisting:dealSortSelect dealsearchParams="${dealsearchParams}" dealsResponseDataList="${dealsResponseDataList}" />
									</div>
								</div>
							</div>

<div class="accommodation-selection-wrap clearfix y_accommodationSelectionSection">
			<div class="col-xs-12 col-sm-9 y_nonItineraryContentArea">
			<cms:pageSlot position="DealRefinement" var="feature">
                    <cms:component component="${feature}" />
            </cms:pageSlot>
			<div class="col-xs-12">
<div class="viewResults">
<ul id="y_hotelResults" class="clearfix accommodation-items" aria-label="hotel results">
<deallisting:dealDetailsList dealsResponseDataList="${dealsResponseDataList}" />
</ul>
</div>
</div></div>

<div class="col-xs-12 col-sm-3">
				<aside id="sidebar" class="y_reservationSideBar reservation">
					<div class="main-wrap">
						<cms:pageSlot position="Reservation" var="feature" element="div">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					<div class="promotions hidden-xs">
						<cms:pageSlot position="SideContent" var="feature" element="section">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</aside>
			</div>
		</div>
	</div>
	<div class="y_customerReviewsModal"></div>
</template:page>
