<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:object escapeXml="false">
    <json:property name="result" value="${quoteMessage}"/>
    <spring:theme code="quote.create.success" var="successMsg"/>
    <spring:theme code="quote.create.error" var="errorMsg"/>
    <json:property name="success" value="${successMsg}"/>
    <json:property name="error" value="${errorMsg}"/>
</json:object>
