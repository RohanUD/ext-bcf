<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<%@ taglib prefix="refinement" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting/refinement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>
<json:object escapeXml="false">
   <json:property name="htmlContent">
      <c:if test="${not empty packageSearchResponseProperties}">
         <c:set var="resultsView" value="${fn:toUpperCase(resultsViewType)}" />
         <div class="row">
            <c:choose>
               <c:when test="${resultsView == 'LISTVIEW'}">
                  <packagelisting:packageListView propertiesListParams="${packageSearchResponseProperties}" />
               </c:when>
               <c:otherwise>
                  <packagelisting:packageGridView propertiesListParams="${packageSearchResponseProperties}" />
               </c:otherwise>
            </c:choose>
         </div>
      </c:if>
   </json:property>
   <json:property name="hasMoreResults" value="${hasMoreResults}" />
   <json:property name="totalNumberOfResults" value="${totalNumberOfResults}" />
   <json:property name="totalNumberOfResultsText">
      <c:choose>
         <c:when test="${totalNumberOfResults gt 0}">
            <spring:theme code="text.package.listing.found.package.number" arguments="${totalShownResults}, ${totalNumberOfResults}" />
         </c:when>
         <c:otherwise>
            <spring:theme code="text.package.listing.empty.list" />
         </c:otherwise>
      </c:choose>
   </json:property>
   <json:property name="paginationContent">
      <div class="row mb-5 clearfix">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mb-3">
            <p class="y_shownResultId font-weight-light font-italic show-page-number">
            <h3 class="h4">
               <spring:theme code="text.package.listing.shown.package.results" arguments="${startingNumberOfResults}, ${totalShownResults}, ${totalNumberOfResults}" />
            </h3>
            </p>
         </div>
         <pagination:jsonpagination paginationCss="y_showSpecificPagePackageResults"/>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 text-center">
            <div class="back-top-btn">
               <a href="#">
                  <span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span>
                  <strong>
                     <spring:theme code="text.accommodation.lsiting.back.to.top" text="Back To Top" />
                  </strong>
               </a>
            </div>
         </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <hr>
         </div>
      </div>
      </div>
   </json:property>
</json:object>
