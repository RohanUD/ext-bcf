<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<json:object escapeXml="false">
	<json:property name="htmlContent">
		<c:set var="maxPassengerQuantity" value="${adultMaxValue}" />
		<c:set var="accommodationAvailabilityResponse" value="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse}" />
<c:set var="editPackageUrl" value="${fn:escapeXml(request.contextPath)}/vacations/hotels/edit/${accommodationAvailabilityResponse.accommodationReference.accommodationOfferingCode }" />
		<div id="maxPassengerCount" class="hidden">${maxPassengerQuantity}</div>
		<div id="accommodationsQuantity" class="hidden">${accommodationsQuantity}</div>
		<div id="editPackageData" class="p-0">
		   <div class="padding-50 pb-3">
		<p><spring:theme code="text.tripwidget.stay"/></p>
                <fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkInDateTime}" var="parsedDepartDate" pattern="dd/MM/yyyy" />
                <fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />
                <fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkOutDateTime}" var="parsedReturnDate" pattern="dd/MM/yyyy" />
                <fmt:formatDate value="${parsedReturnDate}" var="formattedParsedReturnDate" pattern="E, MMM dd" />

                <div class="font-weight-bold">${formattedParsedDepartDate} <span class="ml-2"> <spring:theme code="text.tripwidget.to"/></span><span class="ml-2">${formattedParsedReturnDate}</span></div>
                 <fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkInDateTime}" var="parsedCheckInDateTime" pattern="dd/MM/yyyy" />
                <fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkOutDateTime}" var="parsedCheckOutDateTime" pattern="dd/MM/yyyy" />
                <c:set var="numberOfNights">
                    <fmt:parseNumber value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}" integerOnly="true"/>
                </c:set>
                <c:choose>
                    <c:when test="${numberOfNights eq 1}">
                        <spring:theme code="text.tripwidget.night" arguments="${numberOfNights}" />
                    </c:when>
                    <c:otherwise>
                        (<spring:theme code="text.tripwidget.nights" arguments="${numberOfNights}" />)
                    </c:otherwise>
                </c:choose>
                <div class="room-type-edit-box mt-4">
		<form:form commandName="travelFinderForm" action="${editPackageUrl}" method="GET" class="fe-validate form-background form-booking-trip" id="y_editPackageForm">
			<div class="row">


			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 package_edit">
                  <div class="edit pull-right" id="collapseEdit"><span class="bcf bcf-icon-edit"></span> edit</div>
             </div>





				<div class="col-md-12 checkVacationInAndCheckOutDiv">


					<div class="row mb-4">
						<div class="input-required-wrap col-lg-4 col-md-4 col-sm-3 col-xs-12">
							<label class="font-weight-light">
								<spring:theme code="text.vacation.packagefinder.checkindate.title" text="Check-in date" />
							</label>
							<input id="y_maxAllowedCheckInCheckOutDateDifference" type="hidden" value="${fn:escapeXml(maxAllowedCheckInCheckOutDateDifference)}" />
							<label class="sr-only" for="y_DatePickerCheckIn">
								<spring:theme var="checkInDatePlaceholderText" code="text.cms.accommodationfinder.checkin.date.placeholder" text="Check In" />
							</label>
							<form:input type="text" id="y_DatePickerCheckIn" value="${travelFinderForm.accommodationFinderForm.checkInDateTime }" path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkInDateTime" class=" bc-dropdown bc-dropdown--big  y_FinderDatePickerCheckIn" placeholder="Check In" />
						</div>
						<div class="input-required-wrap col-lg-4 col-md-4 col-sm-3 col-xs-12">
							<label class="font-weight-light">
								<spring:theme code="text.vacation.packagefinder.checkoutdate.title" text="Check-out date" />
							</label>
							<label class="sr-only" for="y_DatePickerCheckOut">
								<spring:theme var="checkOutDatePlaceholderText" code="text.cms.accommodationfinder.checkout.date.placeholder" text="Check Out" />
							</label>
							<form:input type="text" id="y_DatePickerCheckOut" value="${travelFinderForm.accommodationFinderForm.checkOutDateTime }" path="${fn:escapeXml(formPrefix)}accommodationFinderForm.checkOutDateTime" class="datePickerReturning bc-dropdown bc-dropdown--big  y_FinderDatePickerCheckOut"
								placeholder="Check Out" autocomplete="off" />
						</div>
					</div>





					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 y_passengerWrapper ">
						<label class="font-weight-light">
                        						<spring:theme code="text.vacation.packagefinder.numberofrooms.title" text="Number of rooms" />
                        					</label>
							<div class="input-group text-center mb-3 roomSelector">
								<span class="input-group-btn">
									<button class="btn btn-minus y_roomQtySelectorMinus" type="button" data-type="minus" data-field="">
										<span class="bcf bcf-icon-remove"></span>
									</button>
								</span>
                                <c:set var="noOfRoomsMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.number.rooms.max.count')}"/>
                                 <input type="hidden" id="noOfRoomsMax" name="noOfRoomsMax" value="${noOfRoomsMaxValue}"/>
								<form:input type="text" id="y_roomQuantity" class="form-control" value="${travelFinderForm.accommodationFinderForm.numberOfRooms }" min="0" max="${noOfRoomsMaxValue}" path="accommodationFinderForm.numberOfRooms" />
								<span class="input-group-btn">
									<button class="btn btn-plus y_roomQtySelectorPlus" type="button" data-type="plus" data-field="">
										<span class="bcf bcf-icon-add"></span>
									</button>
								</span>
							</div>
						</div>



					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bc-accordion edit">
                        <label class="font-weight-light">
                            Room1
                        </label>
						<div id="accordion">
							<h3>
								<spring:theme code="label.packagedetails.guestinfo.guest.dropdown.heading" text="Select guests" />
								<span class="custom-arrow"></span>
							</h3>
							<div class="vacationGuestUsers">
								<c:forEach var="roomCandidatesEntry" items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}" varStatus='idx'>
									<c:forEach var="entry" items="${roomCandidatesEntry.passengerTypeQuantityList}" varStatus="i">
										<div class="row">
											<div class="col-6 col-md-8 pr-4 y_passengerWrapper ">
												<div class="pass-fluid-left fluid-text">
													<label for="y_${fn:escapeXml(entry.passengerType.code)}"> ${fn:escapeXml(entry.passengerType.name)} </label>
												</div>
												<div class="input-group y_guestQtySelector text-center mb-3">
														<span class="input-group-btn">
															<button class="btn btn-minus y_guestQtySelectorMinus" type="button" data-type="minus" data-field="">
																<span class="bcf bcf-icon-remove"></span>
															</button>
														</span>
				                                        <c:set var="adultMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.adult.max.count')}"/>
														<form:input type="text" path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${i.index}].quantity" id="y_guestQuantity" class="form-control" value="${entry.quantity}" min="0" max="${adultMaxValue}" />
														<span class="input-group-btn">
															<button class="btn btn-plus y_guestQtySelectorPlus" type="button" data-type="plus" data-field="">
																<span class="bcf bcf-icon-add"></span>
															</button>
														</span>
														<form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" id="y_passengerCode" type="hidden" readonly="true" />
														<form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name" id="y_passengerTypeName" type="hidden" readonly="true" />
														<form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge" type="hidden" readonly="true" />
														<form:input path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge" type="hidden" readonly="true" />
													</div>
											</div>
										</div>
									</c:forEach>
								</c:forEach>
							</div>

						</div>
					</div>




                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label>&nbsp;</label>
									<a id="editPackageButton" class="btn btn-primary confirm-btn confirm-btn--blue editPackage">
                                       <spring:theme code="label.packagedetails.link.submitquote" text="Submit" />
                                    </a>
								</div>


				</div>
			</div>
			</div>
		</form:form>
		</div>
		<div class="row justify-content-md-center mb-3">
			<div class="col-xs-10">
				<h4 class="vacation-h2">
					<c:choose>
						<c:when test="${numroomtype gt 1}">
								${numroomtype}&nbsp<spring:theme code="text.accommodation.details.property.available.room.types" text="Room Types" />
						</c:when>
						<c:otherwise>
								${numroomtype}&nbsp<spring:theme code="text.accommodation.details.property.available.room.type" text="Room Types" />
						</c:otherwise>
					</c:choose>
				</h4>
			</div>
		</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<packageDetails:roomOptions accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
				<c:set var="property" value="${property}" />
			</div>
		</div>
		</div>
	</json:property>
</json:object>
