<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<json:object escapeXml="false">
	<c:choose>
		<c:when test="${not empty validationError}">
			<json:property name="validationError">
				<spring:theme code="${validationError}" />
			</json:property>
		</c:when>
		<c:otherwise>
			<json:property name="accommodationInventoryResponseDatas">
				<table border="1">
					<c:forEach var="accommodationOfferingInventoryResponseData" items="${accommodationOfferingInventoryResponseDatas}">
						<c:forEach var="accommodationInventoryResponseData" items="${accommodationOfferingInventoryResponseData.accommodations}">
							<tr align="left">
								<th colspan="8">${accommodationOfferingInventoryResponseData.accommodationOfferingName}-${accommodationInventoryResponseData.accommodationName}(${accommodationInventoryResponseData.stockLevelType})</th>
							</tr>
							<tr align="right">
								<th></th>
								<th><spring:theme code="label.vacation.inventory.report.result.date" text="Date" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.block" text="Block" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.sold" text="Sold" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.available" text="Available" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.stopSell" text="Stop Sell" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.blockRelease" text="Block Release" /></th>
								<th><spring:theme code="label.vacation.inventory.report.result.stockType" text="Stock Type" /></th>
							</tr>

                            <c:choose>
							<c:when test="${accommodationInventoryResponseData.stockLevelType eq 'ONREQUEST'}">
                                <tr align="left">
                                    <td></td>
                                    <td colspan="7"><spring:theme code="label.vacation.inventory.report.product.stockType.onRequest" /> </td>
                                 </tr>

                                <c:if test="${not empty accommodationInventoryResponseData.stopSellDateRanges}">
                                    <c:forEach var="stopSellDateRange" items="${accommodationInventoryResponseData.stopSellDateRanges}">
                                        <tr align="left">
                                            <td></td>
                                            <td colspan="7">
                                                <spring:theme code="text.vacation.inventory.report.product.stopsell.onRequest" arguments="${stopSellDateRange.startDate},${stopSellDateRange.endDate}" />
                                            </td>
                                         </tr>
                                     </c:forEach>
                                </c:if>
							</c:when>
							<c:when test="${empty accommodationInventoryResponseData.stockLevelInventories}">
                                <tr align="left">
                                    <td></td>
                                    <td colspan="7"> <spring:theme code="label.vacation.inventory.report.noStocks" /></td>
                                </tr>
							</c:when>
							<c:otherwise>
                                <c:forEach var="stockLevelInventoryResponseData" items="${accommodationInventoryResponseData.stockLevelInventories}">
                                    <tr align="right">
                                        <td></td>
                                        <td>${stockLevelInventoryResponseData.date}</td>
                                        <td>${stockLevelInventoryResponseData.initialAvailable}</td>
                                        <td>${stockLevelInventoryResponseData.reserved}</td>
                                        <td>${stockLevelInventoryResponseData.available}</td>
                                        <td>${fn:toUpperCase(stockLevelInventoryResponseData.stopSaleStatus)}</td>
                                        <td>${stockLevelInventoryResponseData.releaseDays}</td>
                                        <td>${stockLevelInventoryResponseData.stockType}</td>
                                    </tr>
                                </c:forEach>
							</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:forEach>
				</table>
			</json:property>
		</c:otherwise>
	</c:choose>
</json:object>
