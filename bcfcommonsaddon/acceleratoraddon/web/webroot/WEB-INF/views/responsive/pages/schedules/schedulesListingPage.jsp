<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="seasonalSchedule" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/schedules" %>
<c:set var="scheduleToList" value="${dailySchedules.scheduleRoutes}"/>
<c:set var="routeInfo" value="${dailySchedules.travelRouteInfo}"/>
<c:url var="dailySchedulesUrl" value="/routes-fares/schedules"/>
<c:url var="dailySchedulesCalUrl" value="/routes-fares/schedules/${routeInfo.departureRouteName}-${routeInfo.arrivalRouteName}/${routeInfo.departureTerminalCode}-${routeInfo.arrivalTerminalCode}?scheduleDate="/>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<jsp:useBean id="now" class="java.util.Date"/>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<p class="fnt-14 margin-top-30 mb-0">
				<spring:theme code="text.route.label" text="Route" />
			</p>

			<div class="col-xs-12 accommodation-star-rating">
				<div class="row">
					<h2 class="text-dark-blue vaction-h2 mt-4 mb-4 font-weight-bold">
						${routeInfo.departureTerminalName}&nbsp;<spring:theme code="text.to.label" text="to"/>&nbsp;${routeInfo.arrivalTerminalName}
					</h2>
				</div>
			</div>

			<div class="col-xs-12 accommodation-star-rating mb-5">
				<div class="row">
					<label cssClass="skip">
						<span class="switch-icon"></span>
						<a href="${switchDirectionUrl}"><spring:theme code='text.switchDirection.label'/></a>
					</label>
				</div>
			</div>

			<c:if test="${routeInfo.reservable eq false}">
				<div class="col-xs-12 accommodation-star-rating paddingLeftOff paddingRightOff text-center">
					<div class="custom-error-1 alert alert-info alert-dismissable">
						<span class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle"></span>
						<spring:theme code="text.schedules.not.bookable.online.label" text="Not Reservalble Online"/>&nbsp;
						<spring:theme code="text.fares.available.at.terminal.label" text="Available to purchase at terminal"/>
					</div>
				</div>
			</c:if>
		</div>
	</div>
</div>

<div class="container">
	<c:set var="dates" value="${scheduleDates}"/>
	<div class="row schedules-wrapper mb-0 margin-bottom-30">
		<%--	Calander nivagation	--%>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-4 col-lg-4 col-sm-4 col-md-offset-4 schedules-calender margin-bottom-30 margin-top-30 routesfaresschedules">
					<c:set var="dates" value="${scheduleDates}"/>
					<c:set var="linksForTabs" value="${scheduleDatesTabLinks}"/>
					<ul class="nav nav-tabs vacation-calender">
						<input type="hidden" class="urlSample" id="urlSample" value="${dailySchedulesCalUrl}" />
						<c:choose>
							<c:when test="${not empty param['scheduleDate']}">
								<fmt:parseDate var="parsedScheduleDate" value="${param.scheduleDate}" pattern="MM/dd/yyyy"/>
								<c:choose>
									<c:when test="${parsedScheduleDate eq now or parsedScheduleDate lt now}">
										<a class="schedule-prev prev y_previous disabled">&#10094;</a>
									</c:when>
									<c:otherwise><a href="${dailySchedulesUrl}${fn:escapeXml(linksForTabs[0])}" class="schedule-prev prev y_previous">&#10094;</a></c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<a class="schedule-prev prev y_previous disabled">&#10094;</a>
							</c:otherwise>
						</c:choose>

                        <c:choose>
                            <c:when test="${ not empty fn:escapeXml(linksForTabs[2])}">
                                <a href="${dailySchedulesUrl}${fn:escapeXml(linksForTabs[2])}" class="schedule-next next y_next">&#10095;</a>
                            </c:when>
                            <c:otherwise>
                                <a class="schedule-next next y_next disabled">&#10095;</a>
                            </c:otherwise>
						</c:choose>

						<li class="tab-links tab-depart">
							<a data-toggle="tab" href="#check-in1" class="nav-link component-to-top">
								<div class="vacation-calen-box vacation-ui-depart">
									<span class="vacation-calen-year-txt"> </span>
									<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
									   aria-hidden="true"></i>
								</div>
							</a></li>

					</ul>

					<div class="tab-content">
						<div id="check-in1" class="tab-pane input-required-wrap">
							<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
								<label><spring:theme
										code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>
								<input type="text" class=" form-control datepicker-input">
								<div id="datepickerSchedule" class="bc-dropdown--big" data-autoclose="true">
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>



		<%-- Sailing Duration --%>
		<c:if test="${not empty sailingDurations}">
			<div class="col-xs-12 text-center margin-bottom-30">
				<c:choose>
					<c:when test="${sailingDurations['minDuration'] eq sailingDurations['maxDuration']}">
						<spring:theme code="text.schedules.sailingDuration.label" arguments="${sailingDurations['minDuration']}"/>&nbsp;<b>${sailingDurations['minDuration']}</b>
					</c:when>
					<c:otherwise>
						<spring:theme code="text.schedules.varied.sailingDuration.label"/>&nbsp;<b>${sailingDurations['minDuration']} - ${sailingDurations['maxDuration']}</b>
					</c:otherwise>
				</c:choose>
			</div>
		</c:if>
		<table id="myDummyTable" class="schedule-table">
			<c:if test="${fn:length(scheduleToList) ne 0}">
				<thead>
				<tr>
					<th><spring:theme code="text.depart.label" text="Depart"/></th>
					<th><spring:theme code="text.arrive.label" text="Arrive"/></th>
					<th><i><spring:theme code="text.stopsTransfers.label" text="Stops / Transfers"/></i></th>
				</tr>
				</thead>
			</c:if>
			<tbody>
			<c:choose>
				<c:when test="${fn:length(scheduleToList) == 0}">
					<p class="schedules-li-lable">No Schedule Available</p>
				</c:when>
				<c:otherwise>
					<c:forEach var="scheduleResponse" items="${scheduleToList}" varStatus="loop">
						<tr>
							<c:choose>
								<%-- For Direct Routes --%>
								<c:when test="${fn:length(scheduleResponse.transportOfferings) eq 1}">
									<c:forEach var="transportOffering" items="${scheduleResponse.transportOfferings}" varStatus="loop1">
										<td>
											<fmt:formatDate value="${transportOffering.departureTime}" pattern="h:mm a"/>
											<span class="dash-line"></span>
										</td>
										<td>
											<fmt:formatDate value="${transportOffering.arrivalTime}" pattern="h:mm a"/>
										</td>
										<td>
											<c:set var="nonStopLegend" value="true"/>
											<span class="icon-dot"></span> <span class="non-stop-text"><spring:theme code="text.schedules.nonStop.label"/></span>
										</td>
									</c:forEach>
								</c:when>
								<%-- For Stopovers --%>
								<c:when test="${fn:length(scheduleResponse.transportOfferings) gt 1}">
									<c:forEach var="transportOffering" items="${scheduleResponse.transportOfferings}" varStatus="counter">
										<c:if test="${scheduleResponse.route.origin.code eq transportOffering.sector.origin.code}">
											<c:set var="departureTransportOffering" value="${transportOffering}" />
										</c:if>
										<c:if test="${scheduleResponse.route.destination.code eq transportOffering.sector.destination.code}">
											<c:set var="arrivalTransportOffering" value="${transportOffering}" />
										</c:if>
									</c:forEach>
									<c:if test="${not empty departureTransportOffering and not empty arrivalTransportOffering}">
										<td><fmt:formatDate value="${departureTransportOffering.departureTime}" pattern="h:mm a" /></td>
										<td><fmt:formatDate value="${arrivalTransportOffering.arrivalTime}" pattern="h:mm a" /></td>
										<td>
											<div class="progtrckr" data-progtrckr-steps="5">
												<c:forEach var="stopOverTransportOffering" items="${scheduleResponse.transportOfferings}" varStatus="index">
													<c:choose>
														<c:when test="${index.first}">
															<c:set var="previousTransportOffering" value="${stopOverTransportOffering}" />
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${stopOverTransportOffering.ebookingSailingCode eq previousTransportOffering.ebookingSailingCode}">
																	<p class="stop-over-blank-circle">
																		<c:set var="stopAtLegend" value="true"/>
																		<span><spring:theme code="text.schedules.stopAt.label" text="Stop at" arguments="${stopOverTransportOffering.originLocationCity}" /></span>
																	</p>
																</c:when>
																<c:otherwise>
																	<p class="stop-over-line-circle">
																		<c:set var="transferAtLegend" value="true"/>
																		<span><spring:theme code="text.schedules.transferAt.label" text="Transfer at" arguments="${stopOverTransportOffering.originLocationCity}" /></span>
																	</p>
																</c:otherwise>
															</c:choose>
															<c:set var="previousTransportOffering" value="${stopOverTransportOffering}" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</div>
										</td>
									</c:if>
								</c:when>
							</c:choose>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			</tbody>
		</table>

		<c:if test="${nonStopLegend eq 'true' or transferAtLegend eq 'true' or stopAtLegend eq 'true'}">
			<div class="schedules-dataList-lable">
				<label class="schedules-li-lable"><small><spring:theme code="text.legend.lable"/></small></label>
				<ul class="list-unstyled list-inline schedules-li mb-0">
					<c:if test="${nonStopLegend eq 'true'}">
						<li><span class="icon-dot"></span> <spring:theme code="text.schedules.nonStop.label"/></li>
					</c:if>

					<c:if test="${transferAtLegend eq 'true'}">
						<li><span class="icon-dot-line"></span> <spring:theme code="text.transfer.lable"/></li>
					</c:if>

					<c:if test="${stopAtLegend eq 'true'}">
						<li><span class="icon-dot-blank"></span> <spring:theme code="text.stop.lable"/></li>
					</c:if>
				</ul>
				<hr class="hr-grey margin-top-0">

			</div>
		</c:if>

		<div>
			<form:form commandName="scheduleFinderForm" method="get">
				<c:url var="seasonalScheduleUrl" value="/seasonal-schedules">
					<c:param name="departurePort" value="${routeInfo.departureTerminalCode}"/>
					<c:param name="arrivalPort" value="${routeInfo.arrivalTerminalCode}"/>
				</c:url>
			</form:form>
			<seasonalSchedule:viewSeasonalSchedule viewPage="seasonalSchedules" viewPageUrl="${seasonalScheduleUrl}"/>
		</div>

	</div>
	<div class="row info-dangerous-cargo">
		<div class="col-lg-12">
			<p class="clear-both padding-left-20 padding-right-20"><spring:theme code="dangerous.cargo.sailings.desc.label" text="Looking for information on dangerous cargo sailings"/></p>
		</div>
	</div>


	<c:if test="${routeInfo.reservable eq false}">
		<div class="col-xs-12 accommodation-star-rating mb-5">
			<span class="bcf bcf-icon-notice-outline bcf-2x"></span><br/>
			<b><spring:theme code="text.schedules.not.bookable.online.label" text="Not Reservalble Online"/>&nbsp;</b><br/>
			<spring:theme code="text.purchase.at.terminal.label" text="Purchase at terminal"/>
		</div>
	</c:if>
</div>

<div class="container">
	<div class="row information-div">
		<c:choose>
			<%-- Book This Route Link --%>

			<c:when test="${routeInfo.reservable}">
				<c:url var="bookThisRouteUrl" value="/">
					<c:param name="sailing" value="true"/>
					<c:param name="departureLocation" value="${routeInfo.departureTerminalCode}"/>
					<c:param name="departureLocationName"
							 value="${routeInfo.departureLocationName} - ${routeInfo.departureTerminalName}"/>
					<c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}"/>
					<c:param name="arrivalLocationName" value="${routeInfo.arrivalLocationName} - ${routeInfo.arrivalTerminalName}"/>
					<c:if test="${not empty param['scheduleDate']}">
						<c:param name="departingDateTime" value="${param['scheduleDate']}"/>
					</c:if>
				</c:url>
				<spring:theme var="bookThisRouteLabel" code='sailing.link.bookThisRoute.text'/>
				<common:sailingLink url="${bookThisRouteUrl}" imgSource=""
									label="${bookThisRouteLabel}" cssStyle="blue-color bcf bcf-icon-calendar bcf-2x schedules-icons"/>
			</c:when>
			<%-- Caluculate fare Link --%>
			<c:otherwise>
				<c:url var="fareCalculateUrl" value="/routes-fares/fare-calculator">
					<c:param name="departureLocation" value="${routeInfo.departureTerminalCode}" />
					<c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}" />
					<c:param name="departingDateTime" value="${param['scheduleDate']}"/>
				</c:url>

				<spring:theme var="calculatefareLabel" code='sailing.link.calculateFare.text'/>
				<common:sailingLink url="${fareCalculateUrl}" imgSource=""
									label="${calculatefareLabel}" cssStyle="blue-color bcf bcf-icon-calendar bcf-2x schedules-icons"/>
			</c:otherwise>
		</c:choose>

		<%-- Current Conditions Link --%>
		<c:url var="currentContUrl" value="/current-conditions/${routeInfo.departureRouteName}-${routeInfo.arrivalRouteName}/${routeInfo.departureTerminalCode}-${routeInfo.arrivalTerminalCode}"/>
		<spring:theme code="sailing.link.currentConditions.text" var="currentCondLabel"/>
		<common:sailingLink url="${currentContUrl}" imgSource="" label="${currentCondLabel}"
							cssStyle="blue-color bcf bcf-icon-ferry schedules-icons"/>

		<%-- Signup for route notices Link --%>
		<c:url var="subscriptionsUrl" value="/my-account/subscriptions"/>
		<spring:theme var="signupRouteLabel" code='sailing.link.signupForRouteNotices.text'/>
		<common:sailingLink url="${subscriptionsUrl}" imgSource="" label="${signupRouteLabel}"
							cssStyle="blue-color bcf bcf-icon-email schedules-icons"/>
	</div>
</div>
