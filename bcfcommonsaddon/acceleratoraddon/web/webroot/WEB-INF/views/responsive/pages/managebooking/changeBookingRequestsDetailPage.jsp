<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<spring:url value="/manage-booking/booking-details/" var="bookingDetailsUrl"/>

<c:choose>
<c:when test="${not empty csTicketDatas}">
<div class="account-overview-table">
    <table class="table techne-table">
        <thead>
            <tr>
                <th><spring:theme code="text.changebooking.request.ticketId"/></th>
                <th><spring:theme code="text.changebooking.request.customerName"/></th>
                <th><spring:theme code="text.changebooking.request.customerEmail"/></th>
                <th><spring:theme code="text.changebooking.request.customerContact"/></th>
                <th><spring:theme code="text.changebooking.request.changeDetails"/></th>
                 <th><spring:theme code="text.changebooking.request.bookingId"/></th>
                <th><spring:theme code="text.changebooking.request.status"/></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${csTicketDatas}" var="csTicketData">
                <tr class="techne-table-xs-left">
                    <td class="techne-table-xs-left-slot">
                        ${csTicketData.code}
                     </td>
                     <td class="techne-table-xs-left-slot">
                        ${csTicketData.customerName}
                     </td>
                     <td class="techne-table-xs-left-slot">
                        ${csTicketData.customerEmail}
                    </td>
                    <td class="techne-table-xs-left-slot">
                       ${csTicketData.customerContactNumber}
                    </td>
                    <td class="techne-table-xs-left-slot">
                       ${csTicketData.changeDescription}
                    </td>
                    <td class="techne-table-xs-left-slot">
                       <a href="${bookingDetailsUrl}${csTicketData.bookingReference}" > ${csTicketData.bookingReference}</a>
                    </td>
                    <td class="techne-table-xs-left-slot">
                       ${csTicketData.ticketStatus}
                    </td>
                    <td>
                        <c:url value="/manage-booking/change-booking-requests/close-ticket" var="closeTicketUrl" />
                        <form:form id="closeTicketForm" name="closeTicketForm" action="${closeTicketUrl}" method="post">
                            <input id="ticketId" name="ticketId" type="hidden" value="${csTicketData.code}"/>
                            <button class="btn btn-secondary mb-3 mt-1" type="submit" id="closeTicketFormButton" value="Submit">
                               <spring:theme code="button.changebooking.request.closeticket"/>
                            </button>
                        </form:form>
                     </td>
                      <td>
                        <c:if test="${csTicketData.ticketStatusCode ne 'InProgress'}">
                            <c:url value="/manage-booking/change-booking-requests/mark-as-inprogress" var="markAsInProgressUrl" />
                             <form:form id="markAsInProgressForm" name="markAsInProgressForm" action="${markAsInProgressUrl}" method="post">
                                <input id="ticketId" name="ticketId" type="hidden" value="${csTicketData.code}"/>
                                <button class="btn btn-secondary mb-3 mt-1" type="submit" id="markAsInprogressFormButton" value="Submit">
                                    <spring:theme code="button.changebooking.request.markasinprogress"/>
                                </button>
                              </form:form>
                         </c:if>
                        </td>
                 </tr>
            </c:forEach>
          </tbody>
          </table>
        </div>
        </c:when>
        <c:otherwise>
            <spring:theme code="text.changebooking.request.notickets"/>
        </c:otherwise>
		</c:choose>
