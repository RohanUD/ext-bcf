<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/dealdetails"%>
<%@ taglib prefix="deallisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/deallisting"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="nextUrl" value="/cart/summary" />
<c:url var="addDealToCart" value="/cart/addDeal" />
<template:page pageTitle="${pageTitle}">
	<progress:travelBookingProgressBar stage="packages" amend="${amend}" bookingJourney="${bookingJourney}" />
	<c:if test="${!packagesResponseData.available}">
		<packageDetails:stockNotAvailableModel />
	</c:if>
	<div class="container">
		<div id="y_departureDate" value="${departureDate}">
			<form:form modelAttribute="addDealToCartForm" class="y_addBundleToCartForm" id="addDealBundleToCartForm" action="${fn:escapeXml(addDealToCart)}" method="post">
				<form:input type="hidden" path="parentDealId" value="${dealBundleTemplateId}" />
				<form:input type="hidden" path="startingDate" value="${departureDate}" />
				<c:forEach var="packageResponse" items="${packagesResponseData.packageResponses}">
					<c:if test="${not empty packageResponse.transportPackageResponse.errorMessage}">
						<spring:theme code="${packageResponse.transportPackageResponse.errorMessage}" />
					</c:if>
					<c:set var="accommodationAvailabilityResponse" value="${packageResponse.accommodationPackageResponse.accommodationAvailabilityResponse}" />
					<c:set var="transportData" value="${packageResponse.transportPackageResponse.fareSearchResponse}" />
					<c:forEach var="pricedItinerary" items="${transportData.pricedItineraries}">
						<c:if test="${pricedItinerary.originDestinationRefNumber == 0}">
							<c:set var="departureRoute" value="${pricedItinerary.itinerary.route}" />
							<c:if test="${fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings) gt 0}">
								<c:set var="originLocationCity" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].originLocationCity}" />
								<c:set var="destinationLocationCity" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].destinationLocationCity}" />
							</c:if>
						</c:if>
					</c:forEach>
					<fmt:formatDate value="${accommodationAvailabilityResponse.roomStays[0].checkInDate}" pattern="dd/MM/yyyy" var="dealStartDate" />
					<fmt:formatDate value="${accommodationAvailabilityResponse.roomStays[0].checkOutDate}" pattern="dd/MM/yyyy" var="dealEndDate" />
					<div class="col-xs-12">
						<h2 class="h2">
							<spring:theme code="text.page.dealdetails.title" text="Deal Details" />
						</h2>
					</div>
					<div class="margin-reset clearfix package-details-wrap">
						<div class="col-xs-12 col-sm-9 y_nonItineraryContentArea">
							<accommodationDetails:propertyDetails property="${accommodationAvailabilityResponse.accommodationReference}" />
							<packageDetails:selectedRoomStays roomStays="${accommodationAvailabilityResponse.roomStays}" />
							<div class="row form-group">
								<div class="col-xs-12 col-sm-offset-8 col-sm-offset-8 col-sm-4 y_packageSearch" style="display: none;">
									<a href="" class="y_search_more_packages btn btn-primary col-xs-12" data-toggle="modal" data-target="#more-packages-modal"> <spring:theme code="button.page.dealdetails.packages.showmore" text="Explore more packages" />
									</a>
									<deallisting:dealfinderattributes propertyData="${accommodationAvailabilityResponse}" transportData="${transportData}" returnRouteLocation="${departureRoute.destination}" departureRouteLocation="${departureRoute.origin}" departureLocationCity="${originLocationCity}"
										returnLocationCity="${destinationLocationCity}" departureDate="${dealStartDate}" returnDate="${dealEndDate}" />
								</div>
							</div>
							<div class="container">
								<packageDetails:selectedTransportation isDynamicPackage="false" transportPackageResponse="${packageResponse.transportPackageResponse}" />
							</div>
							<packageDetails:transportationOptions dealFormPath="addBundleToCartForms" transportPackageResponse="${packageResponse.transportPackageResponse}" />
						</div>
					</div>
				</c:forEach>
			</form:form>
			<c:if test="${packagesResponseData.available}">
				<button type="submit" id="y_addDealToCart" class="btn btn-primary btn-block bottom-align">
					<spring:theme text="Add Deal To Cart" />
				</button>
				<c:url var="removeDealFromCart" value="/cart/summary/removeDeal?dealId=${dealBundleTemplateId}&date=${departureDate}" />
				<a href="${removeDealFromCart}" class="btn btn-secondary col-xs-12">Remove</a>
			</c:if>
			Total Package Price Now: ${packagesResponseData.price.actualRate.formattedValue} <br /> Total Package Price Was: ${packagesResponseData.price.wasRate.formattedValue} <br /> Total Package Discount: ${packagesResponseData.price.totalDiscount.formattedValue} <br /> Total Package Base Price:
			${packagesResponseData.price.basePrice.formattedValue} <br /> Total Package taxes: ${packagesResponseData.price.totalTax.price.formattedValue} <br />
			<div class="margin-reset clearfix package-details-wrap">
				<div class="col-xs-12 col-sm-3">
					<aside id="sidebar" class="y_reservationSideBar reservation">
						<div class="main-wrap">
							<cms:pageSlot position="Reservation" var="feature" element="div">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<div class="row y_packageDetailsContinue">
								<div class="visible-xs-block col-xs-offset-1 col-xs-10">
									<a href="${nextUrl}" class="btn btn-secondary col-xs-12"> <spring:theme code="text.package.details.button.continue" text="Continue" />
									</a>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>
		<div class="y_continueBar continue-bar hidden-xs">
			<div class="container">
				<div class="row y_packageDetailsContinue">
					<div class="col-xs-offset-9 col-xs-3">
						<a href="${nextUrl}" class="btn btn-secondary col-xs-12"> <spring:theme code="text.package.details.button.continue" text="Continue" />
						</a>
					</div>
				</div>
			</div>
		</div>
		<reservation:fullReservationOverlay />
		<packageDetails:addPackageToCartSuccessModel />
		<packageDetails:addPackageToCartErrorModal />
		<deallisting:dealfinderform finderFormTitleCode="text.page.dealdetails.deal.finder.title.message" finderFormTitleText="Explore More Packages" />
</template:page>
