<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template"
           tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format"
           tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sprng" uri="http://www.springframework.org/tags" %>
<c:url var="seasonalSchedulesUrl" value="/seasonal-schedules?"/>
<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="container-fluid px-0  py-4 padding-top-20 margin-top-40 margin-bottom-40 clear-both">
    <div class="container">
        <%-- Page Title and description --%>
        <div class="row text-center">
            <div class="col-md-offset-1 col-sm-offset-3 col-lg-10 col-md-10 col-sm-6 col-xs-12 margin-bottom-10">
                <h5 class="font-weight-bold"><sprng:theme code="text.view.seasonal.schedule.by.route.label"
                                                          text="View seasonal schedule by route"/></h5>
            </div>

        </div>

        <div class="row">
            <div class="col-md-offset-2 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-5 mob-selector">
                <select id="seasonalScheduleSelectRoute" class="form-control selectpicker">
                    <option selected disabled><spring:theme code="text.schedules.selectRoute.label"/></option>
                    <c:forEach items="${schedules}" var="entry">
                        <c:set var="routes" value="${entry.value}"/>
                        <c:forEach items="${routes}" var="route" varStatus="couter">
                            <option value="${route.sourceTerminalCode}-${route.destinationTerminalCode}">
                                    ${route.sourceTerminalName} - ${route.destinationTerminalName}
                            </option>
                        </c:forEach>
                    </c:forEach>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6  col-xs-12 mb-5">
                <button id="viewSeasonalSchedule" class="btn btn-primary btn-block"><spring:theme
                        code="text.schedules.viewSchedule.button.label"/></button>
            </div>
            <form action="${seasonalSchedulesUrl}" id="seasonalSchedulesFomByRoute" method="get">
                <input type="hidden" id="departurePort" name="departurePort" value="">
                <input type="hidden" id="arrivalPort" name="arrivalPort" value="">
            </form>
        </div>
    </div>
</div>
