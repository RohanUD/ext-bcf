<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template"
           tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format"
           tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="dailySchedulesUrl" value="schedules-landing"/>
<spring:htmlEscape defaultHtmlEscape="false"/>
<jsp:useBean id="now" class="java.util.Date"/>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h4 class="title font-weight-bold"><spring:theme code="text.schedules.todaysSchedule.label" text="Today's schedule"/></h4>
        </div>
    </div>
    <div id="tabs" class="terminal-listing">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <p class="fnt-14"><spring:theme code="text.schedules.selectRegion.label" text="Select a region"/></p>
            </div>
            <div class="col-lg-4 col-md-4 terminal-listing-tab">
                <label><spring:theme code="text.cc.view.label" text="View"/></label>
                <ul>
                    <li><a href="#tabs-1"> <spring:theme code="text.view.list.label" text="List"/></a></li>
                    <li><a href="#tabs-2" id="mapviewtab"> <spring:theme code="text.view.map.label" text="Map"/></a></li>
                </ul>
            </div>
        </div>
        <div id="tabs-1">
            <div id="schedulesList">
                <c:set var="todaysDate"><fmt:formatDate value='${now}' pattern='MMM dd yyyy'/></c:set>
                <c:forEach items="${schedules}" var="entry">
                    <div class="js-accordion js-accordion-default">
                        <h4 class="schedule-header"><spring:theme code="text.cc.atAglance.${entry.key}.label" /></h4>
                        <div>
                            <div class="row">
                                <c:set var="routes" value="${entry.value}"/>
                                <c:forEach items="${routes}" var="route" varStatus="couter">
                                    <c:url var="scheduleListingUrlUrl" value="/routes-fares/schedules/${route.sourceRouteName}-${route.destinationRouteName}/${route.sourceTerminalCode}-${route.destinationTerminalCode}"/>
                                    <div class="col-lg-5 col-md-5 col-xs-12 padding-0 padding-right-30">
                                        <div class="schedule-grid">
                                            <a href="${scheduleListingUrlUrl}"> ${route.sourceTerminalName} - ${route.destinationTerminalName}
                                                
                                            </a>
                                            <span class="text-right"> <i class="bcf bcf-icon-right-arrow"></i></span>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div id="tabs-2">
        <div class="p-relative">
        <div class="bg-image-map-text"><img src="../../../../_ui/responsive/common/images/bg-map.png"> <h2><spring:theme code="text.choose.a.region" text="Choose a region to start"/></h2></div>
            <div id="showMapResponse" ></div>
              <div class="col-md-5 schrdules-drop-down">
                <c:if test="${not empty regionFinderForm}">
                    <form:select path="routeRegions" class="selectpicker form-control schedule-dropdwon-custom map-dropdwon-custom" id="y_routeRegionSelection">
                        <form:options items="${routeRegions}"/>
                    </form:select>
                </c:if>
           </div>
        </div>
</div>
    </div>
</div>
