<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="seasonalSchedule" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/schedules" %>
<jsp:useBean id="now" class="java.util.Date" />
<c:url var="seasonalSchedulesUrl" value="/seasonal-schedules"/>
<c:set var="seasoneSchedule" value="${seasonalScheduleData.schedulesData}"/>

<c:url var="switchDirectionUrl"
       value="seasonal-schedules?departurePort=${routeInfo.arrivalTerminalCode}&arrivalPort=${routeInfo.departureTerminalCode}&departureDate=${selectedDateSchedule}"/>

<c:url var="dailySchedulesUrl"
       value="routes-fares/schedules/${routeInfo.arrivalRouteName}-${routeInfo.departureRouteName}/${routeInfo.arrivalTerminalCode}-${routeInfo.departureTerminalCode}"/>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <p class="fnt-14 margin-top-10 mb-0">
                <spring:theme code="text.route.label" text="Route" />
            </p>
            <h2 class="text-dark-blue"><b>${routeInfo.departureTerminalName}&nbsp;<spring:theme
                    code="text.to.label"/>&nbsp;${routeInfo.arrivalTerminalName}</b>
            </h2>
            <p><a href="${switchDirectionUrl}"><i class="bcf bcf-icon-stacked-arrows-horizontal"></i> <spring:theme code="text.switchDirection.label" text="Swicth direction"/> </a></p>

            <c:forEach items="${seasoneSchedule.schedules}" var="schedule">
                <c:if test="${not empty schedule.sailingDates and schedule.sailingDates ne 'sailingDates'}">
                    <c:set var="showSelectedDaysOnlyWarningMessage" value="true"/>
                </c:if>
            </c:forEach>
            <c:if test="${showSelectedDaysOnlyWarningMessage}">
                <div class="seasonal-message-box">

                    <div class="error-msg-text-box">

                        <p class="inline-block text-sm d-flex"><span class="bcf bcf-icon-notice-outline seasonal-notice-icon "></span> <strong><spring:theme code="seasonal.schedules.info.label"/></strong></p>
                        <p class="text-sm"><spring:theme code="seasonal.schedules.desc1.label"/>&nbsp;<a href="${dailySchedulesUrl}"><spring:theme
                                code="seasonal.daily.schedules.label"/> </a> <spring:theme code="seasonal.schedules.desc2.label"/>. </p>
                    </div>
                </div>
            </c:if>
            <form id="seasonalSchedulesForm" action="${seasonalSchedulesUrl}" method="get">


                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3">
                        <input type="hidden" name="departurePort" value="${seasonalScheduleData.departurePort}">
                        <input type="hidden" name="arrivalPort" value="${seasonalScheduleData.arrivalPort}">
                        <c:if test="${not empty seasonalScheduleData.scheduleDates}">
                            <select class="selectpicker form-control" id="seasonalScheduleDate" name="departureDate">
                                <c:forEach items="${seasonalScheduleData.scheduleDates}" var="scheduleDate">
                                    <fmt:formatDate value="${scheduleDate.from}" var="fromDate" pattern="yyyMMdd"/>
                                    <fmt:formatDate value="${scheduleDate.to}" var="toDate" pattern="yyyMMdd"/>

                                    <fmt:formatDate value="${scheduleDate.from}" var="fromDateDisplay" pattern="MMM dd, yyyy"/>
                                    <fmt:formatDate value="${scheduleDate.to}" var="toDateDisplay" pattern="MMM dd, yyyy"/>
                                    <c:set var="dateCode" value="${fromDate}-${toDate}"/>
                                    <option value="${dateCode}" <c:if test='${selectedDateSchedule eq dateCode}'>selected</c:if>>
                                            ${fromDateDisplay} - ${toDateDisplay}
                                    </option>
                                </c:forEach>
                            </select>
                        </c:if>
                    </div>
                </div>

                <c:choose>
                    <c:when test="${not empty seasoneSchedule.schedules}">
                        <div>
                            <div class="text-center margin-top-10 margin-bottom-20">
                                <c:choose>
                                    <c:when test="${sailingDurations['maxDuration'] eq sailingDurations['minDuration']}">
                                        <spring:theme
                                                code="text.schedules.sailingDuration.label"/><b>${sailingDurations['minDuration']}</b>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:theme code="text.schedules.varied.sailingDuration.label"/>
                                        <b>${sailingDurations['minDuration']} - ${sailingDurations['maxDuration']}</b>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="seasonal-date-update-box">
                                <spring:theme code="seasonal.schedules.updated.label"/>&nbsp; <fmt:formatDate value="${now}"
                                                                                                              pattern="HH:mm, MMM dd, yyyy"/>
                            </div>
                            <div class="seasonal-schedule-wrapper">
                                <table class="table table-seasonal-schedule">
                                    <c:forEach items="${seasoneSchedule.schedules}" var="schedule">
                                        <thead>
                                        <tr>
                                            <th><spring:theme code="text.depart.label" text="Depart"/></th>
                                            <th><spring:theme code="text.arrive.label" text="Arrive"/></th>
                                            <th><spring:theme code="text.days.label" text="Days"/></th>
                                        </tr>
                                        </thead>

                                        <tr>
                                            <td class="fnt-18 lowercase-text"><strong>${schedule.departureTimeDisplay}</strong><span class="dash-line"></span></td>
                                            <td class="fnt-18 lowercase-text"><strong>${schedule.arrivalTimeDisplay}</strong></td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${fn:contains(schedule.sailingDates, '#') and fn:contains(schedule.sailingDates, '!') and empty schedule.scheduleDays}">
                                                        <c:set var="stringWithHashAndExclamation" value="${fn:replace(schedule.sailingDates, '#', '')}"/>
                                                        ${fn:replace(stringWithHashAndExclamation, '!', '')}
                                                    </c:when>
                                                    <c:when test="${fn:contains(schedule.sailingDates, '#') and empty schedule.scheduleDays}">
                                                        ${fn:replace(schedule.sailingDates, '#', '')}
                                                    </c:when>
                                                    <c:when test="${fn:contains(schedule.sailingDates, '#') and not empty schedule.scheduleDays}">
                                                        <spring:theme code="text.select.days.only.label" text="Select days only"/>
                                                    </c:when>
                                                    <c:when test="${fn:contains(schedule.sailingDates, '!')}">
                                                        <spring:theme code="text.select.days.only.label" text="Select days only"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${schedule.sailingDates}
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                        <c:if test="${not empty schedule.scheduleDays}">
                                            <tr>
                                                <td colspan="3">
                                                    <table class="table ">
                                                        <tr>
                                                            <th colspan="3"><spring:theme code="text.selected.days.label"
                                                                                          arguments="${schedule.departureTimeDisplay}"
                                                                                          text="Sailing available these days only"/>:
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <c:forEach items="${schedule.sailingDatesList}" var="sailDate"
                                                                       varStatus="counter">
                                                            <td>${sailDate}</td>
                                                            <c:if test="${counter.count % 3 eq 0}">
                                                        </tr>
                                                        <tr>
                                                            </c:if>
                                                            </c:forEach>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <tr>
                                            <td colspan="3">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th><spring:theme code="text.stopsTransfers.label"
                                                                          text="Stops/Transfers"/></th>
                                                    </tr>
                                                    </thead>
                                                    <tr>
                                                        <td class="progtrckr" data-progtrckr-steps="5">
                                                            <c:choose>
                                                                <c:when test="${not empty schedule.legs and fn:length(schedule.legs) eq 2}">
                                                                <ul class="list-unstyled list-inline schedules-li mb-0">
                                                                    <li><span class="icon-dot"></span>
                                                                    <spring:theme code="text.schedules.nonStop.label" text="Non-stop"/></li>
                                                                </c:when>
                                                                <c:when test="${not empty schedule.legs}">
                                                                    <c:forEach items="${schedule.legs}" var="leg">
                                                                        <c:choose>
                                                                            <c:when test="${leg.transferPortFlag}">
                                                                                <p class="progtrckr-done"><spring:theme
                                                                                        code="text.schedules.transferAt.label"
                                                                                        text="Transfer at"
                                                                                        arguments="${leg.legDestination}"/></p>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <p class="progtrckr-done"><spring:theme
                                                                                        code="text.schedules.stopAt.label"
                                                                                        text="Stop at"
                                                                                        arguments="${leg.legDestination}"/></p>
                                                                            </c:otherwise>
                                                                        </c:choose>

                                                                    </c:forEach>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <spring:theme code="text.schedules.nonStop.label"
                                                                                  text="Non-stop"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </c:forEach>
                                    <tr>
                                        <td colspan="3">
                                            <label class="schedules-li-lable"><small><spring:theme code="text.legend.lable"/></small></label>
                                            <ul class="list-unstyled list-inline schedules-li mb-0">
                                                <li><span class="icon-dot"></span> <spring:theme code="text.schedules.nonStop.label"/></li>
                                                <li><span class="icon-dot-line"></span> <spring:theme code="text.transfer.lable"/></li>
                                                <li><span class="icon-dot-blank"></span> <spring:theme code="text.stop.lable"/></li>
                                                <li><span class="bcf bcf-icon-info-solid"></span> <spring:theme code="text.speacial.sailing.info.lable"/></li>

                                            </ul>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <spring:theme code="seasonalSchedules.notFound.label"/>
                    </c:otherwise>
                </c:choose>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row information-div margin-bottom-30">

        <div class="col-md-offset-1 col-sm-offset-1 col-lg-offset-2 col-lg-12  col-md-10 col-sm-10 col-xs-12 ">
            <div class="row">
                <c:url var="bookThisRouteUrl" value="/">
                    <c:param name="sailing" value="true"/>
                    <c:param name="departureLocation" value="${routeInfo.departureTerminalCode}"/>
                    <c:param name="departureLocationName" value="${routeInfo.departureLocationName} - ${routeInfo.departureTerminalName}"/>
                    <c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}"/>
                    <c:param name="arrivalLocationName" value="${routeInfo.arrivalLocationName} - ${routeInfo.arrivalTerminalName}"/>
                    <c:if test="${not empty param['scheduleDate']}">
                        <c:param name="departingDateTime" value="${param['scheduleDate']}"/>
                    </c:if>
                </c:url>

                <spring:theme var="bookThisRouteLabel" code='sailing.link.bookThisRoute.text'/>
                <common:sailingLink url="${bookThisRouteUrl}" imgSource="" label="${bookThisRouteLabel}"
                                    cssStyle="blue-color bcf bcf-icon-calendar schedules-icons"/>

                <%-- Signup for route notices Link --%>
                <c:url var="subscriptionsUrl" value="/my-account/subscriptions"/>
                <spring:theme var="signupRouteLabel" code='sailing.link.signupForRouteNotices.text'/>
                <common:sailingLink url="${subscriptionsUrl}" imgSource="" label="${signupRouteLabel}"
                                    cssStyle="blue-color bcf bcf-icon-email schedules-icons"/>
            </div>
        </div>
    </div>
</div>

<seasonalSchedule:viewSeasonalSchedule viewPage="dailySchedules" viewPageUrl="${dailySchedulesUrl}"/>
