<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="mapPath" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/map"%>
<c:url var="dailySchedulesUrl" value="/routes-fares/schedules/"/>
<json:object escapeXml="false">
    <json:property name="routeRegionMap">
    <div id="showMapResponse">
    <c:if test="${not empty travelRoutelist}">
    <c:set var="googleApiKey" value="${jalosession.tenant.config.getParameter('google.api.key')}"/>
    <c:if test="${not empty googleApiKey}">
         <c:set var="url" value="${dailySchedulesUrl}"/>
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
            <div class="map-wrap y_propertyMap" id="showmap"
                data-schedulesurl="${url}"
                data-googleapi="${googleApiKey}">
            </div>
        </div>
         </div>
    </c:if>
    </c:if>
    <mapPath:maptravelroute travelRoutelist="${travelRoutelist}" originToDestinationMapping="${originToDestinationMapping}" />
    </div>

   </json:property>
</json:object>
