<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/myaccount"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
<spring:url value="/checkout/paynow" var="payNowUrl" />
<spring:url value="/manage-booking/unlink-booking" var="unlinkBookingUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />

   <div class="panel-heading px-0">
      <p>
         <b>
            <spring:theme code="text.account.myVacations.label" />
         </b>
      </p>
   </div>

      <c:choose>
          <c:when test="${not empty myBookings}">
          <div id="tabs" class="terminal-listing">
             <div class="row">
                <div class="col-lg-4 terminal-listing-tab margin-top-30 margin-bottom-30">
                   <ul>
                      <li>
                         <a href="#tabs-1" class="y_vacationsList" data-upcoming="true">
                            <spring:theme code="text.page.mybooking.upcoming.bookings" text="Upcoming" />
                         </a>
                      </li>
                      <li>
                         <a href="#tabs-2" class="y_vacationsList" data-upcoming="false">
                            <spring:theme code="text.page.mybooking.past.bookings" text="Past" />
                         </a>
                      </li>
                   </ul>
                </div>
             </div>
                     <div id="tabs-1">
                        <div id="vacation_details">
                           <div class="vacation_details_placeholder">
                              <myaccount:travelBookings myBookings="${myBookings}"/>
                           </div>
                           <div class="row text-center margin-top-30">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="y_shownResultId text-center mb-4">
                                    <p>
                                       <spring:theme code="text.ferry.listing.shown.results"
                                          arguments="${startingNumberOfResults},${totalShownResults},${totalNumberOfResults}" />
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div class="acco-pagination  text-center">
                              <p>
                                 <a href="javascript:;" class="y_showSpecificVacationsPageResults prev"
                                    data-pagenumber="${pageNum-1}">
                                    <span
                                       class="y_showPreviousVacationsPageResults" disabled>
                                       <spring:theme
                                          code="text.ferry.listing.show.previous.page"
                                          text="Previous Page" />
                                    </span>
                                 </a>
                                 |
                                 <a href="">
                                    <strong>
                                 <a href="javascript:;"
                                    class="y_showSpecificVacationsPageResults next"
                                    data-pagenumber="${pageNum}"> <span
                                    class="y_showNextVacationsPageResults"> <spring:theme
                                    code="text.ferry.listing.show.next.page" text="Next Page" />
                                 </span>
                                 </a></strong></a>
                              </p>
                           </div>
                        </div>
                     </div>
                     <div id="tabs-2">
                        <div id="vacation_details">
                           <div class="vacation_details_placeholder row">
                              <myaccount:travelBookings myBookings="${myBookings}"/>
                           </div>
                        </div>
                     </div>
                  </div>
            </c:when>
            <c:otherwise>
                <spring:theme code="text.page.mybookings.no.bookings" text="No bookings found"/>
             </c:otherwise>
            </c:choose>
