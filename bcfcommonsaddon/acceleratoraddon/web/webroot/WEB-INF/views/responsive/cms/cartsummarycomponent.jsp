<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ taglib prefix="cartsummary" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cartsummary"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>

 <cart:asmSupplierComments />

<c:if test="${not empty globalReservationDataList.packageReservations}">

	<bcfglobalreservation:packageReservationDetails packageDataList="${globalReservationDataList.packageReservations}" />
</c:if>
<c:if test="${not empty globalReservationDataList.transportReservations}">
	<bcfglobalreservation:transportReservations reservationDataList="${globalReservationDataList.transportReservations}" />
</c:if>
<c:if test="${not empty globalReservationDataList.accommodationReservations}">
	<div class="container">
		<c:forEach items="${globalReservationDataList.accommodationReservations.accommodationReservations}" var="accommodationReservationData" varStatus="accommodationResVarStatus">
			<c:if test="${not empty accommodationReservationData}">
				<accommodationReservation:hotelReservation accommodationReservationData="${accommodationReservationData}" />
			</c:if>

			<hr class="hr-payment">
			<div class="row mb-5">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<c:set var="roomStays" value="${accommodationReservationData.roomStays}" />
					<bcfglobalreservation:roomReservation roomStays="${roomStays}"/>
				</div>
			</div>
			<hr class="hr-payment">
		</c:forEach>
	</div>
</c:if>
<c:if test="${not empty globalReservationDataList.activityReservations}">
    <bcfglobalreservation:totalTaxFare />
	<bcfglobalreservation:activityReservations activityReservationDataList="${globalReservationDataList.activityReservations}" />
</c:if>

<cartsummary:cartSummaryPageActions />
