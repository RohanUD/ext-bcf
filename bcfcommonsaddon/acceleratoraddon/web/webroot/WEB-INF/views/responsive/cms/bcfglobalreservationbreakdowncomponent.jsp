<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="guest" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="bcfglobalreservationbreakdown" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservationbreakdown"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
	<div class="row">
		<c:if test="${not empty globalReservationData}">
			<bcfglobalreservationbreakdown:transportreservationbreakdown reservationDataList="${globalReservationData.transportReservation}" />
			<bcfglobalreservationbreakdown:accommodationreservationbreakdown accommodationReservationDataList="${globalReservationData.accommodationReservation}" />
			<bcfglobalreservationbreakdown:activityreservationbreakdown activityDataList="${globalReservationData.activityReservation}" />
			<bcfglobalreservationbreakdown:packagereservationbreakdown packageDataList="${globalReservationData.packageReservation}" />
			<div class="row mt-4">
				<div>
					<c:if test="${not empty globalReservationData.totalFare.fees}">
						<p>${feesLabel}</p>
						<c:forEach items="${globalReservationData.totalFare.fees}" var="fee" varStatus="feeIdx">
							<div class="y_feeData">
								<Strong>${fee.name}</Strong>&nbsp:&nbsp <Strong>${fee.price.formattedValue}</Strong> <br>
								<c:if test="${fee.amendable && allowAmendBookingFees}">
									<spring:theme code="amend.booking.fees.label" />
									<input type="text" id="y_waiveOff_amount" name="amount" />
									<input type="hidden" id="y_waiveOff_cacheKey" name="cachekey" value="${fee.cacheKey}" />
									<input type="submit" value="Submit" id="y_waiveOffSubmit" />
								</c:if>
							</div>
						</c:forEach>
					</c:if>
				</div>
				<div class="col-sm-4">
					<c:if test="${not empty globalReservationData.totalFare.taxPrice}">
						<p>${taxesLabel }</p>
						<Strong>${globalReservationData.totalFare.taxPrice.formattedValue}</Strong>
						<br>
					</c:if>
				</div>
				<div class="col-sm-4">
					<c:if test="${not empty globalReservationData.totalFare.discounts}">
						<p>${discountsLabel }</p>
						<c:forEach items="${globalReservationData.totalFare.discounts}" var="discount" varStatus="discountIdx">
							<Strong>${discount.purpose}</Strong>&nbsp:&nbsp
						<Strong>${discount.price.formattedValue}</Strong>
							<br>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</c:if>
	</div>
</div>
