<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="inventory" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/inventory"%>
<script type="text/javascript">
	var accommodationLevelDatasStr = '${accommodationLevelDatas}';
	var activityLevelDatasStr='${activityLevelDatas}'
</script>
<div class="container margin-top-30">
 <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
<div class="tab-wrapper full-tabs package-details-tabs">
    <ul class="nav nav-tabs save-nav-tab-on-change" role="tablist">
        <li role="presentation" class="active"><a href="#tab-01" aria-controls="tab-01" role="tab" data-toggle="tab" class="text-center" aria-selected="true"> <spring:theme code="text.asm.tab.accommodation.inventory.report" text="Accommodation Inventory Report" />
        </a></li>
        <li role="presentation" class="pl-1"><a href="#tab-02" aria-controls="tab-02" role="tab" data-toggle="tab" class="text-center"> <spring:theme code="text.asm.tab.activity.inventory.report" text="Activity Inventory Report" />
        </a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tab-01">
                <inventory:accommodationInventoryReportForm />
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab-02">
                <inventory:activityInventoryReportForm />
        </div>
    </div>
</div>
</div>
</div>
</div>
