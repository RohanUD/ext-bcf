<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="displayFilters" value="true" />
<c:if test="${displayFilters}">
	<c:if test="${not empty activitySearchParamsError}">
		<div class="col-xs-12">
			<div class="row">
				<div class="alert alert-danger alert-dismissable">
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
					<spring:theme code="${activitySearchParamsError}" />
				</div>
			</div>
		</div>
	</c:if>
	<c:url value="/vacations/activities" var="activitySearchUrl"/>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sidebar-offcanvas" id="filter">
        <div class="filter-form clearfix nomargin">
            <div class="filter-facets activity-listing-select">

		                <div class="container padding-0">
		                <h4 class="font-weight-bold mb-5">
                    <spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:" />
                </h4>
                	<div class="row">
                    <c:forEach items="${facetSearchPageData.facets}" var="facet">
		                    	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-0">
                        <activities:activityFacetFilter facetData="${facet}" />
                        </div>
                    </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="clearfix">&nbsp;</div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                        <c:url var="activityListingParamUrl" value="/vacations/activities/parameter" />
                        <form:form commandName="activityFinderForm" class="y_activityFinderForm" action="${activityListingParamUrl}" method="GET">
                            <form:hidden path="destination" />
                            <form:hidden path="date" />
                            <form:hidden path="activity" />
                            <form:hidden path="activityCategoryTypes" />
                            <form:hidden path="query" />
                            <form:hidden path="sort" />
                            <form:hidden path="currentPage" />
                            <form:hidden path="changeActivity" />
                            <form:hidden path="changeActivityCode" />
                        </form:form>
                    </div>
                </div>

            </div>

</c:if>
<div>
    <pagination:pageNumbers/>
</div>
<div class="container">
    <div class="row mb-5" id="y_displaySortSelection">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-right sort">
        	<div class="col-lg-9 col-md-9 col-sm-7 col-xs-9 blank padding-0"></div>
            <div class="sorter">
         <span class="font-weight-bold fnt-14">
            <spring:theme code="activity.sort.title" text="Sort by:" />
         </span>
			</div>
         <form action="${requestScope['javax.servlet.forward.request_uri']}">
            <select name="sort" class="activies-select fnt-14 y_activitySearchCriteriaData" onChange="this.form.submit();">
               <option selected disabled>
                  <spring:theme code="accommodation.sort.order.select.sort" text="Select a sort" />
               </option>
               <c:forEach var="sort" items="${facetSearchPageData.sorts}">
                  <option value="${fn:escapeXml(sort.code)}" ${param.sort eq sort.code ? 'selected' : ''}>
                  <c:choose>
                     <c:when test="${not empty sort.name}">
                        ${fn:escapeXml(sort.name)}
                     </c:when>
                     <c:otherwise>
                        <spring:theme code="activity.sort.order.${sort.code}" text="${fn:escapeXml(sort.code)}" />
                     </c:otherwise>
                  </c:choose>
                  </option>
               </c:forEach>
            </select>
            <pagination:storeRequestParams currentField="sort"/>
         </form>
      </div>
   </div>
</div>
