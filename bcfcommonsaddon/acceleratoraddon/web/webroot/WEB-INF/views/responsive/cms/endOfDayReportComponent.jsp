<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<form:form commandName="agentDeclareForm" modelAttribute="agentDeclareForm" action="/endofday-report/confirm-endofday-report"
	class="form-background" id="agentDeclareForm" method="post">
    <div class="container">
        <b><spring:theme code="text.agent.declare.data.entry" /></b>
        <br />

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.portlocation" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="portLocation" id="portLocation" value="${agentDeclareForm.portLocation}" readonly="true"/>
            </div>
        </div>

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.userId" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="userId" id="userId" value="${agentDeclareForm.userId}" readonly="true"/>
            </div>
        </div>

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.userName" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="userName" id="userName" value="${agentDeclareForm.userName}" readonly="true"/>
            </div>
        </div>

        <br />
        <br />
        <br />

        <jsp:useBean id="date" class="java.util.Date"/>
        <c:set var="sessionStartDate"><fmt:formatDate value="${agentDeclareForm.sessionStart}" type="date" pattern="dd/MM/yyyy HH:mm"/></c:set>

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.sessionStart" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="sessionStart" id="sessionStart" value="${sessionStartDate}" readonly="true"/>
            </div>
        </div>

        <c:set var="sessionEndDate"><fmt:formatDate value="${agentDeclareForm.declareEnd}" type="date" pattern="dd/MM/yyyy HH:mm"/></c:set>

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.declareEnd" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="declareEnd" id="declareEnd" value="${sessionEndDate}" readonly="true"/>
            </div>
        </div>

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.declareBy" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="declareBy" id="declareBy" value="${agentDeclareForm.declareBy}" readonly="true"/>
            </div>
        </div>

	</div>

    <br />
    <br />

	<div class="container">
        <b><spring:theme code="text.agent.declare.payment.methods" /></b>
        <br />

        <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.paymentMethod" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.declaredAmount" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.depositReference" /></b>
            </div>
        </div>

        <br />

         <div class="row input-row">
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <b><spring:theme code="text.agent.declare.cashPaymentMethod" /></b>
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="cashDeclaredAmount" id="cashDeclaredAmount" />
            </div>
            <div class="input-required-wrap col-xs-12 col-sm-4">
                <form:input type="text" path="bankDepositNumber" id="bankDepositNumber" />
            </div>
        </div>

         <c:forEach items="${creditCardTypes}" var="creditCardType">
            <div class="row input-row">
                <div class="input-required-wrap col-xs-12 col-sm-4">
                    <b>${creditCardType.value}</b>
                </div>
                <div class="col-md-5 col-sm-4 col-xs-12">
                    <form:input type="text" path="creditCardDeclaredTypes[${creditCardType.key}]" id="creditCardDeclaredTypes[${creditCardType.key}]" />
                </div>
            </div>
        </c:forEach>

        <c:forEach items="${giftCardTypes}" var="giftCardType">
            <div class="row input-row">
                <div class="input-required-wrap col-xs-12 col-sm-4">
                    <b>${giftCardType.value}</b>
                </div>
                <div class="col-md-5 col-sm-4 col-xs-12">
                    <form:input type="text" path="giftCardDeclaredTypes[${giftCardType.key}]" id="giftCardDeclaredTypes[${giftCardType.key}]" />
                </div>
            </div>
        </c:forEach>
    </div>

    <input type="hidden" name="agentToDeclare" value="${agentDeclareForm.userId}" />

    <div class="row margin-top-20 margin-bottom-40">
        <div class="col-lg-3 col-md-3 col-md-offset-5">
            <button class="btn btn-primary btn-block" type="submit" value="Submit">
               <spring:theme code="text.agent.declare.submitbutton" text="Declare" />
            </button>
        </div>
    </div>

</form:form>
