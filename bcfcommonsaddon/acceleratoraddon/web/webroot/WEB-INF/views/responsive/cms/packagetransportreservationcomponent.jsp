<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_transportReservationComponentId" />
<div class="">
  <c:choose>
		<c:when test="${reservable}">
		<c:if test="${empty change}">
			<div class="green-bar-msg">
				<span class="bcf bcf-icon-checkmark"></span>
				<c:choose>
					<c:when test="${not empty tripType && tripType eq 'RETURN'}">
						<spring:theme code="text.packageferryselection.return.selected" text="Return sailing selection" />
					</c:when>
					<c:otherwise>
						<spring:theme code="text.packageferryselection.departure.selected" text="Departure sailing selection" />
					</c:otherwise>
				</c:choose>
				&nbsp;<strong><spring:theme code="text.packageferryselection.departure.confirmed" text="confirmed!" /></strong>
				
			</div>
		</c:if>
		</c:when>
		<c:otherwise>
			<div class="container">
				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 p-header pt-2 pb-2">
							<div class="fare-table-inner-wrap">
								
									<h3><b><spring:theme code="label.packageferryreview.sailingoptions.heading" /></b></h3>
									<h4><b><spring:theme code="label.packageferryreview.sailingoptions.subheading" /></b></h4>
								
								<p>
									<spring:theme code="label.packageferryreview.sailingoptions.description" /></p>
							</div>
						</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>

	<div class="container">
	  <c:if test="${not empty reservationDataList}">

        <c:forEach items="${reservationDataList.reservationDatas}" var="reservation" varStatus="reservationItemIdx">
            <c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
               <c:choose>
                  <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
                	<ferryselection:transportreservationdecider item="${item}" type="OutBound" />
                  </c:when>
               </c:choose>
               <c:choose>
                 <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
                   <ferryselection:transportreservationdecider item="${item}" type="InBound" />
                 </c:when>
               </c:choose>
            </c:forEach>
          </c:forEach>
		</c:if>
        <ferryselection:transportreservationpackagebuttons />
	</div>
</div>
