<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
	<json:property name="htmlContent">
		<c:set var="paxCount" value="0" />
		<div id="y_packageAccessibilityPlaceholder">
			<c:forEach var="paxType" items="${travelFinderForm.fareFinderForm.passengerTypeQuantityList}" varStatus="i">
				<c:forEach begin="1" end="${paxType.quantity}" varStatus="j">
					<div class="col-12 col-md-6 col-lg-6 bc-accordion mb-5">
						<label>
							<spring:theme code="text.ferry.farefinder.passengerinfo.passenger.accesibility" text="PASSENGER" />
							&nbsp;${paxCount+1} : ${paxType.passengerType.name}
						</label>
						<div id="accordion-accessibility-${paxCount+1}">
							<div id="y_accessibility_dropdown_${paxCount+1}">
								<h3 class="font-bold-6">
									Please make a Selection
									<span class="custom-arrow"></span>
								</h3>
								
									<div id="y_accessibility_div_${paxCount+1}">
										<div id="y_accessibility_clearselection_${paxCount+1}" class="btn-block border clear-accesibility-needs-item hidden">
											<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.clear.selection" text="clear selection" />
										</div>
										<c:forEach items="${travelFinderForm.fareFinderForm.accessibilityRequestDataList[paxCount].ancillaryRequestDataList}" var="ancillaryProduct" varStatus="k">
											<div class="passenger-option-select">
												<label class="custom-radio-input mb-3">
													<form:radiobutton class="accesibility-needs-item" path="travelFinderForm.fareFinderForm.accessibilityRequestDataList[${paxCount}].selection" value="${ancillaryProduct.code}" />${ancillaryProduct.name}
													<span class="checkmark"></span>
												</label>
											</div>
										</c:forEach>
										<c:forEach items="${travelFinderForm.fareFinderForm.accessibilityRequestDataList[paxCount].specialServiceRequestDataList}" var="specialServiceRequest" varStatus="l">
											 
												<div class="passenger-option-select">
													<label class="custom-checkbox-input mb-3">
														<form:checkbox class="accesibility-needs-item" id="specialServiceRequest_${specialServiceRequest.code}_${paxCount+1}" value="${specialServiceRequest.selection}"
															path="travelFinderForm.fareFinderForm.accessibilityRequestDataList[${paxCount}].specialServiceRequestDataList[${l.index}].selection" />${specialServiceRequest.name}
														<span class="checkmark-checkbox"></span>
													</label>
												</div>
												<c:if test="${specialServiceRequest.code eq 'OTHR'}">
													<div id="other_accessibility_need_${paxCount+1}" class="hidden required-accessibility_need passenger-textarea-select">
														<form:textarea type="text" id="textbox_other_accessibility_need" path="travelFinderForm.fareFinderForm.accessibilityRequestDataList[${paxCount}].specialServiceRequestDataList[${l.index}].description"
															value="${fareFinderForm.accessibilityRequestDataList[paxCount].specialServiceRequestDataList[l.index].description}"></form:textarea>
													</div>
												</c:if>
											
										</c:forEach>
									</div>
								
							</div>
						</div>
						<div class="accesibility-selected passenger-select-field hidden">
							<ul>
							</ul>
						</div>
					</div>
					<c:set var="paxCount" value="${paxCount+1}" />
				</c:forEach>
			</c:forEach>
		</div>
	</json:property>
</json:object>
