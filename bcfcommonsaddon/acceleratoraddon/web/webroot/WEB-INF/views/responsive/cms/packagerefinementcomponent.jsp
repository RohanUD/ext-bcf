<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="refinement" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting/refinement"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container clear-both">
  <div class="row margin-top-40">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
      <h2 class="vacation-h2 mt-3">
         <spring:theme code="text.package.listing.title" text="Accommodation" />
      </h2>
    </div>
  </div>
<c:set var="displayFilters" value="false" />
<c:if test="${not empty fn:escapeXml(filterPropertyName) or not empty fn:escapeXml(priceRange) or totalNumberOfResults gt 0}">
   <c:set var="displayFilters" value="true" />
</c:if>
<c:forEach items="${packageSearchResponse.criterion.facets}" var="facet">
   <c:if test="${not empty facet.values && not displayFilters}">
      <c:forEach items="${facet.values}" var="facetValue">
         <c:if test="${facetValue.selected}">
            <c:set var="displayFilters" value="true" />
         </c:if>
      </c:forEach>
   </c:if>
</c:forEach>
<c:if test="${displayFilters}">
   <c:if test="${not empty accommodationSearchParamsError}">
      <div class="col-xs-12">
         <div class="row">
            <div class="alert alert-danger alert-dismissable">
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
               <spring:theme code="${accommodationSearchParamsError}" />
            </div>
         </div>
      </div>
   </c:if>
   <div class="row">
       <div class="filter-form clearfix col-lg-8 col-sm-12 col-md-8 col-xs-12">
          <div class="filter-facets">
             <h3 class="vacation-h3">
                <spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:" />
             </h3>
             <div class="accommodations_select mb-4">
                  <ul class="nav nav-pills">
                     <c:forEach items="${packageSearchResponse.criterion.facets}" var="facet">
                        <refinement:facetFilter facetData="${facet}" />
                     </c:forEach>
                     <li>
                        <label>&nbsp;</label>
                        <a href="#" class="y_packageListing_clearAllFacetFilter filter-clear-all-btn hidden">
                            <spring:theme code="package.listing.facet.clearAll" text="Clear All" />
                        </a>
                     </li>
                  </ul>
              </div>
          </div>
       </div>
   </div>
   <div class="row sidebar-offcanvas" id="filter">
      <c:url value="/package-listing" var="packageListingUrl" />
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <form action="${packageListingUrl}" method="GET" id="y_packageListingFacetForm">
            <c:forEach items="${accommodationSearchParams}" var="paramDetail">
               <input type="hidden" name="${fn:escapeXml(paramDetail.key)}" value="${fn:escapeXml(paramDetail.value)}" />
            </c:forEach>
            <c:forEach items="${packageSearchResponse.criterion.sorts}" var="sort">
               <c:if test="${sort.selected }">
                  <input type="hidden" name="sort" value="${fn:escapeXml(sort.code)}" />
               </c:if>
            </c:forEach>
            <input type="hidden" name="q" value="${fn:escapeXml(packageSearchResponse.criterion.query)}" />
            <input type="hidden" name="priceRange" value="${fn:escapeXml(priceRange)}" />
            <input id="y_resultsViewTypeForFacetForm" type="hidden" name="resultsViewType" value="${fn:escapeXml(resultsViewType)}" />
         </form>
      </div>
   </div>
   <hr class="hr-payment">
</c:if>
</div>
