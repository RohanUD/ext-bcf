
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="deallisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/deallisting"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="displayFilters" value="true" />

<c:if test="${displayFilters}">
	<c:if test="${not empty dealSearchParamsError}">
    		<div class="col-xs-12">
    			<div class="row">
    				<div class="alert alert-danger alert-dismissable">
    					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
    					<spring:theme code="${dealSearchParamsError}" />
    				</div>
    			</div>
    		</div>
    </c:if>
	<c:url value="/deal-selection" var="dealSearchUrl" />
	<div class="col-xs-6 col-sm-12 sidebar-offcanvas" id="filter">
		<div>
			<form action="${dealSearchUrl}" method="GET" id="y_dealSearchFacetForm">
				<c:forEach items="${dealsearchParams}" var="paramDetail">
					<input type="hidden" name="${fn:escapeXml(paramDetail.key)}" value="${fn:escapeXml(paramDetail.value)}" />
				</c:forEach>
				<input type="hidden" name="q" value="${fn:escapeXml(dealsResponse.query)}" />
				<input id="y_resultsViewTypeForFacetForm" type="hidden" name="resultsViewType" value="${fn:escapeXml(resultsViewType)}" />
			</form>
		</div>
		<div class="row">
			<div class="clearfix">
				<div class="filter-form clearfix col-xs-12">
					<div class="filter-facets">
						<h3 class="hidden-xs">
							<spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:" />
						</h3>
						<ul class="nav nav-pills col-sm-12">
							<c:forEach items="${dealsResponseDataList.facets}" var="facet">
								<deallisting:dealFacetFilter facetData="${facet}" />
							</c:forEach>
						</ul>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</c:if>



