<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<div class="booking-reservation margin-bottom-40 text-center">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-md-offset-3">
			<div class="row">
				<div class="col-lg-10 col-md-10 col-md-offset-1">
					<span class="bcf bcf-icon-info-solid fnt-28"></span>
					<h4>
						<strong><spring:theme code="text.booking.confirmation.tentative.booking.title"></spring:theme></strong>
					</h4>
					<p>
						<spring:theme code="text.booking.confirmation.tentative.booking.paragraph"></spring:theme>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<c:set var="reservationDataList" value="${globalReservationData.transportReservations}" />
<c:if test="${reservationDataList != null}">
	<c:forEach var="reservation" items="${reservationDataList.reservationDatas}">
		<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && reservation != null}">
			<c:forEach var="reservationItem" items="${reservation.reservationItems}">
				<div class="journey-wrapper">
					<div class="panel-heading">
						<c:set var="transportOfferings" value="${reservationItem.reservationItinerary.originDestinationOptions[0].transportOfferings}" />
						<c:set var="originSector" value="${transportOfferings[0].sector}" />
						<c:set var="item" value="${reservationItem}" />
						<c:set var="finalSector" value="${transportOfferings[fn:length(transportOfferings) - 1].sector}" />
						<h3 class="panel-title text-center my-3">
							<c:choose>
								<c:when test="${reservationItem.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
									<fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
									<spring:theme code="label.payment.reservation.transport.departure.sailing.time.prefix" /> : ${departureDate}
									</c:when>
								<c:when test="${reservationItem.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
									<fmt:formatDate var="returnDate" pattern="EEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[fn:length(transportOfferings) - 1].departureTime}" />
									<spring:theme code="label.payment.reservation.transport.return.sailing.time.prefix" /> : ${returnDate}
									</c:when>
							</c:choose>
						</h3>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<p class="text-center">
								<spring:theme code="label.booking.confirmation.tentative.booking.reference.prefix" text="Tentative booking ref" />
								#: <strong>${reservationItem.bcfBookingReference}</strong>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-offset-4 col-md-4">
							<div class="row">
								<div class="clearfix"></div>
								<div class="col-lg-5 col-xs-5 text-center">
									<fmt:formatDate var="departureTime" pattern="HH:mm a" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
									<p class="fnt-14 m-0">${departureTime}</p>
									<p class="font-weight-bold mb-1">${fn:escapeXml(originSector.origin.location.name)}</p>
									<p>(${fn:escapeXml(originSector.origin.name)})</p>
								</div>
								<div class="col-lg-2 col-xs-2 text-center">
									<span class="passengers-arrow-icon-blue booking-confirmation-arrow"></span>
								</div>
								<div class="col-lg-5 col-xs-5 text-center">
									<fmt:formatDate var="arrivalTime" pattern="HH:mm a" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[fn:length(transportOfferings) - 1].arrivalTime}" />
									<p class="fnt-14 m-0">${arrivalTime}</p>
									<p class="font-weight-bold mb-1">${fn:escapeXml(finalSector.destination.location.name)}</p>
									<p>(${fn:escapeXml(finalSector.destination.name)})</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</c:if>
	</c:forEach>
</c:if>
