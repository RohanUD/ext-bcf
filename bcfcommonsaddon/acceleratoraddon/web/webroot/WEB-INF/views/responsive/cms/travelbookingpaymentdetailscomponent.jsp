<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${isPackageInOrder}">
	<div class="panel panel-default total">
		<div class="panel-body">
			<div class="col-xs-12">
				<dl class="text-right">
					<dt>
						<spring:theme code="text.cms.booking.transport.total.title" text="Transport Booking Total" />
					</dt>
					<dd>
						<format:price priceData="${transportBookingTotalAmount}" />
					</dd>
					<dt>
						<spring:theme code="text.cms.booking.accommodation.total.title" text="Accommodation Booking Total" />
					</dt>
					<dd>
						<format:price priceData="${accommodationBookingTotalAmount}" />
					</dd>
					<dt>
						<spring:theme code="text.cms.booking.total.title" text="Booking Total" />
					</dt>
					<dd>
						<format:price priceData="${total}" />
					</dd>
				</dl>
			</div>
		</div>
	</div>
</c:if>
