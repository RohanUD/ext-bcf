<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:url var="scheduleFinderUrl"
	   value="/routes-fares/schedules/"/>
<form:form commandName="scheduleFinderForm"
	action="${fn:escapeXml(scheduleFinderUrl)}" method="GET"
	class="fe-validate form-background form-booking-trip"
	id="y_scheduleFinderForm">
	<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<c:if test="${not empty title}">
					<h5>${title}</h5>
				</c:if>
				<c:if test="${not empty description}">
					<p>${description}</p>
				</c:if>
			</div>
		</div>

		<div class="custom-error-2 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>
		<div class="row-fluid schedules">
			<div class="bc-accordion ferry-drop">
				<label><spring:theme
					code="text.cms.farefinder.departure.location.placeholder"
					text="From" /> </label>
				<div class="accordion-34 component-to-top" id="fromLocationDropDown"
					data-inputselector="fromLocation"
					data-routeinfoselector="y_fareFinderOriginLocationCode"
					data-suggestionselector="y_fareFinderDestinationLocationSuggestionType">
					<h3>
						<span class="dropdown-text">
							<strong><spring:theme
								code="label.ferry.farefinder.dropdown.default.heading" text="Location" /></strong>
							<br />
							<span><spring:theme
								code="label.ferry.farefinder.dropdown.default.subheading" text="Location" /></span>
							<span class="custom-arrow"></span>
						</span>
					</h3>
					<div>

					</div>
				</div>
			</div>
			<div class="bc-accordion ferry-drop">
				<label><spring:theme
						code="text.cms.farefinder.arrival.location.placeholder" text="To" /> </label>
				<div class="accordion-34 component-to-top" id="toLocationDropDown"
					data-inputselector="toLocation"
					data-routeinfoselector="y_fareFinderDestinationLocationCode"
					data-suggestionselector="y_fareFinderOriginLocationSuggestionType">
					<h3>
						<span class="dropdown-text">
							<strong><spring:theme
								code="label.ferry.farefinder.dropdown.default.heading" text="Location" /></strong>
							<br />
							<span><spring:theme
								code="label.ferry.farefinder.dropdown.default.subheading" text="Location" /></span>
							<span class="custom-arrow"></span>
						</span>
						<span class="custom-arrow"></span>
					</h3>
					<input type="text" class="dropdown-hiddenInput hidden" />
					<div>

					</div>
				</div>
			</div>
			<input type="hidden" id="routeType" value="" />
			<form:input type="text" id="fromScheduleLocation"
					path="${fn:escapeXml(formPrefix)}fromLocation"
					cssErrorClass="fieldError"
					class="hidden y_scheduleFinderOriginLocation input-grid col-xs-12 form-control"
					placeholder="From" autocomplete="off" />
<form:input type="text" id="toScheduleLocation"
					path="${fn:escapeXml(formPrefix)}toLocation"
					cssErrorClass="fieldError"
					class="hidden col-xs-12 y_scheduleFinderDestinationLocation input-grid form-control"
					placeholder="To" autocomplete="off" />
			<form:hidden path="${fn:escapeXml(formPrefix)}departureLocation"
				class="y_scheduleFinderOriginLocationCode" maxlenght="5" />
				<div id="y_scheduleArrivalLocationTravelroute" class="autocomplete-suggestions-wrapper hidden"></div>
				<div id="y_scheduleDepartureLocationTravelroute" class="autocomplete-suggestions-wrapper hidden"></div>
			<form:hidden path="${fn:escapeXml(formPrefix)}arrivalLocation"
				class="y_scheduleFinderDestinationLocationCode" maxlenght="5" />
			<div class="bc-accordion ferry-drop schedules-calender">
				<ul class="nav nav-tabs vacation-calender">
					<li class="tab-links tab-depart">
						<label><spring:theme
							code="text.schedules.departure.date.label"
							text="Departure Date" /></label>
						<a data-toggle="tab" href="#check-in" class="nav-link component-to-top">
							<div class="vacation-calen-box vacation-ui-depart">
								<span class="vacation-calen-date-txt">Date</span>
								<span class="vacation-calen-year-txt current-year-custom">2018</span>
								<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
									aria-hidden="true"></i>
								</div>
							</a></li>

								</ul>

								<div class="tab-content">
									<div id="check-in" class="tab-pane input-required-wrap depart-calendar schedule-calendar">
										<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
											<label><spring:theme
												code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>

												<form:input type="text"
													path="${fn:escapeXml(formPrefix)}scheduleDate"
													class="datepicker-input input-grid form-control "
													placeholder="${fn:escapeXml(departingDatePlaceholderText)}"
													autocomplete="off" />
												<div id="datepicker-schedule" class="bc-dropdown--big clearfix" data-autoclose="true">
												</div>
											</div>
										</div>

										</div>
									</div>
									<div class="schedule-finder-source-location-code hidden"></div>
									<div class="schedule-finder-destination-location-code hidden"></div>
										<div class="schedule-finder-confirm-btn schedule-btn-width padding-0">
											<button class="btn btn-primary btn-block"
													type="submit" id="scheduleFinderFindButton" value="Submit"><spring:theme
													code="text.view.schedule.lable" text="View schedule"/></button>
										</div>

		</div>
		<div class="clearfix"></div>
<hr>
	</div>

</form:form>
