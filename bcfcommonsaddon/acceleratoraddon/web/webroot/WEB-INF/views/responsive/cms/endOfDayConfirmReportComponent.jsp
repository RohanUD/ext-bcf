<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<form:form commandName="agentDeclareForm" modelAttribute="agentDeclareForm" action="/endofday-report/declare-endofday-report"
	class="form-background" id="agentDeclareForm" method="post">

<div class="container">
    <b><spring:theme code="text.agent.declare.confirm.data.header" /></b>
    <br />

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.portlocation" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
           <input type="text" value="${agentDeclareData.PortLocation}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.userId" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${agentDeclareData.UserId}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.userName" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${agentDeclareData.UserName}" readonly="true"/>
        </div>
    </div>

    <br />
    <br />
    <br />

    <jsp:useBean id="date" class="java.util.Date"/>
    <c:set var="sessionStartDate"><fmt:formatDate value="${agentDeclareData.SessionStart}" type="date" pattern="dd/MM/yyyy HH:mm"/></c:set>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.sessionStart" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${sessionStartDate}" readonly="true"/>
        </div>
    </div>

    <c:set var="sessionEndDate"><fmt:formatDate value="${agentDeclareData.DeclareEnd}" type="date" pattern="dd/MM/yyyy HH:mm"/></c:set>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.declareEnd" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${sessionEndDate}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.declareBy" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <form:input type="text" path="declareBy" value="${agentDeclareData.supervisorID}" readonly="true"/>
        </div>
    </div>

</div>

<br />
<br />

<div class="container">
    <b><spring:theme code="text.agent.declare.confirm.payment.summary" /></b>
    <br />

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.paymentMethod" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.transaction" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.systemAmount" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.declaredAmount" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.varianceAmount" /></b>
        </div>
    </div>

    <br />

    <c:set var="transactionTotal" value="0" />
    <c:set var="amountTotal" value="0" />
    <c:set var="declareTotal" value="0" />
    <c:set var="varianceTotal" value="0" />

    <c:forEach items="${endOfDayReportData.paymentTransactions}" var="paymentTransactions">
        <div class="row input-row">
            <div class="col-sm-4">
                <b>${paymentTransactions.tenderType}</b>
            </div>
            <div class="col-sm-2">
                <b>${paymentTransactions.totalTransactions}</b>
            </div>
            <c:set var="transactionTotal" value="${transactionTotal+paymentTransactions.totalTransactions}" />
            <div class="col-sm-2">
                <b>${currentCurrency.symbol}${paymentTransactions.systemAmount}</b>
            </div>
            <c:set var="amountTotal" value="${amountTotal + paymentTransactions.systemAmount}" />
            <div class="col-sm-2">
                <b>${currentCurrency.symbol}${paymentTransactions.declaredAmount}</b>
            </div>
            <c:set var="declareTotal" value="${declareTotal + paymentTransactions.declaredAmount}" />

            <c:set var="varianceAmount" value="${paymentTransactions.systemAmount - paymentTransactions.declaredAmount}" />
            <c:set var="varianceStyle" value="" />
            <c:if test="${varianceAmount < 0}">
                <c:set var="varianceStyle" value="color:red" />
            </c:if>
            <div class="col-sm-2" style="${varianceStyle}">
                <b>${currentCurrency.symbol}${varianceAmount}</b>
            </div>

            <c:set var="varianceTotal" value="${varianceTotal + varianceAmount}" />
        </div>
    </c:forEach>

    <div class="row input-row">
        <hr>
     </div>

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.total" /></b>
        </div>
        <div class="col-sm-2">
            <b>${transactionTotal}</b>
        </div>
        <div class="col-sm-2">
            <b>${currentCurrency.symbol}${amountTotal}
        </div>
        <div class="col-sm-2">
            <b>${currentCurrency.symbol}${declareTotal}</b>
        </div>

        <c:set var="varianceStyle" value="" />
        <c:if test="${varianceTotal < 0}">
            <c:set var="varianceStyle" value="color:red" />
        </c:if>
        <div class="col-sm-2" style="${varianceStyle}">
            <b>${currentCurrency.symbol}${varianceTotal}</b>
        </div>
    </div>

    <div class="row input-row">
        <hr>
    </div>

    <div class="row input-row">
            <div class="col-sm-14">
                <div class="col-sm-4">
                    <b><spring:theme code="text.agent.declare..cash.depositReference" /></b>
                </div>
                <div class="col-sm-4">
                    <form:input type="text" path="bankDepositNumber" value="${agentDeclareData.BankDepositNumber}" readonly="true"/>
                </div>
            </div>
    </div>

</div>

<br />
<br />

<div class="container">
    <b><spring:theme code="text.agent.declare.confirm.transaction.summary" /></b>
    <br />

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.paymentType" /></b>
        </div>
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.transaction" /></b>
        </div>
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.transactionAmount" /></b>
        </div>
    </div>

    <br />

    <c:set var="totalTransactions" value="0" />
    <c:set var="totalAmounts" value="0" />

    <c:forEach items="${endOfDayReportData.packageDetails}" var="entry">

        <div class="row input-row">
            <div class="col-sm-4">
                <b><spring:theme code="text.agent.declare.confirm.package.${entry.paymentType}" /></b>
            </div>
            <div class="col-sm-4">
                <input type="text" value="${entry.totalTransactions}" readonly="true"/>
            </div>
            <c:set var="totalTransactions" value="${totalTransactions + entry.totalTransactions}" />
            <div class="col-sm-4">
                <input type="text" value="${entry.totalAmount}" readonly="true"/>
            </div>
            <c:if test="${entry.paymentType eq 'PURCHASE'}">
                <c:set var="totalAmounts" value="${totalAmounts + entry.totalAmount}" />
            </c:if>
            <c:if test="${entry.paymentType eq 'REFUND'}">
                <c:set var="totalAmounts" value="${totalAmounts - entry.totalAmount}" />
            </c:if>
        </div>

    </c:forEach>

    <div class="row input-row">
        <hr>
    </div>

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.package.total" /></b>
        </div>
        <div class="col-sm-4">
            <input type="text" value="${totalTransactions}" readonly="true"/>
        </div>
        <div class="col-sm-4">
            <input type="text" value="${totalAmounts}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <hr>
    </div>

    <br />

</div>

<div class="container">
    <b><spring:theme code="text.agent.declare.confirm.transaction.statistics" /></b>
    <br />

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.transaction.highestsale" /></b>
        </div>
        <div class="col-sm-4">
            <b><input type="text" value="${endOfDayReportData.transactionStatistics.highestSale}" readonly="true"/></b>
        </div>
    </div>
    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.transaction.lowestsale" /></b>
        </div>
        <div class="col-sm-4">
            <b><input type="text" value="${endOfDayReportData.transactionStatistics.lowestSale}" readonly="true"/></b>
        </div>
    </div>
    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.transaction.Averagesale" /></b>
        </div>
        <div class="col-sm-4">
            <b><input type="text" value="${endOfDayReportData.transactionStatistics.averageSale}" readonly="true"/></b>
        </div>
    </div>
  </div>

    <br />

<div class="container">
    <b><spring:theme code="text.agent.declare.confirm.footer" /></b>
    <br />

    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.footer.audit" /></b>
        </div>
        <div class="col-sm-4">
            <b><form:checkbox path="auditRequestFlag"/>
            <spring:theme code="text.agent.declare.confirm.footer.audit.yes" /></b>
        </div>
    </div>
    <div class="row input-row">
        <div class="col-sm-4">
            <b><spring:theme code="text.agent.declare.confirm.footer.audit.request" /></b>
        </div>
        <div class="col-sm-4">
            <b><form:input type="text" path="auditComment"/></b>
        </div>
    </div>
</div>

    <div class="row input-row">
        <hr>
    </div>

    <br />

</div>

    <input type="hidden" name="endOfDayReportData" value="${endOfDayReportData}" readonly="true"/>

    <div class="row margin-top-20 margin-bottom-40">
        <div class="col-lg-3 col-md-3 col-md-offset-5">
            <button class="btn btn-primary btn-block" type="submit" value="Submit">
               <spring:theme code="text.agent.declare.submitbutton" text="Declare" />
            </button>
        </div>
    </div>

</form:form>
