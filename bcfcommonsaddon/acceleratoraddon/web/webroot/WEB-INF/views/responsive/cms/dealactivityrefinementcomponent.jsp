
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="displayFilters" value="true" />
<c:if test="${displayFilters}">
	<c:if test="${not empty activitySearchParamsError}">
    		<div class="col-xs-12">
    			<div class="row">
    				<div class="alert alert-danger alert-dismissable">
    					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
    					<spring:theme code="${activitySearchParamsError}" />
    				</div>
    			</div>
    		</div>
    </c:if>
	<c:url value="/vacations/activities" var="activitySearchUrl"/>
	<div class="col-xs-6 col-sm-12 sidebar-offcanvas" id="filter">
		<div>
			<form:form id="y_activitySearchFacetForm" method="GET" commandName="activityFinderForm" action="${activitySearchUrl}">
    		<form:input type="hidden" path="destination"/>
    		<form:input type="hidden" path="date"/>
    		<form:input type="hidden" path="activityType"/>
    		<form:input type="hidden" path="activity"/>
			<c:forEach items="${activityFinderForm.guestData}" var="activity" varStatus="i">
			<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].guestType"/>
			<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].quantity"/>
			<c:forEach items="${activity.age}" var="age" varStatus="j">
			<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].age[${fn:escapeXml(j.index)}]"/>
			</c:forEach>
			</c:forEach>
				<input type="hidden" name="q" value="${fn:escapeXml(activitiesResponseDataList.query)}" />
				<input id="y_resultsViewTypeForFacetForm" type="hidden" name="resultsViewType" value="${fn:escapeXml(resultsViewType)}" />
			</form:form>
		</div>
		<div class="row">
			<div class="clearfix">
				<div class="filter-form clearfix col-xs-12">
					<div class="filter-facets">
						<h3 class="hidden-xs">
							<spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:" />
						</h3>
						<ul class="nav nav-pills col-sm-12">
							<c:forEach items="${activitiesResponseDataList.facets}" var="facet">
								<activities:activityFacetFilter facetData="${facet}" />
							</c:forEach>
						</ul>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</c:if>



