<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border-right m-pb-2 package-header-top-box">
                    <p><spring:theme code="text.tripwidget.destination" /></p>
                    <p class="m-0">
                        <span class="mr-2 font-weight-bold">${accommodationFinderForm.destinationLocationName}</span>
                        <fmt:parseDate value="${accommodationFinderForm.checkInDateTime}" var="parsedCheckInDateTime" pattern="MM/dd/yyyy" />
                        <fmt:parseDate value="${accommodationFinderForm.checkOutDateTime}" var="parsedCheckOutDateTime" pattern="MM/dd/yyyy" />
                        <c:set var="numberOfNights">
                            <fmt:parseNumber value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}" integerOnly="true"/>
                        </c:set>
                        <c:choose>
                            <c:when test="${numberOfNights eq 1}">
                                <spring:theme code="text.tripwidget.night" arguments="${numberOfNights}" />
                            </c:when>
                            <c:otherwise>
                                (<spring:theme code="text.tripwidget.nights" arguments="${numberOfNights}" />)
                            </c:otherwise>
                        </c:choose>
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border-right package-header-top-box">
                        <p><spring:theme code="text.tripwidget.stay"/></p>
                        <fmt:parseDate value="${fareFinderForm.departingDateTime}" var="parsedDepartDate" pattern="MM/dd/yyyy" />
                        <fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />

                        <div class="font-weight-bold">${formattedParsedDepartDate} <span class="ml-2"> <spring:theme code="text.tripwidget.to"/></span></div>

                        <fmt:parseDate value="${fareFinderForm.returnDateTime}" var="parsedReturnDate" pattern="MM/dd/yyyy" />
                        <fmt:formatDate value="${parsedReturnDate}" var="formattedParsedReturnDate" pattern="E, MMM dd" />
                        <div class="font-weight-bold">${formattedParsedReturnDate}</div>

                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
            <div class="row">
                <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 m-mb-2 package-header-top-box">
                        <p><spring:theme code="text.tripwidget.route"/></p>
                        <p class="mb-1">
                            <c:set var="shipCount" value="1"/>
                            <c:set var="sailingLabelCode" value="text.tripwidget.sailing"/>
                            <c:if test="${travelFinderForm.fareFinderForm.tripType=='RETURN'}">
                               <c:set var="shipCount" value="2"/>
                               <c:set var="sailingLabelCode" value="text.tripwidget.sailings"/>
                            </c:if>
                            <spring:theme code="${sailingLabelCode}" arguments="${shipCount}"/>
                        </p>
                        <p class="m-0">
                           <strong>${fareFinderForm.departureLocationName}</strong> - <strong>${fareFinderForm.arrivalLocationName}</strong>
                        </p>
                </div>
                <a href="${travelFinderForm.referer}">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right m-text-left edit-link">
                    <a href="/VacationSelectionPage?modify=${modify}">
                        <span class="bcf bcf-icon-edit"></span>
                        <spring:theme code="text.tripwidget.edit" text="edit"/>
                    </a>
                </div>
                </a>
            </div>
        </div>

    </div>
</div>
