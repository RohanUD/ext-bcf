<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="vehicleinfo" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryInfo"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="deallisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/deallisting"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<progress:travelBookingProgressBar stage="passengers-vehicles" amend="${amend}" bookingJourney="${bookingJourney}" />
<div class="row">
       <div class="col-xs-12 col-sm-12 side-to-top">
                <cms:pageSlot position="TripContent" var="feature">
                 <cms:component component="${feature}" />
                 </cms:pageSlot>
             </div >
       </div>
<c:set var="formPrefix" value="" />
<c:url var="nextPageUrl" value="${nextURL}" />
<div id="contextPath" class="hidden">${contextPath}</div>
<cart:cartTimer />
<div class="container">
	<div class="alert alert-danger hidden form-error-messages">
	</div>
	<form:form commandName="travelFinderForm" action="${fn:escapeXml(nextPageUrl)}" method="GET" class="fe-validate form-background form-booking-trip passengers-vehicles-form" id="y_accessibilityneeds">
		<form:errors path="${fn:escapeXml(formPrefix)}fareFinderForm.passengerTypeQuantityList" cssClass="alert alert-danger" />
		<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="vacation-h2">
                    <spring:theme code="label.vacation.travelfinder.farefinder.passengers.heading" text="Ferry passenger(s)" />
                </h2>
            </div>
        </div>
		<div class="row mb-3">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="input-group ">
					<label class="custom-checkbox-input have-access-align show" for="y_packageCheckAccessibilityNeed">
						<form:checkbox id="y_packageCheckAccessibilityNeed" path="${fn:escapeXml(formPrefix)}fareFinderForm.accessibilityNeedsCheckOutbound" />
						<spring:theme code="label.vacation.travelfinder.farefinder.accessibility.consent.checkbox.label" text="Have accessibility needs" />
						<span class="checkmark-checkbox"></span>
					</label>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
                <div id="y_packageAccessibilityPlaceholderParent" class="sr-only package-passenger">
                    <div id="y_packageAccessibilityPlaceholder"></div>
                </div>
            </div>
		</div>
		<hr>
		<vehicleinfo:packagevehicleinfo formPrefix="${formPrefix}" travelFinderForm="${travelFinderForm}" />
		<hr>

		<div class="row margin-top-40">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
			    <c:if test="${travelFinderForm.onlyAccommodationChange}">
                     <a class="btn btn-secondary mb-3 mt-1" href="cart/summary">
                        <spring:theme code="text.ferry.farefinder.skip.ferry.change" text="SKIP FERRY CHANGE" />
                     </a>
                 </c:if>

				<button class="btn btn-primary" type="submit" id="fareFinderFindButton" value="Submit">

					<spring:theme code="text.ferry.farefinder.proceed.to.sailing.selection.page" text="CONFIRM AND FIND SAILING" />
				</button>
			</div>
		</div>
	</form:form>
	</div>
