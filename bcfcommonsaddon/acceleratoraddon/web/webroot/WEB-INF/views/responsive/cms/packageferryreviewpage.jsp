<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="hidden alert alert-danger quoteerror">
    <span class="icon-alert" aria-hidden="true"></span>
    <button class="close quote-msg-close quote-error-close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
    <span class="y_anonymoususer_quoteerror mb-2 quote-msg"></span>
</div>
<div class="hidden quotesuccess green-bar-msg">
    <span class="bcf bcf-icon-checkmark" aria-hidden="true"></span>
    <button class="close quote-msg-close quote-success-close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
    <span class="y_anonymoususer_quotesuccess mb-2 quote-msg"></span>
</div>

<progress:travelBookingProgressBar stage="sailing" amend="${amend}" bookingJourney="${bookingJourney}" />
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<div class="row">
       <div class="col-xs-12 col-sm-12 side-to-top">
                <cms:pageSlot position="TripContent" var="feature">
                 <cms:component component="${feature}" />
                 </cms:pageSlot>
             </div >
       </div>
