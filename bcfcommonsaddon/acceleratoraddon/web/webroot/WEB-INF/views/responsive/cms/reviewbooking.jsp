<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="reviewbooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/transportreservation"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
	<div class="col-xl-10 offset-1 mt-5">
	<c:if test="${not empty globalReservationData.transportReservation}">
		<h2 class="title my-3 payment-h2"><strong><spring:theme code="label.payment.reservation.summary.header" text="Booking details" /></strong> <span class="edit-icon-blue"></span></h2>
		<div class="row  float-right">
             <p>
                 <a data-toggle="modal" href="#journeydetailseditModal">edit<span class="glyphicon glyphicon-pencil"></span></a>
             </p>
        </div>
		<c:forEach items="${globalReservationData.transportReservation.reservationDatas}" var="reservation" varStatus="reservationItemIdx">
			<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && not empty reservation.reservationItems}">
				<div class="p-accordion">
				<c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
              <div class="p-box">
                <div class="p-header pt-2 pb-2"> 
                  <h4><a>
                  <c:choose>
					<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
						<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
					</c:when>
					<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
						<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
					</c:when>
				</c:choose>
                  	  <fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
						<spring:theme code="${labelPrefix}" text="Departs" />
						: ${departureDate}
                  </a></h4>                 
                </div>
                <div class="p-collapse collapse" style="">
                  <div class="p-body">
                    <div class="add-on-box">
							<c:choose>
								<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
									<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.total"></c:set>
								</c:when>
								<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
									<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.total"></c:set>
								</c:when>
							</c:choose>
							<fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
							<reviewbooking:sailinginformation itinerary="${item.reservationItinerary}" />
							<reviewbooking:travellercount vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />							
							<div class="row">
			                    <div class="col-xl-12">
			                      <p class="text-center my-3"><a href="#">${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.name}  <span class="bcf bcf-icon-info-solid"></span></a></p>
			                    </div>
			                    <div class="col-xl-10 offset-1">
			                      <div class="vp-box">
			                        <h5 class="my-3"><strong><spring:theme code="label.payment.reservation.traveller.header" text="Vehicles & passengers" /></strong></h5>
			                        <div class="offset-1">
			                         <div class="row">
			                         	<ul class="col-xl-12 col-12 list-unstyled payment-vp">
											<reservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" />
											<reservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
											<reservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.offerBreakdowns}" />
											<li>
													<c:if test="${item.carryingDangerousGoods eq true}"></c:if>
													<spring:theme code="label.payment.reservation.traveller.details.carrying.dangerous.goods" text="(Carrying Dangerous Goods)" />
												
											</li>
										</ul>
			                         </div>
			                        </div>
			                        <h5 class="my-3"><strong><spring:theme code="label.payment.reservation.sailing.price.breakup.header" text="Taxes fees & discounts" /></strong></h5>
			                        <div class="offset-1">
			                         
			                           <div class="row">
			                                    <ul class="list-unstyled col-xl-9 col-9">
			                                      <li><spring:theme code="label.payment.reservation.sailing.price.brakup.tax.header" text="GST/HST" /></li>
			                                      <li><spring:theme code="label.payment.reservation.sailing.price.brakup.discount.header" text="Saver discount" /></li>
			                                    </ul>
			                                    <ul class="list-unstyled col-xl-3 col-3 text-right">
			                                      <li><format:price priceData="${item.reservationPricingInfo.totalFare.taxPrice}" /></li>
			                                      <li><format:price priceData="${item.reservationPricingInfo.totalFare.discountPrice}" /></li>
			                                    </ul>
			                                  </div>
			                       
			                        </div>
			                      </div>
			                      <div class="departure-bx payment-total">
			                        <h5><spring:theme code="${labelPrefix}" text="Sailing total" /> <span><strong><format:price priceData="${item.reservationPricingInfo.totalFare.totalPrice}" /></strong></span></h5>
			                      </div>
			                    </div>
                 			 </div>
						</div>
                  </div>
                </div>
              </div>
              </c:forEach>
              </div>
			</c:if>
		</c:forEach>
		
		<div class="row">
            <div class="col-sm-12">
              <div class="mt-4">
                <p><spring:theme code="text.payment.reservation.due.payment.info" text="Due payment info" /></p>
                <p><a id="fareTermsAndConditionLink" href="<c:url value="/faretermsandconditionpage"/>">
                <spring:theme code="text.payment.reservation.fares.terms.conditions.link" text="Fare terms & conditions" /></a></p>
                <ul class="list-unstyled payment">
                  <li class="p-2">
                  	<spring:theme code="text.payment.reservation.transport.total.trip.cost" text="Total trip costs" />
                  	<span><format:price priceData="${globalReservationData.transportReservation.totalFare.totalPrice}" /></span>
                  </li>
                  <li class="light">
                  	<strong><spring:theme code="text.payment.reservation.transport.total.due.now" text="Total due now" /></strong> 
                  	<span><strong><format:price priceData="${globalReservationData.transportReservation.amountToPay}" /></strong></span>
                  </li>
                  <li class="p-2">
					<spring:theme code="text.payment.reservation.transport.total.pay.at.terminal" text="Pay at the terminal" />
					<span><format:price priceData="${globalReservationData.transportReservation.payAtTerminal}" /></span>
				</li>
                </ul>
              </div>
            </div>
          </div>
	</c:if>
	</div>
</div>

 <popupmodals:fareTermsAndConditionModal />
