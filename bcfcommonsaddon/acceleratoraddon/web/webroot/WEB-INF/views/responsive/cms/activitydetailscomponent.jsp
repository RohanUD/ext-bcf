<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="guest" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/util"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="activity-listing-banner">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="image">
			    <c:if test="${not empty activityDetails.dataMedia}">
				    <img class='js-responsive-image' alt='${activityDetails.dataMediaAltText}' title='${activityDetails.dataMediaAltText}' data-media='${activityDetails.dataMedia}' />
				</c:if>
				<div class="activity-banner-text-box">
					<div class="container">
						<div class="accommodation-name-awards">
							<div class="row">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
									<div class="white-icon pt-5">
										<c:forEach items="${activityDetails.activityCategoryTypes}" var="categoryType">
											<span class="${categoryType}"></span>
										</c:forEach>
									</div>
									<p class="text-uppercase my-4">
										<spring:theme code="activity.details.label.activity" text="ACTIVITY" />
									</p>
									<h1 class="font-weight-bold mb-5 mt-0">${activityDetails.name}</h1>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 location-section fnt-12">
									<span class="location-icon">
										<i class="fas fa-map-marker-alt"></i>
									</span>
									<span class="location-text">${activityDetails.destination.name},${activityDetails.destination.superlocation[0].name}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 margin-top-20 margin-bottom-30 padding-0">
			<h2 class="padding-top-20 padding-bottom-20"><spring:theme code="activity.details.label.details" text="DETAILS" /></h2>
			<div>${activityDetails.description}</div>
		</div>
	<div class="row activity-listing">
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 p-relative availability-box-space">
			<div class="availability-box">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="available-price-sec">
							<h5 class="bc-text-blue">
								<spring:theme code="activity.details.available.in.text" text="AVAILABLE IN" />
								&nbsp;${activityDetails.destination.name}, ${activityDetails.destination.superlocation[0].name}&nbsp;
								<fmt:formatDate value="${activityAvailibilityStart}" var="activityAvailibilityStart" pattern="MMM dd" />
								<fmt:formatDate value="${activityAvailibilityEnd}" var="activityAvailibilityEnd" pattern="MMM dd" />
								<span class="normal-weight"><spring:theme code="activity.details.from.text" text="FROM"/></span> ${activityAvailibilityStart} ${activityAvailibilityEnd}
							</h5>
							<div class="activities-deatils-price text-center">
								<div class="font-weight-bold text-blue price-text">
									<spring:theme code="text.activity.details.peradult.price" arguments="${activityDetails.price.actualRate.formattedValue}" />
								</div>
								<p class="fnt-14 price-sub-text">
									<spring:theme code="text.activity.details.peradult.price1" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <c:url var="activityDetailsAddToCartUrl" value="/perform/add-activity-to-cart" />
						<form:form commandName="addActivityToCartData" class="y_addActivityToCartData" action="${activityDetailsAddToCartUrl}" method="POST">
							<fieldset>
								<form:input type="hidden" path="activityCode" />
								<form:input type="hidden" path="changeActivity" value="${changeActivity}"/>
								<form:input type="hidden" path="changeActivityCode" value="${changeActivityCode}" />
							    <form:input type="hidden" path="changeActivityRef" value="${changeActivityRef}" />
								<c:if test="${changeActivity}">
								    <form:input type="hidden" path="changeActivityDate" value="${changeActivityDate}" />
                                    <form:input type="hidden" path="changeActivityTime" value="${changeActivityTime}" />
								</c:if>
                                <c:if test="${isOrderAmend}">
                                    <form:input type="hidden" path="isAmendOrder" value="${isOrderAmend}" />
                                    <form:input type="hidden" path="bookingReference" value="${amendOrderCode}" />
                                </c:if>

								<div class="row mb-5">
									<h4 class="font-weight-bold col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<spring:theme code="text.activitiesdetails.date.label" text="Date & Time" />
									</h4>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="vacation-calender-section activities-details-calender">
											<ul class="nav nav-tabs vacation-calender">
												<li class="tab-links tab-depart add-full-width activity-tour-date">
													<label>
														<spring:theme code="text.activitiesdetails.selectdate.label" text="Select a date" />
													</label>
													<a data-toggle="tab" href="#check-in" class="nav-link component-to-top">
														<div class="vacation-calen-box vacation-ui-depart">
															<span class="vacation-calen-date-txt"><spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" /></span>
															<c:choose>
                                                                <c:when test="${isAmendOrder}">
                                                                    <span class="vacation-calen-year-txt">${addActivityToCartData.activityDate}</span>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <span class="vacation-calen-year-txt current-year-custom"></span>
                                                                </c:otherwise>
															</c:choose>
															<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big" aria-hidden="true"></i>
														</div>
													</a>
												</li>
												<c:if test="${not empty activityDetails.activitySchedules}">
                                                    <li class="tab-links tab-depart activity-tour-time">
                                                        <label>
                                                            <spring:theme code="text.activitiesdetails.selecttime1.label" text="Select a time" />
                                                        </label>
                                                        <div class="activities-details-select">
                                                            <form:select class="y_startTime selectpicker" path="startTime" cssErrorClass="fieldError">
                                                                <form:option value="" disabled="true" selected="true">
                                                                    <spring:theme code="activity.details.select.time.option.text" text="Tour time" />
                                                                </form:option>

                                                                <c:forEach var="activitySchedule" items="${activityDetails.activitySchedules}">
                                                                    <fmt:formatDate value="${activitySchedule.startDate}" var="startDateFormatted" pattern="MM/dd/yyyy" />
                                                                    <fmt:formatDate value="${activitySchedule.endDate}" var="endDateFormatted" pattern="MM/dd/yyyy" />

                                                                    <form:option value="${activitySchedule.startTime}" data-startdate="${startDateFormatted}"
                                                                        data-endDate="${endDateFormatted}"> <util:TwelveHourFormattedTime time="${activitySchedule.startTime}" /> </form:option>
                                                                </c:forEach>
                                                            </form:select>
                                                            
                                                        </div>
                                                    </li>
												</c:if>
											</ul>
											<div class="tab-content">
												<div id="check-in" class="tab-pane input-required-wrap depart-calendar">
													<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
														<label>Search date:mm/dd/yyyy</label>
														<form:input path="activityDate" type="text" class="bc-dropdown depart form-control datepicker-input valid" autocomplete="off" aria-invalid="false" />
														<div id="datepicker-activitydetail" class="bc-dropdown--big"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row test" >
										  <div class="col-xs-12">
										  <h4 class="font-weight-bold">
												<spring:theme code="label.activity.details.guests" text="Guests" />
											</h4>
										  </div>	
											
											<guest:participantData guestDatas="${addActivityToCartData.guestData}" maxGuestPerPax="${addActivityToCartData.maxGuestCountPerPaxType}"/>
										</div>
									</div>
									<div class="passenger-select-field hidden">
										<ul class="activity-msg-field">
											<li class="y_adult_selection hidden"></li>
											<li class="y_child_selection hidden"></li>
											<li class="y_total hidden font-weight-bold"></li>
										</ul>
									</div>
								</div>
								<div class="row mb-5 add_activity">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
										<form:button id="addActivityToCart" class="btn btn-primary y_addActivity custom-btn available-price-sec">
										    <c:choose>
										        <c:when test="${activityDetails.blockTypeStatus eq 'ONREQUEST'}">
										            <spring:theme code="label.activity.details.add.on.request.activity.button" text="On Request" />
										        </c:when>
										        <c:otherwise>
										            <spring:theme code="label.activity.details.add.activity.button" text="Add Activity" />
										        </c:otherwise>
                                           </c:choose>
										</form:button>
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
										<div class="fnt-14">
											<spring:theme code="text.activity.details.calltobook" />
										</div>
										<span class="fnt-14 text-blue-light">
											<spring:theme code="text.activity.details.calltobcferry1" />
										</span>
									</div>
								</div>
							</fieldset>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 margin-top-20 margin-bottom-30">
		    <h2 class="mb-4 vacation-h2"><spring:theme code="text.package.details.activity.policies" text="Activity Policies" /></h2>
			<div>${activityDetails.termsAndConditions}</div>
		</div>

	</div>
	
	<jsp:include page="/WEB-INF/views/addons/bcfstorefrontaddon/responsive/pages/vacations/vacationsDestinationDetailsPackagesComponent.jsp" />
</div>
<div class="container">
<div class="row">
<div class="col-md-12 col-xs-12 col-sm-12">
		
			<div class="accommodation-image package-list-owl activity-details-owl-carousel">
              <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(activityDetails.images)}">
				<c:forEach var="image" items="${activityDetails.images}">
					<c:set var="dataMedia" value="${image.url}" />
					<c:if test="${not empty dataMedia}">
                        <div class="item">
                            <img class='js-responsive-image' alt='${image.altText}' title='${image.altText}' data-media='${dataMedia}' />
                                <div class="slider-count">
                                    <div class="slider-img-thumb"></div>
                                    <div class="slider-num">1/5</div>
                                </div>
                        </div>
                    </c:if>
				</c:forEach>
			</div>
			</div>
		</div>
	</div>
	</div>



<div class="modal" id="y_activityAvailabilityChangeModal" tabindex="-1" role="dialog" aria-labelledby="y_activityAvailabilityChangeModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="activityAvailabilityChangeLabel">
					Activity availability
				</h3>
			</div>
			<div class="modal-body">
				<p>This booking is on request.</p>
                <p>Your credit card will only be charged when we have confirmed availability.</p>
                <p>Do you want to continue?</p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_proceedAddToCart" class="btn btn-primary btn-block">
							<spring:theme code="text.page.managemybooking.fresh.booking.confirmation" text="Confirm" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

