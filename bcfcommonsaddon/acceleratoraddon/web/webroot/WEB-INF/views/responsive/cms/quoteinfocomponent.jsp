<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="y_quoteInfoComponent">
	<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_quoteinfoComponentId" />
	<c:if test="${quoteExpirationTime !=null}">
		<input type="hidden" value="${fn:escapeXml(quoteExpirationTime)}" id="y_quoteExpirationTime" />
		<div id="y_timer"></div>
	</c:if>
</div>
<c:if test="${not empty removedSailingFromCart}">
	<div class="modal fade in" id="y_cartUpdatedModal" tabindex="-1" role="dialog" aria-labelledby="cartUpdatedModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<h4 class="modal-title" id="cartUpdatedModal">
						<spring:theme code="text.page.cartrestoration.sailings.removed.title" />
					</h4>
				</div>
				<div class="modal-body">
					<spring:theme code="text.page.cartrestoration.sailings.removed.message" />
					<br>
					<c:forEach items="${removedSailingFromCart}" var="removedSailing" varStatus="removedSailingIdx">
						<div>${removedSailingIdx.index+1}.&nbsp;${ removedSailing.origin.name}-${ removedSailing.destination.name}</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</c:if>