<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="displayFilters" value="true" />
<div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
        <div class="card offer-box" style="background:${backgroundColour}">
        <c:if test="${not empty featuredText}">
             <div class="featured-bx">${featuredText}</div>
         </c:if>
          <div class="card-no-body row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              
                <div class="card-body">
                  <h2 class="card-title">${activityProductDataForFeatured.name}</h2>
                  <div class="card-text">${activityProductDataForFeatured.promoText}</div>
                  <c:if test="${not empty link}">
                        <button class="btn btn-default">
                            <cms:component component="${link}" evaluateRestriction="true" />
                        </button>
                    </c:if>
                </div>
              </div>
              <div class="col-lg-8 col-xs-12 card-media-wrapper card-media-right">
                  <c:if test="${not empty activityProductDataForFeatured.dataMedia}">
                     <img class='js-responsive-image' alt='${activityProductDataForFeatured.dataMediaAltText}' title='${activityProductDataForFeatured.dataMediaAltText}' data-media='${activityProductDataForFeatured.dataMedia}' />
                  </c:if>
                 <div class="from-circle">
                     <span class="text-light-blue"> ${activityProductDataForFeatured.price.actualRate.formattedValue}*</span>
                 </div>
              </div>
           </div>
        </div>
        </div>
    </div>
</div>
