<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<c:url var="nextPageUrl" value="/cart/summary" />
<spring:htmlEscape defaultHtmlEscape="false" />
<progress:travelBookingProgressBar stage="activities" amend="${amend}" bookingJourney="${bookingJourney}" />
<div class="row">
	<div class="col-xs-12 col-sm-12 side-to-top">
		<cms:pageSlot position="TripContent" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
</div>
<c:if test="${not empty globalReservationDataList.packageReservations}">
	<c:if test="${activityRemoved eq true}">
		<div class="container">
		<div class="row">
		<div class="alert alert-success alert-dismissable text-center">
			<div class="internal-container">
			    <span class="bcf bcf-icon-checkmark text-bold"></span>
				<span class="itemremove text-bold"><spring:theme code="label.dealactivitylisting.text.itemremove" text="The item has been successfully removed." /></span>
			    <span class="notintotal"><spring:theme code="label.dealactivitylisting.text.notintotal" text="It will no longer be reflected in your total." /></span>
		    </div>
		</div>
		</div>
		</div>
	</c:if>
	<c:forEach items="${globalReservationDataList.packageReservations.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
		<c:if test="${not empty packageReservation.activityReservations}">
			<bcfglobalreservation:activityReservations activityReservationDataList="${packageReservation.activityReservations}" isDealPackage="true"/>
			<c:set var="activityReservations" value="true" />
		</c:if>
	</c:forEach>
	<div class="row">
	    <div class="col-md-4 col-md-offset-4">
	<c:if test="${activityReservations eq true or activityRemoved eq true}">
		<a href="${fn:escapeXml(nextPageUrl)}" class="btn btn-primary btn-block">
			<spring:theme code="label.dealactivitylisting.link.continueandreviewpackage" text="Continue and review package" />
		</a>
	</c:if>
	    </div>
	</div>
</c:if>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="h2">
				<strong><spring:theme code="activitysearch.title" text="Please choose your Activity" /></strong>
			</h2>
		</div>
	</div>
	<!-- <div class="row margin-bottom-20">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 font-italic"></div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-right" id="y_displaySortSelection">
			<activities:dealActivitySortSelect activityFinderForm="${activityFinderForm}" activitiesResponseDataList="${facetSearchPageData}" />
		</div>
	</div> -->
	<div class="accommodation-selection-wrap clearfix y_accommodationSelectionSection y_nonItineraryContentArea">
		<cms:pageSlot position="ActivityRefinement" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>

        <div class="viewResults y_nonItineraryContentArea">
            <c:choose>
        	    <c:when test="${fn:length(facetSearchPageData.activityResponseData) == 0}">
        		    <p>
        			    <spring:theme code="text.activity.noresult" text="No Activities Available" />
        		    </p>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="${fn:escapeXml(nextPageUrl)}" class="btn btn-primary btn-block">
                                <spring:theme code="label.dealactivitylisting.link.continueandreviewpackage" text="Continue and review package" />
                            </a>
	                    </div>
	                </div>
        	    </c:when>
        	    <c:otherwise>
        		    <div class="results-list" id="y_activityResults" aria-label="activity search results">
                        <activities:dealActivityDetailsList />
                    </div>
                    <div class="y_paginationContent">

                    <hr/>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="${fn:escapeXml(nextPageUrl)}" class="btn btn-primary btn-block">
                                <spring:theme code="label.dealactivitylisting.link.continueandreviewpackage" text="Continue and review package" />
                            </a>
	                    </div>
	                </div>
                </c:otherwise>
            </c:choose>
            <pagination:globalpagination/>
         </div>
	</div>
</div>


<div class="modal" id="y_activityAvailabilityChangeModal" tabindex="-1" role="dialog" aria-labelledby="y_activityAvailabilityChangeModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="activityAvailabilityChangeLabel">
					Activity availability
				</h3>
			</div>
			<div class="modal-body">
				<p>This booking is on request.</p>
                <p>Your credit card will only be charged when we have confirmed availability.</p>
                <p>Do you want to continue?</p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_proceedAddToCart" class="btn btn-primary btn-block">
							<spring:theme code="text.page.managemybooking.fresh.booking.confirmation" text="Confirm" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block">
							<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
