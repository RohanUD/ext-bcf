<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="confirmationpage" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/confirmationpage"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:choose>
	<c:when test="${bookingJourney=='BOOKING_TRANSPORT_ONLY'}">
		<confirmationpage:transportsummary reservationDataList="${globalReservationData.transportReservations}"></confirmationpage:transportsummary>
	</c:when>
	<c:otherwise>
	    <div class="col-lg-12 col-md-12 col-xs-12 confirm-green-bg margin-bottom-30">
            <div class="text-center">
                <bcfglobalreservation:bookingConfirmation />
            </div>
        </div>
        <div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<bcfglobalreservation:reservationSummary />
			</div>
		</div>
        <hr>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <bookingDetails:termsAndConditions/>
                </div>
        </div>

	    <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <c:if test="${not empty propertyTermsConditionMap}">
                    <bcfglobalreservation:propertyTermsCondition propertyTermsConditionMap="${propertyTermsConditionMap}" />
                </c:if>
            </div>
        </div>

		<div class="row">
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        				<c:if test="${not empty activityTermsConditionMap}">
        					<bcfglobalreservation:activityTermsCondition activityTermsConditionMap="${activityTermsConditionMap}" />
        				</c:if>
            </div>
        </div>
                </div>
	</c:otherwise>
</c:choose>

