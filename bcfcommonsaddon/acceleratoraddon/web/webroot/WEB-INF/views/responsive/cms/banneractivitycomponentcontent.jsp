<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="displayFilters" value="true" />
<div class="card bg-dark text-white hero-max-height" style="background:${backgroundColour}">
  <c:if test="${not empty activityProductDataForBanner.dataMediaAltText}">
    <img class='js-responsive-image card-img' alt='${activityProductDataForBanner.dataMediaAltText}' title='${activityProductDataForBanner.dataMediaAltText}' data-media='${activityProductDataForBanner.dataMedia}' />
  </c:if>
  <div class="card-img-overlay">
    <div class="container">
        <div class="accommodation-name-awards row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="deal-details-icon pt-5 deal-labels">
                    <c:forEach items="${activityProductDataForBanner.activityCategoryTypes}" var="categoryType">
                        <span class="${categoryType}"></span>
                    </c:forEach>
                </div>
                <p class="card-text">Featured Activity</p>
                <h1 class="card-title">${activityProductDataForBanner.name}</h1>
                <a class="btn btn-default y_viewDetails"><spring:theme code="text.deal.listing.selection" /> ></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="from-circle"><span class="text-light-blue display-block">FROM</span><span class="text-dark-blue">${activityProductDataForBanner.price.actualRate.formattedValue}*</span></div>
            </div>
        </div>
    </div>
  </div>
</div>
