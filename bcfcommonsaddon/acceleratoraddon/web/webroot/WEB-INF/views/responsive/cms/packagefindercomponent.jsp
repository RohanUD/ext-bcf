<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="travelfinder" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/travelfinder"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="travelFinderUrl" value="/view/PackageFinderComponentController/search" />
<div id="tabs-2" class="travelforms">
    <div class="bc-search-container__form-box pt-2 pb-2 box-2 package-hotel-details-tabs">
      <div class="custom-error-3 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>

        <form:form commandName="travelFinderForm" action="${travelFinderUrl}" method="POST" class="fe-validate form-background form-booking-trip" id="y_travelFinderForm">



            <fieldset>
                <c:if test="${hideFinderTitle ne true}">
                <div class="img-center-mobile">
                   <img src="${commonResourcePath}/images/image-logo.jpg">
                </div>
                <span><form:errors path="*" cssClass="error"/></span>
                
                    <legend class="with-icon heading-booking-trip primary-legend hidden" id="trip-finder-modal-title">
                        <c:if test="${!showComponent}">
                        <a role="button" data-toggle="collapse" aria-expanded="true" data-target=".panel-modify" class="panel-heading panel-header-link collapsable collapsed">
                            </c:if>
                            <c:choose>
                                <c:when test="${!showComponent}">
                                    <spring:theme code="text.cms.farefinder.modify.title" text="Modify Booking" />
                                </c:when>
                                <c:otherwise>
                                    <spring:theme code="text.cms.farefinder.title" text="Booking Trip" />
                                </c:otherwise>
                            </c:choose>
                            <c:if test="${!showComponent}">
                        </a>
                        </c:if>
                    </legend>
                </c:if>
                <c:if test="${hideFinderTitle ne true}">
                    <c:if test="${!showComponent}">
                        <a role="button" data-toggle="collapse" aria-expanded="true" data-target=".panel-modify" class="panel-heading panel-header-link collapsable collapsed">
                    </c:if>
                    <c:if test="${!showComponent}">
                        <spring:theme code="text.cms.accommodationfinder.modify.title" text="Modify Search" />
                    </c:if>
                    <c:if test="${!showComponent}">
                        </a>
                    </c:if>
                    </legend>
                </c:if>
                <div class="${ showComponent ? '' : 'panel-modify collapse'} ">
                    <div id="accommodation-finder-panel" class="fieldset-inner-wrapper">
                        <c:set var="formPrefix" value="" />
                        <travelfinder:travelfinderform formPrefix="${formPrefix}" idPrefix="travel" />
                        <div class="row vacation-flow-hide">
                                <div class="col-md-12 col-sm-12 col-xs-12 pt-2 pt-xl-0">
                                <label class="font-weight-light">
                                    <spring:theme code="text.vacation.packagefinder.numberofrooms.title" text="Number of rooms" />
                                </label>
                                <label class="sr-only" for="hotelAccommodationQuantity">
                                    <spring:theme var="roomQuantityPlaceholderText" code="text.cms.accommodationfinder.accommodationquantity.placeholder" text="Accommodation Quantity" /> ${fn:escapeXml(roomQuantityPlaceholderText)}
                                </label>
                                <form:select class="bc-dropdown bc-dropdown--big  y_accommodationRoomQuantity" id="hotelAccommodationQuantity" path="accommodationFinderForm.numberOfRooms">
                                    <c:forEach var="entry" items="${accommodationsQuantity}">
                                        <form:option value="${fn:trim(entry)}" htmlEscape="true" />
                                    </c:forEach>
                                </form:select>
                            </div>
                      </div>
                                <div class="row vacation-flow-hide">
                                        <c:forEach var="roomCandidatesEntry" items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}" varStatus='idx'>
                                            <div id="room[${fn:escapeXml(idx.index+1)}]" class="guest-types  ${ idx.index >= travelFinderForm.accommodationFinderForm.numberOfRooms ? 'hidden' : ''}">
                                              <div class="col-xs-12 col-md-12 col-sm-12">
                                                <label class="font-weight-light">
                                                    <spring:theme code="text.cms.accommodationfinder.room" arguments="${idx.index+1}" />
                                                </label>
                                              </div>

                                                    <c:set var="formPrefix" value="accommodationFinderForm.roomStayCandidates[${idx.index}]." />
                                                    <form:input path="${fn:escapeXml(formPrefix)}roomStayCandidateRefNumber" type="hidden" />
                                                    <passenger:passengertypequantity formPrefix="${formPrefix}" passengerTypeQuantityList="${roomCandidatesEntry.passengerTypeQuantityList}" />

                                            </div>
                                        </c:forEach>
                                        <div class="col-xs-12 y_roomStayCandidatesError"></div>
                                </div>


                      <div class="row vacation-flow-hide">
                        <div class="col-xs-2 btn-wrapper full-width">
                            <label class="invisible font-weight-light">Confirm Button</label>
                            <c:choose>
                                <c:when test="${empty travelRouteData}">
                                    <form:button class="confirm-btn confirm-btn--blue" value="Submit">
                                        <spring:theme code="text.cms.accommodationfinder.button.submit" text="Search" />
                                    </form:button>
                                </c:when>
                                <c:otherwise>
                                    <form:button class="confirm-btn confirm-btn--blue" type="button" value="Submit" id="searchRooms" >
                                        <spring:theme code="text.cms.accommodationfinder.packagepromotion.button.submit" text="Search" />
                                    </form:button>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>

                        <%--  Begin CTA (Search Accommodations) section --%>
                </div>
                    <%--  End CTA (Search Accommodations) section --%>

                    <%-- / .fieldset-inner-wrapper --%>
            </fieldset>
        </form:form>
    </div>
</div>
