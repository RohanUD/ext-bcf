<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template"
           tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format"
           tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<%-- Page Title and description --%>
<div class="panel panel-primary panel-list">
    <div class="panel-heading">
        <h1 class="title"><spring:theme code="text.schedulesLanding.todaysSchedule.label" text="Today's schedule"/></h1>
        <p><spring:theme code="text.schedulesLanding.selectRegion.label" text="Select a region"/></p>
    </div>
</div>

<div class="right">
    <spring:theme code="text.cc.view.label" text="View"/><br/>
    <a id="ccLandingList" href="" onclick="return false">  <spring:theme code="text.view.list.label" text="List"/> </a>&nbsp;
    <a id="ccLandingMap" href="" onclick=" return false"><spring:theme code="text.view.map.label" text="Map"/></a>
</div>

<hr/>
<c:forEach items="${schedules}" var="entry">
    <h2>${entry.key}</h2>
    <c:set var="routes" value="${entry.value}"/>
    <c:forEach items="${routes}" var="route" varStatus="couter">
        <a href=""> ${route.sourceLocationName} (route.sourceTerminalName) - ${route.destinationLocationName} (${route.destinationTerminalName}) </a>
    </c:forEach>
</c:forEach>
