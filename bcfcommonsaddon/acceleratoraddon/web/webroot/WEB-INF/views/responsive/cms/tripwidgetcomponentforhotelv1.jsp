<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border-right m-pb-2 package-header-top-box">
				<p>
					<spring:theme code="text.tripwidget.destination" />
				</p>
				<p class="m-0">
					<span class="mr-2 font-weight-bold">${accommodationFinderForm.destinationLocationName}</span>
					<fmt:parseDate value="${accommodationFinderForm.checkInDateTime}" var="parsedCheckInDateTime" pattern="MM/dd/yyyy" />
					<fmt:parseDate value="${accommodationFinderForm.checkOutDateTime}" var="parsedCheckOutDateTime" pattern="MM/dd/yyyy" />
					<c:set var="numberOfNights">
						<fmt:parseNumber value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}" integerOnly="true" />
					</c:set>
					<c:choose>
						<c:when test="${numberOfNights eq 1}">
							<spring:theme code="text.tripwidget.night" arguments="${numberOfNights}" />
						</c:when>
						<c:otherwise>
                                (<spring:theme code="text.tripwidget.nights" arguments="${numberOfNights}" />)
                            </c:otherwise>
					</c:choose>
				</p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 package-header-top-box">
				<p>
					<spring:theme code="text.tripwidget.stay" />
				</p>
				<fmt:parseDate value="${accommodationFinderForm.checkInDateTime}" var="parsedDepartDate" pattern="MM/dd/yyyy" />
				<fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />
				<div class="font-weight-bold">${formattedParsedDepartDate}
					<span class="ml-2">
						<spring:theme code="text.tripwidget.to" />
					</span>
				</div>
				<fmt:parseDate value="${accommodationFinderForm.checkOutDateTime}" var="parsedReturnDate" pattern="MM/dd/yyyy" />
				<fmt:formatDate value="${parsedReturnDate}" var="formattedParsedReturnDate" pattern="E, MMM dd" />
				<div class="font-weight-bold">${formattedParsedReturnDate}</div>
			</div>
		</div>
	</div>
</div>
