<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="viewResults y_nonItineraryContentArea">
   <c:choose>
      <c:when test="${fn:length(facetSearchPageData.activityResponseData) == 0}">
         <p>
            <spring:theme code="text.activity.noresult" text="No Activities Available" />
         </p>
      </c:when>
      <c:otherwise>
         <div class="results-list" id="y_activityResults" aria-label="activity search results">
            <activities:activityDetailsList />
         </div>
      </c:otherwise>
   </c:choose>
   <pagination:globalpagination/>
</div>
