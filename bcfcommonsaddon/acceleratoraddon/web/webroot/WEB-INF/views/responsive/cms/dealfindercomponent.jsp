<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:url var="dealFinderUrl" value="/view/DealFinderComponentController/search" />
<form:form commandName="dealFinderForm" action="${fn:escapeXml(dealFinderUrl)}" method="POST" class="fe-validate form-background form-booking-trip" id="y_dealFinderForm">
<div class="container p-0">
            
	<div class="row ">

	<div class="input-required-wrap col-xs-12 col-sm-4">

    		<label for="fromDealLocation" class="home-label-mobile"> FROM
    			<spring:theme var="departureLocationPlaceholderText" code="text.cms.farefinder.departure.location.placeholder" text="From" />
    		</label>
    		<form:input type="text" id="fromDealLocation" path="${fn:escapeXml(formPrefix)}departureDealLocationName" cssErrorClass="fieldError" class="y_dealFinderOriginLocation input-grid col-xs-12 form-control" placeholder="${fn:escapeXml(departureLocationPlaceholderText)}" autocomplete="off" />
    		<form:hidden path="${fn:escapeXml(formPrefix)}departureLocation" class="y_dealFinderOriginLocationCode" maxlenght="5"/>
    		<%-- <form:hidden path="${fn:escapeXml(formPrefix)}departureLocationSuggestionType" class="y_dealFinderOriginLocationSuggestionType" />--%>
    		<div id="y_dealFinderOriginLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
    	</div>

<div class="input-required-wrap col-xs-12 col-sm-4">
		<label for="toDealLocation" class="home-label-mobile"> TO
			<spring:theme var="arrivalLocationPlaceholderText" code="text.cms.farefinder.arrival.location.placeholder" text="To" />
		</label>
		<form:input type="text" id="toDealLocation" path="${fn:escapeXml(formPrefix)}arrivalDealLocationName" cssErrorClass="fieldError" class="col-xs-12 y_dealFinderDestinationLocation input-grid form-control" placeholder="${fn:escapeXml(arrivalLocationPlaceholderText)}" autocomplete="off" />
		<form:hidden path="${fn:escapeXml(formPrefix)}arrivalLocation" class=" y_dealFinderDestinationLocationCode" maxlenght="5"/>
		<%-- <form:hidden path="${fn:escapeXml(formPrefix)}arrivalLocationSuggestionType" class="y_dealFinderDestinationLocationSuggestionType" />--%>
		<div id="y_dealFinderDestinationLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
	</div>
	<div class="input-required-wrap col-xs-12 col-sm-4">
     <label for="hotelName" class="home-label-mobile"> Location/Hotel
                      <spring:theme var="hotelNamePlaceholderText" code="text.cms.dealfinder.hotelname.placeholder" text="Location" />
             </label>
    		<label class="sr-only" for="y_accommodationDealFinderLocation">
    			<spring:theme var="destinationLocationPlaceholderText" code="text.cms.accommodationfinder.destination.placeholder" text="Where are you going?" />
    			${fn:escapeXml(destinationLocationPlaceholderText)}
    		</label>
    		<form:input type="search" class="y_accommodationDealFinderLocation input-grid col-xs-12 form-control" id="y_accommodationDealFinderLocation" path="${fn:escapeXml(formPrefix)}location" cssErrorClass="fieldError" placeholder="${fn:escapeXml(destinationLocationPlaceholderText)}" autocomplete="off" />
    		<form:hidden path="destinationLocation" class="y_accommodationDealFinderLocationCode" />
    		<form:hidden path="latitude" class="y_accommodationDealFinderLocationLatitude" />
    		<form:hidden path="longitude" class="y_accommodationDealFinderLocationLongitude" />
    		<form:hidden path="suggestionType" class="y_accommodationDealFinderLocationSuggestionType" />
    		<div id="y_accommodationDealFinderLocationSuggestions" class="hidden clearfix"></div>
    	</div>
	</div>
	

	

<div class="row">
    <div class="input-required-wrap  col-xs-12 col-sm-4" id="js-return">
		<label for="departingDateTime" class="home-label-mobile">Departure
			<spring:theme var="departingDatePlaceholderText" code="text.cms.dealfinder.departure.date.placeholder" text="Departure Date" />
		</label>
		<form:input type="text" path="${fn:escapeXml(formPrefix)}departureDate" class="col-xs-12 datepicker input-grid form-control y_fareFinderDatePickerDeparting y_transportDepartDate" placeholder="${fn:escapeXml(departingDatePlaceholderText)}" autocomplete="off" />
		 <div class="y_departureDateError col-md-12 departureDate-error"></div>
	</div>

   <div class="input-required-wrap col-xs-12 col-sm-4">
    	<label for="duration" class="home-label-mobile"> Duration
        	<spring:theme var="starRatingPlaceholderText" code="text.cms.dealfinder.duration.placeholder" text="Duration" />
        </label>
        <form:input path="duration" class="form-control" />
         <div class="y_durationError col-md-12 duration-error"></div>
    </div>


    <div  class="input-required-wrap col-xs-12 col-sm-4">
	<label for="starRating" class="home-label-mobile"> Star Rating
    	<spring:theme var="starRatingPlaceholderText" code="text.cms.dealfinder.starRating.placeholder" text="Star Rating" />
    </label>
    <form:select class="form-control ${fn:escapeXml(starRating)} y_${fn:escapeXml(starRating)}Select" id="y_${fn:escapeXml(starRating)}" path="${fn:escapeXml(formPrefix)}starRating" cssErrorClass="fieldError">
   			<option value="" selected="selected" disabled="disabled"><spring:message code="deals.pleaseSelect"/></option>
   			<c:forEach begin="1"
            end="5"
            varStatus="loop">
            <form:option value="${fn:escapeXml(loop.index)}"> ${fn:escapeXml(loop.index)} </form:option>
            </c:forEach>
            </form:select>
	</div>
    </div>

<div class="row input-row less-margin">
   <div  class="input-required-wrap col-xs-12 col-sm-12">
         <label for="vacationExperience" class="home-label-mobile"> Vacation Experience
            <spring:theme var="vacationExperiencePlaceholderText" code="text.cms.dealfinder.vacationExperience.placeholder" text="Vacation Experience" />
         </label>
         <form:select class="form-control ${fn:escapeXml(dealType)} y_${fn:escapeXml(dealType)}Select" multiple="false" id="y_${fn:escapeXml(dealType)}" path="${fn:escapeXml(formPrefix)}dealType" cssErrorClass="fieldError">
         		<option value="" selected="selected" disabled="disabled"><spring:message code="vacations.dealType.pleaseSelect"/></option>
         		<c:forEach var="entry" items="${dealTypeMap}">
         		<form:option value="${entry.key}">${entry.value}</form:option>
                </c:forEach>
         </form:select>


    </div>
</div>


<div class="row">
    	<div class="col-xl-12 mt-4">
			<button class="confirm-btn confirm-btn--blue" type="submit" id ="dealFinderFindButton" value="Submit">FIND</button>
		</div>
		</div>

		
		</div>
</form:form>
