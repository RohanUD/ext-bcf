<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />


<c:url var="submitRequestUrl" value="/view/ChangeBookingFormComponentController/change-booking" />

<div class="modal fade" id="y_changeBookingFormModal" tabindex="-1" role="dialog" aria-labelledby="y_changeBookingFormModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="limitedSailingsLabel">
					<spring:theme code="text.changebooking.form.request" text="Change Booking" />
				</h3>
			</div>
            <div class="modal-body">

			<form:form id="y_changeBookingForm" modelAttribute="changeBookingForm" action="${submitRequestUrl}" method="POST" class="y_changeBookingForm">
            			<spring:theme code="text.changebooking.form.request" text="Change Booking" />
				<div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="form-group">
				    <spring:theme code="text.changebooking.form.firstname"/>
				    <form:input type="text" path="firstname" class="form-control" />
				    <form:errors path="firstname" />
				  </div>
				  
				  <div class="form-group">
				    <spring:theme code="text.changebooking.form.lastname"/>
				    <form:input type="text" path="lastname" class="form-control"  />
				    <form:errors path="lastname" />
				  </div>
				  
				  <div class="form-group">
				   <spring:theme code="text.changebooking.form.email"/>
				    <form:input type="text" path="email" class="form-control"  />
				    <form:errors  path="email" />
				  </div>
				  
				  <div class="form-group">
				    <spring:theme code="text.changebooking.form.contact"/>
				    <form:input type="text" path="contactnumber" class="form-control"  />
				    <form:errors path="contactnumber" />
				  </div>
				  
                    <input type="hidden" name="orderCode" id="orderCode" value="${fn:escapeXml(bookingReference)}">
                    <input type="hidden" name="changeBookingErrorFlag" id="changeBookingErrorFlag" value="${changeBookingErrorFlag}">
                    <input type="hidden" name="guestBookingIdentifier" id="guestBookingIdentifier" value="${guestBookingIdentifier}">

                 <div class="form-group">
				    <spring:theme code="text.changebooking.form.changedetails"/>
				   <form:textarea path="description" class="y_requestMessage form-control" id="description" maxlength="255" />
				     <form:errors  path="description" />
				 </div>
				  
            	</div>
            	<div class="form-group col-xs-12 col-sm-4 col-xs-offset-0 col-sm-offset-8">
            		<button type="submit" class="btn btn-primary btn-block y_addRequestButton">
            			<spring:theme code="button.changebooking.form.submit" />
            		</button>
            	</div>
            	</div>
            </form:form>
 	        </div>
		</div>
	</div>
</div>

        <div class="col-md-6">
            <button class="btn btn-primary btn-block" type="submit" id="changeBookingFormButton" value="Submit">
            <spring:theme code="button.changebooking.form.request" text="Change Booking Form" />
            </button>
        </div>

