<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="emailItinerary" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/emailitinerary"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<cart:asmSupplierComments />

<c:if test="${displayTravelBookingDetailsComponent}">
    <c:if test="${asmLoggedIn}">
        <bookingDetails:asmCommonBookingDetails userRoles="${userRoles}" asmOrderData="${asmOrderData}"/>
    </c:if>
    <hr>
<c:choose>
	<c:when test="${empty pageNotAvailable}">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-lg-8">
					<dl class="booking status">
						<dt>
							<spring:theme code="text.page.managemybooking.bookingstatus" text="Booking Status:" />
						</dt>
						<dd>
							<bookingDetails:status code="${globalReservationData.bookingStatusCode}" name="${globalReservationData.bookingStatusName}" />
						</dd>
					</dl>
					<p class="booking-status-code"><spring:theme code="text.page.mybooking.packageId" /> <span class="reservation-code"> ${globalReservationData.code}</span></p>

				</div>
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<input type="hidden" value="${fn:escapeXml(bookingReference)}" name="bookingReference" id="bookingReference">
					<commonsBookingDetails:globalBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="CANCEL_BOOKING" />
				</div>

                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <input type="hidden" value="${fn:escapeXml(bookingReference)}" name="bookingReference" id="bookingReference">
                    <commonsBookingDetails:globalBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="APPLY_GOODWILL_REFUND" />
               </div>
		</div>

        <cms:pageSlot position="InternalComments" var="features">
            <cms:component component="${features}" element="section" />
        </cms:pageSlot>

		<c:choose>
        	<c:when test="${bookingJourney eq 'BOOKING_ALACARTE'}">
        	<bookingDetails:accommodationBookingDetails accommodationReservationData="${globalReservationData.accommodationReservationData}" isTravelSite="true" customerReviews="${customerReviews}" />

             <c:if test="${not empty bcfGlobalReservationData}">
		        <bcfglobalreservation:totalFare globalReservationData="${bcfGlobalReservationData}" />
		     </c:if>

               <commonsBookingDetails:globalBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_ALACARTE_BOOKING" />


        	</c:when>
        	<c:otherwise>

        <c:if test="${globalReservationData.accommodationReservationData != null}">
			<bookingDetails:accommodationBookingDetails accommodationReservationData="${globalReservationData.accommodationReservationData}" isTravelSite="true" customerReviews="${customerReviews}" />
		</c:if>

		<c:if test="${globalReservationData.accommodationReservationData ==null && globalReservationData.reservationData != null}">
			<bookingDetails:transportBookingDetails reservationData="${globalReservationData.reservationData}" />
		</c:if>
		<c:if test="${globalReservationData.activityReservations ==null}">
        	<bcfglobalreservation:activityReservations activityReservationDataList="${bcfGlobalReservationData.activityReservations}"  actionType="EDIT_BOOKING" />
        </c:if>
		<c:if test="${globalReservationData.reservationData.bookingStatusCode != 'ACTIVE_DISRUPTED_PENDING'}">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
						<c:if test="${globalReservationData.accommodationReservationData != null}">
							<c:set var="actionName">
								<spring:theme code="button.booking.details.accommodation.booking.action.cancel.transport.booking" text="Cancel Flight Booking" />
							</c:set>

						</c:if>
					</div>
				</div>
		</c:if>

        <hr>

        <c:if test="${not empty bcfGlobalReservationData}">
            <bcfglobalreservation:totalFare globalReservationData="${bcfGlobalReservationData}" />
        </c:if>
			</c:otherwise>
                </c:choose>
		<div class="y_cancelBookingConfirm"></div>

    <div class="row mb-2 mt-4">
        <div class="col-xs-6 text-center">
            <emailItinerary:emailItinerary/>
        </div>
    </div>
	</c:when>
	<c:otherwise>
		<div class="alert alert-danger " role="alert">
			<p>
				<spring:theme code="${pageNotAvailable}" />
			</p>
		</div>
	</c:otherwise>
</c:choose>
<commonsBookingDetails:addRoomToPackageErrorModal/>
<div class="modal transparent fade" id="y_addRoomModal" tabindex="-1" role="dialog" aria-hidden="true"></div>
</c:if>
