<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="packagedetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<progress:travelBookingProgressBar stage="sailing" amend="${amend}" bookingJourney="${bookingJourney}" />
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<ferryselection:packagesailingdetails travelFinderForm="${travelFinderForm}" isReturn="${isReturn}" />
<div class="row">
       <div class="col-xs-12 col-sm-12 side-to-top">
                <cms:pageSlot position="TripContent" var="feature">
                 <cms:component component="${feature}" />
                 </cms:pageSlot>
             </div >
       </div>
<div class="y_packageFerrySelectionPage">
		<div class="white-bg">
				<c:choose>
					<c:when test="${not empty fareSelection && not empty fareSelection.pricedItineraries}">
				
						<input type="hidden" id="y_tripType"
							value="${fn:escapeXml(tripType)}" />
							
							<div class="container">
						<c:if test="${isReturn == 'false'}">
					<div class="row margin-top-20">
						<div class="fare-table-inner-wrap col-lg-12 col-md-12 col-xs-12">
							<h4 class="font-weight-bold">
								<spring:theme code="label.packageferryselection.fareselection.departure" />
							</h4>
							<p class="mb-2">
								<spring:theme code="label.packageferryselection.fareselection.departure.description" />
							</p>

							<i class="last-updated-txt"> <spring:theme code="label.packageferryselection.fareselection.lastupdated" />${dateNow}</i>
							<hr>
						</div>
					</div>

					<div class="row">
					<div class="col-md-4 col-xs-6">
						<h4 class="font-weight-bold">
							<spring:theme code="label.fareselection.sailingoptions" />
						</h4>
					</div>
					<div class="col-md-4 col-xs-6 d-xl-none d-lg-none pull-right">
						<div class="p-icon-box time-tabs-section">
							<div class="p-icon time-tab active">
								<span> <input type="radio" name="sailingTime" value="All">
									All
								</span>
							</div>
							<div class="p-icon time-tab">
								<span> <input type="radio" name="sailingTime" value="am">
									am
								</span>
							</div>
							<div class="p-icon time-tab">
								<span> <input type="radio" name="sailingTime" value="pm">
									pm
								</span>
							</div>
						</div>
					</div>

					<div class="col-md-4 d-none d-sm-block pull-right">
						<div class="p-icon-box font-size-17">
							<div class="p-icon time-tab active">
								<span> <u><strong><input type="radio"
											name="sailingTime" value="All"> All</strong></u>
								</span>
							</div>
							<div class="p-icon time-tab">
								<span> <input type="radio" name="sailingTime" value="am">
									am
								</span>
							</div>
							<div class="p-icon time-tab">
								<span> <input type="radio" name="sailingTime" value="pm">
									pm
								</span>
							</div>
						</div>
					</div>
				</div>


				<div id="y_outbound" class="y_fareResultTabWrapper ">
					<ferryselection:ferryschedule fareSelection="${fareSelection}"
						refNumber="${outboundRefNumber}" />
				</div>
				</c:if>
				</div>
							<c:if test="${isReturn == 'true'}">
								<div class="green-bar-msg">
									<span class="bcf bcf-icon-checkmark"></span>
									<spring:theme code="text.packageferryselection.departure.selected" text="Departure sailing selection " />
									&nbsp;<strong><spring:theme code="text.packageferryselection.departure.confirmed" text="confirmed!" /></strong>
									
								</div>
								<cart:cartTimer />
								<div class="container">
								<ferryselection:transportselection type="OutBound" />
								<h2 class="margin-top-30 vacation-h2">
									<spring:theme code="label.packageferryselection.fareselection.return" />
								</h2>
								<p class="fnt-14">
								   <spring:theme code="label.packageferryselection.fareselection.departure.description" />
								</p>
								<p class="fnt-14 last-updated-txt">
									<i><spring:theme code="label.packageferryselection.fareselection.lastupdated" /> ${dateNow}</i>
								<hr>
								<div class="row">
									<div class="col-md-4 col-xs-6">
										<h4 class="vacation-h2 mt-3">
											<spring:theme code="label.fareselection.sailingoptions" />
										</h4>
									</div>
									<div class="col-md-4 col-xs-6 d-xl-none d-lg-none pull-right">
                                        <div class="p-icon-box time-tabs-section">
                                            <div class="p-icon time-tab active">
                                                <span> <input type="radio" name="sailingTime" value="All">
                                                    All
                                                </span>
                                            </div>
                                            <div class="p-icon time-tab">
                                                <span> <input type="radio" name="sailingTime" value="am">
                                                    am
                                                </span>
                                            </div>
                                            <div class="p-icon time-tab">
                                                <span> <input type="radio" name="sailingTime" value="pm">
                                                    pm
                                                </span>
                                            </div>
                                        </div>
                                    </div>
								</div>
								<div id="y_inbound" class="y_fareResultTabWrapper clearfix">
									<ferryselection:ferryschedule fareSelection="${fareSelection}" refNumber="${inboundRefNumber}" />
								</div>
								</div>
							</c:if>
						</c:when>
						<c:otherwise>
							<div class="alert alert-danger" role="alert">
								<c:forEach var="errorMessage" items="${errorMessages}">
									<p>
										<spring:theme text="${errorMessage}" />
									</p>
								</c:forEach>
							</div>
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty priceDisplayPassengerType and not empty fareSelection.pricedItineraries[0].itineraryPricingInfos[0]}">
						<div class="alert alert-warning" role="alert">
							<p>
								<c:forEach var="ptcFareBreakdownData" items="${fareSelection.pricedItineraries[0].itineraryPricingInfos[0].ptcFareBreakdownDatas}" varStatus="ptcIdx">
									<c:if test="${priceDisplayPassengerType==ptcFareBreakdownData.passengerTypeQuantity.passengerType.code}">
										<spring:theme code="text.fare.selection.disclaimer" arguments="${fn:escapeXml(ptcFareBreakdownData.passengerTypeQuantity.passengerType.name)}" />
									</c:if>
								</c:forEach>
							</p>
						</div>
					</c:if>
						<cms:pageSlot position="LeftContent" var="feature" element="section">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					
				</div>
			
	</div>
<fareselection:compareFares />
<reservation:fullReservationOverlay />
<fareselection:addBundleToCartValidationModal />
<fareselection:fareSelectionValidationModal />
<packagedetails:addPackageToCartErrorModal />
