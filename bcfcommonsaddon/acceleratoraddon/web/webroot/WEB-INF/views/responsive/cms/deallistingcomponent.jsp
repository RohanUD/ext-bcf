<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="deallisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/deallisting"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="deals">
   <div class="container">
      <div class="row margin-top-20">
         <div class="col-lg-12 col-xs-12">
            <h4 class="margin-top-30">
               <b>
                  <spring:theme code="text.cms.accommodationrefinement.filterby" />
               </b>
            </h4>
         </div>
      </div>
   </div>
   <div class="container margin-bottom-20">
      <div class="row">
         <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-20">
            <label class="font-weight-light">
               <spring:theme
                  code="text.deals.listing.label.destination" />
            </label>
            <form action="${requestScope['javax.servlet.forward.request_uri']}">
                <select class="selectpicker form-control valid" name="hotelLocationName" onChange="this.form.submit();">
                   <option value="ALL">
                      <spring:theme
                         code="text.deals.listing.label.destination.option.default" />
                   </option>
                   <c:forEach var="facet" items="${facetSearchPageData.facets}">
                      <c:if test="${facet.code eq 'hotelCityCodes'}">
                         <c:forEach var="hotelLocationName" items="${facet.values}">
                            <option value="${hotelLocationName.code}" ${param.hotelLocationName eq hotelLocationName.code ? 'selected' : '' }>
                                ${fn:escapeXml(hotelLocationName.name)}
                            </option>
                         </c:forEach>
                      </c:if>
                   </c:forEach>
                </select>
                <input type="hidden" name="page" value="0"/>
            </form>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-xs-12">
            <div id="y_packageResults" class="row"
               aria-label="package search results">
               <deallisting:dealListView deals="${facetSearchPageData.results}" />
            </div>
         </div>
      </div>
   </div>
   <pagination:globalpagination/>
</div>
