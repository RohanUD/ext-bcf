<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ taglib prefix="accommodationBooking" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationbooking"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:url value="/option-booking/save" var="optionBookingSaveUrl" />
<c:url value="/option-booking/cancel/${globalReservationDataList.code}" var="optionBookingCancelUrl" />
<c:url value="/option-booking/convert-to-booking/${globalReservationDataList.code}" var="optionBookingConvertToCartUrl" />

<div class="y_alacarteReservationComponent">

<cart:asmSupplierComments />

<div class="row margin-bottom-40">
<div class="pageLabel--package-ferry-passenger-info">
<div class="container">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="total-price-section text-center">
                <div>
                ${globalReservationDataList.totalFare.totalPrice.formattedValue}&nbsp;
                    <span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.total" text="Total" />
                    &nbsp;<strong>${totalFare.totalPrice.formattedValue}</strong></span> +
                    ${totalFare.taxPrice.formattedValue}&nbsp;
                    <spring:theme code="label.reservation.summary.taxesandfees" text="taxes and fees" />

                </div>


                <div>
                    <c:choose>
                        <c:when test="${globalReservationDataList.amountToPay.value >=0 }">
                                   <span class="total-price-text"><spring:theme code="reservation.amount.pay.label" text="Amount to Pay" /></span>
                                                                          :&nbsp;${globalReservationDataList.amountToPay.formattedValue}
                        </c:when>
                        <c:otherwise>
                               <span class="total-price-text"><spring:theme code="reservation.refund.amount.label" text="Refund Amount" /></span>                                                                  :&nbsp;${globalReservationDataList.refundAmount.formattedValue}
                         </c:otherwise>

                    </c:choose>
                </div>

                <span class="vehicle-view-details"><bcfglobalreservation:bcfGlobalReservationModalLink /></span>
            </div>

        <cart:cartTimer />
        </div>
  </div>
 </div>
  <c:if test="${not empty reservationDataList}">
     <div class="container">
        <c:forEach items="${reservationDataList.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
            <transportreservation:transportReservation reservationData="${reservationData}" alacarteBooking="true" />
        </c:forEach>
    </div>
   </c:if>

	<div class="container">

            <div class="row">
                <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20 margin-top-20">
                    <a href="${contextPath}${fn:escapeXml(addAnotherSailing)}" class="btn btn-primary btn-block">
                        <spring:theme code="label.fareselectionreview.add.sailing" text="Add sailing" />
                    </a>
                 </div>
             </div>
	</div>

    <c:if test="${not empty accommodationMap}">
        <div class="container">
            <div class="panel-heading">
                <div class="row">
                    <span class="my-package-heading">
                        <spring:theme code="text.cms.travelfinder.hotel.tab" />
                    </span>
                </div>
            </div>
            <c:forEach var="entry" items="${accommodationMap}">
               <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-4">
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <accommodationdetails:propertyCategoryTypeDetails property="${entry.key}" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <h5 class="mb-1">
                                <strong> &puncsp;${fn:escapeXml(entry.key.accommodationOfferingName)}</strong>
                            </h5>
                        </div>
                    </div>
                </div>
               </div>
                <c:forEach var="accommodationReservationData" items="${entry.value}"  varStatus="idx">

                    <hr class="hr-payment">
                    <div class="row mb-5">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <c:set var="roomStays" value="${accommodationReservationData.roomStays}" />
                            <bcfglobalreservation:alacarteRoomReservation  roomStays="${roomStays}" accommodationOfferingCode="${accommodationReservationData.accommodationReference.accommodationOfferingCode}" />
                        </div>
                    </div>



                </c:forEach>
              </c:forEach>

              <c:if test="${asmLoggedIn}">
                  <li><bookingDetails:asmTravelBookingDetails asmOrderData="${asmOrderData}"/></li>
              </c:if>
        </div>
    </c:if>

    <div class="container">
      <div class="row">
            <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                            <a href="${contextPath}/?hotel=true" class="btn btn-primary btn-block">
                                <spring:theme code="label.accommodation.add.hotel" text="Add Hotel" />
                            </a>
             </div>
       </div>
     </div>
    </div>


    <c:if test="${not empty globalReservationDataList.activityReservations}">
    	<bcfglobalreservation:activityReservations alacarteBooking="true" activityReservationDataList="${globalReservationDataList.activityReservations}" />
    </c:if>
	<c:url value="/vacations/activities" var="activitySearchUrl"/>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                <a href="${activitySearchUrl}" class="btn btn-primary btn-block">
                    <spring:theme code="label.activity.details.add.activity.button" text="Add Activity" />
                </a>
             </div>
         </div>
         <c:if test="${asmLoggedIn}">
             <li><bookingDetails:asmActivityBookingDetails asmOrderData="${asmOrderData}"/></li>
         </c:if>
     </div>
   </div>

    <c:choose>
        <c:when test="${globalReservationDataList.optionBooking}">
           <div class="container">
              <div class="row">
                <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                    <a href="${optionBookingConvertToCartUrl}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.convert.to.booking" /></a>
                </div>

                <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                    <a href="${optionBookingCancelUrl}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.cancel.optionbooking" /></a>
                </div>
            </div>
           </div>
        </c:when>
        <c:otherwise>
            <div class="container">
               <div class="row">
                   <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                       <a href="${optionBookingSaveUrl}" class="btn btn-primary btn-block">
                           <spring:theme code="label.fareselectionreview.button.save.as.optionbooking" />
                       </a>
                    </div>
                </div>
            </div>
        </c:otherwise>
    </c:choose>

 <div class="container">
  <div class="row">

      <c:choose>
        <c:when test="${northernRoute}">
            <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                            <a href="${fn:escapeXml(amenitiesURL)}" class="btn btn-primary btn-block">
                                <spring:theme code="label.northernroute.add.amenities" text="Add amenities" />
                            </a>
            </div>
            <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                        <a href="${fn:escapeXml(nextURL)}" class="btn btn-primary btn-block">
                            <spring:theme code="label.northernroute.skip.amenities" text="Continue without amenities" />
                        </a>
            </div>
        </c:when>
        <c:otherwise>

            <c:if test="${not globalReservationDataList.optionBooking}">
                <div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
                    <a href="${fn:escapeXml(nextURL)}" class="btn btn-primary btn-block">
                        <spring:theme code="label.fareselectionreview.continue" text="Continue" />
                    </a>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>


    </div>
    </div>

    <fareselection:removeferrymodal/>
    <accommodationBooking:removeroommodal/>

</div>
