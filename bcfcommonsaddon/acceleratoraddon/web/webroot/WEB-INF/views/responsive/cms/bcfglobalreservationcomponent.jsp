<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<input type="hidden" value="${component.uid}" id="y_bcfGlobalReservationComponentId" />
<div class="container">
<div class="vacation-booking-details">
    <c:choose>
        <c:when test="${alacarteJourney}">
           <bcfglobalreservation:alacarteReservationSummary />
        </c:when>
        <c:otherwise>
               <bcfglobalreservation:reservationSummary />
        </c:otherwise>
    </c:choose>
</div>
</div>
