<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="guest" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:url var="activityFinderUrl" value="/view/ActivityFinderComponentController/search" />
<form:form commandName="activityFinderForm" action="${fn:escapeXml(activityFinderUrl)}" method="POST" class="fe-validate form-background form-booking-trip" id="y_activityFinderForm">
<div class="container p-0">
             
	<div class="row ">

	<div class="input-required-wrap col-xs-12 col-sm-4">

    		<label for=activityDestination class="home-label-mobile"> Destination
    			<spring:theme var="destinationPlaceholderText" code="text.cms.farefinder.departure.location.placeholder" text="Destination" />
    		</label>
    		<form:input type="text" id="activityDestination" path="" cssErrorClass="fieldError" class="y_activityFinderDestinationLocation input-grid col-xs-12 form-control" placeholder="${fn:escapeXml(departureLocationPlaceholderText)}" autocomplete="off" />
    		<form:hidden path="${fn:escapeXml(formPrefix)}destination" class="y_activityFinderDestinationLocationCode"/>
    		<div id="y_activityFinderDestinationLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
    	</div>
    	
    	<div class="input-required-wrap  col-xs-12 col-sm-4" id="js-return">
		<label for="departingDateTime" class="home-label-mobile">Departure
			<spring:theme var="departingDatePlaceholderText" code="text.cms.dealfinder.departure.date.placeholder" text="Departure Date" />
		</label>
		<form:input type="text" path="${fn:escapeXml(formPrefix)}date" class="col-xs-12 datepicker input-grid form-control y_fareFinderDatePickerDeparting y_transportDepartDate" placeholder="${fn:escapeXml(departingDatePlaceholderText)}" autocomplete="off" />
		 <div class="y_departureDateError col-md-12 departureDate-error"></div>
	</div>
	
	 	
    	<div class="col-xl-4 pt-2">
    	<div class="row input-row less-margin">
   <div  class="input-required-wrap col-sm-12 col-xl-6">
         <label for="y_activityTypeSelect" class="home-label-mobile"> Activity Type
            <spring:theme var="vacationExperiencePlaceholderText" code="text.cms.dealfinder.vacationExperience.placeholder" text="Vacation Experience" />
         </label>
         <form:select class="form-control ${fn:escapeXml(activityType)}" id="y_activityTypeSelect" multiple="false" path="${fn:escapeXml(formPrefix)}activityType" cssErrorClass="fieldError">
         		<option class="ANY" value="ANY" selected="selected" >ANY</option>
         		<c:forEach var="entry" items="${activityTypeData}">
         		<form:option value="${entry.code}">${entry.name}</form:option>
                </c:forEach>
         </form:select>
    </div>

<input type="hidden" id="activityValues" value="${activities}">

   <div  class="input-required-wrap col-sm-12 col-xl-6">
         <label for="y_activitySelect" class="home-label-mobile"> Activities
            <spring:theme var="vacationExperiencePlaceholderText" code="text.cms.dealfinder.vacationExperience.placeholder" text="Vacation Experience" />
         </label>
         <form:select class="form-control ${fn:escapeXml(activity)}" id="y_activitySelect" multiple="false" path="${fn:escapeXml(formPrefix)}activity" cssErrorClass="fieldError">
         		<option class="ANY" value="ANY" selected="selected">ANY</option>
         		<c:forEach var="entry" items="${activities}">
         		<form:option class="${entry.activityType}" value="${entry.code}">${entry.name}</form:option>
                </c:forEach>
         </form:select>
    </div>
</div>
</div>


<guest:participantData formPrefix="${formPrefix}" guestData="${activityFinderForm.guestData}" maxGuestPerPax="${activityFinderForm.maxGuestCountPerPaxType}" />

<div class="col-xl-12 pt-3">
			<button class="confirm-btn confirm-btn--blue" type="submit" id ="dealFinderFindButton" value="Submit">FIND</button>
		</div>
</div></div>
</form:form>
