<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="passengerCount" value="0" />
<c:set var="vehicleCount" value="0" />
<c:set var = "vehicleInfo"/>
<div class="vacation-blue-bg">
	<div class="row mb-3">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center vacation-edit-relative">
			<p class="mb-2">
				<strong>${travelFinderForm.accommodationFinderForm.destinationLocationName}</strong>
			</p>
			<fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkInDateTime}" var="parsedCheckInDateTime" pattern="dd/MM/yyyy" />
			<fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkOutDateTime}" var="parsedCheckOutDateTime" pattern="dd/MM/yyyy" />
			<p class="mb-0">
				<c:set var="numberOfNights">
					<fmt:parseNumber value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}" integerOnly="true" />
				</c:set>
				<c:choose>
					<c:when test="${numberOfNights eq 1}">
						<spring:theme code="text.tripwidget.night" arguments="${numberOfNights}" />
					</c:when>
					<c:otherwise>
                     (
                     <spring:theme code="text.tripwidget.nights" arguments="${numberOfNights}" />
						<spring:theme code="tripwidget.returnsailing" text="return sailing" />
                     )
                  </c:otherwise>
				</c:choose>
			</p>
			<a href="${travelFinderForm.referer}">
			<div class="vacation-edit">
				<i class="bcf bcf-icon-edit mr-2" aria-hidden="true"></i>
				<spring:theme code="text.tripwidget.edit" text="edit" />
			</div>
			</a>
		</div>
	</div>
	<div class="vacation-blue-center-box">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center">
				<fmt:parseDate value="${travelFinderForm.fareFinderForm.departingDateTime}" var="parsedDepartDate" pattern="MMM dd yyyy" />
				<fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />
				<p class="mb-1">${formattedParsedDepartDate}</p>
				<p class="mb-0">
					<strong>${travelFinderForm.fareFinderForm.departureLocationName}</strong>
				</p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
				<span class="package-arrow-icon margin-top-20"></span>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center">
				<fmt:parseDate value="${travelFinderForm.fareFinderForm.returnDateTime}" var="parsedReturnDate" pattern="MMM dd yyyy" />
				<fmt:formatDate value="${parsedReturnDate}" var="formattedParsedReturnDate" pattern="E, MMM dd" />
				<p class="mb-1">${formattedParsedReturnDate}</p>
				<p class="mb-0">
					<strong>${travelFinderForm.fareFinderForm.arrivalLocationName}</strong>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
		<div class="info-box-detail-pass">
			<p class="text-center mb-0">
				<span class="align-middle">
					<c:forEach items="${travelFinderForm.fareFinderForm.passengerTypeQuantityList}" var="passengerTypeQuantity">
						<c:set var="passengerCount" value="${passengerCount + passengerTypeQuantity.quantity}" />
					</c:forEach>
					<span class="bcf bcf-icon-passengers"></span> ${passengerCount}
				</span>
				<span class="align-middle">
					<span class="bcf bcf-icon-nights"></span>
					<fmt:formatNumber value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}" type="number" maxFractionDigits="0" />
				</span>
				<span class="align-middle">
					<span class="bcf bcf-icon-hotel-room"></span> ${travelFinderForm.accommodationFinderForm.numberOfRooms}
				</span>
				<span class="align-middle">
					<c:forEach items="${travelFinderForm.fareFinderForm.vehicleInfo}" var="vehicleInfoQuantity">
						<c:set var="vehicleCount" value="${vehicleCount + vehicleInfoQuantity.qty}" />
						<c:set var="vehicleInfo" value="${vehicleInfoQuantity.vehicleType.code}" />
					</c:forEach>
					<c:if test="${vehicleCount ne 0}">
					<span class="${vehicleInfo}"></span> ${vehicleCount}
					</c:if>
				</span>
			</p>
		</div>
	</div>
</div>
<div class="row margin-top-20">

   <c:choose>
   <c:when test="${availabilityStatus eq 'ON_REQUEST'}">
   <div class="col-xs-6 text-center br-1">
   	<p class="m-0">
   		<b><spring:theme code="text.package.booking.on.request.title" text="Booking is on request"/></b>
   	</p>
   		<p class="m-0 fnt-14">
   	  <spring:theme code="text.package.booking.on.request.ccard.message"/>
   	</p>
   	<input type="hidden" name="availabilityStatus" id="availabilityStatus" value="${availabilityStatus}" />
   	</div>
   	<div class="col-xs-6 ">
            <div class="p-chart text-center">${globalReservationDataList.totalFare.totalPrice.formattedValue}
                <span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.total" text="Total" />
                &nbsp;<strong>${totalFare.totalPrice.formattedValue}</strong></span> +
                ${totalFare.taxPrice.formattedValue}&nbsp;
                <spring:theme code="label.reservation.summary.taxesandfees" text="taxes and fees" />
                <bcfglobalreservation:bcfGlobalReservationModalLink />
            </div>
        </div
</c:when>
<c:otherwise>

    <div class="col-xs-12">
            <div class="text-center">${globalReservationDataList.totalFare.totalPrice.formattedValue}
                <span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.total" text="Total" />
                &nbsp;<strong>${totalFare.totalPrice.formattedValue}</strong></span> +
                ${totalFare.taxPrice.formattedValue}&nbsp;
                <spring:theme code="label.reservation.summary.taxesandfees" text="taxes and fees" />
                <bcfglobalreservation:bcfGlobalReservationModalLink />
            </div>
        </div>
        						</c:otherwise>

        </c:choose>

</div>
<fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkInDateTime}" var="parsedCheckInDateTime" pattern="dd/MM/yyyy" />
<fmt:parseDate value="${travelFinderForm.accommodationFinderForm.checkOutDateTime}" var="parsedCheckOutDateTime" pattern="dd/MM/yyyy" />
