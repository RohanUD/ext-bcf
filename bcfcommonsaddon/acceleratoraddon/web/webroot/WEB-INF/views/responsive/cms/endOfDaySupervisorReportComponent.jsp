<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="container">
    <b><spring:theme code="text.agent.declare.supervisor.data.header" /></b>
    <br />

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.portlocation" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
           <input type="text" value="${agentDeclareForm.portLocation}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.supervisor.userId" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${agentDeclareForm.userId}" readonly="true"/>
        </div>
    </div>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.supervisor.userName" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${agentDeclareForm.userName}" readonly="true"/>
        </div>
    </div>

    <br />
    <br />
    <br />

    <jsp:useBean id="date" class="java.util.Date"/>
    <c:set var="sessionStartDate"><fmt:formatDate value="${agentDeclareForm.sessionStart}" type="date" pattern="dd/MM/yyyy hh:mm"/></c:set>

    <div class="row input-row">
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <b><spring:theme code="text.agent.declare.supervisor.sessionStart" /></b>
        </div>
        <div class="input-required-wrap col-xs-12 col-sm-4">
            <input type="text" value="${sessionStartDate}" readonly="true"/>
        </div>
    </div>

</div>

<br />
<br />

<div class="container">
    <b><spring:theme code="text.agent.declare.supervisor.summary" /></b>
    <br />

    <div class="row input-row">
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.agent.portLocation" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.agent.userId" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.agent.userName" /></b>
        </div>
        <div class="col-sm-2">
            <b><spring:theme code="text.agent.declare.agent.sessionStart" /></b>
        </div>
    </div>

    <br />

    <c:forEach items="${agents}" var="agent">
        <div class="row input-row">
            <div class="col-sm-2">
                <b>TS</b>
            </div>
            <div class="col-sm-2">
                <b>${agent.customerId}</b>
            </div>
            <div class="col-sm-2">
                <b>${agent.name}</b>
            </div>
            <jsp:useBean id="agentStartDate" class="java.util.Date"/>
            <c:set var="agentStartDate"><fmt:formatDate value="${agent.endofdayDeclareTime}" type="date" pattern="dd/MM/yyyy hh:mm"/></c:set>

            <div class="col-sm-2">
                <b>${agentStartDate}</b>
            </div>

            <c:set var="style" value="" />
            <c:set var="disabled" value="" />

            <c:if test="${agent.selected}">
                <c:set var="style" value="style='background:red'" />
                <c:set var="disabled" value="disabled" />
            </c:if>

            <div class="col-sm-2">
                <form action="/endofday-report">
                    <input type="hidden" name="agentToDeclare" value="${agent.customerId}" />
                    <button class="btn btn-primary btn-block" ${style} type="submit" value="Submit" ${disabled}>
                        <spring:theme code="text.agent.declare.submitbutton" text="Declare" />
                    </button>
                </form>
            </div>
        </div>
    </c:forEach>

</div>
