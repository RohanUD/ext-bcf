<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ attribute name="travelFinderForm" required="true" type="com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm"%>
<%@ attribute name="index" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<input type="hidden" id="standardVehicleHeightPortLimit" value="${travelFinderForm.fareFinderForm.standardVehicleHeightPortLimit}">
<input type="hidden" id="standardVehicleLengthPortLimit" value="${travelFinderForm.fareFinderForm.standardVehicleLengthPortLimit}">
<div class="vehicle-content">
	<p class="mb-5 fnt-14">
		<spring:theme code="text.ferry.farefinder.vehicletype.standard.details" text="Standard Vehicle under 5,500 kg including cars, trucks, SUVs, vans, motorhomes and vehicles pulling trailers." />
	</p>
	<div class="row margin-top-40">
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p for="standardVehicleHeight">
                <strong> <spring:theme code="text.ferry.farefinder.vehicletype.height" text="Vehicle Height" />
                </strong>
                <span data-toggle="modal" data-target="#vehicle-description" class="bcf bcf-icon-info-solid icon-blue"></span>
            </p>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="vehicle-radio-box">
				<label for="under7HeightRadbtn" class="custom-radio-input">
					<spring:theme code="label.ferry.farefinder.vehicleInfo.under7" text="Under7ft(2.13m)" />
					<form:radiobutton id="under7HeightPackage_${index}" path="fareFinderForm.vehicleInfo[${index}].vehicleType.code" value="UH_standard" class="y_fareFinderVehicleTypeBtn radioCode item-vehicleHeight" name="radio" />
					<span class="checkmark"></span>
				</label>
				<ul class="list-inline mb-0">
					<li>
						<i class="bcf bcf-icon-std-car bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-truck bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-suv bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-van bcf-2x"></i>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="vehicle-radio-box">
				<label for="over7HeightRadbtn" class="custom-radio-input">
					<spring:theme code="label.ferry.farefinder.vehicleInfo.over7" text="Over 7ft(2.13m)" />
					<form:radiobutton id="over7HeightPackage_${index}" path="fareFinderForm.vehicleInfo[${index}].vehicleType.code" value="OS_standard" name="radio" class="y_fareFinderVehicleTypeBtn radioCode item-vehicleHeight" />
					<span class="checkmark"></span>
					<form:hidden path="fareFinderForm.travellingWithVehicle" value="true" />
				</label>
				<ul class="list-inline mb-0">
					<li>
                        <i class="bcf bcf-icon-van-cargo bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-suv-cargo bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-camper bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-rv bcf-2x"></i>
                    </li>
				</ul>
			</div>
			<div class="row mt-2 pl-5 unit-converter unit-converter--alt d-none" id="Over7heightPackageDiv_${index}">
				<div class="col-md-11 col-md-offset-1" id="vehicle_height_inputs">
				    <p id="heightft_${index}" for="heightInput" class="label" aria-hidden="true">
                        <spring:theme code="label.ferry.farefinder.vehicleInfo.totalHeight" text="Total Height" />
                    </p>
                     <div class="unit-converter__input-container">
					<div class="unit-converter__inputs">
						<input id="Over7heightboxinft_${index}" value="0" class="fareheightInput js-convert-units non-spinner" step="any" type="number" />
						<label id="heightf_${index}" for="heightInput">
							<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /> </span>
						</label>
					</div>
					<div class="unit-converter__inputs">
						<input id="heightinm_${index}" value="0" class="js-convert-units non-spinner fareheightInputInMtrs" step="any" type="number" />
						<label id="heightm_${index}" for="lengthInput">
							<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
						</label>
					</div>
					</div>
					<c:set var="standardHeightError">
						<form:errors path="${fn:escapeXml(formPrefix)}fareFinderForm.vehicleInfo[${fn:escapeXml(index)}].standardVehicleHeightError" />
					</c:set>
					<c:if test="${not empty standardHeightError}">
						<p class="standardError_${fn:escapeXml(index)}">${standardHeightError}</p>
					</c:if>
				</div>
				<div class="col-lg-12 col-xs-12 js-standard-height-port-restriction-warning_${index} hidden">
					<p class="col-sm-12 warning-msg bcf-icon-text" id="limitedSilings">
						<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
						<spring:theme code="label.vacation.travelfinder.farefinder.vehicle.exceeding.port.height.limitation"/>
					</p>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p for="standardVehicleLength">
                <strong><spring:theme code="text.ferry.farefinder.vehicletype.length" text="Vehicle Length" /> </strong>
                <span data-toggle="modal" data-target="#vehicle-description" class="bcf bcf-icon-info-solid icon-blue"></span>
            </p>
        </div>
    </div>
	<div class="row">
		<div class="input-required-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="vehicle-radio-box">
						<label for="under20LengthRadbtn" class="custom-radio-input">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.under20" text="Under 20ft(6.10 m)" />
							<input type="radio" id="under20LengthPackage_${index}" value="UH" name="vehicleLength" class="y_fareFinderVehicleTypeBtn item-vehicleLength" />
							<span class="checkmark"></span>
						</label>
						<ul class="list-inline mb-0">
					<li>
						<i class="bcf bcf-icon-std-car bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-truck bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-suv bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-van bcf-2x"></i>
						</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="vehicle-radio-box">
						<label for="over20LengthRadbtn" class="custom-radio-input">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.over20" text="Over 20 ft(6.10 m)" />
							<input type="radio" id="over20LengthPackage_${index}" value="OS" name="vehicleLength" class="y_fareFinderVehicleTypeBtn item-vehicleLength" />
							<span class="checkmark"></span>
						</label>
						<ul class="list-inline mb-0">
					<li>
                        <i class="bcf bcf-icon-truck-trailer bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-truck-boat bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-large-rv bcf-2x"></i>
                    </li>
						</ul>
					</div>
					<div class="row mt-2 pl-5  unit-converter unit-converter--alt d-none" id="Over20lengthDivPackage_${index}">
						<div class="col-md-11 col-md-offset-1" id="vehicle_length_inputs">
						    <p id="lengthft_${index}" class="label block-text" aria-hidden="true">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.totalLength" text="Total Length" />
                            </p>
                            <div class="unit-converter__input-container">
							<div class="unit-converter__inputs">
								<input id="Over20lengthboxinft_${index}" value="0" class="js-convert-units non-spinner farelengthInput" step="any" type="number" />
								<label id="lengthf_${index}" for="lengthInput">
									<span class="sr-only">feet</span><span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /> </span>
								</label>
							</div>
							<div class="unit-converter__inputs mt-2">
								<input id="lengthboxinm_${index}" value="0" class="js-convert-units non-spinner farelengthInputInMtrs" step="any" type="number" />
								<label id="lengthm_${index}" for="lengthInput">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
								</label>
							</div>
							</div>
							<c:set var="standardLengthError">
								<form:errors path="${fn:escapeXml(formPrefix)}fareFinderForm.vehicleInfo[${fn:escapeXml(index)}].standardVehicleLengthError" />
							</c:set>
							<c:if test="${not empty standardLengthError}">
								<p class="standardError_${fn:escapeXml(index)}">${standardLengthError}</p>
							</c:if>
						</div>
						<div class="col-lg-12 col-xs-12 js-standard-length-port-restriction-warning_${index} hidden">
							<p class="col-sm-12 warning-msg mt-0 bcf-icon-text" id="limitedSilings">
								<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
								<spring:theme code="label.vacation.travelfinder.farefinder.vehicle.exceeding.port.length.limitation"/>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
