<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>
<%@ attribute name="travelFinderForm" required="true" type="com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm"%>
<%@ taglib prefix="vehicleinfo" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryInfo"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<input type="hidden" id="skipVehicle" value="${vehicleToSkip}">
<c:forEach var="entry" items="${travelFinderForm.fareFinderForm.vehicleInfo}" varStatus="i">
	<div class="farevehicle_${i.index} ${i.index==1 && travelFinderForm.fareFinderForm.returnWithDifferentVehicle==false?' hidden':''} margin-top-40">
		<form:errors path="fareFinderForm.vehicleInfo[${i.index}].vehicleType.code" />
		<form:errors path="fareFinderForm.travellingWithVehicle" />
		<div class="row">
			<div class="col-md-12">
				<h2 class="vacation-h2">
					<spring:theme code="label.vacation.travelfinder.farefinder.vehicle.heading" text="vehicle" />
					<span class="bcf bcf-icon-info-solid icon-blue"></span>
				</h2>
				<p class="fnt-14">
					<spring:theme code="label.ferry.farefinder.vehicle.details" text="Specific measurements based on your car type are required to provide accurate deck space availability." />
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<vehicleinfo:packagestandardvehicleinfo travelFinderForm="${travelFinderForm}" index="${i.index}" />		
			</div>
		</div>			
	</div>
	<form:input class="form-control" path="fareFinderForm.vehicleInfo[${i.index}].qty" type="hidden" value="1" autocomplete="off" />
	<form:input id="height_${i.index}" class=" fareheightInput js-convert-units non-spinner" step="any" type="hidden" path="${fn:escapeXml(formPrefix)}fareFinderForm.vehicleInfo[${fn:escapeXml(i.index)}].height" />
	<form:input id="length_${i.index}" class="js-convert-units non-spinner farelengthInput" step="any" type="hidden" path="${fn:escapeXml(formPrefix)}fareFinderForm.vehicleInfo[${fn:escapeXml(i.index)}].length" />
</c:forEach>
<div class="modal fade" id="y_showLimitedSailingsModal" tabindex="-1" role="dialog" aria-labelledby="y_showLimitedSailingsModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="limitedSailingsLabel">
					<spring:theme code="text.ferry.limited.sailings.modal.title" text="Limited Sailings" />
				</h3>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger" id="limitedSilings">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                  	<spring:theme code="text.ferry.limited.sailings.message" text="Your sailings may be limited based on your input criteria." />
                </div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_freshBooking" class="btn btn-primary btn-block">
							<spring:theme code="text.page.managemybooking.fresh.booking.confirmation" text="Confirm" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
