<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty globalReservationDataList.activityReservations}">
    <div class="row margin-bottom-20 margin-top-40 text-center">
        <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
            <c:url value="/vacations/activities" var="activitySearchUrl"/>
            <a href="${fn:escapeXml(activitySearchUrl)}" class="btn btn-primary btn-block">
                <spring:theme code="text.activitiesListing.page.addAnotherActivityButton" text="Add Another Activity" />
            </a>
        </div>
    </div>
</c:if>
<c:choose>
    <c:when test="${isASMSession}">

        <c:if test="${not globalReservationDataList.optionBooking}">
            <div class="row margin-bottom-20 margin-top-40 text-center">
                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="${fn:escapeXml(nextUrl)}" class="btn btn-primary btn-block"> <spring:theme code="text.fareselection.button.continue" text="Continue" /></a>
                </div>
            </div>
            <div class="row margin-bottom-20 margin-top-40 text-center">
                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="/option-booking/save" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.save.as.optionbooking" /></a>
                </div>
            </div>
        </c:if>

         <c:if test="${globalReservationDataList.optionBooking}">
            <div class="row margin-bottom-20 margin-top-40 text-center">
                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="/option-booking/convert-to-booking/${globalReservationDataList.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.convert.to.booking" /></a>
                </div>

                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="/option-booking/cancel/${globalReservationDataList.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.cancel.optionbooking" /></a>
                </div>
            </div>
         </c:if>
    </c:when>
    <c:otherwise>
        <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
            <div class="row margin-bottom-20 margin-top-40 text-center">
                  <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                      <a href="${fn:escapeXml(loginToCheckoutUrl)}" class="btn btn-primary btn-block"> <spring:theme code="label.fareselectionreview.log.in" text="Login to checkout" /></a>
                  </div>
            </div>
        </sec:authorize>

        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <div class="row margin-bottom-20 margin-top-40 text-center">
                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="${fn:escapeXml(nextUrl)}" class="btn btn-primary btn-block"> <spring:theme code="text.fareselection.button.continue" text="Continue" /></a>
                </div>
            </div>
        </sec:authorize>

        <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
            <div class="row margin-bottom-20 margin-top-40 text-center">
                <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <a href="${fn:escapeXml(nextUrl)}" class="btn btn-primary btn-block"> <spring:theme code="label.fareselectionreview.continue.as.guest" text="Continue as Guest" /></a>
                </div>
            </div>
        </sec:authorize>
    </c:otherwise>
</c:choose>

