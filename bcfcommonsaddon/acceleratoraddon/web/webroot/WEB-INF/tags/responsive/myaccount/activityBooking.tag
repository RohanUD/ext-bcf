<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="activityReservationData" required="true" type="com.bcf.facades.reservation.data.ActivityReservationData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty activityReservationData}">
    <spring:theme code="text.page.mybooking.activity.name" />${activityReservationData.activityDetails.name}<br/>
    <spring:theme code="text.page.mybooking.activity.date" />${activityReservationData.activityDate}
</c:if>
