<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="accommodationReservationData" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationReservationData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty accommodationReservationData.roomStays}">
	<c:set var="roomStay" value="${accommodationReservationData.roomStays[0]}" />
	<fmt:formatDate value="${roomStay.checkInDate}" var="checkInDate" />
	<fmt:formatDate value="${roomStay.checkOutDate}" var="checkOutDate" />
</c:if>
<dl class="mb-0">
     <dd>
         <b><spring:theme code="text.page.mybooking.hotel" /></b> ${fn:escapeXml(accommodationReservationData.accommodationReference.accommodationOfferingName)}
     </dd><br/>
   <dd>
       <b><spring:theme code="text.page.mybooking.checkInDate" /></b> ${checkInDate}
   </dd><br/> 
   <dd>
       <b><spring:theme code="text.page.mybooking.checkOutDate"/></b> ${checkOutDate}
   </dd>
</dl>
