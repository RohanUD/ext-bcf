<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="reservationData" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="lastReservationItemIdx" value="${fn:length(reservationData.reservationItems)-1}" />
<c:set var="lastTransportOfferingIdx" value="${fn:length(reservationData.reservationItems[lastReservationItemIdx].reservationItinerary.originDestinationOptions[0].transportOfferings)-1}" />
<dl>
    <c:set var="departureVehicle" value="${fn:escapeXml(reservationData.reservationItems[0].reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.name)}" />
    <c:set var="returnVehicle" value="${fn:escapeXml(reservationData.reservationItems[1].reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.name)}" />
	<fmt:formatDate value="${reservationData.reservationItems[0].reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" pattern="${datePattern}" var="departureDate" />
	<fmt:formatDate value="${reservationData.reservationItems[lastReservationItemIdx].reservationItinerary.originDestinationOptions[0].transportOfferings[lastTransportOfferingIdx].arrivalTime}" pattern="${datePattern}" var="arrivalDate" />
	<dd>
		<div class="margin-bottom-30">
		   <b><spring:theme code="text.page.mybooking.route" /></b> ${fn:escapeXml(reservationData.reservationItems[0].reservationItinerary.route.destination.name)}<spring:theme code="text.page.mybooking.route.to" />${fn:escapeXml(reservationData.reservationItems[1].reservationItinerary.route.destination.name)}</div>
		<div class="margin-bottom-30">
		   <b><spring:theme code="text.page.mybooking.departure.sailing" /></b> ${departureDate},${departureVehicle} ${reservationData.reservationItems[0].bcfBookingReference}
		</div>
		<div class="margin-bottom-30">
		  <b><spring:theme code="text.page.mybooking.return.sailing" /></b> ${arrivalDate},${returnVehicle} ${reservationData.reservationItems[lastReservationItemIdx].bcfBookingReference}
		</div>

	</dd>
</dl>
