<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/myaccount"%>
<%@ attribute name="myBookings" required="true" type="java.util.List"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl"/>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="panel-body collapse in" id="my-bookings">
    <c:choose>
       <c:when test="${not empty myBookings}">
          <div class="row">
             <div class="col-xs-12 accommodation-items y_myBookings my-bookings">
                <c:forEach items="${myBookings}" var="myBooking" varStatus="bIdx">
                   <div class="booking-list">
                      <div class="accommodation-details ">
                         <div class="row">
                            <div class="col-xs-12 col-sm-12 vacation-booking-data-text">
                               <div class="clearfix">
                                     
                                        <c:set var="isDisplayBookingReferenceCode" value="true" />
                                        <c:set var="packageReservations" value="${myBooking.packageReservations}" />
                                        <c:forEach items="${packageReservations.packageReservationDatas}" var="packageReservationData" varStatus="cIdx">
                                           <c:if test="${not empty isDisplayBookingReferenceCode && isDisplayBookingReferenceCode}">
                                           
                                           <div class="row">
                                       <div class="col-md-6 col-md-offset-3 col-xs-12 fnt-14">
                                         <dd class="fnt-18">
                                                 <b><spring:theme code="text.page.mybooking.packageId" /></b> ${myBooking.code}
                                                 <br/>
                                              </dd>
                                           </c:if>
                                            <c:if test="${not empty myBooking.depositPayDate}">
                                                <dd class="margin-bottom-20">
                                                    <b><spring:theme code="text.page.mybooking.depositPayDate" text="Balance Due Date" />:</b>${myBooking.depositPayDate}
                                                </dd>
                                            </c:if>
                                           <dd class="margin-bottom-40">
                                              <b><spring:theme code="text.page.mybooking.bookingStatus" /></b> ${packageReservationData.reservationData.bookingStatusName}
                                           </dd>
                                          
                                           <c:if test="${not empty packageReservationData.accommodationReservationData && !(packageReservationData.accommodationReservationData.bookingStatusCode == 'CANCELLED' && not empty packageReservationData.reservationData && packageReservationData.reservationData.bookingStatusCode != 'CANCELLED')}">
                                              <myaccount:accommodationBooking accommodationReservationData="${packageReservationData.accommodationReservationData}" />
                                           </c:if>
                                       </div>
                                     <div class="col-xs-12"><hr></div>
                                       <div class="col-md-6 col-md-offset-3 col-xs-12 fnt-14">
                                          <c:if test="${not empty packageReservationData.activityReservations	&& not empty packageReservationData.reservationData && packageReservationData.reservationData.bookingStatusCode != 'CANCELLED'}">
                                              <c:set var="activityReservations" value="${packageReservationData.activityReservations}" />
                                              <c:forEach items="${activityReservations.activityReservationDatas}" var="activityReservationData" varStatus="dIdx">
                                                 <myaccount:activityBooking activityReservationData="${activityReservationData}" />
                                              </c:forEach>
                                           </c:if>
                                           <c:if test="${not empty packageReservationData.reservationData && !(packageReservationData.reservationData.bookingStatusCode == 'CANCELLED'
                                              && not empty packageReservationData.accommodationReservationData && packageReservationData.accommodationReservationData.bookingStatusCode != 'CANCELLED')}">
                                              <myaccount:travelBooking reservationData="${packageReservationData.reservationData}" />
                                              <c:set var="isDisplayBookingReferenceCode" value="false" />
                                           </c:if>
                                       </div>
                                     </div>

                                     <div class="row">
                                     <div class="col-md-4 col-md-offset-4 col-xs-12">
                                     <a href="${viewBookingUrl}/${myBooking.code}" class="btn btn-primary btn-block">
                                     <spring:theme code="button.page.mybookings.viewBookingDetails" />
                                     </a>
                                     </div>
                                     </div>
                                 
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   </c:forEach>
                   <c:if test="${not empty myBooking.activityReservations && empty myBooking.accommodationReservations}">
                      <c:set var="activityReservations" value="${myBooking.activityReservations}" />
                      <c:if test="${not empty isDisplayBookingReferenceCode && isDisplayBookingReferenceCode}">
                         <dd>
                            <spring:theme code="text.page.mybooking.packageId" />${myBooking.code}
                            <br/>
                         </dd>
                      </c:if>
                      <dd>
                         <spring:theme code="text.page.mybooking.bookingStatus" />${myBooking.bookingStatusName}
                      </dd>
                      <c:forEach items="${activityReservations.activityReservationDatas}" var="activityReservationData" varStatus="eIdx">
                         <myaccount:activityBooking activityReservationData="${activityReservationData}" />
                      </c:forEach>
                      <c:if test="${not empty myBooking.transportReservations && not empty myBooking.transportReservations.reservationDatas}">
                         <c:forEach items="${myBooking.transportReservations.reservationDatas}" var="reservationData" varStatus="fIdx">
                            <c:if test="${!(reservationData.bookingStatusCode == 'CANCELLED')}">
                               <myaccount:travelBooking reservationData="${reservationData}" />
                            </c:if>
                         </c:forEach>
                      </c:if>
                      <div class="row">
                         <div class="col-xs-12 col-sm-6 pull-right">
                            <a href="${viewBookingUrl}/${myBooking.code}" class="btn btn-primary btn-block">
                               <spring:theme code="button.page.mybookings.viewBookingDetails" />
                            </a>
                         </div>
                      </div>
                   </c:if>
                   <c:if test="${not empty myBooking.accommodationReservations && not empty myBooking.accommodationReservations.accommodationReservations}">
                      <c:set var="isDisplayBookingReferenceCode" value="true" />
                      <c:set var="accommodationReservations" value="${myBooking.accommodationReservations}" />
                      <c:set var="transportReservations" value="${myBooking.transportReservations}" />
                      <c:if test="${not empty isDisplayBookingReferenceCode && isDisplayBookingReferenceCode}">
                         <dd>
                            <spring:theme code="text.page.mybooking.packageId" />${myBooking.code}
                            <br/>
                         </dd>
                      </c:if>
                   
                      <dd>
                         <spring:theme code="text.page.mybooking.bookingStatus" />${transportReservations.reservationDatas[0].bookingStatusName}
                      </dd>
                     
                      <c:forEach items="${accommodationReservations.accommodationReservations}" var="accommodationReservationData" varStatus="gIdx">
                         <c:if test="${not empty accommodationReservationData && !(accommodationReservationData.bookingStatusCode == 'CANCELLED')}">

                               <myaccount:accommodationBooking accommodationReservationData="${accommodationReservationData}" />
                        
                         </c:if>
                      </c:forEach>
                      <c:if test="${not empty myBooking.activityReservations}">
                         <c:set var="activityReservations" value="${myBooking.activityReservations}" />
                         <c:forEach items="${activityReservations.activityReservationDatas}" var="activityReservationData" varStatus="hIdx">
                            <myaccount:activityBooking activityReservationData="${activityReservationData}" />
                         </c:forEach>
                      </c:if>
                      <c:if test="${not empty myBooking.transportReservations && not empty myBooking.transportReservations.reservationDatas}">
                         <c:forEach items="${myBooking.transportReservations.reservationDatas}" var="reservationData" varStatus="iIdx">
                            <c:if test="${!(reservationData.bookingStatusCode == 'CANCELLED')}">
                               <myaccount:travelBooking reservationData="${reservationData}" />
                            </c:if>
                         </c:forEach>
                      </c:if>
                      <div class="row">
                         <div class="col-xs-12 col-sm-6 pull-right">
                            <a href="${viewBookingUrl}/${myBooking.code}" class="btn btn-primary btn-block">
                               <spring:theme code="button.page.mybookings.viewBookingDetails" />
                            </a>
                         </div>
                      </div>
                   </c:if>
                </c:forEach>
             </div>
          </div>
       </c:when>
       <c:otherwise>
          <spring:theme code="text.page.mybookings.not.found" />
       </c:otherwise>
    </c:choose>
 </div>
