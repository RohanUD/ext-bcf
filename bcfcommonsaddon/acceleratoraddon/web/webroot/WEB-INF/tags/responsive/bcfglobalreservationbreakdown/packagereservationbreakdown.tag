<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="packageDataList" required="true" type="com.bcf.facades.reservation.data.PackageReservationDataList"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/transportreservation"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="y_packageReservationComponent">
	<c:if test="${not empty packageDataList}">
		<c:forEach items="${packageDataList.packageReservationData}" var="packageReservation" varStatus="packageReservationItemIdx">
			<b>Package with </b>
			<spring:theme code="text.cms.accommodationbreakdown.stayat" text="Stay at" />&puncsp;${fn:escapeXml(packageReservation.accommodationReservationData.accommodationReference.accommodationOfferingName)}
			<c:set var="bookingStatus">
				<spring:theme code="booking.status.${packageDataList.bookingStatusCode}" text="" />
			</c:set>
			<c:choose>
				<c:when test="${not empty bookingStatus}">
			        ${fn:escapeXml(bookingStatus)}
			    </c:when>
				<c:otherwise>
			        ${fn:escapeXml(packageDataList.bookingStatusName)}
			    </c:otherwise>
			</c:choose>
			<c:if test="${not empty packageReservation.reservationData}">
				<bcfglobalreservation:transportReservation reservationDataList="${packageReservation.reservationData}" />
			</c:if>
			<c:if test="${not empty packageReservation.accommodationReservationData}">
				<bcfglobalreservation:accommodationReservation accommodationReservationDataList="${packageReservation.accommodationReservationData}" />
			</c:if>
		</c:forEach>
	</c:if>
</div>
