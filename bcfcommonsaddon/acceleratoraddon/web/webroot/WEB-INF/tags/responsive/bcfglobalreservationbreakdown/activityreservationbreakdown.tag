<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="activityDataList" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/transportreservation"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/util"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="y_activityReservationComponent">
<c:if test="${not empty activityDataList}">
<p>Activities Selected : </p>
<c:forEach items="${activityDataList}" var="activityReservation" varStatus="activityItemIdx">
<p>Activity Name :<span>${activityReservation.activityDetails.name}</span></p>
For<div class="col-sm-6">${fn:escapeXml(activityReservation.quantity)}&nbsp;${fn:escapeXml(activityReservation.guestType)}</div><span>ON</span>
<fmt:formatDate pattern="${dateFormat}" value="${activityReservation.activityDate}" /><span>AT</span> <util:TwelveHourFormattedTime time="${activityReservation.activityTime}" />
<div class="col-sm-6">${fn:escapeXml(activityReservation.destination)}&nbsp;</div>
<div class="col-sm-2">
								<c:if test="${not empty activityReservation.totalFare.totalBaseExtrasPrice}">
									<p class="blue-color mt-2">
										<strong><u>${currencyFareLabel }</u></strong>
									</p>
								</c:if>
							</div>
							<div class="col-sm-4">
								<h4>
									<strong><format:price priceData="${activityReservation.totalFare.totalBaseExtrasPrice}" /></strong>
								</h4>
							</div>
<div class="row mt-4">
						<div>
							<c:if test="${not empty reservation.totalFare}">
								<p>
									<spring:theme code="reservation.total.pay.label" text="Total" />
								</p>
								<Strong>${reservation.totalFare.totalPrice.formattedValue}</Strong>
							</c:if>
						</div>
					</div>
</c:forEach>
</c:if>
</div>
