<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="reservationDataList" required="true" type="com.bcf.facades.reservation.data.ReservationDataList"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/transportreservation"%>
<c:if test="${reservationDataList != null}">
	<div class="panel panel-default my-account-secondary-panel"></div>
	<c:forEach var="reservation" items="${reservationDataList.reservationDatas}">
		<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && reservation != null}">
			<c:forEach var="reservationItem" items="${reservation.reservationItems}">
				<div class="journey-wrapper add-on-box y_cartReservationComponent">
					<bookingDetails:bookingitem reservationData="${reservation}" reservationItem="${reservationItem}" cssClass="panel panel-default my-account-secondary-panel" />
				</div>
				<div>
					<reservation:reservationModifications cartModifiedEntries="${reservationDataList.changedReservationDatas}" journeyReferenceNumber="${reservation.journeyReferenceNumber}" originDestinationRefNumber="${reservationItem.originDestinationRefNumber}" />
				</div>
			</c:forEach>
		</c:if>
	</c:forEach>
	<c:if test="${not empty reservationDataList.totalFare.fees}">
		<p>${feesLabel}</p>
		<c:forEach items="${reservationDataList.totalFare.fees}" var="fee" varStatus="feeIdx">
			<Strong>${fee.name}</Strong>&nbsp:&nbsp
    						<Strong>${fee.price.formattedValue}</Strong>
			<br>
		</c:forEach>
	</c:if>
</c:if>
