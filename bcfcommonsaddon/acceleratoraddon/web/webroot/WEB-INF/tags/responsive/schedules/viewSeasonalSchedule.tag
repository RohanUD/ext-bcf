<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="viewPage" required="true" type="java.lang.String" %>
<%@ attribute name="viewPageUrl" required="true" type="java.lang.String" %>

<div class="col-md-offset-4 col-sm-offset-3 col-lg-offset-4 col-lg-4 col-sm-6 col-md-4 col-xs-12">
    <a href="${viewPageUrl}" class="btn btn-outline-primary  btn-block margin-bottom-20"><spring:theme code="view.${viewPage}.label"/></a>
   
</div>
