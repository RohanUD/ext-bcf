<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row-fluid margin-top-17">
	<div class="bc-accordion ferry-drop">
		<c:choose>
			<c:when test="${empty destinationCode}">
				<label>
					<spring:theme code="label.farefinder.vacation.description" text="Destination" />
				</label>
				<div id="accommodation-accordion1" class="accordion-34 accordion-bg-white component-to-top">
					<h3>
						<span class="dropdown-text">
							<c:choose>
						        <c:when test="${not empty accommodationFinderForm.destinationLocationName}">
                                    <strong class="drop-text destination-text"> ${accommodationFinderForm.destinationLocationName}</strong>
                                </c:when>
                                <c:otherwise>
                                    <strong class="drop-text destination-text"><spring:theme code="label.farefinder.vacation.destination.dropdown.placeholder" text="Select a destination (city)" /></strong>
                                </c:otherwise>
                            </c:choose>
							<span class="custom-arrow"></span>
						</span>
					</h3>
					<div>
						<div class="vacation-range-box">
							<div class="form-height-fix style-3">
								<c:forEach var="bcfVacationLocationEntry" items="${bcfVacationLocations}">
									<ul>
										<c:set var="geoAreaVacationLocation" value="${bcfVacationLocationEntry.key}" />
										<c:set var="cityVacationLocations" value="${bcfVacationLocationEntry.value}" />
										<span class="second-span pl-5 pb-3">${geoAreaVacationLocation.name}</span>
										<c:forEach var="cityVacationLocation" items="${cityVacationLocations}">
											<li>
												<a class="sub-link" data-code="${cityVacationLocation.code}" data-city="" data-terminal="">${cityVacationLocation.name}</a>
											</li>
										</c:forEach>
									</ul>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>
				<form:hidden path="${fn:escapeXml(formPrefix)}destinationLocation" class="y_accommodationDestinationLocation" />
			</c:when>
			<c:otherwise>
				<label>
					<spring:theme code="label.farefinder.vacation.description" text="Destination" />
				</label><br>
				${destinationName}
				<input type="hidden" id="y_destinationCode" value="${destinationCode}" />
				<form:hidden path="${fn:escapeXml(formPrefix)}destinationLocation" class="y_accommodationDestinationLocation" value="${destinationCode}" />
			</c:otherwise>
		</c:choose>
	</div>

	<div class="bc-accordion ferry-drop vacation-calender-section">
		<ul class="nav nav-tabs vacation-calender package-hotel-details-ul">
			<li class="tab-links tab-depart">
				<label>
					<spring:theme code="label.farefinder.vacation.date.checkin" text="Check-in-date" />
				</label>
				<a data-toggle="tab" href="#accommodation-check-in" class="nav-link component-to-top">
					<div class="vacation-calen-box accommodation-ui-depart">
						<span class="vacation-calen-date-txt">
							<spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" />
						</span>
						<span class="vacation-calen-year-txt accommodation-current-year-custom">${accommodationFinderForm.checkInDateTime}</span>
						<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big" aria-hidden="true"></i>
					</div>
				</a>
			</li>
			<li class="tab-links tab-return">
				<label>
					<spring:theme code="label.farefinder.vacation.date.checkout" text="Check-out-date" />
				</label>
				<a data-toggle="tab" href="#accommodation-check-out" class="nav-link component-to-top">
					<div class="vacation-calen-box accommodation-ui-return">
						<span class="vacation-calen-date-txt">
							<spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" />
						</span>
						<span class="vacation-calen-year-txt accommodation-current-year-custom">${accommodationFinderForm.checkOutDateTime}</span>
						<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big" aria-hidden="true"></i>
					</div>
				</a>
			</li>
		</ul>
		<div class="tab-content package-hotel-details-content">
			<div id="accommodation-check-in" class="tab-pane input-required-wrap  depart-calendar">
				<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
					<label>
						<spring:theme code="label.farefinder.vacation.date.dateformat" text="Search date:mm/dd/yyyy" />
					</label>
					<form:input type="text" path="${fn:escapeXml(formPrefix)}checkInDateTime" class="form-control datePickerDeparting  datepicker bc-dropdown depart datepicker-input  y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckIn" placeholder="Check In" autocomplete="off" />
					<div id="accommodationdatepicker" class="bc-dropdown--big"></div>
				</div>
			</div>
			<div id="accommodation-check-out" class="tab-pane input-required-wrap return-calendar">
				<div class="bc-dropdown calender-box tab-content-return" id="js-return">
					<label>
						<spring:theme code="label.farefinder.vacation.date.dateformat" text="Search date:mm/dd/yyyy" />
					</label>
					<form:input type="text" path="${fn:escapeXml(formPrefix)}checkOutDateTime" class="form-control datePickerReturning  datepicker datepicker-input bc-dropdown arrive y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckOut" placeholder="Check Out" autocomplete="off" />
					<div id="accommodationdatepicker1" class="bc-dropdown--big"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="row">
  <c:set var="noOfRoomsMaxValue" value="${jalosession.tenant.config.getParameter('alacarte.passenger.type.number.rooms.max.count')}"/>
  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 accommodation-rooms-wrapper">
      <label><spring:theme code="label.farefinder.vacation.rooms.no" text="Number of rooms" /></label>
      <div class="input-group input-btn-mp">
          <span class="fa bcf bcf-icon-remove btn-number" disabled="disabled" data-type="minus" data-field="numberOfRooms"></span>
          <input type="text" name="numberOfRooms" class="input-number min-input2" value="1" min="1" max="${noOfRoomsMaxValue}" readonly>
          <span class="fa bcf bcf-icon-add btn-number" data-type="plus" data-field="numberOfRooms"></span>
      </div>
  </div>
  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 accommodation-room-widget-wrapper">
  </div>
  <div class="snippets hidden">
    <div class="row accommodation-room-widget hidden">
    <input type="hidden" name="" class="room-stay-ref-number" value="" readonly>
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 bc-accordion select-guest-custom">
      <label class="room-title"><spring:theme code="label.farefinder.vacation.rooms.title" text="Room" /> <span></span></label>
      <div class="accordion-bg-white component-to-top accommodation-select-guest-accordion js-accordion js-accordion-default">
          <h3><span><spring:theme code="label.farefinder.vacation.rooms.guest" text="Select guest" /></span><span class="custom-arrow"></span></h3>
          <div class="row">
				  <c:set var="adultMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.adult.max.count')}"/>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 margin-top-10 nopadding">
                      <label><spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 19 yrs+" /></label>
                         <div class="input-group input-btn-mp">
                          <span class="fa bcf bcf-icon-remove btn-number" disabled="disabled" data-type="minus" data-field=""></span>
                          <input type="text" name="" class="form-control input-number accommodation-adult-qty" value="1" min="1" max="${adultMaxValue}" readonly>
                          <input type="hidden" name="" class="form-control accommodation-adult-passenger" value="adult" readonly>
                          <span class="fa bcf bcf-icon-add btn-number" data-type="plus" data-field=""></span>
                      </div>
                  </div>
			          <c:set var="childMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.child.max.count')}"/>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 margin-top-10 nopadding">
                      <label><spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-18 yrs" /></label>
                      <div class="input-group input-btn-mp">
                          <span class="fa bcf bcf-icon-remove btn-number" disabled="disabled" data-type="minus" data-field=""></span>
                          <input type="text" name="" class="form-control input-number accommodation-child-qty" value="0" min="0" max="${childMaxValue}" readonly>
                          <input type="hidden" name="" class="form-control accommodation-child-passenger" value="child" readonly>
                          <span class="fa bcf bcf-icon-add btn-number" data-type="plus" data-field=""></span>
                          <div class="room-number hidden"></div>
                      </div>
                  </div>
              <div class="mb-4 accommodation-child-box-wrapper clearfix">
              </div>
          </div>
      </div>

      <div class="accommodation-guest-gray vacation-guest-gray hidden">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <p><span class="accommodation-adult-count">1</span> x <spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 19 yrs+" /></p>
            <p><span class="accommodation-child-count">0</span> x <spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-18 yrs" /></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 accommodation-child-box">
      <label class="accommodation-child-label-name"><spring:theme code="label.farefinder.vacation.rooms.child.title" text="Child" /> <span class="child-label-name-span">1</span> <spring:theme code="label.farefinder.vacation.rooms.age" text="age" /></label>
      <select class="form-control custom-select">

      </select>
  </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right margin-top-15-mobile">
    <label class="hidden-xs">&nbsp;</label>

            <form:button class="btn btn-primary btn-block" value="Submit" id="bookAnAccommodation">
                <spring:theme code="label.accommodation.rooms.submit" text="Book Accommodation" />
            </form:button>

  </div>
  </div>

