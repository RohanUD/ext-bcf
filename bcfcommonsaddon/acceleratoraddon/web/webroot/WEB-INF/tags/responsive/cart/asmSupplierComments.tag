<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<input type="hidden" name="hasSupplierComments" id="hasSupplierComments" value="${hasSupplierComments}">
<div class="modal fade" id="y_asmSupplierCommentsModal" tabindex="-1"
    role="dialog" aria-labelledby="asmSupplierCommentsLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                </button>
                <h3 class="modal-title" id="replanJourneyLabel">
                    <spring:theme
                        code="label.asm.supplier.comments"
                        text="Supplier Comments" />
                </h3>
            </div>

            <div class="modal-body">
                <c:url var="addSupplierCommentsUrl" value="${addSupplierCommentsUrl}" />
                <div class="y_asmCommentContent">
                    <c:if test="${not empty comments}">
                        <table border="1" cellspacing="5">
                             <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Agent</th>
                                    <th>Comment</th>
                                </tr>
                              </thead>
                               <c:forEach items="${comments}" var="comment" varStatus="j">
                                    <tr border="1">
                                      <td border="1">${comment.date}</td>
                                       <td border="1">${comment.agent}</td>
                                        <td border="1">${comment.comment}</td>
                                    </tr>
                               </c:forEach>
                        </table>
                    </c:if>
                </div>
                <form:form action="${fn:escapeXml(addSupplierCommentsUrl)}" method="POST" id="y_AddSupplierComments">
                    <div style="margin-top:20px" class="tab-content">
                        <spring:theme code="label.asm.add.supplier.comments" text="Add Supplier Comments" />

                        <div class="input-required-wrap">
                             <input type="hidden" name="code" id="code" value="${fn:escapeXml(code)}">
                              <input type="hidden" name="ref" id="ref" value="${fn:escapeXml(ref)}">
                               <input type="hidden" name="orderCode" id="orderCode" value="${fn:escapeXml(orderCode)}">
                             <input type="text" name="comment" id="comment">
                         </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-xs-offset-0 col-sm-offset-6">
                        <a class="btn btn-primary btn-block y_addSupplierComments">
                            <spring:theme code="label.asm.add.supplier.comments" />
                        </a>
                    </div>
                </form:form>
            </div>

        </div>
    </div>
</div>


