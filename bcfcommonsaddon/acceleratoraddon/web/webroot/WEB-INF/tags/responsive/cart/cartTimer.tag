<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

    <c:if test="${not empty showTimerStart && showTimerStart eq true}">
        <div class="cart-bar-msg">
                <p>${holdTimeMessage}</p>

        </div>
    </c:if>
