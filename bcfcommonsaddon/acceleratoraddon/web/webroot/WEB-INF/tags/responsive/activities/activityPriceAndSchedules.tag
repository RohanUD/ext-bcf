<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>
 <%@ attribute name="activityPricePerPassengerWithSchedule" required="false" type="com.bcf.facades.activity.data.ActivityPricePerPassengerWithSchedule"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="action" value="/cart/addDealActivity" />
<c:if test="${not empty activityPricePerPassengerWithSchedule}">
        <json:object escapeXml="false">
            <json:property name="pricePerPassengerAndSchedules">
                <form:form method="post" commandName="addActivityToDealCartForm" class="add-activity-form" action="${action}">
                <form:input type="hidden" path="productCode" value="${activityPricePerPassengerWithSchedule.productCode}" />
                <form:input type="hidden" path="journeyRefNumber" value="${activityPricePerPassengerWithSchedule.journeyRefNumber}" />
                <form:input type="hidden" path="changeActivity" value="${changeActivity}" />
                <form:input type="hidden" path="changeActivityCode" value="${changeActivityCode}" />

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 main-div">
                    <div class="custom-accordion">
                        <p><strong><spring:theme code="text.vacation.ticket.label" text="Tickets" /></strong></p>
                        <div class="custom-accordion-header manage-custom-accordion-header">Select your ticket <span class="custom-arrow"></span></div>
                        <div class="custom-accordion-content  hidden-obj" style="display: none;">
                            <div class="row mb-4">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 y_passengerWrapper">
                                    <c:forEach items="${activityPricePerPassengerWithSchedule.activityPricePerPassenger}" var="activityPricePerPassenger" varStatus="i">
                                        <activities:activityPricePerPassenger pricePerPassenger="${activityPricePerPassenger}" index="${i.index}"/>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="passenger-select-field hidden">
                        <ul class="activity-msg-field">
                            <li class="y_adult_selection hidden"></li>
                            <li class="y_youth_selection hidden"></li>
                            <li class="y_child_selection hidden"></li>
                            <li class="y_infant_selection hidden"></li>
                            <li class="y_total hidden font-weight-bold"></li>
                        </ul>
                    </div>
                </div>


                <c:choose>
                    <c:when
                        test="${not empty activityPricePerPassengerWithSchedule.availableSchedules}">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <div class="custom-accordion">
                                <p><strong><spring:theme code="text.vacation.ticket.label" text="Tickets" /></strong></p>
                                <div class="custom-accordion-header manage-custom-accordion-header">Select your ticket <span class="custom-arrow"></span></div>
                                <div class="custom-accordion-content pb-0 hidden-obj" style="display: none;">

                        <c:forEach
                            items="${activityPricePerPassengerWithSchedule.availableSchedules}"
                            var="activitySchedule" varStatus="y">


                            <activities:activitySchedule
                                activitySchedule="${activitySchedule}" productCode="${activityPricePerPassengerWithSchedule.productCode}" checkInDate="${activityPricePerPassengerWithSchedule.checkInDate}"  checkOutDate="${checkOutDate}" index="${y.index}" />

                        </c:forEach>
                                <div class="row accommodation-grey-bg fnt-14">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <span class="bcf bcf-icon-info-solid ml-0 pull-left"></span>
                                            <p class="mb-0 ml-5">
                                                <spring:theme code="text.vacation.dont.see.label" text="Don't see the date or time you would like? Call us at 1-888-000-0000 to reserve your spot." />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <c:if test="${not empty activityPricePerPassengerWithSchedule.selectedDate}">
                            <c:set var="selectedDate" value="${activityPricePerPassengerWithSchedule.selectedDate}" />
                            </c:if>
                            <div class="passenger-select-field <c:if test='${empty selectedDate}'> hidden </c:if> ">
                                <ul>
                                    <li class="y_activitytime">
                                        ${selectedDate}
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </c:when>
                    <c:otherwise>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                      <ul class="nav nav-tabs vacation-calender activity-calender">
                        <li class="tab-links tab-depart">
                        <p><strong>Check In Time</strong></p>
                          <a data-toggle="tab" href="#check-in" class="nav-link activity-tab component-to-top">
                          <div class="vacation-calen-box vacation-ui-depart">
                           <span class="vacation-calen-date-txt"><spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" /></span>
                           	<c:choose>
                                   <c:when test="${ not empty activityPricePerPassengerWithSchedule.selectedDate}">
                                       <span class="vacation-calen-date-txt">${activityPricePerPassengerWithSchedule.selectedDate}</span>
                                   </c:when>
                                   <c:otherwise>
                                       <span class="vacation-calen-date-txt current-year-custom"></span>
                                   </c:otherwise>
                            </c:choose>


                            <i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
                              aria-hidden="true"></i>
                          </div>
                        </a></li>
                      </ul>



                      <div class="tab-content">
                        <div id="check-in" class="tab-pane input-required-wrap activity-tab-content depart-calendar">
                          <div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
                            <label>Search date:dd/mm/yyyy</label>
                             <input name="selectedDate" value="${activityPricePerPassengerWithSchedule.selectedDate}" type="text" class="bc-dropdown depart datepicker-input valid" value="" autocomplete="off" aria-invalid="false">
                            <div class="datepicker5 bc-dropdown--big">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                   </c:otherwise>
                </c:choose>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form:button id="addActivityToDealCart" type="submit" class="btn btn-primary custom-btn">
                        <c:choose>
                            <c:when test="${activityPricePerPassengerWithSchedule.blocktypeStatus eq 'ONREQUEST'}">
                                <spring:theme code="text.activity.add.to.package.on.request" />
                            </c:when>
                            <c:otherwise>
                                <spring:theme code="text.activity.add.to.package" text="Add To Package" />
                            </c:otherwise>
                        </c:choose>
                    </form:button>
                </div>

                </form:form>
            </json:property>
        </json:object>
</c:if>
