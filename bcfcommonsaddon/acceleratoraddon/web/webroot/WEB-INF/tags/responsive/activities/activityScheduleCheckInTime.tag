<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="selectedDate" required="false" type="java.lang.String"%>
<form:input type="text" path="selectedDate" value="${selectedDate}" id="y_activityCheckInDatePicker" class="y_activityCheckInDatePicker form-control" placeholder="Activity Check In"
autocomplete="off"/>
