<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="activityFinderForm" required="true" type="com.bcf.bcfcommonsaddon.forms.cms.ActivityFinderForm"%>
<%@ attribute name="activitiesResponseDataList" required="true" type="com.bcf.facades.activity.search.response.data.ActivitiesResponseData"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:url value="/vacations/activities" var="activitySearchUrl"/>
<form:form id="y_activitySearchSortForm" method="GET" commandName="activityFinderForm" action="${activitySearchUrl}">
    <form:input type="hidden" path="destination"/>
    <form:input type="hidden" path="date"/>
    <form:input type="hidden" path="activityCategoryTypes"/>
    <form:input type="hidden" path="activity"/>
	<c:forEach items="${activityFinderForm.guestData}" var="activity" varStatus="i">
	<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].guestType"/>
	<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].quantity"/>
	<c:forEach items="${activity.age}" var="age" varStatus="j">
	<form:input type="hidden" path="${fn:escapeXml(formPrefix)}guestData[${fn:escapeXml(i.index)}].age[${fn:escapeXml(j.index)}]"/>
	</c:forEach>
	</c:forEach>
   	<input id="y_resultsViewTypeForSortForm" type="hidden" name="resultsViewType" value="${fn:escapeXml(resultsViewType )}"/>
    <input type="hidden" name="q" value="${fn:escapeXml(activitiesResponseDataList.query)}" />
    <label for="y_activitySearchSortSelect">
        <spring:theme code="accommodation.sort.by.title" text="Sort by:" />
    </label>
    <select class="form-control" name="sort" id="y_activitySearchSortSelect">
        <option selected disabled>
            <spring:theme code="accommodation.sort.order.select.sort" text="Select a sort"/>
        </option>
        <c:forEach var="sort" items="${activitiesResponseDataList.sorts}">
            <option value="${fn:escapeXml(sort.code)}" ${sort.selected ? 'selected' : ''}>
                <c:choose>
                    <c:when test="${not empty sort.name}">
                        ${fn:escapeXml(sort.name)}
                    </c:when>
                    <c:otherwise>
                        <spring:theme code="activity.sort.order.${sort.code}" text="${fn:escapeXml(sort.code)}" />
                    </c:otherwise>
                </c:choose>
            </option>
        </c:forEach>
    </select>
</form:form>
