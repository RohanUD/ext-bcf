<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="guestDatas" required="true" type="java.util.List"%>
<%@ attribute name="maxGuestPerPax" required="true" type="java.lang.Integer"%>
<c:forEach var="guestDataItem" items="${guestDatas}" varStatus="indexStatus">
    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6 pr-0">
        <div class="y_${guestDataItem.guestType}GuestTypeDiv">
            <label for="y_${guestDataItem.guestType}"> <spring:theme code="label.activity.details.guests.${guestDataItem.guestType}" /> ${guestDataItem.ageRange} </label>
            <c:set var="guestQuantity" >${guestDataItem.quantity}</c:set>
            <form:input type="hidden" path="guestData[${indexStatus.index}].guestType" />
            <div class="input-btn-mp y_${guestDataItem.guestType}Select">
                <span class="fa bcf bcf-icon-remove btn-number activity-passenger-selector" data-type="minus" data-field="guestData[${indexStatus.index}].quantity"></span>
                <input type="text" readonly="" class="font-weight-bold text-blue" value="${guestQuantity}" min="0" max="${maxGuestPerPax}" id="y_${fn:escapeXml(guestDataItem.guestType)}" name="guestData[${indexStatus.index}].quantity">
                <span class="fa bcf bcf-icon-add btn-number activity-passenger-selector" data-type="plus" data-field="guestData[${indexStatus.index}].quantity"></span>
            </div>

        </div>
	</div>
</c:forEach>
<div class="col-md-3 col-sm-4 y_childAgeDropdown"></div>
