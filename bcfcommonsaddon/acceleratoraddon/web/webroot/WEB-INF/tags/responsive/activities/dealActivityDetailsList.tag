<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="activities" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/activities"%>

<c:choose>
   <c:when test="${fn:length(facetSearchPageData.activityResponseData) == 0}">
      <p>
         <spring:theme code="text.activity.noresult" text="No Activities Available" />
      </p>
   </c:when>
   <c:otherwise>
      <div class="row">
         <c:forEach var="activityResponse" items="${facetSearchPageData.activityResponseData}" varStatus="loop">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-5 activity-item-box">
               <div class="accommodation-image package-list-owl">
                  <div
                     class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(activityResponse.images)}">
                     <c:forEach var="image" items="${activityResponse.images}">
                        <c:set var="dataMedia" value="${image.url}" />
                        <div class="image">
                           <img class='js-responsive-image' alt='${altText}'
                              title='${altText}' data-media='${dataMedia}' />
                           <div class="slider-count">
                              <div class="slider-img-thumb"></div>
                              <div class="slider-num">1/5</div>
                           </div>
                        </div>
                     </c:forEach>
                  </div>
               </div>
               <div class="accommodation-details deal-details">
                  <div class="row">
                     <div
                        class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mb-3 m-mb-2 deal-details-border">
                        <h3 class="vacation-h3 mb-3">
                           ${fn:escapeXml(activityResponse.name)}
                        </h3>
                        <div class="text-div mb-3 fnt-14"">
                           ${fn:escapeXml(activityResponse.description)}
                           <c:out value="${activityResponse.description}" />
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                        <div class="row margin-bottom-20">
                           <c:if test="${not empty activityResponse.adultPriceValue}">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 accommodation-duration-price activity-duration-price text-center">
                                 <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                    value="${activityResponse.adultPriceValue}"
                                    var="adultAmount" />
                                 <p class="fnt-14">
                                    <spring:theme code="text.activity.listing.peradult.price"
                                       arguments="${adultAmount}" />
                                 </p>
                              </div>
                           </c:if>
                        </div>
                        <c:choose>
                           <c:when test="${not activityResponse.available && activityResponse.availableStatus eq 'NOT_BOOKABLE_ONLINE'}">
                              <a href="${fn:escapeXml(request.contextPath)}/get-quote?name=${activityResponse.name}" class="btn btn-primary btn-small">
                                 <spring:theme code="text.package.listing.button.notBookableOnline" />
                              </a>
                           </c:when>
                           <c:when test="${activityResponse.available && activityResponse.availableStatus eq 'OPEN'}">
                              <div class="row y_price">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input name="price_y_spAdult" type="hidden"
                                       value="${activityResponse.adultPriceValue}" data-currency="$"
                                       class="price_y_spAdult" />
                                    <c:set var="productCode" value="${activityResponse.code}" />
                                    <input name="productCode" type="hidden" value="${productCode}"
                                       class="y_productCode" /> <input name="changeActivity"
                                       type="hidden" value="${changeActivity}"
                                       class="y_changeActivity" />
                                    <input name="changeActivityCode" type="hidden" value="${changeActivityCode}" class="y_changeActivityCode" />
                                    <c:if test="${empty activityPricePerPassengerWithScheduleMap || empty activityPricePerPassengerWithScheduleMap[productCode]}">
                                       <button type="button" class="btn activity-view-btn btn-small btn-primary">
                                          <span class="btn-txt"><spring:theme code="text.activity.view.details" text="View Details" /></span>
                                       </button>
                                    </c:if>
                                 </div>
                              </div>
                           </c:when>
                           <c:otherwise>
                              <div class="row y_price sold-out-text">
                                 <spring:theme code="text.activity.listing.activity.soldOut" />
                              </div>
                           </c:otherwise>
                        </c:choose>
                     </div>
                  </div>
                  <div class="row y_pricePerPassenger_${productCode}">
                     <c:if test="${not empty activityPricePerPassengerWithScheduleMap}">
                        <c:if
                           test="${not empty activityPricePerPassengerWithScheduleMap[productCode]}">
                           <activities:activityPriceAndSchedules
                              activityPricePerPassengerWithSchedule="${activityPricePerPassengerWithScheduleMap[productCode]}" />
                        </c:if>
                     </c:if>
                  </div>
               </div>
            </div>
            <c:if test="${(loop.index + 1) % 2 == 0}">
      </div>
      <div class="row">
      </c:if>
      </c:forEach>
      </div>
   </c:otherwise>
</c:choose>
