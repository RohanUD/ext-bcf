<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="container">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 activity-listing-img-box">
         <div class="row">
            <c:forEach var="activityResponse" items="${facetSearchPageData.activityResponseData}" varStatus="loop">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-5 height-calculation">
                  <div class="accommodation-image">
                     <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery">
                        <c:forEach var="image" items="${activityResponse.images}">
                           <c:set var="dataMedia" value="${image.url}" />
                           <div class="image">
                              <img class='js-responsive-image' alt='${image.altText}' title='${image.altText}' data-media='${dataMedia}' />
                           </div>
                        </c:forEach>
                     </div>
                  </div>
                  <div class="activity-image-box">
                     <div class="row mb-4">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 height-calculation-b">
                           <div class="deal-details-icon">
                              <c:forEach items="${activityResponse.activityCategoryTypes}" var="categoryType">
                                 <span class="${categoryType}"></span>
                              </c:forEach>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                           <div class="accommodation-name-awards">
                              <h2 class="font-weight-bold text-blue mt-0">${activityResponse.name}</h2>
                              <a id="y_LearnMore" href="${contextPath}/vacations/activities/${activityResponse.code}">
                                 <strong>
                                    <spring:theme code="activity.listing.learn.more.link.text" />
                                 </strong>
                                 <i class="far fa-angle-right"></i>
                              </a>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 border-left">
                           <c:choose>
                              <c:when test="${activityResponse.availableStatus eq 'NOT_BOOKABLE_ONLINE'}">
                                 <a href="${fn:escapeXml(request.contextPath)}/get-quote?name=${activityResponse.name}" class="btn btn-primary">
                                    <spring:theme code="text.activity.listing.activity.notBookableOnline" />
                                 </a>
                              </c:when>
                              <c:when test="${not empty activityResponse.adultPriceValue}">
                                 <div class="accommodation-duration-price activity-listing-price-box">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                       value="${activityResponse.adultPriceValue}"
                                       var="adultAmount" />
                                    <p>
                                       <spring:theme code="text.activity.listing.peradult.price" arguments="${adultAmount}" />
                                    </p>
                                    <c:choose>
                                       <c:when test="${changeActivity}">
                                          <c:url var="detailsPageUrl" value="/vacations/activities/${activityResponse.code}?changeActivity=true&changeActivityCode=${changeActivityCode}&date=${changeActivityDate}&time=${changeActivityTime}&changeActivityRef=${changeActivityRef}" />
                                       </c:when>
                                       <c:otherwise>
                                          <c:url var="detailsPageUrl" value="/vacations/activities/${activityResponse.code}" />
                                       </c:otherwise>
                                    </c:choose>
                                    <a href="${detailsPageUrl}" >
                                       <button class="btn btn-primary btn-block">
                                          <c:choose>
                                             <c:when test="${activityResponse.blocktypeStatus eq 'ONREQUEST'}">
                                                <spring:theme code="activity.listing.button.onRequest.text" />
                                             </c:when>
                                             <c:otherwise>
                                                <spring:theme code="activity.listing.button.bookNow.text" />
                                             </c:otherwise>
                                          </c:choose>
                                       </button>
                                    </a>
                                 </div>
                              </c:when>
                              <c:otherwise>
                                 <div class="accommodation-duration-price activity-listing-price-box sold-out-text box-align-center">
                                    <spring:theme code="text.activity.listing.activity.soldOut" />
                                 </div>
                              </c:otherwise>
                           </c:choose>
                        </div>
                     </div>
                  </div>
               </div>
               <c:if test="${(loop.index + 1) % 2 == 0}">
         </div>
         <div class="row">
         </c:if>
         </c:forEach>
         </div>
      </div>
   </div>
</div>
