<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="pricePerPassenger" required="true" type="com.bcf.facades.activity.data.ActivityPricePerPassenger"%>
<%@ attribute name="index" required="true" type="java.lang.Integer"%>
<%@ attribute name="code" required="false" type="java.lang.String"%>


<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-6 y_passengerWrapper margin-bottom-17">
    <label for="y_${fn:escapeXml(pricePerPassenger.passengerType.code)}"> ${pricePerPassenger.passengerType.name} - ${pricePerPassenger.price.actualRate.formattedValue} </label>

 	<div class="input-btn-mp" data-controller="y_${fn:escapeXml(pricePerPassenger.passengerType.code)}">
	    <span class="fa bcf bcf-icon-remove btn-number activity-passenger-selector y_${fn:escapeXml(pricePerPassenger.passengerType.code)}" data-type="minus" data-field="${code}_activityPricePerPassenger[${index}].passengerType.quantity" style="pointer-events: none;"></span>
	    	<input type="text" readonly="" class="input-number adult-qty font-weight-bold" value="${fn:escapeXml(pricePerPassenger.passengerType.quantity)}" min="0" max="9" id="y_${fn:escapeXml(pricePerPassenger.passengerType.code)}" name="${code}_activityPricePerPassenger[${index}].passengerType.quantity">
	    <span class="fa bcf bcf-icon-add btn-number activity-passenger-selector y_${fn:escapeXml(pricePerPassenger.passengerType.code)}" data-type="plus" data-field="${code}_activityPricePerPassenger[${index}].passengerType.quantity"></span>
    </div>
   </div>

   <form:input
		path="activityPricePerPassenger[${index}].passengerType.quantity"
		type="hidden" data-id="y_${fn:escapeXml(pricePerPassenger.passengerType.code)}" value="${fn:escapeXml(pricePerPassenger.passengerType.quantity)}" data-price="${pricePerPassenger.price.actualRate.formattedValue}" readonly="true" />

    <form:input
        path="activityPricePerPassenger[${index}].passengerType.code"
        class="y_passengerTypeCode" type="hidden" readonly="true" value="${fn:escapeXml(pricePerPassenger.passengerType.code)}"/>
    <form:input
        path="activityPricePerPassenger[${index}].passengerType.name"

        type="hidden" readonly="true" />
