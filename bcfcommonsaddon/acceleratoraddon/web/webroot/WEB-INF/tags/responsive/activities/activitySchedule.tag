<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="checkInDate" required="false" type="java.util.Date"%>
<%@ attribute name="productCode" required="false" type="java.lang.String"%>
<%@ attribute name="checkOutDate" required="false" type="java.util.Date"%>
<%@ attribute name="activitySchedule" required="true" type="com.bcf.facades.activity.data.ActivityScheduleData"%>
<%@ attribute name="index" required="true" type="java.lang.Integer"%>
<c:set var="formattedCheckInDate">
	<fmt:formatDate value="${checkInDate}" type="DATE" pattern="yyyy-MM-dd HH:mm:ss" />
</c:set>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<div class="passenger-option-select activity-radio-time">

		<c:choose>
            <c:when test="${not empty productCode}">
                <c:if test="${not empty activityProductWithScheduleDatesMap and not empty activityProductWithScheduleDatesMap[productCode]}">
                    <c:set var="scheduleDates" value="${activityProductWithScheduleDatesMap[productCode]}"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:set var="scheduleDates" value="${datesGroupByScheduleMap[activitySchedule]}"/>
               </c:otherwise>
        </c:choose>

        <c:if test="${not empty scheduleDates}">
            <c:forEach var="date" items="${scheduleDates}">
                <label class="custom-radio-input">
                    <c:set var="formattedDate">
                        <fmt:formatDate value="${date}" type="DATE" pattern="yyyy-MM-dd HH:mm:ss" />
                    </c:set>
                    <c:set var="formattedDateToShow">
                        <fmt:formatDate value="${date}" type="DATE" pattern="EEEE, MMMM dd : hh:mm a" />
                    </c:set>
                    <c:choose>
                         <c:when test="${formattedCheckInDate==formattedDate}">
                            <input id="y_activitySchedule" checked="checked" class="activity-selection-radio" name="checkInDate" type="radio" value="${date}" data-formattedDate="${formattedDateToShow}" data-starttime="${activitySchedule.startTime}">${formattedDateToShow}
                         </c:when>
                        <c:otherwise>
                            <input id="y_activitySchedule"   class="activity-selection-radio" name="checkInDate" type="radio" value="${date}" data-formattedDate="${formattedDateToShow}" data-starttime="${activitySchedule.startTime}">${formattedDateToShow}
                         </c:otherwise>
                    </c:choose>
                    <span class="checkmark"></span>
                </label>
            </c:forEach>
        </c:if>
		</label>
		</div>
	</div>
</div>
