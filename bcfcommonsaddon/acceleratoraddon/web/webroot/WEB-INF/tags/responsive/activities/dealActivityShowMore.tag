<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${totalNumberOfResults gt 0}">
<div class="container">
			<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mb-3">
            			<p class="y_shownResultId font-weight-light font-italic text-gray">
            				<spring:theme code="text.accommodation.listing.shown.hotels.results" arguments="${startingNumberOfResults}, ${totalShownResults}, ${totalNumberOfResults}" />
            			</p>
            		</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="acco-pagination text-center">
						<li>
						    <a href="#" class="y_showDealActivitySpecificPageResults deal prev" data-pagenumber="${pageNum-1}">
						        <div class="y_showPreviousPageResults">
							        <spring:theme code="text.accommodation.lsiting.show.previous.page" text="< Prev" />
						        </div>
						    </a>
						</li>
				        <li>
					        <a class="y_currentPageNumber" data-pagenumber="${pageNum}">${pageNum}</a>
				        </li>
				        <li>
					        <a href="#" class="y_showDealActivitySpecificPageResults deal next" data-pagenumber="${pageNum+1}">
						        <c:choose>
							        <c:when test="${hasMoreResults}">
								        <button class="y_showNextPageResults">
									        <spring:theme code="text.accommodation.lsiting.show.next.page" text="Next >" />
								        </button>
							        </c:when>
							        <c:otherwise>
								        <button class="y_showNextPageResults" disabled>
									        <spring:theme code="text.accommodation.lsiting.show.next.page" text="Next >" />
								        </button>
							        </c:otherwise>
						        </c:choose>
					        </a>
				        </li>
					</ul>
				</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 text-center">
							<div class="back-top-btn">
								<a href="#"><span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span><strong>
										<spring:theme code="text.accommodation.lsiting.back.to.top" text="Back To Top" /></strong></a>
							</div>
                		</div>

			</div>
</div>
</c:if>
