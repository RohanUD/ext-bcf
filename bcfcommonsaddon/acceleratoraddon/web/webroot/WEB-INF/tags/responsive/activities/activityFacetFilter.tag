<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
<%@ attribute name="isDealActivity" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty facetData.values}">
	<c:if test="${isDealActivity  eq false || (isDealActivity eq true && facetData.name ne 'destination')}">
		<form action="${requestScope['javax.servlet.forward.request_uri']}">
			<div class="filter-form clearfix col-lg-12 col-sm-12 col-md-12 col-xs-12">
				<div class="filter-facets">
					<div class="accommodations_select mb-4">
						<ul class="nav nav-pills">
							<li role="presentation" class="dropdown filter-li full-width-dropdown">
								<p class="text-grey">
									<small><spring:theme code="activity.search.facet.${facetData.name}" /></small>
								</p>
								<a id="${facetData.name}" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									<spring:theme code="activity.search.facet.${facetData.name}" />
									<span class="fa fa-angle-down pull-right"></span>
								</a>
								<ul class="dropdown-menu filter-dropdown package-list-checkbox">
									<c:forEach items="${facetData.values}" var="facetValue" varStatus="fcIdx">
										<c:set var="facetSelected" value="false" />
										<c:forEach items="${paramValues}" var="selectedFacet">
											<c:if test="${selectedFacet.key eq facetData.code}">
												<c:forEach items="${selectedFacet.value}" var="selectedFacetValue">
													<c:if test="${selectedFacetValue eq facetValue.code}">
														<c:set var="facetSelected" value="true" />
													</c:if>
												</c:forEach>
											</c:if>
										</c:forEach>
										<li class="">
											<div class="checkbox">
												<label class="custom-checkbox-input show deal-details-icon" for="facet_${facetValue.code}_${fcIdx.count}">
													<input type="checkbox" ${facetSelected ? 'checked="checked"' : ''} name="${facetData.code}" id="facet_${facetValue.code}_${fcIdx.count}" value="${facetValue.code}" /> <span>${facetValue.name}</span> <span class="checkmark-checkbox"></span>
												</label>
											</div>
										</li>
									</c:forEach>
									<c:forEach items="${paramValues}" var="selectedFacet">
										<c:if test="${selectedFacet.key ne facetData.code}">
											<c:forEach items="${selectedFacet.value}" var="selectedFacetValue" varStatus="loop">
												<input type="hidden" name="${selectedFacet.key}" value="${selectedFacet.value[loop.index]}" />
											</c:forEach>
										</c:if>
									</c:forEach>
									<li>
										<input class="btn btn-primary custom-btn my-4" type="submit" value="Apply">
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</form>
	</c:if>
</c:if>
