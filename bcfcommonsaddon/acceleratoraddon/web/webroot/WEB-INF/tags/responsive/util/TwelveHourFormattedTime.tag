
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ attribute name="time" required="true" type="java.lang.String"%>

<c:if test="${not empty time}">
    <fmt:parseDate value="${time}" pattern="HH:mm" var="parsedTime" />
    <fmt:formatDate value="${parsedTime}" var="startTimeFormatted" pattern="hh:mm aa" />
    ${startTimeFormatted}
</c:if>
