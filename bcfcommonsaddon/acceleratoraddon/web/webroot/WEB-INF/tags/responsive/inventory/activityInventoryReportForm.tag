<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-12 margin-top-20"><h4><spring:theme code="header.vacation.inventory.report.activity" text="Activity Inventory Report Form" /></h4></div>
<div class="row">
	<div class="col-md-12">
				<c:url var="activityInventoryReportUrl" value="inventory-reports/activity" />
				<form:form commandName="activityInventoryReportForm" class="y_activityInventoryReportForm" action="${activityInventoryReportUrl}" method="POST">
					<fieldset>
					<div class="col-md-6">
						<label for="startDate"> <spring:theme code="label.vacation.inventory.report.startDate" text="Start Date" /></label>
						<form:input type="text" id="activityStartDate" path="startDate" class="col-xs-12 datepicker y_activityStartDate form-control" placeholder="dd/MM/yyyy" autocomplete="off" />
						</div>
						<div class="col-md-6">
						<label for="endDate"> <spring:theme code="label.vacation.inventory.report.endDate" text="End Date" /></label>
						<form:input type="text" id="activityEndDate" path="endDate" class="col-xs-12 datepicker y_activityEndDate form-control" placeholder="dd/MM/yyyy" autocomplete="off" />
						</div>
						<div class="col-md-12  margin-top-30 ">
						<p><b> <spring:theme code="label.vacation.inventory.report.daysOfWeek" text="Days of Week" /></b>
						</p></div>
						<div class="col-md-12  margin-top-10">
							
							<c:forEach var="dayOfWeek" items="${activityInventoryReportForm.daysOfWeek}" varStatus="varStartus">
							  <label class="margin-top-10 custom-checkbox-input" for="activityDaysOfWeek${varStartus.count}">${dayOfWeek}
							   	<input id="activityDaysOfWeek${varStartus.count}" name="daysOfWeek" class="y_activityDaysOfWeek" type="checkbox" value="${dayOfWeek}" checked="checked">
								<span class="checkmark-checkbox"></span>
							  	</label>
						  </c:forEach>
						</div>
						<div class="col-md-12 margin-top-20">
						<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<label for="region"> <spring:theme code="label.vacation.inventory.report.region" text="Region" /></label> <input class="y_activityRegionSelector form-control" type="text" placeholder="Select a region">
							<form:input type="hidden" path="region" class="col-xs-12 y_activityRegion" />
						</div>
						<div class="form-group">
							<label for="city"> <spring:theme code="label.vacation.inventory.report.city" text="City" /></label> <input class="y_activityCitySelector form-control" type="text" placeholder="Select a city">
							<form:input type="hidden" path="city" class="col-xs-12 y_activityCity" />
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group">
							<label for="activity"> <spring:theme code="label.vacation.inventory.report.activity" text="Activity" /></label> <input class="y_activitySelector form-control" type="text" placeholder="Select an activity">
							<form:input type="hidden" path="activity" class="col-xs-12 y_activity" />
						</div>
						</div>
						</div>
						</div>
						<div class="col-md-12 margin-top-20">
						<div class="row margin-bottom-20">
						<div class="col-md-6">
						<form:button class="btn btn-primary btn-block  col-xs-12 col-sm-6  y_activityFindInventory">
						    <spring:theme code="button.vacation.inventory.report.findInventory" text="Find Inventory" />
						</form:button>
						</div>
						<div class="col-md-6">
						<a class="btn btn-primary btn-block col-xs-12 col-sm-6 y_activityReset">
                            <spring:theme code="button.vacation.inventory.report.reset" text="Reset" />
                         </a>

						</div>
						</div>
						</div>
					</fieldset>
				</form:form>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-12 y_activityInventoryResponseHtml"></div>
		</div>
	</div>
</div>
