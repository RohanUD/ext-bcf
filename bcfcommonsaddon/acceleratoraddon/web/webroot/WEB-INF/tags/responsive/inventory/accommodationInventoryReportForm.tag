<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-12 margin-top-20"><h4><spring:theme code="header.vacation.inventory.report.accommodation" text="Accommodation Inventory Report Form" /></h4></div>
<div class="row">
	<div class="col-md-12 col-xs-12 margin-top-30">
				<c:url var="accommodationInventoryReportUrl" value="/inventory-reports/accommodation" />
				<form:form commandName="accommodationInventoryReportForm" class="y_accommodationInventoryReportForm" action="${accommodationInventoryReportUrl}" method="POST">
					<fieldset>
					<div class="col-md-6">
					<label for="startDate"> <spring:theme code="label.vacation.inventory.report.startDate" text="Start Date" /></label>
						<form:input type="text" path="startDate" class="col-xs-12 datepicker y_accommodationStartDate form-control" placeholder="dd/MM/yyyy" autocomplete="off" />
					</div>
						<div class="col-md-6">
						<label for="endDate"> <spring:theme code="label.vacation.inventory.report.endDate" text="End Date" /></label>
						<form:input type="text" path="endDate" class="col-xs-12 datepicker y_accommodationEndDate form-control" placeholder="dd/MM/yyyy" autocomplete="off" />
						</div>
						<div class="col-md-12  margin-top-30 ">
						<p><b><spring:theme code="label.vacation.inventory.report.daysOfWeek" text="Days of Week" /></b></p>
					</div>
					<div class="col-md-12  margin-top-10">
							<c:forEach var="dayOfWeek" items="${accommodationInventoryReportForm.daysOfWeek}" varStatus="varStartus">
						  <label class="custom-checkbox-input margin-top-10" for="daysOfWeek${varStartus.count}">${dayOfWeek}
						   	<input id="daysOfWeek${varStartus.count}" name="daysOfWeek" class="y_accommodationDaysOfWeek" type="checkbox" value="${dayOfWeek}" checked="checked">
							
						  	<span class="checkmark-checkbox"></span>
						  	</label>
						  </c:forEach>
						</div>
						<div class="col-md-12 margin-top-20">
						<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<label for="region"> <spring:theme code="label.vacation.inventory.report.region" text="Region" /></label> 
							<input class="y_accommodationRegionSelector form-control" type="text" placeholder="Select a region">
							<form:input type="hidden" path="region" class="col-xs-12 y_accommodationRegion" />
						</div>
						<div class="form-group">
							<label for="city"> <spring:theme code="label.vacation.inventory.report.city" text="City" /></label> <input class="y_accommodationCitySelector form-control" type="text" placeholder="Select a city">
							<form:input type="hidden" path="city" class="col-xs-12 y_accommodationCity" />
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group">
							<label for="hotel"> <spring:theme code="label.vacation.inventory.report.hotel" text="Hotel" /></label> <input class="y_accommodationHotelSelector form-control" type="text" placeholder="Select a hotel">
							<form:input type="hidden" path="hotel" class="col-xs-12 y_accommodationHotel" />
						</div>
						<div class="form-group">
							<label for="room"> <spring:theme code="label.vacation.inventory.report.room" text="Room" /></label> <input class="y_accommodationRoomSelector form-control" type="text" placeholder="Select a room">
							<form:input type="hidden" path="room" class="col-xs-12 y_accommodationRoom" />
						</div>
						</div>
						</div>
						</div>
						<div class="col-md-12 margin-top-20">
						<div class="row margin-bottom-20">
						<div class="col-md-6">
						<form:button class="btn btn-primary btn-block y_accommodationFindInventory">
							<spring:theme code="button.vacation.inventory.report.findInventory" text="Find Inventory" />
						</form:button>
						</div>
						<div class="col-md-6">
						<a class="btn btn-primary btn-block y_accommodationReset">
							<spring:theme code="button.vacation.inventory.report.reset" text="Reset" />
						</a>
						</div>
						</div>
						</div>
					</fieldset>
				</form:form>
	</div>
</div>
<div class="row">
	
			<div class="col-md-12 y_accommodationInventoryResponseHtml"></div>
		
</div>
