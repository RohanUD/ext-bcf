<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row margin-top-40 margin-bottom-30">
	<div class="col-md-offset-2 col-lg-4 col-md-4 col-xs-12 text-center mt-5 mob-mb30">
			<a href="${contextPath}${fn:escapeXml(addActivities)}" class="btn btn-primary btn-block">
				<spring:theme code="label.packageferryreview.add.activities" text="Add activities" />
			</a>
	</div>
		<div class="col-lg-4 col-md-4 col-xs-12 text-center mt-5 mobile-mt">
    			<a href="${contextPath}${fn:escapeXml(confirmAndReview)}" class="btn btn-transparent btn-block">
    				<spring:theme code="label.packageferryreview.confirm.and.review" text="Confirm and review" />
    			</a>
    	</div>
</div>
