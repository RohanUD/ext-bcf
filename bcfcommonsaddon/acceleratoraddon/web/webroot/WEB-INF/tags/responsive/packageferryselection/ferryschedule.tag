<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="fareSelection" required="true" type="de.hybris.platform.commercefacades.travel.FareSelectionData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${refNumber == outboundRefNumber}">
		<c:set var="sortby" value="outbound-sort-by" />
		<c:set var="rowresult" value="outbound-row-result-" />
	</c:when>
	<c:when test="${refNumber == inboundRefNumber}">
		<c:set var="sortby" value="inbound-sort-by" />
		<c:set var="rowresult" value="inbound-row-result-" />
	</c:when>
</c:choose>
<c:set var="noOfOptions" value="${refNumber == outboundRefNumber ? noOfOutboundOptions : noOfInboundOptions }" />
<c:set var="dates" value="${refNumber == outboundRefNumber ? outboundDates : outboundDates }" />
<c:set var="linksForTabs" value="${refNumber == outboundRefNumber ? outboundTabLinks : inboundTabLinks }" />
<div class="nav-tabs-wrapper y_fareResultNavTabs">
	<%-- Tab panes --%>
	<div class="y_fareResultTabContent tab-content">
		<div class="table-responsive">
			<c:choose>
				<c:when test="${empty fareSelection.pricedItineraries || noOfOptions == 0}">
					<div>
						<spring:theme code="fareselection.noofferings" />
					</div>
				</c:when>
				<c:otherwise>
					<c:forEach items="${fareSelection.pricedItineraries}" var="pricedItinerary" varStatus="idx">
						<c:if test="${pricedItinerary.available && pricedItinerary.originDestinationRefNumber == refNumber}">
									<div class="p-card">
										<div class="col-md-12 col-sm-12">
											<c:set var="resultrowid" value="${rowresult}${idx.index}" />
											<div class="row">
												<div class="col-md-7 col-xs-8">
													<div class="row pt-4 padding-0-mobile">
														<c:set var="numberOfConnections" value="${fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings)}" />
														<c:choose>
															<c:when test="${numberOfConnections > 1}">
																<c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
																<c:set var="lastOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
															</c:when>
															<c:otherwise>
																<c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
																<c:set var="lastOffering" value="${firstOffering}" />
															</c:otherwise>
														</c:choose>
														<div class="col-xs-4 padding-0-mobile">
															<div class="p-chart text-center">
																<div class="pc-1">
																	<spring:theme code="fareselection.depart" />
																</div>
																<div class="fnt-24 ferrytime">
																	<fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${firstOffering.departureTime}" />
																</div>
															</div>
														</div>
														<div class="col-xs-4 padding-0-mobile">
															<div class="p-chart text-center">
																<div class="pc-1 distance-time-text">
																	<c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.days'] && pricedItinerary.itinerary.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
																			code="transport.offering.status.result.days" />
																	</c:if>
																	<c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] && pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.hours'])}
																	<spring:theme code="transport.offering.status.result.hours" />
																	</c:if>
																	&nbsp;${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.minutes'])}
																	<spring:theme code="transport.offering.status.result.minutes" />
																</div>
																<div class="distance-icon-line">
																	<span class="bcf bcf-icon-wave-line distance-wave-icon icon-blue"></span>
																	<div class="distance-line icon-blue"></div>
																</div>
															</div>
														</div>
														<div class="col-xs-4 padding-0-mobile">
															<div class="p-chart text-center">
																<div class="pc-1">
																	<spring:theme code="fareselection.arrive" />
																</div>
																<div class="fnt-24">
																	<fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${lastOffering.arrivalTime}" />
																</div>
															</div>
														</div>
														<div class="col-xs-12 pt-4 hidden-md hidden-lg">
					                                               <c:set var="currentTansportOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
														    <a href="/ship-info/${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal" class="sailing-ferry-name">
                                                                <div class="row">
                                                                    <fareselection:flightDetails pricedItinerary="${pricedItinerary}" noOfConnections="${numberOfConnections}" />
                                                                </div>
                                                            </a>
                                                        </div>
													</div>
												</div>
												<div class="col-md-3 pt-4 mob-ferry-name">
					                                    <c:set var="currentTansportOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
														<a href="/ship-info/${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal" class="sailing-ferry-name">
															<fareselection:flightDetails pricedItinerary="${pricedItinerary}" noOfConnections="${numberOfConnections}" />
														</a>
												</div>
												<div class="col-md-2 col-xs-4 border-left padding-0-mobile">
													<div class="p-chart p-cash">
														<div class="pc-3 text-center">
															<c:choose>
																<%-- removing the subBucketCapacity check for now as it is either 0 or null from list sailing SIT service --%>
                                                                <%-- <c:when test="${pricedItinerary.isBookable and !pricedItinerary.zeroSubBucketFreeCapacity }"> --%>
                                                                    <c:when test="${pricedItinerary.isBookable}">
																	<ferryselection:packageofferinglistbundles pricedItinerary="${pricedItinerary}" refNumber="${refNumber}" name="refNumber-${refNumber}" />
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${pricedItinerary.isBookable and pricedItinerary.zeroSubBucketFreeCapacity }">
																			<p class="text-red">
																				<b><spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" /><br> <spring:theme code="text.listsailing.bundle.fare.sold.out.online" text="Online" /> </b>
																			</p>
																		</c:when>
																		<c:otherwise>
																			<p>
																				<spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" />
																			</p>
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
														</div>
													</div>
												</div>
										<div class="duration-info col-xs-6 col-sm-5 col-reset">
											<c:if test="${numberOfConnections > 1}">
                                                  &nbsp;<spring:theme code="${numberOfConnections > 2 ? 'fareselection.offering.stops' : 'fareselection.offering.stop'}" />
												<c:forEach items="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings}" var="transportOffering" varStatus="stopIdx">
													<c:if test="${stopIdx.index < (numberOfConnections - 1)}">
                                                	&nbsp;${stopIdx.index==0 ?'':','} ${transportOffering.destinationLocationCity}&nbsp;
                                                <p>
															<spring:theme code="fareselection.layover" />&nbsp;
															<c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverDays > 0}">
                                                	${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverDays} D
                                                </c:if>
															<c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverHour > 0}">
                                                 &nbsp; ${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverHour} H
                                                </c:if>
															<c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverMinutes > 0}">
                                                 &nbsp; ${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[stopIdx.index].layoverMinutes} M
                                                </c:if>
														</p>
													</c:if>
												</c:forEach>
											</c:if>
										</div>
									</div>
											<c:set var="firstElement" value="${false}" />
										</div>
									</div>
							<c:set var="firstElement" value="${false}" />
						</c:if>
					</c:forEach>
					<c:if test="${isToShowOpenTicket}">
                        <ferryselection:openticket fareSelection="${fareSelection}" refNumber="${refNumber}"/>
                    </c:if>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
