<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="dealFormPath" required="false" type="java.lang.String"%>
<%@ attribute name="itineraryPricingInfo" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItineraryDateTime" type="java.util.Date"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/dealdetails"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<c:choose>
	<c:when test="${refNumber == outboundRefNumber}">
		<c:set var="pricedItineraryDateTime" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings) - 1].arrivalTime}" />
	</c:when>
	<c:otherwise>
		<c:set var="pricedItineraryDateTime" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
	</c:otherwise>
</c:choose>
<c:forEach items="${pricedItinerary.itineraryPricingInfos}" var="itineraryPricingInfo">
	<c:if test="${itineraryPricingInfo.bundleType == fareproductFarebasiscode}">
		<c:set var="showDealForm" value="false" />
		<c:if test="${not empty dealFormPath}">
			<c:set var="showDealForm" value="true" />
		</c:if>
		<c:if test="${not empty itineraryPricingInfo}">
			<li class="vp-box" class="${itineraryPricingInfo.promotional ? 'promotional-price' : ''} ${itineraryPricingInfo.selected ? 'selected' : ''}">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<c:if test="${pricedItinerary.departedSailing ne true}">
								<a href="${contextPath}${fn:escapeXml(nextURL)}" id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}" class="btn-block sailing-btn btn btn-primary y_packageFerryResultSelect">
									<spring:theme code="text.fareselection.button.select" text="Select" />
								</a>
							</c:if>
					</div>
				</div>
				<c:if test="${not empty dealBundleTemplateId}">
					<span class="selected">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					</span>
					<span class="select">
						<spring:theme code="text.accommodation.details.accommodation.select.price" text="select" />
					</span>
				</c:if>
				<c:choose>
					<c:when test="${not empty dealFormPath}">
						<dealdetails:addBundleToCartForm dealFormPath="${dealFormPath}" isDynamicPackage="false" openTicket="false" addBundleToCartUrl="${addBundleToCartUrl}" itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}" />
					</c:when>
					<c:otherwise>
						<fareselection:addBundleToCartForm itineraryPricingInfo="${itineraryPricingInfo}" isDynamicPackage="false" openTicket="false" addBundleToCartUrl="${addBundleToCartUrl}" pricedItinerary="${pricedItinerary}" />
					</c:otherwise>
				</c:choose>
			</li>
		</c:if>
	</c:if>
</c:forEach>

