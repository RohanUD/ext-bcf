<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="item" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<%@ attribute name="journeyRefNo" required="false" type="java.lang.Integer"%>
<%@ attribute name="showRemoveOption" required="false" type="java.lang.Boolean"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${type eq 'OutBound'}">
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
				<c:set var="specificItem" value="${item}"></c:set>
				<c:choose>
					<c:when test="${bookingJourney eq 'BOOKING_PACKAGE'}">
						<c:set var="changeURL" value="/package-select-ferry?change=true"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="changeURL" value="/fare-selection/change-selection/${journeyRefNo}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}"></c:set>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
				<c:set var="specificItem" value="${item}"></c:set>
				<c:choose>
					<c:when test="${bookingJourney eq 'BOOKING_PACKAGE'}">
						<c:set var="changeURL" value="/package-select-ferry?isReturn=true&change=true&index=0"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="changeURL" value="/fare-selection/change-selection/${journeyRefNo}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}"></c:set>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>
<div class="p-card mb-3">
	<div class="row">
		<div class="col-xs-8 col-md-9 border-right-divider">
			<div>
				<div class="col-md-8 col-sm-12">
					<div class="row pt-4 padding-0-mobile">
						<c:set var="numberOfConnections" value="${fn:length(specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings)}" />
						<c:choose>
							<c:when test="${numberOfConnections > 1}">
								<c:set var="firstOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
								<c:set var="lastOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
							</c:when>
							<c:otherwise>
								<c:set var="firstOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
								<c:set var="lastOffering" value="${firstOffering}" />
							</c:otherwise>
						</c:choose>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1">
									<spring:theme code="fareselection.depart" />
								</div>
								<div class="fnt-24">
									<c:choose>
										<c:when test="${specificItem.openTicketSelected}">
											<spring:theme code="fareselection.openselection" text="Open" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate pattern="${reservationItineraryDateFormat}" value="${firstOffering.departureTime}" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1 distance-time-text">
									<c:if test="${not empty specificItem.reservationItinerary.duration['transport.offering.status.result.days'] && specificItem.reservationItinerary.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
											code="transport.offering.status.result.days" />
									</c:if>
									<c:if test="${not empty specificItem.reservationItinerary.duration['transport.offering.status.result.hours'] && specificItem.reservationItinerary.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.hours'])}
										<spring:theme code="transport.offering.status.result.hours" />
									</c:if>
									&nbsp;${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.minutes'])}
									<spring:theme code="transport.offering.status.result.minutes" />
								</div>
								<div class="distance-icon-line">
									<span class="bcf bcf-icon-wave-line distance-wave-icon icon-blue"></span>
									<div class="distance-line icon-blue"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1">
									<spring:theme code="fareselection.arrive" />
								</div>
								<div class="fnt-24">
									<c:choose>
										<c:when test="${specificItem.openTicketSelected}">
											<spring:theme code="fareselection.openselection" text="Open" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate pattern="${reservationItineraryDateFormat}" value="${lastOffering.arrivalTime}" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${specificItem.openTicketSelected eq false}">
                  <div class="col-md-4 col-sm-12 padding-0-mobile qwerty">
                      <a href="/ship-info/${fn:escapeXml(specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.code)}" class="sailing-ferry-name">
                          <c:set var="currentTansportOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
                          ${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.name)}</i>
                      </a>
                      <c:if test="${not empty specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].availability.code and specificItem.reservationItinerary.route.routeType=='LONG' and bookingJourney ne 'BOOKING_PACKAGE'}">
                          <label class="mb-0" >
                                <spring:theme code="text.listsailing.ProductAvailabilityEnum.${fn:escapeXml(specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].availability.code)}" />
                          </label>
                      </c:if>
                  </div>
                </c:if>
			</div>
		</div>
		<div class="col-xs-4 col-md-3 border-left m-border-left text-center">
			<div class="p-chart p-cash">
				<c:if test="${bookingJourney ne 'BOOKING_PACKAGE'}">

					<div class="pc-1 text-center padding-0-mobile">
						<spring:theme code="label.packageferryreview.total.fare" text="Total Fare" /></div>
					<div class="pc-2 text-center padding-0-mobile">
						<strong><format:price priceData="${specificItem.reservationPricingInfo.totalFare.totalPrice}" /></strong>
					</div>

				</c:if>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center cng-btn">
					<c:if test="${!showRemoveOption}">

						<a href="${contextPath}${fn:escapeXml(changeURL)}" class="sailing-btn btn btn-outline-blue width-auto">
							<spring:theme code="label.packageferryreview.reservation.change" text="Change" />
							<span class="glyphicons glyphicons-chevron-up"></span>
						</a>



					</c:if>
					<c:if test="${showRemoveOption}">

						<button class="btn btn-primary removeSailingButton" type="button" id="removeSailingButton_${journeyRefNo}_${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}">

							<input id="removeSailingURL" type="hidden" value="${fn:escapeXml(request.contextPath)}/fare-selection/remove-selection/${journeyRefNo}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}"/>
							<spring:theme code="label.packageferryreview.reservation.remove" text="Remove" />
						</button>


					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>
