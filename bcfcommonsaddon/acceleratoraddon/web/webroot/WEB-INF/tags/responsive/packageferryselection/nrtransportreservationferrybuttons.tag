<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="col-md-8 col-sm-12 col-md-offset-2">
	<div class="row">
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        	<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
        		<c:set var="continueText">
					<spring:theme code="text.page.travellerdetails.button.continue" text="Continue" />
				</c:set>
				<input type="submit" value="${continueText}" class="btn btn-primary btn-block" formaction="/traveller-details/guest-checkout"/>
            </div>
		</sec:authorize>
		<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
        	<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
        		<c:set var="checkoutAsGuest">
					<spring:theme code="label.fareselectionreview.continue.as.guest" text="Checkout as a guest" />
				</c:set>
				<input type="submit" value="${checkoutAsGuest}" class="btn btn-primary btn-block" formaction="/traveller-details/guest-checkout"/>
            </div>
        	<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
        		<c:set var="loginCheckout">
					<spring:theme code="label.fareselectionreview.log.in" text="Login to checkout" />
				</c:set>
				<input type="submit" value="${loginCheckout}" class="btn btn-primary btn-block" formaction="/traveller-details/checkout-login"/>
            </div>
		</sec:authorize>
	</div>
	<div class="row">
		<c:if test="${!maxSailingAllowed and !isModifyingTransportBooking and !isFerryOptionBooking}">
			<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
				<a href="${contextPath}${fn:escapeXml(addAnotherSailing)}" class="btn btn-outline-blue btn-block">
					<spring:theme code="label.fareselectionreview.add.another.sailing" text="Add another sailing" />
				</a>
			</div>
		</c:if>
	</div>
</div>
