<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ attribute name="fareSelection" required="false" type="de.hybris.platform.commercefacades.travel.FareSelectionData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/dealdetails"%>

<spring:htmlEscape defaultHtmlEscape="false" />
		<div class="p-card">
		<c:choose>
        	<c:when test="${refNumber == outboundRefNumber}">
        		<div id="y_outbound" class="y_fareResultTabWrapper  clearfix">
        	</c:when>
        	<c:when test="${refNumber == inboundRefNumber}">
        	<div id="y_inbound" class="y_fareResultTabWrapper  clearfix">
        	</c:when>
        </c:choose>


		<div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-10 col-xs-8 border-right">
					<div class="row pt-2 padding-0-mobile border-right-mobile">
						<div class="col-xs-12 padding-0-mobile">
							<div class="pc-1 mb-2">
                               <h5><span class="bcf bcf-icon-wave-line icon-blue"></span> <spring:theme code="text.packageferryselection.openticket.heading" text="~~ OPEN TICKET ~~" /> <span class="bcf bcf-icon-wave-line icon-blue"></span></h5>
                                <h5 class="mb-1"><b><spring:theme code="text.packageferryselection.openticket.subheading" text="Open tickets do not include a reservation." /></b></h5>
                                    <p class="fnt-14 mb-0"><spring:theme code="text.packageferryselection.openticket.description" text="Can't find the sailing you want?" /></p>
                            </div>

						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-4 padding-0-mobile">
					<div class="p-chart p-cash">
						<div class="pc-3 text-center">
								<li class="vp-box">
									<c:set var="showDealForm" value="false" />
                                        <c:if test="${not empty dealFormPath}">
                                            <c:set var="showDealForm" value="true" />
                                        </c:if>
                                        <c:if test="${not empty firstItinerary}">
                                            <c:choose>
                                                <c:when test="${not empty dealFormPath}">
                                                    <dealdetails:addBundleToCartForm isDynamicPackage="false" openTicket="true" dealFormPath="${dealFormPath}" addBundleToCartUrl="${addBundleToCartUrl}" itineraryPricingInfo="${firstItinerary.itineraryPricingInfos[0]}" pricedItinerary="${firstItinerary}" />
                                                </c:when>
                                                <c:otherwise>
                                                    <fareselection:addBundleToCartForm isDynamicPackage="false" openTicket="true" itineraryPricingInfo="${firstItinerary.itineraryPricingInfos[0]}" addBundleToCartUrl="${addBundleToCartUrl}" pricedItinerary="${firstItinerary}" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
									<div class="row">
								<div class="col-lg-12 col-ms-12 col-sm-12 col-xs-12 transform-90">
											<a href="${contextPath}${fn:escapeXml(nextURL)}" class="btn btn-block btn-primary y_packageFerryResultSelect">
													<spring:theme code="text.fareselection.button.select" text="Select" />
											</a>
										</div>
									</div>
								</li>
							</div>
						</div>
					</div>
					<div class="duration-info col-xs-6 col-sm-5 col-reset"></div>
				</div>
			</div>
		</div>
    </div>
