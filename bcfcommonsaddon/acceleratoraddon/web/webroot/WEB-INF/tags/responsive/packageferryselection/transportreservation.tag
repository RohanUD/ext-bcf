<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="item" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${type eq 'OutBound'}">
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
				<c:set var="specificItem" value="${item}"></c:set>
			</c:when>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
				<c:set var="specificItem" value="${item}"></c:set>
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>
<div class="sailing-select mb-3">
	<div class="row">
		<div class="col-xs-8 col-md-10 package-border-right">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="row pt-4 padding-0-mobile">
						<c:set var="numberOfConnections" value="${fn:length(specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings)}" />
						<c:choose>
							<c:when test="${numberOfConnections > 1}">
								<c:set var="firstOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
								<c:set var="lastOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
							</c:when>
							<c:otherwise>
								<c:set var="firstOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
								<c:set var="lastOffering" value="${firstOffering}" />
							</c:otherwise>
						</c:choose>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1">
									<spring:theme code="fareselection.depart" />
								</div>
								<div class="fnt-24">
									<c:choose>
										<c:when test="${specificItem.openTicketSelected}">
											<spring:theme code="fareselection.openselection" text="Open" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate pattern="${reservationItineraryDateFormat}" value="${firstOffering.departureTime}" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1 distance-time-text">
									<c:if test="${not empty specificItem.reservationItinerary.duration['transport.offering.status.result.days'] && specificItem.reservationItinerary.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
											code="transport.offering.status.result.days" />
									</c:if>
									<c:if test="${specificItem.openTicketSelected ne 'true'}">
									<c:if test="${not empty specificItem.reservationItinerary.duration['transport.offering.status.result.hours'] && specificItem.reservationItinerary.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.hours'])}
										<spring:theme code="transport.offering.status.result.hours" />
									</c:if>
									&nbsp;${fn:escapeXml(specificItem.reservationItinerary.duration['transport.offering.status.result.minutes'])}
									<spring:theme code="transport.offering.status.result.minutes" />
									</c:if>
								</div>
								<div class="distance-icon-line">
									<span class="bcf bcf-icon-wave-line distance-wave-icon icon-blue"></span>
									<div class="distance-line icon-blue"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-4 padding-0-mobile">
							<div class="p-chart text-center check-icon-sec">
								<div class="pc-1">
									<spring:theme code="fareselection.arrive" />
								</div>
								<div class="fnt-24">
									<c:choose>
										<c:when test="${specificItem.openTicketSelected}">
											<spring:theme code="fareselection.openselection" text="Open" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate pattern="${reservationItineraryDateFormat}" value="${lastOffering.arrivalTime}" />
										</c:otherwise>
									</c:choose>
								</div>
								<div class="check-icon-radio">
									<span class="bcf bcf-icon-checkmark"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${specificItem.openTicketSelected eq 'false'}">
                    <div class="col-md-4 col-sm-12 padding-0-mobile">
                        <a href="/ship-info/${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.code}" class="sailing-ferry-name">
                            <c:set var="currentTansportOffering" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0]}" />
                            &nbsp;${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.name)}</i>
                        </a>
                    </div>
				</c:if>
			</div>
		</div>
		<div class="col-xs-4 col-md-2 package-border-left">
		<div class="text-center">
		<c:choose>
			<c:when test="${sessionBookingJourney eq 'BOOKING_TRANSPORT_ONLY'}">
				<p class="pc-1">Total fare</p>
				<h5 class="mb-0 fnt-24"><span>&#36;</span>${specificItem.reservationPricingInfo.totalFare.totalPrice.value}</h5>
				<c:choose>
					<c:when test="${not empty specificItem.reservationPricingInfo.itineraryPricingInfo.totalFare.priceDifference}">
						<span class="price-desc">${fn:escapeXml(specificItem.reservationPricingInfo.itineraryPricingInfo.totalFare.priceDifference.formattedValue)}</span>
					</c:when>
					<c:otherwise>
						<span class="price-desc">${specificItem.reservationPricingInfo.itineraryPricingInfo.available && specificItem.reservationPricingInfo.itineraryPricingInfo.totalFare.totalPrice.value > 0 ? specificItem.reservationPricingInfo.itineraryPricingInfo.totalFare.totalPrice.formattedValue : '&mdash;'}</span>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
		</div>
		</div>
	</div>
</div>
