<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="travelFinderForm" required="true" type="com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm"%>
<%@ attribute name="isReturn" required="true" type="java.lang.Boolean"%>
<%@ attribute name="refNumber" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class=" row">
	<c:forEach var="accessibility" items="${travelFinderForm.fareFinderForm.accessibilityRequestDataList}" varStatus="i">
		<div class="col-xs-6">
			<c:forEach var="specialService" items="${accessibility.specialServiceRequestDataList}" varStatus="i">
				<c:if test="${specialService.selection}">
				</c:if>
			</c:forEach>
			<c:forEach var="ancillary" items="${accessibility.ancillaryRequestDataList}" varStatus="i">
				<c:if test="${ancillary.code eq accessibility.selection}">
				</c:if>
			</c:forEach>
		</div>
	</c:forEach>
</div>
