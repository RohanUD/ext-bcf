<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="item" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${type eq 'OutBound'}">
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
				<c:set var="specificItem" value="${item}"></c:set>
			</c:when>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
				<c:set var="specificItem" value="${item}"></c:set>
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>

	<div class="p-card">
		<div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<div class="row pt-2 padding-0-mobile border-right-mobile border-right">
						<div class="col-xs-12 padding-0-mobile">
							<div class="p-chart text-center">
								<div class="pc-1">
									<p><spring:theme code="label.packageferryreview.depart.heading" />
									<span class="bcf bcf-icon-info-solid"></span></p>
								</div>
								<p>
									<b><fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
									${departureDate}</b>
								</p>
								<div>
									<a href="#">
										<spring:theme code="label.packageferryreview.viewschedule.url" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-6 padding-0-mobile">
					<div class="pt-2 padding-0-mobile p-chart p-cash">
						<div class="pc-3">
							<spring:theme code="label.packageferryreview.included" />
						</div>
						<div class="pc-3">
							<c:choose>
								<c:when test="${not empty travellerDataCodeMap}">
									<c:forEach items="${travellerDataCodeMap}" var="entry">
										<li>${fn:length(entry.value)} x ${entry.key}</li>
									</c:forEach>
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
