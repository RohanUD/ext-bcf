<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty reservationDataList}">
	<c:forEach items="${reservationDataList.reservationDatas}" var="reservation" varStatus="reservationItemIdx">
		<c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
			<c:choose>
				<c:when test="${type eq 'OutBound'}">
					<c:choose>
						<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
							<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
							<c:set var="specificItem" value="${item}"></c:set>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
							<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
							<c:set var="specificItem" value="${item}"></c:set>
						</c:when>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:forEach>
</c:if>
<c:if test="${not empty specificItem}">
<div class="p-box">
	<div class="row">
		<div class="col-xs-10 p-header margin-top-20 ">
			<h4>
					<fmt:formatDate var="departureDate" pattern="EEE, MMM dd, yyyy" value="${specificItem.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
					<b><spring:theme code="${labelPrefix}" text="Departs" />:</b> <span class="font-weight-normal">${departureDate}</span>
			</h4>
		</div>
		<div class="col-xs-2 p-header text-right custom-accordion">
				<a>
					<i class="bcf bcf-icon-up-arrow" aria-hidden="true"></i>
				</a>
		</div>
	</div>
	<c:if test="${bcfFareFinderForm.readOnlyResults ne true}">
	    <ferryselection:transportreservation item="${specificItem}" type="${type}" />
	</c:if>
</div>
<hr class="margin-top-30 margin-bottom-20">
</c:if>
