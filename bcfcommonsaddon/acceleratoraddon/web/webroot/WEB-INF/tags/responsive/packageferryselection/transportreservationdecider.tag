<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ attribute name="item" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<%@ attribute name="journeyRefNo" required="false" type="java.lang.Integer"%>
<%@ attribute name="showRemoveOption" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<spring:htmlEscape defaultHtmlEscape="false" />
<div class="p-box margin-bottom-30">
	<c:choose>
		<c:when test="${type eq 'OutBound'}">
			<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
		</c:when>
		<c:when test="${type eq 'InBound'}">
			<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${reservable}">
			<div class="row margin-top-30">
				<div class="col-xs-10 p-header">
					<h4>
							<fmt:formatDate var="departureDate" pattern="EEE, MMM dd, yyyy" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
							<b><spring:theme code="${labelPrefix}" text="Departs"/>:</b> <span class="font-weight-normal">${departureDate}</span>
					</h4>
				</div>
				<div class="col-xs-2 p-header text-right custom-accordion">
						<a>
							<i class="bcf bcf-icon-up-arrow" aria-hidden="true"></i>
						</a>
				</div>
			</div>
			<ferryselection:transportreservationchange showRemoveOption="${showRemoveOption}" journeyRefNo="${journeyRefNo}" item="${item}" type="${type}" />
	        <c:if test="${isB2bCustomer || salesChannel=='CallCenter' || salesChannel=='TravelCentre'}">
                  <div class="row">
                   <div class="col-md-4 col-sm-6 col-xs-12">
                       <div class="form-group reference-code">
                          <p><spring:theme code="label.payment.reference.code" text="Add a booking reference code" />
						  	<a href="javascript:void(0)" data-container="body" data-toggle="popover"
								data-placement="bottom" data-html="true" title="" class="popoverThis" data-content="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
								<span class="bcf bcf-icon-info-solid"></span>
							</a>
						  </p>
                          <form:input type="text" labelKey="label.payment.reference.code" path="referenceCodes" value="${item.externalBookingReference}" class=" form-control" />
                          <form:input type="hidden" idKey="od-ref" path="bookingRefs" value="${item.bcfBookingReference}" />
                       </div>
                   </div>
               </div>
           </c:if>
		</c:when>
		<c:otherwise>
			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
						<h4><b><spring:theme code="${labelPrefix}" text="Departs" /> </b></h4>
				
				<ferryselection:transportreservationopenticket item="${item}" type="${type}" />
				</div>		
				
			</div>
		
			
		</c:otherwise>
	</c:choose>
</div>
<hr class="margin-top-30 margin-bottom-20">
