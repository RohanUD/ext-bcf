<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="packageDataList" required="true" type="com.bcf.facades.reservation.data.PackageReservationDataList"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<c:forEach items="${packageDataList.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
	<c:set var="roomStay" value="${packageReservation.accommodationReservationData.roomStays[0]}" />
	<fmt:formatDate value="${roomStay.checkInDate}" var="checkInDate" pattern="E, MMM dd" />
	<fmt:formatDate value="${roomStay.checkOutDate}" var="checkOutDate" pattern="E, MMM dd" />
	<c:set value="${packageReservation.reservationData.reservationItems[0]}" var="item" />
	<c:set value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings}" var="transportOffering" />
	<div class="row mb-5">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<span class="my-package-heading">
			<spring:theme code="text.booking.confirmation.sailing.detail.prefix" />
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-4 col-sm-offset-3">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
				<p class="mb-1">${checkInDate}</p>
				<p class="font-weight-bold mb-1">${transportOffering[0].sector.origin.location.name}</p>
				<p class="text-dark mb-0">(${transportOffering[0].sector.origin.name})</p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<span class="my-package-arrows-icon"></span>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
				<p class="mb-1">${checkOutDate}</p>
				<p class="font-weight-bold mb-1">${transportOffering[fn:length(transportOffering)-1].sector.destination.location.name}</p>
				<p class="text-dark mb-0">${transportOffering[fn:length(transportOffering)-1].sector.destination.name}</p>
			</div>
		</div>
	</div>
	<div class="row py-4">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="text-center bc-text-blue">
				<a class="show_hide_content" data-display="more-detail-vc-1" data-content="toggle-text">
					<span class="more-details hidden">More details</span>
					<span class="less-details">Less details</span>
					<i class="fa fa-angle-down bc-text-blue pl-2 pr-2"></i>
				</a>
			</div>
			<hr class="hr-payment">
			<div class="more-detail-vc-1">
				<c:forEach items="${packageReservation.reservationData.reservationItems}" var="item" varStatus="itemIdx">
					<c:choose>
						<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
							<ferryselection:transportreservationdecider item="${item}" type="OutBound" />
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
							<ferryselection:transportreservationdecider item="${item}" type="InBound" />
						</c:when>
					</c:choose>
				</c:forEach>

				<bcfglobalreservation:passengerReservation packageDataList="${packageDataList}" />

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <spring:theme code="label.ferry.farefinder.vehicle.heading" text="Vehicle" />
                    ${packageReservation.reservationData.reservationItems[0].reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas[0].vehicleTypeQuantity.vehicleType.name}
                </div>
            </div>
                <hr class="hr-payment">
			</div>
		</div>
	</div>
</c:forEach>
