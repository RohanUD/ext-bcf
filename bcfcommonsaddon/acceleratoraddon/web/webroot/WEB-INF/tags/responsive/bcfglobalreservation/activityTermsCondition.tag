<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="activityTermsConditionMap" required="true" type="java.util.Map"%>
<h4><spring:theme code="text.package.details.activity.policies" text="Activity policies" /></h4>
<c:forEach var="activityTermsConditionMapEntry" items="${activityTermsConditionMap}">
	<ul>
		<li>
			<p>${activityTermsConditionMapEntry.key}</p>
			<div class="row">${activityTermsConditionMapEntry.value}</div>
		</li>
	</ul>
</c:forEach>
