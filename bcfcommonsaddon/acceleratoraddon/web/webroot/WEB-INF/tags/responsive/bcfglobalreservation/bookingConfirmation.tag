<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="booking-confirm-box text-center">
    <div class="confirm-quarter">
        <i class="fas fa-check"></i>
	    <div class="confirm-text"><spring:theme code="checkout.multi.cart.validation.modal.OK" text="OK" /></div>
	  <h4>
		<strong><spring:theme code="text.booking.confirmation.detailed.text" text="Your booking has been confirmed." /></strong>
	  </h4>
	  <p>
		<spring:theme code="text.booking.confirmation.detailed.paragraph" text="A confirmation has been sent to your email address." />
	  </p>
    </div>
	<div>
		<c:if test="${globalReservationData.transportReservations.paymentProcessingMethod eq 'PINPAD'}">
			<c:if test="${showAdditionalMessages}">
				<c:if test="${not empty globalReservationData.transportReservations.operatorDisplayMessage}">
					<p>
						<spring:theme code="text.booking.confirmation.operator.display.msg" text="Operator display message" />
						: ${globalReservationData.transportReservations.operatorDisplayMessage}
					</p>
				</c:if>
				<c:if test="${not empty globalReservationData.transportReservations.approvalOrDeclineMessage}">
					<p>
						<spring:theme code="text.booking.confirmation.approval.or.decline.msg" text="Approval or decline message" />
						: ${globalReservationData.transportReservations.approvalOrDeclineMessage}
					</p>
				</c:if>
				<c:if test="${globalReservationData.transportReservations.signatureRequiredFlag}">
					<p>
						<spring:theme code="text.payment.print.pinpad.receipt.message" text="Please print receipt" />
					</p>
				</c:if>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<a href="${contextPath}/payment/receipt/order/${bookingReference}" class="btn btn-primary btn-block" id="print-receipt-link">
							<spring:theme code="text.booking.confirmation.print.receipt" text="Print receipt" />
						</a>
					</div>
				</div>
			</c:if>
		</c:if>
	</div>
</div>
