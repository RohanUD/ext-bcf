<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/util"%>
<%@ attribute name="activityReservation" required="true" type="com.bcf.facades.reservation.data.ActivityReservationData"%>
<%@ attribute name="groupedActivityReservation" required="true" type="java.util.List"%>

<div class="col-md-offset-1 y_activityReservationComponent">
	<p>
          <strong>${activityReservation.activityDetails.name}</strong>
    </p>

    <c:set var="clubbedPaxWithQuantity" value="" />
    <c:set var="entryNumbers" value="" />
    <c:forEach items="${groupedActivityReservation}" var="groupedActivityReservationItem" varStatus="idx">
        <c:set var="clubbedPaxWithQuantity" value="${clubbedPaxWithQuantity}${fn:escapeXml(groupedActivityReservationItem.quantity)}x&nbsp;${fn:escapeXml(groupedActivityReservationItem.guestType)}" />

        <c:if test="${not idx.last}">
            <c:set var="clubbedPaxWithQuantity" value="${clubbedPaxWithQuantity}, " />
        </c:if>
        <c:set var="entryNumbers" value="${entryNumbers}${groupedActivityReservationItem.entryNumber}" />

        <c:if test="${not idx.last}">
         <c:set var="entryNumbers" value="${entryNumbers}-" />
        </c:if>
    </c:forEach>

    <p><span>${clubbedPaxWithQuantity}</span></p>


    <fmt:parseDate value="${activityReservation.activityDate}" pattern="MMM dd yyyy" var="parsedDate" />
    <fmt:formatDate value="${parsedDate}" var="activityDateFormatted" pattern="EEEE, MMMM dd yyyy" />
	<p>
	    ${activityDateFormatted}
    </p>
	<p> <util:TwelveHourFormattedTime time="${activityReservation.activityTime}" /></p>

    <c:if test="${not empty activityReservation.totalFare.totalBaseExtrasPrice}">
        <p class="blue-color mt-2">
            <strong><u>${currencyFareLabel}</u></strong>
        </p>
    </c:if>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			<strong><format:price priceData="${activityReservation.totalFare.totalBaseExtrasPrice}" /></strong>
		</h4>
	</div>

        <div class="row margin-top-20 margin-bottom-20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <c:if test="${not empty reservation.totalFare}">
                        <p>
                            <spring:theme code="reservation.total.pay.label" text="Total" />
                        </p>
                        <Strong>${reservation.totalFare.totalPrice.formattedValue}</Strong>
                    </c:if>
            </div>
        </div>


</div>

