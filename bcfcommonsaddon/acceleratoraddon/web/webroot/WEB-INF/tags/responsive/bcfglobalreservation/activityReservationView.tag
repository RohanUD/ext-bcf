<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/util"%>
<%@ attribute name="activityReservation" required="true" type="com.bcf.facades.reservation.data.ActivityReservationData"%>
<%@ attribute name="groupedActivityReservation" required="true" type="java.util.List"%>
<%@ attribute name="showActionButtons" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showTotals" required="false" type="java.lang.Boolean"%>

<div class="y_activityReservationComponent">

	<p>
        <strong>${activityReservation.activityDetails.name}</strong>
	</p>

 <strong>Tickets:</strong>
 <c:set var="clubbedActivityPrice" value="${0}" />
 <c:set var="currencySymbol" value="${fn:substring(activityReservation.totalFare.basePrice.formattedValue,0,1)}" />
    <c:set var="clubbedPaxWithQuantity" value="" />
    <c:forEach items="${groupedActivityReservation}" var="groupedActivityReservationItem" varStatus="idx">
        <c:set var="priceBeforeTax" value="${groupedActivityReservationItem.totalFare.totalPrice.value - groupedActivityReservationItem.totalFare.taxPrice.value}" />
        <p><span>
           ${fn:escapeXml(groupedActivityReservationItem.quantity)}x&nbsp;${fn:escapeXml(groupedActivityReservationItem.guestType)}:&nbsp;${currencySymbol}${priceBeforeTax}
       </span></p>

        <c:set var="clubbedActivityPrice" value="${clubbedActivityPrice + priceBeforeTax}" />
    </c:forEach>

    <p>
        <strong>Date:</strong>
        <fmt:parseDate value="${activityReservation.activityDate}" pattern="MMM dd yyyy" var="parsedDate" />
        <fmt:formatDate value="${parsedDate}" var="activityDateFormatted" pattern="EEEE, MMMM dd yyyy" />${activityDateFormatted}
        <c:if test="${not empty activityReservation.activityTime}">
        :<util:TwelveHourFormattedTime time="${activityReservation.activityTime}" />
        </c:if>
    </p>

    <c:if test="${showTotals}">
        <p>Activities subtotal: ${fn:substring(activityReservation.totalFare.basePrice.formattedValue,0,1)}${clubbedActivityPrice}</p>
    </c:if>

    <c:if test="${showActionButtons}">
        <div class="row margin-bottom-30">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="fnt-14 margin-bottom-20">
                <c:set var="changeActivityUrlSuffix" value="?changeActivity=true&changeActivityCode=${activityReservation.activityDetails.code}&journeyRefNum=${activityReservation.journeyRefNumber}&date=${activityReservation.activityDate}&time=${activityReservation.activityTime}" />
                    <c:choose>
                        <c:when test="${isDealPackage}">
                            <c:url var="removeActivityUrl" value="/deal-activity-listing/remove-activity/${activityReservation.activityDetails.code}/${activityReservation.journeyRefNumber}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" />
                            <c:url var="changeActivityUrl" value="/deal-activity-listing/${changeActivityUrlSuffix}" />
                        </c:when>
                        <c:otherwise>
                            <c:url var="removeActivityUrl" value="/vacations/activities/remove-activity/${activityReservation.activityDetails.code}/${activityReservation.journeyRefNumber}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" />
                            <c:url var="changeActivityUrl" value="/vacations/activities/${activityReservation.activityDetails.code}/${changeActivityUrlSuffix}" />
                        </c:otherwise>
                    </c:choose>
                    <a href="${removeActivityUrl}"><spring:theme code="text.activity.reservation.remove" text="Remove" /></a> |
                    <a href="${changeActivityUrl}"><spring:theme code="text.activity.reservation.change" text="Change" /></a>
                </div>
            </div>
        </div>
    </c:if>
</div>
