<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ attribute name="globalReservationData" required="true" type="com.bcf.facades.reservation.data.BcfGlobalReservationData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="y_packageReservationComponent">
	<div class="row mt-2">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		     <h5><b><spring:theme code="text.package.details.package.bookingdetails.label" text="Booking Details" /></b></h5>
		</div>
	</div>



	<c:forEach items="${globalReservationData.packageReservations.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                <c:if test="${not empty orderNo}">
                    <h5><b><spring:theme code="text.page.mybooking.packageId" text="Package ID" /> ${orderNo}
                </c:if>

                <ul class="vacation-booking-box fnt-14">
                    <li><accommodationReservation:accommodationReservation accommodationReservation="${packageReservation.accommodationReservationData}" accommodationResVarStatus="1" />
                     <c:if test="${not empty globalReservationData.promotions}">
                        <p>
                           <strong><spring:theme code="text.accommodation.details.property.promotion" text="Promotion" /></strong> :
                            <ul>
                             <c:forEach var="promotion" items="${globalReservationData.promotions}" varStatus="count">
                                   <li>${fn:escapeXml(promotion)}</li>
                             </c:forEach>
                              </ul>
                        </p>
                     </c:if>
                    </li>
                <hr>
                    <li><bcfglobalreservation:transportReservation reservationData="${packageReservation.reservationData}" /></li>
                </ul>
            </div>
        </div>
		<c:if test="${not empty packageReservation.activityReservations}">
           <h5> <spring:theme code="text.package.details.activities.label" text="Activities" /></h5>
            <c:forEach items="${packageReservation.activityReservations.clubbedActivityReservationDatasByActivity}" var="activityReservationDataEntry">
                <bcfglobalreservation:activityReservationView activityReservation="${activityReservationDataEntry.value[0]}" groupedActivityReservation="${activityReservationDataEntry.value}" showActionButtons="false"/>
            </c:forEach>
		</c:if>
	</c:forEach>
</div>
