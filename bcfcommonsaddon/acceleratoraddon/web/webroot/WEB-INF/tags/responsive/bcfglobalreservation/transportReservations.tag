<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ attribute name="reservationDataList" required="true" type="com.bcf.facades.reservation.data.ReservationDataList"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<h4 class="margin-bottom-30">
	<c:choose>
		<c:when test="${isFerryOptionBooking}">
			<spring:theme code="label.optional.booking.reservation.summary.header" text="Tentative booking details" />
		</c:when>
		<c:otherwise>
		    <c:choose>
        		<c:when test="${confirmBooking}">
        		    <spring:theme code="text.booking.details.changes" text="Booking details with changes" />
        		</c:when>
                <c:otherwise>
                		<spring:theme code="label.payment.reservation.summary.header" text="Booking details" />

                </c:otherwise>
            </c:choose>
		</c:otherwise>
	</c:choose>
</h4>
<c:forEach items="${reservationDataList.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
	<transportreservation:transportReservation reservationData="${reservationData}" />
</c:forEach>
     	<c:if test="${fn:length(reservationDataList.reservationDatas) gt 1 or fn:length(reservationDataList.reservationDatas[0].reservationItems) gt 1 }">
         <div class="row margin-top-20 margin-bottom-20">
             <div class="col-lg-12 col-xs-12">
                 <c:url var="emptyBasketUrl" value="/view/ReservationTotalsComponentController/emptyBasket" />
                 <a href="javascript:;"  data-toggle="modal" data-target="#removeAllModal" >
                 <i class="fas fa-trash-alt"></i> &nbsp;
                 <spring:theme code="label.payment.reservation.sailing.entry.removeall" text="Remove all bookings" /></a>
             </div>
          </div>
        </c:if>

<div class="modal fade" tabindex="-1" role="dialog" id="removeAllModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
            </button>
            <h4 class="modal-title">
               <spring:theme code="label.payment.reservation.sailing.entry.removeall" text="Remove all bookings" />
            </h4>
         </div>
         <div class="modal-body">
            <p>
               <spring:theme code="label.payment.reservation.sailing.modal.body.entry.removeall" text="Are you sure you want to remove all bookings? This action cannot be undone." />
            </p>
         </div>
         <div class="modal-footer">
            <a href="${emptyBasketUrl}" class="btn btn-transparent">
               <spring:theme code="text.company.disable.confirm.yes" text="Yes" />
            </a>
            <button type="button" class="btn btn-transparent" data-dismiss="modal">
               <spring:theme code="text.company.disable.confirm.no" text="No" />
            </button>
         </div>
      </div>
   </div>
</div>
