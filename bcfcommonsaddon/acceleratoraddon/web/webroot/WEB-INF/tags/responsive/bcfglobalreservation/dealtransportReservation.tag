 <%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="reservation" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/transportreservation"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="y_transportReservationComponent">
	<c:if test="${not empty reservation}">
			<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && not empty reservation.reservationItems}">
				<div class="add-on-box mt-4 mb-4">
					<c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
						<div class="add-on-box mt-4 mb-4 row">
						<c:forEach var="reservationItem" items="${reservation.reservationItems}">
                         <div class="journey-wrapper add-on-box y_cartReservationComponent">
                          <bookingDetails:bookingitem reservationData="${reservation}" reservationItem="${reservationItem}" cssClass="panel panel-default my-account-secondary-panel" />
                        	</div>
                        	</c:forEach>
						<div class="row mt-4">
							<div class="col-sm-12">
								<ul class="col-sm-12 list-unstyled">
									<reservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
								</ul>
								<ul class="col-sm-12 list-unstyled">
									<reservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" />
								</ul>
								<ul class="col-sm-12 list-unstyled">
									<reservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.offerBreakdowns}" />
								</ul>
							</div>
							<div class="col-sm-6">
								<c:forEach var="travellerData" items="${item.reservationItinerary.travellers}">
									<c:if test="${travellerData.travellerType == 'PASSENGER'}">
										<c:forEach items="${item.reservationItinerary.originDestinationOptions[0].transportOfferings}" var="transportOffering">
											<c:set var="sectorOriginDestinationCode" value="${transportOffering.sector.origin.code} - ${transportOffering.sector.destination.code}" />
											<c:set var="showRouteCode" value="false" />
										</c:forEach>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</c:forEach>
				</div>
			</c:if>
	</c:if>
</div>
