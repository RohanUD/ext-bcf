<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="saveQuoteUrl" value="/quote/create" />
<c:if test="${not empty globalReservationData}">


	<c:if test="${not empty globalReservationData.transportReservations}">
        <c:forEach items="${globalReservationData.transportReservations.reservationDatas}" var="reservationData">
            <transportreservation:transportReservation reservationData="${reservationData}" />
        </c:forEach>

        <hr>
	</c:if>
	<c:if test="${not empty globalReservationData.accommodationReservations}">
		<bcfglobalreservation:accommodationReservations accommodationReservationDataList="${globalReservationData.accommodationReservations}" />
	</c:if>
     <c:if test="${not empty globalReservationData.promotions}">
        <p>
           <strong><spring:theme code="text.accommodation.details.property.promotion" text="Promotion" /></strong> :
             <c:forEach var="promotion" items="${globalReservationData.promotions}" varStatus="count">
                  ${promotion}
                  <c:if test="${count.index gt 1}">
                   ,
                  </c:if>
             </c:forEach>
        </p>
     </c:if>
	<c:if test="${not empty globalReservationData.activityReservations}">
	<c:forEach items="${globalReservationData.activityReservations.clubbedActivityReservationDatasByActivity}" var="activityReservationDataEntry">
    		<bcfglobalreservation:activityReservationView activityReservation="${activityReservationDataEntry.value[0]}" groupedActivityReservation="${activityReservationDataEntry.value}" />
    </c:forEach>
	</c:if>
	<c:if test="${not empty globalReservationData}">
		<bcfglobalreservation:totalFare globalReservationData="${globalReservationData}" />
	</c:if>


	<c:if test="${!isAmendment}">
        <div class="y_reservationsummary_modal hidden">

                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                    <div class="col-lg-5 col-md-5 col-sm-3 mt-3 pull-right">
                        <a href="#" id="y_loggedinuser_submit" class="btn btn-primary find-button  mb-4  col-sm-10">
                            <spring:theme code="label.packagedetails.link.savequote" text="Save quote" />
                        </a>
                    </div>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                <div class="row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="y_anonymoususer_save_quote mb-2 msg-success"></div>
                        <a href="#" id="y_anonymoususer_savequote" class="btn btn-primary">
                            <spring:theme code="label.packagedetails.link.savequote" text="Save quote" />
                        </a>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="y_anonymoususer_email_submit hidden">
                                <input type="text" id="y_anonymoususer_email" class="form-control mb-3" placeholder="Email address" autocomplete="off" />
                                <div class="y_anonymoususer_emailerror mb-2 msg-error"></div>
                                <a href="#" id="y_anonymoususer_submit" class="btn btn-primary">
                                    <spring:theme code="label.packagedetails.link.submitquote" text="Submit" />
                                </a>
                            </div>
                        </div>
                    </div>
                </sec:authorize>
        </div>
	</c:if>
</c:if>
