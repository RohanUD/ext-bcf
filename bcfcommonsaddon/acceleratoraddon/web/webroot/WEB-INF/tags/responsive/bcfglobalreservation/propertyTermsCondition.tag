<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="propertyTermsConditionMap" required="true" type="java.util.Map"%>
<hr/>
<div class="row">
<div class="col-lg-10 col-md-10 col-md-offset-1">
<h4 class="vacation-h2 mt-5 mb-5"><spring:theme code="text.accommodation.details.hotel.policies" text="Hotel policies" /></h4>
<c:forEach var="propertyTermsConditionMapEntry" items="${propertyTermsConditionMap}">
	<ul>
		<li>
			<p>${propertyTermsConditionMapEntry.key}</p>
			<div class="row">${propertyTermsConditionMapEntry.value}</div>
		</li>
	</ul>
</c:forEach>
</div>
</div>
