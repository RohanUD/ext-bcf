<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ attribute name="reservationData" required="true"
	type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>

<h5><b><spring:theme code="text.package.details.package.sailingdetails.label" text="Package"/></b></h5>

<div class="y_transportReservationComponent">

	<p>
		<c:set var="departureTransportOfferings"
			value="${reservationData.reservationItems[0].reservationItinerary.originDestinationOptions[0].transportOfferings}" />
		<c:set var="returnTransportOfferings"
			value="${reservationData.reservationItems[1].reservationItinerary.originDestinationOptions[0].transportOfferings}" />
		<c:set var="departureTO" value="${departureTransportOfferings[0]}" />
		<c:set var="returnTO" value="${returnTransportOfferings[0]}" />
		<strong><spring:theme code="text.route.label" text="Route" />:</strong> ${departureTO.sector.origin.name} to
		${departureTransportOfferings[fn:length(departureTransportOfferings)-1].sector.destination.name}
	</p>
    <input id="ebookingId" name="ebookingId" type="hidden" value="${reservationData.reservationItems[0].bcfBookingReference}"/>
	<c:if test="${reservationData.defaultSailing eq false}">


        <p>
            <strong><spring:theme
                    code="text.transport.details.sailing.departure"
                    text="Departure Sailing" />:</strong>
                <c:choose>
                    <c:when test="${!reservationData.reservationItems[0].openTicketSelected}">
                        <fmt:formatDate value="${departureTO.departureTime}"
                                        var="departureSailing" pattern="MMM dd, yyyy 'at' h:mm a" />
                          ${departureSailing}, <a href="/ship-info/${fn:escapeXml(departureTO.transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal" class="sailing-ferry-name">${departureTO.transportVehicle.vehicleInfo.name}</a> <span class="booking-reference-number">${reservationData.reservationItems[0].bcfBookingReference}</span>
                    </c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${departureTO.departureTime}"
                                        var="departureSailing" pattern="MMM dd, yyyy" />
                         <spring:theme code="label.open.ticket" text="Open Ticket" /> ${departureSailing}
                    </c:otherwise>
                </c:choose>
                <c:if test="${asmLoggedIn}">
                    <c:choose>
                        <c:when test="${northernRoute}">
                            <li><bookingDetails:asmNorthernTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                        </c:when>
                        <c:otherwise>
                            <li><bookingDetails:asmTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                        </c:otherwise>
                    </c:choose>
                </c:if>
        </p>
        <c:if test="${not empty returnTO}">
            <p>
                <strong><spring:theme
                        code="text.transport.details.sailing.return" text="Return Sailing" />:</strong>
                 <c:choose>
                    <c:when test="${!reservationData.reservationItems[1].openTicketSelected}">
                    <fmt:formatDate value="${returnTO.departureTime}" var="returnSailing"
                     pattern="MMM dd, yyyy 'at' h:mm a" />
                      ${returnSailing}, <a href="/ship-info/${fn:escapeXml(returnTO.transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal" class="sailing-ferry-name">${returnTO.transportVehicle.vehicleInfo.name}</a>  <span class="booking-reference-number">${reservationData.reservationItems[1].bcfBookingReference}</span>
                    </c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${returnTO.departureTime}" var="returnSailing"
                                            pattern="MMM dd, yyyy" />
                                         <spring:theme code="label.open.ticket" text="Open Ticket" /> ${returnSailing}
                    </c:otherwise>
                </c:choose>
                <c:if test="${asmLoggedIn}">
                    <c:choose>
                        <c:when test="${northernRoute}">
                            <li><bookingDetails:asmNorthernTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                        </c:when>
                        <c:otherwise>
                            <li><bookingDetails:asmTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </p>
        </c:if>
	</c:if>
    <p>
        <strong><spring:theme
                code="label.ferry.farefinder.vehicle.heading" text="Vehicle" />:</strong>
        <c:choose>
            <c:when test="${reservationData.defaultSailing eq true}">
                ${vehicleName}
            </c:when>
            <c:otherwise>
                ${reservationData.reservationItems[0].reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas[0].vehicleTypeQuantity.vehicleType.name}
            </c:otherwise>
        </c:choose>
    </p>
	<p>
		<c:set var="travellerCount" value="0" />
		<c:forEach var="traveller"
			items="${reservationData.reservationItems[0].reservationItinerary.travellers}">
			<c:if test="${traveller.travellerType eq 'PASSENGER'}">
				<c:set var="travellerCount" value="${travellerCount+1}" />
			</c:if>

		</c:forEach>

		<strong><spring:theme
				code="label.vacation.travelfinder.farefinder.passengers.heading"
				text="Ferry passenger(s)" />:</strong> ${travellerCount}
    </p>

</div>
