<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ attribute name="packageDataList" required="true" type="com.bcf.facades.reservation.data.PackageReservationDataList"%>
<%@ attribute name="showProgressBar" required="false" type="java.lang.Boolean"%>
<%@
taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<c:if test="${showProgressBar ne false}">
    <progress:travelBookingProgressBar stage="mypackage" amend="${amend}" bookingJourney="${bookingJourney}" />
</c:if>

<bcfglobalreservation:totalTaxFare />
${globalReservationData.totalFare}
<div class="container">
	<c:forEach items="${packageDataList.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
		<div class="y_packageReservationComponent">
			<div class="row margin-bottom-30 margin-top-20">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="my-package-heading margin-top-20">
						<spring:theme code="text.page.package.mybookings" />
					</div>
				</div>
			</div>
			<bcfglobalreservation:sailingReservation packageDataList="${packageDataList}" />
			<c:if test="${not empty packageReservation.accommodationReservationData}">
				<bcfglobalreservation:hotelReservation packageDataList="${packageDataList}" />
			</c:if>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="text-center bc-text-blue">
						<a class="show_hide_content" data-display="more-detail-vc-2" data-content="toggle-text">
							<span class="more-details hidden">More details</span>
							<span class="less-details">Less details</span>
							<i class="fa fa-angle-down bc-text-blue pl-2 pr-2"></i>
						</a>
					</div>
					<div class="more-detail-vc-2">

						<hr class="hr-payment">
							<c:forEach items="${packageDataList.packageReservationDatas}" var="packageReservation2" varStatus="packageReservation2ItemIdx">
								<c:set var="roomStays" value="${packageReservation2.accommodationReservationData.roomStays}" />
								<bcfglobalreservation:roomReservation roomStays="${roomStays}"/>
							</c:forEach>
					</div>
				</div>
			</div>
			<hr class="hr-payment">
			<div class="row margin-bottom-20 margin-top-20">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span class="my-package-heading">
						<spring:theme code="text.activities.label" />
					</span>
				</div>
			</div>
			<c:if test="${not empty globalReservationDataList.activityReservations}">
				<bcfglobalreservation:activityReservations activityReservationDataList="${globalReservationDataList.activityReservations}" />
			</c:if>
			<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
						<c:if test="${not empty packageReservation.activityReservations}">
							<bcfglobalreservation:activityReservations activityReservationDataList="${packageReservation.activityReservations}" isDealPackage="true"/>
						</c:if>
						<c:choose>
							<c:when test="${isAmendment}">
								<spring:theme var="addActivity" code="text.activityListing.page.manageActivityButton" text="Manage Activity" />
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${empty packageReservation.activityReservations}">
										<spring:theme var="addActivity" code="text.activityListing.page.addActivityButton" text="Add Activity" />
									</c:when>
									<c:otherwise>
										<spring:theme var="addActivity" code="text.activitiesListing.page.addAnotherActivityButton" text="Add Another Activity" />
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
						<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
						<c:set var="activityUrl" value="/deal-activity-listing" />
						<a href="${activityUrl}" class="btn btn-outline-primary btn-block margin-bottom-20 y_addRequestButton"> ${addActivity}</a>
						</div>
						</div>
					</div>
			</div>
	</c:forEach>
	<hr class="hr-payment">
</div>
