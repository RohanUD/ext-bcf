<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/util"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="alacarteBooking" required="false" type="java.lang.Boolean"%>
<%@ attribute name="isDealPackage" required="false" type="java.lang.Boolean"%>
<%@ attribute name="activityReservation" required="true" type="com.bcf.facades.reservation.data.ActivityReservationData"%>
<%@ attribute name="groupedActivityReservation" required="true" type="java.util.List"%>
<%@ attribute name="actionType" required="false" type="java.lang.String"%>
<div class="container">
<div class="row">
<div class="y_activityReservationComponent">
	<p>
          <strong>${activityReservation.activityDetails.name}</strong>
    </p>


    <c:if test="${showAvailabilityStatus}">
        <p>
           <strong><spring:theme code="text.accommodation.availability.status" text="Availability Status:" /></strong>&nbsp;${activityReservation.availabilityStatus}
        </p>
    </c:if>


     <c:if test="${not empty activityReservation.promotions}">
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="padding-top:5px">
             <spring:theme code="text.accommodation.details.property.promotion" text="Promotion" /> :
             <ul>
                 <c:forEach var="promotion" items="${activityReservation.promotions}">
                     <li>${fn:escapeXml(promotion)}</li>
                 </c:forEach>
             </ul>
         </div>
     </c:if>

    <c:set var="clubbedActivityPrice" value="${0}" />
    <c:set var="currencySymbol" value="${fn:substring(activityReservation.totalFare.basePrice.formattedValue,0,1)}" />
    <c:set var="entryNumbers" value="" />
    <c:forEach items="${groupedActivityReservation}" var="groupedActivityReservationItem" varStatus="idx">
        <c:set var="priceBeforeTax" value="${groupedActivityReservationItem.totalFare.totalPrice.value - groupedActivityReservationItem.totalFare.taxPrice.value}" />
        <p><span>
            ${fn:escapeXml(groupedActivityReservationItem.quantity)}x&nbsp;${fn:escapeXml(groupedActivityReservationItem.guestType)}:&nbsp;${currencySymbol}${priceBeforeTax}
        </span></p>
        <c:set var="clubbedActivityPrice" value="${clubbedActivityPrice + priceBeforeTax}" />
        <c:set var="entryNumbers" value="${entryNumbers}${groupedActivityReservationItem.entryNumber}" />

        <c:if test="${not idx.last}">
         <c:set var="entryNumbers" value="${entryNumbers}-" />
        </c:if>
    </c:forEach>
<p>Total: ${currencySymbol}${clubbedActivityPrice}</p>

    <c:if test="${alacarteBooking}">
        <div class="row margin-bottom-30">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="fnt-14 margin-bottom-20">
                    <c:set var="alacarteChangeActivityUrlSuffix" value="?changeActivity=true&changeActivityRef=${entryNumbers}&date=${activityReservation.activityDate}&time=${activityReservation.activityTime}" />
                    <c:url var="removeActivityUrl" value="/vacations/activities/remove-activity/${entryNumbers}"/>
                    <c:url var="changeActivityUrl" value="/vacations/activities/${activityReservation.activityDetails.code}${alacarteChangeActivityUrlSuffix}" />

                    <a href="${removeActivityUrl}"><spring:theme code="text.activity.reservation.remove" text="Remove" /></a> |
                    <a href="${changeActivityUrl}"><spring:theme code="text.activity.reservation.change" text="Change" /></a>

                 </div>
             </div>

        </div>

    </c:if>


    <fmt:parseDate value="${activityReservation.activityDate}" pattern="MMM dd yyyy" var="parsedDate" />
    <fmt:formatDate value="${parsedDate}" var="activityDateFormatted" pattern="EEEE, MMMM dd yyyy" />
	<p>
	    ${activityDateFormatted}
    </p>
	<p> <util:TwelveHourFormattedTime time="${activityReservation.activityTime}" /></p>

    <c:if test="${not empty activityReservation.totalFare.totalBaseExtrasPrice}">
        <p class="blue-color mt-2">
            <strong><u>${currencyFareLabel}</u></strong>
        </p>
    </c:if>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			<strong><format:price priceData="${activityReservation.totalFare.totalBaseExtrasPrice}" /></strong>
		</h4>
	</div>

        <div class="row margin-top-20 margin-bottom-20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <c:if test="${not empty reservation.totalFare}">
                        <p>
                            <spring:theme code="reservation.total.pay.label" text="Total" />
                        </p>
                        <Strong>${reservation.totalFare.totalPrice.formattedValue}</Strong>
                    </c:if>
            </div>
        </div>




    <c:if test="${!alacarteBooking}">

        <c:if test="${empty bookingReference}">
            <div class="row margin-bottom-30">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="fnt-14 margin-bottom-20">
                    <c:set var="changeActivityUrlSuffix" value="?changeActivity=true&changeActivityCode=${activityReservation.activityDetails.code}&journeyRefNum=${activityReservation.journeyRefNumber}&date=${activityReservation.activityDate}&time=${activityReservation.activityTime}" />
                        <c:choose>
                            <c:when test="${isDealPackage}">
                                <c:url var="removeActivityUrl" value="/deal-activity-listing/remove-activity/${activityReservation.activityDetails.code}/${activityReservation.journeyRefNumber}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" />
                                <c:url var="changeActivityUrl" value="/deal-activity-listing/${changeActivityUrlSuffix}" />
                            </c:when>
                            <c:otherwise>
                                <c:url var="removeActivityUrl" value="/vacations/activities/remove-activity/${activityReservation.activityDetails.code}/${activityReservation.journeyRefNumber}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" />
                                <c:url var="changeActivityUrl" value="/vacations/activities/${activityReservation.activityDetails.code}/${changeActivityUrlSuffix}" />
                            </c:otherwise>
                        </c:choose>
                        <a href="${removeActivityUrl}"><spring:theme code="text.activity.reservation.remove" text="Remove" /></a> |
                        <a href="${changeActivityUrl}"><spring:theme code="text.activity.reservation.change" text="Change" /></a>
                    </div>
                </div>
            </div>



        </c:if>

        <c:if test="${actionType == 'EDIT_BOOKING'}">
            <commonsBookingDetails:globalBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_STANDALONE_ACTIVITY" activityReservation="${activityReservation}"/>
            <commonsBookingDetails:globalBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="CANCEL_STANDALONE_ACTIVITY" activityReservation="${activityReservation}"/>
        </c:if>
     </c:if>


    <c:if test="${showSupplierComments}">
         <c:url value="/view/AsmCommentsComponentController/activity/add-supplier-comment" var="getSupplierComments" />

         <a class="add-supplier-comments" href="${fn:escapeXml(getSupplierComments)}" data-code="${activityReservation.activityDetails.code}" data-type="activity">
            <spring:theme code="label.asm.add.supplier.comments" text="Add Supplier Comment" />
         </a>
    </c:if>



</div>

</div>
</div>