<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="globalReservationData" required="true" type="com.bcf.facades.reservation.data.BcfGlobalReservationData"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<c:choose>
	<c:when test="${globalReservationData.bookingJourneyType eq 'BOOKING_TRANSPORT_ONLY'}">
		<c:set var="totalTripCost">
			<spring:theme code="text.payment.reservation.transport.total.trip.cost" text="Total trip costs" />
		</c:set>
		<c:set var="totalTaxPrice">
			<spring:theme code="text.payment.reservation.transport.total.trip.tax" text="Total Tax" />
		</c:set>
		<c:set var="totalDueNow">
			<spring:theme code="text.payment.reservation.transport.total.due.now" text="Total due now" />
		</c:set>
		<c:set var="feesIncluded">
            <spring:theme code="text.ferry.fees.included" text="Included in total price" />
        </c:set>
	</c:when>
	<c:otherwise>
		<c:set var="totalTripCost">
			<spring:theme code="text.payment.reservation.transport.total.trip.cost.booking.package" text="Total trip costs" />
		</c:set>
		<c:set var="totalTaxPrice">
			<spring:theme code="text.payment.reservation.transport.total.trip.tax.booking.package" text="Taxes and fees" />
            <a href="#" data-container="body" data-toggle="popover" class="popoverThis pull-right" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.checkout.additionalinfo.totalcost"/>">
            <span class="bcf bcf-icon-info-solid"></span></a>
		</c:set>
		<c:set var="totalDueNow">
			<spring:theme code="text.payment.reservation.transport.total.due.now.booking.package" text="Total due now" />
		</c:set>
		<c:set var="totalTripSubtotal">
			<spring:theme code="text.payment.reservation.transport.total.trip.subtotal.booking.package" text="Package price" />
		</c:set>
		<c:set var="feesIncluded">
            <spring:theme code="text.vacation.fees.included" text="Included in package price" />
        </c:set>
	</c:otherwise>
</c:choose>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
	    <ul class="list-unstyled payment payment-confirmation-list">
	    <c:choose>
        	<c:when test="${globalReservationData.bookingJourneyType eq 'BOOKING_TRANSPORT_ONLY'}">

            <li class="light margin-top-20">
               ${totalTripCost}
               <a href="#" data-container="body" data-toggle="popover" class="popoverThis pull-right" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.checkout.additionalinfo.totalcost"/>">
                   <span class="bcf bcf-icon-info-solid"></span></a>

                <span><format:price priceData="${globalReservationData.totalFare.totalPrice}" /></span>
            </li>
            </c:when>
            	<c:otherwise>

                <hr>
                <li>
                   ${totalTripSubtotal}
                    <span><format:price priceData="${globalReservationData.totalFare.subTotalPrice}" /></span>
                </li>

            </c:otherwise>
            </c:choose>

             <c:if test="${not empty globalReservationData.totalFare.taxPrice and globalReservationData.totalFare.taxPrice.value gt 0}">
                 <li>
                    ${totalTaxPrice}
                    <span><format:price priceData="${globalReservationData.totalFare.taxPrice}" /></span>
                </li>
            </c:if>

            <c:if test="${not empty globalReservationData.vacationFees}">
                <c:forEach var="vacationFee" items="${globalReservationData.vacationFees}">
                    <li class="p-2">
                        ${vacationFee.feeType} ${feesIncluded}
                        <span><format:price priceData="${vacationFee.vacationFee}" /></span>
                    </li>
                </c:forEach>
            </c:if>
                <c:set var="showPayAtTerminal" value="true" />
	            <c:if test="${globalReservationData.previouslyPaid.totalPrice.value gt 0}">
	                   <li class="yellow">
	                        <strong>
	                        <c:choose>
                                <c:when test="${globalReservationData.amendBooking}">
                                    <spring:theme code="text.payment.reservation.transport.previously.paid" text="Previously paid" />
                                </c:when>
                                <c:otherwise>
                                    <spring:theme code="text.payment.reservation.transport.total.paid" text="Total paid" />
                                </c:otherwise>
	                        </c:choose>
	                        <c:if test="${isOnRequestBooking}">
	                            <spring:theme code="text.onrequest.payment.pending" text="(Payment not processed)" />
	                        </c:if>
	                        </strong>
	                        <span><format:price priceData="${globalReservationData.previouslyPaid.totalPrice}" /></span>
	                    </li>
	            </c:if>
	            <c:if test="${not empty globalReservationData.depositAmount}">
                       <li class="yellow">
                            <strong><spring:theme code="text.asm.deposit.paid" text="Deposit paid" />
                            <c:if test="${isOnRequestBooking}">
                                <spring:theme code="text.onrequest.payment.pending" text="(Payment not processed)" />
                            </c:if>
                            </strong>
                            <span>-<format:price priceData="${globalReservationData.depositAmount}" /></span>
                        </li>
                </c:if>


                 <c:if test="${not empty globalReservationData.totalFare.goodWillRefund  &&  globalReservationData.totalFare.goodWillRefund.value gt 0}">
                    <li class="light">
                        <strong><spring:theme code="label.goodwill.refund.amount" text="GoodWill Refund" /></strong>
                        <span><strong><format:price priceData="${globalReservationData.totalFare.goodWillRefund}" /></strong></span>

                    </li>
                </c:if>

                <c:if test="${globalReservationData.amountToPay.value > 0.0 or globalReservationData.amountToPay.value lt 0}">
	                <li class="light">
	                    <c:choose>
	                        <c:when test="${globalReservationData.amountToPay.value > 0.0}">
	                            <strong>${totalDueNow}</strong>
	                            <span><strong><format:price priceData="${globalReservationData.amountToPay}" /></strong></span>
	                        </c:when>
	                        <c:when test="${globalReservationData.amountToPay.value lt 0}">
	                            <c:set var="showPayAtTerminal" value="false" />
	                            <strong><spring:theme code="label.refund.amount" text="Refund" /></strong>
	                            <span><strong><format:price priceData="${globalReservationData.amountToPay}" /></strong></span>
	                        </c:when>
	                    </c:choose>
	                </li>
				</c:if>
                <c:if test="${not empty globalReservationData.payAtTerminal and globalReservationData.payAtTerminal.value gt 0 and showPayAtTerminal}">
	                <li>
	                    <spring:theme code="text.payment.reservation.transport.total.pay.at.terminal" text="Pay at the terminal" />
	                    <span><format:price priceData="${globalReservationData.payAtTerminal}" /></span>
	                </li>
                </c:if>
        </ul>
	</div>
</div>
