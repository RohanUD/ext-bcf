<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>
<cart:cartTimer />
<c:choose>
	<c:when test="${availabilityStatus eq 'ON_REQUEST'}">
		<div class="row total-price-section">
			<div class="col-xs-6 text-center br-1">
				<p class="m-0">
					<b><spring:theme code="text.package.booking.on.request.title" text="Booking is on request" /></b>
				</p>
				<p class="m-0 fnt-14">
					<spring:theme code="text.package.booking.on.request.ccard.message" />
				</p>
				<input type="hidden" name="availabilityStatus" id="availabilityStatus" value="${availabilityStatus}" />
			</div>
			<div class="col-xs-6 ">
				<div class="p-chart text-center">${globalReservationDataList.totalFare.subTotalPrice.formattedValue}
					<span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.total" text="Total" /> &nbsp;<strong>${totalFare.subTotalPrice.formattedValue}</strong></span> + ${totalFare.taxPrice.formattedValue}&nbsp;
					<spring:theme code="label.reservation.summary.taxesandfees" text="taxes and fees" />
					<bcfglobalreservation:bcfGlobalReservationModalLink />
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="row total-price-section">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="p-chart text-center">
					<span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.total" text="Total" /> &nbsp;<strong>${globalReservationDataList.totalFare.subTotalPrice.formattedValue}</strong></span><span class="fnt-14"> +
						${globalReservationDataList.totalFare.taxPrice.formattedValue}&nbsp; <spring:theme code="label.reservation.summary.taxesandfees" text="taxes and fees" /><span> <span class="vehicle-view-details"><bcfglobalreservation:bcfGlobalReservationModalLink /></span>
				</div>
				<c:if test="${isAmendment}">
					<div class="p-chart text-center">
						<span class="total-price-text"><spring:theme code="text.cms.accommodationsummary.previouspaid.total" text="Previous Paid Total" /> &nbsp;<strong>${globalReservationDataList.previouslyPaid.totalPrice.value}</strong></span> + ${globalReservationDataList.previouslyPaid.taxPrice.value}&nbsp;
					</div>
				</c:if>
			</div>
		</div>
	</c:otherwise>
</c:choose>
