
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring"
uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c"
uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme"
tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format"
tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@
attribute name="packageDataList" required="true"
type="com.bcf.facades.reservation.data.PackageReservationDataList"%>
<%@
taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib
prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach items="${packageDataList.packageReservationDatas}"
	var="packageReservation" varStatus="packageReservationItemIdx">
	<c:set
		value="${packageReservation.reservationData.reservationItems[0]}"
		var="item" />
	<div class="row mb-3 mt-5">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<span class="my-package-heading">
				<spring:theme
					code="label.ferry.farefinder.passengerinfo.base.passengers.heading" />
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<c:forEach items="${item.reservationItinerary.travellers}"
				var="traveller" varStatus="travellerCount">
				<c:if test="${traveller.travellerType eq 'PASSENGER' }">
					<p class="mb-1 fnt-14"></p>

					<c:if
						test="${traveller.travellerInfo.passengerType.code eq 'adult'}">
						<c:set var="padultcount" value="${padultcount+1}" />
						<c:set var="padult"
							value="${padultcount} x ${traveller.travellerInfo.passengerType.code} ${traveller.travellerInfo.passengerType.name} "
							scope="page" />
					</c:if>
					<c:if
						test="${traveller.travellerInfo.passengerType.code eq 'child'}">
						<c:set var="pchildcount" value="${pchildcount+1}" />
						<c:set var="pchild"
							value="${pchildcount} x ${traveller.travellerInfo.passengerType.code} ${traveller.travellerInfo.passengerType.name} "
							scope="page" />

					</c:if>
					<c:if
						test="${traveller.travellerInfo.passengerType.code eq 'infant'}">
						<c:set var="pinfantcount" value="${pinfantcount+1}" />
						<c:set var="pinfant"
							value="${pinfantcount} x ${traveller.travellerInfo.passengerType.code} ${traveller.travellerInfo.passengerType.name} "
							scope="page" />
					</c:if>
					<c:forEach
						items="${traveller.specialRequestDetail.specialServiceRequests}"
						var="specialServiceRequests">
						${specialServiceRequests.name}
					</c:forEach>
				</c:if>
			</c:forEach>
		</div>
	</div>
<p class="fnt-14">
	<c:if test="${not empty padult}">
		${padult}
		<br>
	</c:if>
	<c:if test="${not empty pchild}">
		${pchild}
		<br>
	</c:if>
	<c:if test="${not empty pinfant}">
		${pinfant}
		<br>
	</c:if>
	</p>
</c:forEach>
<hr class="hr-payment">
