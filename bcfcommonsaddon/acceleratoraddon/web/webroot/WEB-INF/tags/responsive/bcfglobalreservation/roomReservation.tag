<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="roomStays" required="true" type="java.util.List"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>

<c:forEach var="roomStay" items="${roomStays}">
<div class="row mb-3">
    <bcfglobalreservation:guestReservation roomStay="${roomStay}" />
</div>
	<c:forEach var="roomType" items="${roomStay.roomTypes}">
		<div class="row mb-3">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">${fn:escapeXml(roomType.name)}</div>

			 <c:if test="${showSupplierComments}">
                <br>
                <div class="row">
                    <div class="col-lg-12 text-center">

                        <c:url value="/view/AsmCommentsComponentController/accommodation/add-supplier-comment" var="getSupplierComments" />
                        <a class="add-supplier-comments" href="${fn:escapeXml(getSupplierComments)}"  data-code="${roomStay.accommodationCode}" data-ref="${roomStay.roomStayRefNumber}" data-type="accommodation">
                            <spring:theme code="label.asm.add.supplier.comments" text="Add Supplier Comment" />
                        </a>
                    </div>
                </div>

             </c:if>



		    <c:if test="${not empty roomStay.promotions}">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="padding-top:5px">
                   <spring:theme code="text.accommodation.details.property.promotion" text="Promotion" /> :
                    <ul>
                        <c:forEach var="promotion" items="${roomStay.promotions}">
                            <li>${fn:escapeXml(promotion)}</li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

		</div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="accommodation-details available-room-img">
                        <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(roomType.images)}">
                                <c:forEach items="${roomStay.roomTypes[0].images}" var="accommodationImage">
                                    <div class="image-img">
                                        <img class="js-responsive-image" data-media='${accommodationImage.url}' alt='${accommodationImage.altText}' title='${accommodationImage.altText}'>
                                        <div class="slider-count">
                                            <div class="slider-img-thumb"></div>
                                            <div class="slider-num">1/4</div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <ul class="small clearfix y_features">
                        <li>${roomStay.roomTypes[0].description}</li>
                        <li>${roomStay.roomTypes[0].occupancies[0].quantityMax} &nbsp;<spring:theme code="text.accommodation.details.roomstay.features.maxOccupancy"/></li>
                        <c:if test="${not empty roomStay.roomTypes[0].sizeMeasurement}">
                            <li>${fn:escapeXml(roomStay.roomTypes[0].sizeMeasurement)} &nbsp;<spring:theme code="text.accommodation.details.roomstay.features.sizeMeasurement"/></li>
                        </c:if>
                        <c:if test="${not empty roomType.facilities}">
                                <c:forEach var="facility" items="${roomStay.roomTypes[0].facilities}">
                                    <li>${fn:escapeXml(facility.shortDescription)}</li>
                                </c:forEach>
                            </ul>
                            <a href="#" class="more hidden">
                                <spring:theme code="text.accommodation.details.roomstay.features.more" />
                            </a>
                            <a href="#" class="less hidden">
                                <spring:theme code="text.accommodation.details.roomstay.features.less" />
                            </a>
                        </c:if>
                    </div>
                </div>


	</c:forEach>
</c:forEach>
