<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ attribute name="accommodationReservationDataList" required="true" type="com.bcf.facades.reservation.data.AccommodationReservationDataList"%>
<c:forEach items="${accommodationReservationDataList.accommodationReservations}" var="accommodationReservationData" varStatus="accommodationResVarStatus">
    <accommodationReservation:accommodationReservation accommodationReservation="${accommodationReservationData}" accommodationResVarStatus="${accommodationResVarStatus.count}"/>
</c:forEach>
