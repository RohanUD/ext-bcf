<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="saveQuoteUrl" value="/quote/create" />
<c:if test="${not empty globalReservationData}">
	<c:if test="${not empty globalReservationData.transportReservations}">
		<c:choose>
			<c:when test="${cmsPage.uid == 'paymentMethodPage' or isFerryOptionBooking}">
				<bcfglobalreservation:transportReservations reservationDataList="${globalReservationData.transportReservations}" />
			</c:when>
			<c:otherwise>
				<c:forEach items="${globalReservationData.transportReservations.reservationDatas}" var="reservationData">
					<transportreservation:transportReservation reservationData="${reservationData}" />
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${not empty globalReservationData.accommodationReservations}">
		<bcfglobalreservation:accommodationReservations accommodationReservationDataList="${globalReservationData.accommodationReservations}" />
	</c:if>
	<c:if test="${not empty globalReservationData.activityReservations}">
	<c:forEach items="${globalReservationData.activityReservations.clubbedActivityReservationDatasByActivity}" var="activityReservationDataEntry">
		<bcfglobalreservation:activityReservationView activityReservation="${activityReservationDataEntry.value[0]}" groupedActivityReservation="${activityReservationDataEntry.value}"/>
	</c:forEach>
	</c:if>
	<c:if test="${not empty globalReservationData.packageReservations}">
		<bcfglobalreservation:packageReservations globalReservationData="${globalReservationData}" />
	</c:if>


	<c:if test="${not empty orderNo}">
        <p>
    		<strong><spring:theme
    				code="label.vacation.travelfinder.farefinder.booking.number"
    				text="Booking Number" /></strong> : ${orderNo}
    	</p>
    	</c:if>

	<c:if test="${not empty globalReservationData}">
		<bcfglobalreservation:totalFare globalReservationData="${globalReservationData}" />
	</c:if>
	<div class="y_reservationsummary_modal hidden">

	    <c:if test="${not empty globalReservationData.packageReservations}">
	        <c:set var="isDefaultSailingPresent" value="false" />
            <c:forEach items="${globalReservationData.packageReservations.packageReservationDatas}" var="packageReservationData" varStatus="packageReservationDataStatus">
                <c:if test="${not empty packageReservationData.reservationData  and packageReservationData.reservationData.defaultSailing}">
                    <c:set var="isDefaultSailingPresent" value="true" />
               </c:if>
            </c:forEach>

            <c:if test="${!isDefaultSailingPresent}">
                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                    <div class="col-sm-5 mt-3 pull-right">
                        <a href="#" id="y_loggedinuser_submit" class="btn btn-primary find-button  mb-4  col-sm-10">
                            <spring:theme code="label.packagedetails.link.savequote" text="Save quote" />
                        </a>
                    </div>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                <div class="row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="y_anonymoususer_save_quote mb-2 msg-success"></div>
                        <a href="#" id="y_anonymoususer_savequote" class="btn btn-primary">
                            <spring:theme code="label.packagedetails.link.savequote" text="Save quote" />
                        </a>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="y_anonymoususer_email_submit hidden">
                                <input type="text" id="y_anonymoususer_email" class="form-control mb-3" placeholder="Email address" autocomplete="off" />
                                <div class="y_anonymoususer_emailerror mb-2 msg-error"></div>
                                <a href="#" id="y_anonymoususer_submit" class="btn btn-primary">
                                    <spring:theme code="label.packagedetails.link.submitquote" text="Submit" />
                                </a>
                            </div>
                        </div>
                    </div>
                </sec:authorize>
            </c:if>
		</c:if>
	</div>
</c:if>
