<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="activityReservationDataList" required="true" type="com.bcf.facades.reservation.data.ActivityReservationDataList"%>
<%@ attribute name="alacarteBooking" required="false" type="java.lang.Boolean"%>
<%@ attribute name="isDealPackage" required="false" type="java.lang.Boolean"%>
<%@ attribute name="actionType" required="false" type="java.lang.String"%>

<c:if test="${not empty activityReservationDataList.clubbedActivityReservationDatasByActivity}">
	<div class="container">
	<div class="row">
	            <div class="col-xs-12 padding-0">
	             <h2 class="vacation-h2"><spring:theme code="text.activities.selected.name" text="Activities Selected" /></h2>
	            </div>
	</div>
	</div>

    <c:forEach items="${activityReservationDataList.clubbedActivityReservationDatasByActivity}" var="activityReservationDataEntry" varStatus="activityReservationItemIdx">
	    <bcfglobalreservation:activityReservation alacarteBooking="${alacarteBooking}" activityReservation="${activityReservationDataEntry.value[0]}" groupedActivityReservation="${activityReservationDataEntry.value}" isDealPackage="${isDealPackage}" actionType="${actionType}"/>
    </c:forEach>

</c:if>
