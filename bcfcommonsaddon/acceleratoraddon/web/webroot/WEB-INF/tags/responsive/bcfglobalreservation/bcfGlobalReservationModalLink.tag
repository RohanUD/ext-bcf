<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
	<a class="y_viewReservationDetails ml-3 fnt-14">
		 <strong><spring:theme code="text.deal.listing.selection" text="View Details" /></strong>
	</a>
<div class="modal fade y_bcfGlobalReservationModalComponent" role="dialog" aria-labelledby="y_bcfGlobalReservationModalComponent">
	<div class="modal-dialog text-left" role="document">
		<div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><spring:theme code="text.payment.booking.details" text="Booking details" /></h4>
            </div>
            <div class="modal-body">
                <div class="y_bcfGlobalReservationModalComponentBody">
                    <cms:pageSlot position="BottomContent" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
                </div>
            </div>
        </div>
	</div>
</div>
