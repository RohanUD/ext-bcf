<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="roomStays" required="true" type="java.util.List"%>

<%@ attribute name="accommodationOfferingCode" required="true" type="java.lang.String"%>

<div class="row mb-3 mt-5">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<span class="my-package-heading">
			<spring:theme code="text.cms.accommodationfinder.accommodation.single.room" />
		</span>
	</div>
</div>
<c:forEach var="roomStay" items="${roomStays}">
	<c:forEach var="roomType" items="${roomStay.roomTypes}">
		<div class="row mb-3">
			<div class="col-lg-5 col-md-10 col-sm-10 col-xs-10">${fn:escapeXml(roomType.name)}</div>

            <c:url value="/view/AsmCommentsComponentController/accommodation/add-supplier-comment" var="getSupplierComments" />
            <a class="add-supplier-comments" href="${fn:escapeXml(getSupplierComments)}" data-code="${roomStay.accommodationCode}" data-ref="${roomStay.roomStayRefNumber}" data-type="accommodation">
                <spring:theme code="label.asm.add.supplier.comments" text="Add Supplier Comment" />
            </a>


            <c:if test="${not empty roomStay.promotions}">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="padding-top:5px">
                    <spring:theme code="text.accommodation.details.property.promotion" text="Promotion" /> :
                    <ul>
                        <c:forEach var="promotion" items="${roomStay.promotions}">
                            <li>${fn:escapeXml(promotion)}</li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

		</div>
		<div class="row">

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<div class=image>
					<c:forEach items="${roomStay.roomTypes[0].images}" var="accommodationImage">
						<img data-media="${accommodationImage.url}" alt="${fn:escapeXml(roomType.name)}" class="img-responsive">
					</c:forEach>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
				<c:if test="${not empty roomType.facilities}">
					<ul class="small clearfix y_features">
						<c:forEach var="facility" items="${roomStay.roomTypes[0].facilities}">
							<li>${fn:escapeXml(facility.shortDescription)}</li>
						</c:forEach>
					</ul>
					<a href="#" class="more hidden">
						<spring:theme code="text.accommodation.details.roomstay.features.more" />
                        <span class="bcf bcf-icon-down-arrow"></span>
                    </a>
					<a href="#" class="less hidden">
						<spring:theme code="text.accommodation.details.roomstay.features.less" />
                        <span class="bcf bcf-icon-up-arrow"></span>
                    </a>
				</c:if>
			</div>

            <fmt:formatDate value="${roomStay.checkInDate}" pattern="E MMM dd,yyyy" var="checkInDate" />
            <fmt:formatDate value="${roomStay.checkOutDate}" pattern="E MMM dd,yyyy" var="checkOutDate" />
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
                <p class="mb-1">
                    <spring:theme code="text.vacation.packagefinder.checkindate.title" />
                </p>
                <p class="font-weight-bold mb-1">${checkInDate}</p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <span class="my-package-arrows-icon"></span>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
                <p class="mb-1">
                    <spring:theme code="text.vacation.packagefinder.checkoutdate.title" />
                </p>
                <p class="font-weight-bold mb-1">${checkOutDate}</p>
            </div>

            <c:if test="${fn:length(roomStay.guestCounts)>0}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
                    <span class="my-package-heading">
                        <spring:theme code="text.cms.accommodationbreakdown.guests" text="Guests" />
                    </span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <c:forEach items="${roomStay.guestCounts}" var="guestCount">
                        <c:if test="${guestCount.quantity > 0}">

                            <p class="mb-1">

                              Guests Details:
                                ${guestCount.quantity} x ${guestCount.passengerType.name}
                            </p>
                        </c:if>
                    </c:forEach>
                </div>
            </c:if>

            <div class="col-xs-4 col-md-2 wapper-center margin-top-10">
                <button class="btn btn-primary btn-block removeRoomButton" type="button" id="removeRoomButton_${roomStay.roomStayRefNumber}">

                <input id="removeRoomURL" type="hidden" value="${fn:escapeXml(request.contextPath)}/cart/remove-room/${roomStay.roomStayRefNumber}"/>
                  <spring:theme code="label.packageferryreview.reservation.remove" text="Remove" />
                     </button>

            </div>
            <c:set var="accommodationPageUrl" value="/accommodation-search?${urlParametersMap[roomStay.roomStayRefNumber]}" />


            <div class="row">
                <div class="col-lg-12 text-center">
                    <a href="${accommodationPageUrl}">
                        <i class="bcf bcf-icon-edit"></i> Modify
                    </a>
                </div>
            </div>

		 </div>


        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <spring:theme code="text.accommodation.availability.status" text="Availability Status:" />&nbsp;${roomStay.availabilityStatus}

             </div>

        </div>


	</c:forEach>
<hr class="hr-payment" style="border-top: 2px solid #b3b3b4">
</c:forEach>
