
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="packageDataList" required="true"
	type="com.bcf.facades.reservation.data.PackageReservationDataList"%>
	<%@ taglib prefix="accommodationdetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>


<c:forEach items="${packageDataList.packageReservationDatas}"
	var="packageReservation" varStatus="packageReservationItemIdx">

    <c:url var="detailsPageUrl" value="/package-listing?${packageDetailsUrlParams}&modify=true" />

	<c:set var="roomStay"
		value="${packageReservation.accommodationReservationData.roomStays[0]}" />
	<div class="panel-heading">
	    <div class="row">
	        <span class="my-package-heading"><spring:theme code="text.cms.travelfinder.hotel.tab"/></span>
	    </div>
	</div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-4 col-sm-offset-3">
                <div class="row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <accommodationdetails:propertyCategoryTypeDetails property="${packageReservation.accommodationReservationData.accommodationReference}" />

                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <h5 class="mb-1"><strong>
                            &puncsp;${fn:escapeXml(packageReservation.accommodationReservationData.accommodationReference.accommodationOfferingName)}</strong>
                        </h5>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mb-3">
                        <c:set
                                value="${packageReservation.reservationData.reservationItems[0]}"
                                var="item" />
                            ${packageReservation.accommodationReservationData.accommodationReference.address.town}&puncsp;-&puncsp;
                            <c:set var="numberOfNights">
                                <fmt:parseNumber
                                    value="${(roomStay.checkOutDate.time-roomStay.checkInDate.time)/ (1000*60*60*24)}"
                                    integerOnly="true" />
                            </c:set>
                            ${numberOfNights}&puncsp;<spring:theme code="text.cms.accommodationfinder.accommodation.multiple.nights"/>,
                            <c:set var="numberOfRooms" value="${fn:length(packageReservation.accommodationReservationData.roomStays)}" />
                            ${numberOfRooms}&puncsp;<spring:theme code="text.cms.accommodationsummary.room"/>
                    </div>
                </div>

                <div class="row mb-3">
                    <fmt:formatDate value="${roomStay.checkInDate}"
                        pattern="E MMM dd,yyyy" var="checkInDate" />
                    <fmt:formatDate value="${roomStay.checkOutDate}"
                        pattern="E MMM dd,yyyy" var="checkOutDate" />
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
                            <p class="mb-1"><spring:theme code="text.vacation.packagefinder.checkindate.title" /></p>
                            <p class="font-weight-bold mb-1">${checkInDate}</p>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <span class="my-package-arrows-icon"></span>
                     </div>
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center p-0">
                            <p class="mb-1"><spring:theme code="text.vacation.packagefinder.checkoutdate.title" /></p>
                            <p class="font-weight-bold mb-1">${checkOutDate}</p>
                     </div>


                </div>

                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="${detailsPageUrl}">
                            <i class="bcf bcf-icon-edit"></i> Modify
                        </a>
                    </div>
                </div>

            </div>
        </div>

</c:forEach>
