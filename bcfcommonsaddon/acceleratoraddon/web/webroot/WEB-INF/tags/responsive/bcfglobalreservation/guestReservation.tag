
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="roomStay" required="true" type="de.hybris.platform.commercefacades.accommodation.ReservedRoomStayData"%>
<c:if test="${fn:length(roomStay.guestCounts)>0}">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
		<span class="my-package-heading">
			<spring:theme code="text.cms.accommodationbreakdown.guests" text="Guests" />
		</span>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<c:forEach items="${roomStay.guestCounts}" var="guestCount">
			<c:if test="${guestCount.quantity > 0}">
				<p class="mb-1">
					<spring:theme code="text.cms.accommodationbreakdown.guests.details" text="Guests Details" arguments="${guestCount.quantity},${guestCount.passengerType.name}" />
				</p>
			</c:if>
		</c:forEach>
	</div>
</c:if>
