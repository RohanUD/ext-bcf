<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%@ attribute name="perPersonPackagePrice" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="lengthOfStay" required="true" type="java.lang.Integer"%>
	<c:if test="${isAmendment}">

		<c:choose>
			<c:when test="${perPersonPackagePrice.value >= 0}">
				<spring:theme code="text.accommodation.additional.per.person.payment.amount" />
			</c:when>
			<c:otherwise>
				<spring:theme code="text.accommodation.per.person.refund.amount" />
			</c:otherwise>
		</c:choose>
	</c:if>
	<div class="fnt-14">
		${lengthOfStay}&nbsp
		<c:choose>
			<c:when test="${lengthOfStay gt 1}">
				<spring:theme code="text.page.deallisting.deal.nights" text="nights" />
			</c:when>
			<c:otherwise>
				<spring:theme code="text.page.deallisting.deal.night" text="night" />
			</c:otherwise>
		</c:choose>
		&nbsp
		<spring:theme code="text.page.deallisting.deal.trip.duration" text="& return ferry from" />
	</div>
	<div class="vacations-listing--section-ttile-big">
		<format:price priceData="${perPersonPackagePrice}" />
	</div>
	<div class="per-person-txt-minus">
		<spring:message code="text.page.deallisting.deal.trip.price.perperson.tooltip" var="tooltipText" />
		<div class="info-tooltip" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="${fn:escapeXml(tooltipText)}">
            <span class="bc-text-blue fnt-14">
                <spring:theme code="text.page.deallisting.deal.trip.price.perperson" text="per person" />
            </span>
        </div>
        <div class="vacations-listing--section-title-small">
            <spring:theme code="text.page.deallisting.deal.trip.price.extra" text="+ taxes and fees" />
        </div>
	</div>
	<hr>
