<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:if test="${not empty facetData.values}">
   <li role="presentation" class="dropdown filter-li ${fn:length(facetData.values) gt 10 ? 'full-width-dropdown' : ''} package-listing-filters">
      <label>
         <spring:theme code="package.listing.facet.${facetData.name}" />
      </label>
      <a data-toggle="dropdown" class="filter-btn" href="#" role="button" aria-haspopup="true" aria-expanded="false">
         <span class="totalCheckboxesChecked"></span>
         <spring:theme code="package.listing.facet.select.${facetData.name}"/>
         <c:set var="facetValuesFilter" value="selectedFacetValues_${facetData.code}"/>
         <span class="caret"></span>
      </a>
      <ul class="dropdown-menu filter-dropdown package-list-checkbox">
         <c:if test="${not empty requestScope[facetValuesFilter]}">
            <c:set var="selectedFacetValues" value="${fn:split(requestScope[facetValuesFilter], ',')}" />
             <c:forEach items="${facetData.values}" var="facetValue" varStatus="fcIdx">
                <c:set var="displayCurrentFacetValue" value="false"/>
                <c:forTokens var="selectedFacetValue" items="${requestScope[facetValuesFilter]}" delims=";">
                    <c:if test="${selectedFacetValue eq facetValue.code }">
                        <c:set var="displayCurrentFacetValue" value="true"/>
                    </c:if>
                </c:forTokens>
                <c:if test="${displayCurrentFacetValue}">
                    <li class="${fn:length(facetData.values) gt 10 ? 'col-sm-4' : ''}">
                       <div class="checkbox">
                          <label class="custom-checkbox-input show deal-details-icon" for="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}">
                             <input type="checkbox" class="y_packageListingFacet" ${facetValue.selected ? 'checked="checked"' : ''} id="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}" value=":${facetData.code}:${facetValue.code}" />
                             <input type="hidden" name="_passengerInfoForm.accessibilityNeedsCheckOutbound" value="on">
                             <span class="${fn:toUpperCase(facetValue.code)}"></span>
                             <spring:theme code="${facetValue.name}" text="${fn:escapeXml(facetValue.name)}" />
                             <span class="checkmark-checkbox"></span>
                          </label>
                       </div>
                    </li>
                </c:if>
             </c:forEach>
         </c:if>
         <c:if test="${empty requestScope[facetValuesFilter]}">
             <c:forEach items="${facetData.values}" var="facetValue" varStatus="fcIdx">
                <li class="${fn:length(facetData.values) gt 10 ? 'col-sm-4' : ''}">
                   <div class="checkbox">
                      <label class="custom-checkbox-input show deal-details-icon" for="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}">
                         <input type="checkbox" class="y_packageListingFacet" ${facetValue.selected ? 'checked="checked"' : ''} id="facet_${fn:escapeXml(facetData.code)}_${fcIdx.count}" value=":${facetData.code}:${facetValue.code}" />
                         <input type="hidden" name="_passengerInfoForm.accessibilityNeedsCheckOutbound" value="on">
                         <span class="${fn:toUpperCase(facetValue.code)}"></span>
                         <spring:theme code="${facetValue.name}" text="${fn:escapeXml(facetValue.name)}" />
                         <span class="checkmark-checkbox"></span>
                      </label>
                   </div>
                </li>
             </c:forEach>
         </c:if>
         <li>
            <button class="btn btn-primary custom-btn my-4 y_packageListingFacetApply">
                <spring:theme code="text.packagelisting.apply.button" text="Apply" />
            </button>
	     </li>
      </ul>
   </li>
</c:if>
