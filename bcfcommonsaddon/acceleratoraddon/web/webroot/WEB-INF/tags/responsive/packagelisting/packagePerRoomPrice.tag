<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="lengthOfStay" required="true" type="java.lang.Integer" %>
<%@ attribute name="perPersonPackagePrice" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="discount" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<spring:htmlEscape defaultHtmlEscape="false"/>
<div>
    <div class="fnt-14"> ${lengthOfStay}&nbsp;
        <c:choose>
            <c:when test="${lengthOfStay gt 1}">
                <spring:theme code="text.page.deallisting.deal.nights" text="nights"/>&nbsp;
            </c:when>
            <c:otherwise>
                <spring:theme code="text.page.deallisting.deal.night" text="night"/>&nbsp;
            </c:otherwise>
        </c:choose>
        <spring:theme code="text.page.deallisting.deal.trip.duration" text="& return ferry from"/>
    </div>
    <div class="vacations-listing--section-ttile-big">
        <spring:theme code="text.page.deallisting.deal.trip.price.total" text="Price:"/><format:price priceData="${perPersonPackagePrice}" /><br>
        <spring:theme code="text.page.deallisting.deal.trip.price.discount" text="Discount:"/>${discount.formattedValue}<br>
    </div>
    <div class="per-person-txt-minus">
        <spring:message code="text.page.deallisting.deal.trip.price.perperson.tooltip" var="tooltipText"/>
        <a href="#" class="info-tooltip" data-toggle="tooltip" title="${tooltipText}" tabindex="0">
            <spring:theme code="text.page.deallisting.deal.trip.price.perperson" text="per person"/>
        </a>
    </div>
    <div class="vacations-listing--section-title-small">
        <spring:theme code="text.page.deallisting.deal.trip.price.extra" text="+ taxes and fees"/>
    </div>
</div>

