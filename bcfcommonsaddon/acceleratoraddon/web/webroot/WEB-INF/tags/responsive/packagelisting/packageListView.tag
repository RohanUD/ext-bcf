<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="propertiesListParams" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<c:if test="${fn:length(propertiesListParams) gt 0}">
    <c:forEach var="property" items="${propertiesListParams}" varStatus="propID">
    <c:if test="${property.availabilityStatus eq 'AVAILABLE'}">
        <packagelisting:packageListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
    </c:if>
    <c:if test="${property.availabilityStatus eq 'STOPSALE'}">
        <packagelisting:packageListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
    </c:if>
        <c:if test="${property.availabilityStatus eq 'ON_REQUEST'}">
            <packagelisting:packageListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
        </c:if>
        <c:if test="${property.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
            <packagelisting:packageListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
        </c:if>
        <c:if test="${property.availabilityStatus eq 'SOLD_OUT'}">
            <packagelisting:packageListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
        </c:if>
        <c:if test="${(propID.index + 1) % 2 == 0}">
            </div>
            <div class="row">
        </c:if>
    </c:forEach>
</c:if>
