<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationdetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ attribute name="packageData" required="true" type="de.hybris.platform.commercefacades.packages.PackageData"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%@ attribute name="stayDateRange" required="true" type="de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData"%>
<c:set var="maxFractionDigits" value="1" />
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ${packageData.promoted ? 'promoted' : ''}">
	<div class="accommodation-image package-list-owl">
		<div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(packageData.images)}">
			<c:forEach var="image" items="${packageData.images}">
				<c:if test="${image.imageType eq 'PRIMARY'}">
					<c:set var="dataMedia" value="${image.url}" />
					<div class="image">
						<img class='js-responsive-image' alt='${image.altText}' title='${image.altText}' data-media='${dataMedia}' />
						<div class="slider-count">
						    <div class="slider-img-thumb"></div>
                            <div class="slider-num">1/5</div>
                        </div>
					</div>
				</c:if>
			</c:forEach>
		
        <c:if test="${packageData.availabilityStatus eq 'SOLD_OUT' && fn:length(packageData.images) gt 0}">
            <div class="overlay-sold-out">
                <div class="overlay-text">
                    <p><spring:theme code="message.package.hotel.not.available" /></p>
                    <a href="/#tabs-2" class="btn-white mt-4">Modify search</a>
                </div>
		    </div>
        </c:if>
	</div>
    </div>
	<div class="deal-details margin-top-0">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<accommodationdetails:propertyCategoryTypeDetails property="${packageData}" />
			</div>
		</div>
		<div class="row flex-box">
			<div class="col-lg-8 col-md-8 col-sm-7 col-xs-7 deal-details-border">
				<h2 class="margin-top-0 title-listing">
					${fn:escapeXml(packageData.accommodationOfferingName)}
					<c:if test="${packageData.isDealOfTheDay}">
						<c:out value="(DEAL OF THE DAY)" />
					</c:if>
				</h2>
                <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                    <spring:param name="locationId"  value="${packageData.reviewLocationId}"/>
                </spring:url>
				<div class="review-img">
				    <div class="fnt-14 trip-advisor-title">Trip Advisor Traveler Rating</div>
					<p class="trip-advisor-reviews">
					    <img src="${fn:escapeXml(packageData.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating" width="100" height="30">
					    <a class="ratings--review-container tripAdvisorLink" href="${tripAdvisorURL}">${packageData.customerReviewData.numOfReviews} Reviews</a>
					</p>
				</div>
				<div class="text-div fnt-14 text-reviews">
					${packageData.description}
				</div>
			<c:url var="detailsPageUrl"
				   value="/package-details/${packageData.accommodationOfferingCode}?${urlParameters}&modify=${modify}"/>
				<div class="learn-link">
					<a href="/package-details/${packageData.accommodationOfferingCode}?${urlParameters}&modify=${modify}&tab=tab-01"><spring:theme code="text.package.listing.learn.more"/> <i class="fas bcf bcf-icon-right-arrow ml-1 fnt-16"></i></a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 text-center price-info-section">
			    <c:choose>
                    <c:when test="${packageData.availabilityStatus eq 'SOLD_OUT'}">
                        <div class="sold-out-section">
                            <div class="sold-out-text box-align-center">
                                <spring:theme code="text.package.listing.hotel.sold.out" />
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${not empty packageData.totalPackagePrice}">
                                <packagelisting:packagePerPersonPrice perPersonPackagePrice="${packageData.perPersonPackagePrice}" lengthOfStay="${stayDateRange.lengthOfStay}" />
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-10 margin-bottom-17 total-price-wrapper nopadding">

                            <div class="total text-center fnt-14">
                             <c:choose>
                                <c:when test="${isAmendment}">
                                 <c:choose>
                                    <c:when test="${packageData.totalPackagePrice.value >= 0}">
                                        <spring:theme code="text.accommodation.additional.total.amount"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:theme code="text.accommodation.total.refund.amount"/>
                                    </c:otherwise>
                                </c:choose>
                                </c:when>
                                 <c:otherwise>
                                    <spring:theme code="text.package.listing.total.price" text="Total price:"/>
                                  </c:otherwise>
                                  </c:choose>

                                 </div>


                                <h4 class="text-center small-price">
                                    <span class="sr-only">
                                        <spring:theme code="text.page.deallisting.deal.price.sale.sr" text="Sale price" />
                                    </span>
                                    <strong><format:price priceData="${packageData.totalPackagePrice}" /></strong>
                                </h4>
                                <div class="vacations-listing--section-title-small price-text-section">
                                    <spring:theme code="text.page.deallisting.deal.trip.price.extra" text="+ taxes and fees" />
                                </div>
                            </div>
                        </c:if>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center p-0 package-req-btn-height">
                            <c:choose>
                                <c:when test="${packageData.availabilityStatus eq 'AVAILABLE'}">
                                    <a href="${detailsPageUrl}" class="btn btn-primary py-2 margin-top-10">
                                        <spring:theme code="text.package.listing.button.continue" />
                                    </a>
                                </c:when>
                                <c:when test="${packageData.availabilityStatus eq 'SOLD_OUT'}">
                                    <div class="sold-out-section">
                                        <div class="sold-out-text box-align-center">
                                            <spring:theme code="text.package.listing.hotel.sold.out" />
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${packageData.availabilityStatus eq 'ON_REQUEST'}">
                                    <a href="${detailsPageUrl}" class="btn btn-primary white-spc">
                                        <spring:theme code="text.package.listing.button.request.booking" />
                                    </a>
                                </c:when>
                                 <c:when test="${packageData.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
                                    <a href="${detailsPageUrl}" class="btn btn-primary margin-top-10">
                                        <spring:theme code="text.package.listing.button.notBookableOnline" />
                                    </a>
                                </c:when>
                            </c:choose>
                        </div>
                    </c:otherwise>
                </c:choose>
			</div>
		</div>
	</div>
</div>
