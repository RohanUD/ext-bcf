<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="deals" required="true" type="java.util.List"%>
<c:forEach var="deal" items="${deals}" varStatus="loop">
	<div class=" col-lg-6 col-md-6  col-sm-6  col-xs-12 ">
		<div class="deal-items ">
			<c:if test="${fn:length(deal.imageUrls) gt 0}">
				<img class='img-fluid js-responsive-image' alt='Deal Image' data-media='${deal.imageUrls[0].url}' alt='${deal.imageUrls[0].altText}' title='${deal.imageUrls[0].altText}'>
			</c:if>

			<div class="deal-text">
				<h2 class="text-dark-blue"><b>${deal.name}</b></h2>
				<p class="mb-5">${deal.description}</p>
				<p>
					<a href="${requestScope['javax.servlet.forward.request_uri']}/${deal.packageDealHotelListingUrl}">
					    <b><spring:theme code="text.deals.listing.label.view.packages" />
					        <i class="far bcf bcf-icon-right-arrow"></i>
					    </b>
					</a>
				</p>
			</div>
		</div>
	</div>
</c:forEach>
