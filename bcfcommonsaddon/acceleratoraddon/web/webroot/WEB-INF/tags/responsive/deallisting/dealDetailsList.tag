<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="dealsResponseDataList" required="true" type="com.bcf.facades.deals.search.response.data.DealsResponseData"%>
<c:choose>
	<c:when test="${fn:length(dealsResponseDataList.dealResponseData) == 0}">
		<p>No Deals Available.</p>
	</c:when>
	<c:otherwise>
		<c:forEach var="dealResponse" items="${dealsResponseDataList.dealResponseData}">
			<li class="col-xs-12">
				<div class="clearfix">
					<div class="col-xs-5 col-sm-4  accommodation-image">
						<div class="row">
							<c:forEach var="image" items="${dealResponse.mainImageMediaUrl}">
								<img src="${image}" alt="${fn:escapeXml(dealResponse.dealName)}">
							</c:forEach>
						</div>
					</div>
					<div class="accommodation-details col-xs-offset-5 col-xs-7 col-sm-offset-4 col-sm-8">
						<div class="row">
							<div class="accommodation-name-awards col-xs-12 col-sm-8">
								<div class="clearfix">
									<div class="col-xs-12">
										<div class="row">
											<h3>
												<c:url var="detailsPageUrl" value="/accommodation-details/${dealResponse.dealId}?${urlParameters}" />
												<a href="${detailsPageUrl}">${dealResponse.dealDescription}</a>
												<c:forEach var="hotel" items="${dealResponse.hotelName}">
													<h2>${hotel}</h2>
												</c:forEach>
											</h3>
											<div class="col-xs-12 accommodation-star-rating">
												<div class="row">
													<div class="col-md-12">
															<c:set var="rating" value="${dealResponse.starRating gt 15 ? 15 : dealResponse.starRating}" />
															<span class="sr-only">${fn:escapeXml(rating)} <spring:theme code="text.accommodation.listing.star.rating.stars" />
															</span>
															<c:if test="${rating gt 0}">
																<span aria-label="stars"> <c:forEach begin="0" end="${rating -1}" varStatus="loop">
																		<span>&#9733;</span>
																	</c:forEach>
																</span>
															</c:if>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<c:if test="${not empty dealResponse.price}">
								<div class="accommodation-duration-price col-xs-12 col-sm-4">
									<div class="price">
										<p>From: ${dealResponse.price.formattedValue}</p>
										<c:if test="${dealResponse.discount gt 0}">
											<div class="ribbon-container">
												<span><c:choose>
														<c:when test="${dealResponse.discountType eq 'PERCENT'}">
															<spring:theme code="text.deal.listing.saving.percentage" arguments="${dealResponse.discount}" />
														</c:when>
														<c:otherwise>
															<spring:theme code="text.deal.listing.saving.amount" arguments="${dealResponse.discount}" />
														</c:otherwise>
													</c:choose></span>
											</div>
										</c:if>
										<c:set var="numberOfNights" value="${fn:length(dealsResponseDataList.dealResponseData)}" />
										<ul class="room-types">
											<li><c:choose>
													<c:when test="${numberOfNights == '1' }">${fn:escapeXml(numberOfNights)}&nbsp;
											<spring:theme code="text.cms.accommodationfinder.accommodation.single.night" text="Night" />
													</c:when>
													<c:otherwise>${fn:escapeXml(numberOfNights)}&nbsp;
											<spring:theme code="text.cms.accommodationfinder.accommodation.multiple.nights" text="Nights" />
													</c:otherwise>
												</c:choose></li>
											<c:forEach var="info" items="${property.rateRange.accommodationInfos}">
												<li>${fn:escapeXml(info.cardinality)}x${fn:escapeXml(info.accommodationName)}</li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</c:if>
							<c:forEach var="origin" items="${dealResponse.originLocation}">
								<c:forEach var="destination" items="${dealResponse.destinationLocation}">
									<c:forEach var="originName" items="${dealResponse.originLocationName}">
										<c:forEach var="destinationName" items="${dealResponse.destinationLocationName}">
											<div class="col-xs-12">
												<div class="col-md-6">
													<div class="row reviews">
														<p><spring:theme code="dealselection.outbound" /> </p>
													</div>
													<div class="row">
														<p>${originName}(${origin})->${destinationName}(${destination})</p>
													</div>
												</div>
												<div class="col-md-6	">
													<div class="row reviews">
													<p><spring:theme code="dealselection.inbound" /><spring:theme code="dealselection.inbound" /></p>
													</div>
													<div class="row">
														<p>${destinationName}(${destination})->${originName}(${origin})</p>
													</div>
												</div>
											</div>
										</c:forEach>
									</c:forEach>
								</c:forEach>
							</c:forEach>
							<c:if test="${dealResponse.available}">

							<spring:theme code="text.deallisting.deal.available" />
							</c:if>

							<spring:theme code="text.deallisting.stock.${dealResponse.stockType.code}" />
							<div class="col-md-12">
								<div class="row">
									<div class="col-xs-12  col-sm-12 text-right">
										<c:url var="detailsPageUrl" value="/deal-details?dealBundleTemplateId=${dealResponse.dealId}&dealSelectedDepartureDate=${dealSelectedDepartureDate}" />
										<a href="${detailsPageUrl}" class="btn btn-secondary"> <spring:theme code="text.deal.listing.selection" />
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		</c:forEach>
	</c:otherwise>
</c:choose>
