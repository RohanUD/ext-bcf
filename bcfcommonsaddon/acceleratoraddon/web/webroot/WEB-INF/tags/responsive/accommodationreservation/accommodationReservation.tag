<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>
<%@ attribute name="accommodationReservation" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationReservationData"%>
<%@ attribute name="accommodationResVarStatus" type="java.lang.Integer"%>

<h5><b><spring:theme code="text.package.details.package.label" text="Package" />: ${accommodationReservation.accommodationReference.accommodationOfferingName} </b></h5>

<div class="y_accommodationReservationComponent">
	<p>
		<strong><spring:theme code="text.cms.travelfinder.hotel.tab" text="Hotel" />:</strong> ${accommodationReservation.accommodationReference.accommodationOfferingName}

	</p>

	<c:set var="roomStay" value="${accommodationReservation.roomStays[0]}" />

     <c:if test="${showAvailabilityStatus}">
         <p>
            <strong><spring:theme code="text.accommodation.availability.status" text="Availability Status:" /></strong>&nbsp;${roomStay.availabilityStatus}
         </p>
     </c:if>

	<c:set var="numberOfNights">
		<fmt:parseNumber value="${(roomStay.checkOutDate.time-roomStay.checkInDate.time)/ (1000*60*60*24)}" integerOnly="true" />
	</c:set>
	<p>
		<strong><spring:theme code="text.cms.accommodationfinder.numberofnights.placeholder" text="Number of Nights" />:</strong> ${numberOfNights}
	</p>

	<c:choose>
        <c:when test="${fn:length(accommodationReservation.roomStays) gt 1}">
            <c:forEach var="roomStayItem" items="${accommodationReservation.roomStays}" varStatus="roomStayItemStatus">
                <p><strong>Room ${roomStayItemStatus.index+1} </strong>
                 <c:if test="${showSupplierComments}">
                        <c:url value="/view/AsmCommentsComponentController/accommodation/add-supplier-comment" var="getSupplierComments" />
                       &nbsp;&nbsp;&nbsp;&nbsp; <a class="add-supplier-comments" href="${fn:escapeXml(getSupplierComments)}" data-code="${roomStayItem.accommodationCode}" data-ref="${roomStayItem.roomStayRefNumber}" data-type="accommodation">
                            <spring:theme code="label.asm.add.supplier.comments" text="Add Supplier Comment" />
                        </a>
                  </c:if>
                  </p>


                <c:set var="guestCountTotal" value="0" />
                <c:forEach var="guestCount" items="${roomStayItem.guestCounts}">
                    <c:set var="guestCountTotal" value="${guestCountTotal+guestCount.quantity}" />
                </c:forEach>
                <p>
                    <strong> <spring:theme code="text.cms.accommodationfinder.numberofguests.placeholder" text="Number of Guests" />:</strong> ${guestCountTotal}
                </p>

                <p>
                		<fmt:formatDate value="${roomStayItem.checkInDate}" pattern="MMM dd, yyyy" var="checkInDate" />
                		<strong><spring:theme code="text.vacation.packagefinder.checkindate.title" text="Check-in date" />:</strong> ${checkInDate}
                	</p>
                	<p>
                		<fmt:formatDate value="${roomStayItem.checkOutDate}" pattern="MMM dd, yyyy" var="checkOutDate" />
                		<strong><spring:theme code="text.vacation.packagefinder.checkoutdate.title" text="Check-out date" />:</strong> ${checkOutDate}
                	</p>
                	<p>
                		<strong><spring:theme code="text.accommodation.details.property.room.type" text="Room type" />:</strong> ${roomStayItem.roomTypes[0].name}
                	</p>

            </c:forEach>
        </c:when>
        <c:otherwise>
            <c:set var="guestCountTotal" value="0" />
            <c:forEach var="guestCount" items="${roomStay.guestCounts}">
                <c:set var="guestCountTotal" value="${guestCountTotal+guestCount.quantity}" />
            </c:forEach>
            <p>
                <strong> <spring:theme code="text.cms.accommodationfinder.numberofguests.placeholder" text="Number of Guests" />:</strong> ${guestCountTotal}
            </p>

            <p>
            		<fmt:formatDate value="${roomStay.checkInDate}" pattern="MMM dd, yyyy" var="checkInDate" />
            		<strong><spring:theme code="text.vacation.packagefinder.checkindate.title" text="Check-in date" />:</strong> ${checkInDate}
            	</p>
            	<p>
            		<fmt:formatDate value="${roomStay.checkOutDate}" pattern="MMM dd, yyyy" var="checkOutDate" />
            		<strong><spring:theme code="text.vacation.packagefinder.checkoutdate.title" text="Check-out date" />:</strong> ${checkOutDate}
            	</p>
            	<p>
            		<strong><spring:theme code="text.accommodation.details.property.room.type" text="Room type" />:</strong> ${roomStay.roomTypes[0].name}
            	</p>

        </c:otherwise>
    </c:choose>

     <c:forEach var="currentRoomStay" items="${accommodationReservation.roomStays}">
        <c:if test="${not empty currentRoomStay.reservedGuests[0].profile.firstName}">
         <p>
            <strong> <spring:theme code="text.page.pendingorders.header.order.placedby" text="Guest Name" />:</strong>
             <c:forEach var="guestData" items="${currentRoomStay.reservedGuests}">
                ${guestData.profile.firstName}&nbsp;${guestData.profile.lastName}
            </c:forEach>
         </p>
         </c:if>
     </c:forEach>


</div>
