<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="accommodationdetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ attribute name="packageData" required="true" type="de.hybris.platform.commercefacades.packages.PackageData"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%@ attribute name="stayDateRange" required="true" type="de.hybris.platform.commercefacades.accommodation.search.StayDateRangeData"%>
<c:set var="maxFractionDigits" value="1" />
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 package-list-carousel ${packageData.promoted ? 'promoted' : ''}">
	<div class="accommodation-image">
		<div class="js-owl-carousel main-image-carousel ">
			<c:forEach var="image" items="${packageData.images}">
				<c:if test="${image.imageType eq 'PRIMARY'}">
					<c:set var="dataMedia" value="${image.url}" />
					<div class="image">
						<img class='js-responsive-image' alt='${image.altText}' title='${image.altText}' data-media='${dataMedia}' />
					</div>
				</c:if>
			</c:forEach>
		</div>

	</div>
	<div class="deal-details">
		<div class="row">
			<div class="col-lg-12">
				<accommodationdetails:propertyCategoryTypeDetails property="${packageData}" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 mb-3 deal-details-border">
				<h3>
					${fn:escapeXml(packageData.accommodationOfferingName)}
				</h3>
                <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                    <spring:param name="locationId"  value="${packageData.reviewLocationId}"/>
                </spring:url>
				<div class="review-img">
					<c:if test="${not empty packageData.customerReviewData.ratingImageUrl}">
						<img src="${fn:escapeXml(packageData.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating">
					</c:if>

					<a class="font-weight-bold tripAdvisorLink" href="${tripAdvisorURL}">${packageData.customerReviewData.numOfReviews} Reviews</a>
				</div>
				<div class="text-div">
					${fn:escapeXml(packageData.description)}
					<c:out value="${packageData.description}" />
				</div>
				<div class="learn-link">
					<a href="${packageData.nextUrl}&tab=tab-01">
						Learn more <i class="fas bcf bcf-icon-right-arrow ml-1"></i>
					</a>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<c:if test="${not empty packageData.totalPackagePrice}">
					<div class=" col-lg-12 ">
						<p class="total text-center">
						<div class="fnt-14">
						  <c:out value="${packageData.propertyInformation}" />
						</div>
						<div class="fnt-14">
						  <spring:theme code="text.package.hotel.listing.fromPrice" text="From price:" />
						</div>
						
							<div class="vacations-listing--section-ttile-big">
		                         <format:price priceData="${packageData.totalPackagePrice}" />
</div>

<div class="per-person-txt-minus">
		<div class="info-tooltip" data-toggle="tooltip" title="Per person price = total package price divided by number of adults and children travelling (excluding infants)." tabindex="0">
			<div class="bc-text-blue fnt-14">
				<spring:theme code="text.package.hotel.listing.perPerson" text="per person" /><br>
				<span class="vacations-listing--section-title-small">
					<spring:theme code="text.package.hotel.listing.taxLabel" text="+ taxes and fee, based on double occupancy" /></span>
			</div>
		</div>
	</div>

							
						</p>
					</div>
				</c:if>
				<%--<c:url var="detailsPageUrl" value="/package-hotel-details/${packageData.accommodationOfferingCode}" />--%>
                <div class="col-lg-12 mt-3 text-center sold-out-text box-align-center">
				<c:choose>
                    <c:when test="${packageData.availabilityStatus eq 'AVAILABLE'}">
                            <a href="${packageData.nextUrl}" class="btn btn-primary ">
                                <spring:theme code="text.package.listing.button.continue" />
                            </a>
                    </c:when>
                    <c:when test="${packageData.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
                        <a href="${fn:escapeXml(request.contextPath)}/get-quote?name=${packageData.accommodationOfferingName}" class="btn btn-primary">
                            <spring:theme code="text.package.listing.button.notBookableOnline" />
                        </a>
                    </c:when>
                </c:choose>
                </div>
			</div>
		</div>
	</div>
</div>
