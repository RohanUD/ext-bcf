<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="propertiesListParams" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="packagehotellisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagehotellisting"%>
<div class="container">
<div class="row">
<c:if test="${fn:length(propertiesListParams) gt 0}">
<div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-10"><h2>${headingText }</h2></div>
    <div class="row">
        <c:forEach var="property" items="${propertiesListParams}" varStatus="propID">
             <c:if test="${property.availabilityStatus eq 'AVAILABLE'}">
                <packagehotellisting:packageHotelListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
            </c:if>
            <c:if test="${property.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
                <packagehotellisting:packageHotelListItem packageData="${property}" stayDateRange="${packageSearchResponse.criterion.stayDateRange}"/>
            </c:if>
            <c:if test="${(propID.index + 1) % 2 == 0}">
                </div>
                <div class="row">
            </c:if>
        </c:forEach>
    </div>
</c:if>
</div>
</div>
