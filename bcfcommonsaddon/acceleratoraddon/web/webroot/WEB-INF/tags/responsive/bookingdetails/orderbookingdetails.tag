<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="commonsBookingDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${globalReservationData.accommodationReservationData != null}">
			<bookingDetails:accommodationBookingDetails accommodationReservationData="${globalReservationData.accommodationReservationData}" isTravelSite="true" customerReviews="${customerReviews}" />
		</c:if>
		<c:if test="${globalReservationData.reservationData != null}">
			<bookingDetails:transportBookingDetails reservationData="${globalReservationData.reservationData}" />
		</c:if>
		<c:if test="${globalReservationData.reservationData.bookingStatusCode != 'ACTIVE_DISRUPTED_PENDING'}">
			<div class="fieldset">
				<div class="row">
					<div class="col-xs-12 col-sm-offset-4 col-sm-4">
						<c:if test="${globalReservationData.accommodationReservationData != null}">
							<c:set var="actionName">
								<spring:theme code="button.booking.details.accommodation.booking.action.cancel.transport.booking" text="Cancel Flight Booking" />
							</c:set>

						</c:if>
					</div>
					<div class="col-xs-12 col-sm-4">

					</div>
				</div>
			</div>
		</c:if>
