<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="bookingActionResponseData" required="true" type="de.hybris.platform.commercefacades.travel.BookingActionResponseData"%>
<%@ attribute name="activityReservation" required="false" type="com.bcf.facades.reservation.data.ActivityReservationData"%>
<%@ attribute name="actionType" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:forEach items="${bookingActionResponseData.globalBookingActions}" var="bookingActionData">

	<c:if test="${actionType == bookingActionData.actionType}">
		<c:set var="actionData" value="${bookingActionData}" />
	</c:if>
</c:forEach>

<c:if test="${actionData.enabled}">

<c:choose>
	<c:when test="${actionType == 'AMEND_ALACARTE_BOOKING'}">
	    <c:set var="y_class" value="y_amendAlacarteBooking" />
           <c:set var="disabled" value="false" />
           	<c:url value="${actionData.actionUrl}" var="actionUrl" />
           <form action="${actionUrl}" method="post">
            <input type="hidden" name="CSRFToken" value="${CSRFToken.token}" />
             <button type="submit" class="btn btn-primary btn-block"><spring:theme code="button.booking.details.accommodation.booking.action.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" /></button>
           </form>

	</c:when>
	    <c:when test="${actionType == 'AMEND_STANDALONE_ACTIVITY'}">
            <c:url value="${actionData.actionUrl}/${activityReservation.activityDetails.code}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" var="actionUrl" />
            <a class="btn btn-primary btn-block ${y_class}" href="${actionUrl}" ${disabled ? 'disabled' : ''} >
                <spring:theme code="button.booking.details.travel.global.booking.action.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" />
            </a>
        </c:when>
        <c:when test="${actionType == 'CANCEL_STANDALONE_ACTIVITY'}">
            <c:url value="${actionData.actionUrl}/${activityReservation.activityDetails.code}/${actionData.originDestinationRefNumber}/${activityReservation.activityDate}?time=${activityReservation.activityTime}" var="actionUrl" />
            <a class="btn btn-primary btn-block ${y_class}" href="${actionUrl}" ${disabled ? 'disabled' : ''} >
                <spring:theme code="button.booking.details.travel.global.booking.action.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" />
            </a>
        </c:when>
        <c:when test="${actionType == 'APPLY_GOODWILL_REFUND'}">

            <c:url value="${actionData.actionUrl}" var="actionUrl" />&nbsp;
             <a class="btn btn-primary btn-block ${y_class}" href="${actionUrl}" ${disabled ? 'disabled' : ''} >
                <spring:theme code="button.booking.details.travel.global.booking.action.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" />
            </a>
         </c:when>
    	<c:otherwise>
    		  <c:if test="${actionType == 'CANCEL_BOOKING'}">
                 				<c:set var="y_class" value="y_cancelBookingButton" />
                  </c:if>


            	<c:url value="${actionData.actionUrl}" var="actionUrl" />
            	<a class="btn btn-primary btn-block ${y_class}" href="${actionUrl}" ${disabled ? 'disabled' : ''} >
            		<spring:theme code="button.booking.details.travel.global.booking.action.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" />
            	</a>
    	</c:otherwise>
    </c:choose>


</c:if>
