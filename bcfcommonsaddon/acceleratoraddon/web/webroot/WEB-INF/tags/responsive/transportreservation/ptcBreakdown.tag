<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="ptcBreakdownList" required="true" type="java.util.List"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%-- <div class="sr-only"><spring:theme code="sr.itinerary.passengers" /></div> --%>
<c:forEach items="${ptcBreakdownList}" var="ptcFareBreakdown" varStatus="ptcIdx">
	<li>
		<div class="row">
			<ul class="col-xs-9 col-md-9">
				<li>${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.quantity)}&nbsp;x&nbsp;${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.passengerType.name)}</li>
			</ul>
			<ul class="col-xs-3 col-md-3 text-right">
				<li>${fn:escapeXml(ptcFareBreakdown.passengerFare.baseFare.formattedValue)}</li>
			</ul>
		</div>
		<c:forEach items="${ptcFareBreakdown.passengerFare.taxes}" var="taxes" varStatus="taxesIdx">
			<div>
				<Strong>${taxes.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${taxes.code}</Strong>
			</div>
		</c:forEach>
		<c:forEach items="${ptcFareBreakdown.passengerFare.discounts}" var="discounts" varStatus="discountsIdx">
			<div>
				<Strong>${discounts.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${discounts.purpose}</Strong>
			</div>
		</c:forEach>
	</li>
	<%-- <p>${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.quantity)}&nbsp;x&nbsp;${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.passengerType.name)}:&nbsp;<strong>${fn:escapeXml(ptcFareBreakdown.passengerFare.baseFare.formattedValue)}</strong>
	</p> --%>
</c:forEach>
