<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="vehicleBreakdownList" required="true" type="java.util.List"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%-- <div class="sr-only"><spring:theme code="sr.itinerary.passengers" /></div> --%>
<c:forEach items="${vehicleBreakdownList}" var="vehicleFareBreakdown" varStatus="vehicleIdx">
	<li>
		<div class="row">
			<ul class="col-xs-9 col-md-9">
				<li>
					${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.qty)}&nbsp;x&nbsp;${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.name)}
				<c:if test="${vehicleFareBreakdown.vehicleTypeQuantity.carryingLivestock eq true}">
					<spring:theme code="label.payment.reservation.transport.carrying.livestock" text="(Carrying Livestock)" />
				</c:if>
				</li>
			</ul>
			<ul class="col-xs-3 col-md-3">
				<li>${fn:escapeXml(vehicleFareBreakdown.vehicleFare.baseFare.formattedValue)}</li>
			</ul>
		</div>
		<div class="col-sm-12">${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.vehicleWithSidecarOrTrailer)==true?'Vehicle with Sidecar/Trailer':'' }</div>
		<c:forEach items="${vehicleFareBreakdown.vehicleFare.taxes}" var="taxes" varStatus="taxesIdx">
			<div>
				<Strong>${taxes.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${taxes.code}</Strong>
			</div>
		</c:forEach>
		<c:forEach items="${vehicleFareBreakdown.vehicleFare.discounts}" var="discounts" varStatus="discountsIdx">
			<div>
				<Strong>${discounts.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${discounts.purpose}</Strong>
			</div>
		</c:forEach>		
	</li>
</c:forEach>
