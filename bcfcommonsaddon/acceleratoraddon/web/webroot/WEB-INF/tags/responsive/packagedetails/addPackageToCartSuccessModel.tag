<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal fade" id="y_addPackageToCartSuccessModal" tabindex="-1" role="dialog" aria-labelledby="addPackageToCartSuccessModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="addPackageToCartSuccessModal">
					<spring:theme code="text.package.details.add.package.to.cart.success.modal.title"/>
				</h4>
			</div>
			<div class="modal-body">
				<div>
				<spring:theme code="text.package.details.add.package.to.cart.success.modal.message" />
				</div>
			</div>
		</div>
	</div>
</div>
