<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="accommodationAvailabilityResponse" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData" %>

<c:url var="changePackageAccommodationUrl" value="/cart/accommodation/package-change"/>
<form:form id="packageChangeAccommodationForm" commandName="packageChangeAccommodationForm" name="packageChangeAccommodationForm" class="y_changeAccommodationForm" action="${changePackageAccommodationUrl}" method="post">
    <input type="hidden" name="accommodationOfferingCode" value="${fn:escapeXml(accommodationAvailabilityResponse.accommodationReference.accommodationOfferingCode)}"/>
    <input type="hidden" id="y_numberOfRooms" name="numberOfRooms" value="${travelFinderForm.accommodationFinderForm.numberOfRooms}"/>
    <input type="hidden" id="y_checkInDate" name="checkInDateTime"/>
    <input type="hidden" id="y_checkOutDate" name="checkOutDateTime"/>
    <input type="hidden" id="y_accommodationCode" name="accommodationCode"/>
    <input type="hidden" id="y_roomRateCodes" name="roomRateCodes"/>
    <input type="hidden" id="y_roomRateDates" name="roomRateDates"/>
    <input type="hidden" id="y_ratePlanCode" name="ratePlanCode"/>
    <input type="hidden" id="y_roomStayRefNumber" name="roomStayRefNumber"/>
    <input type="hidden" id="y_promotion" name="promotionCode" value="${promotionCode}"/>
    <input type="hidden" id="y_changeVar" name="change" />
    <input type="hidden" id="y_onlyAccommodationChange" name="onlyAccommodationChange" />


    <c:forEach var="roomCandidatesEntry" items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}" varStatus='idx'>
        <c:forEach var="entry" items="${roomCandidatesEntry.passengerTypeQuantityList}" varStatus="i">

	<form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" value="${entry.passengerType.code}" id="y_passengerCode" type="hidden" readonly="true" />
	<form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" id="y_passengerQuantity" value="${entry.quantity}" type="hidden" readonly="true" />
    <form:input path="roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].childAges" id="y_childAges" value="${entry.childAgesArray}" type="hidden" readonly="true" />


        </c:forEach>
    </c:forEach>
</form:form>
