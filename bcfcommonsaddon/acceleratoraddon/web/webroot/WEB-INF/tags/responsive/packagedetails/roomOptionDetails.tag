<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ attribute name="roomStays" required="true" type="java.util.List"%>
<%@ attribute name="reservedRoomStayRefNumber" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="accommodationDetails" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="packagelisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagelisting"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="accommodation-items clearfix pl-0">
	<c:forEach var="roomStay" items="${roomStays}" varStatus="roomStayIdx">
		<c:if test="${roomStay.roomStayRefNumber eq reservedRoomStayRefNumber && fn:length(roomStay.ratePlans) > 0}">

			<div class="y_roomStayContainer">
				<fmt:formatDate value="${roomStay.checkInDate}" var="checkInDateFormatted" pattern="MM/dd/yyyy" />
				<fmt:formatDate value="${roomStay.checkOutDate}" var="checkOutDateFormatted" pattern="MM/dd/yyyy" />
				<input type="hidden" name="checkInDate" class="y_checkInDate" value="${fn:escapeXml(checkInDateFormatted)}"> <input type="hidden" name="checkOutDate" class="y_checkOutDate" value="${fn:escapeXml(checkOutDateFormatted)}"> <input type="hidden" name="roomStayRefNumber"
					class="y_roomStayRefNumber" value="${fn:escapeXml(reservedRoomStayRefNumber)}"> <input type="hidden" name="numberOfRooms" class="y_numberOfRooms" value="${travelFinderForm.accommodationFinderForm.numberOfRooms}">
				<c:forEach var="roomType" items="${roomStay.roomTypes}">
					<input type="hidden" name="accommodationCode" class="y_accommodationCode" value="${fn:escapeXml(roomType.code)}" />
					<div class="package-gray-box price-option accommodation-grey-bg">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<div class="accommodation-details available-room-img">
									<accommodationDetails:roomTypeDetails roomType="${roomType}" roomStay="${roomStay}"/>
								</div>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
								<div class="accommodation-features">
									<div class="row">
										<div class="col-lg-7 col-md-7 col-sm-6 col-xs-7">
											<h5>
												<strong>${fn:escapeXml(roomType.name)}</strong>
											</h5>
											<ul class="ul-disc clearfix y_features">
									            <accommodationDetails:roomTypeDescription roomType="${roomType}" roomStay="${roomStay}"/>
											</ul>
											<a href="#" class="more hidden"> <spring:theme code="text.accommodation.details.roomstay.features.more" /> <span class="bcf bcf-icon-down-arrow"></span>
											</a>
											<a href="#" class="less hidden"> <spring:theme code="text.accommodation.details.roomstay.features.less" /> <span class="bcf bcf-icon-up-arrow"></span>
											</a>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-6 col-xs-5 border-left m-text-center available-room-right">
											<legend class="sr-only">
												<spring:theme code="text.package.details.room.selection.sr" arguments="Room selection" />
											</legend>
											<c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planID">


												<c:if test="${hidePriceAndBooking == null }">

													<p>
														<input type="hidden" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
														<c:set var="roomRate" value="${ratePlan.roomRates[0]}" />
														<fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
														<input type="hidden" name="roomRate${roomRateIdx.index}" code="${fn:escapeXml(roomRate.code)}" value="${roomRateDateFormatted}" class="y_roomRate" />
														<c:choose>
															<c:when test="${not empty ratePlan.perPersonPlanPrice and empty showPerRoomPrice}">
																<packagelisting:packagePerPersonPrice perPersonPackagePrice="${ratePlan.perPersonPlanPrice}" lengthOfStay="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse.lengthOfStay}" />
															</c:when>
															<c:otherwise>
																<packagelisting:packagePerRoomPrice discount="${ratePlan.totalDiscount}" perPersonPackagePrice="${ratePlan.perPersonPlanPrice}" lengthOfStay="${packageAvailabilityResponse.accommodationPackageResponse.accommodationAvailabilityResponse.lengthOfStay}" />
															</c:otherwise>
														</c:choose>
													</p>
													<p class=" clearfix m-text-center fnt-14" for="room-${fn:escapeXml(reservedRoomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}">
														<span class="totalPrice"> <c:choose>
																<c:when test="${isAmendment}">
																	<c:choose>
																		<c:when test="${ratePlan.actualRate.value>0}">
																			<spring:theme code="text.accommodation.additional.total.amount" />
																		</c:when>
																		<c:otherwise>
																			<spring:theme code="text.accommodation.total.refund.amount" />
																		</c:otherwise>
																	</c:choose>
																</c:when>
																<c:otherwise>
																	<spring:theme code="text.accommodation.details.accommodation.total.price" />
																</c:otherwise>
															</c:choose> &nbsp;
															<p class=" m-text-center">
															    <b><format:price priceData="${ratePlan.actualRate}"/></b>

															</p>
														</span> <span>${fn:escapeXml(ratePlan.priceDifference.formattedValue)}</span>
													</p>
												</c:if>
											</c:forEach>
											<div class="text-center">

											<c:set var="changeValue" value="false"/>
                                            <c:if test="${not empty modify}">
                                                <c:set var="changeValue" value="${modify}"/>
                                            </c:if>

                                            <input type="hidden" name="onlyAccommodationChange" id="onlyAccommodationChange" value="${travelFinderForm.onlyAccommodationChange}"/>
                                            <input type="hidden" name="changeVar" id="changeVar" value="${changeValue}"/>
												<c:if test="${hidePriceAndBooking == null}">
													<fieldset>
														<legend class="sr-only">
															<spring:theme code="text.package.details.room.selection.sr" arguments="Room selection" />
														</legend>

														<c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planID">

															<p class="y_ratePlanAttributes">
																<c:set var="stock" value="${ratePlan.availableQuantity}" />

																	<input type="hidden" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
																	<c:forEach var="roomRate" items="${ratePlan.roomRates}" varStatus="roomRateIdx">
																		<fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
																		<input type="hidden" name="roomRate${roomRateIdx.index}" code="${fn:escapeXml(roomRate.code)}" value="${roomRateDateFormatted}" class="y_roomRate" />
																	</c:forEach>
																	<span class="price-select custom-btn margin-top-30">
																	 <c:choose>
                                                                         <c:when test="${ratePlan.availabilityStatus eq 'NOT_BOOKABLE_ONLINE'}">
                                                                            <a href="${fn:escapeXml(request.contextPath)}/get-quote?name=${roomRate.code}" class="btn btn-primary">
                                                                                <spring:theme code="text.package.details.room.button.notBookableOnline" />
                                                                            </a>
                                                                         </c:when>
                                                                         <c:when test="${ratePlan.availabilityStatus eq 'SOLD_OUT'}">
                                                                            <div class="sold-out-text">
                                                                                <spring:theme code="text.package.details.room.button.notAvailable" />
                                                                            </div>
                                                                         </c:when>
                                                                         <c:otherwise>
                                                                             <c:choose>
                                                                                    <c:when test="${ratePlan.availabilityStatus eq 'ON_REQUEST'}">
                                                                                        <c:set var="bookingTitle" value="Request booking" />
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <c:set var="bookingTitle" value="Book This Room" />
                                                                                    </c:otherwise>
                                                                                </c:choose> <input id="room-${fn:escapeXml(reservedRoomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}" type="radio" name="room_${fn:escapeXml(reservedRoomStayRefNumber)}" class="y_changeAccommodationButton hidden" />
                                                                                <c:choose>
                                                                                    <c:when test="${travelFinderForm.onlyAccommodationChange || modify eq true}">
                                                                                        <input id="bookNow" type="button" value="${bookingTitle}" class="y_addAccommodationToCart btn btn-primary" />

                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <input id="bookNow" type="button" value="${bookingTitle}" class="custom-btn y_addPackageToCart btn btn-primary" />
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:otherwise>
																		</c:choose>
																	</span>
																	</label>
																	</span>

															</p>
														</c:forEach>
													</fieldset>
												</c:if>
											</div>
										</div>
									</div>
								</div>
								<div class="margin-top-20">
									<c:if test="${hidePriceAndBooking == null}">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center margin-bottom-20">
											<fieldset>
												<legend class="sr-only">
													<spring:theme code="text.package.details.room.selection.sr" arguments="Room selection" />
												</legend>
												<c:forEach var="ratePlan" items="${roomStay.ratePlans}" varStatus="planID">
													<p class="y_ratePlanAttributes clearfix">
														<c:set var="stock" value="${ratePlan.availableQuantity}" />

															<input type="hidden" name="ratePlanCode" value="${fn:escapeXml(ratePlan.code)}" class="y_ratePlanCode" />
															<c:forEach var="roomRate" items="${ratePlan.roomRates}" varStatus="roomRateIdx">
									    						<fmt:formatDate value="${roomRate.stayDateRange.startTime}" var="roomRateDateFormatted" pattern="MM/dd/yyyy" />
																<input type="hidden" name="roomRate${roomRateIdx.index}" code="${fn:escapeXml(roomRate.code)}" value="${roomRateDateFormatted}" class="y_roomRate" />
															</c:forEach>
															</label>
															</span>

													</p>
												</c:forEach>
											</fieldset>
										</div>
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<c:if test="${hidePriceAndBooking == null}">
						<div class="row justify-content-end">
							<div class="col-sm-12 col-md-6">
								<p>
									<input id="room-${fn:escapeXml(reservedRoomStayRefNumber)}_ratePlan-${planID.index}_option-${roomStayIdx.index}" type="radio" name="room_${fn:escapeXml(reservedRoomStayRefNumber)}" class="y_changeAccommodationButton hidden" />
								</p>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
	</c:forEach>
</div>
