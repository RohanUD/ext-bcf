<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="isDynamicPackage" required="true" type="java.lang.Boolean"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<c:url var="addBundleToCartUrl" value="/cart/add-transport-bundle" />
<form:form id="packageAddBundleToCartForm-${fn:escapeXml(pricedItinerary.originDestinationRefNumber)}" name="addBundleToCartForm" action="${addBundleToCartUrl}" method="POST" class="y_packageAddBundleToCartForm">
	<input type="hidden" readonly="readonly" name="travelRouteCode" value="${fn:escapeXml(pricedItinerary.itinerary.route.code)}" />
	<input type="hidden" readonly="readonly" name="originDestinationRefNumber" value="${fn:escapeXml(pricedItinerary.originDestinationRefNumber)}" />
	<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTypeName" value="${fn:escapeXml(itineraryPricingInfo.bundleTypeName)}" />
	<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleType" value="${fn:escapeXml(itineraryPricingInfo.bundleType)}" />
	<input type="hidden" readonly="readonly" name="itineraryPricingInfo.itineraryIdentifier" value="${fn:escapeXml(itineraryPricingInfo.itineraryIdentifier)}" />
	<input type="hidden" readonly="readonly" name="dynamicPackage" value="${fn:escapeXml(isDynamicPackage)}" />
	<c:forEach var="entry" items="${itineraryPricingInfo.bundleTemplates}" varStatus="idx1">
		<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].fareProductBundleTemplateId" value="${fn:escapeXml(entry.fareProductBundleTemplateId)}" />
		<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].bundleType" value="${fn:escapeXml(entry.bundleType)}" />
		<c:forEach var="fareProductEntry" items="${entry.fareProducts}" varStatus="idx2">
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].fareProducts[${idx2.index}].code" value="${fn:escapeXml(fareProductEntry.code)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].fareProducts[${idx2.index}].bookingClass" value="${fn:escapeXml(fareProductEntry.bookingClass)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].fareProducts[${idx2.index}].fareBasisCode" value="${fn:escapeXml(fareProductEntry.fareBasisCode)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(idx1.index)}].fareProducts[${fn:escapeXml(idx2.index)}].fareProductType" value="${fn:escapeXml(fareProductEntry.fareProductType)}" />
		</c:forEach>
		<c:forEach var="transportOfferingEntry" items="${entry.transportOfferings}" varStatus="idx3">
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].code" value="${fn:escapeXml(transportOfferingEntry.code)}" />
			<fmt:formatDate value="${transportOfferingEntry.departureTime}" var="formattedDepartureDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
			<fmt:formatDate value="${transportOfferingEntry.arrivalTime}" var="formattedArrivalDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].departureTime" value="${fn:escapeXml(formattedDepartureDate)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].departureTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.departureTimeZoneId)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].arrivalTime" value="${fn:escapeXml(formattedArrivalDate)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].arrivalTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.arrivalTimeZoneId)}" />
			<input type="hidden" readonly="readonly" name="itineraryPricingInfo.bundleTemplates[${idx1.index}].transportOfferings[${idx3.index}].sector.code" value="${fn:escapeXml(transportOfferingEntry.sector.code)}" />
		</c:forEach>
	</c:forEach>
	<c:forEach var="entry" items="${itineraryPricingInfo.ptcFareBreakdownDatas}" varStatus="i">
		<input type="hidden" readonly="readonly" name="passengerTypeQuantityList[${i.index}].passengerType.code" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.code)}" />
		<input type="hidden" readonly="readonly" name="passengerTypeQuantityList[${i.index}].passengerType.name" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.name)}" />
		<input type="hidden" readonly="readonly" name="passengerTypeQuantityList[${i.index}].quantity" value="${fn:escapeXml(entry.passengerTypeQuantity.quantity)}" />
		<input type="hidden" readonly="readonly" name="passengerTypeQuantityList[${i.index}].passengerType.bcfCode" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.bcfCode)}" />
	</c:forEach>
	<c:forEach var="entry" items="${itineraryPricingInfo.vehicleFareBreakdownDatas}" varStatus="i">
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].vehicleType.code" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.code)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].length" value="${fn:escapeXml(entry.vehicleTypeQuantity.length)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].width" value="${fn:escapeXml(entry.vehicleTypeQuantity.width)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].height" value="${fn:escapeXml(entry.vehicleTypeQuantity.height)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].carryingLivestock" value="${fn:escapeXml(entry.vehicleTypeQuantity.carryingLivestock)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].vehicleWithSidecarOrTrailer" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleWithSidecarOrTrailer)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].vehicleType.category" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.category)}" />
		<input type="hidden" readonly="readonly" name="vehicleTypeQuantityList[${i.index}].qty" value="${fn:escapeXml(entry.vehicleTypeQuantity.qty)}" />
	</c:forEach>
	<c:forEach var="entry" items="${itineraryPricingInfo.otherCharges}" varStatus="i">
		<input type="hidden" readonly="readonly" name="otherChargeDataList[${i.index}].code" value="${entry.code}" />
		<input type="hidden" readonly="readonly" name="otherChargeDataList[${i.index}].quantity" value="${entry.quantity}" />
	</c:forEach>

	<input type="hidden" readonly="readonly" name="errorClass" value="vehicleTypeQuantityList[" />
</form:form>
