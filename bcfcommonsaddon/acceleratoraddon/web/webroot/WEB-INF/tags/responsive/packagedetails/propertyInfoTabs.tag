<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="accommodationDetails"
	tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="property" required="true"
	type="de.hybris.platform.commercefacades.accommodation.PropertyData"%>
<%@ attribute name="numroomtype" required="false"
	type="java.lang.Integer"%>
<%@ attribute name="accommodationAvailabilityResponse" required="true"
	type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData"%>
<%@ taglib prefix="packageDetails"
	tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="passenger"
	tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/passenger"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="maxPassengerQuantity" value="${maxPassengerQuantity}" />
<c:set var="maxChildQuantity" value="${maxChildQuantity}" />
<c:set var="editPackageUrl"
	   value="${fn:escapeXml(request.contextPath)}/vacations/hotels/edit/${accommodationAvailabilityResponse.accommodationReference.accommodationOfferingCode }"/>
<div id="maxPassengerCount" class="hidden">${maxPassengerQuantity}</div>
<div id="accommodationsQuantity" class="hidden">${accommodationsQuantity}</div>
<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="tab-wrapper full-tabs package-details-tabs">
			<ul class="nav nav-tabs save-nav-tab-on-change" role="tablist">
				<li role="presentation" class="pl-1"><a href="#tab-01"
					aria-controls="tab-01" role="tab" data-toggle="tab"
					class="text-center" aria-selected="true"> <spring:theme
							code="text.accommodation.details.property.about" text="About" />
				</a></li>
				<li role="presentation" class="active"><a href="#tab-02"
					aria-controls="tab-02" role="tab" data-toggle="tab"
					class="text-center"> <spring:theme
							code="text.accommodation.details.property.room.types"
							text="Room Types" /> (${numroomtype})
				</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade" id="tab-01">
					<div class="about-us-box">
						<accommodationDetails:propertyOverview property="${property}" />
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade editPackageTab active in"
					id="tab-02">
					<c:if test="${empty hidePriceAndBooking}">
						<div id="editPackageData" class="p-0">
							<div class="padding-50 pb-3">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 room-type-stay-sec">
										<p class="light-gray-text text-sm">
											<spring:theme code="text.tripwidget.stay" />
										</p>
										<fmt:parseDate
											value="${travelFinderForm.accommodationFinderForm.checkInDateTime}"
											var="parsedCheckInDateTime" pattern="MM/dd/yyyy" />
										<fmt:formatDate value="${parsedCheckInDateTime}"
											var="formattedParsedDepartDate" pattern="E, MMM dd" />
										<fmt:parseDate
											value="${travelFinderForm.accommodationFinderForm.checkOutDateTime}"
											var="parsedCheckOutDateTime" pattern="MM/dd/yyyy" />
										<fmt:formatDate value="${parsedCheckOutDateTime}"
											var="formattedParsedReturnDate" pattern="E, MMM dd" />
										<p>
											<strong> ${formattedParsedDepartDate} &nbsp;<span
												class="ml-2"><spring:theme code="text.tripwidget.to" /></span>
												<span class="ml-2">${formattedParsedReturnDate}</span></strong>
										</p>
										<c:set var="numberOfNights">
											<fmt:parseNumber
												value="${((parsedCheckOutDateTime.time-parsedCheckInDateTime.time)/(1000*60*60*24))}"
												integerOnly="true" />
										</c:set>
										<c:choose>
											<c:when test="${numberOfNights eq 1}">
												<spring:theme code="text.tripwidget.night"
													arguments="${numberOfNights}" />
											</c:when>
											<c:otherwise>
											<p class="text-sm">(<spring:theme code="text.tripwidget.nights"
													arguments="${numberOfNights}" />)</p>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
								<c:url var="travelFinderUrl" value="/view/PackageFinderComponentController/search" />
								 <div class="custom-error-3 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>
								<div class="room-type-edit-box mt-4">
									<form:form commandName="travelFinderForm"
										action="${travelFinderUrl}" method="POST"
										class="fe-validate form-background form-booking-trip"
										id="y_travelFinderForm">

   										 <form:input type="hidden" path="accommodationFinderForm.modify" value="${modify}"/>
										
                                        <c:set var="quantity" value="0"/>
                                        <c:choose>
                                            <c:when test="${travelFinderForm.accommodationFinderForm.numberOfRooms eq 1}">
                                                <c:set var="roomStayCandidate" value="${travelFinderForm.accommodationFinderForm.roomStayCandidates[0]}"/>
                                                <c:forEach var="passengerQuantity" items="${roomStayCandidate.passengerTypeQuantityList}">
                                                    <c:set var="quantity" value="${quantity + passengerQuantity.quantity}"/>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="roomStayCandidates" items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}">
                                                    <c:forEach var="passengerQuantity" items="${roomStayCandidates.passengerTypeQuantityList}">
                                                        <c:set var="quantity" value="${quantity + passengerQuantity.quantity}"/>
                                                    </c:forEach>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                

										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											    <div class="edit-room-icons">
			                                        <span class="bcf bcf-icon-hotel-room">
			                                              <span class="caps-f">
			                                                ${travelFinderForm.accommodationFinderForm.numberOfRooms}
			                                              </span>
			                                              </span>
			                                            <span class="bcf bcf-icon-account">
			                                            <span class="caps-f">
			                                                ${quantity}
			                                            </span>
			                                        </span>
                                       			</div>
                                    		</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 package_edit">
												<div class="collapseEditClose edit pull-right" id="collapseEdit">
													<span class="bcf bcf-icon-edit bcf-bule-icon"></span> edit
												</div>
												<div class="collapseEditClose edit pull-right" id="collapseClose" style="display:none">
                                                    <span class="bcf edit-close bcf-bule-icon"><span class="edit-close-icon">&times;</span></span> Cancel
                                                </div>
											</div>
										</div>
                                    	
                                    	 <div class="checkVacationInAndCheckOutDiv">
											<div class="row margin-top-20">

												<div
													class="col-md-8 col-lg-7 col-sm-12 col-xs-12 vacation-calender-section hotel-detail-tab-calender">
													<ul class="nav nav-tabs vacation-calender">
														<li class="tab-links tab-depart"><label>Check-in date</label>
															<a data-toggle="tab" href="#check-in" class="nav-link component-to-top">
																<div class="vacation-calen-box vacation-ui-depart">
																	<span class="vacation-calen-date-txt"><spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" /></span>
                                                                    <span class="vacation-calen-year-txt current-year-custom-date">${travelFinderForm.accommodationFinderForm.checkInDateTime}<i
																		class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
																		aria-hidden="true"></i>
																</div>
														</a></li>
														<li class="tab-links tab-return"><label>Check-out date</label>
															<a data-toggle="tab" href="#check-out" class="nav-link component-to-top">
																<div class="vacation-calen-box vacation-ui-return">
																	<span class="vacation-calen-date-txt">Date</span> <span
																		class="vacation-calen-year-txt current-year-custom-date">${travelFinderForm.accommodationFinderForm.checkOutDateTime}</span> <i
																		class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
																		aria-hidden="true"></i>
																</div>
														</a></li>
													</ul>

													<div class="tab-content">
														<div id="check-in"
															class="tab-pane input-required-wrap depart-calendar">
															<div class="bc-dropdown calender-box tab-content-depart"
																id="js-depart">
																<label>Search date: mm/dd/yyyy</label>
																  <form:input type="text"
																	path="accommodationFinderForm.checkInDateTime"
																	class="datePickerDeparting datepicker bc-dropdown depart datepicker-input y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckIn"
																	placeholder="Check In" autocomplete="off" />
																<div id="datepicker" class="bc-dropdown--big"></div>
															</div>
														</div>
														<div id="check-out"
															class="tab-pane input-required-wrap return-calendar">
															<div class="bc-dropdown calender-box tab-content-return"
																id="js-return">
																<label>Search date: mm/dd/yyyy</label>
																 <form:input type="text"
																	path="accommodationFinderForm.checkOutDateTime"
																	class="datePickerReturning datepicker arrive datepicker-input bc-dropdown y_${fn:escapeXml( idPrefix)}FinderDatePickerCheckOut"
																	placeholder="Check Out" autocomplete="off" />
																<div id="datepicker1" class="bc-dropdown--big"></div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="row margin-top-20">
												<div
													class="col-lg-2 col-md-3 col-sm-3 col-xs-12 y_passengerWrapper ">
													<label class="font-weight-light"> <spring:theme
															code="text.vacation.packagefinder.numberofrooms.title"
															text="Number of rooms" />
													</label>
			                                        <c:set var="noOfRoomsMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.number.rooms.max.count')}"/>
													 <input type="hidden" id="noOfRoomsMax" name="noOfRoomsMax" value="${noOfRoomsMaxValue}"/>
													<div
														class="input-group input-btn-mp text-center mb-3 roomSelector">
														<span class="fa bcf bcf-icon-remove btn-number" disabled="disabled"
															data-type="minus"
															data-field="accommodationFinderForm.numberOfRooms"></span>
														<form:input type="text" id="y_roomQuantity"
															class="input-number min-input2"
															value="${travelFinderForm.accommodationFinderForm.numberOfRooms }"
															min="1" max="${noOfRoomsMaxValue}"
															path="accommodationFinderForm.numberOfRooms" />
														<span class="fa bcf bcf-icon-add btn-number" data-type="plus"
															data-field="accommodationFinderForm.numberOfRooms"></span>


													</div>
												</div>
												<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
													<div class="row">
														<c:forEach var="roomCandidatesEntry"
															items="${travelFinderForm.accommodationFinderForm.roomStayCandidates}"
															varStatus='idx'>
															<div
																class="col-xs-12 bc-accordion guest-types select-guest-custom package-edit-sec custom-room-all custom-room-${idx.index+1}">
																<label class="font-weight-light"> Room
																	${idx.index+1} </label>
																<div class="custom-accordion">
																	<h3>
																		<spring:theme
																			code="label.packagedetails.guestinfo.guest.dropdown.heading"
																			text="Select guests" />
																		<span class="custom-arrow"></span>
																	</h3>
																	<div class="vacationGuestUsers">

																		<c:forEach var="entry"
																			items="${roomCandidatesEntry.passengerTypeQuantityList}"
																			varStatus="i">
                                                                                <c:choose>
                                                                                    <c:when test="${entry.passengerType.code eq 'adult'}">
                                                                                        <c:set var="minQuantity" value="1" />
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <c:set var="minQuantity" value="0" />
                                                                                    </c:otherwise>
                                                                                </c:choose>
																			<div
																				class="col-lg-6 col-md-6 col-sm-12 col-xs-6 margin-top-10 y_passengerWrapper ">
																					<label
																						for="y_${fn:escapeXml(entry.passengerType.code)}">
																						<c:choose>
                                                                                             <c:when test="${entry.passengerType.code eq 'adult'}">
                                                                                                  <spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 19 yrs+" />
                                                                                             </c:when>
                                                                                             <c:otherwise>
                                                                                                 <spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-18 yrs" />
                                                                                             </c:otherwise>
                                                                                        </c:choose>
                                                                                     </label>

                                                                              <c:set var="adultMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.adult.max.count')}"/>
                                                                              <c:set var="childMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.child.max.count')}"/>
																				<div class="input-group input-btn-mp">
																					<span class="fa bcf bcf-icon-remove btn-number"
																						disabled="disabled" data-type="minus"
																						data-field="accommodationFinderForm.roomStayCandidates[${idx.index}].passengerTypeQuantityList[${i.index}].quantity"></span>
																					<form:input type="text"
																						path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${i.index}].quantity"
																						class="${fn:escapeXml(entry.passengerType.code)}-qty y_${fn:escapeXml(entry.passengerType.code)}Select"
																						value="${entry.quantity}" min="${minQuantity}"
																						max="${ entry.passengerType.code eq 'adult'?adultMaxValue:childMaxValue}" />

																					 <div id="room-number${idx.index}" class="room-number hidden">${idx.index}</div>

																					<span class="fa bcf bcf-icon-add btn-number"
																						data-type="plus"
																						data-field="accommodationFinderForm.roomStayCandidates[${idx.index}].passengerTypeQuantityList[${i.index}].quantity"></span>
                                                                                    <form:input
                            															path="accommodationFinderForm.roomStayCandidates[${ idx.index}].roomStayCandidateRefNumber"
                            															id="y_roomStayCandidateRefNumber" type="hidden" readonly="true" />

																					<form:input
																						path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code"
																						id="y_passengerCode" type="hidden" readonly="true" />
																					<form:input
																						path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name"
																						id="y_passengerTypeName" type="hidden"
																						readonly="true" />
																					<form:input
																						path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge"
																						type="hidden" readonly="true" />
																					<form:input
																						path="accommodationFinderForm.roomStayCandidates[${ idx.index}].passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge"
																						type="hidden" readonly="true" />

																				</div>

																			</div>
																			<c:if test="${entry.passengerType.code eq 'child'}">

                                                                                <div id="child-box-wrapper${ idx.index}${i.index}" class="mb-4 child-box-wrapper clearfix"></div>

                                                                                <c:forEach var="childAge" items="${entry.childAges}" varStatus="k">

                                                                                    <div id="childAge${ idx.index}${i.index}${k.index}" class="hidden">${childAge}</div>

                                                                                </c:forEach>

                                                                            </c:if>
																		</c:forEach>

																	</div>
																</div>
																<div class="vacation-guest-gray hidden">
																	<div class="row">
																		<div class="col-lg-12 col-md-12 col-sm-12">
																			<c:forEach var="entry"
																				items="${roomCandidatesEntry.passengerTypeQuantityList}"
																				varStatus="i">
																				<p>
																					<span
																						class="${fn:escapeXml(entry.passengerType.code)}-count">0</span>
																					x
																					<c:choose>
                                                                                     <c:when test="${entry.passengerType.code eq 'adult'}">
                                                                                          <spring:theme code="label.farefinder.vacation.rooms.adult" text="Adult 12 yrs+" />
                                                                                     </c:when>
                                                                                     <c:otherwise>
                                                                                         <spring:theme code="label.farefinder.vacation.rooms.child" text="Child 0-12 yrs" />
                                                                                     </c:otherwise>
                                                                                  </c:choose>
																				</p>
																			</c:forEach>
																		</div>
																	</div>
																</div>

															</div>

														</c:forEach>

														<div class="snippets hidden">
                                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 child-box">
                                                                  <label class="child-label-name"><spring:theme code="label.farefinder.vacation.rooms.child.title" text="Child" /> &nbsp;<span class="child-label-name-span">1</span> <spring:theme code="label.farefinder.vacation.rooms.age" text="age" /></label>
                                                                  <select class="form-control custom-select">

                                                                  </select>
                                                              </div>
                                                         </div>
													</div>

													<form:hidden path="accommodationFinderForm.destinationLocation" class="y_accommodationDestinationLocation" value="${travelFinderForm.accommodationFinderForm.destinationLocation}" />

                                                    <form:hidden path="travelRoute" class="y_travelFinderTravelRoute" value="${travelFinderForm.travelRoute}"/>
												</div>


												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
													<label>&nbsp;</label>
													<div>
													<form:button class="confirm-btn confirm-btn--blue" value="Submit">
                                                        <spring:theme code="text.cms.accommodationfinder.button.submit" text="Search" />
                                                    </form:button>

														</a>
													</div>
												</div>
											</div>

										</div>
									</form:form>
								</div>
								<div class="row mb-3">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<h4 class="vacation-h2">
											<c:choose>
											<c:when test="${numroomtype eq 0}">
											<spring:theme code="text.package.details.accommodation.not.available" text="No rooms available"/>
											<spring:theme code="text.package.details.accommodation.change.parameter" text="Modify Search Parameters"/>
											</c:when>
												<c:when test="${numroomtype gt 1}">
                                        ${numroomtype}&nbsp<spring:theme
														code="text.accommodation.details.property.available.room.types"
														text="Room Types" />
												</c:when>
												<c:otherwise>
                                        ${numroomtype}&nbsp<spring:theme
														code="text.accommodation.details.property.available.room.type"
														text="Room Types" />
												</c:otherwise>
											</c:choose>
										</h4>
									</div>
								</div>

							</div>
					</c:if>


					<div class="row mb-3">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<c:choose>
								<c:when test="${not empty hidePriceAndBooking}">
									<packageDetails:roomOptionsForPackageDeal
										accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
								</c:when>
								<c:otherwise>
									<packageDetails:roomOptions
										accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
								</c:otherwise>
							</c:choose>
							<c:set var="property" value="${property}" />
						</div>
					</div>
					<div class="row mt-5 mb-5">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<div class="back-top-btn">
								<a href="#"><span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span><strong>
										Back to top</strong></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
