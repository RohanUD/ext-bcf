<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ attribute name="accommodationAvailabilityResponse" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty accommodationAvailabilityResponse.roomStays}">
	<div id="roomOptionsCollapse">
		<c:forEach var="reservedRoomStay" items="${accommodationAvailabilityResponse.reservedRoomStays}" varStatus="idx">
			<c:if test="${!reservedRoomStay.nonModifiable}">
				<packageDetails:roomOptionDetails roomStays="${accommodationAvailabilityResponse.roomStayGroups}" reservedRoomStayRefNumber="${reservedRoomStay.roomStayRefNumber}"/>
			</c:if>
		</c:forEach>
		<c:set var="fareSelectionProvided" value="${packageAvailabilityResponse.transportPackageResponse.fareSearchResponse}" />
		<c:if test="${not empty fareSelectionProvided.pricedItineraries}">
			<packageDetails:selectedTransportation isDynamicPackage="true" transportPackageResponse="${packageAvailabilityResponse.transportPackageResponse}" />
		</c:if>
	</div>
</c:if>
