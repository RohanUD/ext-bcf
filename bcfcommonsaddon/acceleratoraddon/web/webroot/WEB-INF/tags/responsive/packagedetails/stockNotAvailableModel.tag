<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div>
		<div class="text-center">
			<div class="alert alert-warning cookies-msg-hide" id="js-alert-msg-hide">
			<span>
				<h3><spring:theme code="text.dealdetails.stock.unavailable.title"/> </h3>
			</span>
					<spring:theme code="text.dealdetails.stock.unavailable" /><span class="alert-btn-span">
					<button class="btn btn-transparent-white alert-btn" data-dismiss="alert" aria-label="Close">Close</button>
				</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
