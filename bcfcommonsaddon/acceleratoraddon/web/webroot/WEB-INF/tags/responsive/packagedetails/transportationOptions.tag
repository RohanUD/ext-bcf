<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="transportPackageResponse" required="true" type="de.hybris.platform.commercefacades.packages.response.TransportPackageResponseData"%>
<%@ attribute name="dealFormPath" required="false" type="java.lang.String"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty transportPackageResponse.fareSearchResponse && not empty transportPackageResponse.fareSearchResponse.pricedItineraries}">
	<div class="row form-group y_packageFareSelection">
		<div class="col-xs-12 col-sm-offset-8 col-sm-offset-8 col-sm-4">
			<button class="y_flightOptionsCollapse btn btn-primary col-xs-12" type="button" data-toggle="collapse" data-target="#flightOptionsCollapse" aria-expanded="false" aria-controls="flightOptionsCollapse">
				<span class="show-text">
					<spring:theme code="text.package.details.button.show.transportation.options" text="Show more ferry options" />
				</span>
				<span class="hide-text hidden">
					<spring:theme code="text.package.details.button.hide.transportation.options" text="Hide ferry options" />
				</span>
			</button>
		</div>
	</div>
	<c:set var="tripType" value="SINGLE" />
	<c:forEach items="${transportPackageResponse.fareSearchResponse.pricedItineraries}" var="pricedItinerary">
		<c:if test="${pricedItinerary.originDestinationRefNumber==1}">
			<c:set var="tripType" value="RETURN" />
		</c:if>
	</c:forEach>
	<div id="flightOptionsCollapse" class="collapse y_fareSelectionSection fare-table-inner-wrap">
		<input type="hidden" id="y_tripType" value="${fn:escapeXml(tripType)}" />
		<%-- Outbound offerings table --%>
		<div class="panel panel-accommodation panel-journey-choices">
			<div class="panel-heading">
				<h3 class="panel-title">
					${fn:escapeXml(transportPackageResponse.fareSearchResponse.pricedItineraries[0].itinerary.route.origin.name)}&nbsp;
					<span aria-hidden="true" class="glyphicon glyphicon-arrow-right"></span>
					${fn:escapeXml(transportPackageResponse.fareSearchResponse.pricedItineraries[0].itinerary.route.destination.name)}
				</h3>
			</div>

			<div id="y_outbound" class="y_fareResultTabWrapper clearfix">
				<packageDetails:packageOfferingList dealFormPath="${dealFormPath}" fareSelection="${transportPackageResponse.fareSearchResponse}" refNumber="${outboundRefNumber}" />
			</div>
		</div>
		<c:if test="${tripType == 'RETURN'}">
			<%-- Return offerings table --%>
			<div class="panel panel-accommodation panel-journey-choices">
				<div class="panel-heading">
					<h3 class="panel-title">
						${fn:escapeXml(transportPackageResponse.fareSearchResponse.pricedItineraries[0].itinerary.route.destination.name)}&nbsp;
						<span aria-hidden="true" class="glyphicon glyphicon-arrow-right"></span>
						${fn:escapeXml(transportPackageResponse.fareSearchResponse.pricedItineraries[0].itinerary.route.origin.name)}
					</h3>
				</div>
				<div id="y_inbound" class="y_fareResultTabWrapper clearfix">
					<packageDetails:packageOfferingList dealFormPath="${dealFormPath}" fareSelection="${transportPackageResponse.fareSearchResponse}" refNumber="${inboundRefNumber}" />
				</div>
			</div>
		</c:if>
	</div>
</c:if>
