<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<%@ attribute name="accommodationAvailabilityResponse" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationAvailabilityResponseData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty accommodationAvailabilityResponse.roomStays}">
	<packageDetails:changeAccommodationForm accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
    <input type="hidden" id="promotionAccommodationOfferingCode" name="promotionAccommodationOfferingCode" value="${promotionAccommodationOfferingCode}" />
    <input type="hidden" id="promotionRoomRateProducts" name="promotionRoomRateProducts" value="${promotionRoomRateProducts}" />
    <input type="hidden" id="promotionCode" name="promotionCode" value="${promotionCode}" />

	<cms:component uid="PackageFinderComponent" />
    <div class="ajax-post-response">
        <packageDetails:roomOptionsCollapse accommodationAvailabilityResponse="${accommodationAvailabilityResponse}" />
    </div>
</c:if>
