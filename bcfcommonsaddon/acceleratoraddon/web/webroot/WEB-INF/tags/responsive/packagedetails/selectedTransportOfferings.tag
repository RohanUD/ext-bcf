<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itinerary" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryData"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItineraryIndex" required="true" type="java.lang.Integer"%>
<%@ attribute name="originDestinationRefNum" type="java.lang.Integer"%>
<%@ taglib prefix="packageDetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagedetails"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="numberOfConnections" value="${fn:length(itinerary.originDestinationOptions[0].transportOfferings)}" />
<c:choose>
	<c:when test="${numberOfConnections > 1}">
		<c:set var="firstOffering" value="${itinerary.originDestinationOptions[0].transportOfferings[0]}" />
		<c:set var="lastOffering" value="${itinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
	</c:when>
	<c:otherwise>
		<c:set var="firstOffering" value="${itinerary.originDestinationOptions[0].transportOfferings[0]}" />
		<c:set var="lastOffering" value="${firstOffering}" />
	</c:otherwise>
</c:choose>
<div class="journey-wrapper">
	<div class="panel-body">
		<div class="col-xs-12">
			<div class="journey-details row">
				<c:if test="${originDestinationRefNum == 0}">
					<input id="y_selectedDepartureTime_${originDestinationRefNum}" type="hidden" value="${firstOffering.departureTime.time}" />
				</c:if>
				<c:if test="${originDestinationRefNum == 1}">
					<input id="y_selectedArrivalTime_${originDestinationRefNum}" type="hidden" value="${lastOffering.arrivalTime.time}" />
				</c:if>
			</div>
		</div>
	</div>
</div>
