<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="originToDestinationMapping" required="true" type="java.util.Map"%>
<%@ attribute name="travelRoutelist" required="false" type="java.util.List"%>
<%@ attribute name="currentConditionsByTerminals" required="false" type="java.util.Map"%>

    <c:if test="${not empty travelRoutelist}">
    <div id="listOfTravelRouteCount" value="${travelRoutelist.size()}"></div>
    <c:forEach items="${travelRoutelist}" var="travelRoute" varStatus="status">
      <c:if test="${travelRoute.mapKmlFileUrl ne null}">
        <input type="hidden" id="routeOriginDestination${status.index}" data-terminalname="${travelRoute.origin.name}"
            data-routeoriginname="${travelRoute.origin.routeName}" data-routeorigincode="${travelRoute.origin.code}"
            data-mapkmlfile="${travelRoute.mapKmlFileUrl}" data-mapkmlinternalfilename="${travelRoute.origin.code}-${travelRoute.destination.code}"
            data-index="${status.index}"/>
         </c:if>
    </c:forEach>
    </c:if>

    <c:if test="${not empty currentConditionsByTerminals}">
    <div id="listOfCurrentConditionsCount" value="${currentConditionsByTerminals.size()}"></div>
    <c:forEach items="${currentConditionsByTerminals}" var="travelRouteMap" varStatus="statusidx">
    <div id="originDesitinationMapCount${statusidx.index}" value="${travelRouteMap.value.size()}"></div>
        <c:forEach items="${travelRouteMap.value}" var="travelRoute" varStatus="status">
           <c:if test="${travelRoute.mapKmlFileUrl ne null}">
            <input type="hidden" id="routeOriginDestination${statusidx.index}${status.index}"
                        data-mapkmlfile="${travelRoute.mapKmlFileUrl}" data-mapkmlinternalfilename="${travelRoute.sourceTerminalCode}-${travelRoute.destinationTerminalCode}" data-index="${status.index}"/>
            </c:if>
        </c:forEach>
    </c:forEach>
    </c:if>

    <c:set var="originFilteredName" value=""></c:set>
    <c:set var="destinationlist" value=""></c:set>
    <c:set var="destinationFilteredName" value=""></c:set>
        <div id="originToDestinationMappingCount" value="${originToDestinationMapping.size()}"></div>
        <c:forEach items="${originToDestinationMapping}" var="originDestination" varStatus="odstatus">
            <c:set var="origin" value="${originDestination.key}"/>
            <c:set var="destination" value="${originDestination.value}"/>
            <c:set var="routeOriginFilterSpace" value="${fn:replace(origin, ' ', '')}"></c:set>
            <c:set var="routeOriginFilterOpenBracket" value="${fn:replace(routeOriginFilterSpace, '(', '-')}"></c:set>
            <c:set var="originFilteredNameAndCode" value="${fn:replace(routeOriginFilterOpenBracket, ')', '')}"></c:set>
            <c:set var="routeOriginName" value="${fn:substringBefore(originFilteredNameAndCode, '|')}"></c:set>

            <c:set var="originFilteredCode" value="${fn:substringAfter(originFilteredNameAndCode, '|')}"></c:set>
            <c:set var="originFilteredName" value="${fn:toLowerCase(routeOriginName)}"></c:set>
            <c:set var="originNameWithNoFilter" value="${fn:substringBefore(origin, '|')}"></c:set>

            <input type="hidden" id="routeOrigin${odstatus.index}" data-originfilteredname="${originFilteredName}"
            data-originfilteredcode="${originFilteredCode}" data-originname="${originNameWithNoFilter}"
            data-index="${odstatus.index}"/>

            <input type="hidden" id="destinationCount${odstatus.index}" data-destinationcount="${destination.size()}"
            data-index="${odstatus.index}"/>
               <div id="destinationCount" value="${destination.size()}"></div>
               <c:forEach items="${destination}" var="destination" varStatus="dstatus">
                      <c:set var="routeDestinationFilterSpace" value="${fn:replace(destination, ' ', '')}"></c:set>
                      <c:set var="routeDestinationFilterOpenBracket" value="${fn:replace(routeDestinationFilterSpace, '(', '-')}"></c:set>
                      <c:set var="routeDestinationFilterCloseBracket" value="${fn:replace(routeDestinationFilterOpenBracket, ')', '')}"></c:set>
                      <c:set var="routeDestinationFilteredNameAndCode" value="${routeDestinationFilterCloseBracket}"></c:set>
                      <c:set var="routeDestinationName" value="${fn:substringBefore(routeDestinationFilteredNameAndCode, '|')}"></c:set>

                      <c:set var="destinationFilteredName" value="${fn:toLowerCase(routeDestinationName)}"></c:set>
                      <c:set var="destinationFilteredCode" value="${fn:substringAfter(routeDestinationFilteredNameAndCode, '|')}"></c:set>
                      <c:set var="destinationNameWithNoFilter" value="${fn:substringBefore(destination, '|')}"></c:set>

                      <input type="hidden" id="routeDestination${odstatus.index}${dstatus.index}"
                      data-destinationfilteredname="${destinationFilteredName}"
                      data-destinationfilteredcode="${destinationFilteredCode}"
                      data-destinationname="${destinationNameWithNoFilter}" data-index="${odstatus.index}${dstatus.index}"/>
               </c:forEach>
        </c:forEach>
