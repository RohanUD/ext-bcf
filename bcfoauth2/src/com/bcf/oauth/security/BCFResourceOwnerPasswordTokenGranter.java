/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.oauth.security;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import com.bcf.integration.activedirectory.service.ADUserService;
import com.bcf.integrations.core.exception.IntegrationException;


public class BCFResourceOwnerPasswordTokenGranter extends ResourceOwnerPasswordTokenGranter implements Serializable
{
	private transient ADUserService adUserService;
	private transient ADAuthenticationStrategy adAuthenticationStrategy;

	public BCFResourceOwnerPasswordTokenGranter(final AuthenticationManager authenticationManager,
			final AuthorizationServerTokenServices tokenServices,
			final ClientDetailsService clientDetailsService, final OAuth2RequestFactory requestFactory)
	{
		super(authenticationManager, tokenServices, clientDetailsService, requestFactory);
	}

	@Override
	protected OAuth2Authentication getOAuth2Authentication(final ClientDetails client, final TokenRequest tokenRequest)
	{
		final String username = tokenRequest.getRequestParameters().get(ADAuthenticationStrategy.USER);
		if (getAdAuthenticationStrategy().isADUserAuthenticationApplicable(username))
		{
			callADAuthenticationService(tokenRequest);
			final OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
			return new OAuth2Authentication(storedOAuth2Request,
					getAdAuthenticationStrategy().getUserPwdAuthenticationToken(username));
		}
		return super.getOAuth2Authentication(client, tokenRequest);
	}

	private void callADAuthenticationService(final TokenRequest tokenRequest)
	{
		try
		{
			getAdUserService()
					.authenticateAndUpdateADUser(tokenRequest.getRequestParameters().get(ADAuthenticationStrategy.USER),
							tokenRequest.getRequestParameters().get(ADAuthenticationStrategy.PASS));
		}
		catch (final IntegrationException e)
		{
			throw new InvalidGrantException(
					"Could not authenticate user: " + tokenRequest.getRequestParameters().get(ADAuthenticationStrategy.USER));
		}
	}

	public ADUserService getAdUserService()
	{
		return adUserService;
	}

	@Required
	public void setAdUserService(final ADUserService adUserService)
	{
		this.adUserService = adUserService;
	}

	public ADAuthenticationStrategy getAdAuthenticationStrategy()
	{
		return adAuthenticationStrategy;
	}

	@Required
	public void setAdAuthenticationStrategy(final ADAuthenticationStrategy adAuthenticationStrategy)
	{
		this.adAuthenticationStrategy = adAuthenticationStrategy;
	}
}
