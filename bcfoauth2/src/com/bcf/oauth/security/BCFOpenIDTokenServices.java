/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.oauth.security;

import de.hybris.platform.webservicescommons.model.OAuthAccessTokenModel;
import de.hybris.platform.webservicescommons.oauth2.token.dao.OAuthTokenDao;
import de.hybris.platform.webservicescommons.oauth2.token.provider.HybrisOpenIDTokenServices;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;


public class BCFOpenIDTokenServices extends HybrisOpenIDTokenServices
{
	private OAuthTokenDao authTokenDao;
	private ADAuthenticationStrategy adAuthenticationStrategy;

	@Override
	public OAuth2Authentication loadAuthentication(final String accessTokenValue) throws AuthenticationException,
			InvalidTokenException
	{
		try
		{
			final OAuthAccessTokenModel accessTokenById = getAuthTokenDao()
					.findAccessTokenById(getAdAuthenticationStrategy().getAuthenticationTokenKey(accessTokenValue));
			final String user = accessTokenById.getUser().getUid();
			if (!getAdAuthenticationStrategy().isADUserAuthenticationApplicable(user))
			{
				return super.loadAuthentication(accessTokenValue);
			}
			return (OAuth2Authentication) getAdAuthenticationStrategy().loadADAuthenticationToken(user);
		}
		catch (final Exception e)
		{
			throw new InvalidTokenException(e.getMessage(), e);
		}
	}

	public OAuthTokenDao getAuthTokenDao()
	{
		return authTokenDao;
	}

	@Required
	public void setAuthTokenDao(final OAuthTokenDao authTokenDao)
	{
		this.authTokenDao = authTokenDao;
	}

	public ADAuthenticationStrategy getAdAuthenticationStrategy()
	{
		return adAuthenticationStrategy;
	}

	@Required
	public void setAdAuthenticationStrategy(final ADAuthenticationStrategy adAuthenticationStrategy)
	{
		this.adAuthenticationStrategy = adAuthenticationStrategy;
	}
}
