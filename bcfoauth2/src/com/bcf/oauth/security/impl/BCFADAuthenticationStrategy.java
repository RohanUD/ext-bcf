/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.oauth.security.impl;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import com.bcf.core.util.StreamUtil;
import com.bcf.oauth.security.ADAuthenticationStrategy;


public class BCFADAuthenticationStrategy implements ADAuthenticationStrategy
{
	private Map<String, String> adUserRoleMap;
	private UserService userService;

	@Override
	public boolean isADUserAuthenticationApplicable(final String username)
	{
		try
		{
			return BooleanUtils.isTrue(getUserService().getUserForUID(username).getLdapaccount());
		}
		catch (final UnknownIdentifierException e)
		{
			return true;
		}
	}

	@Override
	public String getAuthenticationTokenKey(final String tokenValue)
	{
		try
		{
			final MessageDigest messageDigest = MessageDigest.getInstance(ADAuthenticationStrategy.SHA_256);
			return String
					.format(ADAuthenticationStrategy.TOKEN_KEY_FORMAT, new BigInteger(ADAuthenticationStrategy.TOKEN_SIG_NUM,
							messageDigest.digest(tokenValue.getBytes(StandardCharsets.UTF_8))));
		}
		catch (final NoSuchAlgorithmException e)
		{
			throw new IllegalStateException("SHA-256 algorithm not supported.");
		}
	}

	@Override
	public Authentication loadADAuthenticationToken(final String username)
	{
		final OAuth2Request oAuth2Request = new OAuth2Request(getAuthenticationTokenDetails(username),
				ADAuthenticationStrategy.SMARTEDIT, getGrantedAuthorities(username), true, getAuthenticationScopes(),
				getAuthenticationResourceIds(),
				null, null,
				null);
		return new OAuth2Authentication(oAuth2Request, getUserPwdAuthenticationToken(username));
	}

	@Override
	public Authentication getUserPwdAuthenticationToken(final String user)
	{
		final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				user, null, getGrantedAuthorities(user));
		authenticationToken.setDetails(getAuthenticationTokenDetails(user));
		return authenticationToken;
	}

	@Override
	public List<GrantedAuthority> getGrantedAuthorities(final String username)
	{
		return StreamUtil.safeStream(getUserService().getUserForUID(username).getGroups())
				.map(this::getGrantedAuthoritiesForPrincipal)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	@Override
	public Map<String, String> getAuthenticationTokenDetails(final String username)
	{
		final Map<String, String> tokenDetails = new HashMap<>();
		tokenDetails.put(ADAuthenticationStrategy.GRANT_TYPE, ADAuthenticationStrategy.PASS);
		tokenDetails.put(ADAuthenticationStrategy.CLIENT_ID, ADAuthenticationStrategy.SMARTEDIT);
		tokenDetails.put(ADAuthenticationStrategy.USER, username);
		return tokenDetails;
	}

	@Override
	public Set<String> getAuthenticationScopes()
	{
		return new HashSet<>(Arrays.asList(ADAuthenticationStrategy.BASIC, ADAuthenticationStrategy.OPEN_ID));
	}

	@Override
	public Set<String> getAuthenticationResourceIds()
	{
		return new HashSet<>(Arrays.asList(ADAuthenticationStrategy.HYBRIS));
	}

	private List<GrantedAuthority> getGrantedAuthoritiesForPrincipal(final PrincipalGroupModel principal)
	{
		return StreamUtil.safeStream(getAdUserRoleMap().entrySet())
				.filter(entry -> principal.getUid().equals(entry.getValue()))
				.map(entry -> new SimpleGrantedAuthority(entry.getKey()))
				.collect(Collectors.toList());
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public Map<String, String> getAdUserRoleMap()
	{
		return adUserRoleMap;
	}

	@Required
	public void setAdUserRoleMap(final Map<String, String> adUserRoleMap)
	{
		this.adUserRoleMap = adUserRoleMap;
	}

}
