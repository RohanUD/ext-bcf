/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.oauth.security;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;


public interface ADAuthenticationStrategy
{
	String GRANT_TYPE = "grant_type";
	String CLIENT_ID = "client_id";
	String USER = "username";
	String PASS = "password";
	String SMARTEDIT = "smartedit";
	String BASIC = "basic";
	String OPEN_ID = "openid";
	String HYBRIS = "hybris";
	String SHA_256 = "SHA-256";
	String UTF_8 = "UTF-8";
	String TOKEN_KEY_FORMAT = "%032x";
	int TOKEN_SIG_NUM = 1;

	Map<String, String> getAuthenticationTokenDetails(String username);

	String getAuthenticationTokenKey(String tokenValue);

	Set<String> getAuthenticationScopes();

	boolean isADUserAuthenticationApplicable(String username);

	Set<String> getAuthenticationResourceIds();

	List<GrantedAuthority> getGrantedAuthorities(String username);

	Authentication loadADAuthenticationToken(String username);

	Authentication getUserPwdAuthenticationToken(String username);
}
