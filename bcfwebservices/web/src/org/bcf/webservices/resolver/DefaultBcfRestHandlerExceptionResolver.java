/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.resolver;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.webservicescommons.dto.error.ErrorListWsDTO;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.errors.factory.WebserviceErrorFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bcf.webservices.factory.BcfWebserviceErrorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import com.bcf.webservices.errors.data.ErrorListWsData;
import com.bcf.webservices.errors.data.ErrorWsData;


	public  class DefaultBcfRestHandlerExceptionResolver extends AbstractHandlerExceptionResolver
	{

		private static final Logger LOG = LoggerFactory.getLogger(DefaultBcfRestHandlerExceptionResolver.class);
		public static final String PROPERTY_ROOT_KEY = "webservicescommons.resthandlerexceptionresolver.";
		public static final String GLOBAL_STATUS_PROPERTY_KEY = "webservicescommons.resthandlerexceptionresolver.%s.status";
		public static final String EXTENSION_STATUS_PROPERTY_KEY = "webservicescommons.resthandlerexceptionresolver.%s.%s.status";
		public static final String GLOBAL_LOGSTACK_PROPERTY_KEY = "webservicescommons.resthandlerexceptionresolver.%s.logstack";
		public static final String EXTENSION_LOGSTACK_PROPERTY_KEY = "webservicescommons.resthandlerexceptionresolver.%s.%s.logstack";
		public static final String DEFAULT_STATUS_PROPERTY = "webservicescommons.resthandlerexceptionresolver.DEFAULT.status";
		public static final String DEFAULT_LOGSTACK_PROPERTY = "webservicescommons.resthandlerexceptionresolver.DEFAULT.logstack";
		private ConfigurationService configurationService;
		private String propertySpecificKey;

		private HttpMessageConverter<?>[] messageConverters;
		private BcfWebserviceErrorFactory bcfWebserviceErrorFactory;

		private WebserviceErrorFactory webserviceErrorFactory;


		public HttpMessageConverter<?>[] getMessageConverters() //NOSONAR
		{
			return messageConverters;
		}

		public void setMessageConverters(final HttpMessageConverter<?>[] messageConverters)
		{
			this.messageConverters = messageConverters;
		}

		public BcfWebserviceErrorFactory getBcfWebserviceErrorFactory()
		{
			return bcfWebserviceErrorFactory;
		}

		public void setBcfWebserviceErrorFactory(final BcfWebserviceErrorFactory bcfWebserviceErrorFactory)
		{
			this.bcfWebserviceErrorFactory = bcfWebserviceErrorFactory;
		}

		public WebserviceErrorFactory getWebserviceErrorFactory()
		{
			return webserviceErrorFactory;
		}

		public void setWebserviceErrorFactory(final WebserviceErrorFactory webserviceErrorFactory)
		{
			this.webserviceErrorFactory = webserviceErrorFactory;
		}

		@Override
		protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
			response.setStatus(this.calculateStatusFromException(ex));

			ErrorListWsData errorListDatas = new ErrorListWsData();
			ErrorListWsDTO errorListDtos = new ErrorListWsDTO();

			List<ErrorWsData> errorDataList;
			List<ErrorWsDTO> errorDtoList;
			ServletServerHttpRequest inputMessage = new ServletServerHttpRequest(request);
			ServletServerHttpResponse outputMessage = new ServletServerHttpResponse(response);

			try {
				if (ex instanceof WebserviceValidationException) {
					errorDataList = this.getBcfWebserviceErrorFactory().createErrorList(((WebserviceValidationException)ex).getValidationObject());
					errorListDatas.setErrors(errorDataList);
					ModelAndView modelAndView = this.writeWithMessageConverters(errorListDatas, inputMessage, outputMessage);
					return modelAndView;
				} else {
					errorDtoList = this.getWebserviceErrorFactory().createErrorList(ex);
					errorListDtos.setErrors(errorDtoList);
					ModelAndView modelAndView = this.writeWithMessageConverters(errorListDtos, inputMessage, outputMessage);
					return modelAndView;
				}
				} catch (Exception e) {
				LOG.error("Handling of [" + ex.getClass().getName() + "] resulted in Exception", e);
			} finally {
				outputMessage.close();
			}


			return null;
		}

		protected ModelAndView writeWithMessageConverters(Object returnValue, HttpInputMessage inputMessage, HttpOutputMessage outputMessage) throws
				IOException
		{
			List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();
			if (acceptedMediaTypes.isEmpty())
			{
				acceptedMediaTypes = Collections.singletonList(MediaType.ALL);
			}

			MediaType.sortByQualityValue(acceptedMediaTypes);
			Class<?> returnValueType = returnValue.getClass();
			if (this.messageConverters != null)
			{
				Iterator iterator = acceptedMediaTypes.iterator();

				while (iterator.hasNext())
				{
					MediaType acceptedMediaType = (MediaType) iterator.next();
					HttpMessageConverter[] httpMessageConverter = this.messageConverters;
					int length = this.messageConverters.length;

					for (int i = 0; i < length; ++i)
					{
						HttpMessageConverter messageConverter = httpMessageConverter[i];
						if (messageConverter.canWrite(returnValueType, acceptedMediaType))
						{
							messageConverter.write(returnValue, acceptedMediaType, outputMessage);
							return new ModelAndView();
						}
					}
				}

			}
			return null;
		}

		protected int calculateStatusFromException(Exception e) {
			Integer status;
			if (this.propertySpecificKey != null) {
				status = this.getIntegerConfiguration(String.format(EXTENSION_STATUS_PROPERTY_KEY, this.propertySpecificKey, e.getClass().getSimpleName()));
				if (status != null) {
					return status;
				}
			}

			status = this.getIntegerConfiguration(String.format(GLOBAL_STATUS_PROPERTY_KEY, e.getClass().getSimpleName()));
			if (status != null) {
				return status;
			} else {
				status = this.getIntegerConfiguration(DEFAULT_STATUS_PROPERTY);
				return status != null ? status : 400;
			}
		}

		protected boolean shouldDisplayStack(Exception e) {
			Boolean displayStack = null;
			if (this.propertySpecificKey != null) {
				displayStack = this.getBooleanConfiguration(String.format(EXTENSION_LOGSTACK_PROPERTY_KEY, this.propertySpecificKey, e.getClass().getSimpleName()));
				if (displayStack != null) {
					return displayStack;
				}
			}

			displayStack = this.getBooleanConfiguration(String.format(GLOBAL_LOGSTACK_PROPERTY_KEY, e.getClass().getSimpleName()));
			if (displayStack != null) {
				return displayStack;
			} else {
				displayStack = this.getBooleanConfiguration(DEFAULT_LOGSTACK_PROPERTY);
				return displayStack != null ? displayStack : true;
			}
		}

		protected Integer getIntegerConfiguration(String key) {
			return this.configurationService == null ? null : this.configurationService.getConfiguration().getInteger(key, null);
		}

		protected Boolean getBooleanConfiguration(String key) {
			return this.configurationService == null ? null : this.configurationService.getConfiguration().getBoolean(key, null);
		}

		public void setPropertySpecificKey(String propertySpecificKey) {
			this.propertySpecificKey = propertySpecificKey;
		}

		public void setConfigurationService(ConfigurationService configurationService) {
			this.configurationService = configurationService;
		}


}
