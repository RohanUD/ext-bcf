/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.bcf.webservices.validator;

import de.hybris.platform.jalo.JaloItemNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.travelservices.enums.TransportOfferingStatus;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.webservices.travel.data.ScheduleData;


/**
 * Validates instances of {@link ScheduleData}.
 */
@Component("scheduleValidator")
public class ScheduleValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(ScheduleValidator.class);

	@Override
	public boolean supports(final Class clazz)
	{
		return ScheduleData.class.isAssignableFrom(clazz);
	}

	@Resource(name = "typeService")
	private TypeService typeService;


	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final ScheduleData scheduleData = (ScheduleData) target;

		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BcfFacadesConstants.SCHEDULE_SYNC_DATE_FORMAT, i18nService.getCurrentLocale());
		simpleDateFormat.setLenient(false);
		validateScheduleDepartureTime(errors, simpleDateFormat, scheduleData);
		validateScheduleArrivalTime(errors, simpleDateFormat, scheduleData);

		if (scheduleData.getSailingCode() == null || !StringUtils.hasText(scheduleData.getSailingCode()))
		{
			errors.reject("scheduleupdate.missingsailingcode", new String[]
					{ scheduleData.getDepartureTime(), scheduleData.getOriginTerminal() }, "schedule.sailingCode.missing");

		}

		if (scheduleData.getOriginTerminal() == null || !StringUtils.hasText(scheduleData.getOriginTerminal()))
		{
			errors.reject("scheduleupdate.missingoriginalterminal",
					new String[] { scheduleData.getSailingCode(), scheduleData.getDepartureTime() },
					"schedule.originalTerminal.missing");
		}

		if (scheduleData.getDestinationTerminal() == null || !StringUtils.hasText(scheduleData.getDestinationTerminal()))
		{
			errors.reject("scheduleupdate.missingdestinationterminal",
					new String[] { scheduleData.getSailingCode(), scheduleData.getDepartureTime() },
					"schedule.destinationTerminal.missing");
		}

		if (StringUtils.isEmpty(scheduleData.getStatus()))
		{
			scheduleData.setStatus(TransportOfferingStatus.SCHEDULED.getCode());
		}
		else
		{
			try
			{
				typeService.getEnumerationValue(TransportOfferingStatus._TYPECODE.toLowerCase(), scheduleData.getStatus());
			}
			catch (final JaloItemNotFoundException | UnknownIdentifierException ex)
			{
				LOG.error("Could not find schedule status for " + scheduleData.getStatus() + ", Sailing code " + scheduleData
						.getSailingCode() + ", departure time " + scheduleData.getDepartureTime());
				LOG.error("Setting to default status :" + TransportOfferingStatus.SCHEDULED.getCode());
				scheduleData.setStatus(TransportOfferingStatus.SCHEDULED.getCode());
			}
		}

		if (scheduleData.getTravelSector() == null || !StringUtils.hasText(scheduleData.getTravelSector()))
		{
			errors.reject("scheduleupdate.missingtravelsector",
					new String[] { scheduleData.getSailingCode(), scheduleData.getDepartureTime() }, "schedule.travelSector.missing");
		}

		if (scheduleData.getTransportVehicle() == null || !StringUtils.hasText(scheduleData.getTransportVehicle()))
		{
			errors.reject("scheduleupdate.missingtransportvehicle",
					new String[] { scheduleData.getSailingCode(), scheduleData.getDepartureTime() },
					"schedule.transportVehicle.missing");
		}

	}

	private void validateScheduleArrivalTime(final Errors errors, final SimpleDateFormat simpleDateFormat,
			final ScheduleData scheduleData)
	{
		if (scheduleData.getArrivalTime() == null || !StringUtils.hasText(scheduleData.getArrivalTime()))
		{
			errors.reject("scheduleupdate.missingarrivaltime",
					new String[] { scheduleData.getSailingCode(), scheduleData.getDepartureTime() }, "schedule.arrivalTime.missing");
		}
		else
		{

			try
			{
				simpleDateFormat.parse(scheduleData.getArrivalTime());
			}
			catch (final ParseException e)
			{
				errors.reject("scheduleupdate.invalidarrivaltime", new String[]
						{ scheduleData.getSailingCode(), scheduleData.getDepartureTime() }, "schedule.arrivalTime.invalid");
			}


		}
	}

	private void validateScheduleDepartureTime(final Errors errors, final SimpleDateFormat simpleDateFormat,
			final ScheduleData scheduleData)
	{
		if (scheduleData.getDepartureTime() == null || !StringUtils.hasText(scheduleData.getDepartureTime()))
		{
			errors.reject("scheduleupdate.missingdeparturetime",
					new String[] { scheduleData.getSailingCode(), scheduleData.getOriginTerminal() },
					"schedule.departureTime.missing");
		}
		else
		{

			try
			{
				simpleDateFormat.parse(scheduleData.getDepartureTime());

			}
			catch (final ParseException e)
			{
				errors.reject("scheduleupdate.invaliddeparturetime", new String[]
						{ scheduleData.getSailingCode(), scheduleData.getDepartureTime() }, "schedule.departureTime.invalid");


			}
		}
	}


}
