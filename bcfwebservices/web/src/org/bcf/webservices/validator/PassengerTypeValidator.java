/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.bcf.webservices.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.webservices.travel.data.PassengerType;
import com.bcf.webservices.travel.data.ScheduleData;


/**
 * Validates instances of {@link ScheduleData}.
 */
@Component("passengerTypeValidator")
public class PassengerTypeValidator implements Validator {

    @Override
    public boolean supports(final Class clazz) {
        return PassengerTypeValidator.class.isAssignableFrom(clazz);
    }

    @Resource(name = "typeService")
    private TypeService typeService;


    @Resource(name = "i18nService")
    private I18NService i18nService;

    @Override
    public void validate(final Object target, final Errors errors) {
        final PassengerType passengerType = (PassengerType) target;

        if (StringUtils.isEmpty(passengerType.getCode())) {
            errors.reject("passengertype.mandatoryfield.code", "passengertype.mandatoryfield.code");
        }

        if (StringUtils.isEmpty(passengerType.getName())) {
            errors.reject("passengertype.mandatoryfield.name", "passengertype.mandatoryfield.name");
        }

        if (passengerType.getMinAge() <= 0) {
            errors.reject("passengertype.mandatoryfield.minAge", "passengertype.mandatoryfield.minAge");
        }
    }


}
