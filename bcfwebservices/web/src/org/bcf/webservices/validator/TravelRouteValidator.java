/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.webservices.travel.data.RouteInfo;
import com.bcf.webservices.travel.data.RoutesData;


/**
 * Validates instances of {@link RoutesData}.
 */
@Component("travelRouteValidator")
public class TravelRouteValidator implements Validator
{
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RoutesData.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final RoutesData routesData = (RoutesData) target;

		if (routesData.getRoutes() == null)
		{
			errors.reject("travelrouteupdate.missingroutes", new RoutesData[] { routesData }, "travel.routes.missing");
			return;
		}
		for(RouteInfo routeInfo : routesData.getRoutes()){
			validateRoutesInfo(errors, routeInfo);
		}
	}

	private void validateRoutesInfo(Errors errors, final RouteInfo routeInfo)
	{
		if (routeInfo.getCode() == null || !StringUtils.hasText(routeInfo.getCode()))
		{
			errors.reject("travelrouteupdate.missingroutecode", new String[]
					{routeInfo.getName()}, "travel.route.code.missing");
		}
		if (routeInfo.getName() == null || !StringUtils.hasText(routeInfo.getName()))
		{
			errors.reject("travelrouteupdate.missingroutename", new String[]
					{routeInfo.getCode()}, "travel.route.name.missing");
		}
		if (routeInfo.getSectors() == null)
		{
			errors.reject("travelrouteupdate.missingroutesectors", new String[]
					{routeInfo.getCode()}, "travel.route.sectors.missing");
		}
		if (routeInfo.getOrigin() == null || !StringUtils.hasText(routeInfo.getOrigin()))
		{
			errors.reject("travelrouteupdate.missingrouteorigin", new String[]
					{routeInfo.getCode()}, "travel.route.origin.missing");
		}
		if (routeInfo.getDestination() == null || !StringUtils.hasText(routeInfo.getDestination()))
		{
			errors.reject("travelrouteupdate.missingroutedestination", new String[]
					{routeInfo.getCode()}, "travel.route.destination.missing");
		}
	}
}
