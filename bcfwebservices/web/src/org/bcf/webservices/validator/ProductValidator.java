/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.bcf.webservices.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.travelservices.model.product.FeeProductModel;
import java.text.ParseException;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.webservices.travel.data.ProductData;
import com.bcf.webservices.travel.data.ScheduleData;


/**
 * Validates instances of {@link ScheduleData}.
 */
@Component("productValidator")
public class ProductValidator implements Validator
{
	private  static final Logger LOG = LoggerFactory.getLogger(ProductValidator.class);

	@Override
	public boolean supports(final Class clazz)
	{
		return ScheduleData.class.isAssignableFrom(clazz);
	}

	@Resource(name = "typeService")
	private TypeService typeService;


	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final ProductData productData = (ProductData) target;

		if (StringUtils.isEmpty(productData.getCode()))
		{
			errors.reject("product.mandatoryfield.code", "productData.mandatory.field.code");

		}

		if (StringUtils.isEmpty(productData.getName()))
		{
			errors.reject("product.mandatoryfield.name", "productData.mandatory.field.name");

		}

		if (StringUtils.isEmpty(productData.getType()))
		{
			errors.reject("product.mandatoryfield.type", "productData.mandatory.field.type");

		}

		if (StringUtils.equalsAnyIgnoreCase(FeeProductModel._TYPECODE, productData.getType()) && CollectionUtils
				.isEmpty(productData.getVessels()))
		{
			errors.reject("product.mandatoryfield.vessels", "productData.mandatory.field.vessels");
		}


		if (StringUtils.isNotEmpty(productData.getStartDate()) && !this.validateDate(productData.getStartDate()))
		{
			errors.reject("product.invalid.startDate", "productData.mandatory.field.startDate");
		}

		if(StringUtils.isNoneEmpty(productData.getEndDate()) && !this.validateDate(productData.getEndDate())) {
			errors.reject("product.invalid.endDate", "productData.mandatory.field.endDate");
		}
	}

	private boolean validateDate(final String dateString)
	{
		try
		{
			return DateUtils.parseDate(dateString, BcfFacadesConstants.PRODUCTS_SYNC_DATE_FORMAT) != null;
		}
		catch (final ParseException e)
		{
			LOG.error("Date format is not valid for the given string " + dateString);
		}
		return false;
	}
}
