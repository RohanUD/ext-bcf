/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.bcf.webservices.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.core.util.StreamUtil;
import com.bcf.webservices.travel.data.FacilityData;
import com.bcf.webservices.travel.data.ScheduleData;
import com.bcf.webservices.travel.data.ShipInfoData;


/**
 * Validates instances of {@link ScheduleData}.
 */
@Component("shipInfoValidator")
public class ShipInfoValidator implements Validator {

    @Override
    public boolean supports(final Class clazz) {
        return ShipInfoValidator.class.isAssignableFrom(clazz);
    }

    @Resource(name = "typeService")
    private TypeService typeService;


    @Resource(name = "i18nService")
    private I18NService i18nService;

    @Override
    public void validate(final Object target, final Errors errors) {
        final ShipInfoData shipInfoData = (ShipInfoData) target;

        if (StringUtils.isEmpty(shipInfoData.getCode())) {
            errors.reject("shipInfo.mandatoryfield.code", "shipInfo.mandatoryfield.code");
        }

        if (StringUtils.isEmpty(shipInfoData.getName())) {
            errors.reject("shipInfo.mandatoryfield.name", "shipInfo.mandatoryfield.name");
        }

        StreamUtil.safeStream(shipInfoData.getFacilities()).forEach(facilityData -> {
            this.validateShipFacility(facilityData, errors);
        });
    }

    private void validateShipFacility(final FacilityData facilityData, final Errors errors)
    {
        if (StringUtils.isEmpty(facilityData.getCode())) {
            errors.reject("shipFacility.mandatoryfield.code", "shipFacility.mandatoryfield.code");
        }

        if (StringUtils.isEmpty(facilityData.getName())) {
            errors.reject("shipFacility.mandatoryfield.name", "shipFacility.mandatoryfield.name");
        }
    }


}
