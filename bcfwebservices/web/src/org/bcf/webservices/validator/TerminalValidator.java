/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.bcf.webservices.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.webservices.travel.data.ScheduleData;
import com.bcf.webservices.travel.data.TerminalInfo;
import com.bcf.webservices.travel.data.TerminalsData;


/**
 * Validates instances of {@link ScheduleData}.
 */
@Component("terminalValidator")
public class TerminalValidator implements Validator
{

	@Override
	public boolean supports(final Class clazz)
	{
		return TerminalsData.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final TerminalsData terminalsData = (TerminalsData) target;

		if (terminalsData.getTerminals() == null)
		{
			errors.reject("terminalupdate.missingterminals", new TerminalsData[] { terminalsData }, "terminals.missing");
			return;
		}
		for(TerminalInfo terminalInfo : terminalsData.getTerminals()){
			validateTerminalInfo(errors, terminalInfo);
		}

	}

	private void validateTerminalInfo(final Errors errors, final TerminalInfo terminalInfo)
	{
		if (terminalInfo.getCode() == null || !StringUtils.hasText(terminalInfo.getCode()))
		{
			errors.reject("terminalupdate.missingterminalcode", new String[]
					{terminalInfo.getName()}, "terminal.code.missing");
		}
		if (terminalInfo.getName() == null || !StringUtils.hasText(terminalInfo.getName()))
		{
			errors.reject("terminalupdate.missingterminalname", new String[]
					{terminalInfo.getCode()}, "terminal.name.missing");
		}
		if (terminalInfo.getAddressLine1() == null)
		{
			errors.reject("terminalupdate.missingterminaladdressline1", new String[]
					{terminalInfo.getCode()}, "terminal.address.line1.missing");
		}
		if (terminalInfo.getCity() == null)
		{
			errors.reject("terminalupdate.missingterminalcity", new String[]
					{terminalInfo.getCode()}, "terminal.city.missing");
		}
		if (terminalInfo.getGeographicAreaCode() == null)
		{
			errors.reject("terminalupdate.missinggeographicareacode", new String[]
					{terminalInfo.getCode()}, "terminal.geographicarea.code.missing");
		}
		if (terminalInfo.getGeographicAreaName() == null)
		{
			errors.reject("terminalupdate.missinggeographicareaname", new String[]
					{terminalInfo.getCode()}, "terminal.geographicarea.name.missing");
		}
		if (terminalInfo.getCountry() == null)
		{
			errors.reject("terminalupdate.missingcountry", new String[]
					{terminalInfo.getCode()}, "terminal.country.missing");
		}
		if (terminalInfo.getPostalCode() == null)
		{
			errors.reject("terminalupdate.missingpostalcode", new String[]
					{terminalInfo.getCode()}, "terminal.postalcode.missing");
		}
	}


}
