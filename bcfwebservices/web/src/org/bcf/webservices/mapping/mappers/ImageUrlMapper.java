/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.mapping.mappers;

import ma.glasnost.orika.MappingContext;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercewebservicescommons.dto.product.ImageWsDTO;
import de.hybris.platform.webservicescommons.mapping.mappers.AbstractCustomMapper;
import org.bcf.webservices.constants.YcommercewebservicesConstants;

public class ImageUrlMapper extends AbstractCustomMapper<ImageData, ImageWsDTO>
{
    @Override
    public void mapAtoB(final ImageData a, final ImageWsDTO b, final MappingContext context)
    {
        // other fields are mapped automatically

        context.beginMappingField("url", getAType(), a, "url", getBType(), b);
        try
        {
            if (shouldMap(a, b, context))
            {
                StringBuilder url = new StringBuilder(YcommercewebservicesConstants.V2_ROOT_CONTEXT)
                        .append(a.getUrl());
                b.setUrl(url.toString());
            }
        }
        finally
        {
            context.endMappingField();
        }
    }
}
