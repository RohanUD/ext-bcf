/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.factory.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.bcf.webservices.conv.ErrorConverter;
import org.bcf.webservices.factory.BcfWebserviceErrorFactory;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.webservices.errors.data.ErrorWsData;


public class DefaultBcfWebserviceErrorFactory implements BcfWebserviceErrorFactory
{

	private List<ErrorConverter> converters;


	@Override
	public List<ErrorWsData> createErrorList(Object obj) {
		List<ErrorWsData> errors = this.createTarget();
		Iterator it = this.converters.iterator();

		while(it.hasNext()) {
			ErrorConverter converter = (ErrorConverter)it.next();
			if (converter.supports(obj.getClass()))
			{
				errors.addAll(converter.convert(obj));
			}
		}

		return errors;
	}



	protected List<ErrorWsData> createTarget() {
		return new LinkedList();
	}

	@Required
	public void setConverters(List<ErrorConverter> converters) {
		this.converters = converters;
	}
}
