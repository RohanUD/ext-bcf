/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.bcf.webservices.validator.TravelRouteValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.webservices.travelroutes.BcfTravelRoutesFacade;
import com.bcf.webservices.travel.data.RoutesData;


@RestController
@RequestMapping(value = "/datasync/travelRoute")
public class TravelRouteSyncController extends BaseController
{
	private static final Logger LOG = Logger.getLogger(TravelRouteSyncController.class);
	private static final String TAVELROUTE_CODE_PATTERN = "{code}";

	@Resource(name = "bcfTravelRoutesFacade")
	private BcfTravelRoutesFacade bcfTravelRoutesFacade;

	@Resource(name = "travelRouteValidator")
	private TravelRouteValidator travelRouteValidator;


	@RequestMapping(method = RequestMethod.POST, consumes =MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateTravelRoutes(@RequestBody final RoutesData routesData,
			final HttpServletResponse response)
	{
		if(routesData!=null && CollectionUtils.isNotEmpty(routesData.getRoutes()))
		{
			final Errors errors = validate(routesData);
			if(errors.hasErrors()){
				throw new WebserviceValidationException(errors);
			}
			try
			{
				bcfTravelRoutesFacade.createOrUpdateRoutes(routesData);
			} catch (final Exception e)
			{
				LOG.error("exception happened while processing travel route sync process", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/" + TAVELROUTE_CODE_PATTERN)
	@ResponseBody
	public ResponseEntity<String> deleteTravelRoute(@PathVariable final String code, final HttpServletResponse response)
	{
		try
		{
			bcfTravelRoutesFacade.deleteTravelRoute(code);
		}
		catch (final UnknownIdentifierException ex)
		{
			final Errors errors = new BeanPropertyBindingResult(code, "routeData");
			errors.reject("travelrouteupdate.invalidfield.code",new String[] {code},"travel.route.code.invalid");
			throw new WebserviceValidationException(errors);
		}
		return new ResponseEntity( HttpStatus.OK);
	}

	protected Errors validate(final RoutesData routesData)
	{
		final Errors errors = new BeanPropertyBindingResult(routesData, "routeData");
		travelRouteValidator.validate(routesData, errors);
		return errors;
	}
}
