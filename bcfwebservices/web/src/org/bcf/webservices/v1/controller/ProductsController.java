/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.bcf.webservices.validator.ProductValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.webservices.travel.data.ProductsData;


@RestController
@RequestMapping(value = "/datasync/products")
public class ProductsController extends BaseController
{

	private static final String PRODUCT_CODE_PATTERN = "{productCode}";

	@Resource(name = "productValidator")
	private ProductValidator productValidator;

	@Resource(name = "ancillaryProductFacade")
	private AncillaryProductFacade ancillaryProductFacade;


	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateSchedules(@RequestBody final ProductsData productsData,
			final HttpServletResponse response)
	{
		if (productsData != null && CollectionUtils.isNotEmpty(productsData.getProducts()))
		{
			validate(productsData);
			ancillaryProductFacade.createOrModifyProduct(productsData);
		}
		return new ResponseEntity(HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/" + PRODUCT_CODE_PATTERN)
	@ResponseBody
	public ResponseEntity<String> deleteProduct(@PathVariable final String productCode, final HttpServletResponse response)
	{
		ancillaryProductFacade.deleteProduct(productCode);
		return new ResponseEntity( HttpStatus.OK);
	}


	protected void validate(final ProductsData productsData)
	{
		final Errors errors = new BeanPropertyBindingResult(productsData, "productsData");
		productsData.getProducts().stream().forEach(productData -> {
			productValidator.validate(productData, errors);
		});

		if (errors.hasErrors())
		{
			throw new WebserviceValidationException(errors);
		}
	}
}
