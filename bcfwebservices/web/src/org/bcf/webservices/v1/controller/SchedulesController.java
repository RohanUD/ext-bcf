/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.bcf.webservices.validator.ScheduleValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.webservices.schedules.BcfSchedulesFacade;
import com.bcf.webservices.travel.data.SchedulesData;


@RestController
@RequestMapping(value = "/datasync/schedule")
public class SchedulesController extends BaseController
{

	private static final String SAILING_CODE_PATTERN = "{sailingCode}";
	public static final String SCHEDULE_DATA = "scheduleData";

	@Resource(name = "scheduleValidator")
	private ScheduleValidator scheduleValidator;

	@Resource(name = "bcfSchedulesFacade")
	private BcfSchedulesFacade bcfSchedulesFacade;


	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateSchedules(@RequestBody final SchedulesData schedulesData,
			final HttpServletResponse response) throws Exception
	{
		if (schedulesData != null && CollectionUtils.isNotEmpty(schedulesData.getSchedules()))
		{
			validate(schedulesData);
			bcfSchedulesFacade.createOrUpdateSchedules(schedulesData);
		}
		return new ResponseEntity(HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/" + SAILING_CODE_PATTERN, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	SchedulesData getSchedules(@PathVariable final String sailingCode, final HttpServletResponse response)
	{
		if (StringUtils.isEmpty(sailingCode))
		{
			final Errors errors = new BeanPropertyBindingResult(sailingCode, SCHEDULE_DATA);
			errors.reject("sailingCode.mandatoryfield.code", "schedule.sailingCode.mandatoryfield.code");
			throw new WebserviceValidationException(errors);
		}

		try
		{
			return bcfSchedulesFacade.getTravelRouteSchedulesForTransferSailingCode(sailingCode);
		} catch (UnknownIdentifierException ex) {
			sendErrorResults(sailingCode);
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/" + SAILING_CODE_PATTERN)
	@ResponseBody
	public ResponseEntity<String> deleteSchedules(@PathVariable final String sailingCode, final HttpServletResponse response)
	{
		try
		{
			bcfSchedulesFacade.deleteSchedules(sailingCode);
		} catch (UnknownIdentifierException ex) {
			sendErrorResults(sailingCode);
		}
		return new ResponseEntity( HttpStatus.OK);

	}

	private void sendErrorResults(final String sailingCode) {
		final Errors errors = new BeanPropertyBindingResult(sailingCode, SCHEDULE_DATA);
		errors.reject("schedule.invalidfield.code",new String[] {sailingCode},"schedule.sailingCode.invalid");
		throw new WebserviceValidationException(errors);
	}


	protected void validate(final SchedulesData schedulesData)
	{
		final Errors errors = new BeanPropertyBindingResult(schedulesData, SCHEDULE_DATA);
		schedulesData.getSchedules().stream().forEach(scheduleData->
			scheduleValidator.validate(scheduleData, errors));

		if (errors.hasErrors())
		{
			throw new WebserviceValidationException(errors);
		}
	}

}
