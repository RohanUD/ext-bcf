/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.bcf.webservices.validator.ShipInfoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.shipinfo.BcfShipInfoFacade;
import com.bcf.webservices.travel.data.ShipInfosData;


@RestController
@RequestMapping(value = "/datasync/shipinfo")
public class ShipInfoController extends BaseController
{

	@Resource(name = "shipInfoValidator")
	private ShipInfoValidator shipInfoValidator;

	@Resource
	private BcfShipInfoFacade bcfShipInfoFacade;

	private static final String SHIPINFO_CODE_PATTERN = "{shipInfoCode}";


	@RequestMapping(method = RequestMethod.POST, consumes =MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateShipInfos(@RequestBody final ShipInfosData shipInfosData,
			final HttpServletResponse response)
	{

		validate(shipInfosData);
		bcfShipInfoFacade.createOrModifyShipInfo(shipInfosData);
		return new ResponseEntity(HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/" + SHIPINFO_CODE_PATTERN)
	@ResponseBody
	public ResponseEntity<String> deleteShipInfo(@PathVariable final String shipInfoCode,
			final HttpServletResponse response)
	{
		bcfShipInfoFacade.deleteShipInfo(shipInfoCode);
		return new ResponseEntity( HttpStatus.OK);
	}


	protected void validate(final ShipInfosData shipInfosData)
	{
		final Errors errors = new BeanPropertyBindingResult(shipInfosData, "shipInfoData");
		shipInfosData.getShipInfos().stream().forEach(shipInfoData->{
			shipInfoValidator.validate(shipInfoData, errors);
		});

		if (errors.hasErrors())
		{
			throw new WebserviceValidationException(errors);
		}
	}
}
