/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bcf.core.model.ferry.FerryTandCModel;
import com.bcf.core.service.BcfTermsAndConditionsService;
import com.bcf.notification.request.order.createorder.TermsAndConditionsData;
import com.bcf.notification.response.email.TermsAndConditionsResponseData;


@Controller
@RequestMapping(value = "/termsandcondition")
public class TermsAndConditionsController extends BaseController
{
	private static final Logger LOG = Logger.getLogger(VelocityTemplateController.class);

	@Resource
	private BcfTermsAndConditionsService bcfTermsAndConditionsService;

	@Resource
	Converter<FerryTandCModel, TermsAndConditionsData> bcfTermsAndConditionsWSConverter;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public TermsAndConditionsResponseData getTermsAndConditionsForCodes(@RequestParam(name = "codes") final List<String> codes,
			final HttpServletResponse response)
	{
		final TermsAndConditionsResponseData templateResponseData = new TermsAndConditionsResponseData();
		try
		{
			List<FerryTandCModel> ferryTandCModel = getBcfTermsAndConditionsService()
					.getTermsAndConditionsModels(codes);
			List<TermsAndConditionsData> ferryTandCData = bcfTermsAndConditionsWSConverter.convertAll(ferryTandCModel);
			templateResponseData.setTermsAndConditions(ferryTandCData);
		}
		catch (final Exception e)
		{
			LOG.error(
					String.format("Exception occurred while fetching Terms And Condition data for codes %s", String.join(",", codes)),
					e);
		}
		return templateResponseData;
	}

	public BcfTermsAndConditionsService getBcfTermsAndConditionsService()
	{
		return bcfTermsAndConditionsService;
	}

	public void setBcfTermsAndConditionsService(final BcfTermsAndConditionsService bcfTermsAndConditionsService)
	{
		this.bcfTermsAndConditionsService = bcfTermsAndConditionsService;
	}
}
