/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package org.bcf.webservices.v1.controller;

import static com.bcf.core.constants.BcfCoreConstants.CATALOG_VERSION;
import static com.bcf.core.constants.BcfCoreConstants.TRANSACTION_CATALOG_ID;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bcf.core.email.service.BcfCMSEmailPageService;
import com.bcf.core.helper.VelocityTemplateHelper;
import com.bcf.notification.request.email.TemplateRequestData;
import com.bcf.notification.response.email.TemplateResponseData;


@Controller
@RequestMapping(value = "/notification/template")
public class VelocityTemplateController extends BaseController
{
	private static final Logger LOG = Logger.getLogger(VelocityTemplateController.class);


	@Resource
	private BcfCMSEmailPageService bcfCMSEmailPageService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private MediaService mediaService;

	@Resource
	VelocityTemplateHelper velocityTemplateHelper;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public TemplateResponseData getTemplate(final TemplateRequestData templateRequestData,
			final HttpServletResponse response)
	{
		String templateId = velocityTemplateHelper.getEmailTemplateId(templateRequestData.getEventType());
		int emailTemplateVersionId = templateRequestData.getEmailTemplateVersion();

		if (emailTemplateVersionId <= 0)
		{
			LOG.warn(String.format(
					"Email template VersionId [%d] is not valid for template [%s], event type [%s]. Getting the default email template version",
					emailTemplateVersionId, templateId, templateRequestData.getEventType()));
			emailTemplateVersionId = 1;
		}

		final EmailPageTemplateModel emailPageTemplateModel = bcfCMSEmailPageService
				.getEmailPageTemplateForTemplateId(templateId, emailTemplateVersionId,
						catalogVersionService.getCatalogVersion(TRANSACTION_CATALOG_ID, CATALOG_VERSION));

		final RendererTemplateModel bodyRenderTemplate = emailPageTemplateModel.getHtmlTemplate();
		final RendererTemplateModel subjectRenderTemplate = emailPageTemplateModel.getSubject();

		final MediaModel bodyContent = bodyRenderTemplate.getContent();
		final MediaModel subjectContent = subjectRenderTemplate.getContent();

		final InputStream bodyInputStream = mediaService.getStreamFromMedia(bodyContent);
		final InputStream subjectInputStream = mediaService.getStreamFromMedia(subjectContent);

		final TemplateResponseData templateResponseData = new TemplateResponseData();
		templateResponseData.setTemplateIdentifier(templateId);
		templateResponseData.setTemplateVersion(emailTemplateVersionId);
		try
		{
			final byte[] bodyBytes = IOUtils.toByteArray(bodyInputStream);
			final byte[] subjectBytes = IOUtils.toByteArray(subjectInputStream);
			templateResponseData.setSubjectInputStream(subjectBytes);
			templateResponseData.setBodyInputStream(bodyBytes);
		}
		catch (final IOException e)
		{
			LOG.error("Exception occurred while converting email subject/body to Input Stream", e);
		}
		return templateResponseData;
	}
}
