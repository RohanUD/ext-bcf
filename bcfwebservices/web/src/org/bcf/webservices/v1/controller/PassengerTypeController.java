/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.bcf.webservices.validator.PassengerTypeValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.webservices.travel.data.PassengerTypesData;

@RestController
@RequestMapping(value = "/datasync/passengerType")
public class PassengerTypeController extends BaseController {

    private static final String PASSENGER_TYPE_CODE_PATTERN = "{code}";
    @Resource(name = "passengerTypeValidator")
    private PassengerTypeValidator passengerTypeValidator;

    @Resource(name = "defaultBCFPassengerTypeFacade")
    private BCFPassengerTypeFacade bcfPassengerTypeFacade;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> updateSchedules(@RequestBody final PassengerTypesData passengerTypesData,
                                                  final HttpServletResponse response) throws Exception {

        if (Objects.nonNull(passengerTypesData) && CollectionUtils.isNotEmpty(passengerTypesData.getPassengerTypes())) {
            validate(passengerTypesData);
            bcfPassengerTypeFacade.createOrUpdatePassengerTypes(passengerTypesData.getPassengerTypes());
        }
        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/" + PASSENGER_TYPE_CODE_PATTERN)
    @ResponseBody
    public ResponseEntity<String> deletePassengerType(@PathVariable final String code, final HttpServletResponse response)
	 {
        try
        {
            bcfPassengerTypeFacade.deletePassengerTypes(code);
        } catch (UnknownIdentifierException ex) {
            final Errors errors = new BeanPropertyBindingResult(code, "passengerType");
            errors.reject("passengertype.invalidfield.code",new String[] {code},"passengertype.mandatoryfield.invalid.code");
            throw new WebserviceValidationException(errors);
        }
        return new ResponseEntity( HttpStatus.OK);
    }

    protected void validate(final PassengerTypesData passengerTypesData) {
        final Errors errors = new BeanPropertyBindingResult(passengerTypesData, "passengerType");

        passengerTypesData.getPassengerTypes().stream().forEach(passengerType -> {
            passengerTypeValidator.validate(passengerType, errors);
        });

        if (errors.hasErrors()) {
            throw new WebserviceValidationException(errors);
        }
    }
}
