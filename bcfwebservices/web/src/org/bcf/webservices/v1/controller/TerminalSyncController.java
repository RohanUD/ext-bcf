/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package org.bcf.webservices.v1.controller;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bcf.webservices.validator.TerminalValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bcf.facades.webservices.terminals.BcfTerminalsFacade;
import com.bcf.webservices.travel.data.TerminalsData;


@RestController
@RequestMapping(value = "/datasync/terminal")
public class TerminalSyncController extends BaseController
{
	private static final Logger LOG = Logger.getLogger(TerminalSyncController.class);
	private static final String TERMINAL_CODE_PATTERN = "{code}";

	@Resource(name = "terminalValidator")
	private TerminalValidator terminalValidator;

	@Resource(name = "bcfTerminalsFacade")
	private BcfTerminalsFacade bcfTerminalsFacade;


	@RequestMapping(method = RequestMethod.POST, consumes =MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateTerminals(@RequestBody final TerminalsData terminalsData,
			final HttpServletResponse response)
	{
		final Errors errors = validate(terminalsData);
		if(errors.hasErrors()){
			throw new WebserviceValidationException(errors);
		}
		try
		{
			bcfTerminalsFacade.createOrUpdateTerminals(terminalsData);
		}
		catch (final Exception e)
		{
			LOG.error("exception happened while processing terminal sync process", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/" + TERMINAL_CODE_PATTERN)
	@ResponseBody
	public ResponseEntity<String> deleteTerminal(@PathVariable final String code, final HttpServletResponse response)
	{
		try
		{
			bcfTerminalsFacade.deleteTerminal(code);
		}
		catch (final UnknownIdentifierException ex)
		{
			final Errors errors = new BeanPropertyBindingResult(code, "terminalsData");
			errors.reject("terminalsData.invalidfield.code",new String[] {code},"terminal.code.invalid");
			throw new WebserviceValidationException(errors);
		}

		return new ResponseEntity( HttpStatus.OK);
	}

	protected Errors validate(final TerminalsData terminalsData)
	{
		final Errors errors = new BeanPropertyBindingResult(terminalsData, "terminalsData");
		terminalValidator.validate(terminalsData, errors);
		return errors;
	}


}
