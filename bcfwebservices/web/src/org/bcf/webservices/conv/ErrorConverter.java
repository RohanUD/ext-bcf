/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.conv;

import java.util.List;
import com.bcf.webservices.errors.data.ErrorWsData;


public interface ErrorConverter
{

	List<ErrorWsData> convert( Object o);

	boolean supports(Class clazz);
}
