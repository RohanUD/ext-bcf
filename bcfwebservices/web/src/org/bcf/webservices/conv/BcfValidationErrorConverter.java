/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package org.bcf.webservices.conv;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.webservices.errors.data.ErrorWsData;


public class BcfValidationErrorConverter implements ErrorConverter
{

		private I18NService i18NService;

		private PropertySourceFacade propertySourceFacade;



	@Override
	public boolean supports(final Class clazz) {
		return Errors.class.isAssignableFrom(clazz);
	}

	@Override
	public List<ErrorWsData> convert( final Object o) throws ConversionException
	{
		final List<ErrorWsData> webserviceErrorList=new ArrayList<>();
		populate(o,webserviceErrorList );
		return webserviceErrorList;
	}

	public void populate(final Object o, final List<ErrorWsData> webserviceErrorList)
	{
		final Errors errors = (Errors)o;
		Iterator errorIterator = errors.getGlobalErrors().iterator();

		while(errorIterator.hasNext()) {
			final ErrorWsData errorDto = new ErrorWsData();
			final ObjectError error = (ObjectError)errorIterator.next();
			errorDto.setType(error.getCode());
			errorDto.setMessage(propertySourceFacade.getPropertySourceValueMessage(error.getDefaultMessage(),error.getArguments()));
			webserviceErrorList.add(errorDto);
		}

		errorIterator = errors.getFieldErrors().iterator();

		while(errorIterator.hasNext()) {
			final ErrorWsData errorDto = new ErrorWsData();
			final ObjectError error = (ObjectError)errorIterator.next();
			errorDto.setType(error.getCode());
			errorDto.setMessage(propertySourceFacade.getPropertySourceValueMessage(error.getDefaultMessage(),error.getArguments()));
			webserviceErrorList.add(errorDto);
		}

	}

	protected I18NService getI18NService()
		{
			return i18NService;
		}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}

	@Required
		public void setI18NService(final I18NService i18NService)
		{
			this.i18NService = i18NService;
		}

}
