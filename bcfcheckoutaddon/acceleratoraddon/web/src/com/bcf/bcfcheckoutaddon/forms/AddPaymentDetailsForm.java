/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms;




import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


/**
 * Form to get data from the tokenizer page and holds the information needed
 * regarding token.
 */
public class AddPaymentDetailsForm
{
	private String firstName;
	private String lastName;
	private AddressForm billingAddress;
	private String paymentType;
	private String cardType;
	private String cardNumber;
	private String cardCVNumber;
	private String cardExpirationMonth;
	private String cardExpirationYear;
	private String token;
	private String ctcCardNo;

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public AddressForm getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final AddressForm billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getCardCVNumber()
	{
		return cardCVNumber;
	}

	public void setCardCVNumber(final String cardCVNumber)
	{
		this.cardCVNumber = cardCVNumber;
	}

	public String getCardExpirationMonth()
	{
		return cardExpirationMonth;
	}

	public void setCardExpirationMonth(final String cardExpirationMonth)
	{
		this.cardExpirationMonth = cardExpirationMonth;
	}

	public String getCardExpirationYear()
	{
		return cardExpirationYear;
	}

	public void setCardExpirationYear(final String cardExpirationYear)
	{
		this.cardExpirationYear = cardExpirationYear;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getCtcCardNo()
	{
		return ctcCardNo;
	}

	public void setCtcCardNo(final String ctcCardNo)
	{
		this.ctcCardNo = ctcCardNo;
	}
}
