/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms;

public class UpdatePaymentCardForm
{
	private int formNumber;
	private String code;
	private String cardType;
	private String expiryMonth;
	private String expiryYear;
	private String cardHolderFirstName;
	private String cardHolderLastName;

	private String addressLine1;
	private String addressLine2;
	private String city;
	private String country;
	private String province;
	private String zipCode;

	public int getFormNumber()
	{
		return formNumber;
	}

	public void setFormNumber(final int formNumber)
	{
		this.formNumber = formNumber;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getExpiryMonth()
	{
		return expiryMonth;
	}

	public void setExpiryMonth(final String expiryMonth)
	{
		this.expiryMonth = expiryMonth;
	}

	public String getExpiryYear()
	{
		return expiryYear;
	}

	public void setExpiryYear(final String expiryYear)
	{
		this.expiryYear = expiryYear;
	}

	public String getCardHolderFirstName()
	{
		return cardHolderFirstName;
	}

	public void setCardHolderFirstName(final String cardHolderFirstName)
	{
		this.cardHolderFirstName = cardHolderFirstName;
	}

	public String getCardHolderLastName()
	{
		return cardHolderLastName;
	}

	public void setCardHolderLastName(final String cardHolderLastName)
	{
		this.cardHolderLastName = cardHolderLastName;
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getProvince()
	{
		return province;
	}

	public void setProvince(final String province)
	{
		this.province = province;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}
}
