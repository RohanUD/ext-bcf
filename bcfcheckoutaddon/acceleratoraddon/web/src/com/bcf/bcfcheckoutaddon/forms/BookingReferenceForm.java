package com.bcf.bcfcheckoutaddon.forms;

import java.util.List;


public class BookingReferenceForm
{
	private List<String> referenceCodes;
	private List<String> bookingRefs;
	private List<String> journeyRefNo;

	public List<String> getReferenceCodes()
	{
		return referenceCodes;
	}

	public void setReferenceCodes(List<String> referenceCodes)
	{
		this.referenceCodes = referenceCodes;
	}

	public List<String> getBookingRefs()
	{
		return bookingRefs;
	}

	public void setBookingRefs(List<String> bookingRefs)
	{
		this.bookingRefs = bookingRefs;
	}

	public List<String> getJourneyRefNo()
	{
		return journeyRefNo;
	}

	public void setJourneyRefNo(List<String> journeyRefNo)
	{
		this.journeyRefNo = journeyRefNo;
	}
}
