/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms.validation;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfcheckoutaddon.forms.UpdatePaymentCardForm;
import com.bcf.bcfstorefrontaddon.form.validators.BaseValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;


@Component("myAccountUpdatePaymentCardFormValidator")
public class MyAccountUpdatePaymentCardFormValidator extends BaseValidator
{

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePaymentCardForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{

		final UpdatePaymentCardForm updatePaymentCardForm = (UpdatePaymentCardForm) object;

		validateField("zipCode", updatePaymentCardForm.getZipCode(), errors);

		final String country = updatePaymentCardForm.getCountry();
		validateField("country", country, errors);

		final String province = updatePaymentCardForm.getProvince();
		final String regionsRequiredForCountries = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);
		if (StringUtils.isNotEmpty(regionsRequiredForCountries))
		{
			final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
			if (regionCountries.contains(country))
			{
				if (Objects.isNull(province) || StringUtils.equals(province, "-1"))
				{
					errors.rejectValue("province", "register.province.invalid");
				}
			}
		}
	}
}
