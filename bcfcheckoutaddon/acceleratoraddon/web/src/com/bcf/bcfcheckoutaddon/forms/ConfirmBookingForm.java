/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 03/07/19 14:54
 */

package com.bcf.bcfcheckoutaddon.forms;




import java.util.List;


/**
 * Form to get data from the tokenizer page and holds the information needed
 * regarding token.
 */
public class ConfirmBookingForm
{
	private List<String> referenceCodes;

	private List<String> bookingRefs;

	private boolean termsCheck;

	public List<String> getBookingRefs()
	{
		return bookingRefs;
	}

	public void setBookingRefs(final List<String> bookingRefs)
	{
		this.bookingRefs = bookingRefs;
	}

	public List<String> getReferenceCodes()
	{
		return referenceCodes;
	}

	public void setReferenceCodes(final List<String> referenceCodes)
	{
		this.referenceCodes = referenceCodes;
	}


	public boolean isTermsCheck()
	{
		return termsCheck;
	}

	public void setTermsCheck(final boolean termsCheck)
	{
		this.termsCheck = termsCheck;
	}
}
