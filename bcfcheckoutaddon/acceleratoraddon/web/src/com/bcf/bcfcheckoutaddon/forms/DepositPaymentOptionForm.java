/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms;

/**
 * payment details for deposit
 */
public class DepositPaymentOptionForm
{
	private String depositAmount;

	private String paymentType;

	private String depositPayDate;

	public String getDepositAmount()
	{
		return depositAmount;
	}

	public void setDepositAmount(final String depositAmount)
	{
		this.depositAmount = depositAmount;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getDepositPayDate()
	{
		return depositPayDate;
	}

	public void setDepositPayDate(final String depositPayDate)
	{
		this.depositPayDate = depositPayDate;
	}
}
