/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms;




import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import java.util.List;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;


/**
 * Form to get data from the tokenizer page and holds the information needed
 * regarding token.
 */
public class PaymentDetailsForm
{
	private String firstName;
	private String lastName;
	private String email;
	private String checkEmail;
	private String phoneNumber;
	private String country;
	private String paymentType;
	private String cardType;
	private String cardNumber;
	private String cardCVNumber;
	private String cardExpirationMonth;
	private String cardExpirationYear;
	private String cardExpiration;
	private String token;
	private String receiptNo;
	private String agentComment;
	private String availabilityStatus;
	private boolean termsCheck;
	private AddressForm billingAddress;
	private List<String> referenceCodes;
	private List<String> bookingRefs;
	private String roomCode;
	private List<RoomGuestData> roomGuestData;
	private List<ActivityGuestData> activityLeadPaxInfo;
	private boolean allowChangesAtPOS;
	private String specialInstructions;
	private String vacationInstructions;
	private String ctcCardNo;
	private boolean saveCTCcard;
	private String savedCTCcardCode;
	private String savedCCCardCode;
	private boolean deferPayment;
	private boolean saveCCCard;
	private boolean useNewCard;
	private String postCode;

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getCheckEmail()
	{
		return checkEmail;
	}

	public void setCheckEmail(final String checkEmail)
	{
		this.checkEmail = checkEmail;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getCardCVNumber()
	{
		return cardCVNumber;
	}

	public void setCardCVNumber(final String cardCVNumber)
	{
		this.cardCVNumber = cardCVNumber;
	}

	public String getCardExpirationMonth()
	{
		return cardExpirationMonth;
	}

	public void setCardExpirationMonth(final String cardExpirationMonth)
	{
		this.cardExpirationMonth = cardExpirationMonth;
	}

	public String getCardExpirationYear()
	{
		return cardExpirationYear;
	}

	public void setCardExpirationYear(final String cardExpirationYear)
	{
		this.cardExpirationYear = cardExpirationYear;
	}

	public String getCardExpiration()
	{
		return cardExpiration;
	}

	public void setCardExpiration(final String cardExpiration)
	{
		this.cardExpiration = cardExpiration;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getReceiptNo()
	{
		return receiptNo;
	}

	public void setReceiptNo(final String receiptNo)
	{
		this.receiptNo = receiptNo;
	}

	public String getAgentComment()
	{
		return agentComment;
	}

	public void setAgentComment(final String agentComment)
	{
		this.agentComment = agentComment;
	}

	public boolean isTermsCheck()
	{
		return termsCheck;
	}

	public void setTermsCheck(final boolean termsCheck)
	{
		this.termsCheck = termsCheck;
	}

	public AddressForm getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final AddressForm billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public String getAvailabilityStatus()
	{
		return availabilityStatus;
	}

	public void setAvailabilityStatus(final String availabilityStatus)
	{
		this.availabilityStatus = availabilityStatus;
	}

	public List<String> getReferenceCodes()
	{
		return referenceCodes;
	}

	public void setReferenceCodes(final List<String> referenceCodes)
	{
		this.referenceCodes = referenceCodes;
	}

	public List<RoomGuestData> getRoomGuestData()
	{
		return roomGuestData;
	}

	public void setRoomGuestData(final List<RoomGuestData> roomGuestData)
	{
		this.roomGuestData = roomGuestData;
	}

	public List<ActivityGuestData> getActivityLeadPaxInfo()
	{
		return activityLeadPaxInfo;
	}

	public void setActivityLeadPaxInfo(final List<ActivityGuestData> activityLeadPaxInfo)
	{
		this.activityLeadPaxInfo = activityLeadPaxInfo;
	}

	public String getRoomCode()
	{
		return roomCode;
	}

	public void setRoomCode(final String roomCode)
	{
		this.roomCode = roomCode;
	}

	public List<String> getBookingRefs()
	{
		return bookingRefs;
	}

	public void setBookingRefs(final List<String> bookingRefs)
	{
		this.bookingRefs = bookingRefs;
	}

	public boolean isAllowChangesAtPOS()
	{
		return allowChangesAtPOS;
	}

	public void setAllowChangesAtPOS(final boolean allowChangesAtPOS)
	{
		this.allowChangesAtPOS = allowChangesAtPOS;
	}

	public String getSpecialInstructions()
	{
		return specialInstructions;
	}

	public void setSpecialInstructions(final String specialInstructions)
	{
		this.specialInstructions = specialInstructions;
	}

	public String getVacationInstructions()
	{
		return vacationInstructions;
	}

	public void setVacationInstructions(final String vacationInstructions)
	{
		this.vacationInstructions = vacationInstructions;
	}

	public String getCtcCardNo()
	{
		return ctcCardNo;
	}

	public void setCtcCardNo(final String ctcCardNo)
	{
		this.ctcCardNo = ctcCardNo;
	}

	public boolean isSaveCTCcard()
	{
		return saveCTCcard;
	}

	public void setSaveCTCcard(final boolean saveCTCcard)
	{
		this.saveCTCcard = saveCTCcard;
	}

	public String getSavedCTCcardCode()
	{
		return savedCTCcardCode;
	}

	public void setSavedCTCcardCode(final String savedCTCcardCode)
	{
		this.savedCTCcardCode = savedCTCcardCode;
	}

	public String getSavedCCCardCode()
	{
		return savedCCCardCode;
	}

	public void setSavedCCCardCode(final String savedCCCardCode)
	{
		this.savedCCCardCode = savedCCCardCode;
	}

	public boolean isDeferPayment()
	{
		return deferPayment;
	}

	public void setDeferPayment(final boolean deferPayment)
	{
		this.deferPayment = deferPayment;
	}

	public boolean isSaveCCCard()
	{
		return saveCCCard;
	}

	public void setSaveCCCard(final boolean saveCCCard)
	{
		this.saveCCCard = saveCCCard;
	}

	public boolean isUseNewCard()
	{
		return useNewCard;
	}

	public void setUseNewCard(final boolean useNewCard)
	{
		this.useNewCard = useNewCard;
	}

	public String getPostCode()
	{
		return postCode;
	}

	public void setPostCode(String postCode)
	{
		this.postCode = postCode;
	}
}
