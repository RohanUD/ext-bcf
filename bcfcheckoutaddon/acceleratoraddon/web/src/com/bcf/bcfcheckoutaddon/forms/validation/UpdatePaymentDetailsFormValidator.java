/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfcheckoutaddon.forms.PaymentDetailsForm;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * This validator validates PaymentDetailsForm.
 */
@Component("updatePaymentDetailsFormValidator")
public class UpdatePaymentDetailsFormValidator implements Validator
{

	private static final String PAYMENT_CARD_NUMBER = "payment.card.number";
	private static final String PAYMENT_CARD_EXPIRY_YEAR = "payment.card.expiry.year";
	private static final String PAYMENT_CARD_EXPIRY_MONTH = "payment.card.expiry.month";

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final PaymentDetailsForm form = (PaymentDetailsForm) object;

		if (StringUtils.isBlank(form.getCardType()))
		{
			errors.rejectValue("cardType", PAYMENT_CARD_NUMBER);
		}

		if (StringUtils.isBlank(form.getCardNumber()))
		{
			errors.rejectValue("cardNumber", PAYMENT_CARD_NUMBER);
		}
		if (StringUtils.isBlank(form.getCardExpiration().substring(3)))
		{
			errors.rejectValue(BcfFacadesConstants.Payment.CARD_EXPIRATION, PAYMENT_CARD_EXPIRY_YEAR);
		}

		if (StringUtils.isBlank(form.getCardExpiration().substring(0, 2)))
		{
			errors.rejectValue(BcfFacadesConstants.Payment.CARD_EXPIRATION, PAYMENT_CARD_EXPIRY_MONTH);
		}

	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return PaymentDetailsForm.class.equals(aClass);
	}
}
