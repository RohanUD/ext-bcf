/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms;

public class GiftCardPaymentDetailsForm
{
	private String amount;
	private String giftCardNo;
	private String paymentType;
	private String giftCardType;

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getGiftCardNo()
	{
		return giftCardNo;
	}

	public void setGiftCardNo(final String giftCardNo)
	{
		this.giftCardNo = giftCardNo;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getGiftCardType()
	{
		return giftCardType;
	}

	public void setGiftCardType(final String giftCardType)
	{
		this.giftCardType = giftCardType;
	}
}
