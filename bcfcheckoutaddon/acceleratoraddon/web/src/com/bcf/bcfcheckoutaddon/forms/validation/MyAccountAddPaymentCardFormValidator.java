/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfcheckoutaddon.forms.AddPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.PaymentDetailsForm;
import com.bcf.bcfstorefrontaddon.form.validators.BaseValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * This validator validates PaymentDetailsForm.
 */
@Component("myAccountAddPaymentCardFormValidator")
public class MyAccountAddPaymentCardFormValidator extends BaseValidator implements Validator
{
	private static final String PAYMENT_CARD_NUMBER = "payment.card.number";
	private static final String PAYMENT_CARD_EXPIRY_YEAR = "payment.card.expiry.year";
	private static final String PAYMENT_CARD_EXPIRY_MONTH = "payment.card.expiry.month";

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddPaymentDetailsForm form = (AddPaymentDetailsForm) object;

		final String cardType = form.getCardType();

		if (Objects.nonNull(cardType) && Objects.equals(cardType, BcfCoreConstants.CTC_TC_CARD))
		{
			if (StringUtils.isEmpty(form.getCtcCardNo()))
			{
				errors.rejectValue("ctcCardNo", PAYMENT_CARD_NUMBER);
			}
		}
		else
		{
			if (StringUtils.isEmpty(form.getCardExpirationMonth()))
			{
				errors.rejectValue(BcfFacadesConstants.Payment.CARD_EXPIRATION, PAYMENT_CARD_EXPIRY_MONTH);
			}

			if (StringUtils.isEmpty(form.getCardExpirationYear()))
			{
				errors.rejectValue(BcfFacadesConstants.Payment.CARD_EXPIRATION, PAYMENT_CARD_EXPIRY_YEAR);
			}

			validateBillingAddress(form.getBillingAddress(), errors);
		}
	}

	private void validateBillingAddress(final AddressForm addPaymentDetailsForm, final Errors errors)
	{
		validateField("billingAddress.postcode", addPaymentDetailsForm.getPostcode(), errors);

		final String countryIso = addPaymentDetailsForm.getCountryIso();
		validateField("billingAddress.countryIso", countryIso, errors);

		final String regionsRequiredForCountries = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);

		if (StringUtils.isNotEmpty(regionsRequiredForCountries))
		{
			final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
			if (regionCountries.contains(addPaymentDetailsForm.getCountryIso()))
			{
				if (Objects.isNull(addPaymentDetailsForm.getRegionIso()) || StringUtils
						.equals(addPaymentDetailsForm.getRegionIso(), "-1"))
				{
					errors.rejectValue("billingAddress.regionIso", "profile.region.invalid");
				}
			}
		}
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return PaymentDetailsForm.class.equals(aClass);
	}
}
