/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps.MyAccountBasePaymentController;
import com.bcf.bcfcheckoutaddon.forms.AddPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.UpdatePaymentCardForm;


@Controller
@RequestMapping("/my-account/payment-cards")
public class MyAccountPaymentController extends MyAccountBasePaymentController
{

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addPaymentCard(
			@RequestParam(value = "paymentMethod", defaultValue = "CreditCard", required = false) final String paymentMethod,
			final Model model) throws CMSItemNotFoundException
	{
		return getAddPaymentCardForm(paymentMethod, model);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addPaymentCard(final AddPaymentDetailsForm addPaymentDetailsForm, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		return addPaymentCard(addPaymentDetailsForm, request, bindingResult, redirectAttributes);
	}

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentCards(final Model model) throws CMSItemNotFoundException
	{
		return getPaymentCards(model);
	}

	@RequestMapping(value = "/remove/{paymentInfoId}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String removePaymentCard(
			@RequestParam(value = "cardType", defaultValue = "creditCard", required = false) final String cardType,
			@PathVariable final String paymentInfoId, final Model model, final RedirectAttributes redirectAttributes)
	{
		return removePaymentCard(cardType, paymentInfoId, redirectAttributes);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePaymentCard(final UpdatePaymentCardForm updatePaymentCardForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		return updatePaymentCard(updatePaymentCardForm, bindingResult, redirectAttributes);
	}
}
