/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfcheckoutaddon.forms.CashPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.GiftCardPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.PaymentDetailsForm;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.CTCTCCardPaymentInfoModel;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping(value = "/checkout/multi/payment")
public class PostPaymentController extends PaymentMethodCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(PostPaymentController.class);
	private static final String ERROR_PLACE_ORDER = "checkout.error.placeorder";
	public static final String CART_VALIDATION_EXCEPTION_ENCOUNTERED_WHILE_VALIDATING_THE_CART = "CartValidationException encountered while validating the cart";
	public static final String ORDER_PROCESSING_EXCEPTION_ENCOUNTERED_WHILE_PLACING_ORDER = "OrderProcessingException encountered while placing order";
	public static final String INTEGRATION_EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER = "Integration exception encountered for while placing order";
	public static final String EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER = "Exception encountered for while placing order";
	public static final String GIFT_CARD_TYPE = "giftCardType";

	@Resource(name = "channelSpecificPlaceOrderErrorMap")
	private Map<String, String> channelSpecificPlaceOrderErrorMap;

	@Resource(name = "channelSpecificRefundOrderErrorMap")
	private Map<String, String> channelSpecificRefundOrderErrorMap;

	@Resource(name = "channelSpecificPermanentTokenErrorMap")
	private Map<String, String> channelSpecificPermanentTokenErrorMap;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "travelCustomerFacade")
	private BCFCustomerFacade travelCustomerFacade;

	@Resource(name = "defaultBcfTravelCartFacade")
	private DefaultBcfTravelCartFacade defaultBcfTravelCartFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "customerAccountService")
	private BcfCustomerAccountService customerAccountService;

	/**
	 * @param paymentDetailsForm The spring form of the order being submitted
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/placeOrder")
	public String placeOrder(@Valid final PaymentDetailsForm paymentDetailsForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final String availabilityStatus = defaultBcfTravelCartFacade.getPackageAvailabilityStatus();
		paymentDetailsForm.setAvailabilityStatus(availabilityStatus);
		final Map<String, String> resultMap = getRequestParameterMap(request);
		try
		{
			checkAnonymousUser(resultMap, request, response);
		}
		catch (final DuplicateUidException e)
		{
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
			return redirectToPaymentPage(model, redirectModel);
		}
		if (Objects.nonNull(paymentDetailsForm.getSavedCTCcardCode()))
		{
			final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
			final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = customerAccountService
					.getCtcTcCardPaymentInfoForCode(customerModel, paymentDetailsForm.getSavedCTCcardCode());
			if (Objects.nonNull(ctcTcCardPaymentInfoModel))
			{
				resultMap.put(BcfFacadesConstants.Payment.CTC_CARD_NUMBER, ctcTcCardPaymentInfoModel.getCode());
			}
		}
		resultMap.put(BcfFacadesConstants.Payment.SAVE_CTC_CARD, Boolean.toString(paymentDetailsForm.isSaveCTCcard()));
		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		AddressData addressData = null;
		if (StringUtils.equalsIgnoreCase(SalesApplication.WEB.getCode(), channel) && StringUtils
				.isBlank(resultMap.get(BcfFacadesConstants.Payment.CTC_CARD_NUMBER)) && StringUtils
				.isBlank(resultMap.get((BcfFacadesConstants.Payment.SAVED_CC_CARD_CODE))))
		{
			addressData = createBillingAddress(paymentDetailsForm);
		}
		if (StringUtils.isNotBlank(paymentDetailsForm.getAgentComment()))
		{
			getBcfTravelCheckoutFacade().saveAgentComment(paymentDetailsForm.getAgentComment());
		}

		getBcfTravelCheckoutFacade()
				.saveAdditionalInstructions(paymentDetailsForm.isAllowChangesAtPOS(), paymentDetailsForm.getSpecialInstructions(),
						paymentDetailsForm.getVacationInstructions(), paymentDetailsForm.isDeferPayment(), getPaymentType(resultMap));

		bcfBookingFacade.prepareAndSaveLeadPassengerDetails(resultMap);

		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		decisionData.setMethod(StringUtils.EMPTY);
		try
		{
			resultMap.put(BcfCoreConstants.ON_REQUEST_ORDER, availabilityStatus);
			getBcfTravelCheckoutFacade().placeOrder(addressData, resultMap, channel, decisionData);
			if (StringUtils.isEmpty(decisionData.getError()))
			{
				model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS, availabilityStatus);
				decisionData.setAvailabilityStatus(availabilityStatus);
				final OrderData orderData = decisionData.getOrderData();
				BcfControllerUtil.cleanUpSession(request, getSessionService());
				return redirectToOrderConfirmationPage(orderData);
			}
			setGlobalErrorMessage(decisionData.getMethod(), decisionData.getResponseCode(), decisionData.getError(), model, channel,
					decisionData.getOperatorDisplayMessage(), decisionData.getApprovalOrDeclineMessage());
		}
		catch (final IntegrationException e)
		{
			LOG.error("Integration exception encountered while placing order", e);
			GlobalMessages.addErrorMessage(model,
					e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).findFirst().get());
		}
		catch (final CartValidationException e)
		{
			LOG.error(CART_VALIDATION_EXCEPTION_ENCOUNTERED_WHILE_VALIDATING_THE_CART, e);
			GlobalMessages.addErrorMessage(model, decisionData.getError());
		}
		catch (final OrderProcessingException e)
		{
			LOG.error(ORDER_PROCESSING_EXCEPTION_ENCOUNTERED_WHILE_PLACING_ORDER, e);
			setGlobalErrorMessage(decisionData.getMethod(), decisionData.getResponseCode(), decisionData.getError(), model, channel,
					decisionData.getOperatorDisplayMessage(), decisionData.getApprovalOrDeclineMessage());
		}
		catch (final InsufficientStockLevelException | InvalidCartException e)
		{
			LOG.error("Exception encountered while placing order", e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return redirectToPaymentPage(model, redirectModel);
	}

	@RequestMapping(value = "/travelcentre/placeOrder")
	public String callConfirmBooking(final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		try
		{
			final String availabilityStatus = defaultBcfTravelCartFacade.getPackageAvailabilityStatus();

			decisionData.setAvailabilityStatus(availabilityStatus);
			getBcfTravelCheckoutFacade().callConfirmBookingAndInitiateBusinessProcess(decisionData, null);
			if (StringUtils.isEmpty(decisionData.getError()))
			{
				model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS, availabilityStatus);
				decisionData.setAvailabilityStatus(availabilityStatus);
				final OrderData orderData = decisionData.getOrderData();
				BcfControllerUtil.cleanUpSession(request, getSessionService());
				return redirectToOrderConfirmationPage(orderData);
			}
			setGlobalErrorMessage(decisionData.getMethod(), decisionData.getResponseCode(), decisionData.getError(), model, channel,
					decisionData.getOperatorDisplayMessage(), decisionData.getApprovalOrDeclineMessage());
		}
		catch (final IntegrationException e)
		{
			LOG.error(INTEGRATION_EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			GlobalMessages.addErrorMessage(model,
					e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).findFirst().get());
		}
		catch (final OrderProcessingException e)
		{
			LOG.error(ORDER_PROCESSING_EXCEPTION_ENCOUNTERED_WHILE_PLACING_ORDER, e);
			setGlobalErrorMessage(decisionData.getMethod(), decisionData.getResponseCode(), decisionData.getError(), model, channel,
					decisionData.getOperatorDisplayMessage(), decisionData.getApprovalOrDeclineMessage());
		}
		catch (final InvalidCartException e)
		{
			LOG.error(EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return redirectToPaymentPage(model, redirectModel);
	}

	/**
	 * @param paymentDetailsForm The card payment details form
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/card/pay", method = RequestMethod.POST, produces = "application/json")
	public String payByCard(@Valid final PaymentDetailsForm paymentDetailsForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);

		try
		{
			checkAnonymousUser(resultMap, request, response);
		}
		catch (final DuplicateUidException e)
		{
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
			return redirectToPaymentPage(model, redirectModel);
		}

		saveAdditionalComment(paymentDetailsForm);

		if (CollectionUtils.isNotEmpty(paymentDetailsForm.getReferenceCodes()))
		{
			getBcfTravelCheckoutFacade()
					.saveReferenceCode(paymentDetailsForm.getReferenceCodes(), paymentDetailsForm.getBookingRefs());
		}

		getBcfTravelCheckoutFacade()
				.saveAdditionalInstructions(paymentDetailsForm.isAllowChangesAtPOS(), paymentDetailsForm.getSpecialInstructions(),
						paymentDetailsForm.getVacationInstructions(), paymentDetailsForm.isDeferPayment(), getPaymentType(resultMap));

		final String paymentType = resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE);
		if (StringUtils.equalsIgnoreCase("savedCTCCard", paymentType) || StringUtils.equalsIgnoreCase("newCTCCard", paymentType))
		{
			resultMap.put(BcfFacadesConstants.Payment.PAYMENT_TYPE, "CTCCard");
			if (Objects.nonNull(paymentDetailsForm.getSavedCTCcardCode()))
			{
				final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
				final CTCTCCardPaymentInfoModel ctcTcCardPaymentInfoModel = customerAccountService
						.getCtcTcCardPaymentInfoForCode(customerModel, paymentDetailsForm.getSavedCTCcardCode());
				if (Objects.nonNull(ctcTcCardPaymentInfoModel))
				{
					resultMap.put(BcfFacadesConstants.Payment.CTC_CARD_NUMBER, ctcTcCardPaymentInfoModel.getCode());
				}
			}
			resultMap.put(BcfFacadesConstants.Payment.SAVE_CTC_CARD, Boolean.toString(paymentDetailsForm.isSaveCTCcard()));
		}

		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		try
		{
			if (Objects.isNull(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW)))
			{
				String availabilityStatus = null;
				final String sessionBookingJourney = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
				if (BcfstorefrontaddonWebConstants.BOOKING_PACKAGE.equals(sessionBookingJourney) ||
						BcfstorefrontaddonWebConstants.BOOKING_ACTIVITY_ONLY.equals(sessionBookingJourney))
				{
					availabilityStatus = getBcfTravelCartFacade().getPackageAvailabilityStatus();
					resultMap.put(BcfCoreConstants.ON_REQUEST_ORDER, availabilityStatus);
				}
				getBcfTravelCheckoutFacade().makePayment(createBillingAddress(resultMap), resultMap, channel, decisionData);
			}
			else
			{
				final String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
				getBcfTravelCheckoutFacade().makePaymentForPayNow(resultMap, channel, bookingReference, decisionData);
			}
			if (StringUtils.isBlank(decisionData.getError()))
			{
				decisionData.setConfirmBookingUrl(determineConfirmBookingUrl(channel,
						getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW)));
			}

			bcfBookingFacade.prepareAndSaveLeadPassengerDetails(resultMap);
		}
		catch (final IntegrationException e)
		{

			LOG.error(INTEGRATION_EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			decisionData.setError(e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).findFirst().get());
			GlobalMessages.addErrorMessage(model, decisionData.getError());
		}
		catch (final CartValidationException e)
		{
			LOG.error(CART_VALIDATION_EXCEPTION_ENCOUNTERED_WHILE_VALIDATING_THE_CART, e);
			GlobalMessages.addErrorMessage(model, decisionData.getError());
		}
		catch (final OrderProcessingException e)
		{
			LOG.error(ORDER_PROCESSING_EXCEPTION_ENCOUNTERED_WHILE_PLACING_ORDER, e);
			setGlobalErrorMessage(decisionData.getMethod(), decisionData.getResponseCode(), decisionData.getError(), model, channel,
					decisionData.getOperatorDisplayMessage(), decisionData.getApprovalOrDeclineMessage());
		}
		catch (final InsufficientStockLevelException e)
		{
			LOG.error(EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		if (Objects.nonNull(decisionData.getTransactionDetails()))
		{
			decisionData.getTransactionDetails()
					.setCartCode(Objects.nonNull(cartService.getSessionCart()) ? cartService.getSessionCart().getCode() : null);
		}
		model.addAttribute("decisionData", decisionData);
		return BcfcheckoutaddonControllerConstants.Views.Pages.Payment.PayNowResponse;
	}

	private void saveAdditionalComment(@Valid final PaymentDetailsForm paymentDetailsForm)
	{
		if (StringUtils.isNotBlank(paymentDetailsForm.getAgentComment()))
		{
			getBcfTravelCheckoutFacade().saveAgentComment(paymentDetailsForm.getAgentComment());
		}
	}

	/**
	 * @param cashPaymentDetailsForm The cash payment details form
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/cash/pay", method = RequestMethod.POST, produces = "application/json")
	public String payByCash(@Valid @ModelAttribute("cashPaymentDetailsForm") final CashPaymentDetailsForm cashPaymentDetailsForm,
			final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);
		try
		{
			checkAnonymousUser(resultMap, request, response);
		}
		catch (final DuplicateUidException e)
		{
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
			return redirectToPaymentPage(model, redirectModel);
		}
		bcfBookingFacade.prepareAndSaveLeadPassengerDetails(resultMap);
		return makePayment(model, request, resultMap);
	}

	/**
	 * @param giftCardPaymentDetailsForm The cash payment details form
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/gift/pay", method = RequestMethod.POST, produces = "application/json")
	public String payByGiftCard(
			@Valid @ModelAttribute("giftCardPaymentDetailsForm") final GiftCardPaymentDetailsForm giftCardPaymentDetailsForm,
			final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);
		resultMap.put(GIFT_CARD_TYPE, giftCardPaymentDetailsForm.getGiftCardType());
		if (resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE) == null)
		{
			resultMap.put(BcfFacadesConstants.Payment.PAYMENT_TYPE, BcfFacadesConstants.Payment.GIFT_CARD);
		}
		try
		{
			checkAnonymousUser(resultMap, request, response);
		}
		catch (final DuplicateUidException e)
		{
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
			return redirectToPaymentPage(model, redirectModel);
		}
		bcfBookingFacade.prepareAndSaveLeadPassengerDetails(resultMap);
		return makePayment(model, request, resultMap);
	}

	private void checkAnonymousUser(final Map<String, String> resultMap,
			final HttpServletRequest request, final HttpServletResponse response) throws DuplicateUidException
	{

		if (getBcfTravelCheckoutFacade().isAnonymousCheckout() && userService
				.isAnonymousUser(cartService.getSessionCart().getUser()))
		{
			processAnonymousCheckoutUserRequest(resultMap, request, response);
		}
		else
		{
			getBcfTravelCheckoutFacade().populateMapWithCustomerData(resultMap);
		}

	}

	protected String makePayment(final Model model, final HttpServletRequest request, final Map<String, String> resultMap)
	{
		/**
		 * the below map needs to be created using the form
		 */
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		try
		{
			if (Objects.isNull(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW)))
			{
				final String sessionBookingJourney = getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
				if (BcfstorefrontaddonWebConstants.BOOKING_PACKAGE.equals(sessionBookingJourney) ||
						BcfstorefrontaddonWebConstants.BOOKING_ACTIVITY_ONLY.equals(sessionBookingJourney))
				{
					resultMap.put(BcfCoreConstants.ON_REQUEST_ORDER, getBcfTravelCartFacade().getPackageAvailabilityStatus());
				}


				getBcfTravelCheckoutFacade().makePayment(createBillingAddress(resultMap), resultMap, channel, decisionData);
			}
			else
			{
				final String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
				getBcfTravelCheckoutFacade().makePaymentForPayNow(resultMap, channel, bookingReference, decisionData);
			}
			if (StringUtils.isBlank(decisionData.getError()) && decisionData.getAmountToPay() == 0d)
			{
				decisionData.setConfirmBookingUrl(determineConfirmBookingUrl(channel,
						getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW)));
			}
			model.addAttribute("decisionData", decisionData);
		}
		catch (final IntegrationException e)
		{
			LOG.error(INTEGRATION_EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			GlobalMessages.addErrorMessage(model,
					e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).findFirst().get());
		}
		catch (final CartValidationException e)
		{
			LOG.error(CART_VALIDATION_EXCEPTION_ENCOUNTERED_WHILE_VALIDATING_THE_CART, e);
			GlobalMessages.addErrorMessage(model, decisionData.getError());
		}
		catch (final OrderProcessingException e)
		{
			LOG.error(ORDER_PROCESSING_EXCEPTION_ENCOUNTERED_WHILE_PLACING_ORDER, e);
		}
		catch (final InsufficientStockLevelException e)
		{
			LOG.error(EXCEPTION_ENCOUNTERED_FOR_WHILE_PLACING_ORDER, e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return BcfcheckoutaddonControllerConstants.Views.Pages.Payment.PayNowResponse;
	}

	protected String redirectToPaymentPage(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.SESSION_CHANGE_DATES);
		return enterStep(model, redirectAttributes);
	}

	private void processAnonymousCheckoutUserRequest(final Map<String, String> resultMap,
			final HttpServletRequest request, final HttpServletResponse response)
			throws DuplicateUidException
	{
		final String name = resultMap.get("firstName") + " " + resultMap.get("lastName");
		travelCustomerFacade
				.createGuestUserForAnonymousCheckout(resultMap.get("email"), name, resultMap.get("firstName"),
						resultMap.get("lastName"), resultMap.get("phoneNumber"), null, resultMap.get("country"));
		guidCookieStrategy.setCookie(request, response);
		getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
		getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID, resultMap.get("email"));
	}

	private AddressData createBillingAddress(final PaymentDetailsForm paymentDetailsForm)
	{
		final AddressData addressData = new AddressData();
		final AddressForm billingAddress = paymentDetailsForm.getBillingAddress();
		addressData.setBillingAddress(true);
		addressData.setFirstName(billingAddress.getFirstName());
		addressData.setLine1(billingAddress.getLine1());
		addressData.setLine2(billingAddress.getLine2());
		addressData.setTown(billingAddress.getTownCity());
		addressData.setPostalCode(billingAddress.getPostcode());
		addressData.setCountry(i18NFacade.getCountryForIsocode(billingAddress.getCountryIso()));
		addressData.setRegion(i18NFacade.getRegion(billingAddress.getCountryIso(), billingAddress.getRegionIso()));
		addressData.setPhone(paymentDetailsForm.getPhoneNumber());

		return addressData;
	}

	private AddressData createBillingAddress(final Map<String, String> resultMap)
	{
		final AddressData addressData = new AddressData();

		addressData.setBillingAddress(true);
		addressData.setFirstName(resultMap.get("firstName"));
		addressData.setLastName(resultMap.get("lastName"));
		addressData.setPostalCode(resultMap.get("postCode"));
		addressData.setCountry(i18NFacade.getCountryForIsocode(resultMap.get("country")));
		addressData.setPhone(resultMap.get("phoneNumber"));
		addressData.setEmail(resultMap.get("email"));
		return addressData;
	}

	private void setGlobalErrorMessage(final String method, final String responseCode, final String error, final Model model,
			final String channel, final String operatorDisplayMsg, final String approvalOrDeclineMsg)
	{
		final Object[] localizationArguments = { responseCode, error, operatorDisplayMsg, approvalOrDeclineMsg };
		switch (method)
		{
			case BcfFacadesConstants.PROCESS_PAYMENT:
			case BcfFacadesConstants.GET_PERMANENT_TOKEN:
			case BcfFacadesConstants.REFUND_ORDER:
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						getChannelSpecificErrorMessage(method, channel), localizationArguments);
				break;
			default:
				GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
	}

	private String getChannelSpecificErrorMessage(final String method, final String channel)
	{
		switch (method)
		{
			case BcfFacadesConstants.PROCESS_PAYMENT:
				return channelSpecificPlaceOrderErrorMap.get(channel);
			case BcfFacadesConstants.GET_PERMANENT_TOKEN:
				return channelSpecificPermanentTokenErrorMap.get(channel);
			case BcfFacadesConstants.REFUND_ORDER:
				return channelSpecificRefundOrderErrorMap.get(channel);
			default:
				return ERROR_PLACE_ORDER;
		}
	}

	protected String determineConfirmBookingUrl(final String currentSalesChannel, final String bookingReference)
	{
		if (SalesApplication.TRAVELCENTRE.equals(SalesApplication.valueOf(currentSalesChannel))
				|| SalesApplication.TRAVELCENTREFERRYONLY.equals(SalesApplication.valueOf(currentSalesChannel)))
		{
			if (StringUtils.isNotBlank(bookingReference))
			{
				return getPaymentSubmitButtonUrlForPayNow(bookingReference);
			}
			return "/checkout/multi/payment/travelcentre/placeOrder";
		}
		return "/checkout/multi/payment/placeOrder";
	}

	protected String getPaymentSubmitButtonUrlForPayNow(final String bookingReference)
	{
		return "/checkout/paynow/confirm/" + bookingReference;
	}

	protected String getPaymentType(final Map<String, String> resultMap)
	{
		final String paymentType = resultMap.get(BcfFacadesConstants.Payment.PAYMENT_TYPE);
		if (StringUtils.isBlank(paymentType))
		{
			return BcfCoreConstants.CREDIT_CARD;
		}
		else if (StringUtils.equalsIgnoreCase("CTCCard", paymentType))
		{
			return BcfCoreConstants.CTC_TC_CARD;
		}
		return paymentType;
	}
}
