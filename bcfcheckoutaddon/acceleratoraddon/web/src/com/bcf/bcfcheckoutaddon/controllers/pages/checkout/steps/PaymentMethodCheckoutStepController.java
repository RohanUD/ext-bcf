/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PaymentDetailsForm;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.SelectPaymentOptionResponseData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import java.util.Collections;
import java.util.Objects;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.constants.BcfcheckoutaddonWebConstants;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.travel.data.PrePaymentCartValidationResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractPaymentMethodCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(PaymentMethodCheckoutStepController.class);
	private static final String ERROR_PLACE_ORDER = "checkout.error.placeorder";
	private static final String REFUND_INITIATED = "checkout.refund.initiated";



	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@RequestMapping(value = "/select-flow", method = RequestMethod.GET)
	public String initCheck(final Model model, final RedirectAttributes redirectModel)
			throws InvalidCartException
	{
		if (getBcfTravelCartFacade().isAmendmentCart())
		{
			final PriceData totalToPay = getBcfControllerUtil().getTotalToPay();

			//for on request amendment and card payment, show confirm booking page directly
			if (bcfTravelCartFacadeHelper.isOnRequestAmendment() && bcfTravelCartFacadeHelper.isCardPayment())
			{
				return REDIRECT_PREFIX + "/checkout/multi/payment-method/confirmBooking";
			}

			switch (getBcfTravelCheckoutFacade().definePaymentAction(new Double(totalToPay.getValue().doubleValue())))
			{
				case BcfFacadesConstants.Payment.ACTION_REFUND:
					return getRefundFlowAction(model, redirectModel);
				case BcfFacadesConstants.Payment.NO_ACTION:
					return REDIRECT_PREFIX + "/checkout/multi/payment-method/confirmBooking";
				default:
					return REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
			}
		}
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		return REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	}

	private String getRefundFlowAction(final Model model, final RedirectAttributes redirectModel) throws InvalidCartException
	{

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isNotEmpty(sessionBookingJourney)
				&& StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE))
		{
			return performRefund(model, redirectModel);
		}
		else
		{
			return REDIRECT_PREFIX + "/checkout/multi/payment-method/confirmBooking";
		}
	}

	@RequestMapping(value =
			{ "/add" }, method = RequestMethod.POST)
	public String add(final Model model, @Valid final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (StringUtils.isNotEmpty(sessionBookingJourney)
				&& !StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY)
				&& CollectionUtils.isEmpty(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.PAYMENT_TRANSACTIONS)))
		{
			GlobalMessages.addErrorMessage(model, "payment.option.no.payment.selected");
			return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		model.addAttribute("paymentFormUrl", getPaymentSubmitButtonUrl());
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
		paymentInfoData.setId(paymentDetailsForm.getPaymentId());
		paymentInfoData.setCardType(paymentDetailsForm.getCardTypeCode());
		paymentInfoData.setAccountHolderName(paymentDetailsForm.getNameOnCard());
		paymentInfoData.setCardNumber(paymentDetailsForm.getCardNumber());
		paymentInfoData.setStartMonth(paymentDetailsForm.getStartMonth());
		paymentInfoData.setStartYear(paymentDetailsForm.getStartYear());
		paymentInfoData.setExpiryMonth(paymentDetailsForm.getExpiryMonth());
		paymentInfoData.setExpiryYear(paymentDetailsForm.getExpiryYear());
		if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			paymentInfoData.setSaved(true);
		}
		paymentInfoData.setIssueNumber(paymentDetailsForm.getIssueNumber());

		final AddressData addressData;

		final AddressForm addressForm = paymentDetailsForm.getBillingAddress();
		addressData = new AddressData();
		if (addressForm != null)
		{
			addressData.setId(addressForm.getAddressId());
			addressData.setTitleCode(addressForm.getTitleCode());
			addressData.setFirstName(addressForm.getFirstName());
			addressData.setLastName(addressForm.getLastName());
			addressData.setLine1(addressForm.getLine1());
			addressData.setLine2(addressForm.getLine2());
			addressData.setTown(addressForm.getTownCity());
			addressData.setPostalCode(addressForm.getPostcode());
			addressData.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));
			if (addressForm.getRegionIso() != null)
			{
				addressData.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
			}

			addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
		}

		getAddressVerificationFacade().verifyAddressData(addressData);
		paymentInfoData.setBillingAddress(addressData);

		final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(paymentInfoData);
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId()))
		{
			if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) && getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		model.addAttribute("paymentId", newPaymentSubscription.getId());
		setCheckoutStepLinksForModel(model, getCheckoutStep());


		return getCheckoutStep().nextStep();
	}
	@RequestMapping(value =
			{ "/confirm-booking" }, method = RequestMethod.POST)
	public String confirmBooking(final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, InvalidCartException{

		return placeOrder(model, redirectModel);

	}

	@RequestMapping(value =
			{ "/confirm-refund" }, method = RequestMethod.POST)
	public String confirmRefund(final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, InvalidCartException{

		return performRefund(model, redirectModel);

	}

	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	public String remove(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes)
	{
		getUserFacade().unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart" + ".removed");
		return getCheckoutStep().currentStep();
	}

	protected String performRefund(final Model model, final RedirectAttributes redirectModel) throws InvalidCartException
	{
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		if(assistedServiceFacade.isAssistedServiceAgentLoggedIn()){
			return REDIRECT_PREFIX + "/checkout/refund/refund-selection";
		}

		try
		{
			final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
			PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
			decisionData = getBcfTravelCheckoutFacade().placeOrderAndRefund(channel,decisionData);
			if (Objects.nonNull(decisionData) && StringUtils.isEmpty(decisionData.getError()))

			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, REFUND_INITIATED);
				return redirectToOrderConfirmationPage(decisionData.getOrderData());
			}
			LOG.error("Error encountered while performing refund and placing order");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
			return getCheckoutStep().currentStep();
		}
		catch (final IntegrationException e)
		{
			LOG.error("IntegrationException encountered while performing refund and placing order");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
		}
		catch (final CartValidationException | OrderProcessingException | InsufficientStockLevelException e)
		{
			LOG.error("Exception encountered for while performing refund and placing order", e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return getCheckoutStep().currentStep();
	}

	/**
	 * This method gets called when the "Use These Payment Details" button is clicked. It sets the selected payment
	 * method on the checkout facade and reloads the page highlighting the selected payment method.
	 *
	 * @param selectedPaymentMethodId - the id of the payment method to use.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId,
			@RequestParam("card_cvNumber") final String cvNumber, final RedirectAttributes model)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (StringUtils.isNotEmpty(sessionBookingJourney)
				&& !StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY)
				&& CollectionUtils.isEmpty(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.PAYMENT_TRANSACTIONS)))
		{
			GlobalMessages.addFlashMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "payment.option.no.payment.selected");
			return getCheckoutStep().currentStep();
		}

		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId);
			getSessionService().setAttribute(BcfCoreConstants.CREDIT_CARD_INFO_SECURITY_CODE, cvNumber);
		}
		return getCheckoutStep().nextStep();
	}



	@RequestMapping(value = "/check-payment-options", method = RequestMethod.GET, produces = "application/json")
	@RequireHardLogIn
	public String isPaymentOptionSelected(final Model model)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isNotEmpty(sessionBookingJourney)
				&& !StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY)
				&& CollectionUtils.isEmpty(getSessionService().getAttribute(BcfstorefrontaddonWebConstants.PAYMENT_TRANSACTIONS)))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.SELECT_PAYMENT_OPTION_RESPONSE_DATA,
					buildResponseData(false, BcfstorefrontaddonWebConstants.SELECT_PAYMENT_JS_ERROR));
			return BcfstorefrontaddonControllerConstants.Views.Pages.Checkout.SelectPaymentResponse;
		}
		model.addAttribute(BcfstorefrontaddonWebConstants.SELECT_PAYMENT_OPTION_RESPONSE_DATA, buildResponseData(true, null));
		return BcfstorefrontaddonControllerConstants.Views.Pages.Checkout.SelectPaymentResponse;
	}

	protected SelectPaymentOptionResponseData buildResponseData(final boolean valid, final String error)
	{
		final SelectPaymentOptionResponseData response = new SelectPaymentOptionResponseData();
		response.setErrors(Collections.singletonList(error));
		response.setValid(valid);
		return response;
	}

	protected String placeOrder(final Model model, final RedirectAttributes redirectModel) throws InvalidCartException
	{
		PlaceOrderResponseData decisionData = null;
		try
		{

			boolean onRequestAmendment = false;
			if (bcfTravelCartFacadeHelper.isOnRequestAmendment() && bcfTravelCartFacadeHelper.isCardPayment())
			{
				onRequestAmendment = true;
			}


			final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
			decisionData = new PlaceOrderResponseData();
			getBcfTravelCheckoutFacade().placeOrderWithoutPayment(channel, decisionData);
			if (StringUtils.isEmpty(decisionData.getError()))
			{
				// update amount paid and amount to pay for on request amendment (card payment).
				if (onRequestAmendment)
				{
					bcfTravelCartFacadeHelper.updateAmountPaidForOnRequestAmendment(decisionData.getOrder());
				}
				return redirectToOrderConfirmationPage(decisionData.getOrderData());
			}
			LOG.error("Error encountered while placing order");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
			return getCheckoutStep().previousStep();
		}
		catch (final IntegrationException e)
		{
			LOG.error("IntegrationException encountered while placing order");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
		}
		catch (final CartValidationException | OrderProcessingException | InsufficientStockLevelException e)
		{
			LOG.error("Exception encountered for while placing order", e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return getCheckoutStep().previousStep();
	}

	@Override
	protected String redirectToOrderConfirmationPage(final OrderData orderData)
	{
		final StringBuilder orderCodeToAppend = new StringBuilder();
		if (StringUtils.isNotBlank(orderData.getOriginalOrderCode()))
		{
			orderCodeToAppend.append(orderData.getOriginalOrderCode());
		}
		else
		{
			orderCodeToAppend.append(orderData.getCode());
		}
		return BcfcheckoutaddonWebConstants.REDIRECT_URL_BOOKING_CONFIRMATION + orderCodeToAppend.toString();
	}

	@RequestMapping(value = "/verify-cart", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	PrePaymentCartValidationResponseData validateCartAjax(final Model model)
	{

		return getBcfTravelCartFacade().verifyCartForPayment();

	}

}
