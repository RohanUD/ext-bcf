/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.customer.TravelCustomerFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.constants.BcfcheckoutaddonConstants;
import com.bcf.bcfcheckoutaddon.constants.BcfcheckoutaddonWebConstants;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfcheckoutaddon.forms.BCFPlaceOrderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.ebooking.ConfirmBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.traveller.BCFTravellerFacade;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{
	private static final String TRAVELLERS_GROUP_BY_JOURNEY = "travellersGroupByJourney";
	private static final String SUMMARY = "summary";
	private static final String HOME_PAGE_PATH = "/";
	private static final String AGENT_COMMENT = "agentComment";
	public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "travelCustomerFacade")
	private TravelCustomerFacade travelCustomerFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "confirmBookingIntegrationFacade")
	private ConfirmBookingIntegrationFacade confirmBookingIntegrationFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		if (!bcfTravelCartFacade.hasEntries())
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}

		final int maxEmailAllowed = Integer.parseInt(bcfConfigurablePropertiesService.getBcfPropertyValue("maxEmailAllowed"));
		model.addAttribute("maxEmailAllowed", maxEmailAllowed);
		model.addAttribute("bcfPlaceOrderForm", new BCFPlaceOrderForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		model.addAttribute(BcfcheckoutaddonWebConstants.AMEND, bcfTravelCartFacade.isAmendmentCart());

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}

		model.addAttribute(BcfstorefrontaddonWebConstants.CART_TOTAL, bcfTravelCartFacade.getCartTotal());
		if (BookingJourneyType.BOOKING_TRANSPORT_ONLY.equals(sessionBookingJourney))
		{
			setSpecialAssistanceToSummary(model);
		}
		final PriceData totalAmount = bcfTravelCartFacade.getBookingTotal(bcfTravelCartFacade.getOriginalOrderCode());
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_TOTAL, totalAmount);

		final PriceData partialPaymentAmount = bcfTravelCartFacade.getPartialPaymentAmount();
		if (Objects.nonNull(partialPaymentAmount))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.SELECTED_PAYMENT, partialPaymentAmount);
		}

		String specialServiceRequestCommentAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue("specialServiceRequestComment");
		if (StringUtils.isEmpty(specialServiceRequestCommentAllowed))
		{
			specialServiceRequestCommentAllowed = "false";
		}
		if (specialServiceRequestCommentAllowed.equals("true"))
		{
			final String agentComment = bcfTravelCartFacade.getAgentComment();
			model.addAttribute(AGENT_COMMENT, agentComment);
		}
		model.addAttribute(BcfcheckoutaddonConstants.AMOUNT_TO_PAY, bcfTravelCartFacade.getAmountToPay());
		model.addAttribute(BcfstorefrontaddonWebConstants.PAY_AT_TERMINAL, bcfTravelCartFacade.getPayAtTerminal());
		model.addAttribute("showWaiveOff", true);
		return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}

	private void setSpecialAssistanceToSummary(final Model model)
	{
		final CartFilterParamsDto cartFilterParams = bcfTravelCartFacade
				.initializeCartFilterParams(null, null, Boolean.FALSE, Collections.emptyList());
		model.addAttribute(TRAVELLERS_GROUP_BY_JOURNEY, travellerFacade.getTravellersGroupedByJourney(cartFilterParams));
	}

	/**
	 * @param placeOrderForm The spring form of the order being submitted
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return String
	 * @throws CMSItemNotFoundException
	 * @throws InvalidCartException
	 * @throws CommerceCartModificationException
	 */
	@RequestMapping(value = "/proceedToPayment")
	public String proceedToPayment(@ModelAttribute("bcfPlaceOrderForm") final BCFPlaceOrderForm bcfPlaceOrderForm,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectModel,
			final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final List<String> emails = bcfPlaceOrderForm.getEmails();
		final List<String> emailAddresses = new ArrayList<>();
		for (final String email : emails)
		{
			if (!email.equalsIgnoreCase(""))
			{
				emailAddresses.add(email);
			}
		}
		if (validateOrderForm(bcfPlaceOrderForm, model))
		{
			return enterStep(model, redirectModel);
		}

		if (CollectionUtils.isNotEmpty(emailAddresses))
		{
			bcfTravelCartService.saveAdditionalEmails(emailAddresses);
		}
		return REDIRECT_PREFIX + BcfcheckoutaddonWebConstants.PAYMENT_DETAILS_PATH;
	}

	@Override
	protected String redirectToOrderConfirmationPage(final OrderData orderData)
	{
		final StringBuilder orderCodeToAppend = new StringBuilder();
		//if this is amend order flow, then original order code won't be null
		if (StringUtils.isNotBlank(orderData.getOriginalOrderCode()))
		{
			orderCodeToAppend.append(orderData.getOriginalOrderCode());
		}
		else
		{
			orderCodeToAppend.append(orderData.getCode());
		}
		return BcfcheckoutaddonWebConstants.REDIRECT_URL_BOOKING_CONFIRMATION + orderCodeToAppend.toString();
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm The spring form of the order being submitted
	 * @param model          A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final BCFPlaceOrderForm bcfPlaceOrderForm, final Model model)
	{
		boolean invalid = false;

		if (!bcfPlaceOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}

		final List<String> emails = bcfPlaceOrderForm.getEmails();
		final List<String> emailAddresses = new ArrayList<>();
		for (final String email : emails)
		{
			if (!email.equalsIgnoreCase(""))
			{
				emailAddresses.add(email);
			}
		}

		final int emailValidationLength = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("emailValidationLength"));

		if (validateEmail(emailAddresses, emailValidationLength))
		{
			bcfPlaceOrderForm.setEmails(emailAddresses);
			model.addAttribute("bcfPlaceOrderForm", bcfPlaceOrderForm);
			GlobalMessages.addErrorMessage(model, "checkout.error.email.invalid");
			invalid = true;
			return invalid;
		}

		return invalid;
	}

	private String getPreviousPageURL()
	{
		if (!travelCheckoutFacade.containsLongRoute())
		{
			return BcfcheckoutaddonWebConstants.ANCILLARY_ROOT_URL;
		}
		else
		{
			return BcfcheckoutaddonWebConstants.TRAVELLER_DETAILS_PATH;
		}
	}


	private boolean validateEmail(final List<String> emails, final int emailValidationLength)
	{
		boolean invalid = false;
		for (final String email : emails)
		{
			if (StringUtils.isEmpty(email) || StringUtils.length(email) > emailValidationLength || !EMAIL_REGEX.matcher(email)
					.matches())
			{
				invalid = true;
			}
		}
		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return REDIRECT_PREFIX + getPreviousPageURL();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return REDIRECT_PREFIX + BcfcheckoutaddonWebConstants.PAYMENT_DETAILS_PATH;
	}

	@Override
	protected void setCheckoutStepLinksForModel(final Model model, final CheckoutStep checkoutStep)
	{
		model.addAttribute("previousStepUrl", getPreviousPageURL());
		model.addAttribute("currentStepUrl", BcfcheckoutaddonWebConstants.SUMMARY_PAGE_URL);
		model.addAttribute("nextStepUrl", BcfcheckoutaddonWebConstants.PAYMENT_DETAILS_PATH);
		model.addAttribute("progressBarId", checkoutStep.getProgressBarId());
	}

	@ModelAttribute("reservationCode")
	public String getReservationCode()
	{
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			return bcfTravelCartFacade.getOriginalOrderCode();
		}
		return bcfTravelCartFacade.getCurrentCartCode();
	}

	@ModelAttribute("disableCurrencySelector")
	public Boolean getDisableCurrencySelector()
	{
		return Boolean.TRUE;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}

	protected ConfirmBookingIntegrationFacade getConfirmBookingIntegrationFacade()
	{
		return confirmBookingIntegrationFacade;
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
