/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.constants.BcfcheckoutaddonWebConstants;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps.util.CheckoutStepUtil;
import com.bcf.bcfcheckoutaddon.forms.ConfirmBookingForm;
import com.bcf.bcfcheckoutaddon.forms.PaymentDetailsForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfstorefrontaddon.util.BcfPageUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;
import com.bcf.facades.payment.BcfPaymentFacade;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;


public abstract class AbstractPaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	private static final String PAYMENT_METHOD = "payment-method";
	private static final String PAYMENT_METHOD_CMS_PAGE_LABEL = "paymentMethodPage";
	private static final String SPECIAL_SERVICE_REQUEST_COMMENT = "specialServiceRequestComment";
	private static final String SPECIAL_SERVICE_REQUEST_COMMENT_ALLOWED = "specialServiceRequestCommentAllowed";
	private static final String AGENT_COMMENT = "agentComment";
	private static final String MY_ACCOUNT_BOOKING_DETAILS = "/my-account/my-bookings";
	private static final String MANAGE_CARDS_URL = "manageCardsURL";
	private static final String MANAGE_CARDS_URL_PERSONAL = "/my-account/payment-cards";
	private static final String MANAGE_CARDS_URL_BUSINESS = "/my-business-account/payment-cards";
	private static final String CANCEL_URL = "cancelUrl";

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfPaymentFacade")
	private BcfPaymentFacade bcfPaymentFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "userService")
	private BcfUserService userService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "defaultBcfTravelCartFacade")
	private DefaultBcfTravelCartFacade defaultBcfTravelCartFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "userFacade")
	private BcfUserFacade bcfUserFacade;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "travelCommercePriceFacade")
	private TravelCommercePriceFacade travelCommercePriceFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService travelCartService;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfPageUtil")
	private BcfPageUtil bcfPageUtil;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return getCheckoutFacade().getSupportedCardTypes();
	}

	@ModelAttribute("months")
	protected List<SelectOption> getMonths()
	{
		return CheckoutStepUtil.getMonths();
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		return CheckoutStepUtil.getStartYears();
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		return CheckoutStepUtil.getExpiryYears();
	}

	@ModelAttribute("paymentTokenizerClientID")
	public String getClientId()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.clientId");
	}

	@ModelAttribute("paymentTokenizerHostURL")
	public String getClientHost()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.hostURL");
	}

	@ModelAttribute("paymentTokenizerURL")
	public String getTokenizerHost()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.tokenizerURL");
	}

	@Override
	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return bcfTravelCheckoutFacade.getDeliveryCountries();
	}

	@Override
	protected void prepareDataForPage(final Model model)
	{
		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("supportedCountries", bcfTravelCheckoutFacade.getDeliveryCountries());
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(getCheckoutFacade().isExpressCheckoutAllowedForCart()));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(getCheckoutFacade().isTaxEstimationEnabledForCart()));
	}




	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		if (!getBcfTravelCartFacade().isCurrentCartValid())
		{
			return REDIRECT_PREFIX + "/";
		}
		else if (!getBcfTravelCartFacade().isAmendCurrentCartValid())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.invalid.amend.cart");
			return REDIRECT_PREFIX + MY_ACCOUNT_BOOKING_DETAILS;
		}

		model.addAttribute("paymentFormUrl", getPaymentSubmitButtonUrl());
		model.addAttribute(BcfCoreConstants.IS_B2B_CUSTOMER, getSessionService()
				.getAttribute(BcfCoreConstants.IS_B2B_CUSTOMER));
		model.addAttribute("bookingJourney", bcfTravelCheckoutFacade.getBookingJourney());
		model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS, bcfTravelCartFacade.getPackageAvailabilityStatus());
		final boolean anonymousUser = getUserFacade().isAnonymousUser();
		if (!anonymousUser)
		{
			final CustomerData customerData = bcfTravelCheckoutFacade.getCustomerData();
			model.addAttribute("customerData", customerData);
		}
		else
		{
			model.addAttribute("customerData", new CustomerData());
		}
		setupAddPaymentPage(model);
		getSessionService().removeAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		final String specialServiceRequestCommentAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(SPECIAL_SERVICE_REQUEST_COMMENT);
		if (StringUtils.isNotBlank(specialServiceRequestCommentAllowed) && StringUtils
				.equalsIgnoreCase(specialServiceRequestCommentAllowed, "true"))
		{
			model.addAttribute(SPECIAL_SERVICE_REQUEST_COMMENT_ALLOWED, specialServiceRequestCommentAllowed);
			final String agentComment = bcfTravelCartFacade.getAgentComment();
			model.addAttribute(AGENT_COMMENT, agentComment);
		}
		final List<CTCTCCardData> ctcTcCardDataList = bcfUserFacade.getCtcTcCardsPaymentInfos();
		model.addAttribute("savedCTCCard", ctcTcCardDataList);
		final boolean deferPayment = bcfTravelCheckoutFacade.hasCommercialVehiclesOnly();
		model.addAttribute("deferPayment", deferPayment);
		model.addAttribute("isSubscriptionOnly",
				AccountType.SUBSCRIPTION_ONLY.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()));
		model.addAttribute("vacationPolicyTermsAndConditions", bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(null));
		model.addAttribute("bcfNonRefundableCostTermsAndConditions",
				bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(null));
		model.addAttribute(MANAGE_CARDS_URL, userService.isB2BCustomer() ? MANAGE_CARDS_URL_BUSINESS : MANAGE_CARDS_URL_PERSONAL);
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		final ContentPageModel contentPage = getContentPageForLabelOrId(PAYMENT_METHOD_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.PaymentDetailsPostPage;
	}


	@RequestMapping(value = "/confirmBooking", method = RequestMethod.GET)
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStepConfirmBooking(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{

		if (!getBcfTravelCartFacade().isCurrentCartValid())
		{
			return REDIRECT_PREFIX + "/";
		}

		model.addAttribute(BcfCoreConstants.IS_B2B_CUSTOMER, getSessionService()
				.getAttribute(BcfCoreConstants.IS_B2B_CUSTOMER));
		model.addAttribute("bookingJourney", bcfTravelCheckoutFacade.getBookingJourney());
		model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS, bcfTravelCartFacade.getPackageAvailabilityStatus());
		final boolean anonymousUser = getUserFacade().isAnonymousUser();
		if (!anonymousUser)
		{
			final CustomerData customerData = bcfTravelCheckoutFacade.getCustomerData();
			model.addAttribute("customerData", customerData);
		}
		else
		{
			model.addAttribute("customerData", new CustomerData());
		}
		getSessionService().removeAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		final String specialServiceRequestCommentAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(SPECIAL_SERVICE_REQUEST_COMMENT);
		if (StringUtils.isNotBlank(specialServiceRequestCommentAllowed) && StringUtils
				.equalsIgnoreCase(specialServiceRequestCommentAllowed, "true"))
		{
			model.addAttribute(SPECIAL_SERVICE_REQUEST_COMMENT_ALLOWED, specialServiceRequestCommentAllowed);
			final String agentComment = bcfTravelCartFacade.getAgentComment();
			model.addAttribute(AGENT_COMMENT, agentComment);
		}
		final ContentPageModel contentPage = getContentPageForLabelOrId(PAYMENT_METHOD_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		model.addAttribute("isSubscriptionOnly",
				AccountType.SUBSCRIPTION_ONLY.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()));
		model.addAttribute("vacationPolicyTermsAndConditions", bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(null));
		model.addAttribute("bcfNonRefundableCostTermsAndConditions",
				bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(null));
		model.addAttribute(MANAGE_CARDS_URL, userService.isB2BCustomer() ? MANAGE_CARDS_URL_BUSINESS : MANAGE_CARDS_URL_PERSONAL);
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		final ConfirmBookingForm confirmBookingForm = new ConfirmBookingForm();
		model.addAttribute("confirmBookingForm", confirmBookingForm);
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		model.addAttribute(BcfcheckoutaddonWebConstants.AMEND, getBcfTravelCartFacade().isAmendmentCart());
		model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID, contentPage.isDisplayOrderId());


		final Pair<String, Double> submitBookingUrlAndIsRefund = bcfTravelCartFacadeHelper
				.getSubmitBookingUrlAndIsRefund(bcfControllerUtil.getTotalToPay());
		model.addAttribute("submitButtonURL", submitBookingUrlAndIsRefund.getLeft());
		model.addAttribute("amountToPay", submitBookingUrlAndIsRefund.getRight());
		model.addAttribute("confirmBooking", true);

		model.addAttribute(CANCEL_URL, bcfTravelCartFacadeHelper.getPaymentCancelUrl());
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		return BcfcheckoutaddonControllerConstants.Views.Pages.MultiStepCheckout.confirmBookingPage;
	}


	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}



	@ModelAttribute("reservationCode")
	public String getReservationCode()
	{
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			return bcfTravelCartFacade.getOriginalOrderCode();
		}
		return bcfTravelCartFacade.getCurrentCartCode();
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		model.addAttribute(BcfcheckoutaddonWebConstants.AMEND, getBcfTravelCartFacade().isAmendmentCart());
		prepareDataForPage(model);
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		final CartModel sessionCart = cartService.getSessionCart();
		if (BookingJourneyType.BOOKING_PACKAGE.getCode()
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY))
				|| BookingJourneyType.BOOKING_ALACARTE.getCode()
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			final List<RoomGuestData> roomGuestDatas = getBcfTravelCartFacade()
					.getRoomDatas(sessionCart.getEntries().stream().filter(entry -> entry.getActive()).collect(
							Collectors.toList()));
			if (roomGuestDatas != null && roomGuestDatas.size() >= 1)
			{
				model.addAttribute(BcfFacadesConstants.ROOM_GUEST_DATAS, roomGuestDatas);

			}
			model.addAttribute("vendor", "hotel");
		}
		else if (BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode()
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			final List<ActivityGuestData> activityGuestDatas = getBcfTravelCartFacade()
					.getUniqueActivityDatas(sessionCart.getEntries());
			if (CollectionUtils.isNotEmpty(activityGuestDatas) && activityGuestDatas.size() > 1)
			{
				model.addAttribute(BcfFacadesConstants.ACTIVITY_DATAS, activityGuestDatas);
			}
			model.addAttribute("vendor", "Activity Vendor");
		}
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);
		setSavedPaymentInfos(model, paymentDetailsForm);
		model.addAttribute("paymentCardTypes", bcfPaymentFacade.getPaymentCardTypes());
		model.addAttribute(BcfcheckoutaddonWebConstants.HIDE_CONTINUE, "HIDE");
		final ContentPageModel contentPage = getContentPageForLabelOrId(PAYMENT_METHOD_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);


		}
		final boolean isAnonymous = getUserService().isAnonymousUser(getUserService().getCurrentUser()) || travelCartService
				.isGuestUserCart(cartService.getSessionCart());


		boolean hideNewCardAndBillingDetails = false;
		if (!isAnonymous && !paymentDetailsForm.isUseNewCard())
		{

			hideNewCardAndBillingDetails = true;
		}
		model.addAttribute("hideNewCardAndBillingDetails", hideNewCardAndBillingDetails);
		model.addAttribute("isAnonymous", isAnonymous);
		model.addAttribute("isAmendment", bcfTravelCartFacade.isAmendmentCart());
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		bcfPageUtil.storeCmsPageInModel(model, cmsPage);
	}

	protected void setSavedPaymentInfos(final Model model, final PaymentDetailsForm paymentDetailsForm)
	{
		final List<CCPaymentInfoData> savedPaymentInfos;
		if (!getUserFacade().isAnonymousUser())
		{
			savedPaymentInfos = getUserFacade().getCCPaymentInfos(true);
			if (CollectionUtils.isNotEmpty(savedPaymentInfos))
			{
				StreamUtil.safeStream(savedPaymentInfos).forEach(ccPaymentInfoData -> {
							ccPaymentInfoData
									.setExpired(isCardExpired(ccPaymentInfoData.getExpiryMonth(), ccPaymentInfoData.getExpiryYear()));
						}
				);
				model.addAttribute("paymentInfos", savedPaymentInfos);
				paymentDetailsForm.setUseNewCard(savedPaymentInfos.stream().allMatch(CCPaymentInfoData::isExpired));
			}
			else
			{
				paymentDetailsForm.setUseNewCard(true);
			}
		}
	}

	protected boolean isCardExpired(final String month, final String year)
	{
		final DateTimeFormatter ccMonthFormatter = DateTimeFormatter.ofPattern("MM/uu");
		final String creditCardExpiryDateString = month + "/" + year;
		try
		{
			final YearMonth lastValidMonth = YearMonth.parse(creditCardExpiryDateString, ccMonthFormatter);
			if (YearMonth.now(ZoneId.systemDefault()).isAfter(lastValidMonth))
			{
				return true;
			}
		}
		catch (final DateTimeParseException dtpe)
		{
			return true;
		}
		return false;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

	protected BcfSalesApplicationResolverFacade getBcfSalesApplicationResolverFacade()
	{
		return bcfSalesApplicationResolverFacade;
	}

	protected BcfTravelCheckoutFacade getBcfTravelCheckoutFacade()
	{
		return bcfTravelCheckoutFacade;
	}

	protected String getPaymentSubmitButtonUrl()
	{
		return "/checkout/multi/payment/placeOrder";
	}

	protected UserService getUserService()
	{
		return userService;
	}


	public BcfControllerUtil getBcfControllerUtil()
	{
		return bcfControllerUtil;
	}

	public void setBcfControllerUtil(final BcfControllerUtil bcfControllerUtil)
	{
		this.bcfControllerUtil = bcfControllerUtil;
	}
}
