/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps.MyAccountBasePaymentController;
import com.bcf.bcfcheckoutaddon.forms.AddPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.UpdatePaymentCardForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/my-business-account/payment-cards")
public class MyBusinessAccountPaymentController extends MyAccountBasePaymentController
{
	private static final Logger LOG = Logger.getLogger(MyBusinessAccountPaymentController.class);

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addPaymentCard(
			@RequestParam(value = "paymentMethod", defaultValue = "CreditCard", required = false) final String paymentMethod,
			final Model model) throws CMSItemNotFoundException
	{
		return getAddPaymentCardForm(paymentMethod, model);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addPaymentCard(final AddPaymentDetailsForm addPaymentDetailsForm, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		return addPaymentCard(addPaymentDetailsForm, request, bindingResult, redirectAttributes);
	}

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentCards(final Model model) throws CMSItemNotFoundException
	{
		return getPaymentCards(model);
	}

	@RequestMapping(value = "/remove/{paymentInfoId}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String removePaymentCard(
			@RequestParam(value = "cardType", defaultValue = "creditCard", required = false) final String cardType,
			@PathVariable final String paymentInfoId, final Model model, final RedirectAttributes redirectAttributes)
	{
		return removePaymentCard(cardType, paymentInfoId, redirectAttributes);
	}

	@RequestMapping(value = "/updateCTCCard", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateAccountPaymentCard(final UpdatePaymentCardForm updatePaymentCardForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		myAccountUpdatePaymentCardFormValidator.validate(updatePaymentCardForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			redirectAttributes.addFlashAttribute(PAYMENT_CARD_FORM_ATTR, updatePaymentCardForm);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
		}

		try
		{
			bcfCustomerFacade.updateAccountPaymentCard(populateAccountPaymentProfileInfo(updatePaymentCardForm));
			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, "text.payment.card.update.successful");
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.service.notAvailable");
			redirectAttributes.addFlashAttribute(PAYMENT_CARD_FORM_ATTR, updatePaymentCardForm);
		}

		return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
	}

	private CTCTCCardData populateAccountPaymentProfileInfo(final UpdatePaymentCardForm updatePaymentCardForm)
	{
		final CTCTCCardData ctcTcCardsPaymentInfoForCode = userFacade
				.getCtcTcCardsPaymentInfoForCode(updatePaymentCardForm.getCode());
		ctcTcCardsPaymentInfoForCode.setCardNumber(updatePaymentCardForm.getCode());
		ctcTcCardsPaymentInfoForCode.setCardType(BcfCoreConstants.CTC_TC_CARD);
		ctcTcCardsPaymentInfoForCode.setBillingAddress(populateAddressData(updatePaymentCardForm));
		return ctcTcCardsPaymentInfoForCode;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePaymentCard(final UpdatePaymentCardForm updatePaymentCardForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		return updatePaymentCard(updatePaymentCardForm, bindingResult, redirectAttributes);
	}
}
