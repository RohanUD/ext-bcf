package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.forms.BookingReferenceForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping(value = "/checkout/multi")
public class BookingReferenceUpdateController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(BookingReferenceUpdateController.class);
	private static final String ERROR_BOOKING_REFERENCE_UPDATE = "checkout.error.bookingreferenceupdate";

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "modifyBookingIntegrationFacade")
	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;


	@RequestMapping(value = "/bookingref-update", method = { RequestMethod.GET, RequestMethod.POST })
	public String initCheck(final Model model,
			@ModelAttribute("bookingReferenceForm") final BookingReferenceForm bookingReferenceForm,
			final BindingResult result, final RedirectAttributes redirectModel)
			throws InvalidCartException

	{
		final String sessionJourney=sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionJourney.equals(BookingJourneyType.BOOKING_ALACARTE.getCode()))
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.ALACARTE_REVIEW_PAGE_PATH;
		}

		final List<String> bookingRefs = bookingReferenceForm.getBookingRefs();
		final List<String> referenceCodes = bookingReferenceForm.getReferenceCodes();

		if (CollectionUtils.isNotEmpty(referenceCodes))
		{
			try
			{
				bcfTravelCheckoutFacade.saveReferenceCode(referenceCodes, bookingRefs);
				modifyBookingIntegrationFacade.updateCartWithMakeBookingResponse(cartService.getSessionCart(), true);
			}
			catch (final IntegrationException e)
			{
				LOG.error("IntegrationException encountered while performing update booking referenceCodes");
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_BOOKING_REFERENCE_UPDATE);
			}
		}

		if (cartService.getSessionCart().isFerryOptionBooking())
		{
			return REDIRECT_PREFIX + "/checkout/ferry-option-booking-details";
		}
		return REDIRECT_PREFIX + "/checkout/multi/payment-method/select-flow";
	}

	@RequestMapping(value = "/update-booking-ref-and-add-sailing", method = { RequestMethod.GET, RequestMethod.POST })
	public String updateAndAddNewSailing(@ModelAttribute("bookingReferenceForm") final BookingReferenceForm bookingReferenceForm,
			final RedirectAttributes redirectModel)
	{
		final List<String> bookingRefs = bookingReferenceForm.getBookingRefs();
		final List<String> referenceCodes = bookingReferenceForm.getReferenceCodes();

		if (CollectionUtils.isNotEmpty(referenceCodes))
		{
			try
			{
				bcfTravelCheckoutFacade.saveReferenceCode(referenceCodes, bookingRefs);
				modifyBookingIntegrationFacade.updateCartWithMakeBookingResponse(cartService.getSessionCart(), true);
			}
			catch (final IntegrationException e)
			{
				LOG.error("IntegrationException encountered while performing update booking referenceCodes");
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_BOOKING_REFERENCE_UPDATE);
			}
		}
		return REDIRECT_PREFIX + BcfFacadesConstants.REMOVE_FORM_AND_REDIRECT_TO_HOME_URL;
	}
}
