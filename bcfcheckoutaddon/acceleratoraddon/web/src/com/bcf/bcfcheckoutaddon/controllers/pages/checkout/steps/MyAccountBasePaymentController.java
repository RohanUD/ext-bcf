/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.travelfacades.facades.TravelI18NFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.forms.AddPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.UpdatePaymentCardForm;
import com.bcf.bcfcheckoutaddon.forms.validation.MyAccountAddPaymentCardFormValidator;
import com.bcf.bcfcheckoutaddon.forms.validation.MyAccountUpdatePaymentCardFormValidator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.bcfstorefrontaddon.util.PaymentCardUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.CountryDropDownUtil;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.payment.BcfPaymentFacade;
import com.bcf.facades.payment.CTCTCCardData;
import com.bcf.facades.payment.PaymentCardsData;
import com.bcf.integrations.core.exception.IntegrationException;


public class MyAccountBasePaymentController extends BcfAbstractPageController
{

	private static final Logger LOG = Logger.getLogger(MyAccountBasePaymentController.class);

	protected static final String REDIRECT_TO_PAYMENTS_CARDS_PAGE = REDIRECT_PREFIX + "/my-account/payment-cards";
	protected static final String REDIRECT_TO_ADD_PAYMENT_CARD_PAGE = REDIRECT_PREFIX + "/my-account/payment-cards/add";

	protected static final String PAYMENT_CARD_FORM_ATTR = "updatePaymentCardForm";
	public static final String FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.updatePaymentCardForm";
	protected static final String ACCOUNT_PAYMENT_CARD_FORM_ATTR = "accountUpdatePaymentCardForm";

	protected static final String ADD_PAYMENT_CARD_FORM_ATTR = "addPaymentDetailsForm";
	public static final String ADD_PAYMENT_CARD_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.addPaymentDetailsForm";

	protected static final String PAYMENT_CARDS_CMS_PAGE = "paymentCardsPage";
	protected static final String ADD_PAYMENT_CARDS_CMS_PAGE = "addPaymentCardsPage";

	protected static final String ADD_PAYMENT_METHOD = "addPaymentMethod";
	public static final String SERVICE_NOT_AVAILABLE = "text.service.notAvailable";

	@Resource(name = "bcfCustomerFacade")
	protected BCFCustomerFacade bcfCustomerFacade;

	@Resource(name = "userFacade")
	protected BcfUserFacade userFacade;

	@Resource(name = "myAccountAddPaymentCardFormValidator")
	protected MyAccountAddPaymentCardFormValidator myAccountAddPaymentCardFormValidator;

	@Resource(name = "myAccountUpdatePaymentCardFormValidator")
	protected MyAccountUpdatePaymentCardFormValidator myAccountUpdatePaymentCardFormValidator;

	@Resource(name = "travelI18NFacade")
	protected TravelI18NFacade travelI18NFacade;

	@Resource(name = "i18NFacade")
	protected I18NFacade i18NFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	protected BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfPaymentFacade")
	protected BcfPaymentFacade bcfPaymentFacade;

	@Resource(name = "userService")
	protected BcfUserService userService;

	protected String getPaymentCards(final Model model) throws CMSItemNotFoundException
	{
		PaymentCardsData paymentCards = null;
		try
		{
			if (userService.isB2BCustomer())
			{
				paymentCards = bcfCustomerFacade.getBusinessPaymentCards();
			}
			else
			{
				paymentCards = bcfCustomerFacade.getPaymentCards();
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error(
					"Exception while getting the payment cards from CRM for the customer " + userService.getCurrentUser().getUid());
			LOG.error(e.getMessage(), e);
			GlobalMessages.addErrorMessage(model, SERVICE_NOT_AVAILABLE);
		}
		if (Objects.nonNull(paymentCards))
		{
			model.addAttribute("paymentCards", paymentCards);
			final UpdatePaymentCardForm updatePaymentCardForm;
			if (model.asMap().containsKey(PAYMENT_CARD_FORM_ATTR))
			{
				updatePaymentCardForm = (UpdatePaymentCardForm) model.asMap().get(PAYMENT_CARD_FORM_ATTR);
			}
			else if (model.asMap().containsKey(ACCOUNT_PAYMENT_CARD_FORM_ATTR))
			{
				updatePaymentCardForm = (UpdatePaymentCardForm) model.asMap().get(ACCOUNT_PAYMENT_CARD_FORM_ATTR);
			}
			else
			{
				updatePaymentCardForm = new UpdatePaymentCardForm();
			}
			populateFormBeans(model, paymentCards, updatePaymentCardForm);
		}
		model.addAttribute(BcfCoreConstants.IS_B2B_CUSTOMER, userService.isB2BCustomer());
		return getPage(model, PAYMENT_CARDS_CMS_PAGE);
	}

	protected String getAddPaymentCardForm(
			@RequestParam(value = "paymentMethod", defaultValue = "CreditCard", required = false) String paymentMethod,
			final Model model) throws CMSItemNotFoundException
	{
		final AddPaymentDetailsForm addPaymentDetailsForm;
		if (model.asMap().containsKey(ADD_PAYMENT_CARD_FORM_ATTR))
		{
			addPaymentDetailsForm = (AddPaymentDetailsForm) model.asMap().get(ADD_PAYMENT_CARD_FORM_ATTR);
		}
		else
		{
			addPaymentDetailsForm = new AddPaymentDetailsForm();
		}

		model.addAttribute(ADD_PAYMENT_CARD_FORM_ATTR, addPaymentDetailsForm);

		if (model.asMap().containsKey(ADD_PAYMENT_METHOD))
		{
			paymentMethod = (String) model.asMap().get(ADD_PAYMENT_METHOD);
		}
		model.addAttribute(ADD_PAYMENT_METHOD, paymentMethod);

		if (StringUtils.equals(paymentMethod, BcfCoreConstants.CTC_TC_CARD))
		{
			addPaymentDetailsForm.setCardType(BcfCoreConstants.CTC_TC_CARD);
		}
		else
		{
			model.addAttribute("paymentCardTypes", bcfPaymentFacade.getPaymentCardTypes());
		}

		model.addAttribute(BcfCoreConstants.IS_B2B_CUSTOMER, userService.isB2BCustomer());
		return getPage(model, ADD_PAYMENT_CARDS_CMS_PAGE);
	}

	protected String updatePaymentCard(final UpdatePaymentCardForm updatePaymentCardForm, final BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		myAccountUpdatePaymentCardFormValidator.validate(updatePaymentCardForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			redirectAttributes
					.addFlashAttribute(PAYMENT_CARD_FORM_ATTR, updatePaymentCardForm);
			redirectAttributes.addFlashAttribute(FORM_BINDING_RESULT + updatePaymentCardForm.getFormNumber(), bindingResult);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
		}

		try
		{
			bcfCustomerFacade.updatePaymentCard(populatePaymentProfileInfo(updatePaymentCardForm));
			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "text.payment.card.update.successful");
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
			redirectAttributes.addFlashAttribute(PAYMENT_CARD_FORM_ATTR, updatePaymentCardForm);
		}

		return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
	}

	protected String addPaymentCard(final AddPaymentDetailsForm addPaymentDetailsForm, final HttpServletRequest request,
			final BindingResult bindingResult, final RedirectAttributes redirectAttributes)
	{
		myAccountAddPaymentCardFormValidator.validate(addPaymentDetailsForm, bindingResult);
		final String addPaymentMethod = StringUtils.equals(addPaymentDetailsForm.getCardType(), BcfCoreConstants.CTC_TC_CARD) ?
				BcfCoreConstants.CTC_TC_CARD : BcfCoreConstants.CREDIT_CARD;

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			redirectAttributes.addFlashAttribute(ADD_PAYMENT_CARD_FORM_ATTR, addPaymentDetailsForm);
			redirectAttributes.addFlashAttribute(ADD_PAYMENT_CARD_FORM_BINDING_RESULT, bindingResult);
			redirectAttributes.addFlashAttribute("regions", getRegions(addPaymentDetailsForm.getBillingAddress().getCountryIso()));
			redirectAttributes.addFlashAttribute(ADD_PAYMENT_METHOD, addPaymentMethod);
			return REDIRECT_TO_ADD_PAYMENT_CARD_PAGE;
		}
		try
		{
			bcfCustomerFacade.addPaymentCard(getRequestParameterMap(request));
			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "text.payment.card.add.successful");
		}
		catch (final IntegrationException | ModelSavingException ex)
		{
			LOG.error(ex.getMessage(), ex);
			redirectAttributes.addFlashAttribute(ADD_PAYMENT_CARD_FORM_ATTR, addPaymentDetailsForm);
			redirectAttributes.addFlashAttribute(ADD_PAYMENT_METHOD, addPaymentMethod);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
			return REDIRECT_TO_ADD_PAYMENT_CARD_PAGE;
		}
		return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
	}

	protected String removePaymentCard(
			@RequestParam(value = "cardType", defaultValue = "creditCard", required = false) final String cardType,
			@PathVariable final String paymentInfoId,
			final RedirectAttributes redirectAttributes)
	{
		try
		{
			bcfCustomerFacade.removePaymentCard(cardType, paymentInfoId);
			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "text.payment.card.remove.successful");
		}
		catch (final IntegrationException | ModelNotFoundException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}

		return REDIRECT_TO_PAYMENTS_CARDS_PAGE;
	}

	private CCPaymentInfoData populatePaymentProfileInfo(final UpdatePaymentCardForm updatePaymentCardForm)
	{
		final CCPaymentInfoData ccPaymentInfoData = userFacade.getCCPaymentInfoForCode(updatePaymentCardForm.getCode());
		final AddressData addressData = populateAddressData(updatePaymentCardForm);

		ccPaymentInfoData.setBillingAddress(addressData);

		return ccPaymentInfoData;
	}

	protected AddressData populateAddressData(final UpdatePaymentCardForm updatePaymentCardForm)
	{
		final AddressData addressData = new AddressData();
		addressData.setLine1(updatePaymentCardForm.getAddressLine1());
		addressData.setTown(updatePaymentCardForm.getCity());
		addressData.setPostalCode(updatePaymentCardForm.getZipCode());
		if (StringUtils.isNotEmpty(updatePaymentCardForm.getProvince()))
		{
			final RegionData regionData = new RegionData();
			regionData.setIsocode(updatePaymentCardForm.getProvince());
			addressData.setRegion(regionData);
		}
		final CountryData countryData = new CountryData();
		countryData.setIsocode(updatePaymentCardForm.getCountry());
		addressData.setCountry(countryData);
		return addressData;
	}

	private void populateFormBeans(final Model model, final PaymentCardsData paymentCardsData,
			final UpdatePaymentCardForm updatedPaymentCardForm)
	{
		populateFormBeanForCreditCards(model, paymentCardsData.getSavedCards(), updatedPaymentCardForm);
		populateFormBeanForCTCTCCards(model, paymentCardsData.getSavedCtcTcCards(), updatedPaymentCardForm);
	}

	private void populateFormBeanForCreditCards(final Model model, final List<CCPaymentInfoData> savedCards,
			final UpdatePaymentCardForm updatedPaymentCardForm)
	{
		if (CollectionUtils.isNotEmpty(savedCards))
		{
			final Map<String, List<RegionData>> regionsByCountry = new HashMap<>();
			int i = 0;
			for (final CCPaymentInfoData ccPaymentInfoData : savedCards)
			{
				i += 1;
				if (StringUtils.isNotEmpty(updatedPaymentCardForm.getCode()) && StringUtils
						.equals(updatedPaymentCardForm.getCode(), ccPaymentInfoData.getId()))
				{
					if (Objects.nonNull(updatedPaymentCardForm.getCountry()))
					{
						final String countryIsocode = updatedPaymentCardForm.getCountry();
						final List<RegionData> regions = this.getRegions(countryIsocode);
						regionsByCountry.put(updatedPaymentCardForm.getCountry(), regions);
					}
					model.addAttribute("editedUpdatePaymentCardForm", updatedPaymentCardForm);
					model.addAttribute(PAYMENT_CARD_FORM_ATTR + i, updatedPaymentCardForm);
					model.addAttribute("expand", "updatePaymentCard" + i);
					continue;
				}

				final UpdatePaymentCardForm updatePaymentCardForm = new UpdatePaymentCardForm();
				updatedPaymentCardForm.setFormNumber(i);
				updatePaymentCardForm.setCode(ccPaymentInfoData.getId());
				if (StringUtils.isNotEmpty(ccPaymentInfoData.getAccountHolderName()))
				{
					final String accountHolderName = ccPaymentInfoData.getAccountHolderName();
					try
					{
						updatePaymentCardForm.setCardHolderFirstName(accountHolderName.substring(0, accountHolderName.indexOf(' ')));
						updatePaymentCardForm.setCardHolderLastName(
								accountHolderName.substring(accountHolderName.indexOf(' ') + 1));
					}
					catch (final StringIndexOutOfBoundsException siob)
					{
						LOG.error("Last name of the account holder for the card " + ccPaymentInfoData.getId() + " is not found");
						updatePaymentCardForm.setCardHolderFirstName(accountHolderName);
					}

				}
				updatePaymentCardForm.setCardType(ccPaymentInfoData.getCardType());
				final AddressData billingAddress = ccPaymentInfoData.getBillingAddress();
				populateBillingAddress(updatePaymentCardForm, billingAddress, regionsByCountry);
				model.addAttribute(PAYMENT_CARD_FORM_ATTR + i, updatePaymentCardForm);
			}
			model.addAttribute("cc_regionsByCountry", regionsByCountry);
		}
	}

	private void populateFormBeanForCTCTCCards(final Model model, final List<CTCTCCardData> savedCards,
			final UpdatePaymentCardForm updatedPaymentCardForm)
	{
		if (CollectionUtils.isNotEmpty(savedCards))
		{
			final Map<String, List<RegionData>> regionsByCountry = new HashMap<>();
			int i = 0;
			for (final CTCTCCardData ccPaymentInfoData : savedCards)
			{
				i += 1;
				if (StringUtils.isNotEmpty(updatedPaymentCardForm.getCode()) && StringUtils
						.equals(updatedPaymentCardForm.getCode(), ccPaymentInfoData.getCode()))
				{
					model.addAttribute(ACCOUNT_PAYMENT_CARD_FORM_ATTR + i, updatedPaymentCardForm);
					model.addAttribute("expand", "updateCtcTcPaymentCard" + i);
					continue;
				}

				final UpdatePaymentCardForm updatePaymentCardForm = new UpdatePaymentCardForm();
				updatePaymentCardForm.setCode(ccPaymentInfoData.getCode());
				updatePaymentCardForm.setCardType(BcfCoreConstants.CTC_TC_CARD);
				final AddressData billingAddress = ccPaymentInfoData.getBillingAddress();
				populateBillingAddress(updatePaymentCardForm, billingAddress, regionsByCountry);
				model.addAttribute(ACCOUNT_PAYMENT_CARD_FORM_ATTR + i, updatePaymentCardForm);
			}
			model.addAttribute("ctc_regionsByCountry", regionsByCountry);
		}
	}

	private void populateBillingAddress(final UpdatePaymentCardForm updatePaymentCardForm, final AddressData billingAddress,
			final Map<String, List<RegionData>> regionsByCountry)
	{
		if (Objects.nonNull(billingAddress))
		{
			updatePaymentCardForm.setAddressLine1(billingAddress.getLine1());
			updatePaymentCardForm.setCity(billingAddress.getTown());
			updatePaymentCardForm.setZipCode(billingAddress.getPostalCode());
			if (Objects.nonNull(billingAddress.getRegion()))
			{
				updatePaymentCardForm.setProvince(billingAddress.getRegion().getIsocode());
			}
			if (Objects.nonNull(billingAddress.getCountry()))
			{
				final String countryIsocode = billingAddress.getCountry().getIsocode();
				updatePaymentCardForm.setCountry(countryIsocode);
				final List<RegionData> regions = this.getRegions(countryIsocode);
				if (!regionsByCountry.containsKey(countryIsocode))
				{
					regionsByCountry.put(countryIsocode, regions);
				}
			}
		}
	}

	private String getPage(final Model model, final String cmsPageId) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageId));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageId));
		return getViewForPage(model);
	}

	protected Map<String, String> getRequestParameterMap(final HttpServletRequest request)
	{
		final Map<String, String> map = new HashMap<String, String>();

		final Enumeration myEnum = request.getParameterNames();
		while (myEnum.hasMoreElements())
		{
			final String paramName = (String) myEnum.nextElement();
			final String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}
		if (!map.containsKey(BcfCoreConstants.CTC_CARD_NUMBER))
		{
			map.put(BcfCoreConstants.ON_REQUEST_ORDER, BcfCoreConstants.PACKAGE_ON_REQUEST);
			map.put(BcfstorefrontaddonWebConstants.BOOKING_PACKAGE, BcfstorefrontaddonWebConstants.BOOKING_ACTIVITY_ONLY);
		}

		map.put(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		map.put(BookingJourneyType._TYPECODE, BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode());

		return map;
	}

	@ModelAttribute("paymentTokenizerClientID")
	public String getClientId()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.clientId");
	}

	@ModelAttribute("paymentTokenizerHostURL")
	public String getClientHost()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.hostURL");
	}

	@ModelAttribute("paymentTokenizerURL")
	public String getTokenizerHost()
	{
		return getConfigurationService().getConfiguration().getString("payment.tokenizer.tokenizerURL");
	}

	@ModelAttribute("months")
	protected List<AbstractCheckoutController.SelectOption> getMonths()
	{
		return PaymentCardUtil.getMonths();
	}

	@ModelAttribute("expiryYears")
	public List<AbstractCheckoutController.SelectOption> getExpiryYears()
	{
		return PaymentCardUtil.getExpiryYears();
	}

	@ModelAttribute("countries")
	protected List<CountryData> getCountries()
	{
		final List<CountryData> allCountry = travelI18NFacade.getAllCountries();
		return new CountryDropDownUtil().getAllCountry(allCountry);
	}

	@RequestMapping(value = "/getRegions")
	public @ResponseBody
	List<RegionData> getRegions(@RequestParam final String countryIsoCode)
	{
		if (StringUtils.isEmpty(countryIsoCode))
		{
			return Collections.emptyList();
		}
		return i18NFacade.getRegionsForCountryIso(countryIsoCode);
	}

	@ModelAttribute(BcfstorefrontaddonControllerConstants.ModelAttributes.AccountPages.TITLE)
	public String getTitle()
	{
		return "text.pageTitle.account.paymentCards";
	}
}
