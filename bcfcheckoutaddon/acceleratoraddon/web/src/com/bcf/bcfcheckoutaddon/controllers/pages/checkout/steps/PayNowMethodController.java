/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping(value = "/checkout/paynow")
public class PayNowMethodController extends AbstractPaymentMethodCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(PayNowMethodController.class);
	private static final String ERROR_PLACE_ORDER = "checkout.error.placeorder";
	public static final String PAY_NOW_GENERIC_ERROR_CODE = "accommodation.pay.now.generic.error";
	private static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String PAY_NOW_PAYMENT_METHOD_CMS_PAGE_LABEL = "payNowPaymentMethodPage";

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "channelSpecificPlaceOrderErrorMap")
	private Map<String, String> channelSpecificPlaceOrderErrorMap;

	@Resource(name = "channelSpecificRefundOrderErrorMap")
	private Map<String, String> channelSpecificRefundOrderErrorMap;

	@Resource(name = "channelSpecificPermanentTokenErrorMap")
	private Map<String, String> channelSpecificPermanentTokenErrorMap;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;


	@RequestMapping(value = "/{bookingReference}", method = RequestMethod.GET)
	public String paynow(@PathVariable(name = "bookingReference") final String bookingReference, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		model.addAttribute("paymentFormUrl", getPaymentSubmitButtonUrl(bookingReference));
		setupAddPaymentPage(model);
		final PriceData amount = bookingFacade.getAmountToBePaidForOrder(bookingReference);
		if (amount.getValue().doubleValue() <= 0d)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					PAY_NOW_GENERIC_ERROR_CODE);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
		}
		model.addAttribute("isAmountDue", amount.getValue().doubleValue() > 0d);
		model.addAttribute("amountToPay", bookingFacade.getAmountToPay(bookingReference));
		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW, bookingReference);

		final ContentPageModel contentPage = getContentPageForLabelOrId(PAY_NOW_PAYMENT_METHOD_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);

		return BcfcheckoutaddonControllerConstants.Views.Pages.Order.PayNowPaymentDetailsPostPage;
	}

	@RequestMapping(value = "/complete-payment/{bookingReference}", method = RequestMethod.POST)
	public String completePaymentForWeb(@PathVariable(name = "bookingReference") final String bookingReference,
			final HttpServletRequest request, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		return completePayment(bookingReference, request, model, redirectAttributes);
	}

	@RequestMapping(value = "/confirm/{bookingReference}", method = RequestMethod.GET)
	public String completePaymentForTravelCentre(@PathVariable(name = "bookingReference") final String bookingReference,
			final HttpServletRequest request, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		return completePayment(bookingReference, request, model, redirectAttributes);
	}

	protected String completePayment(final String bookingReference,
			final HttpServletRequest request, final Model model, final RedirectAttributes redirectAttributes)
	{
		removeSessionAttributes();
		final Map<String, String> resultMap = getRequestParameterMap(request);
		final String channel = getBcfSalesApplicationResolverFacade().getCurrentSalesChannel();
		final PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
		try
		{
			getBcfTravelCheckoutFacade().completePayment(resultMap, channel, decisionData, bookingReference);
			if (StringUtils.isEmpty(decisionData.getError()))
			{
				return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
			}
			if (StringUtils.equalsIgnoreCase(decisionData.getMethod(), BcfFacadesConstants.PROCESS_PAYMENT))
			{
				final Object[] localizationArguments =
						{ decisionData.getResponseCode(), decisionData.getError() };
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						channelSpecificPlaceOrderErrorMap.get(channel), localizationArguments);
			}
			else if (StringUtils.equalsIgnoreCase(decisionData.getMethod(), BcfFacadesConstants.GET_PERMANENT_TOKEN))
			{
				final Object[] localizationArguments =
						{ decisionData.getResponseCode(), decisionData.getError() };
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						channelSpecificPermanentTokenErrorMap.get(channel), localizationArguments);
			}
			else
			{
				GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error("Integration exception encountered for while placing order", e);
			GlobalMessages.addErrorMessage(model,
					e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).findFirst().get());
		}
		catch (final OrderProcessingException e)
		{
			LOG.error("Exception encountered for while placing order", e);
			GlobalMessages.addErrorMessage(model, ERROR_PLACE_ORDER);
		}
		return BcfcheckoutaddonControllerConstants.Views.Pages.Order.PayNowPaymentDetailsPostPage;
	}

	protected void removeSessionAttributes()
	{
		getSessionService().removeAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
	}

	protected String getPaymentSubmitButtonUrl(final String bookingReference)
	{
		return "/checkout/paynow/complete-payment/"+bookingReference;
	}

	protected UserService getUserService()
	{
		return userService;
	}
}
