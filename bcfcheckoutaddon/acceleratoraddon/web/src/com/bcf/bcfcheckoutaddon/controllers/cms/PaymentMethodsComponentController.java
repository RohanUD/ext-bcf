/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers.cms;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcheckoutaddon.controllers.BcfcheckoutaddonControllerConstants;
import com.bcf.bcfcheckoutaddon.forms.DepositPaymentOptionForm;
import com.bcf.bcfcheckoutaddon.forms.GiftCardPaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.forms.PaymentDetailsForm;
import com.bcf.bcfcheckoutaddon.model.components.PaymentMethodsComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.GiftCardType;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.vacations.RoomGuestData;


@Controller("PaymentMethodsComponentController")
@RequestMapping(value = BcfcheckoutaddonControllerConstants.Views.Cms.PaymentMethodsComponent)
public class PaymentMethodsComponentController extends SubstitutingCMSAddOnComponentController<PaymentMethodsComponentModel>
{
	private static final Logger LOG = Logger.getLogger(PaymentMethodsComponentController.class);

	private static final String CARD = "card";
	private static final String CASH = "cash";
	private static final String GIFT = "gift";
	private static final String PAYMENT_URL = "/checkout/multi/payment/{paymentType}/pay";


	@Resource(name="allowedPaymentMethodsPerSalesChannel")
	private Map<String, String> allowedPaymentMethodsPerSalesChannel;

	@Resource(name="paymentFormsMap")
	private Map<String, String> paymentFormsMap;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name="bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "cartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "userFacade")
	private BcfUserFacade bcfUserFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	private void removeDeposit(final String bookingReference, final List<String> allowedPaymentMethods) {
		if (StringUtils.isNotEmpty(bookingReference)) {
			allowedPaymentMethods.removeIf(allowedPaymentMethod -> BcfCoreConstants.DEPOSIT.equalsIgnoreCase(allowedPaymentMethod));
		}
	}

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final PaymentMethodsComponentModel component)
	{
		final String currentSalesChannel = bcfSalesApplicationResolverFacade.getCurrentSalesChannel();
		final String paymentMethodsCommaSeparatedString = allowedPaymentMethodsPerSalesChannel.get(currentSalesChannel);
		final String bookingReference = sessionService.getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
		final List<String> allowedPaymentMethods = BCFPropertiesUtils.convertToList(paymentMethodsCommaSeparatedString);
		removeDeposit(bookingReference, allowedPaymentMethods);

		model.addAttribute("allowedPaymentMethods", allowedPaymentMethods);
		setPaymentForms(allowedPaymentMethods, model);
		PriceData amountToPay = StringUtils.isBlank(bookingReference) ? bcfTravelCartFacade.getAmountToPay() : bookingFacade.getAmountToBePaidForOrder(bookingReference);
		model.addAttribute("amountToPay", amountToPay);
		model.addAttribute("nextPageUrl", determineNextPageUrl(currentSalesChannel, bookingReference));
		final CartModel sessionCart = bcfTravelCartService.getSessionCart();
		if (BookingJourneyType.BOOKING_PACKAGE.getCode()
				.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)) || BookingJourneyType.BOOKING_ALACARTE.getCode()
				.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			final List<RoomGuestData> roomGuestDatas = getBcfTravelCartFacade().getRoomDatas(sessionCart.getEntries().stream().filter(entry->entry.getActive()).collect(
					Collectors.toList()));
			if (roomGuestDatas != null && roomGuestDatas.size()>1){
				model.addAttribute(BcfFacadesConstants.ROOM_GUEST_DATAS, roomGuestDatas);

			}

		}
		model.addAttribute("bookingReference", bookingReference);
		model.addAttribute("actualAmountToPay", amountToPay);
		model.addAttribute("paymentInfos", bcfUserFacade.isAnonymousUser() ? null : bcfUserFacade.getCCPaymentInfos(true));
	}

	protected void setPaymentForms(final List<String> allowedPaymentMethods, final Model model)
	{
		for (final String paymentMethod : allowedPaymentMethods)
		{
			final String className = paymentFormsMap.get(paymentMethod);
			Object form = null;
			try
			{
				final Class clazz = Class.forName(className);
				form = clazz.newInstance();
			}
			catch (final ClassNotFoundException e)
			{
				LOG.error("Cannot find class for:" + paymentMethod, e);
			}
			catch (final IllegalAccessException | InstantiationException e)
			{
				LOG.error("failed to initialize payment details form for payment method:" +paymentMethod, e);
			}

			if(form instanceof PaymentDetailsForm)
			{
				model.addAttribute("paymentDetailsForm", form);
				model.addAttribute("paymentByCardFormUrl", getPaymentSubmitButtonUrl(CARD));
			}
			else if(form instanceof GiftCardPaymentDetailsForm)
			{
				final Map<String, String> giftCardTypeMap = new TreeMap<String, String>();
				enumerationService.getEnumerationValues(GiftCardType.class).stream().forEach(
						giftCardType -> {
							giftCardTypeMap.put(giftCardType.getCode(), enumerationService.getEnumerationName(giftCardType));
						}
				);

				model.addAttribute("giftCardTypes", giftCardTypeMap);

				model.addAttribute("giftPaymentDetailsForm", form);
				model.addAttribute("paymentByGiftFormUrl", getPaymentSubmitButtonUrl(GIFT));
			}
			else if (form instanceof DepositPaymentOptionForm) {
				model.addAttribute("depositPaymentOptionForm", form);
			}
			else
			{
				model.addAttribute("cashPaymentDetailsForm", form);
				model.addAttribute("paymentByCashFormUrl", getPaymentSubmitButtonUrl(CASH));
			}
		}

	}

	protected Object determineNextPageUrl(final String currentSalesChannel, final String bookingReferecne)
	{
		if(SalesApplication.TRAVELCENTRE.equals(SalesApplication.valueOf(currentSalesChannel)))
		{
			if(StringUtils.isNotBlank(bookingReferecne))
			{
				return getPaymentSubmitButtonUrlForPayNow(bookingReferecne);
			}
			return "/checkout/multi/payment/travelcentre/placeOrder";
		}
		return "/checkout/multi/payment/placeOrder";
	}

	protected String getPaymentSubmitButtonUrl(final String paymentType)
	{
		return PAYMENT_URL.replaceAll("\\{paymentType}", paymentType);
	}

	protected String getPaymentSubmitButtonUrlForPayNow(final String bookingReference)
	{
		return "/checkout/paynow/confirm/"+bookingReference;
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}

}
