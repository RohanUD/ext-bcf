/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.controllers;


import com.bcf.bcfcheckoutaddon.model.components.PaymentMethodsComponentModel;


public interface BcfcheckoutaddonControllerConstants
{

	String ADDON_PREFIX = "addon:/bcfcheckoutaddon/";

	/**
	 * Class with view name constants
	 */
	interface Views
	{

		interface Pages
		{

			interface Order
			{
				String PayNowPaymentDetailsPostPage = ADDON_PREFIX + "pages/order/payNowPaymentDetailsPostPage";
				String PayNowConfirmationPage = ADDON_PREFIX + "pages/order/paynow/confirmation";
			}

			interface MultiStepCheckout
			{
				String AddPaymentMethodPage = ADDON_PREFIX + "pages/checkout/multi/addPaymentMethodPage";
				String PaymentDetailsPostPage = ADDON_PREFIX + "pages/checkout/multi/paymentDetailsPostPage";
				String confirmBookingPage = ADDON_PREFIX + "pages/checkout/multi/confirmBookingPage";

				String CheckoutSummaryPage = ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
				String ChoosePaymentTypePage = ADDON_PREFIX + "pages/checkout/multi/choosePaymentTypePage";
			}

			interface Payment
			{
				String PayNowResponse = ADDON_PREFIX + "pages/order/paynow/payNowJSONResponse";
			}

		}

		interface Fragments
		{

			interface Checkout
			{
				String BillingAddressForm = "fragments/checkout/billingAddressForm";
			}
		}

		interface Cms
		{

			String _Prefix = "/view/";

			String _Suffix = "Controller";

			/**
			 * CMS components that have specific handlers
			 */
			String PaymentMethodsComponent = _Prefix + PaymentMethodsComponentModel._TYPECODE + _Suffix;
		}
	}
}
