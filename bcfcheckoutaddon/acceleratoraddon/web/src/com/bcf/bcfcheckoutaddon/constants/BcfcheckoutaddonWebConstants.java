/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.constants;

/**
 * Global class for all Bcfcheckoutaddon web constants. You can add global constants for your extension into this class.
 */
public final class BcfcheckoutaddonWebConstants
{
	private BcfcheckoutaddonWebConstants()
	{
		// empty to avoid instantiating this constant class
	}

	public static final String REDIRECT_PREFIX = "redirect:";
	public static final String NEXT_URL = "nextURL";
	public static final String HIDE_CONTINUE = "hideContinue";
	public static final String RESERVATION = "reservation";
	public static final String HIDE = "HIDE";
	public static final String REDIRECT_URL_BOOKING_CONFIRMATION = REDIRECT_PREFIX + "/checkout/bookingConfirmation/";
	public static final String AMEND = "amend";

	// URL CONSTANTS
	public static final String TRAVELLER_DETAILS_PATH = "/traveller-details";
	public static final String PAYMENT_DETAILS_PATH = "/checkout/multi/payment-method/select-flow";
	public static final String SUMMARY_PAGE_URL = "/checkout/multi/summary/view";
	public static final String ANCILLARY_ROOT_URL = "/ancillary";
}
