<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="paymentmethod" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi/cms"%>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group row">
				<div class="col-xs-12">
					<spring:theme code="text.total.amountToPay.label" />
					<p>${actualAmountToPay.formattedValue}</p>
					<spring:htmlEscape defaultHtmlEscape="false" />
					<div class="y_makePaymentErrorBody alert alert-danger hidden"></div>
					<div id="y_remainingAmount" class="y_remainingAmount hidden"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<c:if test="${fn:length(allowedPaymentMethods) > 0}">
	<c:forEach items="${allowedPaymentMethods}" var="paymentMethod" varStatus="i">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<c:if test="${paymentMethod eq 'Deposit'}">
						<h4>
							<spring:theme code="text.payment.method.title.deposit" text="Deposit" />
						</h4>
						<paymentmethod:depositPaymentOption paymentType="${paymentMethod}" />
					</c:if>
					<c:if test="${paymentMethod eq 'Cash'}">
						<h4>
							<spring:theme code="text.payment.method.title.cash" text="Cash payment" />
						</h4>
						<paymentmethod:cashPaymentMethod paymentType="${paymentMethod}" />
					</c:if>
					<c:if test="${paymentMethod eq 'GiftCard'}">
						<h4>
							<spring:theme code="text.payment.method.title.gift.card" text="Gift card payment" />
						</h4>
						<paymentmethod:giftCardPaymentMethod paymentType="${paymentMethod}" />
					</c:if>
					<c:if test="${paymentMethod eq 'Card'}">
						<h4>
							<spring:theme code="text.payment.method.title.card" text="Card payment" />
						</h4>
						<paymentmethod:cardPaymentMethod />
					</c:if>
				</div>
			</div>
		</div>
	</c:forEach>
</c:if>
