<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/address"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="voucher" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/voucher"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="packageprogress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="ferryprogress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ferryprogress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="paymentmethod" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi/cms"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="termsandconditions" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/termsandconditions"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<c:set var="receiptNoidLabel">
	<spring:theme code="checkout.paymentType.receiptNo.placeholder" />
</c:set>
	<c:choose>
		<c:when test="${'BOOKING_TRANSPORT_ONLY' eq bookingJourney}">
			<ferryprogress:fareFinderProgressBar stage="payment" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
		</c:when>
		<c:otherwise>
			<packageprogress:travelBookingProgressBar stage="confirmation" amend="${amend}" bookingJourney="${bookingJourney}" />
		</c:otherwise>
	</c:choose>
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
<modifybooking:editBookingHeaderBar/>
<modifybooking:originalBookingDetails/>

    <c:choose>
        <c:when test="${availabilityStatus eq 'ON_REQUEST'}">
            <div class="total-price-section text-center">
                <p>
                    <b><spring:theme code="text.package.booking.on.request.review.title" /></b> <br>
                    <spring:theme code="text.package.booking.on.request.review.message1" arguments="${fn:escapeXml(vendor)}" />
                    <br>
                    <spring:theme code="text.package.booking.on.request.review.message2" arguments="${fn:escapeXml(vendor)}" />
                </p>
            </div>
        </c:when>
    </c:choose>

	<section>
		<div class="payment-details container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <c:choose>
                        <c:when test="${amountToPay < 0}">


                            <div class="confirm-booking alert alert-info text-center">
                                <p class="bcf bcf-icon-notice-outline bcf-2x"></p>
                                <h4><strong><spring:theme code="text.refund.reduce.price" text="Your change(s) will reduce the price of your booking" /></strong></h4>
                                <p class="apply-change"><spring:theme code="text.refund.changes.submit" text="Submit below to apply the change(s) to your booking and receive your refund." /></p>
                            </div>


                        </c:when>
                        <c:when test="${amountToPay > 0}">

                            <div class="confirm-booking alert alert-info text-center">
                                <p class="bcf bcf-icon-notice-outline bcf-2x"></p>
                                <h4><strong><spring:theme code="text.extra.payment" text="Your change(s) will need extra paymentt" /></strong></h4>
                                <p class="apply-change"><spring:theme code="text.extra.payment.changes.submit" text="Please submit below to apply the change(s) to your booking." /></p>
                            </div>

                        </c:when>
                        <c:otherwise>

                        <div class="confirm-booking alert alert-info text-center">
                            <p class="bcf bcf-icon-notice-outline bcf-2x"></p>
                            <h4><strong><spring:theme code="text.nopayment.changes" text="Your change(s) will not require extra payment" /></strong></h4>
                            <p class="apply-change"><spring:theme code="text.nopayment.changes.submit" text="Please submit below to apply the change(s) to your booking." /></p>
                        </div>

                        </c:otherwise>

                    </c:choose>
                    <c:if test="${'BOOKING_TRANSPORT_ONLY' ne bookingJourney}">
                        <h4 class="title payment-h2">
                            <spring:theme code="text.booking.details.changes" text="Booking details with changes" />
                         </h4>
                          </c:if >
                    <c:if test="${salesChannel=='Web'}">
                        <form:form id="confirmBookingDetailsForm" name="confirmBookingDetailsForm" commandName="confirmBookingForm" class="create_update_payment_form" action="${submitButtonURL}" method="POST">
                            <cms:pageSlot position="Reservation" var="feature" element="section">
                                <cms:component component="${feature}" />
                            </cms:pageSlot>
                            <termsandconditions:customertermsandconditions/>

                            <div class="container">
                                <div class="row my-5">
                                    <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
                                    <p class="margin-top-30 text-sm"><spring:theme code="text.payment.check.privacy.statement"/><br/>
                                    </p>
                                    </div>
                                 </div>
                            </div>

                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
                                        <input id="y_confirmBookingDetailsButton" class="btn btn-primary btn-block" type="submit" value="<spring:theme code='label.confirm.changes.submit' text='Submit change(s)' />" class="btn-success y btn-block bottom-align" />
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </c:if>

                     <c:if test="${salesChannel=='CallCenter' || salesChannel=='TravelCentre' || salesChannel=='TravelCentreFerryOnly'}">
                        <form:form id="confirmBookingDetailsForm" name="confirmBookingDetailsForm" commandName="confirmBookingForm" class="create_update_payment_form" action="${submitButtonURL}" method="POST">

                             <cms:pageSlot position="Reservation" var="feature" element="section">
                                <cms:component component="${feature}" />
                             </cms:pageSlot>
                             <termsandconditions:customerandbcftermsandconditions />

                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">

                                     <button type="submit" class="btn btn-primary btn-block"
                                                             id="y_confirmBookingDetailsButton">
                                                             <spring:theme code="label.confirm.changes.submit"
                                                                 text="Submit change(s)" />
                                                          </button>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                     </c:if>

                       <cms:pageSlot position="InternalComments" var="features">
                          <cms:component component="${features}" element="section" />
                      </cms:pageSlot>

                       <div class="container">
                         <div class="row margin-top-20 margin-bottom-20">
                             <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
                                 <a class="mb-3 mt-1" href="${cancelUrl}">
                                     X&nbsp;<spring:theme code="label.cancel.changes.submit" text="Cancel changes" />
                                 </a>
                           </div>
                        </div>
                     </div>

                </div>
            </div>
		</div>

	</section>

<checkout:CartValidationWebModal />
<modifybooking:abortCancellation/>
</div>
</template:page>
<c:if test="${salesChannel=='Web'}">
	<script type="text/javascript" src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>

