<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object escapeXml="false">
	<json:property name="valid" value="${decisionData.valid}" />
	<c:if test="${decisionData.valid}">
		<json:property name="remainingAmount" value="${decisionData.amountToPay}" />
		<json:property name="paymentType" value="${decisionData.paymentType}" />
	</c:if>
	<c:if test="${not empty decisionData.confirmBookingUrl}">
		<json:property name="confirmBookingUrl" value="${decisionData.confirmBookingUrl}" />
	</c:if>
	<c:if test="${not empty decisionData.error}">
		<json:property name="error">
			<p>
				<spring:theme code="checkout.error.paymentethod.formentry.invalid" />
			</p>
			<c:if test="${decisionData.paymentType eq 'PINPAD'}">
				<c:if test="${not empty decisionData.transactionDetails.operatorDisplayMessage}">
					<p>
						<spring:theme code="text.booking.confirmation.operator.display.msg" text="Operator display message" />
						: ${decisionData.transactionDetails.operatorDisplayMessage}
					</p>
				</c:if>
				<c:if test="${not empty decisionData.transactionDetails.approvalOrDeclineMessage}">
					<p>
						<spring:theme code="text.booking.confirmation.approval.or.decline.msg" text="Approval or decline message" />
						: ${decisionData.transactionDetails.approvalOrDeclineMessage}
					</p>
				</c:if>
				<c:if test="${decisionData.transactionDetails.signatureRequiredFlag}">
					<p>
						<spring:theme code="text.payment.print.pinpad.receipt.message" text="Please print receipt" />
					</p>
				</c:if>
				<div class="row">
					<div class="col-md-offset-4 col-md-4">
						<form:form target="_blank" commandName="transactionDetails" action="${fn:escapeXml(request.contextPath)}/payment/receipt/cart" method="POST">
							<input type="hidden" name="approvalOrDeclineMessage" value="${decisionData.transactionDetails.approvalOrDeclineMessage}"/>
							<input type="hidden" name="transactionAmountInCents" value="${decisionData.transactionDetails.transactionAmountInCents}"/>
							<input type="hidden" name="transactionType" value="${decisionData.transactionDetails.transactionType}"/>
							<input type="hidden" name="transactionTimeStamp" value="${decisionData.transactionDetails.transactionTimeStamp}"/>
							<input type="hidden" name="cardType" value="${decisionData.transactionDetails.cardType}"/>
							<input type="hidden" name="cardNumberToPrint" value="${decisionData.transactionDetails.cardNumberToPrint}"/>
							<input type="hidden" name="approvalNumber" value="${decisionData.transactionDetails.approvalNumber}"/>
							<input type="hidden" name="serviceProviderPaymentTerminalId" value="${decisionData.transactionDetails.serviceProviderPaymentTerminalId}"/>
							<input type="hidden" name="transactionRefNumber" value="${decisionData.transactionDetails.transactionRefNumber}"/>
							<input type="hidden" name="cardEntryMethod" value="${decisionData.transactionDetails.cardEntryMethod}"/>
							<input type="hidden" name="emvApplicationLabel" value="${decisionData.transactionDetails.emvApplicationLabel}"/>
							<input type="hidden" name="emvAid" value="${decisionData.transactionDetails.emvAid}"/>
							<input type="hidden" name="emvTvr" value="${decisionData.transactionDetails.emvTvr}"/>
							<input type="hidden" name="emvTsi" value="${decisionData.transactionDetails.emvTsi}"/>
							<input type="hidden" name="emvPinEntryMessage" value="${decisionData.transactionDetails.emvPinEntryMessage}"/>
							<input type="hidden" name="signatureRequiredFlag" value="${decisionData.transactionDetails.signatureRequiredFlag}"/>
							<input type="hidden" name="debitAccountType" value="${decisionData.transactionDetails.debitAccountType}"/>
							<input type="hidden" name="cartCode" value="${decisionData.transactionDetails.cartCode}"/>
							<button type="submit" class="btn btn-primary btn-block">
								<spring:theme code="text.booking.confirmation.print.receipt" text="Print receipt" />
							</button>
						</form:form>
					</div>
				</div>
			</c:if>
		</json:property>
	</c:if>
</json:object>
