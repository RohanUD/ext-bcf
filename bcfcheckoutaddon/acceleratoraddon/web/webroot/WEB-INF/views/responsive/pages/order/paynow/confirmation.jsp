<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<h2 class="h2">
            						<spring:theme code="text.paymentdetails.paynow.confirmation.description"  />
            					</h2>
</template:page>

