<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>
<spring:url value="/checkout/multi/summary/proceedToPayment" var="proceedToPayment" />
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl" />
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<c:choose>
		<c:when test="${bookingJourney=='BOOKING_PACKAGE'}">
			<progress:travelBookingProgressBar stage="confirmation" amend="${amend}" bookingJourney="${bookingJourney}" />
		</c:when>
		<c:otherwise>
			<progress:bookingProgressBar stage="confirmation" amend="${amend}" bookingJourney="${bookingJourney}" />
		</c:otherwise>
	</c:choose>
	<div class="container">
		<div class="row">
			<div class="white-bg">
				<div class="col-sm-10 offset-1 js-main-box">
					<!-- <div class=""> -->
					<cms:pageSlot position="CenterContent" var="feature" element="div">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="white-bg">
				<form:form action="${proceedToPayment}"  onsubmit="return send_mail()"  id="placeOrderForm1" commandName="bcfPlaceOrderForm">
					<div class="container">
						<div class="row">
							<div class="white-bg">
								<div class="col-sm-10 offset-1 js-main-box">
									<c:choose>
										<c:when test="${not empty agentComment}">
										<h4>
                                        <spring:theme code="checkout.summary.accessbilitydeclaration.headline" />
                                        </h4>
											<spring:theme code="checkout.summary.agent.notes" />
											<br>
											${agentComment}
										</c:when>
										<c:otherwise>
										<c:if test="${not empty bcfTravellersPerJourney}">
										<c:forEach items="${travellersGroupByJourney.travellerDataPerJourney}" var="bcfTravellersPerJourney"> 
                        <h4>Journey: ${bcfTravellersPerJourney.travelRoute.name}</h4> 
                        <c:forEach items="${bcfTravellersPerJourney.travellerData }" var="traveller"> 
                          <c:if test="${traveller.travellerType eq 'PASSENGER' and not empty traveller.specialRequestDetail.specialServiceRequests }"> 
                            <div> 
                              <c:set var="travellerCodeMap" value="${bcfTravellersPerJourney.namesMap[traveller.travellerInfo.passengerType.code]}" /> 
                              <label>${fn:escapeXml(travellerCodeMap[traveller.label])}</label> 
											
															<br>
															<c:forEach items="${traveller.specialRequestDetail.specialServiceRequests}" var="specialRequest">
												${specialRequest.name}<br>
															</c:forEach>
															</div>
														</c:if>
													</c:forEach>
											</c:forEach>
											</c:if>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
					<br> <br>
								<div id="container">
									<div class="row">
										<div class="col-sm-10 offset-1">
											<input type='hidden' id='maxEmailAllowed' name='maxEmailAllowed' value='${maxEmailAllowed}' />
											<div class="col-md-4 p-0 js_holder">
												<div>
												<p>
												<spring:theme code="checkout.summary.additionalEmail.headline" />
												</p>
													<form:input name="email0" type="email" class="form-control email-field" placeholder="Enter your Email" path="emails[0]" />
													<span class="error" id=e_emails0  style="color:red">
													</span>
												</div>
												<br>
											</div>
										</div>
									</div>
								</div>
									
									<div class="row">
								<div class="col-sm-10 offset-1">
										<div class="col-md-12 p-0 mt-2">
											<p id="addRow" class="btn btn-primary">
												<spring:theme code="checkout.summary.email.add"/>
											</p>
										</div>
									</div>
									</div>
								
							
					<div class="row">
						<div class="col-sm-6 ml-auto">
							<div class="pl-2 pr-2 pt-3 pb-3">
								<div class="form-check">
									<form:checkbox id="Terms1" path="termsCheck" />
									<label class="form-check-label mt-3" for="Terms1">
										<h4>
											I agree to BC Ferries <u>Terms and Conditions</u> and <u>Privacy Policy</u>
										</h4>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 mt-3 pull-right">
						<button id="placeOrder" type="submit" class="btn btn-primary find-button  mb-4  col-sm-4 text-uppercase">Checkout</button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</template:page>
