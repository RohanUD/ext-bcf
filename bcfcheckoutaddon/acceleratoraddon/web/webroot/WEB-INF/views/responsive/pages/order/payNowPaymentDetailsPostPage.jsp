<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation"
	tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="address"
	tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/address"%>
<%@ taglib prefix="checkout"
	tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="voucher"
	tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/voucher"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="account"
	tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="progress"
	tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<div id="globalMessages">
	<common:globalMessages />
</div>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
<c:url value="${paymentFormUrl}" var="submitButtonURL" />
<c:set var="receiptNoidLabel">
	<spring:theme code="checkout.paymentType.receiptNo.placeholder" />
</c:set>
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">



	<section>
		<div class=" payment-details">
			<c:if test="${not empty paymentFormUrl}">

				<div class="col-xs-12">
					<h2 class="h2">
						<spring:theme code="text.paymentdetails.paynow.description" />${amountToPay.formattedValue}
					</h2>
				</div>

				<div class="col-xs-12">
					<h2 class="h2">
						<spring:theme
							code="checkout.summary.paymentMethod.enterDetails.header"
							text="Enter your Payment Details" />
					</h2>
				</div>


				<c:if test="${salesChannel=='Web'}">
					<div class="payment-wrap y_nonItineraryContentArea">
						<c:if test="${not empty paymentInfos}">
							<div class="panel panel-primary panel-list">
								<div class="panel-heading">
									<h3 class="title">
										<spring:theme
											code="checkout.summary.paymentMethod.savedPayments.header"
											text="Saved Payment Options" />
									</h3>
								</div>
								<div class="panel-body" id="saved-payment">
									<c:forEach items="${paymentInfos}" var="paymentInfo"
										varStatus="status">
										<div class="panel panel-default my-account-secondary-panel">
											<div class="panel-heading">
												<h3 class="panel-title">Card
													${fn:escapeXml(status.count)}</h3>
											</div>
											<div class="panel-body option">
												<div class="fieldset">
													<div class="row">
														<account:paymentInfo paymentInfo="${paymentInfo}" />
														<div class="col-xs-12 col-md-6">
															<div class="paymentCVVNumber">
																<label>Please Enter CVV number</label> <input
																	type="text" id="card_cvNumber" value="" />
																<div id="cvvError"></div>
															</div>
															<div
																class="form-group col-xs-6 col-sm-6 col-md-6 bottom-align-parent">
																<form:form
																	action="${fn:escapeXml(request.contextPath)}/checkout/multi/payment-method/choose"
																	method="POST">
																	<input type="hidden" name="selectedPaymentMethodId"
																		value="${fn:escapeXml(paymentInfo.id)}" />
																	<input type="hidden" name="card_cvNumber" value="">
																	<br />
																	<button type="submit"
																		class="savedPaymentSubmit btn btn-primary btn-block bottom-align"
																		tabindex="${fn:escapeXml(status.count + 21)}">
																		<spring:theme code="checkout.multi.sop.useToPay"
																			text="Select" />
																	</button>
																</form:form>
															</div>
															<div
																class="form-group col-xs-6 col-sm-6 col-md-6 bottom-align-parent">
																<form:form
																	action="${fn:escapeXml(request.contextPath)}/checkout/multi/payment-method/remove"
																	method="POST">
																	<input type="hidden" name="paymentInfoId"
																		value="${fn:escapeXml(paymentInfo.id)}" />
																	<button type="submit"
																		class="btn btn-default btn-block bottom-align"
																		tabindex="${fn:escapeXml(status.count + 22)}">
																		<spring:theme code="checkout.multi.sop.remove"
																			text="Remove" />
																	</button>
																</form:form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>
						<form:form id="silentOrderPostForm" name="silentOrderPostForm"
							commandName="paymentDetailsForm"
							class="create_update_payment_form" action="${submitButtonURL}"
							method="POST">
							<ycommerce:testId code="paymentDetailsForm">
								<input type="hidden" name="monerisToken" />

								<hr class="hr-payment">
								<div class="container">
									<%-- Payment Details --%>
									<div class="row">
										<div class="col-lg-12 col-xs-12 mt-5">
											<h2 class="title my-3 payment-h2">
												<spring:theme
													code="checkout.summary.paymentMethod.payment.header"
													text="New Payment Method" />
											</h2>
										</div>
										<%-- / .row --%>
									</div>
									<div class="row">
										<div class="form-group col-sm-9">
											<ul class="list-inline mb-0 payment-img py-4">
												<c:forEach items="${paymentCardTypes}" var="paymentCard"
													varStatus="cardIdx">
													<li id="cardType_${paymentCard.code}"
														class="cardType_${paymentCard.code}"><form:radiobutton
															path="cardType" value="${paymentCard.code}" /> <span><img
															src="../../../_ui/responsive/common/images/${paymentCard.code}.png"
															width="50"></span></li>
												</c:forEach>
											</ul>
										</div>
										<div>
											<form:input type="hidden" id="pd-card-token" path="token" />
											<form:input type="hidden" id="pd-card-number"
												path="cardNumber" />
										</div>
									</div>
									<%-- / .row --%>
									<div class="row">
										<div class="form-group col-sm-3 col-xs-12 nowrap">
											<iframe id="monerisFrame" src="about:blank" scrolling="no"
												frameborder="0"></iframe>
											<div class="monerisFrameError fe-error"></div>
										</div>
										<div class="col-sm-3 col-xs-12 nowrap">
											<formElement:formInputBox maxlength="5" inputCSS="y_card_expiry selectpicker form-control"
																	  path="cardExpiration" mandatory="true" tabindex="5" placeholder="mm/yy"
																	  idKey="pd-expiry" labelKey="label.payment.card.expiry"/>
										</div>
										<div class="col-sm-3 col-xs-12 nowrap">
											<formElement:formInputBox idKey="pd-card-verification"
												labelKey="payment.cvn" path="cardCVNumber"
												inputCSS="y_cardVerification" mandatory="true" tabindex="8" />
										</div>
									</div>
								</div>

								<div class="container">
									<div class="row">
										<div class="col-lg-12 col-xs-12 mt-5">
											<h2 class="title my-3 payment-h2">
												<spring:theme
													code="checkout.summary.paymentMethod.billing.address.header"
													text="Billing Address" />
											</h2>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-12">
											<formElement:formInputBox idKey="first-name1"
												labelKey="label.payment.billing.nameOnCard"
												inputCSS="y_first_name" path="billingAddress.firstName"
												tabindex="6" mandatory="true" />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<formElement:formInputBox idKey="addressLine1"
												labelKey="address.line1" path="billingAddress.line1"
												tabindex="6" mandatory="true" inputCSS="y_address_line1" />
										</div>
										<div class="form-group col-sm-6">
											<formElement:formInputBox idKey="addressLine2"
												labelKey="address.line2" path="billingAddress.line2"
												tabindex="6" mandatory="true" inputCSS="y_address_line2" />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<formElement:formInputBox idKey="city"
												labelKey="address.townCity" path="billingAddress.townCity"
												tabindex="6" mandatory="true" inputCSS="y_city" />
										</div>
										<div class="form-group col-sm-6 select-custom-dropdown">
											<formElement:formSelectBox idKey="country"
												labelKey="label.payment.guest.country"
												path="billingAddress.countryIso" mandatory="true"
												skipBlank="false"
												skipBlankMessageKey="text.payment.guest.country.dropdown"
												items="${supportedCountries}" itemValue="isocode"
												tabindex="10"
												selectCSSClass="y_country billing-address-country form-control" />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<spring:theme code="register.region.dropdown.message"
												text="choose region" />
											<form:select class="billing-address-regions form-control"
												id="billing-address-regions" path="billingAddress.regionIso"
												cssErrorClass="fieldError">
												<form:option value="-1" disabled="true" selected="selected">
													<spring:theme code="text.page.default.select"
														text="please select" />
												</form:option>
											</form:select>
										</div>
										<div class="form-group col-sm-6">
											<formElement:formInputBox idKey="postalcode"
												labelKey="address.postalcode" path="billingAddress.postcode"
												tabindex="6" mandatory="true" inputCSS="y_postal_code" />
										</div>
									</div>
								</div>

								<hr class="hr-payment">


								<div class="container">
									<div clas="row">
										<div clas="col-md-12">
											<p class="text-center activeShow">
												<span class="text-blue"> <spring:theme
														code="text.payment.view.terms.conditions"
														text="View terms and conditions" /> <span><i
														class="bcf bcf-icon-down-arrow"></i><span class="terms-conditions-detail">
											</p>
										</div>

										<div class="col-md-12 payment-agree hidden">
										<p>
												<cms:pageSlot position="TermsConditions" var="feature"
													element="section">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</p>
											<div class="input-group col-md-offset-5 pt-4">
												<label class="custom-checkbox-input"><form:checkbox
														path="termsCheck" /> <spring:theme
														code="text.payment.consent.terms.conditions"
														text="I agree to the terms and conditions" /> <input
													type="checkbox"> <span class="checkmark-checkbox"></span>
												</label>
											</div>
										</div>

									</div>
								</div>

							</ycommerce:testId>
						</form:form>
					</div>

					<div class="container">

						<fieldset>
							<div class="row mb-5">
								<div class="col-lg-12 col-md-12 col-sm-12 text-center mt-2 mb-3">
									<button type="submit" id="ht_submit" class="btn btn-primary">
										<spring:theme code="text.payment.form.submit.button"
											text="Checkout" />
									</button>
								</div>
							</div>
							<%-- / .row --%>
						</fieldset>
					</div>
				</c:if>
				<c:if test="${salesChannel=='CallCenter'}">
					<div class="container">
						<div
							class="col-xs-12 col-sm-9 payment-wrap y_nonItineraryContentArea">
							<div class="form-group col-xs-12 col-sm-6 col-md-3 pull-right">
								<form:form id="y_agentPayNow" commandName="paymentDetailsForm"
									action="${submitButtonURL}" method="POST">
									<c:if test="${specialServiceRequestCommentAllowed}">
									        <c:if test="${not empty agentComment}">
                                            <h4>
                                            <spring:theme code="checkout.summary.agent.notes" />
                                            </h4>
                                            	<br>
                                            	${agentComment}
                                            </c:if>
										<div class="form-group col-sm-12">
											<formElement:formTextArea idKey="agent-comment"
												labelKey="label.payment.agent.comment" path="agentComment" />
										</div>
									</c:if>

									<div class="form-group col-sm-12">
                                    	<formElement:formTextArea idKey="special-instructions" labelKey="label.payment.special.instructions" path="specialInstructions" />
                                    </div>

									<c:if test="${bookingJourney=='BOOKING_PACKAGE'}">
									<div class="form-group col-sm-12">
										<formElement:formTextArea idKey="vacation-instructions" labelKey="label.payment.vacation.instructions" path="vacationInstructions" />
									</div>
									</c:if>

									<h4 class="text-center activeShow">
										<span class="text-blue"> <spring:theme
												code="text.payment.view.terms.conditions"
												text="View terms and conditions" /> <i
											class="bcf bcf-icon-down-arrow"></i></span>
									</h4>

									<div class="col-md-12 payment-agree hidden">
										<div class="input-group offset-md-5 pt-4">
											<label class="custom-checkbox-input"><form:checkbox
													path="termsCheck" /> <spring:theme
													code="text.payment.consent.terms.conditions"
													text="I agree to the terms and conditions" /> <input
												type="checkbox"> <span class="checkmark-checkbox"></span>
											</label>
										</div>
									</div>

									<input id="y_agentPayNowBtn" type="submit"
										value="<spring:theme code="checkout.summary.paymentMethod.payNow.button" text="Pay Now" />"
										class="btn-success y btn-block bottom-align" />
								</form:form>
							</div>
						</div>
					</div>
				</c:if>
				<c:if test="${salesChannel=='TravelCentre'}">
					<input type="hidden" value="payRemaining" id="payRemaining" name="payRemaining" class="payRemaining"/>
					<cms:pageSlot position="PaymentMethods" var="feature">
						<cms:component component="${feature}" element="section" />
					</cms:pageSlot>
				</c:if>
			</c:if>
		</div>
	</section>
	<input type="hidden" id="ht_token" />
	<input type="hidden" id="ht_id" value="${paymentTokenizerClientID }" />
	<input type="hidden" id="ht_host" value="${paymentTokenizerHostURL }" />
	<input type="hidden" id="ht_host_tokenizerURL"
		value="${paymentTokenizerURL }" />
</template:page>
<c:if test="${salesChannel=='Web'}">
	<script type="text/javascript"
		src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>
