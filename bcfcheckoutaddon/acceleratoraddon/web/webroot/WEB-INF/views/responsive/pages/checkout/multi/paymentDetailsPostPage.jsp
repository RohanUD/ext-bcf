<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/address"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="voucher" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/voucher"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="packageprogress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="ferryprogress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ferryprogress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="paymentmethod" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi/cms"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="termsandconditions" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/termsandconditions"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<cms:pageSlot position="TopHeader" var="feature">
	<cms:component component="${feature}" element="section" />
</cms:pageSlot>
<c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
<c:url value="${paymentFormUrl}" var="submitButtonURL" />
<c:set var="receiptNoidLabel">
	<spring:theme code="checkout.paymentType.receiptNo.placeholder" />
</c:set>
	<c:choose>
		<c:when test="${'BOOKING_TRANSPORT_ONLY' eq bookingJourney}">
			<ferryprogress:fareFinderProgressBar stage="payment" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
		</c:when>
		<c:otherwise>
			<packageprogress:travelBookingProgressBar stage="confirmation" amend="${amend}" bookingJourney="${bookingJourney}" />
		</c:otherwise>
	</c:choose>
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
<modifybooking:editBookingHeaderBar/>
<modifybooking:originalBookingDetails/>

    <c:choose>
        <c:when test="${availabilityStatus eq 'ON_REQUEST'}">
            <div class="total-price-section text-center">
                <p>
                    <b><spring:theme code="text.package.booking.on.request.review.title" /></b> <br>
                    <spring:theme code="text.package.booking.on.request.review.message1" arguments="${fn:escapeXml(vendor)}" />
                    <br>
                    <spring:theme code="text.package.booking.on.request.review.message2" arguments="${fn:escapeXml(vendor)}" />
                </p>
            </div>
        </c:when>
    </c:choose>
	<section>
		<div class="payment-details">
			<c:if test="${not empty paymentFormUrl}">
				<c:if test="${bookingJourney ne 'BOOKING_TRANSPORT_ONLY'}">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 hidden">
								<h2 class="h2">
									<spring:theme code="checkout.summary.paymentMethod.enterDetails.header" text="Enter your Payment Details" />
								</h2>
							</div>
						</div>
					</div>
					<cms:pageSlot position="PaymentOptions" var="feature">
						<cms:component component="${feature}" element="section" />
					</cms:pageSlot>
				</c:if>
				<c:if test="${salesChannel=='Web'}">
					<div class="payment-wrap y_nonItineraryContentArea">
						<form:form id="silentOrderPostForm" name="silentOrderPostForm" commandName="paymentDetailsForm" class="create_update_payment_form" action="${submitButtonURL}" method="POST">
							<ycommerce:testId code="paymentDetailsForm">
								<input type="hidden" name="monerisToken" />
								<form:input path="paymentType" id="y_paymentType" type="hidden" />
								<%-- Guest Checkout --%>
								<div class="container">
									<div class="row">
										<div class="col-lg-12 col-xs-12">

											<c:if test="${isAnonymous}">
												<h4 class="title payment-h2">
													<spring:theme code="label.payment.form.guest.checkout" text="Guest checkout" />
												</h4>
											</c:if>
											<c:if test="${!isAnonymous}">
												<h2 class="title payment-h2">
													<spring:theme code="label.payment.form.customer.checkout" text="Customer checkout" />
												</h2>
											</c:if>
										</div>
									</div>

									<c:if test="${isSubscriptionOnly}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>
                                                    <spring:theme code="label.payment.guest.firstname" text="First Name"/>
                                                </label>
                                                <input name="firstName" value="${customerData.firstName}" class="form-control" readonly/>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>
                                                    <spring:theme code="label.payment.guest.lastname" text="Last Name"/>
                                                </label>
                                                <input name="lastName" value="${customerData.lastName}" class="form-control" readonly/>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>
                                                    <spring:theme code="label.payment.guest.phonenumber" text="First Name"/>
                                                </label>
                                                <input name="phoneNumber" value="${customerData.contactNumber}"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                    </c:if>

									<c:if test="${isAnonymous}">
										<div class="row">
											<div class="col-sm-6">
													<c:if test="${isAnonymous}">
													<formElement:formInputBox idKey="first-name2"
																			  labelKey="label.payment.guest.firstname"
																			  path="firstName" inputCSS="y_first_name"
																			  tabindex="6" mandatory="true"/>
											            </c:if>
													<c:if test="${!isAnonymous}">
													<label>
														<spring:theme code="label.payment.guest.firstname" text="First Name"/>
													</label>
													<input name="firstName" value="${customerData.firstName}"
														   class="form-control"/>
												    </c:if>
											</div>
											<div class="col-sm-6">
												<c:if test="${isAnonymous}">
													<formElement:formInputBox idKey="last-name"
																			  labelKey="label.payment.guest.lastname"
																			  path="lastName" inputCSS="y_last_name" tabindex="6"
																			  mandatory="true"/>
												 </c:if>
												<c:if test="${!isAnonymous}">
													<label>
														<spring:theme code="label.payment.guest.lastname" text="Last Name"/>
													</label>
													<input name="lastName" value="${customerData.lastName}" class="form-control"/>
												 </c:if>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<c:if test="${isAnonymous}">
													<formElement:formInputBox idKey="email" labelKey="label.payment.guest.email"
																			  path="email" inputCSS="y_email" tabindex="6"
																			  mandatory="true"/>
													 </c:if>
												<c:if test="${!isAnonymous}">
													<label>
														<spring:theme code="label.payment.guest.email" text="First Name"/>
													</label>
													<input name="email" value="${customerData.email}" class="form-control"/>
												 </c:if>
											</div>
											<c:if test="${isAnonymous}">
												<div class="col-sm-6">
													<formElement:formInputBox idKey="confirm-email"
																			  labelKey="label.payment.guest.confirmemail"
																			  inputCSS="y_confirm_email" path="checkEmail"
																			  tabindex="6" mandatory="true"/>
												</div>
											</c:if>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<c:if test="${isAnonymous}">
													<formElement:formInputBox idKey="phone-number"
																			  labelKey="label.payment.guest.phonenumber"
																			  path="phoneNumber" inputCSS="y_phone_number"
																			  tabindex="6" mandatory="true"/>
												</c:if>
												<c:if test="${!isAnonymous}">
													<label>
														<spring:theme code="label.payment.guest.phonenumber" text="First Name"/>
													</label>
													<input name="phoneNumber" value="${customerData.contactNumber}"
														   class="form-control"/>
												</c:if>
											</div>
											<c:if test="${isAnonymous}">
												<div class="col-sm-6 select-custom-dropdown">
													<formElement:formSelectBox idKey="country1"
																			   labelKey="label.payment.guest.country"
																			   path="country" mandatory="false" skipBlank="false"
																			   skipBlankMessageKey="text.payment.guest.country.dropdown"
																			   items="${supportedCountries}" itemValue="isocode"
																			   tabindex="10"
																			   selectCSSClass="y_country form-control selectpicker"
																			   dataSize="10"/>
												</div>
											</c:if>
										</div>
										<div class="row">
                                        	<div class="col-sm-6">
										<c:if test="${isAnonymous}">
                                            <formElement:formInputBox idKey="postal-code"
                                                                      labelKey="label.payment.guest.postcode"
                                                                      path="postCode"
                                                                      tabindex="6" mandatory="false"/>
                                        </c:if>
                                        </div>
                                        </div>
										</c:if>


									<checkout:captureLeadPaxDetails />


								</div>
								<div class="container">
									<hr class="hr-payment">
									<%-- Payment Details --%>
									<div class="row">
										<div class="col-lg-12 col-xs-12">
											<h4 class="title payment-h2">
												<spring:theme code="checkout.summary.paymentMethod.payment.header" text="New Payment Method" />
											</h4>
										</div>
										<%-- / .row --%>
									</div>
									<c:choose>
											<c:when test="${isAnonymous}">
												<c:if test="${deferPayment}">
													<paymentmethod:ctcCardPaymentMethod />
													<label class="custom-checkbox-input">
														<form:checkbox id="js-defer-payment-to-cc" path="deferPayment" />
														<spring:theme code="text.payment.cc.card.deferPayment" text="Defer payment to credit card" />
														<input type="checkbox"> <span class="checkmark-checkbox"></span>
													</label>
													<h5>
														<spring:theme code="text.payment.use.different.cc.label" text="USE A DIFFERENT CREDIT CARD" />
													</h5>
													<hr class="hr-payment">
													<label class="custom-radio-input">
														<spring:theme code="text.payment.use.different.cc.check" text="Use a different credit card" />
														<input type="radio" id="use-new-card-radio" ${paymentDetailsForm.useNewCard ? 'checked' : ''} /> <span class="checkmark"></span>
													</label>
													<hr class="hr-payment">
												</c:if>
											</c:when>
											<c:otherwise>
												<c:if test="${isB2bCustomer or deferPayment}">
													<paymentmethod:ctcCardPaymentMethod />
												</c:if>
												<paymentmethod:ccCardPaymentMethod />
												<c:if test="${deferPayment}">
													<label class="custom-checkbox-input">
														<form:checkbox id="js-defer-payment-to-cc" path="deferPayment" />
														<spring:theme code="text.payment.cc.card.deferPayment" text="Defer payment to credit card" />
														<input type="checkbox"> <span class="checkmark-checkbox"></span>
													</label>
												</c:if>
												<c:if test="${not empty paymentInfos or isB2bCustomer or deferPayment}">
													<h5>
														<spring:theme code="text.payment.use.different.cc.label" text="USE A DIFFERENT CREDIT CARD" />
													</h5>
													<hr class="hr-payment">
													<label class="custom-radio-input">
														<spring:theme code="text.payment.use.different.cc.check" text="Use a different credit card" />
														<input type="radio" id="use-new-card-radio" ${paymentDetailsForm.useNewCard ? 'checked' : ''} /> <span class="checkmark"></span>
													</label>
													<hr class="hr-payment">
												</c:if>
											</c:otherwise>
									</c:choose>
									<paymentmethod:newCardAndBillingDetails/>
								</div>
								<div class="container">
                                 <c:if test="${isAmendment && !isAnonymous}">
                                    <h4 class="title payment-h2">
                                        <spring:theme code="text.booking.details.changes" text="Booking details with changes" />
                                    </h4>
                                </c:if>
                                </div>
								<cms:pageSlot position="Reservation" var="feature" element="section">
									<cms:component component="${feature}" />
								</cms:pageSlot>

								<termsandconditions:customertermsandconditions/>
								<c:if test="${deferPayment}">
									<br>
									<div class="container">
										<label class="custom-checkbox-input display-inline">
											<form:checkbox path="allowChangesAtPOS" />
											<spring:theme code="text.payment.check.isAllowChangesAtPOS" text="Allow booking changes at terminal" />
											<input type="checkbox">
											<span class="checkmark-checkbox"></span>
										</label>
                                        <a href="#" data-container="body" data-toggle="popover" class="popoverThis" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.payment.allow.booking.changes.at.terminal"/>">
                                        <span class="bcf bcf-icon-info-solid"></span></a>
									</div>
								</c:if>
							</ycommerce:testId>
						</form:form>
						<cms:pageSlot position="JourneyDetailsEdit" var="feature" element="section">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					<div class="container">
						<div class="row my-5">
							<div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
								<button type="submit" id="ht_submit" class="btn btn-block btn-primary">
									<spring:theme code="text.payment.form.submit.button" text="Checkout" />
								</button>
							</div>
						</div>
					</div>
		            <c:if test="${'BOOKING_PACKAGE' eq bookingJourney}">
                        <div class="container">
                            <div class="row my-5">
                                <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
                                <p class="margin-top-30 text-sm">
                                <label class="y_passengerWrapper"><spring:theme code="text.payment.check.privacy.statement"/></label>
                                </p>
                                </div>
                             </div>
                         </div>
                     </c:if>
				</c:if>



				<c:if test="${salesChannel=='CallCenter' || salesChannel=='TravelCentre' || salesChannel=='TravelCentreFerryOnly'}">

					<div class="payment-wrap y_nonItineraryContentArea">
						<form:form id="y_agentPayNow" commandName="paymentDetailsForm" action="${submitButtonURL}" method="POST">
								<div class="container">
								<div class="row">
										<div class="col-lg-12 col-xs-12">
											<c:if test="${isAnonymous}">
												<h4 class="title payment-h2">
													<spring:theme code="label.payment.form.guest.checkout" text="Guest checkout" />
												</h4>
												</c:if>
											<c:if test="${!isAnonymous}">
												<h2 class="title payment-h2">
													<spring:theme code="label.payment.form.customer.checkout" text="Customer checkout" />
												</h2>
												</c:if>
										</div>
									</div>
								<div class="row">
									<div class="col-sm-6">
										<formElement:formInputBox idKey="first-name2" labelKey="label.payment.guest.firstname" path="firstName" value="${customerData.firstName}" inputCSS="y_first_name" tabindex="6" mandatory="true" />
									</div>
									<div class="col-sm-6">
										<formElement:formInputBox idKey="last-name" labelKey="label.payment.guest.lastname" path="lastName" value="${customerData.lastName}" inputCSS="y_last_name" tabindex="6" mandatory="true" />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<formElement:formInputBox idKey="email" labelKey="label.payment.guest.email" path="email" value="${customerData.email}" inputCSS="y_email" tabindex="6" mandatory="true" />
									</div>
									<div class="col-sm-6">
										<formElement:formInputBox idKey="confirm-email" labelKey="label.payment.guest.confirmemail" value="${customerData.email}" inputCSS="y_confirm_email" path="checkEmail" tabindex="6" mandatory="true" />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<formElement:formInputBox idKey="phone-number" labelKey="label.payment.guest.phonenumber" value="${customerData.contactNumber}" path="phoneNumber" inputCSS="y_phone_number" tabindex="6" mandatory="true" />
									</div>
									<div class="col-sm-6 select-custom-dropdown">

                                        <div class="control">
                                            <label class="control-label" for="country1">
                                                <spring:theme code="label.payment.guest.country"/>

                                                    <span class="mandatory">
                                                        <spring:theme code="login.required" var="loginRequiredText" />
                                                    </span>

                                                    <span class="skip">
                                                        <form:errors path="country"/>
                                                    </span>
                                            </label>
                                            <form:select id="country1" path="country"   cssClass="y_country form-control" tabindex="10" >
                                                 <c:forEach var="supportedCountry" items="${supportedCountries}">
                                                     <c:choose>
                                                      <c:when test="${supportedCountry.isocode==customerData.country}">
                                                      <form:option value="${supportedCountry.isocode}" selected="selected">${supportedCountry.name}</form:option>
                                                      </c:when>
                                                      <c:otherwise>
                                                      <form:option value="${supportedCountry.isocode}">${supportedCountry.name}</form:option>
                                                      </c:otherwise>
                                                      </c:choose>
                                                  </c:forEach>
                                             </form:select>

                                        </div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
                                			<formElement:formInputBox idKey="postCode" labelKey="address.postalcode" value="${customerData.postcode}" path="postCode" tabindex="6" mandatory="true" inputCSS="y_postal_code" />
                                		</div>
                                </div>
								</div>
								<form:input path="paymentType" id="y_paymentType" type="hidden" />


                                <checkout:captureLeadPaxDetails />


	                            <div class="container">
                                    <c:if test="${isAmendment && !isAnonymous}">
                                        <h4 class="title payment-h2">
                                            <spring:theme code="text.booking.details.changes" text="Booking details with changes" />
                                        </h4>
                                    </c:if>
                                </div>


								<cms:pageSlot position="Reservation" var="feature" element="section">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<termsandconditions:customerandbcftermsandconditions />
								<c:if test="${salesChannel=='CallCenter'}">
									<paymentmethod:callCentreAgentPayment />
								</c:if>
							</form:form>
						</div>
				</c:if>


			</c:if>

            <c:if test="${salesChannel=='TravelCentre' || salesChannel=='TravelCentreFerryOnly'}">
                <cms:pageSlot position="PaymentMethods" var="feature">
                    <cms:component component="${feature}" element="section" />
                </cms:pageSlot>
            </c:if>

            <cms:pageSlot position="InternalComments" var="feature">
                <cms:component component="${feature}" element="section" />
            </cms:pageSlot>
		</div>
	</section>
	<input type="hidden" id="ht_token" />
	<input type="hidden" id="ht_id" value="${paymentTokenizerClientID }" />
	<input type="hidden" id="ht_host" value="${paymentTokenizerHostURL }" />
	<input type="hidden" id="ht_host_tokenizerURL" value="${paymentTokenizerURL }" />
	<checkout:CartValidationWebModal />
<modifybooking:abortCancellation/>
</div>
</template:page>
<c:if test="${salesChannel=='Web'}">
	<script type="text/javascript" src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>
