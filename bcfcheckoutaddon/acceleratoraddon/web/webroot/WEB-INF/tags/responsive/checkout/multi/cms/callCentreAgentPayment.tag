<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="paymentmethod" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi/cms"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<c:if test="${salesChannel=='CallCenter'}">
	<div class="form-group">
		<div class="row">
			<hr class="hr-payment">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-xs-12">
						<h4 class="title payment-h2">
							<spring:theme code="checkout.summary.paymentMethod.payment.header" text="Payment Method" />
						</h4>
					</div>
				</div>
				<hr class="hr-payment">
			</div>
			<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
				<c:if test="${not isSubscriptionOnly}">
					<c:if test="${not empty savedCTCCard and (isB2bCustomer or deferPayment)}">
						<div class="container">
							<div class="asm-payment-type-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="radio-btn-bx padding-0-mobile" id="">
											<label class="custom-radio-input">
												<spring:theme code="checkout.summary.paymentMethod.savedCTCCard.text" text="Pay using saved BC Ferries payment card" />
												<form:radiobutton path="paymentType" value="savedCTCCard" cssClass="asm-payment-type-selector" />
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="asm-payment-type-options hidden">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12 nowrap">
											<h3 class="panel-title">
												<spring:theme code="text.payment.ctc.card.header" />
											</h3>
											<form:select class="selectpicker form-control" id="savedCTCcardCode" multiple="false" path="savedCTCcardCode" cssErrorClass="fieldError">
												<form:option value="-1" disabled="true" selected="selected">
													<spring:theme code="text.payment.ctcCard.select" text="Choose payment card" />
												</form:option>
												<c:forEach var="entry" items="${savedCTCCard}">
													<form:option value="${entry.code}">
														<spring:theme code="text.payment.saved.ctcCard.prefix" text="ends with" />&nbsp;${entry.displayCardNumber}</form:option>
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
							</div>
							<hr class="hr-payment">
						</div>
					</c:if>
					<c:if test="${isB2bCustomer or deferPayment}">
						<div class="container">
							<div class="asm-payment-type-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="radio-btn-bx padding-0-mobile" id="">
											<label class="custom-radio-input">
												<spring:theme code="checkout.summary.paymentMethod.newCTCCard.text" text="Pay using new BC Ferries payment card" />
												<form:radiobutton path="paymentType" value="newCTCCard" cssClass="asm-payment-type-selector" />
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="asm-payment-type-options hidden">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12 nowrap">
											<label for="y_ctcCard">
												<spring:theme code="text.payment.ctc.card.enter.number" />
												&nbsp;#
											</label>
											<form:input path="ctcCardNo" class="form-control" id="y_ctcCard" autocomplete="off" />
											<c:if test="${isB2bCustomer}">
												<div class="mt-4">
													<label class="custom-checkbox-input">
														<form:checkbox path="saveCTCcard" />
														<spring:theme code="text.payment.check.saveCTCcard" text="Save this BC Ferries payment card" />
														<input type="checkbox"> <span class="checkmark-checkbox"></span>
													</label>
												</div>
											</c:if>
										</div>
									</div>
								</div>
							</div>
							<hr class="hr-payment">
						</div>
					</c:if>
					<c:if test="${not empty paymentInfos}">
						<div class="container">
							<div class="asm-payment-type-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="radio-btn-bx padding-0-mobile" id="">
											<label class="custom-radio-input">
												<spring:theme code="checkout.summary.paymentMethod.savedCard.text" text="Saved Card" />
												<form:radiobutton path="paymentType" value="savedCard" cssClass="asm-payment-type-selector" />
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="asm-payment-type-options hidden">
									<div class="saved-credit-cards mt-4">
										<div class="row">
											<div class="col-md-12">
												<div id="ccCardFormGroup" class="form-group">
													<c:forEach var="entry" items="${paymentInfos}">
														<div class="col-md-4 col-sm-6 col-xs-12 nowrap padding-left-0">
															<label class="custom-radio-input">
																&nbsp;&nbsp;
																<img src="../../../../_ui/responsive/common/images/${entry.cardType}.png" width="50">
																&nbsp;&nbsp;${entry.cardNumber}&nbsp;&nbsp;&nbsp;
																<c:choose>
																	<c:when test="${entry.expired}">
																		<spring:theme code="text.payment.cc.expired" text="Expired" />
																	</c:when>
																	<c:otherwise>
																		<spring:theme code="text.payment.cc.expiry.prefix" text="exp." />&nbsp;${entry.expiryMonth}/${entry.expiryYear}
																	</c:otherwise>
																</c:choose>
																<form:radiobutton path="savedCCCardCode" value="${entry.id}" />
																<span class="checkmark"></span>
															</label>
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr class="hr-payment">
						</div>
					</c:if>
				</c:if>
			</sec:authorize>
			<div class="container">
				<div class="asm-payment-type-wrapper">
					<div class="row">
						<div class="col-md-10">
							<div class="radio-btn-bx padding-0-mobile" id="">
								<label class="custom-radio-input">
									<spring:theme code="checkout.summary.paymentMethod.cardNotPresent.text" text="Card is not present" />
									<form:radiobutton path="paymentType" value="cardNotPresent" cssClass="asm-payment-type-selector" />
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<hr class="hr-payment">
				<div class="row">
					<c:if test="${specialServiceRequestCommentAllowed}">
						<c:if test="${not empty agentComment}">
							<div class="col-md-3 col-sm-6 col-xs-12">
								<h4>
									<spring:theme code="checkout.summary.agent.notes" />
								</h4>
								<br>${agentComment}
							</div>
						</c:if>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<formElement:formTextArea idKey="agent-comment" labelKey="label.payment.agent.comment" path="agentComment" />
						</div>
					</c:if>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<formElement:formTextArea idKey="special-instructions" labelKey="label.payment.special.instructions" path="specialInstructions" />
					</div>
					<c:if test="${bookingJourney=='BOOKING_PACKAGE'}">
						<div class="col-md-3 col-sm-6 col-xs-12">
							<formElement:formTextArea idKey="vacation-instructions" labelKey="label.payment.vacation.instructions" path="vacationInstructions" />
						</div>
					</c:if>
				</div>
			</div>
			<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
				<c:if test="${not isSubscriptionOnly}">
					<c:if test="${deferPayment}">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<label class="custom-checkbox-input">
										<form:checkbox id="js-defer-payment-to-cc" path="deferPayment" />
										<spring:theme code="text.payment.cc.card.deferPayment" text="Defer payment to credit card" />
										<input type="checkbox"> <span class="checkmark-checkbox"></span>
									</label>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${isB2bCustomer or deferPayment}">
						<br>
						<div class="container">
							<label class="custom-checkbox-input display-inline">
								<form:checkbox path="allowChangesAtPOS" />
								<spring:theme code="text.payment.check.isAllowChangesAtPOS" text="Allow booking changes at terminal" />
								<input type="checkbox"> <span class="checkmark-checkbox"></span>
							</label>
                            <a href="#" data-container="body" data-toggle="popover" class="popoverThis" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.payment.allow.booking.changes.at.terminal"/>">
                            <span class="bcf bcf-icon-info-solid"></span></a>
						</div>
					</c:if>
				</c:if>
			</sec:authorize>
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
						<input id="y_agentPayNowBtn" class="btn btn-primary btn-block" type="submit" value="<spring:theme code="checkout.summary.paymentMethod.payNow.card.button" text="Pay By Card" />" class="btn-success y btn-block bottom-align" />
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
