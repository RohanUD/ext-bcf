<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="paymentType" required="true" type="java.lang.String"%>

<c:url value="${paymentByGiftFormUrl}" var="submitButtonURL" />
<div class="container">
  <div class="row">
<div class="col-xs-12 payment-wrap y_nonItineraryContentArea giftCardPaymentWrap">
	<div id="giftCardFormGroup" class="form-group  row">
				<form:form method="post" commandName="giftPaymentDetailsForm"
					action="${submitButtonURL}" id="y_agentPayNowByGiftCard" class="giftCard">
					<form:input path="paymentType" id="y_paymentType" value="${paymentType}" type="hidden" />
					<div class="col-md-5 col-sm-4 col-xs-12 pay-box">
					    <form:select class="form-control" id="giftCardType" path="giftCardType">
                            <c:forEach items="${giftCardTypes}" var="giftCardType">
                                <form:option value="${giftCardType.key}"> ${giftCardType.value} </form:option>
                            </c:forEach>
                        </form:select>
                    </div>

					<div class="col-md-5 col-sm-4 col-xs-12">
					<label for="y_amount"> Amount By Gift Card </label>
					<form:input path="amount" class="form-control" id="y_amount" autocomplete="off" />
					</div>
					<div class="col-md-5 col-sm-4 col-xs-12">
					<label for="y_giftCard"> Gift Card No. </label>
					<form:input path="giftCardNo" class="form-control" id="y_giftCard" autocomplete="off" />
					</div>
					<div class="col-md-3 col-sm-4 col-xs-12">
					<label> &nbsp;</label>
					<input id="y_giftPayNowBtn" type="submit" class="btn btn-primary btn-block"
						value="<spring:theme code="checkout.summary.paymentMethod.payNow.giftcard.button" text="Pay By Gift Card" />"xxxx
						class="btn-success y btn-block bottom-align" />
						</div>
				</form:form>
				<span class="input-group-btn add-gift-card">
                	<button class="btn btn-plus y_extraGiftCardSelectorPlus" type="button" data-type="plus" data-field="">
                			<span class="bcf bcf-icon-add"></span>
                	</button>
                </span>
	</div>
</div>
	</div>
</div>
