<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<div class="new-card-and-billing-details ${hideNewCardAndBillingDetails ? 'hidden' : ''}">
	<div class="row">
		<div class="form-group col-sm-9">
			<ul class="list-inline mb-0 payment-img py-4">
				<c:forEach items="${paymentCardTypes}" var="paymentCard" varStatus="cardIdx">
					<li id="cardType_${paymentCard.code}" class="cardType_${paymentCard.code}">
						<form:radiobutton path="cardType" value="${paymentCard.code}" />
						<span> <img src="../../../../_ui/responsive/common/images/${paymentCard.code}.png" width="50">
						</span>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div>
			<form:input type="hidden" id="pd-card-token" path="token" />
			<form:input type="hidden" id="pd-card-number" path="cardNumber" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12 nowrap">
			<formElement:formInputBox idKey="first-name1" labelKey="label.payment.billing.nameOnCard" inputCSS="y_first_name" path="billingAddress.firstName" tabindex="6" mandatory="true" />
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12 nowrap">
			<iframe id="monerisFrame" src="about:blank" scrolling="no" frameborder="0"></iframe>
			<div class="monerisFrameError fe-error"></div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12 nowrap">
			<formElement:formInputBox maxlength="5" inputCSS="y_card_expiry selectpicker form-control" path="cardExpiration" mandatory="true" tabindex="5" placeholder="mm/yy" idKey="pd-expiry" labelKey="label.payment.card.expiry" />
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<label>
				<spring:theme code="payment.cvn" text="CVV"/>
                <a href="javascript:;" data-container="body" class="popoverThis pull-right" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.payment.cvv.additionalinfo" />">
                    <span class="bcf bcf-icon-info-solid"></span>
                </a>
			</label>
			<form:input id="pd-card-verification" path="cardCVNumber" class="y_cardVerification form-control" mandatory="true" tabindex="8" />
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<h4 class="title payment-h2">
				<spring:theme code="checkout.summary.paymentMethod.billing.address.header" text="Billing Address" />
			</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<formElement:formInputBox idKey="addressLine1" labelKey="address.line1" path="billingAddress.line1" tabindex="6" mandatory="true" inputCSS="y_address_line1" />
		</div>
		<div class="col-sm-6">
			<formElement:formInputBox idKey="addressLine2" labelKey="address.line2" path="billingAddress.line2" tabindex="6"
									  inputCSS="y_address_line2"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<formElement:formInputBox idKey="city" labelKey="address.townCity" path="billingAddress.townCity" tabindex="6" maxlength="22" mandatory="true" inputCSS="y_city" />
		</div>
		<div class="col-sm-6 form-group">
			<formElement:formSelectBox idKey="country" labelKey="address.country" path="billingAddress.countryIso"
									   mandatory="true" skipBlank="false"
									   skipBlankMessageKey="text.payment.guest.country.dropdown" items="${supportedCountries}"
									   itemValue="isocode" tabindex="10"
									   selectCSSClass="y_country billing-address-country form-control"
									   dataSize="10"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 form-group billing-address-regions-group">
			<label>
				<spring:theme code="register.region.dropdown.message" text="choose
											region" />
			</label>
			<form:select class="billing-address-regions form-control" id="billing-address-regions" path="billingAddress.regionIso" cssErrorClass="fieldError">
				<form:option value="-1" disabled="true" selected="selected">
					<spring:theme code="text.page.default.select" text="please select" />
				</form:option>
			</form:select>
		</div>
		<div class="col-sm-6">
			<formElement:formInputBox idKey="postalcode" labelKey="address.postalcode" path="billingAddress.postcode" tabindex="6" mandatory="true" inputCSS="y_postal_code" />
		</div>
	</div>

<c:if test="${!isAnonymous}">
		<c:if test="${salesChannel=='Web' and not isSubscriptionOnly}">
			<label class="custom-checkbox-input">
				<form:checkbox path="saveCCCard"/>
				<spring:theme code="text.payment.check.saveCCCard" text="Save this card"/>
				<input type="checkbox"> <span class="checkmark-checkbox"></span>
			</label>
		</c:if>
	</c:if>
	<c:if test="${'BOOKING_TRANSPORT_ONLY' eq bookingJourney}">
         <div class="container">
            <div class="row my-5">
                <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
                <p class="margin-top-30 text-sm"><spring:theme code="text.payment.check.privacy.statement"/></p>
                <br/>
                </div>
             </div>
         </div>
     </c:if>
	<hr class="hr-payment">
</div>
