<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="payment-wrap y_nonItineraryContentArea ctcCardPaymentWrap">
	<div id="ctcCardFormGroup" class="form-group">
		<div class="row">
			<c:if test="${not empty savedCTCCard}">
				<div class="col-md-6 col-sm-6 col-xs-12 nowrap">
					<h3 class="panel-title">
						<spring:theme code="text.payment.ctc.card.header" />
					</h3>
					<form:select class="selectpicker form-control" id="savedCTCcardCode" multiple="false" path="savedCTCcardCode" cssErrorClass="fieldError">
						<form:option value="-1" disabled="true" selected="selected">
							<spring:theme code="text.payment.ctcCard.select" text="Choose payment card" />
						</form:option>
						<c:forEach var="entry" items="${savedCTCCard}">
							<form:option value="${entry.code}">
								<spring:theme code="text.payment.saved.ctcCard.prefix" text="ends with" />&nbsp;${entry.displayCardNumber}</form:option>
						</c:forEach>
					</form:select>
				</div>
			</c:if>
			<div class="col-md-6 col-sm-6 col-xs-12 nowrap">
				<label for="y_ctcCard">
					<c:choose>
						<c:when test="${empty savedCTCCard}">
							<spring:theme code="text.payment.ctc.card.enter.number" />&nbsp;#
						</c:when>
						<c:otherwise>
							<spring:theme code="text.payment.ctc.card.enter.different.number" />&nbsp;#
						</c:otherwise>
					</c:choose>
				</label>
				<form:input path="ctcCardNo" class="form-control" id="y_ctcCard" autocomplete="off" />
				<c:if test="${isB2bCustomer}">
					<div class="mt-4">
						<label class="custom-checkbox-input">
							<form:checkbox path="saveCTCcard" />
							<spring:theme code="text.payment.check.saveCTCcard" text="Save this BC Ferries payment card" />
							<input type="checkbox"> <span class="checkmark-checkbox"></span>
						</label>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
