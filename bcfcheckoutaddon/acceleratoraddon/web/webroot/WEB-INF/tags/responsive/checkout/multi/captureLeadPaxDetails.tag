<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>

<c:if test="${bookingJourney=='BOOKING_PACKAGE' || bookingJourney=='BOOKING_ALACARTE' }">
<c:if test="${not empty roomGuestDatas}">
    <c:set var="numberOfRooms" value="${fn:length(roomGuestDatas)}" />
    <input type="hidden" name="roomCount" value="${numberOfRooms}" />
        <div class="container">
            <hr class="hr-payment">
            <h4><spring:theme code="text.payment.hotel.primary.guest.names.header"/>
            <a href="javascript:;" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.payment.primary.guest.names.additionalinfo" />">
                <span class="bcf bcf-icon-info-solid"></span>
            </a></h4>
            <c:forEach var="roomGuestData" items="${roomGuestDatas}" varStatus="i">

                <div class="row">
                    <div class="col-lg-12">
                        <form:input path="${fn:escapeXml(formPrefix)}roomGuestData[${i.index}].roomRefNumber" id="roomRefNumber" cssClass="y_roomRefNumber" type="hidden" value="${roomGuestData.roomRefNumber}"/>
                        <form:input path="${fn:escapeXml(formPrefix)}roomGuestData[${i.index}].accommodationCode" cssClass="y_accommodationCode" id="accommodationCode" type="hidden" value="${roomGuestData.accommodationCode}"/>
                        <form:input path="${fn:escapeXml(formPrefix)}roomGuestData[${i.index}].checkInDate" cssClass="y_checkInDate" id="checkInDate" type="hidden" value="${roomGuestData.checkInDate}"/>

                        <h5>Room ${i.index+1}: ${roomGuestData.accommodatonName}</h5>
                        <h5>Date : ${roomGuestData.checkInDate}</h5>
                    </div>
                    <div class="col-sm-6">
                        <formElement:formInputBox idKey="first-names" labelKey="label.roomDatas.firstname" path="${fn:escapeXml(formPrefix)}roomGuestData[${i.index}].firstName"  value="${roomGuestData.firstName}" inputCSS="y_first_names" tabindex="6" mandatory="true" />
                    </div>
                    <div class="col-sm-6">
                        <formElement:formInputBox idKey="last-names" labelKey="label.roomDatas.lastname" path="${fn:escapeXml(formPrefix)}roomGuestData[${i.index}].lastName" value="${roomGuestData.lastName}" inputCSS="y_last_names" tabindex="6" mandatory="true" />
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:if>
</c:if>
<c:if test="${bookingJourney=='BOOKING_ACTIVITY_ONLY'}">
<c:if test="${not empty activityGuestDatas}">
    <c:set var="numberOfActivities" value="${fn:length(activityGuestDatas)}" />
    <input type="hidden" name="activityCount" value="${numberOfActivities}" />
        <div class="container">
            <hr class="hr-payment">
            <c:forEach var="activityGuestData" items="${activityGuestDatas}" varStatus="i">
                <div class="row">
                    <h5>Activity ${i.index+1}: ${activityGuestData.activityCode}</h5>
                    <form:input type="hidden" id="activityLeadPaxInfo_activityCode_${i.index}" name="${fn:escapeXml(formPrefix)}activityLeadPaxInfo[${i.index}].activityCode" path="${fn:escapeXml(formPrefix)}activityLeadPaxInfo[${i.index}].activityCode" cssClass="y_activityCode" value="${activityGuestData.activityCode}" />
                    <div class="col-sm-6">
                        <formElement:formInputBox idKey="activityLeadPaxInfo_firstName_${i.index}" labelKey="label.activityLeadPaxInfo.firstName" path="${fn:escapeXml(formPrefix)}activityLeadPaxInfo[${i.index}].firstName" inputCSS="y_activity_first_name" value="${activityGuestData.firstName}" tabindex="6" mandatory="true" />
                    </div>
                    <div class="col-sm-6">
                        <formElement:formInputBox idKey="activityLeadPaxInfo_lastName_${i.index}" labelKey="label.activityLeadPaxInfo.lastName" path="${fn:escapeXml(formPrefix)}activityLeadPaxInfo[${i.index}].lastName" inputCSS="y_activity_last_name" value="${activityGuestData.lastName}" tabindex="7" mandatory="true" />
                    </div>
                </div>
            </c:forEach>
        </div>
</c:if>
</c:if>
