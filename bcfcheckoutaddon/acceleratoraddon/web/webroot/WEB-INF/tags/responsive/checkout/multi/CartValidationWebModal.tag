<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal fade" id="y_cartValidationWebModal" tabindex="-1" role="dialog" aria-labelledby="y_cartValidationWebModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="cartValidationLabel">
					<spring:theme code="checkout.multi.cart.validation.modal.invalid.sailing.title" text="Invalid Sailing" />
				</h3>
			</div>
			<div class="modal-body">

				<p>
					<div class="font-weight-bold"><span id = "y_OriginTerminal">  </span> : <span id="y_DepartureTime"></span>
					&nbsp->&nbsp<span id = "y_ArrivalTerminal">  </span> : <span id="y_ArrivalTime"></span></div>
					<spring:theme code="checkout.multi.cart.validation.modal.statement"   />
				</p>

			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_accept" class="btn btn-primary btn-block" data-dismiss="modal">
							<spring:theme code="checkout.multi.cart.validation.modal.OK" text="OK" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6 y_continueBooking hidden">
                        <button id="y_continueBookingBtn" class="btn btn-primary btn-block " >
                            <spring:theme code="checkout.multi.cart.validation.modal.continue.booking" text="Continue With Booking" />
                        </button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
