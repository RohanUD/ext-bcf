<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="paymentType" required="true" type="java.lang.String"%>

<c:url value="${paymentByCashFormUrl}" var="submitButtonURL" />

<div class="container">
  <div class="row">
<div class="col-xs-12 payment-wrap y_nonItineraryContentArea">
	<div class="form-group row">
				<form:form method="post" commandName="cashPaymentDetailsForm"
					action="${submitButtonURL}" id="y_agentPayNowByCash">
					<form:input path="paymentType" id="y_paymentType" value="${paymentType}" type="hidden" />
					<div class="col-md-5 col-sm-4 col-xs-12">
					<label for="y_amount"> Amount By Cash </label>
					<form:input path="amount" class="form-control" type="text" id="y_amount" value="${amount}" autocomplete="off" />
					</div>
					<div class="col-md-5 col-sm-4 col-xs-12">
					<label for="y_receiptNo"> Receipt No. </label>
					<form:input path="receiptNo" type="text" class="form-control" id="y_receiptNo" value="${receiptNo}" autocomplete="off" />
					</div>
					<div class="col-md-2 col-sm-4 col-xs-12">
					<label> &nbsp;</label>
					<input id="y_cashPayNowBtn" type="submit" class="btn btn-primary btn-block"
						value="<spring:theme code="checkout.summary.paymentMethod.payNow.cash.button" text="Pay By Cash" />"
						class="btn-success y btn-block bottom-align" />
						</div>
				</form:form>
	</div>
</div>
</div>
</div>
