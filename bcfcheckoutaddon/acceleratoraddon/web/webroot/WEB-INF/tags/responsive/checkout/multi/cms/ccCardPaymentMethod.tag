<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:if test="${not empty paymentInfos}">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12 nowrap">
			<h4 class="panel-title">
				<spring:theme code="text.payment.cc.card.header" />
			</h4>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 nowrap text-right">
			<a href="${manageCardsURL}">
				<spring:theme code="text.payment.cc.manage.cards" text="Manage cards" />
			</a>
		</div>
	</div>
	<hr class="hr-payment">
	<div class="payment-wrap y_nonItineraryContentArea ccCardPaymentWrap">
		<div id="ccCardFormGroup" class="form-group">
			<c:forEach var="entry" items="${paymentInfos}">
				<div class="col-md-4 col-sm-6 col-xs-12 nowrap padding-left-0">
					<label class="custom-radio-input">
						&nbsp;&nbsp;
						<img src="../../../../_ui/responsive/common/images/${entry.cardType}.png" width="50">
						&nbsp;&nbsp;${entry.cardNumber}&nbsp;&nbsp;&nbsp;
						<c:choose>
							<c:when test="${entry.expired}">
								<spring:theme code="text.payment.cc.expired" text="Expired" />
							</c:when>
							<c:otherwise>
								<spring:theme code="text.payment.cc.expiry.prefix" text="exp." />&nbsp;${entry.expiryMonth}/${entry.expiryYear}
						</c:otherwise>
						</c:choose>
						<form:radiobutton path="savedCCCardCode" value="${entry.id}" />
						<span class="checkmark"></span>
					</label>
				</div>
			</c:forEach>
		</div>
	</div>
	<div class="clearfix"></div>
	<hr class="hr-payment">
</c:if>
