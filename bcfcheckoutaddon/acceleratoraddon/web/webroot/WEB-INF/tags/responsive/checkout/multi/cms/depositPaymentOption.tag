<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="paymentType" required="true" type="java.lang.String"%>

<div class="container">
  <div class="row">
<div class="col-xs-12 payment-wrap y_nonItineraryContentArea">
	<div class="form-group row">
		<form:form method="post" commandName="depositPaymentOptionForm" id="y_agentPayNowDeposit">
			<form:input path="paymentType" id="y_paymentType" value="${paymentType}" type="hidden" />
			<div class="col-md-5 col-sm-4 col-xs-12">
				<label for="y_amount"> Deposit amount </label>
				<form:input path="depositAmount" class="form-control" type="text" id="y_deposit_amount" value="${depositAmount}" autocomplete="off" />
			</div>
			<div class="bc-accordion ferry-drop vacation-calender-section">
				<ul class="nav nav-tabs vacation-calender package-hotel-details-ul">
					<li class="tab-links tab-depart">
						<label for="y_deposit_pay_date"><spring:theme code="text.page.mybooking.depositPayDate" text="Balance Due Date" /></label>
						<a data-toggle="tab" href="#check-in" class="nav-link component-to-top">
							<div class="vacation-calen-box vacation-ui-depart">
								<span class="vacation-calen-date-txt">
									<spring:theme code="label.farefinder.vacation.date.placeholder" text="Date" />
								</span>
								<span class="vacation-calen-year-txt current-year-custom"></span>
								<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big" aria-hidden="true"></i>
							</div>
						</a>
					</li>
				</ul>
				<div class="tab-content package-hotel-details-content">
					<div id="check-in" class="tab-pane input-required-wrap  depart-calendar">
						<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
							<label>
								<spring:theme code="label.farefinder.vacation.date.dateformat" text="Search date:mm/dd/yyyy" />
							</label>
							<form:input type="hidden" path="depositPayDate" class="form-control datePickerDeparting  datepicker bc-dropdown depart datepicker-input" placeholder="Check In" autocomplete="off" id="y_deposit_pay_date" />
							<div id="datepicker" class="bc-dropdown--big"></div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>
</div>
</div>
</div>
