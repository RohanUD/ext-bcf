ACC.silentorderpost = {

	spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),

	bindUseDeliveryAddress: function ()
	{
		$('#useDeliveryAddress').on('change', function ()
		{
			if ($('#useDeliveryAddress').is(":checked"))
			{
				var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
				ACC.silentorderpost.enableAddressForm();
				ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
				ACC.silentorderpost.disableAddressForm();
			}
			else
			{
				ACC.silentorderpost.clearAddressForm();
				ACC.silentorderpost.enableAddressForm();
			}
		});

		if ($('#useDeliveryAddress').is(":checked"))
		{
			var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
			ACC.silentorderpost.enableAddressForm();
			ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
			ACC.silentorderpost.disableAddressForm();
		}
	},

	bindSubmitSilentOrderPostForm: function ()
	{
		$('.submit_silentOrderPostForm').click(function ()
		{
			ACC.common.blockFormAndShowProcessingMessage($(this));
			$('.billingAddressForm').filter(":hidden").remove();
			ACC.silentorderpost.enableAddressForm();
			$('#silentOrderPostForm').submit();
		});
	},

	bindCycleFocusEvent: function ()
	{
		$('#lastInTheForm').blur(function ()
		{
			$('#silentOrderPostForm [tabindex$="10"]').focus();
		})
	},

	isEmpty: function (obj)
	{
		if (typeof obj == 'undefined' || obj === null || obj === '') return true;
		return false;
	},

	disableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', true);
		$('select[id^="address\\."]').prop('disabled', true);
	},

	enableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', false);
		$('select[id^="address\\."]').prop('disabled', false);
	},

	clearAddressForm: function ()
	{
		$('input[id^="address\\."]').val("");
		$('select[id^="address\\."]').val("");
	},

	useDeliveryAddressSelected: function ()
	{
		if ($('#useDeliveryAddress').is(":checked"))
		{
			$('#address\\.country').val($('#useDeliveryAddressData').data('countryisocode'));
			ACC.silentorderpost.disableAddressForm();
		}
		else
		{
			ACC.silentorderpost.clearAddressForm();
			ACC.silentorderpost.enableAddressForm();
		}
	},
	
	

	bindCreditCardAddressForm: function ()
	{
		$('#billingCountrySelector :input').on("change", function ()
		{
			var countrySelection = $(this).val();
			var options = {
				'countryIsoCode': countrySelection
			};
			ACC.silentorderpost.displayCreditCardAddressForm(options);
		})
	},

	displayCreditCardAddressForm: function (options, callback)
	{
		$.ajax({ 
			url: ACC.config.encodedContextPath + '/checkout/multi/payment/billingaddressform',
			async: true,
			data: options,
			dataType: "html",
			beforeSend: function ()
			{
				$('#billingAddressForm').html(ACC.silentorderpost.spinner);
			}
		}).done(function (data)
				{
					$("#billingAddressForm").html(data);
					if (typeof callback == 'function')
					{
						callback.call();
					}
				});
	},
	
	bindVoucherRedeemButton : function() {
		$(document).on('click', '#y_voucherRedeemBtn', function(e) {
			ACC.silentorderpost.redeemVoucher(this);
		});
		$(document).on('keydown', '#y_voucherCode', function(e) {
			// Enter key = fill input field with the current suggestion
		    if ((e.keyCode || e.which) == 13) {
		    	e.preventDefault();
		    	ACC.silentorderpost.redeemVoucher($("#y_voucherRedeemBtn"));
		    }
		});
	},
	
	redeemVoucher : function(trigger) {
		var voucherCode = $("#y_voucherCode").val();
		if (voucherCode.trim().length > 0) {
			var ajaxRep = ACC.services.redeemVoucher(voucherCode);
			if (ajaxRep.responseJSON.errorMsg == null) {
				ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
				ACC.silentorderpost.showVoucherSuccessMsg(ajaxRep.responseJSON.successMsg);
				
				var numberOfVouchers = $("."+$(trigger).data("target")).length;
				if( numberOfVouchers===1 && !$(".y_appliedVoucherItem").is(":visible")){
					$(".y_appliedVoucherItem .y_voucherCode").html(voucherCode);
					$(".y_appliedVoucherItem .y_voucherReleaseBtn").data("code",voucherCode);
					$(".y_appliedVoucherItem").show();
				}
				else{
					var targetName = $(trigger).data('target'),
	                $targetElem = $('.'+targetName),
	                num     = parseInt($targetElem.last().attr('id').match(/\d+$/)), // index of the greatest "duplicatable" input fields
	                newNum  = new Number(num + 1),      // the numeric ID of the new input field being added
	                $newElem = $('#'+targetName + num).clone().attr('id', targetName + newNum);
					
					$newElem.find(".y_voucherCode").html(voucherCode);
					$newElem.find(".y_voucherReleaseBtn").data("code",voucherCode);
					$newElem.find(".y_voucherReleaseBtn").data("target",targetName + newNum);
					$('#'+targetName + num).after($newElem);
				}
				$("#y_voucherCode").val("");
			} else {
				ACC.silentorderpost.showVoucherErrorMsg(ajaxRep.responseJSON.errorMsg);
			}
		}
	},
	
	bindVoucherReleaseButton : function() {
		$(document).on('click', '.y_voucherReleaseBtn',function(e) {
			var voucherCode = $(this).data('code');
			if (voucherCode.trim().length > 0) {
				var ajaxRep = ACC.services.releaseVoucher(voucherCode);
				if (ajaxRep.responseJSON.errorMsg == null) {
					ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
					ACC.silentorderpost.showVoucherSuccessMsg(ajaxRep.responseJSON.successMsg);
					if($(".y_appliedVoucherItem").length===1){
						$("#"+$(this).data('target')).hide();
					}
					else{
						$("#"+$(this).data('target')).remove();
					}
				} else {
					ACC.silentorderpost.showVoucherErrorMsg(ajaxRep.responseJSON.errorMsg);
				}
			}
		});
	},
	
	showVoucherSuccessMsg : function(successMsg) {
		$("#y_voucherMsgDiv").removeClass("alert-danger");
		$("#y_voucherMsgDiv").addClass("alert-success");
		$("#y_voucherMsg").text(successMsg)
		$("#y_voucherMsgDiv").show();
	},
	
	showVoucherErrorMsg : function(errorMsg) {
		$("#y_voucherMsgDiv").removeClass("alert-success");
		$("#y_voucherMsgDiv").addClass("alert-danger");
		$("#y_voucherMsg").text(errorMsg)
		$("#y_voucherMsgDiv").show();
	}

}

$(document).ready(function ()
{
	with (ACC.silentorderpost)
	{
		bindUseDeliveryAddress()
		bindSubmitSilentOrderPostForm();
		bindCreditCardAddressForm();
		bindVoucherRedeemButton();
		bindVoucherReleaseButton();
	}
});
