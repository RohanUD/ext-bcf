ACC.paymentType = {

    _autoload: [
        "bindPaymenttypeValidationMessages",
        "bindPaymentTypeSelect",
        "showHideCostCenterSelect",
        "bindPaymentOptionRadio",
        "bindExtraGiftCard",
        "formValidationInit",
        "bindTermsAndConditionShow"
    ],
    bindPaymenttypeValidationMessages : function () {
        ACC.PaymenttypeValidation = new validationMessages("ferry-PaymenttypeValidationMessages");
        ACC.PaymenttypeValidation.getMessages("error");
        ACC.PaymenttypeValidation.getMessages("label");
    },
    formValidationInit : function(){
        ACC.paymentType.bindFormValidation($("#y_agentPayNowByCash"));
        ACC.paymentType.bindFormValidation($("#y_agentPayNowByGiftCard"));
        ACC.paymentType.bindFormValidation($("#y_agentPayNowCard"));
        ACC.paymentType.bindFormValidation($("#y_agentPayNowDeposit"));

        if($("#y_agentPayNowDeposit").find("#y_deposit_pay_date").length>0){
            $("#y_agentPayNowDeposit").find("#y_deposit_pay_date").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.deposit.amount.required')
                }
            });
        }

        if($("#y_agentPayNowByCash").find("#y_amount").length>0){
            $("#y_agentPayNowByCash").find("#y_amount").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.cash.amount.required')
                }
            });
        }


        if($("#y_agentPayNowByCash").find("#y_receiptNo").length>0){

            $("#y_agentPayNowByCash").find("#y_receiptNo").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.cash.receiptNo.required')
                }
            });

        }

        if($("#y_agentPayNowByGiftCard").find("#y_amount").length>0){
            $("#y_agentPayNowByGiftCard").find("#y_amount").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.gift.card.amount.required')
                }
            });
        }

        if($("#y_agentPayNowByGiftCard").find("#y_giftCard").length>0){
            $("#y_agentPayNowByGiftCard").find("#y_giftCard").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.gift.card.number.required')
                }
            });
        }

        if($("#y_agentPayNowCard").find("input[type='radio']").length>0){
            $("#y_agentPayNowCard").find("input[type='radio']").rules( "add", {
                required: true,
                messages: {
                    required: ACC.PaymenttypeValidation.message('error.formvalidation.card.required')
                }
            });
        }
    },
    bindTermsAndConditionShow: function(){
        if($('#tmc').find('.hidden').length > 0){
            $('#tmc').find('.activeShow').click();
        }
    },

    bindFormValidation: function(ele){
        ele.validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            onfocusout: function(element) {
                $(element).valid();
            },
            invalidHandler: function(form, validator) {
                $("#y_agentPayNow").valid();
                ele.closest("section.yCmsComponent").find("form").each(function(){
                    if($(this).attr("id") !== ele.attr("id")){
                        $(this).find("span.fe-error").hide();
                        $(this).find("input.fe-error").removeClass("fe-error");
                    }
                })
                ACC.paymentType.bindTermsAndConditionShow();
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler : function(){
                ACC.paymentType.bindTermsAndConditionShow();
                if($("#y_agentPayNowDeposit").length > 0)
                {
	                if ($("#y_deposit_amount").val() != "") {
	                    if (!$("#y_agentPayNowDeposit").valid()) {
	                        return false;
	                    } else {
	                        $("#y_agentPayNowDeposit").find("span.fe-error").hide();
	                        $("#y_agentPayNowDeposit").find("input.fe-error").removeClass("fe-error");
	                    }
	                } else {
	                    $("#y_agentPayNowDeposit").find("span.fe-error").hide();
	                    $("#y_agentPayNowDeposit").find("input.fe-error").removeClass("fe-error");
	                }
                }
                $('.y_makePaymentErrorBody').addClass("hidden");
                //form.submit();
                if ($('#payRemaining').attr("id") !== undefined) {
                    var data = ele.serialize();
                    if($(event.target).hasClass('giftCard'))
                    {
                        var formElem = $(event.target);
                        data = formElem.serialize();
                    }
                    data += "&payRemaining=yes";

                    ACC.paymentType.bindPayNowForTravelCentre(ele.attr("action"),data,ele.attr("method"),formElem);
                } else {
                    if($("#y_agentPayNow").valid()){var formElem = null;
                        var data = ele.serialize();
                        if($(event.target).hasClass('giftCard'))
                        {
                             formElem = $(event.target);
                            data = formElem.serialize();
                        }
                        var extraFields = ["#first-name2","#last-name","#email","#confirm-email","#phone-number","#country1","#room_first-names","#room_last-names","#roomCount","#y_deposit_amount","#y_deposit_pay_date","#postCode"];
                        _.forEach(extraFields,function(field){
                            var fName = $(field).attr("name");
                            var fVal = $(field).val();
                            data +="&"+fName+"="+fVal;
                        })

                        if($("input[name=roomCount]").length){
                            var fName = $("input[name=roomCount]").attr("name");
                            var fVal =  $("input[name=roomCount]").val();
                            data +="&"+fName+"="+fVal;
                        }
                        if($(".y_first_names").length){
                            $(".y_first_names").each(function (index, element) {
                                var fName = $(this).attr("name");
                                var fVal = $(this).val();
                                data +="&"+fName+"="+fVal;
                        });
                        }
                         if($(".y_last_names"). length){
                            $(".y_last_names").each(function (index, element) {
                                var fName = $(this).attr("name");
                                var fVal = $(this).val();
                                data +="&"+fName+"="+fVal;
                            });
                         }
                         if($(".y_roomRefNumber").length){
                             $(".y_roomRefNumber").each(function (index, element) {
                                 var fName = $(this).attr("name");
                                 var fVal = $(this).val();
                                 data +="&"+fName+"="+fVal;
                         });
                         }
                         if($(".y_accommodationCode").length){
                              $(".y_accommodationCode").each(function (index, element) {
                                  var fName = $(this).attr("name");
                                  var fVal = $(this).val();
                                  data +="&"+fName+"="+fVal;
                          });
                          }
                          if($(".y_checkInDate").length){
                                $(".y_checkInDate").each(function (index, element) {
                                    var fName = $(this).attr("name");
                                    var fVal = $(this).val();
                                    data +="&"+fName+"="+fVal;
                            });
                            }
                        if($("input[name=activityCount]").length){
                            var fName = $("input[name=activityCount]").attr("name");
                            var fVal =  $("input[name=activityCount]").val();
                            data +="&"+fName+"="+fVal;
                        }
                        if($(".y_activity_first_name").length){
                            $(".y_activity_first_name").each(function (index, element) {
                                var fName = $(this).attr("name");
                                var fVal = $(this).val();
                                data +="&"+fName+"="+fVal;
                        });
                        }
                         if($(".y_activity_last_name"). length){
                            $(".y_activity_last_name").each(function (index, element) {
                                var fName = $(this).attr("name");
                                var fVal = $(this).val();
                                data +="&"+fName+"="+fVal;
                            });
                         }
                         if($(".y_activityCode"). length){
                             $(".y_activityCode").each(function (index, element) {
                                 var fName = $(this).attr("name");
                                 var fVal = $(this).val();
                                 data +="&"+fName+"="+fVal;
                             });
                          }
                        ACC.paymentType.bindPayNowForTravelCentre(ele.attr("action"),data,ele.attr("method"),formElem);
                    }
                }

            }
        });


    },
    bindPaymentTypeSelect: function ()
    {
        $("input:radio[name='paymentType']").change(function()
        {
            ACC.paymentType.showHideCostCenterSelect();
        });
    },

    showHideCostCenterSelect: function() {
        var paymentTypeSelected =  $("input:radio[name='paymentType']:checked").val();
        if(paymentTypeSelected == "ACCOUNT") {
            $("#costCenter").show();
        } else {
            $("#costCenter").hide();
        }
    },

    bindPayNowForTravelCentre : function(url,data,type,formElem)
    {
        $.when(ACC.services.submitPaymentTypeFormForTravelCentre(url,data,type)).then(
            function (response)
            {
                var jsonData = JSON.parse(response);
                if(jsonData.valid)
                {

                    if(jsonData.confirmBookingUrl != undefined && jsonData.confirmBookingUrl != '')
                    {
                        window.location.href = ACC.config.contextPath+jsonData.confirmBookingUrl;
                    }
                    else
                    {
                        $('.y_makePaymentErrorBody').html('');
                        $('#y_remainingAmount').removeClass('hidden');
                        $('#y_remainingAmount').html('Remaing Amount: <p>'+jsonData.remainingAmount+'</p>');
                        var form = $();
                        if(jsonData.paymentType=='Cash')
                        {
                            form = $('#y_agentPayNowByCash');
                        }
                        if(jsonData.paymentType=='GiftCard')
                        {
                            form = formElem;
                        }if(jsonData.paymentType=='cardPresent' || jsonData.paymentType == 'cardNotPresent')
                        {
                            form = $('#y_agentPayNow');}

                        if(jsonData.paymentType == 'Cash' && jsonData.remainingAmount > 0)
                        {
                            // just clear the form
                            ACC.paymentType.clearPayByCashForm(form);
                        }
                        else
                        {
                            ACC.paymentType.disableForm(form);
                        }
                    }
                }
                else
                {
                    $('.y_makePaymentErrorBody').html(jsonData.error);
                    $('.y_makePaymentErrorBody').removeClass("hidden");
                }
            });
    },

    disableForm : function(form)
    {
        form.find('input').prop('disabled', true);
    },

    clearPayByCashForm : function(form)
    {
        form.find('input#y_amount').val("");
        form.find('input#y_receiptNo').val("");
    },

    bindExtraGiftCard : function()
    {
        var cloneCount = 1;
        $('.y_extraGiftCardSelectorPlus').click(function(){
            var previousGiftCardFormGroup = $('#giftCardFormGroup' + (cloneCount - 1));
                var giftCardFormGroupClone = $('#giftCardFormGroup').clone(true);
                if (cloneCount == 1) {
                giftCardFormGroupClone.attr('id', 'giftCardFormGroup'+ cloneCount++)
                .insertAfter("div#giftCardFormGroup:last");
            } else {
                giftCardFormGroupClone.attr('id', 'giftCardFormGroup' + cloneCount++)
                    .insertAfter(previousGiftCardFormGroup);
            }

            giftCardFormGroupClone.find('#y_amount').val('');
            giftCardFormGroupClone.find('#y_giftCard').val('');
giftCardFormGroupClone.find('#y_amount').prop('disabled', false);
            giftCardFormGroupClone.find('#y_giftCard').prop('disabled', false);
            giftCardFormGroupClone.find('#y_giftPayNowBtn').prop('disabled', false);

            $(this).prop('disabled', true);
        });

    },

    bindPaymentOptionRadio : function()
    {
        $('.y_paymentOptionRadio').on('click', function (e)
        {
            $(this).attr('checked', 'checked');
            $(this).attr('value', true);

            $(this).siblings("input:radio").removeAttr('checked');
            $(this).siblings("input:radio").attr('value', false);
        });
    }
}
