/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.constants;

/**
 * Global class for all Bcfcheckoutaddon constants. You can add global constants for your extension into this class.
 */
public final class BcfcheckoutaddonConstants extends GeneratedBcfcheckoutaddonConstants
{

	public static final String EXTENSIONNAME = "bcfcheckoutaddon";
	public static final String NOINDEX_NOFOLLOW = "noindex,nofollow";
	public static final String META_ROBOTS = "metaRobots";
	public static final String AMOUNT_TO_PAY = "amountToPay";

	private BcfcheckoutaddonConstants()
	{
		// empty to avoid instantiating this constant class
	}

}
