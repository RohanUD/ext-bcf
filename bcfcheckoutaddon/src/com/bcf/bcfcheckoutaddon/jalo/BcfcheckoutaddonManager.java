/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfcheckoutaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.bcfcheckoutaddon.constants.BcfcheckoutaddonConstants;


@SuppressWarnings("PMD")
public class BcfcheckoutaddonManager extends GeneratedBcfcheckoutaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(BcfcheckoutaddonManager.class.getName());

	public static final BcfcheckoutaddonManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfcheckoutaddonManager) em.getExtension(BcfcheckoutaddonConstants.EXTENSIONNAME);
	}

}
