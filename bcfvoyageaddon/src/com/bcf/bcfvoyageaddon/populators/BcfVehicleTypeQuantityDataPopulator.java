/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


public class BcfVehicleTypeQuantityDataPopulator implements Populator<BCFVehicleInformationModel, VehicleTypeQuantityData>
{
	private Converter<VehicleTypeModel, VehicleTypeData> vehicleTypeConverter;

	@Override
	public void populate(final BCFVehicleInformationModel source, final VehicleTypeQuantityData target) throws ConversionException
	{
		target.setVehicleType(getVehicleTypeConverter().convert(source.getVehicleType()));
		target.setHeight(source.getHeight());
		target.setLength(source.getLength());
		target.setWidth(source.getWidth());
		target.setCarryingLivestock(source.getCarryingLivestock());
		target.setVehicleWithSidecarOrTrailer(source.getVehicleWithSidecarOrTrailer());
		target.setQty(1);
	}

	public Converter<VehicleTypeModel, VehicleTypeData> getVehicleTypeConverter()
	{
		return vehicleTypeConverter;
	}

	public void setVehicleTypeConverter(final Converter<VehicleTypeModel, VehicleTypeData> vehicleTypeConverter)
	{
		this.vehicleTypeConverter = vehicleTypeConverter;
	}
}
