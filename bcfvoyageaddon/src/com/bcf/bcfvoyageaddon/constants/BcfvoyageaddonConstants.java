/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.constants;

/**
 * Global class for all Bcfvoyageaddon constants. You can add global constants for your extension into this class.
 */
public final class BcfvoyageaddonConstants extends GeneratedBcfvoyageaddonConstants
{
	public static final String EXTENSIONNAME = "bcfvoyageaddon";

	private BcfvoyageaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
