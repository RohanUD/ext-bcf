/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonConstants;


@SuppressWarnings("PMD")
public class BcfvoyageaddonManager extends GeneratedBcfvoyageaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(BcfvoyageaddonManager.class.getName());

	public static final BcfvoyageaddonManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfvoyageaddonManager) em.getExtension(BcfvoyageaddonConstants.EXTENSIONNAME);
	}

}
