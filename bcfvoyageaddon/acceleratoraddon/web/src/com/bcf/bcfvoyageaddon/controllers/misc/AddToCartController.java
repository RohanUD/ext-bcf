/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.AddToCartResponseData;
import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.TravelBundleTemplateData;
import de.hybris.platform.commercefacades.travel.ancillary.accommodation.data.ConfiguredAccommodationData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.ConfiguredAccommodationFacade;
import de.hybris.platform.travelfacades.facades.TravelRestrictionFacade;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.strategies.AddAccommodationToCartStrategy;
import de.hybris.platform.travelfacades.strategies.AddBundleToCartValidationStrategy;
import de.hybris.platform.travelfacades.strategies.AddToCartValidationStrategy;
import de.hybris.platform.travelfacades.strategies.SelectedAccommodationStrategy;
import de.hybris.platform.travelservices.enums.AmendStatus;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.order.CommerceBundleCartModificationException;
import de.hybris.platform.travelservices.price.data.PriceLevel;
import de.hybris.platform.tx.Transaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;
import com.bcf.bcfvoyageaddon.forms.AddToCartForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.model.order.DealOrderEntryGroupModel;
import com.bcf.core.order.CommerceCartModificationPriceDifferentException;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.BcfDealCartFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.com.bcf.facades.cart.BcfAddToCartHandlerFacade;
import com.bcf.facades.commerce.BCFTravelCommercePriceFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.CartEntryPropertiesData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.constants.BcfintegrationserviceConstants;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class AddToCartController extends AbstractController
{
	protected static final String ERROR_MSG_TYPE = "errorMsg";
	protected static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";
	protected static final String VOUCHER_REDEEM_SUCCESS = "redeemSuccess";
	protected static final String VOUCHER_RELEASE_SUCCESS = "releaseSuccess";
	/**
	 * This constant value dictates the quantity of the fare product to be added in the cart.
	 */

	private static final Logger LOG = Logger.getLogger(AddToCartController.class);
	protected static final long PRODUCT_QUANTITY = 1;
	private static final String PRICE_DIFFERENCE_ERROR_CODE = "Error101";
	private static final String ADD_TO_CART_ERROR = "error";
	private static final String REMOVE_FROM_CART_ERROR = "error";
	private static final String MAKE_BOOKING_ERROR = "makebooking.errorcode.";
	public static final String ADD_BUNDLE_TO_CART_REQUEST_ERROR = "add.bundle.to.cart.request.error";
	private static final String ADD_PRODUCT_TO_CART_RESPONSE = "addProductToCartResponse";
	private static final String ADD_BUNDLE_TO_CART_RESPONSE = "addBundleToCartResponse";
	private static final String NO_ITEMS_ADDED = "basket.information.quantity.noItemsAdded.";
	private static final String REDUCED_NUMBER_OF_ITEMS_ADDED = "basket.information.quantity.reducedNumberOfItemsAdded.";
	private static final String REFERER = "referer";
	private static final String REMOVE_SAILING_FAILED = "reservation.remove.sailing.integration.failure";


	@Resource(name = "bcfProductFacade")
	private BcfProductFacade bcfProductFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfAddToCartHandlerFacade")
	private BcfAddToCartHandlerFacade bcfAddToCartHandlerFacade;

	@Resource(name = "travellerFacade")
	private TravellerFacade travellerFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travelRestrictionFacade")
	private TravelRestrictionFacade travelRestrictionFacade;

	@Resource(name = "addAccommodationToCartStrategy")
	private AddAccommodationToCartStrategy addAccommodationToCartStrategy;

	@Resource(name = "configuredAccommodationFacade")
	private ConfiguredAccommodationFacade configuredAccommodationFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "travelCommercePriceFacade")
	private BCFTravelCommercePriceFacade travelCommercePriceFacade;

	@Resource(name = "addToCartValidationStrategyList")
	private List<AddToCartValidationStrategy> addToCartValidationStrategyList;

	@Resource(name = "selectedAccommodationStrategyList")
	private List<SelectedAccommodationStrategy> selectedAccommodationStrategyList;

	@Resource(name = "addPackageBundleToCartValidationStrategyList")
	private List<AddBundleToCartValidationStrategy> addPackageBundleToCartValidationStrategyList;

	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "bcfDealCartFacade")
	private BcfDealCartFacade bcfDealCartFacade;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "addBundleToCartValidationStrategyList")
	public List<AddBundleToCartValidationStrategy> addBundleToCartValidationStrategyList;

	@Resource(name = "cartService")
	private BCFTravelCartService cartService;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade travelRouteFacade;

	/**
	 * @param addToCartForm
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
	public String addToCart(final AddToCartForm addToCartForm, final Model model)
	{

		final String travellerCode = StringUtils.isNotBlank(addToCartForm.getTravellerCode()) ? addToCartForm.getTravellerCode()
				: null;
		final String travelRouteCode = StringUtils.isNotBlank(addToCartForm.getTravelRouteCode())
				? addToCartForm.getTravelRouteCode()
				: null;
		final String bookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		final boolean followIntegration = (Objects.nonNull(bookingJourney)
				&& (StringUtils.equals(bookingJourney, BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode()) || StringUtils
				.equals(bookingJourney, BookingJourneyType.BOOKING_PACKAGE.getCode())));
		if (validateByStrategies(addToCartForm, travellerCode, travelRouteCode, model))
		{
			return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
		}
		final OrderEntryData existingOrderEntry;
		if (cartService.isAmendmentCart(cartService.getSessionCart()))
		{
			existingOrderEntry = bcfTravelCartFacade
					.getAncillaryOrderEntry(addToCartForm.getProductCode(), addToCartForm.getJourneyRefNumber(),
							addToCartForm.getOriginDestinationRefNumber());
		}
		else
		{
			existingOrderEntry = bcfTravelCartFacade.getExistingAncillaryOrderEntry(addToCartForm.getProductCode(), travelRouteCode,
					addToCartForm.getTransportOfferingCodes(), travellerCode, addToCartForm.getJourneyRefNumber(),
					addToCartForm.getOriginDestinationRefNumber());
		}
		if (Objects.isNull(existingOrderEntry) && addToCartForm.getQty() > 0)
		{
			if (!addToCartForExistingOrderEntry(addToCartForm, travelRouteCode, travellerCode, model, followIntegration))
			{
				return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
			}
		}
		else
		{
			addToCartForOrderEntry(existingOrderEntry, addToCartForm, followIntegration, model);
		}

		if (!model.containsAttribute(ADD_PRODUCT_TO_CART_RESPONSE))
		{
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null, null, null));

		}
		return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
	}

	private boolean validateByStrategies(final AddToCartForm addToCartForm, final String travellerCode,
			final String travelRouteCode, final Model model)
	{
		for (final AddToCartValidationStrategy strategy : addToCartValidationStrategyList)
		{
			final AddToCartResponseData response = strategy.validateAddToCart(addToCartForm.getProductCode(), addToCartForm.getQty(),
					travellerCode, addToCartForm.getTransportOfferingCodes(), travelRouteCode);
			if (!response.isValid())
			{
				model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE, response);
				return true;
			}
		}
		return false;
	}

	protected boolean addToCartForExistingOrderEntry(final AddToCartForm addToCartForm, final String travelRouteCode,
			final String travellerCode, final Model model, final boolean followIntegration)
	{
		try
		{
			final PriceLevel priceLevel = travelCommercePriceFacade.getPriceLevelInfo(addToCartForm.getProductCode(),
					addToCartForm.getTransportOfferingCodes(), travelRouteCode);
			if (priceLevel == null)
			{
				model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED, null, null));
				return false;
			}
			setPriceAndTaxSearchCriteriaInContext(travellerCode, priceLevel, addToCartForm);
			setCartData(followIntegration, priceLevel, travelRouteCode, travellerCode, addToCartForm, model);
		}
		catch (final CommerceCartModificationPriceDifferentException ex)
		{
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ex.getMessage(), null, PRICE_DIFFERENCE_ERROR_CODE));
		}
		catch (final IntegrationException e)
		{
			LOG.info("Product Code: " + addToCartForm.getProductCode() + ", Quantity: " + addToCartForm.getQty(), e);
			final String errorMessage = bcfTravelCartFacadeHelper
					.getErrorMessage(MAKE_BOOKING_ERROR, e.getErorrListDTO().getErrorDto().get(0).getErrorCode(),
							e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, errorMessage, null, BASKET_ERROR_OCCURRED));
		}
		catch (final Exception e)
		{
			LOG.info("Product Code: " + addToCartForm.getProductCode() + ", Quantity: " + addToCartForm.getQty(), e);
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, null, null, BASKET_ERROR_OCCURRED));
		}
		return true;
	}



	private void addToCartForOrderEntry(final OrderEntryData existingOrderEntry, final AddToCartForm addToCartForm,
			final boolean followIntegration, final Model model)
	{
		try
		{
			final long newQuantity = addToCartForm.getQty();
			final AddProductToCartRequestData addProductToCartRequestData = createAddProductToCartRequestData(addToCartForm);
			final CartModificationData cartModification = followIntegration
					? bcfTravelCartFacade.updateCart(addProductToCartRequestData, existingOrderEntry.getEntryNumber(), newQuantity)
					: bcfTravelCartFacade.updateCartEntry(existingOrderEntry.getEntryNumber(), newQuantity);
			bcfTravelCartFacade.removeAmendBookingFees();
			if (cartModification.getQuantityAdded() == newQuantity)
			{
				LOG.info("Product Code:" + cartModification.getEntry().getProduct().getCode() + "has been update with Quantity:"
						+ cartModification.getQuantityAdded());
			}
		}
		catch (final CommerceCartModificationException ex)
		{
			LOG.warn("Couldn't update product with the entry number: " + existingOrderEntry.getEntryNumber() + ".", ex);
		}
		catch (final IntegrationException e)
		{
			final String errorMessage = bcfTravelCartFacadeHelper
					.getErrorMessage(MAKE_BOOKING_ERROR, e.getErorrListDTO().getErrorDto().get(0).getErrorCode(),
							e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, errorMessage, null, StringUtils.EMPTY));
		}
	}

	private void setPriceAndTaxSearchCriteriaInContext(final String travellerCode, final PriceLevel priceLevel,
			final AddToCartForm addToCartForm)
	{
		if (travellerCode != null)
		{
			travelCommercePriceFacade.setPriceAndTaxSearchCriteriaInContext(priceLevel, addToCartForm.getTransportOfferingCodes(),
					travellerFacade.getTravellerFromCurrentCart(travellerCode));
		}
		else
		{
			travelCommercePriceFacade.setPriceAndTaxSearchCriteriaInContext(priceLevel, addToCartForm.getTransportOfferingCodes());
		}
	}


	private void setCartData(final boolean followIntegration, final PriceLevel priceLevel, final String travelRouteCode,
			final String travellerCode, final AddToCartForm addToCartForm, final Model model)
			throws CommerceCartModificationException, IntegrationException
	{
		final AddProductToCartRequestData addProductToCartRequestData = createAddProductToCartRequestData(addToCartForm);
		final CartModificationData cartModification = followIntegration ? bcfTravelCartFacade.addToCart(addProductToCartRequestData)
				: bcfTravelCartFacade.addToCart(addToCartForm.getProductCode(), addToCartForm.getQty());
		bcfTravelCartFacade.setOrderEntryType(de.hybris.platform.travelservices.enums.OrderEntryType.TRANSPORT,
				cartModification.getEntry().getEntryNumber());

		travelCommercePriceFacade.addPropertyPriceLevelToCartEntry(priceLevel, cartModification.getEntry().getProduct().getCode(),
				cartModification.getEntry().getEntryNumber());
		if (cartModification.getQuantityAdded() >= PRODUCT_QUANTITY)
		{

			final CartEntryPropertiesData cartEntryPropertiesData = new CartEntryPropertiesData();
			cartEntryPropertiesData.setTravellerCode(travellerCode);
			cartEntryPropertiesData.setOriginDestinationRefNo(addToCartForm.getOriginDestinationRefNumber());
			cartEntryPropertiesData.setTravelRoute(travelRouteCode);
			cartEntryPropertiesData.setTransportOfferingCodes(addToCartForm.getTransportOfferingCodes());
			cartEntryPropertiesData.setActive(Boolean.TRUE);
			cartEntryPropertiesData.setAmendStatus(AmendStatus.NEW);
			cartEntryPropertiesData.setCartModification(cartModification);
			cartEntryPropertiesData.setJourneyRefNumber(addToCartForm.getJourneyRefNumber());

			addPropertiesToCartEntry(cartEntryPropertiesData);


			bcfTravelCartFacadeHelper.addFeesProductToCart(cartModification.getMakeBookingResponse(),
					addToCartForm.getJourneyRefNumber(), addToCartForm.getOriginDestinationRefNumber());

			if (BookingJourneyType.BOOKING_PACKAGE.getCode()
					.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{
				bcfTravelCartFacade.attachDealOrderEntryGroup(addToCartForm.getJourneyRefNumber(), Arrays.asList(cartModification));
			}

			// Cart recalculation is needed after persisting pricelevel, traveller details,
			// travel route and transport offering codes against cart entry
			// to make "pricing search criteria in context" logic work. Reason is that, OOTB
			// cart calculation gets triggered while creation of a cart entry,
			// and by that time due to missing travel specific details (pricelevel,
			// traveller details, travel route and transport offering codes), incorrect
			// cart calculation results are produced.
			bcfTravelCartFacade.recalculateCart();
		}
		else if (cartModification.getQuantityAdded() == 0L)
		{
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, NO_ITEMS_ADDED + cartModification.getStatusCode(), null, null));
		}
	}

	private AddProductToCartRequestData createAddProductToCartRequestData(final AddToCartForm addToCartForm)
	{
		final AddProductToCartRequestData addProductToCartRequestData = new AddProductToCartRequestData();
		addProductToCartRequestData.setOriginDestinationRefNumber(addToCartForm.getOriginDestinationRefNumber());
		addProductToCartRequestData.setProductCode(addToCartForm.getProductCode());
		addProductToCartRequestData.setQty(addToCartForm.getQty());
		addProductToCartRequestData.setJourneyRefNumber(addToCartForm.getJourneyRefNumber());
		addProductToCartRequestData.setProductType(AncillaryProductModel._TYPECODE);
		return addProductToCartRequestData;
	}

	@RequestMapping(value = "/cart/upgradeBundle", method = RequestMethod.POST)
	public String upgradeBundleInCart(@ModelAttribute("addBundleToCartForm") final AddBundleToCartForm addBundleToCartForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel) throws
			CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException
	{
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			return getUpgradeBundleErrorMessage(ADD_BUNDLE_TO_CART_REQUEST_ERROR, redirectModel);
		}

		// populates the addBundleToCartRequestData
		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		if (Objects.isNull(addBundleToCartRequestData))
		{
			return getUpgradeBundleErrorMessage(ADD_BUNDLE_TO_CART_REQUEST_ERROR, redirectModel);
		}

		// validation
		for (final AddBundleToCartValidationStrategy strategy : addBundleToCartValidationStrategyList)
		{
			final AddToCartResponseData response = strategy.validate(addBundleToCartRequestData);
			if (!response.isValid())
			{
				return getUpgradeBundleErrorMessage(ADD_BUNDLE_TO_CART_REQUEST_ERROR, redirectModel);
			}
		}
		// Remove cart of entries with Origin Destination Ref Num
		final Integer odRefNum = addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber();
		bcfTravelCartFacade.removeEntriesForOriginDestinationRefNumber(odRefNum);

		// perform add to cart
		final String addBundleToCartErrorMessage = bcfTravelCartFacadeHelper.addBundleToCart(addBundleToCartRequestData,
				addBundleToCartForm.isDynamicPackage(), true);
		if (StringUtils.isNotEmpty(addBundleToCartErrorMessage))
		{
			return getUpgradeBundleErrorMessage(addBundleToCartErrorMessage, redirectModel);
		}
		else
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(true, null, odRefNum + 1, null));
		}

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		return REDIRECT_PREFIX
				+ (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.BOOKING_PACKAGE, sessionBookingJourney)
				? BcfstorefrontaddonWebConstants.ANCILLARY_EXTRAS_PATH
				: BcfvoyageaddonWebConstants.ANCILLARY_ROOT_URL);
	}

	protected String getUpgradeBundleErrorMessage(final String errorMsg, final RedirectAttributes redirectModel)
	{
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, errorMsg);

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		return REDIRECT_PREFIX
				+ (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.BOOKING_PACKAGE, sessionBookingJourney)
				? BcfstorefrontaddonWebConstants.ANCILLARY_EXTRAS_PATH
				: BcfvoyageaddonWebConstants.ANCILLARY_ROOT_URL);
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(BcfCoreConstants.ADDTOCART_DATE_PATTERN);
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

	/**
	 * Performs the addBundleToCart for the given addBundleToCartForm and returns the name of the jsp that builds the
	 * JSON response.
	 *
	 * @param addBundleToCartForm the addBundleToCartForm
	 * @param model               the model
	 * @return String the name of the jsp that builds the JSON response.
	 */
	@RequestMapping(value = "/cart/addBundle", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String addToCartBundle(@ModelAttribute("addBundleToCartForm") final AddBundleToCartForm addBundleToCartForm,
			final Model model, final BindingResult bindingResult)

	{

		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		if (validateAddToCartBundle(addBundleToCartRequestData, model, bindingResult))
		{
			if (bcfTravelCartFacadeHelper.isAlacateFlow())
			{
				sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
						BookingJourneyType.BOOKING_ALACARTE.getCode());
			}
			else
			{
				sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
						BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode());
			}
			final Transaction currentTransaction = Transaction.current();
			boolean executed = false;
			try
			{

				currentTransaction.begin();

				final String response = performAddToCartBundle(addBundleToCartRequestData, addBundleToCartForm.isDynamicPackage(),
						model);
				executed = true;
				return response;

			}
			catch (final IntegrationException e)
			{
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, bcfTravelCartFacadeHelper
										.getErrorMessage(MAKE_BOOKING_ERROR, e.getErorrListDTO().getErrorDto().get(0).getErrorCode(),
												e.getErorrListDTO().getErrorDto().get(0).getErrorDetail()),
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;

			}
			catch (final CommerceBundleCartModificationException ex)
			{
				LOG.info(ex.getMessage(), ex);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, ex.getMessage(),
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}
			catch (final UnknownIdentifierException ex)
			{
				LOG.info(ex.getMessage(), ex);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}
			catch (final Exception e)
			{
				LOG.error(e.getMessage(), e);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}
			finally
			{
				if (executed)
				{
					currentTransaction.commit();
				}
				else
				{
					currentTransaction.rollback();
				}
			}
		}
		else
		{
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
	}

	@RequestMapping(value = "/cart/performAddBundle", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String performAddBundleToCart(@ModelAttribute("addBundleToCartForm") final AddBundleToCartForm addBundleToCartForm,
			final Model model)
			throws CommerceCartModificationException, IntegrationException, CommerceBundleCartModificationException
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		return performAddToCartBundle(addBundleToCartRequestData, addBundleToCartForm.isDynamicPackage(), model);
	}

	private String performAddToCartBundle(final AddBundleToCartRequestData addBundleToCartRequestData,
			final boolean isDynamicPackage, final Model model)
			throws CommerceCartModificationException, IntegrationException, CommerceBundleCartModificationException
	{

		final String addBundleToCartErrorMessage = bcfTravelCartFacadeHelper
				.addBundleToCart(addBundleToCartRequestData, isDynamicPackage, true);
	 
		if (StringUtils.isNotEmpty(addBundleToCartErrorMessage))
		{
			throw new CommerceBundleCartModificationException(addBundleToCartErrorMessage);
		}
		else
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null,
					addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber() + 1, null));
			final int selectedJourneyRefNumber = ((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData()
					.stream().findFirst().get()).getJourneyRefNumber();
			sessionService.setAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO, selectedJourneyRefNumber);
		}

		return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
	}


	/**
	 * Performs the addTransportBundleToCart for the given addBundleToCartForm and returns the name of the jsp that
	 * builds the JSON response.
	 *
	 * @param addBundleToCartForm the addBundleToCartForm
	 * @param model               the model
	 * @return String the name of the jsp that builds the JSON response.
	 */
	@RequestMapping(value = "/cart/add-transport-bundle", method = RequestMethod.POST, produces = "application/json")
	public String addTransportBundleToCart(final AddBundleToCartForm addBundleToCartForm, final Model model)
			throws CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException
	{
		sessionService.removeAttribute(BcfCoreConstants.IS_EBOOKING_INTEGRATION_JOURNEY);
		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		if (Objects.isNull(addBundleToCartRequestData))
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade
							.createAddToCartResponse(false, ADD_BUNDLE_TO_CART_REQUEST_ERROR, null, null));
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}


		final Transaction currentTransaction = Transaction.current();
		boolean executed = false;
		try
		{
			currentTransaction.begin();

			final String addBundleToCartErrorMessage = bcfTravelCartFacadeHelper.addBundleToCart(addBundleToCartRequestData,
					addBundleToCartForm.isDynamicPackage(), true);

			bcfTravelCartFacadeHelper
					.manageOriginDestTravellerSessionMap(Integer.valueOf(addBundleToCartForm.getOriginDestinationRefNumber()));

			if (StringUtils.isNotEmpty(addBundleToCartErrorMessage))
			{
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED, null, null));
			}
			else
			{
				sessionService.setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, addBundleToCartForm.getJourneyRefNum());
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null, null, null));
			}

			executed = true;

		}
		catch (final IntegrationException ex)
		{
			LOG.info(ex.getMessage(), ex);
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ex.getMessage(),
							addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
		catch (final CommerceBundleCartModificationException | UnknownIdentifierException ex)
		{
			LOG.info(ex.getMessage(), ex);

			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
							addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
							addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
		finally
		{
			if (executed)
			{
				currentTransaction.commit();
			}
			else
			{
				currentTransaction.rollback();
			}
		}



		return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
	}



	/**
	 * Removes all cart entries for specific origin destination ref number and adds a newly selected bundle to cart for
	 * the same origin destination.
	 *
	 * @param addBundleToCartForm the addBundleToCartForm
	 * @param model               the model
	 * @return String the name of the jsp that builds the JSON response.
	 */
	@RequestMapping(value = "/cart/package-change-transport", method = RequestMethod.POST, produces = "application/json")
	public String changeTransportBundle(final AddBundleToCartForm addBundleToCartForm, final Model model)
			throws CommerceBundleCartModificationException, IntegrationException, CommerceCartModificationException
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		if (Objects.isNull(addBundleToCartRequestData))
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade
							.createAddToCartResponse(false, ADD_BUNDLE_TO_CART_REQUEST_ERROR, null, null));
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}

		// validation
		for (final AddBundleToCartValidationStrategy strategy : addPackageBundleToCartValidationStrategyList)
		{
			final AddToCartResponseData response = strategy.validate(addBundleToCartRequestData);
			if (!response.isValid())
			{
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, response);
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}
		}

		bcfTravelCartFacade.removeEntriesForOriginDestinationRefNumber(
				addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber());

		final String addBundleToCartErrorMessage = bcfTravelCartFacadeHelper.addBundleToCart(addBundleToCartRequestData,
				addBundleToCartForm.isDynamicPackage(), true);
		if (StringUtils.isNotEmpty(addBundleToCartErrorMessage))
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED, null, null));
			bcfTravelCartFacade.removeSessionCart();
		}
		else
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null, null, null));
		}

		return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
	}

	/**
	 * @param accommodationNo
	 * @param previousSelectedAccommodation
	 * @param transportOfferingCode
	 * @param travellerCode
	 * @param originDestinationRefNo
	 * @param travelRoute
	 * @param model
	 * @return String
	 */
	@RequestMapping(value =
			{ "/cart/add/accommodation", "/manage-booking/ancillary/cart/add/accommodation" }, method =
			{ RequestMethod.POST, RequestMethod.GET }, produces = "application/json")
	public String addSelectedAccommodationToCart(@RequestParam("accommodationNo") final String accommodationNo,
			@RequestParam("previousSelectedAccommodation") final String previousSelectedAccommodation,
			@RequestParam("transportOfferingCode") final String transportOfferingCode,
			@RequestParam("travellerCode") final String travellerCode,
			@RequestParam("originDestinationRefNo") final String originDestinationRefNo,
			@RequestParam("travelRoute") final String travelRoute,
			@RequestParam("accommodationMapCode") final String accommodationMapCode, final Model model)
	{
		if (getAddToCartResponse(accommodationNo, previousSelectedAccommodation, transportOfferingCode, travellerCode,
				originDestinationRefNo,
				travelRoute, model))
			return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;

		final String accomodationUid = String.join("-", accommodationMapCode, accommodationNo);
		final ConfiguredAccommodationData accommodation = configuredAccommodationFacade.getAccommodation(accomodationUid);
		if (accommodation == null)
		{
			LOG.error("Selected Accommodation : " + accommodationNo + " not found");
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ADD_TO_CART_ERROR, null, null));
			return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
		}
		if (!accommodation.getBookable())
		{
			LOG.error("Selected Accommodation is not bookable");
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ADD_TO_CART_ERROR, null, null));
			return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
		}
		final ProductData associatedProduct = accommodation.getProduct();
		final List<String> transportOfferingCodes = new ArrayList<>();
		transportOfferingCodes.add(transportOfferingCode);
		final String productCode = associatedProduct.getCode();
		if (addAccommodationToCartStrategy.canAddAccommodationToCart(associatedProduct))
		{
			final OrderEntryData existingOrderEntry = bcfTravelCartFacade.getOrderEntry(productCode, travelRoute,
					transportOfferingCodes, travellerCode, false);
			if (existingOrderEntry == null)
			{
				final long quantity = 1;
				if (getPage(travellerCode, originDestinationRefNo, travelRoute, model, transportOfferingCodes, productCode, quantity))
					return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
			}
			else if (existingOrderEntry.getQuantity() == 0L)
			{
				updateCartEntryForQuantityZero(existingOrderEntry);
			}
		}
		// check if an entry exist for previous product, i.e. user first selected an
		// economy accommodation and
		// now selected business accommodation, then entry with economy accommodation
		// need to be removed(only IF it does not belong to a bundle)
		if (StringUtils.isNotBlank(previousSelectedAccommodation))
		{
			final String previousSelectedAccommodationUid = String.join("-", accommodationMapCode, previousSelectedAccommodation);
			final ConfiguredAccommodationData previousAccommodation = configuredAccommodationFacade
					.getAccommodation(previousSelectedAccommodationUid);
			final ProductData previousAssociatedProduct = previousAccommodation.getProduct();
			if ((associatedProduct == null && previousAssociatedProduct != null)//NOSONAR
					|| !associatedProduct.getCode().equals(previousAssociatedProduct.getCode()))
			{
				final OrderEntryData previousOrderEntry = bcfTravelCartFacade.getOrderEntry(previousAssociatedProduct.getCode(),
						travelRoute, transportOfferingCodes, travellerCode, false);
				// remove cart entry only if it does not belong to a bundle
				updateCartEntry(previousOrderEntry);
			}
		}
		bcfTravelCartFacade.addSelectedAccommodationToCart(transportOfferingCode, travellerCode, accomodationUid);

		model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null, null, null));
		return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
	}

	private boolean getAddToCartResponse(
			final String accommodationNo,
			final String previousSelectedAccommodation,
			final String transportOfferingCode,
			final String travellerCode,
			final String originDestinationRefNo,
			final String travelRoute, final Model model)
	{
		for (final SelectedAccommodationStrategy strategy : selectedAccommodationStrategyList)
		{
			final AddToCartResponseData response = strategy.validateSelectedAccommodation(accommodationNo,
					previousSelectedAccommodation, transportOfferingCode, travellerCode, originDestinationRefNo, travelRoute);

			if (!response.isValid())
			{
				model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE, response);
				return true;
			}
		}
		return false;
	}

	private boolean getPage(final String travellerCode,
			final String originDestinationRefNo,
			final String travelRoute, final Model model,
			final List<String> transportOfferingCodes, final String productCode, final long quantity)
	{
		try
		{
			final PriceLevel priceLevel = travelCommercePriceFacade.getPriceLevelInfo(productCode, transportOfferingCodes,
					travelRoute);
			if (priceLevel == null)
			{
				model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED, null, null));
				return true;
			}
			travelCommercePriceFacade.setPriceAndTaxSearchCriteriaInContext(priceLevel, transportOfferingCodes,
					travellerFacade.getTravellerFromCurrentCart(travellerCode));

			final CartModificationData cartModification = bcfTravelCartFacade.addToCart(productCode, quantity);
			bcfTravelCartFacade.setOrderEntryType(OrderEntryType.TRANSPORT, cartModification.getEntry().getEntryNumber());

			travelCommercePriceFacade.addPropertyPriceLevelToCartEntry(priceLevel,
					cartModification.getEntry().getProduct().getCode(), cartModification.getEntry().getEntryNumber());

			if (populateCartEntryPropertiesData(travellerCode, originDestinationRefNo, travelRoute, model,
					transportOfferingCodes,
					cartModification))
				return true;

		}
		catch (final CommerceCartModificationException ex)
		{
			LOG.info(ex.getMessage(), ex);
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ADD_TO_CART_ERROR, null, null));
			return true;
		}
		return false;
	}

	private void updateCartEntry(final OrderEntryData previousOrderEntry)
	{
		if (previousOrderEntry != null && previousOrderEntry.getBundleNo() == 0)
		{
			try
			{
				bcfTravelCartFacade.updateCartEntry(previousOrderEntry.getEntryNumber(), 0);
			}
			catch (final CommerceCartModificationException e)
			{
				LOG.error("Error removing cart entry for product other than current selected accommodation", e);
			}
		}
	}

	private void updateCartEntryForQuantityZero(final OrderEntryData existingOrderEntry)
	{
		try
		{
			final long newQuantity = 1L;
			final CartModificationData cartModification = bcfTravelCartFacade
					.updateCartEntry(existingOrderEntry.getEntryNumber(), newQuantity);

			if (cartModification.getQuantityAdded() == newQuantity)
			{
				LOG.info("Product Code:" + cartModification.getEntry().getProduct().getCode() + "has been update with Quantity:"
						+ cartModification.getQuantityAdded());
			}
		}
		catch (final CommerceCartModificationException ex)
		{
			LOG.warn("Couldn't update product with the entry number: " + existingOrderEntry.getEntryNumber() + ".", ex);
		}
	}

	private boolean populateCartEntryPropertiesData(
			@RequestParam("travellerCode") final String travellerCode,
			@RequestParam("originDestinationRefNo") final String originDestinationRefNo,
			@RequestParam("travelRoute") final String travelRoute, final Model model,
			final List<String> transportOfferingCodes, final CartModificationData cartModification)
	{
		if (cartModification.getQuantityAdded() >= PRODUCT_QUANTITY)
		{
			final CartEntryPropertiesData addPropertiesToCartEntryData = new CartEntryPropertiesData();
			addPropertiesToCartEntryData.setTravellerCode(travellerCode);
			addPropertiesToCartEntryData.setOriginDestinationRefNo(Integer.parseInt(originDestinationRefNo));
			addPropertiesToCartEntryData.setTravelRoute(travelRoute);
			addPropertiesToCartEntryData.setTransportOfferingCodes(transportOfferingCodes);
			addPropertiesToCartEntryData.setActive(Boolean.TRUE);
			addPropertiesToCartEntryData.setAmendStatus(AmendStatus.NEW);
			addPropertiesToCartEntryData.setCartModification(cartModification);


			addPropertiesToCartEntry(addPropertiesToCartEntryData);
			// Cart recalculation is needed after persisting pricelevel, traveller details,
			// travel route and transport offering codes against cart entry
			// to make "pricing search criteria in context" logic work. Reason is that, OOTB
			// cart calculation gets triggered while creation of a cart entry,
			// and by that time due to missing travel specific details (pricelevel,
			// traveller details, travel route and transport offering codes), incorrect
			// cart calculation results are produced.
			bcfTravelCartFacade.recalculateCart();
		}
		else if (cartModification.getQuantityAdded() == 0L)
		{
			model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ADD_TO_CART_ERROR, null, null));
			return true;
		}
		return false;
	}

	private void addPropertiesToCartEntry(final CartEntryPropertiesData cartEntryPropertiesData)
	{
		final String productCode = cartEntryPropertiesData.getCartModification().getEntry().getProduct().getCode();
		final String addToCartCriteria = travelRestrictionFacade.getAddToCartCriteria(productCode);

		cartEntryPropertiesData.setProductCode(productCode);
		cartEntryPropertiesData.setAddToCartCriteria(addToCartCriteria);
		cartEntryPropertiesData.setEntryNumber(cartEntryPropertiesData.getCartModification().getEntry().getEntryNumber());

		bcfTravelCartFacade.addPropertiesToCartEntry(cartEntryPropertiesData);
	}

	/**
	 * @param accommodationNo
	 * @param transportOfferingCode
	 * @param travellerCode
	 * @param travelRoute
	 * @param model
	 * @return String
	 */
	@RequestMapping(value =
			{ "/cart/remove/accommodation", "/manage-booking/ancillary/cart/remove/accommodation" }, method =
			{ RequestMethod.POST, RequestMethod.GET }, produces = "application/json")
	public String removeSelectedAccommodationFromCart(@RequestParam("accommodationNo") final String accommodationNo,
			@RequestParam("transportOfferingCode") final String transportOfferingCode,
			@RequestParam("travellerCode") final String travellerCode, @RequestParam("travelRoute") final String travelRoute,
			@RequestParam("accommodationMapCode") final String accommodationMapCode, final Model model)
	{

		if (getAddToCartResponse(accommodationNo, null, transportOfferingCode, travellerCode, null, travelRoute, model))
			return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
		final String accomodationUid = String.join("-", accommodationMapCode, accommodationNo);
		final ConfiguredAccommodationData accommodation = configuredAccommodationFacade.getAccommodation(accomodationUid);
		final ProductData associatedProduct = accommodation.getProduct();
		if (associatedProduct != null)
		{
			final List<String> transportOfferingCodes = new ArrayList<>();
			transportOfferingCodes.add(transportOfferingCode);
			final String productCode = associatedProduct.getCode();
			final OrderEntryData existingOrderEntry = bcfTravelCartFacade.getOrderEntry(productCode, travelRoute,
					transportOfferingCodes, travellerCode, false);
			if (existingOrderEntry != null)
			{
				final int bundleNo = existingOrderEntry.getBundleNo();
				// remove cart entry only if it does not belong to a bundle
				if (bundleNo == 0)
				{
					try
					{
						bcfTravelCartFacade.updateCartEntry(existingOrderEntry.getEntryNumber(), 0);
					}
					catch (final CommerceCartModificationException ex)
					{
						LOG.info(ex.getMessage(), ex);
						model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE,
								bcfTravelCartFacade.createAddToCartResponse(false, REMOVE_FROM_CART_ERROR, null, null));
						return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;
					}
				}
			}
		}
		bcfTravelCartFacade.removeSelectedAccommodationFromCart(transportOfferingCode, travellerCode, accommodationNo);
		model.addAttribute(ADD_PRODUCT_TO_CART_RESPONSE, bcfTravelCartFacade.createAddToCartResponse(true, null, null, null));
		return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.AddProductToCartResponse;

	}

	/**
	 * Method to add voucher to cart
	 *
	 * @param voucherCode
	 * @param model
	 * @return json page
	 */
	@RequestMapping(value = "/cart/voucher/redeem", method =
			{ RequestMethod.POST })
	public String redeemVoucher(@RequestParam("voucherCode") final String voucherCode, final Model model)
	{
		try
		{
			bcfTravelCartFacade.applyVoucher(voucherCode);
			model.addAttribute(VOUCHER_REDEEM_SUCCESS, true);
		}
		catch (final VoucherOperationException e)
		{
			model.addAttribute(ERROR_MSG_TYPE, e.getMessage());
		}
		return BcfvoyageaddonControllerConstants.Views.Pages.Cart.VoucherJSONResponse;
	}

	/**
	 * Method to remove voucher from cart
	 *
	 * @param voucherCode
	 * @param model
	 * @return json page
	 */
	@RequestMapping(value = "/cart/voucher/release", method =
			{ RequestMethod.POST })
	public String releaseVoucher(@RequestParam("voucherCode") final String voucherCode, final Model model)
	{
		try
		{
			bcfTravelCartFacade.removeVoucher(voucherCode);
			model.addAttribute(VOUCHER_RELEASE_SUCCESS, true);
		}
		catch (final VoucherOperationException e)
		{
			model.addAttribute(ERROR_MSG_TYPE, e.getMessage());
		}
		return BcfvoyageaddonControllerConstants.Views.Pages.Cart.VoucherJSONResponse;
	}

	@RequestMapping(value = "/cart/updateBundle", method = RequestMethod.POST)
	public String updateBundleInCart(@ModelAttribute("addBundleToCartForm") final AddBundleToCartForm addBundleToCartForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
	{

		// populates the addBundleToCartRequestData
		final AddBundleToCartRequestData addBundleToCartRequestData = getAddBundleToCartRequestData(addBundleToCartForm);
		if (validateAddToCartBundle(addBundleToCartRequestData, model, bindingResult))
		{
			final CartModel cartModel = cartService.getSessionCart();
			List<DealOrderEntryGroupModel> dealOrderEntryGroups = null;
			if (cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries()))
			{
				dealOrderEntryGroups = bcfTravelCartFacade.getDealOrderEntryGroups(cartModel);
			}

			final List<AbstractOrderEntryModel> entries = bcfTravelCartFacade
					.getCartEntriesForRefNo(addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							addBundleToCartRequestData.getSelectedOdRefNumber());

			final Transaction currentTransaction = Transaction.current();
			boolean executed = false;
			try
			{

				currentTransaction.begin();

				/*Remove existing entries from cart*/
				if (CollectionUtils.isNotEmpty(entries) && !bcfTravelCartFacade.isAmendmentCart())
				{
					bcfTravelCartFacade.removeCartEntriesForRefNo(
							addBundleToCartRequestData.getSelectedJourneyRefNumber(),
							addBundleToCartRequestData.getSelectedOdRefNumber(),
							true, false);
				}

				performAddToCartBundle(addBundleToCartRequestData, addBundleToCartForm.isDynamicPackage(), model);

				if (Objects.nonNull(dealOrderEntryGroups))
				{
					bcfTravelCartFacade.updateCartWithDealOrderEntryGroups(dealOrderEntryGroups, cartModel);
				}
				bcfTravelCartFacade.recalculateCart();

				bcfTravelCartFacadeHelper.calculateChangeFees();
				executed = true;

			}
			catch (final IntegrationException e)
			{
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, bcfTravelCartFacadeHelper
										.getErrorMessage(MAKE_BOOKING_ERROR, e.getErorrListDTO().getErrorDto().get(0).getErrorCode(),
												e.getErorrListDTO().getErrorDto().get(0).getErrorDetail()),
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;

			}
			catch (final UnknownIdentifierException ex)
			{
				LOG.info(ex.getMessage(), ex);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}

			catch (final CommerceBundleCartModificationException ex)
			{
				LOG.info(ex.getMessage(), ex);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, ex.getMessage(),
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
			}
			catch (final Exception e)
			{
				LOG.error(e.getMessage(), e);
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
						bcfTravelCartFacade.createAddToCartResponse(false, BASKET_ERROR_OCCURRED,
								addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber(), null));
				return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;

			}
			finally
			{
				if (executed)
				{
					currentTransaction.commit();
				}
				else
				{
					currentTransaction.rollback();
				}
			}

			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
		else
		{
			return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.AddBundleToCartResponse;
		}
	}

	/**
	 * Returns the AddBundleToCartRequestData built from the AddBundleToCartForm.
	 *
	 * @param addBundleToCartForm as the addBundleToCartForm
	 * @return the AddBundleToCartRequestData
	 */
	private AddBundleToCartRequestData getAddBundleToCartRequestData(final AddBundleToCartForm addBundleToCartForm)
	{
		final AddBundleToCartRequestData addBundleToCartRequestData = new AddBundleToCartRequestData();
		addBundleToCartRequestData.setTripType(getTripType(addBundleToCartForm));
		addBundleToCartRequestData.setPassengerTypes(addBundleToCartForm.getPassengerTypeQuantityList());
		addBundleToCartRequestData.setAccessibilityDatas(addBundleToCartForm.getAccessibilitytDataList());
		addBundleToCartRequestData.setVehicleTypes(addBundleToCartForm.getVehicleTypeQuantityList());
		StreamUtil.safeStream(addBundleToCartForm.getVehicleTypeQuantityList()).forEach(vehicleTypeQuantityData -> {
			if (vehicleTypeQuantityData.isCarryingLivestock() && (BcfCoreConstants.UNDER_HEIGHT
					.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode()) || BcfCoreConstants.OVERSIZE
					.equalsIgnoreCase(vehicleTypeQuantityData.getVehicleType().getCode())))
			{
				vehicleTypeQuantityData.getVehicleType().setCode(
						vehicleTypeQuantityData.getVehicleType().getCode() + BcfintegrationserviceConstants.CARRYING_LIVESTOCK_SUFFIX);
			}
		});
		addBundleToCartRequestData.setReturnWithDifferentPassenger(addBundleToCartForm.isReturnWithDifferentPassenger());
		addBundleToCartRequestData.setReturnWithDifferentVehicle(addBundleToCartForm.getReturnWithDifferentVehicle());
		addBundleToCartRequestData.setBundleType(addBundleToCartForm.getItineraryPricingInfo().getBundleType());
		addBundleToCartRequestData.setSpecialServicesData(addBundleToCartForm.getSpecialServiceRequestDataList());
		addBundleToCartRequestData.setLargeItems(addBundleToCartForm.getLargeItems());
		addBundleToCartRequestData.setOpenTicket(addBundleToCartForm.isOpenTicket());

		if (CollectionUtils.isNotEmpty(addBundleToCartForm.getOtherChargeDataList()))
		{
			addBundleToCartRequestData.setOtherChargeDatas(addBundleToCartForm.getOtherChargeDataList());
		}
		if (StringUtils.isNotBlank(addBundleToCartForm.getChangeFareOrReplan()) && StringUtils
				.equalsIgnoreCase("true", addBundleToCartForm.getChangeFareOrReplan()))
		{
			addBundleToCartRequestData.setChangeOrReplan(Boolean.TRUE);
		}
		if (StringUtils.isNotEmpty(addBundleToCartForm.getTransferSailingIdentifier()))
		{
			addBundleToCartRequestData.setTransferSailingIdentifier(addBundleToCartForm.getTransferSailingIdentifier());
		}
		final String travelRouteCode = addBundleToCartForm.getTravelRouteCode();
		final Integer originDestinationRefNumber;
		try
		{
			originDestinationRefNumber = Integer.parseInt(addBundleToCartForm.getOriginDestinationRefNumber());
		}
		catch (final NumberFormatException ex)
		{
			LOG.error("Cannot parse Origin Destination Ref Number to Integer", ex);
			return null;
		}
		final ItineraryPricingInfoData itineraryPricingInfoData = addBundleToCartForm.getItineraryPricingInfo();

		final List<AddBundleToCartData> addBundleToCartDatas = new ArrayList<>();
		for (final TravelBundleTemplateData bundleData : itineraryPricingInfoData.getBundleTemplates())
		{
			final BcfAddBundleToCartData addBundleToCart = new BcfAddBundleToCartData();
			bcfDealCartFacade
					.populateAddBundleToCart(bundleData, addBundleToCart, travelRouteCode, addBundleToCartForm.isDynamicPackage());
			addBundleToCart.setBundleTemplateId(bundleData.getFareProductBundleTemplateId());
			addBundleToCart.setOriginDestinationRefNumber(originDestinationRefNumber);
			addBundleToCart.setTravelRouteCode(travelRouteCode);
			addBundleToCart.setJourneyRefNumber(addBundleToCartForm.getJourneyRefNum());
			addBundleToCart.setCarryingDangerousGoodsInOutbound(hasDangerousGoodsOutbound(addBundleToCartForm));
			addBundleToCart.setCarryingDangerousGoodsInReturn(hasDangerousGoodsInbound(addBundleToCartForm));
			addBundleToCartDatas.add(addBundleToCart);
		}

		addBundleToCartRequestData.setAddBundleToCartData(addBundleToCartDatas);

		addBundleToCartRequestData.setSalesApplication(bcfSalesApplicationResolverService.getCurrentSalesChannel());
		if (bcfTravelCartFacade.isAmendmentCart())
		{
			addBundleToCartRequestData.setAddCachingKeys(true);
			addBundleToCartRequestData.setAmendmentCart(true);
		}
		else
		{
			addBundleToCartRequestData.setAddCachingKeys(false);
		}
		if (Objects.equals(BcfFacadesConstants.REPLAN, addBundleToCartForm.getChangeFareOrReplan()))
		{
			addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.REPLAN);
			addBundleToCartRequestData
					.setSelectedJourneyRefNumber(sessionService.getAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO));
			addBundleToCartRequestData
					.setSelectedOdRefNumber(sessionService.getAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM));
		}
		else if (Objects.equals(BcfFacadesConstants.CHANGE_FARE, addBundleToCartForm.getChangeFareOrReplan()))
		{
			addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.CHANGE_FARE);
			addBundleToCartRequestData.setSelectedJourneyRefNumber(addBundleToCartForm.getJourneyRefNum());
			addBundleToCartRequestData.setSelectedOdRefNumber(addBundleToCartForm.getFareTypeRefNo());
		}
		else
		{
			addBundleToCartRequestData.setJourneyType(BcfFacadesConstants.PLAN);
			addBundleToCartRequestData.setSelectedJourneyRefNumber(addBundleToCartForm.getJourneyRefNum());
			addBundleToCartRequestData.setSelectedOdRefNumber(
					addBundleToCartRequestData.getAddBundleToCartData().get(0).getOriginDestinationRefNumber());
		}
		addBundleToCartRequestData.setHoldTimeInSeconds(
				bcfTravelCartFacadeHelper.getHoldTimeInSeconds(addBundleToCartRequestData.getSalesApplication()));
		addBundleToCartRequestData.setPassengerUidsToBeRemoved(addBundleToCartForm.getPassengerUidsToBeRemoved());
		bcfTravelCartFacadeHelper.updateVehicleCode(addBundleToCartForm.getTravelRouteCode(), addBundleToCartRequestData);
		bcfTravelCartFacadeHelper.setFerryOptionBooking(addBundleToCartRequestData);
		return addBundleToCartRequestData;
	}



	private boolean hasDangerousGoodsOutbound(final AddBundleToCartForm addBundleToCartForm)
	{
		if (addBundleToCartForm.isCarryingDangerousGoodsInOutbound())
		{
			return true;
		}
		if (CollectionUtils.isNotEmpty(addBundleToCartForm.getVehicleTypeQuantityList()))
		{
			return StreamUtil.safeStream(addBundleToCartForm.getVehicleTypeQuantityList()).findFirst().get()
					.isCarryingDangerousGoods();
		}
		return false;
	}

	private boolean hasDangerousGoodsInbound(final AddBundleToCartForm addBundleToCartForm)
	{
		if (StringUtils.equalsIgnoreCase(TripType.RETURN.name(), addBundleToCartForm.getTripType()))
		{
			if (addBundleToCartForm.isCarryingDangerousGoodsInReturn())
			{
				return true;
			}
			if (CollectionUtils.isNotEmpty(addBundleToCartForm.getVehicleTypeQuantityList()) && !addBundleToCartForm
					.getReturnWithDifferentVehicle())
			{
				return StreamUtil.safeStream(addBundleToCartForm.getVehicleTypeQuantityList()).findFirst().get()
						.isCarryingDangerousGoods();
			}
			if (CollectionUtils.isNotEmpty(addBundleToCartForm.getVehicleTypeQuantityList()) && addBundleToCartForm
					.getReturnWithDifferentVehicle())
			{
				return StreamUtil.safeStream(addBundleToCartForm.getVehicleTypeQuantityList()).reduce((a, b) -> b).get()
						.isCarryingDangerousGoods();
			}
		}
		return false;
	}

	private TripType getTripType(final AddBundleToCartForm addBundleToCartForm)
	{
		if (StringUtils.equalsIgnoreCase(TripType.SINGLE.name(), addBundleToCartForm.getTripType()))
		{
			return TripType.SINGLE;
		}
		else if (StringUtils.equalsIgnoreCase(TripType.RETURN.name(), addBundleToCartForm.getTripType()))
		{
			return TripType.RETURN;
		}
		return null;
	}

	private boolean validateAddToCartBundle(final AddBundleToCartRequestData addBundleToCartRequestData, final Model model,
			final BindingResult bindingResult)
	{

		if (addBundleToCartRequestData.isOpenTicket() || configurationService.getConfiguration()
				.getBoolean("cart.skipBundleValidation", false))
		{
			return true;
		}
		if (bindingResult.hasErrors())
		{
			final List<String> errorCodes = bindingResult.getFieldErrors().stream().map(FieldError::getCode)
					.collect(Collectors.toList());

			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, createAddToCartResponseForVehicleValidation(false, errorCodes, null));
			return false;
		}
		// populates the addBundleToCartRequestDatafinal BindingResult

		if (Objects.isNull(addBundleToCartRequestData))
		{
			model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE,
					bcfTravelCartFacade.createAddToCartResponse(false, ADD_BUNDLE_TO_CART_REQUEST_ERROR, null, null));
			return false;
		}

		// validation
		for (final AddBundleToCartValidationStrategy strategy : addBundleToCartValidationStrategyList)
		{
			final AddToCartResponseData response = strategy.validate(addBundleToCartRequestData);
			if (!response.isValid())
			{
				model.addAttribute(ADD_BUNDLE_TO_CART_RESPONSE, response);
				return false;
			}
		}
		return true;
	}

	private AddToCartResponseData createAddToCartResponseForVehicleValidation(final boolean valid,
			final List<String> errorMessageList, final Integer minOriginDestinationRefNumber)
	{
		final AddToCartResponseData response = new AddToCartResponseData();
		response.setValid(valid);
		if (CollectionUtils.isNotEmpty(errorMessageList))
		{
			response.setErrors(errorMessageList);
		}
		if (minOriginDestinationRefNumber != null)
		{
			response.setMinOriginDestinationRefNumber(minOriginDestinationRefNumber);
		}
		return response;
	}

	@RequestMapping(value = "/cart/remove-entry/{journeyRef}/{odRef:.*}", method = RequestMethod.GET)
	public String removeAndCancelEntry(final Model model,
			@PathVariable("journeyRef") final int journeyRef, @PathVariable("odRef") final int odRef,
			final RedirectAttributes redirectModel, final HttpServletRequest request)
	{
		try
		{
			final List<RemoveSailingResponseData> removeSailingResponseDatas = bcfTravelCartFacade
					.modifyCartEntriesForRefNo(journeyRef, odRef,
							bcfTravelCartFacade.getTripType(journeyRef).equals(de.hybris.platform.travelservices.enums.TripType.SINGLE));
			if (CollectionUtils.isNotEmpty(removeSailingResponseDatas))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_SAILING_FAILED);
			}
		}
		catch (final CalculationException | IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_SAILING_FAILED);
		}
		if (!bcfTravelCartFacade.isCurrentCartValid())
		{
			WebUtils.setSessionAttribute(request, BcfFacadesConstants.BCF_FARE_FINDER_FORM, null);
			sessionService.removeAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		}
		return REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	}
}
