/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


@Controller
@RequestMapping("/ferryTracking")
public class CurrentConditionsFerryTrackingPageController extends CurrentConditionsBaseController
{
	private static final String SHOW_COUNT = "showCount";
	private static final String DEPARTING_TERMINALS = "departingTerminals";
	private static final String CURRENT_CONDITIONS_ROUTE_DATA = "currentConditionsRouteData";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getMajorTerminals(@RequestParam(name = "terminalCode", required = false) final String terminalCode,
			final HttpServletRequest request, final Model model)
			throws CMSItemNotFoundException
	{
		model.addAttribute(CURRENT_CONDITIONS_ROUTE_DATA, currentConditionsDetailsFacade.getFerryTrackingLinksByRoutes());

		model.addAttribute(DEPARTING_TERMINALS, currentConditionsDetailsFacade.getDepartingTerminalsForCurrentConditions());
		model.addAttribute("selectedterminalCode", terminalCode);

		final int showCount = Integer.parseInt(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_NEXT_SAILING_SHOW_COUNT));
		model.addAttribute(SHOW_COUNT, showCount);

		return getViewForPage(model, BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_FERRY_TRACKING_CMS_PAGE);
	}
}
