/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/webcams")
public class CurrentConditionsWebcamsPageController extends CurrentConditionsBaseController
{
	private static final Logger LOG = Logger.getLogger(CurrentConditionsWebcamsPageController.class);
	private static final String CURRENT_CONDITIONS_DATA = "currentConditionsData";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getWebcams(@RequestParam(value = "terminalCode", required = false) final String terminal,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{

		try {
			model.addAttribute("sourceTerminals", currentConditionsDetailsFacade.getWebcamsTerminalsForCurrentConditions());
			model.addAttribute(CURRENT_CONDITIONS_DATA, currentConditionsDetailsFacade
					.getDeparturesByTerminals(terminal, BcfFacadesConstants.CURRENT_CONDITIONS_WEBCAM_TERMINALS));
		} catch (final IntegrationException intEx) {
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.cc.notAvailable.label", null);
			LOG.error("Current Conditions Integration Exception occurred while access the services", intEx);
			model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_SERVICE_NOT_AVAILABLE, Boolean.TRUE);
		}

		return getViewForPage(model, BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_WEBCAMS_CMS_PAGE);
	}
}
