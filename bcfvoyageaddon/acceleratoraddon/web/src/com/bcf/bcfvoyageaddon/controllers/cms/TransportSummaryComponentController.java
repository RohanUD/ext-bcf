/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.model.components.TransportSummaryComponentModel;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.reservation.data.ReservationDataList;


@Controller("TransportSummaryComponentController")
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.TransportSummaryComponent)
public class TransportSummaryComponentController extends SubstitutingCMSAddOnComponentController<TransportSummaryComponentModel>
{
	private static final Logger LOG = Logger.getLogger(TransportSummaryComponentController.class);

	@Resource(name = "reservationFacade")
	private BCFReservationFacade reservationFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final TransportSummaryComponentModel component)
	{
		final ReservationDataList reservationDataList = reservationFacade.getCurrentReservations();
		model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
		model.addAttribute(BcfvoyageaddonWebConstants.DATE_FORMAT_LABEL, BcfvoyageaddonWebConstants.DATE_FORMAT);
		model.addAttribute(BcfvoyageaddonWebConstants.TIME_FORMAT_LABEL, BcfvoyageaddonWebConstants.TIME_FORMAT);
	}

	/**
	 * This method is responsible for refreshing the component after an operation is performed on the cart
	 *
	 * @param componentUid
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOG.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}
}
