/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.date.CalendarData;


@Controller
public class SchedulesController extends AbstractController
{

	@Resource(name="transportOfferingFacade")
	private BcfTransportOfferingFacade transportOfferingFacade;

	@Resource(name="configurationService")
	private ConfigurationService configurationService;

	@ResponseBody
	@RequestMapping(value="/getDepartureDates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CalendarData> fetchDepartureDates(@RequestParam("origin") final String origin,
			@RequestParam("destination") final String destination,
			@RequestParam("selectedMonth") final int selectedMonth,
			@RequestParam("selectedYear") final int selectedYear,
			final Model model)
	{
		final LocalDate currentDate = LocalDate.now();
		final int currentMonth = currentDate.getMonthValue();

		Date startingDate = Date.from(currentDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date endingDate;

		if(currentMonth!=selectedMonth)
		{
			final Calendar calendar = Calendar.getInstance();
			calendar.set(selectedYear, selectedMonth - 1 , 1,0,0,0);
			startingDate = calendar.getTime();
		}

		endingDate = TravelDateUtils.addMonths(startingDate, configurationService.getConfiguration().getInt("bcfvoyageaddon.departuredates.limit"));

		final String startDate = TravelDateUtils.getTimeForDate(startingDate, BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN);
		final String endDate = TravelDateUtils.getTimeForDate(endingDate, BcfCoreConstants.SOLR_INDEXER_DATE_PATTERN);

		final OriginDestinationInfoData originDestinationInfoData = createOrigniDestinationInfo(origin, destination);
		return transportOfferingFacade.getDepartureDates(originDestinationInfoData, startDate, endDate);
	}

	protected OriginDestinationInfoData createOrigniDestinationInfo(final String origin, final String destination)
	{
		final OriginDestinationInfoData originDestinationInfoData = new OriginDestinationInfoData();
		originDestinationInfoData.setDepartureLocation(origin);
		originDestinationInfoData.setArrivalLocation(destination);
		originDestinationInfoData.setDepartureLocationType(LocationType.AIRPORTGROUP);
		originDestinationInfoData.setArrivalLocationType(LocationType.AIRPORTGROUP);
		return originDestinationInfoData;
	}
}
