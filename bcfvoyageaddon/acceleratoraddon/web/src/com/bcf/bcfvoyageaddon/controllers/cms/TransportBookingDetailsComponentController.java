/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfvoyageaddon.controllers.cms;

import static com.bcf.bcfstorefrontaddon.controllers.pages.BookingDetailsPageController.GUEST_BOOKING_DETAILS_NOT_FOUND_ERROR;

import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commercefacades.travel.BookingActionRequestData;
import de.hybris.platform.commercefacades.travel.enums.ActionTypeOption;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractBookingDetailsComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractBookingDetailsComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.core.services.vacation.helper.ChangeAndCancellationFeeCalculationHelper;
import com.bcf.facades.assistedservicefacades.impl.DefaultBCFAssistedServiceFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.impl.DefaultBcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.order.AsmOrderViewData;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller("TransportBookingDetailsComponentController")
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.TransportBookingDetailsComponent)
public class TransportBookingDetailsComponentController extends AbstractBookingDetailsComponentController
{
	private static final String RESERVATION_DATA_LIST = "reservationDataList";
	private static final Logger LOG = Logger.getLogger(TransportBookingDetailsComponentController.class);

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	@Resource(name = "assistedServiceFacade")
	DefaultBCFAssistedServiceFacade bcfAssistedServiceFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "userFacade")
	private DefaultBcfUserFacade userFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AbstractBookingDetailsComponentModel component)
	{
		String bookingReference = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);

		if (Objects.isNull(bookingReference))
		{
			bookingReference = request.getParameter(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		}

		if (bcfAssistedServiceFacade.isAssistedServiceAgentLoggedIn() && Objects.nonNull(bookingReference))
		{
			final AbstractOrderModel order = bcfTravelBookingFacade.getOrderFromBookingReferenceCode(bookingReference);
			final AsmOrderViewData asmOrderData = bcfTravelBookingFacade.getASMOrderData(order);
			model.addAttribute("asmOrderData", asmOrderData);
			model.addAttribute("asmLoggedIn", Boolean.TRUE);
			final Boolean northernRoute = ChangeAndCancellationFeeCalculationHelper.isNorthernRoute(order.getEntries());
			model.addAttribute("northernRoute", northernRoute);
			final Map<String, String> userRoles = userFacade.getUserRoles();
			model.addAttribute("userRoles", userRoles);
		}

		final String bcfBookingReferences = getSessionService().getAttribute(BcfstorefrontaddonWebConstants.BCF_BOOKING_REFERENCES);
		if (getConfigurationService().getConfiguration().getBoolean(BcfFacadesConstants.SEARCH_FROM_EBOOKING)
				&& bcfBookingReferences != null)
		{
			try
			{
				final boolean isTransportBookingJourney = getBookingFacade().isTransportBookingJourney(bookingReference);

				if (isTransportBookingJourney)
				{
					final ReservationDataList reservationDataList = searchBookingFacade
							.searchBookings(bookingReference, bcfBookingReferences);
					if (Objects.nonNull(reservationDataList) && CollectionUtils.isNotEmpty(reservationDataList.getReservationDatas())
							&& CollectionUtils.isNotEmpty(reservationDataList.getReservationDatas().get(0).getReservationItems()))
					{
						final Date departureTime = reservationDataList.getReservationDatas().get(0).getReservationItems().get(0)
								.getReservationItinerary().getOriginDestinationOptions().get(0).getTransportOfferings().get(0)
								.getDepartureTime();
						reservationDataList.setFilterCriteria(
								departureTime.before(new Date()) ? BcfFacadesConstants.PAST : BcfFacadesConstants.UPCOMING);
						model.addAttribute(RESERVATION_DATA_LIST, reservationDataList);
					}
					else
					{
						model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, true);
						model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE,
								GUEST_BOOKING_DETAILS_NOT_FOUND_ERROR);
						LOG.info(String.format("No booking found for order %s and booking reference %s", bookingReference,
								bcfBookingReferences));
					}
				}
			}
			catch (final IntegrationException e)
			{
				model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, true);
				model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE,
						e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
				LOG.error(e);
			}
			model.addAttribute("displayTransportBookingDetailsComponent", true);
		}
	}

	@Override
	protected BookingActionRequestData createTransportBookingActionRequest(final String bookingReference)
	{
		final BookingActionRequestData bookingActionRequestData = super.createTransportBookingActionRequest(bookingReference);
		bookingActionRequestData.getRequestActions().add(ActionTypeOption.CANCEL_BOOKING);
		return bookingActionRequestData;
	}

	@Override
	protected String getView(final AbstractBookingDetailsComponentModel component)
	{
		final AssistedServiceSession assistedServiceSession = bcfAssistedServiceFacade.getAsmSession();
		if (Objects.nonNull(assistedServiceSession) && Objects.nonNull(assistedServiceSession.getAgent()))
		{
			return "addon:" + "/" + getAddonUiExtensionName(component) + "/" + getCmsComponentFolder() + "/asm"
					+ getViewResourceName(component);
		}
		else
		{
			return super.getView(component);
		}
	}
}
