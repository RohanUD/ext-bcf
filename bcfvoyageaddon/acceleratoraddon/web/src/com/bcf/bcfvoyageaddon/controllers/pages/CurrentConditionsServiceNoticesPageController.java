/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.ServiceNoticeByRouteData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractSearchPageController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;


@Controller
@RequestMapping("/current-conditions/service-notices")
public class CurrentConditionsServiceNoticesPageController extends BcfAbstractSearchPageController
{
	private static final String SERVICE_NOTICE_DETAILS = "serviceNoticeDetails";
	private static final String PAGINATED_SERVICE_NOTICES = "paginatedServiceNotices";
	public static final String MAX_PAGE_LIMIT = "pagination.service.notice.max.page.limit";
	public static final String SERVICE_NOTICE_PAGE_SIZE = "pagination.service.notice.page.size";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getServiceNotices(@RequestParam(name = "serviceNoticeCode", required = false) final String serviceNoticeCode,
			@RequestParam(name = "routeCode", required = false) final String routeCode,
			@RequestParam(value = "page", defaultValue = "0") final int pageNumber,
			@RequestParam(value = "mode", defaultValue = "Page") final String mode,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{

		final ContentPageModel contentPageModel = getContentPageForLabelOrId(
				BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_SERVICE_NOTICES_CMS_PAGE);

		if (StringUtils.isNotEmpty(serviceNoticeCode) && StringUtils.isNotEmpty(routeCode))
		{
			model.addAttribute(SERVICE_NOTICE_DETAILS,
					currentConditionsDetailsFacade.getServiceNoticesForRoute(serviceNoticeCode, routeCode));
		}
		else
		{
			final SearchPageData<ServiceNoticeByRouteData> searchPageData = currentConditionsDetailsFacade
					.getPaginatedServiceNotices(pageNumber, getPageSize());
			populateModel(model, searchPageData, ShowMode.valueOf(mode));
			model.addAttribute(
					PAGINATED_SERVICE_NOTICES, searchPageData);
		}
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);
	}

	private int getPageSize()
	{
		return getConfigurationService().getConfiguration().getInt(SERVICE_NOTICE_PAGE_SIZE);
	}

	/**
	 * overide to use getMaxSearchPageSize() instead of MAX_PAGE_LIMIT, which is made configurable now
	 */
	@Override
	protected boolean isShowAllAllowed(final SearchPageData<?> searchPageData)
	{
		return searchPageData.getPagination().getNumberOfPages() > 1
				&& searchPageData.getPagination().getTotalNumberOfResults() < getMaxSearchPageSize();
	}

	@Override
	protected int getMaxSearchPageSize()
	{
		return getConfigurationService().getConfiguration().getInt(MAX_PAGE_LIMIT);
	}

}
