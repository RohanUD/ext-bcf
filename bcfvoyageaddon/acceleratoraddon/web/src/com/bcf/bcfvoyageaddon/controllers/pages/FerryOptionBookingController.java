/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.order.BcfFerryOptionBookingFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/checkout/ferry-option-booking-details")
public class FerryOptionBookingController extends TravelAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(FerryOptionBookingController.class);
	private static final String FERRY_OPTION_BOOKING_PAGE = "ferryOptionBookingPage";
	private static final String INTEGRATION_EXCEPTION = "Integration exception encountered for while placing order";
	private static final String PLACE_ORDER_ERROR = "checkout.error.placeorder";
	private static final String REDIRECT_TO_OPTION_BOOKING_PAGE = REDIRECT_PREFIX + "/checkout/ferry-option-booking-details";

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "bcfFerryOptionBookingFacade")
	private BcfFerryOptionBookingFacade bcfFerryOptionBookingFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getFerryOptionBookingPage(final Model model) throws CMSItemNotFoundException
	{
		final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
		model.addAttribute(BcfCoreConstants.IS_FERRY_OPTION_BOOKING, sessionCart.isFerryOptionBooking());
		storeCmsPageInModel(model, getContentPageForLabelOrId(FERRY_OPTION_BOOKING_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FERRY_OPTION_BOOKING_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/confirm", method = RequestMethod.GET)
	public String confirmFerryOptionBooking(final Model model, final HttpServletRequest request,
			final HttpServletResponse response,
			final RedirectAttributes redirectModel)
	{
		try
		{
			final PlaceOrderResponseData decisionData = bcfFerryOptionBookingFacade.processFerryOptionBooking();
			if (Objects.nonNull(decisionData) && StringUtils.isEmpty(decisionData.getError()))
			{
				BcfControllerUtil.cleanUpSession(request, getSessionService());
				return REDIRECT_PREFIX + getBookingConfirmationPageURL(decisionData.getOrderData());
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, PLACE_ORDER_ERROR);
				return REDIRECT_TO_OPTION_BOOKING_PAGE;
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error(INTEGRATION_EXCEPTION, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, PLACE_ORDER_ERROR);
			return REDIRECT_TO_OPTION_BOOKING_PAGE;
		}
		catch (final OrderProcessingException | InvalidCartException e)
		{
			LOG.error("Exception encountered for while placing order", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, PLACE_ORDER_ERROR);
			return REDIRECT_TO_OPTION_BOOKING_PAGE;
		}
	}

	private String getBookingConfirmationPageURL(final OrderData orderData)
	{
		final String orderCode = StringUtils.isNotBlank(orderData.getOriginalOrderCode()) ?
				orderData.getOriginalOrderCode() :
				orderData.getCode();
		return "/checkout/bookingConfirmation/" + orderCode;
	}
}
