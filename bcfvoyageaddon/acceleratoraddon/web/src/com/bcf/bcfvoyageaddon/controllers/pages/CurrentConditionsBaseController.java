/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.CurrentConditionsData;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.Resource;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;


@Controller
public class CurrentConditionsBaseController extends BcfAbstractPageController
{
	private static final String IS_SAILINGS_FULL = "isSailingsFull";
	private static final String TOMORROW_DATE = "tomorrowDate";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	protected String getViewForPage(final Model model, final String pageLabel) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPage = getContentPageForLabelOrId(pageLabel);
		storeCmsPageInModel(model, contentPage);
		contentPage.getDescription();
		setUpMetaDataForContentPage(model, contentPage);
		return getViewForPage(model);
	}

	protected void getTomorrowDate(final Model model)
	{
		Date date = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		date = c.getTime();
		model.addAttribute(TOMORROW_DATE, DateFormatUtils.format(date, "yyyy-MM-dd"));
	}

	protected void isSailingsFull(final CurrentConditionsData currentConditionsData, final Model model)
	{

		if (currentConditionsDetailsFacade.isTodaySailingFull(currentConditionsData))
		{
			model.addAttribute(IS_SAILINGS_FULL, true);
		}
		else
		{
			model.addAttribute(IS_SAILINGS_FULL, false);
		}
	}
}
