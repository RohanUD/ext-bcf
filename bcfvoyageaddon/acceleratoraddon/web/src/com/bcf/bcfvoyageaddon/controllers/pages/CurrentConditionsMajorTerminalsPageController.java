/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.CurrentConditionsData;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/majorTerminals")
public class CurrentConditionsMajorTerminalsPageController extends CurrentConditionsBaseController
{

	private static final Logger LOG = Logger.getLogger(CurrentConditionsMajorTerminalsPageController.class);
	private static final String SHOW_COUNT = "showCount";
	private static final String MAJOR_TERMINALS_SAILINGS_LIMIT = "majorTerminalsLaterSailingsCounts";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@RequestMapping(method = RequestMethod.GET)
	public String getMajorTerminals(@RequestParam(value = "terminalCode", required = false) final String terminal,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{

		try
		{
			final CurrentConditionsData currentConditionsData = currentConditionsDetailsFacade
					.getDeparturesByTerminals(terminal, BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS);
			model.addAttribute("sourceTerminals", currentConditionsDetailsFacade.getDepartingTerminalsForCurrentConditions());
			model.addAttribute("southernGulfIslandTerminals", currentConditionsDetailsFacade.getSouternGulfIslandTerminals());
			model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_DATA, currentConditionsData);
			model.addAttribute("noMoreSailings", currentConditionsDetailsFacade.noMoreSailingsForToday(currentConditionsData));
			model.addAttribute("laterSailingsCounts",
					bcfConfigurablePropertiesService.getBcfPropertyValue(MAJOR_TERMINALS_SAILINGS_LIMIT));

			isSailingsFull(currentConditionsData, model);
			getTomorrowDate(model);
		}
		catch (final IntegrationException intEx)
		{
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.cc.notAvailable.label", null);
			LOG.error("Current Conditions Integration Exception occurred while access the services", intEx);
			model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_SERVICE_NOT_AVAILABLE, Boolean.TRUE);

		}

		final int showCount = Integer.parseInt(bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_NEXT_SAILING_SHOW_COUNT));
		model.addAttribute(SHOW_COUNT, showCount);

		return getViewForPage(model, BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS_CMS_PAGE);
	}
}
