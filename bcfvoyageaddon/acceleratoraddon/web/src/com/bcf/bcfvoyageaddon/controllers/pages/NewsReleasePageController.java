/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.NewsData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractSearchPageController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;


@Controller
@RequestMapping("/contact-us/media-room/news-releases")
public class NewsReleasePageController extends BcfAbstractSearchPageController
{
	private static final String NEWS_RELEASE_PAGE_URL = "news-release";
	private static final String NEWS_RELEASE_PAGE_URL_KEY = "newsReleaseURL";
	private static final String NEWS_RELEASE_DETAILS = "serviceNoticeDetails";
	private static final String PAGINATED_NEWS_RELEASE = "paginatedServiceNotices";
	public static final String MAX_PAGE_LIMIT = "pagination.service.notice.max.page.limit";
	public static final String NEWS_RELEASE_PAGE_SIZE = "pagination.news.release.page.size";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@RequestMapping(method = RequestMethod.GET)
	public String getNewsRelease(@RequestParam(name = "code", required = false) final String code,
			@RequestParam(value = "page", defaultValue = "0") final int pageNumber,
			@RequestParam(value = "mode", defaultValue = "Page") final String mode,
			final Model model) throws CMSItemNotFoundException
	{

		if (StringUtils.isNotEmpty(code))
		{
			model.addAttribute(NEWS_RELEASE_DETAILS,
					currentConditionsDetailsFacade.getNewsReleaseForCode(code));
		}
		else
		{
			final SearchPageData<NewsData> searchPageData = currentConditionsDetailsFacade
					.getPaginatedNewsReleases(pageNumber, getPageSize());
			populateModel(model, searchPageData, ShowMode.valueOf(mode));
			model.addAttribute(PAGINATED_NEWS_RELEASE, searchPageData);
		}

		model.addAttribute(NEWS_RELEASE_PAGE_URL_KEY, NEWS_RELEASE_PAGE_URL);

		storeCmsPageInModel(model,
				getContentPageForLabelOrId(BcfvoyageaddonWebConstants.NEWS_RELEASE_CMS_PAGE));

		setUpMetaDataForContentPage(model,
				getContentPageForLabelOrId(BcfvoyageaddonWebConstants.NEWS_RELEASE_CMS_PAGE));
		return getViewForPage(model);
	}

	private int getPageSize()
	{
		return getConfigurationService().getConfiguration().getInt(NEWS_RELEASE_PAGE_SIZE);
	}

	/**
	 * overide to use getMaxSearchPageSize() instead of MAX_PAGE_LIMIT, which is made configurable now
	 */
	@Override
	protected boolean isShowAllAllowed(final SearchPageData<?> searchPageData)
	{
		return searchPageData.getPagination().getNumberOfPages() > 1
				&& searchPageData.getPagination().getTotalNumberOfResults() < getMaxSearchPageSize();
	}

	@Override
	protected int getMaxSearchPageSize()
	{
		return getConfigurationService().getConfiguration().getInt(MAX_PAGE_LIMIT);
	}

}
