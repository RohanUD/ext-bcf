/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.misc;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bcf.facades.terminals.data.BcfRouteInfo;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;



/**
 * Travel Route Controller for fetching terminals data.
 */

@Controller("TravelRouteController")
public class TravelRouteController
{
	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@ResponseBody
	@RequestMapping(value = { "/route-info" }, method = { RequestMethod.GET }, produces = { "application/json" })
	public List<BcfRouteInfo> retrieveRoutes()
	{
		return bcfTravelRouteFacade.retrieveRouteInfo();
	}

	@ResponseBody
	@RequestMapping(value = { "/cc-route-info" }, method = { RequestMethod.GET }, produces = { "application/json" })
	public List<BcfRouteInfo> retrieveRoutesForCurrentConditions()
	{
		return bcfTravelRouteFacade.retrieveRoutesForCurrentConditions();
	}
}
