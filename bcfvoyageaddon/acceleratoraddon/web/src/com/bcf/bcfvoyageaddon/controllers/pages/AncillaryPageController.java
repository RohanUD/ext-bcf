/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 13:23
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.CheckInResponseData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferRequestData;
import de.hybris.platform.commercefacades.travel.ancillary.data.OfferResponseData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commercefacades.travel.seatmap.data.SeatMapResponseData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.TravelBundleTemplateFacade;
import de.hybris.platform.travelfacades.facades.TravelRestrictionFacade;
import de.hybris.platform.travelfacades.fare.search.UpgradeFareSearchFacade;
import de.hybris.platform.travelfacades.strategies.AncillaryOfferGroupDisplayStrategy;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfvoyageaddon.checkin.steps.CheckinStep;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.controllers.pages.steps.checkin.AbstractCheckinStepController;
import com.bcf.bcfvoyageaddon.forms.AddToCartForm;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.extra.product.BCFExtraProductPerJourney;
import com.bcf.facades.extra.product.BCFExtraProducts;
import com.bcf.facades.extra.product.ExtraProductPerVessel;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.offer.BCFOffersFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Ancillary page
 */
@Controller
@SessionAttributes(BcfFacadesConstants.BCF_FARE_FINDER_FORM)
@RequestMapping(
		{ "/ancillary", "/manage-booking/ancillary" })
public class AncillaryPageController extends AbstractCheckinStepController
{
	private static final String CHECK_IN_RESPONSE = "checkInResponse";
	private static final String CATEGORY_RESTRICTION_ERROR = "text.ancillary.category.restriction.error";
	private static final String RESTRICTION_ERRORS = "restrictionErrors";
	private static final String ERROR_RESULT = "errorResult";
	private static final String ANCILLARY_DEFAULT_CMS_PAGE = "ancillaryPage";
	private static final String ANCILLARY_CHECKIN_CMS_PAGE = "ancillaryCheckinPage";
	private static final String ANCILLARY_AMENDMENT_CMS_PAGE = "ancillaryAmendmentPage";
	private static final String NEXT_URL = "/ancillary/next";
	private static final String AMENDMENT_NEXT_URL = "/manage-booking/ancillary/next";
	private static final String ANCILLARY = "ancillary";
	private static final String CART_NOT_AMENDED_ERROR_FLAG = "cartNotAmendedErrorFlag";
	private static final String CART_NOT_AMENDED_ERROR = "text.ancillary.offers.continue.error";
	private static final String ERROR_ADD_EXTRA_TO_CART = "error.ancillary.add.extra.product.to.cart";
	private static final String BCF_EXTRA_PRODUCTS = "bcfExtraProducts";
	private static final String NO_SEARCH_RESULT = "search.result.not.found.amend.ancillaries";


	@Resource(name = "offersFacade")
	private BCFOffersFacade offersFacade;

	@Resource(name = "bcfSpecialAssistanceFacade")
	private BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "travelRestrictionFacade")
	private TravelRestrictionFacade travelRestrictionFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "ancillaryOfferGroupDisplayStrategy")
	private AncillaryOfferGroupDisplayStrategy ancillaryOfferGroupDisplayStrategy;

	@Resource(name = "offerGroupsViewMap")
	private Map<String, String> offerGroupsViewMap;

	@Resource(name = "travelBundleTemplateFacade")
	private TravelBundleTemplateFacade travelBundleTemplateFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "upgradeFareSearchFacade")
	private UpgradeFareSearchFacade upgradeFareSearchFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "ancillaryProductFacade")
	private AncillaryProductFacade ancillaryProductFacade;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfFareSearchFacade")
	private BcfFareSearchFacade fareSearchFacade;

	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAncillaryPage(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		if (!bcfTravelCartFacade.validateOriginDestinationRefNumbersInCart())
		{
			return REDIRECT_PREFIX + "/";
		}

		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.IS_CHECK_IN_JOURNEY, false);
		return getAncillaryPage(model, ANCILLARY_DEFAULT_CMS_PAGE);
	}

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/{originDestinationRefNumber}/{journeyReferenceNumber}/amendment", method = RequestMethod.GET)
	public String getAmendmentAncillaryPage(@PathVariable("journeyReferenceNumber") final int journeyRefNum,
			@PathVariable("originDestinationRefNumber") final int odRefNum, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		final FareSearchRequestData fareSearchRequestData = new FareSearchRequestData();


		final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
		bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyRefNum, odRefNum);

		boolean isReturn = false;
		if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
				.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
		{
			isReturn = true;
		}

		bcfFerrySelectionUtil
				.convert(bcfFareFinderForm, fareSearchRequestData, request.getParameter(BcfvoyageaddonWebConstants.DISPLAY_ORDER),
						isReturn, odRefNum);
		final FareSelectionData fareSelectionData;
		try
		{
			fareSearchFacade.performSearch(fareSearchRequestData);

			if (getSessionService()
					.getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP) == null || MapUtils
					.isEmpty(getSessionService()
							.getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP)))
			{
				final List<String> errorMessages = new ArrayList<>();
				errorMessages.add(propertySourceFacade.getPropertySourceValue(NO_SEARCH_RESULT));
				model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES, errorMessages);
				final ContentPageModel contentPage = getContentPageForLabelOrId(ANCILLARY_AMENDMENT_CMS_PAGE);
				storeCmsPageInModel(model, contentPage);
				setUpMetaDataForContentPage(model, contentPage);
				model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID,contentPage.isDisplayOrderId());

				return getViewForPage(model);
			}
		}
		catch (final IntegrationException e)
		{

			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES,
					e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).collect(Collectors.toList()));

		}

		Map<String, Integer> ancillaryProductsEntryQtyMap = new HashMap<>();
		final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, odRefNum);

		final List<AbstractOrderEntryModel> ancillaryProductsEntry = entries.stream().filter(
				entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof AncillaryProductModel
		).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(ancillaryProductsEntry))
		{
			ancillaryProductsEntryQtyMap = ancillaryProductsEntry.stream().collect(Collectors
					.groupingBy(entry -> ((AncillaryProductModel) entry.getProduct()).getCode(),
							Collectors.summingInt(entry -> ((AbstractOrderEntryModel) entry).getQuantity().intValue())));
		}
		model.addAttribute("ancillaryProductsEntryQtyMap", ancillaryProductsEntryQtyMap);

		final BCFExtraProducts bcfExtraProducts = ancillaryProductFacade
				.getExtraProductsPerJourneyPerLegPerVessel(journeyRefNum, odRefNum);
		model.addAttribute(BCF_EXTRA_PRODUCTS, bcfExtraProducts);

		sessionService.setAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);
		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRefNum);
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM,
				bcfFareFinderForm);

		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.IS_CHECK_IN_JOURNEY, false);
		return getAncillaryPage(model, ANCILLARY_DEFAULT_CMS_PAGE);
	}

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/checkin", method = RequestMethod.GET)
	public String getCheckinAncillaryPage(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		if (!model.asMap().containsKey(CHECK_IN_RESPONSE))
		{
			return REDIRECT_PREFIX + "/";
		}

		final CheckInResponseData checkInResponse = (CheckInResponseData) model.asMap().get(CHECK_IN_RESPONSE);
		final String bookingReference = checkInResponse.getBookingReference();
		bookingFacade.amendOrder(bookingReference, bookingFacade.getCurrentUserUid());
		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.IS_CHECK_IN_JOURNEY, true);
		return getAncillaryPage(model, ANCILLARY_CHECKIN_CMS_PAGE);
	}

	protected String getAncillaryPage(final Model model, final String ancillaryCmsPage) throws CMSItemNotFoundException
	{
		if (!bcfTravelCartFacade.isCurrentCartValid())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.HOME_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ancillaryCmsPage));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ancillaryCmsPage));




		model.addAttribute("addToCartForm", new AddToCartForm());
		model.addAttribute(BcfvoyageaddonWebConstants.AMEND, bcfTravelCartFacade.isAmendmentCart());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, determineNextUrl());


		if (bcfTravelCartFacade.isAmendmentCart())
		{
			setProductAvailabilityInSession(model);
			Map<String, Integer> ancillaryProductsEntryQtyMap = new HashMap<>();
			final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getAllTransportCartEntries();

			final List<AbstractOrderEntryModel> ancillaryProductsEntry = entries.stream().filter(
					entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof AncillaryProductModel
			).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(ancillaryProductsEntry))
			{
				ancillaryProductsEntryQtyMap = ancillaryProductsEntry.stream().collect(Collectors
						.groupingBy(entry -> ((AncillaryProductModel) entry.getProduct()).getCode()+BcfCoreConstants.HYPHEN+entry.getJourneyReferenceNumber()+BcfCoreConstants.HYPHEN+entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber(),
								Collectors.summingInt(entry -> ((AbstractOrderEntryModel) entry).getQuantity().intValue())));
			}
			model.addAttribute("ancillaryProductsEntryQtyMap", ancillaryProductsEntryQtyMap);

			if (model.containsAttribute(BCF_EXTRA_PRODUCTS))
			{

				sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
						BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
				model.addAttribute(BcfvoyageaddonWebConstants.ORIGINAL_ORDER_CODE, bcfTravelCartFacade.getOriginalOrderCode());
			}
			else
			{

				final BCFExtraProducts bcfExtraProducts = ancillaryProductFacade.getExtraProductsPerJourneyPerLegPerVessel();
				model.addAttribute(BCF_EXTRA_PRODUCTS, bcfExtraProducts);

			}
		}
		else
		{


			final BCFExtraProducts bcfExtraProducts = ancillaryProductFacade.getExtraProductsPerJourneyPerLegPerVessel();
			model.addAttribute(BCF_EXTRA_PRODUCTS, bcfExtraProducts);
		}

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
		}
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		return getViewForPage(model);
	}


	@RequestMapping(method = RequestMethod.POST)
	public String saveExtraProductsData(
			@ModelAttribute("BCF_EXTRA_PRODUCTS") final BCFExtraProducts bcfExtraProducts,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
			final HttpServletRequest request, final HttpServletResponse response, final SessionStatus sessionStatus)
	{
		if (bindingResult.hasErrors())
		{
			return REDIRECT_PREFIX + "/ancillary";
		}
		try
		{
			removeUnselectedProducts(bcfExtraProducts.getBcfExtraProductPerJourneyList());
			bcfTravelCartFacade.addExtraProductToCart(bcfExtraProducts);
		}
		catch (final CommerceCartModificationException e)
		{
			GlobalMessages.addErrorMessage(model, ERROR_ADD_EXTRA_TO_CART);
			return REDIRECT_PREFIX + "/ancillary";
		}
		return REDIRECT_PREFIX + determineNextUrl();
	}

	public void removeUnselectedProducts(final List<BCFExtraProductPerJourney> extraProductPerJourney)
	{
		extraProductPerJourney.stream().forEach(extraProduct -> {
			if (Objects.nonNull(extraProduct.getInboundExtraProductDataList()))
			{
				extraProduct.getOutboundExtraProductDataList().forEach(outboundExtraProduct -> {
					populateOutboundExtraProduct(outboundExtraProduct);
				});
			}
			if (Objects.nonNull(extraProduct.getInboundExtraProductDataList()))
			{
				extraProduct.getInboundExtraProductDataList().forEach(inboundExtraProduct -> {
					populateInboundExtraProduct(inboundExtraProduct);
				});
			}
		});
	}

	private void populateInboundExtraProduct(final ExtraProductPerVessel inboundExtraProduct)
	{
		if (Objects.isNull(inboundExtraProduct.getCabinProductList()))
		{
			inboundExtraProduct.setCabinProductList(Collections.emptyList());
		}
		if (Objects.isNull(inboundExtraProduct.getMealProductList()))
		{
			inboundExtraProduct.setMealProductList(Collections.emptyList());
		}
		if (Objects.isNull(inboundExtraProduct.getLoungeProductList()))
		{
			inboundExtraProduct.setLoungeProductList(Collections.emptyList());
		}
	}

	private void populateOutboundExtraProduct(final ExtraProductPerVessel outboundExtraProduct)
	{
		populateInboundExtraProduct(outboundExtraProduct);
	}


	private void setProductAvailabilityInSession(Model model)
	{
		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if(sessionBookingJourney.equals(BcfFacadesConstants.BOOKING_ALACARTE) && getSessionService()
				.getAttribute(BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP)==null)
		{

			List<Integer> longRouteJourneyRef = bcfTravelCartFacadeHelper.getLongRouteJourneyRefNumber();

			if (CollectionUtils.isNotEmpty(longRouteJourneyRef))
			{


				for (Integer journeyRef : longRouteJourneyRef)
				{

					final FareSearchRequestData fareSearchRequestData = new FareSearchRequestData();


					final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
					bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyRef, 0);


					bcfFerrySelectionUtil
								.convert(bcfFareFinderForm, fareSearchRequestData, null,
										false,0);

					try
					{
						fareSearchFacade.performSearch(fareSearchRequestData);

					}
					catch (final IntegrationException e)
					{
						model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES,
								e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).collect(Collectors.toList()));

					}

					if(bcfFareFinderForm.getRouteInfoForm().getTripType().equalsIgnoreCase(TripType.RETURN.name()))
					{
						bcfFerrySelectionUtil
								.convert(bcfFareFinderForm, fareSearchRequestData, null,
										true, 0);

						try
						{
							fareSearchFacade.performSearch(fareSearchRequestData);

						}
						catch (final IntegrationException e)
						{
							model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES,
									e.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).collect(Collectors.toList()));

						}

					}

					}

			}

		}

	}

	@ResponseBody
	@RequestMapping(value =
			{ "/addAccessibilityDeclaration" }, method =
			{ RequestMethod.POST, RequestMethod.GET })
	public String addSelectedAssistanceRequestToCart(@RequestParam("journeyRefNumber") final int journeyRefNumber,
			@RequestParam("travellerUid") final String travellerUid,
			@RequestParam("specialServiceCode") final String specialServiceCode)
	{
		return getBcfSpecialAssistanceFacade().addSpecialRequestToCart(journeyRefNumber, travellerUid, specialServiceCode);
	}

	@ResponseBody
	@RequestMapping(value =
			{ "/removeAccessibilityDeclaration" }, method =
			{ RequestMethod.POST, RequestMethod.GET })
	public String removeSelectedAssistanceRequestFromCart(@RequestParam("journeyRefNumber") final int journeyRefNumber,
			@RequestParam("travellerUid") final String travellerUid,
			@RequestParam("specialServiceCode") final String specialServiceCode)
	{
		return getBcfSpecialAssistanceFacade().removeSpecialRequestFromCart(journeyRefNumber, travellerUid, specialServiceCode);
	}

	@ResponseBody
	@RequestMapping(value =
			{ "/accommodation-map" }, method =
			{ RequestMethod.POST, RequestMethod.GET }, produces =
			{ "application/json" })
	public SeatMapResponseData getAccommodations()
	{

		final OfferRequestData offerRequest = offersFacade.getOffersRequest();
		final OfferResponseData offerResponseData = offersFacade.getAccommodations(offerRequest);
		return offerResponseData.getSeatMap();
	}

	/**
	 * This method determines the next url of checkout flow. If the context is purchase flow, then the next url is
	 * "/ancillary/next" else if the context is Amendments then the next url is "/manage-booking/ancillary/next".
	 *
	 * @return next url
	 */
	protected String determineNextUrl()
	{
		return bcfTravelCartFacade.isAmendmentCart() ? AMENDMENT_NEXT_URL : NEXT_URL;
	}

	/**
	 * Performs a validation on the TravelRestriction for each OfferGroup available.
	 *
	 * @param model
	 * @return the location of the JSON object used to render the error in the front-end
	 */
	@RequestMapping(value = "/check-offer-groups-restriction", method = RequestMethod.GET)
	public String checkCategoryRestrictions(final Model model)
	{
		model.addAttribute(RESTRICTION_ERRORS, travelRestrictionFacade.getCategoryRestrictionErrors());
		return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.TravelRestrictionResponse;
	}

	/**
	 * Redirects user to the next checkout page which is traveller details
	 *
	 * @param redirectModel
	 * @return traveller details page
	 */
	@RequestMapping(value = "/next", method = RequestMethod.GET)
	public String nextPage(final RedirectAttributes redirectModel)
	{
		if (!travelRestrictionFacade.checkCategoryRestrictions())
		{
			redirectModel.addFlashAttribute(ERROR_RESULT, CATEGORY_RESTRICTION_ERROR);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ANCILLARY_ROOT_URL;
		}

		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (bcfTravelCartFacade.isAmendmentCart())
		{
			final String handleAmendmentCartResponse = handleAmendmentCart(sessionBookingJourney);
			if (CART_NOT_AMENDED_ERROR_FLAG.equals(handleAmendmentCartResponse))
			{
				redirectModel.addFlashAttribute(ERROR_RESULT, CART_NOT_AMENDED_ERROR);
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ANCILLARY_AMENDMENT_ROOT_URL;
			}
			return handleAmendmentCartResponse;
		}

		if (travelCheckoutFacade.containsLongRoute() && Arrays.asList(BcfstorefrontaddonWebConstants.BOOKING_ALACARTE,BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE).contains(
				 sessionBookingJourney))
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.TRAVELLER_DETAILS_PATH;
		}

		if (!travelCheckoutFacade.containsLongRoute())
		{
			return getCheckinStep().nextStep();
		}

		if (!bcfTravelCartFacade.isAmendmentCart() && StringUtils
				.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY))
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.TRAVELLER_DETAILS_PATH;
		}

		return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_PATH;
	}

	private String handleAmendmentCart(final String sessionBookingJourney)
	{

		if (userFacade.isAnonymousUser())
		{
			getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
		}

		if (travelCheckoutFacade.containsLongRoute() && Arrays.asList(BcfstorefrontaddonWebConstants.BOOKING_ALACARTE,BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE).contains(
				sessionBookingJourney))
		{

			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.TRAVELLER_DETAILS_PATH;
		}

		if (!travelCheckoutFacade.containsLongRoute())
		{
			return getCheckinStep().nextStep();
		}

		final Integer originDestinationRefNum = sessionService
				.getAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM);
		final Integer journeyRefNum = sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER);
		if (Objects.nonNull(journeyRefNum) && Objects.nonNull(originDestinationRefNum))
		{
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.TRAVELLER_DETAILS_PATH + "?"
					+ BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM + "=" + originDestinationRefNum + "&journeyRefNum="
					+ journeyRefNum;
		}
		return getCheckinStep().nextStep();
	}

	protected CheckinStep getCheckinStep()
	{
		return getCheckinStep(ANCILLARY);
	}

	public BCFSpecialAssistanceFacade getBcfSpecialAssistanceFacade()
	{
		return bcfSpecialAssistanceFacade;
	}

	public void setBcfSpecialAssistanceFacade(final BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade)
	{
		this.bcfSpecialAssistanceFacade = bcfSpecialAssistanceFacade;
	}

}
