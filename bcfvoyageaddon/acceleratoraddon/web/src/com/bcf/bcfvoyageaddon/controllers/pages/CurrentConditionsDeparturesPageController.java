/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/departures")
public class CurrentConditionsDeparturesPageController extends CurrentConditionsBaseController
{

	private static final Logger LOG = Logger.getLogger(CurrentConditionsDeparturesPageController.class);

	private static final String SELECTED_TERMINAL = "selectedTerminal";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getDepartures(@RequestParam(value = "terminalCode", required = false) final String terminal, final HttpServletRequest request, final Model model) throws CMSItemNotFoundException {

		model.addAttribute(SELECTED_TERMINAL, terminal);
		try {
			model.addAttribute("sourceTerminals", currentConditionsDetailsFacade.getDepartingTerminalsForCurrentConditions());
			model.addAttribute("southernGulfIslandTerminals", currentConditionsDetailsFacade.getSouternGulfIslandTerminals());
			model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_DATA,
					currentConditionsDetailsFacade
							.getTodayDeparturesByTerminals(terminal, BcfFacadesConstants.CURRENT_CONDITIONS_MAJOR_TERMINALS));
		}
		catch (final IntegrationException intEx)
		{
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.cc.notAvailable.label", null);
			LOG.error("Current Conditions Integration Exception occurred while access the services", intEx);
			model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_SERVICE_NOT_AVAILABLE, Boolean.TRUE);
		}

		return getViewForPage(model, BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_DEPARTURES_CMS_PAGE);
	}
}
