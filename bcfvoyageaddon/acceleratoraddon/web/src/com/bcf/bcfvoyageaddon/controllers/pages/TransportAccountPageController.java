/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.TravellerPreferenceData;
import de.hybris.platform.commercefacades.travel.search.data.SavedSearchData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.TransportOfferingFacade;
import de.hybris.platform.travelfacades.facades.TravelI18NFacade;
import de.hybris.platform.travelfacades.facades.customer.TravelCustomerFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.enums.DocumentType;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.enums.TripType;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.APIForm;
import com.bcf.bcfvoyageaddon.forms.BCFAddressForm;
import com.bcf.bcfvoyageaddon.forms.PassengerInformationForm;
import com.bcf.bcfvoyageaddon.forms.TravellerDetails;
import com.bcf.bcfvoyageaddon.forms.TravellerForm;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.PassengerInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm;
import com.bcf.bcfvoyageaddon.forms.validation.TravellerFormValidator;
import com.bcf.bcfvoyageaddon.util.BcfAddressDataUtil;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.facades.traveller.BCFTravellerFacade;


/**
 * Extended Controller for home page - adds functionality to AccountPageController
 */
@Controller
public class TransportAccountPageController extends BcfAbstractPageController
{
	// CMS Pages
	protected static final String ACCOUNT_SAVED_SEARCHES_CMS_PAGE = "my-saved-searches";
	protected static final String ACCOUNT_ADVANCE_PASSENGER_CMS_PAGE = "advance-passenger";
	protected static final String ACCOUNT_SAVED_PASSENGERS_CMS_PAGE = "saved-passengers";
	protected static final String ACCOUNT_UPDATE_SAVED_PASSENGERS_CMS_PASSENGER = "update-saved-passenger";
	protected static final String ACCOUNT_ADD_SAVED_PASSENGERS_CMS_PASSENGER = "add-saved-passenger";
	protected static final String PREFERENCES_CMS_PAGE = "preferences";
	private static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";

	protected static final String SAVED_TRAVELLLERS = "savedTravellers";
	protected static final String REMOVED_TRAVELLER = "removedTraveller";
	protected static final String UPDATE_TRAVELLER_SUCCESS_FLAG = "updateTravellerSuccess";
	protected static final String ADDED_TRAVELLER = "addedTraveller";
	protected static final String SAVED_SEARCHES = "savedSearches";
	protected static final String ERROR = "error";
	protected static final String LANGUAGES = "languages";
	protected static final String LANGUAGE_PREFERENCE_TYPE = "LANGUAGE";
	protected static final String TRANSPORT_FACILITY_PREFERENCE_TYPE = "TRANSPORT_FACILITY";
	protected static final String TRANSPORT_FACILITY_PREFERENCE_CODE = "TRANSPORT_FACILITY_CODE";
	protected static final String COUNTRIES = "countries";
	protected static final String NATIONALITIES = "nationalities";
	protected static final String TITLES = "titles";
	protected static final String API_FORM = "apiForm";
	protected static final String ADULT = "adult";
	protected static final String FORM_GLOBAL_CONFIRMATION = "form.global.confirmation";
	private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
	private static final String TEXT_ACCOUNT_ADDRESS_BOOK = "text.account.addressBook";
	private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	private static final String COUNTRY_ATTR = "country";
	private static final String REGIONS_ATTR = "regions";
	private static final String IS_DEFAULT_ADDRESS_ATTR = "isDefaultAddress";
	private static final String COUNTRY_DATA_ATTR = "countryData";
	private static final String ADDRESS_BOOK_EMPTY_ATTR = "addressBookEmpty";
	private static final String TITLE_DATA_ATTR = "titleData";
	private static final String ADDRESS_FORM_ATTR = "addressForm";
	private static final String ADDRESS_DATA_ATTR = "addressData";
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String ADDRESS_BOOK_CMS_PAGE = "customer-address-book";
	private static final String MY_ACCOUNT_ADDRESS_BOOK_URL = "/my-account/address-book";
	private static final String MY_ACCOUNT_URL = "/my-account/";
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
	private static final String TRAVELLER_DETAILS_FORMS = "travellerDetailsForms";
	private static final String TRAVELLER_TITLES = "travellerTitles";
	private static final String PASSENGER_TYPES = "passengerTypes";
	private static final String REASON_FOR_TRAVEL_OPTIONS = "reasonForTravelOptions";
	private static final String SAVED_TRAVELLERS = "savedTravellers";
	private static final String TRAVELLERS_NAMES_MAP = "travellersNamesMap";


	protected static final String BREADCRUMBS = "breadcrumbs";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "travelCustomerFacade")
	private TravelCustomerFacade travelCustomerFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "travellerFormValidator")
	private TravellerFormValidator travellerFormValidator;

	@Resource(name = "travelI18NFacade")
	private TravelI18NFacade travelI18NFacade;

	@Resource(name = "transportOfferingFacade")
	private TransportOfferingFacade transportOfferingFacade;

	@Resource(name = "apiFormValidator")
	private Validator apiFormValidator;

	private String[] adultTitles;
	private String[] childrenTitles;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "bcfAddressDataUtil")
	private BcfAddressDataUtil bcfAddressDataUtil;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "ancillaryProductFacade")
	private AncillaryProductFacade ancillaryProductFacade;

	@Resource(name = "ancillaryProductConverter")
	private Converter<ProductModel, ProductData> ancillaryProductConverter;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "bcfSpecialAssistanceFacade")
	private BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getDeliveryCountries();
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}

	@RequestMapping(value = "/my-account/preferences", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getPreferencesPage(final Model model) throws CMSItemNotFoundException
	{
		final List<TravellerPreferenceData> travellerPreference = travellerFacade.getTravellerPreferences();

		travellerPreference.forEach(tp -> {
			if (StringUtils.equalsIgnoreCase(tp.getType(), TRANSPORT_FACILITY_PREFERENCE_TYPE))
			{
				final Map<String, Map<String, String>> suggestions = transportOfferingFacade.getOriginSuggestions(tp.getValue());

				if (!suggestions.isEmpty())
				{
					final String transportFacilityName = suggestions.get(suggestions.keySet().iterator().next()).keySet().iterator()
							.next();
					final String transportFacilityCode = suggestions.get(suggestions.keySet().iterator().next())
							.get(transportFacilityName);

					model.addAttribute(TRANSPORT_FACILITY_PREFERENCE_TYPE,
							transportFacilityName.replace("<strong>", "").replace("</strong>", ""));
					model.addAttribute(TRANSPORT_FACILITY_PREFERENCE_CODE, transportFacilityCode);
				}
			}

			if (StringUtils.equalsIgnoreCase(tp.getType(), LANGUAGE_PREFERENCE_TYPE))
			{
				model.addAttribute(LANGUAGE_PREFERENCE_TYPE, tp.getValue());
			}
		});

		model.addAttribute(LANGUAGES, travelI18NFacade.getAllLanguages());

		storeCmsPageInModel(model, getContentPageForLabelOrId(PREFERENCES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PREFERENCES_CMS_PAGE));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/save-preferences", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveTravellerPreferencesPage(@RequestParam final String preferredLanguage,
			@RequestParam final String transportFacilityCode, final Model model) throws CMSItemNotFoundException
	{
		final List<TravellerPreferenceData> travellerPreferences = new ArrayList<>();

		if (StringUtils.isNotEmpty(preferredLanguage))
		{
			final TravellerPreferenceData language = new TravellerPreferenceData();
			language.setType(LANGUAGE_PREFERENCE_TYPE);
			language.setValue(preferredLanguage);
			travellerPreferences.add(language);
		}

		if (StringUtils.isNotEmpty(transportFacilityCode))
		{
			final TravellerPreferenceData transportFacilityPreference = new TravellerPreferenceData();
			transportFacilityPreference.setType(TRANSPORT_FACILITY_PREFERENCE_TYPE);
			transportFacilityPreference.setValue(transportFacilityCode);
			travellerPreferences.add(transportFacilityPreference);
		}

		travellerFacade.getSaveTravellerPreferences(travellerPreferences);
		GlobalMessages.addInfoMessage(model, FORM_GLOBAL_CONFIRMATION);

		return getPreferencesPage(model);
	}

	/**
	 * Method responsible for handling GET request for Saved Customer Searches.
	 *
	 * @param model
	 * @return mySavedSearches page
	 */

	@RequestMapping(value = "/my-account/my-saved-searches", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSavedSearchesPage(final Model model) throws CMSItemNotFoundException
	{
		final List<SavedSearchData> savedSearchDatas = travelCustomerFacade.getCustomerSearches();
		model.addAttribute(SAVED_SEARCHES, savedSearchDatas);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_SAVED_SEARCHES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_SAVED_SEARCHES_CMS_PAGE));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	/**
	 * Method responsible for handling GET request to remove the specific saved customer search.
	 *
	 * @param savedSearchID
	 * @param model
	 * @param request
	 * @param redirectModel
	 * @return mySavedSearches page
	 */
	@RequestMapping(value = "/my-account/remove-saved-searches/{savedSearchID}", method = RequestMethod.GET)
	public String getRemoveSavedSearchPage(@PathVariable final String savedSearchID, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel)
	{
		final boolean isSearchRemoved = travelCustomerFacade.removeSavedSearch(savedSearchID);

		if (!isSearchRemoved)
		{
			redirectModel.addFlashAttribute(ERROR, true);
		}

		return REDIRECT_PREFIX + MY_ACCOUNT_URL + ACCOUNT_SAVED_SEARCHES_CMS_PAGE;

	}

	@RequestMapping(value = "/my-account/advance-passenger", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAdvancePassenger(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(COUNTRIES, travelI18NFacade.getAllCountries());
		model.addAttribute(NATIONALITIES, travelI18NFacade.getAllCountries());
		model.addAttribute(TITLES, userFacade.getTitles().stream()
				.filter((titleData -> Arrays.asList(getAdultTitles()).contains(titleData.getCode()))).collect(Collectors.toList()));

		if (!model.containsAttribute(API_FORM))
		{
			final PassengerInformationData passengerInformationData = travellerFacade.getPassengerInformation();
			final APIForm apiForm = new APIForm();

			if (passengerInformationData != null)
			{
				setAPIFormData(apiForm, passengerInformationData);
			}

			model.addAttribute(API_FORM, apiForm);
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_ADVANCE_PASSENGER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_ADVANCE_PASSENGER_CMS_PAGE));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	private void setAPIFormData(final APIForm apiForm, final PassengerInformationData passengerInformationData)
	{
		apiForm.setFirstname(passengerInformationData.getFirstName());
		apiForm.setGender(passengerInformationData.getGender());
		apiForm.setLastname(passengerInformationData.getSurname());
		apiForm.setTitle((passengerInformationData.getTitle() == null) ? null : passengerInformationData.getTitle().getCode());
		apiForm.setDateOfBirth(passengerInformationData.getDateOfBirth() != null ? TravelDateUtils
				.convertDateToStringDate(passengerInformationData.getDateOfBirth(), TravelservicesConstants.DATE_PATTERN) : null);
		apiForm.setCountryOfIssue((passengerInformationData.getCountryOfIssue() == null) ? null
				: passengerInformationData.getCountryOfIssue().getIsocode());
		apiForm.setDocumentExpiryDate(passengerInformationData.getDocumentExpiryDate() != null
				? TravelDateUtils.convertDateToStringDate(passengerInformationData.getDocumentExpiryDate(),
				TravelservicesConstants.DATE_PATTERN)
				: null);
		apiForm.setDocumentNumber(passengerInformationData.getDocumentNumber());
		apiForm.setDocumentType(
				passengerInformationData.getDocumentType() != null ? passengerInformationData.getDocumentType().getCode() : null);
		apiForm.setNationality(
				(passengerInformationData.getNationality() == null) ? null : passengerInformationData.getNationality().getIsocode());
	}

	@RequestMapping(value = "/my-account/advance-passenger", method = RequestMethod.POST)
	@RequireHardLogIn
	public String getAdvancePassenger(@ModelAttribute(API_FORM) final APIForm apiForm, final BindingResult bindingResult,
			final Model model) throws CMSItemNotFoundException
	{
		apiFormValidator.validate(apiForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, TravelacceleratorstorefrontValidationConstants.FORM_GLOBAL_ERROR);
			return getAdvancePassenger(model);
		}

		final PassengerInformationData passengerInformationData = new PassengerInformationData();
		final CountryData countryOfIssue = new CountryData();
		countryOfIssue.setIsocode(apiForm.getCountryOfIssue());
		passengerInformationData.setCountryOfIssue(countryOfIssue);
		passengerInformationData.setDateOfBirth(
				TravelDateUtils.convertStringDateToDate(apiForm.getDateOfBirth(), TravelservicesConstants.DATE_PATTERN));
		passengerInformationData.setDocumentExpiryDate(
				TravelDateUtils.convertStringDateToDate(apiForm.getDocumentExpiryDate(), TravelservicesConstants.DATE_PATTERN));
		passengerInformationData.setDocumentNumber(apiForm.getDocumentNumber());
		passengerInformationData.setDocumentType(DocumentType.valueOf(apiForm.getDocumentType()));

		final CountryData nationality = new CountryData();
		nationality.setIsocode(apiForm.getNationality());
		passengerInformationData.setNationality(nationality);

		final PassengerTypeData passengerTypeData = new PassengerTypeData();
		passengerTypeData.setCode(ADULT);

		passengerInformationData.setPassengerType(passengerTypeData);

		final TitleData titleData = new TitleData();
		titleData.setCode(apiForm.getTitle());

		passengerInformationData.setTitle(titleData);
		passengerInformationData.setFirstName(apiForm.getFirstname());
		passengerInformationData.setSurname(apiForm.getLastname());
		passengerInformationData.setGender(apiForm.getGender());

		final TravellerData travellerData = new TravellerData();
		travellerData.setTravellerInfo(passengerInformationData);
		travellerData.setLabel(ADULT);
		travellerData.setFormId("0");

		travellerFacade.updatePassengerInformation(travellerData);

		GlobalMessages.addInfoMessage(model, FORM_GLOBAL_CONFIRMATION);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_ADVANCE_PASSENGER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_ADVANCE_PASSENGER_CMS_PAGE));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getAdvancePassenger(model);
	}

	@RequestMapping(value = "/my-account/saved-passengers", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSavedPassengersPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(SAVED_TRAVELLLERS, travellerFacade.getSavedTravellersForCurrentUser());
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_SAVED_PASSENGERS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_SAVED_PASSENGERS_CMS_PAGE));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	@RequestMapping(value =
			{ "/allowed-passenger-titles/{passengerTypeCode}" }, method =
			{ RequestMethod.POST, RequestMethod.GET })
	public String getTitlesForPassengerType(final Model model, @PathVariable("passengerTypeCode") final String passengerTypeCode,
			final RedirectAttributes redirectModel)
	{

		model.addAttribute(TRAVELLER_TITLES, passengerTypeFacade.getAllowedTitles(passengerTypeCode));
		return BcfvoyageaddonControllerConstants.Views.Pages.TravellerDetails.AllowedTitlesForPassengerType;
	}

	@RequestMapping(value = "/my-account/add-saved-passenger", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSavedPassengerPageForAdd(final Model model) throws CMSItemNotFoundException
	{
		final List<TravellerData> travellers = new ArrayList<>();
		final TravellerData traveller = travellerFacade.createTraveller(TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER,
				ADULT, ADULT, 0, "0001");
		travellers.add(traveller);
		if (!model.containsAttribute(TRAVELLER_DETAILS_FORMS))
		{
			model.addAttribute(TRAVELLER_DETAILS_FORMS, getTravellerForms(travellers));
		}
		model.addAttribute(TRAVELLERS_NAMES_MAP, travellerFacade.populateTravellersNamesMap(travellers));
		model.addAttribute(REASON_FOR_TRAVEL_OPTIONS, travellerFacade.getReasonForTravelTypes());
		model.addAttribute(SAVED_TRAVELLERS, travellerFacade.getSavedTravellersForCurrentUser());
		model.addAttribute(TRAVELLER_TITLES, passengerTypeFacade.getAllowedTitles(ADULT));
		model.addAttribute(PASSENGER_TYPES, passengerTypeFacade.getPassengerTypes());

		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_ADD_SAVED_PASSENGERS_CMS_PASSENGER));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_ADD_SAVED_PASSENGERS_CMS_PASSENGER));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}


	@RequestMapping(value = "/my-account/add-saved-passenger", method = RequestMethod.POST)
	public String addSavedPassengers(@Valid @ModelAttribute(TRAVELLER_DETAILS_FORMS) final TravellerDetails travellerDetails,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{

		travellerFormValidator.validate(travellerDetails.getTravellerForms(), bindingResult);

		if (bindingResult.hasErrors())
		{
			return getSavedPassengersPage(model);
		}

		final TravellerData addedTraveller = travellerFacade
				.addCustomerSavedTravellers(getTravellerFromForm(travellerDetails).get(0));

		if (addedTraveller == null)
		{
			redirectModel.addFlashAttribute(ERROR, true);
			return REDIRECT_PREFIX + MY_ACCOUNT_URL + ACCOUNT_SAVED_PASSENGERS_CMS_PAGE;
		}


		final PassengerInformationData passengerInformation = (PassengerInformationData) addedTraveller.getTravellerInfo();
		final String travellerName = passengerInformation.getTitle().getName() + " " + passengerInformation.getFirstName() + " "
				+ passengerInformation.getSurname();

		redirectModel.addFlashAttribute(ADDED_TRAVELLER, travellerName);

		return REDIRECT_PREFIX + "/my-account/saved-passengers";
	}

	@RequestMapping(value = "/my-account/update-saved-passenger/{passengerUid}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSavedPassengerPageForUpdate(@PathVariable final String passengerUid, final Model model)
			throws CMSItemNotFoundException
	{
		final List<TravellerData> travellers = new ArrayList<>();
		final TravellerData traveller = travellerFacade.getTraveller(passengerUid);
		final PassengerInformationData passengerInformationData = (PassengerInformationData) traveller.getTravellerInfo();

		travellers.add(traveller);
		if (!model.containsAttribute(TRAVELLER_DETAILS_FORMS))
		{
			model.addAttribute(TRAVELLER_DETAILS_FORMS, getTravellerForms(travellers));
		}
		model.addAttribute(TRAVELLERS_NAMES_MAP, travellerFacade.populateTravellersNamesMap(travellers));
		model.addAttribute(REASON_FOR_TRAVEL_OPTIONS, travellerFacade.getReasonForTravelTypes());
		model.addAttribute(SAVED_TRAVELLERS, travellerFacade.getSavedTravellersForCurrentUser());
		model.addAttribute(TRAVELLER_TITLES,
				passengerTypeFacade.getAllowedTitles(passengerInformationData.getPassengerType().getCode()));
		model.addAttribute(PASSENGER_TYPES, passengerTypeFacade.getPassengerTypes());


		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_UPDATE_SAVED_PASSENGERS_CMS_PASSENGER));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_UPDATE_SAVED_PASSENGERS_CMS_PASSENGER));
		model.addAttribute(BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/update-saved-passenger", method = RequestMethod.POST)
	public String updateSavedPassengers(@Valid @ModelAttribute(TRAVELLER_DETAILS_FORMS) final TravellerDetails travellerDetails,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{

		travellerFormValidator.validate(travellerDetails.getTravellerForms(), bindingResult);

		if (bindingResult.hasErrors())
		{
			return getSavedPassengersPage(model);
		}
		travellerFacade.updateCustomerSavedTravellers(getTravellerFromForm(travellerDetails));
		redirectModel.addFlashAttribute(UPDATE_TRAVELLER_SUCCESS_FLAG, Boolean.TRUE);
		return REDIRECT_PREFIX + "/my-account/saved-passengers";
	}

	private List<TravellerData> getTravellerFromForm(final TravellerDetails travellerDetails)
	{

		final List<TravellerData> travellerData = new ArrayList<>();

		travellerDetails.getTravellerForms().forEach(tf -> {

			final PassengerInformationForm passengerInformation = tf.getPassengerInformation();

			final PassengerInformationData passengerInformationData = new PassengerInformationData();

			final TitleData title = new TitleData();
			title.setCode(passengerInformation.getTitle());
			passengerInformationData.setTitle(title);
			passengerInformationData.setFirstName(passengerInformation.getFirstname());
			passengerInformationData.setSurname(passengerInformation.getLastname());
			passengerInformationData.setGender(passengerInformation.getGender());
			passengerInformationData.setReasonForTravel(passengerInformation.getReasonForTravel());
			passengerInformationData.setSaveDetails(true);
			passengerInformationData.setEmail(passengerInformation.getEmail());

			if (StringUtils.isNotBlank(tf.getUid()))
			{
				passengerInformationData.setSavedTravellerUId(tf.getUid());
			}

			final PassengerTypeData passengerType = new PassengerTypeData();
			passengerType.setCode(passengerInformation.getPassengerTypeCode());

			passengerInformationData.setPassengerType(passengerType);

			final TravellerData traveller = new TravellerData();
			traveller.setFormId(tf.getFormId());
			traveller.setLabel(tf.getLabel());
			traveller.setBooker(tf.getBooker() == null ? false : tf.getBooker());
			traveller.setUid(tf.getUid());
			traveller.setTravellerInfo(passengerInformationData);

			if (StringUtils.isNotBlank(tf.getSelectedSavedTravellerUId()))
			{
				traveller.setSavedTravellerUid(tf.getSelectedSavedTravellerUId());
			}

			travellerData.add(traveller);
		});
		return travellerData;
	}

	private TravellerDetails getTravellerForms(final List<TravellerData> travellers)
	{
		final List<TravellerData> passengers = travellers.stream()
				.filter(traveller -> traveller.getTravellerInfo() instanceof PassengerInformationData)
				.collect(Collectors.toList());
		final List<TravellerForm> travellerForms = new ArrayList<>();

		passengers.forEach(passenger -> {
			final PassengerInformationData passengerInformationData = (PassengerInformationData) passenger.getTravellerInfo();
			final PassengerInformationForm passengerInformationForm = new PassengerInformationForm();
			passengerInformationForm.setPassengerTypeCode(passengerInformationData.getPassengerType().getCode());
			passengerInformationForm.setPassengerTypeName(passengerInformationData.getPassengerType().getName());
			passengerInformationForm.setEmail(passengerInformationData.getEmail());
			passengerInformationForm.setFirstname(passengerInformationData.getFirstName());
			passengerInformationForm.setLastname(passengerInformationData.getSurname());
			passengerInformationForm.setGender(passengerInformationData.getGender());
			if (Objects.nonNull(passengerInformationData.getTitle()))
			{
				passengerInformationForm.setTitle(passengerInformationData.getTitle().getName());
			}
			passengerInformationForm.setReasonForTravel(passengerInformationData.getReasonForTravel());
			final TravellerForm travellerForm = new TravellerForm();
			travellerForm.setLabel(passenger.getLabel());
			travellerForm.setUid(passenger.getUid());
			travellerForm.setSelectedSavedTravellerUId(passenger.getSavedTravellerUid());
			travellerForm.setPassengerInformation(passengerInformationForm);
			travellerForm.setSpecialAssistance(Objects.nonNull(passenger.getSpecialRequestDetail()));
			travellerForm.setBooker(passenger.isBooker());
			travellerForms.add(travellerForm);
		});

		Collections.sort(travellerForms);

		final TravellerDetails travellerDetails = new TravellerDetails();
		travellerDetails.setTravellerForms(travellerForms);

		return travellerDetails;
	}

	@RequestMapping(value = "/my-account/remove-saved-passenger/{passengerUid}", method = RequestMethod.GET)
	public String getRemoveSavedPassengersPage(@PathVariable final String passengerUid, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(passengerUid))
		{
			redirectModel.addFlashAttribute(ERROR, true);
			return REDIRECT_PREFIX + MY_ACCOUNT_URL + ACCOUNT_SAVED_PASSENGERS_CMS_PAGE;
		}

		final TravellerData removedTraveller = travellerFacade.removeSavedTraveller(passengerUid);

		if (removedTraveller == null)
		{
			redirectModel.addFlashAttribute(ERROR, true);
			return REDIRECT_PREFIX + MY_ACCOUNT_URL + ACCOUNT_SAVED_PASSENGERS_CMS_PAGE;
		}

		final PassengerInformationData passengerInformation = (PassengerInformationData) removedTraveller.getTravellerInfo();
		final String travellerName = passengerInformation.getTitle().getName() + " " + passengerInformation.getFirstName() + " "
				+ passengerInformation.getSurname();

		redirectModel.addFlashAttribute(REMOVED_TRAVELLER, travellerName);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_SAVED_PASSENGERS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_SAVED_PASSENGERS_CMS_PAGE));

		return REDIRECT_PREFIX + MY_ACCOUNT_URL + ACCOUNT_SAVED_PASSENGERS_CMS_PAGE;
	}

	@RequestMapping(value = "/my-account/customer-address-book", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(ADDRESS_DATA_ATTR, userFacade.getAddressBook());

		storeCmsPageInModel(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_ADDRESS_BOOK));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/customer-addressform", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("addressCode") final String addressCode,
			@RequestParam("countryIsoCode") final String countryIsoCode, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		populateModelRegionAndCountry(model, countryIsoCode);

		final BCFAddressForm bcfAddressForm = new BCFAddressForm();
		bcfAddressForm.setShippingAddress(true);
		model.addAttribute(ADDRESS_FORM_ATTR, bcfAddressForm);
		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode)
					&& countryIsoCode.equals(addressData.getCountry().getIsocode()))
			{
				model.addAttribute(ADDRESS_DATA_ATTR, addressData);
				bcfAddressDataUtil.convert(addressData, bcfAddressForm);
				break;
			}
		}
		return "fragments/address/countryAddressForm";
	}

	@RequestMapping(value = "/my-account/customer-add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addAddress(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		final BCFAddressForm bcfAddressForm = getPreparedAddressForm();
		model.addAttribute(ADDRESS_FORM_ATTR, bcfAddressForm);
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL, getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	protected BCFAddressForm getPreparedAddressForm()
	{
		final CustomerData currentCustomerData = travelCustomerFacade.getCurrentCustomer();
		final BCFAddressForm bcfAddressForm = new BCFAddressForm();
		bcfAddressForm.setFirstName(currentCustomerData.getFirstName());
		bcfAddressForm.setLastName(currentCustomerData.getLastName());
		bcfAddressForm.setTitleCode(currentCustomerData.getTitleCode());
		return bcfAddressForm;
	}

	@RequestMapping(value = "/my-account/customer-add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addAddress(final BCFAddressForm bcfAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(bcfAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpAddressFormAfterError(bcfAddressForm, model);
			model.addAttribute(ADDRESS_FORM_ATTR, bcfAddressForm);
			return getViewForPage(model);
		}

		final AddressData newAddress = bcfAddressDataUtil.convertToVisibleAddressData(bcfAddressForm);

		if (userFacade.isAddressBookEmpty())
		{
			newAddress.setDefaultAddress(true);
		}
		else
		{
			newAddress.setDefaultAddress(
					bcfAddressForm.getDefaultAddress() != null && bcfAddressForm.getDefaultAddress().booleanValue());
		}

		populateModelRegionAndCountry(model, bcfAddressForm.getCountryIso());
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(isDefaultAddress(bcfAddressForm.getAddressId())));

		userFacade.addAddress(newAddress);


		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added",
				null);

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}


	@RequestMapping(value = "/my-account/customer-edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddress(@PathVariable("addressCode") final String addressCode, final Model model)
			throws CMSItemNotFoundException
	{
		final BCFAddressForm bcfAddressForm = new BCFAddressForm();
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		model.addAttribute(ADDRESS_FORM_ATTR, bcfAddressForm);
		final List<AddressData> addressBook = userFacade.getAddressBook();
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(CollectionUtils.isEmpty(addressBook)));


		for (final AddressData addressData : addressBook)
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode))
			{
				model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(addressData.getCountry().getIsocode()));
				model.addAttribute(COUNTRY_ATTR, addressData.getCountry().getIsocode());
				model.addAttribute(ADDRESS_DATA_ATTR, addressData);
				bcfAddressDataUtil.convert(addressData, bcfAddressForm);

				if (isDefaultAddress(addressData.getId()))
				{
					bcfAddressForm.setDefaultAddress(Boolean.TRUE);
					model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.TRUE);
				}
				else
				{
					bcfAddressForm.setDefaultAddress(Boolean.FALSE);
					model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
				}
				break;
			}
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL, getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute("edit", Boolean.TRUE);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/customer-edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	@RequireHardLogIn
	public String editAddress(final BCFAddressForm bcfAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(bcfAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpAddressFormAfterError(bcfAddressForm, model);
			model.addAttribute(ADDRESS_FORM_ATTR, bcfAddressForm);
			return getViewForPage(model);
		}

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final AddressData newAddress = bcfAddressDataUtil.convertToVisibleAddressData(bcfAddressForm);

		if (Boolean.TRUE.equals(bcfAddressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
		}

		model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(bcfAddressForm.getCountryIso()));
		model.addAttribute(COUNTRY_ATTR, bcfAddressForm.getCountryIso());
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(isDefaultAddress(bcfAddressForm.getAddressId())));

		userFacade.editAddress(newAddress);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.updated",
				null);
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
			{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/set-default-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);
		userFacade.setDefaultAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.default.address.changed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	protected void setUpAddressFormAfterError(final BCFAddressForm bcfAddressForm, final Model model)
	{
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(isDefaultAddress(bcfAddressForm.getAddressId())));
		if (bcfAddressForm.getCountryIso() != null)
		{
			populateModelRegionAndCountry(model, bcfAddressForm.getCountryIso());
		}
	}

	@RequestMapping(value = "/edit-entry/{journeyRef:.*}/{odRef:.*}", method = RequestMethod.GET)
	public String clearFormAndRedirectToHome(final Model model, @PathVariable("journeyRef") final int journeyRef,
			@PathVariable("odRef") final int odRef, final RedirectAttributes redirectModel)
	{
		final CartModel sessionCart = bcfTravelCartService.getSessionCart();
		final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
		if (!Objects.isNull(sessionCart))
		{

			final List<AbstractOrderEntryModel> transportEntries = sessionCart.getEntries().stream()
					.filter(entry -> entry.getActive() && OrderEntryType.TRANSPORT.equals(entry.getType())
							&& journeyRef == entry.getJourneyReferenceNumber()).collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(transportEntries))
			{
				final List<AbstractOrderEntryModel> outboundEntries = transportEntries.stream()
						.filter(e -> e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0).collect(
								Collectors.toList());

				final List<AbstractOrderEntryModel> inboundEntries = transportEntries.stream()
						.filter(e -> e.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 1).collect(
								Collectors.toList());

				final List<AbstractOrderEntryModel> outboundVehicleEntries = StreamUtil.safeStream(outboundEntries)
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.VEHICLE)).collect(
								Collectors.toList());

				final List<AbstractOrderEntryModel> inboundVehicleEntries = StreamUtil.safeStream(inboundEntries)
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.VEHICLE)).collect(
								Collectors.toList());

				final List<AbstractOrderEntryModel> outboundPassengerEntries = outboundEntries.stream()
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.PASSENGER)).collect(
								Collectors.toList());

				final List<AbstractOrderEntryModel> inboundPassengerEntries = inboundEntries.stream()
						.filter(e -> e.getTravelOrderEntryInfo().getTravellers().stream().findFirst().get().getType().equals(
								TravellerType.PASSENGER)).collect(
								Collectors.toList());

				//RouteInfo Form
				final RouteInfoForm routeInfoForm = new RouteInfoForm();
				final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

				if (!CollectionUtils.isEmpty(outboundEntries))
				{
					final AbstractOrderEntryModel cartEntryModel = outboundEntries.stream().findFirst().get();
					final TransportOfferingModel cartEntryTransportOffering = cartEntryModel.getTravelOrderEntryInfo()
							.getTransportOfferings().stream().findFirst().get();
					final TravelOrderEntryInfoModel travelOrderEntryInfo = cartEntryModel.getTravelOrderEntryInfo();
					final TravelRouteModel travelRouteModel = travelOrderEntryInfo.getTravelRoute();

					routeInfoForm.setTripType(TripType.SINGLE.getCode());
					routeInfoForm.setArrivalLocation(travelRouteModel.getDestination().getCode());
					routeInfoForm.setArrivalLocationName(
							travelRouteModel.getDestination().getLocation().getName() + " - " + travelRouteModel.getDestination()
									.getName() + "(" + travelRouteModel.getDestination().getCode() + ")");
					routeInfoForm.setArrivalLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());
					routeInfoForm.setDepartingDateTime(simpleDateFormat.format(cartEntryTransportOffering.getDepartureTime()));
					routeInfoForm.setDepartureLocation(travelRouteModel.getOrigin().getCode());
					routeInfoForm.setDepartureLocationName(
							travelRouteModel.getOrigin().getLocation().getName() + " - " + travelRouteModel.getOrigin().getName() + "("
									+ travelRouteModel.getOrigin().getCode() + ")");
					routeInfoForm.setDepartureLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());

					routeInfoForm.setRouteType(travelRouteModel.getRouteType().getCode());
					routeInfoForm.setWalkOnRoute(Objects.nonNull(travelRouteModel.getWalkOn()) ? travelRouteModel.getWalkOn() : false);
					routeInfoForm.setAllowsWalkOnOptions(Objects.nonNull(travelRouteModel.getAllowsWalkOnOptions()) ?
							travelRouteModel.getAllowsWalkOnOptions() : false);
				}
				if (!CollectionUtils.isEmpty(inboundEntries))
				{
					final AbstractOrderEntryModel cartEntryModel = inboundEntries.stream().findFirst().get();
					final TransportOfferingModel cartEntryTransportOffering = cartEntryModel.getTravelOrderEntryInfo()
							.getTransportOfferings().stream().findFirst().get();
					routeInfoForm.setTripType(TripType.RETURN.getCode());
					routeInfoForm.setReturnDateTime(simpleDateFormat.format(cartEntryTransportOffering.getDepartureTime()));
				}

				bcfFareFinderForm.setRouteInfoForm(routeInfoForm);

				//Passenger form
				final PassengerInfoForm passengerInfoForm = new PassengerInfoForm();
				populatePassengerInfoFormForOutbound(journeyRef, bcfFareFinderForm, outboundPassengerEntries, passengerInfoForm);

				populatePassengerInfoFormForInbound(journeyRef, bcfFareFinderForm, inboundPassengerEntries, passengerInfoForm);


				bcfFareFinderForm.setPassengerInfoForm(passengerInfoForm);
				bcfFareFinderForm.getPassengerInfoForm().setBasePassengers(
						BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfBasePassengers")));
				bcfFareFinderForm.getPassengerInfoForm().setAdditionalPassengers(
						BCFPropertiesUtils
								.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfAdditionalPassengers")));
				bcfFareFinderForm.getPassengerInfoForm().setNorthernPassengers(
						BCFPropertiesUtils
								.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfNorthernPassengers")));
				bcfFerrySelectionUtil.sortPassengers(passengerInfoForm);

				// Vehicle Form
				final VehicleInfoForm vehicleInfoForm = new VehicleInfoForm();
				bcfvoyageaddonComponentControllerUtil.setVehicleData(vehicleInfoForm);
				bcfFerrySelectionUtil.setVehicleDimensionThreshold(bcfFareFinderForm);
				final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();

				populateVehicleInfoFormForOutbound(journeyRef, outboundVehicleEntries, vehicleInfoForm, vehicleTypeQuantityDataList);

				populateVehicleInfoFormForInbound(journeyRef, inboundVehicleEntries, vehicleInfoForm, vehicleTypeQuantityDataList);
				bcfFareFinderForm.setVehicleInfoForm(vehicleInfoForm);
			}

			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		}

		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ROUTE_PAGE;
	}

	private void populateVehicleInfoFormForInbound(final int journeyRef, final List<AbstractOrderEntryModel> inboundVehicleEntries,
			final VehicleInfoForm vehicleInfoForm, final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList)
	{
		if (!CollectionUtils.isEmpty(inboundVehicleEntries))
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			final AbstractOrderEntryModel cartEntryModel = inboundVehicleEntries.stream().findFirst().get();
			final TravelOrderEntryInfoModel travelOrderEntryInfo = cartEntryModel.getTravelOrderEntryInfo();
			final TravelRouteModel travelRouteModel = travelOrderEntryInfo.getTravelRoute();
			final BCFVehicleInformationModel bcfVehicleInformationModel = (BCFVehicleInformationModel) travelOrderEntryInfo
					.getTravellers().stream().findFirst().get().getInfo();
			final VehicleTypeModel vehicleTypeModel = ((BCFVehicleInformationModel) travelOrderEntryInfo
					.getTravellers().stream().findFirst().get().getInfo()).getVehicleType();
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			vehicleTypeData.setCode(vehicleTypeModel.getType().getCode());
			vehicleTypeData.setName(vehicleTypeModel.getName());
			vehicleTypeData.setCategory(vehicleTypeModel.getVehicleCategoryType().getCode());
			populateVehicleTypeQuantityData(journeyRef, 1, vehicleTypeQuantityData, travelRouteModel, bcfVehicleInformationModel,
					vehicleTypeData);
			vehicleTypeQuantityDataList.add(vehicleTypeQuantityData);
			vehicleInfoForm.setVehicleInfo(vehicleTypeQuantityDataList);
			vehicleInfoForm.setReturnWithDifferentVehicle(!bcfFerrySelectionUtil.isReturningWithSameVehicle(vehicleInfoForm));
		}
	}

	private void populateVehicleInfoFormForOutbound(final int journeyRef,
			final List<AbstractOrderEntryModel> outboundVehicleEntries,
			final VehicleInfoForm vehicleInfoForm, final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList)
	{
		if (!CollectionUtils.isEmpty(outboundVehicleEntries))
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			final AbstractOrderEntryModel cartEntryModel = outboundVehicleEntries.stream().findFirst().get();
			final TravelOrderEntryInfoModel travelOrderEntryInfo = cartEntryModel.getTravelOrderEntryInfo();
			final TravelRouteModel travelRouteModel = travelOrderEntryInfo.getTravelRoute();
			final BCFVehicleInformationModel bcfVehicleInformationModel = (BCFVehicleInformationModel) travelOrderEntryInfo
					.getTravellers().stream().findFirst().get().getInfo();
			final VehicleTypeModel vehicleTypeModel = ((BCFVehicleInformationModel) travelOrderEntryInfo
					.getTravellers().stream().findFirst().get().getInfo()).getVehicleType();
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			vehicleTypeData.setCode(vehicleTypeModel.getType().getCode());
			vehicleTypeData.setName(vehicleTypeModel.getName());
			vehicleTypeData.setCategory(vehicleTypeModel.getVehicleCategoryType().getCode());
			vehicleInfoForm.setTravellingWithVehicle(true);
			populateVehicleTypeQuantityData(journeyRef, 0, vehicleTypeQuantityData, travelRouteModel, bcfVehicleInformationModel,
					vehicleTypeData);
			vehicleTypeQuantityDataList.add(vehicleTypeQuantityData);
			vehicleInfoForm.setVehicleInfo(vehicleTypeQuantityDataList);
		}
	}

	private void populatePassengerInfoFormForInbound(final int journeyRef, final BCFFareFinderForm bcfFareFinderForm,
			final List<AbstractOrderEntryModel> inboundPassengerEntries, final PassengerInfoForm passengerInfoForm)
	{
		if (!CollectionUtils.isEmpty(inboundPassengerEntries))
		{

			final List<AbstractOrderEntryModel> abstractOrderEntryModels = inboundPassengerEntries.stream().filter(
					entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
							&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
							FareProductType.PASSENGER)).collect(Collectors.toList());

			final Map<String, Long> travellersMap = abstractOrderEntryModels.stream()
					.map(entry -> entry.getTravelOrderEntryInfo().getTravellers().iterator().next()).collect(Collectors
							.groupingBy(traveller -> ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
									.getCode(), Collectors.counting()));



			final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
			final List<PassengerTypeData> passengerTypesDatas = travellerSortStrategy
					.sortPassengerTypes(passengerTypeFacade.getPassengerTypes());

			for (final PassengerTypeData passengerTypeData : passengerTypesDatas)
			{
				final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
				passengerTypeQuantityData.setPassengerType(passengerTypeData);
				passengerTypeQuantityData.setQuantity(0);

				final Long passengerQuantity = travellersMap.get(passengerTypeData.getCode());

				if (passengerQuantity != null && passengerQuantity > 0)
				{
					passengerTypeQuantityData.setQuantity(passengerQuantity.intValue());
				}
				passengerTypeQuantityList.add(passengerTypeQuantityData);
			}
			passengerInfoForm.setInboundPassengerTypeQuantityList(passengerTypeQuantityList);
			bcfFareFinderForm.setPassengerInfoForm(passengerInfoForm);
			passengerInfoForm.setCarryingDangerousGoodsInReturn(bcfTravelCartFacade.isCarryingDangerousGoodsWalkOn(journeyRef, 1));

			final List<String> largeItemsCodes = BCFPropertiesUtils.convertToList(
					bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES));

			final Map<String, List<ProductData>> ancillaryProductMap = inboundPassengerEntries.stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE) && !largeItemsCodes
							.stream()
							.anyMatch(s -> StringUtils.equalsIgnoreCase(s, entry.getProduct().getCode()))).
							collect(Collectors.groupingBy(
									entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator()
											.next()
											.getInfo()).getPassengerType().getCode(),
									Collectors.mapping(entry -> ancillaryProductConverter.convert(entry.getProduct()),
											Collectors.toList())));

			final Map<String, List<AbstractOrderEntryModel>> specialReqEntriesMap = inboundPassengerEntries.stream().filter(
					entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail() != null
							&& CollectionUtils.isNotEmpty(
							entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail()
									.getSpecialServiceRequest())).collect(
					Collectors.groupingBy(
							entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator().next()
									.getInfo()).getPassengerType().getCode()));

			final List<AccessibilityRequestData> accessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil
					.getFilteredAccessibilityRequestDataList(
							bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList());

			setAccessibilityData(passengerTypeQuantityList, ancillaryProductMap, specialReqEntriesMap,
					accessibilityRequestDataList);
			passengerInfoForm.setInboundaccessibilityRequestDataList(accessibilityRequestDataList);
			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				passengerInfoForm.setAccessibilityNeedsCheckInbound(true);
			}
			bcfFerrySelectionUtil
					.setLargeItemData(bcfFareFinderForm.getPassengerInfoForm(), inboundPassengerEntries, largeItemsCodes, 1);
			passengerInfoForm
					.setReturnWithDifferentPassenger(!bcfFerrySelectionUtil.isReturningWithSamePassenger(passengerInfoForm));

		}
	}

	private void populatePassengerInfoFormForOutbound(final int journeyRef, final BCFFareFinderForm bcfFareFinderForm,
			final List<AbstractOrderEntryModel> outboundPassengerEntries, final PassengerInfoForm passengerInfoForm)
	{
		if (!CollectionUtils.isEmpty(outboundPassengerEntries))
		{

			final List<AbstractOrderEntryModel> abstractOrderEntryModels = outboundPassengerEntries.stream().filter(
					entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
							&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
							FareProductType.PASSENGER)).collect(Collectors.toList());

			final Map<String, Long> travellersMap = abstractOrderEntryModels.stream()
					.map(entry -> entry.getTravelOrderEntryInfo().getTravellers().iterator().next()).collect(Collectors
							.groupingBy(traveller -> ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
									.getCode(), Collectors.counting()));



			final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
			final List<PassengerTypeData> passengerTypesDatas = travellerSortStrategy
					.sortPassengerTypes(passengerTypeFacade.getPassengerTypes());

			for (final PassengerTypeData passengerTypeData : passengerTypesDatas)
			{
				final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
				passengerTypeQuantityData.setPassengerType(passengerTypeData);
				passengerTypeQuantityData.setQuantity(0);

				final Long passengerQuantity = travellersMap.get(passengerTypeData.getCode());

				if (passengerQuantity != null && passengerQuantity > 0)
				{
					passengerTypeQuantityData.setQuantity(passengerQuantity.intValue());
				}
				passengerTypeQuantityList.add(passengerTypeQuantityData);
			}
			passengerInfoForm.setPassengerTypeQuantityList(passengerTypeQuantityList);
			bcfFareFinderForm.setPassengerInfoForm(passengerInfoForm);
			passengerInfoForm.setCarryingDangerousGoodsInOutbound(bcfTravelCartFacade.isCarryingDangerousGoodsWalkOn(journeyRef, 0));

			final List<String> largeItemsCodes = BCFPropertiesUtils.convertToList(
					bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES));

			final Map<String, List<ProductData>> ancillaryProductMap = outboundPassengerEntries.stream()
					.filter(entry -> entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE) && !largeItemsCodes
							.stream()
							.anyMatch(s -> StringUtils.equalsIgnoreCase(s, entry.getProduct().getCode()))).
							collect(Collectors.groupingBy(
									entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator()
											.next()
											.getInfo()).getPassengerType().getCode(),
									Collectors.mapping(entry -> ancillaryProductConverter.convert(entry.getProduct()),
											Collectors.toList())));

			final Map<String, List<AbstractOrderEntryModel>> specialReqEntriesMap = outboundPassengerEntries.stream().filter(
					entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
							&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail() != null
							&& CollectionUtils.isNotEmpty(
							entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail()
									.getSpecialServiceRequest())).collect(
					Collectors.groupingBy(
							entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator().next()
									.getInfo()).getPassengerType().getCode()));

			final List<AccessibilityRequestData> accessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil
					.getFilteredAccessibilityRequestDataList(
							bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList());

			setAccessibilityData(passengerTypeQuantityList, ancillaryProductMap, specialReqEntriesMap,
					accessibilityRequestDataList);
			passengerInfoForm.setAccessibilityRequestDataList(accessibilityRequestDataList);
			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				passengerInfoForm.setAccessibilityNeedsCheckOutbound(true);
			}
			bcfFerrySelectionUtil
					.setLargeItemData(bcfFareFinderForm.getPassengerInfoForm(), outboundPassengerEntries, largeItemsCodes, 0);
		}
	}

	private void populateVehicleTypeQuantityData(final int journeyRef, final int odRef,
			final VehicleTypeQuantityData vehicleTypeQuantityData,
			final TravelRouteModel travelRouteModel, final BCFVehicleInformationModel bcfVehicleInformationModel,
			final VehicleTypeData vehicleTypeData)
	{
		vehicleTypeQuantityData.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityData.setTravelRouteCode(travelRouteModel.getCode());
		vehicleTypeQuantityData.setQty(1);
		vehicleTypeQuantityData.setAllowMotorcycle(BooleanUtils.isTrue(travelRouteModel.getAllowMotorcycles()));
		vehicleTypeQuantityData.setCarryingLivestock(bcfVehicleInformationModel.getCarryingLivestock());
		vehicleTypeQuantityData.setLength(bcfVehicleInformationModel.getLength());
		vehicleTypeQuantityData.setHeight(bcfVehicleInformationModel.getHeight());
		vehicleTypeQuantityData.setWidth(bcfVehicleInformationModel.getWidth());
		vehicleTypeQuantityData.setWeight(bcfVehicleInformationModel.getWeight());
		vehicleTypeQuantityData.setNumberOfAxis(bcfVehicleInformationModel.getNumberOfAxis());
		vehicleTypeQuantityData.setVehicleWithSidecarOrTrailer(bcfVehicleInformationModel.getVehicleWithSidecarOrTrailer());
		vehicleTypeQuantityData
				.setCarryingDangerousGoods(bcfTravelCartFacade.isCarryingDangerousGoodsWithVehicle(journeyRef, odRef));
	}

	private void setAccessibilityData(final List<PassengerTypeQuantityData> passengerTypeQuantityList,
			final Map<String, List<ProductData>> ancillaryProductMap,
			final Map<String, List<AbstractOrderEntryModel>> specialReqEntriesMap,
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		List<ProductData> ancillaryProducts = null;
		List<AbstractOrderEntryModel> specialReqsEntries = null;

		for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
		{

			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				ancillaryProducts = ancillaryProductMap.get(passengerTypeQuantityData.getPassengerType().getCode());
			}

			if (MapUtils.isNotEmpty(specialReqEntriesMap))
			{
				specialReqsEntries = specialReqEntriesMap.get(passengerTypeQuantityData.getPassengerType().getCode());
			}


			for (int i = 0; i < passengerTypeQuantityData.getQuantity(); i++)
			{

				final AccessibilityRequestData accessibilityRequestData = new AccessibilityRequestData();
				accessibilityRequestData.setPassengerType(passengerTypeQuantityData.getPassengerType());
				accessibilityRequestData.setSpecialServiceRequestDataList(
						bcfvoyageaddonComponentControllerUtil
								.getSpecialServiceDisplayList(bcfSpecialAssistanceFacade.getAllSpecialServiceRequest(),
										bcfvoyageaddonComponentControllerUtil.getSpecialServiceRequestProductsSequence()));
				accessibilityRequestData.setAncillaryRequestDataList(
						bcfvoyageaddonComponentControllerUtil
								.getProductListByDisplaySequence(bcfvoyageaddonComponentControllerUtil.getAccessibilityProductData(),
										bcfvoyageaddonComponentControllerUtil.getAccessibilityRequestProductsSequence()));
				accessibilityRequestDataList.add(accessibilityRequestData);
				if (CollectionUtils.isNotEmpty(ancillaryProducts) && ancillaryProducts.size() > i)
				{

					accessibilityRequestData.setSelection(ancillaryProducts.get(i).getCode());
				}
				populateServiceRequest(specialReqsEntries, i, accessibilityRequestData);

			}
		}
	}

	private void populateServiceRequest(final List<AbstractOrderEntryModel> specialReqsEntries, final int i,
			final AccessibilityRequestData accessibilityRequestData)
	{
		if (CollectionUtils.isNotEmpty(specialReqsEntries) && specialReqsEntries.size() > i)
		{

			final List<String> specialReqs = specialReqsEntries.get(i).getTravelOrderEntryInfo().getTravellers().iterator()
					.next()
					.getSpecialRequestDetail()
					.getSpecialServiceRequest().stream().map(specialReq -> specialReq.getCode()).collect(
							Collectors.toList());

			for (final SpecialServiceRequestData specialServiceRequestData : accessibilityRequestData
					.getSpecialServiceRequestDataList())
			{
				if (specialReqs.stream().anyMatch(specialReq -> specialReq.equals(specialServiceRequestData.getCode())))
				{
					specialServiceRequestData.setSelection(true);
				}
			}

		}
	}

	@RequestMapping(value = "/edit-sailing", method = RequestMethod.GET)
	public String editSailingAndRedirectToHome(final Model model, final RedirectAttributes redirectAttributes)
	{
		final int journeyRef = bcfTravelCartFacade.getCurrentJourneyRefNumber();
		redirectAttributes.addFlashAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ROUTE_PAGE;
	}

	/**
	 * Method checks if address is set as defaulth
	 *
	 * @param addressId - identifier for address to check
	 * @return true if address is default, false if address is not default
	 */
	protected boolean isDefaultAddress(final String addressId)
	{
		final AddressData defaultAddress = userFacade.getDefaultAddress();
		return defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressId);
	}

	protected void populateModelRegionAndCountry(final Model model, final String countryIsoCode)
	{
		model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute(COUNTRY_ATTR, countryIsoCode);
	}

	/**
	 * @return the adultTitles
	 */
	protected String[] getAdultTitles()
	{
		return adultTitles;
	}

	/**
	 * @param adultTitles the adultTitles to set
	 */
	@Required
	public void setAdultTitles(final String[] adultTitles)
	{
		this.adultTitles = adultTitles;
	}

	protected String[] getChildrenTitles()
	{
		return childrenTitles;
	}

	@Required
	public void setChildrenTitles(final String[] childrenTitles)
	{
		this.childrenTitles = childrenTitles;
	}

	protected AddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	public void setAddressValidator(final AddressValidator addressValidator)
	{
		this.addressValidator = addressValidator;
	}

	protected I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	public void setI18NFacade(final I18NFacade i18nFacade)
	{
		i18NFacade = i18nFacade;
	}
}
