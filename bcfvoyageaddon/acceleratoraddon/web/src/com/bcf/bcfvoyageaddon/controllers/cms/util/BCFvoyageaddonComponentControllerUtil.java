/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfvoyageaddon.controllers.cms.util;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.enums.FareSelectionDisplayOrder;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.fare.sorting.strategies.AbstractResultSortingStrategy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.PassengerInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.VehicleType.VehicleTypeFacade;
import com.bcf.facades.bcffacades.BcfProductFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.AccessibilitytData;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.rits.cloning.Cloner;


@Component("bcfvoyageaddonComponentControllerUtil")
public class BCFvoyageaddonComponentControllerUtil
{
	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfProductFacade")
	private BcfProductFacade bcfProductFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "fareSelectionSortingStrategyMap")
	private Map<FareSelectionDisplayOrder, AbstractResultSortingStrategy> fareSelectionSortingStrategyMap;

	@Resource(name = "vehicleTypeFacade")
	private VehicleTypeFacade vehicleTypeFacade;

	public void setPassengerTypeQuantityList(final FareFinderForm fareFinderForm, final int quantity,
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		fareFinderForm.setTravellingWithChildren(false);

		for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
		{
			if (passengerTypeQuantityData.getPassengerType().getCode().equals(BcfvoyageaddonWebConstants.PASSENGER_TYPE_ADULT))
			{
				passengerTypeQuantityData.setQuantity(quantity);
			}
		}
		fareFinderForm.setPassengerTypeQuantityList(passengerTypeQuantityList);
	}

	public void setPassengerTypeQuantityList(final PassengerInfoForm passengerInfoForm, final int quantity,
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		final Cloner clone = new Cloner();
		passengerInfoForm.setTravellingWithChildren(false);

		for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
		{
			if (passengerTypeQuantityData.getPassengerType().getCode().equals(BcfvoyageaddonWebConstants.PASSENGER_TYPE_ADULT))
			{
				passengerTypeQuantityData.setQuantity(quantity);
			}
		}
		passengerInfoForm.setPassengerTypeQuantityList(passengerTypeQuantityList);
		passengerInfoForm.setInboundPassengerTypeQuantityList(clone.deepClone(passengerTypeQuantityList));
		passengerInfoForm.setReturnWithDifferentPassenger(false);
	}

	public void setVehicleData(final FareFinderForm fareFinderForm)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypeQuantityDataOutBound = new VehicleTypeQuantityData();
		final VehicleTypeQuantityData vehicleTypeQuantityDataInBound = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeData.setCategory(StringUtils.EMPTY);
		vehicleTypeQuantityDataOutBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityDataInBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataOutBound);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataInBound);
		fareFinderForm.setVehicleInfo(vehicleTypeQuantityList);
		fareFinderForm.setTravellingWithVehicle(
				Boolean.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultTravelingWithVehicle")));
		fareFinderForm.setReturnWithDifferentVehicle(
				Boolean.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultReturnWithDifferentVehicle")));
	}

	public void setVehicleData(final VehicleInfoForm vehicleInfoForm)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityList = new ArrayList<>();
		final VehicleTypeQuantityData vehicleTypeQuantityDataOutBound = new VehicleTypeQuantityData();
		final VehicleTypeQuantityData vehicleTypeQuantityDataInBound = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		final VehicleTypeData vehicleTypeData1 = new VehicleTypeData();
		vehicleTypeData.setCategory(StringUtils.EMPTY);
		vehicleTypeQuantityDataOutBound.setVehicleType(vehicleTypeData);
		vehicleTypeQuantityDataInBound.setVehicleType(vehicleTypeData1);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataOutBound);
		vehicleTypeQuantityList.add(vehicleTypeQuantityDataInBound);
		vehicleInfoForm.setVehicleInfo(vehicleTypeQuantityList);
		vehicleInfoForm.setTravellingWithVehicle(
				Boolean.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultTravelingWithVehicle")));
		vehicleInfoForm.setReturnWithDifferentVehicle(
				Boolean.parseBoolean(getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultReturnWithDifferentVehicle")));
	}

	public static void setReturnVehicleData(final VehicleInfoForm vehicleInfoForm)
	{
		final VehicleTypeQuantityData vehicleTypeQuantityDataInBound = new VehicleTypeQuantityData();
		final VehicleTypeData vehicleTypeData = new VehicleTypeData();
		vehicleTypeQuantityDataInBound.setVehicleType(vehicleTypeData);
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDatas=new ArrayList<>();
		vehicleTypeQuantityDatas.addAll(vehicleInfoForm.getVehicleInfo());
		vehicleTypeQuantityDatas.add(vehicleTypeQuantityDataInBound);
		vehicleInfoForm.setVehicleInfo(vehicleTypeQuantityDatas);
	}

	public FareFinderForm getFareFinderForm(final Model model, final HttpServletRequest request)
	{
		FareFinderForm fareFinderForm = null;
		if (model.containsAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM))
		{
			fareFinderForm = (FareFinderForm) model.asMap().get(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		}
		if (fareFinderForm == null && request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM) != null)
		{
			fareFinderForm = (FareFinderForm) request.getAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM);
		}
		return validateRequiredAttributesArePresent(fareFinderForm);
	}

	/**
	 * Method checks to see if the required attributes on the FareFinderForm are populated. The Method will return the
	 * FareFinderForm object if required fields are populated otherwise it will return null.
	 *
	 * @param fareFinderForm
	 * @return FareFinderForm
	 */
	private FareFinderForm validateRequiredAttributesArePresent(final FareFinderForm fareFinderForm)
	{
		if (fareFinderForm != null && fareFinderForm.getDepartureLocation() == null && fareFinderForm.getArrivalLocation() == null
				&& fareFinderForm.getCabinClass() == null && fareFinderForm.getDepartingDateTime() == null
				&& fareFinderForm.getPassengerTypeQuantityList().isEmpty())
		{
			return null;
		}
		return fareFinderForm;
	}

	public void setVehicleCodeCategoryInfo(final List<VehicleTypeQuantityData> vehicleInfo)
	{
		for (int i = 0; i < vehicleInfo.size(); i++)
		{
			final String code = vehicleInfo.get(i).getVehicleType().getCode();
			String vehicleCode = "";
			String vehicleCategory = "";
			if (StringUtils.isNotEmpty(code) && code.contains("_"))
			{
				vehicleCode = StringUtils.substringBefore(code, "_");
				vehicleCategory = StringUtils.substringAfter(code, "_");
				if (vehicleCategory.contains("_") && StringUtils
						.equalsIgnoreCase(BcfCoreConstants.MOTORCYCLE_WITH_TRAILER, vehicleCode))
				{
					if (StringUtils.equalsIgnoreCase("trailer", StringUtils.substringBefore(vehicleCategory, "_")))
					{
						vehicleInfo.get(i).setLengthProvidedForMCT(true);
					}
					vehicleCategory = StringUtils.substringAfter(vehicleCategory, "_");
				}
				final VehicleTypeData vehicleTypeData = vehicleTypeFacade.getVehicleTypeForVehicleCode(vehicleCode);
				vehicleInfo.get(i).getVehicleType().setCode(vehicleCode);
				vehicleInfo.get(i).getVehicleType().setName(vehicleTypeData.getName());
				vehicleInfo.get(i).getVehicleType().setCategory(vehicleCategory);
				vehicleInfo.get(i).getVehicleType().setDescription(vehicleTypeData.getDescription());
			}
			if (StringUtils.equalsIgnoreCase(BcfCoreConstants.MOTORCYCLE_STANDARD, vehicleInfo.get(i).getVehicleType().getCode()) ||
					StringUtils
							.equalsIgnoreCase(BcfCoreConstants.MOTORCYCLE_WITH_TRAILER, vehicleInfo.get(i).getVehicleType().getCode()))
			{
				vehicleInfo.get(i).setCarryingLivestock(false);
			}
		}
	}

	public List<SpecialServiceRequestData> getSpecialServiceDisplayList(final List<SpecialServiceRequestData> products,
			final List<String> displaySequence)
	{
		final List<SpecialServiceRequestData> specialServiceDisplayList = new ArrayList<>();
		for (final String productCode : displaySequence)
		{
			for (final SpecialServiceRequestData b : products)
			{
				if (productCode.equalsIgnoreCase(b.getCode()))
				{
					specialServiceDisplayList.add(b);
				}
			}
		}
		return specialServiceDisplayList;
	}

	public List<PassengerTypeQuantityData> getFilteredPassengerTypeQuantityDataList(
			final List<PassengerTypeQuantityData> passengerTypeQuantityDataList)
	{
		return passengerTypeQuantityDataList.stream()
				.filter(passengerTypeQuantityData -> passengerTypeQuantityData.getQuantity() > 0).collect(Collectors.toList());
	}

	public List<AccessibilityRequestData> getFilteredAccessibilityRequestDataList(
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		if (accessibilityRequestDataList != null)
		{
			return accessibilityRequestDataList;
		}
		return new ArrayList<>();
	}

	public List<String> getSpecialServiceRequestProductsSequence()
	{
		return BCFPropertiesUtils.convertToList(getBcfConfigurablePropertiesService()
				.getBcfPropertyValue(BcfvoyageaddonWebConstants.SPECIAL_SERVICE_REQUEST_PRODUCTS));
	}

	public List<ProductData> getProductListByDisplaySequence(final List<ProductData> products, final List<String> displaySequence)
	{
		final List<ProductData> productListByDisplaySequence = new ArrayList<>();
		for (final String productCode : displaySequence)
		{
			for (final ProductData a : products)
			{
				if (productCode.equalsIgnoreCase(a.getCode()))
				{
					productListByDisplaySequence.add(a);
				}
			}
		}
		return productListByDisplaySequence;
	}

	public List<ProductData> getAccessibilityProductData()
	{
		final List<ProductData> accessibilityProducts = new ArrayList<>();
		BCFPropertiesUtils.convertToList(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS))
				.forEach(code -> {
							final ProductData accessibilityProduct = bcfProductFacade.getAccessibilityProduct(code);
							if (Objects.nonNull(accessibilityProduct))
							{
								accessibilityProducts.add(accessibilityProduct);
							}
						}
				);
		return accessibilityProducts;
	}

	public List<String> getAccessibilityRequestProductsSequence()
	{
		return BCFPropertiesUtils.convertToList(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
	}

	public List<AccessibilityRequestData> getAccessibilityRequestDataList(
			final List<AccessibilityRequestData> previousRequestDataList,
			final List<AccessibilityRequestData> newRequestDataList)
	{
		if (CollectionUtils.isNotEmpty(previousRequestDataList))
		{
			final List<AccessibilityRequestData> accessibilityRequestData = new ArrayList<>();
			final Map<String, List<AccessibilityRequestData>> previousRequestDataMap = StreamUtil.safeStream(previousRequestDataList)
					.collect(Collectors.groupingBy(o -> o.getPassengerType().getBcfCode()));
			final Map<String, List<AccessibilityRequestData>> newRequestDataMap = StreamUtil.safeStream(newRequestDataList)
					.collect(Collectors.groupingBy(o -> o.getPassengerType().getBcfCode()));
			StreamUtil.safeStream(newRequestDataMap.entrySet()).forEach(newEntry -> {
				if (CollectionUtils.isNotEmpty(previousRequestDataMap.get(newEntry.getKey())))
				{
					final List<AccessibilityRequestData> previousList = previousRequestDataMap.get(newEntry.getKey());
					final List<AccessibilityRequestData> newList = newEntry.getValue();
					final int changeFactor = newList.size() - previousList.size();
					populateAccessibilityRequestData(accessibilityRequestData, previousList, newList, changeFactor);
				}
				else
				{
					accessibilityRequestData.addAll(newEntry.getValue());
				}
			});
			return accessibilityRequestData;
		}
		else
		{
			return newRequestDataList;
		}
	}

	private void populateAccessibilityRequestData(final List<AccessibilityRequestData> accessibilityRequestData,
			final List<AccessibilityRequestData> previousList, final List<AccessibilityRequestData> newList, final int changeFactor)
	{
		if (changeFactor == 0)
		{
			accessibilityRequestData.addAll(previousList);
		}
		else if (changeFactor < 0)
		{
			for (int i = 0; i < newList.size(); i++)
			{
				accessibilityRequestData.add(previousList.get(i));
			}
		}
		else if (changeFactor > 0)
		{
			accessibilityRequestData.addAll(previousList);
			for (int i = 0; i < changeFactor; i++)
			{
				accessibilityRequestData.add(newList.get(i));
			}
		}
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public void setPassengerDetails(final FareSearchRequestData fareSearchRequestData,
			final List<PassengerTypeQuantityData> passengerTypeQuantityDataList,
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		fareSearchRequestData.setPassengerTypes(passengerTypeQuantityDataList);
		if (CollectionUtils.isNotEmpty(accessibilityRequestDataList))
		{
			fareSearchRequestData.setAccessibilities(getAccessibility(accessibilityRequestDataList));
			fareSearchRequestData.setSpecialServices(getSpecialServices(accessibilityRequestDataList));
		}
	}

	public List<AccessibilitytData> getAccessibility(final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		final List<AccessibilitytData> accessibilitytDataList = new ArrayList<>();
		for (final AccessibilityRequestData accessibilityRequestData : accessibilityRequestDataList)
		{
			if (StringUtils.isNotEmpty(accessibilityRequestData.getSelection()))
			{
				final AccessibilitytData accessibilitytData = new AccessibilitytData();
				accessibilitytData.setCode(accessibilityRequestData.getSelection());
				accessibilitytData.setPassengerType(accessibilityRequestData.getPassengerType().getCode());
				accessibilitytData.setType(bcfProductFacade.getAmenityTypeForCode(accessibilityRequestData.getSelection()));
				accessibilitytData.setPassengerUid(accessibilityRequestData.getPassengerUid());
				accessibilitytDataList.add(accessibilitytData);
			}
		}
		return accessibilitytDataList;
	}

	public List<SpecialServicesData> getSpecialServices(final List<AccessibilityRequestData> specialServicesDataList)
	{
		final List<SpecialServicesData> specialServices = new ArrayList<>();
		int index = 0;
		for (final AccessibilityRequestData specialServicesData : specialServicesDataList)
		{
			for (final SpecialServiceRequestData specialService : specialServicesData.getSpecialServiceRequestDataList())
			{
				if (specialService.isSelection() || StringUtils
						.equalsIgnoreCase(BcfCoreConstants.OTHER_ACCESSIBILITY_REQUIREMENT_CODE, specialService.getCode()))
				{
					final SpecialServicesData specialServiceData = new SpecialServicesData();
					specialServiceData.setCode(specialService.getCode());
					specialServiceData.setName(specialService.getName());
					specialServiceData.setPassengerType(specialServicesData.getPassengerType().getCode());
					specialServiceData.setIndex(index);
					specialServiceData.setDescription(specialService.getDescription());
					specialServiceData.setPassengerUid(specialServicesData.getPassengerUid());
					specialServices.add(specialServiceData);
				}
			}
			index++;
		}
		return specialServices;
	}

	/**
	 * Checks how many priced itineraries are available for the relevant journey
	 *
	 * @param fareSelectionData - full response object
	 * @param referenceNumber   - specifies which leg of the journey it is
	 * @return number of options for the journey
	 */
	public int countJourneyOptions(final FareSelectionData fareSelectionData, final int referenceNumber)
	{
		int count = 0;
		if (!CollectionUtils.isEmpty(fareSelectionData.getPricedItineraries()))
		{
			for (final PricedItineraryData pricedItinerary : fareSelectionData.getPricedItineraries())
			{
				if (pricedItinerary.getOriginDestinationRefNumber() == referenceNumber && pricedItinerary.isAvailable())
				{
					count++;
				}
			}
		}
		return count;
	}

	public void setVehicleCodeAndCategory(final VehicleTypeData vehicleTypeData)
	{
		final String code = vehicleTypeData.getCode();
		if (code.contains("_"))
		{
			vehicleTypeData.setCode(StringUtils.substringBefore(code, "_"));
			vehicleTypeData.setCategory(StringUtils.substringAfter(code, "_"));
		}
	}

	/**
	 * Method to sort the FareSelectionData based on the displayOrder. If displayOrder is null, empty or not a valid
	 * FareSelectionDisplayOrder enum, the default sorting by departureDate is applied.
	 *
	 * @param fareSelectionData as the FareSelectionData to be sorted
	 * @param displayOrder      as the String corresponding to a sortingStrategy
	 * @param model
	 */
	public void sortFareSelectionData(final FareSelectionData fareSelectionData, final String displayOrder, final Model model)
	{

		final FareSelectionDisplayOrder displayOrderOption = Arrays.asList(FareSelectionDisplayOrder.values()).stream()
				.filter(val -> val.toString().equals(displayOrder)).findAny().orElse(null);

		if (displayOrderOption != null)
		{
			final AbstractResultSortingStrategy sortingStrategy = fareSelectionSortingStrategyMap.get(displayOrderOption);
			sortingStrategy.sortFareSelectionData(fareSelectionData);
			model.addAttribute(BcfvoyageaddonWebConstants.SELECTED_SORTING, displayOrderOption.toString());
		}
	}
}
