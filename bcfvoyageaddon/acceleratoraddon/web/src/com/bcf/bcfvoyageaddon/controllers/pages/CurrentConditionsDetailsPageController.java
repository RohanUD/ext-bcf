/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.CurrentConditionsData;
import de.hybris.platform.commercefacades.travel.ServiceNoticeData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.facades.currentconditions.TerminalSchedulessIntegrationFacade;
import com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData;
import com.bcf.integration.currentconditionsResponse.data.RouteArrivalDepartureData;
import com.bcf.integration.currentconditionsResponse.data.ScheduleArrivalDepartureData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/current-conditions/")
public class CurrentConditionsDetailsPageController extends CurrentConditionsBaseController
{
	private static final Logger LOG = Logger
			.getLogger(com.bcf.bcfvoyageaddon.controllers.pages.CurrentConditionsDetailsPageController.class);

	private static final String ROUTE_NAMES_PATTERN = "{routeName}";
	private static final String ROUTE_CODE_PATTERN = "{routeCode}";
	private static final String CURRENT_CONDITIONS_ROUTE = "route";
	private static final String AND_CONDITION = ", and ";
	private static final String SELECTED_SAILING = "selectedSailing";

	@Resource
	private BCFTransportFacilityFacade transportFacilityFacade;

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "bcfTransportFacilityFacade")
	private BCFTransportFacilityFacade bcfTransportFacilityFacade;
	
	@Resource(name = "terminalSchedulesIntegrationFacade")
	private TerminalSchedulessIntegrationFacade terminalSchedulesIntegrationFacade;

	@RequestMapping(value = ROUTE_CODE_PATTERN, method = RequestMethod.POST)
	public String getCurrentConditions(@PathVariable final String routeCode, @RequestParam final String selectedSailing,
			final Model model)
	{
		final String departureLocation = routeCode.split("-")[0];
		final String arrivalLocation = routeCode.split("-")[1];
		getCurrentConditionsDataData(model, departureLocation, arrivalLocation);
		model.addAttribute(SELECTED_SAILING, selectedSailing);
		return BcfstorefrontaddonControllerConstants.Views.Pages.CurrentConditions.CurrentConditionsPage;
	}

	@RequestMapping(value = ROUTE_NAMES_PATTERN + "/" + ROUTE_CODE_PATTERN, method = RequestMethod.GET)
	public String getCurrentConditions(@PathVariable final String routeName, @PathVariable final String routeCode,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{

		final String departureLocation = routeCode.split("-")[0];
		final String arrivalLocation = routeCode.split("-")[1];
		getSessionService().setAttribute(BcfFacadesConstants.DEPARTURE_LOCATION, departureLocation);
		getSessionService().setAttribute(BcfFacadesConstants.ARRIVAL_LOCATION, arrivalLocation);

		final DestinationAndOriginForTravelRouteData routeInfo = getCurrentConditionsDataData(model, departureLocation,
				arrivalLocation);
		model.addAttribute(SELECTED_SAILING, request.getParameter(SELECTED_SAILING));

		final TravelRouteData reverseRoute = bcfTravelRouteFacade
				.getDirectTravelRoutesForCurrentConditions(arrivalLocation, departureLocation);
		if (Objects.nonNull(reverseRoute) && reverseRoute.isCurrentConditionsEnabled())
		{
			model.addAttribute("showSwitchDirection", true);
		}

		model.addAttribute("transportFacilityData", transportFacilityFacade.getTravelFacilityDetailsForCode(departureLocation));
		model.addAttribute("downloadFareGuideUrl", currentConditionsDetailsFacade.getRouteFaresGuideUrl());

		final String viewForPage = getViewForPage(model, BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_DETAILS_CMS_PAGE);

		final ContentPageModel contentPage = getContentPageForLabelOrId(
				BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_DETAILS_CMS_PAGE);

		final String[] arguments = { routeInfo.getDepartureTerminalName() + "-" + routeInfo.getArrivalTerminalName() };
		setUpMetaDataForContentPage(model, contentPage, arguments);
		storeCmsPageInModel(model, contentPage);
		setUpDynamicOGGraphData(model, contentPage, request, arguments);
		return viewForPage;
	}

	protected DestinationAndOriginForTravelRouteData getCurrentConditionsDataData(final Model model,
			final String departureLocation, final String arrivalLocation)
	{

		final DestinationAndOriginForTravelRouteData routeInfo = bcfTransportFacilityFacade
				.getTravelRouteInfoForSourceAndDestination(departureLocation, arrivalLocation);
		model.addAttribute("routeInfo", routeInfo);
		model.addAttribute("pageRefreshTime", currentConditionsDetailsFacade.getPageRefreshTime());
		final List<ServiceNoticeData> serviceNotices = currentConditionsDetailsFacade
				.getServiceNoticesForSourceAndDestination(departureLocation, arrivalLocation);
		model.addAttribute("serviceNotices", serviceNotices);

		try
		{
			final TravelRouteData routeData = bcfTravelRouteFacade
					.getDirectTravelRoutesForCurrentConditions(departureLocation, arrivalLocation);

			// Getting route number has to be be moved to Auto Suggestions in Current Conditions Component

			if (Objects.isNull(routeData))
			{
				LOG.error(
						"Current Conditions no direct route found for the given terminals/locations, " + departureLocation
								+ AND_CONDITION
								+ arrivalLocation);
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.internal.error", null);
			}
			else if (StringUtils.isEmpty(routeData.getRoute()))
			{
				LOG.error(
						"Current Conditions route number not found for the given terminals/locations, " + departureLocation
								+ AND_CONDITION
								+ arrivalLocation);
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.internal.error", null);
			}
			else
			{
				model.addAttribute("reservable", routeData.isReservable());
				model.addAttribute("ferryTrackingLink", routeData.getFerryTrackingLink());
				final CurrentConditionsData currentConditionsData = currentConditionsDetailsFacade
						.getCurrentConditionsForRoute(departureLocation, CURRENT_CONDITIONS_ROUTE + routeData.getRoute());
				ScheduleArrivalDepartureData[] currentConditionsNextSailingsResponseData = null;
				if (currentConditionsDetailsFacade.displayTerminalsSchedule(currentConditionsData))
				{
					currentConditionsNextSailingsResponseData = terminalSchedulesIntegrationFacade
							.getCurrentConditionsNextSailingsForRoute(departureLocation, routeData.getRoute(),
									LocalDate.now().plusDays(1L).toString(), BcfFacadesConstants.CURRENT_CONDITIONS_DETAILS_LIMIT_VALUE);
					model.addAttribute("nextSailings", currentConditionsNextSailingsResponseData);
				}
				isSailingsFull(currentConditionsData, model);
				getTomorrowDate(model);
				model.addAttribute("noMoreSailings", currentConditionsDetailsFacade.noMoreSailingsForToday(currentConditionsData));
				model.addAttribute("nextSailingTime",
						currentConditionsDetailsFacade
								.nextSailingsForToday(currentConditionsData, currentConditionsNextSailingsResponseData));
				if (!checkRoutesExists(currentConditionsData))
				{
					LOG.error("Current Conditions route found for the given terminals/locations, " + departureLocation + AND_CONDITION
							+ arrivalLocation);
					GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.cc.no.route.found", null);
				}
				else
				{
					model.addAttribute("currentConditionsData", currentConditionsData);
					final String duration = calculateSailingDuration(currentConditionsData);
					model.addAttribute("sailingDuration", duration);
				}
			}
		}
		catch (final IntegrationException intEx)
		{
			LOG.error("Current Conditions Integration Exception occurred while access the services for the locations "
					+ departureLocation + AND_CONDITION + arrivalLocation, intEx);
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.cc.notAvailable.label", null);
		}
		return routeInfo;
	}

	private String calculateSailingDuration(final CurrentConditionsData currentConditionsData)
	{

		for (final CurrentConditionsResponseData currentConditionsResponseData : currentConditionsData
				.getCurrentConditionsResponseData())
		{
			for (final String key : currentConditionsResponseData.getArrivalDepartures().keySet())
			{
				final List<RouteArrivalDepartureData> routeArrivalDepartureDataList = currentConditionsResponseData
						.getArrivalDepartures()
						.get(key);
				for (final RouteArrivalDepartureData routeArrivalDepartureData : routeArrivalDepartureDataList)
				{
					final String scheduledDeparture = routeArrivalDepartureData.getScheduledDeparture();
					final String scheduledArrival = routeArrivalDepartureData.getScheduledArrival();
					if (StringUtils.isNotEmpty(scheduledDeparture) && StringUtils.isNotEmpty(scheduledArrival))
					{
						final DateTimeFormatter df = DateTimeFormatter
								.ofPattern(BcfCoreConstants.CURRENT_CONDITIONS_SAILING_DURATION_DATE_FORMAT);

						final LocalDateTime depLdt = LocalDateTime.parse(scheduledDeparture, df);
						final LocalDateTime arrLdt = LocalDateTime.parse(scheduledArrival, df);
						final long diff = ChronoUnit.MINUTES.between(depLdt, arrLdt);
						return BCFDateUtils.convertIntoHoursAndMin(diff);
					}
				}
			}
		}
		return null;
	}

	private boolean checkRoutesExists(final CurrentConditionsData currentConditionsData)
	{
		if (Objects.isNull(currentConditionsData) || CollectionUtils
				.isEmpty(currentConditionsData.getCurrentConditionsResponseData()))
		{
			return false;
		}
		else
		{
			final CurrentConditionsResponseData currentConditionsResponseData = currentConditionsData
					.getCurrentConditionsResponseData().iterator().next();
			final Map<String, List<RouteArrivalDepartureData>> arrivalDepartures = currentConditionsResponseData
					.getArrivalDepartures();
			for (final String key : arrivalDepartures.keySet()) //NOSONAR
			{
				if (CollectionUtils.isEmpty(arrivalDepartures.get(key)))
				{
					return false;
				}
			}
		}

		return true;
	}
}
