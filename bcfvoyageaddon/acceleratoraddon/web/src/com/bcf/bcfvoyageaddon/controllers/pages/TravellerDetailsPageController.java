/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.VehicleInformationData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TripType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.security.impl.B2BCheckOutAuthenticatonValidator;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.forms.PassengerInformationForm;
import com.bcf.bcfvoyageaddon.forms.VehicleInformationForm;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.validation.BCFTravellerFormValidator;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.travel.data.BCFTravellerDataPerJourney;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.integrations.core.exception.IntegrationException;
import com.rits.cloning.Cloner;


/**
 * Controller for Traveller Details page
 */
@Controller
@SessionAttributes({ BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, BcfFacadesConstants.BCF_FARE_FINDER_FORM })
@RequestMapping("/traveller-details")
public class TravellerDetailsPageController extends TravelAbstractPageController
{
	private static final String TRAVELLER_DETAILS_CMS_PAGE = "travellerDetailsPage";
	private static final String ADULT_AGE_RANGE = "adultsAgeRange";
	private static final String CHILDREN_AGE_RANGE = "childrenAgeRange";
	private static final String INFANT_AGE_RANGE = "infantsAgeRange";
	private static final String TITLES = "titles";
	private static final String ADULT_TITLES = "adultsTitles";
	private static final String CHILDREN_TITLES = "childrenTitles";
	private static final String REASON_FOR_TRAVEL_OPTIONS = "reasonForTravelOptions";
	private static final String SAVED_TRAVELLERS = "savedTravellers";
	private static final String SAVED_VEHICLE_TRAVELLERS = "savedVehicleTravellers";
	private static final String RESERVATION_DATA = "reservationData";
	private static final String AMEND_TRAVELLER_FAILED = "text.page.managemybooking.amend.traveller.failed";
	private static final String REFERER = "referer";
	// Populated through properties files
	private String[] adultAgesRange;
	private String[] childrenAgeRange;
	private String[] infantAgeRange;
	private String[] adultTitles;
	private String[] childrenTitles;

	private static final String NEXT_URL = "/traveller-details/next";
	private static final String HOME_PAGE_PATH = "/";
	private static final Logger LOG = Logger.getLogger(TravellerDetailsPageController.class);

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "cartFacade")
	private TravelCartFacade cartFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "bcfTravellerFormValidator")
	private BCFTravellerFormValidator travellerFormValidator;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "b2BCheckOutAuthenticatonValidator")
	private B2BCheckOutAuthenticatonValidator b2BCheckOutAuthenticatonValidator;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getTravellerDetailsPage(@RequestParam(value = "journeyRefNum", required = false) final Integer journeyRefNo,
			@RequestParam(value = BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, required = false) final Integer odRefNo,
			@RequestParam(value = "orderCode", required = false) final String orderCode, final HttpServletRequest request,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final String referer = request.getHeader(REFERER);

		if (referer.contains(BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH)) {
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PAYMENT_PAGE_PATH;
		}

		if(StringUtils.isNotEmpty(orderCode))
		{
			try{
				bcfTravelCartFacade.createAndSetAmendableCart(orderCode,true);

			}
			catch (final IntegrationException ex)
			{
				LOG.error(ex);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
				return REDIRECT_PREFIX + request.getHeader(REFERER);
			}

			catch (final BcfOrderUpdateException | CalculationException  ex)
			{
				LOG.error(ex);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_TRAVELLER_FAILED);
				return REDIRECT_PREFIX + request.getHeader(REFERER);
			}
		}
		final CartModel sessionAmendedCart = bcfTravelCartService.getExistingSessionAmendedCart();

		if (setOrderData(orderCode, model, sessionAmendedCart))
			return REDIRECT_PREFIX + HOME_PAGE_PATH;

		if (getTravelCustomerFacade().isCurrentUserB2bCustomer())
		{
			final ValidationResults validationResult = b2BCheckOutAuthenticatonValidator.validate(redirectAttributes);
			if (!ValidationResults.SUCCESS.getResult().equals(validationResult.getResult()))
			{
				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}

		}

		if (!cartFacade.isCurrentCartValid())
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}

		if (!bookingFacade.isCurrentCartOfType(OrderEntryType.TRANSPORT.getCode()))
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}
		final ContentPageModel contentPage = getContentPageForLabelOrId(TRAVELLER_DETAILS_CMS_PAGE);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID,contentPage.isDisplayOrderId());

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}

		final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
		final List<String> routes = Stream.of(routeType.split(",")).collect(Collectors.toList());

		final CartFilterParamsDto cartFilterParams = bcfTravelCartFacade
				.initializeCartFilterParams(journeyRefNo, odRefNo, false, routes);
			final BCFTravellersData bcfTravellersData = travellerFacade.getTravellersGroupedByJourney(cartFilterParams);
			model.addAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, bcfTravellersData);
			model.addAttribute(ADULT_AGE_RANGE, adultAgesRange);
			model.addAttribute(CHILDREN_AGE_RANGE, childrenAgeRange);
			model.addAttribute(INFANT_AGE_RANGE, infantAgeRange);
			model.addAttribute(TITLES, userFacade.getTitles());
			model.addAttribute(REASON_FOR_TRAVEL_OPTIONS, travellerFacade.getReasonForTravelTypes());

			final List<TravellerData> savedTravellers = travellerFacade.getSavedTravellersForCurrentUser();
			final List<TravellerData> savedPassengerTravellers = savedTravellers.stream()
					.filter(travellerData -> TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER.equals(travellerData.getTravellerType()))
					.collect(Collectors.toList());

			final List<TravellerData> savedVehicleTravellers = savedTravellers.stream()
					.filter(travellerData -> BcfFacadesConstants.TRAVELLER_TYPE_VEHICLE.equals(travellerData.getTravellerType()))
					.collect(Collectors.toList());

			model.addAttribute(SAVED_TRAVELLERS, savedPassengerTravellers);
			model.addAttribute(SAVED_VEHICLE_TRAVELLERS, savedVehicleTravellers);

			final List<TitleData> titles = userFacade.getTitles();

			model.addAttribute(ADULT_TITLES, getTravellerTitle(adultTitles, titles));
			model.addAttribute(CHILDREN_TITLES, getTravellerTitle(childrenTitles, titles));

			final ReservationData reservationData = reservationFacade.getCurrentReservationData();
			model.addAttribute(RESERVATION_DATA, reservationData);

			model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, NEXT_URL);
			model.addAttribute(BcfCoreConstants.IS_FERRY_OPTION_BOOKING, cartService.getSessionCart().isFerryOptionBooking());
			bcfOriginalBookingUtil.setAdditionalAttributes(model);
			setActionAttributes(model);

		return getViewForPage(model);
	}

	private void setActionAttributes(final Model model)
	{
		final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionCart.getBookingJourneyType());
		model.addAttribute(BcfFacadesConstants.MAX_SAILING_ALLOWED,
				bcfTravelCheckoutFacade.isMaxSailingExceedsForUser(sessionCart));
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_ANOTHER_SAILING,
				BcfFacadesConstants.REMOVE_FORM_AND_REDIRECT_TO_HOME_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.LOGIN, BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH_ONLY);
		model.addAttribute(BcfvoyageaddonWebConstants.CONTINUE_AS_GUEST, NEXT_URL);
	}

	private boolean setOrderData(
			@RequestParam(value = "orderCode", required = false) final String orderCode,
			final Model model, final CartModel sessionAmendedCart)
	{
		if ( StringUtils.isNotEmpty(orderCode))
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
			if (Objects.isNull(sessionAmendedCart)){

				final Boolean amend = bookingFacade.amendOrder(orderCode, bookingFacade.getCurrentUserUid());
				if (!amend)
				{
					// If cart was not created, return validation message
					return true;
				}
			}
			else if (!StringUtils.equals(orderCode, sessionAmendedCart.getCode()))
			{
				bcfTravelCartService.removeSessionCart();
				final Boolean amend = bookingFacade.amendOrder(orderCode, bookingFacade.getCurrentUserUid());
				if (!amend)
				{
					// If cart was not created, return validation message
					return true;
				}
			}
			model.addAttribute("orderCode", orderCode);
		}
		return false;
	}

	@ModelAttribute("countries")
	public List<CountryData> populateCountries()
	{
		return bcfTravelCheckoutFacade.getDeliveryCountries();
	}

	@ResponseBody
	@RequestMapping(value = "/refresh-provinces", method = RequestMethod.GET, produces = "application/json")
	public List<RegionData> refreshProvinces(@RequestParam(value = "countryIso", required = false) final String countryIso, final Model model)
	{
		if (StringUtils.isNotBlank(countryIso))
		{
			return i18NFacade.getRegionsForCountryIso(countryIso);
		}
		return Collections.emptyList();
	}


	/**
	 * @param bcfTravellersData
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/guest-checkout", method = { RequestMethod.GET, RequestMethod.POST })
	public String saveTravellerDetailsAndGuestCheckout(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA) final BCFTravellersData bcfTravellersData,
			@ModelAttribute(value = "orderCode") final String orderCode,final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel,final HttpServletRequest request, final HttpServletResponse response,
			final SessionStatus sessionStatus)
			throws CMSItemNotFoundException
	{
		return saveTravellerDetails(bcfTravellersData,redirectModel,bindingResult,request,model,sessionStatus,nextPage(),orderCode);
	}

	@RequestMapping(value = "/checkout-login", method = { RequestMethod.GET, RequestMethod.POST })
	public String saveTravellerDetailsAndNavigateToCheckout(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA) final BCFTravellersData bcfTravellersData,
			@ModelAttribute(value = "orderCode") final String orderCode,final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response,
			final SessionStatus sessionStatus) throws CMSItemNotFoundException
	{
		String nextUrl=REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH_ONLY;
		return saveTravellerDetails(bcfTravellersData, redirectModel,bindingResult,request,model,sessionStatus,nextUrl,orderCode);
	}

	@RequestMapping(value = "/validate-traveller-details-forms", method = RequestMethod.POST)
	public String validateTravellerDetailsForm(
			@Valid @ModelAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA) final BCFTravellersData bcfTravellersData,
			final BindingResult bindingResult,
			final Model model)
	{
		return getValidationErrorMessage(bindingResult, model);
	}

	/**
	 * creates PassengerInformationData from PassengerInformationForm coming from TravellerDetailsPage
	 */
	protected PassengerInformationData createPassengerInformationData(final PassengerInformationForm passengerInformation)
	{
		final PassengerInformationData passengerInformationData = new PassengerInformationData();

		final TitleData title = new TitleData();
		title.setCode(passengerInformation.getTitle());
		passengerInformationData.setTitle(title);
		passengerInformationData.setFirstName(passengerInformation.getFirstname());
		passengerInformationData.setSurname(passengerInformation.getLastname());
		passengerInformationData.setGender(passengerInformation.getGender());
		passengerInformationData.setReasonForTravel(passengerInformation.getReasonForTravel());
		passengerInformationData.setSaveDetails(passengerInformation.isSaveDetails());
		passengerInformationData.setEmail(passengerInformation.getEmail());

		if (passengerInformation.isFrequentFlyer())
		{
			passengerInformationData.setMembershipNumber(passengerInformation.getFrequentFlyerMembershipNumber());
		}

		if (StringUtils.isNotBlank(passengerInformation.getSelectedSavedTravellerUId()))
		{
			passengerInformationData.setSavedTravellerUId(passengerInformation.getSelectedSavedTravellerUId());
		}

		final PassengerTypeData passengerType = new PassengerTypeData();
		passengerType.setCode(passengerInformation.getPassengerTypeCode());

		passengerInformationData.setPassengerType(passengerType);
		return passengerInformationData;
	}

	/**
	 * creates VehicleInformationData from VehicleInformationForm coming from TravellerDetailsPage
	 */
	protected VehicleInformationData createVehicleInformationData(final VehicleInformationForm vehicleInformation)
	{
		final VehicleInformationData vehicleInformationData = new VehicleInformationData();

		vehicleInformationData.setCarryingLivestock(vehicleInformation.isCarryingLivestock());
		vehicleInformationData.setVehicleWithSidecarOrTrailer(vehicleInformation.isVehicleWithSidecarOrTrailer());
		vehicleInformationData.setLength(vehicleInformation.getVehicleLength());
		vehicleInformationData.setHeight(vehicleInformation.getVehicleHeight());
		vehicleInformationData.setWidth(vehicleInformation.getVehicleWidth());
		final VehicleTypeData vehicleData = new VehicleTypeData();
		vehicleData.setCategory(vehicleInformation.getVehicleCategory());
		vehicleData.setCode(vehicleInformation.getVehicleCode());
		vehicleData.setName(vehicleInformation.getVehicleName());
		vehicleInformationData.setVehicleType(vehicleData);
		vehicleInformationData.setLicensePlateNumber(vehicleInformation.getLicensePlateNumber());
		vehicleInformationData.setLicensePlateCountry(vehicleInformation.getLicensePlateCountry());
		vehicleInformationData.setLicensePlateProvince(vehicleInformation.getLicensePlateProvince());

		if (StringUtils.isNotBlank(vehicleInformation.getSelectedSavedTravellerUId()))
		{
			vehicleInformationData.setSavedTravellerUId(vehicleInformation.getSelectedSavedTravellerUId());
		}
		return vehicleInformationData;
	}


	/**
	 * Redirects user to the next checkout page which is payment details
	 *
	 * @return payment details page or payment type page
	 */
	protected String nextPage()
	{
		if (cartService.getSessionCart().isFerryOptionBooking())
		{
			return REDIRECT_PREFIX + "/checkout/ferry-option-booking-details";
		}
		return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.PAYMENT_PAGE_PATH;
	}

	/**
	 * Ajax controller which gets the traveller details for the uid.
	 *
	 * @param uid
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/get-traveller", method = RequestMethod.GET)
	public String getTraveller(@RequestParam("uid") final String uid, final Model model)
	{
		final List<TravellerData> travellers = travellerFacade.getSavedTravellersForCurrentUser();

		if (!travellers.isEmpty())
		{
			final Optional<TravellerData> optionalTravellerData = travellers.stream().filter(t -> t.getUid().equals(uid))
					.findFirst();
			if (optionalTravellerData.isPresent())
			{
				model.addAttribute("travellerData", optionalTravellerData.get());
			}

		}
		return BcfvoyageaddonControllerConstants.Views.Pages.TravellerDetails.JSONTravellerDetailsAuthentication;
	}

	/**
	 * Ajax controller which first checks if the user is an anonymous user and if not then it will get the users details
	 *
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/get-current-user-details", method = RequestMethod.GET)
	public String getCurrentUserDetails(final Model model)
	{
		final boolean isAnonymousUser = travellerFacade.isAnonymousUser();
		model.addAttribute("isAuthenticated", !travellerFacade.isAnonymousUser());

		if (!isAnonymousUser)
		{
			final TravellerData travellerData = travellerFacade.getCurrentUserDetails();

			if (travellerData != null)
			{
				model.addAttribute("travellerData", travellerData);
			}
		}
		return BcfvoyageaddonControllerConstants.Views.Pages.TravellerDetails.JSONTravellerDetailsAuthentication;
	}

	protected String getValidationErrorMessage(final BindingResult bindingResult, final Model model)
	{
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}
		return BcfvoyageaddonControllerConstants.Views.Pages.FormErrors.formErrorsResponse;
	}

	protected List<TitleData> getTravellerTitle(final String[] travellerTitles, final List<TitleData> titles)
	{
		final List<String> tt = new ArrayList<>();
		Collections.addAll(tt, travellerTitles);
		return titles.stream().filter(t -> tt.contains(t.getCode())).collect(Collectors.toList());
	}

	protected void disableCachingForResponse(final HttpServletResponse response)
	{
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	}


	private boolean isSamePassengersInbound(final BCFFareFinderForm bcfFareFinderForm)
	{
		return Objects.nonNull(bcfFareFinderForm) && StringUtils
				.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name())
				&& !bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger();
	}

	private boolean isSameVehicleInbound(final BCFFareFinderForm bcfFareFinderForm)
	{
		return Objects.nonNull(bcfFareFinderForm) && StringUtils
				.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name())
				&& !bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn() && bcfFareFinderForm.getVehicleInfoForm()
				.isTravellingWithVehicle() && !bcfFareFinderForm.getVehicleInfoForm().isReturnWithDifferentVehicle();
	}

	private void copyTravellerDetails(
			final BCFTravellerDataPerJourney bcfTravellerDataPerJourney)
	{
		final Cloner cloner = new Cloner();
		final List<TravellerData> outboundTravellerDataList = bcfTravellerDataPerJourney.getOutboundTravellerData();
		final List<TravellerData> inboundTravellerDataList = CollectionUtils
				.isNotEmpty(bcfTravellerDataPerJourney.getInboundTravellerData()) ?
				bcfTravellerDataPerJourney.getInboundTravellerData() :
				new ArrayList<>();

		if (!bcfTravellerDataPerJourney.isIsReturnWithDifferentPassenger())
		{
			List<TravellerData>  outboundFilteredTravellerDataList=StreamUtil.safeStream(outboundTravellerDataList).filter(travellerData->StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_PASSENGER, travellerData.getTravellerType())).collect(
					Collectors.toList());
			List<TravellerData>  inboundFilteredTravellerDataList=StreamUtil.safeStream(inboundTravellerDataList).filter(travellerData->StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_PASSENGER, travellerData.getTravellerType())).collect(
					Collectors.toList());


			if (CollectionUtils.isNotEmpty(inboundFilteredTravellerDataList))
			{

				List<TravellerData> inboundClonedPassengerDataList=new ArrayList();
				for(int i=0;i<outboundFilteredTravellerDataList.size();i++)
				{
					TravellerData travellerData = outboundFilteredTravellerDataList.get(i);
						final TravellerData inboundPassengerData = cloner.deepClone(travellerData);
						if (inboundFilteredTravellerDataList.size() > i)
						{
						inboundPassengerData.setLabel(inboundFilteredTravellerDataList.get(i).getLabel());
					}
					inboundClonedPassengerDataList.add(inboundPassengerData);
				}

				inboundTravellerDataList.removeAll(inboundFilteredTravellerDataList);
				inboundTravellerDataList.addAll(inboundClonedPassengerDataList);
			}


		}
		if (!bcfTravellerDataPerJourney.isIsReturnWithDifferentVehicle())
		{
			List<TravellerData>  outboundFilteredVehicleDataList=StreamUtil.safeStream(outboundTravellerDataList).filter(travellerData->StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, travellerData.getTravellerType())).collect(
					Collectors.toList());
			List<TravellerData>  inboundFilteredVehicleDataList=StreamUtil.safeStream(inboundTravellerDataList).filter(travellerData->StringUtils.equalsIgnoreCase(BcfCoreConstants.TRAVELLER_TYPE_VEHICLE, travellerData.getTravellerType())).collect(
					Collectors.toList());

			if (CollectionUtils.isNotEmpty(inboundFilteredVehicleDataList))
			{

				List<TravellerData> inboundClonedVehicleDataList=new ArrayList();
				for(int i=0;i<outboundFilteredVehicleDataList.size();i++)
				{
					TravellerData travellerData = outboundFilteredVehicleDataList.get(i);
						final TravellerData inboundVehicleData = cloner.deepClone(travellerData);
						if (inboundFilteredVehicleDataList.size() > i)
						{
						inboundVehicleData.setLabel(inboundFilteredVehicleDataList.get(i).getLabel());
					}
					inboundClonedVehicleDataList.add(inboundVehicleData);
				}
				inboundTravellerDataList.removeAll(inboundFilteredVehicleDataList);
				inboundTravellerDataList.addAll(inboundClonedVehicleDataList);
			}

		}
		bcfTravellerDataPerJourney.setInboundTravellerData(inboundTravellerDataList);
	}

	private String saveTravellerDetails(BCFTravellersData bcfTravellersData, RedirectAttributes redirectModel
			, BindingResult bindingResult,HttpServletRequest request, final Model model,final SessionStatus sessionStatus
			, final String nextUrl, final String orderCode) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			return getTravellerDetailsPage(null, null, StringUtils.EMPTY, request, model, redirectModel);
		}
		Integer journeyRefNumber=null ;
		Integer odRefNumber=null;
		try
		{
			for (final BCFTravellerDataPerJourney bcfTravellerDataPerJourney : bcfTravellersData.getTravellerDataPerJourney())
			{

				copyTravellerDetails(bcfTravellerDataPerJourney);
				if (CollectionUtils.isNotEmpty(bcfTravellerDataPerJourney.getOutboundTravellerData()) && CollectionUtils.isNotEmpty(
						bcfTravelCartFacade.getCartEntriesForRefNo(bcfTravellerDataPerJourney.getJourneyReferenceNumber(), 0)))
				{
					journeyRefNumber = bcfTravellerDataPerJourney.getJourneyReferenceNumber();
					odRefNumber = 0;
					final AddBundleToCartRequestData outboundAddBundleToCartRequestData = travellerFacade
							.getRequestData(bcfTravellersData,
									bcfTravellerDataPerJourney.getJourneyReferenceNumber(), 0);

					travellerFacade.updateTravellerInformation(outboundAddBundleToCartRequestData,
							bcfTravellerDataPerJourney.getOutboundTravellerData());

				}

				if (CollectionUtils.isNotEmpty(bcfTravellerDataPerJourney.getInboundTravellerData()) && CollectionUtils
						.isNotEmpty(
								bcfTravelCartFacade.getCartEntriesForRefNo(bcfTravellerDataPerJourney.getJourneyReferenceNumber(), 1)))
				{
					journeyRefNumber = bcfTravellerDataPerJourney.getJourneyReferenceNumber();
					odRefNumber = 1;
					final AddBundleToCartRequestData inboundAddBundleToCartRequestData = travellerFacade
							.getRequestData(bcfTravellersData,
									bcfTravellerDataPerJourney.getJourneyReferenceNumber(), 1);
					travellerFacade.updateTravellerInformation(inboundAddBundleToCartRequestData,
							bcfTravellerDataPerJourney.getInboundTravellerData());
				}
			}
		}
		catch (final IntegrationException ex)
		{
			LOG.error("MakeBooking Failure ", ex);

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());

			if(bcfTravelCartService.isAmendmentCart(null)){
				return REDIRECT_PREFIX+"/traveller-details?journeyRefNum="+journeyRefNumber+"&odRefNum="+odRefNumber+"&orderCode="
						+orderCode;
			}else{
				return REDIRECT_PREFIX+"/traveller-details";
			}
		}

		if (request.getRequestURI().endsWith("clear"))
		{
			sessionStatus.setComplete();
		}

		return nextUrl;
	}


	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getAdultAgesRange()
	{
		return adultAgesRange;
	}

	/**
	 * @param adultAgesRange
	 */
	public void setAdultAgesRange(final String[] adultAgesRange)
	{
		this.adultAgesRange = adultAgesRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getChildrenAgeRange()
	{
		return childrenAgeRange;
	}

	/**
	 * @param childrenAgeRange
	 */
	public void setChildrenAgeRange(final String[] childrenAgeRange)
	{
		this.childrenAgeRange = childrenAgeRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	public String[] getInfantAgeRange()
	{
		return infantAgeRange;
	}

	/**
	 * @param infantAgeRange
	 */
	public void setInfantAgeRange(final String[] infantAgeRange)
	{
		this.infantAgeRange = infantAgeRange;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getAdultTitles()
	{
		return adultTitles;
	}

	/**
	 * @param adultTitles
	 */
	public void setAdultTitles(final String[] adultTitles)
	{
		this.adultTitles = adultTitles;
	}

	/**
	 * @return Array of Age Ranges
	 */
	protected String[] getChildrenTitles()
	{
		return childrenTitles;
	}

	/**
	 * @param childrenTitles
	 */
	public void setChildrenTitles(final String[] childrenTitles)
	{
		this.childrenTitles = childrenTitles;
	}

}
