/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.AddBundleToCartRequestData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TransportOfferingData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.annotations.CheckRequireLoginForBookingChange;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.FareSelectionUrlHelper;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.cart.AddProductToCartRequestData;
import com.bcf.facades.cart.BcfAddBundleToCartData;
import com.bcf.facades.cart.SelectedJourneyData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.MakeBookingIntegrationFacade;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.managebooking.BcfManageBookingFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.order.cancel.enums.CancelStatus;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.integration.ebooking.service.MakeBookingService;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@SessionAttributes(BcfFacadesConstants.BCF_FARE_FINDER_FORM)
public class TransportManageBookingController extends AbstractController
{
	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "travellerFacade")
	private TravellerFacade travellerFacade;

	@Resource(name = "packageFacade")
	private PackageFacade packageFacade;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "fareSelectionUrlHelper")
	private FareSelectionUrlHelper fareSelectionUrlHelper;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;

	@Resource(name = "makeBookingService")
	private MakeBookingService makeBookingService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "makeBookingIntegrationFacade")
	private MakeBookingIntegrationFacade makeBookingIntegrationFacade;

	@Resource(name = "bcfManageBookingFacade")
	private BcfManageBookingFacade bcfManageBookingFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	private static final String CANCEL_TRAVELLER_FAILED = "text.page.managemybooking.cancel.traveller.failed";
	private static final String AMEND_ANCILLARY_FAILED = "text.page.managemybooking.amend.ancillary.failed";
	private static final String REPLAN_JOURNEY_FAILED = "text.page.managemybooking.replan.journey.failed";
	private static final String SAILING_AMENDMENT_FAILED = "error.page.managemybooking.sailing.amendment.failed";
	private static final String CANCEL_TRAVELLER_SUCCESSFUL = "text.page.managemybooking.cancel.traveller.successful";
	private static final String REFUND_ORDER_RESULT = "text.page.managemybooking.refund.order.result";
	private static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String DEPARTURE_TIME_WARNING = "departureTimeWarning";
	private static final String DEPARTURE_TIME_WARNING_CODE = "text.page.managemybooking.cancel.sailing.modal.timerestriction.warning";
	private static final String CANCEL_ORDER_SUCCESSFUL = "text.page.managemybooking.cancel.order.successful";
	private static final String FAILURE_CANCEL_EBOOKINGS = "failureCancelEBookings";
	private static final String EXCEPTION_CANCEL_EBOOKINGS = "exceptionCancelEBookings";
	private static final String CANCEL_PARTIAL_ORDER_SUCCESSFUL = "text.page.managemybooking.cancel.partial.order.successful";
	private static final String CANCEL_PARTIAL_ORDER_FAILED = "text.page.managemybooking.cancel.partial.order.failed";
	private static final Logger LOG = Logger.getLogger(TransportManageBookingController.class);
	private static final String REFERER = "referer";

	@RequestMapping(value = "/manage-booking/replan-journey-request", method = RequestMethod.GET, produces = "application/json")
	@CheckRequireLoginForBookingChange
	public String getReplanJourneyModal(@RequestParam("orderCode") final String orderCode,
			@RequestParam("journeyRefNum") final int journeyRefNum, @RequestParam("odRefNum") final int odRefNum, final Model model)
	{

		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(orderCode, true);

		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex);
			model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			return BcfvoyageaddonControllerConstants.Views.Pages.Replan.ReplanJourneyResponse;
		}

		catch (final BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, REPLAN_JOURNEY_FAILED);
			return BcfvoyageaddonControllerConstants.Views.Pages.Replan.ReplanJourneyResponse;
		}

		final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, odRefNum);
		final TransportOfferingModel transportOffering = entries.get(0).getTravelOrderEntryInfo().getTransportOfferings()
				.stream()
				.filter(Objects::nonNull).reduce((a, b) -> a).get();
		model.addAttribute("ArrivalTime", transportOffering.getArrivalTime());
		model.addAttribute("SailingBy", transportOffering.getTransportVehicle().getTransportVehicleInfo().getName());
		model.addAttribute("DepartDetails", transportOffering.getTravelSector().getOrigin().getLocation().getName() + "("
				+ transportOffering.getTravelSector().getOrigin().getName() + ")");

		model.addAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO, journeyRefNum);
		model.addAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);
		final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();

		bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyRefNum, odRefNum);
		model.addAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, bcfFareFinderForm);
		model.addAttribute("terminalsCacheVersion", cmsSiteService.getCurrentSite().getTerminalsCacheVersion());
		return BcfvoyageaddonControllerConstants.Views.Pages.Replan.ReplanJourneyResponse;
	}


	@RequestMapping(value = "/manage-booking/replan-journey", method = RequestMethod.POST)
	@CheckRequireLoginForBookingChange
	public String replanJourney(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			@RequestParam("selectedJourneyRefNumber") final int journeyRefNum, @RequestParam("odRefNum") final int odRefNum,
			final RedirectAttributes redirectModel, final Model model,
			final BindingResult bindingResult)
	{
		bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyRefNum, odRefNum);
		sessionService.setAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);
		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRefNum);
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
		sessionService.setAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);
		redirectModel.addFlashAttribute(BcfFacadesConstants.CHANGE_FARE_OR_REPLAN,
				"true");
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
	}



	/**
	 * Starts the process of amending ancillaries by creating cart from order and attaching it to the session
	 *
	 * @param orderCode
	 * @return ancillary page for amendments
	 */
	@RequestMapping(value = "/manage-booking/{orderCode}/{originDestinationRefNumber}/{journeyReferenceNumber}/amend-ancillary", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String amendAncillaries(@PathVariable final String orderCode,
			@PathVariable("journeyReferenceNumber") final int journeyRefNum,
			@PathVariable("originDestinationRefNumber") final int odRefNum, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request, final RedirectAttributes redirectModel)
	{

		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(orderCode, true);

		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			return REDIRECT_PREFIX + request.getHeader(REFERER);
		}

		catch (final BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, AMEND_ANCILLARY_FAILED);
			return REDIRECT_PREFIX + request.getHeader(REFERER);
		}
		redirectModel.addFlashAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO, journeyRefNum);
		redirectModel.addFlashAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);

		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);

		final String redirectURL = "/manage-booking/ancillary/" + journeyRefNum + "/" + odRefNum + "/amendment";

		return REDIRECT_PREFIX + redirectURL;
	}

	/**
	 * Creates the and set amendable cart.
	 *
	 * @param orderCode the order code
	 */
	protected void createAndSetAmendableCart(final String orderCode, final boolean searchEbooking)
			throws IntegrationException, BcfOrderUpdateException, CalculationException
	{
		bcfTravelBookingFacade.createAmendableCart(orderCode, bcfTravelBookingFacade.getCurrentUserUid(), searchEbooking);
	}

	/**
	 * Starts the process of amending Travellers by creating cart from order and attaching it to the session
	 *
	 * @param orderCode
	 * @return travellers page for amendments
	 */
	@RequestMapping(value = "/manage-booking/{orderCode}/{originDestinationRefNumber}/{journeyReferenceNumber}/amend-travellers", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String amendTravellers(final HttpServletRequest request,
			final Model model,
			@PathVariable final String orderCode, @PathVariable final int journeyReferenceNumber,
			@PathVariable final int originDestinationRefNumber, final RedirectAttributes redirectModel)
	{

		try
		{
			bcfTravelCartFacade.createAndSetAmendableCart(orderCode, true);

		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex);
			model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
			return BcfvoyageaddonControllerConstants.Views.Pages.Replan.ReplanJourneyResponse;
		}

		catch (final BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, REPLAN_JOURNEY_FAILED);
			return BcfvoyageaddonControllerConstants.Views.Pages.Replan.ReplanJourneyResponse;
		}

		redirectModel.addFlashAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, originDestinationRefNumber);
		final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
		bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyReferenceNumber, originDestinationRefNumber);
		sessionService.setAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);

		redirectModel.addFlashAttribute(BcfFacadesConstants.CHANGE_FARE_OR_REPLAN,
				"true");

		sessionService.setAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, originDestinationRefNumber);
		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyReferenceNumber);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.PASSENGER_PAGE;



	}

	protected SelectedJourneyData populateSelectedJouneyData(final FareFinderForm fareFinderForm)
	{
		final SelectedJourneyData selectedJourneyData = new SelectedJourneyData();
		selectedJourneyData.setPassengerTypeQuantityList(fareFinderForm.getPassengerTypeQuantityList());
		selectedJourneyData.setVehicleInfo(fareFinderForm.getVehicleInfo());
		selectedJourneyData.setTravellingWithVehicle(fareFinderForm.getTravellingWithVehicle());
		selectedJourneyData.setReturnWithDifferentVehicle(fareFinderForm.getReturnWithDifferentVehicle());
		selectedJourneyData.setCarryingDangerousGoodsInOutbound(fareFinderForm.getCarryingDangerousGoodsInOutbound());
		selectedJourneyData.setCarryingDangerousGoodsInReturn(fareFinderForm.getCarryingDangerousGoodsInReturn());

		return selectedJourneyData;
	}


	protected MakeBookingRequestData getMakeBookingRequestData(final AddBundleToCartRequestData addBundleToCartRequestData,
			final AbstractOrderModel sessionAmendedCart)
	{
		final List<AddProductToCartRequestData> addProductToCartRequestDatas = cartFacade
				.createAddProductToCartRequestForAncillaries(sessionAmendedCart,
						((BcfAddBundleToCartData) addBundleToCartRequestData.getAddBundleToCartData().get(0))
								.getTransportOfferingDatas().stream().map(TransportOfferingData::getCode)
								.collect(Collectors.toList()));

		return makeBookingIntegrationFacade.createMakeBookingRequestData((CartModel) sessionAmendedCart, Collections.emptyList(),
				Collections.emptyMap(), Collections.emptyList(), addProductToCartRequestDatas, addBundleToCartRequestData);
	}



	/**
	 * Starts the process of traveller cancellation
	 *
	 * @param model
	 * @param redirectModel
	 * @param orderCode
	 * @param travellerUid
	 * @return traveller cancellation modal
	 */
	@RequestMapping(value = "/manage-booking/cancel-traveller-request", method = RequestMethod.GET, produces = "application/json")
	@CheckRequireLoginForBookingChange
	public String beginCancelTraveller(final Model model, final RedirectAttributes redirectModel,
			@ModelAttribute(value = "orderCode") final String orderCode,
			@ModelAttribute(value = "travellerUid") final String travellerUid)
	{
		final TravellerData cancelledTraveller = travellerFacade.getTraveller(travellerUid);
		if (!bcfTravelBookingFacade.atleastOneAdultTravellerRemaining(orderCode, cancelledTraveller.getLabel()))
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, CANCEL_TRAVELLER_FAILED);
			return BcfvoyageaddonControllerConstants.Views.Pages.Cancel.CancelTravellerResponse;
		}

		final boolean travellerCancellationStarted = bcfTravelBookingFacade.beginTravellerCancellation(orderCode,
				cancelledTraveller.getLabel(), cancelledTraveller.getUid(), bcfTravelBookingFacade.getCurrentUserUid());

		if (travellerCancellationStarted)
		{
			final ReservationData reservation = bcfReservationFacade.getCurrentReservationData();
			model.addAttribute(BcfstorefrontaddonWebConstants.CANCELLED_TRAVELLER, cancelledTraveller);
			model.addAttribute(BcfstorefrontaddonWebConstants.TOTAL_TO_PAY, reservation.getTotalToPay());
			model.addAttribute(BcfstorefrontaddonWebConstants.IS_CANCEL_POSSIBLE, true);
		}
		else
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, CANCEL_TRAVELLER_FAILED);
			model.addAttribute(BcfstorefrontaddonWebConstants.IS_CANCEL_POSSIBLE, false);
		}


		return BcfvoyageaddonControllerConstants.Views.Pages.Cancel.CancelTravellerResponse;
	}

	/**
	 * Proceeds with cancellation of traveller and places a new (amended) order
	 *
	 * @param redirectModel
	 * @param travellerUid
	 * @param orderCode
	 * @return the redirect to booking details page with result of cancellation process
	 */
	@RequestMapping(value = "/manage-booking/cancel-traveller")
	@CheckRequireLoginForBookingChange
	public String cancelTraveller(final RedirectAttributes redirectModel,
			@ModelAttribute(value = "travellerUid") final String travellerUid,
			@ModelAttribute(value = "orderCode") final String orderCode)
	{
		final PriceData totalToPay = bcfTravelBookingFacade.getTotalToPay();
		final TravellerData traveller = travellerFacade.getTraveller(travellerUid);

		if (traveller == null)
		{
			return getCancellTravellerError(redirectModel, orderCode);
		}

		final boolean result = bcfTravelBookingFacade.cancelTraveller(totalToPay, traveller);
		if (result)
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, CANCEL_TRAVELLER_SUCCESSFUL);
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.REFUND_RESULT, REFUND_ORDER_RESULT);
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_PARAMETER,
					buildFormattedTravellerName((PassengerInformationData) traveller.getTravellerInfo()));
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.REFUNDED_AMOUNT, totalToPay);
		}
		else
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, CANCEL_TRAVELLER_FAILED);
		}

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
	}

	@RequestMapping(value = "/manage-booking/cancel-sailing-request", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String getRefundConfirmation(@RequestParam("orderCode") final String orderCode,
			@RequestParam("journeyRefNum") final int journeyRefNum, @RequestParam("odRefNum") final int odRefNum, final Model model)
	{
		final PriceData totalToRefund = bcfTravelBookingFacade.getRefundTotal(orderCode, journeyRefNum, odRefNum);
		model.addAttribute(BcfstorefrontaddonWebConstants.IS_CANCEL_POSSIBLE, true);
		model.addAttribute(BcfstorefrontaddonWebConstants.TOTAL_TO_REFUND, totalToRefund.getFormattedValue());
		if (bcfSalesApplicationResolverFacade.getCurrentSalesChannel().equals(SalesApplication.CALLCENTER.getCode()))
		{
			final int thresholdTimeGap = Integer.parseInt(
					bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.CANCEL_BOOKING_RESTRICTION_TIMEGAP));
			final boolean enabled = bcfReservationFacade
					.isFirstTOMeetsDepartureTimeRestrictionGap(bcfTravelBookingFacade.getBookingByBookingReference(orderCode),
							thresholdTimeGap);
			if (!enabled)
			{
				model.addAttribute(DEPARTURE_TIME_WARNING, DEPARTURE_TIME_WARNING_CODE);
			}
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.BookingDetails.cancelOrderConfirmResponse;
	}

	@RequestMapping(value = "/manage-booking/cancel-sailing", method = RequestMethod.POST)
	@CheckRequireLoginForBookingChange
	public String cancelSailing(@RequestParam("orderCode") final String orderCode,
			@RequestParam("journeyRefNum") final int journeyRefNum, @RequestParam("odRefNum") final int odRefNum, final Model model)
	{
		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);

		final CancelBookingResponseData cancelBookingResponseData = bcfTravelBookingFacade.cancelTransportBookings(orderCode,
				journeyRefNum, odRefNum);
		if (Objects.equals(CancelStatus.COMPLETE_SUCCESS, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(model, orderCode, false, CANCEL_ORDER_SUCCESSFUL);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
		}

		model.addAttribute(FAILURE_CANCEL_EBOOKINGS, cancelBookingResponseData.getFailureCancelEBookings());
		if (Objects.equals(CancelStatus.COMPLETE_FAIL, cancelBookingResponseData.getCancelStatus()))
		{
			model.addAttribute(EXCEPTION_CANCEL_EBOOKINGS, cancelBookingResponseData.getException());
		}
		else if (Objects.equals(CancelStatus.PARTIAL_SUCCESS, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(model, orderCode, false, CANCEL_PARTIAL_ORDER_SUCCESSFUL);
		}
		else if (Objects.equals(CancelStatus.PARTIAL_FAIL, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(model, orderCode, false, CANCEL_PARTIAL_ORDER_FAILED);
		}

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
	}

	/**
	 * Populate cancellation result info.
	 *
	 * @param model              the model
	 * @param orderCode          the order code
	 * @param populateRefundInfo the populate refund info
	 * @param cancellationResult the cancellation result
	 */
	protected void populateCancellationResultInfo(final Model model, final String orderCode, final boolean populateRefundInfo,
			final String cancellationResult)
	{
		model.addAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, cancellationResult);
		if (populateRefundInfo)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.REFUND_RESULT, REFUND_ORDER_RESULT);
			final PriceData refundedAmount = bcfTravelBookingFacade.getRefundTotal(orderCode);
			model.addAttribute(BcfstorefrontaddonWebConstants.REFUNDED_AMOUNT, refundedAmount);
		}
	}

	protected String getCancellTravellerError(final RedirectAttributes redirectModel, final String orderCode)
	{
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCELLATION_RESULT, CANCEL_TRAVELLER_FAILED);
		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
	}

	protected String buildFormattedTravellerName(final PassengerInformationData info)
	{
		final StringJoiner formattedTravellerName = new StringJoiner(" ");
		formattedTravellerName.add(info.getTitle().getName()).add(info.getFirstName()).add(info.getSurname());
		return formattedTravellerName.toString();
	}

	@RequestMapping(value = "/manage-booking/modify-booking", method = RequestMethod.POST)
	@CheckRequireLoginForBookingChange
	public String modifyBooking(@RequestParam("eBookingReferences") final String eBookingReferences,
			@RequestParam("eBookingReferenceToModify") final String eBookingReference, final Model model,
			final RedirectAttributes redirectModel)
	{
		CartModel sessionAmendedCart = null;
		try
		{
			sessionAmendedCart = bcfBookingFacade.createAndSetAmendableCartForEBookingRef(eBookingReference, true);
		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex);
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE,
					ex.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
		}
		catch (final BcfOrderUpdateException | CalculationException ex)
		{
			LOG.error(ex);
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, SAILING_AMENDMENT_FAILED);
		}
		if (Objects.nonNull(sessionAmendedCart) && sessionAmendedCart.getEntries().size() > 0) //NOSONAR
		{
			int journeyReferenceNumber = 0;
			int odReferenceNumber = 0;
			final Optional<AbstractOrderEntryModel> optionalEntry = StreamUtil.safeStream(sessionAmendedCart.getEntries())
					.filter(entry -> entry.getActive() && StringUtils.equalsIgnoreCase(entry.getBookingReference(), eBookingReference))
					.findFirst();
			if (optionalEntry.isPresent())
			{
				journeyReferenceNumber = optionalEntry.get().getJourneyReferenceNumber();
				if (Objects.nonNull(optionalEntry.get().getTravelOrderEntryInfo()))
				{
					odReferenceNumber = optionalEntry.get().getTravelOrderEntryInfo().getOriginDestinationRefNumber().intValue();
				}
			}
			final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
			bcfFerrySelectionUtil
					.initializeFareFinderForm(bcfFareFinderForm, journeyReferenceNumber, odReferenceNumber);
			bcfFerrySelectionUtil.convertToOneWay(bcfFareFinderForm, odReferenceNumber);
			model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
			sessionService.setAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE, eBookingReference);
			sessionService.setAttribute(BcfFacadesConstants.AMENDING_BOOKING_REFERENCES, eBookingReferences);
			sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyReferenceNumber);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ROUTE_PAGE;
		}
		else
		{
			LOG.error("Unable to create amendable cart");
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, Boolean.TRUE);
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGE, SAILING_AMENDMENT_FAILED);
		}
		return REDIRECT_PREFIX + "/manage-booking/booking-details?eBookingCode=" + eBookingReferences;
	}

	@RequestMapping(value = "/manage-booking/abort-modification", method = RequestMethod.GET)
	@CheckRequireLoginForBookingChange
	public String abortModifyBooking()
	{
		final String amendingBookingReferences = sessionService.getAttribute(BcfFacadesConstants.AMENDING_BOOKING_REFERENCES);
		bcfBookingFacade.removeAmendableCart();
		sessionService.removeAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		sessionService.removeAttribute(BcfFacadesConstants.AMENDING_BOOKING_REFERENCES);
		return REDIRECT_PREFIX + "/manage-booking/booking-details?eBookingCode=" + amendingBookingReferences;
	}

	@RequestMapping(value = "/manage-booking/original-booking/{originalBookingReference}", method = RequestMethod.POST)
	public String getOriginalBooking(@PathVariable final String originalBookingReference, final Model model)
	{
		final boolean isAmendingSailing = bcfTravelCartFacade.isAmendingSailing();
		if (isAmendingSailing && StringUtils.isNotBlank(originalBookingReference))
		{
			try
			{
				model.addAttribute("originalReservation", searchBookingFacade.searchBookings(null, originalBookingReference));
			}
			catch (final IntegrationException e)
			{
				model.addAttribute("hasErrorFlag", true);
				model.addAttribute("errorMsg",
						e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
				LOG.error(e);
			}
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.TransportBookings.transportBookingsResponseJson;
	}

}
