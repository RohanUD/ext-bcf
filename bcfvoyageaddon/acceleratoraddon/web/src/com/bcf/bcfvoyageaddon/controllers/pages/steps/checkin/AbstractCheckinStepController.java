/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages.steps.checkin;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.travelfacades.facades.CheckInFacade;
import de.hybris.platform.travelfacades.fare.search.UpgradeFareSearchFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfvoyageaddon.checkin.steps.CheckinGroup;
import com.bcf.bcfvoyageaddon.checkin.steps.CheckinStep;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;


public abstract class AbstractCheckinStepController extends TravelAbstractPageController
{
	private static final String REDIRECT = "redirect:";

	@Resource(name = "checkinFlowGroupMap")
	private Map<String, CheckinGroup> checkinFlowGroupMap;

	@Resource(name = "checkInFacade")
	private CheckInFacade checkInFacade;

	@Resource(name = "upgradeFareSearchFacade")
	private UpgradeFareSearchFacade upgradeFareSearchFacade;

	@ModelAttribute("checkoutSteps")
	public List<CheckinSteps> addCheckinStepsToModel()
	{
		final CheckinGroup checkinGroup = getCheckinFlowGroupMap().get("travelCheckinGroup");
		final Map<String, CheckinStep> progressBarMap = checkinGroup.getCheckinProgressBar();
		final List<CheckinSteps> checkinSteps = new ArrayList<>(progressBarMap.size());

		for (final Map.Entry<String, CheckinStep> entry : progressBarMap.entrySet())
		{
			final CheckinStep checkinStep = entry.getValue();
			if (checkinStep.isEnabled())
			{
				checkinSteps.add(new CheckinSteps(checkinStep.getProgressBarId(),
						StringUtils.remove(checkinStep.currentStep(),
								REDIRECT),
						Integer.valueOf(entry.getKey())));
			}
		}
		return checkinSteps;
	}

	protected CheckinStep getCheckinStep(final String currentController)
	{
		final CheckinGroup checkinGroup = getCheckinFlowGroupMap().get(checkInFacade.getCheckinFlowGroupForCheckout());
		return checkinGroup.getCheckinStepMap().get(currentController);
	}

	protected void setCheckinStepLinksForModel(final Model model, final CheckinStep checkinStep)
	{
		model.addAttribute("previousStepUrl", StringUtils.remove(checkinStep.previousStep(), REDIRECT));
		model.addAttribute("nextStepUrl", StringUtils.remove(checkinStep.nextStep(), REDIRECT));
		model.addAttribute("currentStepUrl", StringUtils.remove(checkinStep.currentStep(), REDIRECT));
		model.addAttribute("progressBarId", checkinStep.getProgressBarId());
	}

	public static class CheckinSteps
	{
		private final String progressBarId;
		private final String url;
		private final Integer stepNumber;

		public CheckinSteps(final String progressBarId, final String url, final Integer stepNumber)
		{
			this.progressBarId = progressBarId;
			this.url = url;
			this.stepNumber = stepNumber;
		}

		protected String getProgressBarId()
		{
			return progressBarId;
		}

		protected String getUrl()
		{
			return url;
		}

		protected Integer getStepNumber()
		{
			return stepNumber;
		}
	}

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/upgrade-bundle-options", method = RequestMethod.GET, produces =
			{ "application/json" })
	public String getUpgradeBundleOptionsPage(final Model model)
	{
		final FareSelectionData fareSelectionData = upgradeFareSearchFacade.doUpgradeSearch();
		model.addAttribute(BcfvoyageaddonWebConstants.UPGRADE_BUNDLE_PRICED_ITINERARIES, fareSelectionData.getPricedItineraries());
		final AddBundleToCartForm addBundleToCartForm = new AddBundleToCartForm();
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_BUNDLE_TO_CART_FORM, addBundleToCartForm);
		model.addAttribute(BcfstorefrontaddonWebConstants.ADD_BUNDLE_TO_CART_URL, BcfvoyageaddonWebConstants.UPGRADE_BUNDLE_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.IS_UPGRADE_OPTION_AVAILABLE,
				isUpgradeAvailable(fareSelectionData.getPricedItineraries()));
		return BcfvoyageaddonControllerConstants.Views.Pages.Ancillary.UpdateBundleJSONData;
	}

	protected boolean isUpgradeAvailable(final List<PricedItineraryData> pricedItineraries)
	{
		return CollectionUtils.isNotEmpty(pricedItineraries)
				&& pricedItineraries.stream().allMatch(pricedItinerary -> pricedItinerary.isAvailable()
				&& CollectionUtils.isNotEmpty(pricedItinerary.getItineraryPricingInfos()));
	}

	/**
	 * @return the checkin flow group map
	 */
	protected Map<String, CheckinGroup> getCheckinFlowGroupMap()
	{
		return checkinFlowGroupMap;
	}
}
