/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.cms;

import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.enums.BookingsViewType;
import com.bcf.bcfvoyageaddon.model.components.TransportBookingListComponentModel;
import com.bcf.facades.booking.BcfBookingListFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Transport Booking List Controller for handling requests for My Booking Section in My Account Page.
 */
@Controller("TransportBookingListComponentController")
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.TransportBookingListComponent)
public class TransportBookingListComponentController
		extends SubstitutingCMSAddOnComponentController<TransportBookingListComponentModel>
{
	private static final Logger LOG = Logger.getLogger(TransportBookingListComponentController.class);

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	@Resource(name = "bookingListFacade")
	private BcfBookingListFacade bookingListFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final TransportBookingListComponentModel component)
	{
		final Boolean searchFromEBooking = getConfigurationService().getConfiguration()
				.getBoolean(BcfFacadesConstants.SEARCH_FROM_EBOOKING);
		if (searchFromEBooking)
		{
			try
			{
				model.addAttribute(BcfFacadesConstants.TRANSPORT_BOOKINGS,
						searchBookingFacade.searchUpcomingBookings(BcfFacadesConstants.DEFAULT_PAGE_NUMBER));
				model.addAttribute(BcfFacadesConstants.IS_EBOOKING_ORDER, true);
			}
			catch (final IntegrationException e)
			{
				model.addAttribute(BcfFacadesConstants.HAS_ERROR_FLAG, true);
				model.addAttribute(BcfFacadesConstants.ERROR_MESSAGE, e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
				LOG.error(e);
			}
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.MY_ACCOUNT_BOOKING, bookingListFacade.getCurrentCustomerReservations());
			model.addAttribute(BcfFacadesConstants.IS_EBOOKING_ORDER, false);
		}
	}

	@Override
	protected String getView(final TransportBookingListComponentModel component)
	{
		if (Objects.equals(BookingsViewType.BUSINESS, component.getView()))
			return BcfvoyageaddonControllerConstants.ADDON_PREFIX + BcfvoyageaddonControllerConstants.Views.Cms.ComponentPrefix
					+ StringUtils.lowerCase(component.getUid());
		else
			return super.getView(component);
	}
}
