/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers;


import com.bcf.bcfcommonsaddon.model.components.CurrentConditionsComponentModel;
import com.bcf.bcfvoyageaddon.model.components.FareFinderComponentModel;
import com.bcf.bcfvoyageaddon.model.components.ReservationBreakdownComponentModel;
import com.bcf.bcfvoyageaddon.model.components.TransportBookingDetailsComponentModel;
import com.bcf.bcfvoyageaddon.model.components.TransportBookingListComponentModel;
import com.bcf.bcfvoyageaddon.model.components.TransportOfferingStatusSearchComponentModel;
import com.bcf.bcfvoyageaddon.model.components.TransportSummaryComponentModel;


/**
 */
public interface BcfvoyageaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/bcfvoyageaddon/";

	interface Views
	{

		interface Pages
		{

			interface Account
			{
				String SavedPassengersPage = ADDON_PREFIX + "pages/account/saved-passengers";

				String MyBookingsPage = ADDON_PREFIX + "pages/account/my-bookings";
			}

			interface CheckIn
			{
				String CheckInPage = ADDON_PREFIX + "pages/checkin/checkInPage";

				String CheckInDetailsPage = ADDON_PREFIX + "pages/checkin/checkInDetails";

				String CheckInStatusPage = ADDON_PREFIX + "pages/checkin/checkInStatus";

				String CheckInSuccessPage = ADDON_PREFIX + "pages/checkin/checkInSuccess";
			}

			interface Order
			{
				String OrderConfirmationPage = ADDON_PREFIX + "pages/order/orderConfirmationPage";
			}

			interface Ancillary
			{
				String AncillaryPage = ADDON_PREFIX + "pages/ancillary/ancillaryPage";

				String AddProductToCartResponse = ADDON_PREFIX + "pages/ancillary/addProductToCartResponse";

				String UpdateBundleJSONData = ADDON_PREFIX + "pages/ancillary/updateBundleJSONData";

				String TravelRestrictionResponse = ADDON_PREFIX + "pages/booking/travelRestrictionResponse";
			}

			interface FareSelection
			{
				String FareSelectionPage = ADDON_PREFIX + "pages/booking/fareSelectionPage";

				String fareSelectionSortingResult = ADDON_PREFIX + "pages/booking/fareSelectionSortingResult";

				String savedSearchResult = ADDON_PREFIX + "pages/booking/savedSearch";

				String AddBundleToCartResponse = ADDON_PREFIX + "pages/booking/addBundleToCartResponse";
			}

			interface Booking
			{
				String BookingConfirmationPage = ADDON_PREFIX + "pages/order/bookingConfirmationPage";
			}

			interface FlightStatus
			{
				String flightStatusSearchResponse = ADDON_PREFIX + "pages/transportofferingstatus/flightStatusSearchResponse";
			}

			interface TripFinder
			{
				String DestinationLocationJSONResponse = ADDON_PREFIX + "pages/tripfinder/destinationLocationsJSONResponse";
			}

			interface FormErrors
			{
				String formErrorsResponse = ADDON_PREFIX + "pages/tripfinder/formErrorsResponse";
			}

			interface Suggestions
			{
				String JSONSearchResponse = ADDON_PREFIX + "pages/suggestions/suggestionsSearchJsonResponse";
			}

			interface Accessibility
			{
				String accessibilityNeedsPerPAX = ADDON_PREFIX + "pages/passenger/accessibilityNeedsPerPAX";
				String inboundAccessibilityNeedsPerPAX = ADDON_PREFIX + "pages/passenger/inboundAccessibilityNeedsPerPAX";
			}

			interface Cancel
			{
				String CancelTravellerResponse = ADDON_PREFIX + "pages/cancel/cancelTravellerResponse";
			}

			interface Cart
			{
				String VoucherJSONResponse = ADDON_PREFIX + "pages/cart/voucherJSONResponse";
			}

			interface Replan
			{
				String ReplanJourneyResponse = ADDON_PREFIX + "pages/replan/replanJourneyResponse";
			}

			interface TravellerDetails
			{
				String TravellerDetailsPage = ADDON_PREFIX + "pages/travellerdetails/travellerDetailsPage";

				String JSONTravellerDetailsAuthentication = ADDON_PREFIX
						+ "pages/travellerdetails/JSONTravellerDetailsAuthentication";

				String AllowedTitlesForPassengerType = ADDON_PREFIX + "pages/travellerdetails/allowedTitlesForPassengerTypeResponse";
			}

			interface FareCalculator
			{
				String FareCalculatorPage = ADDON_PREFIX + "pages/farecalculator/farecalculatorsummarypage";
			}
		}

		interface Cms // NOSONAR
		{
			String ComponentPrefix = "cms/"; // NOSONAR
		}
	}

	interface Actions
	{

		interface Cms
		{

			String _Prefix = "/view/";

			String _Suffix = "Controller";

			/**
			 * CMS components that have specific handlers
			 */
			String FareFinderComponent = _Prefix + FareFinderComponentModel._TYPECODE + _Suffix;

			String CurrentConditionsComponent = _Prefix + CurrentConditionsComponentModel._TYPECODE + _Suffix;



			String TransportOfferingStatusSearchComponent = _Prefix + TransportOfferingStatusSearchComponentModel._TYPECODE
					+ _Suffix;

			String ReservationBreakdownComponent = _Prefix + ReservationBreakdownComponentModel._TYPECODE + _Suffix;

			String TransportBookingDetailsComponent = _Prefix + TransportBookingDetailsComponentModel._TYPECODE + _Suffix;

			String TransportBookingListComponent = _Prefix + TransportBookingListComponentModel._TYPECODE + _Suffix;

			String TransportSummaryComponent = _Prefix + TransportSummaryComponentModel._TYPECODE + _Suffix;
		}
	}
}
