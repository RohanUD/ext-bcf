/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 4/7/19 9:31 AM
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.ALACARTE_REVIEW_PAGE_PATH;
import static com.bcf.facades.constants.BcfFacadesConstants.TRANSPORTOFFERING_DATE_FORMAT;
import static org.springframework.http.HttpHeaders.REFERER;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.OptionBookingForm;
import com.bcf.core.service.BcfSalesApplicationResolverService;
import com.bcf.core.util.BCFDateUtils;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.UserValidationException;
import com.bcf.facades.optionbooking.BcfOptionBookingFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping(value = "/option-booking")
public class OptionBookingPageController extends AbstractSearchPageController
{

	private static final Logger LOG = Logger.getLogger(OptionBookingPageController.class);
	private static final String OPTION_BOOKING_LIST_PAGE = "optionBookingListPage";
	private static final String OPTION_BOOKING_DETAILS_PAGE = "optionBookingDetailsPage";
	private static final String OPTION_BOOKING_PAGE_URL_SUFFIX = "/option-booking";
	private static final String OPTION_BOOKING_LIST_PAGE_URL_SUFFIX = OPTION_BOOKING_PAGE_URL_SUFFIX + "/list";
	private static final String OPTION_BOOKING_DETAILS_PAGE_URL_SUFFIX = OPTION_BOOKING_PAGE_URL_SUFFIX + "/details/";
	private static final String OPTION_BOOKING_UPDATE_URL = OPTION_BOOKING_PAGE_URL_SUFFIX + "/update";
	private static final String CART_SUMMARY_PAGE_URL = "/cart/summary";
	private static final String QUOTE_DETAILS_PAGE_URL_PREFIX = "/my-account/my-quotes/";
	private static final String NO_PERMISSION_TO_ACCESS_OPTION_BOOKING = "Permission denied to perform any operations on Option Booking.";


	@Resource(name = "bcfOptionBookingFacade")
	private BcfOptionBookingFacade bcfOptionBookingFacade;

	@Resource(name = "bcfSalesApplicationResolverService")
	private BcfSalesApplicationResolverService bcfSalesApplicationResolverService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String getPage(@RequestParam(value = "page", defaultValue = "0") final int page,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		// Handle paged search results
		final PageableData pageableData = createPageableData(page, 5, null, AbstractSearchPageController.ShowMode.Page);
		final SearchPageData searchPageData = bcfOptionBookingFacade.getOptionBookings(pageableData);

		populateModel(model, searchPageData, AbstractSearchPageController.ShowMode.Page);

		storeCmsPageInModel(model, getContentPageForLabelOrId(OPTION_BOOKING_LIST_PAGE));

		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(OPTION_BOOKING_LIST_PAGE));
		model.addAttribute("searchPageData", searchPageData);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/details/{optionBookingId}", method = RequestMethod.GET)
	public String getOptionBookingDetails(@PathVariable(value = "optionBookingId") final String optionBookingId,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{

		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		final CartData optionBookingDetail = bcfOptionBookingFacade.getOptionBookingDetailsForCode(optionBookingId);

		final BcfGlobalReservationData bcfGlobalReservationData = bcfOptionBookingFacade.getCurrentGlobalReservationData();

		storeCmsPageInModel(model, getContentPageForLabelOrId(OPTION_BOOKING_DETAILS_PAGE));

		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(OPTION_BOOKING_DETAILS_PAGE));
		model.addAttribute("optionBookingDetail", optionBookingDetail);
		model.addAttribute("updateOptionBookingUrl", OPTION_BOOKING_UPDATE_URL);
		model.addAttribute("optionBookingForm", new OptionBookingForm());
		model.addAttribute(BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/save", method = RequestMethod.GET)
	public String saveCartAsOptionBooking(final HttpServletRequest request, final RedirectAttributes redirectAttributes)
	{
		String optionBookingCode = StringUtils.EMPTY;

		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		try
		{
			optionBookingCode = bcfOptionBookingFacade.createOrUpdateOptionBooking(null);
		}
		catch (Exception e)
		{
			LOG.error(String.format("Error while saving option booking %s", optionBookingCode), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Error in creating Option Booking for cart. " + e.getMessage());

			return getCartPageUrl(request);
		}

		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
				"Option Booking " + optionBookingCode + " Created ");
		return getCartPageUrl(request);
	}

	private String getCartPageUrl(final HttpServletRequest request)
	{
		String referer = request.getHeader(REFERER);
		if (StringUtils.isNotBlank(referer) && referer.contains(ALACARTE_REVIEW_PAGE_PATH))
		{
			return REDIRECT_PREFIX + ALACARTE_REVIEW_PAGE_PATH;
		}
		return REDIRECT_PREFIX + CART_SUMMARY_PAGE_URL;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateOptionBookingCart(final OptionBookingForm optionBookingForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{

		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		if (StringUtils.isBlank(optionBookingForm.getUpdatedExpirationDate()))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Provide updated expiry date. Option Booking cannot be Updated!");
			return REDIRECT_PREFIX + OPTION_BOOKING_DETAILS_PAGE_URL_SUFFIX + optionBookingForm.getOptionBookingId();
		}

		if (BCFDateUtils
				.isDatePartEqual(optionBookingForm.getOriginalExpirationDate(), optionBookingForm.getUpdatedExpirationDate()))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Dates are same, nothing to update!");

			return REDIRECT_PREFIX + OPTION_BOOKING_DETAILS_PAGE_URL_SUFFIX + optionBookingForm.getOptionBookingId();
		}

		Date updatedDate = TravelDateUtils.getDate(optionBookingForm.getUpdatedExpirationDate(), TRANSPORTOFFERING_DATE_FORMAT);

		try
		{
			bcfOptionBookingFacade.updateExpiryDate(optionBookingForm.getOptionBookingId(), updatedDate);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"Option Booking Updated!");
		}
		catch (CalculationException | IntegrationException | UserValidationException | CartValidationException e)
		{
			LOG.error("Error in updating expiration date", e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Error in updating expiration date:" + e.getMessage());
		}
		return REDIRECT_PREFIX + OPTION_BOOKING_DETAILS_PAGE_URL_SUFFIX + optionBookingForm.getOptionBookingId();
	}

	@RequestMapping(value = "/convert-to-booking/{optionBookingId}", method = RequestMethod.GET)
	public String convertOptionBookingToPackageBooking(final HttpServletRequest request,
			@PathVariable(value = "optionBookingId") final String optionBookingId,
			final Model model, final RedirectAttributes redirectAttributes)
	{

		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		bcfOptionBookingFacade.convertOptionBookingToCart(optionBookingId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
				"Option Booking converted successfully to booking Cart");
		return getCartPageUrl(request);
	}


	@RequestMapping(value = "/convert-quote-to-option/{quoteBookingId}", method = RequestMethod.GET)
	public String convertQuoteToOptionBooking(@PathVariable(value = "quoteBookingId") final String quoteBookingId,
			final Model model, final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		try
		{
			bcfOptionBookingFacade.convertQuoteToOptionBooking(quoteBookingId);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"Quete Booking converted successfully to option booking");
			return REDIRECT_PREFIX + CART_SUMMARY_PAGE_URL;
		}
		catch (Exception e)
		{
			LOG.error(String.format("Error while creating option booking %s from Quote Booking", quoteBookingId), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Error in creating Option Booking for quote.");
			return REDIRECT_PREFIX + QUOTE_DETAILS_PAGE_URL_PREFIX + quoteBookingId;
		}
	}

	@RequestMapping(value = "/cancel/{optionBookingId}", method = RequestMethod.GET)
	public String cancelOptionBooking(final HttpServletRequest request,
			@PathVariable(value = "optionBookingId") final String optionBookingId,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		if (!bcfOptionBookingFacade.isAllowedToAccessOptionBooking())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					NO_PERMISSION_TO_ACCESS_OPTION_BOOKING);
			return getCartPageUrl(request);
		}

		try
		{
			bcfOptionBookingFacade.cancelOptionBooking(optionBookingId);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"Option booking cancelled successfully");
			return REDIRECT_PREFIX + OPTION_BOOKING_LIST_PAGE_URL_SUFFIX;
		}
		catch (Exception ex)
		{
			LOG.error(String.format("Error in cancelling Option Booking %s", optionBookingId), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Error in cancelling Option Booking " + optionBookingId);
			return getCartPageUrl(request);
		}
	}

	@Override
	protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		super.populateModel(model, searchPageData, showMode);
		final PaginationData paginationData = searchPageData.getPagination();
		final int next = ((paginationData.getCurrentPage() + 1) < paginationData.getNumberOfPages()) ?
				(paginationData.getCurrentPage() + 1) :
				paginationData.getCurrentPage();
		final int previous = ((paginationData.getCurrentPage() - 1) > 0) ? (paginationData.getCurrentPage() - 1) : 0;

		model.addAttribute("naxtPage", next);
		model.addAttribute("previousPage", previous);
	}
}
