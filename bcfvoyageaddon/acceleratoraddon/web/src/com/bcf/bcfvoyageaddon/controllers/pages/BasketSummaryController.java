/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH_ONLY;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.LOGIN_TO_CHECKOUT_URL_KEY;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.travelfacades.facades.packages.PackageFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.pages.steps.checkin.AbstractCheckinStepController;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller("BasketSummaryController")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
@RequestMapping(value = "/cart/summary")
public class BasketSummaryController extends AbstractCheckinStepController
{
	private static final String CART_DEFAULT_CMS_PAGE = "basketSummaryPage";
	private static final String BASKET_SUMMARY = "/cart/summary";
	private static final String REMOVE_SAILING_FAILED = "reservation.remove.sailing.integration.failure";
	private static final String CHECKOUT_LOGIN = "/login/checkout";
	private static final String BOOKING_DETAILS_URL = "/manage-booking/booking-details/";
	private static final String ANCILLARY_PAGE_URL = "/ancillary";
	private static final String IS_ASM_SESSION = "isASMSession";

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "cancelBookingIntegrationFacade")
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "packageFacade")
	private PackageFacade packageFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@Resource(name = "defaultBcfTravelCartFacade")
	private DefaultBcfTravelCartFacade defaultBcfTravelCartFacade;

	@Resource
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	/**
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method =
			{ RequestMethod.POST, RequestMethod.GET })
	public String getBasketSummaryPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		getSessionService().removeAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		getSessionService().removeAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM);
		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();
		if (!cartFacade.hasSessionCart()
				|| (BookingJourneyType.BOOKING_TRANSPORT_ONLY
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
				&& !cartFacade.validateOriginDestinationRefNumbersInCart())
		{
			return REDIRECT_PREFIX + "/";
		}

		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.IS_CHECK_IN_JOURNEY, false);

		model.addAttribute(BcfstorefrontaddonWebConstants.CART_TOTAL, cartFacade.getCartTotal());
		model.addAttribute("enableSaveQuote", packageFacade.isPackageInCart());
		model.addAttribute("showWaiveOff", false);
		model.addAttribute(BcfstorefrontaddonWebConstants.GLOBAL_RESERVATION_DATA, bcfGlobalReservationData);
		model.addAttribute(BcfstorefrontaddonWebConstants.NEXT_URL,
				determineNexUrl(request));
		model.addAttribute(LOGIN_TO_CHECKOUT_URL_KEY, LOGIN_PAGE_PATH_ONLY);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACTIVITY_URL, request.getContextPath() + "/deal-activity-listing");
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS,
				defaultBcfTravelCartFacade.getPackageAvailabilityStatus());
		model.addAttribute("isAmendment", defaultBcfTravelCartFacade.isAmendmentCart());
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
				getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY));
		model.addAttribute("packageDetailsUrlParams", getSessionService().getAttribute("packageDetailsUrlParams"));


		final Boolean isASMSession = assistedServiceFacade.getAsmSession() != null;
		model.addAttribute(IS_ASM_SESSION, isASMSession);
		model.addAttribute("showSupplierComments",
				assistedServiceFacade.isAssistedServiceAgentLoggedIn() && !BookingJourneyType.BOOKING_TRANSPORT_ONLY
						.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)));


		if (BookingJourneyType.BOOKING_PACKAGE.getCode()
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			Map<String, List<TravellerData>> travellerDataCodeMap = null;
			if (Objects.nonNull(bcfGlobalReservationData.getPackageReservations()) && CollectionUtils
					.isNotEmpty(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()))
			{
				final ReservationData reservationData = bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()
						.stream().findFirst().get().getReservationData();
				if(reservationData!=null){
					travellerDataCodeMap = bcfReservationFacade.getTravellerDataMap(reservationData);
				}

			}
			if (Objects.nonNull(travellerDataCodeMap))
			{
				model.addAttribute(BcfFacadesConstants.RESERVABLE, false);
				model.addAttribute(BcfvoyageaddonWebConstants.TRAVELLER_DATA_CODE_MAP, travellerDataCodeMap);
			}
			else
			{
				model.addAttribute(BcfFacadesConstants.RESERVABLE, true);
			}

		}

		final CartTimerData cartTimerData=bcfCartTimerFacade.getCartStartTimerDetails();
		if(cartTimerData!=null){
			model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
			model.addAttribute("holdTimeMessage",cartTimerData.getHoldTimeMessage());
		}
		return getBasketSummaryPage(model, CART_DEFAULT_CMS_PAGE);
	}

	protected String determineNexUrl(final HttpServletRequest request)
	{
		if (travelCheckoutFacade
				.containsLongRoute() && !travelCheckoutFacade.containsDefaultSailing())
		{
			return request.getContextPath() + ANCILLARY_PAGE_URL;
		}
		return request.getContextPath() + "/checkout/multi/payment-method/select-flow";
	}

	@RequestMapping(value = "/removeDeal", method =
			{ RequestMethod.POST, RequestMethod.GET }, produces = "application/json")
	public String removeDealToCart(@RequestParam("dealId") final String dealId, @RequestParam("date") final String date,
			final RedirectAttributes redir)
	{
		final List<RemoveSailingResponseData> removeSailingResponseDatas;
		try
		{
			removeSailingResponseDatas = cartFacade.removeDealFromCart(dealId,
					TravelDateUtils.convertStringDateToDate(date, BcfstorefrontaddonWebConstants.DATE_FORMAT));
			cartFacade.recalculateCart();
		}
		catch (final IntegrationException ex)
		{
			GlobalMessages.addFlashMessage(redir, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_SAILING_FAILED);
			return REDIRECT_PREFIX + BASKET_SUMMARY;
		}
		redir.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCEL_BOOKING_FAILURE_SAILINGS_FROM_CART,
				cancelBookingIntegrationFacade.filterCancelBookingList(removeSailingResponseDatas, false));
		return REDIRECT_PREFIX + BASKET_SUMMARY;

	}


	protected String getBasketSummaryPage(final Model model, final String basketSummaryCmsPage) throws CMSItemNotFoundException
	{
		if (!cartFacade.isCurrentCartValid())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.HOME_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(basketSummaryCmsPage));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(basketSummaryCmsPage));

		return getViewForPage(model);
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	public String nextPage()
	{
		if (BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY))
				|| BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION
				.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
		{
			return REDIRECT_PREFIX + ANCILLARY_PAGE_URL;
		}

		if (userFacade.isAnonymousUser() && !cartFacade.isAmendmentCart())
		{
			return REDIRECT_PREFIX + CHECKOUT_LOGIN;
		}

		if (cartFacade.isAmendmentCart())
		{
			if (!cartFacade.hasCartBeenAmended())
			{
				return REDIRECT_PREFIX + BOOKING_DETAILS_URL + cartFacade.getOriginalOrderCode();
			}

			if (BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION
					.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{
				return REDIRECT_PREFIX + ANCILLARY_PAGE_URL;
			}

		}

		if (!userFacade.isAnonymousUser())
		{
			return REDIRECT_PREFIX + BcfstorefrontaddonWebConstants.SUMMARY_DETAILS_PATH;
		}

		return REDIRECT_PREFIX + CHECKOUT_LOGIN;
	}

}
