/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.FareCalculatorForm;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * Controller for Fare Selection page
 */
@Controller
@RequestMapping("/faresummary")
public class FareCalculatorSummaryPageController extends BcfAbstractPageController
{
	private static final String CALCULATE_FARE_SUMMARY_PAGE_ID = "fareCalculatorSummaryPage";
	private static final String OUT_BOUND_FORM = "outBoundForm";
	private static final String SHOW_NON_BOOKABLE_INFO = "showNonBookableInfo";
	private static final String LOADING_RESTRICTED_VEHICLE_CODES = "OS,SEMI,SBT,CV";
	private static final String SHOW_LOADING_RESTRICTION_INFO = "showLoadingRestrictionInfo";

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@RequestMapping(method = RequestMethod.POST)
	public String calculateFareResults(final FareCalculatorForm fareCalculatorForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (sessionService.getAttribute(OUT_BOUND_FORM) == null && fareCalculatorForm.getPricedItinerary().getItinerary()
				.getTripType().equals(TripType.RETURN))
		{
			sessionService.setAttribute(OUT_BOUND_FORM, fareCalculatorForm);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_CALCULATOR_FARE_SELECTION_ROOT_URL + "?isReturn=true&index=0";
		}
		else
		{
			final FareCalculatorForm outboundForm = sessionService.getAttribute(OUT_BOUND_FORM);
			final FareCalculatorForm inboundForm = fareCalculatorForm;
			model.addAttribute(SHOW_NON_BOOKABLE_INFO, isNonBookableRouteSelected(outboundForm, inboundForm));
			model.addAttribute(SHOW_LOADING_RESTRICTION_INFO, isOversizeOrOver5500VehicleSelected(outboundForm, inboundForm));
			model.addAttribute(OUT_BOUND_FORM, outboundForm);
			model.addAttribute(BcfFacadesConstants.FARE_CALCULATOR_FORM, inboundForm);
			model.addAttribute(BcfvoyageaddonWebConstants.DATE_NOW, new java.util.Date());
			model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
			model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
			storeCmsPageInModel(model, getContentPageForLabelOrId(CALCULATE_FARE_SUMMARY_PAGE_ID));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CALCULATE_FARE_SUMMARY_PAGE_ID));
			return getViewForPage(model);
		}
	}

	private boolean isOversizeOrOver5500VehicleSelected(final FareCalculatorForm outboundForm,
			final FareCalculatorForm inboundForm)
	{
		final List<String> loadingRestrictedVehicleCodes = BCFPropertiesUtils.convertToList(LOADING_RESTRICTED_VEHICLE_CODES);
		if (Objects.nonNull(outboundForm) && Objects.nonNull(outboundForm.getItineraryPricingInfo()) && CollectionUtils
				.isNotEmpty(outboundForm.getItineraryPricingInfo().getVehicleFareBreakdownDatas()))
		{
			return StreamUtil.safeStream(outboundForm.getItineraryPricingInfo().getVehicleFareBreakdownDatas()).anyMatch(
					vehicle -> loadingRestrictedVehicleCodes.contains(vehicle.getVehicleTypeQuantity().getVehicleType().getCode()));
		}
		if (Objects.nonNull(inboundForm) && Objects.nonNull(inboundForm.getItineraryPricingInfo()) && CollectionUtils
				.isNotEmpty(inboundForm.getItineraryPricingInfo().getVehicleFareBreakdownDatas()))
		{
			return StreamUtil.safeStream(inboundForm.getItineraryPricingInfo().getVehicleFareBreakdownDatas()).anyMatch(
					vehicle -> loadingRestrictedVehicleCodes.contains(vehicle.getVehicleTypeQuantity().getVehicleType().getCode()));
		}
		return false;
	}

	private boolean isNonBookableRouteSelected(final FareCalculatorForm outboundForm, final FareCalculatorForm inboundForm)
	{
		return (Objects.nonNull(outboundForm) && !outboundForm.getPricedItinerary().getItinerary().getRoute().isReservable()) || (
				Objects.nonNull(inboundForm) && !inboundForm.getPricedItinerary()
						.getItinerary().getRoute().isReservable());
	}
}
