/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.facades.ReservationFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.security.impl.B2BCheckOutAuthenticatonValidator;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.validation.BCFTravellerFormValidator;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.cart.CartFilterParamsDto;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.travel.data.BCFTravellersData;
import com.bcf.facades.traveller.BCFTravellerFacade;


/**
 * Controller for Traveller Details page
 */
@Controller
@SessionAttributes({ BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, BcfFacadesConstants.BCF_FARE_FINDER_FORM })
@RequestMapping("/update-travellers")
public class UpdateTravellersPageController extends TravelAbstractPageController
{
	private static final String UPDATE_TRAVELLERS_PAGE = "updateTravellersPage";
	private static final String RESERVATION_DATA = "reservationData";
	private static final String AMEND_TRAVELLER_FAILED = "text.page.managemybooking.amend.traveller.failed";
	private static final String REFERER = "referer";


	private static final String NEXT_URL = "/traveller-details/next";
	private static final String HOME_PAGE_PATH = "/";
	private static final Logger LOG = Logger.getLogger(UpdateTravellersPageController.class);

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "cartFacade")
	private TravelCartFacade cartFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "bcfTravellerFormValidator")
	private BCFTravellerFormValidator travellerFormValidator;

	@Resource(name = "reservationFacade")
	private ReservationFacade reservationFacade;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "b2BCheckOutAuthenticatonValidator")
	private B2BCheckOutAuthenticatonValidator b2BCheckOutAuthenticatonValidator;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String removeTravellersPage(
			@RequestParam(value = "orderCode", required = false) final String orderCode, final HttpServletRequest request,
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		final CartModel sessionAmendedCart = bcfTravelCartService.getExistingSessionAmendedCart();

		if (setOrderData(orderCode, model, sessionAmendedCart))
			return REDIRECT_PREFIX + HOME_PAGE_PATH;

		if (getTravelCustomerFacade().isCurrentUserB2bCustomer())
		{
			final ValidationResults validationResult = b2BCheckOutAuthenticatonValidator.validate(redirectAttributes);
			if (!ValidationResults.SUCCESS.getResult().equals(validationResult.getResult()))
			{
				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}

		}

		if (!cartFacade.isCurrentCartValid())
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}

		if (!bookingFacade.isCurrentCartOfType(OrderEntryType.TRANSPORT.getCode()))
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_TRAVELLERS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_TRAVELLERS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}

		List<String> routes = new ArrayList<>();
		if (travelCheckoutFacade.containsLongRoute())
		{
			final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
			routes = Stream.of(routeType.split(",")).collect(Collectors.toList());
		}

		final String eBookingRef = sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		final Integer journeyRefNum = sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER);

		//get passenger types and quantity map from the form
		final Map<String, Integer> passengersMap = bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList().stream()
				.filter(passengerTypeQuantityData -> passengerTypeQuantityData.getQuantity() > 0)
				.collect(Collectors
						.groupingBy(paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode(), Collectors.summingInt(
								PassengerTypeQuantityData::getQuantity)));

		//get passenger types and quantity Map from the cart

		Map<String, Long> travellerCartMap = new HashMap<>();
		if (travelCheckoutFacade.containsLongRoute())
		{
			travellerCartMap = travellerFacade
					.getPassengersTypeAndQuantityFromCart(journeyRefNum, eBookingRef);
		}
		else
		{

			travellerCartMap = travellerFacade
					.getAccessibilityPassengersTypeAndQuantityFromCart(journeyRefNum, eBookingRef);
		}

		//get list of passenger type and uids selected for removal
		final List<Pair<String, String>> passengerTypeAndUidsToBeRemoved = bcfFareFinderForm.getPassengerInfoForm()
				.getPassengerTypeAndUidsToBeRemoved();

		//remove the selected passenger type and uids, from the map created from cart
		if (CollectionUtils.isNotEmpty(passengerTypeAndUidsToBeRemoved))
		{
			for (final Pair<String, String> passengerTypeAndUidToBeRemoved : passengerTypeAndUidsToBeRemoved)
			{
				if (travellerCartMap.containsKey(passengerTypeAndUidToBeRemoved.getLeft()))
				{
					if (travellerCartMap.get(passengerTypeAndUidToBeRemoved.getLeft()).intValue() > 1)
					{
						travellerCartMap.put(passengerTypeAndUidToBeRemoved.getLeft(),
								travellerCartMap.get(passengerTypeAndUidToBeRemoved.getLeft()) - 1);
					}
					else
					{
						travellerCartMap.remove(passengerTypeAndUidToBeRemoved.getLeft());
					}
				}
			}
		}

		final Set<String> passengerTypes = new HashSet<>();

		//get the list of passengerTypes to be removed
		if (MapUtils.isNotEmpty(travellerCartMap))
		{
			for (final Map.Entry<String, Long> travellerCartMapEntry : travellerCartMap.entrySet())
			{
				if (passengersMap.containsKey(travellerCartMapEntry.getKey()))
				{
					if (passengersMap.get(travellerCartMapEntry.getKey()).intValue() < travellerCartMapEntry.getValue().intValue())
					{
						passengerTypes.add(travellerCartMapEntry.getKey());
					}
				}
			}
		}

		if (CollectionUtils.isEmpty(passengerTypes))
		{
			return REDIRECT_PREFIX + "/VehicleSelectionPage";
		}
		final BCFTravellersData bcfTravellersData = travellerFacade
				.getPassengersByTypeGroupedByJourney(journeyRefNum, eBookingRef, routes, passengerTypes,
						passengerTypeAndUidsToBeRemoved);
		travellerFacade.getUserSelectedSpecialServiceData(bcfTravellersData,
				bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList(),
				bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList());


		model.addAttribute(BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, bcfTravellersData);
		final ReservationData reservationData = reservationFacade.getCurrentReservationData();
		model.addAttribute(RESERVATION_DATA, reservationData);
		model.addAttribute(BcfvoyageaddonWebConstants.NEXT_URL, "/VehicleSelectionPage");
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		setActionAttributes(model);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/remove/{passengerType}/{uid}", method = RequestMethod.GET)
	public String getRemoveSavedSearchPage(@PathVariable final String passengerType, @PathVariable final String uid,
			final Model model,
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final HttpServletRequest request, final RedirectAttributes redirectModel)
	{

		final String eBookingRef = sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		final Integer journeyRefNum = sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER);

		List<Pair<String, String>> passengerTypeAndUidsToBeRemove = bcfFareFinderForm.getPassengerInfoForm()
				.getPassengerTypeAndUidsToBeRemoved();

		if (CollectionUtils.isEmpty(passengerTypeAndUidsToBeRemove))
		{

			passengerTypeAndUidsToBeRemove = new ArrayList<Pair<String, String>>();

		}
		final List<String> passengerUids = passengerTypeAndUidsToBeRemove.stream()
				.map(passengerTypeAndUidToBeRemove -> passengerTypeAndUidToBeRemove.getRight()).collect(
						Collectors.toList());
		if (!passengerUids.contains(uid))
		{
			passengerTypeAndUidsToBeRemove.add(Pair.of(passengerType, uid));
		}


		bcfFareFinderForm.getPassengerInfoForm().setPassengerTypeAndUidsToBeRemoved(passengerTypeAndUidsToBeRemove);
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);

		final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
		final List<String> routes = Stream.of(routeType.split(",")).collect(Collectors.toList());

		final Integer originDestinationRefNum = sessionService
				.getAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM);

		final CartFilterParamsDto cartFilterParams = bcfTravelCartFacade
				.initializeCartFilterParams(journeyRefNum, originDestinationRefNum, false, routes);

		final Map<String, Long> travellerCartMap = travellerFacade.getPassengersTypeAndQuantityFromCart(journeyRefNum, eBookingRef);

		if (travellerCartMap.containsKey(passengerType))
		{

			if (travellerCartMap.get(passengerType).intValue() > 1)
			{
				travellerCartMap.put(passengerType, travellerCartMap.get(passengerType) - 1);

			}
			else
			{
				travellerCartMap.remove(passengerType);
			}
		}

		if (MapUtils.isNotEmpty(travellerCartMap))
		{

			final Set<String> passengerTypes = new HashSet<>();

			final Map<String, Integer> passengersMap = bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList()
					.stream()
					.filter(passengerTypeQuantityData -> passengerTypeQuantityData.getQuantity() > 0)
					.collect(Collectors
							.groupingBy(paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode(), Collectors.summingInt(
									PassengerTypeQuantityData::getQuantity)));





			for (final Map.Entry<String, Long> travellerCartMapEntry : travellerCartMap.entrySet())
			{

				if (passengersMap.containsKey(travellerCartMapEntry.getKey()))
				{

					if (passengersMap.get(travellerCartMapEntry.getKey()).intValue() < travellerCartMapEntry.getValue().intValue())
					{
						passengerTypes.add(travellerCartMapEntry.getKey());

					}

				}
				else
				{

					passengerTypes.add(travellerCartMapEntry.getKey());
				}

			}

			if (CollectionUtils.isEmpty(passengerTypes))
			{
				if (bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
				{
					return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
				}
				return REDIRECT_PREFIX + "/VehicleSelectionPage";

			}

		}

		return REDIRECT_PREFIX + "/update-travellers";
	}


	private boolean setOrderData(
			@RequestParam(value = "orderCode", required = false) final String orderCode,
			final Model model, final CartModel sessionAmendedCart)
	{
		if (StringUtils.isNotEmpty(orderCode))
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
			if (Objects.isNull(sessionAmendedCart))
			{

				final Boolean amend = bookingFacade.amendOrder(orderCode, bookingFacade.getCurrentUserUid());
				if (!amend)
				{
					// If cart was not created, return validation message
					return true;
				}
			}
			else if (!StringUtils.equals(orderCode, sessionAmendedCart.getCode()))
			{
				bcfTravelCartService.removeSessionCart();
				final Boolean amend = bookingFacade.amendOrder(orderCode, bookingFacade.getCurrentUserUid());
				if (!amend)
				{
					// If cart was not created, return validation message
					return true;
				}
			}
			model.addAttribute("orderCode", orderCode);
		}
		return false;
	}

	private void setActionAttributes(final Model model)
	{
		final CartModel sessionCart = bcfTravelCheckoutFacade.getSessionCart();
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionCart.getBookingJourneyType());
		model.addAttribute(BcfFacadesConstants.MAX_SAILING_ALLOWED,
				bcfTravelCheckoutFacade.isMaxSailingExceedsForUser(sessionCart));
		model.addAttribute(BcfvoyageaddonWebConstants.ADD_ANOTHER_SAILING,
				BcfFacadesConstants.REMOVE_FORM_AND_REDIRECT_TO_HOME_URL);
		model.addAttribute(BcfvoyageaddonWebConstants.LOGIN, BcfstorefrontaddonWebConstants.LOGIN_PAGE_PATH_ONLY);
		model.addAttribute(BcfvoyageaddonWebConstants.CONTINUE_AS_GUEST, NEXT_URL);
	}



}
