/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.cms;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.model.components.ReservationBreakdownComponentModel;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.util.ReservationBreakdownDataHelper;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.reservation.data.ReservationDataList;




@Controller("ReservationBreakdownComponentController")
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.ReservationBreakdownComponent)
public class ReservationBreakdownComponentController
		extends SubstitutingCMSAddOnComponentController<ReservationBreakdownComponentModel>
{
	private static final Logger LOG = Logger.getLogger(ReservationBreakdownComponentController.class);

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "reservationBreakdownDataHelper")
	private ReservationBreakdownDataHelper reservationBreakdownDataHelper;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ReservationBreakdownComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		if (StringUtils.isNotBlank(bookingReference))
		{
			final ReservationDataList reservationDataList = bookingFacade
					.getBookingsByBookingReferenceAndAmendingOrder(bookingReference);
			Map<String, String> qrImageMap = new HashMap<>();
			try
			{
				//THIS METHOD IS TEMPORARY AND WILL BE REMOVED
				final AbstractOrderModel order = bookingFacade.getOrderFromBookingReferenceCode(bookingReference);
				qrImageMap = reservationBreakdownDataHelper.getQRImageMap(order);
			}
			catch (final Exception e)
			{
				LOG.error("Exception : ", e);
			}
			model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
			model.addAttribute(BcfvoyageaddonWebConstants.AMEND, bookingFacade.isAmendment(bookingReference));
			model.addAttribute(BcfvoyageaddonWebConstants.HIDE_BUTTON, "HIDE");
			model.addAttribute(BcfvoyageaddonWebConstants.DISABLE_CURRENCY_SELECTOR, Boolean.TRUE);
			model.addAttribute(BcfvoyageaddonWebConstants.DATE_FORMAT_LABEL, BcfvoyageaddonWebConstants.DATE_FORMAT_MONTH_DAY);
			model.addAttribute(BcfvoyageaddonWebConstants.TIME_FORMAT_LABEL, BcfvoyageaddonWebConstants.TIME_FORMAT_12HR);
			model.addAttribute("QRImageMap", qrImageMap);
		}
	}
}
