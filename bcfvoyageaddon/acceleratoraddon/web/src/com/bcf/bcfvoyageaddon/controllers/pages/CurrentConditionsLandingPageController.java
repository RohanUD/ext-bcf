/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.TravelRouteInfo;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.facades.currentconditions.CurrentConditionsDetailsFacade;
import com.bcf.facades.location.BcfTravelLocationFacade;

@Controller
@RequestMapping("/current-conditions")
public class CurrentConditionsLandingPageController extends CurrentConditionsBaseController
{
	private static final String CURRENT_CONDITIONS_BY_TERMINALS = "currentConditionsByTerminals";

	@Resource(name = "currentConditionsDetailsFacade")
	private CurrentConditionsDetailsFacade currentConditionsDetailsFacade;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getCurrentConditionsLanding(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		final Map<String, List<TravelRouteInfo>> currentConditionsByTerminals = currentConditionsDetailsFacade.getCurrentConditionsByTerminals();
		model.addAttribute("originToDestinationMapping", bcfTravelLocationFacade.createMappingForReturnJourneys(currentConditionsByTerminals));

		model.addAttribute(CURRENT_CONDITIONS_BY_TERMINALS, currentConditionsByTerminals);
		storeCmsPageInModel(model, getContentPageForLabelOrId(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_LANDING_CMS_PAGE));
		setUpMetaDataForContentPage(model,
				getContentPageForLabelOrId(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_LANDING_CMS_PAGE));
		return getViewForPage(model);
	}
}
