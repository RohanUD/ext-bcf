/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.fare.search.resolvers.FareSearchHashResolver;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.constants.TravelservicesConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.TravelAbstractPageController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.AddBundleToCartForm;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.FareCalculatorForm;
import com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.constants.BcfintegrationcoreConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.RouteType;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.BcfTravelRouteUtil;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;
import com.bcf.facades.BcfTransportOfferingFacade;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.impl.BcfCartTimerFacade;
import com.bcf.facades.cart.timer.CartTimerData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.fare.search.BcfFareSearchFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.reservation.data.ReservationDataList;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integration.error.ErrorDTO;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Fare Selection page
 */
@Controller
@SessionAttributes(BcfFacadesConstants.BCF_FARE_FINDER_FORM)
@RequestMapping("/fare-selection")
public class FareSelectionPageController extends TravelAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(FareSelectionPageController.class);

	private static final String FARE_SELECTION_CMS_PAGE = "fareSelectionPage";
	private static final String FARE_CALCULTOR_FARE_SELECTION_CMS_PAGE = "fareCalculatorFareSelectionPage";
	private static final String NO_SEARCH_RESULT = "search.result.not.found.fareFinderForm";
	private static final String REMOVE_SAILING_FAILED = "reservation.remove.sailing.integration.failure";
	private static final String OUT_BOUND_FORM = "outBoundForm";
	private static final String INDEX = "index";
	private static final String NEW_SAILING_DATE = "newSailingDate";

	protected static final String SEARCH = "/search";
	private static final String FARE_CALCULATOR_PAGE_URL = "fareCalculatorPageUrl";

	@Resource(name = "bcfFareSearchFacade")
	private BcfFareSearchFacade fareSearchFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "fareFinderValidator")
	private AbstractTravelValidator fareFinderValidator;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "fareSearchHashResolver")
	private FareSearchHashResolver fareSearchHashResolver;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;


	@Resource(name = "bcfTransportOfferingFacade")
	private BcfTransportOfferingFacade bcfTransportOfferingFacade;

	@Resource(name = "bcfCartTimerFacade")
	private BcfCartTimerFacade bcfCartTimerFacade;

	/**
	 * Method responsible for handling GET request on Fare Selection page
	 *
	 * @param bcfFareFinderForm
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return fare selection page
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getFareSelectionPage(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		return displayFaresForPage(bcfFareFinderForm, bindingResult, model, request,
				getContentPageForLabelOrId(FARE_SELECTION_CMS_PAGE));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fare-calculator")
	public String getFareCalculatorFareSelectionPage(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		try
		{
			model.addAttribute(FARE_CALCULATOR_PAGE_URL, BcfvoyageaddonWebConstants.FARE_CALCULATOR_PAGE);
			return displayFaresForPage(bcfFareFinderForm, bindingResult, model, request,
					getContentPageForLabelOrId(FARE_CALCULTOR_FARE_SELECTION_CMS_PAGE));
		}
		catch (final Exception e)
		{
			LOG.error(e);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_CALCULATOR_PAGE;
		}
	}

	private String displayFaresForPage(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) @Valid final BCFFareFinderForm bcfFareFinderForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final ContentPageModel contentPageModel)
	{
		if (bindingResult.hasErrors())
		{
			request.setAttribute(BcfvoyageaddonWebConstants.BCF_FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
		}
		else
		{
			sessionService.setAttribute(TravelservicesConstants.SEARCH_SEED, fareSearchHashResolver.generateSeed());

			final RouteInfoForm routeInfoForm = bcfFareFinderForm.getRouteInfoForm();
			final StringBuilder sb = new StringBuilder("/routes-fares/schedules/");
			sb.append(BcfTravelRouteUtil.translateName(routeInfoForm.getDepartureLocationName())).append("-");
			sb.append(BcfTravelRouteUtil.translateName(routeInfoForm.getArrivalLocationName())).append("/");
			sb.append(routeInfoForm.getDepartureLocation()).append("-").append(routeInfoForm.getArrivalLocation());
			model.addAttribute("dailySchedulesUrl", sb.toString());

			updateFareFinderFormWithNewDate(request, bcfFareFinderForm);
			addAttributeForSessionBookingJourney(model);

			final FareSearchRequestData fareSearchRequestData = new FareSearchRequestData();
			boolean isReturn = false;
			if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
					.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
			{
				isReturn = true;
			}
			final Integer odRefNum = getOdRefNumber(isReturn);

			bcfFerrySelectionUtil
					.convert(bcfFareFinderForm, fareSearchRequestData, request.getParameter(BcfvoyageaddonWebConstants.DISPLAY_ORDER),
							isReturn, odRefNum);


			try

			{
				final TravelRouteData travelRouteData = bcfFerrySelectionUtil
						.getTravelRouteData(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation(),
								bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
				model.addAttribute(BcfvoyageaddonWebConstants.WAITLIST_LINK, travelRouteData.getWaitListLink());
				model.addAttribute(BcfvoyageaddonWebConstants.TRAVEL_AGENT_WAITLIST_LINK, travelRouteData.getTravelAgentWaitListLink());
				model.addAttribute(BcfvoyageaddonWebConstants.ENABLE_REVENUE_MANAGEMENT, travelRouteData.isEnableRevenueManagement());
				model.addAttribute(BcfvoyageaddonWebConstants.DATE_NOW, new Date());
				model.addAttribute(BcfvoyageaddonWebConstants.ROUTE_TYPE, fareSearchRequestData.getRouteType());
				final String salesChannel = bcfSalesApplicationResolverFacade.getCurrentSalesChannel();

				populateModel(model, bcfFareFinderForm, request, contentPageModel, fareSearchRequestData, salesChannel, odRefNum);

			final FareSelectionData fareSelectionData = fareSearchFacade.performSearch(fareSearchRequestData);

				final boolean showWaitlistLink = fareSelectionData.getPricedItineraries().stream()
						.anyMatch(pricedItineraryData -> !pricedItineraryData.isZeroSubBucketFreeCapacity());
				model.addAttribute(BcfvoyageaddonWebConstants.SHOW_WAITLIST_FORM,!showWaitlistLink);

				if ((Objects.isNull(fareSelectionData) || Collections.isEmpty(fareSelectionData.getPricedItineraries())) || !(showWaitlistLink))
				{
					final List<String> errorMessages = new ArrayList<>();
					errorMessages.add(propertySourceFacade.getPropertySourceValue(NO_SEARCH_RESULT));
					model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES, errorMessages);
					if (StringUtils.equalsIgnoreCase(RouteType.LONG.getCode(), fareSearchRequestData.getRouteType()))
					{
						model.addAttribute(BcfvoyageaddonWebConstants.NEXT_AVAILABILITY_DATES, fareSearchFacade
								.getNextAvailabilityDates(fareSearchRequestData, fareSelectionData.getCalenderSailingRange()));
					}
					storeCmsPageInModel(model, contentPageModel);
					setUpMetaDataRobotsForContentPage(model, contentPageModel.getMetaIndexable(), contentPageModel.getMetaFollow());
					setUpMetaDataForContentPage(model, contentPageModel);
					model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
					request.setAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, bcfFareFinderForm);
					model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID, contentPageModel.isDisplayOrderId());
					return getViewForPage(model);
				}



				// sort fareSelectionData by displayOrder
				final String displayOrder = fareSearchRequestData.getSearchProcessingInfo().getDisplayOrder();
				bcfvoyageaddonComponentControllerUtil.sortFareSelectionData(fareSelectionData, displayOrder, model);

				populateModelAttributes(model, odRefNum, fareSelectionData, salesChannel);

				sessionService.setAttribute(BcfvoyageaddonWebConstants.SESSION_FARE_SELECTION_DATA, fareSelectionData);
				populateModel(model, bcfFareFinderForm.getRouteInfoForm().getTripType(),
						bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(), fareSelectionData,
						bcfFareFinderForm.isReadOnlyResults());
				setJourneyRef(model, odRefNum, fareSelectionData);

				final CartTimerData cartTimerData = bcfCartTimerFacade.getCartStartTimerDetails();
				if (cartTimerData != null)
				{
					model.addAttribute("showTimerStart", cartTimerData.isShowTimerStart());
					model.addAttribute("holdTimeMessage", cartTimerData.getHoldTimeMessage());
				}
				return getViewForPage(model);

			}


			catch (final IntegrationException ex)
			{
				model.addAttribute(BcfvoyageaddonWebConstants.ERROR_MESSAGES,
						ex.getErorrListDTO().getErrorDto().stream().map(ErrorDTO::getErrorDetail).collect(Collectors.toList()));
			}

		}

		return getViewForPage(model);
	}

	private String populateModel(final Model model, final BCFFareFinderForm bcfFareFinderForm, final HttpServletRequest request,
			final ContentPageModel contentPageModel, final FareSearchRequestData fareSearchRequestData, final String salesChannel,
			final Integer odRefNum)
	{


		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, salesChannel);
		final boolean isFareCalculatorFlow = FARE_CALCULTOR_FARE_SELECTION_CMS_PAGE.equals(contentPageModel.getUid());
		setNextUrl(bcfFareFinderForm, model, request, isFareCalculatorFlow);

		setIsReturn(bcfFareFinderForm, model, request);

		if (isFareCalculatorFlow)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FARE_SELECTION_REDIRECT_URL,
					BcfvoyageaddonWebConstants.FARE_CALCULATOR_FARE_SELECTION_ROOT_URL);
		}
		else
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FARE_SELECTION_REDIRECT_URL,
					BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL);
		}
		model.addAttribute(BcfFacadesConstants.CHANGE_FARE_OR_REPLAN,
				request.getParameter(BcfFacadesConstants.CHANGE_FARE_OR_REPLAN));

		model.addAttribute(BcfFacadesConstants.BUNDLE_TYPE_NAME, request.getParameter(BcfFacadesConstants.BUNDLE_TYPE_NAME));
		model.addAttribute(BcfFacadesConstants.TRANSPORT_OFFERINGS,
				request.getParameter(BcfFacadesConstants.TRANSPORT_OFFERINGS));
		model.addAttribute(BcfFacadesConstants.ACCESSIBILITIES, fareSearchRequestData.getAccessibilities());
		model.addAttribute(BcfFacadesConstants.SPECIALSERVICES, fareSearchRequestData.getSpecialServices());
		final String propertyValue = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfvoyageaddonWebConstants.SKIP_VEHICLE_DIMENSIONS_PROPERTY);
		final List<String> vehicleToSkip = BCFPropertiesUtils.convertToList(propertyValue);
		model.addAttribute(BcfvoyageaddonWebConstants.VEHICLE_TO_SKIP,
				vehicleToSkip);

		model.addAttribute("carryingDangerousGoodsInOutbound", fareSearchRequestData.isCarryingDangerousGoodsInOutbound());
		model.addAttribute("carryingDangerousGoodsInReturn", fareSearchRequestData.isCarryingDangerousGoodsInReturn());
		model.addAttribute("returnWithDifferentPassenger", fareSearchRequestData.isReturnWithDifferentPassenger());
		model.addAttribute("returnWithDifferentVehicle", fareSearchRequestData.isReturnWithDifferentVehicle());
		model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_ITINERARY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));



		if (StringUtils.isNotBlank(bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			sessionService.setAttribute(BcfstorefrontaddonWebConstants.SESSION_TRIP_TYPE,
					bcfFareFinderForm.getRouteInfoForm().getTripType());
		}

		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataRobotsForContentPage(model, contentPageModel.getMetaIndexable(), contentPageModel.getMetaFollow());
		setUpMetaDataForContentPage(model, contentPageModel);

		request.setAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM, bcfFareFinderForm);

		model.addAttribute("isAmendmentCart", bcfTravelCartFacade.isAmendmentCart());
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		bcfFareFinderForm.setCurrentPage(BcfvoyageaddonWebConstants.FARE_SELECTION_PAGE);

		if (CollectionUtils.isNotEmpty(bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeAndUidsToBeRemoved()))
		{

			model.addAttribute("passengerUidsToBeRemoved",
					bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeAndUidsToBeRemoved().stream()
							.map(passengerTypeAndUidToBeRemoved -> passengerTypeAndUidToBeRemoved.getRight())
							.collect(Collectors.toList()));
		}


		return getViewForPage(model);


	}

	private void setJourneyRef(final Model model, final Integer odRefNum, final FareSelectionData fareSelectionData)
	{
		if (bcfTravelCartFacadeHelper.isAlacateFlow())
		{
			model.addAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER,
					sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER));
			model.addAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM,
					odRefNum);
		}
		else if (bcfTravelCartFacade.isAmendmentCart())
		{
			final int originalODRef = bcfTravelCartFacade.getOriginalODReference();
			model.addAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER,
					bcfTravelCartFacade.getOriginalJourneyReference());
			model.addAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, originalODRef);
			StreamUtil.safeStream(fareSelectionData.getPricedItineraries()).forEach(pricedItineraryData -> pricedItineraryData
					.setOriginDestinationRefNumber(originalODRef));
		}
		else
		{
			model.addAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER,
					sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER));
			model.addAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM,
					odRefNum);
		}
	}

	private void populateModelAttributes(final Model model, final int odRefNum, final FareSelectionData fareSelectionData,
			final String salesChannel)
	{
		final PricedItineraryData firstItinerary = bcfFerrySelectionUtil
				.getFirstItinerary(fareSelectionData, odRefNum);
		if (!model.asMap().containsKey(FARE_CALCULATOR_PAGE_URL) && firstItinerary != null)
		{
			model.addAttribute("isReservable", firstItinerary.getItinerary().getRoute().isReservable());
		}
		if (salesChannel != null && BCFPropertiesUtils
				.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.ALCARTE_SALES_CHANNELS))
				.contains(salesChannel))
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FIRST_ITINERARY, firstItinerary);

			if (firstItinerary != null)
			{
				model.addAttribute("isOpenTicketAllowed",
						bcfTravelRouteFacade.isOpenTicketAllowedForRoute(firstItinerary.getItinerary().getRoute().getCode()));
			}

		}
	}

	private void setIsReturn(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) @Valid final BCFFareFinderForm bcfFareFinderForm,
			final Model model, final HttpServletRequest request)
	{
		if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
				.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
		{
			final TravelRouteData travelRouteReturnData = getTravelRouteData(
					bcfFareFinderForm.getRouteInfoForm().getArrivalLocation(),
					bcfFareFinderForm.getRouteInfoForm().getDepartureLocation());
			final ReservationDataList reservationDataList = bcfReservationFacade
					.getCurrentReservationsByTravelRoute(travelRouteReturnData.getCode());
			model.addAttribute(BcfvoyageaddonWebConstants.RESERVATION_DATA_LIST, reservationDataList);
			model.addAttribute(BcfFacadesConstants.IS_RETURN, "true");
			model.addAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, BcfFacadesConstants.BOOKING_TRANSPORT_ONLY);
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.IS_RETURN, "false");
		}
	}

	private void setNextUrl(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) @Valid final BCFFareFinderForm bcfFareFinderForm,
			final Model model, final HttpServletRequest request, final boolean isFareCalculatorFlow)
	{

		if (request.getParameterMap().containsKey(INDEX) || request.getParameterMap().containsKey("change") || bcfFareFinderForm
				.getRouteInfoForm().getTripType().equals(TripType.SINGLE.name()))
		{
			model.addAttribute(BcfFacadesConstants.NEXT_URL, bcfControllerUtil.getNextPageURL());
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.NEXT_URL,
					getNextUrl(bcfFareFinderForm.getRouteInfoForm().getTripType(), request, isFareCalculatorFlow));
		}
	}

	@RequestMapping(value = "/change-selection/{journeyRef}/{odRefNum}", method = RequestMethod.GET)
	public String changeFerrySelection(
			@PathVariable(value = "journeyRef") final Integer journeyRef, @PathVariable("odRefNum") final int odRefNum,
			final RedirectAttributes redirectModel, final Model model, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		final BCFFareFinderForm bcfFareFinderForm = new BCFFareFinderForm();
		bcfFerrySelectionUtil.initializeFareFinderForm(bcfFareFinderForm, journeyRef, odRefNum);
		bcfFerrySelectionUtil.convertToOneWay(bcfFareFinderForm,odRefNum);
		sessionService.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, journeyRef);
		sessionService.setAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM, odRefNum);
		final String eBookingRef=bcfFerrySelectionUtil.getEbookingRef(journeyRef,odRefNum);
		if(eBookingRef!=null){
			sessionService.setAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE, eBookingRef);
		}
		WebUtils.setSessionAttribute(request, BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.ROUTE_PAGE + "?modify=true";


	}

	@RequestMapping(value = "/remove-selection/{journeyRef}/{odRefNum}", method = RequestMethod.POST)
	public String removeFerrySelection(
			@PathVariable(value = "journeyRef") final Integer journeyRef, @PathVariable("odRefNum") final int odRefNum,
			final RedirectAttributes redirectModel, final HttpServletRequest request)
	{

		try
		{
			bcfTravelCartFacade.modifyCartEntriesForRefNo(journeyRef, odRefNum, false);


		}
		catch (final CalculationException | IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_SAILING_FAILED);
		}

		return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();
	}

	private Integer getOdRefNumber(final boolean isReturn)
	{

		final Object odRefNumber = sessionService.getAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM);
		if (bcfTravelCartFacade.isAmendmentCart() && odRefNumber != null)
		{
			return (Integer) odRefNumber;
		}
		else if (isReturn)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	private TravelRouteData getTravelRouteData(final String arrivalLocation, final String departureLocation)
	{
		if (arrivalLocation == null || departureLocation == null)
		{
			return null;
		}
		return (bcfTravelRouteFacade.getTravelRoutes(arrivalLocation, departureLocation))
				.stream().findFirst().orElse(null);
	}


	private void updateFareFinderFormWithNewDate(final HttpServletRequest request,
			final BCFFareFinderForm bcfFareFinderForm)
	{
		if (request.getParameterMap().containsKey(NEW_SAILING_DATE) && Objects
				.nonNull(request.getParameterMap().containsKey(NEW_SAILING_DATE)))
		{
			final Calendar cal = Calendar.getInstance();
			cal.setTime(TravelDateUtils
					.getDate(request.getParameter(NEW_SAILING_DATE), BcfintegrationcoreConstants.BCF_SAILING_DATE_TIME_PATTERN));
			if (request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN) && request
					.getParameter(BcfFacadesConstants.IS_RETURN).equals("true"))
			{
				bcfFareFinderForm.getRouteInfoForm()
						.setReturnDateTime(
								TravelDateUtils.convertDateToStringDate(cal.getTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			}
			else
			{
				bcfFareFinderForm.getRouteInfoForm()
						.setDepartingDateTime(
								TravelDateUtils.convertDateToStringDate(cal.getTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			}
		}
	}

	private void addAttributeForSessionBookingJourney(final Model model)
	{
		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (sessionBookingJourney != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY, sessionBookingJourney);
		}
		else
		{
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
			model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
		}
	}

	@RequestMapping(value = "/clear-session-form", method = RequestMethod.GET)
	public String clearFormAndRedirectToHome(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm, final Model model)
	{
		return REDIRECT_PREFIX + "/";
	}

	@RequestMapping(value = "/clear-session-calculatefare", method = RequestMethod.GET)
	public String clearFormAndRedirectToCalculateFare(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm, final Model model)
	{
		resetFormFields(bcfFareFinderForm);
		sessionService.removeAttribute(OUT_BOUND_FORM);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_CALCULATOR_PAGE;
	}

	/**
	 * Method called to sort the FareSelectionData based on a selected sorting parameter.
	 *
	 * @param bcfFareFinderForm as the fareFinderForm
	 * @param bindingResult     as the bindingResult of the fareFinderForm
	 * @param refNumber         as the refNumber of the leg to order
	 * @param displayOrder      as the selected parameter used to order the fareSelectionData
	 * @param model
	 * @param request
	 * @param response
	 * @return the JSON object to refresh the result section with the sorted results
	 */
	@RequestMapping(value = "/sorting-fare-selection-results", method = RequestMethod.POST)
	public String sortingFareSelectionResults(@Valid final BCFFareFinderForm bcfFareFinderForm, final BindingResult bindingResult,
			@RequestParam final String refNumber, @RequestParam final String displayOrder, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList() == null)
		{
			bcfFareFinderForm.getPassengerInfoForm().setPassengerTypeQuantityList(createPassengerTypes(request));
		}

		fareFinderValidator.validate(bcfFareFinderForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			request.setAttribute(BcfvoyageaddonWebConstants.FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
		}
		else
		{
			try
			{
				final FareSelectionData fareSelectionData = sessionService
						.getAttribute(BcfvoyageaddonWebConstants.SESSION_FARE_SELECTION_DATA);

				if (displayOrder.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_AND_UNDERSCORES))
				{
					bcfvoyageaddonComponentControllerUtil.sortFareSelectionData(fareSelectionData, displayOrder, model);
				}
				populateModel(model, bcfFareFinderForm.getRouteInfoForm().getTripType(),
						bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(), fareSelectionData,
						bcfFareFinderForm.isReadOnlyResults());
				model.addAttribute(BcfvoyageaddonWebConstants.REF_NUMBER, refNumber);
			}
			catch (final NumberFormatException ex)
			{
				LOG.error("Cannot parse ref number to integer", ex);
			}
		}

		final ContentPageModel contentPage = getContentPageForLabelOrId(FARE_SELECTION_CMS_PAGE);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataRobotsForContentPage(model, contentPage.getMetaIndexable(), contentPage.getMetaFollow());
		setUpMetaDataForContentPage(model, contentPage);

		return BcfvoyageaddonControllerConstants.Views.Pages.FareSelection.fareSelectionSortingResult;
	}

	/**
	 * Method takes the request URL and extracts the passenger type and passenger quantity to create a list of
	 * PassengerTypeQuantityData
	 *
	 * @param request
	 * @return List<PassengerTypeQuantityData>
	 */
	protected List<PassengerTypeQuantityData> createPassengerTypes(final HttpServletRequest request)
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();

		final List<PassengerTypeData> passengerTypesDatas = travellerSortStrategy
				.sortPassengerTypes(passengerTypeFacade.getPassengerTypes());

		final String maxPersonalAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		final int maxQuantity = StringUtils.isNotEmpty(maxPersonalAllowed) ? Integer.parseInt(maxPersonalAllowed) : 10;

		for (final PassengerTypeData passengerTypeData : passengerTypesDatas)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(0);

			final String passengerQuantity = request.getParameter(passengerTypeData.getCode());

			if (StringUtils.isNotBlank(passengerQuantity))
			{
				updatePassengerTypeQuantityData(passengerQuantity, passengerTypeQuantityData, passengerTypeData, maxQuantity);
			}
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}

	private void updatePassengerTypeQuantityData(final String passengerQuantity,
			final PassengerTypeQuantityData passengerTypeQuantityData, final PassengerTypeData passengerTypeData,
			final int maxQuantity)
	{

		try
		{
			final int quantity = Integer.parseInt(passengerQuantity);
			if (quantity < 0 || quantity > maxQuantity)
			{
				passengerTypeQuantityData
						.setQuantity(passengerTypeData.getCode().equalsIgnoreCase(BcfvoyageaddonWebConstants.PASSENGER_TYPE_ADULT)
								? BcfFacadesConstants.DEFAULT_ADULTS
								: 0);
			}
			else
			{
				passengerTypeQuantityData.setQuantity(quantity);
			}
		}
		catch (final NumberFormatException nfe)
		{
			passengerTypeQuantityData
					.setQuantity(passengerTypeData.getCode().equalsIgnoreCase(BcfvoyageaddonWebConstants.PASSENGER_TYPE_ADULT)
							? BcfFacadesConstants.DEFAULT_ADULTS
							: 0);
		}
	}



	private String getNextUrl(final String tripType, final HttpServletRequest request, final boolean isFareCalculatorFlow)
	{
		final String fareSelectionRootUrl = isFareCalculatorFlow ?
				BcfvoyageaddonWebConstants.FARE_CALCULATOR_FARE_SELECTION_ROOT_URL :
				BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
		if (TripType.RETURN.name().equals(tripType) && !request.getParameterMap().containsKey(BcfFacadesConstants.IS_RETURN))
		{
			return fareSelectionRootUrl + "?" + BcfFacadesConstants.IS_RETURN + "=true&index=0";
		}
		return fareSelectionRootUrl + "?" + BcfFacadesConstants.IS_RETURN + "=" + request
				.getParameter(BcfFacadesConstants.IS_RETURN)
				+ "&index=1";
	}

	/**
	 * Method handles the preparation of the Model object
	 *
	 * @param tripType
	 * @param departureDateTime
	 * @param model
	 * @param fareSelectionData
	 */
	public void populateModel(final Model model, final String tripType, final String departureDateTime,
			final FareSelectionData fareSelectionData, final Boolean readOnlyResults)
	{
		final String priceDisplayPassengerType = configurationService.getConfiguration()
				.getString(BcfvoyageaddonWebConstants.PRICE_DISPLAY_PASSENGER_TYPE);
		model.addAttribute(BcfvoyageaddonWebConstants.MODEL_PRICE_DISPLAY_PASSENGER_TYPE, priceDisplayPassengerType);

		model.addAttribute(BcfvoyageaddonWebConstants.FARE_SELECTION, fareSelectionData);
		model.addAttribute(BcfvoyageaddonWebConstants.TRIP_TYPE, tripType);
		if (readOnlyResults)
		{
			final FareCalculatorForm fareCalculatorForm = new FareCalculatorForm();
			model.addAttribute(BcfvoyageaddonWebConstants.FARE_CALCULATOR_FORM, fareCalculatorForm);
			model.addAttribute("isFareFinderComponent", true);
		}
		else
		{
			final AddBundleToCartForm addBundleToCartForm = new AddBundleToCartForm();
			model.addAttribute(BcfvoyageaddonWebConstants.ADD_BUNDLE_TO_CART_FORM, addBundleToCartForm);
			model.addAttribute("isFareFinderComponent", false);
		}
		model.addAttribute(BcfstorefrontaddonWebConstants.ADD_BUNDLE_TO_CART_URL, BcfvoyageaddonWebConstants.ADD_BUNDLE_URL);
		final List<Date> outboundDates = getDatesForTabs(departureDateTime);
		model.addAttribute(BcfvoyageaddonWebConstants.OUTBOUND_DATES, outboundDates);

		model.addAttribute(BcfvoyageaddonWebConstants.TAB_DATE_FORMAT, BcfvoyageaddonWebConstants.FARE_SELECTION_TAB_DATE_FORMAT);
		model.addAttribute(BcfvoyageaddonWebConstants.PI_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormat"));
		model.addAttribute(BcfvoyageaddonWebConstants.PI_VIEW_JOURNEY_DATE_FORMAT,
				bcfConfigurablePropertiesService.getBcfPropertyValue("timeFormatForViewJourney"));
		model.addAttribute(BcfFacadesConstants.DATE_FORMAT, BcfvoyageaddonWebConstants.DATE_FORMAT);
		model.addAttribute(BcfFacadesConstants.TIME_FORMAT, BcfvoyageaddonWebConstants.TIME_FORMAT);
		model.addAttribute(BcfvoyageaddonWebConstants.OUTBOUND_REF_NUMBER, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.INBOUND_REF_NUMBER, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER);
		model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_OUTBOUND_OPTIONS,
				bcfvoyageaddonComponentControllerUtil
						.countJourneyOptions(fareSelectionData, TravelfacadesConstants.OUTBOUND_REFERENCE_NUMBER));
		model.addAttribute(BcfvoyageaddonWebConstants.NO_OF_INBOUND_OPTIONS,
				bcfvoyageaddonComponentControllerUtil
						.countJourneyOptions(fareSelectionData, TravelfacadesConstants.INBOUND_REFERENCE_NUMBER));
	}

	/**
	 * Creates a list of dates which will be displayed in Fare Selection table tabs
	 *
	 * @param selectedDateString - date selected for a leg
	 * @return a list of dates for tabs
	 */
	private List<Date> getDatesForTabs(final String selectedDateString)
	{
		final List<Date> dates = new ArrayList<>();
		final SimpleDateFormat formatter = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

		Date selectedDate = null;
		try
		{
			selectedDate = formatter.parse(selectedDateString);
			final Calendar cal = new GregorianCalendar();
			cal.setTime(selectedDate);
			cal.add(Calendar.DAY_OF_MONTH, -3);
			for (int i = 0; i < 10; i++)
			{
				cal.add(Calendar.DAY_OF_MONTH, 1);
				dates.add(cal.getTime());
			}
		}
		catch (final ParseException e)
		{
			LOG.error("Error while parsing dates for tabs: " + selectedDateString);
		}

		return dates;
	}

	private void resetFormFields(final BCFFareFinderForm bcfFareFinderForm)
	{
		bcfFareFinderForm.setRouteInfoForm(null);
		bcfFareFinderForm.setPassengerInfoForm(null);
		bcfFareFinderForm.setVehicleInfoForm(null);
		bcfFareFinderForm.setCurrentPage(null);
	}
}
