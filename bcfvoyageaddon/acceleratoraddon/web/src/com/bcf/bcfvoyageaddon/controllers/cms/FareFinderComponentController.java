/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.TripType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfstorefrontaddon.util.BcfOriginalBookingUtil;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.controllers.misc.AddToCartController;
import com.bcf.bcfvoyageaddon.enums.FerrySearchPage;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.PassengerInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm;
import com.bcf.bcfvoyageaddon.model.components.FareFinderComponentModel;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.RouteType;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.message.code.resolver.CustomMessageCodesResolver;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.VehicleType.VehicleTypeFacade;
import com.bcf.facades.add.to.cart.helpers.RemoveInboundLegHelper;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.bcffacades.BcfUserFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;
import com.bcf.facades.travel.data.BcfRouteReponseData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.traveller.BCFTravellerFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;
import com.bcf.integrations.core.exception.IntegrationException;
import com.rits.cloning.Cloner;


/**
 * Fare Finder Controller for handling requests for the Fare Finder Component.
 */
@Controller("FareFinderComponentController")
@SessionAttributes(BcfFacadesConstants.BCF_FARE_FINDER_FORM)
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.FareFinderComponent)
public class FareFinderComponentController extends AbstractFinderComponentController
{

	private static final Logger LOG = Logger.getLogger(FareFinderComponentController.class);
	protected static final String SEARCH = "/search";
	protected static final String FARE_CALCULATOR_SEARCH = "/fareCalculatorSearch";
	protected static final String NEXT_STEP = "/next-step";
	private static final String VEHICLES = "vehicles";
	private static final String DEFAULT_TRIP_TYPE = "defaultTripType";
	private static final String PASSENGER_SELECTION_PAGE = "/PassengerSelectionPage";
	private static final String VEHICLE_SELECTION_PAGE = "/VehicleSelectionPage";
	protected static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";
	public static final String OUTBOUND = "outbound";

	@Resource(name = "routeInfoValidator")
	protected AbstractTravelValidator routeInfoValidator;

	@Resource(name = "passengerInfoValidator")
	protected AbstractTravelValidator passengerInfoValidator;

	@Resource(name = "vehicleInfoValidator")
	protected AbstractTravelValidator vehicleInfoValidator;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "vehicleTypeFacade")
	private VehicleTypeFacade vehicleTypeFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "transportFacilityFacade")
	private BCFTransportFacilityFacade bcfTransportFacilityFacade;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "ferrySearchPageViewMap")
	private Map<FerrySearchPage, String> ferrySearchPageViewMap;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfSpecialAssistanceFacade")
	private BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade;

	@Resource(name = "messageCodesResolver")
	private CustomMessageCodesResolver customMessageCodesResolver;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "removeInboundLegHelper")
	private RemoveInboundLegHelper removeInboundLegHelper;

	@Resource(name = "ancillaryProductFacade")
	protected AncillaryProductFacade ancillaryProductFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfOriginalBookingUtil")
	private BcfOriginalBookingUtil bcfOriginalBookingUtil;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade travelCheckoutFacade;

	@Resource(name = "travellerFacade")
	private BCFTravellerFacade travellerFacade;

	@Resource(name = "userService")
	private BcfUserService userService;

	@Resource(name = "userFacade")
	private BcfUserFacade bcfUserFacade;

	/**
	 * Method responsible for populating the data model with initial data for the component
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
	{
		final FareFinderComponentModel fareFinderComponentModel = (FareFinderComponentModel) component;
		final BCFFareFinderForm bcfFareFinderForm;

		if (Objects.nonNull(model.asMap().get(BcfFacadesConstants.BCF_FARE_FINDER_FORM)))
		{
			bcfFareFinderForm = (BCFFareFinderForm) model.asMap().get(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		}
		else
		{
			bcfFareFinderForm = new BCFFareFinderForm();
			model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		}


		switch (fareFinderComponentModel.getView())
		{
			case ROUTEINFO:
				setRouteData(model, request, bcfFareFinderForm, fareFinderComponentModel);
				break;
			case PASSENGERINFO:
				setPassengerData(model, bcfFareFinderForm);
				break;
			case VEHICLEINFO:
				setVehicleData(model, bcfFareFinderForm);
				break;
			case FARECALCULATORINFO:
				setFareCalculatorData(model, bcfFareFinderForm, fareFinderComponentModel, request);
				break;
			case EDITJOURNEYINFO:
				updateCurrentPageInForm(bcfFareFinderForm);
				break;
			default:
				break;
		}

		if (StringUtils.equals(request.getParameter("sailing"), "true"))
		{
			populateRouteInfoFromRequest(request, bcfFareFinderForm);
		}
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		if (!bcfUserFacade.isAnonymousUser())
		{
			model.addAttribute("isFullAccountUser",
					AccountType.FULL_ACCOUNT.equals(((CustomerModel) userService.getCurrentUser()).getAccountType()));
		}
		getSessionService().removeAttribute(BcfFacadesConstants.DEPARTURE_LOCATION);
		getSessionService().removeAttribute(BcfFacadesConstants.ARRIVAL_LOCATION);
	}

	private void updateCurrentPageInForm(final BCFFareFinderForm bcfFareFinderForm)
	{
		if (StringUtils.equals(sessionService.getAttribute(BcfstorefrontaddonWebConstants.PAGE_LABEL), PASSENGER_SELECTION_PAGE))
		{
			bcfFareFinderForm.setCurrentPage(BcfvoyageaddonWebConstants.PASSENGER_INFO);
		}
		else if (StringUtils.equals(sessionService.getAttribute(BcfstorefrontaddonWebConstants.PAGE_LABEL), VEHICLE_SELECTION_PAGE))
		{
			bcfFareFinderForm.setCurrentPage(BcfvoyageaddonWebConstants.VEHICLE_INFO);
		}
		else
		{
			bcfFareFinderForm.setCurrentPage(BcfvoyageaddonWebConstants.DEFAULT);
		}
		sessionService.setAttribute(BcfstorefrontaddonWebConstants.PAGE_LABEL, BcfvoyageaddonWebConstants.DEFAULT);
	}

	private void setFareCalculatorData(final Model model, final BCFFareFinderForm bcfFareFinderForm,
			final FareFinderComponentModel fareFinderComponentModel, final HttpServletRequest request)
	{
		setPassengerData(model, bcfFareFinderForm);
		setVehicleData(model, bcfFareFinderForm);
		setRouteData(model, request, bcfFareFinderForm, fareFinderComponentModel);
		if (Objects.nonNull(request))
		{
			populateRouteInfoFromRequest(request, bcfFareFinderForm);
		}
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
	}

	private void populateRouteInfoFromRequest(final HttpServletRequest request, final BCFFareFinderForm bcfFareFinderForm)
	{
		final String arrivalLocation = request.getParameter("arrivalLocation");
		if (Objects.nonNull(arrivalLocation))
		{
			final String departingDateTime = StringUtils.isEmpty(request.getParameter("scheduleDate")) ?
					request.getParameter("departingDateTime") :
					request.getParameter("scheduleDate");
			bcfFareFinderForm.getRouteInfoForm().setDepartingDateTime(departingDateTime);

			final String departureLocation = request.getParameter("departureLocation");
			final String departureLocationName = request.getParameter("departureLocationName");
			final String departureLocationSuggType = request.getParameter("departureLocationSuggType");
			bcfFareFinderForm.getRouteInfoForm().setDepartureLocationSuggestionType(departureLocationSuggType);
			if (StringUtils.isEmpty(departureLocationSuggType))
			{
				bcfFareFinderForm.getRouteInfoForm().setDepartureLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());
			}

			final String arrivalLocationSuggType = request.getParameter("arrivalLocationSuggType");
			bcfFareFinderForm.getRouteInfoForm().setArrivalLocationSuggestionType(arrivalLocationSuggType);
			if (StringUtils.isEmpty(arrivalLocationSuggType))
			{
				bcfFareFinderForm.getRouteInfoForm().setArrivalLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());
			}

			bcfFareFinderForm.getRouteInfoForm().setDepartureLocation(departureLocation);
			bcfFareFinderForm.getRouteInfoForm().setDepartureLocationName(departureLocationName);
			if (StringUtils.isEmpty(departureLocationName) && StringUtils.isNotEmpty(departureLocation))
			{
				final TransportFacilityData departureLocationData = bcfTransportFacilityFacade
						.getTransportFacility(departureLocation);
				bcfFareFinderForm.getRouteInfoForm().setDepartureLocationName(departureLocationData.getName());
			}

			bcfFareFinderForm.getRouteInfoForm().setArrivalLocation(arrivalLocation);
			final String arrivalLocationName = request.getParameter("arrivalLocationName");
			bcfFareFinderForm.getRouteInfoForm().setArrivalLocationName(arrivalLocationName);
			if (StringUtils.isEmpty(arrivalLocationName))
			{
				final TransportFacilityData arrivalLocationData = bcfTransportFacilityFacade
						.getTransportFacility(arrivalLocation);
				bcfFareFinderForm.getRouteInfoForm().setArrivalLocationName(arrivalLocationData.getName());
			}

			final RouteType routeType = bcfTravelRouteFacade
					.getRouteType(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation(),
							bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
			if (Objects.nonNull(routeType))
			{
				bcfFareFinderForm.getRouteInfoForm().setRouteType(routeType.getCode());
			}
		}
	}

	private void setRouteData(final Model model, final HttpServletRequest request, final BCFFareFinderForm bcfFareFinderForm,
			final FareFinderComponentModel fareFinderComponentModel)
	{
		model.addAttribute(BcfstorefrontaddonWebConstants.CURRENT_PAGE, BcfvoyageaddonWebConstants.ROUTE_INFO);
		boolean modify = false;
		if (request != null && request.getParameter(BcfstorefrontaddonWebConstants.MODIFY) != null && request
				.getParameter(BcfstorefrontaddonWebConstants.MODIFY).equals("true"))
		{
			modify = true;
		}

		if (!modify && (bcfTravelCartFacadeHelper.isAlacateFlow() || !bcfTravelCartFacade.isAmendmentCart()))
		{
			getSessionService()
					.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER, bcfTravelCartFacade.getNextJourneyRefNumber());
		}

		if (Objects.nonNull(model.asMap().get(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER)))
		{
			getSessionService()
					.setAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER,
							model.asMap().get(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER));
		}

		if (Objects.isNull(bcfFareFinderForm.getRouteInfoForm()))
		{
			initializeRouteInfoForm(bcfFareFinderForm, model);
		}

		model.addAttribute("links", fareFinderComponentModel != null ? fareFinderComponentModel.getLinks() : null);
		model.addAttribute("longRouteCalenderLimit",
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfvoyageaddonWebConstants.LONG_ROUTE_MAX_CALENDER_LIMIT));
		model.addAttribute("shortRouteCalenderLimit",
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfvoyageaddonWebConstants.SHORT_ROUTE_MAX_CALENDER_LIMIT));
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
	}

	private void setVehicleData(final Model model, final BCFFareFinderForm bcfFareFinderForm)
	{
		if (Objects.isNull(bcfFareFinderForm.getVehicleInfoForm()))
		{
			initializeVehicleInfoForm(bcfFareFinderForm, model);
		}
		else if (bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().size() == 1 && "RETURN"
				.equals(bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			BCFvoyageaddonComponentControllerUtil.setReturnVehicleData(bcfFareFinderForm.getVehicleInfoForm());
		}
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && Objects.nonNull(bcfFareFinderForm.getRouteInfoForm()))
		{
			setTravelRouteInfo(bcfFareFinderForm, bcfFareFinderForm.getVehicleInfoForm(), model);
		}
		bcfFerrySelectionUtil.setVehicleDimensionThreshold(bcfFareFinderForm);
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
		model.addAttribute("hasNonOptionSailingInCart", bcfTravelCartFacade.hasNonOptionSailingInCart());
	}

	private void setPassengerData(final Model model, final BCFFareFinderForm bcfFareFinderForm)
	{
		bcfFerrySelectionUtil.clearReturnSelections(bcfFareFinderForm);
		bcfFerrySelectionUtil.updateWalkOnAttributes(bcfFareFinderForm.getRouteInfoForm());
		if (Objects.isNull(bcfFareFinderForm.getPassengerInfoForm()))
		{
			initializePassengerInfoForm(bcfFareFinderForm);
		}
		if (Objects.nonNull(bcfFareFinderForm.getRouteInfoForm()))
		{
			final boolean allowAdditionalPassengerTypes = bcfTravelRouteFacade
					.getAllowAdditionalPassengerTypes(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation(),
							bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
			model.addAttribute("allowAdditionalPassengerTypes", allowAdditionalPassengerTypes);
		}
		final String maxPassengersAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		bcfFareFinderForm.getPassengerInfoForm()
				.setMaxPassengersAllowed(StringUtils.isNotEmpty(maxPassengersAllowed) ? Integer.parseInt(maxPassengersAllowed) : 9);
		bcfFerrySelectionUtil.cleanUpPassengerInfoForm(bcfFareFinderForm);
		bcfFareFinderForm.getPassengerInfoForm().setPassengerTypeAndUidsToBeRemoved(null);
		bcfOriginalBookingUtil.setAdditionalAttributes(model);
	}

	@RequestMapping(value = "PassengerInfo", method = RequestMethod.POST)
	public String getPassengerInfoPage(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final RedirectAttributes redirectModel, final BindingResult bindingResult)
	{
		validateForm(routeInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					AddToCartController.ADD_BUNDLE_TO_CART_REQUEST_ERROR);
			return REDIRECT_PREFIX + "/";
		}
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.PASSENGER_PAGE;
	}

	@Override
	protected String getView(final AbstractFinderComponentModel component)
	{
		final FareFinderComponentModel fareFinderComponentModel = (FareFinderComponentModel) component;
		return ferrySearchPageViewMap.get(fareFinderComponentModel.getView());
	}

	private void initializeRouteInfoForm(final BCFFareFinderForm bcfFareFinderForm, final Model model)
	{
		final RouteInfoForm routeInfoForm = new RouteInfoForm();
		routeInfoForm.setTripType(bcfConfigurablePropertiesService.getBcfPropertyValue(DEFAULT_TRIP_TYPE));
		bcfFareFinderForm.setRouteInfoForm(routeInfoForm);
		bcfFareFinderForm.setThresholdHours(Double.parseDouble(bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.SAME_DAY_TRANSPORT_BOOKING_THRESHOLD_HOURS)));
		model.addAttribute("terminalsCacheVersion", cmsSiteService.getCurrentSite().getTerminalsCacheVersion());
	}

	private void initializePassengerInfoForm(final BCFFareFinderForm bcfFareFinderForm)
	{
		final PassengerInfoForm passengerInfoForm = new PassengerInfoForm();
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = getPassengerTypeQuantityList();
		bcfvoyageaddonComponentControllerUtil
				.setPassengerTypeQuantityList(passengerInfoForm, BcfFacadesConstants.DEFAULT_ADULTS,
						passengerTypeQuantityList);
		passengerInfoForm.setLargeItems(getLargeItemData());
		passengerInfoForm.setLargeItemsInbound(getLargeItemData());
		bcfFareFinderForm.setPassengerInfoForm(passengerInfoForm);
		bcfFareFinderForm.getPassengerInfoForm().setBasePassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfBasePassengers")));
		bcfFareFinderForm.getPassengerInfoForm().setAdditionalPassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfAdditionalPassengers")));
		bcfFareFinderForm.getPassengerInfoForm().setNorthernPassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue("bcfNorthernPassengers")));
		bcfFerrySelectionUtil.sortPassengers(passengerInfoForm);
	}



	private List<LargeItemData> getLargeItemData()
	{
		final List<ProductData> productDataList = ancillaryProductFacade.getAncillaryProductsForCodes(
				BCFPropertiesUtils.convertToList(
						bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES)));
		return StreamUtil.safeStream(productDataList)
				.map(this::createLargeItemsData)
				.collect(Collectors.toList());
	}

	private LargeItemData createLargeItemsData(final ProductData productData)
	{
		final LargeItemData largeItemsData = new LargeItemData();
		largeItemsData.setCode(productData.getCode());
		largeItemsData.setName(productData.getName());
		largeItemsData.setQuantity(0);
		return largeItemsData;
	}

	private void initializeVehicleInfoForm(final BCFFareFinderForm bcfFareFinderForm, final Model model)
	{
		final VehicleInfoForm vehicleInfoForm = new VehicleInfoForm();
		bcfvoyageaddonComponentControllerUtil.setVehicleData(vehicleInfoForm);
		model.addAttribute(VEHICLES, vehicleTypeFacade.getAllVehicleTypes());
		final String propertyValue = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfvoyageaddonWebConstants.SKIP_VEHICLE_DIMENSIONS_PROPERTY);
		final List<String> vehicleToSkip = BCFPropertiesUtils.convertToList(propertyValue);
		model.addAttribute(BcfvoyageaddonWebConstants.VEHICLE_TO_SKIP, vehicleToSkip);
		bcfFareFinderForm.setVehicleInfoForm(vehicleInfoForm);
	}

	private void setTravelRouteInfo(final BCFFareFinderForm bcfFareFinderForm, final VehicleInfoForm vehicleInfoForm,
			final Model model)
	{
		final TravelRouteData travelRouteData = bcfFerrySelectionUtil
				.getTravelRouteData(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation(),
						bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
		if (Objects.isNull(travelRouteData))
		{
			return;
		}
		vehicleInfoForm.getVehicleInfo().get(0).setTravelRouteCode(travelRouteData.getCode());
		vehicleInfoForm.getVehicleInfo().get(0).setAllowMotorcycle(travelRouteData.isAllowMotorcycles());
		model.addAttribute("reservable", travelRouteData.isReservable());

		if (bcfFareFinderForm.getRouteInfoForm().getTripType().equalsIgnoreCase("RETURN"))
		{
			final TravelRouteData travelRouteReturnData = bcfFerrySelectionUtil
					.getTravelRouteData(bcfFareFinderForm.getRouteInfoForm().getArrivalLocation(),
							bcfFareFinderForm.getRouteInfoForm().getDepartureLocation());
			vehicleInfoForm.getVehicleInfo().get(1).setTravelRouteCode(travelRouteReturnData.getCode());
			vehicleInfoForm.getVehicleInfo().get(1).setAllowMotorcycle(travelRouteReturnData.isAllowMotorcycles());
		}

		final String routesReservableData = StreamUtil.safeStream(bcfTravelRouteFacade.getAllTravelRoutes())
				.filter(routeData -> null != routeData.getCode())
				.map(this::getTravelRouteReservableValue)
				.collect(Collectors.joining(BcfstorefrontaddonWebConstants.COMMA));
		model.addAttribute("routesReservableData", routesReservableData);
	}

	private String getTravelRouteReservableValue(final TravelRouteData travelRouteData)
	{
		return travelRouteData.getCode().concat(BcfstorefrontaddonWebConstants.EQUALS)
				.concat(String.valueOf(travelRouteData.isReservable()));
	}

	/**
	 * Request handler for any search requests from the Fare Finder Component. This method will first perform JSR303
	 * validation on the fareFinderForm bean before performing any custom validation.
	 *
	 * @param bcfFareFinderForm
	 * @param bindingResult
	 * @param model
	 * @return the location of the JSON tag used to render the errors in the front-end
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/validate-fare-finder-form", method = RequestMethod.POST)
	public String validateFareFinderForm(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final BindingResult bindingResult, final Model model)
	{
		if (TripType.SINGLE.name().equals(bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			bcfFareFinderForm.getRouteInfoForm().setReturnDateTime("");
		}
		bcfFareFinderForm.setSkipMaxAllowedValidation(true);
		validateCompleteForm(bcfFareFinderForm, bindingResult);
		bcfFareFinderForm.setSkipMaxAllowedValidation(false);
		final boolean hasErrorFlag = bindingResult.hasErrors();
		model.addAttribute(BcfvoyageaddonWebConstants.HAS_ERROR_FLAG, hasErrorFlag);
		if (hasErrorFlag)
		{
			model.addAttribute(BcfvoyageaddonWebConstants.FIELD_ERRORS, bindingResult.getFieldErrors());
		}
		return BcfvoyageaddonControllerConstants.Views.Pages.FormErrors.formErrorsResponse;
	}

	@RequestMapping(value = "/submit-fare-finder-form", method = RequestMethod.POST)
	public String submitEditJourneyForm(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm, final Model model)
	{
		clonePassengersList(bcfFareFinderForm);
		setVehicleData(model, bcfFareFinderForm);
		cloneVehicleAndCleanForm(bcfFareFinderForm);
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		switch (bcfFareFinderForm.getCurrentPage())
		{
			case BcfvoyageaddonWebConstants.PASSENGER_INFO:
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.PASSENGER_PAGE;
			case BcfvoyageaddonWebConstants.VEHICLE_INFO:
				if (bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
				{
					return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
				}
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.VEHICLE_PAGE;
			default:
				if (StringUtils.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getTripType(), TripType.SINGLE.name()))
				{
					try
					{
						removeInboundLegHelper.removeInboundLeg();
					}
					catch (final IntegrationException e)
					{
						LOG.error("Integration exception encountered while calling Cancel Booking API", e);
						GlobalMessages.addErrorMessage(model, BASKET_ERROR_OCCURRED);
					}
				}
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
		}
	}

	private void validateCompleteForm(final BCFFareFinderForm bcfFareFinderForm, final BindingResult bindingResult)
	{
		validateForm(routeInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.FARE_CALCULATOR_FORM);
		if (!bindingResult.hasErrors() && Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm()) && !StringUtils
				.equals(BcfvoyageaddonWebConstants.PASSENGER_INFO, bcfFareFinderForm.getCurrentPage()))
		{
			validateForm(passengerInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.FARE_CALCULATOR_FORM);
		}
		if (!bindingResult.hasErrors() && Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm())
				&& !bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn()
				&& !StringUtils.equals(BcfvoyageaddonWebConstants.PASSENGER_INFO, bcfFareFinderForm.getCurrentPage())
				&& !StringUtils.equals(BcfvoyageaddonWebConstants.VEHICLE_INFO, bcfFareFinderForm.getCurrentPage()))
		{
			cloneVehicleAndCleanForm(bcfFareFinderForm);
			validateForm(vehicleInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		}
	}

	@RequestMapping(value = "/retrieve-accessibility-needs", method = RequestMethod.POST)
	public String fetchAccessibilityPerPAX(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			@RequestParam("boundFor") final String boundFor, final Model model)
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList;
		final List<AccessibilityRequestData> previousAccessibilityRequestDataList;
		if (boundFor.equals(OUTBOUND))
		{
			passengerTypeQuantityList = bcfvoyageaddonComponentControllerUtil.getFilteredPassengerTypeQuantityDataList(
					bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList());

			previousAccessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil.getFilteredAccessibilityRequestDataList(
					bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList());
		}
		else
		{
			passengerTypeQuantityList = bcfvoyageaddonComponentControllerUtil.getFilteredPassengerTypeQuantityDataList(
					bcfFareFinderForm.getPassengerInfoForm().getInboundPassengerTypeQuantityList());

			previousAccessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil.getFilteredAccessibilityRequestDataList(
					bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList());
		}
		final List<AccessibilityRequestData> newAccessibilityRequestDataList = new ArrayList<>();
		passengerTypeQuantityList.stream()
				.forEach(passengerTypeQuantityData -> IntStream.range(0, passengerTypeQuantityData.getQuantity()).forEach(value -> {
					final AccessibilityRequestData accessibilityRequestData = new AccessibilityRequestData();
					accessibilityRequestData.setPassengerType(passengerTypeQuantityData.getPassengerType());
					accessibilityRequestData.setSpecialServiceRequestDataList(
							bcfvoyageaddonComponentControllerUtil
									.getSpecialServiceDisplayList(bcfSpecialAssistanceFacade.getAllSpecialServiceRequest(),
											bcfvoyageaddonComponentControllerUtil.getSpecialServiceRequestProductsSequence()));
					accessibilityRequestData.setAncillaryRequestDataList(
							bcfvoyageaddonComponentControllerUtil
									.getProductListByDisplaySequence(bcfvoyageaddonComponentControllerUtil.getAccessibilityProductData(),
											bcfvoyageaddonComponentControllerUtil.getAccessibilityRequestProductsSequence()));
					newAccessibilityRequestDataList.add(accessibilityRequestData);
				}));
		final List<AccessibilityRequestData> accessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil
				.getAccessibilityRequestDataList(previousAccessibilityRequestDataList, newAccessibilityRequestDataList);
		if (boundFor.equals(OUTBOUND))
		{
			bcfFareFinderForm.getPassengerInfoForm().setAccessibilityRequestDataList(accessibilityRequestDataList);
			model.addAttribute(BcfvoyageaddonWebConstants.BOUNDFOR, "");
			model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
			return BcfvoyageaddonControllerConstants.Views.Pages.Accessibility.accessibilityNeedsPerPAX;
		}
		bcfFareFinderForm.getPassengerInfoForm().setInboundaccessibilityRequestDataList(accessibilityRequestDataList);
		model.addAttribute(BcfvoyageaddonWebConstants.BOUNDFOR, boundFor);
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		return BcfvoyageaddonControllerConstants.Views.Pages.Accessibility.inboundAccessibilityNeedsPerPAX;
	}


	@RequestMapping(value = "/remove-accessibility-needs", method = RequestMethod.POST)
	public void removeAccessibilityPerPAX(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			@RequestParam("boundFor") final String boundFor, final Model model)
	{
		if (boundFor.equals(OUTBOUND))
		{
			bcfFareFinderForm.getPassengerInfoForm().setAccessibilityRequestDataList(Collections.emptyList());
		}
		if (boundFor.equals("inbound"))
		{
			bcfFareFinderForm.getPassengerInfoForm().setInboundaccessibilityRequestDataList(Collections.emptyList());
		}
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
	}

	@ResponseBody
	@RequestMapping(value =
			{ "/travel-routes" }, method =
			{ RequestMethod.POST, RequestMethod.GET }, produces =
			{ "application/json" })
	public BcfRouteReponseData getTravelRoutes()
	{
		final BcfRouteReponseData bcfRouteData = new BcfRouteReponseData();
		bcfRouteData.setTravelRoute(bcfTravelRouteFacade.getAllTravelRoutes());
		bcfRouteData.setTerminalsCacheVersion(cmsSiteService.getCurrentSite().getTerminalsCacheVersion());
		return bcfRouteData;
	}

	@RequestMapping(value = NEXT_STEP, method = RequestMethod.POST)
	public String handlePassengerDetails(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final RedirectAttributes redirectModel, final BindingResult bindingResult, final Model model)
	{
		validateForm(passengerInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.BCF_FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.PASSENGER_PAGE;
		}

		if (bcfTravelCartFacadeHelper.isAlacateFlow())
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_ALACARTE);
		}
		else
		{
			getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
					BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);
		}

		clonePassengersList(bcfFareFinderForm);
		model.addAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);

		if (bcfTravelCartFacade.isAmendmentCart())
		{

			final String routeType = bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LONG_ROUTE_TYPE);
			final List<String> routes = Stream.of(routeType.split(",")).collect(Collectors.toList());

			final Set<String> passengerTypes = new HashSet<>();

			final String eBookingRef = sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
			final Integer journeyRefNum = sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER);

			final Map<String, Integer> passengersMap = bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList()
					.stream()
					.filter(passengerTypeQuantityData -> passengerTypeQuantityData.getQuantity() > 0)
					.collect(Collectors
							.groupingBy(paxTypeAndQty -> paxTypeAndQty.getPassengerType().getCode(), Collectors.summingInt(
									PassengerTypeQuantityData::getQuantity)));


			Map<String, Long> travellerCartMap = new HashMap<>();
			if (travelCheckoutFacade.containsLongRoute())
			{
				travellerCartMap = travellerFacade
						.getPassengersTypeAndQuantityFromCart(journeyRefNum, eBookingRef);
			}
			else
			{

				travellerCartMap = travellerFacade
						.getAccessibilityPassengersTypeAndQuantityFromCart(journeyRefNum, eBookingRef);
			}



			for (final Map.Entry<String, Long> travellerCartMapEntry : travellerCartMap.entrySet())
			{

				if (passengersMap.containsKey(travellerCartMapEntry.getKey()))
				{

					if (passengersMap.get(travellerCartMapEntry.getKey()).intValue() < travellerCartMapEntry.getValue().intValue())
					{
						passengerTypes.add(travellerCartMapEntry.getKey());

					}

				}
				else
				{

					passengerTypes.add(travellerCartMapEntry.getKey());
				}

			}

			if (CollectionUtils.isNotEmpty(passengerTypes))
			{
				return REDIRECT_PREFIX + "/update-travellers";

			}

		}


		if (bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
		{
			setVehicleData(model, bcfFareFinderForm);


			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
		}
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.VEHICLE_PAGE;
	}

	private void clonePassengersList(final BCFFareFinderForm bcfFareFinderForm)
	{
		final Cloner clone = new Cloner();
		if (bcfFareFinderForm.getRouteInfoForm().getTripType().equals(TripType.RETURN.getCode()) && !bcfFareFinderForm
				.getPassengerInfoForm().getReturnWithDifferentPassenger())
		{
			bcfFareFinderForm.getPassengerInfoForm()
					.setInboundPassengerTypeQuantityList(
							clone.deepClone(bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList()));
		}
	}

	@RequestMapping(value = FARE_CALCULATOR_SEARCH, method = RequestMethod.POST)
	public String performFareCalculatorSearch(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final RedirectAttributes redirectModel, final Model model, final BindingResult bindingResult)
	{
		final boolean travellingAsWalkOn = bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn();
		if (bcfFareFinderForm.getPassengerInfoForm() != null && travellingAsWalkOn)
		{
			bcfFareFinderForm.setVehicleInfoForm(null);
		}
		else
		{
			cloneVehicleAndCleanForm(bcfFareFinderForm);
		}
		setFareCalculatorData(model, bcfFareFinderForm, null, null);
		if (bcfFareFinderForm.getPassengerInfoForm() != null)
		{
			bcfFareFinderForm.getPassengerInfoForm().setTravellingAsWalkOn(travellingAsWalkOn);
		}
		validateCompleteForm(bcfFareFinderForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			redirectModel
					.addFlashAttribute(BcfvoyageaddonWebConstants.BCF_FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
			return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_CALCULATOR_PAGE;
		}
		getSessionService().setAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_CALCULATOR_FARE_SELECTION_ROOT_URL;
	}

	/**
	 * The method is called when the form doesn't have any binding errors. It does a redirect to the FareSelection page
	 *
	 * @param bcfFareFinderForm as the input FareFinderForm
	 * @param redirectModel
	 * @param bindingResult
	 * @return a string representing the redirect to the FareSelection page
	 */
	@RequestMapping(value = SEARCH, method = RequestMethod.POST)
	public String performSearch(
			@Valid @ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) final BCFFareFinderForm bcfFareFinderForm,
			final RedirectAttributes redirectModel, final Model model, final BindingResult bindingResult)
	{
		if (!bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
		{
			removeWalkOnOptions(bcfFareFinderForm);
			cloneVehicleAndCleanForm(bcfFareFinderForm);
			validateForm(vehicleInfoValidator, bcfFareFinderForm, bindingResult, BcfFacadesConstants.BCF_FARE_FINDER_FORM);
			if (bindingResult.hasErrors())
			{
				redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "true");
				redirectModel.addFlashAttribute(BcfvoyageaddonWebConstants.BCF_FARE_FINDER_FORM_BINDING_RESULT, bindingResult);
				return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.VEHICLE_PAGE;
			}
		}
		model.addAttribute(BcfvoyageaddonWebConstants.HIDDEN_MODAL, "false");
		getSessionService().setAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		redirectModel.addFlashAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM, bcfFareFinderForm);
		return REDIRECT_PREFIX + BcfvoyageaddonWebConstants.FARE_SELECTION_ROOT_URL;
	}

	private void cloneVehicleAndCleanForm(final BCFFareFinderForm bcfFareFinderForm)
	{
		bcfvoyageaddonComponentControllerUtil.setVehicleCodeCategoryInfo(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo());
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && bcfFareFinderForm.getVehicleInfoForm()
				.isTravellingWithVehicle() && TripType.RETURN.name().equals(bcfFareFinderForm.getRouteInfoForm().getTripType())
				&& !bcfFareFinderForm.getVehicleInfoForm().isReturnWithDifferentVehicle())
		{
			final List<VehicleTypeQuantityData> vehicleInfo = bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo();
			final Cloner clone = new Cloner();
			final VehicleTypeQuantityData returnVehicle = clone.deepClone(vehicleInfo.get(0));
			returnVehicle.setTravelRouteCode(vehicleInfo.get(1).getTravelRouteCode());
			vehicleInfo.set(1, returnVehicle);
		}
		bcfFerrySelectionUtil.cleanVehicleInfoForm(bcfFareFinderForm);
		bcfFerrySelectionUtil.updateThresholdForOver9FeetWideVehicle(bcfFareFinderForm);
	}

	private void removeWalkOnOptions(
			@ModelAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM) @Valid final BCFFareFinderForm bcfFareFinderForm)
	{
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && bcfFareFinderForm.getVehicleInfoForm()
				.isTravellingWithVehicle())
		{
			bcfFareFinderForm.getPassengerInfoForm().setCarryingDangerousGoodsInOutbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setCarryingDangerousGoodsInReturn(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsCheckOutbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsCheckInbound(false);
			StreamUtil.safeStream(bcfFareFinderForm.getPassengerInfoForm().getLargeItems())
					.forEach(largeItemData -> largeItemData.setQuantity(0));
			StreamUtil.safeStream(bcfFareFinderForm.getPassengerInfoForm().getLargeItemsInbound())
					.forEach(largeItemData -> largeItemData.setQuantity(0));
			if (Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm().getExtraBaggageOutbound()))
			{
				bcfFareFinderForm.getPassengerInfoForm().getExtraBaggageOutbound().setQuantity(0);
			}
			if (Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm().getExtraBaggageInbound()))
			{
				bcfFareFinderForm.getPassengerInfoForm().getExtraBaggageInbound().setQuantity(0);
			}
		}
	}

	private void validateForm(final AbstractTravelValidator validator, final BCFFareFinderForm bcfFareFinderForm,
			final BindingResult bindingResult, final String formName)
	{
		validator.setTargetForm(formName);
		validator.setAttributePrefix("");
		((BeanPropertyBindingResult) bindingResult).setMessageCodesResolver(customMessageCodesResolver);
		//Skip validation for Max Permissible sailings only for FareCalculator
		bcfFareFinderForm.setSkipMaxAllowedValidation(false);
		if (BcfFacadesConstants.FARE_CALCULATOR_FORM.equals(formName))
		{
			bcfFareFinderForm.setSkipMaxAllowedValidation(true);
		}
		validator.validate(bcfFareFinderForm, bindingResult);
	}

}
