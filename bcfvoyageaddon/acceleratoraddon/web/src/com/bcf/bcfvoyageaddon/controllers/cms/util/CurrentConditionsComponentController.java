/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.controllers.cms.util;


import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfcommonsaddon.model.components.CurrentConditionsComponentModel;
import com.bcf.bcfstorefrontaddon.controllers.cms.AbstractFinderComponentController;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.BcfvoyageaddonControllerConstants;
import com.bcf.bcfvoyageaddon.forms.cms.CurrentConditionsForm;


/**
 * Current Conditions Controller for handling requests for the Current Conditions Component.
 */
@Controller("CurrentConditionsComponentController")
@RequestMapping(value = BcfvoyageaddonControllerConstants.Actions.Cms.CurrentConditionsComponent)
public class CurrentConditionsComponentController extends AbstractFinderComponentController
{
    /**
     * Method responsible for populating the data model with initial data for the component
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final AbstractFinderComponentModel component)
    {
        final CurrentConditionsComponentModel currentConditionsComponentModel = (CurrentConditionsComponentModel) component;
        final CurrentConditionsForm currentConditionsForm = new CurrentConditionsForm();
        model.addAttribute(BcfvoyageaddonWebConstants.CURRENT_CONDITIONS_FORM, currentConditionsForm);
        model.addAttribute("title", currentConditionsComponentModel.getTitle());
        model.addAttribute("description", currentConditionsComponentModel.getDescription());
    }
}
