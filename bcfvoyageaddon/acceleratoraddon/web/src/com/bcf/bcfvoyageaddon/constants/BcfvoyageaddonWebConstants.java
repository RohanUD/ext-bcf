/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.constants;

/**
 * Global class for all Bcfvoyageaddon web constants. You can add global constants for your extension into this class.
 */
public final class BcfvoyageaddonWebConstants // NOSONAR
{
	public static final String TRANSPORT_OFFERING_STATUS_FORM = "transportOfferingStatusForm";
	public static final String TRANSPORT_OFFERING_STATUS_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.transportOfferingStatusForm";
	public static final String TRANSPORT_OFFERING_DATA_LIST = "transportOfferingDataList";
	public static final String HAS_ERROR_FLAG = "hasErrorFlag";
	public static final String WAITLIST_LINK = "waitListLink";
	public static final String TRAVEL_AGENT_WAITLIST_LINK = "travelAgentWaitListLink";
	public static final String SHOW_WAITLIST_FORM = "showWaitlistLink";
	public static final String ENABLE_REVENUE_MANAGEMENT = "enableRevenueManagement";
	public static final String ROUTE_TYPE = "routeType";
	public static final String ERROR_MESSAGE = "errorMsg";
	public static final String FIELD_ERRORS = "fieldErrors";
	public static final String FARE_FINDER_FORM = "fareFinderForm";
	public static final String CURRENT_CONDITIONS_FORM = "currentConditionsForm";
	public static final String ADD_BUNDLE_TO_CART_FORM = "addBundleToCartForm";
	public static final String FARE_CALCULATOR_FORM = "fareCalculatorForm";
	public static final String PRICED_ITINERARY_DATE_FORMAT = "HH:mm dd MMM";
	public static final String FARE_SELECTION_TAB_DATE_FORMAT = "dd MMMM";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String TIME_FORMAT = "HH:mm";
	public static final String DATE_FORMAT_MONTH_DAY = "MMM dd";
	public static final String TIME_FORMAT_12HR = "hh:mm a";
	public static final String PASSENGER_TYPE_ADULT = "adult";
	public static final String PASSENGER_TYPE_ESCORTADULT = "escortAdult";
	public static final String PASSENGER_TYPE_DISABLEDADULT = "disabledAdult";
	public static final String PASSENGER_TYPE_SENIOR = "senior";
	public static final String PASSENGER_TYPE_NRADULT = "nrAdult";
	public static final String PASSENGER_TYPE_NRSTUDENTADULT = "nrStudentAdult";
	public static final String PASSENGER_TYPE_NRSENIOR = "nrSenior";
	public static final String PASSENGER_TYPE_NRESCORTADULT = "nrEscortAdult";
	public static final String PASSENGER_TYPE_NRDISABLEDADULT = "nrDisabledAdult";
	public static final String FARE_SELECTION = "fareSelection";
	public static final String FIRST_ITINERARY = "firstItinerary";

	public static final String TRIP_TYPE = "tripType";
	public static final String OUTBOUND_DATES = "outboundDates";
	public static final String SCHEDULE_DATES = "scheduleDates";
	public static final String TAB_DATE_FORMAT = "fareSelectionTabDateFormat";
	public static final String PI_DATE_FORMAT = "pricedItineraryDateFormat";
	public static final String PI_VIEW_JOURNEY_DATE_FORMAT = "pricedItineraryViewJourneyDateFormat";
	public static final String SESSION_FARE_SELECTION_DATA = "sessionFareSelection";
	public static final String PTC_FARE_BREAKDOWN_SUMMARY = "ptcFareBreakdownSummary";
	public static final String SUMMARY_TAXES = "taxesSummary";
	public static final String SUMMARY_DISCOUNTS = "discountsSummary";
	public static final String PRICE_DISPLAY_PASSENGER_TYPE = "fare.selection.itinerary.price.display.passengertype";
	public static final String MODEL_PRICE_DISPLAY_PASSENGER_TYPE = "priceDisplayPassengerType";
	public static final String FARE_FINDER_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.fareFinderForm";
	public static final String BCF_FARE_FINDER_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.bcfFareFinderForm";
	public static final String DEAL_FINDER_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.dealFinderForm";
	public static final String OUTBOUND_REF_NUMBER = "outboundRefNumber";
	public static final String INBOUND_REF_NUMBER = "inboundRefNumber";
	public static final String REF_NUMBER = "refNumber";
	public static final String SELECTED_SORTING = "selectedSorting";
	public static final String NO_OF_OUTBOUND_OPTIONS = "noOfOutboundOptions";
	public static final String NO_OF_INBOUND_OPTIONS = "noOfInboundOptions";
	public static final String ORIGIN = "origin";
	public static final String DESTINATION = "destination";
	public static final String FARE_SELECTION_REDIRECT_URL = "fareSelectionRedirectUrl";
	public static final String DISPLAY_ORDER = "displayOrder";

	public static final String RESERVATION_DATA_LIST = "reservationDataList";
	public static final String TRAVELLER_DATA_CODE_MAP = "travellerDataCodeMap";
	public static final String EXTRAS_SUMMARY = "extrasSummary";
	public static final String NEXT_URL = "nextURL";
	public static final String AMENITIES_URL = "amenitiesURL";
	public static final String NORTHERN_ROUTE = "northernRoute";
	public static final String AMEND = "amend";
	public static final String ADD_ACTIVITIES = "addActivities";
	public static final String CONFIRM_AND_REVIEW = "confirmAndReview";
	public static final String ADD_ANOTHER_SAILING = "addAnotherSailing";
	public static final String LOGIN = "login";
	public static final String CREATE_AN_ACCOUNT = "createAnAccount";
	public static final String CONTINUE_AS_GUEST = "continueAsGuest";

	public static final String HIDE_BUTTON = "hideButton";

	public static final String SESSION_BOOKING_REFERENCE = "sessionBookingReference";
	public static final String SESSION_ORIGIN_DESTINATION_REF_NUMBER = "sessionOriginDestinationRefNumber";
	public static final String SESSION_TRAVELLERS_TO_CHECK_IN = "sessionTravellersToCheckIn";

	public static final String BOOKING_REFERENCE = "bookingReference";
	public static final String ORIGIN_DESTINATION_REF_NUMBER = "originDestinationRefNumber";
	public static final String TRAVELLERS = "travellers";
	public static final String NATIONALITIES = "nationalities";
	public static final String COUNTRIES = "countries";


	// URL CONSTANTS

	public static final String MAX_PASSENGER_QUANTITY = "maxPassengerQuantity";
	public static final String MAX_CHILD_QUANTITY = "maxChildQuantity";
	public static final String ACCOMMODATION_QUANTITY = "accommodationsQuantity";
	public static final String TRAVELLER_DETAILS_PATH = "/traveller-details";
	public static final String FARE_SELECTION_ROOT_URL = "/fare-selection";
	public static final String FARE_CALCULATOR_FARE_SELECTION_ROOT_URL = "/fare-selection/fare-calculator";
	public static final String ANCILLARY_ROOT_URL = "/ancillary";
	public static final String ANCILLARY_AMENDMENT_ROOT_URL = "/manage-booking/ancillary/amendment";
	public static final String CHECK_IN_URL = "/manage-booking/check-in";
	public static final String CHECK_IN_SUCCESS_URL = "/manage-booking/check-in/success";
	public static final String CHECK_IN_FAILED_URL = "/manage-booking/check-in/failed";
	public static final String ADD_BUNDLE_URL = "/cart/addBundle";
	public static final String UPDATE_BUNDLE_URL = "/cart/updateBundle";
	public static final String UPGRADE_BUNDLE_URL = "/cart/upgradeBundle";
	public static final String ADD_ACTIVITIES_URL = "/deal-activity-listing";
	public static final String CONFIRM_AND_REVIEW_URL = "/cart/summary";

	public static final String ORIGINAL_ORDER_CODE = "originalOrderCode";

	public static final String BOOKING_DETAILS_USER_VALIDATION_ERROR = "booking.details.user.validation.error";
	public static final String DISABLE_CURRENCY_SELECTOR = "disableCurrencySelector";

	// Upgrade Bundle Section Constants
	public static final String UPGRADE_BUNDLE_PRICED_ITINERARIES = "pricedItineraries";
	public static final String IS_UPGRADE_OPTION_AVAILABLE = "isUpgradeOptionAvailable";

	public static final String DATE_PATTERN = "datePattern";

	public static final String MY_BOOKINGS_PAGE_SIZE = "myaccount.mybookings.page.size";
	public static final String PAGE_SIZE = "pageSize";

	public static final String ERROR_MESSAGES = "errorMessages";
	public static final String NEXT_AVAILABILITY_DATES = "nextAvailabilityDates";

	public static final String ARRIVAL_LABEL = "arrivalLabel";
	public static final String DEPARTURE_LABEL = "departureLabel";
	public static final String VEHICLE_NAME_LABEL = "vehicleNameLabel";
	public static final String FARE_TYPE_LABEL = "fareTypeLabel";
	public static final String CURRENCY_FARE_LABEL = "currencyFareLabel";
	public static final String TAXES_LABEEL = "taxesLabel";
	public static final String DISCOUNT_LABEL = "discountsLabel";

	public static final String JOURNEY_REF_NUMBER = "journeyRefNum";

	public static final String DATE_FORMAT_LABEL = "dateFormat";
	public static final String TIME_FORMAT_LABEL = "timeFormat";
	public static final String VEHICLE_TO_SKIP = "vehicleToSkip";
	public static final String SKIP_VEHICLE_DIMENSIONS_PROPERTY = "vehicleTypesToSkipDimensionValidation";
	public static final String ACCESSIBILITY_CATEGORY_CODES = "accessibilityCategoryCodes";
	public static final String SPECIAL_SERVICE_REQUEST_PRODUCTS = "specialServiceRequestProducts";

	public static final String ARRIVAL_LOCATION = "arrivalLocation";
	public static final String DESTINATION_LOCATION = "destination";
	public static final String DATE = "date";
	public static final String ACTIVITY_TYPE = "activityType";
	public static final String ACTIVITY = "activity";
	public static final String DEPARTURE_LOCATION = "departureLocation";
	public static final String DEPARTURE_LOCATION_NAME = "departureLocationName";
	public static final String ARRIVAL_LOCATION_NAME = "arrivalLocationName";
	public static final String QUESTION = "?";
	public static final String DEAL_FINDER_FORM = "dealFinderForm";
	public static final String GET_A_QUOTE_FORM = "getAQuoteForm";
	public static final String ACTIVITY_FINDER_FORM = "activityFinderForm";
	public static final String SCHEDULE_FINDER_FORM = "scheduleFinderForm";
	public static final String ERROR_TYPE_EMPTY_DURATION = "EmptyDuration";
	public static final String FIELD_ARRIVAL_LOCATION = "arrivalLocation";
	public static final String FIELD_DEPARTURE_LOCATION = "departureLocation";
	public static final String ERROR_TYPE_INVALID_FIELD_FORMAT = "InvalidFormat";
	public static final String FIELD_LOCATION = "location";
	public static final String DEAPRTURE_DATE = "departureDate";
	public static final String DURATION = "duration";
	public static final String STAR_RATING = "starRating";
	public static final String DEAL_TYPE = "dealType";
	public static final String BOUNDFOR = "boundFor";

	public static final String ROUTE_PAGE = "/RouteSelectionPage";
	public static final String PASSENGER_PAGE = "/PassengerSelectionPage";
	public static final String FARE_CALCULATOR_PAGE = "/routes-fares/fare-calculator";
	public static final String VEHICLE_PAGE = "/VehicleSelectionPage";
	public static final String PASSENGER_INFO = "PassengerInfo";
	public static final String VEHICLE_INFO = "VehicleInfo";
	public static final String ROUTE_INFO = "RouteInfo";
	public static final String DEFAULT = "default";
	public static final String HIDDEN_MODAL = "hiddenModalVar";
	public static final String FARE_SELECTION_PAGE = "fare-selection";

	public static final String COMPONENT_UID = "CurrentConditionsLandingComponent";
	public static final String CURRENT_CONDITIONS_LANDING_CMS_PAGE = "currentconditionslandingpage";
	public static final String CURRENT_CONDITIONS_DEPARTURES_CMS_PAGE = "ccDeparturesPage";
	public static final String CURRENT_CONDITIONS_DETAILS_CMS_PAGE = "currentconditionsdetailspage";
	public static final String CURRENT_CONDITIONS_MAJOR_TERMINALS_CMS_PAGE = "ccMajorTerminalsPage";
	public static final String CURRENT_CONDITIONS_FERRY_TRACKING_CMS_PAGE = "ccFerryTrackingPage";
	public static final String CURRENT_CONDITIONS_WEBCAMS_CMS_PAGE = "ccWebcamsPage";
	public static final String CURRENT_CONDITIONS_SERVICE_NOTICES_CMS_PAGE = "ccServiceNoticesPage";

	public static final String CURRENT_CONDITIONS_SERVICE_NOT_AVAILABLE = "ccServiceNotAvailable";
	public static final String CURRENT_CONDITIONS_DATA = "currentConditionsData";

	public static final String NEWS_RELEASE_CMS_PAGE = "newsReleasePage";

	public static final String DATE_NOW = "dateNow";
	public static final String RETURN = "RETURN";
	public static final String LONG_ROUTE_MAX_CALENDER_LIMIT = "long.route.max.calendar.limit";
	public static final String SHORT_ROUTE_MAX_CALENDER_LIMIT = "short.route.max.calendar.limit";

	private BcfvoyageaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
