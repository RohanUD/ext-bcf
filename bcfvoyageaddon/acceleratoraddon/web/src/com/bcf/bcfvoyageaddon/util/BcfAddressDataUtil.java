/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.util;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import com.bcf.bcfvoyageaddon.forms.BCFAddressForm;
import com.bcf.facades.constants.BcfFacadesConstants;


@Component("bcfAddressDataUtil")
public class BcfAddressDataUtil
{
	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	public void convertBasic(final AddressData source, final BCFAddressForm target)
	{
		target.setAddressId(source.getId());
		target.setTitleCode(source.getTitleCode());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());
		target.setTownCity(source.getTown());
		target.setPostcode(source.getPostalCode());
		target.setCountryIso(source.getCountry().getIsocode());
		target.setPhoneType(source.getPhoneType());
		target.setPhone(source.getPhone());
	}

	public void convert(final AddressData source, final BCFAddressForm target)
	{
		convertBasic(source, target);
		target.setSaveInAddressBook(Boolean.valueOf(source.isVisibleInAddressBook()));
		target.setShippingAddress(Boolean.valueOf(source.isShippingAddress()));
		target.setBillingAddress(Boolean.valueOf(source.isBillingAddress()));
		target.setPhone(source.getPhone());
		target.setPhoneType(source.getPhoneType());

		if (source.getRegion() != null && !StringUtils.isEmpty(source.getRegion().getIsocode()))
		{
			target.setRegionIso(source.getRegion().getIsocode());
		}
	}

	public AddressData convertToVisibleAddressData(final BCFAddressForm addressForm)
	{
		final AddressData addressData = convertToAddressData(addressForm);
		addressData.setVisibleInAddressBook(true);
		return addressData;
	}

	public AddressData convertToAddressData(final BCFAddressForm addressForm)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressForm.getAddressId());
		addressData.setTitleCode(addressForm.getTitleCode());
		addressData.setFirstName(addressForm.getFirstName());
		addressData.setLastName(addressForm.getLastName());
		addressData.setLine1(addressForm.getLine1());
		addressData.setLine2(addressForm.getLine2());
		addressData.setTown(addressForm.getTownCity());
		addressData.setPostalCode(addressForm.getPostcode());
		addressData.setPhone(addressForm.getPhone());
		addressData.setPhoneType(addressForm.getPhoneType());
		if ((addressForm.getAddressType()).equals(BcfFacadesConstants.SHIPPING_ADDRESS_TYPE))
		{
			addressData.setBillingAddress(false);
			addressData.setShippingAddress(true);
		}

		if ((addressForm.getAddressType()).equals(BcfFacadesConstants.BILLING_ADDRESS_TYPE))
		{
			addressData.setBillingAddress(true);
			addressData.setShippingAddress(false);
		}

		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = i18NFacade.getCountryForIsocode(addressForm.getCountryIso());
			addressData.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = i18NFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			addressData.setRegion(regionData);
		}

		return addressData;
	}

}
