/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.util;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.travel.FareSearchRequestData;
import de.hybris.platform.commercefacades.travel.FareSelectionData;
import de.hybris.platform.commercefacades.travel.OriginDestinationInfoData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;
import de.hybris.platform.commercefacades.travel.SearchProcessingInfoData;
import de.hybris.platform.commercefacades.travel.SpecialServiceRequestData;
import de.hybris.platform.commercefacades.travel.TransportOfferingPreferencesData;
import de.hybris.platform.commercefacades.travel.TravelPreferencesData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.enums.FareSelectionDisplayOrder;
import de.hybris.platform.commercefacades.travel.enums.TransportOfferingType;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.AncillaryProductModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.model.user.PassengerInformationModel;
import de.hybris.platform.travelservices.model.user.SpecialServiceRequestModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.services.TransportFacilityService;
import de.hybris.platform.travelservices.services.TravelRouteService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.fest.util.Collections;
import org.springframework.stereotype.Component;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.controllers.cms.util.BCFvoyageaddonComponentControllerUtil;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.PassengerInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm;
import com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;
import com.bcf.core.enums.FareProductType;
import com.bcf.core.enums.RouteType;
import com.bcf.core.model.traveller.BCFVehicleInformationModel;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.user.BcfUserService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.AncillaryProductFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.VehicleTypeData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.specialassistance.BCFSpecialAssistanceFacade;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


@Component("bcfFerrySelectionUtil")
public class BcfFerrySelectionUtil
{
	private static final String BASE_PASSENGERS = "bcfBasePassengers";
	private static final String ADDITIONAL_PASSENGERS = "bcfAdditionalPassengers";
	private static final String NORTHERN_PASSENGERS = "bcfNorthernPassengers";

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfvoyageaddonComponentControllerUtil")
	private BCFvoyageaddonComponentControllerUtil bcfvoyageaddonComponentControllerUtil;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "passengerTypeFacade")
	private PassengerTypeFacade passengerTypeFacade;

	@Resource(name = "ancillaryProductConverter")
	private Converter<ProductModel, ProductData> ancillaryProductConverter;

	@Resource(name = "bcfSpecialAssistanceFacade")
	private BCFSpecialAssistanceFacade bcfSpecialAssistanceFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "ancillaryProductFacade")
	private AncillaryProductFacade ancillaryProductFacade;

	@Resource(name = "travelRouteService")
	private TravelRouteService travelRouteService;

	@Resource(name = "transportFacilityService")
	TransportFacilityService transportFacilityService;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@Resource(name = "userService")
	private BcfUserService userService;

	public void convert(final BCFFareFinderForm bcfFareFinderForm, final FareSearchRequestData fareSearchRequestData,
			final String displayOrder, final boolean isReturn, final Integer odRefNumber)
	{


		final TransportOfferingPreferencesData transportOfferingPreferencesData = new TransportOfferingPreferencesData();
		transportOfferingPreferencesData.setTransportOfferingType(TransportOfferingType.DIRECT);

		// create Travel Preferences

		final TravelPreferencesData travelPreferences = new TravelPreferencesData();
		travelPreferences.setCabinPreference("M");
		travelPreferences.setTransportOfferingPreferences(transportOfferingPreferencesData);

		//set calender view to true
		fareSearchRequestData.setShowCalenderView(true);

		// create OriginDestinationInfoData

		final List<OriginDestinationInfoData> originDestinationInfoData = new ArrayList<>();

		final OriginDestinationInfoData departureInfo = new OriginDestinationInfoData();
		departureInfo.setReferenceNumber(odRefNumber);

		if (isReturn)
		{
			departureInfo.setDepartureLocation(bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
			departureInfo.setDepartureLocationType(
					enumerationService.getEnumerationValue(LocationType.class,
							bcfFareFinderForm.getRouteInfoForm().getArrivalLocationSuggestionType()));
			departureInfo.setArrivalLocation(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation());
			departureInfo.setArrivalLocationType(
					enumerationService.getEnumerationValue(LocationType.class,
							bcfFareFinderForm.getRouteInfoForm().getDepartureLocationSuggestionType()));
			departureInfo.setDepartureTime(
					TravelDateUtils
							.convertStringDateToDate(bcfFareFinderForm.getRouteInfoForm().getReturnDateTime(),
									BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			departureInfo.setArrivalTime(TravelDateUtils
					.convertStringDateToDate(bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(),
							BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			if (!bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
			{
				bcfvoyageaddonComponentControllerUtil
						.setVehicleCodeAndCategory(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(1).getVehicleType());
				fareSearchRequestData
						.setVehicleInfoData(Collections.list(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(1)));
			}
			// populate passenger types
			populatePassengerInfo(bcfFareFinderForm, fareSearchRequestData);
		}
		else
		{
			departureInfo.setDepartureLocation(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation());
			departureInfo.setDepartureLocationType(
					enumerationService.getEnumerationValue(LocationType.class,
							bcfFareFinderForm.getRouteInfoForm().getDepartureLocationSuggestionType()));
			departureInfo.setArrivalLocation(bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
			departureInfo.setArrivalLocationType(
					enumerationService.getEnumerationValue(LocationType.class,
							bcfFareFinderForm.getRouteInfoForm().getArrivalLocationSuggestionType()));
			departureInfo.setDepartureTime(
					TravelDateUtils
							.convertStringDateToDate(bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(),
									BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			if (StringUtils.isNotBlank(bcfFareFinderForm.getRouteInfoForm().getReturnDateTime()))
			{
				departureInfo.setArrivalTime(TravelDateUtils
						.convertStringDateToDate(bcfFareFinderForm.getRouteInfoForm().getReturnDateTime(),
								BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
			}
			if (!bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn())
			{
				bcfvoyageaddonComponentControllerUtil
						.setVehicleCodeAndCategory(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(0).getVehicleType());
				fareSearchRequestData
						.setVehicleInfoData(Collections.list(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(0)));
			}
			// populate passenger types
			bcfvoyageaddonComponentControllerUtil.setPassengerDetails(fareSearchRequestData,
					bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList(),
					bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList());

			if (bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn() && bcfFareFinderForm.getPassengerInfoForm()
					.isLargeItemsCheckOutbound())
			{
				fareSearchRequestData.setLargeItems(getLargeItemList(bcfFareFinderForm.getPassengerInfoForm().getLargeItems()));
			}
		}

		originDestinationInfoData.add(departureInfo);

		// set fareSearchRequestData

		fareSearchRequestData.setTravellingAsWalkOn(bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn());
		fareSearchRequestData.setReturnWithDifferentPassenger(
				BooleanUtils.toBoolean(bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger()));
		fareSearchRequestData.setReturnWithDifferentVehicle(Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) ?
				bcfFareFinderForm.getVehicleInfoForm().isReturnWithDifferentVehicle() : false);
		fareSearchRequestData.setOriginDestinationInfo(originDestinationInfoData);
		fareSearchRequestData.setTripType(TripType.valueOf(bcfFareFinderForm.getRouteInfoForm().getTripType()));
		fareSearchRequestData.setRouteType(bcfFareFinderForm.getRouteInfoForm().getRouteType());
		fareSearchRequestData.setTravelPreferences(travelPreferences);
		fareSearchRequestData
				.setCarryingDangerousGoodsInOutbound(bcfFareFinderForm.getPassengerInfoForm().isCarryingDangerousGoodsInOutbound());
		fareSearchRequestData
				.setCarryingDangerousGoodsInReturn(bcfFareFinderForm.getPassengerInfoForm().isCarryingDangerousGoodsInReturn());

		// set searchProcessingInfoData and displayOption
		final SearchProcessingInfoData searchProcessingInfoData = new SearchProcessingInfoData();

		if (StringUtils.isNotBlank(displayOrder))
		{
			searchProcessingInfoData.setDisplayOrder(displayOrder);
		}
		else
		{
			searchProcessingInfoData.setDisplayOrder(FareSelectionDisplayOrder.DEPARTURE_TIME.toString());
		}
		fareSearchRequestData.setSearchProcessingInfo(searchProcessingInfoData);

		fareSearchRequestData
				.setSalesApplication(SalesApplication.valueOf(bcfSalesApplicationResolverFacade.getCurrentSalesChannel()));
	}

	private void populatePassengerInfo(final BCFFareFinderForm bcfFareFinderForm,
			final FareSearchRequestData fareSearchRequestData)
	{
		if (StringUtils.equals(TripType.RETURN.name(), bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			if (Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger()) && bcfFareFinderForm
					.getPassengerInfoForm().getReturnWithDifferentPassenger())
			{
				bcfvoyageaddonComponentControllerUtil.setPassengerDetails(fareSearchRequestData,
						bcfFareFinderForm.getPassengerInfoForm().getInboundPassengerTypeQuantityList(),
						bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList());
			}
			else
			{
				bcfvoyageaddonComponentControllerUtil.setPassengerDetails(fareSearchRequestData,
						bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList(),
						bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList());
			}
			if (bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn() && Objects
					.nonNull(bcfFareFinderForm.getPassengerInfoForm()
							.getReturnWithDifferentPassenger()) && bcfFareFinderForm.getPassengerInfoForm()
					.getReturnWithDifferentPassenger() && bcfFareFinderForm.getPassengerInfoForm()
					.isLargeItemsCheckInbound())
			{
				fareSearchRequestData
						.setLargeItems(getLargeItemList(bcfFareFinderForm.getPassengerInfoForm().getLargeItemsInbound()));
			}
			else if (Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger())
					&& !bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger()
					&& bcfFareFinderForm.getPassengerInfoForm().isLargeItemsCheckOutbound())
			{
				fareSearchRequestData
						.setLargeItems(getLargeItemList(bcfFareFinderForm.getPassengerInfoForm().getLargeItems()));
			}
		}
	}

	public void initializeFareFinderForm(final BCFFareFinderForm bcfFareFinderForm, final int journeyRefNum, final int odRefNum)
	{
		final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, odRefNum);
		final List<AbstractOrderEntryModel> otherLegEntries;
		final List<AbstractOrderEntryModel> allEntriesForJourney;
		bcfFareFinderForm.setThresholdHours(Double.parseDouble(bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.SAME_DAY_TRANSPORT_BOOKING_THRESHOLD_HOURS)));
		final RouteInfoForm routeInfoForm = new RouteInfoForm();
		final TravelRouteModel travelRoute = entries.get(0).getTravelOrderEntryInfo().getTravelRoute();
		final String originLocationName =
				travelRoute.getTravelSector().stream().findFirst().get().getOrigin().getLocation().getName() + " - " + travelRoute
						.getOrigin().getName() + "(" + travelRoute.getOrigin().getCode() + ")";
		final String originLocationCode = travelRoute.getOrigin().getCode();
		final String destinationLocationName =
				travelRoute.getTravelSector().stream().findFirst().get().getDestination().getLocation().getName() + " - "
						+ travelRoute.getDestination().getName() + "(" + travelRoute.getDestination().getCode() + ")";
		final String destinationLocationCode = travelRoute.getDestination().getCode();
		final PassengerInfoForm passengerInfoForm = new PassengerInfoForm();
		if (BcfCoreConstants.INBOUND_REFERENCE_NUMBER == odRefNum)
		{
			otherLegEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER);
			routeInfoForm.setTripType(TripType.RETURN.name());
			routeInfoForm.setDepartingDateTime(getDepartureTime(otherLegEntries));
			routeInfoForm.setReturnDateTime(getDepartureTime(entries));
			routeInfoForm.setDepartureLocationName(destinationLocationName);
			routeInfoForm.setDepartureLocation(destinationLocationCode);
			routeInfoForm.setArrivalLocationName(originLocationName);
			routeInfoForm.setArrivalLocation(originLocationCode);
			initializePassengerForm(passengerInfoForm, journeyRefNum, BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER, otherLegEntries);
			initializePassengerForm(passengerInfoForm, journeyRefNum, odRefNum, entries);
			passengerInfoForm.setReturnWithDifferentPassenger(!isReturningWithSamePassenger(passengerInfoForm));
			allEntriesForJourney = Stream.concat(otherLegEntries.stream(), entries.stream()).collect(Collectors.toList());
		}
		else
		{
			otherLegEntries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, BcfCoreConstants.INBOUND_REFERENCE_NUMBER);
			initializePassengerForm(passengerInfoForm, journeyRefNum, odRefNum, entries);
			if (CollectionUtils.isNotEmpty(otherLegEntries))
			{
				routeInfoForm.setTripType(TripType.RETURN.name());
				routeInfoForm.setReturnDateTime(getDepartureTime(otherLegEntries));
				initializePassengerForm(passengerInfoForm, journeyRefNum, BcfCoreConstants.INBOUND_REFERENCE_NUMBER, otherLegEntries);
				allEntriesForJourney = Stream.concat(entries.stream(), otherLegEntries.stream()).collect(Collectors.toList());
				passengerInfoForm.setReturnWithDifferentPassenger(!isReturningWithSamePassenger(passengerInfoForm));
			}
			else
			{
				routeInfoForm.setTripType(TripType.SINGLE.name());
				routeInfoForm.setReturnDateTime("");
				allEntriesForJourney = entries;
			}
			routeInfoForm.setDepartureLocationName(originLocationName);
			routeInfoForm.setDepartureLocation(originLocationCode);
			routeInfoForm.setArrivalLocationName(destinationLocationName);
			routeInfoForm.setArrivalLocation(destinationLocationCode);
			routeInfoForm.setDepartingDateTime(getDepartureTime(entries));
		}
		routeInfoForm.setArrivalLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());
		routeInfoForm.setDepartureLocationSuggestionType(LocationType.AIRPORTGROUP.getCode());
		routeInfoForm.setRouteType(entries.get(0).getTravelOrderEntryInfo().getTravelRoute().getRouteType().getCode());
		routeInfoForm.setWalkOnRoute(Objects.nonNull(travelRoute.getWalkOn()) ? travelRoute.getWalkOn() : false);
		routeInfoForm.setAllowsWalkOnOptions(Objects.nonNull(travelRoute.getAllowsWalkOnOptions()) ?
				travelRoute.getAllowsWalkOnOptions() : false);
		bcfFareFinderForm.setRouteInfoForm(routeInfoForm);
		bcfFareFinderForm.setPassengerInfoForm(passengerInfoForm);
		final List<AbstractOrderEntryModel> vehicleEntryModels = allEntriesForJourney.stream()
				.filter(entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
						&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
						FareProductType.VEHICLE)).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(vehicleEntryModels))
		{
			passengerInfoForm.setTravellingAsWalkOn(false);
			final VehicleInfoForm vehicleInfoForm = new VehicleInfoForm();
			initializeVehicleForm(vehicleInfoForm, allEntriesForJourney);
			setVehicleDimensionThreshold(bcfFareFinderForm);
			bcfFareFinderForm.setVehicleInfoForm(vehicleInfoForm);
			vehicleInfoForm.setTravellingWithVehicle(true);
			vehicleInfoForm.setReturnWithDifferentVehicle(!isReturningWithSameVehicle(vehicleInfoForm));
			updateThresholdForOver9FeetWideVehicle(bcfFareFinderForm);
			bcfFareFinderForm.setVehicleInfoForm(vehicleInfoForm);
		}
		else
		{
			passengerInfoForm.setTravellingAsWalkOn(true);
		}
	}
	public String getEbookingRef(final int journeyRefNum, final int odRefNum)
	{
		final List<AbstractOrderEntryModel> entries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, odRefNum);
		if(CollectionUtils.isNotEmpty(entries)){

			return entries.stream().filter(entry->OrderEntryType.TRANSPORT.equals(entry.getType()) && entry.getBookingReference()!=null).map(entry->entry.getBookingReference()).findAny().orElse(null);
		}
		return null;
	}


	protected String getDepartureTime(final List<AbstractOrderEntryModel> entries)
	{
		final TransportOfferingModel firstTransportOffering = entries.get(0).getTravelOrderEntryInfo().getTransportOfferings()
				.stream().filter(Objects::nonNull).reduce((a, b) -> a).get();
		return TravelDateUtils
				.convertDateToStringDate(firstTransportOffering.getDepartureTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
	}

	public void initializePassengerForm(final PassengerInfoForm passengerInfoForm, final int journeyRefNum,
			final int odRefNum, List<AbstractOrderEntryModel> entries)
	{
		if (entries == null)
		{
			entries = bcfTravelCartService.getCartEntriesForRefNo(journeyRefNum, odRefNum);

		}
		final List<AbstractOrderEntryModel> abstractOrderEntryModels = entries.stream().filter(
				entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
						&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
						FareProductType.PASSENGER)).collect(Collectors.toList());
		final Map<String, Long> travellersMap = abstractOrderEntryModels.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getTravellers().iterator().next()).collect(Collectors
						.groupingBy(traveller -> ((PassengerInformationModel) traveller.getInfo()).getPassengerType()
								.getCode(), Collectors.counting()));
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<PassengerTypeData> passengerTypesDatas = travellerSortStrategy
				.sortPassengerTypes(passengerTypeFacade.getPassengerTypes());
		for (final PassengerTypeData passengerTypeData : passengerTypesDatas)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(0);

			final Long passengerQuantity = travellersMap.get(passengerTypeData.getCode());

			if (passengerQuantity != null && passengerQuantity > 0)
			{
				passengerTypeQuantityData.setQuantity(passengerQuantity.intValue());
			}
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		passengerInfoForm.setBasePassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BASE_PASSENGERS)));
		passengerInfoForm.setAdditionalPassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(ADDITIONAL_PASSENGERS)));
		passengerInfoForm.setNorthernPassengers(
				BCFPropertiesUtils.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(NORTHERN_PASSENGERS)));
		sortPassengers(passengerInfoForm);
		final List<String> largeItemsCodes = BCFPropertiesUtils.convertToList(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.LARGE_ITEMS_PRODUCTS_CODES));
		final List<String> accessibilityProductCodes = BCFPropertiesUtils.convertToList(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.ACCESSIBILITY_REQUEST_PRODUCTS));
		final List<String> specialServiceProductCodes = BCFPropertiesUtils.convertToList(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfvoyageaddonWebConstants.SPECIAL_SERVICE_REQUEST_PRODUCTS));
		final Map<String, List<ProductData>> ancillaryProductMap = entries.stream()
				.filter(entry -> entry.getProduct().getItemtype().equals(AncillaryProductModel._TYPECODE) && accessibilityProductCodes
						.contains(entry.getProduct().getCode())).collect(Collectors.groupingBy(
						entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator().next()
								.getInfo()).getPassengerType().getCode(),
						Collectors.mapping(entry -> ancillaryProductConverter.convert(entry.getProduct()), Collectors.toList())));

		final Map<String, List<AbstractOrderEntryModel>> specialReqEntriesMap = entries.stream().filter(
				entry -> entry.getProduct().getItemtype().equals(FareProductModel._TYPECODE)
						&& entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail() != null
						&& CollectionUtils
						.isNotEmpty(entry.getTravelOrderEntryInfo().getTravellers().iterator().next().getSpecialRequestDetail()
								.getSpecialServiceRequest())).collect(
				Collectors.groupingBy(
						entry -> ((PassengerInformationModel) entry.getTravelOrderEntryInfo().getTravellers().iterator().next()
								.getInfo()).getPassengerType().getCode()));
		final List<AccessibilityRequestData> accessibilityRequestDataList = bcfvoyageaddonComponentControllerUtil
				.getFilteredAccessibilityRequestDataList(passengerInfoForm.getAccessibilityRequestDataList());
		setAccessibilityData(passengerTypeQuantityList, ancillaryProductMap, specialReqEntriesMap, accessibilityRequestDataList);
		if (BcfCoreConstants.INBOUND_REFERENCE_NUMBER == odRefNum)
		{
			passengerInfoForm.setInboundPassengerTypeQuantityList(passengerTypeQuantityList);
			passengerInfoForm.setCarryingDangerousGoodsInReturn(
					abstractOrderEntryModels.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods());
			passengerInfoForm.setInboundaccessibilityRequestDataList(accessibilityRequestDataList);
			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				passengerInfoForm.setAccessibilityNeedsCheckInbound(true);
			}
		}
		else
		{
			passengerInfoForm.setPassengerTypeQuantityList(passengerTypeQuantityList);
			passengerInfoForm.setCarryingDangerousGoodsInOutbound(
					abstractOrderEntryModels.get(0).getTravelOrderEntryInfo().isCarryingDangerousGoods());
			passengerInfoForm.setAccessibilityRequestDataList(accessibilityRequestDataList);
			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				passengerInfoForm.setAccessibilityNeedsCheckOutbound(true);
			}
		}
		setLargeItemData(passengerInfoForm, entries, largeItemsCodes, odRefNum);
	}

	public void sortPassengers(final PassengerInfoForm passengerInfoForm)
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityDataList = new ArrayList<>();
		final List<String> orderedPassengersList = new ArrayList<>(passengerInfoForm.getBasePassengers());
		orderedPassengersList.addAll(passengerInfoForm.getAdditionalPassengers());
		orderedPassengersList.addAll(passengerInfoForm.getNorthernPassengers());
		StreamUtil.safeStream(orderedPassengersList).forEach(paxCode ->
				StreamUtil.safeStream(passengerInfoForm.getPassengerTypeQuantityList()).forEach(passengerTypeQuantityData -> {
					if (StringUtils
							.equalsIgnoreCase(paxCode, passengerTypeQuantityData.getPassengerType().getCode()))
					{
						passengerTypeQuantityDataList.add(passengerTypeQuantityData);
					}
				})
		);
		passengerInfoForm.setPassengerTypeQuantityList(passengerTypeQuantityDataList);
	}

	public void setLargeItemData(final PassengerInfoForm passengerInfoForm, final List<AbstractOrderEntryModel> entries,
			final List<String> largeItemCodes, final int odRefNumber)
	{
		final Map<String, List<AbstractOrderEntryModel>> stringListMap = StreamUtil.safeStream(entries)
				.filter(entry -> StringUtils.equalsIgnoreCase(AncillaryProductModel._TYPECODE, entry.getProduct().getItemtype()))
				.collect(Collectors.groupingBy(o -> o.getProduct().getCode()));
		final List<ProductData> productDataList = ancillaryProductFacade.getAncillaryProductsForCodes(largeItemCodes);
		final List<LargeItemData> largeItemsDataList = new ArrayList<>();
		StreamUtil.safeStream(productDataList).forEach(ancillaryProduct -> {
			final LargeItemData largeItemsData = new LargeItemData();
			largeItemsData.setCode(ancillaryProduct.getCode());
			largeItemsData.setName(ancillaryProduct.getName());
			if (CollectionUtils.isNotEmpty(stringListMap.get(ancillaryProduct.getCode())))
			{
				largeItemsData.setQuantity(stringListMap.get(ancillaryProduct.getCode()).size());
			}
			else
			{
				largeItemsData.setQuantity(0);
			}
			largeItemsDataList.add(largeItemsData);
		});
		if (CollectionUtils.isNotEmpty(largeItemsDataList))
		{
			if (odRefNumber == 0)
			{
				passengerInfoForm.setLargeItems(largeItemsDataList);
			}
			else if (odRefNumber == 1)
			{
				passengerInfoForm.setLargeItemsInbound(largeItemsDataList);
			}
		}
		if (StreamUtil.safeStream(passengerInfoForm.getLargeItems()).anyMatch(largeItemData -> largeItemData.getQuantity() > 0))
		{
			passengerInfoForm.setLargeItemsCheckOutbound(true);
		}
		if (StreamUtil.safeStream(passengerInfoForm.getLargeItemsInbound())
				.anyMatch(largeItemData -> largeItemData.getQuantity() > 0))
		{
			passengerInfoForm.setLargeItemsCheckInbound(true);
		}
	}

	private void setAccessibilityData(final List<PassengerTypeQuantityData> passengerTypeQuantityList,
			final Map<String, List<ProductData>> ancillaryProductMap,
			final Map<String, List<AbstractOrderEntryModel>> specialReqEntriesMap,
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		List<ProductData> ancillaryProducts = null;
		List<AbstractOrderEntryModel> specialReqsEntries = null;

		for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
		{

			if (MapUtils.isNotEmpty(ancillaryProductMap))
			{
				ancillaryProducts = ancillaryProductMap.get(passengerTypeQuantityData.getPassengerType().getCode());
			}

			if (MapUtils.isNotEmpty(specialReqEntriesMap))
			{
				specialReqsEntries = specialReqEntriesMap.get(passengerTypeQuantityData.getPassengerType().getCode());
			}


			for (int i = 0; i < passengerTypeQuantityData.getQuantity(); i++)
			{

				final AccessibilityRequestData accessibilityRequestData = new AccessibilityRequestData();
				accessibilityRequestData.setPassengerType(passengerTypeQuantityData.getPassengerType());
				accessibilityRequestData.setSpecialServiceRequestDataList(
						bcfvoyageaddonComponentControllerUtil
								.getSpecialServiceDisplayList(bcfSpecialAssistanceFacade.getAllSpecialServiceRequest(),
										bcfvoyageaddonComponentControllerUtil.getSpecialServiceRequestProductsSequence()));
				accessibilityRequestData.setAncillaryRequestDataList(
						bcfvoyageaddonComponentControllerUtil
								.getProductListByDisplaySequence(bcfvoyageaddonComponentControllerUtil.getAccessibilityProductData(),
										bcfvoyageaddonComponentControllerUtil.getAccessibilityRequestProductsSequence()));
				accessibilityRequestDataList.add(accessibilityRequestData);
				if (CollectionUtils.isNotEmpty(ancillaryProducts) && ancillaryProducts.size() > i)
				{

					accessibilityRequestData.setSelection(ancillaryProducts.get(i).getCode());
				}
				setSpecialServiceRequestSelection(specialReqsEntries, i, accessibilityRequestData);

			}
		}
	}

	private void setSpecialServiceRequestSelection(final List<AbstractOrderEntryModel> specialReqsEntries, final int i,
			final AccessibilityRequestData accessibilityRequestData)
	{
		if (CollectionUtils.isNotEmpty(specialReqsEntries) && specialReqsEntries.size() > i)
		{
			accessibilityRequestData
					.setPassengerUid(specialReqsEntries.get(i).getTravelOrderEntryInfo().getTravellers().iterator().next().getUid());

			final List<String> specialReqs = specialReqsEntries.get(i).getTravelOrderEntryInfo().getTravellers().iterator()
					.next()
					.getSpecialRequestDetail()
					.getSpecialServiceRequest().stream().map(SpecialServiceRequestModel::getCode).collect(
							Collectors.toList());

			for (final SpecialServiceRequestData specialServiceRequestData : accessibilityRequestData
					.getSpecialServiceRequestDataList())
			{
				if (specialReqs.stream().anyMatch(specialReq -> specialReq.equals(specialServiceRequestData.getCode())))
				{
					specialServiceRequestData.setSelection(true);
				}
			}

		}
	}

	private void initializeVehicleForm(final VehicleInfoForm vehicleInfoForm, final List<AbstractOrderEntryModel> entries)
	{
		final List<AbstractOrderEntryModel> vehicleEntryModels = entries.stream().filter(
				entry -> entry.getType().equals(OrderEntryType.TRANSPORT) && entry.getProduct() instanceof FareProductModel
						&& ((FareProductModel) entry.getProduct()).getFareProductType().equals(
						FareProductType.VEHICLE)).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(vehicleEntryModels))
		{
			final String travelRoute = vehicleEntryModels.get(0).getTravelOrderEntryInfo().getTravelRoute().getCode();
			final List<VehicleTypeQuantityData> vehicleInfo = new ArrayList<>();
			populateVehicleInfo(vehicleEntryModels, travelRoute, vehicleInfo);
			vehicleInfoForm.setVehicleInfo(vehicleInfo);
		}
	}

	private void populateVehicleInfo(final List<AbstractOrderEntryModel> vehicleEntryModels, final String travelRoute,
			final List<VehicleTypeQuantityData> vehicleInfo)
	{
		for (final AbstractOrderEntryModel vehicleEntryModel : vehicleEntryModels)
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = new VehicleTypeQuantityData();
			final VehicleTypeData vehicleTypeData = new VehicleTypeData();
			final BCFVehicleInformationModel vehicleInfoModel = (BCFVehicleInformationModel) vehicleEntryModel
					.getTravelOrderEntryInfo()
					.getTravellers().iterator().next().getInfo();
			if (Objects.nonNull(vehicleInfoModel) && Objects.nonNull(vehicleInfoModel.getVehicleType()) && Objects
					.nonNull(vehicleInfoModel.getVehicleType().getType()))
			{
				vehicleTypeData.setCode(vehicleInfoModel.getVehicleType().getType().getCode());
				vehicleTypeData.setCategory(vehicleInfoModel.getVehicleType().getVehicleCategoryType().getCode());
				vehicleTypeQuantityData.setVehicleType(vehicleTypeData);
				vehicleTypeQuantityData.setQty(1);
				vehicleTypeQuantityData
						.setCarryingDangerousGoods(vehicleEntryModel.getTravelOrderEntryInfo().isCarryingDangerousGoods());
				vehicleTypeQuantityData.setHeight(vehicleInfoModel.getHeight() != null ? vehicleInfoModel.getHeight() : 0);
				vehicleTypeQuantityData.setLength(vehicleInfoModel.getLength() != null ? vehicleInfoModel.getLength() : 0);
				vehicleTypeQuantityData.setWeight(vehicleInfoModel.getWeight() != null ? vehicleInfoModel.getWeight() : 0);
				vehicleTypeQuantityData.setCarryingLivestock(BooleanUtils.toBoolean(vehicleInfoModel.getCarryingLivestock()));
				vehicleTypeQuantityData
						.setNumberOfAxis(vehicleInfoModel.getNumberOfAxis() != null ? vehicleInfoModel.getNumberOfAxis() : 0);
				vehicleTypeQuantityData
						.setVehicleWithSidecarOrTrailer(BooleanUtils.toBoolean(vehicleInfoModel.getVehicleWithSidecarOrTrailer()));
				vehicleTypeQuantityData.setTravelRouteCode(travelRoute);
				vehicleInfo.add(vehicleTypeQuantityData);
			}
		}
	}

	private List<LargeItemData> getLargeItemList(final List<LargeItemData> largeItemsList)
	{
		final List<LargeItemData> largeItemList = new ArrayList<>();
		StreamUtil.safeStream(largeItemsList).forEach(largeItemsData -> {
			if (largeItemsData.getQuantity() > 0 && StringUtils.isNotBlank(largeItemsData.getCode()))
			{
				final LargeItemData largeItemData = new LargeItemData();
				largeItemData.setCode(largeItemsData.getCode());
				largeItemData.setName(largeItemsData.getName());
				largeItemData.setQuantity(largeItemsData.getQuantity());
				largeItemList.add(largeItemData);
			}
		});
		return CollectionUtils.isNotEmpty(largeItemList) ? largeItemList : null;
	}

	public boolean isAdult(final String code)
	{
		return (StringUtils.isNotEmpty(code) && BCFPropertiesUtils
				.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.PASSENGER_ADULT_LIST))
				.contains(code));
	}

	public PricedItineraryData getFirstItinerary(final FareSelectionData fareSelectionData, final int refNumber)
	{
		if (CollectionUtils.isNotEmpty(fareSelectionData.getPricedItineraries()))
		{

			for (final PricedItineraryData pricedItineraryData : fareSelectionData.getPricedItineraries())
			{

				if (pricedItineraryData.isAvailable() && pricedItineraryData.getOriginDestinationRefNumber() == refNumber
						&& CollectionUtils.isNotEmpty(pricedItineraryData.getItineraryPricingInfos()))
				{
					return pricedItineraryData;
				}
			}
		}
		return null;
	}

	public void updateThresholdForOver9FeetWideVehicle(final BCFFareFinderForm bcfFareFinderForm)
	{
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && CollectionUtils
				.isNotEmpty(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo()))
		{
			bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().stream().forEach(vehicleTypeQuantityData -> {
				if (BooleanUtils.toBoolean(vehicleTypeQuantityData.getOver9FtWide()))
				{
					final double over9FeetWideThreshold = Double.parseDouble(
							bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.OVER_9_FEET_WIDE_THRESHOLD_HOURS));
					if (over9FeetWideThreshold > bcfFareFinderForm.getThresholdHours())
					{
						bcfFareFinderForm.setThresholdHours(over9FeetWideThreshold);
					}
					return;
				}
				else
				{
					bcfFareFinderForm.setThresholdHours(Double.parseDouble(bcfConfigurablePropertiesService
							.getBcfPropertyValue(BcfFacadesConstants.SAME_DAY_TRANSPORT_BOOKING_THRESHOLD_HOURS)));
				}
			});
		}
	}

	public void cleanUpPassengerInfoForm(final BCFFareFinderForm bcfFareFinderForm)
	{
		if (Objects.nonNull(bcfFareFinderForm.getRouteInfoForm()) && Objects.nonNull(bcfFareFinderForm.getPassengerInfoForm()))
		{
			if (StringUtils.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getRouteType(), RouteType.SHORT.getCode()))
			{
				final List<String> nrPaxCodes = BCFPropertiesUtils
						.convertToList(bcfConfigurablePropertiesService.getBcfPropertyValue(NORTHERN_PASSENGERS));
				StreamUtil.safeStream(bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList())
						.forEach(paxType -> {
							if (nrPaxCodes.contains(paxType.getPassengerType().getCode()))
							{
								paxType.setQuantity(0);
							}
						});
				StreamUtil.safeStream(bcfFareFinderForm.getPassengerInfoForm().getInboundPassengerTypeQuantityList())
						.forEach(paxType -> {
							if (nrPaxCodes.contains(paxType.getPassengerType().getCode()))
							{
								paxType.setQuantity(0);
							}
						});
				final List<AccessibilityRequestData> nrOutboundAccessibility = StreamUtil
						.safeStream(bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList())
						.filter(accessibilityRequestData -> nrPaxCodes.contains(accessibilityRequestData.getPassengerType().getCode()))
						.collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList()))
				{
					bcfFareFinderForm.getPassengerInfoForm().getAccessibilityRequestDataList().removeAll(nrOutboundAccessibility);
				}
				final List<AccessibilityRequestData> nrReturnAccessibility = StreamUtil
						.safeStream(bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList())
						.filter(accessibilityRequestData -> nrPaxCodes.contains(accessibilityRequestData.getPassengerType().getCode()))
						.collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList()))
				{
					bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList().removeAll(nrReturnAccessibility);
				}
			}
			if (!bcfFareFinderForm.getRouteInfoForm().isWalkOnRoute())
			{
				bcfFareFinderForm.getPassengerInfoForm().setTravellingAsWalkOn(false);
			}
		}
	}

	public boolean isReturningWithSamePassenger(final PassengerInfoForm passengerInfoForm)
	{
		if (CollectionUtils.isNotEmpty(passengerInfoForm.getPassengerTypeQuantityList()) && CollectionUtils
				.isNotEmpty(passengerInfoForm.getInboundPassengerTypeQuantityList()))
		{
			for (int i = 0; i < passengerInfoForm.getPassengerTypeQuantityList().size(); i++)
			{
				if (passengerInfoForm.getPassengerTypeQuantityList().get(i).getQuantity() != passengerInfoForm
						.getInboundPassengerTypeQuantityList()
						.get(i).getQuantity())
				{
					return false;
				}
			}
		}
		return true;
	}

	public boolean isReturningWithSameVehicle(final VehicleInfoForm vehicleInfoForm)
	{
		return CollectionUtils.isNotEmpty(vehicleInfoForm.getVehicleInfo()) && vehicleInfoForm.getVehicleInfo().size() == 2
				&& vehicleInfoForm.getVehicleInfo().get(0).getVehicleType().getCode() == vehicleInfoForm.getVehicleInfo().get(1)
				.getVehicleType().getCode() && vehicleInfoForm.getVehicleInfo().get(0).getWidth() == vehicleInfoForm
				.getVehicleInfo().get(1).getWidth() && vehicleInfoForm.getVehicleInfo().get(0).getHeight() == vehicleInfoForm
				.getVehicleInfo().get(1).getHeight() && vehicleInfoForm.getVehicleInfo().get(0).getLength() == vehicleInfoForm
				.getVehicleInfo().get(1).getLength()
				&& vehicleInfoForm.getVehicleInfo().get(0).getNumberOfAxis() == vehicleInfoForm.getVehicleInfo().get(1)
				.getNumberOfAxis() && vehicleInfoForm.getVehicleInfo().get(0).getWeight() == vehicleInfoForm.getVehicleInfo()
				.get(1).getWeight();
	}

	public void convertToOneWay(final BCFFareFinderForm bcfFareFinderForm, final int odRef)
	{
		bcfFareFinderForm.getRouteInfoForm().setTripType(TripType.SINGLE.name());
		if (odRef == 0)
		{
			bcfFareFinderForm.getPassengerInfoForm().setReturnWithDifferentPassenger(Boolean.FALSE);
			bcfFareFinderForm.getPassengerInfoForm().setInboundPassengerTypeQuantityList(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setInboundaccessibilityRequestDataList(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setAccessibilityNeedsCheckInbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsCheckInbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsInbound(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setCarryingDangerousGoodsInReturn(false);
			bcfFareFinderForm.getPassengerInfoForm().setExtraBaggageInbound(null);
			if (!bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn() && Objects
					.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && bcfFareFinderForm.getVehicleInfoForm()
					.isTravellingWithVehicle())
			{
				bcfFareFinderForm.getVehicleInfoForm().setVehicleInfo(java.util.Collections.singletonList(
						StreamUtil.safeStream(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo()).findFirst().get()));
				bcfFareFinderForm.getVehicleInfoForm().setReturnWithDifferentVehicle(false);
			}
		}
		else
		{
			final String arrivalLocation = bcfFareFinderForm.getRouteInfoForm().getArrivalLocation();
			final String departureLocation = bcfFareFinderForm.getRouteInfoForm().getDepartureLocation();
			final String arrivalLocationName = bcfFareFinderForm.getRouteInfoForm().getArrivalLocationName();
			final String departureLocationName = bcfFareFinderForm.getRouteInfoForm().getDepartureLocationName();
			final String arrivalLocationSuggestionType = bcfFareFinderForm.getRouteInfoForm().getArrivalLocationSuggestionType();
			final String departureLocationSuggestionType = bcfFareFinderForm.getRouteInfoForm().getDepartureLocationSuggestionType();
			final String returnDateTime = bcfFareFinderForm.getRouteInfoForm().getReturnDateTime();
			bcfFareFinderForm.getRouteInfoForm().setArrivalLocation(departureLocation);
			bcfFareFinderForm.getRouteInfoForm().setDepartureLocation(arrivalLocation);
			bcfFareFinderForm.getRouteInfoForm().setArrivalLocationName(departureLocationName);
			bcfFareFinderForm.getRouteInfoForm().setDepartureLocationName(arrivalLocationName);
			bcfFareFinderForm.getRouteInfoForm().setArrivalLocationSuggestionType(departureLocationSuggestionType);
			bcfFareFinderForm.getRouteInfoForm().setDepartureLocationSuggestionType(arrivalLocationSuggestionType);
			bcfFareFinderForm.getRouteInfoForm().setDepartingDateTime(returnDateTime);
			bcfFareFinderForm.getPassengerInfoForm()
					.setPassengerTypeQuantityList(bcfFareFinderForm.getPassengerInfoForm().getInboundPassengerTypeQuantityList());
			bcfFareFinderForm.getPassengerInfoForm().setAccessibilityRequestDataList(
					bcfFareFinderForm.getPassengerInfoForm().getInboundaccessibilityRequestDataList());
			bcfFareFinderForm.getPassengerInfoForm()
					.setAccessibilityNeedsCheckOutbound(bcfFareFinderForm.getPassengerInfoForm().isAccessibilityNeedsCheckInbound());
			bcfFareFinderForm.getPassengerInfoForm()
					.setLargeItemsCheckOutbound(bcfFareFinderForm.getPassengerInfoForm().isLargeItemsCheckInbound());
			bcfFareFinderForm.getPassengerInfoForm().setLargeItems(bcfFareFinderForm.getPassengerInfoForm().getLargeItemsInbound());
			bcfFareFinderForm.getPassengerInfoForm()
					.setCarryingDangerousGoodsInOutbound(bcfFareFinderForm.getPassengerInfoForm().isCarryingDangerousGoodsInReturn());
			bcfFareFinderForm.getPassengerInfoForm()
					.setExtraBaggageOutbound(bcfFareFinderForm.getPassengerInfoForm().getExtraBaggageInbound());
			bcfFareFinderForm.getPassengerInfoForm().setReturnWithDifferentPassenger(Boolean.FALSE);
			bcfFareFinderForm.getPassengerInfoForm().setInboundPassengerTypeQuantityList(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setInboundaccessibilityRequestDataList(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setAccessibilityNeedsCheckInbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsCheckInbound(false);
			bcfFareFinderForm.getPassengerInfoForm().setLargeItemsInbound(java.util.Collections.emptyList());
			bcfFareFinderForm.getPassengerInfoForm().setCarryingDangerousGoodsInReturn(false);
			bcfFareFinderForm.getPassengerInfoForm().setExtraBaggageInbound(null);
			if (!bcfFareFinderForm.getPassengerInfoForm().isTravellingAsWalkOn() && Objects
					.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && bcfFareFinderForm.getVehicleInfoForm()
					.isTravellingWithVehicle())
			{
				java.util.Collections.reverse(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo());
				bcfFareFinderForm.getVehicleInfoForm().setVehicleInfo(java.util.Collections.singletonList(
						StreamUtil.safeStream(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo()).findFirst().get()));
				bcfFareFinderForm.getVehicleInfoForm().setReturnWithDifferentVehicle(false);
			}
		}
	}

	public TravelRouteData getTravelRouteData(final String arrivalLocation, final String departureLocation)
	{
		if (arrivalLocation == null || departureLocation == null)
		{
			return null;
		}
		return (bcfTravelRouteFacade.getTravelRoutes(arrivalLocation, departureLocation)).stream().findFirst().orElse(null);
	}

	public void setVehicleDimensionThreshold(final BCFFareFinderForm bcfFareFinderForm)
	{
		final TravelRouteData travelRouteData;
		if (Objects.nonNull(bcfFareFinderForm) && Objects.nonNull(bcfFareFinderForm.getRouteInfoForm()) && Objects
				.nonNull(bcfFareFinderForm.getVehicleInfoForm()))
		{
			travelRouteData = getTravelRouteData(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation(),
					bcfFareFinderForm.getRouteInfoForm().getArrivalLocation());
			if (Objects.nonNull(travelRouteData) && StringUtils.isNotBlank(travelRouteData.getCode()))
			{
				final TravelRouteModel travelRoute = travelRouteService.getTravelRoute(travelRouteData.getCode());
				if (Objects.nonNull(travelRoute))
				{
					final TransportFacilityModel originTransportFacility = transportFacilityService
							.getTransportFacility(travelRoute.getOrigin().getCode());
					final TransportFacilityModel destinationTransportFacility = transportFacilityService
							.getTransportFacility(travelRoute.getDestination().getCode());
					if (Objects.nonNull(originTransportFacility) && Objects.nonNull(destinationTransportFacility))
					{
						bcfFareFinderForm.getVehicleInfoForm()
								.setStandardVehicleLengthPortLimit(getMinOrZero(originTransportFacility.getStandardVehicleLength(),
										destinationTransportFacility.getStandardVehicleLength()));
						bcfFareFinderForm.getVehicleInfoForm()
								.setStandardVehicleHeightPortLimit(getMinOrZero(originTransportFacility.getStandardVehicleHeight(),
										destinationTransportFacility.getStandardVehicleHeight()));
						bcfFareFinderForm.getVehicleInfoForm()
								.setOver5500VehicleLengthPortLimit(getMinOrZero(originTransportFacility.getOver5500VehicleLength(),
										destinationTransportFacility.getOver5500VehicleLength()));
						bcfFareFinderForm.getVehicleInfoForm()
								.setOver5500VehicleHeightPortLimit(getMinOrZero(originTransportFacility.getOver5500VehicleHeight(),
										destinationTransportFacility.getOver5500VehicleHeight()));
						bcfFareFinderForm.getVehicleInfoForm()
								.setVehicleWidthPortLimit(getMinOrZero(originTransportFacility.getVehicleWidth(),
										destinationTransportFacility.getVehicleWidth()));
					}
				}
			}
			bcfFareFinderForm.getVehicleInfoForm().setOver5500VehicleHeightWarningLimit(
					Integer.parseInt(bcfConfigurablePropertiesService.getBcfPropertyValue("over5500VehicleHeightWarningLimit")));
			bcfFareFinderForm.getVehicleInfoForm().setOver5500VehicleLengthWarningLimit(
					Integer.parseInt(bcfConfigurablePropertiesService.getBcfPropertyValue("over5500VehicleLengthWarningLimit")));
		}
	}

	private int getMinOrZero(Integer x, Integer y)
	{
		if (Objects.isNull(x))
		{
			x = 0;
		}
		if (Objects.isNull(y))
		{
			y = 0;
		}
		return Math.min(x, y);
	}

	public void updateWalkOnAttributes(final RouteInfoForm routeInfoForm)
	{
		if (Objects.nonNull(routeInfoForm) && StringUtils.isNotBlank(routeInfoForm.getDepartureLocation()) && StringUtils
				.isNotBlank(routeInfoForm.getArrivalLocation()))
		{
			routeInfoForm.setWalkOnRoute(
					bcfTravelRouteFacade.isWalkOnRoute(routeInfoForm.getDepartureLocation(), routeInfoForm.getArrivalLocation()));
			routeInfoForm.setAllowsWalkOnOptions(bcfTravelRouteFacade
					.walkOnOptionsAllowed(routeInfoForm.getDepartureLocation(), routeInfoForm.getArrivalLocation()));
			final RouteType routeType = bcfTravelRouteFacade
					.getRouteType(routeInfoForm.getDepartureLocation(), routeInfoForm.getArrivalLocation());
			if (Objects.nonNull(routeType))
			{
				routeInfoForm.setRouteType(routeType.getCode());
			}
		}
	}

	public void clearReturnSelections(final BCFFareFinderForm bcfFareFinderForm)
	{
		if (bcfFareFinderForm.getRouteInfoForm() != null && TripType.SINGLE.toString()
				.equals(bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			if (bcfFareFinderForm.getPassengerInfoForm() != null)
			{
				bcfFareFinderForm.getPassengerInfoForm().setReturnWithDifferentPassenger(false);
				bcfFareFinderForm.getPassengerInfoForm().setInboundaccessibilityRequestDataList(java.util.Collections.emptyList());
				bcfFareFinderForm.getPassengerInfoForm().setInboundPassengerTypeQuantityList(java.util.Collections.emptyList());
			}
			if (bcfFareFinderForm.getVehicleInfoForm() != null)
			{
				bcfFareFinderForm.getVehicleInfoForm().setReturnWithDifferentVehicle(false);
			}
		}
	}

	public void cleanVehicleInfoForm(final BCFFareFinderForm bcfFareFinderForm)
	{
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm()) && bcfFareFinderForm.getVehicleInfoForm()
				.isTravellingWithVehicle())
		{
			cleanVehicleFields(bcfFareFinderForm.getVehicleInfoForm(), BcfCoreConstants.OUTBOUND_REFERENCE_NUMBER);
			if (StringUtils.equals(TripType.RETURN.name(), bcfFareFinderForm.getRouteInfoForm().getTripType()))
			{
				cleanVehicleFields(bcfFareFinderForm.getVehicleInfoForm(), BcfCoreConstants.INBOUND_REFERENCE_NUMBER);
			}
		}
	}

	private void cleanVehicleFields(final VehicleInfoForm vehicleInfoForm, final int odRef)
	{
		final VehicleTypeQuantityData vehicleData = vehicleInfoForm.getVehicleInfo().get(odRef);
		if (Objects.isNull(vehicleData) || Objects.isNull(vehicleData.getVehicleType()) || Objects
				.isNull(vehicleData.getVehicleType().getCode()))
		{
			return;
		}
		switch (vehicleData.getVehicleType().getCode())
		{
			case BcfCoreConstants.MOTORCYCLE_STANDARD:
				vehicleData.setLength(0);
				vehicleData.setHeight(0);
				vehicleData.setWidth(0);
				vehicleData.setWeight(0);
				vehicleData.setNumberOfAxis(0);
				vehicleData.setOver9FtWide(null);
				vehicleData.setAdjustable(null);
				vehicleData.setGroundClearance(0);
				break;
			case BcfCoreConstants.MOTORCYCLE_WITH_TRAILER:
				vehicleData.setHeight(0);
				vehicleData.setWidth(0);
				vehicleData.setWeight(0);
				vehicleData.setNumberOfAxis(0);
				vehicleData.setOver9FtWide(null);
				vehicleData.setAdjustable(null);
				vehicleData.setGroundClearance(0);
				if (!vehicleData.isLengthProvidedForMCT())
				{
					vehicleData.setLength(0);
				}
				break;
			case BcfCoreConstants.UNDER_HEIGHT:
				vehicleData.setHeight(0);
				vehicleData.setWidth(0);
				vehicleData.setWeight(0);
				vehicleData.setNumberOfAxis(0);
				vehicleData.setOver9FtWide(null);
				vehicleData.setAdjustable(null);
				vehicleData.setGroundClearance(0);
				if (StringUtils.equalsIgnoreCase("underLength", vehicleData.getStandardOversizeLengthOpted()))
				{
					vehicleData.setLength(0);
				}
				break;
			case BcfCoreConstants.OVERSIZE:
				vehicleData.setWidth(0);
				vehicleData.setWeight(0);
				vehicleData.setNumberOfAxis(0);
				vehicleData.setOver9FtWide(null);
				vehicleData.setAdjustable(null);
				vehicleData.setGroundClearance(0);
				if (StringUtils.equalsIgnoreCase("underLength", vehicleData.getStandardOversizeLengthOpted()))
				{
					vehicleData.setLength(0);
				}
				break;
			default:
				if (AccountType.FULL_ACCOUNT.equals(((CustomerModel) userService.getCurrentUser()).getAccountType()))
				{
					if (!BooleanUtils.toBoolean(vehicleData.getOver9FtWide()))
					{
						vehicleData.setWidth(0);
						if (vehicleData.getLength() <= vehicleInfoForm.getOver5500VehicleLengthPortLimit()
								&& vehicleData.getHeight() <= vehicleInfoForm.getOver5500VehicleHeightPortLimit())
						{
							vehicleData.setWidth(0);
							vehicleData.setWeight(0);
							vehicleData.setNumberOfAxis(0);
							vehicleData.setAdjustable(null);
							vehicleData.setGroundClearance(0);
						}
					}
				}
				else
				{
					vehicleData.setWidth(0);
					vehicleData.setWeight(0);
					vehicleData.setNumberOfAxis(0);
					vehicleData.setAdjustable(null);
					vehicleData.setGroundClearance(0);
				}
		}
		vehicleData.setStandardOversizeLengthOpted(null);
		vehicleData.setLengthProvidedForMCT(false);
	}

}
