/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.VehicleCategoryType;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


@Component("bcfNonStandardVehicleFareFinderValidator")
public class BcfNonStandardVehicleFareFinderValidator extends VehicleInfoValidator implements Validator
{
	private static final String LENGTH_ERROR_FIELD = "motorcycleLengthError";

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return FareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityData = (List<VehicleTypeQuantityData>) object;
		validateVehicleType(vehicleTypeQuantityData, errors);
	}

	private void validateVehicleType(final List<VehicleTypeQuantityData> vehicleTypeQuantityData, final Errors errors)
	{
		for (int i = 0; i < vehicleTypeQuantityData.size(); i++)
		{
			if ((Objects.nonNull(vehicleTypeQuantityData.get(i).getVehicleType())
					&& StringUtils.isNotEmpty(vehicleTypeQuantityData.get(i).getVehicleType().getCategory())) && (
					vehicleTypeQuantityData.get(i).getVehicleType().getCategory().toUpperCase()
							.equalsIgnoreCase(VehicleCategoryType.NONSTANDARD.getCode()) && !BcfCoreConstants.MOTORCYCLE_STANDARD
							.equalsIgnoreCase(vehicleTypeQuantityData.get(i).getVehicleType().getCode())))
			{
				final TravelRouteModel travelRouteModel = getTravelRoute(vehicleTypeQuantityData.get(i).getTravelRouteCode());
				final TransportFacilityModel originTransportFacilityData = getTransportFacility(
						travelRouteModel.getOrigin().getCode());
				final TransportFacilityModel destinationTransportFacilityData = getTransportFacility(
						travelRouteModel.getDestination().getCode());
				if (Objects.nonNull(originTransportFacilityData) && Objects.nonNull(destinationTransportFacilityData))
				{
					validateNonStandardVehicle(originTransportFacilityData, destinationTransportFacilityData,
							vehicleTypeQuantityData.get(i), i, errors);
				}
			}

		}
	}

	/*
	 *
	 *
	 */
	protected void validateNonStandardVehicle(final TransportFacilityModel originData,
			final TransportFacilityModel destinationData,
			final VehicleTypeQuantityData vehicleTypeQuantityData,
			final int i, final Errors errors)
	{
		switch (vehicleTypeQuantityData.getVehicleType().getCode())
		{
			case "MST":
				validateMotorcycleLength(originData, destinationData, vehicleTypeQuantityData, i, errors);
				break;
			default:
				break;
		}
	}

	private boolean validateMotorcycleLength(final TransportFacilityModel originData, final TransportFacilityModel destinationData,
			final VehicleTypeQuantityData vehicleTypeQuantityData, final int i, final Errors errors)
	{
		if (vehicleTypeQuantityData.getLength() > 0d && vehicleTypeQuantityData.getLength() < 1d)
		{
			rejectValue(errors, VEHICLE_INFO + i + "]." + LENGTH_ERROR_FIELD, ERROR_NOT_VALID_LENGTH);
			return Boolean.FALSE;
		}
		return true;
	}

}
