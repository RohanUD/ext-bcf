/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import de.hybris.platform.travelservices.services.TransportFacilityService;
import de.hybris.platform.travelservices.services.TravelRouteService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.core.vehicletype.VehicleTypeService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


/**
 * Custom validation to validate Vehicle Finder Form attributes which can't be validated using JSR303
 */

@Component("vehicleInfoValidator")
public class VehicleInfoValidator extends AbstractTravelValidator
{
	public static final String FORM_VEHICLE_INFO = "vehicleInfoForm.vehicleInfo[";
	@Resource(name = "findVehicleFareFinderValidatorMap")
	private Map<String, Validator> findVehicleFareFinderValidatorMap;
	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	@Resource(name = "transportFacilityService")
	TransportFacilityService transportFacilityService;
	@Resource(name = "travelRouteService")
	private TravelRouteService travelRouteService;
	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;
	@Resource
	private UserService userService;
	@Resource(name = "timeService")
	private TimeService timeService;
	@Resource(name = "vehicleTypeService")
	private VehicleTypeService vehicleTypeService;
	@Resource(name = "modelService")
	private ModelService modelService;

	private static final String ERROR_VEHICLE_NOT_SELECTED = "vehicleNotSelected";
	private static final String ERROR_CURRENT_DATE = "currentDateEntered";
	private static final String ERROR_PREFIX = "error.ferry.farefinder.vehicleInfo";
	protected static final String WEIGHT = "weight";
	protected static final String ERROR_TYPE_EMPTY_LENGTH_FIELD = "emptyLengthField";
	protected static final String ERROR_TYPE_EMPTY_HEIGHT_FIELD = "emptyHeightField";
	protected static final String ERROR_PLEASE_CALL_TO_BOOK = "pleaseCallToBook";
	protected static final String ERROR_NOT_VALID_HEIGHT = "notValidHeight";
	protected static final String ERROR_NOT_VALID_LENGTH = "notValidLength";
	protected static final String VEHICLE_INFO = "vehicleInfoForm.vehicleInfo[";
	protected static final String ERROR_EXCEEDING_PORT_CONFIG = "exceedingPortConfiguration";
	protected static final String FARE_FINDER_VEHICLE_INFO = "fareFinderForm.vehicleInfo[";
	private static final String OTHER_OVERSIZE_VEH_CODE = "OTH";
	private static final String ERROR_OTHER_VEHICLE_SELECTED = "otherOversized.vehicle.select_another_vehicle";

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return BCFFareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFFareFinderForm bcfFareFinderForm = (BCFFareFinderForm) object;
		validateForThresholdTime(bcfFareFinderForm, errors, "vehicleInfoForm.travellingWithVehicle");
		validateVehicleData(bcfFareFinderForm, errors);
	}

	@Override
	protected void rejectValue(final Errors errors, String attributeName, final String errorCode)
	{
		final String errorMessageCode = ERROR_PREFIX + "." + errorCode;
		if (StringUtils.isNotEmpty(getAttributePrefix()))
		{
			attributeName = getAttributePrefix() + "." + attributeName;
		}
		errors.rejectValue(attributeName, errorMessageCode);
	}

	protected void validateForThresholdTime(final BCFFareFinderForm bcfFareFinderForm, final Errors errors, String field)
	{
		final boolean isInvalidSelection;
		final Date selectedDate = TravelDateUtils
				.convertStringDateToDate(bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(),
						BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final Date minDateTimeForBooking = DateUtils
				.addMinutes(new Date(), (int) Math.round(bcfFareFinderForm.getThresholdHours() * 60));
		final Date currentDate = TravelDateUtils.convertStringDateToDate(
				TravelDateUtils.convertDateToStringDate(timeService.getCurrentTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		if (selectedDate.before(currentDate))
		{
			isInvalidSelection = true;
		}
		else if (TravelDateUtils.isSameDate(selectedDate, currentDate))
		{
			isInvalidSelection = !TravelDateUtils.isSameDate(currentDate, minDateTimeForBooking);
		}
		else
		{
			final LocalTime hour = ZonedDateTime.now().toLocalTime();
			final Date selectedDateTime = DateUtils
					.addSeconds(DateUtils.addMinutes(DateUtils.addHours(selectedDate, hour.getHour()), hour.getMinute()),
							hour.getSecond());
			isInvalidSelection = !selectedDateTime.after(minDateTimeForBooking);
		}
		if (isInvalidSelection)
		{
			final String errorMessageCode = ERROR_PREFIX + "." + ERROR_CURRENT_DATE;
			if (StringUtils.isNotEmpty(getAttributePrefix()))
			{
				field = getAttributePrefix() + "." + field;
			}
			final Object[] localizationArguments = { bcfFareFinderForm.getThresholdHours() };
			errors.rejectValue(field, errorMessageCode, localizationArguments, null);
		}
	}

	protected void validateForCurrentDate(final String date, final Errors errors, final String field)
	{
		final Date selectedDate = TravelDateUtils.convertStringDateToDate(date, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final Date currentDate = TravelDateUtils.convertStringDateToDate(
				TravelDateUtils.convertDateToStringDate(timeService.getCurrentTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

		final boolean pastDate = selectedDate.before(currentDate);
		final boolean todaysDate = selectedDate.equals(currentDate);

		if (pastDate || todaysDate)
		{
			rejectValue(errors, field, ERROR_CURRENT_DATE);
		}
	}

	private boolean validateVehicleCode(final String code, final Errors errors, final int i)
	{
		if (StringUtils.isNotEmpty(code))
		{
			if (StringUtils.equalsIgnoreCase(OTHER_OVERSIZE_VEH_CODE, code))
			{
				rejectValue(errors, FORM_VEHICLE_INFO + i + "].vehicleType.code", ERROR_OTHER_VEHICLE_SELECTED);
			}
			return true;
		}
		rejectValue(errors, FORM_VEHICLE_INFO + i + "].vehicleType.code", ERROR_VEHICLE_NOT_SELECTED);
		return false;
	}

	/**
	 * Method responsible for validating Vehicles
	 *
	 * @param bcfFareFinderForm
	 */
	private void validateVehicleData(final BCFFareFinderForm bcfFareFinderForm, final Errors errors)
	{
		boolean validCode = false;
		if (bcfFareFinderForm.getRouteInfoForm().getTripType().equalsIgnoreCase("SINGLE"))
		{
			final String code = bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(0).getVehicleType().getCode();
			validCode = validateVehicleCode(code, errors, 0);
		}
		if (bcfFareFinderForm.getRouteInfoForm().getTripType().equalsIgnoreCase("RETURN"))
		{
			for (int i = 0; i < bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().size(); i++)
			{
				final String code = bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().get(i).getVehicleType().getCode();
				validCode = validateVehicleCode(code, errors, i);
				if (!validCode)
				{
					return;
				}
			}
		}
		if (!validCode)
		{
			return;
		}
		final Map<String, List<VehicleTypeQuantityData>> vehiclesGroupedByCategory = new HashMap<>();
		bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo().forEach(vehicleTypeQuantityData -> {
			List<VehicleTypeQuantityData> vehicleTypeQuantityDatas = vehiclesGroupedByCategory
					.get(vehicleTypeQuantityData.getVehicleType().getCategory());
			if (CollectionUtils.isEmpty(vehicleTypeQuantityDatas))
			{
				vehicleTypeQuantityDatas = new ArrayList<>();
				vehiclesGroupedByCategory.put(vehicleTypeQuantityData.getVehicleType().getCategory(), vehicleTypeQuantityDatas);
			}
			vehicleTypeQuantityDatas.add(vehicleTypeQuantityData);
		});

		vehiclesGroupedByCategory.forEach((category, vehicleTypeQuantityDatas) -> {
			if (StringUtils.isNotEmpty(category))
			{
				findVehicleFareFinderValidatorMap
						.get(category.toLowerCase())
						.validate(vehicleTypeQuantityDatas, errors);
			}
		});
	}

	protected Boolean validateBlankField(final String fieldName, final String fieldValue, final Errors errors,
			final String errorKey)
	{
		if (StringUtils.isBlank(fieldValue))
		{
			rejectValue(errors, fieldName, errorKey);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	protected TravelRouteModel getTravelRoute(final String code)
	{
		return (travelRouteService.getTravelRoute(code));
	}

	protected TransportFacilityModel getTransportFacility(final String code)
	{
		return (transportFacilityService.getTransportFacility(code));
	}

	protected boolean isAnonymousUser()
	{
		final UserModel user = userService.getCurrentUser();
		final boolean isAnonymousUser = userService.isAnonymousUser(user);
		return isAnonymousUser;
	}

	protected VehicleTypeModel fetchVehicleTypeModel(final VehicleTypeQuantityData vehicleTypeQuantityData)
	{
		if (Objects.nonNull(vehicleTypeQuantityData.getVehicleType().getCode()))
		{
			return getVehicleTypeService().getVehicleForCode(vehicleTypeQuantityData.getVehicleType().getCode());
		}

		return null;
	}

	public Map<String, Validator> getFindVehicleFareFinderValidatorMap()
	{
		return findVehicleFareFinderValidatorMap;
	}

	public void setFindVehicleFareFinderValidatorMap(final Map<String, Validator> findVehicleFareFinderValidatorMap)
	{
		this.findVehicleFareFinderValidatorMap = findVehicleFareFinderValidatorMap;
	}

	public VehicleTypeService getVehicleTypeService()
	{
		return vehicleTypeService;
	}

	public void setVehicleTypeService(final VehicleTypeService vehicleTypeService)
	{
		this.vehicleTypeService = vehicleTypeService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}
}
