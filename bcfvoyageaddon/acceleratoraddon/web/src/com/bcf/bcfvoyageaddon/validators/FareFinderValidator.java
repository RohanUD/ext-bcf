/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelfacades.facades.CabinClassFacade;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


/**
 * Custom validation to valid Fare Finder Form attributes which can't be validated using JSR303
 */

@Component("fareFinderValidator")
public class FareFinderValidator extends AbstractTravelValidator
{

	private static final Logger LOGGER = Logger.getLogger(FareFinderValidator.class);

	@Resource
	private CabinClassFacade cabinClassFacade;

	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource
	private TimeService timeService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	@Autowired
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "findVehicleFareFinderValidatorMap")
	private Map<String, Validator> findVehicleFareFinderValidatorMap;

	private static final String FIELD_RETURN_DATE_TIME = "returnDateTime";
	private static final String FIELD_TRIP_TYPE = "tripType";
	private static final String FIELD_DEPARTING_DATE_TIME = "departingDateTime";
	private static final String FIELD_PASSENGER_TYPE_QUANTITY_LIST = "passengerTypeQuantityList";
	private static final String FIELD_INVALID_NUMBER_OF_PASSENGERS = "InvalidNumberOfPassengers";

	private static final String ERROR_TYPE_INVALID_NUMBER_OF_ADULTS = "InvalidNumberOfAdults";

	private static final String ERROR_TYPE_NO_PASSENGER = "NoPassengerSelected";
	private static final String ERROR_TYPE_NOT_POPULATED = "NotPopulated";
	private static final String ERROR_TYPE_OUT_OF_RANGE = "OutOfRange";
	private static final String ERROR_TYPE_INVALID_TYPE = "InvalidType";
	private static final String ERROR_TYPE_CATEGORY_NOT_SELECTED = "error.ferry.farefinder.vehicleinfo.EmptyCategory";

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return BCFFareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFFareFinderForm bcfFareFinderForm = (BCFFareFinderForm) object;


		// first check if there is a valid trip type and cabin class set
		validateTripType(bcfFareFinderForm, errors);
		if (Objects.nonNull(bcfFareFinderForm.getVehicleInfoForm().getVehicleInfo()))
		{
			validateVehicleData(bcfFareFinderForm.getVehicleInfoForm(), errors);
		}

		// if no errors exist then proceed with the following validations
		if (!errors.hasErrors())
		{
			validateDates(bcfFareFinderForm, errors);
			validatePassengerTypeQuantity(bcfFareFinderForm, errors);
		}
	}

	/**
	 * Method checks if the requested Trip Type is a valid type and sets an INVALID_TYPE error if invalid.
	 *
	 * @param fareFinderForm
	 * @param errors
	 */
	protected void validateTripType(final BCFFareFinderForm fareFinderForm, final Errors errors)
	{
		if (StringUtils.isBlank(fareFinderForm.getRouteInfoForm().getTripType()))
		{
			validateBlankField(FIELD_TRIP_TYPE, fareFinderForm.getRouteInfoForm().getTripType(), errors);
		}
		else
		{
			try
			{
				TripType.valueOf(fareFinderForm.getRouteInfoForm().getTripType());
			}
			catch (final IllegalArgumentException iae)
			{
				LOGGER.debug("No Trip Type found with code " + fareFinderForm.getRouteInfoForm().getTripType());
				rejectValue(errors, FIELD_TRIP_TYPE, ERROR_TYPE_INVALID_TYPE);
			}
		}
	}

	/**
	 * Method responsible for running the following validation on Departing and Arriving dates
	 *
	 * <ul>
	 * <li><b>validateDateFormat</b>(String date, Errors errors, String field)</li>
	 * <li><b>validateDateIsPopulated</b>(String date, Errors errors, String field)</li>
	 * <li><b>validateDepartureAndArrivalDateTimeRange</b>(FareFinderForm fareFinderForm, Errors errors)</li>
	 * </ul>
	 *
	 * @param fareFinderForm
	 * @param errors
	 */
	protected void validateDates(final BCFFareFinderForm fareFinderForm, final Errors errors)
	{

		final boolean isDepartingDatePopulated = validateDateIsPopulated(fareFinderForm.getRouteInfoForm().getDepartingDateTime(),
				errors,
				FIELD_DEPARTING_DATE_TIME);

		boolean isDepartingDateCorrectlyFormatted = false;
		boolean isArrivalDateCorrectlyFormatted = false;

		if (isDepartingDatePopulated)
		{
			isDepartingDateCorrectlyFormatted = validateDateFormatFareFinder(
					fareFinderForm.getRouteInfoForm().getDepartingDateTime(), errors,
					FIELD_DEPARTING_DATE_TIME);

			if (isDepartingDateCorrectlyFormatted)
			{
				validateForPastDateFareFinder(fareFinderForm.getRouteInfoForm().getDepartingDateTime(), errors,
						FIELD_DEPARTING_DATE_TIME);
			}
		}

		if (StringUtils.equalsIgnoreCase(fareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name()))
		{

			final boolean isReturnDatePopulated = validateDateIsPopulated(fareFinderForm.getRouteInfoForm().getReturnDateTime(),
					errors,
					FIELD_RETURN_DATE_TIME);

			if (isReturnDatePopulated)
			{
				isArrivalDateCorrectlyFormatted = validateDateFormatFareFinder(fareFinderForm.getRouteInfoForm().getReturnDateTime(),
						errors,
						FIELD_RETURN_DATE_TIME);
			}
		}

		if (isDepartingDateCorrectlyFormatted && isArrivalDateCorrectlyFormatted)
		{
			validateDepartureAndArrivalDateTimeRange(fareFinderForm, errors);
		}
	}

	/**
	 * Method used to validate Departing and Arrival dates
	 *
	 * @param fareFinderForm
	 * @param errors
	 */
	protected void validateDepartureAndArrivalDateTimeRange(final BCFFareFinderForm fareFinderForm, final Errors errors)
	{

		if (StringUtils.equalsIgnoreCase(fareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name()))
		{
			try
			{
				final SimpleDateFormat formatter = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY);

				final Date departingDate = formatter.parse(fareFinderForm.getRouteInfoForm().getDepartingDateTime());
				final Date arrivalDate = formatter.parse(fareFinderForm.getRouteInfoForm().getReturnDateTime());

				if (departingDate.after(arrivalDate))
				{
					rejectValue(errors, FIELD_DEPARTING_DATE_TIME, ERROR_TYPE_OUT_OF_RANGE);
				}
			}
			catch (final ParseException e)
			{
				LOGGER.error("Unable to pharse String data to Date object:", e);
			}
		}
	}

	/**
	 * Method responsible for checking if date is populate
	 *
	 * @param date
	 * @param errors
	 * @param field
	 * @return
	 */
	protected boolean validateDateIsPopulated(final String date, final Errors errors, final String field)
	{
		if (StringUtils.isNotEmpty(date))
		{
			return true;
		}

		rejectValue(errors, field, ERROR_TYPE_NOT_POPULATED);

		return false;
	}

	protected boolean validateVehicleCode(final String code, final Errors errors, final String field, final int i)
	{
		if (StringUtils.isNotEmpty(code))
		{
			return true;
		}
		rejectValue(errors, "vehicleInfoForm.vehicleInfo[" + i + "].vehicleType.code", ERROR_TYPE_CATEGORY_NOT_SELECTED);
		return false;
	}


	/**
	 * Method responsible for validating Vehicles
	 *
	 * @param vehicleInfoForm
	 */
	public void validateVehicleData(final VehicleInfoForm vehicleInfoForm, final Errors errors)
	{
		boolean validCode = false;
		for (int i = 0; i < vehicleInfoForm.getVehicleInfo().size(); i++)
		{
			final String code = vehicleInfoForm.getVehicleInfo().get(i).getVehicleType().getCode();
			validCode = validateVehicleCode(code, errors, "code", i);
		}
		if (validCode)
		{
			final Map<String, List<VehicleTypeQuantityData>> vehiclesGroupedByCategory = new HashMap<>();
			vehicleInfoForm.getVehicleInfo().forEach(vehicleTypeQuantityData -> {
				List<VehicleTypeQuantityData> vehicleTypeQuantityDatas = vehiclesGroupedByCategory
						.get(vehicleTypeQuantityData.getVehicleType().getCategory());
				if (CollectionUtils.isEmpty(vehicleTypeQuantityDatas))
				{
					vehicleTypeQuantityDatas = new ArrayList<>();
					vehiclesGroupedByCategory.put(vehicleTypeQuantityData.getVehicleType().getCategory(), vehicleTypeQuantityDatas);
				}
				vehicleTypeQuantityDatas.add(vehicleTypeQuantityData);
			});

			vehiclesGroupedByCategory.forEach((category, vehicleTypeQuantityDatas) -> {
				findVehicleFareFinderValidatorMap
						.get(category.toLowerCase())
						.validate(vehicleTypeQuantityDatas, errors);
			});
		}
	}


	/**
	 * Method responsible for checking that at least one adult is present is there are any children or infants traveling
	 *
	 * @param fareFinderForm
	 */
	public void validatePassengerTypeQuantity(final BCFFareFinderForm fareFinderForm, final Errors errors)
	{

		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))
		{
			return;
		}

		if (fareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList() == null)
		{
			return;
		}

		int adultPassengers = 0;
		int nonAdultPassengers = 0;

		for (final PassengerTypeQuantityData passengerTypeQuantity : fareFinderForm.getPassengerInfoForm()
				.getPassengerTypeQuantityList())
		{
			if (!passengerTypeQuantity.getPassengerType().getCode()
					.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_ONLY))
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, "Invalid Passenger Type");
				continue;
			}

			if (bcfFerrySelectionUtil.isAdult(passengerTypeQuantity.getPassengerType().getCode()))
			{
				if (passengerTypeQuantity.getQuantity() < 0)
				{
					rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST,
							"InvalidQuantity" + passengerTypeQuantity.getPassengerType().getCode());
				}
				adultPassengers += passengerTypeQuantity.getQuantity();
			}
			else
			{
				if (passengerTypeQuantity.getQuantity() < 0)
				{
					rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST,
							"InvalidQuantity" + passengerTypeQuantity.getPassengerType().getCode());
				}
				nonAdultPassengers += passengerTypeQuantity.getQuantity();
			}
		}
		validatePassengerCount(adultPassengers, nonAdultPassengers, errors);
	}

	private void validatePassengerCount(final int adultPassengers, final int nonAdultPassengers, final Errors errors)
	{
		if (nonAdultPassengers >= 0 && adultPassengers >= 0)
		{
			if (nonAdultPassengers > 0 && adultPassengers == 0)
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, ERROR_TYPE_INVALID_NUMBER_OF_ADULTS);
			}

			if (nonAdultPassengers + adultPassengers == 0)
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, ERROR_TYPE_NO_PASSENGER);
			}

			validateForMaxPassengerAllowed(adultPassengers, nonAdultPassengers, errors);
		}
	}


	private void validateForMaxPassengerAllowed(final int adultPassengers, final int nonAdultPassengers, final Errors errors)
	{
		final String maxPassengerAllowedStr = getBcfConfigurablePropertiesService().getBcfPropertyValue("maxPassengerAllowed");
		Integer maxPassengerAllowed = 10;
		if (StringUtils.isNotEmpty(maxPassengerAllowedStr) && NumberUtils.isNumber(maxPassengerAllowedStr))
		{
			maxPassengerAllowed = Integer.parseInt(maxPassengerAllowedStr);
		}
		final int numberOfGuests = adultPassengers + nonAdultPassengers;
		if (numberOfGuests > maxPassengerAllowed)
		{
			final Object[] args = new Object[] { maxPassengerAllowed };
			rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, FIELD_INVALID_NUMBER_OF_PASSENGERS, args);
		}


	}

	public Map<String, Validator> getFindVehicleFareFinderValidatorMap()
	{
		return findVehicleFareFinderValidatorMap;
	}

	public void setFindVehicleFareFinderValidatorMap(final Map<String, Validator> findVehicleFareFinderValidatorMap)
	{
		this.findVehicleFareFinderValidatorMap = findVehicleFareFinderValidatorMap;
	}
}
