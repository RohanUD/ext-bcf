/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.enums.AccountType;
import com.bcf.core.model.traveller.VehicleTypeModel;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


@Component("bcfStandardVehicleFareFinderValidator")
public class BcfStandardVehicleFareFinderValidator extends VehicleInfoValidator implements Validator
{
	public static final String OVER7HTWITHLIVESTOCK = "OSL";
	public static final String OVER7HT = "OS";
	public static final String UNDERHEIGHT = "UH";
	protected static final String ERROR_SELECT_UNDER20_VEHICLE = "selectUnder20Vehicle";
	protected static final String ERROR_SELECT_UNDER7_VEHICLE = "selectUnder7Vehicle";
	protected static final String LENGTH_ERROR_FIELD = "standardVehicleLengthError";
	protected static final String HEIGHT_ERROR_FIELD = "standardVehicleHeightError";

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return FareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityData = (List<VehicleTypeQuantityData>) object;

		for (int i = 0; i < vehicleTypeQuantityData.size(); i++)
		{
			if (Objects.nonNull(vehicleTypeQuantityData.get(i).getVehicleType()) &&
					StringUtils.isNotEmpty(vehicleTypeQuantityData.get(i).getVehicleType().getCategory()))
			{
				final TravelRouteModel travelRouteModel = getTravelRoute(vehicleTypeQuantityData.get(i).getTravelRouteCode());
				final TransportFacilityModel originTransportFacilityData = getTransportFacility(
						travelRouteModel.getOrigin().getCode());
				final TransportFacilityModel destinationTransportFacilityData = getTransportFacility(
						travelRouteModel.getDestination().getCode());

				if (Objects.nonNull(originTransportFacilityData) && Objects.nonNull(destinationTransportFacilityData))
				{
					validateStandardVehicle(originTransportFacilityData, destinationTransportFacilityData, vehicleTypeQuantityData, i,
							VEHICLE_INFO, errors);
				}
			}
		}
	}

	protected void validateStandardVehicle(final TransportFacilityModel originData, final TransportFacilityModel destinationData,
			final List<VehicleTypeQuantityData> vehicleTypeQuantityData,
			final int i, final String attributeName, final Errors errors)
	{
		switch (vehicleTypeQuantityData.get(i).getVehicleType().getCode())
		{
			case OVER7HTWITHLIVESTOCK:
				validateOver7Ht(vehicleTypeQuantityData.get(i), originData, destinationData, i, attributeName, errors);
				break;
			case OVER7HT:
				validateOver7Ht(vehicleTypeQuantityData.get(i), originData, destinationData, i, attributeName, errors);
				break;
			case UNDERHEIGHT:
				validateUnder7Ht(vehicleTypeQuantityData.get(i), originData, destinationData, i, attributeName, errors);
				break;
			default:
				break;
		}
	}

	private boolean validateBlankLength(final VehicleTypeQuantityData vehicleTypeQuantityData,
			final int i,
			final Errors errors)
	{
		final boolean isNotBlankHeight = validateBlankField(VEHICLE_INFO + i + "]." + HEIGHT_ERROR_FIELD,
				vehicleTypeQuantityData.getHeight() <= 0 ? null
						: String.valueOf(vehicleTypeQuantityData.getHeight()),
				errors, ERROR_TYPE_EMPTY_HEIGHT_FIELD);

		return isNotBlankHeight;
	}

	private boolean validateOver7Ht(final VehicleTypeQuantityData vehicleTypeQuantityData,
			final TransportFacilityModel originData,
			final TransportFacilityModel destinationData,
			final int i, final String attributeName,
			final Errors errors)
	{
		final boolean isNotBlank = validateBlankLength(vehicleTypeQuantityData, i, errors);

		if (!isNotBlank)
		{
			return false;
		}

		final VehicleTypeModel vehicleTypeModel = fetchVehicleTypeModel(vehicleTypeQuantityData);
		final Double minHeight = vehicleTypeModel.getMinHeight();
		final boolean isAnonymous = isAnonymousUser();
		final boolean notValidLength = validateLength(vehicleTypeQuantityData, originData, destinationData, i, attributeName,
				vehicleTypeModel,
				errors);

		if (!notValidLength)
		{
			return false;
		}

		if (vehicleTypeQuantityData.getHeight() != 0 && vehicleTypeQuantityData.getHeight() <= minHeight)
		{
			rejectValue(errors, attributeName + i + "]." + HEIGHT_ERROR_FIELD, ERROR_SELECT_UNDER7_VEHICLE);
			return Boolean.FALSE;
		}

		if (vehicleTypeQuantityData.getHeight() != 0 && vehicleTypeQuantityData.getHeight() > minHeight)
		{
			final int heightThreshold = Math
					.min(originData.getStandardVehicleHeight(), destinationData.getStandardVehicleHeight());

			if (vehicleTypeQuantityData.getHeight() > heightThreshold && !AccountType.FULL_ACCOUNT
					.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()))
			{
				rejectValue(errors, attributeName + i + "]." + HEIGHT_ERROR_FIELD, ERROR_EXCEEDING_PORT_CONFIG);
				return Boolean.FALSE;
			}
		}
		return isNotBlank;
	}

	private boolean validateUnder7Ht(final VehicleTypeQuantityData vehicleTypeQuantityData,
			final TransportFacilityModel originData,
			final TransportFacilityModel destinationData,
			final int i, final String attributeName,
			final Errors errors)
	{
		final VehicleTypeModel vehicleTypeModel = fetchVehicleTypeModel(vehicleTypeQuantityData);
		return validateLength(vehicleTypeQuantityData, originData, destinationData, i, attributeName, vehicleTypeModel, errors);
	}

	private boolean validateLength(final VehicleTypeQuantityData vehicleTypeQuantityData,
			final TransportFacilityModel originData,
			final TransportFacilityModel destinationData,
			final int i, final String attributeName,
			final VehicleTypeModel vehicleTypeModel,
			final Errors errors)
	{
		final Double minLength = vehicleTypeModel.getMinLength();
		final boolean isAnonymous = isAnonymousUser();

		if (vehicleTypeQuantityData.getLength() != 0)
		{
			if (vehicleTypeQuantityData.getLength() <= minLength)
			{
				rejectValue(errors, attributeName + i + "]." + LENGTH_ERROR_FIELD, ERROR_SELECT_UNDER20_VEHICLE);
				return Boolean.FALSE;
			}

			if (vehicleTypeQuantityData.getLength() > minLength)
			{
				final int lengthThreshold = Math
						.min(Objects.nonNull(originData.getStandardVehicleLength()) ? originData.getStandardVehicleLength() : 0,
								Objects.nonNull(destinationData.getStandardVehicleLength()) ?
										destinationData.getStandardVehicleLength() :
										0);
				if (vehicleTypeQuantityData.getLength() > lengthThreshold && !AccountType.FULL_ACCOUNT
						.equals(((CustomerModel) getUserService().getCurrentUser()).getAccountType()))
				{
					rejectValue(errors, attributeName + i + "]." + LENGTH_ERROR_FIELD, ERROR_EXCEEDING_PORT_CONFIG);
					return Boolean.FALSE;
				}
			}
		}
		return true;
	}
}
