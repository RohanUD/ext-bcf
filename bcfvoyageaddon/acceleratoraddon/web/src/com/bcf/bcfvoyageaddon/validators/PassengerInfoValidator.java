/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.bcfvoyageaddon.util.BcfFerrySelectionUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * Custom validation to validate Passenger Info Form attributes which can't be validated using JSR303
 */

@Component("passengerInfoValidator")
public class PassengerInfoValidator extends AbstractTravelValidator
{

	@Autowired
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfFerrySelectionUtil")
	private BcfFerrySelectionUtil bcfFerrySelectionUtil;

	private static final String FIELD_PASSENGER_TYPE_QUANTITY_LIST = "passengerInfoForm.passengerTypeQuantityList";
	private static final String FIELD_INVALID_NUMBER_OF_PASSENGERS = "InvalidNumberOfPassengers";
	private static final String ERROR_INVALID_QUANTITY = "InvalidQuantity";
	private static final String ERROR_TYPE_INVALID_NUMBER_OF_ADULTS = "InvalidNumberOfAdults";
	private static final String ERROR_TYPE_NO_PASSENGER = "NoPassengerSelected";
	private static final String ERROR_PREFIX = "error.ferry.farefinder.passengerinfo";

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return BCFFareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFFareFinderForm bcfFareFinderForm = (BCFFareFinderForm) object;
		validatePassengerTypeQuantity(bcfFareFinderForm.getPassengerInfoForm().getPassengerTypeQuantityList(), errors);
		if (BooleanUtils.isTrue(bcfFareFinderForm.getPassengerInfoForm().getReturnWithDifferentPassenger()))
		{
			validatePassengerTypeQuantity(bcfFareFinderForm.getPassengerInfoForm().getInboundPassengerTypeQuantityList(), errors);
		}
	}

	@Override
	protected void rejectValue(final Errors errors, String attributeName, final String errorCode)
	{
		final String errorMessageCode = ERROR_PREFIX + "." + errorCode;
		if (StringUtils.isNotEmpty(getAttributePrefix()))
		{
			attributeName = getAttributePrefix() + "." + attributeName;
		}
		errors.rejectValue(attributeName, errorMessageCode);
	}

	/**
	 * Method responsible for checking that at least one adult is present is there are any children or infants traveling
	 *
	 * @param passengerTypeQuantityList
	 */
	private void validatePassengerTypeQuantity(final List<PassengerTypeQuantityData> passengerTypeQuantityList,
			final Errors errors)
	{
		final String sessionBookingJourney = sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))
		{
			return;
		}

		if (passengerTypeQuantityList == null)
		{
			return;
		}

		int adultPassengers = 0;
		int nonAdultPassengers = 0;

		for (final PassengerTypeQuantityData passengerTypeQuantity : passengerTypeQuantityList)
		{
			if (!passengerTypeQuantity.getPassengerType().getCode()
					.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_ONLY))
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, FIELD_INVALID_NUMBER_OF_PASSENGERS);
				continue;
			}

			if (bcfFerrySelectionUtil.isAdult(passengerTypeQuantity.getPassengerType().getCode()))
			{
				if (passengerTypeQuantity.getQuantity() < 0)
				{
					rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST,
							ERROR_INVALID_QUANTITY + passengerTypeQuantity.getPassengerType().getCode());
				}
				adultPassengers += passengerTypeQuantity.getQuantity();
			}
			else
			{
				if (passengerTypeQuantity.getQuantity() < 0)
				{
					rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST,
							ERROR_INVALID_QUANTITY + passengerTypeQuantity.getPassengerType().getCode());
				}
				nonAdultPassengers += passengerTypeQuantity.getQuantity();
			}
		}
		validatePassengerCount(adultPassengers, nonAdultPassengers, errors);
	}

	private void validatePassengerCount(final int adultPassengers, final int nonAdultPassengers, final Errors errors)
	{
		if (nonAdultPassengers >= 0 && adultPassengers >= 0)
		{
			if (nonAdultPassengers > 0 && adultPassengers == 0)
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, ERROR_TYPE_INVALID_NUMBER_OF_ADULTS);
			}

			if (nonAdultPassengers + adultPassengers == 0)
			{
				rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, ERROR_TYPE_NO_PASSENGER);
			}

			validateForMaxPassengerAllowed(adultPassengers, nonAdultPassengers, errors);
		}
	}

	private void validateForMaxPassengerAllowed(final int adultPassengers, final int nonAdultPassengers, final Errors errors)
	{
		final String maxPassengersAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		final int maxPassengerQuantity = StringUtils.isNotEmpty(maxPassengersAllowed) ? Integer.parseInt(maxPassengersAllowed) : 9;
		final int numberOfGuests = adultPassengers + nonAdultPassengers;
		if (numberOfGuests > maxPassengerQuantity)
		{
			final Object[] args = new Object[] { maxPassengerQuantity };
			rejectValue(errors, FIELD_PASSENGER_TYPE_QUANTITY_LIST, FIELD_INVALID_NUMBER_OF_PASSENGERS, args);
		}
	}

}
