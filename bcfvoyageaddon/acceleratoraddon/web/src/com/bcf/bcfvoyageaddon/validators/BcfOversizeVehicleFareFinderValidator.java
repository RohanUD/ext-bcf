/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.travel.TravelRouteModel;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.core.enums.VehicleCategoryType;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


@Component("bcfOversizeVehicleFareFinderValidator")
public class BcfOversizeVehicleFareFinderValidator extends VehicleInfoValidator implements Validator
{
	private static final String ERROR_TYPE_EMPTY_AXLE_FIELD = "emptyAxleField";
	private static final String ERROR_TYPE_EMPTY_WEIGHT_FIELD = "emptyWeightField";
	private static final String NUMBEROFAXIS = "numberOfAxis";
	private static final String LENGTH_ERROR_FIELD = "oversizevehicleLengthError";
	private static final String HEIGHT_ERROR_FIELD = "oversizeVehicleHeightError";

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return BcfOversizeVehicleFareFinderValidator.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityData = (List<VehicleTypeQuantityData>) object;
		validateVehicleType(vehicleTypeQuantityData, errors);
	}

	private void validateVehicleType(final List<VehicleTypeQuantityData> vehicleTypeQuantityData, final Errors errors)
	{
		for (int i = 0; i < vehicleTypeQuantityData.size(); i++)
		{
			if ((Objects.nonNull(vehicleTypeQuantityData.get(i).getVehicleType())
					&& StringUtils.isNotEmpty(vehicleTypeQuantityData.get(i).getVehicleType().getCategory()))
					&& (vehicleTypeQuantityData.get(i).getVehicleType().getCategory().toUpperCase()
					.equalsIgnoreCase(VehicleCategoryType.OVERSIZE.getCode())))
			{
				final boolean isNotBlank = validateBlankFieldForOversizeVehicle(vehicleTypeQuantityData, i, errors);
				if (isNotBlank)
				{
					final TravelRouteModel travelRouteModel = getTravelRoute(vehicleTypeQuantityData.get(i).getTravelRouteCode());
					final TransportFacilityModel originTransportFacilityData = getTransportFacility(
							travelRouteModel.getOrigin().getCode());
					final TransportFacilityModel destinationTransportFacilityData = getTransportFacility(
							travelRouteModel.getDestination().getCode());
					if (Objects.nonNull(originTransportFacilityData) && Objects.nonNull(destinationTransportFacilityData))
					{
						validateOversizeVehicle(originTransportFacilityData, destinationTransportFacilityData,
								vehicleTypeQuantityData,
								i, errors);
					}
				}
			}

		}
	}

	private boolean validateBlankFieldForOversizeVehicle(final List<VehicleTypeQuantityData> vehicleTypeQuantityData,
			final int i, final Errors errors)
	{
		final boolean lengthNotBlank = validateBlankField(VEHICLE_INFO + i + "]." + LENGTH_ERROR_FIELD,
				vehicleTypeQuantityData.get(i).getLength() <= 0 ? null : String.valueOf(vehicleTypeQuantityData.get(i).getLength()),
				errors, ERROR_TYPE_EMPTY_LENGTH_FIELD);
		final boolean heightNotBlank = validateBlankField(VEHICLE_INFO + i + "]." + HEIGHT_ERROR_FIELD,
				vehicleTypeQuantityData.get(i).getHeight() <= 0 ? null : String.valueOf(vehicleTypeQuantityData.get(i).getHeight()),
				errors, ERROR_TYPE_EMPTY_HEIGHT_FIELD);
		if (Objects.nonNull(vehicleTypeQuantityData.get(i).getOver9FtWide()) && vehicleTypeQuantityData.get(i).getOver9FtWide())
		{
			final boolean axisNotBlank = validateBlankField(VEHICLE_INFO + i + "]." + NUMBEROFAXIS,
					vehicleTypeQuantityData.get(i).getNumberOfAxis() <= 0 ?
							null : String.valueOf(vehicleTypeQuantityData.get(i).getNumberOfAxis()),
					errors, ERROR_TYPE_EMPTY_AXLE_FIELD);
			final boolean weightNotBlank = validateBlankField(VEHICLE_INFO + i + "]." + WEIGHT,
					vehicleTypeQuantityData.get(i).getWeight() <= 0 ?
							null :
							String.valueOf(vehicleTypeQuantityData.get(i).getWeight()),
					errors, ERROR_TYPE_EMPTY_WEIGHT_FIELD);
			return lengthNotBlank && heightNotBlank && axisNotBlank && weightNotBlank;
		}
		return lengthNotBlank && heightNotBlank;
	}

	private boolean validateOversizeVehicle(final TransportFacilityModel originData, final TransportFacilityModel destinationData,
			final List<VehicleTypeQuantityData> vehicleTypeQuantityData,
			final int i, final Errors errors)
	{
		final int lengthThreshold = Math.min(originData.getOver5500VehicleLength(), destinationData.getOver5500VehicleLength());
		final int heightThreshold = Math.min(originData.getOver5500VehicleHeight(), destinationData.getOver5500VehicleHeight());
		final boolean isAnonymous = isAnonymousUser();
		if (isAnonymous)
		{
			if (vehicleTypeQuantityData.get(i).getLength() > lengthThreshold)
			{
				rejectValue(errors, VEHICLE_INFO + i + "]." + LENGTH_ERROR_FIELD, ERROR_NOT_VALID_LENGTH);
				return Boolean.FALSE;
			}
			if (vehicleTypeQuantityData.get(i).getHeight() > heightThreshold)
			{
				rejectValue(errors, VEHICLE_INFO + i + "]." + HEIGHT_ERROR_FIELD, ERROR_NOT_VALID_HEIGHT);
				return Boolean.FALSE;
			}
		}
		return true;
	}
}
