/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.validators;

import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.bcfvoyageaddon.constants.BcfvoyageaddonWebConstants;
import com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;


/**
 * Custom validation to validate Route Info Form attributes which can't be validated using JSR303
 */

@Component("routeInfoValidator")
public class RouteInfoValidator extends AbstractTravelValidator
{

	private static final Logger LOGGER = Logger.getLogger(RouteInfoValidator.class);

	private static final String MAX_ALLOWED = "PassengerInfoForm";
	private static final String ERROR_TYPE_MAX_ALLOWED = "MoreThanMaxAllowed";
	private static final String FIELD_RETURN_DATE_TIME = "routeInfoForm.returnDateTime";
	private static final String FIELD_TRIP_TYPE = "routeInfoForm.tripType";
	private static final String FIELD_DEPARTING_DATE_TIME = "routeInfoForm.departingDateTime";
	private static final String FIELD_DEPARTING_LOCATION = "routeInfoForm.departureLocation";
	private static final String FIELD_ARRIVAL_LOCATION = "routeInfoForm.arrivalLocation";
	private static final String ERROR_TYPE_NOT_POPULATED = "routeInfoForm.NotPopulated";
	private static final String ERROR_TYPE_OUT_OF_RANGE = "routeInfoForm.OutOfRange";
	private static final String ERROR_TYPE_INVALID_TYPE = "routeInfoForm.InvalidType";

	@Resource
	private CartService cartService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfCartService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return BCFFareFinderForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFFareFinderForm bcfFareFinderForm = (BCFFareFinderForm) object;

		if (StringUtils.isEmpty(bcfFareFinderForm.getRouteInfoForm().getDepartureLocation()))
		{
			rejectValue(errors, FIELD_DEPARTING_LOCATION, ERROR_TYPE_NOT_POPULATED);
		}
		if (StringUtils.isEmpty(bcfFareFinderForm.getRouteInfoForm().getArrivalLocation()))
		{
			rejectValue(errors, FIELD_ARRIVAL_LOCATION, ERROR_TYPE_NOT_POPULATED);
		}
		// first check if there is a valid trip type and cabin class set
		validateTripType(bcfFareFinderForm, errors);
		if (!bcfFareFinderForm.isSkipMaxAllowedValidation())
		{
			maxSailingAllowed(errors, bcfFareFinderForm.getRouteInfoForm().getTripType());
		}
		// if no errors exist then proceed with the following validations
		if (!errors.hasErrors())
		{
			validateDates(bcfFareFinderForm, errors);
		}
	}

	private void maxSailingAllowed(final Errors errors, final String tripType)
	{
		final AbstractOrderModel abstractOrderModel = getCartService().getSessionCart();
		final List<AbstractOrderEntryModel> entries = abstractOrderModel.getEntries()
				.stream().filter(entry -> entry.getActive() && Objects.isNull(entry.getEntryGroup()) && (
						StringUtils.isNotBlank(entry.getCacheKey()) || StringUtils.isNotBlank(entry.getBookingReference())))
				.collect(Collectors.toList());

		Map<String, List<AbstractOrderEntryModel>> entriesByCacheKeyOrBookingRef = new HashMap<>();

		final List<AbstractOrderEntryModel> bookingRefEntries = StreamUtil.safeStream(entries)
				.filter(entry -> entry.getBookingReference() != null).collect(Collectors.toList());
		final List<AbstractOrderEntryModel> cacheKeyRefEntries = StreamUtil.safeStream(entries)
				.filter(entry -> entry.getCacheKey() != null).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(cacheKeyRefEntries))
		{
			entriesByCacheKeyOrBookingRef = StreamUtil.safeStream(cacheKeyRefEntries)
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getCacheKey));
		}
		else if (CollectionUtils.isNotEmpty(bookingRefEntries))
		{
			entriesByCacheKeyOrBookingRef = StreamUtil.safeStream(bookingRefEntries)
					.collect(Collectors.groupingBy(AbstractOrderEntryModel::getBookingReference));
		}
		final Integer journeyRefNum = sessionService.getAttribute(BcfvoyageaddonWebConstants.JOURNEY_REF_NUMBER);
		final int sizeOfCart = entriesByCacheKeyOrBookingRef.size();
		final int tempEntriesToAdd = tripType.equals(String.valueOf(TripType.RETURN)) ? 2 : 1;
		final int maxAllowed = cartFacade.getMaxSailingAllowed();
		if ((Objects.nonNull(journeyRefNum) && journeyRefNum > cartFacade.getCurrentJourneyRefNumber()) && (
				sizeOfCart + tempEntriesToAdd > maxAllowed))
		{
			rejectValue(errors, MAX_ALLOWED, ERROR_TYPE_MAX_ALLOWED);
		}

	}

	/**
	 * Method checks if the requested Trip Type is a valid type and sets an INVALID_TYPE error if invalid.
	 *
	 * @param bcfFareFinderForm
	 * @param errors
	 */
	protected void validateTripType(final BCFFareFinderForm bcfFareFinderForm, final Errors errors)
	{
		if (StringUtils.isBlank(bcfFareFinderForm.getRouteInfoForm().getTripType()))
		{
			validateBlankField(FIELD_TRIP_TYPE, bcfFareFinderForm.getRouteInfoForm().getTripType(), errors);
		}
		else
		{
			try
			{
				TripType.valueOf(bcfFareFinderForm.getRouteInfoForm().getTripType());
			}
			catch (final IllegalArgumentException iae)
			{
				LOGGER.debug("No Trip Type found with code " + bcfFareFinderForm.getRouteInfoForm().getTripType());
				rejectValue(errors, FIELD_TRIP_TYPE, ERROR_TYPE_INVALID_TYPE);
			}
		}
	}

	/**
	 * Method responsible for running the following validation on Departing and Arriving dates
	 *
	 * <ul>
	 * <li><b>validateDateFormat</b>(String date, Errors errors, String field)</li>
	 * <li><b>validateDateIsPopulated</b>(String date, Errors errors, String field)</li>
	 * <li><b>validateDepartureAndArrivalDateTimeRange</b>(FareFinderForm fareFinderForm, Errors errors)</li>
	 * </ul>
	 *
	 * @param bcfFareFinderForm
	 * @param errors
	 */
	private void validateDates(final BCFFareFinderForm bcfFareFinderForm, final Errors errors)
	{

		final boolean isDepartingDatePopulated = validateDateIsPopulated(
				bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(),
				errors,
				FIELD_DEPARTING_DATE_TIME);

		boolean isDepartingDateCorrectlyFormatted = false;
		boolean isArrivalDateCorrectlyFormatted = false;

		if (isDepartingDatePopulated)
		{
			isDepartingDateCorrectlyFormatted = validateDateFormatFareFinder(
					bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(), errors,
					FIELD_DEPARTING_DATE_TIME);

			if (isDepartingDateCorrectlyFormatted)
			{
				validateForPastDateFareFinder(bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime(), errors,
						FIELD_DEPARTING_DATE_TIME);
			}
		}

		if (StringUtils.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name()))
		{

			final boolean isReturnDatePopulated = validateDateIsPopulated(bcfFareFinderForm.getRouteInfoForm().getReturnDateTime(),
					errors,
					FIELD_RETURN_DATE_TIME);

			if (isReturnDatePopulated)
			{
				isArrivalDateCorrectlyFormatted = validateDateFormatFareFinder(
						bcfFareFinderForm.getRouteInfoForm().getReturnDateTime(),
						errors,
						FIELD_RETURN_DATE_TIME);
			}
		}

		if (isDepartingDateCorrectlyFormatted && isArrivalDateCorrectlyFormatted)
		{
			validateDepartureAndArrivalDateTimeRange(bcfFareFinderForm, errors);
		}
	}

	/**
	 * Method used to validate Departing and Arrival dates
	 *
	 * @param bcfFareFinderForm
	 * @param errors
	 */
	private void validateDepartureAndArrivalDateTimeRange(final BCFFareFinderForm bcfFareFinderForm, final Errors errors)
	{

		if (StringUtils.equalsIgnoreCase(bcfFareFinderForm.getRouteInfoForm().getTripType(), TripType.RETURN.name()))
		{
			try
			{
				final SimpleDateFormat formatter = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

				final Date departingDate = formatter.parse(bcfFareFinderForm.getRouteInfoForm().getDepartingDateTime());
				final Date arrivalDate = formatter.parse(bcfFareFinderForm.getRouteInfoForm().getReturnDateTime());

				if (departingDate.after(arrivalDate))
				{
					rejectValue(errors, FIELD_DEPARTING_DATE_TIME, ERROR_TYPE_OUT_OF_RANGE);
				}
			}
			catch (final ParseException e)
			{
				LOGGER.error("Unable to pharse String data to Date object:", e);
			}
		}
	}

	/**
	 * Method responsible for checking if date is populate
	 *
	 * @param date
	 * @param errors
	 * @param field
	 * @return
	 */
	private boolean validateDateIsPopulated(final String date, final Errors errors, final String field)
	{
		if (StringUtils.isNotEmpty(date))
		{
			return true;
		}

		rejectValue(errors, field, ERROR_TYPE_NOT_POPULATED);

		return false;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

}
