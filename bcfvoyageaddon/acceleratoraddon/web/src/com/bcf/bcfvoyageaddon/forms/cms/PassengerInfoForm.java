/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.forms.cms;

import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import com.bcf.facades.ferry.ExtraBaggageData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;


public class PassengerInfoForm
{

	private List<PassengerTypeQuantityData> passengerTypeQuantityList;
	private Boolean returnWithDifferentPassenger;
	private List<PassengerTypeQuantityData> inboundPassengerTypeQuantityList;
	private boolean travellingWithChildren;
	private List<AccessibilityRequestData> accessibilityRequestDataList;
	private List<AccessibilityRequestData> inboundaccessibilityRequestDataList;
	private boolean travellingAsWalkOn;
	private boolean accessibilityNeedsCheckOutbound;
	private boolean accessibilityNeedsCheckInbound;
	private boolean largeItemsCheckOutbound;
	private boolean largeItemsCheckInbound;
	private List<LargeItemData> largeItems;
	private List<LargeItemData> largeItemsInbound;
	private boolean carryingDangerousGoodsInOutbound;
	private boolean carryingDangerousGoodsInReturn;
	private List<String> basePassengers;
	private List<String> additionalPassengers;
	private List<String> northernPassengers;
	private int maxPassengersAllowed;
	private ExtraBaggageData extraBaggageOutbound;
	private ExtraBaggageData extraBaggageInbound;
	private List<Pair<String, String>> passengerTypeAndUidsToBeRemoved;



	public boolean isCarryingDangerousGoodsInOutbound()
	{
		return carryingDangerousGoodsInOutbound;
	}

	public List<LargeItemData> getLargeItemsInbound()
	{
		return largeItemsInbound;
	}

	public void setLargeItemsInbound(final List<LargeItemData> largeItemsInbound)
	{
		this.largeItemsInbound = largeItemsInbound;
	}

	public void setCarryingDangerousGoodsInOutbound(final boolean carryingDangerousGoodsInOutbound)
	{
		this.carryingDangerousGoodsInOutbound = carryingDangerousGoodsInOutbound;
	}

	public boolean isCarryingDangerousGoodsInReturn()
	{
		return carryingDangerousGoodsInReturn;
	}

	public void setCarryingDangerousGoodsInReturn(final boolean carryingDangerousGoodsInReturn)
	{
		this.carryingDangerousGoodsInReturn = carryingDangerousGoodsInReturn;
	}

	public boolean isLargeItemsCheckOutbound()
	{
		return largeItemsCheckOutbound;
	}

	public void setLargeItemsCheckOutbound(final boolean largeItemsCheckOutbound)
	{
		this.largeItemsCheckOutbound = largeItemsCheckOutbound;
	}

	public boolean isLargeItemsCheckInbound()
	{
		return largeItemsCheckInbound;
	}

	public void setLargeItemsCheckInbound(final boolean largeItemsCheckInbound)
	{
		this.largeItemsCheckInbound = largeItemsCheckInbound;
	}

	public List<LargeItemData> getLargeItems()
	{
		return largeItems;
	}

	public void setLargeItems(final List<LargeItemData> largeItems)
	{
		this.largeItems = largeItems;
	}

	public List<AccessibilityRequestData> getInboundaccessibilityRequestDataList()
	{
		return inboundaccessibilityRequestDataList;
	}

	public void setInboundaccessibilityRequestDataList(
			final List<AccessibilityRequestData> inboundaccessibilityRequestDataList)
	{
		this.inboundaccessibilityRequestDataList = inboundaccessibilityRequestDataList;
	}

	public List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		return passengerTypeQuantityList;
	}

	public void setPassengerTypeQuantityList(final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		this.passengerTypeQuantityList = passengerTypeQuantityList;
	}

	public boolean getTravellingWithChildren()
	{
		return travellingWithChildren;
	}

	public void setTravellingWithChildren(final boolean travellingWithChildren)
	{
		this.travellingWithChildren = travellingWithChildren;
	}

	public List<AccessibilityRequestData> getAccessibilityRequestDataList()
	{
		return accessibilityRequestDataList;
	}

	public void setAccessibilityRequestDataList(
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		this.accessibilityRequestDataList = accessibilityRequestDataList;
	}

	public Boolean getReturnWithDifferentPassenger()
	{
		return returnWithDifferentPassenger;
	}

	public void setReturnWithDifferentPassenger(final Boolean returnWithDifferentPassenger)
	{
		this.returnWithDifferentPassenger = returnWithDifferentPassenger;
	}

	public List<PassengerTypeQuantityData> getInboundPassengerTypeQuantityList()
	{
		return inboundPassengerTypeQuantityList;
	}

	public void setInboundPassengerTypeQuantityList(final List<PassengerTypeQuantityData> inboundPassengerTypeQuantityList)
	{
		this.inboundPassengerTypeQuantityList = inboundPassengerTypeQuantityList;
	}

	public boolean isTravellingAsWalkOn()
	{
		return travellingAsWalkOn;
	}

	public void setTravellingAsWalkOn(final boolean travellingAsWalkOn)
	{
		this.travellingAsWalkOn = travellingAsWalkOn;
	}

	public boolean isAccessibilityNeedsCheckOutbound()
	{
		return accessibilityNeedsCheckOutbound;
	}

	public void setAccessibilityNeedsCheckOutbound(final boolean accessibilityNeedsCheckOutbound)
	{
		this.accessibilityNeedsCheckOutbound = accessibilityNeedsCheckOutbound;
	}

	public boolean isAccessibilityNeedsCheckInbound()
	{
		return accessibilityNeedsCheckInbound;
	}

	public void setAccessibilityNeedsCheckInbound(final boolean accessibilityNeedsCheckInbound)
	{
		this.accessibilityNeedsCheckInbound = accessibilityNeedsCheckInbound;
	}

	public List<String> getBasePassengers()
	{
		return basePassengers;
	}

	public void setBasePassengers(final List<String> basePassengers)
	{
		this.basePassengers = basePassengers;
	}

	public List<String> getAdditionalPassengers()
	{
		return additionalPassengers;
	}

	public void setAdditionalPassengers(final List<String> additionalPassengers)
	{
		this.additionalPassengers = additionalPassengers;
	}

	public List<String> getNorthernPassengers()
	{
		return northernPassengers;
	}

	public void setNorthernPassengers(final List<String> northernPassengers)
	{
		this.northernPassengers = northernPassengers;
	}

	public int getMaxPassengersAllowed()
	{
		return maxPassengersAllowed;
	}

	public void setMaxPassengersAllowed(final int maxPassengersAllowed)
	{
		this.maxPassengersAllowed = maxPassengersAllowed;
	}

	public ExtraBaggageData getExtraBaggageOutbound()
	{
		return extraBaggageOutbound;
	}

	public void setExtraBaggageOutbound(final ExtraBaggageData extraBaggageOutbound)
	{
		this.extraBaggageOutbound = extraBaggageOutbound;
	}

	public ExtraBaggageData getExtraBaggageInbound()
	{
		return extraBaggageInbound;
	}

	public void setExtraBaggageInbound(final ExtraBaggageData extraBaggageInbound)
	{
		this.extraBaggageInbound = extraBaggageInbound;
	}

	public List<Pair<String, String>> getPassengerTypeAndUidsToBeRemoved()
	{
		return passengerTypeAndUidsToBeRemoved;
	}

	public void setPassengerTypeAndUidsToBeRemoved(
			final List<Pair<String, String>> passengerTypeAndUidsToBeRemoved)
	{
		this.passengerTypeAndUidsToBeRemoved = passengerTypeAndUidsToBeRemoved;
	}
}
