/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms.validation;


import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.facades.travel.data.BCFTravellerDataPerJourney;
import com.bcf.facades.travel.data.BCFTravellersData;


@Component("bcfTravellerFormValidator")
public class BCFTravellerFormValidator extends TravellerFormValidator
{
	private static final String FIRST_TRAVELLER_FORM = "0";
	private static final String TRAVELLER_FORMS = "travellerData";
	private static final String SUPER_FORM = "travellerDataPerJourney";
	private static final String FIELD_TITLE = "travellerInfo.title.code";
	private static final String FIELD_GENDER = "travellerInfo.gender";
	private static final String FIELD_EMAIL = "travellerInfo.email";
	private static final String ERROR_TYPE_NOT_EMPTY = "NotEmpty";
	private static final String ERROR_TYPE_PATTERN = "Pattern";
	private static final String FIELD_FIRSTNAME = "travellerInfo.firstName";
	private static final String FIELD_LASTNAME = "travellerInfo.surname";

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFTravellersData bcfTravellersData = (BCFTravellersData) object;

		for (int i = 0; i < bcfTravellersData.getTravellerDataPerJourney().size(); i++)
		{
			final BCFTravellerDataPerJourney travellerDataPerjrouney = bcfTravellersData.getTravellerDataPerJourney().get(i);
			final String superFormId = String.valueOf(i);

			List<TravellerData> travellers = Stream.concat(travellerDataPerjrouney.getOutboundTravellerData().stream(),
					travellerDataPerjrouney.getInboundTravellerData().stream())
					.collect(Collectors.toList());

			travellers.forEach(travellerData -> {
				if (TravelfacadesConstants.TRAVELLER_TYPE_PASSENGER.equalsIgnoreCase(travellerData.getTravellerType()))
				{
					final PassengerInformationData passengerInformation = (PassengerInformationData) travellerData.getTravellerInfo();

					if (StringUtils.equals(travellerData.getFormId(), FIRST_TRAVELLER_FORM))
					{
						validateBookerIsTravelling(travellerData.isBooker(), travellerData.getFormId(), errors);
					}
					validateTitle(errors, passengerInformation.getTitle().getCode(), travellerData.getFormId(), superFormId);
					validateGender(errors, passengerInformation.getGender(), travellerData.getFormId(), superFormId);
					validatePassengerName(passengerInformation, errors, travellerData.getFormId(), superFormId);

					validateEmail(errors, passengerInformation.getEmail(), travellerData.getFormId(), superFormId);

				}
			});
		}
	}

	protected void validateTitle(final Errors errors, final String title, final String formId, final String superformId)
	{
		if (StringUtils.isEmpty(title))
		{
			rejectValue(errors, FIELD_TITLE, formId, superformId, ERROR_TYPE_NOT_EMPTY);
		}
	}

	protected void validateGender(final Errors errors, final String gender, final String formId, final String superformId)
	{
		if (StringUtils.isEmpty(gender))
		{
			rejectValue(errors, FIELD_GENDER, formId, superformId, ERROR_TYPE_NOT_EMPTY);
		}
	}

	protected void validatePassengerName(final PassengerInformationData passengerInformation, final Errors errors,
			final String formId, final String superformId)
	{
		validateName(passengerInformation.getFirstName(), FIELD_FIRSTNAME, formId, superformId, errors);
		validateName(passengerInformation.getSurname(), FIELD_LASTNAME, formId, superformId, errors);
	}

	protected void validateName(final String name, final String attributeName, final String formId, final String superformId,
			final Errors errors)
	{
		if (StringUtils.isNotBlank(name.trim())
				&& !name.matches(TravelacceleratorstorefrontValidationConstants.REGEX_LETTERS_DASHES_SPACES_FIRST_LETTER))
		{
			rejectValue(errors, attributeName, formId, superformId, ERROR_TYPE_PATTERN);
		}
		else if (StringUtils.isEmpty(name.trim()))
		{
			rejectValue(errors, attributeName, formId, superformId, ERROR_TYPE_NOT_EMPTY);
		}
	}

	protected void validateEmail(final Errors errors, final String email, final String formId, final String superformId)
	{
		if (StringUtils.isNotBlank(email) && (StringUtils.length(email) > 255 || !validateEmailAddress(email)))
		{
			rejectValue(errors, FIELD_EMAIL, formId, superformId, ERROR_TYPE_PATTERN);
		}
	}

	protected void rejectValue(final Errors errors, final String attributeName, final String formId, final String superformId,
			final String errorCode)
	{
		final String attribute = SUPER_FORM + "[" + superformId + "]." + TRAVELLER_FORMS + "[" + formId + "]." + attributeName;
		final String errorMessageCode = errorCode + "." + TRAVELLER_FORMS + "." + attributeName;
		errors.rejectValue(attribute, errorMessageCode);
	}

}
