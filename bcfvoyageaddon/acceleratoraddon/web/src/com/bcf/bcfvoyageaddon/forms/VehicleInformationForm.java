/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms;

public class VehicleInformationForm
{
	private String vehicleCode;
	private String vehicleName;
	private String vehicleCategory;
	private int vehicleLength;
	private int vehicleWidth;
	private int vehicleHeight;
	private boolean carryingLivestock;
	private boolean vehicleWithSidecarOrTrailer;
	private String licensePlateNumber;
	private String licensePlateCountry;
	private String licensePlateProvince;
	private String selectedSavedTravellerUId;

	public String getVehicleCode()
	{
		return vehicleCode;
	}

	public void setVehicleCode(final String vehicleCode)
	{
		this.vehicleCode = vehicleCode;
	}

	public String getVehicleName()
	{
		return vehicleName;
	}

	public void setVehicleName(final String vehicleName)
	{
		this.vehicleName = vehicleName;
	}

	public String getLicensePlateNumber()
	{
		return licensePlateNumber;
	}

	public void setLicensePlateNumber(final String licensePlateNumber)
	{
		this.licensePlateNumber = licensePlateNumber;
	}

	public String getLicensePlateCountry()
	{
		return licensePlateCountry;
	}

	public void setLicensePlateCountry(final String licensePlateCountry)
	{
		this.licensePlateCountry = licensePlateCountry;
	}

	public String getLicensePlateProvince()
	{
		return licensePlateProvince;
	}

	public void setLicensePlateProvince(final String licensePlateProvince)
	{
		this.licensePlateProvince = licensePlateProvince;
	}

	public String getSelectedSavedTravellerUId()
	{
		return selectedSavedTravellerUId;
	}

	public void setSelectedSavedTravellerUId(final String selectedSavedTravellerUId)
	{
		this.selectedSavedTravellerUId = selectedSavedTravellerUId;
	}

	public String getVehicleCategory()
	{
		return vehicleCategory;
	}

	public void setVehicleCategory(final String vehicleCategory)
	{
		this.vehicleCategory = vehicleCategory;
	}

	public int getVehicleLength()
	{
		return vehicleLength;
	}

	public void setVehicleLength(final int vehicleLength)
	{
		this.vehicleLength = vehicleLength;
	}

	public int getVehicleWidth()
	{
		return vehicleWidth;
	}

	public void setVehicleWidth(final int vehicleWidth)
	{
		this.vehicleWidth = vehicleWidth;
	}

	public int getVehicleHeight()
	{
		return vehicleHeight;
	}

	public void setVehicleHeight(final int vehicleHeight)
	{
		this.vehicleHeight = vehicleHeight;
	}

	public boolean isCarryingLivestock()
	{
		return carryingLivestock;
	}

	public void setCarryingLivestock(final boolean carryingLivestock)
	{
		this.carryingLivestock = carryingLivestock;
	}

	public boolean isVehicleWithSidecarOrTrailer()
	{
		return vehicleWithSidecarOrTrailer;
	}

	public void setVehicleWithSidecarOrTrailer(final boolean vehicleWithSidecarOrTrailer)
	{
		this.vehicleWithSidecarOrTrailer = vehicleWithSidecarOrTrailer;
	}


}
