/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.forms.cms;

public class RouteInfoForm
{

	private String tripType;
	private String arrivalLocation;
	private String departureLocation;
	private String arrivalLocationName;
	private String departureLocationName;
	private String arrivalLocationSuggestionType;
	private String departureLocationSuggestionType;
	private String departingDateTime;
	private String returnDateTime;
	private String routeType;
	private boolean walkOnRoute;
	private boolean allowsWalkOnOptions;

	public String getTripType()
	{
		return tripType;
	}

	public void setTripType(final String tripType)
	{
		this.tripType = tripType;
	}

	public String getArrivalLocation()
	{
		return arrivalLocation;
	}

	public void setArrivalLocation(final String arrivalLocation)
	{
		this.arrivalLocation = arrivalLocation;
	}

	public String getDepartureLocation()
	{
		return departureLocation;
	}

	public void setDepartureLocation(final String departureLocation)
	{
		this.departureLocation = departureLocation;
	}

	public String getArrivalLocationName()
	{
		return arrivalLocationName;
	}

	public void setArrivalLocationName(final String arrivalLocationName)
	{
		this.arrivalLocationName = arrivalLocationName;
	}

	public String getDepartureLocationName()
	{
		return departureLocationName;
	}

	public void setDepartureLocationName(final String departureLocationName)
	{
		this.departureLocationName = departureLocationName;
	}

	public String getArrivalLocationSuggestionType()
	{
		return arrivalLocationSuggestionType;
	}

	public void setArrivalLocationSuggestionType(final String arrivalLocationSuggestionType)
	{
		this.arrivalLocationSuggestionType = arrivalLocationSuggestionType;
	}

	public String getDepartureLocationSuggestionType()
	{
		return departureLocationSuggestionType;
	}

	public void setDepartureLocationSuggestionType(final String departureLocationSuggestionType)
	{
		this.departureLocationSuggestionType = departureLocationSuggestionType;
	}

	public String getDepartingDateTime()
	{
		return departingDateTime;
	}

	public void setDepartingDateTime(final String departingDateTime)
	{
		this.departingDateTime = departingDateTime;
	}

	public String getReturnDateTime()
	{
		return returnDateTime;
	}

	public void setReturnDateTime(final String returnDateTime)
	{
		this.returnDateTime = returnDateTime;
	}

	public String getRouteType()
	{
		return routeType;
	}

	public void setRouteType(final String routeType)
	{
		this.routeType = routeType;
	}

	public boolean isWalkOnRoute()
	{
		return walkOnRoute;
	}

	public void setWalkOnRoute(final boolean walkOnRoute)
	{
		this.walkOnRoute = walkOnRoute;
	}

	public boolean isAllowsWalkOnOptions()
	{
		return allowsWalkOnOptions;
	}

	public void setAllowsWalkOnOptions(final boolean allowsWalkOnOptions)
	{
		this.allowsWalkOnOptions = allowsWalkOnOptions;
	}
}
