/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms.cms;

import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import java.util.List;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travel.passenger.AccessibilityRequestData;


/**
 * FareFinderForm object used to bind with the FareFinderComponent.jsp and uses JSR303 validation.
 */

public class FareFinderForm
{
	private boolean accessibilityNeedsCheckOutbound;
	private List<AccessibilityRequestData> accessibilityRequestDataList;
	private List<PassengerTypeQuantityData> passengerTypeQuantityList;
	private Boolean travellingWithChildren;
	private String tripType;
	private String cabinClass;
	private String arrivalLocation;
	private String departureLocation;
	private String arrivalLocationName;
	private String departureLocationName;
	private String departingDateTime;
	private String returnDateTime;
	private Boolean returnWithDifferentVehicle;
	private Boolean travellingWithVehicle;
	private Boolean carryingDangerousGoodsInOutbound;
	private Boolean carryingDangerousGoodsInReturn;
	private int standardVehicleHeightPortLimit;
	private int standardVehicleLengthPortLimit;
	private List<VehicleTypeQuantityData> vehicleInfo;


	public String getArrivalLocationName()
	{
		return arrivalLocationName;
	}

	public void setArrivalLocationName(final String arrivalLocationName)
	{
		this.arrivalLocationName = arrivalLocationName;
	}

	public String getDepartureLocationName()
	{
		return departureLocationName;
	}

	public void setDepartureLocationName(final String departureLocationName)
	{
		this.departureLocationName = departureLocationName;
	}

	public String getArrivalLocation()
	{
		return arrivalLocation;
	}

	public void setArrivalLocation(final String arrivalLocation)
	{
		this.arrivalLocation = arrivalLocation;
	}

	public String getDepartureLocation()
	{
		return departureLocation;
	}

	public void setDepartureLocation(final String departureLocation)
	{
		this.departureLocation = departureLocation;
	}

	public String getDepartingDateTime()
	{
		return departingDateTime;
	}

	public void setDepartingDateTime(final String departingDateTime)
	{
		this.departingDateTime = departingDateTime;
	}

	public String getReturnDateTime()
	{
		return returnDateTime;
	}

	public void setReturnDateTime(final String returnDateTime)
	{
		this.returnDateTime = returnDateTime;
	}

	public String getTripType()
	{
		return tripType;
	}

	public void setTripType(final String tripType)
	{
		this.tripType = tripType;
	}

	public List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		return passengerTypeQuantityList;
	}

	public void setPassengerTypeQuantityList(final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		this.passengerTypeQuantityList = passengerTypeQuantityList;
	}

	public String getCabinClass()
	{
		return cabinClass;
	}

	public void setCabinClass(final String cabinClass)
	{
		this.cabinClass = cabinClass;
	}

	public Boolean getTravellingWithChildren()
	{
		return travellingWithChildren;
	}

	public void setTravellingWithChildren(final Boolean travellingWithChildren)
	{
		this.travellingWithChildren = travellingWithChildren;
	}

	public List<VehicleTypeQuantityData> getVehicleInfo()
	{
		return vehicleInfo;
	}

	public void setVehicleInfo(final List<VehicleTypeQuantityData> vehicleInfo)
	{
		this.vehicleInfo = vehicleInfo;
	}

	public Boolean getReturnWithDifferentVehicle()
	{
		return returnWithDifferentVehicle;
	}

	public void setReturnWithDifferentVehicle(final Boolean returnWithDifferentVehicle)
	{
		this.returnWithDifferentVehicle = returnWithDifferentVehicle;
	}

	public Boolean getTravellingWithVehicle()
	{
		return travellingWithVehicle;
	}

	public void setTravellingWithVehicle(final Boolean travellingWithVehicle)
	{
		this.travellingWithVehicle = travellingWithVehicle;
	}

	public Boolean getCarryingDangerousGoodsInOutbound()
	{
		return carryingDangerousGoodsInOutbound;
	}

	public void setCarryingDangerousGoodsInOutbound(final Boolean carryingDangerousGoodsInOutbound)
	{
		this.carryingDangerousGoodsInOutbound = carryingDangerousGoodsInOutbound;
	}

	public Boolean getCarryingDangerousGoodsInReturn()
	{
		return carryingDangerousGoodsInReturn;
	}

	public void setCarryingDangerousGoodsInReturn(final Boolean carryingDangerousGoodsInReturn)
	{
		this.carryingDangerousGoodsInReturn = carryingDangerousGoodsInReturn;
	}

	public boolean isAccessibilityNeedsCheckOutbound()
	{
		return accessibilityNeedsCheckOutbound;
	}

	public void setAccessibilityNeedsCheckOutbound(final boolean accessibilityNeedsCheckOutbound)
	{
		this.accessibilityNeedsCheckOutbound = accessibilityNeedsCheckOutbound;
	}

	public List<AccessibilityRequestData> getAccessibilityRequestDataList()
	{
		return accessibilityRequestDataList;
	}

	public void setAccessibilityRequestDataList(
			final List<AccessibilityRequestData> accessibilityRequestDataList)
	{
		this.accessibilityRequestDataList = accessibilityRequestDataList;
	}

	public int getStandardVehicleHeightPortLimit()
	{
		return standardVehicleHeightPortLimit;
	}

	public void setStandardVehicleHeightPortLimit(final int standardVehicleHeightPortLimit)
	{
		this.standardVehicleHeightPortLimit = standardVehicleHeightPortLimit;
	}

	public int getStandardVehicleLengthPortLimit()
	{
		return standardVehicleLengthPortLimit;
	}

	public void setStandardVehicleLengthPortLimit(final int standardVehicleLengthPortLimit)
	{
		this.standardVehicleLengthPortLimit = standardVehicleLengthPortLimit;
	}

}
