/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms.cms;

import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PricedItineraryData;


/**
 * FareCalculatorForm object used to passed valued from fare selection to fare summary page.
 */

public class FareCalculatorForm
{
	private ItineraryPricingInfoData itineraryPricingInfo;
	private int journeyRefNum;
	private String travelRouteCode;
	private PricedItineraryData pricedItinerary;
	private String shipName;
	private int departureDurationHour;
	private int departureDurationMinute;
	private int departureVehicleQuantity;
	private String departureVehicleName;
	private String departureVehiclePrice;

	public ItineraryPricingInfoData getItineraryPricingInfo()
	{
		return itineraryPricingInfo;
	}

	public void setItineraryPricingInfo(final ItineraryPricingInfoData itineraryPricingInfo)
	{
		this.itineraryPricingInfo = itineraryPricingInfo;
	}

	public int getJourneyRefNum()
	{
		return journeyRefNum;
	}

	public void setJourneyRefNum(final int journeyRefNum)
	{
		this.journeyRefNum = journeyRefNum;
	}

	public String getTravelRouteCode()
	{
		return travelRouteCode;
	}

	public void setTravelRouteCode(final String travelRouteCode)
	{
		this.travelRouteCode = travelRouteCode;
	}

	public PricedItineraryData getPricedItinerary()
	{
		return pricedItinerary;
	}

	public void setPricedItinerary(final PricedItineraryData pricedItinerary)
	{
		this.pricedItinerary = pricedItinerary;
	}

	public String getShipName()
	{
		return shipName;
	}

	public void setShipName(final String shipName)
	{
		this.shipName = shipName;
	}

	public int getDepartureDurationHour()
	{
		return departureDurationHour;
	}

	public void setDepartureDurationHour(final int departureDurationHour)
	{
		this.departureDurationHour = departureDurationHour;
	}

	public int getDepartureDurationMinute()
	{
		return departureDurationMinute;
	}

	public void setDepartureDurationMinute(final int departureDurationMinute)
	{
		this.departureDurationMinute = departureDurationMinute;
	}

	public int getDepartureVehicleQuantity()
	{
		return departureVehicleQuantity;
	}

	public void setDepartureVehicleQuantity(final int departureVehicleQuantity)
	{
		this.departureVehicleQuantity = departureVehicleQuantity;
	}

	public String getDepartureVehicleName()
	{
		return departureVehicleName;
	}

	public void setDepartureVehicleName(final String departureVehicleName)
	{
		this.departureVehicleName = departureVehicleName;
	}

	public String getDepartureVehiclePrice()
	{
		return departureVehiclePrice;
	}

	public void setDepartureVehiclePrice(final String departureVehiclePrice)
	{
		this.departureVehiclePrice = departureVehiclePrice;
	}
}
