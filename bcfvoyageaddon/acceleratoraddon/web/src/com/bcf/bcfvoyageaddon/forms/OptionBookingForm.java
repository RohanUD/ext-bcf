/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 5/7/19 8:38 AM
 */

package com.bcf.bcfvoyageaddon.forms;

public class OptionBookingForm
{
	private String optionBookingId;
	private String originalExpirationDate;
	private String updatedExpirationDate;

	public String getOptionBookingId()
	{
		return optionBookingId;
	}

	public void setOptionBookingId(final String optionBookingId)
	{
		this.optionBookingId = optionBookingId;
	}

	public String getOriginalExpirationDate()
	{
		return originalExpirationDate;
	}

	public void setOriginalExpirationDate(final String originalExpirationDate)
	{
		this.originalExpirationDate = originalExpirationDate;
	}

	public String getUpdatedExpirationDate()
	{
		return updatedExpirationDate;
	}

	public void setUpdatedExpirationDate(final String updatedExpirationDate)
	{
		this.updatedExpirationDate = updatedExpirationDate;
	}
}
