/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfvoyageaddon.forms;

import de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import java.util.List;
import com.bcf.facades.ferry.AccessibilitytData;
import com.bcf.facades.ferry.LargeItemData;
import com.bcf.facades.ferry.OtherChargeData;
import com.bcf.facades.ferry.SpecialServicesData;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


/**
 * AddBundleToCartForm object used to bind with the AddBundleToCartForm and uses JSR303 validation.
 */
public class AddBundleToCartForm
{
	private String dealId;
	private boolean isParentDeal;
	private boolean isOpenTicket;
	private String travelRouteCode;
	private ItineraryPricingInfoData itineraryPricingInfo;
	private List<PassengerTypeQuantityData> passengerTypeQuantityList;
	private String originDestinationRefNumber;
	private List<VehicleTypeQuantityData> vehicleTypeQuantityList;
	private boolean returnWithDifferentPassenger;
	private boolean returnWithDifferentVehicle;
	private int journeyRefNum;
	private String changeFareOrReplan;
	private int fareTypeRefNo;
	private boolean carryingDangerousGoodsInOutbound;
	private boolean carryingDangerousGoodsInReturn;
	private String errorClass;
	private String transferSailingIdentifier;
	private List<AccessibilitytData> accessibilitytDataList;
	private List<SpecialServicesData> specialServiceRequestDataList;
	private List<LargeItemData> largeItems;
	private boolean dynamicPackage;
	private List<OtherChargeData> otherChargeDataList;
	private List<String> passengerUidsToBeRemoved;


	public List<String> getPassengerUidsToBeRemoved()
	{
		return passengerUidsToBeRemoved;
	}

	public void setPassengerUidsToBeRemoved(final List<String> passengerUidsToBeRemoved)
	{
		this.passengerUidsToBeRemoved = passengerUidsToBeRemoved;
	}

	private String tripType;

	public boolean isOpenTicket()
	{
		return isOpenTicket;
	}

	public void setOpenTicket(final boolean openTicket)
	{
		isOpenTicket = openTicket;
	}

	public List<SpecialServicesData> getSpecialServiceRequestDataList()
	{
		return specialServiceRequestDataList;
	}

	public void setSpecialServiceRequestDataList(final List<SpecialServicesData> specialServiceRequestDataList)
	{
		this.specialServiceRequestDataList = specialServiceRequestDataList;
	}

	public String getDealId()
	{
		return dealId;
	}

	public void setDealId(final String dealId)
	{
		this.dealId = dealId;
	}

	public String getChangeFareOrReplan()
	{
		return changeFareOrReplan;
	}

	public void setChangeFareOrReplan(final String changeFareOrReplan)
	{
		this.changeFareOrReplan = changeFareOrReplan;
	}

	public int getFareTypeRefNo()
	{
		return fareTypeRefNo;
	}

	public void setFareTypeRefNo(final int fareTypeRefNo)
	{
		this.fareTypeRefNo = fareTypeRefNo;
	}

	/**
	 * @return the travelRouteCode
	 */
	public String getTravelRouteCode()
	{
		return travelRouteCode;
	}

	/**
	 * @param travelRouteCode the travelRouteCode to set
	 */
	public void setTravelRouteCode(final String travelRouteCode)
	{
		this.travelRouteCode = travelRouteCode;
	}

	/**
	 * @return the itineraryPricingInfo
	 */
	public ItineraryPricingInfoData getItineraryPricingInfo()
	{
		return itineraryPricingInfo;
	}

	/**
	 * @param itineraryPricingInfo the itineraryPricingInfo to set
	 */
	public void setItineraryPricingInfo(final ItineraryPricingInfoData itineraryPricingInfo)
	{
		this.itineraryPricingInfo = itineraryPricingInfo;
	}

	/**
	 * @return the passengerTypeQuantityList
	 */
	public List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		return passengerTypeQuantityList;
	}

	/**
	 * @param passengerTypeQuantityList the passengerTypeQuantityList to set
	 */
	public void setPassengerTypeQuantityList(
			final List<PassengerTypeQuantityData> passengerTypeQuantityList)
	{
		this.passengerTypeQuantityList = passengerTypeQuantityList;
	}

	/**
	 * @return the originDestinationRefNumber
	 */
	public String getOriginDestinationRefNumber()
	{
		return originDestinationRefNumber;
	}

	/**
	 * @param originDestinationRefNumber the originDestinationRefNumber to set
	 */
	public void setOriginDestinationRefNumber(final String originDestinationRefNumber)
	{
		this.originDestinationRefNumber = originDestinationRefNumber;
	}

	public List<VehicleTypeQuantityData> getVehicleTypeQuantityList()
	{
		return vehicleTypeQuantityList;
	}

	public void setVehicleTypeQuantityList(final List<VehicleTypeQuantityData> vehicleTypeQuantityList)
	{
		this.vehicleTypeQuantityList = vehicleTypeQuantityList;
	}

	public int getJourneyRefNum()
	{
		return journeyRefNum;
	}

	public void setJourneyRefNum(final int journeyRefNum)
	{
		this.journeyRefNum = journeyRefNum;
	}

	public boolean isCarryingDangerousGoodsInOutbound()
	{
		return carryingDangerousGoodsInOutbound;
	}

	public void setCarryingDangerousGoodsInOutbound(final boolean carryingDangerousGoodsInOutbound)
	{
		this.carryingDangerousGoodsInOutbound = carryingDangerousGoodsInOutbound;
	}

	public boolean isCarryingDangerousGoodsInReturn()
	{
		return carryingDangerousGoodsInReturn;
	}

	public void setCarryingDangerousGoodsInReturn(final boolean carryingDangerousGoodsInReturn)
	{
		this.carryingDangerousGoodsInReturn = carryingDangerousGoodsInReturn;
	}

	public String getErrorClass()
	{
		return errorClass;
	}

	public void setErrorClass(final String errorClass)
	{
		this.errorClass = errorClass;
	}

	public String getTransferSailingIdentifier()
	{
		return transferSailingIdentifier;
	}

	public void setTransferSailingIdentifier(final String transferSailingIdentifier)
	{
		this.transferSailingIdentifier = transferSailingIdentifier;
	}

	public boolean isReturnWithDifferentPassenger()
	{
		return returnWithDifferentPassenger;
	}

	public void setReturnWithDifferentPassenger(final boolean returnWithDifferentPassenger)
	{
		this.returnWithDifferentPassenger = returnWithDifferentPassenger;
	}

	public void setReturnWithDifferentVehicle(final boolean returnWithDifferentVehicle)
	{
		this.returnWithDifferentVehicle = returnWithDifferentVehicle;
	}

	public boolean getReturnWithDifferentVehicle()
	{

		return returnWithDifferentVehicle;
	}

	public boolean isParentDeal()
	{
		return isParentDeal;
	}

	public void setParentDeal(final boolean parentDeal)
	{
		isParentDeal = parentDeal;
	}

	public List<AccessibilitytData> getAccessibilitytDataList()
	{
		return accessibilitytDataList;
	}

	public void setAccessibilitytDataList(final List<AccessibilitytData> accessibilitytDataList)
	{
		this.accessibilitytDataList = accessibilitytDataList;
	}

	public boolean isDynamicPackage()
	{
		return dynamicPackage;
	}

	public void setDynamicPackage(final boolean dynamicPackage)
	{
		this.dynamicPackage = dynamicPackage;
	}

	public List<LargeItemData> getLargeItems()
	{
		return largeItems;
	}

	public void setLargeItems(final List<LargeItemData> largeItems)
	{
		this.largeItems = largeItems;
	}

	public List<OtherChargeData> getOtherChargeDataList()
	{
		return otherChargeDataList;
	}

	public void setOtherChargeDataList(final List<OtherChargeData> otherChargeDataList)
	{
		this.otherChargeDataList = otherChargeDataList;
	}

	public String getTripType()
	{
		return tripType;
	}

	public void setTripType(final String tripType)
	{
		this.tripType = tripType;
	}
}
