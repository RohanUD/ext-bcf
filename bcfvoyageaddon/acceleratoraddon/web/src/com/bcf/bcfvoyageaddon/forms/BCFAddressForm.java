/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class BCFAddressForm extends AddressForm

{
	private String addressType;
	private String phoneType;

	public void setAddressType(final String addressType)
	{
		this.addressType = addressType;
	}

	public String getAddressType()
	{
		return addressType;
	}

	public String getPhoneType()
	{
		return phoneType;
	}

	public void setPhoneType(final String phoneType)
	{
		this.phoneType = phoneType;
	}


}


