/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.forms.cms;

/**
 * FareFinderForm object used to bind with the FareFinderComponent.jsp and uses JSR303 validation.
 */

public class BCFFareFinderForm
{

	private RouteInfoForm routeInfoForm;
	private PassengerInfoForm passengerInfoForm;
	private VehicleInfoForm vehicleInfoForm;
	private boolean readOnlyResults;
	private String currentPage;
	private boolean skipMaxAllowedValidation;
	private double thresholdHours;

	public RouteInfoForm getRouteInfoForm()
	{
		return routeInfoForm;
	}

	public void setRouteInfoForm(final RouteInfoForm routeInfoForm)
	{
		this.routeInfoForm = routeInfoForm;
	}

	public PassengerInfoForm getPassengerInfoForm()
	{
		return passengerInfoForm;
	}

	public void setPassengerInfoForm(final PassengerInfoForm passengerInfoForm)
	{
		this.passengerInfoForm = passengerInfoForm;
	}

	public VehicleInfoForm getVehicleInfoForm()
	{
		return vehicleInfoForm;
	}

	public void setVehicleInfoForm(final VehicleInfoForm vehicleInfoForm)
	{
		this.vehicleInfoForm = vehicleInfoForm;
	}

	public boolean isReadOnlyResults()
	{
		return readOnlyResults;
	}

	public void setReadOnlyResults(final boolean readOnlyResults)
	{
		this.readOnlyResults = readOnlyResults;
	}

	public String getCurrentPage()
	{
		return currentPage;
	}

	public void setCurrentPage(final String currentPage)
	{
		this.currentPage = currentPage;
	}

	public boolean isSkipMaxAllowedValidation()
	{
		return skipMaxAllowedValidation;
	}

	public void setSkipMaxAllowedValidation(final boolean skipMaxAllowedValidation)
	{
		this.skipMaxAllowedValidation = skipMaxAllowedValidation;
	}

	public double getThresholdHours()
	{
		return thresholdHours;
	}

	public void setThresholdHours(final double thresholdHours)
	{
		this.thresholdHours = thresholdHours;
	}
}
