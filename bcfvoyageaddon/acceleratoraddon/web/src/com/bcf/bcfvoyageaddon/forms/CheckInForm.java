/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms;

import java.util.List;


public class CheckInForm
{

	private List<APIForm> apiFormList;

	/**
	 * @return
	 */
	public List<APIForm> getApiFormList()
	{
		return apiFormList;
	}

	/**
	 * @param apiFormList
	 */
	public void setApiFormList(final List<APIForm> apiFormList)
	{
		this.apiFormList = apiFormList;
	}
}
