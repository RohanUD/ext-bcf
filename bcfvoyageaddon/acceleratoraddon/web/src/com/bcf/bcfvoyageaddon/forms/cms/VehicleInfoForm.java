/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfvoyageaddon.forms.cms;

import java.util.List;
import com.bcf.facades.ferry.VehicleTypeQuantityData;


public class VehicleInfoForm
{

	private boolean returnWithDifferentVehicle;
	private boolean travellingWithVehicle;
	private List<VehicleTypeQuantityData> vehicleInfo;
	private boolean carryingDangerousGoodsInOutbound;
	private boolean carryingDangerousGoodsInReturn;
	private int standardVehicleLengthPortLimit;
	private int standardVehicleHeightPortLimit;
	private int over5500VehicleLengthPortLimit;
	private int over5500VehicleHeightPortLimit;
	private int over5500VehicleLengthWarningLimit;
	private int over5500VehicleHeightWarningLimit;
	private int vehicleWidthPortLimit;

	public boolean isReturnWithDifferentVehicle()
	{
		return returnWithDifferentVehicle;
	}

	public void setReturnWithDifferentVehicle(final boolean returnWithDifferentVehicle)
	{
		this.returnWithDifferentVehicle = returnWithDifferentVehicle;
	}

	public boolean isTravellingWithVehicle()
	{
		return travellingWithVehicle;
	}

	public void setTravellingWithVehicle(final boolean travellingWithVehicle)
	{
		this.travellingWithVehicle = travellingWithVehicle;
	}

	public List<VehicleTypeQuantityData> getVehicleInfo()
	{
		return vehicleInfo;
	}

	public void setVehicleInfo(final List<VehicleTypeQuantityData> vehicleInfo)
	{
		this.vehicleInfo = vehicleInfo;
	}

	public boolean isCarryingDangerousGoodsInOutbound()
	{
		return carryingDangerousGoodsInOutbound;
	}

	public void setCarryingDangerousGoodsInOutbound(final boolean carryingDangerousGoodsInOutbound)
	{
		this.carryingDangerousGoodsInOutbound = carryingDangerousGoodsInOutbound;
	}

	public boolean isCarryingDangerousGoodsInReturn()
	{
		return carryingDangerousGoodsInReturn;
	}

	public void setCarryingDangerousGoodsInReturn(final boolean carryingDangerousGoodsInReturn)
	{
		this.carryingDangerousGoodsInReturn = carryingDangerousGoodsInReturn;
	}

	public int getStandardVehicleLengthPortLimit()
	{
		return standardVehicleLengthPortLimit;
	}

	public void setStandardVehicleLengthPortLimit(final int standardVehicleLengthPortLimit)
	{
		this.standardVehicleLengthPortLimit = standardVehicleLengthPortLimit;
	}

	public int getStandardVehicleHeightPortLimit()
	{
		return standardVehicleHeightPortLimit;
	}

	public void setStandardVehicleHeightPortLimit(final int standardVehicleHeightPortLimit)
	{
		this.standardVehicleHeightPortLimit = standardVehicleHeightPortLimit;
	}

	public int getOver5500VehicleLengthPortLimit()
	{
		return over5500VehicleLengthPortLimit;
	}

	public void setOver5500VehicleLengthPortLimit(final int over5500VehicleLengthPortLimit)
	{
		this.over5500VehicleLengthPortLimit = over5500VehicleLengthPortLimit;
	}

	public int getOver5500VehicleHeightPortLimit()
	{
		return over5500VehicleHeightPortLimit;
	}

	public void setOver5500VehicleHeightPortLimit(final int over5500VehicleHeightPortLimit)
	{
		this.over5500VehicleHeightPortLimit = over5500VehicleHeightPortLimit;
	}

	public int getOver5500VehicleLengthWarningLimit()
	{
		return over5500VehicleLengthWarningLimit;
	}

	public void setOver5500VehicleLengthWarningLimit(final int over5500VehicleLengthWarningLimit)
	{
		this.over5500VehicleLengthWarningLimit = over5500VehicleLengthWarningLimit;
	}

	public int getOver5500VehicleHeightWarningLimit()
	{
		return over5500VehicleHeightWarningLimit;
	}

	public void setOver5500VehicleHeightWarningLimit(final int over5500VehicleHeightWarningLimit)
	{
		this.over5500VehicleHeightWarningLimit = over5500VehicleHeightWarningLimit;
	}

	public int getVehicleWidthPortLimit()
	{
		return vehicleWidthPortLimit;
	}

	public void setVehicleWidthPortLimit(final int vehicleWidthPortLimit)
	{
		this.vehicleWidthPortLimit = vehicleWidthPortLimit;
	}
}
