/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfvoyageaddon.forms;

import javax.validation.Valid;


public class TravellerForm implements Comparable<TravellerForm>
{
	private String formId;
	private String selectedSavedTravellerUId;
	private String uid;
	private String label;
	private String travellerType;
	private Boolean booker;
	private Boolean specialAssistance;

	@Valid
	private PassengerInformationForm passengerInformation;

	@Valid
	private VehicleInformationForm vehicleInformation;

	public String getUid()
	{
		return uid;
	}

	public void setUid(final String uid)
	{
		this.uid = uid;
	}

	public String getTravellerType()
	{
		return travellerType;
	}

	public void setTravellerType(final String travellerType)
	{
		this.travellerType = travellerType;
	}

	public PassengerInformationForm getPassengerInformation()
	{
		return passengerInformation;
	}

	public void setPassengerInformation(final PassengerInformationForm passengerInformation)
	{
		this.passengerInformation = passengerInformation;
	}

	public VehicleInformationForm getVehicleInformation()
	{
		return vehicleInformation;
	}

	public void setVehicleInformation(final VehicleInformationForm vehicleInformation)
	{
		this.vehicleInformation = vehicleInformation;
	}

	@Override
	public int compareTo(final TravellerForm o) //NOSONAR
	{
		return this.getLabel().compareTo(o.getLabel());
	}

	public String getSelectedSavedTravellerUId()
	{
		return selectedSavedTravellerUId;
	}

	public void setSelectedSavedTravellerUId(final String selectedSavedTravellerUId)
	{
		this.selectedSavedTravellerUId = selectedSavedTravellerUId;
	}

	public String getFormId()
	{
		return formId;
	}

	public void setFormId(final String formId)
	{
		this.formId = formId;
	}

	public Boolean getBooker()
	{
		return booker;
	}

	public void setBooker(final Boolean booker)
	{
		this.booker = booker;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(final String label)
	{
		this.label = label;
	}

	public Boolean isSpecialAssistance()
	{
		return specialAssistance;
	}

	public void setSpecialAssistance(final Boolean specialAssistance)
	{
		this.specialAssistance = specialAssistance;
	}


}
