ACC.advancePassengerInfoForm = {

    _autoloadTracc : [
        "bindAdvancePassengerInfoValidationMessages",
        "bindingAdvancePassengerInfoForm"
    ],
    bindAdvancePassengerInfoValidationMessages : function () {
        ACC.AdvancePassengerInfoValidationMessages = new validationMessages("ferry-AdvancePassengerInfoValidationMessages");
        ACC.AdvancePassengerInfoValidationMessages.getMessages("error");
    },
    bindingAdvancePassengerInfoForm : function() {
        $("input[name='firstname']").attr('maxLength','35');
        $("input[name='lastname']").attr('maxLength','35');

        $('#apiForm').validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            onfocusout: function(element) {
                $(element).valid();
            },
            rules: {
                firstname: {
                    required: true,
                    nameValidation: true
                },
                lastname: {
                    required: true,
                    nameValidation: true
                }
            },
            messages: {
                firstname: {
                    required: ACC.AdvancePassengerInfoValidationMessages.message('error.formvalidation.firstName'),
                    nameValidation: ACC.AdvancePassengerInfoValidationMessages.message('error.formvalidation.firstNameValid')
                },
                lastname: {
                    required: ACC.AdvancePassengerInfoValidationMessages.message('error.formvalidation.lastName'),
                    nameValidation: ACC.AdvancePassengerInfoValidationMessages.message('error.formvalidation.lastNameValid')
                }
            }
        });
    }

};