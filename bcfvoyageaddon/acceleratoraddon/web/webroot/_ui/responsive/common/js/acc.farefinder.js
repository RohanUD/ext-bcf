ACC.farefinder = {
    _autoloadTracc: [
        "validationMesagesInit",
        "initCustomBindingToelements",
        "init",
        "loadSelectedPassengersBanners",
        "showInboundPassengersDiv",
        "showHotelsTab",
        "loadOtherAccessibilityNeedTextBox",
        "fireQueryToFetchAllRouteInfo",
        "showRemoveSailingModal",
        "showHotelOrVacationTab",
        "bindAddAHotelLinkToTab",
        "bindSeasonalSchedulesLink",
        "bindOutboundSeniorToolTip",
        "bindInboundSeniorToolTip"


    ],

    allTravelSectors:[],
    componentParentSelector: '#y_fareFinderForm',
    minRelativeReturnDate:0,
    terminalcacheversion:0,

    init: function () {
        // Sets the passenger quantity hidden value to the actualy value that is
    // displaying to user. This is a fix for browser back button.
        var $passengerQuantityArray = $(".y_fareFinderPassengerQuantity");
        $passengerQuantityArray.each(function () {
            $(this).attr('value', parseInt($(this).parent().find('.y_fareFinderPassengerQuantitySpan').text()));
        });
        ACC.farefinder.renderDateUiBasedOnTripType();
        ACC.farefinder.renderTabUI();
        ACC.farefinder.bindChangeTripTypeButton();
        ACC.farefinder.bindTravelWithChildrenButton();
        ACC.farefinder.bindFareFinderValidation();
        ACC.farefinder.bindFareFinderLocationEvents();
        ACC.farefinder.returnPassengerDetails();
        ACC.farefinder.selectVehicleType();
        ACC.farefinder.showNorthernPassengerTypes();
        ACC.farefinder.convertVehicleDimensionInMeters();
        ACC.farefinder.dangerousGoodCheckBox();
        ACC.farefinder.travellingWithVehicleCheckBox();
        ACC.farefinder.returnwithdifferentvehicle();
        ACC.farefinder.vehicleinboundcheckbox();
        ACC.farefinder.sidecarcheckbox();
	    ACC.farefinder.bindRemoveAmendmentCartBeforeSearch();
	    ACC.farefinder.bindPassengerQuanitySelectorOutbound();
	    ACC.farefinder.bindPassengerQuanitySelectorInbound();
        ACC.farefinder.checkAccessibilityNeed();
        ACC.farefinder.checkCarryingLargeItem();
        ACC.farefinder.checkInboundAccessibilityNeed();
        ACC.farefinder.showHideDivs();
        ACC.farefinder.bindChangeTravellingOptionDiv();
        ACC.farefinder.bindShowHideAdditionalVehicleFields();
        ACC.farefinder.bindStandardVehicleLimitSailings();
        ACC.farefinder.bindStandardVehiclePortRestrictionPackage();
        ACC.farefinder.bindAccessibilitySelectorButton();
        ACC.farefinder.bindReturnAccessibilitySelectorButton();
        ACC.farefinder.bindClearSelectionButton();
        ACC.farefinder.bindOtherAccessibilityNeedTextBox();
	    ACC.farefinder.removeAlert();
	    ACC.farefinder.currentOutboundPassengerCount();
	    ACC.farefinder.outboundPassengerQuantityBanner();
	    ACC.farefinder.currentInboundPassengerCount();
	    ACC.farefinder.inboundPassengerQuantityBanner();
	    ACC.farefinder.loadOutboundAccessibilityNeeds();
	    ACC.farefinder.loadInboundAccessibilityNeeds();
	    ACC.farefinder.bindLargeItemQuanitySelectorOutbound();
	    ACC.farefinder.bindLargeItemQuanitySelectorInbound();
	    ACC.farefinder.checkInboundCarryingLargeItem();
	    ACC.farefinder.checkCarryingDangerousGoods();
	    ACC.farefinder.checkCarryingDangerousGoodsInbound();
	    ACC.farefinder.bindResidentProgramPage();
	    ACC.farefinder.setVehicleDimensionValues();
	    ACC.farefinder.renderCalenderSelectedDateToTab();
	    ACC.farefinder.bindJourneyDetailsEditModal();
	    ACC.farefinder.bindAddAnotherSailingButton();
	    ACC.farefinder.bindRedirectToHomeButton();
	    ACC.farefinder.bindJourneyDetailsEditFareSelection();
	    ACC.farefinder.vehicleAccordionUIRenderOnEdit();
	    ACC.farefinder.vehicleAccordionUIEventBinder();
	    ACC.farefinder.allowWalkOnForPassengersWithSpecialNeeds();
	    ACC.farefinder.triggerWalkOnOptions();
	    ACC.farefinder.bindOver5500kgWidthSelector();
	    ACC.farefinder.manageError();
    },

     bindAddAHotelLinkToTab:function(){

       $("#addAHotel").on("click", function(e) {
            $('#ui-id-2').click();
        })
     },

      bindOutboundSeniorToolTip:function(){
         $(".y_outboundPassengerQtySelectorPlus.y_senior").on("click", function() {
            var seniorCount = $("#y_outboundPassengerQuantity.y_senior").val();
            if($(this).hasClass('y_senior') && seniorCount == 1){
                $(this).popover('show');
            }
            else
            {
                $(this).popover('destroy');
            }
         });

      },

      bindInboundSeniorToolTip:function(){
         $(".y_inboundPassengerQtySelectorPlus.y_senior").on("click", function() {
            var seniorCount = $("#y_inboundPassengerQuantity.yr_senior").val();
            if($(this).hasClass('y_senior') && seniorCount == 1){
                $(this).popover('show');
            }
            else
            {
                $(this).popover('destroy');
            }
         });

      },


     bindSeasonalSchedulesLink:function(){
          $("#seasonalSchedulesLink").on("click", function(e) {
               var departLoc=$(".y_fareFinderOriginLocationCode").val();
               var arrLoc=$(".y_fareFinderDestinationLocationCode").val();
               if(departLoc != "" && arrLoc != "") {
               ACC.farefinder.redirectToURl(ACC.config.contextPath+"/seasonal-schedules?departurePort="+departLoc+"&arrivalPort="+arrLoc);
               }
           })
        },

    validationMesagesInit : function(){
      ACC.validationMessages = new validationMessages("ferry-routeInfoForm");
      ACC.validationMessages.getMessages("error");
      ACC.validationMessages.getMessages("label");
    },
    loadSelectedPassengersBanners : function () {
      if("PassengerSelectionPage" == $("#currentPage").text()){
        $('[id=y_outboundPassengerQuantity]').each(function() {
      var quantity = parseInt($(this).val());
      var passengerCode = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerCode").attr('value');
      var passengerTypeName = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
      ACC.farefinder.outboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
      });
        if($('input[id="y_differentPassengerInReturn"]:checked').length > 0) {
            $('[id=y_inboundPassengerQuantity]').each(function() {
            var quantity = parseInt($(this).val());
            var passengerCode = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerCode").attr('value');
            var passengerTypeName = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
            ACC.farefinder.inboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
          });
      }
      }
    },

    showInboundPassengersDiv : function () {
      if("PassengerSelectionPage" == $("#currentPage").text()){
            if($('input[id="y_differentPassengerInReturn"]:checked').length > 0) {
              $("#y_returnPassenger").removeClass("hidden");
        }else{
          $("#y_returnPassenger").addClass("hidden");
        }
            $("#accordion-returnPassenger").accordion({ header: "h3", collapsible: true, active: false });
      }
    },

    showHotelsTab : function () {
      if("Web" == $("#y_currentSalesChannel").text() || "CallCenter" == $("#y_currentSalesChannel").text()){
        $("a[href$='tabs-3']").addClass("hidden");
      }
      else{
        $("a[href$='tabs-3']").removeClass("hidden");
      }
    },

    loadOutboundAccessibilityNeeds : function () {
      if("PassengerSelectionPage" === $("#currentPage").text() || "fareCalculatorPage" === $("#currentPage").text()){
          if($('input[id="y_checkAccessibilityNeed"]:checked').length > 0) {
            ACC.farefinder.showOutboundAccessibilityNeeds();
      }else{
        $("#y_accessibilityPlaceholderParent").addClass("sr-only");
      }
          $("div[id^='accordion-accessibility-']").accordion({ header: "h3", collapsible: true, active: false });
      }
    },

    loadInboundAccessibilityNeeds : function () {
      if("PassengerSelectionPage" == $("#currentPage").text() || "fareCalculatorPage" === $("#currentPage").text()){
          if($('input[id="y_checkReturnAccessibilityNeed"]:checked').length > 0) {
            ACC.farefinder.showInboundAccessibilityNeeds();
      }else{
        $("#y_accessibilityReturnPlaceholderParent").addClass("sr-only");
      }
          $("div[id^='accordion-accesibility-need-']").accordion({ header: "h3", collapsible: true, active: false });
      }
    },

    loadOtherAccessibilityNeedTextBox : function () {
      if("PassengerSelectionPage" == $("#currentPage").text()){
        $("input[id^='specialServiceRequest_OTHR_']").each(function(){
         var elem = $(this).parent().parent().find("div[id^='other_accessibility_need_']");
        if (this.checked) {
           elem.addClass('required-accessibility_need');
           elem.removeClass("hidden");
        } else {
            elem.removeClass('required-accessibility_need');
            elem.addClass("hidden");
          }
      });
      }
    },
    renderCalenderSelectedDateToTab : function(){
      if($(".depart").val() != ""){
          $(".ui-depart").trigger("setDate",$(".depart").val());
      }

      if($(".arrive").val() != ""){
        $(".ui-return").trigger("setDate",$(".arrive").val());
      }

    },
    renderTabUI : function(e){
      $("#js-roundtrip").find(".nav-link").click(function(){
        if($(this).hasClass("active")){
          setTimeout(function(){

          $(this).removeClass("active");
          $(this).removeClass("show");
          var tabLink = $(this).attr("href");
          if(tabLink!=""){
            $(tabLink).removeClass("active");
            $(tabLink).removeClass("show");
          }
      }.bind(this),100);
    }
      })
    },
    renderDateUiBasedOnTripType : function(){
      var tripType = $("input[name='routeInfoForm.tripType']:checked").val();
        if(tripType == "SINGLE"){
          $("#y_oneWayRadbtnfare").prop("checked",true);
          if($("#menu1").hasClass("active")){
            $(".ui-depart").click();
          }
          $(".ui-return").hide();
          $(".ui-depart").addClass("cust-fullwidth");
          $("#different-passenger-inbound-btn-div").addClass("hidden");
          $("#different-vehicle-inbound-btn-div").addClass("hidden");
          $(".farevehicle_1").addClass("hidden");
        }else if(tripType == "RETURN"){
          $("#y_roundTripRadbtnfare").prop("checked",true);
          $(".ui-return").show();
          $(".ui-depart").removeClass("cust-fullwidth");
          $("#different-passenger-inbound-btn-div").removeClass("hidden");
          $("#different-vehicle-inbound-btn-div").removeClass("hidden");
        }
    },
    bindChangeTripTypeButton: function () {
        var $returnField = $(ACC.farefinder.componentParentSelector + ' .y_fareFinderReturnField');
        // Event handler for trip type radio buttons
        // Radio button show/hide
        if ($('#reservable').val() == 'false')
        {
            $("[name='routeInfoForm.tripType']").prop("checked", true);
            ACC.farefinder.renderDateUiBasedOnTripType();
        }
        $('#onewayfare').on('click', function () {

          ACC.farefinder.renderDateUiBasedOnTripType();
          $(".ui-return").trigger("setDate","");
        });
        $('#returnfare').on('click', function () {

          ACC.farefinder.renderDateUiBasedOnTripType();
          $(".ui-return").trigger("setDate","");
        });

    },

    bindTravelWithChildrenButton: function () {
        // Event handler for 'travelWithChildrenButton'
        $(".y_fareFinderChildrenTrigger").click(function () {
            $(this).hide();
            $(ACC.farefinder.componentParentSelector + " .y_fareFinderTravelingChildren").removeClass("hidden");
            $("#y_fareFinderTravelingWithChildren").val("true");
        });
    },


     showHotelOrVacationTab : function() {
       if (window.location.search.indexOf("hotel=true") >= 0){
        $('a[href="#tabs-3"]').click();
       }
       if (window.location.search.indexOf("vacation=true") >= 0){
               $('a[href="#tabs-2"]').click();
              }
     },


    setVehicleTravelRouteCode: function () {
      var tripType = $(".y_SelectTripTypefare.color-tab-active").val();
      fromLocation = $(".y_fareFinderOriginLocationCode").val();
      toLocation = $(".y_fareFinderDestinationLocationCode").val();
        $('#farevehicleTravelRouteCode_0').val(fromLocation+"-"+toLocation);
        if(tripType == "RETURN"){
          $('#farevehicleTravelRouteCode_1').val(toLocation+"-"+fromLocation);
        }
    },


    bindFareFinderValidation: function () {
        var $returnDateField = $(".y_fareFinderDatePickerReturning");
        var $departureDateField = $(".y_fareFinderDatePickerDeparting");

        $('#y_fareFinderEditForm').validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            submitHandler: function (form) {
              ACC.farefinder.setVehicleTravelRouteCode();
              $.when(ACC.services.checkAmendmentCartInSession()).then(
              function(data) {
                if(data.result == true){
                  $('#y_confirmFreshBookingModal').modal('show');
                }
                else {
                    var jsonData = ACC.formvalidation.serverFormValidation(form, "validateFareFinderFormAttributes");
                          if (!jsonData.hasErrorFlag) {
                              $('#y_processingModal').modal({
                                  backdrop: 'static',
                                  keyboard: false
                              });
                              form.submit();
                          }
                }
              }
            );
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            rules: {
                departureLocationName: {
                    required: {
                   depends:function(){
                   $(this).val($.trim($(this).val()));
                          return true;
                 }
              },
                    locationIsValid : ".y_fareFinderOriginLocationCode"
                },
                arrivalLocationName: {
                    required: {
                   depends:function(){
                   $(this).val($.trim($(this).val()));
                          return true;
                 }
              },
                    locationIsValid : ".y_fareFinderDestinationLocationCode"
                },
                cabinClass: "required"
            },
            messages: {
                departureLocationName: {
                    required: ACC.validationMessages.message("error.farefinder.from.location"),
                    locationIsValid: ACC.validationMessages.message("error.farefinder.from.locationExists")
                },
                arrivalLocationName: {
                    required: ACC.validationMessages.message("error.farefinder.to.location"),
                    locationIsValid: ACC.validationMessages.message("error.farefinder.to.locationExists")
                },
                cabinClass: ACC.validationMessages.message("error.farefinder.cabin.class")
            }
        });
        // add validation to departure date
        if ($departureDateField && $departureDateField.size() > 0) {
         //   ACC.farefinder.addDepartureFieldValidation($departureDateField);
        }

        // if it's a "round trip", we need to validate the return date field
        if ($('#y_roundTripRadbtn').is(":checked")) {
          //  ACC.farefinder.addReturnFieldValidation($returnDateField, $departureDateField.val());
        }

        // initialize date picker fields
        ACC.farefinder.addDatePickerForFareFinder($departureDateField, $returnDateField);
    },

    bindRemoveAmendmentCartBeforeSearch : function() {
    $("#y_freshBooking").click(
      function() {
        $.when(ACC.services.deleteAmendmentCartFromSession()).then(
          function(data) {
            if(data == true){
              $('#y_confirmFreshBookingModal').modal('hide');
              $('#fareFinderFindButton').click();
            }
          });
      });
  },

    bindFareFinderLocationEvents: function () {
        // validation events bind for y_fareFinderOriginLocation field
    $(".y_fareFinderOriginLocation").keydown(function(e) {
      if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_fareFinderOriginLocationCode").val()==''){
        if((e.keyCode || e.which) == 13){
          e.preventDefault();
        }

        // select the first suggestion
        var suggestions = $(this).parent().find(".autocomplete-suggestions");
        if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
          var firstSuggestion = suggestions.find("a:eq( 1 )");
          firstSuggestion.click();
        }
        $(this).valid();

      };
    });

    $(".y_fareFinderOriginLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
              $(".y_fareFinderOriginLocation").valid();
            }
        });

    $(".y_fareFinderOriginLocation").blur(function (e) {
      $(".y_fareFinderOriginLocation").valid();
    });

    $(".y_fareFinderOriginLocation").focus(function (e) {
      $(this).select();
    });

    // validation events bind for y_fareFinderDestinationLocation field
    $(".y_fareFinderDestinationLocation").keydown(function(e) {
      if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_fareFinderDestinationLocationCode").val()==''){
        if((e.keyCode || e.which) == 13){
          e.preventDefault();
        }

        // select the first suggestion
        var suggestions = $(this).parent().find(".autocomplete-suggestions");
        if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
          var firstSuggestion = suggestions.find("a:eq( 1 )");
          firstSuggestion.click();
        }
        $(this).valid();

      };
    });

    $(".y_fareFinderDestinationLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
              if($(".y_fareFinderDestinationLocation").length==1)
              {
                $(".y_fareFinderDestinationLocation").valid();
              }
            }
        });

    $(".y_fareFinderDestinationLocation").blur(function (e) {
      $(".y_fareFinderDestinationLocation").valid();
    });

    $(".y_fareFinderDestinationLocation").focus(function (e) {
      $(this).select();
    });
    },

    addDatePickerForFareFinder : function($departureDateField, $returnDateField){
         var todayDate = new Date();
         $departureDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'mm/dd/yy',
                onChangeMonthYear:function(year, month, inst){

                },
                beforeShowDay: function(date){
                  return ACC.farefinder.validateBeforeShowDate(date,$(this),"depart");
                },
                beforeShow: function (input) {

                    setTimeout(function() {
                        if($returnDateField.length){
                            $departureDateField.rules('remove')
                        }

                        }, 0);
                },
                onSelect: function (selectedDate) {
                    // add validation to departure date
                    var ele = $(this).siblings(".datepicker-input");


                    $(ele).val(selectedDate);
                    $(".ui-depart").find(".vacation-calen-year-txt").text(selectedDate);


                    $(".ui-return:visible").trigger("setDate",selectedDate);


                    $("#js-roundtrip").find("li.ui-depart").removeClass("active");
                    $("#js-roundtrip").find("div.depart-calendar").removeClass("active");

                }
            });
         if($returnDateField != undefined)
         {
        $returnDateField.datepicker({
            minDate: todayDate,
            maxDate: '+1y',
            dateFormat: 'mm/dd/yy',
            beforeShowDay: function(date){
              return ACC.farefinder.validateBeforeShowDate(date,$(this),"arrive");
            },
            beforeShow: function (input) {

                setTimeout(function() {
                    $returnDateField.rules('remove')
                    }, 0);
            },
            onSelect: function (selectedDate) {
              var ele = $(this).siblings(".datepicker-input");
              $(ele).val(selectedDate);
              $(".ui-return").find(".vacation-calen-year-txt").text(selectedDate);
              $(".ui-datepicker a").removeAttr("href");
              $(ele).change();
              $(".bc-dropdown.arrive").trigger("return-date-selected",{selectedDate:selectedDate});
              $("#js-roundtrip").find("li.ui-return").removeClass("active");
              $("#js-roundtrip").find("div.return-calendar").removeClass("active in");
            }
        });

          //binding input to datepicker
          $(".depart.datepicker-input").keyup(function(){
            $departureDateField.datepicker("setDate",$(this).val());
            var ele = $(this).closest(".tab-pane").attr("id");
            $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
            $(".ui-return").trigger("setDate",$(this).val());
          });

          $(".arrive.datepicker-input").keyup(function(){
            $returnDateField.datepicker("setDate",$(this).val());
            var ele = $(this).closest(".tab-pane").attr("id");
            $("#js-roundtrip").find(".nav-link[href='#"+ele+"']").find(".vacation-calen-year-txt").text($(this).val());
          })

         }

         $('#departingDateTime').keydown(function(e) {
             e.preventDefault();
             return false;
          });
           $('#returnDateTime').keydown(function(e) {
               e.preventDefault();
               return false;
            });


    },

    addDepartureFieldValidation: function ($departureDateField) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();

        $departureDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateUK,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.validationMessages.message("error.farefinder.departure.date")
            }
        });
    },

    setReturnFieldMinDate:function($returnDateField, selectedDate,isUKDate){
      var selectedUSDate=new Date();
      if(selectedDate){
         selectedUSDate=selectedDate;
         if(isUKDate){
           selectedUSDate=ACC.travelcommon.convertToUSDateFareFinder(selectedDate);
         }
      }
       var minDateForReturnDate=ACC.travelcommon.addDaysToUsDate(selectedUSDate,ACC.farefinder.minRelativeReturnDate);
       $returnDateField.datepicker("option", "minDate", minDateForReturnDate);
    },

    addReturnFieldValidation: function ($returnDateField, selectedMinDate) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();
        if (selectedMinDate) {
            minDateUK = selectedMinDate;
        }

        if(!$returnDateField.length){
            return;
        }

        var selectedUSDate=ACC.travelcommon.convertToUSDateFareFinder(minDateUK);
      var minDateForReturnDate=ACC.travelcommon.addDays(selectedUSDate,ACC.farefinder.minRelativeReturnDate);

        $returnDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateForReturnDate,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.validationMessages.message("error.farefinder.arrival.date"),
                dateGreaterEqualTo: ACC.validationMessages.message("error.farefinder.arrival.dateGreaterEqualTo")
            }
        });
    },

    hideReturnField : function(){
     var $returnField = $('.y_fareFinderReturnField');
        $returnField.find("input").rules("remove");
   },

   showReturnField : function(){
     var $returnField = $('.y_fareFinderReturnField');
   },


    /// auto suggestion //
    getTravelOriginMatches: function(travelSectors,locationText) {
      locationText=locationText.toLowerCase();
    var filterTravelSectors=new Object();

    if(!$.isEmptyObject(travelSectors)) {
        var matchingTravelSectors=[];
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
          if(travelSector.origin != null && travelSector.origin.code.toLowerCase()==locationText) {
            matchingTravelSectors.push(travelSector);
            break;
          }
        }


        if($.isEmptyObject(matchingTravelSectors)) {
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
            if( travelSector.origin != null && travelSector.origin.name.toLowerCase().indexOf(locationText) != -1) {
              matchingTravelSectors.push(travelSector);
            }
          }
        }

        if($.isEmptyObject(matchingTravelSectors)) {
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
          if (travelSector.origin != null && (travelSector.origin.location.code.toLowerCase() == locationText || travelSector.origin.location.name.toLowerCase().includes(locationText))) {
            matchingTravelSectors.push(travelSector);
            }
          }
        }

        for(var travelSectorIndex=0; travelSectorIndex<matchingTravelSectors.length; travelSectorIndex++) {
          var travelSector=matchingTravelSectors[travelSectorIndex];
          var code=travelSector.origin.code;
          if(!filterTravelSectors[code]) {
            filterTravelSectors[code]=travelSector;
          }
        }
      }

      return filterTravelSectors;
    },

    getTravelDestinationMatches: function(travelSectors,locationText) {
      locationText=locationText.toLowerCase();
    var filterTravelSectors=new Object();

    if(!$.isEmptyObject(travelSectors)) {
        var matchingTravelSectors=[];
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
          if(travelSector.destination != null && travelSector.destination.code.toLowerCase()==locationText) {
            matchingTravelSectors.push(travelSector);
          }
        }

        if($.isEmptyObject(matchingTravelSectors)) {
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
            if( travelSector.destination != null && travelSector.destination.name.toLowerCase().indexOf(locationText) != -1) {
              matchingTravelSectors.push(travelSector);
            }
          }
        }
      if ($.isEmptyObject(matchingTravelSectors)) {
        for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
          var sector = travelSectors[travelSectorIdx];
          if (sector.destination != null
              && (sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
            matchingTravelSectors.push(sector);
          }
        }
      }

        if(!$.isEmptyObject(matchingTravelSectors))
          filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0] ;
      }

      return filterTravelSectors;
    },

    fireQueryToFetchAllRouteInfo: function(){
      if(true)
      {
        $.when(ACC.services.getRouteInfoAjax()).then(function(data) {
          ACC.farefinder.allRouteInfo=data;
          ACC.farefinder.routeVersion=1;
          localStorage.setItem("routeInfo", JSON.stringify(ACC.farefinder.allRouteInfo));
          localStorage.setItem("routeVersion", ACC.farefinder.routeVersion);
        });
      }else{
        ACC.farefinder.allRouteInfo=JSON.parse(localStorage.getItem("routeInfo"));
      }
      ACC.farefinder.bindRouteInfoOriginDropDown();
      ACC.farefinder.bindRouteInputToDropDown("#fromLocationDropDown");
      ACC.farefinder.bindRouteInputToDropDown("#toLocationDropDown");
      $("body").trigger("routeInfoSet");
    },

    bindRouteInputToDropDown : function(ele){
      var inputSel = $(ele).data("inputselector");
      var inputVal = $("#"+inputSel).val();
      if(inputVal != "" && inputVal != undefined){
        inputVal = inputVal.split(" - ");
        var city = inputVal[0];
        var terminal = "";
        var cityVal ="";
        if(inputVal[1] != undefined){
          terminal = inputVal[1].split("(");
          terminal = terminal[0];
          cityVal = inputVal[1].split("(");
        }else{
            terminal = inputVal[0].split("(");
          terminal = terminal[0];
          cityVal = inputVal[0].split("(");
        }

        if(cityVal[0]){
        $(ele).find(".dropdown-text strong").html(cityVal[0]);
        }
        else{
        $(ele).find(".dropdown-text strong").html(city);
        }
      if(terminal != "" && cityVal[1] != undefined){
          $(ele).find(".dropdown-text span").html('('+cityVal[1]);
        }
      else {
            $(ele).find(".dropdown-text span").html('');
      }
      }
    },

    renderRouteDropDownHtml : function(routes,bookableMap,destinationIsWalkOnMap,destinationRouteTypeMap,motorCycleAllowedMap,allowAdditionalPassengerTypesMap){
      var html = '<h3><span class="dropdown-text"><strong>'+ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")+'</strong> <br> <span class="terminal-text">'+ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.subheading")+'</span></span> <span class="custom-arrow"></span></h3><div class="vacation-range-box">  <div class="form-height-fix style-3">';
        if(bookableMap!=undefined){
          html+=' <span class="first-span pl-5 pb-3">Bookable online </span>';
        }
        _.forEach(routes,function(val,key){
          if(val.geoGraphicalArea.name != null && val.geoGraphicalArea.name != ""){
            html+='<ul><span class="second-span pl-5 pb-3">'+val.geoGraphicalArea.name+'</span>';
          }else{
            html+='<ul><span class="second-span pl-5 pb-3">'+val.geoGraphicalArea.code+'</span>';
          }
          _.forEach(val.area,function(el){
            var dest = "";
            _.forEach(el.destinationRoutes,function(destination){
              if(ACC.farefinder.destinationRoute == undefined){
                ACC.farefinder.destinationRoute = {};
              }
              ACC.farefinder.destinationRoute[destination.destination] = destination;
              var isWalkOn = "false";
              var routeType = "";
              var isBookable = "0";
              var motorCycleAllowed = "false";
              var allowAdditionalPassengerTypes = "false";

              if(destination.isWalkOn)
                isWalkOn="true";

              if(destination.routeType!=undefined)
                routeType=destination.routeType;

             if(bookableMap != undefined && bookableMap[destination.code]){
            	  isBookable = "1";
              }
              else if(destination.isBookable){
            	  isBookable = "1";
              }
               

              if(destination.motorcycleAllowed)
                motorCycleAllowed = "true";

              if(destination.allowAdditionalPassengerTypes)
                allowAdditionalPassengerTypes = "true";

              dest+="|"+destination.destination+":"+isWalkOn+":"+routeType+":"+isBookable+":"+motorCycleAllowed+":"+allowAdditionalPassengerTypes;

            });
            if(dest!=""){
              dest = dest.substr(1);
            }

            var extra = "";
            if(destinationRouteTypeMap!=undefined && destinationRouteTypeMap[el.code]!=undefined){
              extra += " data-routetype="+destinationRouteTypeMap[el.code];
            }

            if(destinationIsWalkOnMap!=undefined && destinationIsWalkOnMap[el.code]!=undefined){
                extra += " data-iswalkon="+destinationIsWalkOnMap[el.code];
              }
              if(destinationRouteTypeMap==undefined){
                  extra += " data-destination="+dest;
              }

            if(motorCycleAllowedMap!=undefined && motorCycleAllowedMap[el.code]!=undefined){
              extra += " data-motorcycleallowed="+motorCycleAllowedMap[el.code];
            }
            if(allowAdditionalPassengerTypesMap!=undefined && allowAdditionalPassengerTypesMap[el.code]!=undefined){
              extra += " data-allowadditionalpassengertypes="+allowAdditionalPassengerTypesMap[el.code];
            }
              if(bookableMap != undefined && bookableMap[el.code]!= undefined && bookableMap[el.code]==1){
                if(el.isBookable== false){
                  html+='<li> <a  style="color: #666666;"class="sub-link bookable" '+extra+' data-code="'+el.code+'" data-travelroute="'+el.travelRouteName+'" data-city="'+el.city.name+'" data-terminal="'+el.name+'">*'+el.name+'</a></li>';
                }else {
                   html+='<li> <a class="sub-link bookable" '+extra+' data-code="'+el.code+'" data-travelroute="'+el.travelRouteName+'" data-city="'+el.city.name+'" data-terminal="'+el.name+'">'+el.name+'</a></li>';
                }
              }else if(bookableMap != undefined && bookableMap[el.code]!= undefined && bookableMap[el.code]==0){
            	  html+='<li> <a  style="color: #666666;" class="sub-link non-bookable" '+extra+' data-code="'+el.code+'" data-travelroute="'+el.travelRouteName+'" data-city="'+el.city.name+'" data-terminal="'+el.name+'">*'+el.name+'</a></li>';
              }
              else{
                if(el.isBookable== false){
                  html+='<li> <a  style="color: #666666;" class="sub-link non-bookable" '+extra+' data-code="'+el.code+'" data-travelroute="'+el.travelRouteName+'" data-city="'+el.city.name+'" data-terminal="'+el.name+'">*'+el.name+'</a></li>';
                }else {
                   html+='<li> <a class="sub-link bookable" '+extra+' data-code="'+el.code+'" data-travelroute="'+el.travelRouteName+'" data-city="'+el.city.name+'" data-terminal="'+el.name+'">'+el.name+'</a></li>';
                }
             }
          })
          html+='</ul>';
        })
        html+='</div></div></div></div>';
        return html;
      },
    bindRouteInfoOriginDropDown: function(){
      var travelSectors=ACC.farefinder.allRouteInfo;
      var filteredRoutes = ACC.farefinder.categorizeTravelSectors(travelSectors);
      var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes);

      $("#fromLocationDropDown").html(filteredRouteHtml);
      $("#fromLocationDropDown").find('.sub-link').click(ACC.farefinder.sourceClickHandler);
      $("#fromLocationDropDown").accordion({
        header: "h3",
        collapsible: true,
        active: false,
        beforeActivate: function( event, ui ) {
          var wd = $(event.currentTarget).outerWidth();
			    $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
        }
      });
      $("#ui-id-5").find(".style-3").prepend('<label class="mb-0">*'+ACC.validationMessages.message('label.ferry.farefinder.routeinfo.bookableOnline')+'</label>');

      $("#toLocationDropDown").html(filteredRouteHtml);
      $("#toLocationDropDown").find('.sub-link').click(ACC.farefinder.destinationClickHandlerReverse);
      $("#toLocationDropDown").accordion({
        header: "h3",
        collapsible: true,
        active: false,
        beforeActivate: function( event, ui ) {
          var wd = $(event.currentTarget).outerWidth();
			    $(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
        }
      });
      $("#ui-id-7").find(".style-3").prepend('<label class="mb-0">*'+ACC.validationMessages.message('label.ferry.farefinder.routeinfo.bookableOnline')+'</label>');



      $("#fromLocationDropDown").click(function(){
        $("#toLocationDropDown").closest(".ui-accordion").accordion({active:2});
      });

      $("#toLocationDropDown").click(function(){
        $("#fromLocationDropDown").closest(".ui-accordion").accordion({active:2});
      });

      setTimeout(function(){
        $("body").trigger("setSearchDropDown",$("#toLocationDropDown"));
      },200);
      var dt = new Date();


      if($(".vacation-ui-depart .current-year-custom").length){
            $(".vacation-ui-depart .current-year-custom").html(dt.getFullYear());
      }


      if($(".vacation-ui-return .current-year-custom").length){
            $(".vacation-ui-return .current-year-custom").html(dt.getFullYear());
      }

      if($(".ui-depart .current-year-custom").length && $(".ui-depart .current-year-custom").text()==""){
            $(".ui-depart .current-year-custom").html(dt.getFullYear());
      }

      if($(".ui-return .current-year-custom").length){

          if($(".ui-return .current-year-custom").text()==""){
            $(".ui-return .current-year-custom").html(dt.getFullYear());
          }else{
             if($(".y_fareFinderDatePickerReturning") != undefined){
                $(".y_fareFinderDatePickerReturning").datepicker("setDate",$(".ui-return .current-year-custom").text());
              }
          }
      }
      $(".current-year-custom").removeClass(".current-year-custom");
    },

    bindDropDownselection : function(el){
      $(el).closest(".ui-accordion").accordion({active:2});
      var city = $(el).data("city");
      var terminal = $(el).data("terminal");
      var code = $(el).data("code");
      var travelroute = $(el).data("travelroute");
      if(city == ""){
        city = el.text();
      }
      var eleText = $(el).closest(".ui-accordion").find(".dropdown-text");
      if (terminal !== undefined)
      {
        var cityVal = terminal.split("(");
          if(cityVal[0]) {
          eleText.find("strong").html(cityVal[0]);
          }
          else {
          eleText.find("strong").html(city);
          }

          if(terminal != "" && cityVal[1] != undefined){
              eleText.find("span").html('('+cityVal[1]);
          }
          else{
           eleText.find("span").html('');
          }
      }
      $(el).closest(".accordion-34").trigger("uiChange","show");
      var inputSel = $(el).closest(".ui-accordion").data("inputselector");
      var routeInfoSel = $(el).closest(".ui-accordion").data("routeinfoselector");
      var suggestionSel = $(el).closest(".ui-accordion").data("suggestionselector");
      if(inputSel != undefined){
        ACC.farefinder.bindDropDownValueToInput({city:city, terminal:terminal, code:code},"#"+inputSel);
      }

      if(routeInfoSel != undefined){
        ACC.farefinder.bindDropDownValueToRouteInfo({city:city, terminal:terminal, code:code},"."+routeInfoSel);
      }

      if(suggestionSel != undefined){
        ACC.farefinder.bindDropDownValueToSuggestion({suggestion:"AIRPORTGROUP"},"."+suggestionSel);
      }
    },
    bindDropDownValueToInput : function(value,ele){
      var combVal = value.city+" - "+value.terminal+"("+value.code+")";
      $(ele).val(combVal);
    },
    bindDropDownValueToRouteInfo : function(value,ele){
      $(ele).val(value.code);
    },
    bindDropDownValueToSuggestion : function(value,ele){
      $(ele).val(value.suggestion);
    },
    destinationClickHandlerReverse: function(){
      ACC.farefinder.bindDropDownselection($(this));
      var code = $(this).data("code");
      $("#arrivalLocation").val(code);
      var filteredSource = [];
      _.forEach(ACC.farefinder.allRouteInfo,function(val){
        _.forEach(val.destinationRoutes,function(dest){
          if(dest.destination == code){
            filteredSource.push(val);
          }
        })
      })
      var flags = [], allfilteredSource = [], l = filteredSource.length, i;
      for( i=0; i<l; i++) {
        if( flags[filteredSource[i].code]) continue;
        flags[filteredSource[i].code] = true;
        allfilteredSource.push(filteredSource[i]);
      }
      var filteredRoutes = ACC.farefinder.categorizeTravelSectors(allfilteredSource);
      var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes);

      var htmlEl = $('<div class="accordion-34" id="fromLocationDropDown" data-inputselector="fromLocation" data-routeinfoselector="y_fareFinderOriginLocationCode"  data-suggestionselector="y_fareFinderDestinationLocationSuggestionType"></div>');
      htmlEl.append(filteredRouteHtml);
      $("#fromLocationDropDown").replaceWith(htmlEl);
      $("#fromLocationDropDown").find('.sub-link').click(ACC.farefinder.sourceClickHandlerReverse);
      $("#fromLocationDropDown").find('.sub-link').data("destCode",code);
      $("#fromLocationDropDown").accordion({ header: "h3", collapsible: true, active: false });
      $("#fromLocationDropDown").closest(".bc-accordion").trigger("searchOnKeyPress");
      $("#ui-id-21").find(".style-3").prepend('<label class="mb-0">*'+ACC.validationMessages.message('label.ferry.farefinder.routeinfo.bookableOnline')+'</label>');
      setTimeout(function(){
        $("body").trigger("setSearchDropDown",$("#fromLocationDropDown"));
      },200);
      $("#routeType").val("");

    },
    sourceClickHandlerReverse : function(){
      ACC.farefinder.bindDropDownselection($(this));
      var code = $(this).data("code");
      var destCode = $(this).data("destCode");
      var isWalkOn = false;
      var routeType = "SHORT";
      var isBookable = true;

      $("#departureLocation").val(code);
      _.forEach(ACC.farefinder.allRouteInfo,function(val){
        if(val.code == code){
          _.forEach(val.destinationRoutes,function(dest){
            if(dest.destination == destCode){
              isBookable = dest.isBookable;
              isWalkOn = dest.isWalkOn;
              routeType = dest.routeType;
            }
          })
        }
      })

      $("body").trigger("routeType",{routeType:routeType});

       $("#isWalkOnRoute").val(isWalkOn);
       $("#routeType").val(routeType);

      if(isBookable){
        $(".confirm-btn-bookable").removeClass("hidden");
        $(".confirm-btn-non-bookable").addClass("hidden");
        $(".custom-error-1").addClass("hidden");
        $(".booking-selector").removeClass("hidden");
        if($(".ui-flow-1:visible").length == 0){
          $(".ui-flow-1").show();
          if(ACC.farefinder.routeTypeSel != undefined){
            $("#"+ACC.farefinder.routeTypeSel).attr("checked","checked");
            ACC.farefinder.renderDateUiBasedOnTripType();
          }
        }
      }else{
        $(".confirm-btn-bookable").addClass("hidden");
        $(".confirm-btn-non-bookable").removeClass("hidden");
        $(".custom-error-1").removeClass("hidden");
        $(".booking-selector").addClass("hidden");

        //save route type for restore.

        ACC.farefinder.routeTypeSel = $("input[name='routeInfoForm.tripType']:checked").attr("id");;

        //Selecting one way
        $("#y_oneWayRadbtn").click();
        $(".ui-flow-1").hide();
        ACC.farefinder.renderDateUiBasedOnTripType();
      }
       ACC.farefinder.setUIforLongRouteType();

    },
    sourceClickHandler : function(event,ui){
      ACC.farefinder.bindDropDownselection($(this));
      var destination = $(this).data("destination").split("|");
      var filteredDestination = {};
      var destinationIsWalkOnMap = {};
      var destinationRouteTypeMap = {};
      var motorCycleAllowedMap = {};
      var allowAdditionalPassengerTypesMap = {};
      _.forEach(destination,function(val,key){
        var fD = val.split(":");
        filteredDestination[fD[0]] = fD[3];
        destinationIsWalkOnMap[fD[0]] = fD[1];
        destinationRouteTypeMap[fD[0]] = fD[2];
        motorCycleAllowedMap[fD[0]] = fD[4];
        allowAdditionalPassengerTypesMap[fD[0]] = fD[5];
      });

      var code = $(this).data("code");
      $("#departureLocation").val(code);
      $(".schedule-finder-source-location-code").text(code);
      $("#y_scheduleDepartureLocationTravelroute").html($(this).data("travelroute"));
      destination = _.keys(filteredDestination);
      var filteredRoutes  = ACC.farefinder.allRouteInfo;
      filteredRoutes = _.filter(filteredRoutes,function(el){ if(_.indexOf(destination,el.code)!=-1){ return true;} return false;})
      filteredRoutes = ACC.farefinder.categorizeTravelSectors(filteredRoutes);
      
      var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes,filteredDestination,destinationIsWalkOnMap,destinationRouteTypeMap,motorCycleAllowedMap,allowAdditionalPassengerTypesMap);

      var htmlEl = $('<div class="accordion-34" id="toLocationDropDown" data-inputselector="toLocation" data-routeinfoselector="y_fareFinderDestinationLocationCode"  data-suggestionselector="y_fareFinderOriginLocationSuggestionType"></div>');
      htmlEl.append(filteredRouteHtml);
      $("#toLocationDropDown").replaceWith(htmlEl);
      $("#toLocationDropDown").find('.sub-link').click(ACC.farefinder.destinationClickHandler);
      $("#toLocationDropDown").accordion({ header: "h3", collapsible: true, active: false });
      $("#toLocationDropDown").closest(".bc-accordion").trigger("searchOnKeyPress");
      $("#routeType").val("");
      $("#ui-id-21").find(".style-3").prepend('<label class="mb-0">*'+ACC.validationMessages.message('label.ferry.farefinder.routeinfo.bookableOnline')+'</label>');
    },
    calenderBlockDate: function(date,ele,origin,destination){
      var currMonth = date.getMonth();
      currMonth += 1;
      var currYear = date.getFullYear();
      var currDt = date.getDate();
      var parsedData = ele.data("selDate");

      // Call api if data is not present in array for month and year
      if(parsedData == undefined || parsedData[currYear+"-"+currMonth] == undefined){
        ACC.services.getDepartureDates(origin,destination,currMonth,currYear).then(function(data){
          var parsedData = {};
          if(ele.data("selDate") != undefined){
            parsedData = ele.data("selDate");
          }
          _.forEach(data,function(val){
            parsedData[val.year+"-"+val.month] = {};
            _.forEach(val.dates,function(dt){
              parsedData[val.year+"-"+val.month][val.year+"-"+val.month+"-"+dt] = true;
            })
          })
          if(parsedData[currYear+"-"+currMonth] == undefined){
            parsedData[currYear+"-"+currMonth]={};
          }
          // setting data into html element
          ele.data("selDate",parsedData);
        });
      }

        if(parsedData !== undefined && parsedData[currYear+"-"+currMonth] !== undefined && parsedData[currYear+"-"+currMonth][currYear+"-"+currMonth+"-"+currDt] !== undefined){
          return [true]
        }else{
          return [false]
        }
    },
    validateBeforeShowDate : function(date,ele,tripType){
      // Return true for short trip
      if($("#routeType").val() !== "LONG"){
        return [true];
      }

      // Origin and departure as per trip type
      if(tripType == "depart"){
        var origin = $(".y_fareFinderOriginLocationCode").val();
        var destination = $(".y_fareFinderDestinationLocationCode").val();
      }else{
        var origin = $(".y_fareFinderDestinationLocationCode").val();
        var destination = $(".y_fareFinderOriginLocationCode").val();
      }

      return ACC.farefinder.calenderBlockDate(date,ele,origin,destination);
    },
    setUIforLongRouteType : function(){

      if($("#check-in-datepicker").length !== 0){
          var departDate = $("#check-in-datepicker").datepicker( "getDate" );
          $("#check-in-datepicker").datepicker( "refresh" );
          var departDateValidate = ACC.farefinder.validateBeforeShowDate(departDate,$("#check-in-datepicker"),"depart");
          if(!departDateValidate[0]){
            $("#check-in-datepicker").siblings("input.datepicker-input").val("");
          }
      }

      if($("#check-out-datepicker").length !== 0){
        var arriveDate = $("#check-out-datepicker").datepicker( "getDate" );
        $("#check-out-datepicker").datepicker( "refresh" );
        var arriveDateValidate = ACC.farefinder.validateBeforeShowDate(arriveDate,$("#check-out-datepicker"),"arrive");
        if(!arriveDateValidate[0]){
          $("#check-out-datepicker").siblings("input.datepicker-input").val("");
        }
      }

      if($("#datepicker-schedule").length !== 0){
        var arriveDate = $("#datepicker-schedule").datepicker( "getDate" );
        $("#datepicker-schedule").datepicker( "refresh" );
        var departureLocation = $("#fromScheduleLocation").val();
        var arrivalLocation = $("#toScheduleLocation").val();
        var dateValidate = ACC.farefinder.calenderBlockDate(arriveDate,$("#datepicker-schedule"),departureLocation,arrivalLocation);
        if(!dateValidate[0]){
          $("#datepicker-schedule").siblings("input.datepicker-input").val("");
        }
      }

    },
    destinationClickHandler : function(event,ui){
      ACC.farefinder.bindDropDownselection($(this));
      var isWalkOn = $(this).data("iswalkon");
      var routeType = $(this).data("routetype");

      $("body").trigger("routeType",{routeType:routeType});

       $("#isWalkOnRoute").val(isWalkOn);
       if(isWalkOn){
         $(".travel-mode-selection").removeClass("hidden");
       }
       else{
         $(".travel-mode-selection").addClass("hidden");
       }
       $("#routeType").val(routeType);
       if ($(this).data("allowadditionalpassengertypes"))
       {
            $(".northernPassengersTypesDiv").show();
       }
       else
       {
            $(".northernPassengersTypesDiv").hide();
       }
       if ($(this).data("motorcycleallowed"))
       {
            $(".motorCycleSelectionDiv").show();
       }
       else
       {
            $(".motorCycleSelectionDiv").hide();
       }

       if ($("#isWalkOnRoute").val() == 'true')
       {
           $("#fareCalculatorOutboundWalkOnOptions").show();
       }
       else
       {
            $("#fareCalculatorOutboundWalkOnOptions").hide();
       }
      if($(this).hasClass("bookable")){
        $(".confirm-btn-bookable").removeClass("hidden");
        $(".confirm-btn-non-bookable").addClass("hidden");
        $(".custom-error-1").addClass("hidden");
        $(".booking-selector").removeClass("hidden");
        if($(".ui-flow-1:visible").length == 0){
          $(".ui-flow-1").show();
          if(ACC.farefinder.routeTypeSel != undefined){
            $("#"+ACC.farefinder.routeTypeSel).attr("checked","checked");
            ACC.farefinder.renderDateUiBasedOnTripType();
          }
        }
        var code = $(this).data("code");
        $("#arrivalLocation").val(code);
        $(".schedule-finder-destination-location-code").text(code);
        $("#y_scheduleArrivalLocationTravelroute").html($(this).data("travelroute"));
      }else{
        $(".confirm-btn-bookable").addClass("hidden");
        $(".confirm-btn-non-bookable").removeClass("hidden");
        $(".custom-error-1").removeClass("hidden");
        $(".booking-selector").addClass("hidden");

        //save route type for restore.

        ACC.farefinder.routeTypeSel = $("input[name='routeInfoForm.tripType']:checked").attr("id");

        //Selecting one way
        ACC.farefinder.renderDateUiBasedOnTripType();
        $(".schedule-finder-destination-location-code").text($(this).data("code"));
      $("#y_scheduleArrivalLocationTravelroute").html($(this).data("travelroute"));
      }
      ACC.farefinder.setUIforLongRouteType();
      ACC.carousel.adjustSearchHeight();
    },
    calculateFareHandler : function(ele){
     var dLoc = $(".y_fareFinderOriginLocationCode").val();
          var aLoc = $(".y_fareFinderDestinationLocationCode").val();
          var schDate = $(".depart.datepicker-input").val();
           var dLocName = $(".y_fareFinderOriginLocation").val();
           var aLocName = $(".y_fareFinderDestinationLocation").val();
           var dLocSuggType = $(".y_fareFinderOriginLocationSuggestionType").val();
           var aLocSuggType = $(".y_fareFinderDestinationLocationSuggestionType").val();

          var url = $(ele).data("link")+"?departureLocation="+dLoc+"&arrivalLocation="+aLoc+"&departureLocationName="+dLocName+"&arrivalLocationName="+aLocName+"&scheduleDate="+schDate.split(' ').join('+');
      if(ACC.farefinder.book_a_sailing_validation()){
        ACC.farefinder.redirectToURl(url);
      }
    },
    scheduleListingHandler : function(ele){
      var dLoc = $(".y_fareFinderOriginLocationCode").val();
      var aLoc = $(".y_fareFinderDestinationLocationCode").val();
      var schDate = $(".depart.datepicker-input").val();

        var url = $(ele).data("link")+"-/"+dLoc+"-"+aLoc+"?scheduleDate="+schDate.split(' ').join('+');
      if(ACC.farefinder.book_a_sailing_validation()){
          ACC.farefinder.redirectToURl(url);
        }
    },
    redirectToURl : function(url){
      window.location.href = url;
    },
    categorizeTravelSectors : function(ele){
      var filteredEle = {};
      _.forEach(ele, function(val,key){

        //adding initial geoGraphicalArea value
        if(filteredEle[val.geoGraphicalArea.code] == undefined){
          filteredEle[val.geoGraphicalArea.code] = {geoGraphicalArea:val.geoGraphicalArea};
          filteredEle[val.geoGraphicalArea.code].area = [];
        }

        //adding element of same category
        filteredEle[val.geoGraphicalArea.code].area.push(val);

      });

      return filteredEle;
    },

    getTravelSectorsWithOrigin: function(travelSectors, originCode) {
    var matchingTravelSectors=[];
      if(!$.isEmptyObject(travelSectors)) {
        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
          var travelSector=travelSectors[travelSectorIndex];
          if(travelSector.origin != null && (travelSector.origin.code.toLowerCase()==originCode || travelSector.origin.location.code.toLowerCase()==originCode)) {
            matchingTravelSectors.push(travelSector);
          }
        }
      }
      return matchingTravelSectors;
    },

            returnPassengerDetails: function(){
                 $("#y_differentPassengerInReturn").change(function() {
                            if(this.checked) {
                                $("#y_returnPassenger").removeClass("hidden");
                                }else{
                                $("#y_returnPassenger").addClass("hidden");
                                }
                            $("#accordion-returnPassenger").accordion({ header: "h3", collapsible: true, active: false });
                            $("#accordion-returnPassenger").accordion('refresh');
                            $("#accordion-nr-inbound").accordion('refresh');
                            });

                       },

    travellingWithVehicleCheckBox: function(){
    $("#faretravellingWithVehiclecheckbox").change(function() {
        if(this.checked) {
            $(".farevehicleDetails").removeClass("hidden");
            $('#farey_standardRadbtn_0').prop('checked', true);
            $('#farey_standardRadbtn_0').trigger('click');
          }
        else{
              $(".farevehicleDetails").addClass("hidden");
              $('#farey_standardRadbtn_0').prop('checked', false);
              if($('#farevehicleinboundcheckbox').is(":checked")){
                $("#farevehicleinboundcheckbox").trigger('click');
              }


        }

        });

    },

    selectVehicleType: function(){
        $('.farey_fareFinderVehicleTypeBtn').on('click',function () {
          $('.subOversizeVehicle').addClass("hidden");
          var id = $(this).attr('id');
          var vehicleType = $(this).val();
          var buttonIndex = id.charAt(id.length-1);
          ACC.farefinder.refreshVehicles(vehicleType,buttonIndex);

          });
       },

     showRemoveSailingModal:function(){

       $(".removeSailingButton").on("click", function(e) {
            $('#removeFerryUrl').val($(this).find("#removeSailingURL").val());
            $('#y_showRemoveSailingsModal').modal('show');
        })
        $("#y_removeSailing").on('click',function(e){
             $('#y_showRemoveSailingsModal').modal('hide');
             $("#removeFerryForm").attr('action', $('#removeFerryUrl').val());
             $("#removeFerryForm").submit();
        })
     },

     refreshVehicles: function(vehicleType,index){
         $('#farey_vehicleSubTypeSelect_'+index+ ' option').removeClass('hidden');
         $('#farey_vehicleSubTypeSelect_'+index).val('-1');
         $('#farey_vehicleSubTypeSelect_'+index+' option[value=-1]').removeClass();
         $('#farey_vehicleSubTypeSelect_'+index+' option[value=-1]').addClass(vehicleType);
         $('#farey_vehicleSubTypeSelect_'+index+' option[value=-1]').attr('selected','selected');
          $('#farey_vehicleSubTypeSelect_'+index+' option').each(function(){
          if(!$(this).hasClass(vehicleType)){
            $(this).addClass('hidden');
          }
        })

     },
     keyBindtoDecimal : function(e,ele){
       if(ele.data("prevKey") == undefined){
         prevKey = false;
       }else{
         prevKey = ele.data("prevKey");
       }
       if((( e.which >= 48 && e.which <= 57) || e.which==190)){
         if((ele.val().length >= 3 && ele.val().indexOf(".") == -1 && !prevKey && e.which!==190) || (ele.val().length >= 6 ))
           e.preventDefault();
       }
       if(e.which == 190){
           prevKey = true;
       }
       else {
         prevKey = false;
       }
       if(e.which == 8 && ele.val().length==5){
         prevKey = true;
       }
       ele.data("prevKey",prevKey);
     },
     convertVehicleDimensionInMeters : function() {

    $('.farelengthInput')
        .keyup(
            function(e) {
            ACC.farefinder.calcFareLengthInput($(this));
            }).keydown(function(e){
              ACC.farefinder.keyBindtoDecimal(e,$(this));
              setTimeout(function(){
                if($(this).val()==""){
                  $(this).parents("#vehicle_length_inputs").find("input[id^='lengthboxinm_']").val("");
                }
              }.bind(this),1);
            })

            $('.farelengthInputInMtrs')
                                .keyup(
                                    function(e) {
                                                    var index='';
                                      var lengthText = $.trim($(this).val());
                                      if(lengthText !== ""){

                                          var feet = parseFloat(lengthText) * 3.2808;
                                          var lengthInFeets = feet.toFixed(2);;
                                          if((e.which>=48 && e.which<=57)||e.which==190||e.which==8||e.which==46||e.which==86){
                                            if(lengthInFeets!=NaN){
                                              $(this).parents("#vehicle_length_inputs").find(".farelengthInput").val(lengthInFeets);
                                            } else {
                                                $(this).parents("#vehicle_length_inputs").find(".farelengthInput").val("");
                                            }
                                          }
                                      }
                                    }).keydown(function(e){
                                      ACC.farefinder.keyBindtoDecimal(e,$(this));

                                      setTimeout(function(){
                                        if($(this).val()==""){
                                          $(this).parents("#vehicle_length_inputs").find(".farelengthInput").val("");
                                        }
                                      }.bind(this),1)

                                    })

    $('.fareheightInput')
        .keyup(function(e){

            ACC.farefinder.calcFareInputHeight($(this));
        }).keydown(function(e){
              ACC.farefinder.keyBindtoDecimal(e,$(this));
              setTimeout(function(){
                if($(this).val()==""){
                  $(this).parents("#vehicle_height_inputs").find("input[id^='heightinm_']").val("");
                }
              }.bind(this),1);
            })

            $('.fareheightInputInMtrs')
                                .keyup(
                                    function(e) {
                                      var index='';
                                      var heightText = $.trim($(this).val());
                                      if(heightText !== ""){

                                          var feet = parseFloat(heightText) * 3.2808;
                                          var heightInFT = feet.toFixed(2);
                                          if((e.which>=48 && e.which<=57)||e.which==190||e.which==8||e.which==46||e.which==86){
                                            if(heightInFT!=NaN){
                                                   $(this).parents("#vehicle_height_inputs").find(".fareheightInput").val(heightInFT);
                                            } else {
                                                $(this).parents("#vehicle_height_inputs").find(".fareheightInput").val("");
                                            }
                                          }
                                      }
                                    }).keydown(function(e){
                                      ACC.farefinder.keyBindtoDecimal(e,$(this));
                                      setTimeout(function(){
                                        if($(this).val()==""){
                                          $(this).parents("#vehicle_height_inputs").find(".fareheightInput").val("");
                                        }
                                      }.bind(this),1);
                                    })

    $('.fareWidthInput')
        .keyup(function(e){
            ACC.farefinder.calcFareInputWidth($(this));
        }).keydown(function(e){
              ACC.farefinder.keyBindtoDecimal(e,$(this));
              setTimeout(function(){
                if($(this).val()==""){
                  $(this).parents("#vehicle_width_inputs").find("input[id^='widthinm_']").val("");
                }
              }.bind(this),1);
            })
    $('.fareWidthInputInMtrs')
        .keyup(
            function(e) {
              var index='';
              var widthText = $.trim($(this).val());
              if(widthText !== ""){

                  var feet = parseFloat(widthText) * 3.2808;
                  var widthInFT = feet.toFixed(2);
                  if((e.which>=48 && e.which<=57)||e.which==190||e.which==8||e.which==46||e.which==86){
                    if(widthInFT!=NaN){
                           $(this).parents("#vehicle_width_inputs").find(".fareWidthInput").val(widthInFT);
                    } else {
                        $(this).parents("#vehicle_width_inputs").find(".fareWidthInput").val("");
                    }
                  }
              }
            }).keydown(function(e){
              ACC.farefinder.keyBindtoDecimal(e,$(this));
              setTimeout(function(){
                if($(this).val()==""){
                  $(this).parents("#vehicle_width_inputs").find(".fareWidthInput").val("");
                }
              }.bind(this),1);
            })

            $('.fareheightInput').each(function(){
              if($(this).val()!=""){
                ACC.farefinder.calcFareInputHeight($(this));
              }
            })

            $('.farelengthInput').each(function(){
              if($(this).val()!=""){
                ACC.farefinder.calcFareLengthInput($(this));
              }
            })

            $('.fareWidthInput').each(function(){
              if($(this).val()!=""){
                ACC.farefinder.calcFareInputWidth($(this));
              }
            })
  },
  calcFareLengthInput : function(ele){
    var index='';
    var lengthText = $.trim($(ele).val());
    if(lengthText !== ""){

    var meters = parseFloat(lengthText) / 3.2808;

    var lengthInMtrs = meters.toFixed(2);
    if(lengthInMtrs!=NaN){
    $(ele).parents("#vehicle_length_inputs").find("input[id^='lengthboxinm_']").val(lengthInMtrs);
    } else {
    $(ele).parents("#vehicle_length_inputs").find("input[id^='lengthboxinm_']").val("");
    }

    }
  },
  calcFareInputHeight : function(ele) {
    var index='';
    var heightText = $.trim($(ele).val());
    if(heightText !== ""){

        var meters = parseFloat(heightText) / 3.2808;
        var heightInMtrs = meters.toFixed(2);
        if(heightInMtrs!=NaN){
          $(ele).parents("#vehicle_height_inputs").find("input[id^='heightinm_']").val(heightInMtrs);
        } else {
          $(ele).parents("#vehicle_height_inputs").find("input[id^='heightinm_']").val("");
        }

    }
  },
  calcFareInputWidth : function(ele) {
	    var index='';
	    var widthText = $.trim($(ele).val());
	    if(widthText !== ""){

	        var meters = parseFloat(widthText) / 3.2808;
	        var widthInMtrs = meters.toFixed(2);
	        if(widthInMtrs != NaN){
	          $(ele).parents("#vehicle_width_inputs").find("input[id^='widthinm_']").val(widthInMtrs);
	        } else {
	          $(ele).parents("#vehicle_width_inputs").find("input[id^='widthinm_']").val("");
	        }

	    }
	  },
  dangerousGoodCheckBox: function(){
        $("#faredangerousgoodoutboundcheckbox, #faredangerousgoodinboundcheckbox").change(function() {
            if(this.checked) {
                $(".dangerousgoodsDiv").removeClass("sr-only");
                }else{
                $(".dangerousgoodsDiv").addClass("sr-only");
                }

            });
    },

     showNorthernPassengerTypes: function(){
       $('.y_fareFinderDestinationLocation').on('change', function(){
         var isNorthern = false;
         $("#y_fareFinderDestinationLocationSuggestions").find("a").each(function(){
           if($(this).data('routetype')=='LONG')
           {
             isNorthern = true;
             return false;
           }
        });
         if(isNorthern)
        {
          $('.y_passengers').find('div.y_passengerWrapper').each(function(){
            if($(this).hasClass('LONG') && $(this).hasClass('sr-only'))
            {
              $(this).removeClass('sr-only');
            }
        });
        }
         else
        {
          $('.y_passengers').find('div.y_passengerWrapper').each(function(){
            if($(this).hasClass('LONG') && !$(this).hasClass('sr-only'))
            {
              $(this).addClass('sr-only');
            }
        });
        }
       });
     },

  returnwithdifferentvehicle : function() {
    $(".y_SelectTripTypefare").click(function() {
      id = $(this).attr('id');
      if (id == "returnfare") {
        $(".faredifferentVehicleCheckBox").removeClass("hidden");
        $(".faredangerousGoodReturnCheckBox").removeClass("hidden");
      } else {
        if ($('#farevehicleinboundcheckbox').is(":checked")) {
          $("#farevehicleinboundcheckbox").trigger('click');
        } // Unchecks 'Return with Different Vehicle' checkbox
        $(".faredifferentVehicleCheckBox").addClass("hidden");
        $(".faredangerousGoodReturnCheckBox").addClass("hidden");
        $(".farevehicle_1").addClass("hidden");

      }
    });
  },
  vehicleAccordionUIRenderOnEdit : function(){
    setTimeout(function(){
      ACC.farefinder.vehicleAccordionUIRender();
      ACC.farefinder.vehicleAccordionRenderText();
    },500)
  },
  vehicleAccordionUIRender : function(action){
    $(".vehicle-custom-accordion").find(".ui-accordion-content").each(function(){

      var selId = $(this).attr("id");
      var headerIcon = $(this).closest(".vehicle-custom-accordion").find(".ui-accordion-header[aria-controls="+selId+"]").find(".ui-accordion-header-icon");
      if($(this).find("input[type='radio']:checked").length>0 && action == undefined){
        headerIcon.removeClass("ui-icon");
        if(headerIcon.find('i').length==0)
          headerIcon.append('<i class="fa fa-check" aria-hidden="true" style="background: #fff;position: absolute;right: 0px;"></i>');
      }else{
        headerIcon.addClass("ui-icon");
        headerIcon.find('i').remove();
      }
    })

  },
  vehicleAccordionRenderText : function(){
    $(".vehicle-custom-accordion").find("div.content").each(function(){
      if($(this).find("input[type='radio']:checked").length>0 && $(this).siblings(".custom_accordion_message").length==0){
        var id = $(this).find("input[type='radio']:checked").attr("id");
        id = id.split("_");
        var vehicleHeight = $("input[name='vehicleInfoForm.vehicleInfo["+id[1]+"].vehicleType.code']:checked")

        var vehicleLength = vehicleHeight.closest(".vehicle-custom-accordion").find("input[name='over20Length["+id[1]+"]']:checked").parent().text().trim().split("(");
        vehicleHeight = vehicleHeight.parent().text().trim().split("(");

        var msg = ACC.validationMessages.message("label.farefinder.vehicle.standard");
        var id = $(this).attr("id");
        if($(".ui-accordion-header[aria-controls='"+id+"']").length > 0 ){
          msg = "1 x "+$(".ui-accordion-header[aria-controls='"+id+"']").text().trim();
        }

        msg += " ("
        if(vehicleHeight[0].trim() != ""){
            msg += vehicleHeight[0].trim()
        }

        if(vehicleHeight[0].trim() != "" && vehicleLength[0].trim() != ""){
          msg += ", "
        }
        if(vehicleLength[0].trim() != ""){
            msg += vehicleLength[0].trim()
        }

        msg += ")";

        var ele = $('<div class="row custom_accordion_message clearfix"><div class="mt-4 col-lg-12" style="display:none;"><p class="input-select-msg mt-0"> '+msg+'</p></div></div>').insertAfter($(this));
        ele.find("div").slideToggle();
      }
    })
  },
  vehicleAccordionUIEventBinder : function(){
    $(".vehicle-custom-accordion").on('change','[type="radio"]',function(){

      var ele = $(this).closest(".ui-accordion-content");
      $(this).closest(".vehicle-custom-accordion").find(".ui-accordion-content").each(function(){
        if($(this)[0] !== ele[0]){
          $(this).find("input[type='radio']").removeAttr("checked");
        }
      })
      ACC.farefinder.vehicleAccordionUIRender();
    });

    $("#accordion_0").bind("showAccordion",function(){
        ACC.farefinder.vehicleAccordionRenderText();
        ACC.farefinder.vehicleAccordionUIRender("hide");

    })

    $("#accordion_0").bind("hideAccordion",function(){
      $(this).find(".custom_accordion_message").find("div").slideToggle(function(){
          $(this).find(".custom_accordion_message").remove();
      }.bind(this));
      ACC.farefinder.vehicleAccordionUIRender();

    })

    $("#accordion_1").bind("showAccordion",function(){
        ACC.farefinder.vehicleAccordionRenderText();
        ACC.farefinder.vehicleAccordionUIRender("hide");
    })

    $("#accordion_1").bind("hideAccordion",function(){
      $(this).find(".custom_accordion_message").find("div").slideToggle(function(){
          $(this).find(".custom_accordion_message").remove();
      }.bind(this));
      ACC.farefinder.vehicleAccordionUIRender();
    })

  },
  vehicleinboundcheckbox : function() {
    $("#farevehicleinboundcheckbox").change(function() {
      if (this.checked) {
        $(".farevehicle_1").removeClass("hidden");
        $('#farey_standardRadbtn_1').prop('checked', true);
        $('#farey_standardRadbtn_1').trigger('click');
      }

      else {
        $(".farevehicle_1").addClass("hidden");
        $('#farey_standardRadbtn_1').prop('checked', false);
        $('#farey_commercialRadbtn_1').prop('checked', false);
        $('#farey_overSizeRadbtn_1').prop('checked', false);

      }

    });
  },

  sidecarcheckbox : function() {
    $(".farey_subVehicle").on('change', function() {
      var selected = $(this).find("option:selected").val();
      var vehicleToSkip = document.getElementById("fareskipVehicle").value;
      var id = $(this).attr('id');
      index = id.charAt(id.length - 1);
      if (vehicleToSkip.includes(selected)) {
        $('.faresidecarCheckBox_' + index).removeClass("hidden");
        $('.farevehicleDimensions_' + index).find('input').val(0);
        $('.farevehicleDimensions_' + index).addClass("hidden");
        $('#farelengthInMtrs_' + index).html("");
        $('#fareheightInMtrs_' + index).html("");
        $('#farewidthInMtrs_' + index).html("");
      } else {
        if (!$('.faresidecarCheckBox_' + index).hasClass("hidden")) {
          $('#faresidecarCheckBox_' + index).prop('checked', false);
          $('.faresidecarCheckBox_' + index).addClass("hidden");
          $('.farevehicleDimensions_' + index).removeClass("hidden");
        }
      }
    });
  },
       bindPassengerQuanitySelectorOutbound : function() {
      $('.y_outboundPassengerQtySelectorPlus').click(
        function(e) {
          e.preventDefault();
          var input = $(this).parents(".y_outboundPassengerQtySelector").find("#y_outboundPassengerQuantity");
          var quantity = parseInt($(input).val());
          var currentCount =  ACC.farefinder.currentOutboundPassengerCount();
          var maxAllowed =  parseInt($("#maxPassengerCount").text());
          if (currentCount < maxAllowed) {
            quantity = quantity + 1;
            $(input).val(quantity);
            var passengerCode = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerCode").attr('value');
            var passengerTypeName = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
            ACC.farefinder.outboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
            if($('#y_checkAccessibilityNeed').prop('checked'))
            {
              ACC.farefinder.showOutboundAccessibilityNeeds();
              $("div[id^='accordion-accessibility-']").accordion({ header: "h3", collapsible: true, active: false });
            }
      }
      else{
              $('#PassengerCountalert').removeClass('hidden');
            }
            if((currentCount-1) !== 0){
                $(input).val(quantity).addClass("stepperNonPristineColor");
            }else{
                $(input).val(quantity).removeClass("stepperNonPristineColor");
            }
    });

      $('.y_outboundPassengerQtySelectorMinus').click(function(e) {
        e.preventDefault();
        var input = $(this).parents(".y_outboundPassengerQtySelector").find("#y_outboundPassengerQuantity");
        var quantity = parseInt($(input).val());
        $('#PassengerCountalert').addClass('hidden');
        if (quantity > 0) {
          quantity = quantity - 1;
          $(input).val(quantity);
          var passengerCode = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerCode").attr('value');
          var passengerTypeName = $(this).parents(".y_outboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
          ACC.farefinder.outboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
          if($('#y_checkAccessibilityNeed').prop('checked'))
          {
            ACC.farefinder.showOutboundAccessibilityNeeds();
            $("div[id^='accordion-accessibility-']").accordion({ header: "h3", collapsible: true, active: false });
          }
        }
            if(quantity !== 0){
                $(input).val(quantity).addClass("stepperNonPristineColor");
             }else{
                 $(input).val(quantity).removeClass("stepperNonPristineColor");
             }
      });
  },

  bindLargeItemQuanitySelectorOutbound : function() {
          $('.y_outboundLargeItemQtySelectorPlus').click(
            function(e) {
              e.preventDefault();
              var input = $(this).parents(".y_outboundLargeItemQtySelector").find("#y_outboundLargeItemQuantity");
              var quantity = parseInt($(input).val());
              var currentCount = quantity;
              var maxAllowed =  parseInt($("#maxPassengerCount").text());
              if (currentCount < maxAllowed) {
                quantity = quantity + 1;
                $(input).val(quantity);
              }
            });

          $('.y_outboundLargeItemQtySelectorMinus').click(function(e) {
            e.preventDefault();
            var input = $(this).parents(".y_outboundLargeItemQtySelector").find("#y_outboundLargeItemQuantity");
            var quantity = parseInt($(input).val());
            if (quantity > 0) {
              quantity = quantity - 1;
              $(input).val(quantity);
            }
          });
      },

      bindJourneyDetailsEditModal : function() {
      $('.edit_journey_details_icon').click(
        function(e) {
          e.preventDefault();
                    ACC.farefinder.redirectToURl(ACC.config.contextPath+"/RouteSelectionPage");
        });
      },
      bindAddAnotherSailingButton : function() {
      $('.y_addAnotherSailing').click(
        function(e) {
          e.preventDefault();
          ACC.farefinder.redirectToURl("/");
        });
      },
      bindRedirectToHomeButton : function() {
      $('.y_redirectToHome').click(
        function(e) {
          e.preventDefault();
                    ACC.farefinder.redirectToURl("/");
        });
      },
      bindJourneyDetailsEditFareSelection : function() {
      $('.edit_journey_fare_selection').click(
        function(e) {
          e.preventDefault();
                     ACC.farefinder.redirectToURl(ACC.config.contextPath+"/edit-sailing");
        });
      },
      bindLargeItemQuanitySelectorInbound : function() {
        $('.y_inboundLargeItemQtySelectorPlus').click(
            function(e) {
              e.preventDefault();
              var input = $(this).parents(".y_inboundLargeItemQtySelector").find("#y_inboundLargeItemQuantity");
              var quantity = parseInt($(input).val());
              var currentCount = quantity;
              var maxAllowed =  parseInt($("#maxPassengerCount").text());
              if (currentCount < maxAllowed) {
                quantity = quantity + 1;
                $(input).val(quantity);
              }
            });

          $('.y_inboundLargeItemQtySelectorMinus').click(function(e) {
            e.preventDefault();
            var input = $(this).parents(".y_inboundLargeItemQtySelector").find("#y_inboundLargeItemQuantity");
            var quantity = parseInt($(input).val());
            if (quantity > 0) {
              quantity = quantity - 1;
              $(input).val(quantity);
            }
          });
        },



  currentOutboundPassengerCount : function() {
    var total=0;
    $('[id=y_outboundPassengerQuantity]').each(function() {
      var quantity = parseInt($(this).val());
          if (!isNaN(quantity)) {
              total += quantity;
          }
    });
  return total;
  },

  outboundPassengerQuantityBanner : function(quantity, passengerCode, passengerTypeName){
    var element = $("#outboundAdditionalPassengersBanner").find('#' + passengerCode);
      if(quantity > 0){
        element.text(quantity + ' x ' + passengerTypeName);
        element.removeClass('hidden');
      }
      else{
        element.addClass('hidden');
      }
      var element = $("#outboundNorthernPassengersBanner").find('#' + passengerCode);
      if(quantity > 0){
        element.text(quantity + ' x ' + passengerTypeName);
        element.removeClass('hidden');
      }
      else{
        element.addClass('hidden');
      }
     },

        bindPassengerQuanitySelectorInbound: function () {
      $('.y_inboundPassengerQtySelectorPlus').click(
          function(e) {
            e.preventDefault();
            var input = $(this).parents(".y_inboundPassengerQtySelector").find("#y_inboundPassengerQuantity");
            var quantity = parseInt($(input).val());
            var currentCount =  ACC.farefinder.currentInboundPassengerCount();
            var maxAllowed =  parseInt($("#maxPassengerCount").text());
            if (currentCount < maxAllowed) {
              quantity = quantity + 1;
              $(input).val(quantity);
              var passengerCode = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerCode").attr('value');
              var passengerTypeName = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
              ACC.farefinder.inboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
            }

            if($('#y_checkReturnAccessibilityNeed').prop('checked'))
            {
                  ACC.farefinder.showInboundAccessibilityNeeds();
                  $("div[id^='accordion-accesibility-need-']").accordion({ header: "h3", collapsible: true, active: false });
            }
          });

        $('.y_inboundPassengerQtySelectorMinus').click(function(e) {
          e.preventDefault();
          var input = $(this).parents(".y_inboundPassengerQtySelector").find("#y_inboundPassengerQuantity");
          var quantity = parseInt($(input).val());
          if (quantity > 0) {
            quantity = quantity - 1;
            $(input).val(quantity);
            var passengerCode = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerCode").attr('value');
            var passengerTypeName = $(this).parents(".y_inboundPassengerQtySelector").find("#y_passengerTypeName").attr('value');
            ACC.farefinder.inboundPassengerQuantityBanner(quantity, passengerCode, passengerTypeName);
          }

          if($('#y_checkReturnAccessibilityNeed').prop('checked'))
          {
                ACC.farefinder.showInboundAccessibilityNeeds();
                $("div[id^='accordion-accesibility-need-']").accordion({ header: "h3", collapsible: true, active: false });
          }
        });
    },

    currentInboundPassengerCount : function() {
      var total=0;
      $('[id=y_inboundPassengerQuantity]').each(function() {
        var quantity = parseInt($(this).val());
            if (!isNaN(quantity)) {
                total += quantity;
            }
      });
    return total;
    },

    inboundPassengerQuantityBanner : function(quantity, passengerCode, passengerTypeName){
      var element = $("#inboundAdditionalPassengersBanner").find('#' + passengerCode);
        if(quantity > 0){
          element.text(quantity + ' x ' + passengerTypeName);
          element.removeClass('hidden');
        }
        else{
          element.addClass('hidden');
        }
        var element = $("#inboundNorthernPassengersBanner").find('#' + passengerCode);
        if(quantity > 0){
          element.text(quantity + ' x ' + passengerTypeName);
          element.removeClass('hidden');
        }
        else{
          element.addClass('hidden');
        }
       },

      checkAccessibilityNeed : function() {
    $("#y_checkAccessibilityNeed").change(function(){
      if (this.checked) {
          ACC.farefinder.showOutboundAccessibilityNeeds();
      } else {
        $.when(ACC.services.removeAccessibilityPerPAX($('#y_fareFinderForm').serialize(),"outbound"));
                  location.reload();
          $("#y_accessibilityPlaceholderParent").addClass("sr-only");
                    $("div[id^='accordion-accessibility-']").parent().find('input:radio, input:checkbox').prop('checked', false);
            }
      $("div[id^='accordion-accessibility-']").accordion({ header: "h3", collapsible: true, active: false });
    });
  },

   checkCarryingLargeItem : function() {
        $("#y_caryingLargeItems").change(function(){
          if (this.checked) {
            $("#y_largeItemOutbound").removeClass("hidden");
            } else {
              $("#y_largeItemOutbound").addClass("hidden");
            }
        });
      },

      checkInboundCarryingLargeItem : function() {
                $("#y_caryingLargeItemsInbound").change(function(){
                  if (this.checked) {
                    $("#y_largeItemInbound").removeClass("hidden");
                    } else {
                      $("#y_largeItemInbound").addClass("hidden");
                    }
                });
              },

        checkCarryingDangerousGoods : function() {
              $("#y_caryingDangerousGoods").change(function(){
                if (this.checked) {
                  $("#dangerousGoodsOutboundInfo").removeClass("hidden");
                  } else {
                    $("#dangerousGoodsOutboundInfo").addClass("hidden");
                  }
              });
            },

         checkCarryingDangerousGoodsInbound : function() {
                $("#y_caryingDangerousGoodsInbound").change(function(){
                  if (this.checked) {
                    $("#dangerousGoodsInboundInfo").removeClass("hidden");
                    } else {
                      $("#dangerousGoodsInboundInfo").addClass("hidden");
                    }
                });
              },

  showOutboundAccessibilityNeeds : function () {
    if("PassengerSelectionPage" == $("#currentPage").text()){
      $.when(ACC.services.fetchAccessibilityPerPAX($('#y_fareFinderForm').serialize(),"outbound")).then(
          function(data) {
            $("#y_accessibilityPlaceholder").replaceWith(data.htmlContent);
          }
        );
      ACC.farefinder.bindAccessibilitySelectorButton();
      ACC.farefinder.bindClearSelectionButton();
      ACC.farefinder.bindOtherAccessibilityNeedTextBox();
      ACC.farefinder.bindMessageBoxDropdown();
      ACC.farefinder.allowWalkOnForPassengersWithSpecialNeeds();
      ACC.farefinder.triggerWalkOnOptions();
      $("#y_accessibilityPlaceholderParent").removeClass("sr-only");
    }
  },
  bindMessageBoxDropdown : function(){
    var passengerDropDownString = function(ele){
      var str = [];
      $(ele).each(function(){
        var txt = $(this).closest(".custom-checkbox-input").text().trim();
        if(txt == ""){
          if($(this).closest(".custom-radio-input").text().trim() != ""){
            txt = $(this).closest(".custom-radio-input").text().trim();
          }
        }
        str.push(txt);
      })
      return str.join("<br>");
    }

    setTimeout(function(){
      $("[id^=y_accessibility_dropdown_]").find(".ui-accordion-content").find("input[type=checkbox],input[type=radio]").change(function(){
        var pTag = $(this).closest(".ui-accordion-header").find("p.input-select-msg");
        var messageMarkup = '<p class="input-select-msg mt-0"></p>';
        var messageString = passengerDropDownString($(this).closest(".ui-accordion-content").find("input[type=checkbox]:checked,input[type=radio]:checked"));
        messageMarkup = $(messageMarkup).html(messageString);
        if($(this).closest(".ui-accordion-content").parent().find(".input-select-msg").length == 0){
          $(this).closest(".ui-accordion-content").parent().append(messageMarkup);
        }else {
          $(this).closest(".ui-accordion-content").parent().find(".input-select-msg").html(messageString);
        }
      })
    },100)
  },
  checkInboundAccessibilityNeed: function() {
         $("#y_checkReturnAccessibilityNeed").change(function(){
              if (this.checked) {
                ACC.farefinder.showInboundAccessibilityNeeds();
                 } else {
                 $.when(ACC.services.removeAccessibilityPerPAX($('#y_fareFinderForm').serialize(),"inbound"));
                    location.reload();
                    $("#y_accessibilityReturnPlaceholderParent").addClass("sr-only");
                 }
              $("div[id^='accordion-accesibility-need-']").accordion({ header: "h3", collapsible: true, active: false });

          });
       },

      showInboundAccessibilityNeeds : function() {
        if("PassengerSelectionPage" == $("#currentPage").text()){
        $.when(ACC.services.fetchAccessibilityPerPAX($('#y_fareFinderForm').serialize(), "inbound")).then(
            function(data) {
              $("#y_accessibilityReturnPlaceholder").replaceWith(
                  data.htmlContent);
            });
        ACC.farefinder.bindReturnAccessibilitySelectorButton();
        ACC.farefinder.bindClearSelectionButton();
        ACC.farefinder.bindOtherAccessibilityNeedTextBox();
        ACC.farefinder.allowWalkOnForPassengersWithSpecialNeeds();
        ACC.farefinder.triggerWalkOnOptions();
        $("#y_accessibilityReturnPlaceholderParent").removeClass("sr-only");
        }
  },

      showHideDivs : function(){
              $("input[id^='over7Height_']").change(function(){
              var str =$(this).attr('id');
              var ret = str.split("_");
          if (document.getElementById("over7Height_"+ret[1]).checked) {
            $('#livestockDiv_'+ret[1]).removeClass("hidden");
            $('#Over7heightDiv_'+ret[1]).removeClass('d-none');
            $('#Over7heightboxinft_'+ret[1]).val(0);
            $('#heightinm_'+ret[1]).val(0);
            $('#Oversizeheightboxinft_'+ret[1]).val(0);
            $('#Oversizelengthboxinft_'+ret[1]).val(0);
            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".weight").value=0;
            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".numberOfAxis").value=0;
          }
          ACC.farefinder.bindStandardVehicleLimitSailings(ret[1]);
       });


        $("input[id^='under7Height_']").change(function(){
              var str =$(this).attr('id');
              var ret = str.split("_");
                  if (document.getElementById("under7Height_"+ret[1]).checked) {
                      $('#livestockDiv_'+ret[1]).removeClass("hidden");
                      $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                  }
                  ACC.farefinder.bindStandardVehicleLimitSailings(ret[1]);
                });

        $("input[id^='over20Length_']").change(function(){
              var str =$(this).attr('id');
              var ret = str.split("_");
                  if (document.getElementById("over20Length_"+ret[1]).checked) {
                            $('#livestockDiv_'+ret[1]).removeClass("hidden");
                            $('#Over20lengthDiv_'+ret[1]).removeClass('d-none');
                            $('#Over20lengthboxinft_'+ret[1]).val(0);
                            $('#lengthboxinm_'+ret[1]).val(0);
                            $('#Oversizeheightboxinft_'+ret[1]).val(0);
                            $('#Oversizelengthboxinft_'+ret[1]).val(0);
                            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".weight").value=0;
                            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".numberOfAxis").value=0;
                  }
                  ACC.farefinder.bindStandardVehicleLimitSailings(ret[1]);
                });

                $("input[id^='under20Length_']").change(function(){
                          var str =$(this).attr('id');
                          var ret = str.split("_");
                  if (document.getElementById("under20Length_"+ret[1]).checked) {
                    $('#livestockDiv_'+ret[1]).removeClass("hidden");

                           $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                  }
                  ACC.farefinder.bindStandardVehicleLimitSailings(ret[1]);
                });

        $("input[id^='standardMotorcycle_']").change(function(){
                          var str =$(this).attr('id');
                          var ret = str.split("_");
                  if (document.getElementById("standardMotorcycle_"+ret[1]).checked) {
                            $('#livestockDiv_'+ret[1]).addClass("hidden");
                            $("#policyDiv_"+ret[1]).addClass("hidden");
                            $('#motorCycleLength_'+ret[1]).addClass('d-none');
                            $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                            $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                  }
                });

        $("input[id^='withTrailerMotorcycle_']").change(function(){
          var str =$(this).attr('id');
          var ret = str.split("_");
        if (document.getElementById("withTrailerMotorcycle_"+ret[1]).checked) {
            $('#livestockDiv_'+ret[1]).addClass("hidden");
            $("#policyDiv_"+ret[1]).addClass("hidden");
            $('#motorCycleLength_'+ret[1]).removeClass('d-none');
             $('#Motorcyclelengthboxinft_'+ret[1]).val(0);
            $('#Oversizeheightboxinft_'+ret[1]).val(0);
            $('#Oversizelengthboxinft_'+ret[1]).val(0);
            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".weight").value=0;
            document.getElementById("vehicleInfoForm.vehicleInfo"+ret[1]+".numberOfAxis").value=0;
            $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
            $('#Over7heightDiv_'+ret[1]).addClass('d-none');
        }
        });

        $("input[id^='sidecarMotorcycle_']").change(function(){
                          var str =$(this).attr('id');
                          var ret = str.split("_");
                  if (document.getElementById("sidecarMotorcycle_"+ret[1]).checked) {
                    $('#livestockDiv_'+ret[1]).addClass("hidden");
                    $("#policyDiv_"+ret[1]).addClass("hidden");
                    $('#motorCycleLength_'+ret[1]).addClass('d-none');
                    $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                    $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                  }
                });


          $("input[id^='otherOversize_']").change(function(){
                                  var str =$(this).attr('id');
                                  var ret = str.split("_");
                                    if(document.getElementById("otherOversize_"+ret[1]).checked)
                                    {
                                      ACC.farefinder.manageError('hide','');
                                      $("#other-input_"+ret[1]).removeClass("hidden");
                                      $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                                      $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                                    }
                                    });
                $("#ui-id-10").find("input[type='radio']").change(function(){

                })
                $("input[id^='semiTrailor_']").change(function(){
                            var str =$(this).attr('id');
                                var ret = str.split("_");
                                $("#over9FtDiv_"+ret[1]).removeClass("d-none");
                                if (document.getElementById("semiTrailor_"+ret[1]).checked && !document.getElementById("over9Ft_"+ret[1]).checked )
                                 {
                                  $("#showCallToBookDiv_"+ret[1]).removeClass("d-block");
                                  $("#showCallToBookDiv_"+ret[1]).addClass("d-none");
                                  $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-block");
                                  $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-none");

                                  $('#livestockDiv_'+ret[1]).removeClass("hidden");
                                  $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                                  $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                                  $("#other-input_"+ret[1]).addClass("hidden");
                              }
                                if (document.getElementById("semiTrailor_"+ret[1]).checked && document.getElementById("over9Ft_"+ret[1]).checked )
                                  {
                                   $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-none");
                                     $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-block");
                                  }

                                  $("#other-input").addClass("hidden");
                                  ACC.farefinder.bindOver5500kgWidthSelector();

                            });

                            $("input[id^='semiABTrain_']").change(function(){
                                                var str =$(this).attr('id');
                                                    var ret = str.split("_");
                                                    $("#over9FtDiv_"+ret[1]).removeClass("d-none");
                                                    if (document.getElementById("semiABTrain_"+ret[1]).checked  && !document.getElementById("over9Ft_"+ret[1]).checked )
                                                     {
                                                       $("#showCallToBookDiv_"+ret[1]).removeClass("d-block");
                                                         $("#showCallToBookDiv_"+ret[1]).addClass("d-none");
                                                         $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-block");
                                                         $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-none");

                                                         $('#livestockDiv_'+ret[1]).removeClass("hidden");
                                                         $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                                                         $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                                                         $("#other-input_"+ret[1]).addClass("hidden");
                                                }

                                                    if (document.getElementById("semiABTrain_"+ret[1]).checked && document.getElementById("over9Ft_"+ret[1]).checked )
                                                  {
                                                   $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-none");
                                                     $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-block");
                                                  }
                                                  $("#other-input").addClass("hidden");
                                                  ACC.farefinder.bindOver5500kgWidthSelector();
                                                });
        $("input[id^='straightTruck_']").change(function(){
                                                var str =$(this).attr('id');
                                                    var ret = str.split("_");
                                                    $("#over9FtDiv_"+ret[1]).removeClass("d-none");
                                                    if (document.getElementById("straightTruck_"+ret[1]).checked && !document.getElementById("over9Ft_"+ret[1]).checked )
                                                     {
                                                       $("#showCallToBookDiv_"+ret[1]).removeClass("d-block");
                                                         $("#showCallToBookDiv_"+ret[1]).addClass("d-none");
                                                         $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-block");
                                                         $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-none");

                                                         $('#livestockDiv_'+ret[1]).removeClass("hidden");
                                                         $('#Over20lengthDiv_'+ret[1]).addClass('d-none');
                                                         $('#Over7heightDiv_'+ret[1]).addClass('d-none');
                                                         $("#other-input_"+ret[1]).addClass("hidden");
                                                }

                                                    if (document.getElementById("straightTruck_"+ret[1]).checked && document.getElementById("over9Ft_"+ret[1]).checked )
                                                  {
                                                       $('#showCallToBookDivForGuestUser_'+ret[1]).removeClass("d-none");
                                                         $('#showCallToBookDivForGuestUser_'+ret[1]).addClass("d-block");
                                                  }
                                                  $("#other-input").addClass("hidden");
                                                  ACC.farefinder.bindOver5500kgWidthSelector();
                                                });


        $("input[id^='over9Ft_']").change(function(){
        	var index = $(this).attr('id').split("_")[1];
            ACC.farefinder.bindShowHideAdditionalVehicleFields(index);
        });
        $("input[id^='under9Ft_']").change(function(){
            var index = $(this).attr('id').split("_")[1];
            ACC.farefinder.bindShowHideAdditionalVehicleFields(index);
        });

        $('#vehicleinboundcheckbox').on('change',function(){
                if (document.getElementById("vehicleinboundcheckbox").checked)
                       {
                           document.getElementById("showCallToBookDiv").style.visibility = 'visible';
                       }
                });

                $("input[id^='livestockoutbound_']").change(function(){
                       var str =$(this).attr('id');
                       var ret = str.split("_");
                       if(document.getElementById("livestockoutbound_"+ret[1]).checked)
                      {
                         $("#policyDiv_"+ret[1]).removeClass("sr-only");
                         $("#policyDiv_"+ret[1]).removeClass("hidden");
                      }
                       if(!document.getElementById("livestockoutbound_"+ret[1]).checked)
                      {
                         $("#policyDiv_"+ret[1]).addClass("sr-only");
                         $("#policyDiv_"+ret[1]).addClass("hidden");
                      }
                   });


                var coll = document.getElementsByClassName("collapsible");
                coll.onclick = function() {
                       var divValue = document.getElementsByClassName('collapsible').getAttribute('value');
                       $(suggestionSelect).html(htmlStringStart);
                    };
                var i;

                for (i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var content = this.nextElementSibling;
                    if (content.style.display === "block") {
                      content.style.display = "none";
                    } else {
                      content.style.display = "block";
                    }
                  });
                }

              },
	      bindShowHideAdditionalVehicleFields : function(index) {
	    	  if($.isNumeric(index)){
	    		  if($("input[id^='under9Ft_']").is(':checked'))
		    		{
		    		  $('.js-over5500kg-limited-sailing-warning_' + index).addClass("hidden");
		    		  $('.js-over5500kb-width-input_' + index).addClass("hidden");
		    		  $('.js-over5500kg-conditional-dimensions_' + index).addClass("hidden");
		    		  $('.js-over5500kg-all-dimensions_' + index).addClass("d-block");
		    		  $('.js-over5500kg-all-dimensions_' + index).removeClass("d-none");
		    		  $('#showCallToBookDivForGuestUser_'+index).addClass("d-none");
		    		  $('#showCallToBookDivForGuestUser_'+index).removeClass("d-block");
	  	    		  var length = Number($("#Oversizelengthboxinft_" + index).val());
	  	    		  var height = Number($("#Oversizeheightboxinft_" + index).val());
	    			  var lengthLimit =  Number($("input[id='over5500VehicleLengthPortLimit']").val());
		    		  var heightLimit =  Number($("input[id='over5500VehicleHeightPortLimit']").val());
		    		  var lengthWarningLimit =  Number($("input[id='over5500VehicleLengthWarningLimit']").val());
		    		  var heightWarningLimit =  Number($("input[id='over5500VehicleHeightWarningLimit']").val());
		    		  if($("#isFullAccountUser").val() === "true" && (length > lengthLimit || height > heightLimit))
		    		  	{
		    			  $('.js-over5500kg-conditional-dimensions_' + index).removeClass("hidden");
		    		  	}
		    		  if(length > lengthWarningLimit || height > heightWarningLimit)
	        			{
		    				$('.js-over5500kg-limited-sailing-warning_' + index).removeClass("hidden");
	        			}
		    		}
		    	  else if($("input[id^='over9Ft_']").is(':checked'))
		    		{
		    		  $('.js-over5500kg-limited-sailing-warning_' + index).addClass("hidden");
		    		  $('.js-over5500kb-width-input_' + index).addClass("hidden");
		    		  $('.js-over5500kg-conditional-dimensions_' + index).addClass("hidden");
		    		  $('.js-over5500kg-all-dimensions_' + index).addClass("d-none");
		    		  $('.js-over5500kg-all-dimensions_' + index).removeClass("d-block");
		    		  $('#showCallToBookDivForGuestUser_'+index).addClass("d-none");
		    		  $('#showCallToBookDivForGuestUser_'+index).removeClass("d-block");
		    		  var lengthWarningLimit =  Number($("input[id='over5500VehicleLengthWarningLimit']").val());
		    		  var heightWarningLimit =  Number($("input[id='over5500VehicleHeightWarningLimit']").val());
		    		  if($("#isFullAccountUser").val() === "true")
		    		  {
		    			  $('.js-over5500kb-width-input_' + index).removeClass("hidden");
			    		  $('.js-over5500kg-conditional-dimensions_' + index).removeClass("hidden");
			    		  $('.js-over5500kg-all-dimensions_' + index).addClass("d-block");
			    		  $('.js-over5500kg-all-dimensions_' + index).removeClass("d-none");
		    		  }
		    		  else
		    		  {
		    			  $('#showCallToBookDivForGuestUser_'+index).removeClass("d-none");
		    			  $('#showCallToBookDivForGuestUser_'+index).addClass("d-block");
		    		  }
		    		  if(length > lengthWarningLimit || height > heightWarningLimit)
		    		  {
	    				$('.js-over5500kg-limited-sailing-warning_' + index).removeClass("hidden");
		    		  }
		    		}
	    	  }
	      },
	      bindStandardVehicleLimitSailings : function(index) {
	    	  if($.isNumeric(index)){
	    		  if($("input[id^='over7Height_']").is(':checked'))
		    		{
	    			  $('.js-standard-height-limited-sailing-warning_' + index).addClass("hidden");
	  	    			var height = Number($("#Over7heightboxinft_" + index).val());
		    			if(height >= 13.0)
	        			{
		    				$('.js-standard-height-limited-sailing-warning_' + index).removeClass("hidden");
	        			}
		    		}
	    		  if($("input[id^='over20Length_']").is(':checked'))
		    		{
		    		  $('.js-standard-length-limited-sailing-warning_' + index).addClass("hidden");
		    			  var length = Number($("#Over20lengthboxinft_" + index).val());
			    		  if(length > 60.0)
			    		  {
			    			  $('.js-standard-length-limited-sailing-warning_' + index).removeClass("hidden");
			    		  }
		    		}
	    	  }
	      },
	      bindStandardVehiclePortRestrictionPackage : function(index) {
	    	  if($.isNumeric(index) && "packageFerryInfoPage" === $("#currentPage").text()){
	    		  if($("input[id^='over7HeightPackage_']").is(':checked'))
		    		{
	    			  $('.js-standard-height-port-restriction-warning_' + index).addClass("hidden");
	  	    			var height = Number($("#Over7heightboxinft_" + index).val());
	  	    			var heightPortLimit =  Number($("input[id='standardVehicleHeightPortLimit']").val());
		    			if(height > heightPortLimit)
	        			{
		    				$('.js-standard-height-port-restriction-warning_' + index).removeClass("hidden");
	        			}
		    		}
	    		  if($("input[id^='over20LengthPackage_']").is(':checked'))
		    		{
		    		  $('.js-standard-length-port-restriction-warning_' + index).addClass("hidden");
		    			  var length = Number($("#Over20lengthboxinft_" + index).val());
		    			  var lengthPortLimit = Number($("input[id='standardVehicleLengthPortLimit']").val());
		    			  if(length > lengthPortLimit)
			    		  {
			    			  $('.js-standard-length-port-restriction-warning_' + index).removeClass("hidden");
			    		  }
		    		}
	    		  if(!$('.js-standard-height-port-restriction-warning_' + index).hasClass("hidden") || !$('.js-standard-length-port-restriction-warning_' + index).hasClass("hidden"))
	    			{
	    			  $('#fareFinderFindButton').attr('disabled', 'disabled');
	    			}
	    		  else
	    		  	{
	    			  $('#fareFinderFindButton').removeAttr('disabled');
	    		  	}
	    	  }
	      },
	      bindOver5500kgWidthSelector : function() {
	    	  $("div[class^='js-over5500kg-width-selector_']").addClass("hidden");
	    	  if($("input[id^='semiTrailor_']").is(':checked'))
	    		{
	    		  $("input[id^='semiTrailor_']").each(function(){
	    			  if($(this).is(':checked'))
		      			{
	    				 	var index = $(this).attr('id').split("_")[1];
	    				 	$('.js-over5500kg-width-selector_' + index).removeClass("hidden");
		      			}
	    		  });
	    		}
	    	  if($("input[id^='semiABTrain_']").is(':checked'))
	    		{
	    		  $("input[id^='semiABTrain_']").each(function(){
	    			  if($(this).is(':checked'))
		      			{
	    				 	var index = $(this).attr('id').split("_")[1];
	    				 	$('.js-over5500kg-width-selector_' + index).removeClass("hidden");
		      			}
	    		  });
	    		}
	    	  if($("input[id^='straightTruck_']").is(':checked'))
	    		{
	    		  $("input[id^='straightTruck_']").each(function(){
	    			  if($(this).is(':checked'))
		      			{
	    				 	var index = $(this).attr('id').split("_")[1];
	    				 	$('.js-over5500kg-width-selector_' + index).removeClass("hidden");
		      			}
	    		  });
	    		}
	      },
          bindChangeTravellingOptionDiv : function() {
            $('#proceed_to_vehicle_selection_btn').click(function(e) {
                $("#vehicleSelectionDiv").show();
                $("#proceed_to_sailing_selection_div").removeClass("passenger-tab-color");
                $("#proceed_to_sailing_selection_div").addClass("passenger-tab-transparent");
                $("#proceed_to_vehicle_selection_div").addClass("passenger-tab-color");
                $("#proceed_to_vehicle_selection_btn_div").removeClass("hidden");
                $("#proceed_to_sailing_selection_btn_div").addClass("hidden");
                $("#fareCalculatorOutboundWalkOnOptions").hide();
                $("#y_walkOnOptions").addClass("hidden");
                $("#y_walkOnOptionsInbound").addClass("hidden");
                $("#edit-journey-vehicle-selection-div").removeClass("hidden");
                });
            $('#proceed_to_sailing_selection_btn').on('click',function() {
                $("#vehicleSelectionDiv").hide();
                $("#proceed_to_vehicle_selection_div").removeClass("passenger-tab-color");
                $("#proceed_to_vehicle_selection_div").addClass("passenger-tab-transparent");
                $("#proceed_to_sailing_selection_div").addClass("passenger-tab-color");
                $("#proceed_to_sailing_selection_btn_div").removeClass("hidden");
                $("#proceed_to_vehicle_selection_btn_div").addClass("hidden");
                if($("#isWalkOnRoute").val() == 'true')
                {
                    $("#fareCalculatorOutboundWalkOnOptions").show();
                    $("#y_walkOnOptions").removeClass("hidden");
                    $("#y_walkOnOptionsInbound").removeClass("hidden");
                }
                $("#edit-journey-vehicle-selection-div").addClass("hidden");
                });

                if($('#proceed_to_vehicle_selection_btn').is(':checked')){
                    $("#proceed_to_sailing_selection_div").removeClass("passenger-tab-color");
                    $("#proceed_to_sailing_selection_div").addClass("passenger-tab-transparent");
                    $("#proceed_to_vehicle_selection_div").addClass("passenger-tab-color");
                    $("#proceed_to_vehicle_selection_btn_div").removeClass("hidden");
                    $("#proceed_to_sailing_selection_btn_div").addClass("hidden");
                    $("#y_walkOnOptions").addClass("hidden");
                    $("#y_walkOnOptionsInbound").addClass("hidden");
                    $("#edit-journey-vehicle-selection-div").removeClass("hidden");
                }
                if($('#proceed_to_sailing_selection_btn').is(':checked')){
                    $("#proceed_to_vehicle_selection_div").removeClass("passenger-tab-color");
                    $("#proceed_to_vehicle_selection_div").addClass("passenger-tab-transparent");
                    $("#proceed_to_sailing_selection_div").addClass("passenger-tab-color");
                    $("#proceed_to_sailing_selection_btn_div").removeClass("hidden");
                    $("#proceed_to_vehicle_selection_btn_div").addClass("hidden");
                    $("#y_walkOnOptions").removeClass("hidden");
                    $("#y_walkOnOptionsInbound").removeClass("hidden");
                    $("#edit-journey-vehicle-selection-div").addClass("hidden");

                }
        },
      bindAccessibilitySelectorButton : function() {
        $("div[id^='y_accessibility_dropdown_']").click(function(e) {
           var elem = jQuery(this).next("div[id^='y_accessibility_div_']");
           if(elem.hasClass("sr-only")){
              elem.removeClass("sr-only");
           }
           else{
             elem.addClass("sr-only");
           }
        });
      },
      bindReturnAccessibilitySelectorButton : function() {
                $("div[id^='y_accessibility_return_dropdown_']").click(function(e) {
                   var elem = jQuery(this).next("div[id^='y_accessibility_return_div_']");
                   if(elem.hasClass("sr-only")){
                      elem.removeClass("sr-only");
                   }
                   else{
                     elem.addClass("sr-only");
                   }
                });
              },
      bindClearSelectionButton : function() {
        $("div[id^='y_accessibility_clearselection_']").click(function(e) {
           var elem = jQuery(this).parent();
           elem.find('input:text').val('');
           elem.find('input:radio, input:checkbox').prop('checked', false);
           $(this).closest('.ui-accordion').find('.custom-arrow').removeClass('hidden');
           $(this).closest('.ui-accordion').find('.tickicon').remove();
        });
      },
      bindOtherAccessibilityNeedTextBox : function() {
        $("input[id^='specialServiceRequest_OTHR_']").change(function(){
           var elem = $(this).parent().parent().parent().find("div[id^='other_accessibility_need_']");
          if (this.checked) {
             elem.addClass('required-accessibility_need')
             elem.removeClass("hidden");
          } else {
              elem.removeClass('required-accessibility_need')
              elem.addClass("hidden");
            }
        });
  },
  removeAlert:function(){
      $(".alert-warning").on('click', function () {
      ACC.services.removeAlert();
      });
      },

      bindResidentProgramPage: function () {
                $('#bcResidentProgramLink').on('click', function(e) {
                          event.preventDefault();
                            var url = $(this).attr('href');
                            $.when(ACC.services.contentPopupDataAjax(url)).then(
                          function(data) {
                          console.log(data);
                                $('#bcResidentProgramDetailsDiv').replaceWith(data);
                                $('#bcResidentProgramModal').modal();
                            }
                          );
                  }


                  );
                  },
        setVehicleDimensionValues:function(){

        $("#fareFinderFindButton").on('click',function(e){
        if($("input[id^='over7Height_']").is(':checked'))
          {
	          $("input[id^='over7Height_']").each(function(){
	              if($(this).is(':checked'))
	              {
	            	var index = $(this).attr('id').split("_")[1];
	                var height = $("#Over7heightboxinft_"+index).val();
	                $("#height_"+index).val(height);
	              }
	          });
          }

        if($("input[id^='over20Length_']").is(':checked'))
        {
          $("input[id^='over20Length_']").each(function(){
            if($(this).is(':checked'))
              {
            	var index = $(this).attr('id').split("_")[1];
                var length = Number($("#Over20lengthboxinft_"+index).val());
                $("#length_"+index).val(length);
              }
          });
        }
        if($("input[id^='withTrailerMotorcycle_']").is(':checked'))
        {
          $("input[id^='withTrailerMotorcycle_']").each(function(){
            if($(this).is(':checked'))
              {
            	var index = $(this).attr('id').split("_")[1];
                var length = Number($("#Motorcyclelengthboxinft_"+index).val());
                $("#length_"+index).val(length);
                $("#height_"+index).val('0');
              }
          });
        }

    		if($("input[id^='semiTrailor_']").is(':checked'))
    		{
    			$("input[id^='semiTrailor_']").each(function(){
    				if($(this).is(':checked'))
        			{
    					var index = $(this).attr('id').split("_")[1];
    	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
    	    			$("#height_"+index).val(height);
    	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
    	    			$("#length_"+index).val(length);
    	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
    	    				var width = Number($("#OversizeWidthboxinft_"+index).val());
        	    			$("#width_"+index).val(width);
    	    			}
        			}
    			});
    		}

    		if($("input[id^='semiABTrain_']").is(':checked'))
    		{
    			$("input[id^='semiABTrain_']").each(function(){
    				if($(this).is(':checked'))
        			{
    					var index = $(this).attr('id').split("_")[1];
    	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
    	    			$("#height_"+index).val(height);
    	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
    	    			$("#length_"+index).val(length);
    	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
    	    				var width = Number($("#OversizeWidthboxinft_"+index).val());
        	    			$("#width_"+index).val(width);
    	    			}
        			}
    			});
    		}

    		if($("input[id^='straightTruck_']").is(':checked'))
    		{
    			$("input[id^='straightTruck_']").each(function(){
    				if($(this).is(':checked'))
        			{
    					var index = $(this).attr('id').split("_")[1];
    	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
    	    			$("#height_"+index).val(height);
    	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
    	    			$("#length_"+index).val(length);
    	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
    	    				var width = Number($("#OversizeWidthboxinft_"+index).val());
        	    			$("#width_"+index).val(width);
    	    			}
        			}
    			});
    		}
    		if($("input[id^='under20Length_']").is(':checked'))
    		{
				$("input[id^='under20Length_']").each(function(){
					if($(this).is(':checked'))
	    			{
		    	    	var index = $(this).attr('id').split("_")[1];
		    			$("#length_"+index).val("0");
	    			}
				});
    		}
    		if($("input[id^='under7Height_']").is(':checked'))
    		{
				$("input[id^='under7Height_']").each(function(){
					if($(this).is(':checked'))
	    			{
		    	    	var index = $(this).attr('id').split("_")[1];
		    			$("#height_"+index).val("0");
	    			}
				});
    		}
    	});
    },
            initCustomBindingToelements : function(){

          $(".ui-return").bind("setDate",function(evt,data){
            $(this).removeClass("disabled-nopointer");
            setMinDateToCalender($(this));

          });

      $(".ui-depart").bind("setDate",function(evt,data){
        setDateToCustomCalender($(this),data);

      });

      function setDateToCustomCalender(ele,date){
        var calId = $(ele).find(".nav-link").attr("href");
        var datepickerEle = $(calId).find(".datepicker");
        var input = $(calId).find(".datepicker-input");
        var tab =   $(ele).find(".vacation-calen-year-txt");

        if(date==""){
          var dt = new Date();
          input.val("");
          tab.text(dt.getFullYear());
        }else{
          input.val(date);
          datepickerEle.datepicker("setDate",date);
          tab.text(date);
        }
      }

      function setMinDateToCalender(ele){
        var calId = $(ele).find(".nav-link").attr("href");
        var datepickerEle = $(calId).find(".datepicker");
        var date = $(".y_fareFinderDatePickerDeparting").datepicker("getDate");
        datepickerEle.datepicker("option",{ minDate: date });
      }

      $("body").bind("show-error",function(evt,data){
        $(".custom-error-2").find("span").text(data);
        $(".custom-error-2").removeClass("hidden");
      });

      $("body").bind("hide-error",function(){
        $(".custom-error-2").addClass("hidden");
      });

      $("body").bind("routeType",function(evt,data){
        var long_route_calender_limit = "+18m";
        var short_route_calender_limit = "+12m";
        if($("#long-route-calender-limit").text() != ""){
          long_route_calender_limit = "+"+$("#long-route-calender-limit").text()+"m";
        }
        if($("#short-route-calender-limit").text() != ""){
          short_route_calender_limit = "+"+$("#short-route-calender-limit").text()+"m";
        }
        if(data.routeType == "LONG"){
          $(".y_fareFinderDatePickerDeparting").datepicker("option",{maxDate: long_route_calender_limit});
          $(".y_fareFinderDatePickerReturning").datepicker("option",{maxDate: long_route_calender_limit});
        }else{
          $(".y_fareFinderDatePickerDeparting").datepicker("option",{maxDate: short_route_calender_limit});
          $(".y_fareFinderDatePickerReturning").datepicker("option",{maxDate: short_route_calender_limit});
        }
      });

    },
    allowWalkOnForPassengersWithSpecialNeeds : function() {
    	if("false" === $("#js-farefinder-isWalkOnRoute").text() && "PassengerSelectionPage" === $("#currentPage").text())
		{
    		if($("input[class='js-accessibility-product']:checked").length > 0)	{
    			$(".travel-mode-selection").removeClass("hidden");
    		}
    		else{
    			$(".travel-mode-selection").addClass("hidden");
    		}
		}
    },
    triggerWalkOnOptions : function() {
    	$("input[class='js-accessibility-product']").change(function(){
    		ACC.farefinder.allowWalkOnForPassengersWithSpecialNeeds();
    	});
    },
    manageError : function (t,m,obj){
    	if(t=='show')
    	{
    		var currentObj=$(".form-error-messages");
    		if(obj){ currentObj=obj; }
    		currentObj.html('<span class="icon-alert"></span> <strong>Validation Error!</strong> '+m);
    		currentObj.removeClass('hidden');
    		$('html, body').animate({
    	        scrollTop: currentObj.offset().top
    	    }, 500);
    	}
    	if(t=='hide')
    	{
    		$('.form-error-messages').html('');
    		$('.form-error-messages').addClass('hidden');
    	}
    }
};


$(function(){
  $("input[id^='over7Height_']").each(function(){
    if($(this).is(':checked'))
    {
      var str =$(this).attr('id');
        var ret = str.split("_");
        $("#Over7heightDiv_"+ret[1]).removeClass("d-none");
    }
  });
  $("input[id^='over20Length_']").each(function(){
    if($(this).is(':checked'))
    {
      var str =$(this).attr('id');
        var ret = str.split("_");
        $("#Over20lengthDiv_"+ret[1]).removeClass("d-none");
    }
  });
  $("input[id^='withTrailerMotorcycle_']").each(function(){
    if($(this).is(':checked'))
    {
      var str =$(this).attr('id');
        var ret = str.split("_");
        $("#motorCycleLength_"+ret[1]).removeClass("d-none");
    }
  });

});


$(function() {

  if($("#accordion_0").find("input[type='radio']:checked").length > 0){
    $("#accordion_0").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
        activate: function(event, ui) {
          if(ui.newHeader.length === 0){
            $(this).trigger("showAccordion");
          }else{
            $(this).trigger("hideAccordion");
          }
    }
    });
  }else{
    $("#accordion_0").accordion({
        collapsible: true,
        active: 0,
        heightStyle: "content",
        activate: function(event, ui) {
          if(ui.newHeader.length === 0){
            $(this).trigger("showAccordion");
          }else{
            $(this).trigger("hideAccordion");
          }
    }
    });
  }


});

$(function() {
    $("#accordion_1").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
        activate: function(event, ui) {
          if(ui.newHeader.length === 0){
            $(this).trigger("showAccordion");
          }else{
            $(this).trigger("hideAccordion");
          }
        }
    });
});
$(function () {
  $("#accordion-vehicle").accordion({ collapsible: true, active: false, heightStyle: "content" });
  $("#accordion-vehicle-return").accordion({ collapsible: true, active: false, heightStyle: "content" });
  $("#accordion-content-template").accordion({ collapsible: true, active: false, heightStyle: "content" });
});
$(function () {

  $("#accordion-1").accordion({ header: "h3", collapsible: true, active: false });
  $("#accordion-nr-outbound").accordion({ header: "h3", collapsible: true, active: false });
  $("#accordion-nr-inbound").accordion({ header: "h3", collapsible: true, active: false });
});

$("#dangerousgoodoutboundcheckbox_0").on('click', function () {
        var checked = $(this).is(':checked');
           if (checked) {
              $("#warning-msg_0").show();
              $("#warning-msg_1").show();
           } else {
              $("#warning-msg_0").hide();
              $("#warning-msg_1").hide();
           }
});

$("#dangerousgoodoutboundcheckbox_1").on('click', function () {
        var checked = $(this).is(':checked');
           if (checked) {
              $("#warning-msg_0").show();
              $("#warning-msg_1").show();
           } else {
              $("#warning-msg_0").hide();
              $("#warning-msg_1").hide();
           }
});

$(function(){

  var renderInputOnKeyPress = function(key){
    //$(this).unbind("click");
    var closest_dropdown_id= $(this).closest(".accordion-34").attr("id");

    if(closest_dropdown_id=="vacation-accordion1"){
      var vacationaccordion1element =document.getElementById('vacation-accordion2');
      renderDropDownSearchUI({action:'show',ele:vacationaccordion1element});
      $(closest_dropdown_id).find('.vacation-range-box').css('display','block');
      $(vacationaccordion1element).find('.ui-accordion-content').css('display','none');
    }
    if(closest_dropdown_id=="vacation-accordion2"){
      var vacationaccordion2element =document.getElementById('vacation-accordion1');
      renderDropDownSearchUI({action:'show',ele:vacationaccordion2element});
        $(vacationaccordion2element).find('.ui-accordion-content').css('display','none');
    }
    if(closest_dropdown_id=="currentcondition-accordion1"){
      var vacationaccordion2element =document.getElementById('currentcondition-accordion2');
      renderDropDownSearchUI({action:'show',ele:vacationaccordion2element});
        $(vacationaccordion2element).find('.ui-accordion-content').css('display','none');
    }
    if(closest_dropdown_id=="currentcondition-accordion2"){
      var vacationaccordion1element =document.getElementById('currentcondition-accordion1');
      renderDropDownSearchUI({action:'show',ele:vacationaccordion1element});
      $(vacationaccordion1element).find('.ui-accordion-content').css('display','none');
    }
    //$(this).find('.ui-accordion-header-icon').attr('onclick', 'hideInputOnClick(\''+closest_dropdown_id+'\')');
    if(closest_dropdown_id=="fromLocationDropDown"){
      var toLocationDropDownelement =document.getElementById('toLocationDropDown');
      renderDropDownSearchUI({action:'show',ele:toLocationDropDownelement});
      $(toLocationDropDownelement).find('.vacation-range-box').css('display','none');
      $(closest_dropdown_id).find('.vacation-range-box').css('display','block');
    }
    if(closest_dropdown_id=="toLocationDropDown"){
      var fromLocationDropDownelement =document.getElementById('fromLocationDropDown');
      renderDropDownSearchUI({action:'show',ele:fromLocationDropDownelement});
      $(fromLocationDropDownelement).find('.vacation-range-box').css('display','none');
      $(closest_dropdown_id).find('.vacation-range-box').css('display','block');
    }
   $(this).closest(".accordion-34").trigger("uiChange","hide");
    //$(this).find(".dropdown-hiddenInput")al(key.key);
    $(this).find(".dropdown-hiddenInput").bind("keyup keydown keypress",renderHtmlOnBlankInput);
    key.preventDefault();

    }


    // Restore default text when when input is blank on drop down
    var renderHtmlOnBlankInput = function(key){
      if($(this).val() == ""){
        //$(this).closest(".accordion-34").trigger("uiChange","show");
      }
      filterDropDown($(this).val(),$(this).closest(".accordion-34"));
      if($(this).closest(".accordion-34").attr('id')=="vacation-accordion1"){
        $("#farefinder-routeinfo-vacation-accordion1").html($(this).val());
      }
      if($(this).closest(".accordion-34").attr('id')=="vacation-accordion2"){
        $("#farefinder-routeinfo-vacation-accordion2").html($(this).val());
      }
      if($(this).closest(".accordion-34").attr('id')=="currentcondition-accordion2"){
        $("#farefinder-routeinfo-currentcondition-accordion2").html($(this).val());
      }
      if($(this).closest(".accordion-34").attr('id')=="currentcondition-accordion1"){
        $("#farefinder-routeinfo-currentcondition-accordion1").html($(this).val());
      }
      if($(this).closest(".accordion-34").attr('id')=="fromLocationDropDown"){
        $("#farefinder-routeinfo-fromLocationDropDown").html($(this).val());
      }
      if($(this).closest(".accordion-34").attr('id')=="toLocationDropDown"){
        $("#farefinder-routeinfo-toLocationDropDown").html($(this).val());
      }
    }

    // To render Dropdown ui when user search in dropdown
    var renderDropDownSearchUI = function(data){
      var action = data.action;
      var ele = data.ele;
      var farefinder_routeinfo_div_id = "farefinder-routeinfo-"+$(ele).attr('id');
      $(ele).find("h3").find(".dropdown-hiddenInput").val($("#"+farefinder_routeinfo_div_id).html());
      if(action=="hide"){
        $(ele).find("h3").find(".dropdown-text").hide();
        $(ele).find("h3").find(".dropdown-hiddenInput").removeClass("hidden").focus();
        setTimeout(function(){
            $(ele).find("h3").find(".dropdown-hiddenInput").focus();
          }, 1);
      }else{
        $(ele).find("h3").find(".dropdown-text").show();
        $(ele).find("h3").find(".dropdown-hiddenInput").addClass("hidden");
        //$(ele).find(".dropdown-hiddenInput").unbind("keypress");
        //$(ele).find("h3").bind("click",renderInputOnKeyPress);
        $(ele).find("h3").focus();
        //filterDropDown("",$(ele));
      }
    }

  //binding events to element
  //binding events to element
  $("#fromLocationDropDown").find("h3").append('<input type="text" value="" placeholder="'+ACC.validationMessages.message("placeholder.ferry.farefinder.LocationDropDown.input.format")+'" class="dropdown-hiddenInput hidden" />')
  $("#fromLocationDropDown").find("h3").bind("click",renderInputOnKeyPress);

  $("#fromLocationDropDown").bind("uiChange", function(ele,data){
  if($("#fromLocationDropDown").find("input").hasClass('hidden')){
    renderDropDownSearchUI({action:'hide',ele:this});
  }
  else{
    renderDropDownSearchUI({action:'show',ele:this});
  }
  var toLocationDropDown_element =document.getElementById('toLocationDropDown');
  //renderDropDownSearchUI({action:'show',ele:toLocationDropDown_element});
  });

  $("body").bind("setSearchDropDown",function(ele,dataEle){
    if($(dataEle).find("h3").find('.dropdown-hiddenInput').length ==0) {
          $(dataEle).find("h3").append('<input type="text" value="" placeholder="'+ACC.validationMessages.message("placeholder.ferry.farefinder.LocationDropDown.input.format")+'" class="dropdown-hiddenInput hidden" />')
      }    $(dataEle).find("h3").bind("click",renderInputOnKeyPress);
    $(dataEle).bind("uiChange", function(ele,data){
      if($(dataEle).find("input").hasClass('hidden')){
        renderDropDownSearchUI({action:'hide',ele:dataEle});
      }
      else{
        renderDropDownSearchUI({action:'show',ele:dataEle});
      }
    });
  });



  //binding events to dynamic to dropdown
  $("#toLocationDropDown").closest(".bc-accordion").bind("searchOnKeyPress",function(){
    $("#toLocationDropDown").find("h3").append('<input type="text" placeholder="'+ACC.validationMessages.message("placeholder.ferry.farefinder.LocationDropDown.input.format")+'" class="dropdown-hiddenInput hidden" />')
    $("#toLocationDropDown").find("h3").bind("click",renderInputOnKeyPress);
    $("#toLocationDropDown").bind("uiChange", function(ele,data){
     if($("#toLocationDropDown").find("input").hasClass('hidden')){
       renderDropDownSearchUI({action:'hide',ele:this});
     }
     else{
       renderDropDownSearchUI({action:'show',ele:this});
     }
   });
  });


  // To filter out the search element
  var filterDropDown = function(keyword,ele){
    var keyword = keyword.toLowerCase();
    $(ele).find(".vacation-range-box").find("ul").each(function(){
      var ulEl = $(this);
      var area = $(this).find(".second-span").text().toLowerCase();
      var liHiddenCounter = ulEl.find("li").length;
      ulEl.find("li").each(function(){
        var liEl = $(this);
        if(area.indexOf(keyword) === 0){
          liEl.show();
        }else{
          var code = liEl.find("a").data("code").toLowerCase();
          var city = liEl.find("a").data("city");
          var terminal = liEl.find("a").data("terminal").toLowerCase();
          if(city == "" || city == null){
            city = liEl.find("a").text().trim().toLowerCase();
          }
          else if (city != null){
          city=city.toLowerCase();
          }
          if(keyword==""){
            liEl.show();
          }else{
            if(code.indexOf(keyword)===0 || city.indexOf(keyword)===0 || terminal.indexOf(keyword)===0){
              liEl.show();
            }else{
              liEl.hide();
              liHiddenCounter--;
            }
          }
        }
      });

      if(liHiddenCounter<=0){
        ulEl.hide();
      }else{
        ulEl.show();
      }
    });
  }

})
$(function(){
  //close calender on outside click
  $(document).mouseup(function(e)
  {
      var container = $("#js-roundtrip");

      // if the target of the click isn't the container nor a descendant of the container
      if (!container.is(e.target) && container.has(e.target).length === 0)
      {
          $("#js-roundtrip").find(".nav-item.active").removeClass("active");
          $("#js-roundtrip").find(".tab-pane.active").removeClass("active");
      }
  });
})


//validation book a sailing
$(function(){


  window.formatDate = function(date) {
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];

      var day = ("0" + date.getDate()).slice(-2);
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return monthNames[monthIndex] + " " + day + " " + year;
    }

  window.formatDateMM = function(date) {

      var day = ("0" + date.getDate()).slice(-2);
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return day+"/"+monthIndex+"/"+year;
    }

   window.validateDateinput = function(input,datepicker,format){
     if(format=="dd/mm/yy"){
       if(input === formatDateMM(datepicker)){
         return true;
       }
       return false;
     }else if(format=="mm/dd/yy"){

        if(input === $.datepicker.formatDate("mm/dd/yy", datepicker)){
          return true;
        }
        return false;
     }
      if(input === formatDate(datepicker)){
        return true;
      }
      return false;
  }

  $(".farefinderform-custom").on("submit",function(){
      $('#y_buttonToClick').val('#y_confirmaddpassenger');
      var farefinderform_validation_data = ACC.farefinder.book_a_sailing_validation('BOOKING_TRANSPORT_ONLY');
      ACC.carousel.adjustSearchHeight();
      return farefinderform_validation_data;
  })

  ACC.farefinder.book_a_sailing_validation = function(journeyType) {

      if (ACC.common.shallCartBeRemoved(journeyType) == false) {
          return false;
      }

    if($("#fromLocationDropDown").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")){
      $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.fromTerminal"));
      return false;
    }

    if($("#toLocationDropDown").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")){
      $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.toTerminal"));
      return false;
    }


    if($(".bc-dropdown.depart").val()==""){
      $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.DepDate"));
      return false;
    }
     if(!validateDateinput($(".bc-dropdown.depart").val(),$(".y_fareFinderDatePickerDeparting").datepicker("getDate"),"mm/dd/yy")){
      $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.wrongDateDep"));
      return false;
    }

    if($("input[name='routeInfoForm.tripType']:checked").val()!="SINGLE"){
      if($(".bc-dropdown.arrive").val()==""){
        $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.ArvDate"));
        return false;
      }
      if(!validateDateinput($(".bc-dropdown.arrive").val(),$(".y_fareFinderDatePickerReturning").datepicker("getDate"),"mm/dd/yy")){
        $("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.wrongDateArv"));
        return false;
      }
    }



    return true;
  }

  $(".bc-dropdown.arrive").bind("return-date-selected",function(evt,data){
    console.log(evt,data);
    if($(".bc-dropdown.depart").val()==""){
      $("body").trigger("show-error","Select departure date first");
    }
  });


  $("form.passenger-info-form").submit(function(){

            if($(this).data("submit")=="1"){
                return true;
            }

            if($("#y_caryingLargeItems:checked").length>0 && $("#proceed_to_sailing_selection_btn:checked").length>0){
            var submitChk = true;
                    $(".carrying-large-items").each(function(){
                        if(parseInt($(this).val())>0){
                            $('#y_showLimitedSailingsModal').modal('show');
                            submitChk = false;
                        }
                    })

            }
            return submitChk;
        })

        $("form.passenger-info-form").find("#y_freshBooking").click(function(){
            $("form.passenger-info-form").data("submit","1");
        });
});

$(function(){
	var timerid;
	$("input[id^='Oversizeheightboxinft_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindShowHideAdditionalVehicleFields(idx);
		    }, 100);
		  }
	});
	$("input[id^='heightinm_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindShowHideAdditionalVehicleFields(idx);
		    	ACC.farefinder.bindStandardVehicleLimitSailings(idx);
		    	ACC.farefinder.bindStandardVehiclePortRestrictionPackage(idx);
		    }, 100);
		  }
	});
	$("input[id^='Oversizelengthboxinft_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindShowHideAdditionalVehicleFields(idx);
		    }, 100);
		  }
	});
	$("input[id^='lengthboxinm_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindShowHideAdditionalVehicleFields(idx);
		    	ACC.farefinder.bindStandardVehicleLimitSailings(idx);
		    	ACC.farefinder.bindStandardVehiclePortRestrictionPackage(idx);
		    }, 100);
		  }
	});
	$("input[id^='Over7heightboxinft_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindStandardVehicleLimitSailings(idx);
		    	ACC.farefinder.bindStandardVehiclePortRestrictionPackage(idx);
		    }, 100);
		  }
	});
	$("input[id^='Over20lengthboxinft_']").on("input", function(e) {
		  var value = $(this).val();
		  var idx = $(this).attr('id').split("_")[1];
		  if ($(this).data("lastval") != value) {
		    $(this).data("lastval", value);
		    clearTimeout(timerid);
		    timerid = setTimeout(function() {
		    	ACC.farefinder.bindStandardVehicleLimitSailings(idx);
		    	ACC.farefinder.bindStandardVehiclePortRestrictionPackage(idx);
		    }, 100);
		  }
	});
})

$("#y_reset_page").on("click",function(e){
	e.preventDefault();
	$(".form-vehicle-selection").trigger('reset');
	$('#ferry-option-booking-info-modal').modal('toggle');
})
