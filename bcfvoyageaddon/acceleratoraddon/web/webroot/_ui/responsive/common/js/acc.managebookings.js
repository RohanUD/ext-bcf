ACC.managebookings = {
	advanceSearchDeparture:'',
    advanceSearchArrival:'',
	_autoloadTracc: [
		"init",
		"bindManagebookingsValidationMessages",
		"enableRemoveTravellerButton",
		"bindManageBookingsValidation",
		"bindRemoveTravellerClick",
		"bindRemoveTravellerModalClick",
		"bindRemoveTravellerModalDismiss",
		"bindMyAccountShowMyUpcomingBookings",
		"bindMyAccountShowAdvanceBookings",
		"bindMyAccountShowMyPastBookings",
		"bindCollapsibles",
		"bindPopUpControls",
		"cancellableCheckboxOnClick",
		"enablebMultiCancelButtonOnCheckboxClick",
		"cancelBookingCheckboxOnClick",
		"enablebMultipleBookingCancellationOnCheckboxClick",
		"bindManageBookingCancelSailingModal",
		"bindManageBookingEditSailingModal",
		"bindOriginalBookingViewModal",
		"bindOriginalBookingViewAccordion",
		"bindSortTable",
		"bindPaginateTable",
		"bindSelectAllRadio",
		"bindResetBookingMarkers",
		"bindshipInfoPopUp"
	],
	init: function () {
		$('#check-in-datepicker').datepicker({
			onSelect: function(e) {
				var advDepartDate = $(".adv-depart-date").val(e);
				$(".advSelectedDepartDate").html(advDepartDate.val());
				$("#js-roundtrip").find(".input-required-wrap").removeClass("active");
			  }
		});
		$('#check-out-datepicker').datepicker({
			onSelect: function(e) {
				var advReturnDate = $(".adv-return-date").val(e);
				$(".advSelectedReturnDate").html(advReturnDate.val());
				$("#js-roundtrip").find(".input-required-wrap").removeClass("active");
			  }
		});
		
	},
	bindManagebookingsValidationMessages : function (){
		ACC.ManagebookingsValidationMessages = new validationMessages("ferry-bindManagebookingsValidationMessages");
        ACC.ManagebookingsValidationMessages.getMessages("error");
	},
	bindManageBookingsValidation: function () {
		$("#y_manageBookingsForm").validate({
			errorElement: "span",
			errorClass: "fe-error",
			onfocusout: function (element) {
				$(element).valid();
			},
			rules: {
				bookingReference: "required",
				lastName: "required"
			},
			messages: {
				bookingReference: ACC.ManagebookingsValidationMessages.message('error.managemybooking.booking.reference'),
				lastName: ACC.ManagebookingsValidationMessages.message('error.managemybooking.last.name')
			}
		});
	},
	bindRemoveTravellerClick: function () {
		$(".y_removeTraveller").on('click', function (event) {
			$(".y_removeTraveller").css({
				'pointer-events': 'none'
			});
			event.preventDefault();
			var travellerUid = $(this).closest(".form-group").find("input[name=travellerUid]").val();
			var orderCode = $("input[name=bookingReference]").val();
			var cancelTravellerUrl = $(this).attr('href');
			if (travellerUid != '' && orderCode != '') {
				$.when(ACC.services.cancelTraveller(orderCode, travellerUid)).then(
					function (data) {
						if (data.isCancelPossible) {
							$(".y_cancelTravellerConfirm").html(data.cancelTravellerModalHtml);
							$("#y_cancelTravellerUrl").attr('href', cancelTravellerUrl);
							$('#y_cancelTravellerModal').modal();
						} else {
							$(".y_cancellationResult").removeClass("alert-success");
							$(".y_cancellationResult").addClass("alert-danger");
							$(".y_cancellationResult").show();
							$(".y_cancellationResultContent").html(data.errorMessage);
							$(".y_cancellationRefundResultContent").html("");
						}
					}
				);
			}
		});
	},
	enableRemoveTravellerButton: function () {
		$(".y_removeTraveller").css({
			'pointer-events': ''
		});
	},
	bindRemoveTravellerModalClick: function () {
		$("#y_cancelTravellerUrl").on('click', function (event) {
			$(this).attr('disabled', 'disabled');
		});
	},
	bindRemoveTravellerModalDismiss: function () {
		$(document).on('hidden.bs.modal', "#y_cancelTravellerModal", function () {
			ACC.managebookings.enableRemoveTravellerButton();
		});
	},
	bindMyAccountShowMyUpcomingBookings: function () {
		$("#my-upcoming-bookings").on('click', function (e) {
			e.preventDefault();
			$("#my-bookings-holder").addClass("hidden");
			$(".y_multiBookingCancellationButton").removeClass("hidden");
			$.when(ACC.services.getTransportBookingsAjax($(this).attr("href"))).then(
				function (data) {
					$("#my-bookings-list-div").replaceWith(data.htmlContent);
					$("#my-upcoming-bookings").addClass("disabled");
					$("#my-upcoming-bookings").removeClass("btn-primary");
					$("#my-upcoming-bookings").addClass("btn-secondary");
					$("#my-past-bookings").removeClass("disabled");
					$("#my-past-bookings").removeClass("btn-secondary");
					$("#my-past-bookings").addClass("btn-primary");
				});
			$("#my-bookings-holder").removeClass("hidden");
			ACC.managebookings.bindCollapsibles();
			ACC.managebookings.bindSortTable();
			ACC.managebookings.bindPaginateTable();
			ACC.managebookings.bindSelectAllRadio();
			ACC.managebookings.cancelBookingCheckboxOnClick();
			ACC.managebookings.bindPopUpControls();
		});
	},
	bindMyAccountShowMyPastBookings: function () {
		$("#my-past-bookings").on('click', function (e) {
			e.preventDefault();
			$("#my-bookings-holder").addClass("hidden");
			$.when(ACC.services.getTransportBookingsAjax($(this).attr("href"))).then(
				function (data) {
					$("#my-bookings-list-div").replaceWith(data.htmlContent);
			        $(".y_multiBookingCancellationButton").addClass("hidden");
					$("#my-past-bookings").addClass("disabled");
					$("#my-past-bookings").removeClass("btn-primary");
					$("#my-past-bookings").addClass("btn-secondary");
					$("#my-upcoming-bookings").removeClass("disabled");
					$("#my-upcoming-bookings").removeClass("btn-secondary");
					$("#my-upcoming-bookings").addClass("btn-primary");
				});
			$("#my-bookings-holder").removeClass("hidden");
			ACC.managebookings.bindCollapsibles();
			ACC.managebookings.bindSortTable();
			ACC.managebookings.bindPaginateTable();
			ACC.managebookings.bindSelectAllRadio();
			ACC.managebookings.cancelBookingCheckboxOnClick();
			ACC.managebookings.bindPopUpControls();
		});
	},
	bindMyAccountShowAdvanceBookings: function () {
		$(".advanced-search-wrapper").find("#fromLocationDropDown").accordion({
			activate: function( event, ui ) {
				$(".sub-link").on("click", function(){
					if($(this).parents("#fromLocationDropDown").length > 0){
						ACC.managebookings.advanceSearchDeparture = $(this).data("code");
					}
					else if($(this).parents("#toLocationDropDown").length > 0){
						ACC.managebookings.advanceSearchArrival = $(this).data("code");
					}
				});
			}
		});
		$("#search-advance-bookings").on('click', function (e) {
			e.preventDefault();
			var selectedDeparture = ACC.managebookings.advanceSearchDeparture;
			var advDepartureLabel = $("#fromLocationDropDown").find('h3.ui-accordion-header').find(".dropdown-text strong").text().trim();
			var selectedArrival = ACC.managebookings.advanceSearchArrival;
			var avdArrivalLabel = $("#toLocationDropDown").find('h3.ui-accordion-header').find(".dropdown-text strong").text().trim();
			var departureDateTimeFrom = $('.advSelectedDepartDate').text();
			var departureDateTimeTo = $('.advSelectedReturnDate').text();
			$.when(ACC.services.getAdvanceTransportBookingsAjax($(this).attr("href"), $("#pageNumber").val(), $("#bookingType").val(), $("#bookingReference").val(), selectedDeparture, selectedArrival, departureDateTimeFrom, departureDateTimeTo, $("#pageNumber").val())).then(
				function (data) {
					$("#my-bookings-list-div").replaceWith(data.htmlContent);
					$("#advSearchDeparture").text(advDepartureLabel);
					$("#advSearchArrival").text(avdArrivalLabel);
				});
			if(avdArrivalLabel === 'City'){
				$('.advanced-search-error').removeClass("hidden").focus();
				
			} else{
				$('.advanced-search-error').addClass("hidden");
				$('.selected-advanced-search').removeClass("hidden");
				$('.advanced-search-wrapper').toggle('slow');
			}
			$("#my-bookings-holder").removeClass("hidden");
			ACC.managebookings.bindCollapsibles();
			ACC.managebookings.bindSortTable();
			ACC.managebookings.bindPaginateTable();
			ACC.managebookings.bindSelectAllRadio();
			ACC.managebookings.cancelBookingCheckboxOnClick();
			ACC.managebookings.bindPopUpControls();
		});
	},
	bindCollapsibles: function () {
		$('.p-accordion .p-box').find('.p-header').click(function (e) {
			$(this).closest('.p-accordion').find('.p-header').removeClass('payshow');
			$(this).toggleClass('payshow');
			$('.p-accordion .p-box .p-collapse').collapse('hide');
			$(this).closest('.p-box').find('.p-collapse').collapse('toggle');
		});
	},
	cancellableCheckboxOnClick: function () {
		$('#submitForCancelling').on('click', function () {
			var checkedValue = "";
			$(".cancelBookingRefCheckbox:checked").each(function (i) {
				checkedValue += "," + $(this).val();
			});
			checkedValue = checkedValue.substr(1);
			$("#cancellableOrderCode").val(checkedValue);
		});
	},
	enablebMultiCancelButtonOnCheckboxClick: function () {
		$(".cancelBookingRefCheckbox").on("click", function () {
			if ($(".cancelBookingRefCheckbox:checked").length > 0) {
				$('#submitForCancelling').prop('disabled', false);
			} else {
				$('#submitForCancelling').prop('disabled', true);
			}
		});
	},
	cancelBookingCheckboxOnClick: function () {
		$('.submitForCancelBooking').on('click', function () {
			var checkedValue = "";
			$(".cancelBookingCheckbox:checked").each(function (i) {
				checkedValue += "," + $(this).val();
			});
			checkedValue = checkedValue.substr(1);
			console.log("CheckedValue is  "+checkedValue);
			if (checkedValue == null || checkedValue == '') {
               $("#y_noCheckBoxSelectedModal").modal('show');
               return;
            }
			$(".cancelBookingRef").val(checkedValue);
			$("#y_multipleBookingCancelModal").modal('show');
			ACC.managebookings.bindPopUpControls();
		});
	},
	bindPopUpControls: function () {
		$("#y_multipleBookingCancelUrl").on("click", function () {
			$(".cancelRequestForm").submit();
			$("#y_multipleBookingCancelModal").modal('hide')
		});
		$("#y_noMultipleBookingCancelUrl").on("click", function () {
			$("#y_multipleBookingCancelModal").modal('hide')
		});
	},
	enablebMultipleBookingCancellationOnCheckboxClick: function () {
		$(".cancelBookingCheckbox").on("click", function () {
			if ($(".cancelBookingCheckbox:checked").length > 0) {
				$('.submitForCancelBooking').removeAttr('disabled');
			} else {
				$('.submitForCancelBooking').attr('disabled', 'disabled')
			}
		});
	},
   	bindManageBookingCancelSailingModal : function() {
        $(function() {
            $(".js-manage-booking-cancel-modal").click(function() {
                $("input#bookingReferencesToCancel").val($(this).data('id'));
            });
        });
    },
   	bindManageBookingEditSailingModal : function() {
        $(function() {
            $(".js-manage-booking-edit-modal").click(function() {
                $("input#eBookingReferenceToModify").val($(this).data('id'));
            });
        });
    },
	bindOriginalBookingViewModal : function() {
		if ("true" == $("#js-load-original-booking").text()) {
			$.when(ACC.services.getOriginalBooking($("#js-original-booking-reference").text())).then(
				function(data) {
					$("#original-booking-placeholder").replaceWith(data.htmlContent);
					ACC.carousel.bindAccordion();
					ACC.managebookings.bindOriginalBookingViewAccordion();
				});
		}
	},
	bindOriginalBookingViewAccordion : function() {
		$(function() {
            $(".original-booking-detail-title").click(function() {
                $(".original-booking-detail-title-hide").toggleClass("hidden");
                $(".original-booking-detail-title-view").toggleClass("hidden");
            });
        });
	},
	bindshipInfoPopUp: function () {
		$(document).on("click","#shipInfoPopUp", function (event) {
           event.preventDefault();
           $(this).closest("body").append('<div class="y_shipInfoModal"></div>');
           var code=$('#vesselCode').val();
           $.when(ACC.services.getShipInfo(code)).then(
           function(data){
           $(".y_shipInfoModal").html(data.shipInfoModalHtml);
           $("#y_shipInfosModal").modal('show');
           });
           ACC.global.initImager();
           return false;
		});
	},
	bindSortTable : function() {
		$("#transport-business-bookings th").click(function () {
			var id = parseInt($(this).attr("id"));
			if(id === 1 || id === 2 || id === 3 || id === 5 || id === 6 || id === 7 || id === 11 || id === 12)
			{
				sortTable(id);
			}
		});
	},
	bindPaginateTable : function() {
		var pageSize = parseInt($("input[id=js-bookings-page-size]").val());
		 $('#js-bookings-list-table-body').paginateTable({pagerSelector:'#js-bookings-table-pager',showPrevNext:true,hidePageNumbers:false,perPage:pageSize});
	},
	bindSelectAllRadio : function() {
	    $('#js-select-all-on-page').on('click',function(){
	        if(this.checked){
	            $('.js-checkbox').each(function(){
	                this.checked = true;
	            });
	        }else{
	             $('.js-checkbox').each(function(){
	                this.checked = false;
	            });
	        }
	    });
	    
	    $('.js-checkbox').on('click',function(){
	        if($('.js-checkbox:checked').length == $('.js-checkbox').length){
	            $('#js-select-all-on-page').prop('checked',true);
	        }else{
	            $('#js-select-all-on-page').prop('checked',false);
	        }
	    });
	},
	bindResetBookingMarkers: function () {
		$(".js-reset-booking-markers").on('click', function (e) {
			e.preventDefault();
			var bookingRef = $(this).attr("href");
			$.when(ACC.services.resetBookingMarkers(bookingRef)).then(
				function (data) {
					if(data.result === true){
						$(".marker-" + bookingRef).addClass("hidden");
					}
				});
		});
	}
};

function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("transport-business-bookings");
	  switching = true;
	  dir = "asc"; 
	  while (switching) {
	    switching = false;
	    rows = table.rows;
	    for (i = 1; i < (rows.length - 1); i++) {
	      shouldSwitch = false;
	      x = rows[i].getElementsByTagName("TD")[n];
	      y = rows[i + 1].getElementsByTagName("TD")[n];
	      if (dir == "asc") {
	        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
	          shouldSwitch= true;
	          break;
	        }
	      } else if (dir == "desc") {
	        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
	          shouldSwitch = true;
	          break;
	        }
	      }
	    }
	    if (shouldSwitch) {
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	      switchcount ++;      
	    } else {
	      if (switchcount == 0 && dir == "asc") {
	        dir = "desc";
	        switching = true;
	      }
	    }
	  }
	}

$.fn.paginateTable = function(opts){
    var $this = this,
        defaults = {
            perPage: 10,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers===false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');

    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        if(goToPage >= 0)
    	{
    	 goTo(goToPage);
    	}
    }
     
    function next(){
        var goToPage = parseInt(pager.data("curr")) + 1;
        if(goToPage < numPages)
    	{
    	 goTo(goToPage);
    	}
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        pager.data("curr",page);
      	pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
 
        $("span[id=bookings-start-end]").text(children.slice(startAt, endOn).first().attr('id')+"-"+children.slice(startAt, endOn).last().attr('id'));
        children.find(".cancelBookingCheckbox").removeClass('js-checkbox');
        children.slice(startAt, endOn).find(".cancelBookingCheckbox").addClass('js-checkbox');
    }
    $("span[id=bookings-start-end]").text(children.slice(0, perPage).first().attr('id')+"-"+children.slice(0, perPage).last().attr('id'));
    $("span[id=total-bookings]").text(numItems);
    children.slice(0, perPage).find(".cancelBookingCheckbox").addClass('js-checkbox');
}
