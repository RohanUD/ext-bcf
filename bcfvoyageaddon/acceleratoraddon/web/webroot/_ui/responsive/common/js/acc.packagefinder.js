ACC.packagefinder = {
    _autoloadTracc: [
    	"getAllTravelSectors",
        "init",
       
    ],
    
    allTravelSectors:[],
    componentParentSelector: '#y_travelFinderForm',
    minRelativeReturnDate:0,
    terminalcacheversion:0,
   
    init: function () {
        // Sets the passenger quantity hidden value to the actualy value that is
		// displaying to user. This is a fix for browser back button.
        var $passengerQuantityArray = $(".y_travelFinderPassengerQuantity");
        $passengerQuantityArray.each(function () {
            $(this).attr('value', parseInt($(this).parent().find('.y_travelFinderPassengerQuantitySpan').text()));
        });
        
        ACC.packagefinder.bindPackageChangeTripTypeButton();
        ACC.packagefinder.bindPackageTravelWithChildrenButton();
        //ACC.packagefinder.bindPackageFareFinderValidation();
        ACC.packagefinder.bindPackageFareFinderLocationEvents();
        ACC.packagefinder.bindPackageTravelSectorsOriginAutoSuggest(); 
        ACC.packagefinder.bindPackageTravelSectorsDestinationAutoSuggest();
    },

    bindPackageChangeTripTypeButton: function () {
        $('#oneway').on('click', function () {
        	$("#y_oneWayRadbtn").prop("checked",true);
        	 $("#js-roundtrip #oneway").addClass("color-tab-active");
        	 $("#js-roundtrip #oneway").removeClass("color-tab-disble");
        	 $("#js-roundtrip #return").removeClass("color-tab-active");
        	 $("#js-roundtrip #return").addClass("color-tab-disble");
        	ACC.packagefinder.hideReturnField();
        });
        $('#return').on('click', function () {
        	$("#y_roundTripRadbtn").prop("checked",true);
        	 $("#js-roundtrip #return").addClass("color-tab-active");
        	 $("#js-roundtrip #return").removeClass("color-tab-disble");
        	 $("#js-roundtrip #oneway").removeClass("color-tab-active");
        	 $("#js-roundtrip #oneway").addClass("color-tab-disble");
        	ACC.packagefinder.showReturnField();
            ACC.packagefinder.addReturnFieldValidation($(".y_travelFinderDatePickerReturning"), $(".y_travelFinderDatePickerDeparting").val());
        });
    },

    bindPackageTravelWithChildrenButton: function () {
        // Event handler for 'travelWithChildrenButton'
        $(".y_travelFinderChildrenTrigger").click(function () {
            $(this).hide();
            $(ACC.packagefinder.componentParentSelector + " .y_travelFinderTravelingChildren").removeClass("hidden");
            $("#y_travelFinderTravelingWithChildren").val("true");
        });
    },
    

    bindPackageFareFinderValidation: function () {
        var $returnDateField = $(".y_travelFinderDatePickerReturning");
        var $departureDateField = $(".y_travelFinderDatePickerDeparting");

        $(ACC.packagefinder.componentParentSelector).validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            submitHandler: function (form) {
            	$.when(ACC.services.checkAmendmentCartInSession()).then(
    					function(data) {
    						if(data.result == true){
    							$('#y_confirmFreshBookingModal').modal('show');
    						}
    						else {
    						    var jsonData = ACC.formvalidation.serverFormValidation(form, "validateFareFinderFormAttributes");
    			                if (!jsonData.hasErrorFlag) {
    			                    $('#y_processingModal').modal({
    			                        backdrop: 'static',
    			                        keyboard: false
    			                    });
    			                    form.submit();
    			                }
    						}
    					}
    				);
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            rules: {
                departureLocationName: {
                    required: {
	           			 depends:function(){
	        				 $(this).val($.trim($(this).val()));
	        		            return true;
	        			 }
	        		},
                    locationIsValid : ".y_travelFinderOriginLocationCode"
                },
                arrivalLocationName: {
                    required: {
	           			 depends:function(){
	        				 $(this).val($.trim($(this).val()));
	        		            return true;
	        			 }
	        		},
                    locationIsValid : ".y_travelFinderDestinationLocationCode"
                },
                cabinClass: "required"
            },
            messages: {
                departureLocationName: {
                    required: ACC.addons.bcfstorefrontaddon['error.farefinder.from.location'],
                    locationIsValid: ACC.addons.bcfstorefrontaddon['error.farefinder.from.locationExists']
                },
                arrivalLocationName: {
                    required: ACC.addons.bcfstorefrontaddon['error.farefinder.to.location'],
                    locationIsValid: ACC.addons.bcfstorefrontaddon['error.farefinder.to.locationExists']
                },
                cabinClass: ACC.addons.bcfstorefrontaddon['error.farefinder.cabin.class']
            }
        });
        // add validation to departure date
        if ($departureDateField && $departureDateField.size() > 0) {
            ACC.packagefinder.addDepartureFieldValidation($departureDateField);
        }

        // if it's a "round trip", we need to validate the return date field
        if ($('#y_roundTripRadbtn').is(":checked")) {
            ACC.packagefinder.addReturnFieldValidation($returnDateField, $departureDateField.val());
        }

        // initialize date picker fields
        ACC.packagefinder.addDatePickerForFareFinder($departureDateField, $returnDateField);
    },

    bindPackageFareFinderLocationEvents: function () {
        // validation events bind for y_travelFinderOriginLocation field
		$(".y_travelFinderOriginLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_travelFinderOriginLocationCode").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				// select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a:eq( 1 )");
					firstSuggestion.click();
				}
				$(this).valid();

			}
		});

		$(".y_travelFinderOriginLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
            	$(".y_travelFinderOriginLocation").valid();
            }
        });

		$(".y_travelFinderOriginLocation").blur(function (e) {
			$(".y_travelFinderOriginLocation").valid();
		});

		$(".y_travelFinderOriginLocation").focus(function (e) {
			$(this).select();
		});

		// validation events bind for y_travelFinderDestinationLocation field
		$(".y_travelFinderDestinationLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_travelFinderDestinationLocationCode").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				// select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a:eq( 1 )");
					firstSuggestion.click();
				}
				$(this).valid();

			}
		});

		$(".y_travelFinderDestinationLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
            	if($(".y_travelFinderDestinationLocation").length==1)
            	{
            		$(".y_travelFinderDestinationLocation").valid();
            	}
            }
        });

		$(".y_travelFinderDestinationLocation").blur(function (e) {
			$(".y_travelFinderDestinationLocation").valid();
		});

		$(".y_travelFinderDestinationLocation").focus(function (e) {
			$(this).select();
		});
    },

    addDatePickerForFareFinder : function($departureDateField, $returnDateField){

         var todayDate = new Date();
         $departureDateField.datepicker({
                minDate: todayDate,
                maxDate: '+1y',
                dateFormat: 'M dd yy',

                beforeShow: function (input) {

                    setTimeout(function() {
                        if($returnDateField.length){
                            $departureDateField.rules('remove')
                        }

                        }, 0);
                },
                onClose: function (selectedDate) {
                    // add validation to departure date

                    setTimeout(function() {
                         ACC.packagefinder.addDepartureFieldValidation($departureDateField)
                        }, 0);
                    // add validation to return date, only if it's visible and
                    // validate the current field
                    if ($returnDateField.is(":visible")) {
                        if ($(this).valid()) {
                        	 ACC.packagefinder.setReturnFieldMinDate($returnDateField,selectedDate,true);
                            // change the validation of the 'return date' to
                            // have a minDate of the 'from date'

                            setTimeout(function() {
                                 ACC.packagefinder.addReturnFieldValidation($returnDateField, selectedDate)
                                }, 0);
                        }
                        else {
                        	ACC.packagefinder.setReturnFieldMinDate($returnDateField,todayDate,false);
                            // change the validation of the 'return date' to
                            // have a minDate of the 'from date'

                            setTimeout(function() {
                                 ACC.packagefinder.addReturnFieldValidation($returnDateField, ACC.travelcommon.getTodayUKDateFareFinder())
                                }, 0);

                        }
                    }
                    else {
                        $(this).valid();
                    }
                }
            });
         if($returnDateField != undefined)
         {
        $returnDateField.datepicker({
            minDate: todayDate,
            maxDate: '+1y',
            dateFormat: 'M dd yy',
            beforeShow: function (input) {

                setTimeout(function() {
                    $returnDateField.rules('remove')
                    }, 0);
            },
            onClose: function (selectedDate) {
                if ($departureDateField.valid()) {
                	ACC.packagefinder.setReturnFieldMinDate($returnDateField,$departureDateField.val(),true);
                    // change the validation of the 'return date' to have a
                    // minDate of the 'departure date'

                    setTimeout(function() {
                         ACC.packagefinder.addReturnFieldValidation($returnDateField, $departureDateField.val())
                        }, 0);

                }
                else {
                	ACC.packagefinder.setReturnFieldMinDate($returnDateField,todayDate,false);
                    // change the validation of the 'return date' to have a
                    // minDate of the 'from date'

                    setTimeout(function() {
                        ACC.packagefinder.addReturnFieldValidation($returnDateField, ACC.travelcommon.getTodayUKDateFareFinder())
                        }, 0);

                }
                $(this).valid();
            }
        });
         }
         
         $('#departingDateTime').keydown(function(e) {
        	   e.preventDefault();
        	   return false;
        	});
           $('#returnDateTime').keydown(function(e) {
          	   e.preventDefault();
          	   return false;
          	});


    },

    addDepartureFieldValidation: function ($departureDateField) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();

        $departureDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateUK,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.addons.bcfstorefrontaddon['error.farefinder.departure.date']
            }
        });
    },

    setReturnFieldMinDate:function($returnDateField, selectedDate,isUKDate){
    	var selectedUSDate=new Date();
    	if(selectedDate){
    		 selectedUSDate=selectedDate;
    		 if(isUKDate){
    			 selectedUSDate=ACC.travelcommon.convertToUSDateFareFinder(selectedDate);
    		 }
    	}
    	 var minDateForReturnDate=ACC.travelcommon.addDaysToUsDate(selectedUSDate,ACC.packagefinder.minRelativeReturnDate);
    	 $returnDateField.datepicker("option", "minDate", minDateForReturnDate);
    },

    addReturnFieldValidation: function ($returnDateField, selectedMinDate) {
        var minDateUK = ACC.travelcommon.getTodayUKDateFareFinder();
        if (selectedMinDate) {
            minDateUK = selectedMinDate;
        }

        if(!$returnDateField.length){
            return;
        }

        var selectedUSDate=ACC.travelcommon.convertToUSDateFareFinder(minDateUK);
    	var minDateForReturnDate=ACC.travelcommon.addDays(selectedUSDate,ACC.packagefinder.minRelativeReturnDate);

        $returnDateField.rules("add", {
            required: true,
            dateUKFareFinder: true,
            dateGreaterEqualTo: minDateForReturnDate,
            dateLessEqualTo: ACC.travelcommon.getHundredYearUKDate(),
            messages: {
                required: ACC.addons.bcfstorefrontaddon['error.farefinder.arrival.date'],
                dateGreaterEqualTo: ACC.addons.bcfstorefrontaddon['error.farefinder.arrival.dateGreaterEqualTo']
            }
        });
    },

    hideReturnField : function(){
   	 var $returnField = $('.y_travelFinderReturnField');
   	 $returnField.hide("fast");
        $returnField.find("input").rules("remove");
   },

   showReturnField : function(){
   	 var $returnField = $('.y_travelFinderReturnField');
   	 $returnField.show("fast");
   },


    /// auto suggestion //
    getTravelOriginMatches: function(travelSectors,locationText) {
    	locationText=locationText.toLowerCase();
		var filterTravelSectors=new Object();

		if(!$.isEmptyObject(travelSectors)) {
    		var matchingTravelSectors=[];
    		for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
    			var travelSector=travelSectors[travelSectorIndex];
    			if(travelSector.origin != null && travelSector.origin.code.toLowerCase()==locationText) {
    				matchingTravelSectors.push(travelSector);
    				break;
    			}
    		}


    		if($.isEmptyObject(matchingTravelSectors)) {
				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
					var travelSector=travelSectors[travelSectorIndex];
	    			if( travelSector.origin != null && travelSector.origin.name.toLowerCase().indexOf(locationText) != -1) {
	    				matchingTravelSectors.push(travelSector);
	    			}
	    		}
    		}

    		if($.isEmptyObject(matchingTravelSectors)) {
				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
					var travelSector=travelSectors[travelSectorIndex];
					if (travelSector.origin != null && (travelSector.origin.location.code.toLowerCase() == locationText || travelSector.origin.location.name.toLowerCase().includes(locationText))) {
						matchingTravelSectors.push(travelSector);
	    			}
	    		}
    		}

    		for(var travelSectorIndex=0; travelSectorIndex<matchingTravelSectors.length; travelSectorIndex++) {
    			var travelSector=matchingTravelSectors[travelSectorIndex];
    			var code=travelSector.origin.code;
    			if(!filterTravelSectors[code]) {
    				filterTravelSectors[code]=travelSector;
    			}
    		}
    	}

    	return filterTravelSectors;
    },

    getTravelDestinationMatches: function(travelSectors,locationText) {
    	locationText=locationText.toLowerCase();
		var filterTravelSectors=new Object();

		if(!$.isEmptyObject(travelSectors)) {
    		var matchingTravelSectors=[];
    		for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
    			var travelSector=travelSectors[travelSectorIndex];
    			if(travelSector.destination != null && travelSector.destination.code.toLowerCase()==locationText) {
    				matchingTravelSectors.push(travelSector);
    			}
    		}

    		if($.isEmptyObject(matchingTravelSectors)) {
				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
					var travelSector=travelSectors[travelSectorIndex];
	    			if( travelSector.destination != null && travelSector.destination.name.toLowerCase().indexOf(locationText) != -1) {
	    				matchingTravelSectors.push(travelSector);
	    			}
	    		}
    		}
			if ($.isEmptyObject(matchingTravelSectors)) {
				for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
					var sector = travelSectors[travelSectorIdx];
					if (sector.destination != null
							&& (sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
						matchingTravelSectors.push(sector);
					}
				}
			}
    		
    		if(!$.isEmptyObject(matchingTravelSectors))
    			filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0] ;
    	}

    	return filterTravelSectors;
    },

    getAllTravelSectors: function() {
    	$(document).on("keyup", ".y_travelFinderOriginLocation", function(e) {
			// ignore the Enter key
			if ((e.keyCode || e.which) == 13) {
				return false;
			}

			var locationText = $.trim($(this).val());
			ACC.packagefinder.fireQueryToFetchAllTravelSectors(locationText);
		});
    },

    fireQueryToFetchAllTravelSectors: function(locationText){
    	if(locationText.length >= 3) {
			if($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion"))
				{
			$.when(ACC.services.getTravelSectorsAjax()).then(function(data) {
	    		ACC.packagefinder.allTravelSectors=data.travelRoute;
	    		ACC.packagefinder.terminalcacheversion=data.terminalsCacheVersion;
	    		localStorage.setItem("travelSector", JSON.stringify(ACC.packagefinder.allTravelSectors));
	    		localStorage.setItem("terminalCacheVersion", ACC.packagefinder.terminalcacheversion);
	    	});
		}
			else{
				ACC.packagefinder.allTravelSectors=JSON.parse(localStorage.getItem("travelSector"));
			}
		}
    },

    // Get All Travel Sectors
    bindPackageTravelSectorsOriginAutoSuggest: function () {
        $(".y_travelFinderOriginLocation").autosuggestion({
            inputToClear: ".y_travelFinderDestinationLocation",
            autosuggestServiceHandler: function (locationText) {
                var suggestionSelect = "#y_travelFinderOriginLocationSuggestions";
                var travelSectors=ACC.packagefinder.allTravelSectors;
        		var filterTravelSectors=ACC.packagefinder.getTravelOriginMatches(travelSectors,locationText);
            	if(!$.isEmptyObject(filterTravelSectors)) {
                	var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                	var htmlStringEnd = '</ul> </div>';
                	var htmlStringBody = "";
                	var titleDisplayed = 0;
                	var htmlStringTitle = "";
                	$(suggestionSelect).html(htmlStringStart);

                	for(var code in filterTravelSectors) {
                		var filteredJson=filterTravelSectors[code];
                		if(titleDisplayed == 0){
                			htmlStringTitle = ' <li class="parent"> <span class="title">'+
                    		' <a href="" class="autocomplete-suggestion inactiveLink" data-code= "'+filteredJson.origin.location.code+
                    		'" data-suggestiontype="'+filteredJson.origin.location.locationType+'" >'+ filteredJson.origin.location.name +' - ' + filteredJson.origin.location.superlocation[0].name +  '</a> </span> </li>'+'<li class="child"> <a href="" class="autocomplete-suggestion" '+
                    		'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + ' - '+filteredJson.origin.name+'('+ filteredJson.origin.code +')</a> </li> ';
                			titleDisplayed = 1;
                		}
                		else{
                			htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" '+
                		'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + ' - '+filteredJson.origin.name+'('+ filteredJson.origin.code +')</a> </li> ';
                		}
                	}
                	var htmlContent = htmlStringStart + htmlStringTitle +htmlStringBody + htmlStringEnd;
                	$(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
            	}
            	else{
            		var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                	$(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
            	}
            },
            suggestionFieldChangedCallback: function () {
                // clear the destination fields
                $(".y_travelFinderDestinationLocation").val("");
                $(".y_travelFinderDestinationLocationCode").val("");
            },
            attributes : ["Code", "SuggestionType"]
        });
    },

    // Suggestions/autocompletion for Destination location
    bindPackageTravelSectorsDestinationAutoSuggest: function () {
        $(".y_travelFinderDestinationLocation").autosuggestion({
            autosuggestServiceHandler: function (destinationText) {
                var suggestionSelect = "#y_travelFinderDestinationLocationSuggestions";
                var originCode = $(".y_travelFinderOriginLocationCode").val().toLowerCase();

                var travelSectors=ACC.packagefinder.allTravelSectors;
                if(travelSectors == null || travelSectors.length==0)
                {
                	ACC.packagefinder.fireQueryToFetchAllTravelSectors(originCode);
                	travelSectors=ACC.packagefinder.allTravelSectors;
                }
                var matchedTravelSectors=ACC.packagefinder.getTravelSectorsWithOrigin(travelSectors,originCode);
        		var filterTravelSectors=ACC.packagefinder.getTravelDestinationMatches(matchedTravelSectors,destinationText);

        		if(!$.isEmptyObject(filterTravelSectors)) {
                	var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                	var htmlStringEnd = '</ul> </div>';
                	var htmlStringBody = "";
                	var titleDisplayed = 0;
                	$(suggestionSelect).html(htmlStringStart);

                	for(var code in filterTravelSectors) {
                		var filteredJson=filterTravelSectors[code];
                		if(titleDisplayed == 0 ){
                			htmlStringBody = htmlStringBody + '<li class="parent"> <span class="title">'+
                     		' <a href="" class="autocomplete-suggestion inactiveLink" data-routetype="'+filteredJson.routeType+'" data-code= "'+filteredJson.destination.location.code+
                     		'" data-suggestiontype="'+filteredJson.destination.location.locationType+'" >'+ filteredJson.destination.location.name +' - ' + filteredJson.destination.location.superlocation[0].name +  '</a> </span> </li>' +'<li class="child"> <a href="" class="autocomplete-suggestion" '+
                     		'data-code="'+filteredJson.destination.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + ' - '+filteredJson.destination.name+'('+ filteredJson.destination.code +')</a> </li> ';
                			titleDisplayed = 1;
                		}
                		else{
                		htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" date-routetype="'+filteredJson.routeType+'" '+
                 		'data-code="'+filteredJson.destination.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + ' - '+filteredJson.destination.name+'('+ filteredJson.destination.code +')</a> </li> ';
                		}
                 	}
                 	var htmlContent = htmlStringStart +  htmlStringBody + htmlStringEnd;
                 	$(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
        		}
        		else{
            		var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                	$(suggestionSelect).html(htmlContent);
                    $(suggestionSelect).removeClass("hidden");
            	}
            },
            attributes : ["Code", "SuggestionType"]
        });
    },

    getTravelSectorsWithOrigin: function(travelSectors, originCode) {
		var matchingTravelSectors=[];
    	if(!$.isEmptyObject(travelSectors)) {
    		for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
    			var travelSector=travelSectors[travelSectorIndex];
    			if(travelSector.origin != null && (travelSector.origin.code.toLowerCase()==originCode || travelSector.origin.location.code.toLowerCase()==originCode)) {
    				matchingTravelSectors.push(travelSector);
    			}
    		}
    	}
    	return matchingTravelSectors;
    }
};
