ACC.checkin = {
	_autoloadTracc : [ "bindcheckinValidationMessages","init", "bindCheckinFormValidation" ],

	init : function() {
		var todayDate = new Date();
		$('[class$=documentExpiryDate]').datepicker(
		{	
			changeYear: true,
			changeMonth: true,
			minDate : todayDate,
			yearRange : todayDate.getFullYear() + ":"
					+ (todayDate.getFullYear() + 15),
			onClose : function(selectedDate) {
				$(this).valid();
			}
		});

		$('[class$=dateOfBirth]').datepicker({
			changeYear: true,
			changeMonth: true,
			maxDate : todayDate,
			yearRange : 1900 + ":" + todayDate.getFullYear(),
			onClose : function(selectedDate) {
				$(this).valid();
			}
		});
	},
	bindcheckinValidationMessages : function () {
		ACC.checkinValidationMessages = new validationMessages("ferry-checkinValidationMessages");
        ACC.checkinValidationMessages.getMessages("error");
	},
	bindCheckinFormValidation : function() {
		$("#checkInForm").validate({
			errorElement : "span",
			errorClass : "fe-error",
			onfocusout : function(element) {
				$(element).valid();
			}
		});

		$("[class$=documentType]")
				.each(
						function() {
							$(this)
									.rules(
											"add",
											{
												required : true,
												messages : {
													required : ACC.checkinValidationMessages.message('error.checkinform.documentNumber.required')
												}
											});
						});
		$("[class$=documentNumber]")
				.each(
						function() {
							$(this)
									.rules(
											"add",
											{
												required : true,
												maxlength: 30,
												messages : {
													required : ACC.checkinValidationMessages.message('error.checkinform.documentNumber.required'),
													maxlength: ACC.checkinValidationMessages.message('error.checkinform.documentNumber.maxlength')
												}
											});
						});
		$(".y_checkinApDocExpiry")
				.each(
						function() {
							$(this)
									.rules(
											"add",
											{
												required : true,
												dateUK : true,
												messages : {
													required : ACC.checkinValidationMessages.message('error.checkinform.documentExpiryDate.required'),
													dateUK : ACC.checkinValidationMessages.message('error.checkinform.documentExpiryDate.dateuk')
												}
											});
						});

		$(".y_checkinApDob")
				.each(
						function() {
							$(this)
									.rules(
											"add",
											{
												required : true,
												dateUK : true,
												messages : {
													required : ACC.checkinValidationMessages.message('error.checkinform.dateOfBirth.required'),
													dateUK : ACC.checkinValidationMessages.message('error.checkinform.dateOfBirth.dateuk')
												}
											});

						});
	}
}
