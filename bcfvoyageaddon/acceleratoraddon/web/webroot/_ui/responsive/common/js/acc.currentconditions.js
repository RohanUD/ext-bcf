ACC.currentconditions = {
	_autoloadTracc: [
		"validationMesagesInit",
		"getAllTravelSectors",
		"getTerminalName",
		"refreshAtaGlanceData",
		"bindFerryTrackings",
		"init",
		"mapViewTabClick",
		"bindCurrentConditionsSubmitHandler",
		"bindActivateTabs"
	],

	googleApiKey: $('#currentconditionmap').data('googleapi'),
	componentDealParentSelector: '#y_currentConditionsForm',
	activeWindow: null,
	displayedMap: null,

	init: function () {
		ACC.currentconditions.fireQueryToFetchAllCCRouteInfo();
		ACC.currentconditions.bindCurrentConditionsTravelSectorsOriginAutoSuggest();
		ACC.currentconditions.bindCurrentConditionsTravelSectorsDestinationAutoSuggest();
		ACC.currentconditions.bindCurrentConditionValidation();
	},

	fireQueryToFetchAllCCRouteInfo: function () {
		var ccRouteVersion = localStorage.getItem("ccRouteVersion");
		if ($("#" + ccRouteVersion).length == 0 || parseInt($("#" + ccRouteVersion).val()) != ccRouteVersion) {
			$.when(ACC.services.getCCRouteInfoAjax()).then(function (data) {
				ACC.currentconditions.allCCRouteInfo = data;
				ACC.currentconditions.ccRouteVersion = 1;
				localStorage.setItem("ccRouteInfo", JSON.stringify(ACC.currentconditions.allCCRouteInfo));
				localStorage.setItem("ccRouteVersion", ACC.currentconditions.ccRouteVersion);
				ACC.currentconditions.accordionUI();
			});
		} else {
			ACC.currentconditions.allCCRouteInfo = JSON.parse(localStorage.getItem("ccRouteInfo"));
			ACC.currentconditions.accordionUI();
		}
	},

	bindActivateTabs: function () {
		$(".ccTabJs").click(function () {
			$("#selectedTab").val($(this).attr("name"));
		});
	},

	accordionUI: function () {
		var travelSectors = ACC.currentconditions.allCCRouteInfo;
		var filteredRoutes = ACC.farefinder.categorizeTravelSectors(travelSectors);
		var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes);

		$("#currentcondition-accordion1").html(filteredRouteHtml);
		$("#currentcondition-accordion1").find('.sub-link').click(ACC.currentconditions.sourceClickHandler);
		$("#currentcondition-accordion1").accordion({
			header: "h3",
			collapsible: true,
			active: false,
			heightStyle: "content",
			beforeActivate: function( event, ui ) {
				var wd = $(event.currentTarget).outerWidth();
				$(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
			}
		});

		$("#currentcondition-accordion2").html(filteredRouteHtml);
		$("#currentcondition-accordion2").accordion({
			header: "h3",
			collapsible: true,
			active: false,
			heightStyle: "content",
			beforeActivate: function( event, ui ) {
				var wd = $(event.currentTarget).outerWidth();
				$(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
			}
		});
		$("#currentcondition-accordion2").find('.sub-link').click(ACC.currentconditions.destinationClickHandlerReverse);

		$("#currentcondition-accordion1").click(function () {
			$("#currentcondition-accordion2").closest(".ui-accordion").accordion({
				active: 2,
				beforeActivate: function( event, ui ) {
					var wd = $(event.currentTarget).outerWidth();
					$(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
				}
			});
		});

		$("#currentcondition-accordion2").click(function () {
			$("#currentcondition-accordion1").closest(".ui-accordion").accordion({
				active: 2
			});
		});

		// $("#currentcondition-accordion1").accordion({ header: "h3", collapsible: true, active: false });
		$("#currentcondition-accordion2").accordion({
			header: "h3",
			collapsible: true,
			active: false,
			beforeActivate: function( event, ui ) {
				var wd = $(event.currentTarget).outerWidth();
				$(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
			}
		});
		setTimeout(function () {
			$("body").trigger("setSearchDropDown", $("#currentcondition-accordion1"));
			$("body").trigger("setSearchDropDown", $("#currentcondition-accordion2"));
		}, 200);
	},
	destinationClickHandlerReverse: function (event, ui) {
		ACC.farefinder.bindDropDownselection($(this));
		var code = $(this).data("code");
		var filteredSource = [];
		_.forEach(ACC.currentconditions.allCCRouteInfo, function (val) {
			_.forEach(val.destinationRoutes, function (dest) {
				if (dest.destination == code) {
					filteredSource.push(val);
				}
			})
		})

		filteredRoutes = ACC.farefinder.categorizeTravelSectors(filteredSource);
		var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes);

		var htmlEl = $('<div class="accordion-34" id="currentcondition-accordion1" data-inputselector="toLocation" data-routeinfoselector="y_fareFinderDestinationLocationCode"  data-suggestionselector="y_fareFinderOriginLocationSuggestionType"></div>');
		htmlEl.append(filteredRouteHtml);
		$("#currentcondition-accordion1").replaceWith(htmlEl);
		$("#currentcondition-accordion1").find('.sub-link').click(ACC.currentconditions.sourceClickHandlerReverse);
		$("#currentcondition-accordion1").accordion({
			header: "h3",
			collapsible: true,
			active: false,
			beforeActivate: function( event, ui ) {
				var wd = $(event.currentTarget).outerWidth();
				$(event.currentTarget).siblings("div.ui-accordion-content").css({'width': wd});
			}
		});
		$("#currentcondition-accordion1").closest(".bc-accordion").trigger("searchOnKeyPress");

		setTimeout(function () {
			$("body").trigger("setSearchDropDown", $("#currentcondition-accordion1"));
		}, 200);

		$("#arrivalLocation").val(code);
		$("#arrivalLocationSuggestionType").val("AIRPORTGROUP");

	},
	sourceClickHandlerReverse: function (event, ui) {
		ACC.farefinder.bindDropDownselection($(this));
		$(".y_currentConditionsOriginLocationCode").val($(this).data("code"));
	},
	sourceClickHandler: function (event, ui) {
		ACC.farefinder.bindDropDownselection($(this));
		var destination = $(this).data("destination").split("|");
		var filteredDestination = {};
		var destinationIsWalkOnMap = {};
		var destinationRouteTypeMap = {};
		_.forEach(destination, function (val, key) {
			var fD = val.split(":");
			filteredDestination[fD[0]] = fD[3];
			destinationIsWalkOnMap[fD[0]] = fD[1];
			destinationRouteTypeMap[fD[0]] = fD[2];
		});

		//setting data to hidden fields
		$(".y_currentConditionsOriginLocationCode").val($(this).data("code"));
		$("#y_currentConditionsDepartureLocationTravelroute").html($(this).data("travelroute"));
		destination = _.keys(filteredDestination);
		var filteredRoutes = ACC.farefinder.destinationRoute;
		filteredRoutes = _.filter(filteredRoutes, function (el) {
			if (_.indexOf(destination, el.destination) != -1) {
				return true;
			}
			return false;
		})
		filteredRoutes = ACC.farefinder.categorizeTravelSectors(filteredRoutes);
		var filteredRouteHtml = ACC.farefinder.renderRouteDropDownHtml(filteredRoutes, filteredDestination, destinationIsWalkOnMap, destinationRouteTypeMap);

		var htmlEl = $('<div class="accordion-34" id="currentcondition-accordion2" data-inputselector="toLocation" data-routeinfoselector="y_fareFinderDestinationLocationCode"  data-suggestionselector="y_fareFinderOriginLocationSuggestionType"></div>');
		htmlEl.append(filteredRouteHtml);
		$("#currentcondition-accordion2").replaceWith(htmlEl);
		$("#currentcondition-accordion2").find('.sub-link').click(ACC.currentconditions.destinationClickHandler);
		$("#currentcondition-accordion2").accordion({
			header: "h3",
			collapsible: true,
			active: false
		});
		$("#currentcondition-accordion2").closest(".bc-accordion").trigger("searchOnKeyPress");

		setTimeout(function () {
			$("body").trigger("setSearchDropDown", $("#currentcondition-accordion2"));
		}, 200);
		$(this).closest(".accordion-34").addClass("cc-landing-select-color");
	},
	destinationClickHandler: function (event, ui) {
		ACC.farefinder.bindDropDownselection($(this));
		var code = $(this).data("code");
		$(".y_currentConditionsDestinationLocationCode").val(code);
		$("#y_currentConditionsArrivalLocationTravelroute").html($(this).data("travelroute"));
		$(this).closest(".accordion-34").addClass("cc-landing-select-color");
	},
	validationMesagesInit: function () {
		ACC.validationMessages.currentcondition = new validationMessages("ferry-currentConditionsForm");
		ACC.validationMessages.currentcondition.getMessages("error");
	},
	bindCurrentConditionValidation: function () {

		$(ACC.currentconditions.componentDealParentSelector).validate({
			errorElement: "span",
			errorClass: "fe-error",
			onfocusout: function (element) {
				$(element).valid();
			},
			submitHandler: function (form) {
				$(".custom-error-4").addClass("hidden");
				if ($("#currentcondition-accordion1").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")) {
					$(".custom-error-4 span").html(ACC.validationMessages.currentcondition.message("error.farefinder.currentcondition.from.location"));
					$(".custom-error-4").removeClass("hidden");
					ACC.carousel.adjustSearchHeight();
					return false;
				}

				if ($("#currentcondition-accordion2").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")) {
					$(".custom-error-4 span").html(ACC.validationMessages.currentcondition.message("error.farefinder.currentcondition.to.location"));
					$(".custom-error-4").removeClass("hidden");
					ACC.carousel.adjustSearchHeight();
					return false;
				}

				form.submit();
			},
			onkeyup: false,
			onclick: false
		});
	},
	bindFerryTrackings: function () {
		var loadMoreStateControl = function () {
			$(".content-wrapper").each(function () {

				if ($(this).find(".showMore").length == 0) {
					$(this).find(".ferryTrackingLoadMore").hide();
				} else {
					$(this).find(".ferryTrackingLoadMore").show();
				}

			})
		};
		loadMoreStateControl();
		$('#selectFerryTrackingRoute').on('change', function (e) {
			var routeRegVal = $(this).val();
			var regionVal = routeRegVal.split("__")[0];
			var routeVal = routeRegVal.split("__")[1];

			if (routeRegVal == 'default') {
				$('.ferry-box-map').show();
				$('.ferry-region-map').show();
				$('.ferry-hr-separator').show();
				loadMoreStateControl();
			} else {
				$('.ferry-box-map').hide();
				$('.ferry-region-map').hide();
				$('.ferry-hr-separator').hide();
				$('#' + regionVal).show();
				$('#ferryMap-' + routeVal).show();
				$(".content-wrapper").each(function () {

					if ($(this).find(".ferry-box-map:visible").length <= 6) {
						$(this).find(".ferryTrackingLoadMore").hide();
					}

				})
			}

			$(".content-wrapper").each(function () {
				$(this).removeClass("hidden");
				if ($(this).find(".ferry-box-map:visible").length === 0) {
					$(this).addClass("hidden");
				}
			})

		});
	},
	refreshAtaGlanceData: function () {
		$('#refreshDepartures').on("click", function () {
			var url = location.href.split("?")[0];
			if ($("#terminalCode").length) {
				url = url + "?terminalCode=" + $("#terminalCode").val();
			}
			location.href = url;
		});

		$('#viewAllSailingLink').on("click", function () {
			$("#sailingData:hidden").each(function () {
				$(this).show();
			});
		});

		$('.ferryTrackingLoadMore').on("click", function () {
			$(this).closest(".content-wrapper").find(".showMore.hidden").each(function () {
				$(this).removeClass("hidden")
				$(this).removeClass("showMore")
			});
			$(this).closest(".row").remove();
		});

		$('#ccLandingMap').on("click", function () {
			$("#currentConditionsLandingMap").show();
			$("#currentConditionsLandingList").hide();
		});

		$('#ccLandingList').on("click", function () {
			$("#currentConditionsLandingMap").hide();
			$("#currentConditionsLandingList").show();
		});

		$('#selectDepartingTerminal').on('change', function (e) {
			var selectedIdx = $("#selectDepartingTerminal option:selected").index();
			var url = location.href.split("?")[0];
			if (selectedIdx > 0) {
				var terminalCode = $("#selectDepartingTerminal").val();
				url = url + "?terminalCode=" + terminalCode;
			}
			location.href = url;

		});

	},

	getTerminalName: function () {
		var url = $("#getTerminalNameUrl").val();
		$(document).ready(function () {
			var locationText = $("#SailingDetails").val();
			var vesselName = "";
			if (locationText != undefined && locationText.indexOf("-") !== -1) {
				vesselName = locationText.split("-").pop();
			}
			$('#destTerminalName').replaceWith(vesselName);
		});
		$('#SailingDetails').on('change', function (e) {

			var locationText = $("#SailingDetails").val();
			var vesselName = locationText.split("-").pop();
			$('#destTerminalName').replaceWith(vesselName);

		});
	},

	getAllTravelSectors: function () {
		$(document).on("keyup", ".y_currentConditionsOriginLocation", function (e) {
			// ignore the Enter key
			if ((e.keyCode || e.which) == 13) {
				return false;
			}

			var locationText = $.trim($(this).val());
			ACC.dealfinder.fireQueryToFetchAllTravelSectors(locationText);
		});
	},

	fireQueryToFetchAllTravelSectors: function (locationText) {
		if (locationText.length >= 3) {
			if ($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion")) {
				$.when(ACC.services.getTravelSectorsAjax()).then(function (data) {
					ACC.farefinder.allTravelSectors = data.travelRoute;
					ACC.farefinder.terminalcacheversion = data.terminalsCacheVersion;
					localStorage.setItem("travelSector", JSON.stringify(ACC.farefinder.allTravelSectors));
					localStorage.setItem("terminalCacheVersion", ACC.farefinder.terminalcacheversion);
				});
			} else {
				ACC.farefinder.allTravelSectors = JSON.parse(localStorage.getItem("travelSector"));
			}
		}
	},

	// Get All Travel Sectors
	bindCurrentConditionsTravelSectorsOriginAutoSuggest: function () {
		$(".y_currentConditionsOriginLocation").autosuggestion({
			inputToClear: ".y_currentConditionsDestinationLocation",
			autosuggestServiceHandler: function (locationText) {
				var suggestionSelect = "#y_currentConditionsOriginLocationSuggestions";
				var travelSectors = ACC.farefinder.allTravelSectors;
				var filterTravelSectors = ACC.farefinder.getTravelOriginMatches(travelSectors, locationText);
				if (!$.isEmptyObject(filterTravelSectors)) {
					var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
					var htmlStringEnd = '</ul> </div>';
					var htmlStringBody = "";
					var titleDisplayed = 0;
					var htmlStringTitle = "";
					$(suggestionSelect).html(htmlStringStart);

					for (var code in filterTravelSectors) {
						var filteredJson = filterTravelSectors[code];
						if (titleDisplayed == 0) {
							htmlStringTitle = ' <li class="parent"> <span class="title">' +
								' <a href="" class="autocomplete-suggestion inactiveLink" data-code= "' + filteredJson.origin.location.code +
								'" data-suggestiontype="' + filteredJson.origin.location.locationType + '" >' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.location.superlocation[0].name + '</a> </span> </li>' + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
								'data-code="' + filteredJson.origin.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.name + '(' + filteredJson.origin.code + ')</a> </li> ';
							titleDisplayed = 1;
						} else {
							htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
								'data-code="' + filteredJson.origin.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.origin.location.name + ' - ' + filteredJson.origin.name + '(' + filteredJson.origin.code + ')</a> </li> ';
						}
					}
					var htmlContent = htmlStringStart + htmlStringTitle + htmlStringBody + htmlStringEnd;
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				} else {
					var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
			},
			suggestionFieldChangedCallback: function () {
				// clear the destination fields
				$(".y_currentConditionsDestinationLocation").val("");
				$(".y_currentConditionsDestinationLocationCode").val("");
			},
			attributes: ["Code", "SuggestionType"]
		});
	},

	// Suggestions/autocompletion for Destination location
	bindCurrentConditionsTravelSectorsDestinationAutoSuggest: function () {
		$(".y_currentConditionsDestinationLocation").autosuggestion({
			autosuggestServiceHandler: function (destinationText) {
				var suggestionSelect = "#y_currentConditionsDestinationLocationSuggestions";
				var originCode = $(".y_currentConditionsOriginLocationCode").val().toLowerCase();

				var travelSectors = ACC.farefinder.allTravelSectors;
				if (travelSectors == null || travelSectors.length == 0) {
					ACC.farefinder.fireQueryToFetchAllTravelSectors(originCode);
					travelSectors = ACC.farefinder.allTravelSectors;
				}
				var matchedTravelSectors = ACC.farefinder.getTravelSectorsWithOrigin(travelSectors, originCode);
				var filterTravelSectors = ACC.farefinder.getTravelDestinationMatches(matchedTravelSectors, destinationText);

				if (!$.isEmptyObject(filterTravelSectors)) {
					var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
					var htmlStringEnd = '</ul> </div>';
					var htmlStringBody = "";
					titleDisplayed = 0;
					$(suggestionSelect).html(htmlStringStart);

					for (var code in filterTravelSectors) {
						var filteredJson = filterTravelSectors[code];
						if (titleDisplayed == 0) {
							htmlStringBody = htmlStringBody + '<li class="parent"> <span class="title">' +
								' <a href="" class="autocomplete-suggestion inactiveLink" data-routetype="' + filteredJson.routeType + '" data-code= "' + filteredJson.destination.location.code +
								'" data-suggestiontype="' + filteredJson.destination.location.locationType + '" >' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.location.superlocation[0].name + '</a> </span> </li>' + '<li class="child"> <a href="" class="autocomplete-suggestion" ' +
								'data-code="' + filteredJson.destination.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.name + '(' + filteredJson.destination.code + ')</a> </li> ';
							titleDisplayed = 1;
						} else {
							htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" date-routetype="' + filteredJson.routeType + '" ' +
								'data-code="' + filteredJson.destination.code + '" data-suggestiontype="AIRPORTGROUP">' + filteredJson.destination.location.name + ' - ' + filteredJson.destination.name + '(' + filteredJson.destination.code + ')</a> </li> ';
						}
					}
					var htmlContent = htmlStringStart + htmlStringBody + htmlStringEnd;
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				} else {
					var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
			},
			attributes: ["Code", "SuggestionType"]
		});
	},

	getTravelSectorsWithOrigin: function (travelSectors, originCode) {
		var matchingTravelSectors = [];
		if (!$.isEmptyObject(travelSectors)) {
			for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
				var travelSector = travelSectors[travelSectorIndex];
				if (travelSector.origin != null && travelSector.origin.location.code.toLowerCase() == originCode) {
					matchingTravelSectors.push(travelSector);
				}
			}
		}
		return matchingTravelSectors;
	},

	getTravelDestinationMatches: function (travelSectors, locationText) {
		locationText = locationText.toLowerCase();
		var filterTravelSectors = new Object();

		if (!$.isEmptyObject(travelSectors)) {
			var matchingTravelSectors = [];
			for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
				var travelSector = travelSectors[travelSectorIndex];
				if (travelSector.destination != null && travelSector.destination.location.code.toLowerCase() == locationText) {
					matchingTravelSectors.push(travelSector);
				}
			}

			if ($.isEmptyObject(matchingTravelSectors)) {
				for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
					var travelSector = travelSectors[travelSectorIndex];
					if (travelSector.destination != null && travelSector.destination.code.toLowerCase() == locationText) {
						matchingTravelSectors.push(travelSector);
					}
				}
			}

			if ($.isEmptyObject(matchingTravelSectors)) {
				for (var travelSectorIndex = 0; travelSectorIndex < travelSectors.length; travelSectorIndex++) {
					var travelSector = travelSectors[travelSectorIndex];
					if (travelSector.destination != null && travelSector.destination.location.name.toLowerCase().indexOf(locationText) != -1) {
						matchingTravelSectors.push(travelSector);
					}
				}
			}
			if ($.isEmptyObject(matchingTravelSectors)) {
				for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
					var sector = travelSectors[travelSectorIdx];
					if (sector.destination != null &&
						(sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
						matchingTravelSectors.push(sector);
					}
				}
			}

			if (!$.isEmptyObject(matchingTravelSectors))
				filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0];
		}

		return filterTravelSectors;
	},

	addGoogleMapsApi: function () {
		var callback = "ACC.currentconditions.loadGoogleMap";
		if (callback != undefined && $(".js-googleMapsApi").length == 0) {
			$('head').append('<script async defer class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.currentconditions.googleApiKey + '&callback=' + callback + '"></script>');
		} else if (callback != undefined) {
			eval(callback + "()");
		}
	},
	loadGoogleMap: function () {
		var map = new google.maps.Map(document.getElementById('currentconditionmap'), {
			mapTypeControl: false,
			streetViewControl: false,
			styles: ACC.schedule.mapStyle
		});
		var listOfTravelRouteCount = $('#listOfCurrentConditionsCount').attr('value');
		var processedFile = [];
		var checkDuplicateFile = [];
		var processedTerminal = [];
		var infoContentMap = {};
		var mapKmlFileMap = {};
		var mapDiv = $('.y_currentconditionmapurl');
		var url = mapDiv.data('currentcondtiondetailsmapurl')
		var originToDestinationMappingCount = $('#originToDestinationMappingCount').attr('value');
		for (var j = 0; j < originToDestinationMappingCount; j++) {

			var routeOriginKey = "#routeOrigin" + j;
			var routeOriginFilteredNameDisplay = $(routeOriginKey).data("originfilteredname");
			var routeOriginFilteredCodeDisplay = $(routeOriginKey).data("originfilteredcode");
			var routeOriginNameDisplay = $(routeOriginKey).data("originname");

			var destinationCountKey = "#destinationCount" + j;
			var destinationCountDisplay = $(destinationCountKey).data("destinationcount");

			var infoContent = ACC.viewSailingStatusFrom + "<br><b>" + routeOriginNameDisplay + "</b><br>" + ACC.viewScheduleTo + ": " + "<br>";

			for (var k = 0; k < destinationCountDisplay; k++) {

				var routeDestinationKey = "#routeDestination" + j + k;
				var routeDestinationFilteredNameDisplay = $(routeDestinationKey).data("destinationfilteredname");
				var routeDestinationFilteredCodeDisplay = $(routeDestinationKey).data("destinationfilteredcode");
				var routeDestinationNameDisplay = $(routeDestinationKey).data("destinationname");

				var createURL = url + routeOriginFilteredNameDisplay + "-" + routeDestinationFilteredNameDisplay + "/" +
					routeOriginFilteredCodeDisplay + "-" + routeDestinationFilteredCodeDisplay;
				infoContent = infoContent + '<a href="' + createURL + '">' + routeDestinationNameDisplay + '</a>' + "<br>";

				var terminalCode = routeOriginFilteredCodeDisplay;
				infoContentMap[terminalCode] = infoContent;

			}
		}
		for (var m = 0; m < listOfTravelRouteCount; m++) {
			var originDesitinationMapCounttKey = "#originDesitinationMapCount" + m;
			var originDesitinationMapCount = $(originDesitinationMapCounttKey).attr('value');

			for (var i = 0; i < originDesitinationMapCount; i++) {
				var routeOriginDestinationKey = "#routeOriginDestination" + m + i;
				var mapkmlfile = $(routeOriginDestinationKey).data("mapkmlfile");
				var mapkmlInternalfileName = $(routeOriginDestinationKey).data("mapkmlinternalfilename");
				if(mapkmlfile != undefined) {
					var mapkmlInternalfileNameSeparator = mapkmlInternalfileName.split("-");
					var duplicateMapkmlInternalfileName = mapkmlInternalfileNameSeparator[1] + "-" +
						mapkmlInternalfileNameSeparator[0];
					if (!checkDuplicateFile.includes(duplicateMapkmlInternalfileName)) {
						checkDuplicateFile.push(mapkmlInternalfileName);
						var kmlFilePathName = mapkmlfile;
						var lineSymbol = {
							path: 'M 0,-0.1 0,0.5',
							strokeOpacity: 1,
							scale: 4
						};

						// Comment the below line, if you are testing in local env
						var absoluteFileName = mapkmlfile;

						// And Uncomment the below line to work in local env.
						//var absoluteFileName = mapkmlfile.substring(0, mapkmlfile.indexOf("?"));

						mapKmlFileMap[absoluteFileName] = mapkmlInternalfileName;

						var listenerDebounce;
						var bounds = new google.maps.LatLngBounds();
						var createMarker = function (placemark, doc) {
							var travelRoute = mapKmlFileMap[placemark.styleBaseUrl];
							var sameFileCount = 1;
							if (!processedFile.includes(travelRoute)) {
								processedFile.push(travelRoute);
							} else {
								sameFileCount = 2;
							}
							var travelRouteOriginAndDestinationSeparator = travelRoute.split("-");
							var travelRouteOrigin = travelRouteOriginAndDestinationSeparator[0];
							var travelRouteDestination = travelRouteOriginAndDestinationSeparator[1];
							var isIncluded = true;
							var infoWindowContent;
							if (!processedTerminal.includes(travelRouteOrigin) && sameFileCount == 1) {
								processedTerminal.push(travelRouteOrigin);
								infoWindowContent = infoContentMap[travelRouteOrigin];
								isIncluded = false;
							} else if (!processedTerminal.includes(travelRouteDestination) && sameFileCount == 2) {
								processedTerminal.push(travelRouteDestination);
								infoWindowContent = infoContentMap[travelRouteDestination];
								isIncluded = false;
							}
							var marker = null;
							if (!isIncluded) {
								var markerOptions = geoXML3.combineOptions(geoXml.options.markerOptions, {
									map: geoXml.options.map,
									position: new google.maps.LatLng(placemark.Point.coordinates[0].lat, placemark.Point.coordinates[0].lng)
								});

								// Create the marker on the map
								marker = new google.maps.Marker(markerOptions);

								// Set up and create the infowindow if it is not suppressed
								if (!geoXml.options.suppressInfoWindows && !isIncluded) {
									var infoWindowOptions = geoXML3.combineOptions(geoXml.options.infoWindowOptions, {
										content: infoWindowContent
									});
									if (geoXml.options.infoWindow) {
										marker.infoWindow = geoXml.options.infoWindow;
									} else {
										marker.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
									}
									marker.infoWindowOptions = infoWindowOptions;
									bounds.extend(marker.position);
									var listener = google.maps.event.addListener(map, "idle", function () {
										clearTimeout(listenerDebounce);
										listenerDebounce = setTimeout(map.fitBounds(bounds, 80), 250);
										google.maps.event.removeListener(listener);
									});
									// Infowindow-opening event handler
									google.maps.event.addListener(marker, 'click', function () {
										//								this.infoWindow.close();
										marker.infoWindow.setOptions(this.infoWindowOptions);
										this.infoWindow.open(this.map, this);

									});
									google.maps.event.addListener(map, 'click', function (event) {
										marker.infoWindow.close();
									});
								}
								placemark.marker = marker;
							}
							return marker;
						};

						var geoXml = new geoXML3.parser({
							map: map,
							singleInfoWindow: true,
							createMarker: createMarker,
							markerOptions: {
								icon: {
									url: 'https://snazzy-maps-cdn.azureedge.net/assets/marker-c5ce3bc4-d268-4b8a-9941-285dc369ed54.png',
									scaledSize: new google.maps.Size(
										24,
										24),
									size: new google.maps.Size(
										24,
										24),
									anchor: new google.maps.Point(
										12,
										24)
								}
							},
							polylineOptions: {
								strokeOpacity: 0,
								strokeColor: '#0079a6',
								icons: [{
									icon: lineSymbol,
									offset: '0',
									repeat: '10px'
								}],
								clickable: false,
								strokeWeight: 4
							},
						});
						geoXml.parse(kmlFilePathName);
					}
				}
			}
		}

	},

	mapViewTabClick: function () {
		$('#tabToMapClick').on("click", function () {
			ACC.currentconditions.addGoogleMapsApi();
		});
	},
	bindCurrentConditionsSubmitHandler: function () {
		$(document).on('click', '.js-current-conditions-submit', function (event) {
			event.preventDefault();
			var targetURL = $('.js-current-conditions-url').text() + "/" +$("#y_currentConditionsDepartureLocationTravelroute").html()+"-"+$("#y_currentConditionsArrivalLocationTravelroute").html()+ "/" + $('.y_currentConditionsOriginLocationCode').val() + "-" + $('.y_currentConditionsDestinationLocationCode').val();
			$('#y_currentConditionsForm').attr('action', targetURL);
			$('#y_currentConditionsForm').submit();
		});
	},

	bindCollapsibles: function () {

		$(".view-deck").click(function () {
			$(this).siblings(".view-deck-div").toggle();
			toggleIconChange($(this))
		});

		$(".toggle-arrow").click(function () {
			$(this).closest("tr").next(".toggle-div").toggle();
			toggleIconChange($(this))
		});

		$(".current-condition-sailing-detail").find("#SailingDetails").change(function(){
			sailingDetailUIrender();
		})
	}
};

function refreshCurrentConditions1(originCode, destinationCode) {
	var selectedSailingVal = $(".currentconditions-details-select").val();
	var url = "/current-conditions/" + originCode + "-" + destinationCode+"?selectedSailing="+selectedSailingVal;
	$.when(ACC.services.getCurrentConditionsAjax(url)).then(
		function (data) {
			$("#ccDetails").replaceWith(data.htmlContent);
			console.log("current conditions refreshed at " + new Date());
			sailingDetailUIrenderTemp();

			ACC.currentconditions.refreshAtaGlanceData();
			ACC.currentconditions.mapViewTabClick();
			ACC.currentconditions.bindActivateTabs();

			ACC.currentconditions.bindFerryTrackings();

			ACC.currentconditions.accordionUI();
			ACC.currentconditions.bindCollapsibles();
			$( "#tabs" ).tabs();
			activateTabs();
		}
	);
}

function toggleIconChange(ele){
	if(ele.find(".fa-angle-down").length >0){
		ele.find(".fa-angle-down").addClass("fa-angle-up");
		ele.find(".fa-angle-down").removeClass("fa-angle-down");
	}else{
		ele.find(".fa-angle-up").addClass("fa-angle-down");
		ele.find(".fa-angle-up").removeClass("fa-angle-up");
	}
}

function sailingDetailUIrenderTemp(){
	var ele = $(".current-condition-sailing-detail").find("#SailingDetails");
	var selOpt = ele.val();
	ele.siblings(".view-deck-wrapper").find(".view-deck-full-box").addClass("d-none");
	ele.siblings(".view-deck-wrapper").find(".box-"+selOpt).removeClass("d-none");
}
