ACC.travellerdetails = {

	_autoloadTracc : [
		"bindTravellerdetailsValidationMessages",
		 "getAmendRegions",
		"bindChangeReturningCustomerSelect",
		"bindFrequentFlyerMembershipNumber",
		"bindPayingForBookingCheckBoxes",
        "bindingTravellerInformationForm",
        "bindOnSelectPassengerType",
        "bindEmailValidation",
        "getRegions",
        "bindAcceptableForms",
        "populateRegionsIfCountryAlreadyPresent",
        "bindVehiclePlateNumberChecks"
	],

     bindAcceptableForms: function () {
    	  $('#acceptableFormsLink').on('click', function(e) {
                  event.preventDefault();
                    var url = $(this).attr('href');
                    $.when(ACC.services.contentPopupDataAjax(url)).then(
                	function(data) {
                        $('#acceptableFormsPage').replaceWith(data);
                        $('#acceptableFormsModal').modal();
                    }
                  );
            });},
	
	bindTravellerdetailsValidationMessages : function () {
		ACC.TravellerdetailsValidationMessages = new validationMessages("ferry-TravellerdetailsValidationMessages");
				ACC.TravellerdetailsValidationMessages.getMessages("error");
	},
	currentForm: '.y_passengerInformationForm',

	getAmendRegions : function() {
		var html = "";
		var country = $("#y_amendLicensePlateContry").val();
		var province = $("#y_amendLicensePlateprovince").val();
		if(country)
		{
			$.when(ACC.services.getRegionsAjax(country)).then(function(response) {
				if(response) {
					html = "<option class=\"y_default_please_select\" selected=\"selected\" value=\"-1\" disabled=\"disabled\">" + $(".y_default_please_select").html() + "</option>";
					var i;
					for (i = 0; i < response.length; i++) { 
			            html += "<option value=" + response[i].isocodeShort  + ">" +response[i].name + "</option>"
			        }
					$("select.y_vehicleLicenseProvince").html( html );
					$("select.y_vehicleLicenseProvince").val(province);
				}
			});
		}
	},

	getRegions : function() {
		$('.y_vehicleLicenseCountry').on('change',function(e){
			var html = "";
			if($(this).val() == "US"){
				$(this).parent().parent().find('#showRegionProvince').addClass("hidden");
				$(this).parent().parent().find('#showRegionState').removeClass("hidden");
			}else{
				$(this).parent().parent().find('#showRegionState').addClass("hidden");
				$(this).parent().parent().find('#showRegionProvince').removeClass("hidden");
			}
			$(this).parent().parent().find('#showRegion').removeClass("hidden");
			$.when(ACC.services.getRegionsAjax($(this).val())).then(function(response) {
				if(response) {
					html = "<option class=\"y_default_please_select\" selected=\"selected\" value=\"-1\" disabled=\"disabled\">" + $(".y_default_please_select").html() + "</option>";
					var i;
					for (i = 0; i < response.length; i++) { 
			            html += "<option value=" + response[i].isocodeShort  + ">" +response[i].name + "</option>"
			        }
					$(e.target).closest('div').parent().find("select.y_vehicleLicenseProvince").html( html );
				}
			});
		});
	},
	
	populateRegionsIfCountryAlreadyPresent : function() {
		var country = $(".y_vehicleLicenseCountry");
		if (country != undefined && country.val() != '-1') {
			var html = "";
			if (country.val() == "US") {
				$(country).parent().parent().find('#showRegionProvince').addClass("hidden");
				$(country).parent().parent().find('#showRegionState').removeClass("hidden");
			} else {
				$(country).parent().parent().find('#showRegionState').addClass("hidden");
				$(country).parent().parent().find('#showRegionProvince').removeClass("hidden");
			}
			$(country).parent().parent().find('#showRegion').removeClass("hidden");
			$.when(ACC.services.getRegionsAjax($(country).val())).then(
					function(response) {
						if (response) {
							html = "<option class=\"y_default_please_select\" selected=\"selected\" value=\"-1\" disabled=\"disabled\">" + $(".y_default_please_select").html() + "</option>";
							var i;
							for (i = 0; i < response.length; i++) { 
					            html += "<option value=" + response[i].isocodeShort  + ">" +response[i].name + "</option>"
					        }
							$("select[y_vehicleLicenseProvince]").html(html);
						}
					});
		}
	},

	bindFrequentFlyerMembershipNumber : function() {
/*		$("#y_travellerForms").find('.y_passengerLastname').val(ACC.addons.bcfstorefrontaddon['error.formvalidation.943']);
*/
		$('.y_travellerDetailsMembershipYesBtn').each( function(index) {
			$(this).on('click', function() {
				$(this).closest('.row').find('.y_membershipNumber').show("fast");
				$(this).closest('.row').find('.y_passengerFrequentFlyerMembershipNumber').prop('disabled', false);
			});
		});

		$('.y_travellerDetailsMembershipNoBtn').each( function(index) {
			$(this).on('click', function() {
				$(this).closest('.row').find('.y_membershipNumber').hide("fast");
				$(this).closest('.row').find('.y_passengerFrequentFlyerMembershipNumber').prop('disabled', true);
			});
		});

	},
	bindOnSelectPassengerType : function() {
		$('.y_passengerType' ).change(function() {
			var passengerTypeCode = $(this).val();
			$.when(ACC.services.refreshTitles(passengerTypeCode)).then(
					function( data, textStatus, jqXHR ) {
						$('#y_passengerTitleSelect').html(data.htmlContent);
					},
					// error
					function( data, textStatus, jqXHR ) {
					  	// handle the error
					}


			);
		});
	},


	bindChangeReturningCustomerSelect : function() {
		$( '.y_existingCustomer' ).change(function() {
			var userId = $(this).val();
			var $form = $(this).closest(ACC.travellerdetails.currentForm);

			if(userId){
				$.when( ACC.services.getTravellerAjax(userId) ).then(
					// success
					function( data, textStatus, jqXHR ) {
						ACC.travellerdetails.populateFormFields($form, data.travellerInfo, userId);
						ACC.travellerdetails.updatePassengerSelect($form);
					},
					// error
					function( data, textStatus, jqXHR ) {
					  	// handle the error
					}
				);
			}
			else
			{
				ACC.travellerdetails.updatePassengerSelect($form);
				ACC.travellerdetails.resetFormFields($form);
			}
		});
	},

	bindPayingForBookingCheckBoxes : function() {
		$("input:radio[class=y_bookerIsTravelling]").click(function() {
			// first Adult form
			var $form = $("#passenger-info-0");

			if($(this).val() == 'true'){

				$form.find('.y_existingCustomer').prop("disabled", true);
				$form.find('.y_existingCustomer').val("");

				$.when( ACC.services.getCurrentUserDetailsAjax() ).then(
					// success
					function( data, textStatus, jqXHR ) {

						ACC.travellerdetails.resetFormFields($form);
						if(data.isAuthenticated){
							ACC.travellerdetails.populateFormFields($form, data.travellerInfo);
						}
					},
					// error
					function( data, textStatus, jqXHR ) {
						// no details found so reset the form so new details can be entered
						ACC.travellerdetails.resetFormFields($form);
					}
				);
			}
			else
			{
				$form.find('.y_existingCustomer').prop("disabled", false);
			}
			ACC.travellerdetails.updatePassengerSelect($form);
		});
	},

	populateFormFields : function($form, travellerInfo, userId) {
		if(!travellerInfo.title || $form.find('.y_passengerTitle option[value=' + travellerInfo.title + ']').length === 0 ){
			$form.find('.y_passengerTitle option:first').attr('selected','selected');
		}else{
			$form.find('.y_passengerTitle').val(travellerInfo.title);
		}

		$form.find('.y_passengerFirstname').val($.parseHTML(travellerInfo.firstName)[0].textContent);
		$form.find('.y_passengerLastname').val($.parseHTML(travellerInfo.surname)[0].textContent);
		$form.find('.y_passengerReasonForTravel').val(travellerInfo.reasonForTravel);
		// populate selectedSavedTravellerUId
		$form.find('.y_passengerSelectedSavedTravellerUId').val($.parseHTML(travellerInfo.uid)[0].textContent);
		if(travellerInfo.email){
		    $form.find('.y_passengerEmail').val($.parseHTML(travellerInfo.email)[0].textContent);
		}

		// populate Gender checkbox
		if(travellerInfo.gender){
			$form.find('input:radio[class=y_passengerGender][value=' + travellerInfo.gender + ']').prop('checked', true);
		}
		else
		{
			$form.find('input:radio[class=y_passengerGender][value=male]').prop('checked', true);
		}

		// populate Frequent Flyer checkbox
		if(travellerInfo.membershipNumber){
			$form.find('.y_travellerDetailsMembershipYesBtn').click();
			$form.find('.y_passengerFrequentFlyerMembershipNumber').val(travellerInfo.membershipNumber);
		}
		else
		{
			$form.find('.y_travellerDetailsMembershipNoBtn').click();
		}

		// populate Special Assistance check box
		if(travellerInfo.specialRequestDetail && travellerInfo.specialRequestDetail.specialServiceRequests.length !== 0){
			$form.find('input:checkbox[class=y_specialassistance]').prop('checked', true);
		}
		else
		{
			$form.find('input:checkbox[class=y_specialassistance]').prop('checked', false);
		}

		$form.siblings()
		.find(".y_existingCustomer option[value='" + userId + "']")
		.attr("disabled","true");
	},

	resetFormFields : function($form) {
		$form.find('.y_passengerTitle').val("");
		$form.find('.y_passengerFirstname').val("");
		$form.find('.y_passengerLastname').val(ACC.TravellerdetailsValidationMessages.message('error.formvalidation.lastName'));
		$form.find('.y_passengerReasonForTravel').val("");
		$form.find('.y_passengerFrequentFlyerMembershipNumber').val("");
		$form.find('.y_passengerFrequentFlyerMembershipNumber').prop('disabled', true);
		$form.find('input:radio[class=y_travellerDetailsMembershipNoBtn]').prop('checked', true);
		$form.find('.y_membershipNumber').hide("fast");
		$form.find('input:radio[class=y_passengerGender][value=male]').prop('checked', true);
		$form.find('input:checkbox[class=y_savedetails]').prop('checked', false);
		$form.find('input:checkbox[class=y_specialassistance]').prop('checked', false);
	},

	updatePassengerSelect: function($form){

		var selected = [];

		$('.y_existingCustomer option:selected').each(function(){
			selected.push($(this).val());
		});

		$form.siblings()
			.find(".y_existingCustomer option").each(function () {
			if ($.inArray($(this).val(), selected) < 0) {
				$(this).removeAttr("disabled");
			}
		});
	},
    bindingTravellerInformationForm: function() {
        var $form = $("#y_travellerForms");
        if($form.is('form')){
        $form.validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            submitHandler : function(form) {
				var jsonData = ACC.formvalidation.serverFormValidation(form, "validateTravellerDetailsForms");
                if (!jsonData.hasErrorFlag) {
                    $('#y_processingModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    if($(element).valid())
                    {
                    	form.submit();
                    }
                }
            },
            onfocusout: function(element) {
                $(element).valid();
            },
            invalidHandler: function(form, validator) {
		        var errors = validator.numberOfInvalids();
		        if (errors) {
		            validator.errorList[0].element.focus();
		        }
		    }
        });

        $(".y_passengerFirstname" ).each(function(){
            $(this).rules( "add", {
              required: true,
              maxlength:50,
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.firstName')
              }
            });
        });

        $(".y_passengerPhoneNumber" ).each(function(){
            $(this).rules( "add", {
              required: true,
              digits:true,
              maxlength:30,
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.phoneNo')
              }
            });
        });

        $(".y_vehicleLicenseNumber" ).each(function(){
            $(this).rules( "add", {
              required: function(element) {
            	var uid = $(element).attr('id').split("__")[1];
              	return !$("#vehiclePlateNumberCheck__" + uid + ":checked").length;
              },
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.vehicle_license_plate_number')
              }
            });
        });

		$(".y_vehicleLicenseCountry" ).each(function(){
            $(this).rules( "add", {
              required: function(element) {
              	var uid = $(element).attr('id').split("__")[1];
              	return !$("#vehiclePlateNumberCheck__" + uid + ":checked").length;
              },
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.vehicle_licence_country')
              }
            });
        });

        $(".y_vehicleLicenseProvince" ).each(function(){
            $(this).rules( "add", {
              required: function(element) {
              	var uid = $(element).attr('id').split("__")[1];
              	return !$("#vehiclePlateNumberCheck__" + uid + ":checked").length;
              },
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.vehicle_license_province')
              }
            });
        });

        $(".y_passengerGender" ).each(function(){
            $(this).rules( "add", {
              required: true,
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.payment.gender')
              }
            });
        });

        $(".y_passengerLastname" ).each(function(){
            $(this).rules( "add", {
              required: true,
              maxlength: 50,
              messages: {
                required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.lastName'),
                nameValidation: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.lastNameValid')
              }
            });
        });

        $(".y_passenger_email").each(function(){
            $(this).rules( "add", {
              required: true,
              maxlength: 100,
              validateEmailPattern: true,
              messages: {
            	required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.payment.email')
              }
            });
        });

    	$(".y_passenger_confirm_email").each(function(){
            $(this).rules( "add", {
        	required: true,
            equalTo: $(this).closest("div.js-emails-box").find(".y_passenger_email"),
            messages: {
              required: ACC.TravellerdetailsValidationMessages.message('error.formvalidation.payment.confirm.email')
              }
            });
        });

        }
    },
    bindEmailValidation : function(){
		$(".y_passengerEmail" ).on("keyup", function(){
            $(this).rules( "add", {
            	maxlength:255,
            	validateEmailPattern: true,
	              messages: {
	                validateEmailPattern: ACC.TravellerdetailsValidationMessages.message('error.lead.guest.email.invalid')
	              }
	        });
        });
	},
	bindVehiclePlateNumberChecks : function(removeValidation) {
		$("input[id^='vehiclePlateNumberCheck__']").change(function() {
			  var uid = $(this).attr('id').split("__")[1];
			  if ($(this).is(':checked')) {
				  $(".license-plate-informative").removeClass("hidden");
			  }
			  else
			  {
				  $(".license-plate-informative").addClass("hidden");
			  }
		});
	}
};
