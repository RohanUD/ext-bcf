(function(){
  window.validationMessages = function(formName){
    this.formName = formName;
    this.messageList = {};
    this.getMessages = function(messageCategory){
      if(ACC.services){
        ACC.services.getValidationMessages(messageCategory, this.formName).then(this.setMessageList.bind(this));
      }
    }
    this.message = function(key){
      return this.messageList[key];
    }
    this.setMessageList = function(data){
      _.merge(this.messageList,data);
    }

  }
})()
