(function(){
ACC.passengerValidationMessages = new validationMessages("ferry-passengerForm");
ACC.passengerValidationMessages.getMessages("error");

$('.passenger-info-form').on('submit',function(){
	manage_error('hide','');
	var error=[];
	var check_passeger=check_passenger_exist('');
	var check_adult=check_adult_with_infant('');
	var accessibility_need=check_accessibility_need();
	if(check_passeger){ error.push(check_passeger); }
	if(check_adult){ error.push(check_adult); }
	if(accessibility_need){ error.push(accessibility_need); }


	if($('#y_differentPassengerInReturn').prop('checked'))
	{
		var check_passeger=check_passenger_exist('return');
		var check_adult=check_adult_with_infant('return');
		if(check_passeger){ error.push(check_passeger+' Return'); }
		if(check_adult){ error.push(check_adult+' Return'); }
	}

	if(error.length>0){
		manage_error('show',error[0]);
		return false;
	}else{
		return true;
	}

});

$('.y_outboundLargeItemQtySelector .input-group-btn').on('click',function(){
	var obj=$(this).parent();
	var total=0;
	obj.closest('.carrying-large-item-box').find('input').each(function(){
		total=total+parseInt($(this).val());
	});
	if(total>9){
		var currentInput=obj.find('input');
		currentInput.val(parseInt(currentInput.val())-1);
	}
});

$("body").on('change','[type="radio"]',function(){
	if($(this).parent().parent().parent().parent().hasClass('ui-accordion-content'))
	{
		$(this).closest('.ui-accordion').find('.custom-arrow').addClass('hidden');
		$(this).closest('.ui-accordion').find('.tickicon').remove();
		$(this).closest('.ui-accordion').find('.custom-arrow').after('<span class="tickicon pull-right"></span>');
	}
});

$('body').on('click','.y_outboundLargeItemQtySelectorPlus,.y_outboundLargeItemQtySelectorMinus',function(){
	var inputObj=$(this).closest('.y_outboundLargeItemQtySelector').find('.carrying-large-items');
	var total=0;
	$('.carrying-large-items').each(function(){
		total=total+parseInt($(this).val());
	});
	if(total>9){
		var currentValue=parseInt(inputObj.val());
		inputObj.val(currentValue-1);
	}
});

function check_passenger_exist(tp)
{
	var total=0;
	var c='passenger-quantity';
	if(tp=='return'){ c='passenger-quantity-return'; }
	$('.'+c).each(function(){
		total=total+parseInt($(this).val());
	});
	if(total==0){
		return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.check_passenger_selected');
	}
}

function check_adult_with_infant(tp)
{
	var check=true;
	var r='';
	if(tp=='return'){
		r='r';
	}

	if($('.y'+r+'_infant').val()>0){
		if($('.y'+r+'_adult').val()!=0 || $('.y'+r+'_senior').val()!=0)
		{
			check=false;
		}

		if($('.y'+r+'_nrAdult').val())
		{
			if($('.y'+r+'_nrAdult').val()!=0 || $('.y'+r+'_nrStudentChild').val()!=0 || $('.y'+r+'_nrEscortAdult').val()!=0 || $('.y'+r+'_nrEscortChild').val()!=0 || $('.y'+r+'_nrDisabledChild').val()!=0 || $('.y'+r+'_nrSenior').val()!=0)
			{
				check=false;
			}
		}
		if(check)
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.check_adult');
		}
	}
	return false;

}

function check_accessibility_need()
{
	var check=[];
	$('.required-accessibility_need').each(function(){
		if($(this).parent().find('.other-accessibility-need').prop('checked') && $(this).children('input').val().trim()==''){
			check.push($(this));
		}
	});
	if(check.length){
		var accordionId=check[0].closest('.ui-accordion').attr('id');
		var label=$('#'+accordionId).closest('.bc-accordion').find('h4').text().trim();
		$("#"+accordionId).accordion( "option", "active", 0 );
		var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.accessibility_need_for')+' '+label;
		if(check[0].closest('#y_returnPassenger').length)
		{
			errorMessage=errorMessage+' in passenger return';
		}
		return errorMessage;
	}else{
		return false;
	}
}


function manage_error(t,m,obj){
	if(t=='show')
	{
		var currentObj=$(".form-error-messages");
		if(obj){ currentObj=obj; }
		currentObj.html('<span class="icon-alert"></span> <strong>Validation Error!</strong> '+m);
		currentObj.removeClass('hidden');
		$('html, body').animate({
	        scrollTop: currentObj.offset().top
	    }, 500);
	}
	if(t=='hide')
	{
		$('.form-error-messages').html('');
		$('.form-error-messages').addClass('hidden');
	}
}



// Passenger vehicles validations

$('.passengers-vehicles-form').on('submit',function(){
	manage_error('hide','');
	var error=[];
	var accessibility_need=check_accessibility_need();
	var vehicle_height_selected=check_vehicle_height_selected();
	var vehicle_length_selected=check_vehicle_length_selected();
	var height_feet_selected=check_height_feet_selected();
	var length_feet_selected=check_length_feet_selected();
	var height_as=check_height_as();

	if(accessibility_need && $('#y_packageCheckAccessibilityNeed').prop('checked')){ error.push(accessibility_need); }
	if(vehicle_height_selected){ error.push(vehicle_height_selected); }
	if(vehicle_length_selected){ error.push(vehicle_length_selected); }
	if(height_feet_selected){ error.push(height_feet_selected); }
	if(length_feet_selected){ error.push(length_feet_selected); }
	if(height_as){ error.push(height_as); }

	if(error.length)
	{
		$('#y_showLimitedSailingsModal').modal('hide');
		manage_error('show',error[0]);
		return false;
	}
	else
	{
		return true;
	}
});


//Adding accessibility items selected to list
$('body').on('change','.accesibility-needs-item',function(){
	var currentText=$(this).closest('label').text().trim();
	var currentId=$(this).attr('id');
	var displayObject=$(this).closest('.bc-accordion').find('.accesibility-selected');
	$(displayObject).removeClass('hidden');
	var html='';

	if($(this).prop('checked'))
	{
		if($(this).attr('type')=='radio')
		{
			html='<li class="radio-text">'+currentText+'</li>';
			if($(displayObject).find('.radio-text').length){
				$(displayObject).find('.radio-text').html(currentText);
			}
			else{
				$(displayObject).find('ul').append(html);
			}
		}
		else
		{
			var extraClass='';
			if($(this).hasClass('other-accessibility-need')){
				extraClass+='<span class="accesibility-needs-item-box"> : '+other_accessibility_text+'</span>';
			}

			html='<li class="'+$(this).attr('type')+' item-'+currentId+'">'+currentText+extraClass+'</li>';
			$(displayObject).find('ul').append(html);
		}
	}
	else
	{
		$('.item-'+currentId).remove();
	}

});

//Removing all accessibility items from list
$('body').on('click','.clear-accesibility-needs-item',function(){
	var displayObject=$(this).closest('.bc-accordion').find('.accesibility-selected');
	$(displayObject).find('li').remove();
});

$('body').on('change','#under20LengthPackage_0',function(){
	if($(this).prop('checked')==true){
		$('#Over20lengthDivPackage_0').addClass('d-none');
	}
});

function check_vehicle_height_selected()
{
	var checked=false;
	$('.item-vehicleHeight').each(function(){ if($(this).prop('checked')==true){ checked=true; } })
	if(checked==false)
	{
		return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_vehicle_height');
	}
	else
	{
		return false;
	}
}

function check_vehicle_length_selected()
{
	var checked=false;
	$('.item-vehicleLength').each(function(){ if($(this).prop('checked')==true){ checked=true; } })
	if(checked==false)
	{
		return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_vehicle_length');
	}
	else
	{
		return false;
	}
}

function check_height_feet_selected()
{
	if($('#over7HeightPackage_0').prop('checked')==true)
	{
		if($('#Over7heightboxinft_0').val()==0 || $('#Over7heightboxinft_0').val()=='')
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.add_total_vehicle_height');
		}
		if(Number.isInteger(parseFloat($('#Over7heightboxinft_0').val()))==false)
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.add_total_vehicle_height_without_decimal');
		}
	}
	return false;
}

function check_length_feet_selected()
{
	if($('#over20LengthPackage_0').prop('checked')==true)
	{
		if($('#Over20lengthboxinft_0').val()==0 || $('#Over20lengthboxinft_0').val()=='')
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.add_total_vehicle_length');
		}
		if(Number.isInteger(parseFloat($('#Over20lengthboxinft_0').val()))==false)
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.add_total_vehicle_length_without_decimal');
		}
	}
	return false;
}

function check_height_as()
{
	if($('#over7HeightPackage_0').prop('checked')==true)
	{
		var checked=false;
		$('.item-vehicleLength').each(function(){ if($(this).prop('checked')==true){ checked=true; } })
		if(checked==false)
		{
			return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_vehicle_length');
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

$(document).ready(function(){
	$('.accesibility-needs-item:checked:checked').each(function(){
		var currentText=$(this).closest('label').text().trim();
		var currentId=$(this).attr('id');
		var displayObject=$(this).closest('.bc-accordion').find('.accesibility-selected');
		$(displayObject).removeClass('hidden');
		var html='';
		if($(this).attr('type')=='radio')
		{
			html='<li class="radio-text">'+currentText+'</li>';
			if($(displayObject).find('.radio-text').length){
				$(displayObject).find('.radio-text').html(currentText);
			}
			else{
				$(displayObject).find('ul').append(html);
			}
		}
		else
		{
			var extraClass='';
			$("input[id^='specialServiceRequest_OTHR_']").addClass('other-accessibility-need');
			if($(this).hasClass('other-accessibility-need')){
				var other_accessibility_text=$(this).closest('.ui-accordion-content').find('#textbox_other_accessibility_need').val();
				extraClass+='<span class="accesibility-needs-item-box"> : '+other_accessibility_text+'</span>';
			}
			html='<li class="'+$(this).attr('type')+' item-'+currentId+'">'+currentText+extraClass+'</li>';
			$(displayObject).find('ul').append(html);
		}
	});
});

$('body').on('blur','.required-accessibility_need input',function(){
	var displayObject=$(this).closest('.bc-accordion').find('.accesibility-selected');
	$(displayObject).find('.accesibility-needs-item-box').html(" : "+$(this).val());
});

//Tab Saving to url
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');

    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}

$('.save-nav-tab-on-change li').on('click',function(){
	var tabId=$(this).find('a').attr('href').replace('#','');
	if($.urlParam('tab'))
	{
		var newurl = window.location.origin+window.location.pathname+replaceQueryParam('tab',tabId,window.location.search);
		window.history.replaceState({ path: newurl }, '', newurl);
	}
	else
	{
		var newurl = $(location).attr('href') + '&tab='+tabId;
		window.history.replaceState({ path: newurl }, '', newurl);
	}
});

if($.urlParam('tab')){
    $("ul.nav-tabs > li").removeClass('active');
	$('.nav-tabs a[href="#' + $.urlParam('tab') + '"]').parent().addClass('active');
	$('.tab-pane').removeClass('active');
	$('#'+$.urlParam('tab')).addClass('active in');

}


//Passenger Vehicle Selection Page Ferry Flow

$('.form-vehicle-selection').on('submit',function(e){
	$('.fe-error').remove();
	manage_error('hide','');
	var error=[];
	var check_vehicle_for_return='';
	var check_vehicle=check_vehicle_selected('0');
	if(check_vehicle){ error.push(check_vehicle); }

	if($('#farevehicleinboundcheckbox').prop('checked') && !check_vehicle){
		check_vehicle_for_return=check_vehicle_selected('1');
		if(check_vehicle_for_return){ error.push(check_vehicle_for_return); }

		//either both vehicles should exceed port restrictions or none as booking flow is different for both
		var hasExeededPortLimitsOutbound = hasExeededPortLimits('0');
		var hasExeededPortLimitsInbound = hasExeededPortLimits('1');
		if(hasExeededPortLimitsOutbound || hasExeededPortLimitsInbound){
			if(!(hasExeededPortLimitsOutbound && hasExeededPortLimitsInbound)){
				error.push(ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.invalid_combination'));
			}
		}
	}

	if(error.length)
	{
		if(check_vehicle)
		{
			manage_error('show',error[0],$('.error-message-0'));
			$('.farevehicle_1 .form-error-messages').addClass('hidden');
		}
		if(check_vehicle_for_return && !check_vehicle)
		{
			manage_error('show',error[0],$('.error-message-1'));
			$('.farevehicle_0 .form-error-messages').addClass('hidden');
		}
		else
		{
			manage_error('show',error[0],$('.error-message-0'));
			$('.farevehicle_1 .form-error-messages').addClass('hidden');
		}
		return false;
	}
	else
	{
		if(!$('.modal.in').length)
		{
			showConditionalModals(e);
		}
		return true;
	}
});

var check_vehicle_selected=function(t){
	var errorFound=[];
	var selected=false;
	var currentSelected='';
	var accordionId='';
	$('[name="vehicleInfoForm.vehicleInfo['+t+'].vehicleType.code"]').each(function(){
		if($(this).prop('checked')){
			selected=true;
			currentSelected=$(this).attr('id');
		}
	});

	if(selected==false)
	{
		accordionId=$('#accordion_'+t).find('.ui-accordion-header').attr('id');
		if(!$('#'+accordionId).hasClass('ui-state-active'))
		{
			$('#'+accordionId).click();
		}
		return ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_vehicle');
	}
	if(currentSelected=='under7Height_'+t || currentSelected=='over7Height_'+t || currentSelected=='under20Length_'+t || currentSelected=='over20Length_'+t){
        if(($('#under7Height_'+t).prop('checked') || $('#over7Height_'+t).prop('checked')) && ($('#under20Length_'+t).prop('checked') || $('#over20Length_'+t).prop('checked'))){
            if($('#over7Height_'+t).prop('checked') || $('#over20Length_'+t).prop('checked'))
            {
                var errorMessage='';
                var valHeight = Number($('#Over7heightboxinft_'+t).val());
                var valLength = Number($('#Over20lengthboxinft_'+t).val());
                if(valHeight <= 7 && $('#over7Height_'+t).prop('checked'))
                {
                    errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_under_height');
                    errorFound.push(errorMessage);
                    $('#Over7heightboxinft_'+t).closest('#vehicle_height_inputs').append('<span class="fe-error">'+errorMessage+'</span>');
                }
                if(valLength<=20 && $('#over20Length_'+t).prop('checked'))
                {
                    errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_under_length');
                    errorFound.push(errorMessage);
                    $('#Over20lengthboxinft_'+t).closest('#vehicle_length_inputs').append('<span class="fe-error">'+errorMessage+'</span>');
                }
                if(errorFound.length){
                    accordionId=$('#over7Height_'+t).closest('.ui-accordion-content').attr('aria-labelledby');
                    if(!$('#'+accordionId).hasClass('ui-state-active'))
                    {
                        $('#'+accordionId).click();
                    }
                }
            }
        }else{
                errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_height_length');
                errorFound.push(errorMessage);
                return errorMessage;
        }
    }


	if(currentSelected=='withTrailerMotorcycle_'+t && $('#Motorcyclelengthboxinft_'+t).val() < 1.0)
	{
		var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_length_of_trailer');
		accordionId=$('#Motorcyclelengthboxinft_'+t).closest('.ui-accordion-content').attr('aria-labelledby');
		if(!$('#'+accordionId).hasClass('ui-state-active'))
		{
			$('#'+accordionId).click();
		}
		$('#Motorcyclelengthboxinft_'+t).closest('.unit-converter__inputs').append('<span class="fe-error">'+ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_length')+'</span>');
		return errorMessage;
	}

	if(currentSelected=='semiTrailor_'+t || currentSelected=='semiABTrain_'+t || currentSelected=='straightTruck_'+t || currentSelected=='otherOversize_'+t)
	{

		if(currentSelected=='otherOversize_'+t)
		{
			if($('#other-input_'+t).find('textarea').val()=='')
			{
				var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.add_description');
				errorFound.push(errorMessage);
				$('#other-input_'+t).append('<span class="fe-error">'+errorMessage+'</span>');
			}
		}

		if(!$("input[id='under9Ft_"+t+"']").is(':checked') && !$("input[id='over9Ft_"+t+"']").is(':checked')){
			var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_9feet_wide_radio');
			errorFound.push(errorMessage);
			$('.vehicle-over9ft-selection-error_'+t).append('<span class="fe-error">'+errorMessage+'</span>');
		}
		if(($('#OversizeWidthboxinft_'+t).val() <= 9.0 || $('#OversizeWidthboxinft_'+t).val() > 15.0) && !$('.js-over5500kb-width-input_' + t).hasClass("hidden")){
			var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.enter_valid_width');
			errorFound.push(errorMessage);
			$('#OversizeWidthboxinft_'+t).closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
		}
		if($('#Oversizelengthboxinft_'+t).val()==0){
			var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_length');
			errorFound.push(errorMessage);
			$('#Oversizelengthboxinft_'+t).closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
		}
		if($('#Oversizeheightboxinft_'+t).val()==0){
			var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_height');
			errorFound.push(errorMessage);
			$('#Oversizeheightboxinft_'+t).closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
		}
		if(!$('.js-over5500kg-conditional-dimensions_' + t).hasClass("hidden")){
			if(parseInt($('[name="vehicleInfoForm.vehicleInfo['+t+'].numberOfAxis"]').val())==0){
				var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_axis');
				errorFound.push(errorMessage);
				$('[name="vehicleInfoForm.vehicleInfo['+t+'].numberOfAxis"]').closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
			}
			if(parseInt($('[name="vehicleInfoForm.vehicleInfo['+t+'].weight"]').val())==0){
				var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_valid_weight');
				errorFound.push(errorMessage);
				$('[name="vehicleInfoForm.vehicleInfo['+t+'].weight"]').closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
			}
			if(parseInt($('[name="vehicleInfoForm.vehicleInfo['+t+'].groundClearance"]').val())==0){
				var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.enter_valid_ground_clearance');
				errorFound.push(errorMessage);
				$('[name="vehicleInfoForm.vehicleInfo['+t+'].groundClearance"]').closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
			}
			if(!$("input[id='adjustable_yes_"+t+"']").is(':checked') && !$("input[id='adjustable_no_"+t+"']").is(':checked')){
				var errorMessage=ACC.passengerValidationMessages.message('error.ferry.passengerForm.passengers.select_is_adjustable');
				errorFound.push(errorMessage);
				$('[name="vehicleInfoForm.vehicleInfo['+t+'].adjustable"]').closest('.unit-converter').append('<span class="fe-error">'+errorMessage+'</span>');
			}
		}
		accordionId=$('#Oversizeheightboxinft_'+t).closest('.ui-accordion-content').attr('aria-labelledby');
		if(!$('#'+accordionId).hasClass('ui-state-active'))
		{
			$('#'+accordionId).click();
		}
	}
	if(errorFound.length){ return errorFound[0]; }
}

function hasExeededPortLimits(t) {
	var selected=false;
	var currentSelected='';
	$('[name="vehicleInfoForm.vehicleInfo['+t+'].vehicleType.code"]').each(function(){
		if($(this).prop('checked')){
			selected=true;
			currentSelected=$(this).attr('id');
		}
	});
	if(selected && (currentSelected=='semiTrailor_'+t || currentSelected=='semiABTrain_'+t || currentSelected=='straightTruck_'+t))
	{
		var height = Number($("#Oversizeheightboxinft_"+t).val());
		var length = Number($("#Oversizelengthboxinft_"+t).val());
		var width = 0;
		if(Number($('#OversizeWidthboxinft_'+t).val()) > 9.0){
			width = Number($("#OversizeWidthboxinft_"+t).val());
		}
		var lengthLimit =  Number($("input[id='over5500VehicleLengthPortLimit']").val());
		var heightLimit =  Number($("input[id='over5500VehicleHeightPortLimit']").val());
		if("true" === $("input[id='isFullAccountUser']").val() && (length > lengthLimit || height > heightLimit || width > 9.0))
		{
			return true;
		}
	}
	if(selected && (currentSelected=='over7Height_'+t || currentSelected=='over20Length_'+t))
	{
		var height = Number($("#Over7heightboxinft_" + t).val());
		var length = Number($("#Over20lengthboxinft_" + t).val());
		var lengthLimit =  Number($("input[id='standardVehicleLengthPortLimit']").val());
		var heightLimit =  Number($("input[id='standardVehicleHeightPortLimit']").val());
		if("true" === $("input[id='isFullAccountUser']").val() && (length > lengthLimit || height > heightLimit))
		{
			return true;
		}
	}
	return false;
}

//Custom Accordion
$('.custom-accordion a').on('click',function(){
	var state='up';
	if($(this).find('i').hasClass('fa-chevron-up')){ state='down'; }
	if(state=='down')
	{
		$(this).find('i').removeClass('fa-chevron-up');
		$(this).find('i').addClass('fa-chevron-down');
		$(this).closest('.p-box').find('.sailing-select').slideUp();
	}
	else
	{
		$(this).find('i').removeClass('fa-chevron-down');
		$(this).find('i').addClass('fa-chevron-up');
		$(this).closest('.p-box').find('.sailing-select').slideDown();
	}
});


$('body').on('click','.show_hide_content',function(){
    var displayBox=$(this).attr('data-display');
    if($(this).find('i').hasClass('fa-angle-down'))
    {
        $(this).find('i').removeClass('fa-angle-down');
        $(this).find('i').addClass('fa-angle-up');
        $(this).find(".more-details").removeClass("hidden");
        $(this).find(".less-details").addClass("hidden");

        $('.'+displayBox).slideUp();

    }
    else
    {
        $(this).find('i').removeClass('fa-angle-up');
        $(this).find('i').addClass('fa-angle-down');
        $(this).find(".more-details").addClass("hidden");
        $(this).find(".less-details").removeClass("hidden");
        $('.'+displayBox).slideDown();

    }
});


})();

function isPaxSelected(formElement,paxType){
    if(formElement.find('[id^=y_][id$='+paxType+']').length > 0){
       return (formElement.find('[id^=y_][id$='+paxType+']').val()>0)
    }
    return false;
}


function showConditionalModals (e) {
	
    if($("input[id^='over7Height_']").is(':checked'))
      {
      $("input[id^='over7Height_']").each(function(){
          if($(this).is(':checked'))
          {
        	var index = $(this).attr('id').split("_")[1];
            var height = Number($("#Over7heightboxinft_"+index).val());
    		var heightPortLimit =  Number($("input[id='standardVehicleHeightPortLimit']").val());
    		if("true" === $("input[id='isFullAccountUser']").val() && height > heightPortLimit)
  			{
				if("true" === $("input[id='hasNonOptionSailingInCart']").val())
					{
      				e.preventDefault();
      				$('#invalid-sailing-combination-modal').modal('show');
					}
				else
					{
      				e.preventDefault();
      				$('#ferry-option-booking-info-modal').modal('show');
					}
  			}
    		else if(height > heightPortLimit)
  			{
  				e.preventDefault();
  				$('#non-bookabale-veh-error-modal').modal('show');
  			}
          }

    });
      }

    if($("input[id^='over20Length_']").is(':checked'))
    {
      $("input[id^='over20Length_']").each(function(){
        if($(this).is(':checked'))
          {
        	var index = $(this).attr('id').split("_")[1];
            var length = Number($("#Over20lengthboxinft_"+index).val());
            var lengthPortLimit =  Number($("input[id='standardVehicleLengthPortLimit']").val());
			if("true" === $("input[id='isFullAccountUser']").val() && length > lengthPortLimit)
			{
				if("true" === $("input[id='hasNonOptionSailingInCart']").val())
					{
        				e.preventDefault();
        				$('#invalid-sailing-combination-modal').modal('show');
					}
				else
					{
        				e.preventDefault();
        				$('#ferry-option-booking-info-modal').modal('show');
					}
			}
			else if(length > lengthPortLimit)
			{
				e.preventDefault();
				$('#non-bookabale-veh-error-modal').modal('show');
			}
          }
      });
    }

    	if($("input[id^='semiTrailor_']").is(':checked'))
		{
			$("input[id^='semiTrailor_']").each(function(){
				if($(this).is(':checked'))
    			{
					var index = $(this).attr('id').split("_")[1];
	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
	    			var width = 0.0;
	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
	    				width = Number($("#OversizeWidthboxinft_"+index).val());
	    			}
	    			var lengthPortLimit =  Number($("input[id='over5500VehicleLengthPortLimit']").val());
	    			var heightPortLimit =  Number($("input[id='over5500VehicleHeightPortLimit']").val());
	    			if("true" === $("input[id='isFullAccountUser']").val() && (length > lengthPortLimit || height > heightPortLimit || ($("#over9Ft_"+index).is(':checked') && width > 9.0)))
        			{
	    				if("true" === $("input[id='hasNonOptionSailingInCart']").val())
	    					{
                				e.preventDefault();
                				$('#invalid-sailing-combination-modal').modal('show');
	    					}
	    				else
	    					{
                				e.preventDefault();
                				$('#ferry-option-booking-info-modal').modal('show');
	    					}
        			}
	    			else if(length > lengthPortLimit || height > heightPortLimit)
        			{
        				e.preventDefault();
        				$('#non-bookabale-veh-error-modal').modal('show');
        			}
    			}
			});
		}

		if($("input[id^='semiABTrain_']").is(':checked'))
		{
			$("input[id^='semiABTrain_']").each(function(){
				if($(this).is(':checked'))
    			{
					var index = $(this).attr('id').split("_")[1];
	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
	    			var width = 0.0;
	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
	    				width = Number($("#OversizeWidthboxinft_"+index).val());
	    			}
	    			var lengthPortLimit =  Number($("input[id='over5500VehicleLengthPortLimit']").val());
	    			var heightPortLimit =  Number($("input[id='over5500VehicleHeightPortLimit']").val());
	    			if("true" === $("input[id='isFullAccountUser']").val() && (length > lengthPortLimit || height > heightPortLimit || ($("#over9Ft_"+index).is(':checked') && width > 9.0)))
        			{
	    				if("true" === $("input[id='hasNonOptionSailingInCart']").val())
	    					{
                				e.preventDefault();
                				$('#invalid-sailing-combination-modal').modal('show');
	    					}
	    				else
	    					{
                				e.preventDefault();
                				$('#ferry-option-booking-info-modal').modal('show');
	    					}
        			}
	    			else if(length > lengthPortLimit || height > heightPortLimit)
        			{
        				e.preventDefault();
        				$('#non-bookabale-veh-error-modal').modal('show');
        			}
    			}
			});
		}

		if($("input[id^='straightTruck_']").is(':checked'))
		{
			$("input[id^='straightTruck_']").each(function(){
				if($(this).is(':checked'))
    			{
					var index = $(this).attr('id').split("_")[1];
	    			var height = Number($("#Oversizeheightboxinft_"+index).val());
	    			var length = Number($("#Oversizelengthboxinft_"+index).val());
	    			var width = 0.0;
	    			if(Number($('#OversizeWidthboxinft_'+index).val()) > 9.0){
	    				width = Number($("#OversizeWidthboxinft_"+index).val());
	    			}
	    			var lengthPortLimit =  Number($("input[id='over5500VehicleLengthPortLimit']").val());
	    			var heightPortLimit =  Number($("input[id='over5500VehicleHeightPortLimit']").val());
	    			if("true" === $("input[id='isFullAccountUser']").val() && (length > lengthPortLimit || height > heightPortLimit || ($("#over9Ft_"+index).is(':checked') && width > 9.0)))
        			{
	    				if("true" === $("input[id='hasNonOptionSailingInCart']").val())
	    					{
                				e.preventDefault();
                				$('#invalid-sailing-combination-modal').modal('show');
	    					}
	    				else
	    					{
                				e.preventDefault();
                				$('#ferry-option-booking-info-modal').modal('show');
	    					}
        			}
	    			else if(length > lengthPortLimit || height > heightPortLimit)
        			{
        				e.preventDefault();
        				$('#non-bookabale-veh-error-modal').modal('show');
        			}
    			}
			});
		}
}