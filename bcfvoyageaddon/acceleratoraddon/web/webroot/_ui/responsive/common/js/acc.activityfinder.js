ACC.activityfinder = {
    _autoloadTracc: [
       "getAllTravelSectors",
        "init",
        "bindSortSelect",
        "bindFacetCheckboxes"

    ],

    componentActivityParentSelector: '#y_activityFinderForm',


    init: function () {

        ACC.activityfinder.bindActivityFinderLocationEvents();
        ACC.activityfinder.bindActivityTravelSectorsOriginAutoSuggest();
        ACC.activityfinder.showChildDropDown();
         ACC.activityfinder.selectActivityType();
         ACC.activityfinder.bindFareFinderValidation();
    },

 bindFareFinderValidation: function () {

        $(ACC.activityfinder.componentActivityParentSelector).validate({
         errorElement: "span",
                    errorClass: "fe-error",
                    ignore: ".fe-dont-validate",
                    submitHandler: function (form) {
                    	$.when(ACC.services.checkAmendmentCartInSession()).then(
            					function(data) {
            						if(data.result == true){
            							$('#y_confirmFreshBookingModal').modal('show');
            						}
            						else {
            			                    form.submit();
            						}
            					}
            				);
                    },
                    onfocusout: function (element) {
                        $(element).valid();
                    },
                    rules: {
                    date:{
                    required: {
                              	           			 depends:function(){
                              	        				 $(this).val($.trim($(this).val()));
                              	        		            return true;
                              	        			 }
                              	        		}
                    }
                    },
                     messages: {
                                    destination: {
                                        required: ACC.addons.bcfstorefrontaddon['error.farefinder.from.location']
                                    }
                                }
         });
         },

     getAllTravelSectors: function() {
        	$(document).on("keyup", ".y_activityFinderDestinationLocation", function(e) {
    			// ignore the Enter key
    			if ((e.keyCode || e.which) == 13) {
    				return false;
    			}

    			var locationText = $.trim($(this).val());
    			ACC.activityfinder.fireQueryToFetchAllTravelSectors(locationText);
    		});
        },

         fireQueryToFetchAllTravelSectors: function(locationText){
            	if(locationText.length >= 3) {
        			if($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion"))
        				{
        			$.when(ACC.services.getTravelSectorsAjax()).then(function(data) {
        	    		ACC.farefinder.allTravelSectors=data.travelRoute;
        	    		ACC.farefinder.terminalcacheversion=data.terminalsCacheVersion;
        	    		localStorage.setItem("travelSector", JSON.stringify(ACC.farefinder.allTravelSectors));
        	    		localStorage.setItem("terminalCacheVersion", ACC.farefinder.terminalcacheversion);
        	    	});
        		}
        			else{
        				ACC.farefinder.allTravelSectors=JSON.parse(localStorage.getItem("travelSector"));
        			}
        		}
            },

    bindActivityFinderLocationEvents: function () {
        // validation events bind for y_ActivityFinderOriginLocation field
		$(".y_activityFinderDestinationLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_activityFinderDestinationLocationCode").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				// select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a:eq( 1 )");
					firstSuggestion.click();
				}
				$(this).valid();

			};
		});

		$(".y_activityFinderDestinationLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
            	$(".y_activityFinderDestinationLocation").valid();
            }
        });

		$(".y_activityFinderDestinationLocation").blur(function (e) {
			$(".y_activityFinderDestinationLocation").valid();
		});

		$(".y_activityFinderDestinationLocation").focus(function (e) {
			$(this).select();
		});
},
	 showChildDropDown: function () {
$('#y_CHILD').change(function(){
 $("#childAgeDropdown").empty();
    var childQty = Number($(this).val());
    var childIndex = $('#childIndex').val();
    var childAgeOption="" ;
    var childAgeSelect="";

     for (var j=0;j<=18;j++)
        {
            var option = '<option value="'+j+'"> '+j+' </option>';
            childAgeOption = childAgeOption + option;
        }

    for (var i = 0; i < childQty; i++)
            {
            var j=i+1;
            var label = '<div class="col-xs-6 col-sm-6"><label for="y_childSelect'+ j +'"> CHILD-'+ j +' AGE </label>'
            var formSelect = '<select class="form-control" id="y_childSelect'
            +j +
            '" name="guestData['
            +childIndex+
            '].age['
            +i+
            ']" >'
            + childAgeOption+
            '</select></div>';
            var data = label + formSelect;
             $("#childAgeDropdown").append(
                            data)
            }
    });
},

selectActivityType: function(){
        $('#y_activityTypeSelect').change(function() {
        	 $('#y_activitySelect').val('ANY');
        	var activityType = $('#y_activityTypeSelect').val();
        	 $('#y_activitySelect option').removeClass('hidden');
        	   $('#y_activitySelect option').each(function(){
                              if(!$(this).hasClass(activityType) && !$(this).hasClass('ANY') && activityType != 'ANY' ){
                                $(this).addClass('hidden');
                              }
                            })
        	});
       },

    // Get All Travel Sectors
        bindActivityTravelSectorsOriginAutoSuggest: function () {
            $(".y_activityFinderDestinationLocation").autosuggestion({
                autosuggestServiceHandler: function (locationText) {
                    var suggestionSelect = "#y_activityFinderDestinationLocationSuggestions";
                    var travelSectors=ACC.farefinder.allTravelSectors;
            		var filterTravelSectors=ACC.farefinder.getTravelOriginMatches(travelSectors,locationText);
                	if(!$.isEmptyObject(filterTravelSectors)) {
                    	var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> <li> <a href="" class="autocomplete-suggestion" data-code="any" data-suggestiontype="AIRPORTGROUP">ANY</a> </li>';
                    	var htmlStringEnd = '</ul> </div>';
                    	var htmlStringBody = "";
                    	var titleDisplayed = 0;
                    	var htmlStringTitle = "";
                    	$(suggestionSelect).html(htmlStringStart);

                    	for(var code in filterTravelSectors) {
                    		var filteredJson=filterTravelSectors[code];
                    		if(titleDisplayed == 0){
                    			htmlStringTitle = '</li>'+'<li class="child"> <a href="" class="autocomplete-suggestion" '+
                        		'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + '</a> </li> ';
                    			titleDisplayed = 1;
                    		}
                    		else{
                    			htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" '+
                    		'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + '</a> </li> ';
                    		}
                    	}
                    	var htmlContent = htmlStringStart + htmlStringTitle +htmlStringBody + htmlStringEnd;
                    	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
                	}
                	else{
                		var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li> <a href="" class="autocomplete-suggestion" data-code="any" data-suggestiontype="AIRPORTGROUP">ANY</a> </li><li>No matching terminals</li></ul></div>';
                    	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
                	}
                },
                suggestionFieldChangedCallback: function () {
                    // clear the destination fields
                },
                attributes : ["Code", "SuggestionType"]
            });
        },

        bindSortSelect: function () {
                $(document).on("change", "#y_activitySearchSortSelect", function () {
                    $("#y_activitySearchSortForm").submit();
                });
            },

         bindFacetCheckboxes: function () {
                    $(document).on("change", ".y_activityFacetCheckbox", function () {
                        $("#y_activitySearchFacetForm").find(":input[name=q]").val($(this).val());
                        $("#y_activitySearchFacetForm").submit();
                    });
                }
};
