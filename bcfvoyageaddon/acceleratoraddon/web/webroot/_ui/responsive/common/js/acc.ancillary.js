ACC.ancillary = {

	config : {
		firstSeatLabelDefault : 1
	},

	seatChartMain : {}, // the current seat map that is shown on the UI (this is
	// changed according to the seatMapIndexCurrent)
	seatMapJsonObj : {},
	seatMapIndexCurrent : 0, // each trip sector (e.g. Luton to Edinburgh)
	// has it's own seat map

	_autoloadTracc : [
			[ "bindSelectSeatButtonClick",
					$(".y_ancillarySeatSection").length != 0 ],
			[ "updateSelectedOptionsInit", $(".y_ancillarySection").length != 0 ],
			[ "bindPassengerRadioChange", $(".y_ancillarySection").length != 0 ],
			[ "bindTabChange", $(".y_ancillarySection").length != 0 ],
			[ "bindAddAnotherItemButton", $(".y_ancillarySection").length != 0 ],
			[ "bindRemoveItemButton", $(".y_ancillarySection").length != 0 ],
			[ "bindQuantityFieldChange", $(".y_ancillarySection").length != 0 ],
			[ "bindCheckboxChange", $(".y_ancillarySection").length != 0 ],
			[ "bindOfferItemChange", $(".y_ancillarySection").length != 0 ],
			[ "bindReservationContinueButton",
					$(".y_ancillarySection").length != 0 ],
			"bindSelectUpgradeBundleOptions",
			"bindAssistanceDeclarationUpdate",
			"bindAccessibilityDeclarationAgentComment",
			"bindExtraProductQuantitySelector" ],

	initSeatMapJSON : function(url) {
		this.seatMapJsonObj = ACC.ancillary.getSeatMap(url);
		if (this.seatMapJsonObj) {
			var itineraryJsonStr = $("#y_itineraryContainer").html();
			ACC.appmodel.itineraryJsonObj = JSON.parse(JSON.stringify(eval("("
					+ itineraryJsonStr + ")")));

			// load that itinerary's seat map from JSON
			var noOfSeatMaps = this.seatMapJsonObj.seatMap.length, i;
			for (i = (noOfSeatMaps - 1); i >= 0; i--) {
				// set the current itinerary number
				this.seatMapIndexCurrent = i;
				this.loadJsonSeatMap();
			}
		}
	},

	updateSelectedOptionsInit : function() {
		// Update all the select dropdown to disable already selected
		// options
		$(".y_ancillaryListElement").each(function(index, input) {
			ACC.ancillary.updateSelectOptions($(input));
		});
	},

	bindSelectSeatButtonClick : function() {
		$('#y_selectSeat')
				.on(
						"click",
						function() {
							if (!$('#y_ancillaryPanelSeatingOptions').is(
									":visible")) {
								var url = ACC.config.contextPath
										+ "/ancillary/accommodation-map";
								var dataAmend = $(this).attr("data-amend");
								if (dataAmend && dataAmend === "true") {
									url = ACC.config.contextPath
											+ "/manage-booking/ancillary/accommodation-map";
								}
								ACC.ancillary.initSeatMapJSON(url);
								+$("#y_selectSeatSpan").hide();
								+$("#y_hideSeatSpan").show();
								+$("body").animate(
										{
											scrollTop : $('#y_hideSeatSpan')
													.offset().top
										}, 300, 'swing');
							} else {
								+$("#y_selectSeatSpan").show();
								+$("#y_hideSeatSpan").hide();

							}
						});
	},

	// load the current itinerary's seat map from JSON
	loadJsonSeatMap : function() {
		var seatChartMapStr = "";
		var seatFeatureStr = "";
		var blankRowStr = "";
		var cabinIndicator = "";
		var seatChartLegendTemp = [];
		var seatChartMap = [];
		var seatChartYAxisLabels = [];
		var seatChartAttrStr = "";
		var seatChartAttributes = {};

		var seatChartXAxisLabels = [];
		var seatChartLegend = [];

		function createSeatChartLegendWithPrice(isUpperDeck, legendPerCabin,
				cabinIndicator, price, className) {
			className += isUpperDeck ? " - Upper" : " - Lower";
			if (price != null) {
				className += " ( From " + price + ")";
			}
			legendPerCabin.push(cabinIndicator);
			legendPerCabin.push("available");
			legendPerCabin.push(className);
		}

		function appendToSeatChartAttribute(isUpperDeck, cabinIndicator,
				cssClass, category, price) {
			if (isUpperDeck) {
				cssClass += "-upper-deck-class";
			} else {
				cssClass += "-lower-deck-class";
			}
			if (!seatChartAttrStr) {
				seatChartAttrStr += " " + cabinIndicator + ": { price : "
						+ price + " , " + "classes : '" + cssClass + "' , "
						+ "category : '" + category + "'} ";
			} else {
				seatChartAttrStr += ", " + cabinIndicator + ": { price : "
						+ price + " , " + "classes : '" + cssClass + "' , "
						+ "category : '" + category + "'} ";
			}
		}

		// add standard items to the legend
		seatChartLegendTemp.push([ 'u', 'unavailable', 'Already Booked' ]);
		seatChartLegendTemp.push([ 's', 'unavailable-curr',
				'Currently Selected' ]);
		seatChartLegendTemp.push([ 'w', 'seatCharts-toilet', 'Toilet' ]);
		seatChartLegendTemp.push([ 'n', 'not-available', 'Not available' ]);
		var chartXAxisLabelStr = "";
		for ( var cabinSizeCtr in this.seatMapJsonObj.seatMap[this.seatMapIndexCurrent].seatMapDetail.cabin) {
			var cabin = this.seatMapJsonObj.seatMap[this.seatMapIndexCurrent].seatMapDetail.cabin[cabinSizeCtr], columnNumCurr = -1, legendPerCabin = [], isUpperDeck = cabin.upperDeckIndicator, price = cabin.priceData ? cabin.priceData.formattedValue
					: null, cssClass = "";
			chartXAxisLabelStr = cabin.columnHeaders;
			if (cabin.cabinName === "Economy Plus") {
				cabinIndicator = "b";
				cssClass = "economy-plus";
				appendToSeatChartAttribute(isUpperDeck, cabinIndicator,
						cssClass, "Economy Plus Class", 100);
				createSeatChartLegendWithPrice(isUpperDeck, legendPerCabin,
						cabinIndicator, price, "Economy Plus");
			} else if (cabin.cabinName === "Economy") {
				cabinIndicator = "a";
				cssClass = "economy";
				appendToSeatChartAttribute(isUpperDeck, cabinIndicator,
						cssClass, "Economy", 50);
				createSeatChartLegendWithPrice(isUpperDeck, legendPerCabin,
						cabinIndicator, price, "Economy");
			} else if (cabin.cabinName === "Business") {
				cabinIndicator = "c";
				cssClass = "business";
				appendToSeatChartAttribute(isUpperDeck, cabinIndicator,
						cssClass, "Business", 200);
				createSeatChartLegendWithPrice(isUpperDeck, legendPerCabin,
						cabinIndicator, price, "Business");
			}
			seatChartLegendTemp.push(legendPerCabin);
			if (cabinSizeCtr == (this.seatMapJsonObj.seatMap[this.seatMapIndexCurrent].seatMapDetail.cabin.length - 1)) {
				this.seatChartLegend = seatChartLegendTemp;
			}
			featureFound = false;
			featureFront = false;
			featureBack = false;
			for ( var i in cabin.rowInfo) {
				for ( var j in cabin.rowInfo[i].seatInfo) {
					if (columnNumCurr === -1
							|| (cabin.rowInfo[i].seatInfo[j].columnNumber === columnNumCurr)) {
						seatChartMapStr += cabinIndicator;
						if (cabin.rowInfo[i].seatInfo[j].seatFeature) {
							featureFound = true;
							seatFeatureStr += "w";
							// TODO: Need to handle facility on LEFT and RIGHT
							// of a seat, currently handling them along with
							// FRONT and BACK, need fixing..
							if (cabin.rowInfo[i].seatInfo[j].seatFeature[0].seatFeaturePosition == 'FRONT'
									|| cabin.rowInfo[i].seatInfo[j].seatFeature[0].seatFeaturePosition == 'RIGHT') {
								featureFront = true;
							} else if (cabin.rowInfo[i].seatInfo[j].seatFeature[0].seatFeaturePosition == 'BACK'
									|| cabin.rowInfo[i].seatInfo[j].seatFeature[0].seatFeaturePosition == 'LEFT') {
								featureBack = true;
							}
						} else {
							seatFeatureStr += "_"
						}
					} else {
						if (cabinIndicator == 'b' || cabinIndicator == 'c') {
							seatChartMapStr += ("___" + cabinIndicator);
						} else {
							seatChartMapStr += ("_" + cabinIndicator);
						}
						if (cabin.rowInfo[i].seatInfo[j].seatFeature) {
							if (cabinIndicator == 'b' || cabinIndicator == 'c') {
								seatFeatureStr += "___w";
							} else {
								seatFeatureStr += "_w";
							}
						} else {
							seatFeatureStr += "_";
						}
					}
					blankRowStr += "_";
					columnNumCurr = cabin.rowInfo[i].seatInfo[j].columnNumber;
				}
				columnNumCurr = -1;
				if (featureFound) {
					if (featureFront) {
						seatChartYAxisLabels.push("");
						seatChartMap.push(blankRowStr);
						seatChartYAxisLabels.push("");
						seatChartMap.push(seatFeatureStr);
						seatChartYAxisLabels.push(cabin.rowInfo[i].rowNum);
						seatChartMap.push(seatChartMapStr);
					} else if (featureBack) {
						seatChartYAxisLabels.push(cabin.rowInfo[i].rowNum);
						seatChartMap.push(seatChartMapStr);
						seatChartYAxisLabels.push("");
						seatChartMap.push(seatFeatureStr);
						seatChartYAxisLabels.push("");
						seatChartMap.push(blankRowStr);
					}

				} else {
					seatChartYAxisLabels.push(cabin.rowInfo[i].rowNum);
					seatChartMap.push(seatChartMapStr);
				}

				seatChartMapStr = "";
				seatFeatureStr = "";
				blankRowStr = "";
				featureFound = false;
				featureFront = false;
				featureBack = false;
			}
		}

		var seatChartAttrJsonStr = "{" + seatChartAttrStr + "}";
		seatChartAttributes = JSON.parse(JSON.stringify(eval("("
				+ seatChartAttrJsonStr + ")")));
		for ( var chartXAxisLabelCtr in chartXAxisLabelStr) {
			seatChartXAxisLabels.push(chartXAxisLabelStr[chartXAxisLabelCtr]);
		}

		ACC.ancillary.firstSeatLabel = this.config.firstSeatLabelDefault;

		ACC.ancillary.seatChartMain = $(
				'#y_ancillarySeatMap' + this.seatMapIndexCurrent).seatCharts(
				{
					map : seatChartMap,
					seats : seatChartAttributes,
					naming : {
						top : true,
						getLabel : function(character, row, column) {
							// return ACC.ancillary.firstSeatLabel++;
							return "";
						},
						columns : seatChartXAxisLabels,
						rows : seatChartYAxisLabels
					},
					legend : {
						node : $('#y_ancillarySeatMapLegend'
								+ this.seatMapIndexCurrent),
						items : ACC.ancillary.seatChartLegend
					},
					click : this.handleSeatClick
				});

		// initialise passengers
		if (!ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats) {
			// if no passenger seats are found in JSON, initialize them
			ACC.ancillary.initializePassengerSeats();
			// if seats were selected and added to cart and page was refreshed,
			// then make those seats pre-selected and populate iniatialized
			// seats with data from cart
			if (ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats) {
				var selectedSeats = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats;
				for ( var selectedSeatCtr in selectedSeats) {
					ACC.ancillary
							.setPassengerSeatFromCart(
									ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats[selectedSeatCtr].traveller.label,
									ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats[selectedSeatCtr].seatNumber);
				}
			}
		} else {
			// if JSON does contain passenger seats, set those passenger seats
			// in the Seat Chart and Radio buttons

			var passengerSeats = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats;
			for ( var passengerSeatCtr in passengerSeats) {
				ACC.ancillary
						.setPassengerSeat(
								passengerSeatCtr,
								ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerSeatCtr].seatNum);
			}
		}
		// initialise seat availability
		var cabin = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].seatMapDetail.cabin;
		for ( var cabinCtr in cabin) {
			if (cabin[cabinCtr].seatAvailability) {
				for ( var seatAvailableCtr in cabin[cabinCtr].seatAvailability) {
					var availabilityIndicator = cabin[cabinCtr].seatAvailability[seatAvailableCtr].availabilityIndicator;
					if ("N" == availabilityIndicator) {
						// below flags are used to handle "amend order" related
						// scenarios.
						var isAlreadySelected = false, isSelectedInPast = false, unavailableSeat = cabin[cabinCtr].seatAvailability[seatAvailableCtr], selectedSeats = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats, selectedSeatCtr;
						// if current 'unavailableSeat' seat is one of the
						// selected seats in the current cart, then it would be
						// shown as
						// selected already, and it should not be shown as
						// blocked.
						for (selectedSeatCtr in selectedSeats) {
							var selectedSeat = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats[selectedSeatCtr];
							if (unavailableSeat.seatNumber == selectedSeat.seatNumber) {
								isAlreadySelected = true;
								isSelectedInPast = true;
								break;
							}
						}
						// however, if current 'unavailableSeat' is not one of
						// the selected seats in the current cart, but it
						// belongs to
						// one of the traveller in current transport offering,
						// then show it as selected rather than blocked.
						if (!isAlreadySelected) {
							for (selectedSeatCtr in selectedSeats) {
								var selectedSeat = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].selectedSeats[selectedSeatCtr];
								if (unavailableSeat.transportOfferingCode == selectedSeat.transportOffering.code
										&& unavailableSeat.traveller.uid == selectedSeat.traveller.uid) {
									isSelectedInPast = true;
									break;
								}
							}
						}
						// if current 'unavailableSeat' is neither one of the
						// selected seats in the current cart, nor it belongs to
						// one of the traveller in current transport
						// offering, and nor it was de-selected by a traveller
						// as part of amend
						// order(ACC.ancillary.travellerMatch(unavailableSeat)
						// call handles the scenario that traveller de-selected
						// a seat as part of amend order and does not select any
						// other seat,
						// and then refreshes the page, he should not see that
						// seat as blocked), then show it as blocked.
						if (!isSelectedInPast
								&& !ACC.ancillary
										.travellerMatch(unavailableSeat)) {
							ACC.ancillary.seatChartMain.status(
									unavailableSeat.seatNumber, 'unavailable');
						}
					} else if ("D" == availabilityIndicator) {
						var disableSeat = cabin[cabinCtr].seatAvailability[seatAvailableCtr];
						ACC.ancillary.seatChartMain.status(
								disableSeat.seatNumber, 'not-available');
					}
				}
			}
		}
	},

	handleSeatClick : function() {
		if (this.status() == 'available') {
			// get the number for the currently selected passenger
			var $radioButtons = $("#seating-tab-"
					+ ACC.ancillary.seatMapIndexCurrent
					+ " input.y_ancillaryPassengerSeatRadio");
			var $radioButtonChecked = $radioButtons.filter(':checked');
			var passengerNum = $radioButtons.index($radioButtonChecked);
			var previousSelectedSeat = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum;
			if (ACC.ancillary.addSeatToCart(this.settings.id,
					previousSelectedSeat, passengerNum)) {
				ACC.ancillary.setPassengerSeat(passengerNum, this.settings.id);
				return 'selected';
			} else {
				return this.style();
			}
		} else if (this.status() == 'selected') {
			// seat has been vacated
			var $radioButtons = $("#seating-tab-"
					+ ACC.ancillary.seatMapIndexCurrent
					+ " input.y_ancillaryPassengerSeatRadio");
			var $radioButtonChecked = $radioButtons.filter(':checked');
			var passengerNum = $radioButtons.index($radioButtonChecked);
			var $radioButtonCurr = $radioButtons.eq(passengerNum);
			$(
					"label[for='" + $radioButtonCurr.attr('id')
							+ "'] .passenger-selected-seat").html("__");
			ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum = "";
			if (ACC.ancillary
					.removeSeatFromCart(this.settings.id, passengerNum)) {
				return 'available';
			} else {
				return this.style();
			}
		} else if (this.status() == 'unavailable') {
			// seat has been already booked
			return 'unavailable';
		} else {
			return this.style();
		}
	},

	initializePassengerSeats : function() {
		var matchingItinerary = ACC.ancillary.getMatchingItinerary();
		var travellers = matchingItinerary.travellers;
		ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats = [];
		var adultNum = 1;
		var childNum = 1;
		var infantNum = 1;
		for (passengerTypeCtr in travellers) {
			if (travellers[passengerTypeCtr].travellerInfo.passengerType.code == 'adult') {
				ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats
						.push({
							"passengerType" : "Adult" + adultNum,
							"seatNum" : "",
							"code" : travellers[passengerTypeCtr].label
						});
			} else if (travellers[passengerTypeCtr].travellerInfo.passengerType.code == 'child') {
				ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats
						.push({
							"passengerType" : "Child" + childNum,
							"seatNum" : "",
							"code" : travellers[passengerTypeCtr].label
						});
			} else if (travellers[passengerTypeCtr].travellerInfo.passengerType.code == 'infant') {
				ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats
						.push({
							"passengerType" : "Infant" + infantNum,
							"seatNum" : "",
							"code" : travellers[passengerTypeCtr].label
						});
			}

			++adultNum;
			++childNum;
			++infantNum;
		}
	},

	// set passenger seat in the Seat Chart and Radio buttons
	setPassengerSeat : function(passengerNum, seatNumber) {
		var $radioButtons = $("#seating-tab-"
				+ ACC.ancillary.seatMapIndexCurrent
				+ " input.y_ancillaryPassengerSeatRadio");
		var $radioButtonChecked = $radioButtons.filter(':checked');
		var $radioButtonCurr = $radioButtons.eq(passengerNum);

		// make previously selected seat available
		if (seatNumber != ""
				&& ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum != "") {
			ACC.ancillary.seatChartMain
					.status(
							ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum,
							'available');
		}
		// set the passenger's seat to the newly selected seat
		if (seatNumber != "") {
			ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum = seatNumber;
		}
		$(
				"label[for='" + $radioButtonCurr.attr('id')
						+ "'] .passenger-selected-seat").html(seatNumber);

		if ($radioButtonChecked[0] === $radioButtonCurr[0]) {
			ACC.ancillary.seatChartMain.get([ seatNumber ]).status('selected');
		} else {
			ACC.ancillary.seatChartMain.get([ seatNumber ]).status(
					'unavailable-curr');
		}
	},

	// if the seats were selected and added to cart and page was refreshed
	setPassengerSeatFromCart : function(travellerCode, seatNumber) {
		var $radioButtons = $("#seating-tab-"
				+ ACC.ancillary.seatMapIndexCurrent
				+ " input.y_ancillaryPassengerSeatRadio");
		var $radioButtonCurr = "";
		for (ctr = 0; ctr < $radioButtons.length; ctr++) {
			if ($radioButtons[ctr].id.indexOf(travellerCode) != -1) {
				$radioButtonCurr = $radioButtons[ctr];
				break;
			}
		}
		var $radioButtonChecked = $radioButtons.filter(':checked');
		$("label[for='" + $radioButtonCurr.id + "'] .passenger-selected-seat")
				.html(seatNumber);

		var passengerSeats = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats;
		for (passengerSeatCtr in passengerSeats) {
			if (ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerSeatCtr].code == travellerCode) {
				ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerSeatCtr].seatNum = seatNumber;
			}
		}
		if ($radioButtonChecked[0] === $radioButtonCurr) {
			ACC.ancillary.seatChartMain.get([ seatNumber ]).status('selected');
		} else {
			ACC.ancillary.seatChartMain.get([ seatNumber ]).status(
					'unavailable-curr');
		}
	},

	getMatchingItinerary : function() {
		var currentTransportOffering = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].transportOffering;
		var noOfItineraries = ACC.appmodel.itineraryJsonObj.length;
		var i;
		for (i = (noOfItineraries - 1); i >= 0; i--) {
			for (j in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions) {
				for (k in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings) {
					if (ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings[k].code == currentTransportOffering.code) {
						return ACC.appmodel.itineraryJsonObj[i];
					}
				}
			}
		}
	},

	travellerMatch : function(unavailableSeat) {
		var matchingItinerary = ACC.ancillary.getMatchingItinerary();
		var travellers = matchingItinerary.travellers;
		for (passengerTypeCtr in travellers) {
			if (unavailableSeat.traveller != null
					&& travellers[passengerTypeCtr].uid == unavailableSeat.traveller.uid) {
				return true;
			}
		}
		return false;

	},

	addSeatToCart : function(selecteSeatNum, previousSelectedSeat,
			selectedPassengerNum) {
		var currentTransportOfferingCode = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].transportOffering.code;
		var accommodationMapCode = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].accommodationMapCode;
		var noOfItineraries = ACC.appmodel.itineraryJsonObj.length;
		var i;
		var associatedItinerary = "";
		for (i = (noOfItineraries - 1); i >= 0; i--) {
			for (j in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions) {
				for (k in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings) {
					if (ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings[k].code == currentTransportOfferingCode) {
						associatedItinerary = ACC.appmodel.itineraryJsonObj[i];
						break;
					}
				}
				if (associatedItinerary != "") {
					break;
				}
			}
			if (associatedItinerary != "") {
				break;
			}
		}
		var selectedTravellerCode = associatedItinerary.travellers[selectedPassengerNum].label;
		var travelRoute = associatedItinerary.route.code;
		var originDestinationRefNo = associatedItinerary.originDestinationOptions[0].originDestinationRefNumber;
		$
				.when(
						ACC.services.addSeatToCartAjax(selecteSeatNum,
								previousSelectedSeat,
								currentTransportOfferingCode,
								selectedTravellerCode, travelRoute,
								originDestinationRefNo, accommodationMapCode))
				.then(
						function(response) {
							var jsonData = JSON.parse(response);

							if (jsonData.valid) {
								ACC.reservation
										.refreshReservationTotalsComponent($(
												"#y_reservationTotalsComponentId")
												.val());
								ACC.reservation
										.refreshTransportSummaryComponent($(
												"#y_transportSummaryComponentId")
												.val());
							} else {
								var output = [];
								jsonData.errors.forEach(function(error) {
									output.push("<p>" + error + "</p>");
								});
								$(
										"#y_addProductToCartErrorModal .y_addProductToCartErrorBody")
										.html(output.join(""));
								$("#y_addProductToCartErrorModal").modal();
							}
							addToCartResult = jsonData.valid;
						});
		return addToCartResult;
	},

	getSeatMap : function(url) {
		var returnedJson;
		$.when(ACC.services.getSeatMapAjax(url)).then(function(response) {
			if (response.seatMap) {
				returnedJson = response;
			} else {
				console.log("No seat Map found");
			}
		});
		return returnedJson;
	},

	removeSeatFromCart : function(selecteSeatNum, selectedPassengerNum) {
		var currentTransportOfferingCode = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].transportOffering.code;
		var accommodationMapCode = ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].accommodationMapCode;
		var noOfItineraries = ACC.appmodel.itineraryJsonObj.length;
		var i;
		var associatedItinerary = "";
		for (i = (noOfItineraries - 1); i >= 0; i--) {
			for (j in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions) {
				for (k in ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings) {
					if (ACC.appmodel.itineraryJsonObj[i].originDestinationOptions[j].transportOfferings[k].code == currentTransportOfferingCode) {
						associatedItinerary = ACC.appmodel.itineraryJsonObj[i];
						break;
					}
				}
				if (associatedItinerary != "") {
					break;
				}
			}
			if (associatedItinerary != "") {
				break;
			}
		}
		var selectedTravellerCode = associatedItinerary.travellers[selectedPassengerNum].label;
		var travelRoute = associatedItinerary.route.code;
		$
				.when(
						ACC.services.removeSeatFromCartAjax(selecteSeatNum,
								currentTransportOfferingCode,
								selectedTravellerCode, travelRoute,
								accommodationMapCode))
				.then(
						function(response) {
							var jsonData = JSON.parse(response);

							if (jsonData.valid) {
								ACC.reservation
										.refreshReservationTotalsComponent($(
												"#y_reservationTotalsComponentId")
												.val());
								ACC.reservation
										.refreshTransportSummaryComponent($(
												"#y_transportSummaryComponentId")
												.val());
							} else {
								var output = [];
								jsonData.errors.forEach(function(error) {
									output.push("<p>" + error + "</p>");
								});
								$(
										"#y_addProductToCartErrorModal .y_addProductToCartErrorBody")
										.html(output.join(""));
								$("#y_addProductToCartErrorModal").modal();
							}
							addToCartResult = jsonData.valid;
						});
		return addToCartResult;
	},

	bindPassengerRadioChange : function() {
		// when you change the currently selected passenger
		$("#y_ancillaryPanelSeatingOptions")
				.on(
						"change",
						"input.y_ancillaryPassengerSeatRadio",
						function() {
							// make all currently selected seats have an
							// "unavailable" state
							if (ACC.ancillary.seatChartMain.find('selected').seatIds[0]) {
								ACC.ancillary.seatChartMain.status(
										ACC.ancillary.seatChartMain
												.find('selected').seatIds[0],
										'unavailable-curr');
							}

							// get the number for the currently selected
							// passenger
							var $radioButtons = $("#seating-tab-"
									+ ACC.ancillary.seatMapIndexCurrent
									+ " input.y_ancillaryPassengerSeatRadio");
							var passengerNum = $radioButtons.index($(this));
							// make current passenger's seat selected
							if (ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum != "") {
								ACC.ancillary.seatChartMain
										.status(
												ACC.ancillary.seatMapJsonObj.seatMap[ACC.ancillary.seatMapIndexCurrent].passengerSeats[passengerNum].seatNum,
												'selected');
							}

							// remove styling classes from all blocks, and the
							// add it only for the selected block
							$('input.y_ancillaryPassengerSeatRadio').parent()
									.removeClass('current-passenger-select'); // using
							// a
							// CSS
							// class
							// without
							// 'y_'
							// because
							// there
							// is
							// styling
							// associated
							// with
							// the
							// selected
							// passenger
							$(this).parent().addClass(
									'current-passenger-select');
						});
	},
	bindTabChange : function() {
		$("#y_ancillaryPanelSeatingOptions .nav > li > a").on(
			"click",
			function() {
				// set the current itinerary number
				ACC.ancillary.seatMapIndexCurrent = parseInt($(this).attr(
					'href').match(/\d+$/));
				ACC.ancillary.seatChartMain = $(
					'#y_ancillarySeatMap'
					+ ACC.ancillary.seatMapIndexCurrent)
					.seatCharts({});
			});

		$(".tab-wrapper li[role=presentation]").on("click",function(){
			var tabPanelId = $(this).find("a[role=tab]").attr("href");
			$(this).parent().find("li[role=presentation]").removeClass("active");
			$(this).parent().find("li[role=presentation]").attr("aria-expanded",false);
			$(this).parent().find("li[role=presentation]").removeClass("active");
			$(this).parent().find("li[role=presentation] a[role=tab]").removeClass("active");

			$(this).addClass("active");
			$(this).attr("aria-expanded",true);
			$(this).parent().find(".tab-content .tab-pane"+tabPanelId).addClass("active");
			$(this).find("li[role=presentation] a[role=tab]").addClass("active");
		});
	},

	// Bind function for 'Add Another Item' Link
	bindAddAnotherItemButton : function() {
		// Toggle class to the parent of the selected/checked radio button
		// Add new item
		$(".y_ancillaryFormAddBlock")
				.on(
						'click keydown',
						function(e) {
							if ($(':animated').length) {
								return false; // don't respond to clicks until
								// animation
								// completed
							}
							// handle the keypress
							if (e.type === "keydown") {
								var keyCode = e.keyCode || e.which;
								// if it's not the Enter key or Spacebar, do
								// nothing
								if (keyCode != 13 && keyCode != 32) {
									return true;
								}
								// prevent default for Spacebar
								else if (keyCode == 32) {
									e.preventDefault();
								}
							}

							var targetName = $(this).attr('data-target'), $targetElem = $('.'
									+ targetName), num = parseInt($targetElem
									.last().attr('id').match(/\d+$/)), // index of the greatest "duplicatable" input fields
							newNum = new Number(num + 1), // the numeric ID of
							// the new
							// input field being added
							$newElem = $('#' + targetName + num).clone().attr(
									'id', targetName + newNum).fadeIn('slow'); // create
							// the
							// new
							// element
							// via
							// clone()
							// and
							// manipulate
							// it's
							// ID
							// using
							// newNum
							// value

							// increment the number on ID's, classes, etc... for
							// the new HTML
							// block
							$newElem.find('#' + targetName + num).attr('id',
									'#' + targetName + newNum);
							// Set the new select field
							$newElem.find("select").each(
									function(index, input) {
										input.name = input.name.replace(
												targetName + num, targetName
														+ newNum);
										input.selectedIndex = 0; // Default
										// selected
										// option is
										// the
										// first one
									});
							// Set the new input field
							$newElem.find("input").each(
									function(index, input) {
										input.name = input.name.replace(
												targetName + num, targetName
														+ newNum);
										input.value = 0; // Default input
										// value is 0
									});
							// Set the new deleteItemButton
							$newElem.find(".y_ancillaryFormDeleteBlock").each(
									function(index, input) {
										input.setAttribute('data-target', input
												.getAttribute('data-target')
												.replace(targetName + num,
														targetName + newNum));
										$(input).css('display', 'block'); // if
										// the
										// first
										// row
										// doesn't
										// have it visible and we
										// are cloning it, make the
										// button appear
									});

							// Make the new row appear after the last one
							$('#' + targetName + num).after($newElem);

							// Set the new minus/plus button
							$newElem.find("button").each(
									function(index, input) {
										input.setAttribute('data-field', input
												.getAttribute('data-field')
												.replace(targetName + num,
														targetName + newNum));
										ACC.ancillary.showQtyButtons($(input),
												false); // Hidden by
										// default
									});

							// Hide the "Add another item" link
							var $li = $targetElem
									.closest('li.y_ancillaryListElement');
							$li.find(".y_ancillaryFormAddBlock")
									.fadeOut('slow');
							$li.find(".y_ancillaryOfferItem").last().find(
									":tabbable:last").focus();
						});
	},

	// Bind function for deleteItem button
	bindRemoveItemButton : function() {
		// Delete item
		// We need to bind it to document because the delete block can be added
		// dynamically
		$(document)
				.on(
						'click keydown',
						'.y_ancillaryFormDeleteBlock',
						function(e) {
							if ($(':animated').length) {
								return false; // don't respond to clicks until
								// animation
								// completed
							}

							// handle the keypress
							if (e.type === "keydown") {
								var keyCode = e.keyCode || e.which;
								// if it's not the Enter key or Spacebar, do
								// nothing
								if (keyCode != 13 && keyCode != 32) {
									return true;
								}
								// prevent default for Spacebar
								else if (keyCode == 32) {
									e.preventDefault();
								}
							}
							$(this).blur(); // blur the element so you can
							// continue tabbing
							// where you left off
							$("#" + $(this).data('target')).prev().find(
									":tabbable:last").focus();

							var targetName = $(this).attr('data-target'), $targetElem = $('#'
									+ targetName), targetGroup = targetName
									.replace(/\d+$/, ""), num = $('.'
									+ targetGroup).length;

							// Removing item from cart
							var productCode = $targetElem.find(
									"select option:selected").attr('value');
							var qty = $targetElem.find("input.y_inputNumber")
									.attr('value');
							var travellerCode = $targetElem.parent().find(
									'.y_ancillary_travellerCode').attr('value');

							var addToCartResult = true;
							if (productCode && qty > 0) {
								addToCartResult = ACC.ancillary
										.addToCartFormSubmit($targetElem,
												productCode, travellerCode,
												-qty);
							}

							if (addToCartResult) {
								var $li = $targetElem
										.closest('li.y_ancillaryListElement');
								var travellerMinOfferGroupQty = parseInt($(this)
										.closest(".y_offerGroup")
										.find(
												"input.y_travellerMinOfferGroupQty")
										.val());
								var minQty = (travellerMinOfferGroupQty > 0) ? travellerMinOfferGroupQty
										: 1;

								// If number of items (num) is greater than 1,
								// i.e this is not
								// the last item, then we delete the row
								if ($li.find(".y_ancillaryOfferItem").size() > minQty) {

									// Make the item disappear and remove it
									$targetElem
											.fadeOut(
													'slow',
													function() {
														$(this).remove();
														ACC.ancillary
																.updateSelectOptions($li); // Update
														// select
														// options
														var allSelected = true;
														$li
																.find("select")
																.each(
																		function(
																				index,
																				item) {
																			if ($(
																					item)
																					.prop(
																							"selectedIndex") == 0) {
																				allSelected = false;
																			}
																		});
														if (allSelected) {
															$li
																	.find(
																			".y_ancillaryFormAddBlock")
																	.fadeIn(
																			'slow'); // Make
															// the
															// 'Add
															// another
															// item'
															// link
															// appear
															// (if
															// it
															// was
															// hidden)
														}
													});
								}

								// If this is the last item, we just reset it to
								// the initial
								// status
								else {
									$targetElem.find(
											".y_ancillaryFormDeleteBlock")
											.fadeOut('slow'); // Hide
									// the
									// deleteItem
									// Button
									$targetElem.find("select").each(
											function(index, input) {
												input.selectedIndex = 0; // Default
												// selected
												// option
												// is the first one
											});
									$targetElem.find("input").each(
											function(index, input) {
												input.value = 0; // Default
												// input
												// value is
												// 0
											});
									$targetElem.find("button").each(
											function(index, input) {
												ACC.ancillary.showQtyButtons(
														$(input), false); // Hidden
												// by
												// default
											});
									ACC.ancillary.updateSelectOptions($li); // Update
									// select
									// options
									$li.find(".y_ancillaryFormAddBlock")
											.fadeOut('slow');
								}
							}
						});
	},

	// Bind function for minus/plus button
	bindQuantityFieldChange : function() {
		$('.y_ancillarySection').on(
				'click',
				'.y_ancillaryFormQuantityWrapper .y_inputNumberChangeBtn',
				function(e) {
					var $target = $("#" + $(this).data('field'));
					var type = $(this).attr('data-type');
					var productCode = $target.find("select option:selected")
							.attr('value');
					var travellerCode = $target.parent().find(
							'.y_ancillary_travellerCode').attr('value');
					if (productCode) {
						if (type == 'minus') {
							addToCartResult = ACC.ancillary
									.addToCartFormSubmit($target, productCode,
											travellerCode, -1);
						}
						if (type == 'plus') {
							addToCartResult = ACC.ancillary
									.addToCartFormSubmit($target, productCode,
											travellerCode, 1);
						}
						if (!addToCartResult) {
							// add to cart returned error, revert the selected
							// option to
							// the original one
							e.stopPropagation();
						}
					}
				});
	},

	// Bind function for select when a different option is selected
	bindOfferItemChange : function() {
		var productCodePrevious = "";
		$(document).on('focus','.y_ancillaryOfferItem select',function(){
			var $target = $(this).closest('.y_ancillaryOfferItem');
			productCodePrevious = $target.find("select option:selected").attr('value');

			//adding the selected element id so that this can be used if there is a price difference
			targetSelectIdForPriceDifference = $target.attr("id");
		}).on('change', '.y_ancillaryOfferItem select', function(e) {
			var $inputBoxTarget = $(this).closest('.y_ancillaryOfferItem').find('.y_inputNumber');
			var $target = $(this).closest('.y_ancillaryOfferItem');
			var oldQty = $inputBoxTarget.attr('value');
			var travellerCode = $target.parent().find('.y_ancillary_travellerCode').attr('value');

							// If there was another product, remove it from the
							// cart
							var addToCartResult = true;
							if (productCodePrevious && oldQty > 0) {
								addToCartResult = ACC.ancillary
										.addToCartFormSubmit($target,
												productCodePrevious,
												travellerCode, -oldQty);
							}

							if (!addToCartResult) {
								// add to cart returned error, revert the
								// selected option to the
								// original one
								$(this).find(
										'option[value="' + productCodePrevious
												+ '"]').prop('selected', true);
							} else {
								// input data-min, data-max and
								// data-defaultvalue are set
								// accordingly to the selected product
								var selectedIndex = $(this).prop(
										"selectedIndex");
								if (selectedIndex == 0) {
									// The default option is selected (no
									// product)
									$inputBoxTarget.val(0);
									$target.find("button").each(
											function(index, input) {
												ACC.ancillary.showQtyButtons(
														$(input), false); // Hidden by default
											});

									var travellerMinOfferGroupQty = parseInt($(
											this)
											.closest(".y_offerGroup")
											.find(
													"input.y_travellerMinOfferGroupQty")
											.val());
									var minQty = (travellerMinOfferGroupQty > 0) ? travellerMinOfferGroupQty
											: 1;
									var $li = $target
											.closest('li.y_ancillaryListElement');
									if ($li.find(".y_ancillaryOfferItem")
											.size() == minQty) {
										$target.find(
												".y_ancillaryFormDeleteBlock")
												.fadeOut('slow');
									}
									var $li = $target
											.closest('li.y_ancillaryListElement');
									$li.find(".y_ancillaryFormAddBlock")
											.fadeOut('slow');

								} else {
									var $selectedOption = $(this)
											.find("option")[selectedIndex];

									// Setting min, max, default value and value
									// in input field
									var min = $($selectedOption).attr(
											"data-min");
									var max = $($selectedOption).attr(
											"data-max");
									$inputBoxTarget.attr("data-min", min);
									$inputBoxTarget.attr("data-max", max);
									var minAsDefault = min > 1;
									var defaultValue = minAsDefault ? min : 1;
									$inputBoxTarget.data('defaultvalue',
											defaultValue);

									var newVal = $inputBoxTarget
											.data('defaultvalue');
									$inputBoxTarget.attr('value', newVal);
									$inputBoxTarget.val(newVal);

					// Add new product to the cart
					var addToCartResult = true;
					if (newVal > 0) {
						var productCode = $target.find("select option:selected").attr('value');
						addToCartResult = ACC.ancillary.addToCartFormSubmit($target, productCode, travellerCode, newVal);
					}
					if (addToCartResult) {
						// addToCart was successful

						$target.find(".y_ancillaryFormDeleteBlock").fadeIn('slow'); // If the deleteItem button was not visible, make it appear

										// if possible, make the "add another
										// item" button
										// appear
										// If the number of row is equal to the
										// number of
										// product that can be selected (size -
										// 1) then hide the
										// "Add another item" link
										var travellerMaxOfferGroupQty = parseInt($(
												this)
												.closest(".y_offerGroup")
												.find(
														"input.y_travellerMaxOfferGroupQty")
												.val());
										var maxQty = (travellerMaxOfferGroupQty != -1 && travellerMaxOfferGroupQty < $target
												.find("option").size() - 1) ? travellerMaxOfferGroupQty
												: $target.find("option").size() - 1;
										var travellerMinOfferGroupQty = parseInt($(
												this)
												.closest(".y_offerGroup")
												.find(
														"input.y_travellerMinOfferGroupQty")
												.val());
										var minQty = (travellerMinOfferGroupQty > 0) ? travellerMinOfferGroupQty
												: 1;
										var $li = $target
												.closest('li.y_ancillaryListElement');

										var allSelected = true;
										$li
												.find("select")
												.each(
														function(index, item) {
															if ($(item)
																	.prop(
																			"selectedIndex") == 0) {
																allSelected = false;
															}
														});

										if ($li.find(".y_ancillaryOfferItem")
												.size() < maxQty
												&& $li
														.find(
																".y_ancillaryOfferItem")
														.size() >= minQty
												&& allSelected) {
											$li
													.find(
															".y_ancillaryFormAddBlock")
													.fadeIn('slow');
										}

									} else {
										// addToCart returned error
										$(this).prop("selectedIndex", 0); // Set
										// the
										// selected
										// option to the
										// default one
										$target.find("input").each(
												function(index, input) {
													input.value = 0; // Default
													// input
													// value
													// is 0
												});
										$target.find("button").each(
												function(index, input) {
													ACC.ancillary
															.showQtyButtons(
																	$(input),
																	false); // Hidden
													// by
													// default
												});
										var $li = $target
												.closest('li.y_ancillaryListElement');
										var travellerMinOfferGroupQty = parseInt($(
												this)
												.closest(".y_offerGroup")
												.find(
														"input.y_travellerMinOfferGroupQty")
												.val());
										var minQty = (travellerMinOfferGroupQty > 0) ? travellerMinOfferGroupQty
												: 1;
										if ($li.find(".y_ancillaryOfferItem")
												.size() <= minQty) {
											$target
													.find(
															".y_ancillaryFormDeleteBlock")
													.fadeOut('slow');
										}

									}

								}

								// Update select options
								var $li = $target
										.closest('li.y_ancillaryListElement');
								ACC.ancillary.updateSelectOptions($li);

							}

						});
	},

	bindCheckboxChange : function() {
		$(document)
				.on(
						'change',
						'.y_ancillaryFormQuantityWrapper .y_OfferProductCheckBoxSelection',
						function(e) {
							var $target = $(this);
							var productCode = $target.attr('value');
							var travellerCode = $target.closest(
									'.y_ancillaryListElement').find(
									'.y_ancillary_travellerCode').attr('value');
							var qty = parseInt($target.attr("min"));
							if (productCode) {
								if (this.checked) // if changed state is
								// "CHECKED"
								{
									var success = ACC.ancillary
											.addToCartFormSubmit($target,
													productCode, travellerCode,
													qty);
									if (!success) {
										this.checked = false;
									}
								} else {
									var success = ACC.ancillary
											.addToCartFormSubmit($target,
													productCode, travellerCode,
													-qty);
									if (!success) {
										this.checked = true;
									}
								}
							}
						});
	},

	// Bind function for continue button in the itinerary component to perform a
	// validation for travel restriction
	bindReservationContinueButton : function() {
		$(document)
				.on(
						'click',
						'.y_reservationContinueButton',
						function(event) {
							var jsonData;
							var url = ACC.config.contextPath
									+ "/ancillary/check-offer-groups-restriction";
							if ($(this).attr("data-amend")) {
								url = ACC.config.contextPath
										+ "/manage-booking/ancillary/check-offer-groups-restriction";
							}
							$.when(
									ACC.services
											.checkOfferGroupsRestriction(url))
									.then(function(response) {
										jsonData = JSON.parse(response);
									});
							if (jsonData.hasErrorFlag) {
								event.preventDefault();
								var output = [];
								jsonData.errors.forEach(function(error) {
									output.push("<p>" + error + "</p>");
								});
								$(
										"#y_travelRestrictionModal .y_travelRestrictionErrorBody")
										.html(output.join(""));
								$("#y_travelRestrictionModal").modal();
							}
							// if isValid == true continue with the submit
						});
	},

	addToCartFormSubmit : function($target, productCode, travellerCode, qty) {

		var travelRouteCode = $target.closest(".tab-wrapper").find(
		".nav-tabs li a.active").parent().find("input[name=travelRouteCode]").attr('value');
		var originDestinationRefNumber = $target.closest(".tab-wrapper").find(
			".nav-tabs li a.active").parent().find("input[name=originDestinationRefNumber]").attr(
			'value');

		var journeyRefNumber = $target.closest(".tab-wrapper").find(
		".nav-tabs li a.active").parent().find("input[name=journeyRefNumber]").attr(
		'value');

		var cachedPrice = $target.find("option:selected").data("cachedprice");

		var transportOfferingCodes = [];
		$target.closest(".y_ancillaryFormQuantityWrapper").find(
				"input[name=transportOfferingCodes]").each(function() {
			transportOfferingCodes.push($(this).val());
		});
		$("#addToCartForm #y_productCode").attr('value', productCode);
		$("#addToCartForm #y_transportOfferingCodes").attr('value',
				transportOfferingCodes);
		$("#addToCartForm #y_travelRouteCode").attr('value',
				travelRouteCode ? travelRouteCode : "");
		$("#addToCartForm #y_travellerCode").attr('value',
				travellerCode ? travellerCode : "");
		$("#addToCartForm #y_quantity").attr('value', qty);
		$("#addToCartForm #y_journeyRefNumber").attr('value',journeyRefNumber);
		$("#addToCartForm #y_originDestinationRefNumber").attr('value', originDestinationRefNumber ? originDestinationRefNumber : 0);
		$("#addToCartForm #y_cachedPrice").attr('value',cachedPrice);


		var addToCartResult;
		$
				.when(ACC.services.addProductToCartAjax())
				.then(
						function(response) {
							var jsonData = JSON.parse(response);

				if(jsonData.valid) {
					ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
					ACC.reservation.refreshTransportSummaryComponent($("#y_transportSummaryComponentId").val());
				} else {
					var output = [];
					jsonData.errors.forEach(function(error) {
						output.push("<p>" + error + "</p>");
					});

					if (jsonData.errorCode == "Error101") {
						var modelButtons_HTML = '';
						modelButtons_HTML += '<div class="modal-button-area">';
						modelButtons_HTML += '<button type="buton" class="btn-cancel">Cancel </button>';
						modelButtons_HTML += '<button type="buton" class="btn-proceed"> Proceed </button>';
						modelButtons_HTML += '</div>';

						$("#y_addProductToCartErrorModal .modal-content").append(modelButtons_HTML);
						ACC.ancillary.bindModalButtons();
					}
					$("#y_addProductToCartErrorModal .y_addProductToCartErrorBody").html(output.join(""));
					$("#y_addProductToCartErrorModal").modal();
				}
				addToCartResult = jsonData.valid;
			});
		return addToCartResult;
	},

	updateSelectOptions : function($li) {
		// create a list of disabled options
		var disabledOptions = [];
		$li.find('select')
				.each(
						function(index, element) {
							var selectedIndex = $(element)
									.prop('selectedIndex');
							if (selectedIndex != 0
									&& jQuery.inArray(selectedIndex,
											disabledOptions) == -1) {
								disabledOptions.push(selectedIndex);
							}
						})
		// update the list
		$li
				.find('select')
				.each(
						function(index, element) {
							for (var count = 0; count < element.options.length; count++) {
								if ($(element).prop('selectedIndex') != count
										&& jQuery.inArray(count,
												disabledOptions) == -1
										&& $(element.options[count]).attr(
												"class") != "y_noProductsAvailable") {
									element.options[count]
											.removeAttribute('disabled');
								} else {
									element.options[count].setAttribute(
											'disabled', 'disabled');
								}
							}
						});
	},

	showQtyButtons : function($button, flag) {
		if (flag) {
			$button.show();
		} else {
			$button.hide();
		}
	},

	bindSelectUpgradeBundleOptions : function() {
		$('#y_upgradeBundleOptionsButton').on("click", function() {
			if (!$('#y_panel-upgrade').is(":visible")) {
				$("#y_selectUpgradeSpan").hide();
				$("#y_hideUpgradeSpan").show();
				ACC.ancillary.getUpgradeBundleOptions();
			} else {
				$("#y_selectUpgradeSpan").show();
				$("#y_hideUpgradeSpan").hide();
			}
		});
	},

	getUpgradeBundleOptions : function() {
		var upgradeBundlesData = '';
		$.when(ACC.services.getUpgradeBundleOptions()).then(function(response) {
			if (response.isUpgradeOptionAvailable) {
				var htmlContent = response.htmlContent;
				$('#y_panel-upgrade').html(htmlContent);
				ACC.ancillary.bindSelectUpgradeBundle();

			} else {
				$("#y_noUpgradeAvailableModal").modal();
				$("#y_selectUpgradeSpan").show();
				$("#y_hideUpgradeSpan").hide();
			}
		});
	},

	bindSelectUpgradeBundle : function() {
		$('.y_bundleType').on(
				"click",
				function() {
					var addBundleToCartForm = $(this).closest(
							"#y_upgradeBundleFormHiddenHtml").find('form');
					addBundleToCartForm.submit();
				});
	},

	showNoUpgradeAvailabileModal : function() {
		if ($("#y_noAccommodationAvailability")) {
			var noAccommodationAvailability = $(
					"#y_noAccommodationAvailability").val();
			if (noAccommodationAvailability == "show") {
				$("#y_noAvailabilityModal").modal();
			}
		}
	},bindAssistanceDeclarationUpdate:function(){
    	$(".accessibilityDeclaration_checkbox_js").on("change",function(){

    		var journeyRefNumber = $(this).data("journeyrefnumber");
    		var travellerUid = $(this).data("travelleruid");
    		var specialServiceCode = $(this).data("specialservicecode");

    		var serviceURL = ACC.config.encodedContextPath + "/ancillary/";
    		var postfixURL = "?journeyRefNumber="+journeyRefNumber+"&travellerUid="+travellerUid+"&specialServiceCode="+specialServiceCode;

    		if($(this).is(":checked")){
    			serviceURL += "addAccessibilityDeclaration";
    		}else{
    			serviceURL += "removeAccessibilityDeclaration";
    		}

    		$.ajax({
    			url : serviceURL+postfixURL,
    			type : "POST",
    			async : false
    		});
    	});
    },
    bindModalButtons:function(){

    	$("#y_addProductToCartErrorModal").unbind().on("click",".modal-button-area .btn-proceed",function(){
    		$("#addToCartForm #y_proceedWithDifferentPriceConsent").attr('value', true);

    		var previousProdToAdd = $("#addToCartForm #y_productCode").val();
    		if(previousProdToAdd){
    			//getting the unique select box for which Proceed Modal has been shown and triggering the addToCart
    			$(".y_ancillaryOfferItem select[name=" + targetSelectIdForPriceDifference +"]").val(previousProdToAdd).trigger('change');
    		}
    		$("#addToCartForm #y_proceedWithDifferentPriceConsent").attr('value', false);

    		removeAndHideErrorModal();
    	});

    	$("#y_addProductToCartErrorModal").on("click",".modal-button-area .btn-cancel",function(){
    		removeAndHideErrorModal();
    	});

    	$("#y_addProductToCartErrorModal").on("click",".modal-header .close",function(){
    		removeAndHideErrorModal();
    	});


    },
    bindAccessibilityDeclarationAgentComment : function() {
        $(".y_reservationContinueButton").on("click", function() {
          var agentComment = $('#agentComment').val();
          var serviceURL = ACC.config.encodedContextPath + "/ancillary/";
          var postfixURL = "?agentComment=" + agentComment;
          serviceURL += "saveAgentComment";
          var url = serviceURL + postfixURL;
          $.when(
            ACC.services
                .checkAgentComment(url))
            .then(function(response) {
              if (response) {
                return true;
              }else{
                $("#agentCommentError").css("color", "red");
                $("#agentCommentError").html("Error occured while saving comment")
                return false;
              }
            });
        });
     },
     bindExtraProductQuantitySelector : function() {
			$('.y_extraProductSelectorPlus').click(function(e) {
				e.preventDefault();
				var input = $(this).parents(".y_extraProductQuantitySelector").find("#y_extraProductQuantity");
				var totalSelected =  0;
				$(this).parents(".js-extra-product-group-by-type").find(".js-product-qty-input").each(function() {
					  totalSelected = totalSelected + parseInt($(this).val());
					});
				var quantity = parseInt($(input).val());
				var maxAllowed =  parseInt($(this).parents(".y_extraProductQuantitySelector").find("#y_extraProductQuantity").attr('max'));
				if (totalSelected < maxAllowed) {
					disableButtons();
					quantity = quantity + 1;
					$(input).val(quantity);
					var form = $(this).closest('form');
					$.when(ACC.services.addBundleToCartAjax(form)).then(
			    	    function(result) {
			    	    	var response = $.parseJSON(result);
			    			if(!response.valid) {
			    			    var output = [];
			    			    response.errors.forEach(function(error) {
			    				    output.push("<p>" + error + "</p>");
			    				});
			    				if (output.length > 0) {
			    					$("#error-add-product-to-cart-modal .add-ancillary-to-cart").html(output.join(""));
			    				}
		        				$("#error-add-product-to-cart-modal").modal("show");
		        				quantity = quantity - 1;
								$(input).val(quantity);
			    		}
			    		enableButtons();
				    });
				}
				if (totalSelected >= maxAllowed) {
					var output = [];
					output.push("<p>" + ACC.services.getValidationMessageForCode('error.ancillary.max.limit.reached.for.leg').responseText + "</p>");
					if (output.length > 0) {
    					$("#error-add-product-to-cart-modal .add-ancillary-to-cart").html(output.join(""));
    				}
    				$("#error-add-product-to-cart-modal").modal("show");
				}
			});
			$('.y_extraProductSelectorMinus').click(function(e) {
				e.preventDefault();
				var input = $(this).parents(".y_extraProductQuantitySelector").find("#y_extraProductQuantity");
				var quantity = parseInt($(input).val());
				if (quantity > 0) {
					disableButtons();
					quantity = quantity - 1;
					$(input).val(quantity);
					var form = $(this).closest('form');
					$.when(ACC.services.addBundleToCartAjax(form)).then(
			    	    function(result) {
			    	    	var response = $.parseJSON(result);
			    			if(!response.valid) {
			    			    var output = [];
			    			    response.errors.forEach(function(error) {
			    				    output.push("<p>" + error + "</p>");
			    				});
			    				if (output.length > 0) {
			    					$("#error-add-product-to-cart-modal .add-ancillary-to-cart").html(output.join(""));
			    				}
		        				$("#error-add-product-to-cart-modal").modal("show");
		        				quantity = quantity + 1;
		        				$(input).val(quantity);
			    		}
			    		enableButtons();
			    	});
				}
		});
     }
}

function removeAndHideErrorModal(){
	$("#y_addProductToCartErrorModal .modal-content .modal-button-area").remove();
	$("#y_addProductToCartErrorModal").modal('hide');
}

$(function () {
	$("div[id^='accordion-extras-page-']").accordion({ collapsible: true, active: false, heightStyle: "content" });
});

function disableButtons(){
	$(".y_extraProductSelectorPlus").attr('disabled', 'disabled');
	$(".y_extraProductSelectorMinus").attr('disabled', 'disabled');
	$("#ancillary-page-submit").addClass("disabled");
}

function enableButtons(){
	$(".y_extraProductSelectorPlus").removeAttr("disabled");
	$(".y_extraProductSelectorMinus").removeAttr("disabled");
	$("#ancillary-page-submit").removeClass("disabled");
}
