ACC.dealfinder = {
    _autoloadTracc: [
       "getAllTravelSectors",
        "init",
       
    ],
    
    componentDealParentSelector: '#y_dealFinderForm',
    
   
    init: function () {
        
        ACC.dealfinder.bindDealFinderLocationEvents();
        ACC.dealfinder.bindDealFinderValidation();
        ACC.dealfinder.bindDealTravelSectorsOriginAutoSuggest();
        ACC.dealfinder.bindDealTravelSectorsDestinationAutoSuggest();
        ACC.dealfinder.bindDealDestinationAutosuggest();
        ACC.dealfinder.bindDealAccommodationFinderLocationEvents();
    },

    bindDealFinderValidation: function () {

        $(ACC.dealfinder.componentDealParentSelector).validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            submitHandler: function (form) {
            	$.when(ACC.services.checkAmendmentCartInSession()).then(
    					function(data) {
    						if(data.result == true){
    							$('#y_confirmFreshBookingModal').modal('show');
    						}
    						else {
    						    var jsonData = ACC.formvalidation.serverFormValidation(form, "validateDealFinderFormAttributes");
    			                if (!jsonData.hasErrorFlag) {
    			                    $('#y_processingModal').modal({
    			                        backdrop: 'static',
    			                        keyboard: false
    			                    });
    			                    form.submit();
    			                }
    						}
    					}
    				);
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            rules: {
                departureDealLocationName: {

                    locationIsValid : ".y_dealFinderOriginLocationCode"
                },
                arrivalDealLocationName: {

                    locationIsValid : ".y_dealFinderDestinationLocationCode"
                }
                },
            messages: {
            	departureDealLocationName: {
                    required: ACC.addons.bcfstorefrontaddon['error.farefinder.from.location'],
                    locationIsValid: ACC.addons.bcfstorefrontaddon['error.farefinder.from.locationExists']
                },
                arrivalDealLocationName: {
                    required: ACC.addons.bcfstorefrontaddon['error.farefinder.to.location'],
                    locationIsValid: ACC.addons.bcfstorefrontaddon['error.farefinder.to.locationExists']
                },
                location: {
                            		required: ACC.addons.bcfstorefrontaddon['error.accommodationfinder.destination.location'],
                            		minlength: ACC.addons.bcfstorefrontaddon['error.accommodationfinder.destination.location.autosuggestion'],
                            		destinationLocationIsValid: ACC.addons.bcfstorefrontaddon['error.accommodationfinder.destination.location.autosuggestion']
                            	}
            }
        });

    },

     getAllTravelSectors: function() {
        	$(document).on("keyup", ".y_dealFinderOriginLocation", function(e) {
    			// ignore the Enter key
    			if ((e.keyCode || e.which) == 13) {
    				return false;
    			}

    			var locationText = $.trim($(this).val());
    			ACC.dealfinder.fireQueryToFetchAllTravelSectors(locationText);
    		});
        },

         fireQueryToFetchAllTravelSectors: function(locationText){
            	if(locationText.length >= 3) {
        			if($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion"))
        				{
        			$.when(ACC.services.getTravelSectorsAjax()).then(function(data) {
        	    		ACC.farefinder.allTravelSectors=data.travelRoute;
        	    		ACC.farefinder.terminalcacheversion=data.terminalsCacheVersion;
        	    		localStorage.setItem("travelSector", JSON.stringify(ACC.farefinder.allTravelSectors));
        	    		localStorage.setItem("terminalCacheVersion", ACC.farefinder.terminalcacheversion);
        	    	});
        		}
        			else{
        				ACC.farefinder.allTravelSectors=JSON.parse(localStorage.getItem("travelSector"));
        			}
        		}
            },

    bindDealFinderLocationEvents: function () {
        // validation events bind for y_dealFinderOriginLocation field
		$(".y_dealFinderOriginLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_dealFinderOriginLocationCode").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				// select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a:eq( 1 )");
					firstSuggestion.click();
				}
				$(this).valid();

			};
		});

		$(".y_dealFinderOriginLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
            	$(".y_dealFinderOriginLocation").valid();
            }
        });

		$(".y_dealFinderOriginLocation").blur(function (e) {
			$(".y_dealFinderOriginLocation").valid();
		});

		$(".y_dealFinderOriginLocation").focus(function (e) {
			$(this).select();
		});

		// validation events bind for y_dealFinderDestinationLocation field
		$(".y_dealFinderDestinationLocation").keydown(function(e) {
			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_dealFinderDestinationLocation").val()==''){
				if((e.keyCode || e.which) == 13){
					e.preventDefault();
				}

				// select the first suggestion
				var suggestions = $(this).parent().find(".autocomplete-suggestions");
				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
					var firstSuggestion = suggestions.find("a:eq( 1 )");
					firstSuggestion.click();
				}
				$(this).valid();

			};
		});

		$(".y_dealFinderDestinationLocation").autosuggestion({
            suggestionFieldChangedCallback: function () {
            	if($(".y_dealFinderDestinationLocation").length==1)
            	{
            		$(".y_dealFinderDestinationLocation").valid();
            	}
            }
        });

		$(".y_dealFinderDestinationLocation").blur(function (e) {
			$(".y_dealFinderDestinationLocation").valid();
		});

		$(".y_dealFinderDestinationLocation").focus(function (e) {
			$(this).select();
		});
    },

    // Get All Travel Sectors
        bindDealTravelSectorsOriginAutoSuggest: function () {
            $(".y_dealFinderOriginLocation").autosuggestion({
                inputToClear: ".y_dealFinderDestinationLocation",
                autosuggestServiceHandler: function (locationText) {
                    var suggestionSelect = "#y_dealFinderOriginLocationSuggestions";
                    var travelSectors=ACC.farefinder.allTravelSectors;
            		var filterTravelSectors=ACC.farefinder.getTravelOriginMatches(travelSectors,locationText);
                	if(!$.isEmptyObject(filterTravelSectors)) {
                    	var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                    	var htmlStringEnd = '</ul> </div>';
                    	var htmlStringBody = "";
                    	var titleDisplayed = 0;
                    	var htmlStringTitle = "";
                    	$(suggestionSelect).html(htmlStringStart);

                    	for(var code in filterTravelSectors) {
                    		var filteredJson=filterTravelSectors[code];
                    		if(titleDisplayed == 0){
                    			htmlStringTitle = '</li>'+'<li class="child"> <a href="" class="autocomplete-suggestion" '+
                        		'data-code="'+filteredJson.origin.location.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + '</a> </li> ';
                    			titleDisplayed = 1;
                    		}
                    		else{
                    			htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" '+
                    		'data-code="'+filteredJson.origin.location.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + '</a> </li> ';
                    		}
                    	}
                    	var htmlContent = htmlStringStart + htmlStringTitle +htmlStringBody + htmlStringEnd;
                    	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
                	}
                	else{
                		var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                    	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
                	}
                },
                suggestionFieldChangedCallback: function () {
                    // clear the destination fields
                    $(".y_dealFinderDestinationLocation").val("");
                    $(".y_dealFinderDestinationLocationCode").val("");
                },
                attributes : ["Code", "SuggestionType"]
            });
        },
        // Suggestions/autocompletion for Destination location
        bindDealTravelSectorsDestinationAutoSuggest: function () {
            $(".y_dealFinderDestinationLocation").autosuggestion({
                autosuggestServiceHandler: function (destinationText) {
                    var suggestionSelect = "#y_dealFinderDestinationLocationSuggestions";
                    var originCode = $(".y_dealFinderOriginLocationCode").val().toLowerCase();

                    var travelSectors=ACC.farefinder.allTravelSectors;
                    if(travelSectors == null || travelSectors.length==0)
                    {
                    	ACC.farefinder.fireQueryToFetchAllTravelSectors(originCode);
                    	travelSectors=ACC.farefinder.allTravelSectors;
                    }
                    var matchedTravelSectors=ACC.dealfinder.getTravelSectorsWithOrigin(travelSectors,originCode);
            		var filterTravelSectors=ACC.dealfinder.getTravelDestinationMatches(matchedTravelSectors,destinationText);

            		if(!$.isEmptyObject(filterTravelSectors)) {
                    	var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
                    	var htmlStringEnd = '</ul> </div>';
                    	var htmlStringBody = "";
                    	titleDisplayed = 0;
                    	$(suggestionSelect).html(htmlStringStart);

                    	for(var code in filterTravelSectors) {
                    		var filteredJson=filterTravelSectors[code];
                    		if(titleDisplayed == 0 ){
                    			htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" '+
                         		'data-code="'+filteredJson.destination.location.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + '</a> </li> ';
                    			titleDisplayed = 1;
                    		}
                    		else{
                    		htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" date-routetype="'+filteredJson.routeType+'" '+
                     		'data-code="'+filteredJson.destination.location.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + '</a> </li> ';
                    		}
                     	}
                     	var htmlContent = htmlStringStart +  htmlStringBody + htmlStringEnd;
                     	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
            		}
            		else{
                		var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
                    	$(suggestionSelect).html(htmlContent);
                        $(suggestionSelect).removeClass("hidden");
                	}
                },
                attributes : ["Code", "SuggestionType"]
            });
        },

         getTravelSectorsWithOrigin: function(travelSectors, originCode) {
        		var matchingTravelSectors=[];
            	if(!$.isEmptyObject(travelSectors)) {
            		for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
            			var travelSector=travelSectors[travelSectorIndex];
            			if(travelSector.origin != null && travelSector.origin.location.code.toLowerCase()==originCode) {
            				matchingTravelSectors.push(travelSector);
            			}
            		}
            	}
            	return matchingTravelSectors;
        },

        getTravelDestinationMatches: function(travelSectors,locationText) {
            	locationText=locationText.toLowerCase();
        		var filterTravelSectors=new Object();

        		if(!$.isEmptyObject(travelSectors)) {
            		var matchingTravelSectors=[];
            		for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
            			var travelSector=travelSectors[travelSectorIndex];
            			if(travelSector.destination != null && travelSector.destination.location.code.toLowerCase()==locationText) {
            				matchingTravelSectors.push(travelSector);
            			}
            		}

            		if($.isEmptyObject(matchingTravelSectors)) {
                        for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++)
                        {
                            var travelSector=travelSectors[travelSectorIndex];
                            if( travelSector.destination != null && travelSector.destination.code.toLowerCase()==locationText)
                            {
                                matchingTravelSectors.push(travelSector);
                            }
                         }
                    }

            		if($.isEmptyObject(matchingTravelSectors)) {
        				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
        					var travelSector=travelSectors[travelSectorIndex];
        	    			if( travelSector.destination != null && travelSector.destination.location.name.toLowerCase().indexOf(locationText) != -1) {
        	    				matchingTravelSectors.push(travelSector);
        	    			}
        	    		}
            		}
        			if ($.isEmptyObject(matchingTravelSectors)) {
        				for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
        					var sector = travelSectors[travelSectorIdx];
        					if (sector.destination != null
        							&& (sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
        						matchingTravelSectors.push(sector);
        					}
        				}
        			}

            		if(!$.isEmptyObject(matchingTravelSectors))
            			filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0] ;
            	}

            	return filterTravelSectors;
            },
        
        bindDealDestinationAutosuggest: function () {
    		if(!$(".y_accommodationDealFinderLocation" ).is('input')){
    			return;
    		}
    		$(".y_accommodationDealFinderLocation").autosuggestion({
                //inputToClear: ".y_fareFinderDestinationLocation",
                autosuggestServiceHandler: function (locationText) {
                    var suggestionSelect = "#y_accommodationDealFinderLocationSuggestions";
                    // make AJAX call to get the suggestions
                    $.when(ACC.services.getSuggestedAccommodationLocationsAjax(locationText)).then(
                        // success
                        function (data, textStatus, jqXHR) {
                            $(suggestionSelect).html(data.htmlContent);
                            if (data.htmlContent) {
                                $(suggestionSelect).removeClass("hidden");
                            }
                        }
                    );
                },
                suggestionFieldChangedCallback: function () {
                	$(".y_accommodationDealFinderLocation").valid();
                },
                attributes : ["Code", "SuggestionType", "Latitude", "Longitude", "Radius"]
            });
    	},
    	
    	bindDealAccommodationFinderLocationEvents : function(){
    		if(!$(".y_accommodationDealFinderLocation").is('input')){
    			return;
    		}

    		$(".y_accommodationDealFinderLocation").keydown(function(e) {
    			if((e.keyCode == 9 || ((e.keyCode || e.which) == 13)) && $(".y_accommodationDealFinderLocationCode").val()==''){
    				if((e.keyCode || e.which) == 13){
    					e.preventDefault();
    				}

    				//select the first suggestion
    				var suggestions = $(this).parent().find(".autocomplete-suggestions");
    				if( $(this).val().length >= 3 && suggestions && suggestions.length != 0 ) {
    					var firstSuggestion = suggestions.find("a").first();
    					firstSuggestion.click();
    				}
    				$(this).valid();

    			};
    		});

    		$(".y_accommodationDealFinderLocation").blur(function (e) {
    			$(".y_accommodationDealFinderLocation").valid();
    		});

    		$(".y_accommodationDealFinderLocation").focus(function (e) {
    			$(this).select();
    		});
    	}

};
