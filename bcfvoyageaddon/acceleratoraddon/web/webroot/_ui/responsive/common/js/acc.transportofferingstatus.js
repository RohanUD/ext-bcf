ACC.transportofferingstatus = {
        
    _autoloadTracc : [
        "bindTransportofferingstatusValidationMessages",
        "bindTransportOfferingStatusForm",
        "bindTransportOfferingSearchForm",
        "bindTransportOfferingSearchFormVisibility",
        "enableCheckOtherFlights"
    ],
    
    componentParentSelector: '#y_nexttransportOfferingStatus',
    
    //  trip status form on Transport Offering Status page
    bindTransportofferingstatusValidationMessages : function () {
        ACC.TransportofferingstatusValidationMessages = new validationMessages("ferry-TransportofferingstatusValidationMessages");
        ACC.TransportofferingstatusValidationMessages.getMessages("error");
    },
    bindTransportOfferingStatusForm: function(){
        var yesterdayDate = new Date();
        yesterdayDate.setDate(yesterdayDate.getDate() - 1);
        
        $("#y_transportOfferingStatusForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            errorElement: "span",
            errorClass: "fe-error",
            onfocusout: function(element) { $(element).valid(); },
            submitHandler: function() {
                ACC.transportofferingstatus.refreshResultTable();
                var number = $("input[name='transportOfferingNumber']").val();
                var departureDate = $("input[name='departureDate']").val();
                if(number && departureDate){
                    ACC.travelcommon.changeUrlParams(["transportOfferingNumber", "departureDate"], [number, departureDate]);
                }
            },            
            rules: {
                transportOfferingNumber: "required",
                departureDate:{
                    required: true,
                    dateUK: true,
                    dateGreaterEqualTo: ACC.travelcommon.convertToUKDate(yesterdayDate)
                }
            },
            messages: {
                transportOfferingNumber: ACC.TransportofferingstatusValidationMessages.message('error.transportofferingstatus.transport.offering.number'),
                departureDate: ACC.TransportofferingstatusValidationMessages.message('error.transportofferingstatus.transport.departure.date')
            }
        });
        $(".y_transportOfferingStatusDepartureDate").datepicker({
            minDate: '-1d',
            onClose : function(selectedDate) {
                $(this).valid();
            }
        });
    },

    refreshResultTable: function() {
        var $form = $("#y_transportOfferingStatusForm");
        var jsonData = ACC.formvalidation.serverFormValidation($form, "refreshTransportOfferingStatusResults");
        if(!jsonData.hasErrorFlag){
            $("#y_transportOfferingStatusErrors").html("");
            $("#y_transportOfferingStatusResultTable").html(jsonData.htmlContent);
        }
    },
    
    //  trip status form on any page (other than the Transport Offering Status form)
    bindTransportOfferingSearchForm : function() {
        var yesterdayDate = new Date();
        yesterdayDate.setDate(yesterdayDate.getDate() - 1);
        
        $("#y_transportOfferingStatusSearchForm").validate({
            errorElement: "span",
            errorClass: "fe-error",
            onfocusout: function(element) { $(element).valid(); },
            submitHandler: function(form) {
                var jsonData = ACC.formvalidation.serverFormValidation(form, "validateTransportOfferingStatusForm");
                if(!jsonData.hasErrorFlag){
                    form.submit();
                }
            },
            rules: {
                transportOfferingNumber: "required",
                departureDate:{
                    required: true,
                    dateUK: true,
                    dateGreaterEqualTo: ACC.travelcommon.convertToUKDate(yesterdayDate)
                }
            },
            messages: {
                transportOfferingNumber: ACC.TransportofferingstatusValidationMessages.message('error.transportofferingstatus.transport.offering.number'),
                departureDate: ACC.TransportofferingstatusValidationMessages.message('error.transportofferingstatus.transport.departure.date')
            }
        });
    },
    
    bindTransportOfferingSearchFormVisibility: function(){
        $(".y_transportOfferingStatusSearchTrigger").click(function(e) {
            e.preventDefault();
            $(this).hide();
            
             $.ajax({
                    url : ACC.config.contextPath + "/view/TransportOfferingStatusSearchComponentController/get-transport-offering-status-search-form",
                    type: 'GET',
                    data : {
                        componentUid : $("#y_transportOfferingStatusSearchComponentId").val()
                    },                  
                    success: function(result){
                        var parentDiv=$(ACC.transportofferingstatus.componentParentSelector).parent();
                        $(ACC.transportofferingstatus.componentParentSelector).remove();
                        parentDiv.append(result);
                        ACC.transportofferingstatus.bindTransportOfferingStatusForm();
                        ACC.transportofferingstatus.bindTransportOfferingSearchForm();
                        $("#flight-status").addClass("in");
                        $(".y_flightStatusPanelHeaderLink").removeClass("collapsed");
                    }
                });
        });
    },

    enableCheckOtherFlights: function () {
        $(".y_checkOtherFlights").removeAttr("disabled");
    }

};
