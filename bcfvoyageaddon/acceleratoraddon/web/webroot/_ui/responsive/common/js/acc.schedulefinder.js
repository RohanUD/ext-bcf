ACC.schedulefinder = {
	_autoloadTracc: [
		"getAllTravelSectors",
		"init",

	],

	//componentDealParentSelector: '#y_scheduleFinderForm',


	init: function () {

		ACC.schedulefinder.bindScheduleTravelSectorsOriginAutoSuggest();
		ACC.schedulefinder.bindScheduleTravelSectorsDestinationAutoSuggest();
		ACC.schedulefinder.bindSchedulesFinderSubmitHandler();
	},

	getAllTravelSectors: function() {
		$(document).on("keyup", ".y_scheduleFinderOriginLocation", function(e) {
			// ignore the Enter key
			if ((e.keyCode || e.which) == 13) {
				return false;
			}

			var locationText = $.trim($(this).val());
			ACC.dealfinder.fireQueryToFetchAllTravelSectors(locationText);
		});
	},

	fireQueryToFetchAllTravelSectors: function(locationText){
		if(locationText.length >= 3) {
			if($("#y_terminalsCacheVersion").val() != localStorage.getItem("terminalCacheVersion"))
			{
				$.when(ACC.services.getTravelSectorsAjax()).then(function(data) {
					ACC.farefinder.allTravelSectors=data.travelRoute;
					ACC.farefinder.terminalcacheversion=data.terminalsCacheVersion;
					localStorage.setItem("travelSector", JSON.stringify(ACC.farefinder.allTravelSectors));
					localStorage.setItem("terminalCacheVersion", ACC.farefinder.terminalcacheversion);
				});
			}
			else{
				ACC.farefinder.allTravelSectors=JSON.parse(localStorage.getItem("travelSector"));
			}
		}
	},

	// Get All Travel Sectors
	bindScheduleTravelSectorsOriginAutoSuggest: function () {
		$(".y_scheduleFinderOriginLocation").autosuggestion({
			inputToClear: ".y_scheduleFinderDestinationLocation",
			autosuggestServiceHandler: function (locationText) {
				var suggestionSelect = "#y_scheduleFinderOriginLocationSuggestions";
				var travelSectors=ACC.farefinder.allTravelSectors;
				var filterTravelSectors=ACC.farefinder.getTravelOriginMatches(travelSectors,locationText);
				if(!$.isEmptyObject(filterTravelSectors)) {
					var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
					var htmlStringEnd = '</ul> </div>';
					var htmlStringBody = "";
					var titleDisplayed = 0;
					var htmlStringTitle = "";
					$(suggestionSelect).html(htmlStringStart);

					for(var code in filterTravelSectors) {
						var filteredJson=filterTravelSectors[code];
						if(titleDisplayed == 0){
							htmlStringTitle = ' <li class="parent"> <span class="title">'+
								' <a href="" class="autocomplete-suggestion inactiveLink" data-code= "'+filteredJson.origin.location.code+
								'" data-suggestiontype="'+filteredJson.origin.location.locationType+'" >'+ filteredJson.origin.location.name +' - ' + filteredJson.origin.location.superlocation[0].name +  '</a> </span> </li>'+'<li class="child"> <a href="" class="autocomplete-suggestion" '+
								'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + ' - '+filteredJson.origin.name+'('+ filteredJson.origin.code +')</a> </li> ';
							titleDisplayed = 1;
						}
						else{
							htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" '+
								'data-code="'+filteredJson.origin.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.origin.location.name + ' - '+filteredJson.origin.name+'('+ filteredJson.origin.code +')</a> </li> ';
						}
					}
					var htmlContent = htmlStringStart + htmlStringTitle +htmlStringBody + htmlStringEnd;
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
				else{
					var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
			},
			suggestionFieldChangedCallback: function () {
				// clear the destination fields
				$(".y_scheduleFinderDestinationLocation").val("");
				$(".y_scheduleFinderDestinationLocationCode").val("");
			},
			attributes : ["Code", "SuggestionType"]
		});
	},

	// Suggestions/autocompletion for Destination location
	bindScheduleTravelSectorsDestinationAutoSuggest: function () {
		$(".y_scheduleFinderDestinationLocation").autosuggestion({
			autosuggestServiceHandler: function (destinationText) {
				var suggestionSelect = "#y_scheduleFinderDestinationLocationSuggestions";
				var originCode = $(".y_scheduleFinderOriginLocationCode").val().toLowerCase();

				var travelSectors=ACC.farefinder.allTravelSectors;
				if(travelSectors == null || travelSectors.length==0)
				{
					ACC.farefinder.fireQueryToFetchAllTravelSectors(originCode);
					travelSectors=ACC.farefinder.allTravelSectors;
				}
				var matchedTravelSectors=ACC.farefinder.getTravelSectorsWithOrigin(travelSectors,originCode);
				var filterTravelSectors=ACC.farefinder.getTravelDestinationMatches(matchedTravelSectors,destinationText);

				if(!$.isEmptyObject(filterTravelSectors)) {
					var htmlStringStart = '<div class="autocomplete-suggestions"> <ul> ';
					var htmlStringEnd = '</ul> </div>';
					var htmlStringBody = "";
					titleDisplayed = 0;
					$(suggestionSelect).html(htmlStringStart);

					for(var code in filterTravelSectors) {
						var filteredJson=filterTravelSectors[code];
						if(titleDisplayed == 0 ){
							htmlStringBody = htmlStringBody + '<li class="parent"> <span class="title">'+
								' <a href="" class="autocomplete-suggestion inactiveLink" data-routetype="'+filteredJson.routeType+'" data-code= "'+filteredJson.destination.location.code+
								'" data-suggestiontype="'+filteredJson.destination.location.locationType+'" >'+ filteredJson.destination.location.name +' - ' + filteredJson.destination.location.superlocation[0].name +  '</a> </span> </li>' +'<li class="child"> <a href="" class="autocomplete-suggestion" '+
								'data-code="'+filteredJson.destination.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + ' - '+filteredJson.destination.name+'('+ filteredJson.destination.code +')</a> </li> ';
							titleDisplayed = 1;
						}
						else{
							htmlStringBody = htmlStringBody + '<li class="child"> <a href="" class="autocomplete-suggestion" date-routetype="'+filteredJson.routeType+'" '+
								'data-code="'+filteredJson.destination.code+'" data-suggestiontype="AIRPORTGROUP">'+ filteredJson.destination.location.name + ' - '+filteredJson.destination.name+'('+ filteredJson.destination.code +')</a> </li> ';
						}
					}
					var htmlContent = htmlStringStart +  htmlStringBody + htmlStringEnd;
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
				else{
					var htmlContent = '<div class="autocomplete-suggestions no-matching-airports"><ul><li>No matching terminals</li></ul></div>';
					$(suggestionSelect).html(htmlContent);
					$(suggestionSelect).removeClass("hidden");
				}
			},
			attributes : ["Code", "SuggestionType"]
		});
	},

	getTravelSectorsWithOrigin: function(travelSectors, originCode) {
		var matchingTravelSectors=[];
		if(!$.isEmptyObject(travelSectors)) {
			for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
				var travelSector=travelSectors[travelSectorIndex];
				if(travelSector.origin != null && travelSector.origin.location.code.toLowerCase()==originCode) {
					matchingTravelSectors.push(travelSector);
				}
			}
		}
		return matchingTravelSectors;
	},

	getTravelDestinationMatches: function(travelSectors,locationText) {
		locationText=locationText.toLowerCase();
		var filterTravelSectors=new Object();

		if(!$.isEmptyObject(travelSectors)) {
			var matchingTravelSectors=[];
			for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
				var travelSector=travelSectors[travelSectorIndex];
				if(travelSector.destination != null && travelSector.destination.location.code.toLowerCase()==locationText) {
					matchingTravelSectors.push(travelSector);
				}
			}

			if($.isEmptyObject(matchingTravelSectors)) {
				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++)
				{
					var travelSector=travelSectors[travelSectorIndex];
					if( travelSector.destination != null && travelSector.destination.code.toLowerCase()==locationText)
					{
						matchingTravelSectors.push(travelSector);
					}
				}
			}

			if($.isEmptyObject(matchingTravelSectors)) {
				for(var travelSectorIndex=0; travelSectorIndex<travelSectors.length;travelSectorIndex++) {
					var travelSector=travelSectors[travelSectorIndex];
					if( travelSector.destination != null && travelSector.destination.location.name.toLowerCase().indexOf(locationText) != -1) {
						matchingTravelSectors.push(travelSector);
					}
				}
			}
			if ($.isEmptyObject(matchingTravelSectors)) {
				for (var travelSectorIdx = 0; travelSectorIdx < travelSectors.length; travelSectorIdx++) {
					var sector = travelSectors[travelSectorIdx];
					if (sector.destination != null
						&& (sector.destination.location.code.toLowerCase() == locationText || sector.destination.location.name.toLowerCase().includes(locationText))) {
						matchingTravelSectors.push(sector);
					}
				}
			}

			if(!$.isEmptyObject(matchingTravelSectors))
				filterTravelSectors[matchingTravelSectors[0].origin.code] = matchingTravelSectors[0] ;
		}

		return filterTravelSectors;
	},
	bindSchedulesFinderSubmitHandler : function()
	{
		$(document).on('click', '#scheduleFinderFindButton', function(event) {
			event.preventDefault();
			var targetURL = $('#y_scheduleFinderForm').attr('action') + $("#y_scheduleDepartureLocationTravelroute").html()+"-"+$("#y_scheduleArrivalLocationTravelroute").html()+ "/" + $(".schedule-finder-source-location-code").text() + "-" + $(".schedule-finder-destination-location-code").text();
			if(scheduleLandingValidation()){
				$('#y_scheduleFinderForm').attr('action', targetURL);
				$('#y_scheduleFinderForm').submit();
			}
		});
	}



};

function scheduleLandingValidation(){
	if($("#fromLocationDropDown").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")){
		$("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.fromTerminal"));
		return false;
	}

	if($("#toLocationDropDown").find(".dropdown-text strong").text() == ACC.validationMessages.message("label.ferry.farefinder.dropdown.default.heading")){
		$("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.toTerminal"));
		return false;
	}


	if($("#scheduleDate").val()==""){
		$("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.DepDate"));
		return false;
	}

	if(!validateDateinput($("#scheduleDate").val(),$("#datepicker-schedule").datepicker("getDate"),"mm/dd/yy")){
		$("body").trigger("show-error",ACC.validationMessages.message("error.ferry.farefinder.bookasailing.validation.wrongDateDep"));
		return false;
	}

	return true;
}
