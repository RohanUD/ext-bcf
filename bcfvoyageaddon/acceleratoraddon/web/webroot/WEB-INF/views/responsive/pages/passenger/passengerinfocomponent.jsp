<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
   <modifybooking:editBookingHeaderBar/>
   <progress:fareFinderProgressBar stage="selectpassenger" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
   <modifybooking:originalBookingDetails/>
   <summary:ferryJourneyTripSummary/>
   <c:url var="nextPageUrl" value="/view/FareFinderComponentController/next-step" />
   <div class="container margin-top-40">
      <c:set var="formPrefix" value="" />
      <div class="alert alert-danger form-error-messages hidden">
      </div>
      <form:form commandName="bcfFareFinderForm" action="${fn:escapeXml(nextPageUrl)}" method="POST" class="fe-validate form-background form-booking-trip" id="y_fareFinderForm">
         <passenger:passengerselection formPrefix="${formPrefix}" />
         <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12 mb-30">
               <div id="proceed_to_vehicle_selection_btn_div" class="${bcfFareFinderForm.routeInfoForm.walkOnRoute ? 'hidden' : '' }">
                  <button class="btn btn-primary find-button btn-block light-blue-btn" type="submit" id="fareFinderFindButton" value="Submit">
                     <spring:theme code="text.ferry.farefinder.passengerinfo.proceed.to.vehicle.selection.page" text="CONFIRM AND ADD VEHICLE" />
                  </button>
               </div>
               <div class="hidden" id="proceed_to_sailing_selection_btn_div" class="${bcfFareFinderForm.routeInfoForm.walkOnRoute ? '' : 'hidden' }">
                  <button class="btn btn-primary find-button btn-block light-blue-btn" type="submit" id="fareFinderFindButton" value="Submit">
                     <spring:theme code="text.ferry.farefinder.proceed.to.sailing.selection.page" text="CONFIRM AND FIND SAILING" />
                  </button>
               </div>
            </div>
         </div>
      </form:form>
   </div>
   <modifybooking:abortCancellation/>
</div>
