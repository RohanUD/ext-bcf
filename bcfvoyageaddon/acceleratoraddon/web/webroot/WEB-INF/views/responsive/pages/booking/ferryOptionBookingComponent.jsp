<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
	<modifybooking:editBookingHeaderBar />
	<progress:fareFinderProgressBar stage="confirmOptional" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
	<modifybooking:originalBookingDetails />
	<input type="hidden" value="${component.uid}" id="y_bcfGlobalReservationComponentId" />
	<cms:pageSlot position="Reservation" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>
</div>
<div class="container">
	<div class="row my-5">
		<div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
			<a href="/checkout/ferry-option-booking-details/confirm" class="btn btn-block btn-primary">
				<spring:theme code="text.optional.booking.form.submit.button" text="Submit request" />
			</a>
		</div>
	</div>
</div>
