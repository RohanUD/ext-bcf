<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<json:object escapeXml="false">

	<json:property name="replanJourneyModalHtml">
		<div class="modal fade" id="y_replanJourneyModal" tabindex="-1"
			role="dialog" aria-labelledby="replanJourneyLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
						</button>
						<h3 class="modal-title" id="replanJourneyLabel">
							<spring:theme
								code="text.page.managemybooking.replan.journey.modal.title"
								text="Replan Journey" />
						</h3>
					</div>
					<c:choose>
					<c:when test="${hasErrorFlag}">
						<p>
							<spring:message code="${errorMsg}" />
						</p>
					</c:when>
					<c:otherwise>					
					<div class="modal-body">

						<c:url var="fareFinderUrl" value="/manage-booking/replan-journey" />
						<c:set var="formPrefix" value="" />
						<c:set var="idPrefix" value="fare" />
						<form:form modelAttribute="fareFinderForm"
							action="${fn:escapeXml(fareFinderUrl)}" method="POST"
							id="y_fareFinderFormManageBooking">

							<div class="row">
							<div class="input-required-wrap">
								<label>Sailing By
									<div><h4>${fn:escapeXml(SailingBy)}</h4></div>
									</label>
									<div class="input-required-wrap">
									<div class="col-xs-6">
									<label for="fromLocation" class="home-label-mobile">
									<spring:theme code="text.cms.farefinder.departure.location.placeholder" text="From" />
									<h4>${fn:escapeXml(DepartDetails)}</h4></div>
									</label>
									<div class="col-xs-6">
									<label>Arrival Time
									<h4>${fn:escapeXml(ArrivalTime)}</h4>
									</label></div>
									</div>
							
							</div>
							</div>
							<div class="row">
								<div class="input-required-wrap col-xs-12 col-sm-4">
									<label for="toLocation" class="home-label-mobile"> TO <spring:theme
											var="arrivalLocationPlaceholderText"
											code="text.cms.farefinder.arrival.location.placeholder"
											text="To" />
									</label>
									<form:hidden
										path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocation"
										class="y_fareFinderOriginLocationCode" />
									<form:input type="text" id="toLocation"
										path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocationName"
										cssErrorClass="fieldError"
										class="col-xs-12 y_fareFinderDestinationLocation input-grid form-control"
										placeholder="${fn:escapeXml(arrivalLocationPlaceholderText)}"
										autocomplete="off" />
										<form:hidden  id="fromLocation"
                                        path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocationName"
                                        cssErrorClass="fieldError"
                                        class="col-xs-12 y_fareFinderDepartureLocation input-grid form-control"
                                        placeholder="${fn:escapeXml(arrivalLocationPlaceholderText)}"
                                        autocomplete="off" />
									<form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocation"
										class="y_fareFinderDestinationLocationCode" />

                                    <form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.routeType" class="y_fareFinderDestinationLocationCode" />

									<form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocationSuggestionType" class="y_fareFinderArrivalLocationSuggestionType" />
									<form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocationSuggestionType" class="y_fareFinderDestinationLocationSuggestionType" />

                                    <form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.returnDateTime" class="y_transportArrivalDate" />
  	                                <form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.tripType" class="y_tripType" />

									<div id="y_fareFinderDestinationLocationSuggestions"
										class="autocomplete-suggestions-wrapper hidden"></div>
								</div>

								<div class="input-required-wrap  col-xs-12 col-sm-4"
									id="js-return">
									<label for="departingDateTime" class="home-label-mobile">Departure
										<spring:theme var="departingDatePlaceholderText"
											code="text.cms.farefinder.departure.date.placeholder"
											text="Departure Date" />
									</label>
									<form:input type="text"
										path="${fn:escapeXml(formPrefix)}routeInfoForm.departingDateTime"
										class="col-xs-12 datepicker input-grid form-control y_${fn:escapeXml( idPrefix )}FinderDatePickerDeparting y_transportDepartDate"
										placeholder="${fn:escapeXml(departingDatePlaceholderText)}"
										autocomplete="off" />


									<input type="hidden" id="selectedJourneyRefNumber"
										name="selectedJourneyRefNumber"
										value="${selectedJourneyRefNumber}" /> <input type="hidden"
										id="odRefNum" name="odRefNum" value="${odRefNum}" /> <input
										type="hidden" value="${terminalsCacheVersion}"
										id="y_terminalsCacheVersion" />
								</div>
							</div>
							<!-- <div class="modal-footer"> -->
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<button
										class="btn btn-secondary  mt-1"
										type="submit" value="Submit">Replan</button>
								
									<button class="btn btn-secondary mt-1"
										data-dismiss="modal">
										<spring:theme
											code="text.page.managemybooking.cancel.booking.close"
											text="No Thanks" />
									</button>
								</div>
							</div>
							<!-- </div> -->
						</form:form>
					</div>
					</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</json:property>
</json:object>
