<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>
<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl" />
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl" />
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<c:url var="nextUrl" value="/cart/summary/next" />
	<c:url var="saveQuoteUrl" value="/quote/create" />
	<div class="container">
		<div class="row">
			<div class="white-bg">
				<div class="col-sm-10 offset-1 js-main-box">
					<cms:pageSlot position="MiddleContent" var="feature" element="div">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
		<div class="modal fade" id="fareFinderModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="white-bg">
									<div class="modify-search panel panel-primary">
										<div class="fieldset-inner-wrapper">
											<cms:pageSlot position="BottomContent" var="feature" element="div">
												<cms:component component="${feature}" />
											</cms:pageSlot>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3 mt-3 pull-right">
			<a href="${fn:escapeXml(nextUrl)}" class="btn btn-primary find-button  mb-4  col-sm-10">
				<spring:theme code="text.fareselection.button.continue" text="Continue" />
			</a>
		</div>
	</div>
	<booking:cancelBookingResponse />
</template:page>
