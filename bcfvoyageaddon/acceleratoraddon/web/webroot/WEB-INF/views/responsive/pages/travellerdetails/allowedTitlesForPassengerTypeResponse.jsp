<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="travellerForm" value="travellerForms[0]" />
<json:object escapeXml="false">
	<json:property name="htmlContent">
		<select id="y_passengerTitleSelect" name="travellerForms[0].passengerInformation.title" class="y_passengerTitle form-control valid" aria-invalid="false">
			<option selected="selected" value="">Select</option>
			<c:forEach var="travellerTitleItem" items="${travellerTitles}" varStatus="travellerTitleIdx">
				<option id="allowedTitle" value="${fn:escapeXml(travellerTitleItem.code)}">${fn:escapeXml(travellerTitleItem.name)}</option>
			</c:forEach>
		</select>
	</json:property>
</json:object>
