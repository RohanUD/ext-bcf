<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="js-next-page-url hidden">${contextPath}${fn:escapeXml(nextURL)}</div>
<c:if test="${not empty isFareFinderComponent && isFareFinderComponent == 'true'}">
    <div class="container margin-top-40"><b><spring:theme code="text.ferry.calculator.summary.fare.calculator" text="Fare Calculator"/></b></div>
</c:if>
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
	<modifybooking:editBookingHeaderBar/>
	    <c:if test="${not empty isFareFinderComponent && isFareFinderComponent == 'false'}">
        <progress:fareFinderProgressBar stage="Sailings" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
    </c:if>
<modifybooking:originalBookingDetails/>
<%-- <progress:bookingProgressBar stage="flights" bookingJourney="${bookingJourney}"/> --%>
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<fareselection:fareCalculatorSailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />
<c:if test="${not empty tripType && tripType eq 'RETURN' && isReturn && !isModifyingTransportBooking}">
	<div class="green-bar-msg">
		<span class="bcf bcf-icon-checkmark"></span>
		<spring:theme code="text.packageferryselection.departure.selected" text="Departure sailing selection" />
		&nbsp;<strong><spring:theme code="text.packageferryselection.departure.confirmed" text="confirmed!" /></strong>
	</div>
</c:if>
<div class="y_fareSelectionPage">
	<c:if test="${isReturn == 'false'}">
		<c:set var="tripTypeLabelCode" value="label.fareselection.departure" />
	</c:if>
	<c:if test="${isReturn == 'true'}">
		<c:set var="tripTypeLabelCode" value="label.fareselection.arrival"></c:set>
	</c:if>
		<c:choose>
			<c:when test="${not empty fareSelection && not empty fareSelection.pricedItineraries}">
				<div class="border-blue-bottom">
					<div class="container">
						<div class="row">
							<div class="white-bg">
								<div class="col-md-12 col-lg-12">
									<input type="hidden" id="y_tripType" value="${fn:escapeXml(tripType)}" />
									<h2>
										<a href="${fareCalculatorPageUrl}" class="edit_FareCalculator">
											<span class="edit-icon"></span>
										</a>
									</h2>
									<c:if test="${isReturn == 'true' && !isModifyingTransportBooking}">
                                     <ferryselection:transportselection type="OutBound" />
                                    </c:if>
									<h4>
										<b><spring:theme code="${tripTypeLabelCode}" /></b>
									</h4>
									<p class="sailing-italic-text">
										<spring:theme code="label.packageferryselection.fareselection.lastupdated" /> ${dateNow}
									</p>
									<c:if test="${fareSelection.calenderSailingRange ne null}">
										<fareselection:calender fareSelection="${fareSelection}" />
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right mt-5">
                            <div class="p-icon-box time-tabs-section">
                            <c:if test="${routeType ne 'LONG'}">
                                <div class="p-icon time-tab active">
                                    <span><input type="radio" name="sailingTime" value="All"> All
                                    </span>
                                </div>
                                <div class="p-icon time-tab">
                                    <span> <input type="radio" name="sailingTime" value="am"> am
                                    </span>
                                </div>
                                <div class="p-icon time-tab">
                                    <span> <input type="radio" name="sailingTime" value="pm"> pm
                                    </span>
                                </div>
                            </c:if>
                            </div>
                        </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <c:if test="${isReturn == 'false'}">
                            <div id="y_outbound" class="y_fareResultTabWrapper  clearfix">
                                <fareselection:offeringList fareSelection="${fareSelection}" refNumber="${odRefNum}" />
                            </div>
                        </c:if>
                        <c:if test="${isReturn == 'true'}">
                            <div id="y_inbound" class="y_fareResultTabWrapper clearfix">
                                <fareselection:offeringList fareSelection="${fareSelection}" refNumber="${odRefNum}" />
                            </div>
                        </c:if>
                   </div>
			  </div>
			</div>
			</c:when>
			<c:otherwise>
				<section>
				    <div>
				        <div class="container waitlist-wrapper bg-gray text-center padding-top-30 padding-bottom-30 margin-bottom-40">
                            <div class="row">
                                  <div class="col-md-offset-3 col-md-6 col-xs-12">
                                      <span class="bcf bcf-icon-notice-outline bcf-2x"></span><br />
                                      <p class="sailing-info-text text-sm">
                                      <c:forEach var="errorMessage" items="${errorMessages}">
                                        <b><spring:theme text="${errorMessage}" /></b>
                                      </c:forEach>
									</p>
                                  
                                        <c:if test="${routeType eq 'LONG'}">
                                         <p class="sailing-info-text text-sm">
                                                <spring:theme code="label.listsailing.get.on.waitlist.description" text="Click below to get on the waitlist" />
										 </p>
                                            <button class="btn btn-secondary btn-block margin-bottom-20">
                                                <spring:theme code="label.listsailing.get.on.waitlist.header" text="Get on waitlist" />
                                            </button>
                                        </c:if>
                                  </div>
                            </div>
                        </div>
					</div>
					


				</section>
				<c:if test="${routeType eq 'LONG'}">
				<div class="container">
					<c:if test="${not empty nextAvailabilityDates}">
						<div class="row">
							<div class="col-md-offset-4 col-md-4 col-xs-12 mt-4 mb-4 text-center padding-top-30
							padding-bottom-30">
								<h5><spring:theme code="label.listsailing.next.available.sailing.dates" />:</h5>
							</div>
						</div>
					</c:if>
					<c:forEach var="nextAvailabilityDate" items="${nextAvailabilityDates}">
						<div class="row">
							<div class="col-md-offset-4 col-md-4 col-xs-12">
								<div class="btn btn-primary btn-block mb-5" id="min_price_for_date">
									<fmt:formatDate var="hiddenDate" pattern="yyyy-MM-dd" value="${nextAvailabilityDate}" />
									<div id="hiddenDate" hidden="hidden">${hiddenDate}</div>
										<fmt:formatDate pattern="EEEEE, MMMMM dd, yyyy" value="${nextAvailabilityDate}" />
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-offset-4 col-md-4 col-xs-12 margin-top-30 ">
							<a href="#"><spring:theme code="label.listsailing.view.schedules" text="View schedule" /></a>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-offset-4 col-md-4 col-xs-12 margin-top-30">
							<p class="sailing-italic-text">
								<spring:theme code="label.packageferryselection.fareselection.lastupdated" />&nbsp;${dateNow}
							</p>
						</div>
					</div>
				</div>
				</c:if>

			</c:otherwise>
		</c:choose>
		<div class="container">
		<c:if test="${not empty priceDisplayPassengerType and not empty fareSelection.pricedItineraries[0].itineraryPricingInfos[0]}">
			<div class="alert alert-warning" role="alert">
				<p>
					<c:forEach var="ptcFareBreakdownData" items="${fareSelection.pricedItineraries[0].itineraryPricingInfos[0].ptcFareBreakdownDatas}" varStatus="ptcIdx">
						<c:if test="${priceDisplayPassengerType==ptcFareBreakdownData.passengerTypeQuantity.passengerType.code}">
							<spring:theme code="text.fare.selection.disclaimer" arguments="${fn:escapeXml(ptcFareBreakdownData.passengerTypeQuantity.passengerType.name)}" />
						</c:if>
					</c:forEach>
				</p>
			</div>
		</c:if>
		<div class="row">
			<cms:pageSlot position="LeftContent" var="feature" element="section">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		<div class="row">
			<div class="col-md-3 col-xs-12 hidden">
				<a href="${fn:escapeXml(accommodationSearchUrl)}" class="btn btn-primary accommodation-search  sr-only
				mb-5">
					<spring:theme code="text.fareselection.button.hotel.book" text="Book Hotel" />
				</a>
			</div>
			<c:if test="${not bcfFareFinderForm.readOnlyResults eq 'true'}">
				<div class="col-md-offset-4 col-md-4 col-xs-12 ${fn:length(fareSelection.pricedItineraries) == 0 && bookingJourney eq 'BOOKING_TRANSPORT_ONLY' ? '' : 'hidden'}">
					<a href="${contextPath}/fare-selection/clear-session-form" class="btn btn-primary find-button btn-block mb-5 enabled">
						<spring:theme code="text.fareselection.button.backtohomepage" text="backtohomepage" />
					</a>
				</div>
			</c:if>
		</div>
    </div>
</div>
<fareselection:compareFares />
<reservation:fullReservationOverlay />
<fareselection:addBundleToCartValidationModal />
<fareselection:fareSelectionValidationModal />
<modifybooking:abortCancellation/>
</div>
