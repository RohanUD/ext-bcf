<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${(isModifyingTransportBooking and cmsPage.uid eq 'RouteSelectionPage') ? 'panel panel-primary modify-sailing-panel' : ''}">
<c:if test="${cmsPage.uid eq 'RouteSelectionPage'}">
	<modifybooking:editBookingHeaderBar/>
	<progress:fareFinderProgressBar stage="selectroute" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
	<modifybooking:originalBookingDetails/>
</c:if>
<div class="${cmsPage.uid eq 'RouteSelectionPage' ? 'container' : ''}">
<div id="y_currentSalesChannel" class="hidden">${salesChannel}</div>
<div class="flow-tab--heading ${cmsPage.uid eq 'RouteSelectionPage' ? 'hidden' : ''}">
  <ul class="list-inline flow-tab--list">
    <c:forEach items="${links}" var="link">
      <li class="tabs">
	    <cms:component component="${link}" evaluateRestriction="true" />
      </li>
    </c:forEach>

  </ul>
</div>
<c:url var="nextPageUrl" value="/view/FareFinderComponentController/PassengerInfo" />
<div id="tabs-1" class="box-1">
  <form:form commandName="bcfFareFinderForm" action="${fn:escapeXml(nextPageUrl)}" method="POST" class="farefinderform-custom fe-validate form-background form-booking-trip" id="y_fareFinderForm">
    <div>
        <c:set var="formPrefix" value="" />
        <farefinder:routeInfo routeInfoForm="${bcfFareFinderForm.routeInfoForm}" formPrefix="${formPrefix}" idPrefix="fare" />
        <input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_farefinderComponentId" /> <input type="hidden" value="${terminalsCacheVersion}" id="y_terminalsCacheVersion" />
        <div class="container y_passengers"></div>
        <%-- CTA (Search Flights) --%>

    </div>
    <div class="row">
      <div class="col-md-12 text-right">
 <div class="returnLabel mr-20" id="change_div_return_label">
        <a href="javascript:;" id="seasonalSchedulesLink"> <spring:theme code="label.ferry.farefinder.routeinfo.triptype.view.seasonal.schedules" text="View Seasonal Schedules" /></a>
        </div>
        <div class="returnLabel" id="change_div_return_label">
            <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package" text="Make it a package & save!" />&nbsp;&nbsp;
            <c:choose>
                <c:when test="${cmsPage.uid eq 'RouteSelectionPage'}">
                    <a href="${contextPath}/vacations"> <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package.add.hotel" text="Add a hotel" /></a>
                </c:when>
                <c:otherwise>
                    <a href="javascript:;" id="addAHotel"> <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package.add.hotel" text="Add a hotel" /></a>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="oneWayLabel sr-only" id="change_div_oneway_label">
          <h5 class="text-center">
          </h5>
        </div>
        <div class="multiSailingLabel sr-only" id="change_div_multisailing_label">
          <h5 class="text-center">
            <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package" text="Make it a package & save!" />&nbsp;&nbsp;
            <c:choose>
                <c:when test="${cmsPage.uid eq 'RouteSelectionPage'}">
                    <a href="${contextPath}/vacations"> <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package.add.hotel" text="Add a hotel" /></a>
                </c:when>
                <c:otherwise>
                    <a href="javascript:;" id="addAHotel"> <spring:theme code="label.ferry.farefinder.routeinfo.triptype.package.add.hotel" text="Add a hotel" /></a>
                </c:otherwise>
            </c:choose>
          </h5>
        </div>
      </div>
    </div>
    <%--  / CTA (Search Flights) --%>
  </form:form>
</div>
<c:if test="${hideFinderTitle eq true}">
</c:if>
</div>
<modifybooking:abortCancellation/>
</div>
