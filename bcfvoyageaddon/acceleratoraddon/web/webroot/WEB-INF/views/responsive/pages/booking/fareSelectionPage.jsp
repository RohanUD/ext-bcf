<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="js-next-page-url hidden">${contextPath}${fn:escapeXml(nextURL)}</div>
<c:if test="${not empty isFareFinderComponent && isFareFinderComponent == 'true'}">
    <div class="container margin-top-40"><b><spring:theme code="text.ferry.calculator.summary.fare.calculator" text="Fare Calculator"/></b></div>
</c:if>
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
	<modifybooking:editBookingHeaderBar/>
	    <c:if test="${not empty isFareFinderComponent && isFareFinderComponent == 'false'}">
        <progress:fareFinderProgressBar stage="Sailings" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
    </c:if>
<modifybooking:originalBookingDetails/>
<%-- <progress:bookingProgressBar stage="flights" bookingJourney="${bookingJourney}"/> --%>
<input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
<c:choose>
    <c:when test="${not empty isFareFinderComponent && isFareFinderComponent == 'true'}">
        <fareselection:fareCalculatorSailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />
    </c:when>
    <c:otherwise>
        <fareselection:SailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />
    </c:otherwise>
</c:choose>
<c:if test="${not empty tripType && tripType eq 'RETURN' && isReturn && !isModifyingTransportBooking}">
	<div class="green-bar-msg">
		<span class="bcf bcf-icon-checkmark"></span>
		<spring:theme code="text.packageferryselection.departure.selected" text="Departure sailing selection" />
		&nbsp;<strong><spring:theme code="text.packageferryselection.departure.confirmed" text="confirmed!" /></strong>
	</div>
</c:if>
<cart:cartTimer />
<div class="y_fareSelectionPage">
	<c:if test="${isReturn == 'false'}">
		<c:set var="tripTypeLabelCode" value="label.fareselection.departure" />
	</c:if>
	<c:if test="${isReturn == 'true'}">
		<c:set var="tripTypeLabelCode" value="label.fareselection.arrival"></c:set>
	</c:if>
    <c:choose>
        <c:when test="${routeType eq 'LONG' && showWaitlistLink eq true}">
            <fareselection:waitListForm/>
			<fareselection:nextAvailableSelectionDates/>
        </c:when>
        <c:otherwise>
		<c:choose>
			<c:when test="${not empty fareSelection && not empty fareSelection.pricedItineraries}">
				<div class="border-blue-bottom">
					<div class="container">
						<div class="row">
							<div class="white-bg">
								<div class="col-md-12 col-lg-12">
									<input type="hidden" id="y_tripType" value="${fn:escapeXml(tripType)}" />
									<h2>
										<a href="#" class="edit_journey_fare_selection">
											<span class="edit-icon"></span>
										</a>
									</h2>
									<c:if test="${isReturn == 'true' && !isModifyingTransportBooking}">
                                     <ferryselection:transportselection type="OutBound" />
                                    </c:if>
									<div class="margin-top-40 margin-bottom-40">
										<h4>
											<b><spring:theme code="${tripTypeLabelCode}" /></b>
										</h4>
                                        <fmt:formatDate value="${dateNow}" pattern="h:mm a, EEE MMM dd, yyyy" var="currentDate"/>
										<p class="sailing-italic-text mt-0">
											<spring:theme code="label.packageferryselection.fareselection.lastupdated" />&nbsp;${currentDate}
										</p>
									</div>
									<c:if test="${fareSelection.calenderSailingRange ne null}">
										<fareselection:calender fareSelection="${fareSelection}" />
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
                    <div class="row margin-top-20">
                        <div class="col-md-6">
							<h4><spring:theme code="text.listsailing.sailing.options" text="Sailing options" /></h4>
						</div>
						<div class="col-md-6 text-right mt-5">
								<div class="p-icon-box time-tabs-section">
								<c:if test="${routeType ne 'LONG'}">
									<div class="p-icon time-tab active">
										<span><input type="radio" name="sailingTime" value="All"> All
										</span>
									</div>
									<div class="p-icon time-tab">
										<span> <input type="radio" name="sailingTime" value="am"> am
										</span>
									</div>
									<div class="p-icon time-tab">
										<span> <input type="radio" name="sailingTime" value="pm"> pm
										</span>
									</div>
								</c:if>
								</div>
							</div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <c:if test="${isReturn == 'false'}">
                            <div id="y_outbound" class="y_fareResultTabWrapper  clearfix">
                                <fareselection:offeringList fareSelection="${fareSelection}" refNumber="${odRefNum}" />
                            </div>
                        </c:if>
                        <c:if test="${isReturn == 'true'}">
                            <div id="y_inbound" class="y_fareResultTabWrapper clearfix">
                                <fareselection:offeringList fareSelection="${fareSelection}" refNumber="${odRefNum}" />
                            </div>
                        </c:if>
                   </div>
			  </div>
			</div>
			</c:when>
			<c:otherwise>
                <fareselection:waitListForm/>
				<c:if test="${routeType eq 'LONG'}">
				    <fareselection:nextAvailableSelectionDates/>
				</c:if>

			</c:otherwise>
		</c:choose>
		</c:otherwise>
		</c:choose>
		<div class="container">
		<c:if test="${not empty priceDisplayPassengerType and not empty fareSelection.pricedItineraries[0].itineraryPricingInfos[0]}">
			<div class="alert alert-warning" role="alert">
				<p>
					<c:forEach var="ptcFareBreakdownData" items="${fareSelection.pricedItineraries[0].itineraryPricingInfos[0].ptcFareBreakdownDatas}" varStatus="ptcIdx">
						<c:if test="${priceDisplayPassengerType==ptcFareBreakdownData.passengerTypeQuantity.passengerType.code}">
							<spring:theme code="text.fare.selection.disclaimer" arguments="${fn:escapeXml(ptcFareBreakdownData.passengerTypeQuantity.passengerType.name)}" />
						</c:if>
					</c:forEach>
				</p>
			</div>
		</c:if>
		<div class="row">
			<cms:pageSlot position="LeftContent" var="feature" element="section">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		<div class="row">
			<div class="col-md-3 col-xs-12 hidden">
				<a href="${fn:escapeXml(accommodationSearchUrl)}" class="btn btn-primary accommodation-search  sr-only
				mb-5">
					<spring:theme code="text.fareselection.button.hotel.book" text="Book Hotel" />
				</a>
			</div>
			<c:if test="${not bcfFareFinderForm.readOnlyResults eq 'true'}">
				<div class="col-md-offset-4 col-md-4 col-xs-12 ${fn:length(fareSelection.pricedItineraries) == 0 && bookingJourney eq 'BOOKING_TRANSPORT_ONLY' ? '' : 'hidden'}">
					<a href="${contextPath}/fare-selection/clear-session-form" class="btn btn-primary find-button btn-block mb-5 enabled">
						<spring:theme code="text.fareselection.button.backtohomepage" text="backtohomepage" />
					</a>
				</div>
			</c:if>
		</div>
    </div>
</div>
<fareselection:compareFares />
<reservation:fullReservationOverlay />
<fareselection:addBundleToCartValidationModal />
<fareselection:fareSelectionValidationModal />
<modifybooking:abortCancellation/>
</div>
