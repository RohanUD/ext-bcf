<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fareFinderProgress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="travellerdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/travellerdetails"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>
<c:url var="submitUrl" value="/traveller-details" />
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
<modifybooking:editBookingHeaderBar/>
<progress:bookingProgressBar stage="personalDetails" bookingJourney="${bookingJourney}" />
<fareFinderProgress:fareFinderProgressBar stage="passengerInfo" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
<modifybooking:originalBookingDetails/>
<fareselection:SailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />
<div class="container panel-details">
	<div class="margin-reset clearfix">
		<form:form id="y_travellerForms" modelAttribute="bcfTravellersData" action="${submitUrl}" method="POST">
			<div class="global-alerts">
				<c:set var="formErrors">
					<form:errors path="*" />
				</c:set>
				<c:if test="${not empty formErrors}">
					<div class="alert alert-danger">
						<span> <spring:theme code="error.travellerdetails.form.error.globalmessage" text="Please make sure all fields are filled." />
						</span>
					</div>
				</c:if>
			</div>
			<div class="row margin-top-40 margin-bottom-20">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mb-2">
				<h4 class="margin-bottom-20">
					<spring:theme code="text.travellerdetails.passengers.header" text="Passenger manifest details" />
				</h4>
				<p>
					<spring:theme code="text.travellerdetails.passengers.header.description" text="Northern route information" />
				</p>
				<a href="#acceptableFormsLink" data-toggle="modal" data-target="#acceptableFormsModal">
					<spring:theme code="text.travellerdetails.acceptable.id.cards.link" text="Please see the list of acceptable forms of ID" />
                   <span class="bcf bcf-icon-info-solid"></span>
				</a>
				
			</div>
			</div>
			<c:choose>
				<c:when test="${not empty bcfTravellersData.travellerDataPerJourney}">
					<div class="panel-list panel-stack margin-top-30">
						<div class="">
							<c:forEach var="travellerDataPerJourney" items="${bcfTravellersData.travellerDataPerJourney}" varStatus="travellerDataIdx">
								<!-- Outbound passenger -->
								<hr class="margin-bottom-30 margin-top-30"/>
								<p class="margin-bottom-20">
									<strong><spring:theme code="text.travellerdetails.passengers.departure.details" text="Departure passenger details"/></strong>
								</p>
					            <fmt:formatDate value="${travellerDataPerJourney.outboundTravelDate}" pattern="E, MMM dd YYYY" />
					            <summary:manifestTripSummaryLocationFormat locationName="${travellerDataPerJourney.travelRoute.origin.name}"/>
					            <br/>
								<c:if test="${fn:length(travellerDataPerJourney.outboundTravellerData) > 0}">
								<travellerdetails:travellers formValues="${travellerDataPerJourney.outboundTravellerData[0]}" idx="0" travellerForm="travellerDataPerJourney[${travellerDataIdx.index}].outboundTravellerData[0]" adultsTitles="${adultsTitles}" childrenTitles="${childrenTitles}"
									hasVehicle="${travellerDataPerJourney.hasVehicleOutbound}" />
								<c:forEach var="travellerData" items="${travellerDataPerJourney.outboundTravellerData}" varStatus="travellerIdx">
									<c:set var="travellerForm" value="travellerDataPerJourney[${travellerDataIdx.index}].outboundTravellerData[${travellerIdx.index}]" />
									<c:set var="idx" value="${travellerIdx.index}" />
									<c:if test="${travellerData.travellerType == 'VEHICLE'}">
										<travellerdetails:vehicleTravellers formValues="${travellerData}" idx="${idx}" travellerForm="${travellerForm}" isReturn="${false}"/>
									</c:if>
								</c:forEach>
								<c:if test="${!travellerDataPerJourney.isReturnWithDifferentPassenger && travellerDataPerJourney.isReturnWithDifferentVehicle}">
								<c:if test="${fn:length(travellerDataPerJourney.inboundTravellerData) > 0}">
									<c:forEach var="travellerData" items="${travellerDataPerJourney.inboundTravellerData}" varStatus="travellerIdx">
										<c:set var="travellerForm" value="travellerDataPerJourney[${travellerDataIdx.index}].inboundTravellerData[${travellerIdx.index}]" />
										<c:set var="idx" value="${travellerIdx.index}" />
										<c:if test="${travellerData.travellerType == 'VEHICLE'}">
											<travellerdetails:vehicleTravellers formValues="${travellerData}" idx="${idx}" travellerForm="${travellerForm}" isReturn="${true}"/>
										</c:if>
									</c:forEach>
								</c:if>
								</c:if>
								<c:forEach var="travellerData" begin="1" items="${travellerDataPerJourney.outboundTravellerData}" varStatus="travellerIdx">
									<c:set var="travellerForm" value="travellerDataPerJourney[${travellerDataIdx.index}].outboundTravellerData[${travellerIdx.index}]" />
									<c:set var="idx" value="${travellerIdx.index}" />
									<c:if test="${travellerData.travellerType == 'PASSENGER'}">
										<travellerdetails:travellers formValues="${travellerData}" idx="${idx}" travellerForm="${travellerForm}" adultsTitles="${adultsTitles}" childrenTitles="${childrenTitles}" hasVehicle="${travellerDataPerJourney.hasVehicleOutbound}" />
									</c:if>
								</c:forEach>
								</c:if>
								<!-- Inbound passenger -->
								<c:if test="${travellerDataPerJourney.isReturnWithDifferentPassenger}">
								<hr class="margin-bottom-30"/>
								<div><span>Please provide details for return travellers</span></div>
								<hr class="margin-bottom-30 margin-top-30"/>
								<p class="margin-bottom-20">
									<strong><spring:theme code="text.travellerdetails.passengers.return.details" text="Return passenger details"/></strong>
								</p>
					            <fmt:formatDate value="${travellerDataPerJourney.inboundTravelDate}" pattern="E, MMM dd YYYY" />
					            <summary:manifestTripSummaryLocationFormat locationName="${travellerDataPerJourney.travelRoute.destination.name}"/>
					            <br/>
								<c:if test="${fn:length(travellerDataPerJourney.inboundTravellerData) > 0}">
									<travellerdetails:travellers formValues="${travellerDataPerJourney.inboundTravellerData[0]}" idx="0" travellerForm="travellerDataPerJourney[${travellerDataIdx.index}].inboundTravellerData[0]" adultsTitles="${adultsTitles}" childrenTitles="${childrenTitles}"
										hasVehicle="${travellerDataPerJourney.hasVehicleInbound}" />
									<c:if test="${travellerDataPerJourney.isReturnWithDifferentVehicle}">
									<c:forEach var="travellerData" items="${travellerDataPerJourney.inboundTravellerData}" varStatus="travellerIdx">
										<c:set var="travellerForm" value="travellerDataPerJourney[${travellerDataIdx.index}].inboundTravellerData[${travellerIdx.index}]" />
										<c:set var="idx" value="${travellerIdx.index}" />
										<c:if test="${travellerData.travellerType == 'VEHICLE'}">
											<travellerdetails:vehicleTravellers formValues="${travellerData}" idx="${idx}" travellerForm="${travellerForm}" isReturn="${true}"/>
										</c:if>
									</c:forEach>
									</c:if>
									<c:forEach var="travellerData" begin="1" items="${travellerDataPerJourney.inboundTravellerData}" varStatus="travellerIdx">
										<c:set var="travellerForm" value="travellerDataPerJourney[${travellerDataIdx.index}].inboundTravellerData[${travellerIdx.index}]" />
										<c:set var="idx" value="${travellerIdx.index}" />
										<c:if test="${travellerData.travellerType == 'PASSENGER'}">
											<travellerdetails:travellers formValues="${travellerData}" idx="${idx}" travellerForm="${travellerForm}" adultsTitles="${adultsTitles}" childrenTitles="${childrenTitles}" hasVehicle="${travellerDataPerJourney.hasVehicleInbound}" />
										</c:if>
									</c:forEach>
								</c:if>
								</c:if>
							</c:forEach>
						</div>
					</div>
					<hr class="margin-bottom-30"/>
					<c:choose>
						<c:when test="${'BOOKING_TRANSPORT_ONLY' eq bookingJourney}">
							<ferryselection:nrtransportreservationferrybuttons />
						</c:when>
						<c:otherwise>
							<div class="row bottom-row margin-top-20">
								<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-4">
									<button type="submit" class="btn btn-primary btn-block">
										<spring:theme code="text.page.travellerdetails.button.continue" text="Continue" />
									</button>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<span> <spring:theme code="text.page.travellerdetails.notravellersfound" text="No travellers found" />
					</span>
				</c:otherwise>
			</c:choose>

					<input type="hidden" id="orderCode" name="orderCode" value="${orderCode}" />
		</form:form>
	</div>
</div>
<cms:pageSlot position="JourneyDetailsEdit" var="feature"
			element="section">
			<cms:component component="${feature}" />
		</cms:pageSlot>
<popupmodals:acceptableFormsModal/>
<modifybooking:abortCancellation/>
</div>
