<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
   <modifybooking:editBookingHeaderBar/>
   <progress:fareFinderProgressBar stage="selectvehicle" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
   <modifybooking:originalBookingDetails/>
   <input type="hidden" id="skipVehicle" value="${vehicleToSkip}">
   <input type="hidden" id="isFullAccountUser" value="${isFullAccountUser}">
   <input type="hidden" id="hasNonOptionSailingInCart" value="${hasNonOptionSailingInCart}">
   <summary:ferryJourneyTripSummary/>
   <c:url var="fareFinderUrl" value="/view/FareFinderComponentController/search" />
   <form:form commandName="bcfFareFinderForm" action="${fn:escapeXml(fareFinderUrl)}" method="POST" class="form-vehicle-selection fe-validate form-background form-booking-trip" id="y_fareFinderForm">
      <vehicle:vehicleInfo bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}" formPrefix="${formPrefix}" idPrefix="fare" />
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12 mt-4 mb-30">
               <button class="btn btn-primary btn-block" type="submit" id="fareFinderFindButton" value="Submit">
                  <spring:theme code="text.ferry.farefinder.proceed.to.sailing.selection.page" text="CONFIRM AND FIND SAILING" />
               </button>
            </div>
         </div>
      </div>
   </form:form>
   <modifybooking:abortCancellation/>
</div>
