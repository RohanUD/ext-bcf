<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fareFinderProgress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="travellerdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/travellerdetails"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<c:url var="submitUrl" value="/traveller-details" />
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
<modifybooking:editBookingHeaderBar/>
<progress:bookingProgressBar stage="personalDetails" bookingJourney="${bookingJourney}" />
<fareFinderProgress:fareFinderProgressBar stage="passengerInfo" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
<modifybooking:originalBookingDetails/>
<fareselection:SailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />

<div class="container panel-details">
	<div class="margin-reset clearfix">
			<div class="row margin-top-40 margin-bottom-20">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-2">
				<h4 class="margin-bottom-20">
					<spring:theme code="text.update.passenger" text="Update passenger details" />
				</h4>
				<p>
				    <spring:theme code="text.update.passenger.changes" text="You've made a change that requires you to update your passenger details." />
				</p>
				<p>
				 <spring:theme code="text.select.passenger" text="Please select the passenger(s) you wish to remove" />
				</p>
			
			 	<c:choose>
				<c:when test="${not empty bcfTravellersData.travellerDataPerJourney}">
                    <hr/>
					<div class="panel-list panel-stack margin-top-30 update-travellers">
						<div class="">
							<c:forEach var="travellerDataPerJourney" items="${bcfTravellersData.travellerDataPerJourney}" varStatus="travellerDataIdx">
								<!-- Outbound passenger -->
								<c:if test="${fn:length(travellerDataPerJourney.outboundTravellerData) > 0}">
                                    <c:forEach var="travellerData" items="${travellerDataPerJourney.outboundTravellerData}" varStatus="travellerIdx">

                                       
                                        <c:if test="${not empty travellerData.specialRequestDetail and not empty travellerData.specialRequestDetail.specialServiceRequests}">
                                            <c:forEach var="specialServiceRequest" items="${travellerData.specialRequestDetail.specialServiceRequests}" varStatus="specialServiceRequestIdx">
                                                  <c:if test="${specialServiceRequest.code ne 'OTHR'}">
                                                     <c:set var="specialServiceRequestDatas" value="${specialServiceRequestDatas}${specialServiceRequest.name}, " />
                                                  </c:if>
                                              </c:forEach>
                                        </c:if>
                                             <p class="margin-bottom-20">
                                                <strong> <spring:theme code="text.update.passenger.${travellerData.travellerInfo.passengerType.code}"/>&nbsp;${fn:escapeXml(travellerData.travellerInfo.passengerType.name)}</strong>
                                            </p>
                                             <p>${travellerData.travellerInfo.firstName}&nbsp;${travellerData.travellerInfo.surname}</p>
                                             <c:if test="${not empty travellerData.specialRequestDetail and not empty travellerData.specialRequestDetail.specialServiceRequests}">
                                                <c:forEach var="specialServiceRequest" items="${travellerData.specialRequestDetail.specialServiceRequests}" varStatus="specialServiceRequestIdx">
                                                      <c:if test="${specialServiceRequest.code ne 'OTHR'}">
                                                      <ul class="special-service-request">
                                                        <li>${specialServiceRequest.name}</li>
                                                      </ul>
                                                      </c:if>
                                                  </c:forEach>
                                            </c:if>
 
                                               <div class="remove">  
                                                    <a href="${contextPath}/update-travellers/remove/${travellerData.travellerInfo.passengerType.code}/${travellerData.uid}">
                                                        <span class="glyphicon glyphicon-remove"></span>&nbsp;
                                                        Remove
                                                    </a>
                                               </div>
                                            <hr/>

                                    </c:forEach>
								</c:if>
								<!-- Inbound passenger -->
								<c:if test="${travellerDataPerJourney.isReturnWithDifferentPassenger}">
                                    <hr/>
                                    <div><span>Please provide details for return travellers</span></div>
                                    <c:if test="${fn:length(travellerDataPerJourney.inboundTravellerData) > 0}">

                                        <c:forEach var="travellerData" items="${travellerDataPerJourney.inboundTravellerData}" varStatus="travellerIdx">
                                             
                                            <c:if test="${not empty specialServiceRequestDatas or not empty travellerData.travellerInfo.firstName}">

                                                <p class="margin-bottom-20">
                                                    <strong><spring:theme code="text.update.passenger.${travellerData.travellerInfo.passengerType.code}"/>&nbsp;${fn:escapeXml(travellerData.travellerInfo.passengerType.name)}</strong>
                                                </p>
                                                 <p>${travellerData.travellerInfo.firstName}&nbsp;${travellerData.travellerInfo.surname}</p>
                                                 <c:if test="${not empty travellerData.specialRequestDetail and not empty travellerData.specialRequestDetail.specialServiceRequests}">
                                                 <c:forEach var="specialServiceRequest" items="${travellerData.specialRequestDetail.specialServiceRequests}" varStatus="specialServiceRequestIdx">
                                                       <c:if test="${specialServiceRequest.code ne 'OTHR'}">
                                                         
                                                       <ul class="special-service-request">
                                                         <li>${specialServiceRequest.name}</li>
                                                       </ul>
                                                       </c:if>
                                                   </c:forEach>
                                                </c:if>
                                                   <div class="remove">
                                                        <a href="${contextPath}/update-travellers/remove/${travellerData.travellerInfo.passengerType.code}/${travellerData.uid}">
                                                            <span class="glyphicon glyphicon-remove"></span>&nbsp;
                                                            Remove
                                                        </a>
                                                   </div>
                                                <hr/>
                                             </c:if>
                                        </c:forEach>
								    </c:if>
								</c:if>
							</c:forEach>
						</div>
					</div>

				</c:when>
				<c:otherwise>
					<span> <spring:theme code="text.page.travellerdetails.notravellersfound" text="No travellers found" />
					</span>
				</c:otherwise>
			</c:choose>


	</div>
</div>
</div>
</div>
<cms:pageSlot position="JourneyDetailsEdit" var="feature"
			element="section">
			<cms:component component="${feature}" />
		</cms:pageSlot>
<popupmodals:acceptableFormsModal/>
<div class="margin-bottom-40">
    <modifybooking:abortCancellation/>
</div>
</div>
