<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
	<json:property name="htmlContent">
		<c:set var="paxCount" value="0" />
		<div id="y_accessibilityPlaceholder">
			<c:forEach var="paxType" items="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}" varStatus="i">
				<c:forEach begin="1" end="${paxType.quantity}" varStatus="j">
					<div class="col-12 col-md-6 col-lg-6 bc-accordion">
						<label>
							<spring:theme code="text.ferry.farefinder.passengerinfo.passenger.accesibility" text="PASSENGER" />
							&nbsp;${paxCount+1} : ${paxType.passengerType.name}
						</label>
						<div id="accordion-accessibility-${paxCount+1}">
							<div id="y_accessibility_dropdown_${paxCount+1}">
								<h3>
									<spring:theme code="label.ancillary.extra.product.dropdown.heading" text="Please make a Selection" />
									<span class="custom-arrow"></span>
								</h3>
								<div>
									<div id="y_accessibility_div_${paxCount+1}">
										<div id="y_accessibility_clearselection_${paxCount+1}" class="btn-block border hidden">
											<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.clear.selection" text="clear selection" />
										</div>
										<c:forEach items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[paxCount].specialServiceRequestDataList}" var="specialServiceRequest" varStatus="l">
											<c:if test="${specialServiceRequest.code ne 'OTHR'}">
												<div class="passenger-option-select">
													<label class="custom-checkbox-input">
														<form:checkbox id="specialServiceRequest_${specialServiceRequest.code}_${paxCount+1}" value="${specialServiceRequest.selection}" path="bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[${paxCount}].specialServiceRequestDataList[${l.index}].selection" />${specialServiceRequest.name}
														<span class="checkmark-checkbox"></span>
													</label>
												</div>
											</c:if>
										</c:forEach>
										<c:forEach items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[paxCount].ancillaryRequestDataList}" var="ancillaryProduct" varStatus="k">
											<div class="passenger-option-select">
												<label class="custom-radio-input">
													<form:radiobutton class="js-accessibility-product" path="bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[${paxCount}].selection" value="${ancillaryProduct.code}" />${ancillaryProduct.name}
													<span class="checkmark"></span>
												</label>
											</div>
										</c:forEach>
										<c:forEach items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[paxCount].specialServiceRequestDataList}" var="specialServiceRequest" varStatus="l">
											<c:if test="${specialServiceRequest.code eq 'OTHR'}">
												<div class="passenger-textarea-select-sec">
													<p class="fnt-14 other-accessibility-text">
														<spring:theme code="label.ferry.farefinder.passengerinfo.other.accessibility.requirements" text="Other accessibility requirements " />
													</p>
													<div id="other_accessibility_need_${paxCount+1}" class="passenger-textarea-select">
														<form:textarea type="text" id="textbox_other_accessibility_need" path="bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList[${paxCount}].specialServiceRequestDataList[${l.index}].description"
															value="${passengerInfoForm.accessibilityRequestDataList[paxCount].specialServiceRequestDataList[l.index].description}" maxlength="80"/>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</div>
					<c:set var="paxCount" value="${paxCount+1}" />
				</c:forEach>
			</c:forEach>
		</div>
	</json:property>
</json:object>
