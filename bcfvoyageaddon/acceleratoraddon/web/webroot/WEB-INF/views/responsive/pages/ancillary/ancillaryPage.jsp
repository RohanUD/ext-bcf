<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ancillary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/ancillary"%>
<%@ taglib prefix="accessibility" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/accessibility"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fareFinderProgress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<c:url var="submitUrl" value="/ancillary" />
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
	<modifybooking:editBookingHeaderBar />
	<progress:bookingProgressBar stage="personalDetails" bookingJourney="${bookingJourney}" />
	<fareFinderProgress:fareFinderProgressBar stage="options" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
	<modifybooking:originalBookingDetails />
	<fareselection:SailingDetails bcfFareFinderForm="${bcfFareFinderForm}" isReturn="${isReturn }" />
	<c:set var="formPrefix" value="" />
	<div class="container">
		<c:forEach items="${bcfExtraProducts.bcfExtraProductPerJourneyList}" var="bcfExtraProductPerJourney" varStatus="i">
			<c:if test="${fn:length(bcfExtraProductPerJourney.outboundExtraProductDataList) > 0}">
				<h4 class="mb-3 margin-top-40">
					<spring:theme code="text.ancillary.departure.sailings.heading" text="Departure ferry options" />
				</h4>
				<c:forEach items="${bcfExtraProductPerJourney.outboundExtraProductDataList}" var="extraProductPerVessel" varStatus="j">
					<c:set var="pathPrefix" value="${fn:escapeXml(formPrefix)}bcfExtraProductPerJourneyList[${fn:escapeXml(i.index)}].outboundExtraProductDataList[${fn:escapeXml(j.index)}]" />
					<ancillary:extraProduct pathPrefix="${pathPrefix}" extraProductPerVessel="${extraProductPerVessel}" journeyRef="${bcfExtraProductPerJourney.journeyReferenceNumber}" odRef="0" transportOfferingCode="${bcfExtraProductPerJourney.transportOfferingCode}"
						routeCode="${bcfExtraProductPerJourney.routeCode}" paxCountPerVessel="${bcfExtraProductPerJourney.paxCount}" location="${bcfFareFinderForm.routeInfoForm.departureLocationName}" dateTime="${bcfFareFinderForm.routeInfoForm.departingDateTime}" />
				</c:forEach>
			</c:if>
			<c:if test="${fn:length(bcfExtraProductPerJourney.inboundExtraProductDataList) > 0}">
				<hr>
				<h4 class="mb-3">
					<spring:theme code="text.ancillary.return.sailings.heading" text="Return ferry options" />
				</h4>
				<c:forEach items="${bcfExtraProductPerJourney.inboundExtraProductDataList}" var="extraProductPerVessel" varStatus="j">
					<c:set var="pathPrefix" value="${fn:escapeXml(formPrefix)}bcfExtraProductPerJourneyList[${fn:escapeXml(i.index)}].inboundExtraProductDataList[${fn:escapeXml(j.index)}]" />
					<ancillary:extraProduct pathPrefix="${pathPrefix}" extraProductPerVessel="${extraProductPerVessel}" journeyRef="${bcfExtraProductPerJourney.journeyReferenceNumber}" odRef="1" transportOfferingCode="${bcfExtraProductPerJourney.transportOfferingCode}"
						routeCode="${bcfExtraProductPerJourney.routeCode}" paxCountPerVessel="${bcfExtraProductPerJourney.paxCount}" location="${bcfFareFinderForm.routeInfoForm.arrivalLocationName}" dateTime="${bcfFareFinderForm.routeInfoForm.returnDateTime}" />
				</c:forEach>
			</c:if>
		</c:forEach>
		<hr class="margin-top-30 margin-bottom-30">
		<div class="row">
			<div class="col-xs-12 col-md-4 col-md-offset-4">
				<c:url var="nextPageURL" value="${nextURL}" />
				<a href="${nextPageURL}" class="btn btn-primary btn-block" id="ancillary-page-submit">
					<spring:theme code="text.ancillary.button.continue" text="Continue" />
				</a>
			</div>
		</div>
	</div>
	<cms:pageSlot position="JourneyDetailsEdit" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<div class="modal fade" id="error-add-product-to-cart-modal" tabindex="-1" role="dialog" aria-labelledby="error-add-product-to-cart-modal-label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
					</button>
					<h4 class="modal-title" id="">
						<spring:theme code="text.ancillary.add.product.to.cart.error.modal.title" />
					</h4>
				</div>
				<div class="modal-body add-ancillary-to-cart">
					<p class="">
						<spring:theme code="error.ancillary.add.extra.product.to.cart" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<modifybooking:abortCancellation />
</div>
