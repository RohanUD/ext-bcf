<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<form:form commandName="bcfFareFinderForm" action="${pageContext.request.contextPath}/view/FareFinderComponentController/fareCalculatorSearch"
	class="farefinderform-custom fe-validate form-background form-booking-tripfe-validate form-background form-booking-trip" id="y_fareFinderForm">
    <div class="container">
    <b><spring:theme code="text.ferry.calculator.summary.fare.calculator" text="Fare Calculator" /></b>
    <br />
    <hr class="mt-5 mb-5"/><br/>
    <strong><spring:theme code="text.ferry.farefinder.routeinfo.header.selection" text="Date & destination" /></strong><br/>
	<farefinder:fareCalculatorRouteInfoAccordion routeInfoForm="${bcfFareFinderForm.routeInfoForm}" formPrefix="${formPrefix}" idPrefix="fare" />
	</div>

    <farefinder:fareCalculatorPassengerInfoAccordion/>
    <input id="readOnlyResults" name="readOnlyResults" value="true" type="hidden"/>
    
    <vehicle:fareCalculatorVehicleFields bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}"
    	formPrefix="${formPrefix}" idPrefix="fare" isFareFinderComponent="true"/>
    <div class="row margin-top-20 margin-bottom-40">
        <div class="col-lg-3 col-md-3 col-md-offset-5">
            <button class="btn btn-primary btn-block" type="submit" id="fareFinderFindButton" value="Submit">
               <spring:theme code="text.ferry.calculate.fare" text="Calculate Fare" />
            </button>
        </div>
    </div>

</form:form>
<input id="reservable" name="reservable" value="${reservable}" type="hidden"/>
<input id="routesReservableData" name="routesReservableData" value="${routesReservableData}" type="hidden"/>

