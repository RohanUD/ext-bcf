<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="js-current-conditions-url hidden">/current-conditions</div>
<c:url var="submitURL" value="/currentconditions-details"/>


<div id="tabs-4" class="departures-cc-component ">
    <div class="container override-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <c:if test="${not empty title}">
                    <h5>${title}</h5>
                </c:if>
                <c:if test="${not empty description}">
                    <p>${description}</p>
                </c:if>
            </div>
        </div>

        <input type="hidden" name="departureLocation" class="y_currentConditionsOriginLocationCode" maxlenght="5"/>
        <input type="hidden" name="arrivalLocation" class=" y_currentConditionsDestinationLocationCode" maxlenght="5" />


        <div class="row-fluid margin-top-17">
            <form:form commandName="currentConditionsForm" method="GET"
                       action="${fn:escapeXml(submitURL)}"
                       class="fe-validate form-background form-booking-trip box-3"
                       id="y_currentConditionsForm">

                <c:set var="formPrefix" value=""/>
                <currentconditions:currentconditions currentConditionsForm="${currentConditionsForm}" formPrefix="${formPrefix}"/>

                <div class="bc-accordion ferry-drop current-condition cond-btn">
                    <button class="btn btn-primary btn-block btn-curn js-current-conditions-submit" type="submit" value="Submit">
                        <spring:theme code="text.currentconditions.view"
                                      text="VIEW CONDITIONS"/>
                    </button>
                </div>
            </form:form>
        </div>
    </div>
</div>
