<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="emailItinerary" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/emailitinerary"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<spring:url value="/manage-booking/personal/confirm-cancellation" var="cancelBookingUrl" />
<spring:url value="/manage-booking/modify-booking" var="editBookingUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${displayTransportBookingDetailsComponent}">
	<c:if test="${hasErrorFlag}">
		<div class="alert alert-danger " role="alert">
			<p>
				<spring:message code="${errorMsg}" text="${errorMsg}" />
			</p>
		</div>
	</c:if>
	<c:if test="${not empty reservationDataList and not empty reservationDataList.reservationDatas}">
		<c:forEach items="${reservationDataList.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
			<div id="booking-detail-page-accordion" class="payment-accordion">
				<c:if test="${not empty reservationData.reservationItems}">
					<c:forEach items="${reservationData.reservationItems}" var="item" varStatus="itemIdx">
						<c:if test="${item.bookingStatus ne 'CANCELLED'}">
							<h4 class="booking-detail-title">
								<c:choose>
									<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
										<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
									</c:when>
									<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
										<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
									</c:when>
								</c:choose>
								<fmt:formatDate var="departureDate" pattern="EEE, MMM dd, yyyy" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
								<spring:theme code="${labelPrefix}" text="Departs" />
								: ${departureDate}
							</h4>
							<div class="add-on-box">
								<c:choose>
									<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
										<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.total"></c:set>
									</c:when>
									<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
										<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.total"></c:set>
									</c:when>
								</c:choose>
								<transportreservation:sailinginformation itinerary="${item.reservationItinerary}" />
								<p class="text-center my-3">
									<spring:theme code="label.search.bookings.sailing.reference.number" text="Booking" />
									#: <span class="font-weight-bold">${item.bcfBookingReference}</span>
								</p>
								<p class="text-center my-3">
									<spring:theme code="text.page.mybookings.bookingStatus" text="Booking Status" />
									: <span class="font-weight-bold">${item.bookingStatus}</span>
								</p>
								<transportreservation:travellercount vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
								<div class="row">
									<div class="col-md-12">
										<p class="text-center my-3">
											<a href="/ship-info/${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.code}" data-toggle="modal" data-target="#detailsModal">${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.name}</a>
										</p>
									</div>
									<div class="col-md-10 col-md-offset-1">
										<div class="vp-box">
											<h5 class="my-3">
												<strong><spring:theme code="text.page.managemybooking.bookingdetails.bundle.name.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" /></strong>
											</h5>
											<p>
												<spring:theme code="text.page.managemybooking.bookingdetails.bundle.description.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" />
											</p>
											<h5 class="my-3">
												<strong><spring:theme code="label.payment.reservation.traveller.header" text="Vehicles & passengers" /></strong>
											</h5>
											<ul class="list-unstyled payment-vp">
												<transportreservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" />
												<transportreservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" travellers="${item.reservationItinerary.travellers}" routeType="${item.reservationItinerary.route.routeType}" />
												<c:forEach var="fee" items="${item.reservationPricingInfo.totalFare.fees}">
													<c:if test="${fee.price.value > 0.0}">
														<li>
															<div class="row ">
																<ul class="col-xs-9 col-md-9">
																	<li>${fee.quantity}&nbsp;x&nbsp;${fee.name}</li>
																</ul>
																<ul class="col-xs-3 col-md-3 text-right">
																	<li>
																		<format:price priceData="${fee.price}" />
																	</li>
																</ul>
															</div>
														</li>
													</c:if>
												</c:forEach>
											</ul>
											<c:if test="${item.reservationItinerary.route.routeType eq 'LONG' and not empty item.reservationPricingInfo.offerBreakdowns}">
												<h5 class="my-3">
													<strong><spring:theme code="reservation.journey.extras" text="Your Extras:" /></strong>
												</h5>
												<ul class="list-unstyled payment-vp">
													<transportreservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.offerBreakdowns}" />
												</ul>
											</c:if>
											<c:if test="${not empty item.reservationPricingInfo.itineraryPricingInfo.largeItemsBreakdownDataList}">
												<transportreservation:largeItems largeItems="${item.reservationPricingInfo.itineraryPricingInfo.largeItemsBreakdownDataList}" />
											</c:if>
											<c:if test="${not empty item.reservationPricingInfo.totalFare.taxes and not empty item.reservationPricingInfo.totalFare.discounts}">
												<div>
													<h5 class="my-3">
														<strong><spring:theme code="label.payment.reservation.sailing.price.breakup.header" text="Taxes fees & discounts" /></strong>
													</h5>
													<ul class="list-unstyled payment-vp">
														<c:forEach var="tax" items="${item.reservationPricingInfo.totalFare.taxes}">
															<c:if test="${tax.price.value > 0.0}">
																<li>
																	<div class="row">
																		<ul class="col-xs-9 col-md-9">
																			<li>
																				<spring:theme code="${tax.code}" />
																			</li>
																		</ul>
																		<ul class="col-xs-3 col-md-3 text-right">
																			<li>
																				<format:price priceData="${tax.price}" />
																			</li>
																		</ul>
																	</div>
																</li>
															</c:if>
														</c:forEach>
														<c:forEach var="discount" items="${item.reservationPricingInfo.totalFare.discounts}">
															<c:if test="${discount.price.value < 0.0}">
																<li>
																	<div class="row">
																		<ul class="col-xs-9 col-md-9">
																			<li>
																				<spring:theme code="${discount.purpose}" />
																			</li>
																		</ul>
																		<ul class="col-xs-3 col-md-3 text-right">
																			<li>
																				<format:price priceData="${discount.price}" />
																			</li>
																		</ul>
																	</div>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</div>
											</c:if>
										</div>
										<div>
											<ul class="list-unstyled payment-vp">
												<li>
													<div class="row">
														<ul class="col-xs-9 col-md-9">
															<li>
																<spring:theme code="text.page.managemybooking.bookingdetails.sailing.total.price" text="Total cost" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<format:price priceData="${item.reservationPricingInfo.totalFare.totalPrice}" />
															</li>
														</ul>
													</div>
												</li>
												<li>
													<div class="row departure-bx">
														<ul class="col-xs-9 col-md-9">
															<li>
																<spring:theme code="text.page.managemybooking.bookingdetails.total.paid" text="Total paid" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<strong><format:price priceData="${item.reservationPricingInfo.paidFare}" /></strong>
															</li>
														</ul>
													</div>
												</li>
												<li>
													<div class="row margin-top-10">
														<ul class="col-xs-9 col-md-9 ">
															<li>
																<spring:theme code="text.page.managemybooking.bookingdetails.due.at.terminal" text="Due at terminal" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<format:price priceData="${item.reservationPricingInfo.dueAtTerminal}" />
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
										<div class="mt-4">
											<c:choose>
												<c:when test="${item.upcoming}">
													<div id="terms-conditions-${item.bcfBookingReference}">
														<bookingDetails:termsAndCondition termsAndCondition="${item.termsAndConditionsData}" />
													</div>
													<div class="row mb-2 mt-4">
														<div class="col-xs-6 text-center">
														    <emailItinerary:emailItinerary/>
														</div>
														<div class="col-xs-6 text-center">
															<a href="javascript:;" onClick="window.print();">
															<i class="fas fa-print"></i> <spring:theme code="text.manage.account.my.bookings.print.itinerary" text="Print itinerary" />
															</a>
														</div>
													</div>
													<c:if test="${item.modifyAllowed}">
														<div class="row mb-2 mt-4 text-center">
															<button type="button" data-toggle="modal" data-target="#manageBookingEditSailingModal" class="btn btn-primary btn-block js-manage-booking-edit-modal" data-id="${item.bcfBookingReference}">
																<span><spring:theme code="text.manage.account.my.bookings.edit.booking" text="Edit booking" /></span>
															</button>
														</div>
													</c:if>
													<c:if test="${item.cancelAllowed}">
														<div class="row mb-2 mt-4 text-center">
															<button type="button" data-toggle="modal" data-target="#manageBookingCancelSailingModal" class="btn btn-transparent btn-block js-manage-booking-cancel-modal" data-id="${item.bcfBookingReference}">
																<span><spring:theme code="text.manage.account.my.bookings.cancel.booking" text="Cancel booking" /></span>
															</button>
														</div>
													</c:if>
												</c:when>
												<c:otherwise>
													<div class="row mb-2 mt-4">
														<div class="col-xs-6 text-center">
															<a href="#">
																<spring:theme code="text.manage.account.my.bookings.email" text="Email" />
															</a>
														</div>
													</div>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
							<c:choose>
								<c:when test="${itemIdx.first && not itemIdx.last}">
									<c:set var="bookingRefsToCancel" value="${item.bcfBookingReference}${','}" />
								</c:when>
								<c:otherwise>
									<c:set var="bookingRefsToCancel" value="${bookingRefsToCancel}${item.bcfBookingReference}" />
								</c:otherwise>
							</c:choose>
						</c:if>
                         <input id="ebookingId" name="ebookingId" type="hidden" value="${item.bcfBookingReference}"/>
					</c:forEach>
				</c:if>
			</div>
		</c:forEach>
        <div class="modal" tabindex="-1" role="dialog" id="manageBookingEditSailingModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
						</button>
						<h4 class="modal-title">
							<spring:theme code="text.page.managemybooking.bookingdetails.edit.booking.modal.title" text="Edit booking" />
						</h4>
					</div>
					<div class="modal-body">
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.edit.booking.confimation.note" text="Please confirm you want to edit this booking" />
						</p>
						<br>
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.edit.booking.confimation.details" text="Changes to your booking won't be applied until you finish checkout. Your total fare will be recalculated and include a booking change fee." />
						</p>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<form:form method="POST" action="${fn:escapeXml(editBookingUrl)}">
									<input type="hidden" id="eBookingReferences" name="eBookingReferences" value="${fn:escapeXml(param.eBookingCode)}" />
									<input type="hidden" id="eBookingReferenceToModify" name="eBookingReferenceToModify" />
									<button type="submit" class="btn btn-transparent btn-block">
										<span><spring:theme code="text.page.managemybooking.bookingdetails.edit.booking.confimation.button" text="Edit booking" /></span>
									</button>
								</form:form>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<button type="button" class="btn btn-transparent btn-block" data-dismiss="modal">
									<spring:theme code="text.page.managemybooking.bookingdetails.edit.booking.abort.button" text="Back" />
								</button>
							</div>
						</div>
						<cms:pageSlot position="ManageBookingEditBookingContent" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
		<div class="modal" tabindex="-1" role="dialog" id="manageBookingCancelSailingModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
						</button>
						<h4 class="modal-title">
							<spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.modal.title" text="Cancel booking" />
						</h4>
					</div>
					<div class="modal-body">
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.confimation.warning" text="Please confirm you want to cancel this booking" />
						</p>
						<br>
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.confimation.note" text="Note: Cancellations can't be undone" />
						</p>
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.confimation.details" text="Cancelled bookings may be refundable depending on the fare type and how far ahead of time you cancel." />
						</p>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<form:form method="POST" action="${fn:escapeXml(cancelBookingUrl)}" commandName="cancelBookingRefs">
									<input type="hidden" id="bookingReferencesToCancel" name="cancelBookingRefs" value="${item.bcfBookingReference}" />
									<button type="submit" class="btn btn-transparent btn-block">
										<span><spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.confimation.button" text="Confirm" /></span>
									</button>
								</form:form>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<button type="button" class="btn btn-transparent btn-block" data-dismiss="modal">
									<spring:theme code="text.page.managemybooking.bookingdetails.cancel.booking.abort.button" text="Back" />
								</button>
							</div>
						</div>
						<cms:pageSlot position="ManageBookingCancelBookingContent" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</c:if>
