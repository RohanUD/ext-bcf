<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/cart"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_transportReservationComponentId" />
<div class="y_transportReservationComponent">
	<div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
		<modifybooking:editBookingHeaderBar />
		<progress:fareFinderProgressBar stage="Sailings" amend="${amend}" fareFinderJourney="${fareFinderJourney}" />
		<modifybooking:originalBookingDetails />
		<c:choose>
			<c:when test="${reservable}">
				<fareselection:SailingDetails bcfFareFinderForm="${bcfFareFinderForm }" isReturn="${isReturn }" />
				<c:choose>
					<c:when test="${availabilityStatus eq 'ON_REQUEST'}">
						<p>
							<br> <b><spring:theme code="text.package.booking.on.request.title" text="Booking is on request" /></b> <br>
							<spring:theme code="text.package.booking.on.request.ccard.message" />
						</p>
					</c:when>
				</c:choose>
				<div class="green-bar-msg">
					<span class="bcf bcf-icon-checkmark"></span>
					<c:choose>
						<c:when test="${not empty tripType && tripType eq 'RETURN'}">
							<spring:theme code="text.packageferryselection.return.selected" text="Return sailing selection" />
						</c:when>
						<c:otherwise>
							<spring:theme code="text.packageferryselection.departure.selected" text="Departure sailing selection" />
						</c:otherwise>
					</c:choose>
					&nbsp;<strong><spring:theme code="text.packageferryselection.departure.confirmed" text="confirmed!" /></strong>

				</div>
			    <cart:cartTimer />
			</c:when>
			<c:otherwise>
				<div class="container">
					<div class="row">
						<div class="white-bg">
							<div class="col-md-10 p-header pt-2 pb-2">
								<div class="fare-table-inner-wrap">
									<h2 class="h4">
										<spring:theme code="label.packageferryreview.sailingoptions.heading" />
										<br> <br>
										<spring:theme code="label.packageferryreview.sailingoptions.subheading" />
									</h2>
									<p>
										<spring:theme code="label.packageferryreview.sailingoptions.description" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

		<form:form id="bookingReferenceForm" commandName="bookingReferenceForm" action="/checkout/multi/bookingref-update" method="post">
		<div class="container">
			<c:if test="${not empty reservationDataList}">

				<c:forEach items="${reservationDataList.reservationDatas}" var="reservation" varStatus="reservationItemIdx">

					<c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
						<c:if test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">

							<ferryselection:transportreservationdecider item="${item}" type="OutBound" journeyRefNo="${reservation.journeyReferenceNumber}" />

						</c:if>
						<c:choose>
							<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
								<ferryselection:transportreservationdecider item="${item}" type="InBound" journeyRefNo="${reservation.journeyReferenceNumber}" />
							</c:when>
						</c:choose>
					</c:forEach>

				</c:forEach>
			</c:if>
			<c:choose>
				<c:when test="${hasLongRoute}">
					<div class="p-card mb-3">
						<span><spring:theme code="label.fare.selection.review.total.sailing.costs" text="Total sailing costs" /></span> <span><format:price priceData="${reservationDataList.totalFare.totalPrice}" /></span>
					</div>
					<div class="col-md-8 col-sm-12 col-md-offset-2">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
								<a href="${contextPath}${fn:escapeXml(ancillaryPageURL)}" class="btn btn-primary btn-block"><spring:theme code="text.fare.selection.review.add.amenities" text="Add amenities" /></a>
							</div>
							<div class="col-lg-5 col-md-5 col-md-offset-3 margin-bottom-20">
								<a href="${contextPath}${fn:escapeXml(travellerDetailsPageURL)}" class="btn btn-outline-blue btn-block"><spring:theme code="text.fare.selection.review.continue.without.amenities" text="Continue without amenities" /></a>
							</div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<ferryselection:transportreservationferrybuttons />
				</c:otherwise>
			</c:choose>
		</div>
		</form:form>
		<modifybooking:abortCancellation />
	</div>
</div>
