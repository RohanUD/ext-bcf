<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="fareFinderUrl" value="/view/FareFinderComponentController/search" />
<form:form commandName="fareFinderForm" action="${fn:escapeXml(fareFinderUrl)}" method="POST" class="fe-validate form-background form-booking-trip" id="y_fareFinderForm">
	<c:if test="${hideFinderTitle ne true}">
		<legend class=" heading-booking-trip primary-legend" id="trip-finder-modal-title">
			<c:if test="${!showComponent}">
				<a role="button" data-toggle="collapse" aria-expanded="true" data-target=".panel-modify" class="panel-heading panel-header-link collapsable collapsed">
			</c:if>
			<c:choose>
				<c:when test="${!showComponent}">
					<spring:theme code="text.cms.farefinder.modify.title" text="Modify Booking" />
				</c:when>
			</c:choose>
			<c:if test="${!showComponent}">
				</a>
			</c:if>
		</legend>
	</c:if>
	<button type="button" class="modal-only close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<div class="${ showComponent ? '' : 'panel-modify collapse'} ">
		<div class="row input-row less-margin">
			<c:set var="formPrefix" value="" />
			<farefinder:transportInfo tripType="${fareFinderForm.tripType}" formPrefix="${formPrefix}" idPrefix="fare" />
			<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_farefinderComponentId" /> <input type="hidden" value="${terminalsCacheVersion}" id="y_terminalsCacheVersion" />
			<div class="container y_passengers p-0">
				<div class="row input-row mt-3">
					<passenger:passengertypequantity formPrefix="${formPrefix}" passengerTypeQuantityList="${fareFinderForm.passengerTypeQuantityList}" />
					<div class="y_passengerTypeQuantityListError"></div>
				</div>
			</div>
			<farefinder:vehicleInfo tripType="${fareFinderForm.tripType}" formPrefix="${formPrefix}" idPrefix="fare" fareFinderForm= "${fareFinderForm}"/>
			<!--  -->
			<%-- CTA (Search Flights) --%>
		</div>
		<%--  / CTA (Search Flights) --%>
		<div class="col-sm-3 mt-4 pull-right">
			<button class="btn btn-primary find-button col-xs-12 col-sm-12 mt-1" type="submit" id="fareFinderFindButton" value="Submit">FIND</button>
			<h5 class="blue-color col-sm-12 p-0 pull-left">
				<b><i class="fa fa-list" aria-hidden="true"></i> View all my booking</b>
			</h5>
		</div>
	</div>
	<%-- / .fieldset-inner-wrapper --%>
	</fieldset>
	<%--  / CTA (Search Flights) --%>
</form:form>
<c:if test="${hideFinderTitle eq true}">
	</div>
</c:if>
