<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="loadAccessibilitiesBox" value="true" />
<div id="loadAccessibilitiesBox" class="hidden">${loadAccessibilitiesBox}</div>
<div id="y_edit_journey_current_page" class="hidden">${bcfFareFinderForm.currentPage}</div>
<div id="journeydetailseditModal" title="Change Trip" class="hidden">
	<div id="hidden-form-field-error"></div>
	<c:url var="nextPageUrl" value="/view/FareFinderComponentController/submit-fare-finder-form" />
	<form:form commandName="bcfFareFinderForm" action="${fn:escapeXml(nextPageUrl)}" method="POST" class="fe-validate form-background form-booking-trip" id="y_fareFinderEditForm">
		<c:set var="formPrefix" value="" />
		<c:if test="${not empty bcfFareFinderForm.routeInfoForm}">
			<farefinder:routeInfo routeInfoForm="${bcfFareFinderForm.routeInfoForm}" formPrefix="${formPrefix}" idPrefix="fare" />
		</c:if>
		<c:if test="${not empty bcfFareFinderForm.passengerInfoForm && bcfFareFinderForm.currentPage ne 'PassengerInfo'}">
			<passenger:passengerselection formPrefix="${formPrefix}" />
		</c:if>
		<div id="edit-journey-vehicle-selection-div">
			<c:if test="${not empty bcfFareFinderForm.vehicleInfoForm && bcfFareFinderForm.currentPage ne 'PassengerInfo' && bcfFareFinderForm.currentPage ne 'VehicleInfo'}">
				<vehicle:vehicleInfo bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}" formPrefix="${formPrefix}" idPrefix="fare" />
			</c:if>
		</div>
		<div class="row text-center my-5">
			<button class="btn btn-primary find-button light-blue-btn mt-1" type="submit" id="fareFinderFindButton" value="Submit">
				<spring:theme code="text.ferry.farefinder.save.and.continue" text="SAVE AND CONTINUE" />
			</button>
		</div>
		<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_farefinderComponentId" />
		<input type="hidden" value="${terminalsCacheVersion}" id="y_terminalsCacheVersion" />
	</form:form>
</div>
