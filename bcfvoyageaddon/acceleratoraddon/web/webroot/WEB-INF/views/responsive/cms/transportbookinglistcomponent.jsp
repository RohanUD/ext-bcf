<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
<spring:url value="/manage-booking/multi-cancel-bookings" var="multiCancelBookingsUrl" />
<spring:url value="/manage-booking/personal/confirm-cancellation" var="requestMultipleBookingCancelUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="">
   <c:choose>
      <c:when test="${not empty emptyCancelOrderRequest || not empty cancellationResult || not empty failureCancelEBookings || not empty notTransportOnlyBooking}">
         <div class="alert alert-danger alert-dismissible y_cancellationResult" role="alert">
            <c:if test="${not empty emptyCancelOrderRequest}">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">X</span>
               </button>
               <p>
                  <spring:theme code="${emptyCancelOrderRequest}" />
               </p>
            </c:if>
            <c:if test="${not empty cancellationResult}">
               <div>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"> x </span>
                  </button>
                  <p>
                     <spring:theme code="${successfulCancelOrderCodes}" />
                     <spring:theme code="${cancellationResult}" />
                  </p>
               </div>
            </c:if>
            <c:if test="${not empty failureCancelEBookings}">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true"> x </span>
               </button>
               <p>
                  <spring:theme code="${failedCancelOrderCodes}" />
                  <spring:theme code="${failureCancelEBookings}" />
               </p>
            </c:if>
            <c:if test="${not empty notTransportOnlyBooking}">
               <div>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"> x </span>
                  </button>
                  <p>
                     <spring:theme code="${notTransportBookingOrderCodes}" />
                     <spring:theme code="${notTransportOnlyBooking}" />
                  </p>
               </div>
            </c:if>
         </div>
      </c:when>
   </c:choose>
   <c:choose>
      <c:when test="${isEBookingOrder}">
         <div class="panel-body">
            <h4>
               <b>
                  <spring:theme code="text.page.mybookings" />
               </b>
            </h4>
            <transportreservation:advancedBookingsSearch bookingType="personal"/>
           <div class="btn-group" role="group">
              <a href="/my-account/my-bookings/transport-bookings/personal/upcoming/1" type="button" class="btn btn-secondary disabled" id="my-upcoming-bookings">
                 <span>
                    <spring:theme code="text.manage.account.my.upcoming.bookings" text="Upcoming" />
                 </span>
              </a>
              <a href="/my-account/my-bookings/transport-bookings/personal/past/1" type="button" class="btn btn-primary" id="my-past-bookings">
                 <span>
                    <spring:theme code="text.manage.account.my.past.bookings" text="Past" />
                 </span>
              </a>
           </div>
            <c:choose>
               <c:when test="${transportBookings == null || empty transportBookings}">
                  <c:choose>
                     <c:when test="${hasErrorFlag}">
                        <spring:message text="${errorMsg}" />
                     </c:when>
                     <c:otherwise>
                        <div class="fieldset">
                           <p>
                              <spring:theme code="text.page.mybookings.not.found" />
                           </p>
                        </div>
                     </c:otherwise>
                  </c:choose>
               </c:when>
               <c:otherwise>
                  <div id="transport-bookings">
                     <div id="my-bookings-holder">
                        <transportreservation:transportBooking transportBookings="${transportBookings}" />
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 text-center">
                        <div class="back-top-btn">
                           <a href="#">
                              <span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span>                              <strong>
                                 <spring:theme code="text.manage.account.my.bookings.back.to.top" text="Back to top" />
                              </strong>
                           </a>
                        </div>
                     </div>
                  </div>
                  <transportreservation:cancelConfirmation />
               </c:otherwise>
            </c:choose>
         </div>
      </c:when>
      <c:otherwise>
         <div class="container">
            <div class="panel-heading px-0">
               <h3 class="title title-collapse " data-toggle="collapse" data-target="#my-bookings" aria-expanded="true">
                  <b>
                     <spring:theme code="text.page.mybookings" />
                  </b>
               </h3>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <p>
                     <b>
                        <spring:theme code="text.page.transport.mybookings" />
                     </b>
                  </p>
               </div>
            </div>
            <div class="panel-body collapse in" id="my-bookings">
               <c:choose>
                  <c:when test="${myBookings == null || empty myBookings}">
                     <c:choose>
                        <c:when test="${hasErrorFlag}">
                           <div class="alert alert-danger alert-dismissible y_cancellationResult" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true"> x </span>
                              </button>
                              <p>
                                 <spring:message text="${errorMsg}" />
                              </p>
                           </div>
                        </c:when>
                        <c:otherwise>
                           <div class="fieldset">
                              <p>
                                 <spring:theme code="\.page.mybookings.not.found" />
                              </p>
                           </div>
                        </c:otherwise>
                     </c:choose>
                  </c:when>
                  <c:otherwise>
                     <div id="transport-items" class="col-xs-12 accommodation-items y_myBookings my-bookings">
                        <c:forEach var="myBooking" items="${myBookings}" varStatus="idx">
                           <c:set var="index" value="0" />
                           <div class="col-xs-12 booking-list">
                              <div class="panel panel-spaced">
                                 <div class="panel-heading">
                                    <h3 class="panel-title">
                                       <c:choose>
                                          <c:when test="${myBooking.reservationDatas[0]
                                             .bookingStatusName eq 'Active'}">
                                             <label class="custom-checkbox-input">
                                             <input type="checkbox" id="cancelCheckbox${idx.index}" class="cancelBookingRefCheckbox checkbox-label-align" value="${myBooking.reservationDatas[0].code}" />
                                             <span class="checkmark-checkbox"></span>
                                             </label>
                                          </c:when>
                                          <c:otherwise>
                                          </c:otherwise>
                                       </c:choose>
                                       <c:set var="index" value="${index +1}" />
                                       <spring:theme code="text.page.mybookings.bookingreference" />
                                       :${fn:escapeXml(myBooking.reservationDatas[0].code)}
                                    </h3>
                                 </div>
                                 <div class="col-xs-6 col-md-5 col-md-offset-4 text-right">
                                    <dl class="status">
                                       <dt>
                                          <spring:theme code="text.page.mybookings.bookingStatus" />
                                          :
                                       </dt>
                                       <dd>
                                          <bookingDetails:status code="${myBooking.reservationDatas[0].bookingStatusCode}" name="${myBooking.reservationDatas[0].bookingStatusName}" />
                                       </dd>
                                    </dl>
                                 </div>
                                 <div class="col-xs-6 col-md-3">
                                    <div class="form-group col-xs-12">
                                       <form id="mybookingForm" method="get" action="${fn:escapeXml(viewBookingUrl)}/${fn:escapeXml(myBooking.reservationDatas[0].code)}">
                                          <input type="hidden" name="bcfBookingReferences" value="${bcfBookingReferences}">
                                          <button type="submit" class="btn btn-primary btn-block" id="view">
                                             <spring:theme code="button.page.mybookings.detail.view" />
                                          </button>
                                       </form>
                                    </div>
                                 </div>
                                 <c:forEach var="journey" items="${myBooking.reservationDatas}" varStatus="sailingCount">
                                    <div class="panel-body">
                                       <div class="row"></div>
                                       <c:forEach var="reservationItem" items="${journey.reservationItems}">
                                          <h2 class="divided title">
                                             <span class="spacer"></span> Sailing: ${sailingCount.index+1}
                                          </h2>
                                          <h3 class="divided title">${fn:escapeXml(reservationItem.reservationItinerary.route.origin.location.name)}(${fn:escapeXml(reservationItem.reservationItinerary.route.origin.code)}) - ${fn:escapeXml(reservationItem.reservationItinerary.route.destination.location.name)}
                                             (${fn:escapeXml(reservationItem.reservationItinerary.route.destination.code)})
                                          </h3>
                                          <div class="row">
                                             <c:forEach items="${reservationItem.reservationItinerary.originDestinationOptions[0].transportOfferings}" var="transportOffering">
                                                <div class="col-xs-12">
                                                   <div class="col-xs-12 col-sm-6 col-md-3">
                                                      <dl>
                                                         <dt>
                                                            <spring:theme code="text.page.managemybooking.reservationitem.sailingby" />
                                                         </dt>
                                                         <dd>${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.name)}</dd>
                                                      </dl>
                                                   </div>
                                                   <div class="col-xs-12 col-sm-6 col-md-3">
                                                      <dl>
                                                         <dt>
                                                            <spring:theme code="text.page.mybookings.departs" />
                                                            :
                                                         </dt>
                                                         <dd>
                                                            <fmt:formatDate value="${transportOffering.departureTime}" pattern="${datePattern}" />
                                                            <br> <strong>${fn:escapeXml(transportOffering.sector.origin.location.name)}(${fn:escapeXml(transportOffering.sector.origin.name)})</strong>
                                                         </dd>
                                                      </dl>
                                                   </div>
                                                   <div class="col-xs-12 col-sm-6 col-md-3">
                                                      <dl>
                                                         <dt>
                                                            <spring:theme code="text.page.mybookings.arrives" />
                                                            :
                                                         </dt>
                                                         <dd>
                                                            <fmt:formatDate value="${transportOffering.arrivalTime}" pattern="${datePattern}" />
                                                            <br> <strong>${fn:escapeXml(transportOffering.sector.destination.location.name)}(${fn:escapeXml(transportOffering.sector.destination.name)})</strong>
                                                         </dd>
                                                      </dl>
                                                   </div>
                                                   <div class="col-xs-12 col-sm-6 col-md-3">
                                                      <dl>
                                                         <dt>
                                                            <spring:theme code="text.page.mybookings.port" />
                                                            :
                                                         </dt>
                                                         <dd>${fn:escapeXml(transportOffering.sector.origin.code)}</dd>
                                                      </dl>
                                                   </div>
                                                </div>
                                             </c:forEach>
                                          </div>
                                       </c:forEach>
                                    </div>
                                 </c:forEach>
                              </div>
                           </div>
                        </c:forEach>
                     </div>
                     <form:form method = "POST" action = "${fn:escapeXml(multiCancelBookingsUrl)}" commandName="cancellableOrderCodes">
                        <input type="hidden" id="cancellableOrderCode" name="cancellableOrderCodes"
                           value="" />
                        <button id="submitForCancelling" type="submit" class="btn btn-primary btn-place-order btn-block " disabled="disabled"></button>
                        <spring:theme code="button.page.mybookings.multi.cancelbooking" />
                     </form:form>
                     <c:if test="${fn:length(myBookings) > pageSize}">
                        <div class="row">
                           <div class="test col-xs-12">
                              <div class="col-sm-5 col-sm-offset-7">
                                 <a href="#" class="col-xs-12 btn btn-primary y_myAccountMyBookingsShowMore" data-pagesize="${fn:escapeXml(pageSize)}">
                                    <spring:theme code="button.page.mybookings.showmore" text="Show More" />
                                 </a>
                              </div>
                           </div>
                        </div>
                     </c:if>
                  </c:otherwise>
               </c:choose>
            </div>
         </div>
      </c:otherwise>
   </c:choose>
</div>

