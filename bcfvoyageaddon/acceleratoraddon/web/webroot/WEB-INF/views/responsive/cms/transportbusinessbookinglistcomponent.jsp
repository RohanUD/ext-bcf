<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
<spring:url value="/manage-booking/multi-cancel-bookings" var="multiCancelBookingsUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<div>
	<c:if test="${not empty emptyCancelOrderRequest || not empty cancellationResult || not empty failureCancelEBookings || not empty notTransportOnlyBooking}">
		<div class="alert alert-danger alert-dismissible y_cancellationResult" role="alert">
			<c:if test="${not empty emptyCancelOrderRequest}">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">X</span>
				</button>
				<p>
					<spring:theme code="${emptyCancelOrderRequest}" />
				</p>
			</c:if>
			<c:if test="${not empty cancellationResult}">
				<div>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"> x </span>
					</button>
					<p>
						<spring:theme code="${successfulCancelOrderCodes}" />
						<spring:theme code="${cancellationResult}" />
					</p>
				</div>
			</c:if>
			<c:if test="${not empty failureCancelEBookings}">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true"> x </span>
				</button>
				<p>
					<spring:theme code="${failedCancelOrderCodes}" />
					<spring:theme code="${failureCancelEBookings}" />
				</p>
			</c:if>
			<c:if test="${not empty notTransportOnlyBooking}">
				<div>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"> x </span>
					</button>
					<p>
						<spring:theme code="${notTransportBookingOrderCodes}" />
						<spring:theme code="${notTransportOnlyBooking}" />
					</p>
				</div>
			</c:if>
		</div>
	</c:if>
	<c:choose>
		<c:when test="${transportBookings == null || empty transportBookings || hasErrorFlag}">
			<c:choose>
				<c:when test="${hasErrorFlag}">
					<div class="alert alert-danger alert-dismissible y_cancellationResult" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">X</span>
						</button>
						<p>
							<spring:message text="${errorMsg}" />
						</p>
					</div>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger alert-dismissible y_cancellationResult" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">X</span>
						</button>
						<p>
							<spring:theme code="text.page.mybookings.not.found" />
						</p>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:if test="${isEBookingOrder}">
				<div class="panel-body">
					<h4>
						<b><spring:theme code="text.page.mybookings" /></b>
					</h4>
					<transportreservation:advancedBookingsSearch bookingType="business"/>
					<div class="mt-4 tab-my-booking margin-bottom-40">
						<div class="btn-group" role="group">
							<a href="/my-account/my-bookings/transport-bookings/business/upcoming/1" type="button" class="btn btn-secondary disabled" id="my-upcoming-bookings">
								<span> <spring:theme code="text.manage.account.my.upcoming.bookings" text="Upcoming" />
								</span>
							</a>
							<a href="/my-account/my-bookings/transport-bookings/business/past/1" type="button" class="btn btn-primary" id="my-past-bookings">
								<span> <spring:theme code="text.manage.account.my.past.bookings" text="Past" />
								</span>
							</a>
						</div>
					</div>
					<div id="transport-bookings">
						<div id="my-bookings-holder">
							<transportreservation:transportBusinessBooking transportBookings="${transportBookings}" />
						</div>
					</div>
				</div>
			</c:if>
		</c:otherwise>
	</c:choose>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 text-center">
	<div class="back-top-btn">
		<a href="#">
			<span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span><strong><spring:theme code="text.manage.account.my.bookings.back.to.top" text="Back to top" /></strong>
		</a>
	</div>
	<div>
		<a href="/" class="btn btn-primary">
			<spring:theme code="label.manage.account.my.business.bookings.book.sailing.button" text="Book a sailing" />
		</a>
	</div>
</div>
<transportreservation:cancelConfirmation />
