<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page trimDirectiveWhitespaces="true"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="y_transportSummaryComponent inner-wrap y_reservationSideBarContent">
	<input type="hidden" value="${fn:escapeXml(component.uid)}" id="y_transportSummaryComponentId" />
	<c:if test="${reservationDataList ne null}">
		<c:forEach items="${reservationDataList.reservationDatas}" var="reservation" varStatus="idx">
			<c:if test="${reservation.bookingStatusCode ne 'CANCELLED'}">
				<section class="panel border border-dark">
					<div class="panel-body reservation " id="summary">
						<div class="sidebar-content-section">
							<c:forEach items="${reservation.reservationItems}" var="item" varStatus="itemIdx">
								<div class="row">
									<div class="col-sm-5">
										Departure <b> <fmt:formatDate pattern="${dateFormat}" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />,<fmt:formatDate pattern="${timeFormat}"
												value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
										</b><br> <strong>${fn:escapeXml(item.reservationItinerary.route.origin.code)}</strong>
									</div>
									<div class="col-sm-5">
										Arrival <b><c:set var="length" value="${fn:length(item.reservationItinerary.originDestinationOptions[0].transportOfferings)}" /> <fmt:formatDate pattern="${dateFormat}" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[length-1].arrivalTime}" />,<fmt:formatDate
												pattern="${timeFormat}" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[length-1].arrivalTime}" /></b><br> <strong>${fn:escapeXml(item.reservationItinerary.route.destination.code)}</strong>
									</div>
									<div class="col-sm-12">
										<hr>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</section>
			</c:if>
		</c:forEach>
	</c:if>
</div>
