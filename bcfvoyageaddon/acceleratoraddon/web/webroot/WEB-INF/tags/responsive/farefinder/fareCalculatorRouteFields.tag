<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="routeInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="isFareFinderComponent" type="java.lang.Boolean"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.routeType" id="routeType"/>
<form:hidden path="${fn:escapeXml(formPrefix)}routeInfoForm.walkOnRoute" id="isWalkOnRoute"/>

<div class="row-fluid margin-top-17">
	<div class="bc-accordion ferry-drop">
		<label>From </label>
		<div class="accordion-34 component-to-top" id="fromLocationDropDown"
			data-inputselector="fromLocation"
			data-routeinfoselector="y_fareFinderOriginLocationCode"
			data-suggestionselector="y_fareFinderDestinationLocationSuggestionType">
			<h3>
				<span class="dropdown-text">
					<strong><spring:theme
						code="label.ferry.farefinder.dropdown.default.heading" text="Location" /></strong>
					<br />
					<span><spring:theme
						code="label.ferry.farefinder.dropdown.default.subheading" text="Location" /></span>
					<span class="custom-arrow"></span>
				</span>
			</h3>
			<div>

			</div>
		</div>
	</div>
	<div class="bc-accordion ferry-drop">
		<label>To </label>
		<div class="accordion-34 component-to-top" id="toLocationDropDown"
			data-inputselector="toLocation"
			data-routeinfoselector="y_fareFinderDestinationLocationCode"
			data-suggestionselector="y_fareFinderOriginLocationSuggestionType">
			<h3>
				<span class="dropdown-text">
					<strong><spring:theme
						code="label.ferry.farefinder.dropdown.default.heading" text="Location" /></strong>
					<br />
					<span><spring:theme
						code="label.ferry.farefinder.dropdown.default.subheading" text="Location" /></span>
					<span class="custom-arrow"></span>
				</span>
				<span class="custom-arrow"></span>
			</h3>
			<input type="text" class="dropdown-hiddenInput hidden" />
			<div>

			</div>
		</div>
	</div>
	<div class="hidden">
		<div class="input-required-wrap">
			<label for="fromLocation" class="home-label-mobile">
				<spring:theme
					code="label.ferry.farefinder.routeinfo.depart.from" text="FROM" />
			</label>
			<spring:theme var="locationPlaceholder"
				code="text.ferry.farefinder.routeinfo.depart.return.location.placeholder"
				text="Location (City or Terminal)" />
			<form:input type="text" id="fromLocation"
				path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocationName"
				cssErrorClass="fieldError"
				class="y_fareFinderOriginLocation bc-dropdown bc-dropdown--big "
				placeholder="${locationPlaceholder}" autocomplete="off"
				/>
			<form:hidden
				path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocation"
				class="y_fareFinderOriginLocationCode" maxlenght="5" />
			<form:hidden
				path="${fn:escapeXml(formPrefix)}routeInfoForm.departureLocationSuggestionType"
				class="y_fareFinderOriginLocationSuggestionType" />
			<div id="y_fareFinderOriginLocationSuggestions"
				class="autocomplete-suggestions-wrapper hidden"></div>
		</div>
	</div>
	<div class="hidden" id="farefinder-routeinfo-fromLocationDropDown"></div>
	<div class="hidden" id="farefinder-routeinfo-toLocationDropDown"></div>
	<div class="hidden" id="farefinder-routeinfo-currentcondition-accordion2"></div>
	<div class="hidden" id="farefinder-routeinfo-currentcondition-accordion1"></div>
	<div class="hidden" id="farefinder-routeinfo-vacation-accordion1"></div>
	<div class="hidden" id="farefinder-routeinfo-vacation-accordion2"></div>
	<div class="hidden" id="to">
		<div class="input-required-wrap">
			<label for="toLocation" class="home-label-mobile">
				<spring:theme
					code="label.ferry.farefinder.routeinfo.return.to" text="TO" />
			</label>
			<form:input type="text" id="toLocation"
				path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocationName"
				cssErrorClass="fieldError"
				class=" y_fareFinderDestinationLocation bc-dropdown bc-dropdown--big "
				placeholder="${locationPlaceholder}" autocomplete="off"
				 />
			<form:hidden
				path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocation"
				class=" y_fareFinderDestinationLocationCode" maxlenght="5" />
			<form:hidden
				path="${fn:escapeXml(formPrefix)}routeInfoForm.arrivalLocationSuggestionType"
				class="y_fareFinderDestinationLocationSuggestionType" />
			<div id="y_fareFinderDestinationLocationSuggestions"
				class="autocomplete-suggestions-wrapper hidden"></div>
		</div>
	</div>
	<div class="bc-accordion ferry-drop pr0">
		<div id="js-roundtrip">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item ui-depart">
					<label for="departingDateTime" class="home-label-mobile">
						<spring:theme
							code="label.ferry.farefinder.routeinfo.depart.date" text="DEPART" />
					</label>

					<a class="nav-link p-0 component-to-top" data-toggle="tab" href="#home">
						<div class="vacation-calen-box">
							<span class="vacation-calen-date-txt">Date</span>
							<span class="vacation-calen-year-txt current-year-custom">${routeInfoForm.departingDateTime}</span>
							<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
								aria-hidden="true"></i>
						</div>
					</a>
				</li>
				<li class="nav-item ui-return disabled-nopointer ${(isModifyingTransportBooking and cmsPage.uid eq 'RouteSelectionPage') ? 'hidden' : ''}">
					<label for="returnDateTime"
						class="home-label-mobile font-weight-light">
						<spring:theme
							code="label.ferry.farefinder.routeinfo.return.date" text="RETURN" />
					</label>
					<a class="nav-link p-0 component-to-top" data-toggle="tab" href="#menu1">
						<div class="vacation-calen-box">
							<span class="vacation-calen-date-txt">Date</span>
							<span class="vacation-calen-year-txt current-year-custom">${routeInfoForm.returnDateTime}</span>
							<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
								aria-hidden="true"></i>
						</div>
					</a>
				</li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div id="home"
					class=" tab-pane input-required-wrap  depart-calendar">
					<div class="bc-dropdown calender-box" id="js-return">
						<label><spring:theme
							code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>
						<form:input type="text"
							path="${fn:escapeXml(formPrefix)}routeInfoForm.departingDateTime"
							class=" bc-dropdown depart datepicker-input form-control"
							placeholder="${datePlaceholder}" autocomplete="off"
							 />
						<div class="datepicker bc-dropdown--big  y_${fn:escapeXml( idPrefix )}FinderDatePickerDeparting y_transportDepartDate"
							id="check-in-datepicker" aria-describedby="check-in-date"></div>
					</div>
				</div>
				<c:choose>
					<c:when
						test="${(routeInfoForm.tripType == 'RETURN') || (routeInfoForm.tripType == null)}">
						<c:set var="showReturnOptions" scope="session" value="true" />
					</c:when>
					<c:otherwise>
						<c:set var="showReturnOptions" scope="session" value="false" />
					</c:otherwise>
				</c:choose>
				<div id="menu1"
					class=" tab-pane fade y_fareFinderReturnField input-required-wrap return-calendar">
					<div class="bc-dropdown calender-box">
						<label><spring:theme
							code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>
						<form:input type="text"
							path="${fn:escapeXml(formPrefix)}routeInfoForm.returnDateTime"
							class="returnOption bc-dropdown arrive datepicker-input form-control"
							placeholder="${datePlaceholder}" autocomplete="off"
						 />
						<div
							class="returnOption bc-dropdown--big  datepicker  y_${fn:escapeXml( idPrefix )}FinderDatePickerReturning y_transportReturnDate"
							id="check-out-datepicker" aria-describedby="check-out-date"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
    <c:if test="${!isFareFinderComponent}">
        <div class="confirm-btn-bookable">
          <button id="y_confirmaddpassenger" class="btn btn-primary btn-block" type="submit" value="Submit">
            <spring:theme code="text.ferry.farefinder.routeinfo.proceed.to.passenger.selection.page" text="CONFIRM AND ADD PASSENGERS" />
          </button>
        </div>
    </c:if>
</div>
