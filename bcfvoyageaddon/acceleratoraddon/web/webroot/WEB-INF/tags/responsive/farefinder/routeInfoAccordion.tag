<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="routeInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="custom-error-1 alert alert-info alert-dismissable hidden"><span class="bcf bcf-icon-notice-outline bcf-2x"></span> <span><b><spring:theme code="error.ferry.farefinder.routeinfo.nonBookable.header" text="You've chosen a non-reservable route." /></span></b> <span><spring:theme code="error.ferry.farefinder.routeinfo.nonBookable.text" text="All purchases can be made at the terminal on the day of travel." /></span></div>
<div class="custom-error-2 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>
<label>*<spring:theme code="label.ferry.farefinder.routeinfo.bookableOnline" text="Some routes shown are not bookable online" /></label>

<div class="row m-0 p-0 mt-30-mob">
    <div class="col-lg-12 col-md-12">

    <c:if test="${empty reservable or reservable eq 'true'}">
        <div id="returnfare" value="RETURN" name="return"
            class="radio-btn-bx padding-0-mobile ${routeInfoForm.tripType == 'RETURN' ? 'color-tab-active' : 'color-tab-disble'} y_SelectTripTypefare">
            <label for="roundTripRadbtn" class="custom-radio-input">
                <spring:theme
                    code="label.ferry.farefinder.routeinfo.triptype.roundtrip"
                    text="Return" />

                <form:radiobutton id="y_roundTripRadbtn"
                    path="${fn:escapeXml(formPrefix)}routeInfoForm.tripType"
                    value="RETURN" class="y_fareFinderTripTypeBtn" />
                <span class="checkmark"></span>
            </label>
        </div>
	</c:if>
	<div value="ONE WAY" id="onewayfare" name="oneway"
		class="radio-btn-bx padding-0-mobile ${routeInfoForm.tripType == 'SINGLE' ? 'color-tab-active' : 'color-tab-active'} y_SelectTripTypefare"
		id="returnfare" value="RETURN">
		<label for="roundTripRadbtn" class="custom-radio-input">
			<spring:theme
				code="label.ferry.farefinder.routeinfo.triptype.oneway"
				text="One-way" />

			<form:radiobutton id="y_oneWayRadbtn"
				path="${fn:escapeXml(formPrefix)}routeInfoForm.tripType"
				value="SINGLE" class="y_fareFinderTripTypeBtn" />
			<span class="checkmark"></span>
		</label>
	</div>
	</div>
</div>

<farefinder:routeFields routeInfoForm="${routeInfoForm}" formPrefix="${formPrefix}" idPrefix="${idPrefix}" isFareFinderComponent="true" />

