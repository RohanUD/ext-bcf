<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="fareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm"%>
<%@ attribute name="tripType" required="true" type="java.lang.String"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${idPrefix }dangerousGoodCheckbox">
	<label id="${idPrefix }dangerousgoodoutbound"> <form:checkbox id="${idPrefix }dangerousgoodoutboundcheckbox" path="${fn:escapeXml(formPrefix)}carryingDangerousGoodsInOutbound" class="${idPrefix }y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.dangerousgoodoutbound"
			text="Carrying Dangerous Goods" />
	</label>
</div>
<div class="${idPrefix }dangerousGoodReturnCheckBox hidden">
	<label id="${idPrefix }dangerousgoodinbound"> <form:checkbox id="${idPrefix }dangerousgoodoutboundcheckbox" path="${fn:escapeXml(formPrefix)}carryingDangerousGoodsInReturn" class="${idPrefix }y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.dangerousgoodinbound" text="Carrying Dangerous Goods" />
	</label>
</div>
<div class="y_vehicleInfoError col-md-12 vehicle-error"></div>
<div class="travellingWithVehicleCheckBox">
	<label id="${idPrefix }travellingWithVehicle"> <form:checkbox id="${idPrefix }travellingWithVehiclecheckbox" path="${fn:escapeXml(formPrefix)}travellingWithVehicle" /> <spring:theme code="text.cms.farefinder.vehicletype.travellingWithVehicle" text="Travelling with a Vehicle" />
	</label>
</div>
<div class="container p-0 ${idPrefix }vehicleDetails ${fareFinderForm.travellingWithVehicle eq true ?'':'hidden'}">
	<div class="${idPrefix }differentVehicleCheckBox ${fareFinderForm.returnWithDifferentVehicle eq true || (fareFinderForm.travellingWithVehicle eq true && tripType=='RETURN') ?'':'hidden'}">
		<label id="${idPrefix }vehicleinbound"> <form:checkbox id="${idPrefix }vehicleinboundcheckbox" path="${fn:escapeXml(formPrefix)}returnWithDifferentVehicle" class="${idPrefix }y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.vehicletype.returnvehicle" text="Different vehicle for return journey" />
		</label>
	</div>
	<vehicle:vehicleTypes formPrefix="${formPrefix}" returnWithDifferentVehicle="${fareFinderForm.returnWithDifferentVehicle}" vehicleTypeQuantityList="${fareFinderForm.vehicleInfo}" idPrefix="${idPrefix}"/>
	<label id="${idPrefix }vehicledangerous" class="hidden"> <input type="checkbox" id="y_dangerous${idPrefix }" class="${idPrefix }y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.vehicletype.Dangerousgoods" text="Dangerous goods" />
	</label>
</div>
