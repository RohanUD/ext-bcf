<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="routeInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.RouteInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="farefinder" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/farefinder"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<%-- / One way / return radio --%>
<%-- From / To input fields --%>
<div class="custom-error-2 alert alert-danger alert-dismissable hidden"><span class="icon-alert"></span> <span></span></div>
<h4>
   <strong>
      <spring:theme code="text.ferry.farefinder.routeinfo.header.selection" text="Date & destination" />
      <spring:theme code="text.ferry.farefinder.routeinfo.header.selection.message" text="Route selection message" var="routeSelectionMessage"/>
   </strong>
   <a href="javascript:;" data-container="body" data-toggle="popover"
      data-placement="bottom" data-html="true" title="" class="popoverThis" data-content="${fn:escapeXml(routeSelectionMessage)}">
   <span class="bcf bcf-icon-info-solid"></span>
   </a>
</h4>
<label class="mb-0">
   *
   <spring:theme code="label.ferry.farefinder.routeinfo.bookableOnline" text="Some routes shown are not bookable online" />
</label>
<div class="custom-error-1 alert alert-info alert-dismissable hidden">
   <span class="bcf bcf-icon-notice-outline bcf-2x"></span>
   <span>
      <b>
         <spring:theme code="error.ferry.farefinder.routeinfo.nonBookable.header" text="You've chosen a non-reservable route." />
   </span>
   </b>
   <span>
      <spring:theme code="error.ferry.farefinder.routeinfo.nonBookable.text" text="All purchases can be made at the terminal on the day of travel." />
   </span>
</div>
<div class="booking-selector ${(isModifyingTransportBooking and cmsPage.uid eq 'RouteSelectionPage') ? 'hidden' : ''}">
   <div  value="ONE WAY" id="onewayfare" name="oneway" class="radio-btn-bx padding-0-mobile ${routeInfoForm.tripType == 'SINGLE' ? 'color-tab-active' : 'color-tab-active'} y_SelectTripType"  id="returnfare" value="RETURN">
      <label for="roundTripRadbtn" class="custom-radio-input">
         <spring:theme code="label.ferry.farefinder.routeinfo.triptype.oneway" text="One-way" />
         <form:radiobutton id="y_oneWayRadbtn" path="${fn:escapeXml(formPrefix)}routeInfoForm.tripType" value="SINGLE" class="y_fareFinderTripTypeBtn" />
         <span class="checkmark"></span>
      </label>
   </div>
   <div id="returnfare" value="RETURN" name="return" class="radio-btn-bx padding-0-mobile ${routeInfoForm.tripType == 'RETURN' ? 'color-tab-active' : 'color-tab-disble'} y_SelectTripType"  >
      <label for="roundTripRadbtn" class="custom-radio-input">
         <spring:theme code="label.ferry.farefinder.routeinfo.triptype.roundtrip" text="Return" />
         <form:radiobutton id="y_roundTripRadbtn" path="${fn:escapeXml(formPrefix)}routeInfoForm.tripType" value="RETURN" class="y_fareFinderTripTypeBtn" />
         <span class="checkmark"></span>
      </label>
   </div>
</div>
<fareselection:addBundleToCartValidationModal />
<farefinder:routeFields routeInfoForm="${routeInfoForm}" formPrefix="${formPrefix}" idPrefix="${idPrefix}" isFareFinderComponent="false" />
<input type="hidden" id="y_moreThanMaxAllowed" value="${moreThanMaxAllowed}" />
<div class="hidden" id="long-route-calender-limit">${longRouteCalenderLimit}</div>
<div class="hidden" id="short-route-calender-limit">${shortRouteCalenderLimit}</div>
<div class="clearfix">
   <p class="confirm-btn-non-bookable hidden mb-3">
      <spring:theme code="error.ferry.farefinder.routeinfo.nonBookable.nonReservable" text="This route is not reservable online. Would you like to:" />
   </p>
</div>
<input id="readOnlyResults" name="readOnlyResults" value="false" type="hidden"/>
<div class="row confirm-btn-non-bookable hidden mb-3">
   <div class="col-lg-4 col-md-4 col-sm-12 mb-2">
      <button class=" confirm-btn confirm-btn--blue " type="button" value="Submit"
         onclick="ACC.farefinder.scheduleListingHandler(this)" data-link="routes-fares/schedules/">
         <spring:theme code="label.ferry.farefinder.routeinfo.continue.to.viewSchedule" text="Continue to view schedule" />
      </button>
   </div>
   <div class="col-lg-4 col-md-4 col-sm-12 mb-2 paddingLeftOff">
      <button class="btn btn-block btn-outline-blue margin-top-30-mobile" type="button" value="Submit" onclick="ACC.farefinder.calculateFareHandler(this)" data-link="routes-fares/fare-calculator">
         <spring:theme code="label.ferry.farefinder.routeinfo.continue.to.calculateFare" text="Calculate fare" />
      </button>
   </div>
</div>
<%-- / From / To input fields --%>
<%-- One way / return radio --%>
