<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="container mt-5">
	<c:set var="formPrefix" value="" />
	<c:set var="loadAccessibilitiesBox" value="true" />
    <div id="loadAccessibilitiesBox" class="hidden">${loadAccessibilitiesBox}</div>
	<passenger:passengerselection formPrefix="${formPrefix}" isFareFinderComponent="true" />
</div>
