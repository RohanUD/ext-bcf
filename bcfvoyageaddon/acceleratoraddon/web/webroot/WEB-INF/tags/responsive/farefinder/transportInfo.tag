<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="tripType" required="true" type="java.lang.String"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<%-- / One way / return radio --%>
<%-- From / To input fields --%>
<div class="container p-0">
	<div class="row input-row">
		<div class="input-required-wrap col-xs-12 col-sm-4">
			<label for="from${idPrefix}Location" class="home-label-mobile"> FROM <spring:theme var="departureLocationPlaceholderText" code="text.cms.farefinder.departure.location.placeholder" text="From" />
			</label>
			<form:input type="text" id="from${idPrefix}Location" path="${fn:escapeXml(formPrefix)}departureLocationName" cssErrorClass="fieldError" class="y_${idPrefix}FinderOriginLocation input-grid col-xs-12 form-control" placeholder="${fn:escapeXml(departureLocationPlaceholderText)}" autocomplete="off" />
			<form:hidden path="${fn:escapeXml(formPrefix)}departureLocation" class="y_${idPrefix}FinderOriginLocationCode" maxlenght="5" />
			<form:hidden path="${fn:escapeXml(formPrefix)}departureLocationSuggestionType" class="y_${idPrefix}FinderOriginLocationSuggestionType" />
			<div id="y_${idPrefix}FinderOriginLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
		</div>
		<div class="input-required-wrap col-xs-12 col-sm-4">
			<label for="to${idPrefix}Location" class="home-label-mobile"> TO <spring:theme var="arrivalLocationPlaceholderText" code="text.cms.farefinder.arrival.location.placeholder" text="To" />
			</label>
			<form:input type="text" id="to${idPrefix}Location" path="${fn:escapeXml(formPrefix)}arrivalLocationName" cssErrorClass="fieldError" class="col-xs-12 y_${idPrefix}FinderDestinationLocation input-grid form-control" placeholder="${fn:escapeXml(arrivalLocationPlaceholderText)}" autocomplete="off" />
			<form:hidden path="${fn:escapeXml(formPrefix)}arrivalLocation" class=" y_${idPrefix}FinderDestinationLocationCode" maxlenght="5" />
			<form:hidden path="${fn:escapeXml(formPrefix)}arrivalLocationSuggestionType" class="y_${idPrefix}FinderDestinationLocationSuggestionType" />
			<div id="y_${idPrefix}FinderDestinationLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
		</div>
		<div class=" col-xs-12 col-sm-4">
			<label for="fromLocation"> &nbsp; </label>
			<h3 class="mb-0 mt-1 blue-color">
				<i class="fa fa-map-o ml-4 text-right" aria-hidden="true"></i> <b>Route map</b>
			</h3>
		</div>
	</div>
</div>
<%-- / From / To input fields --%>
<%-- One way / return radio --%>
<div class="container p-0">
	<div class="row input-row mt-3" id="js-roundtrip">
		<div class="col-sm-4">
			<label for="departingDateTime">Trip Type <spring:theme var="departingDatePlaceholderText" code="text.cms.farefinder.departure.date.placeholder" text="Departure Date" />
			</label>
			<div>
				<button id="oneway${fn:escapeXml(idPrefix)}" name="oneway" class="btn btn-primary ${tripType == 'SINGLE' ? 'color-tab-active' : 'color-tab-disble'}  oneway col-sm-6 y_SelectTripType${fn:escapeXml(idPrefix)}" placeholder="" type="button" value="ONE WAY">
					<form:radiobutton id="y_oneWayRadbtn${fn:escapeXml(idPrefix)}" path="${fn:escapeXml(formPrefix)}tripType" value="SINGLE" class="y_fareFinderTripTypeBtn" />
					<label for="oneWayRadbtn${fn:escapeXml(idPrefix)}"> <spring:theme code="text.cms.farefinder.triptype.oneway" text="One Way" />
					</label>
				</button>
				<button id="return${fn:escapeXml(idPrefix)}" name="return" class="btn btn-primary ${tripType == 'RETURN' ? 'color-tab-active' : 'color-tab-disble'} return col-sm-6 y_SelectTripType${fn:escapeXml(idPrefix)}" placeholder="" type="button" value="RETURN">
					<form:radiobutton id="y_roundTripRadbtn${fn:escapeXml(idPrefix)}" path="${fn:escapeXml(formPrefix)}tripType" value="RETURN" class="y_fareFinderTripTypeBtn" />
					<label for="roundTripRadbtn${fn:escapeXml(idPrefix)}"> <spring:theme code="text.cms.farefinder.triptype.roundtrip" text="Return" />
					</label>
				</button>
			</div>
		</div>
		<%-- Departing / Returning dates input fields --%>
		<div class="input-required-wrap  col-xs-12 col-sm-4" id="js-return">
			<label for="departingDateTime" class="home-label-mobile">Departure <spring:theme var="departingDatePlaceholderText" code="text.cms.farefinder.departure.date.placeholder" text="Departure Date" />
			</label>
			<form:input type="text" path="${fn:escapeXml(formPrefix)}departingDateTime" class="col-xs-12 datepicker input-grid form-control y_${idPrefix}FinderDatePickerDeparting y_transportDepartDate" placeholder="${fn:escapeXml(departingDatePlaceholderText)}" autocomplete="off" />
		</div>
		<c:choose>
			<c:when test="${(tripType == 'RETURN') || (tripType == null)}">
				<c:set var="showReturnOptions" scope="session" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="showReturnOptions" scope="session" value="false" />
			</c:otherwise>
		</c:choose>
		<div class="y_${idPrefix}FinderReturnField input-required-wrap col-xs-12 col-sm-4" style="display: ${showReturnOptions ? 'block' : 'none'};">
			<label for="returnDateTime" class="home-label-mobile">Return <spring:theme var="returnDatePlaceholderText" code="text.cms.farefinder.return.date.placeholder" text="Return Date" />
			</label>
			<form:input type="text" path="${fn:escapeXml(formPrefix)}returnDateTime" class="returnOption col-xs-12 datepicker input-grid form-control y_${idPrefix}FinderDatePickerReturning y_transportReturnDate" placeholder="${fn:escapeXml(returnDatePlaceholderText)}" autocomplete="off" />
		</div>
	</div>
</div>
<%-- Departing / Returning dates input fields --%>
<div class="row input-row less-margin" style="display: none">
	<div class="col-xs-12 col-sm-6">
		<div>
			<label class="sr-only" for="flightClass"> <spring:theme var="defaultCabinClassText" code="text.cms.farefinder.default.cabin.class" text="Select a Class" />
			</label>
			<form:select class="form-control sr-only" id="flightClass" path="${fn:escapeXml(formPrefix)}cabinClass" multiple="no">
				<form:option value="default" disabled="true">${fn:escapeXml(defaultCabinClassText)}</form:option>
				<form:options items="${cabinClasses}" itemValue="code" itemLabel="name" htmlEscape="true" />
			</form:select>
			<c:set var="specialAssistanceText">
				<spring:theme code="text.cms.farefinder.special.assistance"
					text="Please contact us at least 48 hours before your flight departs in order to help us make your journey as easy as possible even if your flight is operated by one of our airline alliance or franchise partners because they may have different restrictions. If you have any questions please call us. To ensure we can provide the best possible service." />
			</c:set>
			<a tabindex="0" class="link-trigger" role="button" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-container="body" data-content="${fn:escapeXml(specialAssistanceText)}"> <spring:theme code="text.cms.farefinder.specialrequirements" text="Special Assistance" /> &nbsp;<i
				class="glyphicon glyphicon-info-sign"></i>
			</a>
		</div>
	</div>
</div>
