<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="stage" required="true" type="java.lang.String"%>
<%@ attribute name="fareFinderJourney" required="true"
	type="java.lang.String"%>
<%@ attribute name="amend" required="false" type="java.lang.Boolean"%>
<%@ attribute name="routeType" required="false" type="java.lang.String"%>
<%@ attribute name="bcfFareFinderForm" required="false"
	type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<section>
	<c:set var="routeType" value="NOTLONG" />
	<c:url value="/RouteSelectionPage" var="routePagePath" />
	<c:url value="/PassengerSelectionPage" var="passengerPath" />
	<c:url value="/VehicleSelectionPage" var="vehiclePath" />
	<c:url value="/fare-selection" var="sailingsPath" />
	<c:url value="/ancillary" var="optionsPath" />
	<c:url value="/checkout/multi/payment-method/add" var="paymentPath" />
	<c:url value="/checkout/ferry-option-booking-details" var="tentativeConfirmationPath" />
	<c:url value="/traveller-details" var="passengerInfo" />
	<div class="package-wizard">
		<div class="container">
			<ul class="nav-wizard ${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn eq true ? 'vehicle-disable' : ''}">
				<c:choose>
					<c:when test="${amend}">
						<li
							class=" ${!(stage eq 'payment') && !(stage eq 'confirmation') ? 'active' : ''}">
							<span class="nav-wizard-text"> <spring:theme
									code="text.booking.progress.bar.amend" text="Amend Selections" />
						</span>
						</li>
						<li class=" ${stage eq 'payment' ? 'active' : ''}"><span
							class="nav-wizard-text"> <spring:theme
									code="text.booking.progress.bar.paymentrefund"
									text="Payment/Refund" />
						</span></li>
						<li class=" ${stage eq 'confirmation' ? 'active' : ''}"><span
							class="nav-wizard-text"> <spring:theme
									code="text.booking.progress.bar.confirmation"
									text="Confirmation" />
						</span></li>
					</c:when>
					<c:otherwise>
						<li class=" ${stage eq 'selectroute' ? 'active' : ''}">
						<div class="display-iflex">
						<a href="${routePagePath}">1</a>
						<div>
						<c:choose>
									<c:when
										test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
                                        <span class="display-block"> <spring:theme
                                                    code="text.ferry.farefinder.long.route.step" text="STEP 1 OF 7" arguments="1" />
                                        </span>
                                         <span class="nav-wizard-text">
										<spring:theme
											code="text.ferry.farefinder.northern.routeinfo.progress.bar.selectroute"
											text="Date & destination" />
											</span>
									</c:when>
									<c:otherwise>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.short.route.step" text="STEP 1 OF 5" arguments="1" />
                                    </span>
                                     <span class="nav-wizard-text">
										<spring:theme
											code="text.ferry.farefinder.core.routeinfo.progress.bar.selectroute"
											text="Date and destination" />
											</span>
									</c:otherwise>
								</c:choose>

						</div>
						</div>
						</li>
						<c:if test="${stage ne 'selectroute'}">
						<li class=" ${stage eq 'selectpassenger' ? 'active' : ''}">
						<div class="display-iflex">
						<a
							href="${stage eq 'selectpassenger' or stage eq 'selectvehicle' or stage eq  'Sailings' or stage eq 'options' or stage eq 'passengerInfo' or stage eq 'payment'? passengerPath: routePagePath}">2
						</a>
						<div>
						 <c:choose>
									<c:when
										test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 2 OF 7" arguments="2" />
                                    </span>
                                    <span class="nav-wizard-text">
										<spring:theme
											code="text.ferry.farefinder.northern.passengerinfo.progress.bar.selectpassenger"
											text="Passenger(s)" />
											</span>
									</c:when>
									<c:otherwise>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.short.route.step" text="STEP 2 OF 5" arguments="2" />
                                    </span>
                                    <span class="nav-wizard-text">
										<spring:theme
											code="text.ferry.farefinder.core.passengerinfo.progress.bar.selectpassenger"
											text="Add passengers" />
											</span>
									</c:otherwise>
								</c:choose>

						</div>
						  </div>
						</li>
						<c:if test="${stage ne 'selectpassenger'}">
						<li class=" ${stage eq 'selectvehicle' ? 'active' : ''} disbled-v">
						<div class="display-iflex">
						<c:choose>
								<c:when
									test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
									<a
										href="${stage eq 'selectvehicle' or stage eq  'Sailings' or stage eq 'options' or stage eq 'passengerInfo' or stage eq 'payment' ? vehiclePath: routePagePath}">3</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 3 OF 7" arguments="3" />
                                    </span>
									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.northern.vehicleinfo.progress.bar.selectvehicle"
											text="Vehicle" />
									</span>
									</div>
								</c:when>
								<c:otherwise>
									<a
										href="${stage eq 'selectvehicle' or stage eq  'Sailings' or stage eq 'payment' ? vehiclePath: routePagePath}">3</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.short.route.step" text="STEP 3 OF 5" arguments="3" />
                                    </span>
									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.core.vehicleinfo.progress.bar.selectvehicle"
											text="Vehicle details" />
									</span>
									</div>
								</c:otherwise>
							</c:choose>
							</div>
							</li>
							<c:if test="${stage ne 'selectvehicle'}">
						<li class=" ${stage eq 'Sailings' ? 'active' : ''}">
						<div class="display-iflex">
						<c:choose>
								<c:when
									test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
									<a
										href="${stage eq  'Sailings' or stage eq 'options' or stage eq 'passengerInfo' or stage eq 'payment' ? sailingsPath: routePagePath}">4</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 4 OF 7" arguments="4" />
                                    </span>
									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.core.sailing.progress.bar.selectSailing"
											text="Sailing(s)" />
									</span>
									</div>
								</c:when>
								<c:otherwise>
									<a
										href="${stage eq  'Sailings' or stage eq 'options' or stage eq 'passengerInfo' or stage eq 'payment' ? sailingsPath: routePagePath}">4</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.short.route.step" text="STEP 4 OF 5" arguments="4" />
                                    </span>
									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.northern.sailing.progress.bar.selectSailing"
											text="Sailing(s)" />
									</span></div>
								</c:otherwise>
							</c:choose>

							</div>

							</li>
							<c:if test="${stage ne 'Sailings'}">
						<c:if
							test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
							<li>
							<div class="display-iflex">
							<a
								href="${stage eq 'options' or stage eq 'passengerInfo' or stage eq 'payment' ? optionsPath: routePagePath}">5</a>
								<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 5 OF 7" arguments="5" />
                                    </span><span class="nav-wizard-text"> <spring:theme
										code="text.ferry.farefinder.northern.sailing.progress.bar.selectSailing.options"
										text="Options" />
							</span>
							</div>
							</div>
							</li>



							<c:if test="${stage ne 'options'}">
							<li class=" ${stage eq 'passengerInfo' ? 'active' : ''}"><div class="display-iflex"><a
								href="${stage eq 'passengerInfo'  or stage eq 'payment' ? passengerInfo: routePagePath}">6</a>
								<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 6 OF 7" arguments="6" />
                                    </span>
                                    <span class="nav-wizard-text"> <spring:theme
										code="text.ferry.farefinder.northern.passengerinfo.progress.bar.selectpassenger.passengerinfo"
										text="Passenger info" />
							</span></div></div></li>
							</c:if>
						</c:if>
						<c:if test="${stage eq 'payment' or stage eq 'confirmOptional'}">
						<li class=" ${stage eq 'payment' or stage eq 'confirmOptional' ? 'active' : ''}">
						<div class="display-iflex">

						<c:choose>
								<c:when test="${isFerryOptionBooking}">
									<a href="${stage eq 'confirmOptional'? tentativeConfirmationPath : routePagePath}">5</a>
									<div>
										<c:choose>
											<c:when test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
												<span class="display-block"> <spring:theme code="text.ferry.farefinder.long.route.step" text="STEP 7 OF 7" arguments="7" />
												</span>
											</c:when>
											<c:otherwise>
												<span class="display-block"> <spring:theme code="text.ferry.farefinder.short.route.step" text="STEP 5 OF 5" arguments="5" />
												</span>
											</c:otherwise>
										</c:choose>
										<span class="nav-wizard-text"> <spring:theme code="text.ferry.farefinder.core.confirm.progress.bar.select.optional.confirm" text="Confirm" />
										</span>
									</div>
								</c:when>
								<c:when
									test="${bcfFareFinderForm.routeInfoForm.routeType == 'LONG'}">
									<a href="${stage eq 'payment'? paymentPath: routePagePath}">7</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.long.route.step" text="STEP 7 OF 7" arguments="7" />
                                    </span>
									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.northern.confirmPay.progress.bar.selectconfirm"
											text="Review and Pay" />
									</span>
									</div>
								</c:when>
								<c:otherwise>
									<a href="${stage eq 'payment' ? paymentPath : routePagePath}">5</a>
									<div>
									<span class="display-block"> <spring:theme
                                        code="text.ferry.farefinder.short.route.step" text="STEP 5 OF 5" arguments="5" />
                                    </span>

									<span class="nav-wizard-text"> <spring:theme
											code="text.ferry.farefinder.core.confirmPay.progress.bar.selectconfirm"
											text="Confirm and pay" />
									</span>
									</div>
								</c:otherwise>
							</c:choose>
							</div>

							</li>
							</c:if>
					</c:if>
					</c:if>
					</c:if>
					</c:if>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</section>
<div class="clearfix"></div>
