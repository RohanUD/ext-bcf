<%@ attribute name="locationName" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="parts" value="${fn:split(locationName, '(')}" />
<c:choose>
	<c:when test="${fn:length(parts) gt 1}">
		<c:set var="partOne" value="${fn:trim(parts[0])}" />
		<c:set var="partTwo" value="${fn:trim(parts[1])}" />
		<p class="mb-0">
			<strong>${partOne}</strong>&nbsp;(${partTwo}
		<p>
	</c:when>
	<c:otherwise>
		<p class="mb-0">
			<strong>${locationName}</strong>
		<p>
	</c:otherwise>
</c:choose>
