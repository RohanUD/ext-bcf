<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="passengerTypeQuantityList" required="true" type="java.util.List"%>
<%@ attribute name="accessibilityRequestDataList" required="true" type="java.util.List"%>
<%@ attribute name="vehicleInfo" required="true" type="com.bcf.facades.ferry.VehicleTypeQuantityData"%>

<c:set var="passengerCount" value="0"/>
<c:forEach var="passenger" items="${passengerTypeQuantityList}" varStatus="i">
   <c:if test="${passenger.quantity > 0 }">
      <c:set var="passengerCount" value="${passengerCount + passenger.quantity}"/>
   </c:if>
</c:forEach>
<c:if test="${passengerCount gt 0 }">
   <span class="bcf bcf-icon-account bluetrip-icon"></span>
   <span class="caps-f quantity">
   ${passengerCount}
   </span>
</c:if>
<c:set var="specialServiceCount" value="0"/>
<c:set var="ctrWHP" value="0"/>
<c:set var="ancillaryIcon"/>
<c:forEach var="accessibility" items="${accessibilityRequestDataList}" varStatus="i">
   <c:forEach var="specialService" items="${accessibility.specialServiceRequestDataList}" varStatus="i">
      <c:if test="${specialService.selection}">
         <c:set var="specialServiceCount" value="${specialServiceCount + 1}"/>
      </c:if>
   </c:forEach>
   <c:forEach var="ancillary" items="${accessibility.ancillaryRequestDataList}" varStatus="i">
      <c:if test="${ancillary.code eq accessibility.selection }">
         <c:set var="ctrWHP" value="${ctrWHP + 1}"/>
         <c:set var="ancillaryIcon" value="${accessibility.selection}"/>
      </c:if>
   </c:forEach>
</c:forEach>
<c:if test="${specialServiceCount gt 0}">
   <span class="bcf bcf-icon-wheelchair bluetrip-icon"></span>
   <span class="caps-f quantity">
   ${specialServiceCount}
   </span>
</c:if>
<c:if test="${ctrWHP > 0}">
   <span class="quantity"><span class="${ancillaryIcon}"></span>${ctrWHP}</span>
</c:if>
<c:if test="${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn eq false}">
   <c:set var="vehicleInfo" value="${vehicleInfo}" />
   <c:if test="${not empty vehicleInfo.vehicleType.code}">
      <c:set var="vehicleCount" value="1"/>
      <span class="quantity"><span class="${vehicleInfo.vehicleType.code}"></span>&nbsp;${vehicleCount}</span>
   </c:if>
</c:if>
