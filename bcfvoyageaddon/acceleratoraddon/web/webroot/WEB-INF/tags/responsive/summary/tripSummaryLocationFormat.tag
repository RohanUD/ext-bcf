<%@ attribute name="locationName" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="locationParts" value="${fn:split(locationName, '-')}" />
<c:if test="${fn:length(locationParts) gt 1}">
    <c:set var="locationSecondPart" value="${fn:trim(locationParts[1])}" />
    <c:set var="locationParts" value="${fn:split(locationSecondPart, '(')}" />
    <c:set var="locationName" value="${fn:trim(locationParts[0])}" />
    <c:set var="portNames" value="${fn:trim(locationParts[1])}" />
    <c:set var="portNamesParts" value="${fn:split(portNames, ')')}" />
    <p class="mb-0"><strong>${locationName}</strong><p>
    <span>(${portNamesParts[0]})</span>
</c:if>

