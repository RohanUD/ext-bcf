<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>

<c:choose>
   <c:when test="${not empty isReturn and isReturn}">
      <div class="row">
         <div class="col-lg-6 col-xs-6 text-center">
            <p class="selacted-date-text mb-1">${bcfFareFinderForm.routeInfoForm.returnDateTime}</p>
            <summary:tripSummaryLocationFormat locationName="${bcfFareFinderForm.routeInfoForm.arrivalLocationName}"/>
         </div>
         <div class="col-lg-6 col-xs-6 text-center">
            <summary:tripSummaryLocationFormat locationName="${bcfFareFinderForm.routeInfoForm.departureLocationName}"/>
         </div>
      </div>
   </c:when>
   <c:otherwise>
      <div class="row">
         <div class="col-lg-6 col-xs-6 text-center">
            <summary:tripSummaryLocationFormat locationName="${bcfFareFinderForm.routeInfoForm.departureLocationName}"/>
         </div>
         <div class="col-lg-6 col-xs-6 text-center">
            <summary:tripSummaryLocationFormat locationName="${bcfFareFinderForm.routeInfoForm.arrivalLocationName}"/>
         </div>
      </div>
   </c:otherwise>
</c:choose>
