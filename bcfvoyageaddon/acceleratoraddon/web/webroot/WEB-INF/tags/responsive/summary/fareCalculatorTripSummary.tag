<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>

<div class="passenger-panel trip-bg p-3">
   <div class="container pb-3">
      <div class="row">
         <div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6">
            <div class="row">
               <div class="col-lg-12 mb-1 text-center margin-bottom-10">
                  <a href="${fareCalculatorPageUrl}" class="edit_journey_fare_selection">
                     <span>
                        <i class="bcf bcf-icon-edit"></i>
                        <spring:theme code="text.ferry.farefinder.edit.journey" text="edit" />
                     </span>
                  </a>
                  <summary:ferryJourneyTripTypeInfo/>
               </div>
            </div>
            <summary:tripSummaryDepartureArrivalInfo/>
         </div>
      </div>
   </div>
</div>
<summary:tripSummaryPassengerVehiclePerTrip/>
