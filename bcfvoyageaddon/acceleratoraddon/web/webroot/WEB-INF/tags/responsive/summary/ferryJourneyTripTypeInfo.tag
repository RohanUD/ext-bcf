<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<p class="selacted-date-text mb-1 selected-journey-trip">
 <spring:theme code="label.ferry.farefinder.routeinfo.depart.date" text="Depart"/>:
 <fmt:parseDate value="${bcfFareFinderForm.routeInfoForm.departingDateTime}" pattern="MM/dd/yyyy" var="departingDateTime" />
 <fmt:formatDate pattern="E, MMM d" value="${departingDateTime}" />
</p>
<c:set var="tripTypeSingle" value="SINGLE"/>
<c:set var="tripTypeReturn" value="RETURN"/>
<c:set var="travelRouteCode" value="${travelRoute.routeCode}"/>
<c:if test="${bcfFareFinderForm.routeInfoForm.tripType eq tripTypeReturn}">
 <p class="selacted-date-text mb-1 selected-journey-trip">
   <span class="tripType-divider">&nbsp;-&nbsp;</span>
    <spring:theme code="label.ferry.farefinder.routeinfo.return.date" text="Return"/>:
    <fmt:parseDate value="${bcfFareFinderForm.routeInfoForm.returnDateTime}" pattern="MM/dd/yyyy" var="returnDateTime" />
    <fmt:formatDate pattern="E, MMM d" value="${returnDateTime}" />
 </p>
</c:if>
<dd>
 <c:if test="${bcfFareFinderForm.routeInfoForm.tripType eq tripTypeSingle}">
    <span class="bcf bcf-icon-single-arrows-right return-tripType"></span>
 </c:if>
 <c:if test="${bcfFareFinderForm.routeInfoForm.tripType eq tripTypeReturn}">
    <span class="bcf bcf-icon-stacked-arrows-horizontal return-tripType"></span>
 </c:if>
</dd>
