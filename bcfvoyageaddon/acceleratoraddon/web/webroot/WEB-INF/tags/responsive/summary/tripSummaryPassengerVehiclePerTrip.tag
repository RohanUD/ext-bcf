<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>

<div class="info-box-detail-pass">
   <div class="container">
      <p class="text-center mb-0">
        <summary:tripSummaryPassengerVehicleInfo passengerTypeQuantityList="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}" accessibilityRequestDataList="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList}" vehicleInfo="${bcfFareFinderForm.vehicleInfoForm.vehicleInfo[0]}"/>
        <c:if test="${bcfFareFinderForm.passengerInfoForm.returnWithDifferentPassenger or bcfFareFinderForm.vehicleInfoForm.returnWithDifferentVehicle}">
            - <summary:tripSummaryPassengerVehicleInfo passengerTypeQuantityList="${bcfFareFinderForm.passengerInfoForm.inboundPassengerTypeQuantityList}" accessibilityRequestDataList="${bcfFareFinderForm.passengerInfoForm.inboundaccessibilityRequestDataList}" vehicleInfo="${bcfFareFinderForm.vehicleInfoForm.vehicleInfo[1]}"/>
        </c:if>
      </p>
   </div>
</div>
