<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="travellerForm" required="true" type="java.lang.String"%>
<%@ attribute name="idx" required="true" type="java.lang.Integer"%>
<%@ attribute name="isReturn" required="true" type="java.lang.Boolean"%>
<%@ attribute name="formValues" required="true" type="de.hybris.platform.commercefacades.travel.TravellerData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty formErrors}">
	<div id="formErrors" class="alert alert-danger">
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateNumber" element="div" />
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateCountry" element="div" />
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateProvince" element="div" />
	</div>
</c:if>
<div id="passenger-info-${fn:escapeXml(idx)}" class="y_passengerInformationForm mb-4">
	<input type="hidden" id="y_amendLicensePlateContry" value="${fn:escapeXml(formValues.travellerInfo.licensePlateCountry)}" /> <input type="hidden" id="y_amendLicensePlateprovince" value="${fn:escapeXml(formValues.travellerInfo.licensePlateProvince)}" />
	<div id="traveller-details-${fn:escapeXml(idx)}" class="">
		<div class="panel-form">
				<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mb-3">
						<p class="margin-top-10"><strong>
						<c:choose>
							<c:when test="${isReturn}">
								<spring:theme code="label.travellerdetails.form.return.vehicles.license.number" text="RETURN VEHICLE LICENSE PLATE NUMBER" />
							</c:when>
							<c:otherwise>
								<spring:theme code="label.travellerdetails.form.vehicles.license.number" text="VEHICLE LICENSE PLATE NUMBER" />
							</c:otherwise>
						</c:choose>
						</strong>
					</p>
						</div>
					<div class=" col-xs-12 col-sm-12 col-md-4 mb-3">
						<label class="custom-checkbox-input">
							<form:checkbox path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateNumberNotAvailable" id="vehiclePlateNumberCheck__${formValues.uid}" />
							<spring:theme code="label.travellerdetails.form.vehicles.license.consent.checkbox" text="Don't know license plate number" />
						    <span class="checkmark-checkbox"></span>
						</label>
					</div>
					</div>
					<div class="license-plate-informative warning-msg mb-3 hidden">
						<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
						<span class="text-sm"><spring:theme code="label.travellerdetails.form.vehicles.license.info" text="If unavailable at the time of booking licence plate number will be taken at the terminal." /></span>
					</div>
					<div class="y-vehicle-details__${formValues.uid} ${fn:escapeXml(formValues.travellerInfo.licensePlateNumberNotAvailable) ? 'hidden' : ''}">
					<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
                    <label>
                        <spring:theme code="label.travellerdetails.form.vehicles.license.plate.number" text="Licence plate number" />
                    </label>
					<label>&nbsp;</label>
					  <form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateNumber" id="y_travellerdetails_${fn:escapeXml(idx)}_license_number__${formValues.uid}" type="text" class="y_vehicleLicenseNumber form-control" placeholder="${fn:escapeXml(licenseNumberPlaceholder)}" />
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.vehicles.license.country" text="Licence plate issuing country" />
						</label>
						<form:select class="custom-select-passenger-page selectpicker y_vehicleLicenseCountry" id="y_travellerdetails_${fn:escapeXml(idx)}_licenseCountry__${formValues.uid}" path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateCountry" cssErrorClass="fieldError">
							<form:option value="-1" disabled="true" selected="selected"> <spring:theme code="text.payment.guest.country.dropdown" /> </form:option>
							<c:choose>
								<c:when test="${not empty countries}">
									<c:forEach var="entry" items="${countries}">
										<form:option value="${entry.isocode}"> ${entry.name} </form:option>
									</c:forEach>
								</c:when>
							</c:choose>
						</form:select>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 hidden mb-3" id="showRegion">
						<label id="showRegionState">
							<spring:theme code="label.travellerdetails.form.vehicles.license.state" text="Licence plate issuing state" />
						</label>
						<label id="showRegionProvince">
							<spring:theme code="label.travellerdetails.form.vehicles.license.province" text="Licence plate issuing province" />
						</label>
						<form:select class="y_vehicleLicenseProvince form-control" id="y_travellerdetails_${fn:escapeXml(idx)}_license_province__${formValues.uid}" path="${fn:escapeXml(travellerForm)}.travellerInfo.licensePlateProvince" cssErrorClass="fieldError">
							<form:option class="y_default_please_select" value="-1" disabled="true" selected="selected"> <spring:theme code="profile.address.region.dropdown.message" /> </form:option>
						</form:select>
					</div>
				</div>
				</div>
		</div>
	</div>
</div>
