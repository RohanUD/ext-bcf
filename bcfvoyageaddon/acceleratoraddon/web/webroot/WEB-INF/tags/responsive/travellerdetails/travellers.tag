<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="adultsTitles" required="true" type="java.util.ArrayList"%>
<%@ attribute name="childrenTitles" required="true" type="java.util.ArrayList"%>
<%@ attribute name="travellerForm" required="true" type="java.lang.String"%>
<%@ attribute name="idx" required="true" type="java.lang.Integer"%>
<%@ attribute name="formValues" required="true" type="de.hybris.platform.commercefacades.travel.TravellerData"%>
<%@ attribute name="hasVehicle" required="true" type="Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="travellerTitles" scope="page" value="${adultsTitles}" />
<c:if test="${fn:containsIgnoreCase(formValues.label, 'child')}">
	<c:set var="travellerTitles" scope="page" value="${childrenTitles}" />
</c:if>
<c:if test="${fn:containsIgnoreCase(formValues.label, 'infant')}">
	<c:set var="travellerTitles" scope="page" value="${childrenTitles}" />
</c:if>
<c:if test="${not empty formErrors}">
	<div id="formErrors" class="alert alert-danger">
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.firstName" element="div" />
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.surname" element="div" />
		<form:errors path="${fn:escapeXml(travellerForm)}.travellerInfo.phoneNumber" element="div" />
	</div>
</c:if>
<div id="passenger-info-${fn:escapeXml(idx)}" class="y_passengerInformationForm mb-4">
	<div class="">
		<c:set var="driver" value="" />
		<c:if test="${idx == 0 && hasVehicle}">
			<c:set var="driver" value=" (Driver)" />
		</c:if>
		<p class="margin-bottom-20">
			<strong>Passenger ${idx+1}:&nbsp;${fn:escapeXml(formValues.travellerInfo.passengerType.name)}</strong>
		</p>
	</div>
	<div id="traveller-details-${fn:escapeXml(idx)}">
		<div class="panel-form">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.heading.firstname" text="First name" />
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.firstName" id="y_travellerdetails_${fn:escapeXml(idx)}_first_name" type="text" class="y_passengerFirstname form-control" placeholder="${fn:escapeXml(firstNamePlaceholder)}" />
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.heading.lastname" text="Last name" />
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.surname" id="y_travellerdetails_${fn:escapeXml(idx)}_last_name" type="text" value="${fn:escapeXml(formValues.travellerInfo.surname)}" class="y_passengerLastname form-control" placeholder="${fn:escapeXml(lastNamePlaceholder)}" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="text.page.travellerdetails.form.heading.gender" text="Gender" />
						</label>
						<form:select class="y_passengerGender custom-select-passenger-page selectpicker" path="${fn:escapeXml(travellerForm)}.travellerInfo.gender" cssErrorClass="fieldError">
							<form:option value="-1" disabled="true" selected="selected"> <spring:theme code="text.travellerdetails.drop.down.make.a.slection" text="Make a selection" /> </form:option>
							<form:option value="female"> <spring:theme code="text.page.travellerdetails.form.heading.gender.female" text="Female" /> </form:option>
							<form:option value="male"> <spring:theme code="text.page.travellerdetails.form.heading.gender.male" text="Male" /> </form:option>
							<form:option value="non-binary"> <spring:theme code="text.page.travellerdetails.form.heading.gender.non.binary" text="Non Binary" /> </form:option>
						</form:select>
					</div>
				</div>

			<c:if test="${idx == 0}">
				<div class="row">
				    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <p class="margin-top-10">
                            <strong>Passenger ${idx+1}&nbsp;contact details&nbsp;<span class="font-weight-normal">(required)</span></strong>
                        </p>
                    </div>
				</div>
				<div class="row js-emails-box">
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.heading.email" text="Email" />
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.email" type="text" id="email" class="y_passenger_email form-control" value="${fn:escapeXml(formValues.travellerInfo.email)}" />
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.heading.confirm.email" text="Confirm email" />
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.checkEmail" type="text" class="y_passenger_confirm_email form-control" value="${fn:escapeXml(formValues.travellerInfo.checkEmail)}" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 mb-3">
						<label>
							<spring:theme code="label.travellerdetails.form.heading.phone.number" text="Phone number" />
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.travellerInfo.phoneNumber" id="y_travellerdetails_${fn:escapeXml(idx)}_phone_number" type="text" value="${fn:escapeXml(formValues.travellerInfo.phoneNumber)}" class="y_passengerPhoneNumber form-control"
							placeholder="${fn:escapeXml(phoneNumberPlaceholder)}" />
					</div>
				</div>
			</c:if>

		</div>
	</div>
</div>
