<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="travellerdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/travellerdetails"%>
<%@ attribute name="adultsTitles" required="true" type="java.util.ArrayList"%>
<%@ attribute name="childrenTitles" required="true" type="java.util.ArrayList"%>
<%@ attribute name="reasonFortravel" required="true" type="java.util.ArrayList"%>
<%@ attribute name="savedTravellers" required="true" type="java.util.ArrayList"%>
<%@ attribute name="travellerForms" required="true" type="java.util.ArrayList"%>
<%@ attribute name="isCollapsable" required="true" type="java.lang.Boolean"%>
<%@ attribute name="showPhoneNumber" required="true" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach var="formValues" items="${travellerForms}" varStatus="formValuesIdx">
	<c:set var="travellerForm" value="travellerForms[${formValuesIdx.index}]" />
	<c:set var="idx" value="${formValuesIdx.index}" />
	<c:set var="formErrors">
		<form:errors path="travellerForms[${fn:escapeXml(idx)}].passengerInformation.*" />
	</c:set>
	<c:if test="${not empty formErrors}">
		<div id="formErrors" class="alert alert-danger">
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.title" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.firstname" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.lastname" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.gender" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.frequentFlyerMembershipNumber" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.reasonForTravel" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.email" element="div" />
			<form:errors path="${fn:escapeXml(travellerForm)}.passengerInformation.contactNumber" element="div" />
		</div>
	</c:if>
	<div id="passenger-info-${fn:escapeXml(idx)}" class="panel panel-default y_passengerInformationForm">
		<form:hidden path="${fn:escapeXml(travellerForm)}.formId" value="${fn:escapeXml(idx)}" />
		<form:hidden path="${fn:escapeXml(travellerForm)}.uid" value="${fn:escapeXml(formValues.uid)}" />
		<form:hidden path="${fn:escapeXml(travellerForm)}.passengerInformation.passengerTypeName" value="${fn:escapeXml(formValues.passengerInformation.passengerTypeName)}" />
 		<form:hidden path="${fn:escapeXml(travellerForm)}.label" value="${fn:escapeXml(formValues.label)}" />
		<form:hidden path="${fn:escapeXml(travellerForm)}.selectedSavedTravellerUId" class="y_passengerSelectedSavedTravellerUId" />
		<div id="traveller-details-${fn:escapeXml(idx)}" class="panel-body collapse in">
			<div class="panel-form">
			<div class="fieldset">
				<sec:authorize access="hasAnyRole('ROLE_CUSTOMERGROUP')">
					<c:if test="${not empty savedTravellers}">
						<div class="row">
							<div class="form-group col-xs-12 col-sm-6 col-md-4">
								<c:set var="hasValidValuesForType" value="false" />
								<c:forEach var="traveller" items="${savedTravellers}">
									<c:if test="${traveller.travellerInfo.passengerType.code == formValues.passengerInformation.passengerTypeCode}">
										<c:set var="hasValidValuesForType" value="true" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					</c:if>
				</sec:authorize>
				<fieldset class="fieldset hide">
					<legend class="sr-only">
						<spring:theme code="sr.travellerdetails.areyoutravelling" />
					</legend>
					<p class="form-label">
						<spring:theme code="text.page.travellerdetails.form.heading.areyoutravelling" text="Are you travelling?" />
					</p>
					<div class="row ">
						<div class="col-xs-10 col-md-5 form-group">
							<label for="travellerForms0.booker" class="radio-inline col-xs-3">
								<form:radiobutton path="${fn:escapeXml(travellerForm)}.booker" class="y_bookerIsTravelling" value="false" checked="true" />
								<spring:theme code="text.page.travellerdetails.form.option.no" text="No" />
							</label>
						</div>
					</div>
					<%-- / .row --%>
				</fieldset>
				<div class="row">
					<div class="form-group col-sm-6 col-md-3">
						<label>
							<spring:theme code="text.page.travellerdetails.form.heading.passengerType" text="Passenger Type" />
						</label>
						<form:select class="y_passengerType form-control" path="${fn:escapeXml(travellerForm)}.passengerInformation.passengerTypeCode" multiple="no" >
							<form:option value="" selected="${empty formValues.passengerInformation.title?'selected':''}">
								<spring:theme code="text.page.travellerdetails.form.option.default" text="Select" />
							</form:option>
							<c:forEach var="passengerType" items="${passengerTypes}" varStatus="passengerTypeIdx">
								<form:option value="${fn:escapeXml(passengerType.code)}" selected="${not empty formValues.passengerInformation.passengerTypeName and formValues.passengerInformation.passengerTypeName==passengerType.name ? 'selected' : ''}"> ${fn:escapeXml(passengerType.name)}</form:option>
							</c:forEach>
						</form:select>
					</div>
				</div> 
				<div class="row">
					<div class="form-group col-sm-6 col-md-2">
						<label>
							<spring:theme code="text.page.travellerdetails.form.heading.title" text="Title" />
						</label>
						<form:select id = "y_passengerTitleSelect" path="${fn:escapeXml(travellerForm)}.passengerInformation.title" multiple="no" class="y_passengerTitle form-control">
							<form:option value="" selected="${empty formValues.passengerInformation.title?'selected':''}">
								<spring:theme code="text.page.travellerdetails.form.option.default" text="Select" />
							</form:option>
							<c:forEach var="travellerTitleItem" items="${travellerTitles}" varStatus="travellerTitleIdx">
								<form:option id="allowedTitle" class ="" value="${fn:escapeXml(travellerTitleItem.code)}" selected="${not empty formValues.passengerInformation.title and formValues.passengerInformation.title==travellerTitleItem.name ? 'selected' : ''}"> ${fn:escapeXml(travellerTitleItem.name)} </form:option>
							</c:forEach>
						</form:select>
					</div>
					<div class="form-group col-xs-12 col-sm-6 col-md-4">
						<div id="td-traveller-gender1" class="form-label">
							<spring:theme code="text.page.travellerdetails.form.heading.gender" text="Gender" />
						</div>
						<label class="radio-inline col-xs-3 col-sm-4">
							<form:radiobutton path="${fn:escapeXml(travellerForm)}.passengerInformation.gender" class="y_passengerGender" value="male" checked="${empty formValues.passengerInformation.gender or formValues.passengerInformation.gender=='male'?'checked':''}" />
							<spring:theme code="text.page.travellerdetails.form.heading.gender.male" text="Male" />
						</label>
						<label class="radio-inline col-xs-3 col-sm-4">
							<form:radiobutton path="${fn:escapeXml(travellerForm)}.passengerInformation.gender" class="y_passengerGender" value="female" checked="${formValues.passengerInformation.gender=='female'?'checked':''}" />
							<spring:theme code="text.page.travellerdetails.form.heading.gender.female" text="Female" />
						</label>
						<label class="radio-inline col-xs-3 col-sm-4">
							<form:radiobutton path="${fn:escapeXml(travellerForm)}.passengerInformation.gender" class="y_passengerGender" value="Non Binary" checked="${formValues.passengerInformation.gender=='non-binary'?'checked':''}" />
							<spring:theme code="text.page.travellerdetails.form.heading.gender.non.binary" text="Non Binary" />
						</label>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 col-md-4">
						<label>
							<spring:theme code="text.page.travellerdetails.form.heading.firstname" text="Firstname" var="firstnamePlaceholder" />
							${fn:escapeXml(firstnamePlaceholder)}
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.passengerInformation.firstname" id="y_travellerdetails_${fn:escapeXml(idx)}_first_name" type="text" class="y_passengerFirstname form-control" placeholder="${fn:escapeXml(firstnamePlaceholder)}" />
					</div>
					<div class="form-group col-xs-12 col-sm-6 col-md-4">
						<label>
							<spring:theme var="lastnamePlaceholder" code="text.page.travellerdetails.form.heading.lastname" text="Lastname" />
							${fn:escapeXml(lastnamePlaceholder)}
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.passengerInformation.lastname" id="y_travellerdetails_${fn:escapeXml(idx)}_last_name" type="text" value="${fn:escapeXml(formValues.passengerInformation.lastname)}" class="y_passengerLastname form-control"
							placeholder="${fn:escapeXml(lastnamePlaceholder)}" />
					</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 col-md-4">
						<label class="nowrap">
							<spring:theme code="text.page.travellerdetails.form.heading.reasonfortravel" text="What is your reason for travel?" />
						</label>
						<form:select path="${fn:escapeXml(travellerForm)}.passengerInformation.reasonForTravel" class="y_passengerReasonForTravel form-control" multiple="no">
							<form:option value="" selected="${empty formValues.passengerInformation.reasonForTravel?'selected':''}">
								<spring:theme code="text.page.travellerdetails.form.option.default" text="Select" />
							</form:option>
							<c:forEach var="reasonFortravelItem" items="${reasonFortravel}" varStatus="reasonFortravelIdx">
								<form:option value="${fn:escapeXml(reasonFortravelItem.code)}" selected="${not empty formValues.passengerInformation.reasonForTravel and formValues.passengerInformation.reasonForTravel==reasonFortravelItem.code ? 'selected' : ''}"> ${fn:escapeXml(reasonFortravelItem.name)} </form:option>
							</c:forEach>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 col-md-4">
						<label>
							<spring:theme code="text.page.travellerdetails.form.heading.email" text="email" var="emailPlaceholder" />
							${fn:escapeXml(emailPlaceholder)}
						</label>
						<form:input path="${fn:escapeXml(travellerForm)}.passengerInformation.email" id="y_travellerdetails_${fn:escapeXml(idx)}_email" type="text" value="${fn:escapeXml(formValues.passengerInformation.email)}" class="y_passengerEmail form-control" placeholder="${fn:escapeXml(emailPlaceholder)}" />
					</div>
				</div>
				<c:if test="${idx == 0 && showPhoneNumber}">
					<div class="row y_passengerContactNumber">
						<div class="form-group col-xs-12 col-sm-6 col-md-4">
							<label>
								<spring:theme code="text.page.travellerdetails.form.heading.contactnumber" text="Phone number" var="contactNumberPlaceholder" />
								${fn:escapeXml(contactNumberPlaceholder)}
							</label>
							<form:input path="${fn:escapeXml(travellerForm)}.passengerInformation.contactNumber" id="y_passengerContactNumber" type="text" value="${fn:escapeXml(formValues.passengerInformation.contactNumber)}" class="form-control" placeholder="${fn:escapeXml(contactNumberPlaceholder)}" />
							<form:input path="${fn:escapeXml(travellerForm)}.passengerInformation.validateContactNumber" id="y_validateContactNumber" type="hidden" value="${fn:escapeXml(showPhoneNumber)}" />
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	</div>
</c:forEach>
