<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${isModifyingTransportBooking and cmsPage.uid ne 'homepage'}">
	<div class="container">
		<div class="row mt-4 text-center">
			<a href="${contextPath}${fn:escapeXml(abortModificationURL)}">
				<span class="glyphicon glyphicon-remove"></span>&nbsp;
				<spring:theme code="text.ferry.common.abort.cancellation" text="Cancel" />
			</a>
		</div>
	</div>
</c:if>
