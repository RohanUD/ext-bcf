<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${isModifyingTransportBooking and cmsPage.uid ne 'homepage'}">
	<div class="panel-heading">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6 text-left">
					<strong><spring:theme code="text.ferry.common.editbooking.title" text="Edit booking" /></strong>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 text-right">
					<a href="${contextPath}${fn:escapeXml(abortModificationURL)}">
						<span class="glyphicon glyphicon-remove"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</c:if>
