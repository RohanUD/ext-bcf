<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${isModifyingTransportBooking and cmsPage.uid ne 'homepage'}">
	<div id="js-load-original-booking" class="hidden">true</div>
	<div id="js-original-booking-reference" class="hidden">${originalBookingReference}</div>
	<div id="original-booking-placeholder"></div>
</c:if>
