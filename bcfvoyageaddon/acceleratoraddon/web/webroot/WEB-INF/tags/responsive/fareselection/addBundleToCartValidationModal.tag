<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal fade" id="y_addBundleToCartValidationModal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="addBundleToCartValidationModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h4 class="modal-title" id="addBundleToCartValidationModalLabel">
					<spring:theme code="fareselection.validation.incorrect" />
				</h4>
			</div>
			<div class="modal-body">
				<p class="y_addBundleToCartValidationBody">
					<spring:theme code="add.bundle.to.cart.request.error" />
				</p>
				<div class="row">
                					<div class="col-xs-12 col-sm-6">
                						<button class="btn btn-primary btn-block y_fareSelectionConfirm" data-minorigindestinationrefnumber>
                							<spring:theme code="text.fareselection.cancel.button.confirmation" text="Confirm" />
                						</button>
                					</div>
                					<div class="col-xs-12 col-sm-6">
                						<button class="btn btn-transparent btn-block" data-dismiss="modal">
                							<spring:theme code="text.fareselection.cancel.button.close" text="No Thanks" />
                						</button>
                					</div>
                				</div>
			</div>
			<div id="y_addBundleToCartValidationFooter" class="modal-footer y_addBundleToCartValidationFooter" style="display:none;">

			</div>
		</div>
	</div>
</div>
