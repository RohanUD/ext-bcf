<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="index" required="true" type="java.lang.Integer"%>
<%@ attribute name="rowresult" required="true" type="String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${pricedItinerary.available && pricedItinerary.originDestinationRefNumber == refNumber}">
	<div class="p-card">
		<div class="col-md-12 col-sm-12">
			<c:set var="resultrowid" value="${rowresult}${idx.index}" />
			<div class="row">
				<div class="col-lg-9 col-md-9 col-xs-12 border-right-divider mb-30">
					<div class="row">
						<div class="col-md-8 col-xs-12">
							<div class="row pt-4 padding-0-mobile">
								<c:set var="numberOfConnections" value="${fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings)}" />
								<c:choose>
									<c:when test="${numberOfConnections > 1}">
										<c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
										<c:set var="lastOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
									</c:when>
									<c:otherwise>
										<c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
										<c:set var="lastOffering" value="${firstOffering}" />
									</c:otherwise>
								</c:choose>
								<div class="col-xs-4 padding-0-mobile">
									<div class="p-chart text-center">
										<div class="pc-1">
											<spring:theme code="fareselection.depart" />
										</div>
										<div class="fnt-24 ferrytime">
											<b><fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${firstOffering.departureTime}" /></b>
										</div>
									</div>
								</div>
								<div class="col-xs-4 padding-0-mobile">
									<div class="p-chart text-center">
										<div class="pc-1 distance-time-text">
											<c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.days'] && pricedItinerary.itinerary.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
													code="transport.offering.status.result.days" />
											</c:if>
											<c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] && pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.hours'])}
																	<spring:theme code="transport.offering.status.result.hours" />
											</c:if>
											&nbsp;${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.minutes'])}
											<spring:theme code="transport.offering.status.result.minutes" />
										</div>
										<div class="distance-icon-line">
											<span class="bcf bcf-icon-wave-line distance-wave-icon icon-blue"></span>
											<div class="distance-line icon-blue"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-4 padding-0-mobile">
									<div class="p-chart text-center">
										<div class="pc-1">
											<spring:theme code="fareselection.arrive" />
										</div>
										<div class="fnt-24">
											<b><fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${lastOffering.arrivalTime}" /></b>
										</div>
									</div>
								</div>
								<c:forEach items="${pricedItinerary.itineraryPricingInfos}" var="percentage1">
									<fmt:parseNumber var="i" type="number" value="${percentage1.totalFare.totalPrice.value}" />
									<c:if test="${not empty i}">
										<c:choose>
											<c:when test="${empty min}">
												<c:set var="min" value="${i}" />
											</c:when>
											<c:when test="${not empty min and i < min}">
												<c:set var="min" value="${i}" />
											</c:when>
										</c:choose>
										
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="col-md-4 col-xs-12 coastal-celebration-link">
							<p class="mb-1 hidden">
								<spring:theme code="fareselection.ferry" />
							</p>
							<div class="clearfix">
								<ul class="flight-details flight-details-stop col-xs-12 sailing-ferry-name">
									<c:forEach items="${pricedItinerary.itinerary.originDestinationOptions[0].distinctTransportVehicles}" var="transportVehicle" varStatus="status">
                            			<a href="/ship-info/${fn:escapeXml(transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal">${fn:escapeXml(transportVehicle.vehicleInfo.name)}</a>
                            			<c:if test="${!status.last}">,&nbsp;</c:if>
                        			</c:forEach>
									<c:if test="${not empty pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].availability.code}">
										<li class="flight-number col-sm-12 test" style="display: block;">
											<spring:theme code="text.listsailing.ProductAvailabilityEnum.${fn:escapeXml(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].availability.code)}" />
										</li>
									</c:if>
								</ul>
								<c:if test='${!offeringIdx.last}'>
									<span class="col-xs-12 one-stop-divide y_fareResultStopDivide glyphicon glyphicon-transfer hidden"></span>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-xs-12 border-left m-border-left text-center">
					<div class="p-chart p-cash">
						<div class="pc-3 text-center">
							<c:choose>
								<%-- removing the subBucketCapacity check for now as it is either 0 or null from list sailing SIT service --%>
								<%-- <c:when test="${pricedItinerary.isBookable and !pricedItinerary.zeroSubBucketFreeCapacity }"> --%>
								<c:when test="${pricedItinerary.isBookable}">
									<div class="pc-1 text-center padding-0-mobile">Total From</div>
									<div class="pc-2 text-center padding-0-mobile">
										<strong><span>&#36;</span><fmt:formatNumber type="number" maxFractionDigits="2" value="${min}" minFractionDigits="2" /> </strong>
									</div>
									<div class="row">
									<div class="col-lg-6 col-md-6 col-md-offset-3">
									<button class="btn btn-primary view-fare-btn btn-block">
										<spring:theme code="text.fareselection.button.viewFare" text="viewFares" />
									</button>
									<button class="btn btn-outline-blue view-fare-btn-close">Close</button>
									</div>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${pricedItinerary.isBookable and pricedItinerary.zeroSubBucketFreeCapacity }">
											<p class="text-red">
												<b><spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" /><br> <spring:theme code="text.listsailing.bundle.fare.sold.out.online" text="Online" /> </b>
											</p>
											<p class="text-gray">
												<b><spring:theme code="text.listsailing.fare.at.terminal.bundle.name" text="Fare at terminal" /><br> <format:price priceData="${pricedItinerary.fareAtTerminal}" /> </b>
											</p>
										</c:when>
										<c:otherwise>
											<div class="pc-2 text-center padding-0-mobile">
												<p class="text-red sold-out-red-text">
													<spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" />
												</p>
											</div>
											<c:set var="showPricing" value="${false}" />
											<c:forEach items="${pricedItinerary.itineraryPricingInfos}" var="itineraryPricingInfo" varStatus="pricingIdx">
												<c:if
													test="${itineraryPricingInfo.bundleType != ecoBundleType && itineraryPricingInfo.bundleType != ecoPlusBundleType && itineraryPricingInfo.bundleType != businessBundleType && not empty itineraryPricingInfo && itineraryPricingInfo.available && itineraryPricingInfo.totalFare.totalPrice.value > 0.0}">
													<c:set var="showPricing" value="${true}" />
												</c:if>
											</c:forEach>
											<c:if test="${showPricing}">
												<div class="row">
													<div class="col-lg-6 col-md-6 col-md-offset-3">
														<button class="btn btn-primary view-fare-btn btn-block">
															<spring:theme code="text.fareselection.button.viewFare" text="viewFares" />
														</button>
														<button class="btn btn-outline-blue view-fare-btn-close"><spring:theme code="text.ferry.listing.close.fare.breakup.button" text="Close" /></button>
													</div>
												</div>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
			<div id="sailing-inner" class="collapse viewfare-box">
				<hr class="line">
				<p class="text-center mb-1">
					<strong>Please select a fare option</strong>
				</p>
				<c:if test="${bcfFareFinderForm.readOnlyResults ne 'true' && enableRevenueManagement eq 'true'}">
					<p class="text-center">
						<a class="" href="#compareFaresLink" data-toggle="modal" data-target="#compareFaresModal"> Compare fares</a>
					</p>
				</c:if>
				<div class="col-md-10 col-md-offset-1 padding-0-mobile">
					<fareselection:offeringListBundles pricedItinerary="${pricedItinerary}" refNumber="${refNumber}" name="refNumber-${refNumber}" />
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    <div class="text-right js-sailing-link" >
                            <c:if test="${numberOfConnections > 1}">
                                <c:set var="flightDetailsCollapseId" value="#collapse${pricedItinerary.id}" />
                                <c:set var="flightDetailsCollapse" value="collapse${pricedItinerary.id}" />
                                <a role="button" data-toggle="collapse" href="${fn:escapeXml(flightDetailsCollapseId)}"
                                aria-expanded="false" aria-controls="${fn:escapeXml(flightDetailsCollapse)}"
                                class="info-trigger detail-link">
                                    <spring:theme code="fareselection.view.journey" /> <i class="bcf bcf-icon-down-arrow"></i>
                                </a>
                            </c:if>
                        </div>
				</div>
				<div class="col-md-10 col-md-offset-1 padding-0-mobile">
				<fareselection:journeyDetails pricedItinerary="${pricedItinerary}" noOfConnections="${numberOfConnections}" />
				</div>
			<c:set var="firstElement" value="${false}" />
		</div>
	</div>
	<div id="sailing-inner" class="collapse viewfare-box">
		<hr class="line">
		<p class="text-center mb-1">
			<strong><spring:theme code="fareselection.fareOption" /></strong>
		</p>
		<c:if test="${bcfFareFinderForm.readOnlyResults ne 'true'}">
			<p class="text-center">
				<a class="" href="#compareFaresLink" data-toggle="modal" data-target="#compareFaresModal">
					<spring:theme code="fareselection.compareFares" />
				</a>
			</p>
		</c:if>
		<div class="col-xl-10 offset-1 padding-0-mobile">
			<hr class="line">
			<fareselection:offeringListBundles pricedItinerary="${pricedItinerary}" refNumber="${refNumber}" name="refNumber-${refNumber}" />
		</div>
	</div>
	<c:set var="firstElement" value="${false}" />
</c:if>
