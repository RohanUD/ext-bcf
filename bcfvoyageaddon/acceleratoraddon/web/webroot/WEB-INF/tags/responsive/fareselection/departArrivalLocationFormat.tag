<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="location" required="true" type="java.lang.String" %>
<c:set var = "locName" value = "${location}"/>
 <c:choose>
     <c:when test="${fn:contains(locName, '-')}">
        <c:set var="locationName" value="${fn:substringAfter(locName,'-')}"/>
     </c:when>
     <c:otherwise>
        <c:set var="locationName" value="${locName}"/>
     </c:otherwise>
 </c:choose>
 <c:choose>
     <c:when test="${fn:contains(locationName, '(')}">
        <c:if test="${fn:contains(locationName, ')(') eq true}">
            <c:set var="Terminal" value ="${fn:substringAfter(fn:substringBefore(locationName,')'), '(')}"/>
        </c:if>
        <c:set var="City" value ="${fn:substringBefore(locationName, '(')}"/>
        <p class="font-weight-bold fnt-18 m-0">${City}</p>
        <c:if test="${not empty Terminal}">
            <p class="fnt-18 m-0">(${Terminal})</p>
        </c:if>
     </c:when>
     <c:otherwise>
        <p class="font-weight-bold fnt-18 m-0">${locationName}</p>
     </c:otherwise>
 </c:choose>
