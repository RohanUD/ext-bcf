<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ attribute name="fareSelection" required="true" type="de.hybris.platform.commercefacades.travel.FareSelectionData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
 <div class="sailing-slide-box-cover">
	<c:forEach items="${fareSelection.calenderSailingRange}" var="sailingSummary" varStatus="idx">
		<c:choose>
			<c:when test="${sailingSummary.requestedDate}">
				<div class="sailing-slide-box text-center inactiveLink">
					<fmt:formatDate var="date" pattern="EEE, MMM dd" value="${sailingSummary.pricingDate}" />
					<div><b>${date}</b></div>
					<div><spring:theme code="label.listsailing.calender.prices.from" /></div>
					<div>
						<b><format:price priceData="${sailingSummary.minPrice}" /></b>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="bg-gray text-center sailing-slide-box" id="min_price_for_date">
					<fmt:formatDate var="hiddenDate" pattern="yyyy-MM-dd" value="${sailingSummary.pricingDate}" />
					<div id="hiddenDate" hidden="hidden">${hiddenDate}</div>
					<fmt:formatDate var="date" pattern="EEE, MMM dd" value="${sailingSummary.pricingDate}" />
					<div class="sailing-slide-bx-date-div">${date}</div>
					<div class="sailing-slide-bx-text-div"><spring:theme code="label.listsailing.calender.prices.from" /></div>
					<div class="sailing-slide-bx-price-div">
						<format:price priceData="${sailingSummary.minPrice}" />
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	</div>
