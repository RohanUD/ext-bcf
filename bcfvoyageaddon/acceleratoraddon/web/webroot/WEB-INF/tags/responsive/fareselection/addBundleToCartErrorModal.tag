<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal fade" id="y_addBundleToCartErrorModal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="addBundleToCartErrorModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h4 class="modal-title" id="addBundleToCartErrorModalLabel">
					<spring:theme code="text.package.details.add.to.cart.error.modal.title" text="Add to cart error" />
				</h4>
			</div>
			<div class="modal-body">
				<p class="y_addBundleToCartErrorBody">
					<spring:theme code="add.bundle.to.cart.request.error" />
				</p>


			</div>
		</div>
	</div>
</div>
