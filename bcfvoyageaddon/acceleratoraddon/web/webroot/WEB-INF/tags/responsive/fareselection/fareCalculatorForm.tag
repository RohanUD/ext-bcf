<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<c:url var="submitUrl" value="/faresummary" />

<form:form modelAttribute="fareCalculatorForm" class="y_fareCalculatorForm" id="fareCalculatorForm-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}" action="${fn:escapeXml(submitUrl)}" method="post">

	<form:input type="hidden" readonly="true" path="travelRouteCode" value="${fn:escapeXml(pricedItinerary.itinerary.route.code)}" />
	<form:input type="hidden" readonly="true" path="journeyRefNum" value="${fn:escapeXml(journeyRefNum)}" />
	<%-- passengerTypeQuantityDatas --%>
	<c:forEach var="entry" items="${itineraryPricingInfo.ptcFareBreakdownDatas}" varStatus="i">
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.ptcFareBreakdownDatas[${fn:escapeXml(i.index)}].passengerTypeQuantity.quantity" value="${fn:escapeXml(entry.passengerTypeQuantity.quantity)}" />
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.ptcFareBreakdownDatas[${fn:escapeXml(i.index)}].passengerTypeQuantity.passengerType.name" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.name)}" />
		<c:forEach var="fareInfo" items="${entry.fareInfos}" varStatus="j">
		    <c:forEach var="fareDetail" items="${fareInfo.fareDetails}" varStatus="k">
		        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.ptcFareBreakdownDatas[${fn:escapeXml(i.index)}].fareInfos[${fn:escapeXml(j.index)}].fareDetails[${fn:escapeXml(k.index)}].fareProduct.price.formattedValue" value="${fn:escapeXml(fareDetail.fareProduct.price.formattedValue)}" />
		        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.ptcFareBreakdownDatas[${fn:escapeXml(i.index)}].fareInfos[${fn:escapeXml(j.index)}].fareDetails[${fn:escapeXml(k.index)}].fareProduct.price.value" value="${fn:escapeXml(fareDetail.fareProduct.price.value)}" />
		    </c:forEach>
		</c:forEach>
	</c:forEach>
	<%-- vehicleTypeQuantityDatas --%>
	<c:forEach var="entry" items="${itineraryPricingInfo.vehicleFareBreakdownDatas}" varStatus="i">
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].vehicleTypeQuantity.qty" value="${fn:escapeXml(entry.vehicleTypeQuantity.qty)}" />
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].vehicleTypeQuantity.vehicleType.code" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.code)}" />
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].vehicleTypeQuantity.vehicleType.name" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.name)}" />
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].vehicleTypeQuantity.vehicleType.description" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.description)}" />
        <c:forEach var="fareDetail" items="${entry.fareInfos.fareDetails}" varStatus="j">
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].fareInfos.fareDetails[${fn:escapeXml(j.index)}].fareProduct.price.formattedValue" value="${fn:escapeXml(fareDetail.fareProduct.price.formattedValue)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.vehicleFareBreakdownDatas[${fn:escapeXml(i.index)}].fareInfos.fareDetails[${fn:escapeXml(j.index)}].fareProduct.price.value" value="${fn:escapeXml(fareDetail.fareProduct.price.value)}" />
        </c:forEach>
	</c:forEach>
	<form:input type="hidden" readonly="true" path="itineraryPricingInfo.totalFare.totalPrice.value" value="${fn:escapeXml(itineraryPricingInfo.totalFare.totalPrice.value)}" />
	<form:input type="hidden" readonly="true" path="itineraryPricingInfo.totalFare.totalPrice.formattedValue" value="${fn:escapeXml(itineraryPricingInfo.totalFare.totalPrice.formattedValue)}" />
	<form:input type="hidden" readonly="true" path="pricedItinerary.itinerary.route.reservable" value="${fn:escapeXml(pricedItinerary.itinerary.route.reservable)}" />
	<form:input type="hidden" readonly="true" path="pricedItinerary.itinerary.tripType" value="${fn:escapeXml(pricedItinerary.itinerary.tripType)}" />
	<form:input type="hidden" readonly="true" path="pricedItinerary.itinerary.route.code" value="${fn:escapeXml(pricedItinerary.itinerary.route.code)}" />
    <c:set var="currentTansportOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
	<form:input type="hidden" readonly="true" path="shipName" value="${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.name)}" />
    <form:input type="hidden" readonly="true" path="departureDurationHour" value="${fn:escapeXml(currentTansportOffering.duration['transport.offering.status.result.hours'])}" />
    <form:input type="hidden" readonly="true" path="departureDurationMinute" value="${fn:escapeXml(currentTansportOffering.duration['transport.offering.status.result.minutes'])}" />

	<c:forEach var="entry" items="${itineraryPricingInfo.bundleTemplates}" varStatus="btIdx">
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProductBundleTemplateId" value="${fn:escapeXml(entry.fareProductBundleTemplateId)}" />
		<form:input class="bundleType" type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].bundleType" value="${fn:escapeXml(entry.bundleType)}" />
		<c:forEach var="fareProductEntry" items="${entry.fareProducts}" varStatus="fpIdx">
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].code" value="${fn:escapeXml(fareProductEntry.code)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].bookingClass" value="${fn:escapeXml(fareProductEntry.bookingClass)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].fareBasisCode" value="${fn:escapeXml(fareProductEntry.fareBasisCode)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].fareProductType" value="${fn:escapeXml(fareProductEntry.fareProductType)}" />
		</c:forEach>

        <c:forEach var="transportOfferingEntry" items="${entry.transportOfferings}" varStatus="toIdx">
            <form:input class="transportOfferingcode" type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].code" value="${fn:escapeXml(transportOfferingEntry.code)}" />
            <fmt:formatDate value="${transportOfferingEntry.departureTime}" var="formattedDepartureDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
            <fmt:formatDate value="${transportOfferingEntry.arrivalTime}" var="formattedArrivalDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].departureTime" value="${fn:escapeXml(formattedDepartureDate)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].departureTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.departureTimeZoneId)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].arrivalTime" value="${fn:escapeXml(formattedArrivalDate)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].arrivalTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.arrivalTimeZoneId)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].sector.code" value="${fn:escapeXml(transportOfferingEntry.sector.code)}" />
        </c:forEach>
	</c:forEach>

	<%-- otherChargesDatas --%>
    <c:forEach var="otherChargesEntry" items="${itineraryPricingInfo.otherCharges}" varStatus="i">
        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.otherCharges[${fn:escapeXml(i.index)}].quantity" value="${fn:escapeXml(otherChargesEntry.quantity)}" />
        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.otherCharges[${fn:escapeXml(i.index)}].name" value="${fn:escapeXml(otherChargesEntry.name)}" />
        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.otherCharges[${fn:escapeXml(i.index)}].code" value="${fn:escapeXml(otherChargesEntry.code)}" />
        <form:input type="hidden" readonly="true" path="itineraryPricingInfo.otherCharges[${fn:escapeXml(i.index)}].price.formattedValue" value="${fn:escapeXml(otherChargesEntry.price.formattedValue)}" />
    </c:forEach>
</form:form>
