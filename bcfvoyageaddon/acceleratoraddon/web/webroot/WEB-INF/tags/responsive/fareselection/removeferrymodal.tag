<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="y_showRemoveSailingsModal" tabindex="-1" role="dialog" aria-labelledby="y_showRemoveSailingsModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="removeSailingsLabel">
					<spring:theme code="text.ferry.remove.modal.title" text="Remove Sailings" />
				</h3>
			</div>
			<div class="modal-body">
				<p class="col-sm-12 mt-0" id="removeSailings">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
					<spring:theme code="text.ferry.remove.message" text="Are you sure you want to remove the sailing" />
				</p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_removeSailing" class="btn btn-primary btn-block">
							<spring:theme code="text.page.managemybooking.fresh.booking.confirmation" text="Confirm" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
		<input type="hidden" name="removeFerryUrl" id="removeFerryUrl">
    <form:form id="removeFerryForm"  method="post"/>


</div>
