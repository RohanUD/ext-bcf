<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
    <c:if test="${not empty nextAvailabilityDates}">
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-xs-12 mt-4 mb-4 text-center padding-top-30
            padding-bottom-30">
                <h5><spring:theme code="label.listsailing.next.available.sailing.dates" /></h5>
            </div>
        </div>
    </c:if>
    <c:forEach var="nextAvailabilityDate" items="${nextAvailabilityDates}">
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-xs-12">
                <div class="btn btn-primary btn-block mb-5" id="min_price_for_date">
                    <fmt:formatDate var="hiddenDate" pattern="yyyy-MM-dd" value="${nextAvailabilityDate}" />
                    <div id="hiddenDate" hidden="hidden">${hiddenDate}</div>
                        <fmt:formatDate pattern="EEEEE, MMMMM dd, yyyy" value="${nextAvailabilityDate}" />
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-xs-12 margin-top-30 ">
            <a href="${dailySchedulesUrl}"><spring:theme code="label.listsailing.view.schedules" text="View schedule" /></a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-xs-12 margin-top-30">
            <p class="sailing-italic-text">
                <spring:theme code="label.packageferryselection.fareselection.lastupdated" />&nbsp;${dateNow}
            </p>
        </div>
    </div>
</div>
