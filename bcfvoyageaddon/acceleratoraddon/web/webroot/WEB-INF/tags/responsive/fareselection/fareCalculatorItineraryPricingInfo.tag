<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ attribute name="pricedItineraryDateTime" required="true" type="java.util.Date"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/dealdetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="dealFormPath" required="false" type="java.lang.String"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="showDealForm" value="false" />
<c:if test="${not empty dealFormPath}">
	<c:set var="showDealForm" value="true" />
</c:if>
<c:if test="${not empty itineraryPricingInfo && itineraryPricingInfo.available}">
	<li class="vp-box" class="${itineraryPricingInfo.promotional ? 'promotional-price' : ''} ${itineraryPricingInfo.selected ? 'selected' : ''}">
		<div class="row">
			<div class="col-md-8 col-xs-8">
				<h4 class="mb-1">
					<b>${itineraryPricingInfo.bundleTypeName}</b>
				</h4>
				<c:choose>
					<c:when test="${itineraryPricingInfo.bundleType eq 'FARE_AT_TERMINAL'}">
						<p>
							<spring:theme code="text.listsailing.bundle.type.without.reservation.description" text="Standard fare without reservation" />
						</p>
					</c:when>
					<c:otherwise>
						<p>
							<spring:theme code="text.listsailing.bundle.type.with.reservation.description" text="With reservation" />
						</p>
						<p class="blue-color price-breakdown margin-top-40" id="price-down">
							<spring:theme code="label.listsailing.bundle.type.price.breakdown.button" text="Price breakdown" />
							<i class="bcf bcf-icon-down-arrow price-breakdown-arrow"></i>
						</p>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="col-md-4 col-xs-4 pl-0">
				<c:choose>
					<c:when test="${itineraryPricingInfo.bundleType eq 'FARE_AT_TERMINAL'}">
						<p class="fare-terminal-price">
							<span class="fare-terminal-total"><spring:theme code="text.listsailing.total" text="Total" /></span>
							<format:price priceData="${pricedItinerary.fareAtTerminal}" />
						</p>
					</c:when>
					<c:otherwise>
						<h5 class="sailing-white-bx">
							<c:choose>
								<c:when test="${not empty itineraryPricingInfo.totalFare.priceDifference}">
									<span class="price-desc">${fn:escapeXml(itineraryPricingInfo.totalFare.priceDifference.formattedValue)}</span>
								</c:when>
								<c:otherwise>
									<span class="price-desc">${itineraryPricingInfo.available && itineraryPricingInfo.totalFare.totalPrice.value > 0 ? itineraryPricingInfo.totalFare.totalPrice.formattedValue : '&mdash;'}</span>
								</c:otherwise>
							</c:choose>
						</h5>
                        <div class="sailing-blue-bx">
                            <c:if test="${pricedItinerary.departedSailing ne true}">
                                <c:choose>
                                    <c:when test="${showDealForm == false}">

                                        <div class="btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-primary js-radio-select active" > <input
                                                type="radio" name="${fn:escapeXml(name)}"
                                                id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}"
                                                ${!pricedItinerary.isBookable ? 'disabled' : ''}
                                                ${itineraryPricingInfo.available ? '' : 'disabled'}
                                                ${itineraryPricingInfo.selected ? 'checked' : ''}
                                                value="${fn:escapeXml(pricedItineraryDateTime.time)}"
                                                class="y_fareSummaryResultSelect" autocomplete="off" /> <spring:theme
                                                    code="text.fareselection.button.select" text="Select" />
                                            </label>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <form:radiobutton path="${dealFormPath}.itineraryPricingInfo.selected" disabled="${!pricedItinerary.isBookable or !itineraryPricingInfo.available ? 'disabled' : ''}" checked="${itineraryPricingInfo.selected ? 'checked' : ''}" name="${fn:escapeXml(name)}"
                                            id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}" value="${!itineraryPricingInfo.selected}" class="y_fareResultSelect" />
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div class="offset-1 pt-4 price-breakdown-box collapse" id="price-box">
			<div class="row">
				<c:forEach var="ptcBreakDownData" items="${itineraryPricingInfo.ptcFareBreakdownDatas }" varStatus="ptcIdx">
					<c:if test="${ptcBreakDownData.passengerTypeQuantity.quantity ne 0}">
						<div class="list-unstyled col-xs-9">${ptcBreakDownData.passengerTypeQuantity.quantity}&nbsp;<span class="res-quantity">x</span>&nbsp;${ptcBreakDownData.passengerTypeQuantity.passengerType.name}</div>
						<div class="list-unstyled col-xs-3">
							<c:forEach var="fareInfo" items="${ptcBreakDownData.fareInfos}" varStatus="fareInfoIdx">
								<c:forEach var="fareDetail" items="${fareInfo.fareDetails}" varStatus="fareDetailIdx">
									<p><format:price priceData="${fareDetail.fareProduct.price}" /></p>
								</c:forEach>
							</c:forEach>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="row">
				<c:forEach var="vehicleFareBreakdownData" items="${itineraryPricingInfo.vehicleFareBreakdownDatas}" varStatus="ptcIdx">
					<c:if test="${vehicleFareBreakdownData.vehicleTypeQuantity.qty ne 0}">
						<div class="list-unstyled col-xs-9">${vehicleFareBreakdownData.vehicleTypeQuantity.qty}&nbsp;<span class="res-quantity">x</span>&nbsp;${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.name}</div>
						<div class="list-unstyled col-xs-3">
							<c:forEach var="fareDetail" items="${vehicleFareBreakdownData.fareInfos.fareDetails}" varStatus="fareDetailIdx">
								<p><format:price priceData="${fareDetail.fareProduct.price}" /></p>
							</c:forEach>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="row">
				<c:forEach var="largeItemsBreakdownData" items="${itineraryPricingInfo.largeItemsBreakdownDataList}">
					<c:if test="${largeItemsBreakdownData.quantity ne 0}">
						<div class="list-unstyled col-xs-9">${largeItemsBreakdownData.quantity}&nbsp;<span class="res-quantity">x</span>&nbsp;${largeItemsBreakdownData.name}</div>
						<div class="list-unstyled col-xs-3">
							<p><format:price priceData="${largeItemsBreakdownData.price}" /></p>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="row">
				<c:forEach var="otherCharge" items="${itineraryPricingInfo.otherCharges}">
					<c:if test="${otherCharge.quantity ne 0}">
						<div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.other.charges" text="Other charges" /></div>
						<div class="list-unstyled col-xs-3">
							<p><format:price priceData="${otherCharge.price}" /></p>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="row">
				<div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.total.fare" text="Total fare (inc. taxes)" /></div>
				<div class="list-unstyled col-xs-3">
					<p><format:price priceData="${itineraryPricingInfo.totalFare.totalPrice}" /></p>
				</div>
			</div>
			<div class="row">
				<div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.due.now" text="Due now" /></div>
				<div class="list-unstyled col-xs-3">
					<p><format:price priceData="${itineraryPricingInfo.amountToPayNow}" /></p>
				</div>
			</div>
			<div class="row">
				<div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.due.at.terminal" text="Due at terminal" /></div>
				<div class="list-unstyled col-xs-3">
					<p><format:price priceData="${itineraryPricingInfo.amountDueAtTerminal}" /></p>
				</div>
			</div>
		</div>
		<div class="offset-1">
			<p class="mb-2 mt-4 fnt-14 small-grey-text">
				<c:forEach var="bundleTemplate" items="${itineraryPricingInfo.bundleTemplates }" varStatus="bundleIdx">${bundleTemplate.description }</c:forEach>
			</p>
		</div>
		<c:if test="${not empty dealBundleTemplateId}">
			<span class="selected"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			</span>
			<span class="select"> <spring:theme code="text.accommodation.details.accommodation.select.price" text="select" />
			</span>
		</c:if>
				<fareselection:fareCalculatorForm itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}"/>
		<hr class="line">
	</li>

</c:if>
