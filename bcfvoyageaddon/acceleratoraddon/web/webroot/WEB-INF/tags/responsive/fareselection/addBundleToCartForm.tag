<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="addBundleToCartUrl" required="false" type="java.lang.String"%>
<%@ attribute name="isDynamicPackage" required="false" type="java.lang.Boolean"%>
<%@ attribute name="openTicket" required="false" type="java.lang.Boolean"%>
<c:url var="addToCartUrl" value="${addBundleToCartUrl}" />
<div class="y_vehicleInfoError col-md-12"></div>

<form:form modelAttribute="addBundleToCartForm" class="y_addBundleToCartForm" id="addBundleToCartForm-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}" action="${fn:escapeXml(addToCartUrl)}" method="post">

	<form:input type="hidden" readonly="true" path="journeyRefNum" value="${fn:escapeXml(journeyRefNum)}" />
	<form:input type="hidden" readonly="true" path="tripType" value="${fn:escapeXml(tripType)}" />
	<form:input type="hidden" readonly="true" path="returnWithDifferentPassenger" value="${fn:escapeXml(returnWithDifferentPassenger)}" />
	<form:input type="hidden" readonly="true" path="returnWithDifferentVehicle" value="${fn:escapeXml(returnWithDifferentVehicle)}" />
	<form:input id="y_changeFareOrReplan" type="hidden" readonly="true" path="changeFareOrReplan" value="${fn:escapeXml(isAmendmentCart)}" />
	<form:input type="hidden" readonly="true" path="carryingDangerousGoodsInOutbound" value="${fn:escapeXml(carryingDangerousGoodsInOutbound)}" />
	<form:input type="hidden" readonly="true" path="carryingDangerousGoodsInReturn" value="${fn:escapeXml(carryingDangerousGoodsInReturn)}" />
	<form:input type="hidden" readonly="true" path="fareTypeRefNo" value="${fn:escapeXml(odRefNum)}" />
	<form:input type="hidden" readonly="true" path="travelRouteCode" value="${fn:escapeXml(pricedItinerary.itinerary.route.code)}" />
	<form:input type="hidden" readonly="true" path="transferSailingIdentifier" value="${fn:escapeXml(pricedItinerary.transferSailingIdentifier)}" />
	<form:input type="hidden" readonly="true" path="originDestinationRefNumber" value="${fn:escapeXml(pricedItinerary.originDestinationRefNumber)}" />
	<form:input type="hidden" readonly="true" path="errorClass" value="vehicleTypeQuantityList[" />
    <form:input type="hidden" readonly="true" path="openTicket" value="${fn:escapeXml(openTicket)}" />
    <c:if test="${not empty passengerUidsToBeRemoved}">
        <c:forEach var="passengerUidToBeRemoved" items="${passengerUidsToBeRemoved}" varStatus="i">
                <form:input type="hidden" readonly="true" path="passengerUidsToBeRemoved[${fn:escapeXml(i.index)}]" value="${passengerUidToBeRemoved}" />
        </c:forEach>
    </c:if>

    <form:input type="hidden" readonly="true" path="dynamicPackage" value="${fn:escapeXml(isDynamicPackage)}" />

	<input id="y_changeFareBundleType" type="hidden" readonly="true" value="${fn:escapeXml(bundleType)}" />
	<input id="y_changeFareTransportOfferings" type="hidden" readonly="true" value="${fn:escapeXml(transportOfferings)}" />
	<%-- passengerTypeQuantityDatas --%>
	<c:forEach var="entry" items="${itineraryPricingInfo.ptcFareBreakdownDatas}" varStatus="i">
		<form:input type="hidden" readonly="true" path="passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.code)}" />
		<form:input type="hidden" readonly="true" path="passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.name)}" />
		<form:input type="hidden" readonly="true" path="passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" value="${fn:escapeXml(entry.passengerTypeQuantity.quantity)}" />
		<form:input type="hidden" readonly="true" path="passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.bcfCode" value="${fn:escapeXml(entry.passengerTypeQuantity.passengerType.bcfCode)}" />
	</c:forEach>
	<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTypeName" value="${fn:escapeXml(itineraryPricingInfo.bundleTypeName)}" />
	<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleType" value="${fn:escapeXml(itineraryPricingInfo.bundleType)}" />
	<form:input type="hidden" readonly="true" path="itineraryPricingInfo.itineraryIdentifier" value="${fn:escapeXml(itineraryPricingInfo.itineraryIdentifier)}" />
	<c:forEach var="entry" items="${itineraryPricingInfo.vehicleFareBreakdownDatas}" varStatus="i">
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].vehicleType.code" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.code)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].vehicleType.name" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.name)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].length" value="${fn:escapeXml(entry.vehicleTypeQuantity.length)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].width" value="${fn:escapeXml(entry.vehicleTypeQuantity.width)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].height" value="${fn:escapeXml(entry.vehicleTypeQuantity.height)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].weight" value="${fn:escapeXml(entry.vehicleTypeQuantity.weight)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].numberOfAxis" value="${fn:escapeXml(entry.vehicleTypeQuantity.numberOfAxis)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].groundClearance" value="${fn:escapeXml(entry.vehicleTypeQuantity.groundClearance)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].adjustable" value="${fn:escapeXml(entry.vehicleTypeQuantity.adjustable)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].carryingLivestock" value="${fn:escapeXml(entry.vehicleTypeQuantity.carryingLivestock)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].carryingDangerousGoods" value="${fn:escapeXml(entry.vehicleTypeQuantity.carryingDangerousGoods)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].vehicleWithSidecarOrTrailer" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleWithSidecarOrTrailer)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].vehicleType.category" value="${fn:escapeXml(entry.vehicleTypeQuantity.vehicleType.category)}" />
		<form:input type="hidden" readonly="true" path="vehicleTypeQuantityList[${fn:escapeXml(i.index)}].qty" value="${fn:escapeXml(entry.vehicleTypeQuantity.qty)}" />
	</c:forEach>
	<%-- accessibility Data --%>
	<c:forEach var="entry" items="${accessibilities}" varStatus="i">
		<form:input type="hidden" readonly="true" path="accessibilitytDataList[${fn:escapeXml(i.index)}].code" value="${fn:escapeXml(entry.code)}" />
		<form:input type="hidden" readonly="true" path="accessibilitytDataList[${fn:escapeXml(i.index)}].type" value="${fn:escapeXml(entry.type)}" />
		<form:input type="hidden" readonly="true" path="accessibilitytDataList[${fn:escapeXml(i.index)}].passengerType" value="${fn:escapeXml(entry.passengerType)}" />
		<form:input type="hidden" readonly="true" path="accessibilitytDataList[${fn:escapeXml(i.index)}].passengerUid" value="${fn:escapeXml(entry.passengerUid)}" />
	</c:forEach>
	<%-- accessibility Data --%>
	<%-- specialServices Data --%>
    	<c:forEach var="entry" items="${specialServices}" varStatus="i">
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].code" value="${fn:escapeXml(entry.code)}" />
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].name" value="${fn:escapeXml(entry.name)}" />
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].passengerType" value="${fn:escapeXml(entry.passengerType)}" />
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].index" value="${fn:escapeXml(entry.index)}" />
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].description" value="${fn:escapeXml(entry.description)}" />
    		<form:input type="hidden" readonly="true" path="specialServiceRequestDataList[${fn:escapeXml(i.index)}].passengerUid" value="${fn:escapeXml(entry.passengerUid)}" />
    	</c:forEach>
    	<%-- specialServices Data --%>
	<c:forEach var="largeItemsBreakdownData" items="${itineraryPricingInfo.largeItemsBreakdownDataList}" varStatus="i">
		<form:input type="hidden" readonly="true" path="largeItems[${fn:escapeXml(i.index)}].code" value="${fn:escapeXml(largeItemsBreakdownData.code)}" />
		<form:input type="hidden" readonly="true" path="largeItems[${fn:escapeXml(i.index)}].name" value="${fn:escapeXml(largeItemsBreakdownData.name)}" />
		<form:input type="hidden" readonly="true" path="largeItems[${fn:escapeXml(i.index)}].quantity" value="${fn:escapeXml(largeItemsBreakdownData.quantity)}" />
	</c:forEach>
	<c:forEach var="entry" items="${itineraryPricingInfo.bundleTemplates}" varStatus="btIdx">
		<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProductBundleTemplateId" value="${fn:escapeXml(entry.fareProductBundleTemplateId)}" />
		<form:input class="bundleType" type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].bundleType" value="${fn:escapeXml(entry.bundleType)}" />
		<c:forEach var="fareProductEntry" items="${entry.fareProducts}" varStatus="fpIdx">
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].code" value="${fn:escapeXml(fareProductEntry.code)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].bookingClass" value="${fn:escapeXml(fareProductEntry.bookingClass)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].fareBasisCode" value="${fn:escapeXml(fareProductEntry.fareBasisCode)}" />
			<form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].fareProducts[${fn:escapeXml(fpIdx.index)}].fareProductType" value="${fn:escapeXml(fareProductEntry.fareProductType)}" />
		</c:forEach>

        <c:forEach var="transportOfferingEntry" items="${entry.transportOfferings}" varStatus="toIdx">
            <form:input class="transportOfferingcode" type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].code" value="${fn:escapeXml(transportOfferingEntry.code)}" />
            <fmt:formatDate value="${transportOfferingEntry.departureTime}" var="formattedDepartureDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
            <fmt:formatDate value="${transportOfferingEntry.arrivalTime}" var="formattedArrivalDate" type="both" pattern="MM/dd/yyyy HH:mm:ss" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].departureTime" value="${fn:escapeXml(formattedDepartureDate)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].departureTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.departureTimeZoneId)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].arrivalTime" value="${fn:escapeXml(formattedArrivalDate)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].arrivalTimeZoneId" value="${fn:escapeXml(transportOfferingEntry.arrivalTimeZoneId)}" />
            <form:input type="hidden" readonly="true" path="itineraryPricingInfo.bundleTemplates[${fn:escapeXml(btIdx.index)}].transportOfferings[${fn:escapeXml(toIdx.index)}].sector.code" value="${fn:escapeXml(transportOfferingEntry.sector.code)}" />
        </c:forEach>


	</c:forEach>
</form:form>
