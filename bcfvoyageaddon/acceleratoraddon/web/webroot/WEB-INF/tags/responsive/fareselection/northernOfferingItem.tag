<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="index" required="true" type="java.lang.Integer"%>
<%@ attribute name="rowresult" required="true" type="String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${pricedItinerary.available && pricedItinerary.originDestinationRefNumber == refNumber}">
    <div class="row p-card northern-sailing-card">
        <div class="col-lg-9 col-md-9 col-xs-12 border-right-divider">
            <c:set var="resultrowid" value="${rowresult}${index}" />
            <div class="row padding-left-10">
                <div class="col-lg-3 col-md-3 col-xs-3">
                    <c:set var="numberOfConnections" value="${fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings)}" />
                    <c:choose>
                        <c:when test="${numberOfConnections > 1}">
                            <c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
                            <c:set var="lastOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[(numberOfConnections-1)]}" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="firstOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
                            <c:set var="lastOffering" value="${firstOffering}" />
                        </c:otherwise>
                    </c:choose>
                    <spring:theme code="fareselection.depart" />
                    <h4 class="date">
                        <fmt:formatDate pattern="h:mm a" value="${firstOffering.departureTime}"/>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-3">
                    <div class="p-chart text-center">
                        <div class="pc-1 distance-time-text">
                            <c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.days'] && pricedItinerary.itinerary.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
                                    code="transport.offering.status.result.days" />
                            </c:if>
                            <c:if test="${not empty pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] && pricedItinerary.itinerary.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.hours'])}
                                                    <spring:theme code="transport.offering.status.result.hours" />
                            </c:if>
                            &nbsp;${fn:escapeXml(pricedItinerary.itinerary.duration['transport.offering.status.result.minutes'])}
                            <spring:theme code="transport.offering.status.result.minutes" />
                        </div>
                        <div class="distance-icon-line">
                            <span class="bcf bcf-icon-wave-line distance-wave-icon icon-blue"></span>
                            <div class="distance-line icon-blue"></div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-3 col-xs-3">
                     <spring:theme code="fareselection.arrive" />
                     <h4 class="date">
                        <fmt:formatDate pattern="h:mm a" value="${lastOffering.arrivalTime}" />
                     </h4>
                 </div>
                 <div class="col-lg-3 col-md-3 col-xs-3">
                    <c:forEach items="${pricedItinerary.itineraryPricingInfos}" var="percentage1">
                        <fmt:parseNumber var="i" type="number" value="${percentage1.totalFare.totalPrice.value}" />
                        <c:if test="${not empty i}">
                            <c:set var="min" value="${i}" />
                        </c:if>
                    </c:forEach>
                    <ul class="flight-details flight-details-stop sailing-ferry-name">
                        <li class="flight-number">
                        <c:forEach items="${pricedItinerary.itinerary.originDestinationOptions[0].distinctTransportVehicles}" var="transportVehicle" varStatus="status">
                   			<a href="/ship-info/${fn:escapeXml(transportVehicle.vehicleInfo.code)}" data-toggle="modal" data-target="#detailsModal">${fn:escapeXml(transportVehicle.vehicleInfo.name)}</a>
                   			<c:if test="${!status.last}">,&nbsp;</c:if>
               			</c:forEach>
                        </li>
                        <c:if test="${not empty pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].availability.code}">
                            <li class="flight-number" >
                                <spring:theme code="text.listsailing.ProductAvailabilityEnum.${fn:escapeXml(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].availability.code)}" />
                            </li>
                        </c:if>
                    </ul>
                    <c:if test='${!offeringIdx.last}'>
                        <span class="one-stop-divide y_fareResultStopDivide glyphicon glyphicon-transfer hidden"></span>
                    </c:if>
                 </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-xs-12 border-left m-border-left text-center northern-card-text">
            <c:choose>
                <%-- removing the subBucketCapacity check for now as it is either 0 or null from list sailing SIT service --%>
                <%-- <c:when test="${pricedItinerary.isBookable and !pricedItinerary.zeroSubBucketFreeCapacity }"> --%>
                <c:when test="${pricedItinerary.isBookable}">
                    <span class="fnt-14 northern-text-form">Total from</span>
                    <h4>
                        $<fmt:formatNumber type="number" maxFractionDigits="2" value="${min}" minFractionDigits="2" />
                    </h4>

                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${pricedItinerary.isBookable and pricedItinerary.zeroSubBucketFreeCapacity }">
                            <p class="text-red">
                                <b><spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" /><br> <spring:theme code="text.listsailing.bundle.fare.sold.out.online" text="Online" /> </b>
                            </p>
                            <p class="text-gray">
                                <b><spring:theme code="text.listsailing.fare.at.terminal.bundle.name" text="Fare at terminal" /><br> <format:price priceData="${pricedItinerary.fareAtTerminal}" /> </b>
                            </p>
                        </c:when>
                        <c:otherwise>
                            <p>
                                <spring:theme code="text.listsailing.bundle.fare.sold.out" text="Sold Out" />
                            </p>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-xs-12 p-0"><hr class="line"></div>
          <fareselection:northernOfferingListBundles pricedItinerary="${pricedItinerary}" refNumber="${refNumber}" name="refNumber-${refNumber}" />
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                	<div class="js-collapse-parent">
                        <div class="js-sailing-link" >
                            <c:if test="${numberOfConnections > 1}">
                                <c:set var="flightDetailsCollapseId" value="#collapse${pricedItinerary.id}" />
                                <c:set var="flightDetailsCollapse" value="collapse${pricedItinerary.id}" />
                                <a role="button" href="${fn:escapeXml(flightDetailsCollapseId)}"
                                aria-expanded="false" aria-controls="${fn:escapeXml(flightDetailsCollapse)}"
                                class="info-trigger detail-link p-0">
                                    <spring:theme code="fareselection.view.journey" /> <i class="bcf bcf-icon-down-arrow"></i>
                                </a>
                            </c:if>
                        </div>
                         <fareselection:journeyDetails pricedItinerary="${pricedItinerary}" noOfConnections="${numberOfConnections}" />
                   </div>
                </div>
    </div>
</c:if>
