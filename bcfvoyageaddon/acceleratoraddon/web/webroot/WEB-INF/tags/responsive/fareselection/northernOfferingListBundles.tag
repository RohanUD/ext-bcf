<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="dealFormPath" required="false" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${refNumber == outboundRefNumber}">
		<c:set var="pricedItineraryDateTime" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[fn:length(pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings) - 1].arrivalTime}" />
	</c:when>
	<c:otherwise>
		<c:set var="pricedItineraryDateTime" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
	</c:otherwise>
</c:choose>
<c:forEach items="${pricedItinerary.itineraryPricingInfos}" var="itineraryPricingInfo" varStatus="pricingIdx">
	<c:if test="${itineraryPricingInfo.bundleType != ecoBundleType && itineraryPricingInfo.bundleType != ecoPlusBundleType && itineraryPricingInfo.bundleType != businessBundleType}">
	            <c:choose>
                    <c:when test="${bcfFareFinderForm.readOnlyResults eq true}">
                        <fareselection:fareCalculatorItineraryPricingInfo dealFormPath="${dealFormPath}" itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}" pricedItineraryDateTime="${pricedItineraryDateTime}" name="${name}" />
                    </c:when>
                    <c:otherwise>
		                <fareselection:northernItineraryPricingInfo dealFormPath="${dealFormPath}" itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}" pricedItineraryDateTime="${pricedItineraryDateTime}" name="${name}" />
		            </c:otherwise>
		         </c:choose>
	</c:if>
</c:forEach>
<c:if test="${pricedItinerary.isBookable}">
<br>
<div class="row">
    <div class="col-md-offset-2 col-md-8 col-xs-12">
        <p class="sailing-italic-text margin-left-7"><spring:theme code="text.listsailing.changing.sailing.time.info" text="Changing your sailing time" /></p>
    </div>
</div>
<hr class="line"></c:if>

