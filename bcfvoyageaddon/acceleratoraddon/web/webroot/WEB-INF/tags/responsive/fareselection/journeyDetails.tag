<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="noOfConnections" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${noOfConnections > 1}">
		<c:set var="count" value="0" scope="page" />
		<c:set var="flightDetailsCollapse" value="collapse${pricedItinerary.id}" />
            <div id="${fn:escapeXml(flightDetailsCollapse)}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <c:forEach items="${pricedItinerary.itinerary.originDestinationOptions}" var="originDestinationOption" varStatus="optionIdx">
                    <c:forEach items="${originDestinationOption.transferTransportOfferings}" var="transferTransportOffering" varStatus="arIdx">
                        <c:forEach items="${transferTransportOffering.value}" var="transportOffering" varStatus="offeringIdx">
                        	<c:set var="count" value="${count + 1}" scope="page"/>
                            <div class="p-tab-content margin-top-20 view-journey-sec">
                                <div class="row">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="p-flow"></div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center ${count < noOfConnections ? 'dashed' : ''} mb-5">
                                        <div class="row mb-5">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mt-5">
                                                <div class="p-chart">
                                                    <div class="pc-top">
                                                        <b>${fn:escapeXml(transportOffering.sector.origin.location.name)}</b>
                                                    </div>
                                                    <div class="pc-1">
                                                        <spring:theme code="fareselection.depart" />
                                                    </div>
                                                    <div class="pc-2">
                                                        <fmt:formatDate pattern="h:mm a" value="${transportOffering.departureTime}" var ="formatDepartureTime" />
                                                         <c:set var="lowerCaseDepartureTime" value="${fn:toLowerCase(formatDepartureTime)}"/>
                                                         ${lowerCaseDepartureTime}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="p-chart">
                                                    <div class="pc-time journey-time-cntrl">
                                                        <c:if test="${not empty transportOffering.duration['transport.offering.status.result.days'] && transportOffering.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(transportOffering.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
                                                                code="transport.offering.status.result.days" />
                                                        </c:if>
                                                        <c:if test="${not empty transportOffering.duration['transport.offering.status.result.hours'] && transportOffering.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(transportOffering.duration['transport.offering.status.result.hours'])}
                                                <spring:theme code="transport.offering.status.result.hours" />
                                                        </c:if>
                                                        &nbsp;${fn:escapeXml(transportOffering.duration['transport.offering.status.result.minutes'])}
                                                        <spring:theme code="transport.offering.status.result.minutes" />
                                                        <c:if test="${fn:length(transportOffering.stopLocations) > 0}">
                                                            <c:set var="stopLocations">
                                                                <c:forEach items="${transportOffering.stopLocations}" var="stop" varStatus="stopIdx">
                                                        ${fn:escapeXml(stop.code)}${!stopIdx.last ? ',' : ''}
                                                    </c:forEach>
                                                            </c:set>
                                                            <c:set var="stopInfoMessage">
                                                                <spring:theme code="transport.offering.stop.info.msg" />
                                                            </c:set>
                                                            <span> <a href="#" class="info-tooltip" data-toggle="tooltip" title="${fn:escapeXml(stopInfoMessage)}&nbsp;${fn:escapeXml(stopLocations)}" tabindex="0">info</a>
                                                            </span>
                                                        </c:if>
                                                    </div>
                                                    <div class="pc-time-icon-wave my-2">
                                                       <span class="bcf bcf-icon-wave-line"></span>
                                                    </div>
                                                </div>
                                                <div class="">
                                                <div class="pc-time-icon">
                                                 <c:if test="${not empty transportOffering.transportVehicle.vehicleInfo.shipIcon}">
                                                        <img class='js-responsive-image' data-media='${transportOffering.transportVehicle.vehicleInfo.shipIcon}' alt='${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.shipIconAltText)}' title='${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.shipIconAltText)}' />
                                                   </c:if>
                                                </div>
                                                <div class="hidden">${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.code)}</div>
                                                <a href="/ship-info/${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.code)}" class="view-journey-img-text" data-toggle="modal" data-target="#detailsModal">${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.name)}</a>
                                                <div class="pc-time journey-time-cntrl">
                                                    <c:if test="${transportOffering.availability == 'SOLDOUT'}">
                                                        <spring:theme code="text.listsailing.ProductAvailabilityEnum.SOLDOUT" text="Cabin Sold Out" />
                                                    </c:if>
                                                    <c:if test="${transportOffering.availability == 'LIMITED'}">
                                                        <spring:theme code="text.listsailing.ProductAvailabilityEnum.LIMITED" text="Limited cabins available" />
                                                    </c:if>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mt-5">
                                                <div class="p-chart">
                                                    <div class="pc-top">
                                                        <b>${fn:escapeXml(transportOffering.sector.destination.location.name)}</b>
                                                    </div>
                                                    <div class="pc-1">
                                                        <spring:theme code="fareselection.arrive" />
                                                    </div>
                                                    <div class="pc-2">
                                                        <fmt:formatDate pattern="h:mm a" value="${transportOffering.arrivalTime}" var ="formatArrivalTime"  />
                                                        <c:set var="lowerCaseArrivalTime" value="${fn:toLowerCase(formatArrivalTime)}"/>
                                                        ${lowerCaseArrivalTime}
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row stop-over-sec">
                                     <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="${count < noOfConnections ? 'p-flow' : ''}"></div>
                                     </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center p-0 ${count < noOfConnections ? 'dashed' : ''} mb-5">
                                        <c:if test="${count < noOfConnections}">
                                            <c:set var="nextTransportVehicleCode" value="${fn:escapeXml(transferTransportOffering.value[offeringIdx.index+1].transportVehicle.vehicleInfo.code)}" />
                                            <div class="journey-stop-over">
                                                <c:choose>
                                                    <c:when test="${nextTransportVehicleCode eq transportOffering.transportVehicle.vehicleInfo.code}">
                                                        <spring:theme code="text.listsailing.northern.sailing.stop.over.at" text="Stop-over at" />&nbsp;${transportOffering.destinationLocationCity}&nbsp;
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div><spring:theme code="text.listsailing.northern.sailing.transfer.at" text="Transfer at" />&nbsp; ${transportOffering.destinationLocationCity}&nbsp;</div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                            <div class="layover-text">
                                                    <spring:theme code="fareselection.layover" />&nbsp;
                                                    <c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverDays > 0}"> 
                                                                                ${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverDays}
                                                                                 <spring:theme code="text.listsailing.northern.sailing.stop.over.d.text"/>
                                                                                  </c:if>
                                                    <c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverHour > 0}">
                                                                                 ${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverHour}
                                                                                 <spring:theme code="text.listsailing.northern.sailing.stop.over.h.text"/>
                                                                                 </c:if>
                                                    <c:if test="${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverMinutes > 0}">
                                                                                 ${pricedItinerary.itinerary.originDestinationOptions[0].layOverData[offeringIdx.index].layoverMinutes}
                                                                                 <spring:theme code="text.listsailing.northern.sailing.stop.over.m.text"/>
                                                                                 </c:if>
                                            </div>
                                            <div>
                                                <c:if test="${nextTransportVehicleCode ne transportOffering.transportVehicle.vehicleInfo.code}">
                                                    <div><spring:theme code="text.listsailing.northern.sailing.ferry.change.note" text="Note: Passengers will need to change ferry in" />&nbsp;${transportOffering.destinationLocationCity}.</div>
                                                </c:if>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>



                            </div>
                        </c:forEach>
                    </c:forEach>
                </c:forEach>
            </div>
	</c:when>
	<c:otherwise>
		<c:if test="${routeType eq 'LONG'}">
		<div class="col-xs-9 col-sm-12">
			<dl class="flight-details flight-details-stop col-xs-12">
				<dt class="mobile-show info-trigger y_fareResultInfoTrigger">
					<i class="bcf bcf-icon-info-solid"></i>
					<spring:theme code="fareselection.sailingdetails" />
				</dt>
				<dd class="flight-number hide-on-mobile col-sm-6">
					<ul>
						<li>
							<c:set var="currentTansportOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
							<span class="heading"><spring:theme code="fareselection.ship" /></span>&nbsp;${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.name)}
						</li>
					</ul>
				</dd>
			</dl>
		</div>
		</c:if>
	</c:otherwise>
</c:choose>
