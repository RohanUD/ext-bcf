<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="bcfFareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>
<%@ attribute name="isReturn" required="true" type="java.lang.Boolean"%>
<%@ attribute name="refNumber" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="summary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/summary"%>

<summary:ferryJourneyTripSummary/>
