<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><spring:htmlEscape defaultHtmlEscape="false" />
<section>
    <div>
        <div class="container waitlist-wrapper bg-gray text-center padding-top-30 padding-bottom-30 margin-bottom-40">
            <div class="row">
                  <div class="col-md-offset-3 col-md-6 col-xs-12">
                      <span class="bcf bcf-icon-notice-outline bcf-2x"></span><br />
                      <p class="sailing-info-text text-sm">
                      <c:forEach var="errorMessage" items="${errorMessages}">
                        <b><spring:theme text="${errorMessage}" /></b>
                      </c:forEach>
                    </p>

                        <c:if test="${not empty waitListLink}">
                         <p class="sailing-info-text text-sm">
                                <spring:theme code="label.listsailing.get.on.waitlist.description" text="Click below to get on the waitlist" />
                         </p>
                         <a href="${waitListLink}" target="_blank">
                            <button class="btn btn-secondary btn-block margin-bottom-20">
                                <spring:theme code="label.listsailing.get.on.waitlist.header" text="Waitlist form" />
                            </button>
                         </a>
                         </c:if>

                        <c:if test="${not empty travelAgentWaitListLink}">
                            <a href="${travelAgentWaitListLink}" target="_blank" class="agent-link"><spring:theme code="label.listsailing.travel.agent.waitlist.link"/> </a>
                        </c:if>
                  </div>
            </div>
        </div>
    </div>
</section>
