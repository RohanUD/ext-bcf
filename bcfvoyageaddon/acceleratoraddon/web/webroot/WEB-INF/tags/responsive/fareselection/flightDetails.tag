<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="noOfConnections" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:choose>
	<c:when test="${noOfConnections > 1}">
		<c:set var="flightDetailsCollapseId" value="#collapse${pricedItinerary.id}" />
		<c:set var="flightDetailsCollapse" value="collapse${pricedItinerary.id}" />

			<div class="js-collapse-parent" >
				<div class="text-right js-sailing-link" id="accordion_journeyDetails" >
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="${fn:escapeXml(flightDetailsCollapseId)}" aria-expanded="true" aria-controls="${fn:escapeXml(flightDetailsCollapse)}" class="info-trigger detail-link">
                        <spring:theme code="fareselection.view.itinerary" /> <i class="fa
                        fa-angle-down"></i>
                    </a>
				</div>

				<div id="${fn:escapeXml(flightDetailsCollapse)}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <c:forEach items="${pricedItinerary.itinerary.originDestinationOptions}" var="originDestinationOption" varStatus="optionIdx">
                        <c:forEach items="${originDestinationOption.transferTransportOfferings}" var="transferTransportOffering" varStatus="arIdx">
                        <c:if test="${arIdx.index != 0 }">
                            <p>Transfer Sailing at ${transferTransportOffering.value[0].sector.origin.location.name}</p>
                        </c:if>
                        <c:forEach items="${transferTransportOffering.value}" var="transportOffering" varStatus="offeringIdx">
                        <div class="p-tab-content margin-top-20 view-journey-sec">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 mt-5">
                                    <div class="p-flow"></div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center mb-5">
                                    <div class="row mb-5">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 mt-5">
                                            <div class="p-chart">
                                                <div class="pc-top"><b>${fn:escapeXml(transportOffering.sector.origin.location.name)}</b></div>
                                                <div class="pc-1"><spring:theme code="fareselection.depart"/></div>
                                                <div class="pc-2"><fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${transportOffering.departureTime}" /></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                                            <div class="p-chart">
                                                <div class="pc-time journey-time-cntrl">
                                                    <c:if test="${not empty transportOffering.duration['transport.offering.status.result.days'] && transportOffering.duration['transport.offering.status.result.days'] != 0}">${fn:escapeXml(transportOffering.duration['transport.offering.status.result.days'])}&nbsp;<spring:theme
                                                            code="transport.offering.status.result.days" />
                                                    </c:if>
                                                    <c:if test="${not empty transportOffering.duration['transport.offering.status.result.hours'] && transportOffering.duration['transport.offering.status.result.hours'] != 0}">${fn:escapeXml(transportOffering.duration['transport.offering.status.result.hours'])}&nbsp;
                                                        <spring:theme code="transport.offering.status.result.hours" />
                                                    </c:if>
                                                    &nbsp;${fn:escapeXml(transportOffering.duration['transport.offering.status.result.minutes'])}&nbsp;
                                                    <spring:theme code="transport.offering.status.result.minutes" />
                                                    <c:if test="${fn:length(transportOffering.stopLocations) > 0}">
                                                        <c:set var="stopLocations">
                                                            <c:forEach items="${transportOffering.stopLocations}" var="stop" varStatus="stopIdx">
                                                                ${fn:escapeXml(stop.code)}${!stopIdx.last ? ',' : ''}&nbsp;
                                                            </c:forEach>
                                                        </c:set>
                                                        <c:set var="stopInfoMessage">
                                                            <spring:theme code="transport.offering.stop.info.msg" />
                                                        </c:set>
                                                        <span> <a href="#" class="info-tooltip" data-toggle="tooltip" title="${fn:escapeXml(stopInfoMessage)}&nbsp;${fn:escapeXml(stopLocations)}" tabindex="0">info</a>
                                                        </span>
                                                    </c:if>
                                                </div>
                                                <div class="pc-time-icon-wave my-2">--------- <span class="bcf bcf-icon-wave-line"></span> ---------</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 mt-5">
                                            <div class="p-chart">
                                                <div class="pc-top"><b>${fn:escapeXml(transportOffering.sector.destination.location.name)}</b></div>
                                                <div class="pc-1"><spring:theme code="fareselection.arrive"/></div>
                                                <div class="pc-2"><fmt:formatDate pattern="${pricedItineraryDateFormat}" value="${transportOffering.arrivalTime}" /></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mt-5 m-margin-top-20">
                                                <div class="pc-time-icon"> <span class="icon-ferry"></span></div>
                                                <div class="pc-time-text"><spring:theme code="fareselection.ferry"/></div>
                                                <ul class="flight-details flight-details-stop col-xs-12">
                                                    <li class="flight-number col-sm-12 test">
                                                        <span class="heading">daman<spring:theme code="fareselection.ship" /></span>&nbsp;${fn:escapeXml(transportOffering.transportVehicle.vehicleInfo.name)}
                                                    </li>
                                                </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <c:if test="${offeringIdx.index < (fn:length(transferTransportOffering.value) - 1)}">
                            <div class="row stop-over-sec">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 mt-5">
                                    <div class="p-flow"></div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center mb-5">
                                <div class="dashed"></div>
                                    <div class="journey-stop-over">
                                    &nbsp;<spring:theme code="text.listsailing.northern.sailing.stop.over.at" />
                                        &nbsp;${arIdx.index==0 ?'':','} ${transportOffering.destinationLocationCity}&nbsp;
                                    </div>
                                    <div class="layover-text">
                                    <p>
                                        <spring:theme code="fareselection.layover" />&nbsp;
                                        <c:set var="layOverTime" value="0 M"/>
                                        <c:if test="${originDestinationOption.layOverData[arIdx.index].layoverDays > 0}"> 
                                          <c:set var="layOverTime" value="${originDestinationOption.layOverData[arIdx.index].layoverDays} D"/>
                                         </c:if>
                                        <c:if test="${originDestinationOption.layOverData[arIdx.index].layoverHour > 0}"> 
                                          <c:set var="layOverTime" value="${originDestinationOption.layOverData[arIdx.index].layoverHour} H"/>
                                         </c:if>
                                        <c:if test="${originDestinationOption.layOverData[arIdx.index].layoverMinutes > 0}"> 
                                          <c:set var="layOverTime" value="${originDestinationOption.layOverData[arIdx.index].layoverMinutes} M"/>
                                         </c:if>
                                         &nbsp; ${layOverTime}
                                    </p>
                                    </div>
                                    <div class="dashed"></div>
                                </div>

                            </div>
                        </c:if>
                        </div>
					    </c:forEach>
						</c:forEach>
						</c:forEach>
                </div>
    </div>
	</c:when>
	<c:otherwise>
		<div class="col-xs-12 col-sm-12">
			<dl class="flight-details flight-details-stop col-xs-12">
				<dd class="flight-number hide-on-mobile col-sm-6">
				<ul>
					<li>
					<c:set var="currentTansportOffering" value="${pricedItinerary.itinerary.originDestinationOptions[0].transportOfferings[0]}" />
					${fn:escapeXml(currentTansportOffering.transportVehicle.vehicleInfo.name)}
					</li>
				</ul>
				</dd>
			</dl>
		</div>
	</c:otherwise>
</c:choose>
