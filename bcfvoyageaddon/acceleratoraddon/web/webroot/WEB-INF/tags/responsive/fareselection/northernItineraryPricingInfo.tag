<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="itineraryPricingInfo" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryPricingInfoData"%>
<%@ attribute name="pricedItinerary" required="true" type="de.hybris.platform.commercefacades.travel.PricedItineraryData"%>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ attribute name="pricedItineraryDateTime" required="true" type="java.util.Date"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/dealdetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="dealFormPath" required="false" type="java.lang.String"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="showDealForm" value="false" />
<c:if test="${not empty dealFormPath}">
	<c:set var="showDealForm" value="true" />
</c:if>
<c:if test="${not empty itineraryPricingInfo && itineraryPricingInfo.available && itineraryPricingInfo.totalFare.totalPrice.value > 0.0}">
<ul class="list-group-custom">
    <li class="vp-box ${itineraryPricingInfo.promotional ? 'promotional-price' : ''}
    ${itineraryPricingInfo
    .selected ? 'selected' : ''}">
		<div class="row">
			<div class="col-md-offset-2 col-md-4 col-xs-12">
			    <c:choose>
                    <c:when test="${pricedItinerary.itinerary.route.fullyPrepaid}">
                            <h4>
                                <b><spring:theme code="text.listsailing.bundle.type.${itineraryPricingInfo.bundleType}.fullyprepaid" text="Standard – Fully Prepaid" /></b>
                            </h4>
                    </c:when>
                    <c:otherwise>
                            <h4>
                                <b><spring:theme code="text.listsailing.bundle.type.${itineraryPricingInfo.bundleType}" text="Standard fare" /></b>
                            </h4>
                    </c:otherwise>
                </c:choose>
				<c:choose>
					<c:when test="${itineraryPricingInfo.bundleType eq 'FARE_AT_TERMINAL'}">
							<spring:theme code="text.listsailing.bundle.type.without.reservation.description" text="Standard fare without reservation" />
					</c:when>
					<c:when test="${pricedItinerary.itinerary.route.fullyPrepaid}">
					        <spring:theme code="text.listsailing.bundle.type.with.fullyprepaid" />
					</c:when>
					<c:otherwise>
							<spring:theme code="text.listsailing.bundle.type.with.reservation.description" text="With reservation" />
					</c:otherwise>
				</c:choose>
			
            <c:if test="${!pricedItinerary.itinerary.route.fullyPrepaid}">
                    <c:forEach var="bundleTemplate" items="${itineraryPricingInfo.bundleTemplates }" varStatus="bundleIdx">${bundleTemplate.description }</c:forEach>
            </c:if>
            </div>

			<div class="col-md-offset-2 col-md-2 col-xs-12 text-center">
				<c:choose>
					<c:when test="${itineraryPricingInfo.bundleType eq 'FARE_AT_TERMINAL'}">
						<p>
							<format:price priceData="${pricedItinerary.fareAtTerminal}" />
						</p>
					</c:when>
					<c:otherwise>
						<c:if test="${bcfFareFinderForm.readOnlyResults ne true}">
                                <c:if test="${pricedItinerary.departedSailing ne true}">
                                    <c:choose>
                                        <c:when test="${showDealForm == false}">
                                            <c:choose>
                                                <c:when test="${not empty itineraryPricingInfo.totalFare.priceDifference}">
                                                    <h5 class="mt-0 mb-0"><span class="price-desc nrth-sailing-white-bx">${fn:escapeXml(itineraryPricingInfo.totalFare.priceDifference.formattedValue)}</span></h5>
                                                </c:when>
                                                <c:otherwise>
                                                    <h5 class="mt-0 mb-0"><span class="price-desc nrth-sailing-white-bx">${itineraryPricingInfo.available && itineraryPricingInfo.totalFare.totalPrice.value > 0.0 ? itineraryPricingInfo.totalFare.totalPrice.formattedValue : '&mdash;'}</span></h5>
                                                </c:otherwise>
                                            </c:choose>
											<div class="btn-group-toggle" data-toggle="buttons">
											<c:if test="${pricedItinerary.isBookable and itineraryPricingInfo.available}">
												<label class="btn btn-primary nrth-sailing-selct-btn btn-block js-radio-select active" > <input
													type="radio" name="${fn:escapeXml(name)}"
													id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}"
													${itineraryPricingInfo.selected ? 'checked' : ''}
													value="${fn:escapeXml(pricedItineraryDateTime.time)}"
													class="y_fareResultSelect ${routeType}" autocomplete="off" data-nexturl="${contextPath}${fn:escapeXml(nextURL)}" /> <spring:theme
														code="text.fareselection.button.select" text="Select" />
												</label>
											</c:if>
											</div>
                                        </c:when>
                                        <c:otherwise>
                                            <form:radiobutton path="${dealFormPath}.itineraryPricingInfo.selected" disabled="${!pricedItinerary.isBookable or !itineraryPricingInfo.available ? 'disabled' : ''}" checked="${itineraryPricingInfo.selected ? 'checked' : ''}" name="${fn:escapeXml(name)}"
                                                id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}" value="${!itineraryPricingInfo.selected}" class="y_fareResultSelect" />
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                        </c:if>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
            <c:if test="${itineraryPricingInfo.totalFare.totalPrice.value > 0.0}">
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                    <div class="js-collapse-parent">
                        <div class="js-sailing-link" >
                            <p class="margin-top-20"><a role="button" href="#sailing-inner" aria-expanded="false" aria-controls="sailing-inner" class="detail-link p-0">
                            <spring:theme code="text.listsailing.bundle.price.breakdown" text="Price breakdown" />
                            <i class="bcf bcf-icon-down-arrow"></i>
                            </a>
                            </p>
                            <div id="sailing-inner" class="panel-collapse collapse">
                               <div class="row">
                                   <div class="col-md-11 col-xs-12 margin-top-30 paddingRightOff">
                                       <div id="price-box">
                                           <div class="row">
                                               <c:forEach var="ptcBreakDownData" items="${itineraryPricingInfo.ptcFareBreakdownDatas}" varStatus="ptcIdx">
                                                   <c:if test="${ptcBreakDownData.passengerTypeQuantity.quantity ne 0}">
                                                       <div class="list-unstyled col-xs-9">${ptcBreakDownData.passengerTypeQuantity.quantity}&nbsp;x&nbsp;${ptcBreakDownData.passengerTypeQuantity.passengerType.name}</div>
                                                       <div class="list-unstyled col-xs-3 text-right">
                                                           <p><format:price priceData="${ptcBreakDownData.totalPrice}" /></p>
                                                       </div>
                                                   </c:if>
                                               </c:forEach>
                                           </div>
                                           <div class="row">
                                               <c:forEach var="vehicleFareBreakdownData" items="${itineraryPricingInfo.vehicleFareBreakdownDatas}" varStatus="ptcIdx">
                                                   <c:if test="${vehicleFareBreakdownData.vehicleTypeQuantity.qty ne 0}">
                                                       <div class="list-unstyled col-xs-9">${vehicleFareBreakdownData.vehicleTypeQuantity.qty}&nbsp;x&nbsp;${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.name}</div><br>${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.description}
                                                       <div class="list-unstyled col-xs-3 text-right">
                                                           <c:forEach var="fareDetail" items="${vehicleFareBreakdownData.fareInfos.fareDetails}" varStatus="fareDetailIdx">
                                                              <p><format:price priceData="${fareDetail.fareProduct.price}" /></p>
                                                           </c:forEach>
                                                       </div>
                                                   </c:if>
                                               </c:forEach>
                                           </div>
												<div class="row">
													<c:forEach var="accessibilityBreakdownData" items="${itineraryPricingInfo.accessibilityBreakdownDataList}">
														<c:if test="${accessibilityBreakdownData.quantity ne 0}">
															<div class="list-unstyled col-xs-9">${accessibilityBreakdownData.quantity}&nbsp;x&nbsp;${accessibilityBreakdownData.name}</div>
															<div class="list-unstyled col-xs-3 text-right">
																<p>
																	<format:price priceData="${accessibilityBreakdownData.price}" />
																</p>
															</div>
														</c:if>
													</c:forEach>
												</div>
												<div class="row">
                                               <c:forEach var="largeItemsBreakdownData" items="${itineraryPricingInfo.largeItemsBreakdownDataList}">
                                                   <c:if test="${largeItemsBreakdownData.quantity ne 0}">
                                                       <div class="list-unstyled col-xs-9">${largeItemsBreakdownData.quantity}&nbsp;x&nbsp;${largeItemsBreakdownData.name}</div>
                                                       <div class="list-unstyled col-xs-3 text-right">
                                                           <p><format:price priceData="${largeItemsBreakdownData.price}" /></p>
                                                       </div>
                                                   </c:if>
                                               </c:forEach>
                                           </div>
                                            <div class="row">
                                                <c:forEach var="otherCharge" items="${itineraryPricingInfo.otherCharges}">
                                                    <c:if test="${otherCharge.quantity ne 0}">
                                                        <div class="list-unstyled col-xs-9">
                                                            <spring:theme code="text.listsailing.price.itinerary.other.charges" text="Other charges" />
                                                        </div>
                                                        <div class="list-unstyled col-xs-3 text-right">
                                                            <p>
                                                                <format:price priceData="${otherCharge.price}" />
                                                            </p>
                                                        </div>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                            <div class="row">
                                               <div class="list-unstyled col-xs-9">
                                                   <h6><spring:theme code="text.listsailing.northern.sailing.bundle.total" text="Total fare " />
                                                        <span class="normal-weight"> <spring:theme code="text.listsailing.northern.sailing.bundle.inclusive.tax" text="(inc.taxes)" /></span>

                                                                                              <spring:message code="text.fareselection.total.fare.message" var="tooltipText" />
                                               		<span class="info-tooltip popoverThis" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="${fn:escapeXml(tooltipText)}">
                                                               <span class="bcf bcf-icon-info-solid"></span>
                                                       </span>
                                                                                              </h6>
                                                                                              
                                               </div>
                                               
                                               <div class="list-unstyled col-xs-3 text-right"><h6>
                                                   <c:choose>
                                                       <c:when test="${not empty itineraryPricingInfo.totalFare.priceDifference}">
                                                           <span class="price-desc">${fn:escapeXml(itineraryPricingInfo.totalFare.priceDifference.formattedValue)}</span>
                                                       </c:when>
                                                       <c:otherwise>
                                                           <span class="price-desc">${itineraryPricingInfo.available && itineraryPricingInfo.totalFare.totalPrice.value > 0 ? itineraryPricingInfo.totalFare.totalPrice.formattedValue : '&mdash;'}</span>
                                                       </c:otherwise>
                                                   </c:choose></h6>
                                               </div>
                                           </div>
                                           <c:if test="${pricedItinerary.itinerary.route.fullyPrepaid ne true}">
                                            <div class="row hidden">
                                                <div class="list-unstyled col-xs-9">
                                                    <spring:theme code="text.listsailing.price.itinerary.due.now" text="Due now" />
                                                </div>
                                                <div class="list-unstyled col-xs-3 text-right">
                                                    <p>
                                                        <format:price priceData="${itineraryPricingInfo.amountToPayNow}" />
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row hidden">
                                                <div class="list-unstyled col-xs-9">
                                                    <spring:theme code="text.listsailing.price.itinerary.due.at.terminal" text="Due at terminal" />
                                                </div>
                                                <div class="list-unstyled col-xs-3 text-right">
                                                    <p>
                                                        <format:price priceData="${itineraryPricingInfo.amountDueAtTerminal}" />
                                                    </p>
                                                </div>
                                            </div>
                                            </c:if>
                                        </div>
                                       <c:if test="${not empty dealBundleTemplateId}">
                                           <span class="selected"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                           </span>
                                           <span class="select"> <spring:theme code="text.accommodation.details.accommodation.select.price" text="select" />
                                           </span>
                                       </c:if>
                                       <c:choose>
                                           <c:when test="${not empty dealFormPath}">
                                               <dealdetails:addBundleToCartForm isDynamicPackage="false" addBundleToCartUrl="${addBundleToCartUrl}" dealFormPath="${dealFormPath}" itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}" />
                                           </c:when>
                                           <c:otherwise>
                                               <fareselection:addBundleToCartForm isDynamicPackage="false" addBundleToCartUrl="${addBundleToCartUrl}" itineraryPricingInfo="${itineraryPricingInfo}" pricedItinerary="${pricedItinerary}" />
                                           </c:otherwise>
                                       </c:choose>
                                   <c:if test="${pricedItinerary.isBookable and itineraryPricingInfo.available and itineraryPricingInfo.bundleType ne 'FARE_AT_TERMINAL'}">
                                   <div class="row">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-lg-8 col-md-8 col-xs-12">
                                   <div class="btn-group-toggle margin-top-20" data-toggle="buttons">
                                        <label class="btn btn-primary btn-block js-radio-select active" > <input
                                            type="radio" name="${fn:escapeXml(name)}"
                                            id="y_frSelect-${fn:escapeXml(pricedItinerary.id)}-${fn:escapeXml(itineraryPricingInfo.bundleType)}"
                                            ${!pricedItinerary.isBookable ? 'disabled' : ''}
                                            ${itineraryPricingInfo.available ? '' : 'disabled'}
                                            ${itineraryPricingInfo.selected ? 'checked' : ''}
                                            value="${fn:escapeXml(pricedItineraryDateTime.time)}"
                                            class="y_fareResultSelect ${routeType}" autocomplete="off" data-nexturl="${contextPath}${fn:escapeXml(nextURL)}" /> <spring:theme
                                                code="text.fareselection.button.select.and.continue" text="Select" />
                                        </label>
                                    </div>
                                    </div>
                                    </div>
                                    </c:if>
                                </div>
                               </div>
                            </div>
                        </div>
                   </div>
                </div>
              </div>  
             </c:if>
	</li>
</ul>
</c:if>
