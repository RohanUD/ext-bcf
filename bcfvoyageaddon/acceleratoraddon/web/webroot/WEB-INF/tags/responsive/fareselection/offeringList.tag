<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="fareSelection" required="true" type="de.hybris.platform.commercefacades.travel.FareSelectionData"%>
<%@ attribute name="refNumber" required="true" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="ferryselection" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packageferryselection"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:choose>
	<c:when test="${refNumber == outboundRefNumber}">
		<c:set var="sortby" value="outbound-sort-by" />
		<c:set var="rowresult" value="outbound-row-result-" />
	</c:when>
	<c:when test="${refNumber == inboundRefNumber}">
		<c:set var="sortby" value="inbound-sort-by" />
		<c:set var="rowresult" value="inbound-row-result-" />
	</c:when>
</c:choose>
<c:set var="noOfOptions" value="${refNumber == outboundRefNumber ? noOfOutboundOptions : noOfInboundOptions }" />
<c:set var="dates" value="${refNumber == outboundRefNumber ? outboundDates : outboundDates }" />
<c:set var="linksForTabs" value="${refNumber == outboundRefNumber ? outboundTabLinks : inboundTabLinks }" />
<div class="nav-tabs-wrapper y_fareResultNavTabs">
	<%-- Tab panes --%>
	<div class="y_fareResultTabContent tab-content">
		<c:forEach items="${dates}" var="tabDate" varStatus="tabIdx">
			<c:choose>
				<c:when test="${tabIdx.index != 2}">
					<div class="tab-pane fade">
						<p>
							<spring:theme code="fareselection.noofferings" />
						</p>
					</div>
				</c:when>
				<c:otherwise>
					<div class="table-responsive">
						<c:choose>
							<c:when test="${empty fareSelection.pricedItineraries}">
								<div>
									<spring:theme code="fareselection.noofferings" />
								</div>
							</c:when>
							<c:otherwise>
							    <c:if test="${isReservable or not empty fareCalculatorPageUrl}">
                                    <c:forEach items="${fareSelection.pricedItineraries}" var="pricedItinerary" varStatus="idx">
                                        <c:choose>
                                            <c:when test="${routeType eq 'LONG'}">
                                                <fareselection:northernOfferingItem pricedItinerary="${pricedItinerary}"  refNumber="${refNumber}" index="${idx.index}" rowresult="${rowresult}" />
                                            </c:when>
                                            <c:otherwise>
                                                <fareselection:southernOfferingItem pricedItinerary="${pricedItinerary}"  refNumber="${refNumber}" index="${idx.index}" rowresult="${rowresult}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:if>
                               <c:if test="${ not empty firstItinerary && isOpenTicketAllowed}">
                                    <ferryselection:openticket fareSelection="${fareSelection}" refNumber="${refNumber}"/>
                                </c:if>

								<fareselection:addBundleToCartValidationModal />
								<fareselection:addBundleToCartErrorModal />
							</c:otherwise>
						</c:choose>
					</div>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</div>
</div>
